(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["suspension-suspension-module"],{

/***/ "./src/app/model/Suspension/ExtensionDetails.ts":
/*!******************************************************!*\
  !*** ./src/app/model/Suspension/ExtensionDetails.ts ***!
  \******************************************************/
/*! exports provided: ExtensionDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionDetails", function() { return ExtensionDetails; });
var ExtensionDetails = /** @class */ (function () {
    function ExtensionDetails() {
        this.IsEditable = false;
    }
    return ExtensionDetails;
}());



/***/ }),

/***/ "./src/app/model/Suspension/JoiningDetails.ts":
/*!****************************************************!*\
  !*** ./src/app/model/Suspension/JoiningDetails.ts ***!
  \****************************************************/
/*! exports provided: JoiningDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoiningDetails", function() { return JoiningDetails; });
var JoiningDetails = /** @class */ (function () {
    function JoiningDetails() {
        this.IsEditable = false;
    }
    return JoiningDetails;
}());



/***/ }),

/***/ "./src/app/model/Suspension/RegulariseDetails.ts":
/*!*******************************************************!*\
  !*** ./src/app/model/Suspension/RegulariseDetails.ts ***!
  \*******************************************************/
/*! exports provided: RegulariseDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegulariseDetails", function() { return RegulariseDetails; });
var RegulariseDetails = /** @class */ (function () {
    function RegulariseDetails() {
    }
    return RegulariseDetails;
}());



/***/ }),

/***/ "./src/app/model/Suspension/RevocationDetails.ts":
/*!*******************************************************!*\
  !*** ./src/app/model/Suspension/RevocationDetails.ts ***!
  \*******************************************************/
/*! exports provided: RevocationDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RevocationDetails", function() { return RevocationDetails; });
var RevocationDetails = /** @class */ (function () {
    function RevocationDetails() {
    }
    return RevocationDetails;
}());



/***/ }),

/***/ "./src/app/model/Suspension/SuspensionDetails.ts":
/*!*******************************************************!*\
  !*** ./src/app/model/Suspension/SuspensionDetails.ts ***!
  \*******************************************************/
/*! exports provided: SuspensionDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspensionDetails", function() { return SuspensionDetails; });
var SuspensionDetails = /** @class */ (function () {
    function SuspensionDetails() {
        this.IsEditable = false;
    }
    return SuspensionDetails;
}());



/***/ }),

/***/ "./src/app/services/Suspension/Suspension_service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/Suspension/Suspension_service.ts ***!
  \***********************************************************/
/*! exports provided: SuspensionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspensionService", function() { return SuspensionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var SuspensionService = /** @class */ (function () {
    function SuspensionService(http, config) {
        this.http = http;
        this.config = config;
    }
    SuspensionService.prototype.GetAllDesignation = function (Id) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('id', Id);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetAllDesignation", { params: params });
    };
    SuspensionService.prototype.GetAllEmpOnSelectedDesignation = function (desigId, isCheckerLogin, comName) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('desigId', desigId).set('isCheckerLogin', isCheckerLogin).set('comName', comName);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetAllEmployees", { params: params });
    };
    SuspensionService.prototype.SavedNewSuspensionDetails = function (objsus) {
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/SavedNewSuspensionDetails", objsus, { responseType: 'text' });
    };
    SuspensionService.prototype.SavedExtensionDetails = function (objsus) {
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/SavedExtensionDetails", objsus, { responseType: 'json' });
    };
    SuspensionService.prototype.SavedRevocationDetails = function (objRevok) {
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/SavedRevocationDetails", objRevok, { responseType: 'json' });
    };
    SuspensionService.prototype.GetSuspensionDetails = function (empId, isCheckerLogin, comName) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('empCode', empId).set('isCheckerLogin', isCheckerLogin).set('comName', comName);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetSuspensionDetails", { params: params });
    };
    SuspensionService.prototype.DeleteSuspensionDetails = function (id, compName) {
        var param = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('id', id).set('compName', compName);
        return this.http.delete("" + this.config.api_base_url + this.config.suspensionControllerName + "/DeleteSuspensionDetails", { params: param, responseType: 'json' });
    };
    SuspensionService.prototype.VerifiedAndRejectedSusDetails = function (id, status, reason) {
        var param = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('idAndcompName', id).set('status', status).set('reason', reason);
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/VerifiedAndRejectedSuspension", null, { params: param, responseType: 'json' });
    };
    SuspensionService.prototype.GetRevocationDetails = function (selectedvalue, empId, isCheckerLogin) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('susId', selectedvalue).set('empId', empId).set('isCheckerLogin', isCheckerLogin);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetRevocationDetails", { params: params });
    };
    SuspensionService.prototype.SavedJoiningDetails = function (objJoining) {
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/SavedJoiningDetails", objJoining, { responseType: 'json' });
    };
    SuspensionService.prototype.GetJoiningDetails = function (selectedvalue, isCheckerLogin) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('joinId', selectedvalue).set('isCheckerLogin', isCheckerLogin);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetJoiningDetails", { params: params });
    };
    SuspensionService.prototype.SavedRegulariseDetails = function (objRegular) {
        return this.http.post("" + this.config.api_base_url + this.config.suspensionControllerName + "/SavedRegulariseDetails", objRegular, { responseType: 'json' });
    };
    SuspensionService.prototype.GetRegulariseDetails = function (selectedvalue, isCheckerLogin) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('joiningId', selectedvalue).set('isCheckerLogin', isCheckerLogin);
        return this.http.get("" + this.config.api_base_url + this.config.suspensionControllerName + "/GetRegulariseDetails", { params: params });
    };
    SuspensionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], SuspensionService);
    return SuspensionService;
}());



/***/ }),

/***/ "./src/app/suspension/extension/extension.component.css":
/*!**************************************************************!*\
  !*** ./src/app/suspension/extension/extension.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1c3BlbnNpb24vZXh0ZW5zaW9uL2V4dGVuc2lvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/suspension/extension/extension.component.html":
/*!***************************************************************!*\
  !*** ./src/app/suspension/extension/extension.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select #singleSelect placeholder=\"Select Designation\" [(value)]=\"selectedvalue\"\r\n                      (selectionChange)=\"ddlDesignationChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempDesignation\" [value]=\"item.desigId\">{{ item.desigDesc }}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Employee:</label>\r\n      <!--<button mat-mini-fab color=\"primary\" class=\"go-btn\">Go</button>-->\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"select employee\" (selectionChange)=\"ddlEmployeeChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempModel\" [value]=\"item.empCd\">\r\n              {{ item.empName }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--------------- Left Form --------------->\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel #exPanel>\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Enter New Suspension Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n\r\n    <form [formGroup]=\"SuspensionDetailsForm\" #formCreation=\"ngForm\" ngNativeValidate\r\n          (ngSubmit)=\"SavedSuspensionDetails(objempExtDetails)\">\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"FromDate\" name=\"FromDate\" [max]=\"todayDate\" [(ngModel)]=\"objempExtDetails.FromDate\"\r\n                 placeholder=\"From Date\" formControlName=\"FromDate\" readonly required />\r\n          <mat-datepicker-toggle matSuffix [for]=\"FromDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #FromDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order Number\" name=\"OrderNo\" [(ngModel)]=\"objempExtDetails.OrderNo\"\r\n                 formControlName=\"OrderNo\" pattern=\"[-(),/0-9a-zA-Z \\\\]*\" required />\r\n                 <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Suspension Period (Enter No of Days)\"\r\n                 (input)=\"AutoFillDate($event.target.value,false)\" type=\"number\" name=\"SusPeriod\" formControlName=\"SusPeriod\"\r\n                 [(ngModel)]=\"objempExtDetails.SusPeriod\" pattern=\"[0-9]*\" />\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!-- <input matInput [matDatepicker]=\"ValidUptoDate\" name=\"ValidUptoDate\" disabled [value]=\"02/02/2019\"\r\n        placeholder=\"Valid Upto\" readonly />\r\n      <mat-datepicker-toggle matSuffix [for]=\"ValidUptoDate\"></mat-datepicker-toggle>\r\n      <mat-datepicker #ValidUptoDate></mat-datepicker> -->\r\n          <input matInput placeholder=\"Valid Upto\" name=\"ValidUptoDate\" disabled [value]=\"validUptoDate\" />\r\n        </mat-form-field>\r\n      </div>\r\n      <!-- <div class=\"col-sm-12 col-md-6 col-lg-6 line-height-57\">\r\n    <mat-checkbox (value)=\"objempExtDetails.IsTillOrder\" formControlName=\"IsTillOrder\"\r\n      [(ngModel)]=\"objempExtDetails.IsTillOrder\" class=\"chk-single\" name=\"IsTillOrder\" color=\"primary\"\r\n      (change)=\"chkTillFurtherOrder($event)\">Till further Orders</mat-checkbox>\r\n  </div> -->\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" name=\"OrderDate\" [(ngModel)]=\"objempExtDetails.OrderDate\"\r\n                 placeholder=\"Order Date\" formControlName=\"OrderDate\" [max]=\"todayDate\" readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"% of Salary during Suspension Period\" maxlength=\"2\" min=\"1\" max=\"99\"\r\n                 type=\"number\" name=\"SalaryPercent\" required formControlName=\"SalaryPercent\" pattern=\"[0-9]*\"\r\n                 [(ngModel)]=\"objempExtDetails.SalaryPercent\" />\r\n          <mat-error>Please enter a value between 50 and 75</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4 mt-10\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!--<textarea matInput placeholder=\"Name\" required></textarea>-->\r\n          <textarea matInput placeholder=\"Reason\" name=\"Remarks\" [(ngModel)]=\"objempExtDetails.Remarks\"\r\n                    formControlName=\"Remarks\" pattern=\"[-(),/a-zA-Z \\\\]*\" required></textarea>\r\n                    <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"isRejected\" class=\"col-sm-12 col-md-4 col-lg-4 mt-10\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Rejected Reason\" name=\"RejectedReason\" [(ngModel)]=\"objempExtDetails.RejectedReason\"\r\n                    formControlName=\"RejectedReason\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"isViewDetails\">\r\n        <div *ngIf=\"this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\"\r\n                  (click)=\"VarifiedSus(_Id,false);hooCheckerLogin = !hooCheckerLogin\"\r\n                  [disabled]=\"SuspensionDetailsForm.invalid\">\r\n            {{_btnText}}\r\n          </button>\r\n          <!-- <button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button> -->\r\n          <button class=\"btn btn-primary\" type=\"button\"\r\n                  (click)=\"RejectedSus(_Id,false);hooCheckerLogin = !hooCheckerLogin\"\r\n                  [disabled]=\"SuspensionDetailsForm.invalid\">\r\n            {{_btnUpdateText}}\r\n          </button>\r\n        </div>\r\n        <div *ngIf=\"!this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"SuspensionDetailsForm.invalid || disableButton\">{{_btnText}}</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>\r\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"ForwardToChecker(objempExtDetails)\"\r\n                  [disabled]=\"SuspensionDetailsForm.invalid || disableButton\">\r\n            {{_btnUpdateText}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Extension Details</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\" [disabled]=\"orderValidFlag\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <!--tabel 2 start-->\r\n    <div *ngIf=\"!this.isCheckerLogin\">\r\n      <div class=\"fom-title mt-40 \">Select Old Suspension Order to enter Extension Details</div>\r\n      <table mat-table [(dataSource)]=\"dataSourceHistory\" #t2Sort=\"matSort\" matSort\r\n             class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- Order No. -->\r\n        <ng-container matColumnDef=\"OrderNo\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Number</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderNo }}</td>\r\n        </ng-container>\r\n        <!-- Order Date -->\r\n        <ng-container matColumnDef=\"OrderDate\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderDate  | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- From Date -->\r\n        <ng-container matColumnDef=\"FromDate\">\r\n          <th mat-header-cell *matHeaderCellDef>From Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.fromDate  | date: 'dd/MM/yyyy' }}</td>\r\n        </ng-container>\r\n        <!-- To Date Order No. -->\r\n        <ng-container matColumnDef=\"ToDate\">\r\n          <th mat-header-cell *matHeaderCellDef>To Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.toDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- No. of Days -->\r\n        <ng-container matColumnDef=\"Remarks\">\r\n          <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.remarks}}</td>\r\n        </ng-container>\r\n        <!---Status-->\r\n        <ng-container matColumnDef=\"Status\">\r\n          <th mat-header-cell *matHeaderCellDef>Status</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{element.status}}</td>\r\n        </ng-container>\r\n        <!--status end-->\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"historyTableColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let element; columns: historyTableColumns\" (click)=\"rowHighlight(element)\"\r\n            [style.background]=\"highlightedRows.indexOf(element) != -1 ?'lightgreen' : ''\"></tr>\r\n      </table>\r\n      <mat-paginator #paginatorTable2 [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n      </mat-paginator>\r\n    </div>\r\n    <div>\r\n      <button *ngIf=\"this.isCheckerLogin\" mat-button class=\"btn-primary\">\r\n        View Forwarded Suspension Order\r\n        Details\r\n      </button>\r\n      <button *ngIf=\"!this.isCheckerLogin\" mat-button class=\"btn-primary\" (click)=\"ShowSuspensionDetails(true)\">\r\n        View New\r\n        Captured Extension Details\r\n      </button>\r\n    </div>\r\n    <div [hidden]=\"!ShowSusDetails\">\r\n      <div class=\"fom-title mt-40 \">\r\n        New Captured Extension Details <button *ngIf=\"!this.isCheckerLogin\" mat-button\r\n                                               class=\"btn btn-warning\" (click)=\"ShowSuspensionDetails(false)\">\r\n          Close\r\n        </button>\r\n      </div>\r\n      <table mat-table [(dataSource)]=\"extDataDetails\" #t1Sort=\"matSort\" matSort\r\n             class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- Order No. -->\r\n        <ng-container matColumnDef=\"OrderNo\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Number</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderNo }}</td>\r\n        </ng-container>\r\n        <!-- Order Date -->\r\n        <ng-container matColumnDef=\"OrderDate\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- From Date -->\r\n        <ng-container matColumnDef=\"FromDate\">\r\n          <th mat-header-cell *matHeaderCellDef>From Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.fromDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- To Date Order No. -->\r\n        <ng-container matColumnDef=\"ToDate\">\r\n          <th mat-header-cell *matHeaderCellDef>To Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.toDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- No. of Days -->\r\n        <ng-container matColumnDef=\"Remarks\">\r\n          <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.remarks}}</td>\r\n        </ng-container>\r\n        <!---Status-->i98\r\n        <ng-container matColumnDef=\"Status\">\r\n          <th mat-header-cell *matHeaderCellDef>Status</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{element.status}}</td>\r\n        </ng-container>\r\n        <!--status end-->\r\n        <ng-container matColumnDef=\"Action\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Action</th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <!-- <a href=\"#\">Edit</a> -->\r\n            <a class=\"material-icons i-info\" *ngIf=\"element.status=='Rejected'?false:true\"\r\n               (click)=\"btnEditClick(element,true)\" matTooltip=\"Info\">info</a>\r\n            <div *ngIf=\"element.isEditable\">\r\n              <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element,false)\">edit</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" *ngIf=\"!isCheckerLogin\"\r\n                 (click)=\"DeleteSuspensionDetails(element.id,true);deletepopup = !deletepopup\">\r\n                delete_forever\r\n              </a>\r\n            </div>\r\n          </td>\r\n        </ng-container>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let element; columns: displayedColumns\"></tr>\r\n      </table>\r\n      <!-- <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator> -->\r\n      <mat-paginator #paginatorTable1 [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n      </mat-paginator>\r\n    </div>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure, you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteSuspensionDetails(_Id,false)\">Yes</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"hooCheckerLogin\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure, you want to verify?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure, you want to reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">{{successMsg}}</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\"\r\n              (click)=\"VarifiedSus(_Id,true)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\"\r\n              (click)=\"RejectedSus(_Id,true)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/suspension/extension/extension.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/suspension/extension/extension.component.ts ***!
  \*************************************************************/
/*! exports provided: ExtensionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionComponent", function() { return ExtensionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/Suspension/Suspension_service */ "./src/app/services/Suspension/Suspension_service.ts");
/* harmony import */ var _model_Suspension_ExtensionDetails__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/Suspension/ExtensionDetails */ "./src/app/model/Suspension/ExtensionDetails.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExtensionComponent = /** @class */ (function () {
    function ExtensionComponent(objsuspensionservice, snackBar, fb) {
        this.objsuspensionservice = objsuspensionservice;
        this.snackBar = snackBar;
        this.fb = fb;
        this.selectedvalue = 0;
        this.susPeriodInNumber = 0;
        this.ShowSusDetails = false;
        this.isCheckerLogin = false;
        this.disableButton = false;
        this.isViewDetails = true;
        this.isRejected = false;
        this.highlightedRows = [];
        this.displayedColumns = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status', 'Action'];
        this.historyTableColumns = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status'];
        this.CreateSuspensionDetailsForm();
    }
    ExtensionComponent.prototype.ngOnInit = function () {
        this._btnUpdateText = 'Forward to  HOO Checker';
        this._btnText = 'Save';
        if (+sessionStorage.getItem('userRoleID') === 9) {
            this.isCheckerLogin = false;
        }
        else {
            this.isCheckerLogin = true;
            this.ShowSusDetails = true;
            this._btnUpdateText = 'Reject';
            this._btnText = 'Accept';
        }
        this.GetAllDesignation();
        this.GetAllEmployees('0');
        this.objempExtDetails = new _model_Suspension_ExtensionDetails__WEBPACK_IMPORTED_MODULE_2__["ExtensionDetails"]();
        this.orderValidFlag = true;
        // this.SuspensionDetailsForm.disable();
        this.SuspensionDetailsForm.get('FromDate').disable();
        // this.SuspensionDetailsForm.get('btnSuspensionDetails').disable();
        this.validUptoDate = '';
        this.reasonText = '';
        this.isVarified = true;
        this.isReasonEmpty = false;
        this.todayDate = new Date();
        // if (this.isCheckerLogin === 'true') {
        //   this.SuspensionDetailsForm.disable();
        // }
    };
    ExtensionComponent.prototype.GetAllEmployees = function (desigId) {
        var _this = this;
        var comName = 'exten';
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice
            .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
            .subscribe(function (res) {
            _this.objempModel = res;
        });
    };
    ExtensionComponent.prototype.GetAllDesignation = function () {
        var _this = this;
        this.objsuspensionservice
            .GetAllDesignation('')
            .subscribe(function (res) {
            _this.objempDesignation = res;
        });
    };
    // GetPayBillGroup() {
    //   this.objsuspensionservice.GetPayBillGroup().subscribe(res => {
    //     this.objempdesig = res;
    //   });
    // }
    ExtensionComponent.prototype.CreateSuspensionDetailsForm = function () {
        this.SuspensionDetailsForm = this.fb.group({
            FromDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            OrderNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            SusPeriod: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(120)]],
            OrderDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            SalaryPercent: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(75), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(50), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(2)]],
            Remarks: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            RejectedReason: ['', '']
        });
    };
    ExtensionComponent.prototype.ddlDesignationChanged = function (desigId) {
        //this.selectedEmpValue = null;
        this.desigddl.placeholder = null;
        if (Number(desigId) === -1) {
            this.selectedvalue = null;
            //  this.objempModel = this.allempList;
            this.desigddl.placeholder = 'Select Designation';
            desigId = '0';
        }
        this.GetAllEmployees(desigId);
    };
    ExtensionComponent.prototype.ShowSuspensionDetails = function (isShow) {
        this.ShowSusDetails = isShow;
    };
    ExtensionComponent.prototype.AutoFillDate = function (daysAndDate, isdate) {
        var fdate;
        if (isdate) {
            fdate = new Date(daysAndDate);
            fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
        }
        else {
            this.susPeriodInNumber = daysAndDate;
            fdate = new Date(this.objempExtDetails.FromDate);
            fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
            this.objempExtDetails.ToDate = new Date(fdate + 'UTC');
        }
        this.validUptoDate = fdate.toDateString();
    };
    // ddlPayBillGroupChanged(payBillGroupId: string) {
    //   this.objsuspensionservice
    //     .GetAllDesignationOnSelectedPayBillGroup(payBillGroupId)
    //     .subscribe(res => {
    //       this.objempDesignation = res;
    //     });
    // }
    ExtensionComponent.prototype.ddlEmployeeChanged = function (empId) {
        this.selectedEmpCode = empId.split('@')[0].trim();
        this.selectedvalue = Number(empId.split('@')[1]);
        // this.SuspensionDetailsForm.get('FromDate').enable();
        this.orderValidFlag = false;
        this.isViewDetails = true;
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.Reset();
        if (this.isCheckerLogin) {
            this._btnText = 'Accept';
        }
    };
    ExtensionComponent.prototype.BindSuspensionDetails = function (empId) {
        var _this = this;
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        var comName = 'exten';
        this.objsuspensionservice.GetSuspensionDetails(empId, checkerLogin, comName).subscribe(function (result) {
            _this.extDataDetails = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.status !== 'Verified'; }));
            _this.extDataDetails.paginator = _this.paginator;
            _this.extDataDetails.sort = _this.t1Sort;
            _this.exPanel.disabled = false;
            if (_this.extDataDetails.data.length > 0) {
                _this.exPanel.close();
                _this.exPanel.disabled = true;
            }
            _this.dataSourceHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.status === 'Verified'; }));
            _this.dataSourceHistory.paginator = _this.paginatorHistory;
            _this.dataSourceHistory.sort = _this.t2Sort;
        });
    };
    ExtensionComponent.prototype.GetSuspensionDetails = function () {
        alert('Go');
    };
    ExtensionComponent.prototype.chkTillFurtherOrder = function (chk) {
        // this.orderValidFlag = chk.checked ? true : false;
        var toDate = this.SuspensionDetailsForm.get('ToDate');
        if (chk.checked) {
            toDate.disable();
            this.objempExtDetails.ToDate = null;
            toDate.setValidators(null);
        }
        else {
            toDate.enable();
            toDate.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required);
        }
        toDate.updateValueAndValidity();
    };
    ExtensionComponent.prototype.SavedSuspensionDetails = function (objsus) {
        var _this = this;
        // tslint:disable-next-line:no-debugger
        debugger;
        objsus.PermddoId = '00003';
        this.disableButton = true;
        // objsus.EmpCode = this.selectedEmpCode;
        // if (objsus.IsTillOrder == null) { objsus.IsTillOrder = false; }
        if (objsus.Status == null) {
            objsus.Status = 'I';
        }
        // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
        // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
        this.objsuspensionservice.SavedExtensionDetails(objsus).subscribe(function (arg) {
            if (+arg > 0) {
                _this.successMsg = 'Record saved successfully';
            }
            _this.myformm.resetForm();
            _this.objempExtDetails.Id = 0;
            _this.objempExtDetails.Status = null;
            _this.BindSuspensionDetails(_this.selectedEmpCode);
            _this.disableButton = false;
            _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
        });
    };
    ExtensionComponent.prototype.ForwardToChecker = function (obj) {
        obj.Status = 'F';
        this.SavedSuspensionDetails(obj);
    };
    ExtensionComponent.prototype.VarifiedSus = function (id, flag) {
        var _this = this;
        if (flag) {
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_exten', 'V', '').subscribe(function (arg) {
                if (+arg > 0) {
                    _this.successMsg = 'Varified successfully';
                }
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            this.myformm.resetForm();
            this.objempExtDetails.Id = 0;
            this.objempExtDetails.Status = null;
            // $('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = true;
        }
    };
    ExtensionComponent.prototype.RejectedSus = function (id, flag) {
        var _this = this;
        if (flag) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                this.successMsg = 'Please enter reason';
                return false;
            }
            if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
                this.isReasonEmpty = true;
                this.successMsg = 'Special characters are not allowed';
                return false;
            }
            this.successMsg = null;
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_exten', 'R', this.reasonText).subscribe(function (arg) {
                if (+arg > 0) {
                    _this.successMsg = 'Rejected successfully';
                }
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
                return false;
            });
            this.myformm.resetForm();
            this.objempExtDetails.Id = 0;
            this.objempExtDetails.Status = null;
            this.hooCheckerLogin = false;
            //$('.dialog__close-btn').click();
        }
        else {
            this.isVarified = false;
            return false;
        }
    };
    ExtensionComponent.prototype.rowHighlight = function (objsus) {
        this.objempExtDetails = new _model_Suspension_ExtensionDetails__WEBPACK_IMPORTED_MODULE_2__["ExtensionDetails"]();
        this.highlightedRows.pop();
        this.highlightedRows.push(objsus);
        this.objempExtDetails.SuspenId = objsus.id;
        console.log(objsus.id);
        // //this.objempSusDetails.FromDate = objsus.fromDate;
        // this.objempSusDetails.OrderDate = objsus.orderDate;
        // this.objempSusDetails.OrderNo = objsus.orderNo;
        // this.objempSusDetails.ToDate = objsus.toDate;
        // this.objempSusDetails.SalaryPercent = objsus.salaryPercent;
        // this.objempSusDetails.Remarks = objsus.remarks;
        // this.objempSusDetails.SusPeriod = objsus.susPeriod;
        var toDate = new Date(objsus.toDate);
        toDate.setDate(toDate.getDate() + 1);
        this.objempExtDetails.FromDate = new Date(toDate + 'UTC');
        this._btnText = 'Save';
        // const fdate = new Date(objsus.fromDate);
        // fdate.setDate(fdate.getDate() + +objsus.susPeriod);
        // this.validUptoDate = fdate.toDateString();
    };
    // btnEditClick(id: number, fromDate: Date, toDate: number, orderNo: string, orderDate: Date, salaryPercent: number, remarks: string) {
    //   this.objempSusDetails.Id = id;
    //   this.objempSusDetails.FromDate = fromDate;
    //   this.objempSusDetails.ToDate = toDate;
    //   this.objempSusDetails.OrderNo = orderNo;
    //   this.objempSusDetails.OrderDate = orderDate;
    //   this.objempSusDetails.Remarks = remarks;
    //   this.objempSusDetails.SalaryPercent = salaryPercent;
    //   }
    ExtensionComponent.prototype.btnEditClick = function (objsus, isViewOnly) {
        debugger;
        this.exPanel.disabled = false;
        this.exPanel.open();
        this.objempExtDetails.Id = objsus.id;
        this.objempExtDetails.FromDate = objsus.fromDate;
        this.objempExtDetails.OrderDate = objsus.orderDate;
        this.objempExtDetails.OrderNo = objsus.orderNo;
        // this.objempExtDetails.ToDate = objsus.toDate;
        this.objempExtDetails.SalaryPercent = objsus.salaryPercent;
        this.objempExtDetails.Remarks = objsus.remarks;
        this.objempExtDetails.SusPeriod = objsus.susPeriod;
        this._btnText = 'Save';
        this._Id = objsus.id;
        this.isRejected = false;
        if (objsus.rejectedReason !== '') {
            this.objempExtDetails.RejectedReason = objsus.rejectedReason;
            this.isRejected = true;
        }
        // const toDate = this.SuspensionDetailsForm.get('ToDate');
        // if (objsus.isTillOrder) {
        //   this.objempSusDetails.ToDate = null;
        //   toDate.disable();
        //   toDate.setValidators(null);
        // } else {
        //   toDate.enable();
        //   toDate.setValidators(Validators.required);
        // }
        // toDate.updateValueAndValidity();
        this.SuspensionDetailsForm.enable();
        var fdate = new Date(objsus.fromDate);
        fdate.setDate(fdate.getDate() + +objsus.susPeriod);
        this.validUptoDate = fdate.toDateString();
        this.objempExtDetails.ToDate = new Date(fdate + 'UTC');
        if (this.isCheckerLogin) {
            this._btnText = 'Accept';
            this.SuspensionDetailsForm.disable();
        }
        this.isViewDetails = true;
        if (isViewOnly) {
            this.SuspensionDetailsForm.disable();
            this.isViewDetails = false;
        }
    };
    ExtensionComponent.prototype.DeleteSuspensionDetails = function (id, flag) {
        var _this = this;
        this._Id = flag ? id : 0;
        this.isViewDetails = true;
        if (!flag) {
            this.objsuspensionservice.DeleteSuspensionDetails(id, 'exten').subscribe(function (arg) {
                _this.successMsg = arg;
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        this.deletepopup = false;
        // $('.dialog__close-btn').click();
    };
    ExtensionComponent.prototype.applyFilter = function (filterValue) {
        if (this.isCheckerLogin) {
            this.extDataDetails.filter = filterValue.trim().toLowerCase();
            if (this.extDataDetails.paginator) {
                this.extDataDetails.paginator.firstPage();
            }
        }
        else {
            this.dataSourceHistory.filter = filterValue.trim().toLowerCase();
            if (this.dataSourceHistory.paginator) {
                this.dataSourceHistory.paginator.firstPage();
            }
        }
    };
    ExtensionComponent.prototype.Reset = function () {
        this._btnText = 'Save';
        this.objempExtDetails = new _model_Suspension_ExtensionDetails__WEBPACK_IMPORTED_MODULE_2__["ExtensionDetails"]();
        // this.SuspensionDetailsForm.reset();
        //    this.SuspensionDetailsForm.reset({
        // });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorTable1'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], ExtensionComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorTable2'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], ExtensionComponent.prototype, "paginatorHistory", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('t1Sort'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], ExtensionComponent.prototype, "t1Sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('t2Sort'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], ExtensionComponent.prototype, "t2Sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formCreation'),
        __metadata("design:type", Object)
    ], ExtensionComponent.prototype, "myformm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelect"])
    ], ExtensionComponent.prototype, "desigddl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('exPanel'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionPanel"])
    ], ExtensionComponent.prototype, "exPanel", void 0);
    ExtensionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-extension',
            template: __webpack_require__(/*! ./extension.component.html */ "./src/app/suspension/extension/extension.component.html"),
            styles: [__webpack_require__(/*! ./extension.component.css */ "./src/app/suspension/extension/extension.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_1__["SuspensionService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], ExtensionComponent);
    return ExtensionComponent;
}());



/***/ }),

/***/ "./src/app/suspension/joining/joining.component.css":
/*!**********************************************************!*\
  !*** ./src/app/suspension/joining/joining.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1c3BlbnNpb24vam9pbmluZy9qb2luaW5nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/suspension/joining/joining.component.html":
/*!***********************************************************!*\
  !*** ./src/app/suspension/joining/joining.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select #singleSelect placeholder=\"Select Designation\" [(value)]=\"selectedvalue\"\r\n            (selectionChange)=\"ddlDesignationChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempDesignation\" [value]=\"item.desigId\">{{ item.desigDesc }}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Employee:</label>\r\n      <!--<button mat-mini-fab color=\"primary\" class=\"go-btn\">Go</button>-->\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Employee\" (selectionChange)=\"ddlEmployeeChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempModel\" [value]=\"item.empCd\">\r\n              {{ item.empName }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--------------- Left Form --------------->\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel #exPanel>\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Joining Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <form #formCreation=\"ngForm\" novalidate [formGroup]=\"JoiningDetailsForm\"\r\n      (ngSubmit)=\"formCreation.valid && SavedJoiningDetails(formCreation.value)\">\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"JoiningDate\" name=\"JoiningDate\" formControlName=\"JoiningDate\"\r\n            placeholder=\"Joining Date\" [(ngModel)]=\"objempJoiningDetails.JoiningDate\" readonly required />\r\n          <mat-datepicker-toggle matSuffix [for]=\"JoiningDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #JoiningDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4 combo-col-3\">\r\n        <div class=\"col-md-12 col-lg-3 pading-0\">\r\n          <label>Select</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-9 pading-0\">\r\n          <mat-radio-group [(ngModel)]=\"objempJoiningDetails.FnAn\" formControlName=\"FnAn\" name=\"JoiningTime\">\r\n            <mat-radio-button value=\"Fn\">Forenoon</mat-radio-button>\r\n            <mat-radio-button value=\"An\">Afternoon</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order No\" name=\"OrderNo\" formControlName=\"OrderNo\"\r\n            [(ngModel)]=\"objempJoiningDetails.OrderNo\" required />\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" name=\"OrderDate\" formControlName=\"OrderDate\"\r\n            placeholder=\"Order Date\" [(ngModel)]=\"objempJoiningDetails.OrderDate\" readonly required />\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div *ngIf=\"isRejected\" class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Rejected Reason\" name=\"RejectedReason\"\r\n            [(ngModel)]=\"objempJoiningDetails.RejectedReason\" formControlName=\"RejectedReason\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div *ngIf=\"isViewDetails\">\r\n        <div *ngIf=\"this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" (click)=\"VarifiedJoining(joiningId,true)\"\r\n            [disabled]=\"JoiningDetailsForm.invalid\">Accept\r\n          </button>\r\n          <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>-->\r\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"RejectedJoining(joiningId,true)\"\r\n            [disabled]=\"JoiningDetailsForm.invalid\">\r\n            Reject\r\n          </button>\r\n        </div>\r\n\r\n        <div *ngIf=\"!this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"JoiningDetailsForm.invalid || disableButton\">Save</button>\r\n          <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"ForwardToChecker(formCreation.value)\"\r\n            [disabled]=\"JoiningDetailsForm.invalid || disableButton\">Forward to HOO Checker</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"note\">\r\n      <span>Note</span>\r\n      Please enter details when the employee is revoked after completion of Suspension period\r\n    </div>\r\n    <div class=\"fom-title\">Revocation Details</div>\r\n    <!-- Search -->\r\n\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\" (keyup)=\"applyFilter($event.target.value)\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n    <table mat-table [(dataSource)]=\"dataJoiningHistory\" #sortingJH=\"matSort\" matSort\r\n      class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"JoiningDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Revocation Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.joiningDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Fn/An\">\r\n        <th mat-header-cell *matHeaderCellDef>Fn/An</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">{{ rowData.fnAn}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"joiningHistoryColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: joiningHistoryColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingJH [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-right view-btn-wraper\">\r\n      <button type=\"submit\" class=\"btn view-detail-btn\">View New Captured Revocation Order Details</button>\r\n\r\n    </div>\r\n\r\n    <div class=\"fom-title\">New captured Suspension Order Details <i class=\"material-icons close-icon\"> clear </i></div>\r\n\r\n\r\n    <table mat-table [(dataSource)]=\"dataJoining\" #sortingJoining=\"matSort\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"JoiningDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Revocation Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.joiningDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Fn/An\">\r\n        <th mat-header-cell *matHeaderCellDef>Fn/An</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">{{ rowData.fnAn}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Action</th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!-- <a href=\"#\">Edit</a> -->\r\n          <!-- <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a> -->\r\n          <div>\r\n            <a class=\"material-icons i-info\" *ngIf=\"element.status=='Rejected'?false:true\"\r\n              (click)=\"btnEditClick(element,true)\" matTooltip=\"Info\">info</a>\r\n            <a class=\"material-icons i-edit\" *ngIf=\"element.isEditable\" matTooltip=\"Edit\"\r\n              (click)=\"btnEditClick(element,false)\">edit</a>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" *ngIf=\"!isCheckerLogin && element.isEditable\"\r\n              (click)=\"DeleteJoiningDetails(element.empJoiningId,true);\">\r\n              delete_forever\r\n            </a>\r\n          </div>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"joiningColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: joiningColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingJoining [pageSize]=\"5\" [pageIndex]=\"0\" [pageSizeOptions]=\"[5,10,15]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteJoiningDetails(joiningId,false)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"hooCheckerPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure, you want to Accept?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure, you want to Reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">{{successMsg}}</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VarifiedJoining(joiningId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedJoining(joiningId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerPopup = !hooCheckerPopup\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/suspension/joining/joining.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/suspension/joining/joining.component.ts ***!
  \*********************************************************/
/*! exports provided: JoiningComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoiningComponent", function() { return JoiningComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _model_Suspension_JoiningDetails__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../model/Suspension/JoiningDetails */ "./src/app/model/Suspension/JoiningDetails.ts");
/* harmony import */ var _services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/Suspension/Suspension_service */ "./src/app/services/Suspension/Suspension_service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var JoiningComponent = /** @class */ (function () {
    function JoiningComponent(objsuspensionservice, snackBar, fb) {
        this.objsuspensionservice = objsuspensionservice;
        this.snackBar = snackBar;
        this.fb = fb;
        this.isCheckerLogin = false;
        this.selectedvalue = 0;
        this.isRejected = false;
        this.isViewDetails = true;
        this.disableButton = false;
        this.joiningHistoryColumns = ['OrderNo', 'OrderDate', 'JoiningDate', 'Fn/An', 'Status'];
        this.joiningColumns = ['OrderNo', 'OrderDate', 'JoiningDate', 'Fn/An', 'Status', 'Action'];
        this.CreateJoiningDetailsForm();
    }
    JoiningComponent.prototype.ngOnInit = function () {
        this.objempJoiningDetails = new _model_Suspension_JoiningDetails__WEBPACK_IMPORTED_MODULE_5__["JoiningDetails"]();
        if (+sessionStorage.getItem('userRoleID') === 9) {
            this.isCheckerLogin = false;
        }
        else {
            this.isCheckerLogin = true;
        }
        this.checkerbtnDisable = true;
        console.log(this.isCheckerLogin);
        this.GetAllDesignation();
        this.GetAllEmployees('0');
        this.reasonText = '';
        this.isReasonEmpty = false;
        this.isRejected = false;
    };
    JoiningComponent.prototype.CreateJoiningDetailsForm = function () {
        this.JoiningDetailsForm = this.fb.group({
            JoiningDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            FnAn: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            OrderNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            OrderDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            RejectedReason: ['', '']
        });
    };
    JoiningComponent.prototype.GetAllDesignation = function () {
        var _this = this;
        this.objsuspensionservice
            .GetAllDesignation('')
            .subscribe(function (res) {
            _this.objempDesignation = res;
        });
    };
    JoiningComponent.prototype.GetAllEmployees = function (desigId) {
        var _this = this;
        var comName = 'joining';
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice
            .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
            .subscribe(function (res) {
            _this.objempModel = res;
        });
    };
    JoiningComponent.prototype.ddlDesignationChanged = function (desigId) {
        debugger;
        this.desigddl.placeholder = null;
        if (Number(desigId) === -1) {
            this.selectedvalue = null;
            //  this.objempModel = this.allempList;
            this.desigddl.placeholder = 'Select Designation';
            desigId = '0';
        }
        this.GetAllEmployees(desigId);
    };
    JoiningComponent.prototype.ddlEmployeeChanged = function (empId) {
        debugger;
        //this.myform.reset();
        this.JoiningDetailsForm.enable();
        this.JoiningDetailsForm.reset();
        this.isViewDetails = true;
        this.checkerbtnDisable = true;
        this.selectedEmpSusId = empId.split('@')[0].trim();
        this.selectedvalue = Number(empId.split('@')[1]);
        this.empId = Number(empId.split('@')[2]);
        console.log(this.selectedEmpSusId);
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
    };
    JoiningComponent.prototype.BindJoiningDetails = function (selectedvalue) {
        var _this = this;
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice.GetJoiningDetails(this.empId, checkerLogin).subscribe(function (result) {
            console.log(result);
            _this.dataJoining = new _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() !== 'V'; }));
            _this.dataJoining.paginator = _this.pagingJoining;
            _this.dataJoining.sort = _this.sortingJoining;
            _this.exPanel.disabled = false;
            if (_this.dataJoining.data.length > 0) {
                _this.exPanel.close();
                _this.exPanel.disabled = true;
            }
            _this.dataJoiningHistory = new _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() === 'V'; }));
            _this.dataJoiningHistory.paginator = _this.pagingJH;
            _this.dataJoiningHistory.sort = _this.sortingJH;
        });
    };
    JoiningComponent.prototype.SavedJoiningDetails = function (objJoining) {
        var _this = this;
        debugger;
        console.log(objJoining);
        this.disableButton = true;
        objJoining.EmpRevokId = Number(this.selectedEmpSusId);
        objJoining.EmpJoiningId = this.joiningId;
        //this.formJoining.reset();
        //return true;
        if (objJoining.FlagStatus === void 0) {
            objJoining.FlagStatus = 'JN';
        }
        // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
        // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
        this.objsuspensionservice.SavedJoiningDetails(objJoining).subscribe(function (arg) {
            _this.successMsg = arg;
            if (+arg > 0) {
                _this.successMsg = 'Record saved successfully';
                _this.joiningId = 0;
                _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                _this.disableButton = false;
                _this.JoiningDetailsForm.reset();
            }
            // this.objempExtDetails.Id = 0;
            // this.objempExtDetails.Status = null;
            // this.BindSuspensionDetails(this.selectedEmpCode);
            _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
        });
    };
    JoiningComponent.prototype.applyFilter = function (filterValue) {
        if (this.isCheckerLogin) {
            this.dataJoining.filter = filterValue.trim().toLowerCase();
            if (this.dataJoining.paginator) {
                this.dataJoining.paginator.firstPage();
            }
        }
        else {
            this.dataJoiningHistory.filter = filterValue.trim().toLowerCase();
            if (this.dataJoiningHistory.paginator) {
                this.dataJoiningHistory.paginator.firstPage();
            }
        }
    };
    JoiningComponent.prototype.btnEditClick = function (objRevok, isViewOnly) {
        debugger;
        this.exPanel.disabled = false;
        this.exPanel.open();
        this.objempJoiningDetails.EmpJoiningId = objRevok.empJoiningId;
        this.objempJoiningDetails.OrderDate = objRevok.orderDate;
        this.objempJoiningDetails.JoiningDate = objRevok.joiningDate;
        this.objempJoiningDetails.OrderNo = objRevok.orderNo;
        this.objempJoiningDetails.FlagStatus = objRevok.flagStatus;
        this.objempJoiningDetails.FnAn = objRevok.fnAn;
        this.joiningId = objRevok.empJoiningId;
        console.log(objRevok.empJoiningId);
        this.isRejected = false;
        if (objRevok.rejectedReason !== '') {
            this.objempJoiningDetails.RejectedReason = objRevok.rejectedReason;
            this.isRejected = true;
        }
        this.JoiningDetailsForm.enable();
        this.isViewDetails = !isViewOnly;
        if (this.isCheckerLogin) {
            this.JoiningDetailsForm.disable();
        }
        // if (this.isCheckerLogin) {
        // this.myform.form.disable();
        //  this.myform.controls.forEach(function(element) {
        //    element.disable();
        //  });
        // }
        if (isViewOnly) {
            this.JoiningDetailsForm.disable();
            this.isViewDetails = false;
        }
    };
    JoiningComponent.prototype.DeleteJoiningDetails = function (id, flag) {
        var _this = this;
        debugger;
        this.joiningId = flag ? id : 0;
        if (!flag) {
            this.objsuspensionservice.DeleteSuspensionDetails(id, 'joining').subscribe(function (arg) {
                _this.successMsg = arg;
                _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        // $('.dialog__close-btn').click();
        this.deletepopup = flag;
    };
    JoiningComponent.prototype.ForwardToChecker = function (obj) {
        obj.FlagStatus = 'F';
        this.SavedJoiningDetails(obj);
    };
    JoiningComponent.prototype.VarifiedJoining = function (id, flag) {
        var _this = this;
        console.log(id);
        if (!flag && id > 0) {
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_joining', 'V', '').subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Varified successfully';
                    _this.joiningId = 0;
                    _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                    _this.JoiningDetailsForm.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        this.hooCheckerPopup = flag;
        this.isVarified = flag;
    };
    JoiningComponent.prototype.RejectedJoining = function (id, flag) {
        var _this = this;
        debugger;
        console.log(id);
        this.hooCheckerPopup = flag;
        if (!flag && id > 0) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                this.successMsg = 'Please enter reason';
                return false;
            }
            if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
                this.isReasonEmpty = true;
                this.successMsg = 'Special characters are not allowed';
                return false;
            }
            this.successMsg = null;
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_joining', 'R', this.reasonText).subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Rejected successfully';
                    _this.joiningId = 0;
                    _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                    _this.JoiningDetailsForm.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            return false;
        }
        this.isVarified = !flag;
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formCreation'),
        __metadata("design:type", Object)
    ], JoiningComponent.prototype, "formJoining", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortingJH'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], JoiningComponent.prototype, "sortingJH", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingJH'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], JoiningComponent.prototype, "pagingJH", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortingJoining'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], JoiningComponent.prototype, "sortingJoining", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingJoining'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], JoiningComponent.prototype, "pagingJoining", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelect"])
    ], JoiningComponent.prototype, "desigddl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('exPanel'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionPanel"])
    ], JoiningComponent.prototype, "exPanel", void 0);
    JoiningComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-joining',
            template: __webpack_require__(/*! ./joining.component.html */ "./src/app/suspension/joining/joining.component.html"),
            styles: [__webpack_require__(/*! ./joining.component.css */ "./src/app/suspension/joining/joining.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_6__["SuspensionService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], JoiningComponent);
    return JoiningComponent;
}());



/***/ }),

/***/ "./src/app/suspension/regularisation/regularisation.component.css":
/*!************************************************************************!*\
  !*** ./src/app/suspension/regularisation/regularisation.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1c3BlbnNpb24vcmVndWxhcmlzYXRpb24vcmVndWxhcmlzYXRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/suspension/regularisation/regularisation.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/suspension/regularisation/regularisation.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select #singleSelect placeholder=\"Select Designation\" [(value)]=\"selectedvalue\"\r\n            (selectionChange)=\"ddlDesignationChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempDesignation\" [value]=\"item.desigId\">{{ item.desigDesc }}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Employee:</label>\r\n      <!--<button mat-mini-fab color=\"primary\" class=\"go-btn\">Go</button>-->\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Employee\" (selectionChange)=\"ddlEmployeeChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempModel\" [value]=\"item.empCd\">\r\n              {{ item.empName }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--------------- Left Form --------------->\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel #exPanel>\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Regularisation Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <form #formCreation=\"ngForm\" novalidate [formGroup]=\"RegulariseDetailsForm\"\r\n      (ngSubmit)=\"formCreation.valid && SavedRegulariseDetails(formCreation.value)\">\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [(ngModel)]=\"objempRegulariseDetails.SuspenCat\" (selectionChange)=\"ddlSuspenCat($event.value)\"\r\n            formControlName=\"SuspenCat\" placeholder=\"Select Category\" required>\r\n            <mat-option *ngFor=\"let item of suspenCats\" [value]=\"item.Id\">{{item.Name}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [(ngModel)]=\"objempRegulariseDetails.SuspenTreated\" formControlName=\"SuspenTreated\"\r\n            placeholder=\"Suspension Period to be treated as\" required>\r\n            <mat-option *ngFor=\"let item of suspenPeriod\" [value]=\"item.Id\">{{item.Name}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order No\" name=\"OrderNo\" formControlName=\"OrderNo\"\r\n            [(ngModel)]=\"objempRegulariseDetails.OrderNo\" required />\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" name=\"OrderDate\" formControlName=\"OrderDate\"\r\n            placeholder=\"Order Date\" [(ngModel)]=\"objempRegulariseDetails.OrderDate\" readonly required />\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks (If any)\" name=\"Remarks\" [(ngModel)]=\"objempRegulariseDetails.Remarks\"\r\n            formControlName=\"Remarks\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div *ngIf=\"isRejected\" class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Rejected Reason\" name=\"RejectedReason\"\r\n            [(ngModel)]=\"objempRegulariseDetails.RejectedReason\" formControlName=\"RejectedReason\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div *ngIf=\"isViewDetails\">\r\n        <div *ngIf=\"this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" (click)=\"VarifiedJoining(regularId,true)\"\r\n            [disabled]=\"RegulariseDetailsForm.invalid\">Accept\r\n          </button>\r\n          <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>-->\r\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"RejectedJoining(regularId,true)\"\r\n            [disabled]=\"RegulariseDetailsForm.invalid\">\r\n            Reject\r\n          </button>\r\n        </div>\r\n\r\n        <div *ngIf=\"!this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"RegulariseDetailsForm.invalid || disableButton\">Save</button>\r\n          <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"ForwardToChecker(formCreation.value)\"\r\n            [disabled]=\"RegulariseDetailsForm.invalid || disableButton\">Forward to HOO Checker</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"note\">\r\n      <span>Note</span>\r\n      Please enter Regularisation details when the employee joines duty after Revocation of\r\n      suspension OR Reporting after Unauthorized absence\r\n    </div>\r\n    <div class=\"fom-title\">Regularisation Details</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\" (keyup)=\"applyFilter($event.target.value)\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n    <table mat-table [(dataSource)]=\"dataRegularHistory\" #sortingRH=\"matSort\" matSort\r\n      class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"SuspenCat\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Suspen Category</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">\r\n          {{rowData.susCatname}}\r\n        </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"SuspenTreated\">\r\n        <th mat-header-cell *matHeaderCellDef>Suspension period to be treated</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">\r\n          {{rowData.sustreatedName}}\r\n        </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Remarks\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.remarks}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"regularHistoryColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: regularHistoryColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingRH [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-right view-btn-wraper\">\r\n      <button type=\"submit\" class=\"btn view-detail-btn\">View New Captured Regularisation Order Details</button>\r\n\r\n    </div>\r\n\r\n    <div class=\"fom-title\">New captured Regularisation Details <i class=\"material-icons close-icon\"> clear </i></div>\r\n\r\n\r\n\r\n    <table mat-table [(dataSource)]=\"dataRegular\" #sortingReg=\"matSort\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"SuspenCat\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Suspen Category</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">\r\n          {{rowData.susCatname}}\r\n          <!-- <div *ngFor=\"let item of suspenCats\"><p *ngIf=\"item.Id===rowData.suspenCat\">{{item.Name}}</p></div> -->\r\n        </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"SuspenTreated\">\r\n        <th mat-header-cell *matHeaderCellDef>Suspension period to be treated</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">\r\n          <!-- <mat-select placeholder=\"select employee\" [(value)]=\"rowData.suspenTreated\" [disabled]=\"true\">\r\n            <mat-option *ngFor=\"let item of suspenPeriod\" [value]=\"item.Id\">{{item.Name}}</mat-option>\r\n          </mat-select> -->\r\n          {{rowData.sustreatedName}}\r\n        </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.orderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Remarks\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.remarks}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef>Action</th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!-- <a href=\"#\">Edit</a> -->\r\n          <!-- <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a> -->\r\n          <div>\r\n            <a class=\"material-icons i-info\" *ngIf=\"element.status=='Rejected'?false:true\"\r\n              (click)=\"btnEditClick(element,true)\" matTooltip=\"Info\">info</a>\r\n            <a class=\"material-icons i-edit\" *ngIf=\"element.isEditable\" matTooltip=\"Edit\"\r\n              (click)=\"btnEditClick(element,false)\">edit</a>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" *ngIf=\"!isCheckerLogin && element.isEditable\"\r\n              (click)=\"DeleteJoiningDetails(element.empJoiningId,true);\">\r\n              delete_forever\r\n            </a>\r\n          </div>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"regularColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: regularColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingReg [pageSize]=\"5\" [pageIndex]=\"0\" [pageSizeOptions]=\"[5,10,15]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteJoiningDetails(regularId,false)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"hooCheckerPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure, you want to Accept?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure, you want to Reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">{{successMsg}}</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VarifiedJoining(regularId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedJoining(regularId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerPopup = !hooCheckerPopup\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/suspension/regularisation/regularisation.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/suspension/regularisation/regularisation.component.ts ***!
  \***********************************************************************/
/*! exports provided: RegularisationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegularisationComponent", function() { return RegularisationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _model_Suspension_RegulariseDetails__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/Suspension/RegulariseDetails */ "./src/app/model/Suspension/RegulariseDetails.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/Suspension/Suspension_service */ "./src/app/services/Suspension/Suspension_service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RegularisationComponent = /** @class */ (function () {
    function RegularisationComponent(objsuspensionservice, snackBar, fb) {
        this.objsuspensionservice = objsuspensionservice;
        this.snackBar = snackBar;
        this.fb = fb;
        this.isCheckerLogin = false;
        this.selectedvalue = 0;
        this.isRejected = false;
        this.disableButton = false;
        this.isViewDetails = true;
        this.regularHistoryColumns = ['SuspenCat', 'SuspenTreated', 'OrderNo', 'OrderDate', 'Remarks', 'Status'];
        this.regularColumns = ['SuspenCat', 'SuspenTreated', 'OrderNo', 'OrderDate', 'Remarks', 'Status', 'Action'];
        this.suspenCats = [{ Id: 1, Name: 'Minor Penalty' },
            { Id: 2, Name: 'Major Penalty' },
            { Id: 3, Name: 'Exonerate' }];
        this.suspenPeriod = [{ Id: 1, Name: 'Duty' },
            { Id: 2, Name: 'Employee on Leave' },
            { Id: 3, Name: 'Dies Non' }];
        this.CreateJoiningDetailsForm();
    }
    RegularisationComponent.prototype.ngOnInit = function () {
        this.objempRegulariseDetails = new _model_Suspension_RegulariseDetails__WEBPACK_IMPORTED_MODULE_3__["RegulariseDetails"]();
        if (+sessionStorage.getItem('userRoleID') === 9) {
            this.isCheckerLogin = false;
        }
        else {
            this.isCheckerLogin = true;
        }
        this.checkerbtnDisable = true;
        console.log(this.isCheckerLogin);
        this.GetAllDesignation();
        this.GetAllEmployees('0');
        this.reasonText = '';
        this.isReasonEmpty = false;
        this.isRejected = false;
        // console.log(this.suspenCats.find(x => x.Id === 1).Name);
    };
    RegularisationComponent.prototype.CreateJoiningDetailsForm = function () {
        this.RegulariseDetailsForm = this.fb.group({
            SuspenCat: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            SuspenTreated: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            OrderNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            OrderDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            Remarks: ['', ''],
            RejectedReason: ['', '']
        });
    };
    RegularisationComponent.prototype.GetAllDesignation = function () {
        var _this = this;
        this.objsuspensionservice
            .GetAllDesignation('')
            .subscribe(function (res) {
            _this.objempDesignation = res;
        });
    };
    RegularisationComponent.prototype.GetAllEmployees = function (desigId) {
        var _this = this;
        var comName = 'regular';
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice
            .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
            .subscribe(function (res) {
            _this.objempModel = res;
        });
    };
    RegularisationComponent.prototype.ddlDesignationChanged = function (desigId) {
        this.desigddl.placeholder = null;
        if (Number(desigId) === -1) {
            this.selectedvalue = null;
            //  this.objempModel = this.allempList;
            this.desigddl.placeholder = 'Select Designation';
            desigId = '0';
        }
        this.GetAllEmployees(desigId);
    };
    RegularisationComponent.prototype.ddlEmployeeChanged = function (empId) {
        debugger;
        //this.myform.reset();
        this.RegulariseDetailsForm.enable();
        this.RegulariseDetailsForm.reset();
        this.isViewDetails = true;
        this.checkerbtnDisable = true;
        this.selectedEmpSusId = empId.split('@')[0].trim();
        this.selectedvalue = Number(empId.split('@')[1]);
        this.empId = Number(empId.split('@')[2]);
        console.log(this.selectedEmpSusId);
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
    };
    RegularisationComponent.prototype.BindJoiningDetails = function (selectedvalue) {
        var _this = this;
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice.GetRegulariseDetails(this.empId, checkerLogin).subscribe(function (result) {
            console.log(result);
            result.forEach(function (item) {
                item.susCatname = _this.suspenCats.find(function (b) { return b.Id === item.suspenCat; }).Name;
                item.sustreatedName = _this.suspenPeriod.find(function (x) { return x.Id === item.suspenTreated; }).Name;
            });
            _this.dataRegular = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() !== 'V'; }));
            _this.dataRegular.paginator = _this.pagingReg;
            _this.dataRegular.sort = _this.sortingReg;
            _this.exPanel.disabled = false;
            if (_this.dataRegular.data.length > 0) {
                _this.exPanel.close();
                _this.exPanel.disabled = true;
            }
            _this.dataRegularHistory = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() === 'V'; }));
            _this.dataRegularHistory.paginator = _this.pagingRH;
            _this.dataRegularHistory.sort = _this.sortingRH;
        });
    };
    RegularisationComponent.prototype.SavedRegulariseDetails = function (objRegular) {
        var _this = this;
        debugger;
        console.log(objRegular);
        this.disableButton = true;
        objRegular.JoiningId = Number(this.selectedEmpSusId);
        objRegular.RegulariseId = this.regularId;
        if (objRegular.FlagStatus === void 0) {
            objRegular.FlagStatus = 'RG';
        }
        this.objsuspensionservice.SavedRegulariseDetails(objRegular).subscribe(function (arg) {
            _this.successMsg = arg;
            if (+arg > 0) {
                _this.successMsg = 'Record saved successfully';
                _this.regularId = 0;
                _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                _this.disableButton = false;
                _this.RegulariseDetailsForm.reset();
            }
            // this.objempExtDetails.Id = 0;
            // this.objempExtDetails.Status = null;
            // this.BindSuspensionDetails(this.selectedEmpCode);
            _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
        });
    };
    RegularisationComponent.prototype.ddlSuspenCat = function (catId) {
        debugger;
        this.objempRegulariseDetails.SuspenTreated = null;
        //const susTreatedddl = this.RegulariseDetailsForm.get('SuspenTreated');
        switch (catId) {
            case 1: {
                this.suspenPeriod = [{ Id: 1, Name: 'Duty' },
                    { Id: 2, Name: 'Employee on Leave' }];
                break;
            }
            case 2: {
                this.suspenPeriod = [{ Id: 2, Name: 'Employee on Leave' },
                    { Id: 3, Name: 'Dies Non' }];
                break;
            }
            default: {
                this.suspenPeriod = [{ Id: 1, Name: 'Duty' },
                    { Id: 2, Name: 'Employee on Leave' },
                    { Id: 3, Name: 'Dies Non' }];
                break;
            }
        }
        // susTreatedddl.setValidators(Validators.required);
        // susTreatedddl.updateValueAndValidity();
    };
    RegularisationComponent.prototype.applyFilter = function (filterValue) {
        if (this.isCheckerLogin) {
            this.dataRegular.filter = filterValue.trim().toLowerCase();
            if (this.dataRegular.paginator) {
                this.dataRegular.paginator.firstPage();
            }
        }
        else {
            this.dataRegularHistory.filter = filterValue.trim().toLowerCase();
            if (this.dataRegularHistory.paginator) {
                this.dataRegularHistory.paginator.firstPage();
            }
        }
    };
    RegularisationComponent.prototype.btnEditClick = function (objReg, isViewOnly) {
        debugger;
        this.exPanel.disabled = false;
        this.exPanel.open();
        this.objempRegulariseDetails.RegulariseId = objReg.regulariseId;
        this.objempRegulariseDetails.SuspenCat = objReg.suspenCat;
        this.objempRegulariseDetails.SuspenTreated = objReg.suspenTreated;
        this.objempRegulariseDetails.OrderNo = objReg.orderNo;
        this.objempRegulariseDetails.OrderDate = objReg.orderDate;
        this.objempRegulariseDetails.FlagStatus = objReg.flagStatus;
        this.objempRegulariseDetails.Remarks = objReg.remarks;
        this.regularId = objReg.regulariseId;
        console.log(objReg.empJoiningId);
        this.isRejected = false;
        if (objReg.rejectedReason !== '') {
            this.objempRegulariseDetails.RejectedReason = objReg.rejectedReason;
            this.isRejected = true;
        }
        this.RegulariseDetailsForm.enable();
        this.isViewDetails = !isViewOnly;
        if (this.isCheckerLogin) {
            this.RegulariseDetailsForm.disable();
        }
        // if (this.isCheckerLogin) {
        // this.myform.form.disable();
        //  this.myform.controls.forEach(function(element) {
        //    element.disable();
        //  });
        // }
        if (isViewOnly) {
            this.RegulariseDetailsForm.disable();
            this.isViewDetails = false;
        }
    };
    RegularisationComponent.prototype.ForwardToChecker = function (obj) {
        obj.FlagStatus = 'F';
        this.SavedRegulariseDetails(obj);
    };
    RegularisationComponent.prototype.DeleteJoiningDetails = function (id, flag) {
        var _this = this;
        debugger;
        this.regularId = flag ? id : 0;
        if (!flag) {
            this.objsuspensionservice.DeleteSuspensionDetails(id, 'joining').subscribe(function (arg) {
                _this.successMsg = arg;
                _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        // $('.dialog__close-btn').click();
        this.deletepopup = flag;
    };
    RegularisationComponent.prototype.VarifiedJoining = function (id, flag) {
        var _this = this;
        console.log(id);
        if (!flag && id > 0) {
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_regular', 'V', '').subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Varified successfully';
                    _this.regularId = 0;
                    _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                    _this.RegulariseDetailsForm.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        this.hooCheckerPopup = flag;
        this.isVarified = flag;
    };
    RegularisationComponent.prototype.RejectedJoining = function (id, flag) {
        var _this = this;
        debugger;
        console.log(id);
        if (!flag && id > 0) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                this.successMsg = 'Please enter reason';
                return false;
            }
            if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
                this.isReasonEmpty = true;
                this.successMsg = 'Special characters are not allowed';
                return false;
            }
            this.successMsg = null;
            this.hooCheckerPopup = flag;
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_regular', 'R', this.reasonText).subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Rejected successfully';
                    _this.regularId = 0;
                    _this.BindJoiningDetails(Number(_this.selectedEmpSusId));
                    _this.RegulariseDetailsForm.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            return false;
        }
        this.hooCheckerPopup = flag;
        this.isVarified = !flag;
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formCreation'),
        __metadata("design:type", Object)
    ], RegularisationComponent.prototype, "formJoining", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortingRH'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RegularisationComponent.prototype, "sortingRH", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingRH'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], RegularisationComponent.prototype, "pagingRH", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortingReg'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RegularisationComponent.prototype, "sortingReg", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingReg'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], RegularisationComponent.prototype, "pagingReg", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelect"])
    ], RegularisationComponent.prototype, "desigddl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('exPanel'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionPanel"])
    ], RegularisationComponent.prototype, "exPanel", void 0);
    RegularisationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-regularisation',
            template: __webpack_require__(/*! ./regularisation.component.html */ "./src/app/suspension/regularisation/regularisation.component.html"),
            styles: [__webpack_require__(/*! ./regularisation.component.css */ "./src/app/suspension/regularisation/regularisation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_6__["SuspensionService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]])
    ], RegularisationComponent);
    return RegularisationComponent;
}());



/***/ }),

/***/ "./src/app/suspension/revocation/revocation.component.css":
/*!****************************************************************!*\
  !*** ./src/app/suspension/revocation/revocation.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1c3BlbnNpb24vcmV2b2NhdGlvbi9yZXZvY2F0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/suspension/revocation/revocation.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/suspension/revocation/revocation.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  \r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select #singleSelect placeholder=\"Select Designation\" [(value)]=\"selectedvalue\"\r\n            (selectionChange)=\"ddlDesignationChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempDesignation\" [value]=\"item.desigId\">{{ item.desigDesc }}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Employee:</label>\r\n      <!--<button mat-mini-fab color=\"primary\" class=\"go-btn\">Go</button> change by nitin  -->\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Employee\" (selectionChange)=\"ddlEmployeeChanged($event.value)\">\r\n            <mat-option *ngFor=\"let item of objempModel\" [value]=\"item.empCd\">\r\n              {{ item.empName }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--------------- Left Form --------------->\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel #revPanel>\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Revocation Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <form #formCreation=\"ngForm\" novalidate\r\n      (ngSubmit)=\"formCreation.valid && SavedRevocationDetails(formCreation.value)\">\r\n      <fieldset [disabled]=\"!isViewDetails\">\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"RevocationDate\" name=\"RevocationDate\" [max]=\"todayDate\"\r\n              [disabled]=\"isCheckerLogin\" placeholder=\"Revocation Date\" [(ngModel)]=\"objempRevokDetails.RevocationDate\"\r\n              readonly required />\r\n            <mat-datepicker-toggle matSuffix [for]=\"RevocationDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #RevocationDate></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <!-- <input matInput placeholder=\"Order No.\"> -->\r\n            <input matInput placeholder=\"Order No\" name=\"RevocOrderNo\" [(ngModel)]=\"objempRevokDetails.RevocOrderNo\"\r\n              [disabled]=\"isCheckerLogin\" pattern=\"[-(),/0-9a-zA-Z \\\\]*\" required>\r\n              <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"RevocOrderDate\" placeholder=\"Order Date\" name=\"RevocOrderDate\"\r\n              [disabled]=\"isCheckerLogin\" [(ngModel)]=\"objempRevokDetails.RevocOrderDate\" [max]=\"todayDate\" readonly\r\n              required />\r\n            <mat-datepicker-toggle matSuffix [for]=\"RevocOrderDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #RevocOrderDate></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <!-- <textarea matInput placeholder=\"Remarks (If any) \"></textarea> -->\r\n            <textarea matInput placeholder=\"Remarks (If any)\" name=\"Remarks\" [disabled]=\"isCheckerLogin\"\r\n              [(ngModel)]=\"objempRevokDetails.Remarks\" pattern=\"[-(),/0-9a-zA-Z \\\\]*\"></textarea>\r\n              <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div *ngIf=\"isRejected\" class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Rejected Reason\" name=\"RejectedReason\" [disabled]=\"isCheckerLogin\"\r\n              [(ngModel)]=\"objempRevokDetails.RejectedReason\"></textarea>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div *ngIf=\"isViewDetails\">\r\n          <div *ngIf=\"this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"VarifiedRevok(revId,true)\"\r\n              [disabled]=\"checkerbtnDisable\">Accept\r\n            </button>\r\n            <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>-->\r\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"RejectedRevok(revId,true)\"\r\n              [disabled]=\"checkerbtnDisable\">\r\n              Reject\r\n            </button>\r\n          </div>\r\n\r\n          <div *ngIf=\"!this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!formCreation.valid ||disableButton\">Save</button>\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>\r\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"ForwardToChecker(formCreation.value)\"\r\n              [disabled]=\"!formCreation.valid || disableButton\">Forward to HOO Checker</button>\r\n          </div>\r\n        </div>\r\n      </fieldset>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"note\">\r\n      <span>Note</span>\r\n      Please enter details when the employee is revoked after completion of Suspension period\r\n    </div>\r\n    <div class=\"fom-title\">Revocation Details</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\" (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n    <table mat-table [(dataSource)]=\"dataRevocationHistory\" #revHSort=\"matSort\" matSort\r\n      class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocOrderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"RevocationDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Revocation Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocationDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Remarks\">\r\n        <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">{{ rowData.remarks}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"revokHistoryColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: revokHistoryColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingRevokH [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-right view-btn-wraper\">\r\n      <button type=\"submit\" class=\"btn view-detail-btn\">View New Captured Revocation Order Details</button>\r\n    </div>\r\n\r\n    <div class=\"fom-title\">New captured Revocation Order Details <i class=\"material-icons close-icon\"> clear </i></div>\r\n\r\n    <table mat-table [(dataSource)]=\"dataRevocation\" #revHSort=\"matSort\" matSort\r\n      class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> OrderNo</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocOrderDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"RevocationDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Revocation Date</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.revocationDate | date: 'dd/MM/yyyy'}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Remarks\">\r\n        <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n        <td mat-cell *matCellDef=\"let rowData\">{{ rowData.remarks}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n        <td mat-cell *matCellDef=\"let rowData\"> {{rowData.flagStatus}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Action</th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!-- <a href=\"#\">Edit</a> -->\r\n          <!-- <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a> -->\r\n          <div>\r\n            <a class=\"material-icons i-info\" *ngIf=\"element.status=='Rejected'?false:true\"\r\n              (click)=\"btnEditClick(element,true)\" matTooltip=\"Info\">info</a>\r\n            <a class=\"material-icons i-edit\" *ngIf=\"element.isEditable\" matTooltip=\"Edit\"\r\n              (click)=\"btnEditClick(element,false)\">edit</a>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" *ngIf=\"!isCheckerLogin && element.isEditable\"\r\n              (click)=\"DeleteRevokDetails(element.empRevocId,true);\">\r\n              delete_forever\r\n            </a>\r\n          </div>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"revokColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let rowData; columns: revokColumns\"></tr>\r\n    </table>\r\n    <mat-paginator #pagingRevok [pageSize]=\"5\" [pageIndex]=\"0\" [pageSizeOptions]=\"[5,10,15]\" showFirstLastButtons>\r\n    </mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteRevokDetails(revId,false)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"hooCheckerPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure, you want to Accept?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure, you want to Reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">{{successMsg}}</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VarifiedRevok(revId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedRevok(revId,false)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerPopup = !hooCheckerPopup\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/suspension/revocation/revocation.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/suspension/revocation/revocation.component.ts ***!
  \***************************************************************/
/*! exports provided: RevocationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RevocationComponent", function() { return RevocationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Suspension/Suspension_service */ "./src/app/services/Suspension/Suspension_service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _model_Suspension_RevocationDetails__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../model/Suspension/RevocationDetails */ "./src/app/model/Suspension/RevocationDetails.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RevocationComponent = /** @class */ (function () {
    function RevocationComponent(objsuspensionservice, snackBar, fb) {
        this.objsuspensionservice = objsuspensionservice;
        this.snackBar = snackBar;
        this.fb = fb;
        this.isCheckerLogin = false;
        this.selectedvalue = 0;
        this.isRejected = false;
        this.disableButton = false;
        this.isViewDetails = true;
        this.revokHistoryColumns = ['OrderNo', 'OrderDate', 'RevocationDate', 'Remarks', 'Status'];
        this.revokColumns = ['OrderNo', 'OrderDate', 'RevocationDate', 'Remarks', 'Status', 'Action'];
        // this.CreateSuspensionDetailsForm();
    }
    RevocationComponent.prototype.ngOnInit = function () {
        this.objempRevokDetails = new _model_Suspension_RevocationDetails__WEBPACK_IMPORTED_MODULE_6__["RevocationDetails"]();
        if (+sessionStorage.getItem('userRoleID') === 9) {
            this.isCheckerLogin = false;
        }
        else {
            this.isCheckerLogin = true;
        }
        this.checkerbtnDisable = true;
        console.log(this.isCheckerLogin);
        this.GetAllDesignation();
        this.GetAllEmployees('0');
        this.reasonText = '';
        this.isReasonEmpty = false;
        this.isRejected = false;
        this.todayDate = new Date();
    };
    RevocationComponent.prototype.ddlDesignationChanged = function (desigId) {
        this.desigddl.placeholder = null;
        if (Number(desigId) === -1) {
            this.selectedvalue = null;
            //  this.objempModel = this.allempList;
            this.desigddl.placeholder = 'Select Designation';
            desigId = '0';
        }
        this.GetAllEmployees(desigId);
    };
    RevocationComponent.prototype.GetAllDesignation = function () {
        var _this = this;
        this.objsuspensionservice
            .GetAllDesignation('')
            .subscribe(function (res) {
            _this.objempDesignation = res;
        });
    };
    RevocationComponent.prototype.GetAllEmployees = function (desigId) {
        var _this = this;
        var comName = 'revok';
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice
            .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
            .subscribe(function (res) {
            _this.objempModel = res;
        });
    };
    RevocationComponent.prototype.ddlEmployeeChanged = function (empId) {
        debugger;
        this.myform.reset();
        this.checkerbtnDisable = true;
        this.selectedEmpSusId = empId.split('@')[0].trim();
        this.selectedvalue = Number(empId.split('@')[1]);
        this.empId = Number(empId.split('@')[2]);
        this.BindRevocationDetails(Number(this.selectedEmpSusId));
    };
    RevocationComponent.prototype.BindRevocationDetails = function (selectedvalue) {
        var _this = this;
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice.GetRevocationDetails(selectedvalue, this.empId, checkerLogin).subscribe(function (result) {
            console.log(result);
            _this.dataRevocation = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() !== 'V'; }));
            _this.dataRevocation.paginator = _this.pagingRevok;
            _this.dataRevocation.sort = _this.revSort;
            _this.exPanel.disabled = false;
            if (_this.dataRevocation.data.length > 0) {
                _this.exPanel.close();
                _this.exPanel.disabled = true;
            }
            _this.dataRevocationHistory = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result.filter(function (obj) { return obj.flagStatus.trim() === 'V'; }));
            _this.dataRevocationHistory.paginator = _this.pagingRevokH;
            _this.dataRevocationHistory.sort = _this.revHSort;
        });
    };
    RevocationComponent.prototype.SavedRevocationDetails = function (objRevok) {
        var _this = this;
        debugger;
        this.disableButton = true;
        objRevok.EmpTransSusId = Number(this.selectedEmpSusId);
        objRevok.EmpRevocId = this.revId;
        // console.log(objRevok.EmpTransSusId);
        // console.log(objRevok.RevocOrderDate);
        // console.log(objRevok.RevocationDate);
        // console.log(objRevok.RevocOrderNo);
        // console.log(objRevok.Remarks);
        // tslint:disable-next-line:no-debugger
        if (objRevok.FlagStatus === void 0) {
            objRevok.FlagStatus = 'RE';
        }
        // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
        // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
        switch (objRevok.FlagStatus) {
            case 'F':
                this.successMsg = 'Revocation Details Forwarded Successfully';
                break;
            case 'R':
                this.successMsg = 'Revocation Details Returned Successfully';
                break;
            default:
                this.successMsg = 'Revocation Details saved successfully';
                break;
        }
        this.objsuspensionservice.SavedRevocationDetails(objRevok).subscribe(function (arg) {
            if (+arg > 0) {
                _this.revId = 0;
                _this.BindRevocationDetails(Number(_this.selectedEmpSusId));
                _this.disableButton = false;
                _this.myform.reset();
            }
            else {
                _this.successMsg = arg;
            }
            // this.objempExtDetails.Id = 0;
            // this.objempExtDetails.Status = null;
            // this.BindSuspensionDetails(this.selectedEmpCode);
            _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
        });
    };
    RevocationComponent.prototype.ForwardToChecker = function (obj) {
        obj.FlagStatus = 'F';
        this.SavedRevocationDetails(obj);
    };
    RevocationComponent.prototype.DeleteRevokDetails = function (id, flag) {
        var _this = this;
        debugger;
        this.revId = flag ? id : 0;
        if (!flag) {
            this.objsuspensionservice.DeleteSuspensionDetails(id, 'revok').subscribe(function (arg) {
                _this.successMsg = arg;
                _this.BindRevocationDetails(Number(_this.selectedEmpSusId));
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        // $('.dialog__close-btn').click();
        this.deletepopup = flag;
    };
    RevocationComponent.prototype.applyFilter = function (filterValue) {
        if (this.isCheckerLogin) {
            this.dataRevocation.filter = filterValue.trim().toLowerCase();
            if (this.dataRevocation.paginator) {
                this.dataRevocation.paginator.firstPage();
            }
        }
        else {
            this.dataRevocationHistory.filter = filterValue.trim().toLowerCase();
            if (this.dataRevocationHistory.paginator) {
                this.dataRevocationHistory.paginator.firstPage();
            }
        }
    };
    RevocationComponent.prototype.btnEditClick = function (objRevok, isViewOnly) {
        debugger;
        this.exPanel.disabled = false;
        this.exPanel.open();
        this.objempRevokDetails.EmpRevocId = objRevok.empRevocId;
        this.objempRevokDetails.RevocOrderNo = objRevok.revocOrderNo;
        this.objempRevokDetails.RevocOrderDate = objRevok.revocOrderDate;
        this.objempRevokDetails.RevocationDate = objRevok.revocationDate;
        this.objempRevokDetails.FlagStatus = objRevok.flagStatus;
        this.objempRevokDetails.Remarks = objRevok.remarks;
        this.revId = objRevok.empRevocId;
        this.isRejected = false;
        if (objRevok.rejectedReason !== '') {
            this.objempRevokDetails.RejectedReason = objRevok.rejectedReason;
            this.isRejected = true;
        }
        this.isViewDetails = !isViewOnly;
        if (this.isCheckerLogin) {
            this.checkerbtnDisable = false;
        }
        // if (this.isCheckerLogin) {
        //  // this.myform.form.disable();
        //  this.myform.controls.forEach(function(element) {
        //    element.disable();
        //  });
        // }
        console.log(this.myform.controls);
        console.log(this.myform.form.controls);
        if (isViewOnly) {
            this.myform.form.disabled = true;
        }
    };
    RevocationComponent.prototype.VarifiedRevok = function (id, flag) {
        var _this = this;
        console.log(id);
        if (!flag && id > 0) {
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_revok', 'V', '').subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Varified successfully';
                    _this.revId = 0;
                    _this.BindRevocationDetails(Number(_this.selectedEmpSusId));
                    _this.myform.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
        }
        this.hooCheckerPopup = flag;
        this.isVarified = flag;
    };
    RevocationComponent.prototype.RejectedRevok = function (id, flag) {
        var _this = this;
        debugger;
        console.log(id);
        this.hooCheckerPopup = flag;
        if (!flag && id > 0) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                this.successMsg = 'Please enter reason';
                return false;
            }
            if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
                this.isReasonEmpty = true;
                this.successMsg = 'Special characters are not allowed';
                return false;
            }
            this.successMsg = null;
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_revok', 'R', this.reasonText).subscribe(function (arg) {
                _this.successMsg = arg;
                if (+arg > 0) {
                    _this.successMsg = 'Rejected successfully';
                    _this.revId = 0;
                    _this.BindRevocationDetails(Number(_this.selectedEmpSusId));
                    _this.myform.reset();
                }
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            return false;
        }
        this.isVarified = !flag;
        return false;
    };
    RevocationComponent.prototype.Reset = function () {
        this.myform.reset();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('revHSort'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RevocationComponent.prototype, "revHSort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingRevokH'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], RevocationComponent.prototype, "pagingRevokH", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('revSort'),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RevocationComponent.prototype, "revSort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pagingRevok'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], RevocationComponent.prototype, "pagingRevok", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formCreation'),
        __metadata("design:type", Object)
    ], RevocationComponent.prototype, "myform", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelect"])
    ], RevocationComponent.prototype, "desigddl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('revPanel'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionPanel"])
    ], RevocationComponent.prototype, "exPanel", void 0);
    RevocationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-revocation',
            template: __webpack_require__(/*! ./revocation.component.html */ "./src/app/suspension/revocation/revocation.component.html"),
            styles: [__webpack_require__(/*! ./revocation.component.css */ "./src/app/suspension/revocation/revocation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_3__["SuspensionService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]])
    ], RevocationComponent);
    return RevocationComponent;
}());



/***/ }),

/***/ "./src/app/suspension/suspension-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/suspension/suspension-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SuspensionRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspensionRoutingModule", function() { return SuspensionRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _suspension_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./suspension.module */ "./src/app/suspension/suspension.module.ts");
/* harmony import */ var _joining_joining_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./joining/joining.component */ "./src/app/suspension/joining/joining.component.ts");
/* harmony import */ var _suspension_suspension_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./suspension/suspension.component */ "./src/app/suspension/suspension/suspension.component.ts");
/* harmony import */ var _extension_extension_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./extension/extension.component */ "./src/app/suspension/extension/extension.component.ts");
/* harmony import */ var _revocation_revocation_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./revocation/revocation.component */ "./src/app/suspension/revocation/revocation.component.ts");
/* harmony import */ var _regularisation_regularisation_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./regularisation/regularisation.component */ "./src/app/suspension/regularisation/regularisation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: '', component: _suspension_module__WEBPACK_IMPORTED_MODULE_2__["SuspensionModule"], children: [
            { path: 'Joining', component: _joining_joining_component__WEBPACK_IMPORTED_MODULE_3__["JoiningComponent"] },
            { path: 'Suspension', component: _suspension_suspension_component__WEBPACK_IMPORTED_MODULE_4__["SuspensionComponent"] },
            { path: 'Extension', component: _extension_extension_component__WEBPACK_IMPORTED_MODULE_5__["ExtensionComponent"] },
            { path: 'Revocation', component: _revocation_revocation_component__WEBPACK_IMPORTED_MODULE_6__["RevocationComponent"] },
            { path: 'Regularisation', component: _regularisation_regularisation_component__WEBPACK_IMPORTED_MODULE_7__["RegularisationComponent"] }
        ]
    }
];
var SuspensionRoutingModule = /** @class */ (function () {
    function SuspensionRoutingModule() {
    }
    SuspensionRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SuspensionRoutingModule);
    return SuspensionRoutingModule;
}());



/***/ }),

/***/ "./src/app/suspension/suspension.module.ts":
/*!*************************************************!*\
  !*** ./src/app/suspension/suspension.module.ts ***!
  \*************************************************/
/*! exports provided: SuspensionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspensionModule", function() { return SuspensionModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _suspension_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./suspension-routing.module */ "./src/app/suspension/suspension-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _joining_joining_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./joining/joining.component */ "./src/app/suspension/joining/joining.component.ts");
/* harmony import */ var _suspension_suspension_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./suspension/suspension.component */ "./src/app/suspension/suspension/suspension.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _extension_extension_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./extension/extension.component */ "./src/app/suspension/extension/extension.component.ts");
/* harmony import */ var _revocation_revocation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./revocation/revocation.component */ "./src/app/suspension/revocation/revocation.component.ts");
/* harmony import */ var _regularisation_regularisation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./regularisation/regularisation.component */ "./src/app/suspension/regularisation/regularisation.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











//import { SharecomponentModule } from '../sharecomponent/sharecomponent.module';


var SuspensionModule = /** @class */ (function () {
    function SuspensionModule() {
    }
    SuspensionModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_joining_joining_component__WEBPACK_IMPORTED_MODULE_5__["JoiningComponent"], _suspension_suspension_component__WEBPACK_IMPORTED_MODULE_6__["SuspensionComponent"], _extension_extension_component__WEBPACK_IMPORTED_MODULE_8__["ExtensionComponent"], _revocation_revocation_component__WEBPACK_IMPORTED_MODULE_9__["RevocationComponent"], _regularisation_regularisation_component__WEBPACK_IMPORTED_MODULE_10__["RegularisationComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _suspension_routing_module__WEBPACK_IMPORTED_MODULE_3__["SuspensionRoutingModule"], _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__["MatTooltipModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_11__["NgxMatSelectSearchModule"]]
        })
    ], SuspensionModule);
    return SuspensionModule;
}());



/***/ }),

/***/ "./src/app/suspension/suspension/suspension.component.css":
/*!****************************************************************!*\
  !*** ./src/app/suspension/suspension/suspension.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .popupbox .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100% !important;\r\n}\r\n\r\n  .min-w {\r\n  width: 173px;\r\n}\r\n\r\n  .reason-label {\r\n  line-height: 75px;\r\n  text-align: right;\r\n}\r\n\r\n  .errormsg{\r\n  color: red;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VzcGVuc2lvbi9zdXNwZW5zaW9uL3N1c3BlbnNpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7Q0FDeEI7O0VBRUM7SUFDRSxZQUFZO0dBQ2I7O0VBRUg7RUFDRSxrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztDQUNaOztFQUVEO0VBQ0Usb0ZBQW9GO0VBQ3BGLHVCQUF1QjtDQUN4Qjs7RUFFRCxlQUFlOztFQUVmO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UscUJBQXFCO0NBQ3RCOztFQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztFQUVEO0VBQ0Usb0dBQW9HO0NBQ3JHOztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7RUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFHRCxnQkFBZ0I7O0VBQ2hCO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7Q0FDRjs7RUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsV0FBVztHQUNaO0NBQ0Y7O0VBQ0QsYUFBYTs7RUFDYjtFQUNFLG9DQUFvQztDQUNyQzs7RUFFRDtFQUNFLFlBQVk7Q0FDYjs7RUFHRDtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsaUJBQWlCO0NBQ2xCOztFQUVEO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvc3VzcGVuc2lvbi9zdXNwZW5zaW9uL3N1c3BlbnNpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxucCB7XHJcbiAgZm9udC1mYW1pbHk6IExhdG87XHJcbn1cclxuXHJcbi5leGFtcGxlLWNhcmQge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiA0cHhcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4vKiAyOS1qYW4tMTkgKi9cclxuXHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5wb3B1cGJveCAubWF0LWhlYWRlci1jZWxsLm1hdC1zb3J0LWhlYWRlci1zb3J0ZWQge1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm10LTEwIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG4uc2VsZWN0LWxibCB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZC1pbmZpeCB7XHJcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1pbi13IHtcclxuICB3aWR0aDogMTczcHg7XHJcbn1cclxuXHJcbi5yZWFzb24tbGFiZWwge1xyXG4gIGxpbmUtaGVpZ2h0OiA3NXB4O1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4uZXJyb3Jtc2d7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/suspension/suspension/suspension.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/suspension/suspension/suspension.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Atul Walia 27/Feb/19--->\r\n<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select #singleSelect placeholder=\"Select Designation\"\r\n                      (selectionChange)=\"ddlDesignationChanged($event.value)\" [(value)]=\"selectedvalue\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"desigfilterddl\" [noEntriesFoundLabel]=\"'No Records Found'\" [placeholderLabel]=\"'Find Designation...'\">\r\n              </ngx-mat-select-search>\r\n            </mat-option>\r\n            <!-- <mat-option [value]=\"-1\">-------- select designation --------</mat-option> -->\r\n            <mat-option *ngFor=\"let item of filteredDesig | async\" [value]=\"item.desigId\">\r\n              {{ item.desigDesc }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Select Employee:</label>\r\n      <!--<button mat-mini-fab color=\"primary\" class=\"go-btn\">Go</button>-->\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Employee\" (selectionChange)=\"ddlEmployeeChanged($event.value)\" [(value)]=\"selectedEmpValue\">\r\n            <mat-option *ngFor=\"let item of objempModel\" [value]=\"item.empCd\">\r\n              {{ item.empName }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--------------- Left Form --------------->\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel #susPanel>\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Enter New Suspension Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <form [formGroup]=\"SuspensionDetailsForm\" #formCreation=\"ngForm\" ngNativeValidate\r\n          (ngSubmit)=\"SavedSuspensionDetails(objempSusDetails)\">\r\n\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"FromDate\" name=\"FromDate\" [max]=\"todayDate\" [(ngModel)]=\"objempSusDetails.FromDate\"\r\n                 placeholder=\"From Date\" formControlName=\"FromDate\" required readonly\r\n                 (dateChange)=\"AutoFillDate($event.target.value,true)\" />\r\n          <mat-datepicker-toggle matSuffix [for]=\"FromDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #FromDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order Number\" name=\"OrderNo\" [(ngModel)]=\"objempSusDetails.OrderNo\"\r\n                 formControlName=\"OrderNo\" pattern=\"[-(),/0-9a-zA-Z \\\\]*\" required />\r\n                 <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Suspension Period (Enter No of Days)\"\r\n                 (input)=\"AutoFillDate($event.target.value,false)\" type=\"number\" name=\"SusPeriod\" formControlName=\"SusPeriod\"\r\n                 [(ngModel)]=\"objempSusDetails.SusPeriod\" pattern=\"[0-9]*\" />\r\n                 <mat-error>Please enter number between 1 and 120</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!-- <input matInput [matDatepicker]=\"ValidUptoDate\" name=\"ValidUptoDate\" disabled [value]=\"02/02/2019\"\r\n      placeholder=\"Valid Upto\" readonly />\r\n    <mat-datepicker-toggle matSuffix [for]=\"ValidUptoDate\"></mat-datepicker-toggle>\r\n    <mat-datepicker #ValidUptoDate></mat-datepicker> -->\r\n          <input matInput placeholder=\"Valid Upto\" name=\"ValidUptoDate\" disabled [value]=\"validUptoDate\" />\r\n        </mat-form-field>\r\n      </div>\r\n        <!-- <div class=\"col-sm-12 col-md-6 col-lg-6 line-height-57\">\r\n          <mat-checkbox (value)=\"objempSusDetails.IsTillOrder\" formControlName=\"IsTillOrder\"\r\n            [(ngModel)]=\"objempSusDetails.IsTillOrder\" class=\"chk-single\" name=\"IsTillOrder\" color=\"primary\"\r\n            (change)=\"chkTillFurtherOrder($event)\">Till further Orders</mat-checkbox>\r\n        </div> -->\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" name=\"OrderDate\" [max]=\"todayDate\" [(ngModel)]=\"objempSusDetails.OrderDate\"\r\n                 placeholder=\"Order Date\" formControlName=\"OrderDate\" readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"% of Salary during Suspension Period\" maxlength=\"2\" min=\"1\" max=\"99\"\r\n                 type=\"number\" name=\"SalaryPercent\" required formControlName=\"SalaryPercent\" pattern=\"[0-9]*\"\r\n                 [(ngModel)]=\"objempSusDetails.SalaryPercent\" />\r\n          <mat-error>Please enter a value between 50 and 75</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!--<textarea matInput placeholder=\"Name\" required></textarea>-->\r\n          <textarea matInput placeholder=\"Reason\" name=\"Remarks\" [(ngModel)]=\"objempSusDetails.Remarks\"\r\n                    formControlName=\"Remarks\" pattern=\"[-(),/a-zA-Z \\\\]*\" required></textarea>\r\n          <!--<textarea matInput placeholder=\"First Name\" required>-->\r\n            <mat-error>Special symbols are not allowed except (/, -)</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n        <div *ngIf=\"isRejected\" class=\"col-sm-12 col-md-4 col-lg-4 mt-10\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Rejected Reason\" name=\"RejectedReason\" [(ngModel)]=\"objempSusDetails.RejectedReason\"\r\n                      formControlName=\"RejectedReason\"></textarea>\r\n          </mat-form-field>\r\n        </div>\r\n        <div *ngIf=\"isViewDetails\">\r\n          <div *ngIf=\"this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"button\" class=\"btn btn-success\"\r\n                    (click)=\"VarifiedSus(_Id,false);hooCheckerLogin = !hooCheckerLogin\"\r\n                    [disabled]=\"SuspensionDetailsForm.invalid\">\r\n              {{_btnText}}\r\n            </button>\r\n            <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>-->\r\n            <button class=\"btn btn-primary\" type=\"button\"\r\n                    (click)=\"RejectedSus(_Id,false);hooCheckerLogin = !hooCheckerLogin\"\r\n                    [disabled]=\"SuspensionDetailsForm.invalid\">\r\n              {{_btnUpdateText}}\r\n            </button>\r\n          </div>\r\n          <div *ngIf=\"!this.isCheckerLogin\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"SuspensionDetailsForm.invalid || disableButton\">{{_btnText}}</button>\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>\r\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"ForwardToChecker(objempSusDetails)\"\r\n                    [disabled]=\"SuspensionDetailsForm.invalid || disableButton\">\r\n              {{_btnUpdateText}}\r\n            </button>\r\n          </div>\r\n        </div>\r\n\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Suspension Details</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\" [disabled]=\"orderValidFlag\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <!--tabel 2 start-->\r\n    <div *ngIf=\"!this.isCheckerLogin\">\r\n      <div class=\"fom-title mt-40 \">Old Suspension Details</div>\r\n      <table mat-table [(dataSource)]=\"dataSourceHistory\" #t2Sort=\"matSort\" matSort\r\n             class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- Order No. -->\r\n        <ng-container matColumnDef=\"OrderNo\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Number</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderNo }}</td>\r\n        </ng-container>\r\n        <!-- Order Date -->\r\n        <ng-container matColumnDef=\"OrderDate\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderDate  | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- From Date -->\r\n        <ng-container matColumnDef=\"FromDate\">\r\n          <th mat-header-cell *matHeaderCellDef>From Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.fromDate  | date: 'dd/MM/yyyy' }}</td>\r\n        </ng-container>\r\n        <!-- To Date Order No. -->\r\n        <ng-container matColumnDef=\"ToDate\">\r\n          <th mat-header-cell *matHeaderCellDef>To Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.toDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- No. of Days -->\r\n        <ng-container matColumnDef=\"Remarks\">\r\n          <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.remarks}}</td>\r\n        </ng-container>\r\n        <!---Status-->\r\n        <ng-container matColumnDef=\"Status\">\r\n          <th mat-header-cell *matHeaderCellDef>Status</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{element.status}}</td>\r\n        </ng-container>\r\n        <!--status end-->\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"historyTableColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let element; columns: historyTableColumns\"></tr>\r\n      </table>\r\n      <mat-paginator #paginatorTable2 [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\" showFirstLastButtons>\r\n      </mat-paginator>\r\n    </div>\r\n    <div>\r\n      <button *ngIf=\"this.isCheckerLogin\" mat-button class=\"btn-primary\">\r\n        View Forwarded Suspension Order\r\n        Details\r\n      </button>\r\n      <button *ngIf=\"!this.isCheckerLogin\" mat-button class=\"btn-primary\" (click)=\"ShowSuspensionDetails(true)\">\r\n        View New\r\n        Captured Suspension Order Details\r\n      </button>\r\n    </div>\r\n    <div [hidden]=\"!ShowSusDetails\">\r\n      <div class=\"fom-title mt-40 \">\r\n        New Captured Suspension Details <button *ngIf=\"!this.isCheckerLogin\" mat-button\r\n                                                class=\"btn btn-warning\" (click)=\"ShowSuspensionDetails(false)\">\r\n          Close\r\n        </button>\r\n      </div>\r\n      <table mat-table [(dataSource)]=\"dataSource\" #t1Sort=\"matSort\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- Order No. -->\r\n        <ng-container matColumnDef=\"OrderNo\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Number</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderNo }}</td>\r\n        </ng-container>\r\n        <!-- Order Date -->\r\n        <ng-container matColumnDef=\"OrderDate\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.orderDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- From Date -->\r\n        <ng-container matColumnDef=\"FromDate\">\r\n          <th mat-header-cell *matHeaderCellDef>From Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.fromDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- To Date Order No. -->\r\n        <ng-container matColumnDef=\"ToDate\">\r\n          <th mat-header-cell *matHeaderCellDef>To Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.toDate | date: 'dd/MM/yyyy'}}</td>\r\n        </ng-container>\r\n        <!-- No. of Days -->\r\n        <ng-container matColumnDef=\"Remarks\">\r\n          <th mat-header-cell *matHeaderCellDef>Remarks</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{ element.remarks}}</td>\r\n        </ng-container>\r\n        <!---Status-->\r\n        <ng-container matColumnDef=\"Status\">\r\n          <th mat-header-cell *matHeaderCellDef>Status</th>\r\n          <td mat-cell *matCellDef=\"let element\">{{element.status}}</td>\r\n        </ng-container>\r\n        <!--status end-->\r\n        <ng-container matColumnDef=\"Action\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Action</th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <!-- <a href=\"#\">Edit</a> -->\r\n            <!-- <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a> -->\r\n            <div>\r\n              <a class=\"material-icons i-info\" *ngIf=\"element.status=='Rejected'?false:true\"\r\n                 (click)=\"btnEditClick(element,true)\" matTooltip=\"Info\">info</a>\r\n              <a class=\"material-icons i-edit\" *ngIf=\"element.isEditable\" matTooltip=\"Edit\"\r\n                 (click)=\"btnEditClick(element,false)\">edit</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" *ngIf=\"!isCheckerLogin && element.isEditable\"\r\n                 (click)=\"DeleteSuspensionDetails(element.id,true);deletepopup = !deletepopup\">\r\n                delete_forever\r\n              </a>\r\n            </div>\r\n          </td>\r\n        </ng-container>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let element; columns: displayedColumns\"></tr>\r\n      </table>\r\n      <!-- <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator> -->\r\n      <mat-paginator #paginatorTable1 [pageSize]=\"3\" [pageIndex]=\"0\" [pageSizeOptions]=\"[3,5,10]\"\r\n                     showFirstLastButtons>\r\n      </mat-paginator>\r\n    </div>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteSuspensionDetails(_Id,false)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"hooCheckerLogin\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure, you want to Accept?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure, you want to Reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">{{successMsg}}</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\"\r\n              (click)=\"VarifiedSus(_Id,true)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\"\r\n              (click)=\"RejectedSus(_Id,true)\">\r\n        Yes\r\n      </button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/suspension/suspension/suspension.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/suspension/suspension/suspension.component.ts ***!
  \***************************************************************/
/*! exports provided: SuspensionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspensionComponent", function() { return SuspensionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/Suspension/Suspension_service */ "./src/app/services/Suspension/Suspension_service.ts");
/* harmony import */ var _model_Suspension_SuspensionDetails__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/Suspension/SuspensionDetails */ "./src/app/model/Suspension/SuspensionDetails.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SuspensionComponent = /** @class */ (function () {
    function SuspensionComponent(objsuspensionservice, snackBar, fb) {
        this.objsuspensionservice = objsuspensionservice;
        this.snackBar = snackBar;
        this.fb = fb;
        this.susPeriodInNumber = 0;
        this.ShowSusDetails = false;
        this.isCheckerLogin = false;
        this.isViewDetails = true;
        this.selectedvalue = 0;
        this.selectedEmpValue = 0;
        this.isRejected = false;
        this.disableButton = false;
        this.displayedColumns = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status', 'Action'];
        this.historyTableColumns = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status'];
        /** control for the selected bank for option groups */
        this.desigfilterddl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        /** list of bank groups filtered by search keyword for option groups */
        this.filteredDesig = new rxjs__WEBPACK_IMPORTED_MODULE_6__["ReplaySubject"](1);
        /** Subject that emits when the component has been destroyed. */
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.CreateSuspensionDetailsForm();
    }
    SuspensionComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.GetPayBillGroup();
        // console.log(sessionStorage.getItem('userRole'));
        // listen for search field value changes
        this.desigfilterddl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesig();
        });
        console.log(sessionStorage.getItem('userRoleID'));
        this._btnUpdateText = 'Forward to  HOO Checker';
        this._btnText = 'Save';
        if (+sessionStorage.getItem('userRoleID') === 9) {
            this.isCheckerLogin = false;
        }
        else {
            this.isCheckerLogin = true;
            this.ShowSusDetails = true;
            this._btnUpdateText = 'Reject';
            this._btnText = 'Accept';
        }
        console.log(this.isCheckerLogin);
        this.GetAllDesignation();
        this.GetAllEmployees('0');
        this.objempSusDetails = new _model_Suspension_SuspensionDetails__WEBPACK_IMPORTED_MODULE_2__["SuspensionDetails"]();
        this.orderValidFlag = true;
        // this.SuspensionDetailsForm.disable();
        this.SuspensionDetailsForm.get('FromDate').disable();
        // this.SuspensionDetailsForm.get('btnSuspensionDetails').disable();
        this.validUptoDate = '';
        this.reasonText = '';
        this.isVarified = true;
        this.isReasonEmpty = false;
        this.todayDate = new Date();
        // if (this.isCheckerLogin === 'true') {
        //   this.SuspensionDetailsForm.disable();
        // }
    };
    SuspensionComponent.prototype.filterDesig = function () {
        if (!this.desigList) {
            return;
        }
        // get the search keyword
        var search = this.desigfilterddl.value;
        if (!search) {
            this.filteredDesig.next(this.desigList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the designs
        // this.filteredDesig.next(
        //   this.desig.filter(desg => {
        //     const showdesigDesc = desg.desigDesc.toLowerCase().indexOf(search) > -1;
        //     if (!showdesigDesc) {
        //       desg.desigDesc = desg.desigDesc.filter(desgn => desgn.name.toLowerCase().indexOf(search) > -1);
        //     }
        //     return desg.desigDesc.length > 0;
        //   })
        // );
        this.filteredDesig.next(this.desigList.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    // Search functionality
    // private filtercontroller()
    // {
    //   if (!this.controllerroleList)
    //   {
    //     return;
    //   }
    //   // get the search keyword
    //   let search = this.controllerFilterCtrl.value;
    //   if (!search)
    //   {
    //     this.filteredcontroller.next(this.controllerroleList.slice());
    //     return;
    //   }
    //   else {
    //     search = search.toLowerCase();
    //   }
    //   // filter the banks
    //   this.filteredcontroller.next(
    //     this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
    //   );
    // }
    SuspensionComponent.prototype.GetAllEmployees = function (desigId) {
        var _this = this;
        console.log(desigId);
        if (this.allempList != null && this.allempList.length > 0) {
            this.objempModel = this.allempList.filter(function (x) { return x.empCd.split('@')[1] == desigId; });
        }
        console.log(this.objempModel);
        var comName = 'suspen';
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        this.objsuspensionservice
            .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
            .subscribe(function (res) {
            _this.objempModel = res;
            if (Number(desigId) === 0) {
                _this.allempList = res;
            }
        });
    };
    SuspensionComponent.prototype.GetAllDesignation = function () {
        var _this = this;
        this.objsuspensionservice.GetAllDesignation('').subscribe(function (res) {
            _this.desigList = res;
            _this.filteredDesig.next(_this.desigList);
        });
    };
    // GetPayBillGroup() {
    //   this.objsuspensionservice.GetPayBillGroup().subscribe(res => {
    //     this.objempdesig = res;
    //   });
    // }
    SuspensionComponent.prototype.CreateSuspensionDetailsForm = function () {
        this.SuspensionDetailsForm = this.fb.group({
            FromDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            OrderNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            SusPeriod: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(120)]],
            OrderDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            SalaryPercent: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].max(75), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(50), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(2)]],
            Remarks: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            RejectedReason: ['', '']
        });
    };
    SuspensionComponent.prototype.ddlDesignationChanged = function (desigId) {
        this.selectedEmpValue = null;
        this.desigddl.placeholder = null;
        if (Number(desigId) === -1) {
            this.selectedvalue = null;
            this.objempModel = this.allempList;
            this.desigddl.placeholder = 'Select Designation';
            return true;
        }
        this.GetAllEmployees(desigId);
    };
    SuspensionComponent.prototype.ShowSuspensionDetails = function (isShow) {
        this.ShowSusDetails = isShow;
    };
    SuspensionComponent.prototype.AutoFillDate = function (daysAndDate, isdate) {
        var fdate;
        if (isdate) {
            fdate = new Date(daysAndDate);
            fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
        }
        else {
            this.susPeriodInNumber = daysAndDate;
            fdate = new Date(this.objempSusDetails.FromDate);
            fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
        }
        this.validUptoDate = fdate.toDateString();
    };
    // ddlPayBillGroupChanged(payBillGroupId: string) {
    //   this.objsuspensionservice
    //     .GetAllDesignationOnSelectedPayBillGroup(payBillGroupId)
    //     .subscribe(res => {
    //       this.objempDesignation = res;
    //     });
    // }
    SuspensionComponent.prototype.ddlEmployeeChanged = function (empId) {
        this.selectedEmpCode = empId.split('@')[0].trim();
        this.selectedvalue = Number(empId.split('@')[1]);
        this.isViewDetails = true;
        this.SuspensionDetailsForm.get('FromDate').enable();
        this.orderValidFlag = false;
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.Reset();
        if (this.isCheckerLogin) {
            this._btnText = 'Accept';
        }
    };
    SuspensionComponent.prototype.BindSuspensionDetails = function (empId) {
        var _this = this;
        var checkerLogin = this.isCheckerLogin ? 'true' : 'false';
        var comName = 'suspen';
        this.objsuspensionservice.GetSuspensionDetails(empId, checkerLogin, comName).subscribe(function (result) {
            console.log(result);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.status !== 'Verified'; }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.t1Sort;
            _this.exPanel.disabled = false;
            if (_this.dataSource.data.length > 0) {
                _this.exPanel.close();
                _this.exPanel.disabled = true;
            }
            _this.dataSourceHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result.filter(function (obj) { return obj.status === 'Verified'; }));
            _this.dataSourceHistory.paginator = _this.paginatorHistory;
            _this.dataSourceHistory.sort = _this.t2Sort;
        });
    };
    SuspensionComponent.prototype.GetSuspensionDetails = function () {
        alert('Go');
    };
    SuspensionComponent.prototype.chkTillFurtherOrder = function (chk) {
        // this.orderValidFlag = chk.checked ? true : false;
        var toDate = this.SuspensionDetailsForm.get('ToDate');
        if (chk.checked) {
            toDate.disable();
            this.objempSusDetails.ToDate = null;
            toDate.setValidators(null);
        }
        else {
            toDate.enable();
            toDate.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required);
        }
        toDate.updateValueAndValidity();
    };
    SuspensionComponent.prototype.SavedSuspensionDetails = function (objsus) {
        var _this = this;
        // tslint:disable-next-line:no-debugger
        debugger;
        this.disableButton = true;
        objsus.PermddoId = '00003';
        objsus.EmpCode = this.selectedEmpCode;
        console.log(this.selectedEmpCode);
        // if (objsus.IsTillOrder == null) { objsus.IsTillOrder = false; }
        if (objsus.Status == null) {
            objsus.Status = 'I';
        }
        // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
        // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
        switch (objsus.Status) {
            case 'F':
                this.successMsg = 'Suspension Details Forwarded Successfully';
                break;
            case 'R':
                this.successMsg = 'Suspension Details Returned Successfully';
                break;
            default:
                this.successMsg = 'Suspension Details saved successfully';
                break;
        }
        this.objsuspensionservice.SavedNewSuspensionDetails(objsus).subscribe(function (arg) {
            if (+arg > 0) {
                _this.myformm.resetForm();
                _this.objempSusDetails.Id = 0;
                _this.objempSusDetails.Status = null;
                _this._btnText = 'Save';
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.disableButton = false;
            }
            else {
                _this.successMsg = arg;
            }
            _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
        });
    };
    SuspensionComponent.prototype.ForwardToChecker = function (obj) {
        obj.Status = 'F';
        this.SavedSuspensionDetails(obj);
    };
    SuspensionComponent.prototype.VarifiedSus = function (id, flag) {
        var _this = this;
        if (flag && this.objempSusDetails.Id > 0) {
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_suspen', 'V', '').subscribe(function (arg) {
                if (+arg > 0) {
                    _this.successMsg = 'Varified successfully';
                }
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            this.myformm.resetForm();
            this.objempSusDetails.Id = 0;
            this.objempSusDetails.Status = null;
            this.validUptoDate = '';
            this.SuspensionDetailsForm.get('FromDate').enable();
            // $('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = true;
        }
    };
    SuspensionComponent.prototype.RejectedSus = function (id, flag) {
        var _this = this;
        if (flag && this.objempSusDetails.Id > 0) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                this.successMsg = 'Please enter reason';
                return false;
            }
            if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
                this.isReasonEmpty = true;
                this.successMsg = 'Special characters are not allowed';
                return false;
            }
            this.successMsg = null;
            this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_suspen', 'R', this.reasonText).subscribe(function (arg) {
                if (+arg > 0) {
                    _this.successMsg = 'Rejected successfully';
                }
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
                return false;
            });
            this.myformm.resetForm();
            this.objempSusDetails.Id = 0;
            this.objempSusDetails.Status = null;
            this.validUptoDate = '';
            this.SuspensionDetailsForm.get('FromDate').enable();
            // $('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = false;
            return false;
        }
    };
    // btnEditClick(id: number, fromDate: Date, toDate: number, orderNo: string, orderDate: Date, salaryPercent: number, remarks: string) {
    //   this.objempSusDetails.Id = id;
    //   this.objempSusDetails.FromDate = fromDate;
    //   this.objempSusDetails.ToDate = toDate;
    //   this.objempSusDetails.OrderNo = orderNo;
    //   this.objempSusDetails.OrderDate = orderDate;
    //   this.objempSusDetails.Remarks = remarks;
    //   this.objempSusDetails.SalaryPercent = salaryPercent;
    //   }
    SuspensionComponent.prototype.btnEditClick = function (objsus, isViewOnly) {
        debugger;
        this.exPanel.disabled = false;
        this.exPanel.open();
        this.objempSusDetails.Id = objsus.id;
        this.objempSusDetails.FromDate = objsus.fromDate;
        this.objempSusDetails.OrderDate = objsus.orderDate;
        this.objempSusDetails.OrderNo = objsus.orderNo;
        this.objempSusDetails.ToDate = objsus.toDate;
        this.objempSusDetails.SalaryPercent = objsus.salaryPercent;
        this.objempSusDetails.Remarks = objsus.remarks;
        this.objempSusDetails.SusPeriod = objsus.susPeriod;
        this._btnText = 'Save';
        this._Id = objsus.id;
        this.isRejected = false;
        if (objsus.rejectedReason !== '') {
            this.objempSusDetails.RejectedReason = objsus.rejectedReason;
            this.isRejected = true;
        }
        this.SuspensionDetailsForm.enable();
        // const toDate = this.SuspensionDetailsForm.get('ToDate');
        // if (objsus.isTillOrder) {
        //   this.objempSusDetails.ToDate = null;
        //   toDate.disable();
        //   toDate.setValidators(null);
        // } else {
        //   toDate.enable();
        //   toDate.setValidators(Validators.required);
        // }
        // toDate.updateValueAndValidity();
        var fdate = new Date(objsus.fromDate);
        fdate.setDate(fdate.getDate() + +objsus.susPeriod);
        this.validUptoDate = fdate.toDateString();
        this.isViewDetails = true;
        if (this.isCheckerLogin) {
            this._btnText = 'Accept';
            this.SuspensionDetailsForm.disable();
        }
        if (isViewOnly) {
            this.SuspensionDetailsForm.disable();
            this.isViewDetails = false;
        }
    };
    SuspensionComponent.prototype.DeleteSuspensionDetails = function (id, flag) {
        var _this = this;
        this.isViewDetails = true;
        this._Id = flag ? id : 0;
        if (!flag) {
            this.objsuspensionservice.DeleteSuspensionDetails(id, 'suspen').subscribe(function (arg) {
                _this.successMsg = arg;
                _this.BindSuspensionDetails(_this.selectedEmpCode);
                _this.snackBar.open(_this.successMsg, null, { duration: 4000 });
            });
            this.objempSusDetails = new _model_Suspension_SuspensionDetails__WEBPACK_IMPORTED_MODULE_2__["SuspensionDetails"]();
            this.validUptoDate = '';
        }
        // $('.dialog__close-btn').click();
        this.deletepopup = false;
    };
    SuspensionComponent.prototype.applyFilter = function (filterValue) {
        if (this.isCheckerLogin) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
            if (this.dataSource.paginator) {
                this.dataSource.paginator.firstPage();
            }
        }
        else {
            this.dataSourceHistory.filter = filterValue.trim().toLowerCase();
            if (this.dataSourceHistory.paginator) {
                this.dataSourceHistory.paginator.firstPage();
            }
        }
    };
    SuspensionComponent.prototype.Reset = function () {
        this._btnText = 'Save';
        this.objempSusDetails = new _model_Suspension_SuspensionDetails__WEBPACK_IMPORTED_MODULE_2__["SuspensionDetails"]();
        // this.SuspensionDetailsForm.reset();
        //    this.SuspensionDetailsForm.reset({
        // });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorTable1'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], SuspensionComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorTable2'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], SuspensionComponent.prototype, "paginatorHistory", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('t1Sort'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], SuspensionComponent.prototype, "t1Sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('t2Sort'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], SuspensionComponent.prototype, "t2Sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('susPanel'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionPanel"])
    ], SuspensionComponent.prototype, "exPanel", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formCreation'),
        __metadata("design:type", Object)
    ], SuspensionComponent.prototype, "myformm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelect"])
    ], SuspensionComponent.prototype, "desigddl", void 0);
    SuspensionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-suspension',
            template: __webpack_require__(/*! ./suspension.component.html */ "./src/app/suspension/suspension/suspension.component.html"),
            styles: [__webpack_require__(/*! ./suspension.component.css */ "./src/app/suspension/suspension/suspension.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Suspension_Suspension_service__WEBPACK_IMPORTED_MODULE_1__["SuspensionService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], SuspensionComponent);
    return SuspensionComponent;
}());



/***/ })

}]);
//# sourceMappingURL=suspension-suspension-module.js.map