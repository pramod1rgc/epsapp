(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["change-change-module"],{

/***/ "./src/app/change/add-ta-pd/add-ta-pd.component.css":
/*!**********************************************************!*\
  !*** ./src/app/change/add-ta-pd/add-ta-pd.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2FkZC10YS1wZC9hZGQtdGEtcGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvYWRkLXRhLXBkL2FkZC10YS1wZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/add-ta-pd/add-ta-pd.component.html":
/*!***********************************************************!*\
  !*** ./src/app/change/add-ta-pd/add-ta-pd.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"addtapdForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\"></textarea>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"addtapdForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"addtapdForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <label>Physically Disabled  </label><br />\r\n\r\n        <mat-radio-group formControlName=\"phsidsble\" required>\r\n          <mat-radio-button value=\"Y\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"N\">No</mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && addtapdForm?.controls.phsidsble?.errors?.required\">\r\n          Select Either Required Value\r\n        </mat-error>\r\n\r\n      </div>\r\n      <!--opt-->\r\n      <div *ngIf=\"PhsidsbleOpt\">\r\n        <div class=\"fom-title\">\r\n          Physical Disability Details\r\n\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          \r\n\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Type of Disability\" formControlName=\"phType\" [compareWith]=\"compare\" required>\r\n                <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n                  {{ot.text1}}\r\n                </mat-option>\r\n              </mat-select>\r\n\r\n              <mat-error>\r\n                Type of Disability Required\r\n              </mat-error>\r\n            </mat-form-field>\r\n         \r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Percentage of disability\" formControlName=\"phPcnt\"\r\n                   required onKeyPress=\"if(this.value.length==5) return false;\" maxlength=\"5\" max=\"100\" min=\"1\"\r\n                   pattern=\"^(?!0)(100|[1-9]?[0-9])(\\.\\d{1,2})?$\" step=\".01\" autocomplete=\"off\">\r\n\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phPcnt?.errors?.required\">Percentage of disability Required !</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phPcnt?.errors?.pattern\">Percentage of disability between 1 to 100</mat-error>\r\n\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <label>Severe (Y/N)   </label><br />\r\n\r\n          <mat-radio-group formControlName=\"phisSevere\">\r\n            <mat-radio-button value=\"Y\">Yes </mat-radio-button>\r\n            <mat-radio-button value=\"N\"> No </mat-radio-button>\r\n          </mat-radio-group>\r\n          <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && addtapdForm?.controls.phisSevere?.errors?.required\">\r\n            Select Either Required Value\r\n          </mat-error>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Certificate no\" formControlName=\"phCertNo\" required>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertNo?.errors?.required\">Certificate No. Required !</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertNo?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">         \r\n            <input matInput (click)=\"phCertDt.open()\" [matDatepicker]=\"phCertDt\" [max]=\"maxDate\" formControlName=\"phCertDt\"\r\n                   placeholder=\"Certificate Date\" autocomplete=off required>\r\n            <mat-datepicker-toggle matSuffix [for]=\"phCertDt\"></mat-datepicker-toggle>\r\n            <mat-datepicker #phCertDt></mat-datepicker>\r\n            <mat-error>Certificate date Required!</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Issuing Authority\" formControlName=\"phCertAuth\" required>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertAuth?.errors?.required\">Issuing Authority Required !</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertAuth?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertAuth?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n            <mat-error *ngIf=\"addtapdForm?.controls.phCertAuth?.errors?.pattern\">Special characters not allowed</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <label>Entitled for Double TA ?  </label><br />\r\n\r\n          <mat-radio-group formControlName=\"phentDoubleTa\" required>\r\n            <mat-radio-button value=\"Y\">Yes </mat-radio-button>\r\n            <mat-radio-button value=\"N\"> No </mat-radio-button>\r\n          </mat-radio-group>\r\n          <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && addtapdForm?.controls.phentDoubleTa?.errors?.required\">\r\n            Select Either Required Value\r\n          </mat-error>\r\n        </div>\r\n\r\n\r\n\r\n      </div>\r\n      \r\n\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{paginator.pageIndex * paginator.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentphsidsble\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Physically Disability\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentphsidsble}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedphsidsble\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Physically Disability\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedphsidsble}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/add-ta-pd/add-ta-pd.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/change/add-ta-pd/add-ta-pd.component.ts ***!
  \*********************************************************/
/*! exports provided: AddTaPDComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTaPDComponent", function() { return AddTaPDComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddTaPDComponent = /** @class */ (function () {
    function AddTaPDComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentphsidsble', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedphsidsble', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.PhsidsbleOpt = false;
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    AddTaPDComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    AddTaPDComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.addtapdForm.enable();
            //bind form
            this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
            this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.addtapdForm.disable();
        }
        this.onChanges(); //reset field by Y or N
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.addtapdForm.disable();
        }
    };
    AddTaPDComponent.prototype.createForm = function () {
        this.addtapdForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            phsidsble: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            phType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            phPcnt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''), phCertNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            phisSevere: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''), phCertDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            phCertAuth: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''), phentDoubleTa: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.addtapdForm.disable();
    };
    AddTaPDComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    AddTaPDComponent.prototype.onChanges = function () {
        var _this = this;
        this.addtapdForm.get('phsidsble')
            .valueChanges
            .subscribe(function (value) {
            var phsidsble = _this.addtapdForm.get('phsidsble');
            var phType = _this.addtapdForm.get('phType');
            var phPcnt = _this.addtapdForm.get('phPcnt');
            var phCertNo = _this.addtapdForm.get('phCertNo');
            var phisSevere = _this.addtapdForm.get('phisSevere');
            var phCertDt = _this.addtapdForm.get('phCertDt');
            var phCertAuth = _this.addtapdForm.get('phCertAuth');
            var phentDoubleTa = _this.addtapdForm.get('phentDoubleTa');
            if (value === 'Y') {
                debugger;
                _this.PhsidsbleOpt = true;
                phType.enable();
                phType.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phPcnt.enable();
                phPcnt.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phCertNo.enable();
                phCertNo.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phisSevere.enable();
                phisSevere.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phCertDt.enable();
                phCertDt.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phCertAuth.enable();
                phCertAuth.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
                phentDoubleTa.enable();
                phentDoubleTa.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            }
            else {
                _this.PhsidsbleOpt = false;
                phType.value == "";
                phType.disable();
                phType.clearValidators();
                phPcnt.value == "";
                phPcnt.disable();
                phPcnt.clearValidators();
                phCertNo.value == "";
                phCertNo.disable();
                phCertNo.clearValidators();
                phisSevere.value == "";
                phisSevere.disable();
                phisSevere.clearValidators();
                phCertDt.value == "";
                phCertDt.disable();
                phCertDt.clearValidators();
                phCertAuth.value == "";
                phCertAuth.disable();
                phCertAuth.clearValidators();
                phentDoubleTa.value == "";
                phentDoubleTa.disable();
                phentDoubleTa.clearValidators();
            }
        });
    };
    AddTaPDComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('doubleta_ph').subscribe(function (res) {
            _this.list = res;
        });
    };
    AddTaPDComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        //debugger;
        this.addtapdForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (!this.addtapdForm.valid) {
            return false;
        }
        //console.log(this.addtapdForm.value);
        if (this.addtapdForm.valid) { //change in proc of save & remove snackbar 
            debugger;
            this._service.InsertaddtapdFormDetails(this.addtapdForm.value, flag).subscribe(function (result) {
                debugger;
                _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.addtapdForm.disable();
            });
        }
    };
    AddTaPDComponent.prototype.getaddtapdFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        //debugger;
        //this.Bindddltype(); //set ddl
        this.pageNumber = 1;
        this._service.GetaddtapdFormDetails(empCd, roleId, flag).subscribe(function (data) {
            //debugger;
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentphsidsble.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    debugger;
                    self.addtapdForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.addtapdForm.enable();
                }
                if (_this.roleId == '5') {
                    self.addtapdForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //debugger;
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedphsidsble.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    AddTaPDComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    AddTaPDComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.addtapdForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            phsidsble: obj.phsidsble,
            phType: obj.phType,
            phPcnt: obj.phPcnt, phCertNo: obj.phCertNo,
            phisSevere: obj.phisSevere, phCertDt: obj.phCertDt,
            phCertAuth: obj.phCertAuth, phentDoubleTa: obj.phentDoubleTa,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        console.log(this.oldOrderNo);
        //debugger;
        this.addtapdForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    AddTaPDComponent.prototype.applyFiltercurrent = function (filterValue) {
        //debugger;
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    AddTaPDComponent.prototype.applyFilterhistory = function (filterValue) {
        //debugger;
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    AddTaPDComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    AddTaPDComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    AddTaPDComponent.prototype.confirmDelete = function () {
        var _this = this;
        //debugger;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'doubleta_ph').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.addtapdForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            //this.pageNumber = 1;
            _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    AddTaPDComponent.prototype.Cancel = function () {
        //debugger;
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    AddTaPDComponent.prototype.cancelForm = function () {
        //debugger;
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.addtapdForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    AddTaPDComponent.prototype.btnInfoClick = function (obj) {
        //debugger;
        this.oldOrderNo = '';
        this.addtapdForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            phsidsble: obj.phsidsble,
            phType: obj.phType,
            phPcnt: obj.phPcnt, phCertNo: obj.phCertNo,
            phisSevere: obj.phisSevere, phCertDt: obj.phCertDt,
            phCertAuth: obj.phCertAuth, phentDoubleTa: obj.phentDoubleTa,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //debugger;
        var empStatus = obj.status;
        this.addtapdForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.addtapdForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    AddTaPDComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.addtapdForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.addtapdForm.get('OrderNo').value;
        this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            //debugger;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    AddTaPDComponent.prototype.updateStatus = function (status) {
        //debugger;   
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.addtapdForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.addtapdForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getaddtapdFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.addtapdForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], AddTaPDComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], AddTaPDComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], AddTaPDComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], AddTaPDComponent.prototype, "sort", void 0);
    AddTaPDComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-ta-pd',
            template: __webpack_require__(/*! ./add-ta-pd.component.html */ "./src/app/change/add-ta-pd/add-ta-pd.component.html"),
            styles: [__webpack_require__(/*! ./add-ta-pd.component.css */ "./src/app/change/add-ta-pd/add-ta-pd.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], AddTaPDComponent);
    return AddTaPDComponent;
}());



/***/ }),

/***/ "./src/app/change/bank-details/bank-details.component.css":
/*!****************************************************************!*\
  !*** ./src/app/change/bank-details/bank-details.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2JhbmstZGV0YWlscy9iYW5rLWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvYmFuay1kZXRhaWxzL2JhbmstZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/bank-details/bank-details.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/change/bank-details/bank-details.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"BankForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"BankForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"BankForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"BankForm.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput formControlName=\"IFSCCode\" maxlength=\"11\" placeholder=\"IFSC Code\" oninput=\"this.value = this.value.toUpperCase()\"\r\n                 (keyup)=\"getBankDetailsByIFSC($event.target.value)\" autocomplete=off required>\r\n          <mat-error *ngIf=\"BankForm?.controls.IFSCCode?.errors?.required\">IFSC Code Required !</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.IFSCCode?.errors?.minlength\">Minimum Length required is 11 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.IFSCCode?.errors?.maxlength\">Maximum Length required is 11 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.IFSCCode?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"BankName\" placeholder=\"Bank Name\" autocomplete=off required [readonly]=\"BKreadonly\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"BranchName\" placeholder=\"Branch Name\" autocomplete=off required [readonly]=\"BKreadonly\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput formControlName=\"savingAcNo\" placeholder=\"Saving A/C No.\" autocomplete=off required\r\n                 [ngClass]=\"{ 'is-invalid': isSubmitted && BankForm.savingAcNo?.errors }\" />\r\n\r\n          <mat-error *ngIf=\"BankForm?.controls.savingAcNo?.errors?.required\">Saving A/C No. Required !</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.savingAcNo?.errors?.minlength\">Minimum Length required is 10 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.savingAcNo?.errors?.maxlength\">Maximum Length required is 20 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.savingAcNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"conSavingAcNo\" placeholder=\"Confirm Saving A/C No.\" autocomplete=off required\r\n                 [ngClass]=\"{ 'is-invalid': isSubmitted && BankForm.conSavingAcNo?.errors }\" />\r\n\r\n          <mat-error *ngIf=\"BankForm?.controls.conSavingAcNo?.errors?.required\">Confirm Saving A/C No. Required !</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.conSavingAcNo?.errors?.minlength\">Minimum Length required is 10 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.conSavingAcNo?.errors?.maxlength\">Maximum Length required is 20 characters.</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.conSavingAcNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n          <mat-error *ngIf=\"BankForm?.controls.conSavingAcNo?.errors?.mustMatch\">Saving A/C No. & Confirm  Saving A/C No. must matching</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <label required>A/c No. of ? </label><br />\r\n\r\n  <mat-radio-group class=\"example-radio-group\" formControlName=\"AccNoof\" required>\r\n    <mat-radio-button value=\"O\">Self</mat-radio-button>\r\n    <mat-radio-button value=\"D\">DDO</mat-radio-button>\r\n  </mat-radio-group>\r\n  <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && BankForm?.controls.AccNoof?.errors?.required\">\r\n    Select Either Required Value\r\n  </mat-error>\r\n\r\n</div>\r\n<div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n  <mat-form-field>\r\n    <input matInput formControlName=\"empCd\">\r\n  </mat-form-field>\r\n  <mat-form-field>\r\n    <input matInput formControlName=\"username\">\r\n  </mat-form-field>\r\n  <mat-form-field>\r\n    <input matInput formControlName=\"oldOrderNo\">\r\n  </mat-form-field>\r\n</div>\r\n<div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n  <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n  <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n  <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n</div>\r\n<div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n  ap\r\n  yp\r\n  <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n  <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n</div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n<!--<h3> {{ BankForm.value | json }} </h3>-->\r\n<!--- Table Right--->\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{paginator.pageIndex * paginator.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentbankName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Bank Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentbankName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentsavingAcNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Saving A/c No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentsavingAcNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentbranchName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Branch Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentbranchName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentifscCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            IFSC Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentifscCode}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentaccNoof\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Acc No of\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentaccNoof}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedbankName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Bank Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedbankName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedsavingAcNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Saving A/c No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedsavingAcNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedbranchName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Branch Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedbranchName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedifscCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            IFSC Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedifscCode}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedaccNoof\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Acc No of\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedaccNoof}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/bank-details/bank-details.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/change/bank-details/bank-details.component.ts ***!
  \***************************************************************/
/*! exports provided: BankDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankDetailsComponent", function() { return BankDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BankDetailsComponent = /** @class */ (function () {
    function BankDetailsComponent(_service, _msg, objBankDetailsService, fb) {
        this._service = _service;
        this._msg = _msg;
        this.objBankDetailsService = objBankDetailsService;
        this.fb = fb;
        this.editable = true;
        this.BKreadonly = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentbankName', 'currentsavingAcNo', 'currentbranchName', 'currentifscCode', 'currentaccNoof', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedbankName', 'revisedsavingAcNo', 'revisedbranchName', 'revisedifscCode', 'revisedaccNoof', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    BankDetailsComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    BankDetailsComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.BankForm.enable();
            this.getBankFormDetails1(this.empCd, this.roleId, 'history');
            this.getBankFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.BankForm.disable();
        }
        //bind form
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.BankForm.disable();
        }
    };
    //to Get Bank account by ifsc
    BankDetailsComponent.prototype.getBankDetailsByIFSC = function (IFSCcodevalue) {
        var _this = this;
        debugger;
        this.objBankDetailsService.getBankDetailsByIFSC(IFSCcodevalue).subscribe(function (res) {
            // debugger;
            _this.BankForm.patchValue({
                BankName: res.bankName,
                BranchName: res.branchName
            });
        });
    };
    // custom validator to check that two fields match
    BankDetailsComponent.prototype.MustMatch = function (controlName, matchingControlName) {
        return function (formGroup) {
            var control = formGroup.controls[controlName];
            var matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    };
    BankDetailsComponent.prototype.createForm = function () {
        this.BankForm = this.fb.group({
            OrderNo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])],
            OrderDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Remarks: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]],
            IFSCCode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z][a-zA-Z0-9 ]+$')]],
            BankName: [''],
            BranchName: [''],
            savingAcNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9]*$')]],
            conSavingAcNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9]*$')]],
            AccNoof: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            empCd: [this.empCd],
            username: [this.username],
            rejectionRemark: [this.rejectionRemark],
            oldOrderNo: ['']
        }, {
            validator: this.MustMatch('savingAcNo', 'conSavingAcNo')
        });
        this.BankForm.disable();
    };
    BankDetailsComponent.prototype.compare = function (o1, o2) {
        //
        if (o1 == o2)
            return true;
        else
            return false;
    };
    BankDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    BankDetailsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        this.BankForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        debugger;
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (!this.BankForm.valid) {
            return false;
        }
        if (this.BankForm.valid) { //change in proc of save & remove snackbar 
            debugger;
            this._service.InsertBankFormDetails(this.BankForm.value, flag).subscribe(function (result) {
                _this.getBankFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getBankFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.isSubmitted = false;
                _this.fwdtochker = false;
                _this.BankForm.disable();
            });
        }
    };
    BankDetailsComponent.prototype.getBankFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetBankFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                debugger;
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentbankName.toLowerCase().includes(filter) ||
                        data.currentsavingAcNo.toLowerCase().includes(filter) ||
                        data.currentbranchName.toLowerCase().includes(filter) ||
                        data.currentifscCode.toLowerCase().includes(filter) ||
                        data.currentaccNoof.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    debugger;
                    self.BankForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.BankForm.enable();
                }
                if (_this.roleId == '5') {
                    self.BankForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedbankName.toLowerCase().includes(filter) ||
                        data.revisedsavingAcNo.toLowerCase().includes(filter) ||
                        data.revisedbranchName.toLowerCase().includes(filter) ||
                        data.revisedifscCode.toLowerCase().includes(filter) ||
                        data.revisedaccNoof.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    BankDetailsComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.BankForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            IFSCCode: obj.ifscCode,
            BankName: obj.bankName,
            BranchName: obj.branchName,
            savingAcNo: obj.savingAcNo,
            conSavingAcNo: obj.conSavingAcNo,
            AccNoof: obj.accNoof,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.BankForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    BankDetailsComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    BankDetailsComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    BankDetailsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    BankDetailsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    BankDetailsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'Bank').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.BankForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getBankFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getBankFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    BankDetailsComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    BankDetailsComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.BankForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    BankDetailsComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.BankForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            IFSCCode: obj.ifscCode,
            BankName: obj.bankName,
            BranchName: obj.branchName,
            savingAcNo: obj.savingAcNo,
            conSavingAcNo: obj.conSavingAcNo,
            AccNoof: obj.accNoof,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.BankForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.BankForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    BankDetailsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.BankForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.BankForm.get('OrderNo').value;
        this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getBankFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getBankFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    BankDetailsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.BankForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getBankFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getBankFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.BankForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getBankFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getBankFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.BankForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], BankDetailsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], BankDetailsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], BankDetailsComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], BankDetailsComponent.prototype, "sort", void 0);
    BankDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bank-details',
            template: __webpack_require__(/*! ./bank-details.component.html */ "./src/app/change/bank-details/bank-details.component.html"),
            styles: [__webpack_require__(/*! ./bank-details.component.css */ "./src/app/change/bank-details/bank-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_6__["BankDetailsService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], BankDetailsComponent);
    return BankDetailsComponent;
}());



/***/ }),

/***/ "./src/app/change/castecategory-details/castecategory-details.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/change/castecategory-details/castecategory-details.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2Nhc3RlY2F0ZWdvcnktZGV0YWlscy9jYXN0ZWNhdGVnb3J5LWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvY2FzdGVjYXRlZ29yeS1kZXRhaWxzL2Nhc3RlY2F0ZWdvcnktZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/castecategory-details/castecategory-details.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/change/castecategory-details/castecategory-details.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"CasteCategoryForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\"></textarea>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"CasteCategoryForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"CasteCategoryForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">New Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Caste Category\" formControlName=\"CasteCategory\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            Caste Category required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{paginator.pageIndex * paginator.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentCasteCategory\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Caste Category\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentCasteCategory}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedCasteCategory\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Caste Category\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedCasteCategory}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Order No. Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/castecategory-details/castecategory-details.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/change/castecategory-details/castecategory-details.component.ts ***!
  \*********************************************************************************/
/*! exports provided: CastecategoryDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastecategoryDetailsComponent", function() { return CastecategoryDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CastecategoryDetailsComponent = /** @class */ (function () {
    function CastecategoryDetailsComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentCasteCategory', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedCasteCategory', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    CastecategoryDetailsComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    CastecategoryDetailsComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('CasteCategory').subscribe(function (res) {
            _this.list = res;
        });
    };
    CastecategoryDetailsComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.CasteCategoryForm.enable();
            this.getCasteCategoryFormDetails1(this.empCd, this.roleId, 'history');
            this.getCasteCategoryFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.CasteCategoryForm.disable();
        }
        //bind form
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.CasteCategoryForm.disable();
        }
    };
    CastecategoryDetailsComponent.prototype.createForm = function () {
        this.CasteCategoryForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            CasteCategory: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.CasteCategoryForm.disable();
    };
    CastecategoryDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    CastecategoryDetailsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.CasteCategoryForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.CasteCategoryForm.valid) { //change in proc of save & remove snackbar 
            debugger;
            this._service.InsertCasteCategoryFormDetails(this.CasteCategoryForm.value, flag).subscribe(function (result) {
                _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.CasteCategoryForm.disable();
            });
        }
    };
    CastecategoryDetailsComponent.prototype.getCasteCategoryFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetCasteCategoryFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                debugger;
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentCasteCategory.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    debugger;
                    self.CasteCategoryForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.CasteCategoryForm.enable();
                }
                if (_this.roleId == '5') {
                    self.CasteCategoryForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedCasteCategory.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    CastecategoryDetailsComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    CastecategoryDetailsComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.CasteCategoryForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            CasteCategory: obj.casteCategory,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.CasteCategoryForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    CastecategoryDetailsComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    CastecategoryDetailsComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    CastecategoryDetailsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    CastecategoryDetailsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    CastecategoryDetailsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'CasteCategory').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.CasteCategoryForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    CastecategoryDetailsComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    CastecategoryDetailsComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.CasteCategoryForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    CastecategoryDetailsComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.CasteCategoryForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            CasteCategory: obj.casteCategory,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.CasteCategoryForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.CasteCategoryForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    CastecategoryDetailsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.CasteCategoryForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.CasteCategoryForm.get('OrderNo').value;
        this._service.ForwardToCheckerCasteCategoryFormUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    CastecategoryDetailsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.CasteCategoryForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerCasteCategoryFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.CasteCategoryForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerCasteCategoryFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getCasteCategoryFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.CasteCategoryForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], CastecategoryDetailsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CastecategoryDetailsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CastecategoryDetailsComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], CastecategoryDetailsComponent.prototype, "sort", void 0);
    CastecategoryDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-castecategory-details',
            template: __webpack_require__(/*! ./castecategory-details.component.html */ "./src/app/change/castecategory-details/castecategory-details.component.html"),
            styles: [__webpack_require__(/*! ./castecategory-details.component.css */ "./src/app/change/castecategory-details/castecategory-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], CastecategoryDetailsComponent);
    return CastecategoryDetailsComponent;
}());



/***/ }),

/***/ "./src/app/change/cgegis-details/cgegis-details.component.css":
/*!********************************************************************!*\
  !*** ./src/app/change/cgegis-details/cgegis-details.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2NnZWdpcy1kZXRhaWxzL2NnZWdpcy1kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvY2hhbmdlL2NnZWdpcy1kZXRhaWxzL2NnZWdpcy1kZXRhaWxzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/change/cgegis-details/cgegis-details.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/change/cgegis-details/cgegis-details.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"CGEGISForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGEGISForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"CGEGISForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"CGEGIS applicable\" formControlName=\"cgegisapplicable\" [compareWith]=\"compare1\" required>\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            CGEGIS applicable required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Revised CGEGIS Group\" formControlName=\"cgegisgroup\" [compareWith]=\"compare2\" required>\r\n            <mat-option *ngFor=\"let ot of list1\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            Revised CGEGIS Group required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"RevisedMembershipDate.open()\" [matDatepicker]=\"RevisedMembershipDate\" [max]=\"maxDate\" formControlName=\"membershipdate\" placeholder=\"Revised Membership Date(in this Group)\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"RevisedMembershipDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #RevisedMembershipDate></mat-datepicker>\r\n          <mat-error>\r\n            Revised Membership Date Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n    \r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentcgegisapplicable\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGEGIS Applicable\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentCGEGISapplicable}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedcgegisapplicable\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGEGIS Applicable\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedCGEGISapplicable}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/cgegis-details/cgegis-details.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/change/cgegis-details/cgegis-details.component.ts ***!
  \*******************************************************************/
/*! exports provided: CgegisDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CgegisDetailsComponent", function() { return CgegisDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CgegisDetailsComponent = /** @class */ (function () {
    function CgegisDetailsComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmpName', 'currentcgegisapplicable', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmpName', 'revisedcgegisapplicable', 'Status'];
        this.list = [];
        this.list1 = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    CgegisDetailsComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    CgegisDetailsComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('CGEGISapplicable').subscribe(function (res) {
            _this.list = res;
        });
        this._service.GetDdlvalue('CGEGISGroup').subscribe(function (res) {
            _this.list1 = res;
        });
    };
    CgegisDetailsComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.CGEGISForm.enable();
            //bind form
            this.getCGEGISFormDetails1(this.empCd, this.roleId, 'history');
            this.getCGEGISFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.CGEGISForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.CGEGISForm.disable();
        }
    };
    CgegisDetailsComponent.prototype.createForm = function () {
        this.CGEGISForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            cgegisapplicable: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            cgegisgroup: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            membershipdate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.CGEGISForm.disable();
    };
    CgegisDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    CgegisDetailsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.CGEGISForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.CGEGISForm.valid) {
            this._service.InsertCGEGISFormDetails(this.CGEGISForm.value, flag).subscribe(function (result) {
                //this.pageNumber = 1;
                _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.CGEGISForm.disable();
            });
        }
    };
    CgegisDetailsComponent.prototype.getCGEGISFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetCGEGISFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentCGEGISapplicable.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                var list_1 = [];
                //enable or disbale form using current status of orderNo user
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.CGEGISForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.CGEGISForm.enable();
                }
                if (_this.roleId == '5') {
                    self.CGEGISForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedCGEGISapplicable.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    CgegisDetailsComponent.prototype.compare1 = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    CgegisDetailsComponent.prototype.compare2 = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    CgegisDetailsComponent.prototype.btnEditClick = function (obj) {
        this.CGEGISForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            cgegisapplicable: obj.cgegisapplicable,
            cgegisgroup: obj.cgegisgroup,
            membershipdate: obj.membershipdate,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.CGEGISForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    CgegisDetailsComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    CgegisDetailsComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    CgegisDetailsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    CgegisDetailsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    CgegisDetailsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'cgegis').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.CGEGISForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    CgegisDetailsComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    CgegisDetailsComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.CGEGISForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    CgegisDetailsComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.CGEGISForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            cgegisapplicable: obj.cgegisapplicable,
            cgegisgroup: obj.cgegisgroup,
            membershipdate: obj.membershipdate,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.CGEGISForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.CGEGISForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = true;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    CgegisDetailsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.CGEGISForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.CGEGISForm.get('OrderNo').value;
        this._service.ForwardToCheckerCGEGISFormUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    CgegisDetailsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.CGEGISForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerCGEGISFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.CGEGISForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerCGEGISFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getCGEGISFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.CGEGISForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], CgegisDetailsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CgegisDetailsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CgegisDetailsComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], CgegisDetailsComponent.prototype, "sort", void 0);
    CgegisDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cgegis-details',
            template: __webpack_require__(/*! ./cgegis-details.component.html */ "./src/app/change/cgegis-details/cgegis-details.component.html"),
            styles: [__webpack_require__(/*! ./cgegis-details.component.css */ "./src/app/change/cgegis-details/cgegis-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], CgegisDetailsComponent);
    return CgegisDetailsComponent;
}());



/***/ }),

/***/ "./src/app/change/cghs-details/cghs-details.component.css":
/*!****************************************************************!*\
  !*** ./src/app/change/cghs-details/cghs-details.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2NnaHMtZGV0YWlscy9jZ2hzLWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvY2docy1kZXRhaWxzL2NnaHMtZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/cghs-details/cghs-details.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/change/cghs-details/cghs-details.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"CGHSForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"CGHSForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <label>Whether CGHS Deduction Applicable</label><br />\r\n        <mat-radio-group formControlName=\"CGHSDeduction\" required>\r\n          <mat-radio-button value=\"Y\" (change)=\"getCGHSDetailsChange('Y');\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"N\" (change)=\"getCGHSDetailsChange('N');\">No</mat-radio-button>\r\n        </mat-radio-group>\r\n       \r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && CGHSForm?.controls.CGHSDeduction?.errors?.required\">\r\n          CGHS Deduction Required !\r\n        </mat-error>\r\n     \r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"CGHSCardNo\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"CGHSCardNo\" placeholder=\"CGHS Card No.\" autocomplete=off required >\r\n          <mat-error *ngIf=\"CGHSForm?.controls.CGHSCardNo?.errors?.required\">CGHS Card No. Required !</mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.CGHSCardNo?.errors?.minlength \">Minimum Length required is 6 </mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.CGHSCardNo?.errors?.maxlength\">Maximum Length required is 10 </mat-error>\r\n          <mat-error *ngIf=\"CGHSForm?.controls.CGHSCardNo?.errors?.pattern\">It only accept (0-9) characters.</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentCGHSDeductionApplicable\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGHS Deduction Applicable\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentCGHSDeductionApplicable}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentCGHSCardNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGHS Card No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentCGHSCardNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedCGHSDeductionApplicable\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGHS Deduction Applicable\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedCGHSDeductionApplicable}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedCGHSCardNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            CGHS Card No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedCGHSCardNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/cghs-details/cghs-details.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/change/cghs-details/cghs-details.component.ts ***!
  \***************************************************************/
/*! exports provided: CghsDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CghsDetailsComponent", function() { return CghsDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CghsDetailsComponent = /** @class */ (function () {
    function CghsDetailsComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmpName', 'currentCGHSDeductionApplicable', 'currentCGHSCardNo', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmpName', 'revisedCGHSDeductionApplicable', 'revisedCGHSCardNo', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.CGHSCardNo = false;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    CghsDetailsComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    CghsDetailsComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.CGHSForm.enable();
            //bind form
            this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
            this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.CGHSForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.CGHSForm.disable();
        }
        this.CGHSCardNo = false;
    };
    CghsDetailsComponent.prototype.getCGHSDetailsChange = function (value) {
        if (value == 'Y') {
            this.CGHSCardNo = true;
        }
        else if (value == 'N') {
            this.CGHSCardNo = false;
        }
    };
    CghsDetailsComponent.prototype.createForm = function () {
        this.CGHSForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            CGHSDeduction: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            CGHSCardNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[0-9]*$")]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.CGHSForm.disable();
    };
    CghsDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    CghsDetailsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        debugger;
        this.isSubmitted = true;
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.CGHSForm.valid) {
            this._service.InsertCGHSFormDetails(this.CGHSForm.value, flag).subscribe(function (result) {
                debugger;
                //this.pageNumber = 1;
                _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                        _this.CGHSCardNo = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.CGHSForm.disable();
            });
        }
    };
    CghsDetailsComponent.prototype.getCGHSFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetCGHSFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source    
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentCGHSDeductionApplicable.toLowerCase().includes(filter) || data.currentCGHSCardNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                var list_1 = [];
                //enable or disbale form using current status of orderNo user
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.CGHSForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.CGHSForm.enable();
                }
                if (_this.roleId == '5') {
                    self.CGHSForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedCGHSDeductionApplicable.toLowerCase().includes(filter) || data.revisedCGHSCardNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    CghsDetailsComponent.prototype.btnEditClick = function (obj) {
        this.CGHSForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            CGHSDeduction: obj.cghsDeduction,
            CGHSCardNo: obj.cghsCardNo,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.CGHSForm.enable();
        this.infoScreen = false;
        debugger;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var cghsDeduction = obj.cghsDeduction; //check cghsDeduction value or not
        if (cghsDeduction == 'Y') {
            this.CGHSCardNo = true;
        }
        else if (cghsDeduction == 'N') {
            this.CGHSCardNo = false;
        }
        var empStatus = obj.status;
        //if (empStatus == 'P' || empStatus == 'V' || empStatus == 'R') {
        //  this.editable = false;
        //}
        //else
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    CghsDetailsComponent.prototype.applyFiltercurrent = function (filterValue) {
        //
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    CghsDetailsComponent.prototype.applyFilterhistory = function (filterValue) {
        //
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    CghsDetailsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    CghsDetailsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.CGHSCardNo = false;
    };
    CghsDetailsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'cghs').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.CGHSCardNo = false;
                _this.CGHSForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    CghsDetailsComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    CghsDetailsComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.CGHSForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    CghsDetailsComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.CGHSForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            CGHSDeduction: obj.cghsDeduction,
            CGHSCardNo: obj.cghsCardNo,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var cghsDeduction = obj.cghsDeduction; //check cghsDeduction value or not
        debugger;
        if (cghsDeduction == 'Y') {
            this.CGHSCardNo = true;
        }
        else if (cghsDeduction == 'N') {
            this.CGHSCardNo = false;
        }
        var empStatus = obj.status;
        this.CGHSForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.CGHSForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = true;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    CghsDetailsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.CGHSForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.CGHSForm.get('OrderNo').value;
        this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    CghsDetailsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.CGHSForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.CGHSForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getCGHSFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.CGHSForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], CghsDetailsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CghsDetailsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], CghsDetailsComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], CghsDetailsComponent.prototype, "sort", void 0);
    CghsDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cghs-details',
            template: __webpack_require__(/*! ./cghs-details.component.html */ "./src/app/change/cghs-details/cghs-details.component.html"),
            styles: [__webpack_require__(/*! ./cghs-details.component.css */ "./src/app/change/cghs-details/cghs-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], CghsDetailsComponent);
    return CghsDetailsComponent;
}());



/***/ }),

/***/ "./src/app/change/change-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/change/change-routing.module.ts ***!
  \*************************************************/
/*! exports provided: ChangeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeRoutingModule", function() { return ChangeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _change_extensionofservice_extensionofservice_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../change/extensionofservice/extensionofservice.component */ "./src/app/change/extensionofservice/extensionofservice.component.ts");
/* harmony import */ var _dateofbirth_dateofbirth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dateofbirth/dateofbirth.component */ "./src/app/change/dateofbirth/dateofbirth.component.ts");
/* harmony import */ var _namegender_namegender_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./namegender/namegender.component */ "./src/app/change/namegender/namegender.component.ts");
/* harmony import */ var _change_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./change.module */ "./src/app/change/change.module.ts");
/* harmony import */ var _pf_nps_pf_nps_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pf-nps/pf-nps.component */ "./src/app/change/pf-nps/pf-nps.component.ts");
/* harmony import */ var _pan_no_pan_no_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pan-no/pan-no.component */ "./src/app/change/pan-no/pan-no.component.ts");
/* harmony import */ var _hra_city_class_hra_city_class_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./hra-city-class/hra-city-class.component */ "./src/app/change/hra-city-class/hra-city-class.component.ts");
/* harmony import */ var _exserviceman_exserviceman_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./exserviceman/exserviceman.component */ "./src/app/change/exserviceman/exserviceman.component.ts");
/* harmony import */ var _designation_designation_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./designation/designation.component */ "./src/app/change/designation/designation.component.ts");
/* harmony import */ var _cgegis_details_cgegis_details_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./cgegis-details/cgegis-details.component */ "./src/app/change/cgegis-details/cgegis-details.component.ts");
/* harmony import */ var _cghs_details_cghs_details_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./cghs-details/cghs-details.component */ "./src/app/change/cghs-details/cghs-details.component.ts");
/* harmony import */ var _joinin_mode_joinin_mode_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./joinin-mode/joinin-mode.component */ "./src/app/change/joinin-mode/joinin-mode.component.ts");
/* harmony import */ var _castecategory_details_castecategory_details_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./castecategory-details/castecategory-details.component */ "./src/app/change/castecategory-details/castecategory-details.component.ts");
/* harmony import */ var _bank_details_bank_details_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./bank-details/bank-details.component */ "./src/app/change/bank-details/bank-details.component.ts");
/* harmony import */ var _contact_details_contact_details_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./contact-details/contact-details.component */ "./src/app/change/contact-details/contact-details.component.ts");
/* harmony import */ var _entitlement_office_entitlement_office_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./entitlement-office/entitlement-office.component */ "./src/app/change/entitlement-office/entitlement-office.component.ts");
/* harmony import */ var _doj_controller_doj_controller_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./doj-controller/doj-controller.component */ "./src/app/change/doj-controller/doj-controller.component.ts");
/* harmony import */ var _tpta_city_class_tpta_city_class_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./tpta-city-class/tpta-city-class.component */ "./src/app/change/tpta-city-class/tpta-city-class.component.ts");
/* harmony import */ var _doe_gov_service_doe_gov_service_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./doe-gov-service/doe-gov-service.component */ "./src/app/change/doe-gov-service/doe-gov-service.component.ts");
/* harmony import */ var _dor_gov_service_dor_gov_service_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./dor-gov-service/dor-gov-service.component */ "./src/app/change/dor-gov-service/dor-gov-service.component.ts");
/* harmony import */ var _add_ta_pd_add_ta_pd_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./add-ta-pd/add-ta-pd.component */ "./src/app/change/add-ta-pd/add-ta-pd.component.ts");
/* harmony import */ var _state_gis_state_gis_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./state-gis/state-gis.component */ "./src/app/change/state-gis/state-gis.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var routes = [
    {
        path: '', component: _change_module__WEBPACK_IMPORTED_MODULE_5__["ChangeModule"], children: [
            { path: 'eos', component: _change_extensionofservice_extensionofservice_component__WEBPACK_IMPORTED_MODULE_2__["ExtensionofserviceComponent"] },
            { path: 'namegender', component: _namegender_namegender_component__WEBPACK_IMPORTED_MODULE_4__["NamegenderComponent"] },
            { path: 'dob', component: _dateofbirth_dateofbirth_component__WEBPACK_IMPORTED_MODULE_3__["DateofbirthComponent"] },
            { path: 'pfnps', component: _pf_nps_pf_nps_component__WEBPACK_IMPORTED_MODULE_6__["PfNpsComponent"] },
            { path: 'panno', component: _pan_no_pan_no_component__WEBPACK_IMPORTED_MODULE_7__["PanNoComponent"] },
            { path: 'hracc', component: _hra_city_class_hra_city_class_component__WEBPACK_IMPORTED_MODULE_8__["HraCityClassComponent"] },
            { path: 'designation', component: _designation_designation_component__WEBPACK_IMPORTED_MODULE_10__["DesignationComponent"] },
            { path: 'exserviceman', component: _exserviceman_exserviceman_component__WEBPACK_IMPORTED_MODULE_9__["ExservicemanComponent"] },
            { path: 'cgegis', component: _cgegis_details_cgegis_details_component__WEBPACK_IMPORTED_MODULE_11__["CgegisDetailsComponent"] },
            {
                path: 'joiningmode', component: _joinin_mode_joinin_mode_component__WEBPACK_IMPORTED_MODULE_13__["JoininModeComponent"]
            },
            { path: 'cghs', component: _cghs_details_cghs_details_component__WEBPACK_IMPORTED_MODULE_12__["CghsDetailsComponent"] },
            { path: 'castecategory', component: _castecategory_details_castecategory_details_component__WEBPACK_IMPORTED_MODULE_14__["CastecategoryDetailsComponent"] },
            { path: 'bankdetails', component: _bank_details_bank_details_component__WEBPACK_IMPORTED_MODULE_15__["BankDetailsComponent"] },
            { path: 'contactdetails', component: _contact_details_contact_details_component__WEBPACK_IMPORTED_MODULE_16__["ContactDetailsComponent"] },
            { path: 'entitledofficevehicle', component: _entitlement_office_entitlement_office_component__WEBPACK_IMPORTED_MODULE_17__["EntitlementOfficeComponent"] },
            { path: 'doj', component: _doj_controller_doj_controller_component__WEBPACK_IMPORTED_MODULE_18__["DojControllerComponent"] },
            { path: 'tptaclass', component: _tpta_city_class_tpta_city_class_component__WEBPACK_IMPORTED_MODULE_19__["TptaCityClassComponent"] },
            { path: 'doe', component: _doe_gov_service_doe_gov_service_component__WEBPACK_IMPORTED_MODULE_20__["DoeGovServiceComponent"] },
            { path: 'dor', component: _dor_gov_service_dor_gov_service_component__WEBPACK_IMPORTED_MODULE_21__["DorGovServiceComponent"] },
            { path: 'adddoubleta', component: _add_ta_pd_add_ta_pd_component__WEBPACK_IMPORTED_MODULE_22__["AddTaPDComponent"] },
            { path: 'stategis', component: _state_gis_state_gis_component__WEBPACK_IMPORTED_MODULE_23__["StateGisComponent"] }
        ]
    }
];
var ChangeRoutingModule = /** @class */ (function () {
    function ChangeRoutingModule() {
    }
    ChangeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChangeRoutingModule);
    return ChangeRoutingModule;
}());



/***/ }),

/***/ "./src/app/change/change.module.ts":
/*!*****************************************!*\
  !*** ./src/app/change/change.module.ts ***!
  \*****************************************/
/*! exports provided: ChangeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeModule", function() { return ChangeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _change_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./change-routing.module */ "./src/app/change/change-routing.module.ts");
/* harmony import */ var _change_extensionofservice_extensionofservice_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../change/extensionofservice/extensionofservice.component */ "./src/app/change/extensionofservice/extensionofservice.component.ts");
/* harmony import */ var _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../loanmgt/loanmgt.module */ "./src/app/loanmgt/loanmgt.module.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _commonddl_commonddl_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./commonddl/commonddl.component */ "./src/app/change/commonddl/commonddl.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _dateofbirth_dateofbirth_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dateofbirth/dateofbirth.component */ "./src/app/change/dateofbirth/dateofbirth.component.ts");
/* harmony import */ var _namegender_namegender_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./namegender/namegender.component */ "./src/app/change/namegender/namegender.component.ts");
/* harmony import */ var _pf_nps_pf_nps_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pf-nps/pf-nps.component */ "./src/app/change/pf-nps/pf-nps.component.ts");
/* harmony import */ var _pan_no_pan_no_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pan-no/pan-no.component */ "./src/app/change/pan-no/pan-no.component.ts");
/* harmony import */ var _hra_city_class_hra_city_class_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./hra-city-class/hra-city-class.component */ "./src/app/change/hra-city-class/hra-city-class.component.ts");
/* harmony import */ var _exserviceman_exserviceman_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./exserviceman/exserviceman.component */ "./src/app/change/exserviceman/exserviceman.component.ts");
/* harmony import */ var _designation_designation_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./designation/designation.component */ "./src/app/change/designation/designation.component.ts");
/* harmony import */ var _cgegis_details_cgegis_details_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./cgegis-details/cgegis-details.component */ "./src/app/change/cgegis-details/cgegis-details.component.ts");
/* harmony import */ var _joinin_mode_joinin_mode_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./joinin-mode/joinin-mode.component */ "./src/app/change/joinin-mode/joinin-mode.component.ts");
/* harmony import */ var _cghs_details_cghs_details_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./cghs-details/cghs-details.component */ "./src/app/change/cghs-details/cghs-details.component.ts");
/* harmony import */ var _castecategory_details_castecategory_details_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./castecategory-details/castecategory-details.component */ "./src/app/change/castecategory-details/castecategory-details.component.ts");
/* harmony import */ var _bank_details_bank_details_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./bank-details/bank-details.component */ "./src/app/change/bank-details/bank-details.component.ts");
/* harmony import */ var _contact_details_contact_details_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./contact-details/contact-details.component */ "./src/app/change/contact-details/contact-details.component.ts");
/* harmony import */ var _entitlement_office_entitlement_office_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./entitlement-office/entitlement-office.component */ "./src/app/change/entitlement-office/entitlement-office.component.ts");
/* harmony import */ var _doj_controller_doj_controller_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./doj-controller/doj-controller.component */ "./src/app/change/doj-controller/doj-controller.component.ts");
/* harmony import */ var _tpta_city_class_tpta_city_class_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./tpta-city-class/tpta-city-class.component */ "./src/app/change/tpta-city-class/tpta-city-class.component.ts");
/* harmony import */ var _doe_gov_service_doe_gov_service_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./doe-gov-service/doe-gov-service.component */ "./src/app/change/doe-gov-service/doe-gov-service.component.ts");
/* harmony import */ var _dor_gov_service_dor_gov_service_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./dor-gov-service/dor-gov-service.component */ "./src/app/change/dor-gov-service/dor-gov-service.component.ts");
/* harmony import */ var _add_ta_pd_add_ta_pd_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./add-ta-pd/add-ta-pd.component */ "./src/app/change/add-ta-pd/add-ta-pd.component.ts");
/* harmony import */ var _state_gis_state_gis_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./state-gis/state-gis.component */ "./src/app/change/state-gis/state-gis.component.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//import { DialogComponent } from '../dialog/dialog.component';

//import { SharecomponentModule } from '../sharecomponent/sharecomponent.module';
























var ChangeModule = /** @class */ (function () {
    function ChangeModule() {
    }
    ChangeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_change_extensionofservice_extensionofservice_component__WEBPACK_IMPORTED_MODULE_5__["ExtensionofserviceComponent"], _dateofbirth_dateofbirth_component__WEBPACK_IMPORTED_MODULE_10__["DateofbirthComponent"], _namegender_namegender_component__WEBPACK_IMPORTED_MODULE_11__["NamegenderComponent"], _commonddl_commonddl_component__WEBPACK_IMPORTED_MODULE_8__["CommonddlComponent"], _pf_nps_pf_nps_component__WEBPACK_IMPORTED_MODULE_12__["PfNpsComponent"], _pan_no_pan_no_component__WEBPACK_IMPORTED_MODULE_13__["PanNoComponent"], _hra_city_class_hra_city_class_component__WEBPACK_IMPORTED_MODULE_14__["HraCityClassComponent"], _exserviceman_exserviceman_component__WEBPACK_IMPORTED_MODULE_15__["ExservicemanComponent"], _designation_designation_component__WEBPACK_IMPORTED_MODULE_16__["DesignationComponent"], _cgegis_details_cgegis_details_component__WEBPACK_IMPORTED_MODULE_17__["CgegisDetailsComponent"], _joinin_mode_joinin_mode_component__WEBPACK_IMPORTED_MODULE_18__["JoininModeComponent"], _cghs_details_cghs_details_component__WEBPACK_IMPORTED_MODULE_19__["CghsDetailsComponent"], _castecategory_details_castecategory_details_component__WEBPACK_IMPORTED_MODULE_20__["CastecategoryDetailsComponent"], _bank_details_bank_details_component__WEBPACK_IMPORTED_MODULE_21__["BankDetailsComponent"], _contact_details_contact_details_component__WEBPACK_IMPORTED_MODULE_22__["ContactDetailsComponent"], _entitlement_office_entitlement_office_component__WEBPACK_IMPORTED_MODULE_23__["EntitlementOfficeComponent"], _doj_controller_doj_controller_component__WEBPACK_IMPORTED_MODULE_24__["DojControllerComponent"], _tpta_city_class_tpta_city_class_component__WEBPACK_IMPORTED_MODULE_25__["TptaCityClassComponent"], _doe_gov_service_doe_gov_service_component__WEBPACK_IMPORTED_MODULE_26__["DoeGovServiceComponent"], _dor_gov_service_dor_gov_service_component__WEBPACK_IMPORTED_MODULE_27__["DorGovServiceComponent"], _add_ta_pd_add_ta_pd_component__WEBPACK_IMPORTED_MODULE_28__["AddTaPDComponent"], _state_gis_state_gis_component__WEBPACK_IMPORTED_MODULE_29__["StateGisComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _change_routing_module__WEBPACK_IMPORTED_MODULE_4__["ChangeRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_2__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_6__["LoanmgtModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_9__["NgxMatSelectSearchModule"]
            ],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_30__["CommonMsg"]]
        })
    ], ChangeModule);
    return ChangeModule;
}());



/***/ }),

/***/ "./src/app/change/commonddl/commonddl.component.css":
/*!**********************************************************!*\
  !*** ./src/app/change/commonddl/commonddl.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  /*lebel dropdown */\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2NvbW1vbmRkbC9jb21tb25kZGwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7Q0FDeEI7O0VBRUM7SUFDRSxZQUFZO0dBQ2I7O0VBRUg7RUFDRSxrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztDQUNaOztFQUVEO0VBQ0Usb0ZBQW9GO0VBQ3BGLHVCQUF1QjtDQUN4Qjs7RUFFRCxlQUFlOztFQUVmO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UscUJBQXFCO0NBQ3RCOztFQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztFQUVEO0VBQ0Usb0dBQW9HO0NBQ3JHOztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7RUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFHRCxnQkFBZ0I7O0VBQ2hCO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7Q0FDRjs7RUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsV0FBVztHQUNaO0NBQ0Y7O0VBQ0QsYUFBYTs7RUFDYjtFQUNFLG9DQUFvQztDQUNyQzs7RUFFRDtFQUNFLFlBQVk7Q0FDYjs7RUFHRDtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsaUJBQWlCO0NBQ2xCOztFQUVEO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7O0VBQ0QsbUJBQW1COztFQUNuQjtFQUNFLFlBQVk7Q0FDYiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZS9jb21tb25kZGwvY29tbW9uZGRsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIG1hcmdpbjogNHB4XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vYXNzZXRzL2ltZy9leGFtcGxlcy9zaGliYTEuanBnJyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLyogMjktamFuLTE5ICovXHJcblxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXJnaW4tcmJsIHtcclxuICBtYXJnaW46IDAgNTBweCAxMHB4IDBcclxufVxyXG5cclxuLmZpbGQtb25lIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OCB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMXB4IC0xcHggcmdiYSgwLDAsMCwuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwwLDAsLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLDAsMCwuMTIpO1xyXG59XHJcblxyXG4ubWF0LWNhcmQge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U0ZTJlMjtcclxufVxyXG5cclxuLmV4YW1wbGUtY2FyZCB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDE4MXB4O1xyXG59XHJcblxyXG5cclxuLyogUmVzcG9uc2l2ZSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjhweCkgYW5kIChtYXgtd2lkdGggOiAxMDI0cHgpIHtcclxuICAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX29uZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMTAyNXB4KSBhbmQgKG1heC13aWR0aCA6IDEzOTdweCkge1xyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX3R3byB7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gIH1cclxufVxyXG4vKjMxL2phbi8xOSovXHJcbi5tLTIwIHtcclxuICBtYXJnaW46IDAgMjBweCAyMHB4IDIwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIG1pbi13aWR0aDogMzAwcHg7XHJcbn1cclxuXHJcbi5tYXQtdGFibGUge1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIG1heC1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxsLm1hdC1zb3J0LWhlYWRlci1zb3J0ZWQge1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm10LTEwIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi8qbGViZWwgZHJvcGRvd24gKi9cclxuLnNlbGVjdC1sYmwge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/change/commonddl/commonddl.component.html":
/*!***********************************************************!*\
  !*** ./src/app/change/commonddl/commonddl.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Pay Bill Group :</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"billCtrl\" placeholder=\"Pay Bill Group\" #singleSelect (selectionChange)=\"BindDropDownDesignation($event.value)\" [(value)]=\"selectedDesign\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"billFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"'Pay Bill Group is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredBill | async\" [value]=\"emp.billgrId\">\r\n              {{emp.billgrDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select [formControl]=\"designCtrl\" placeholder=\"Find Designation\" #singleSelect (selectionChange)=\"BindDropDownEmployee($event.value)\" [(value)]=\"selectedEmp\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.desigId\">\r\n              {{emp.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Employee:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"empCtrl\" placeholder=\"Find Employee\" #singleSelect (selectionChange)=\"SelectedValue($event.value)\">\r\n            <mat-option>\r\n\r\n              <ngx-mat-select-search [formControl]=\"empFilterCtrl\"\r\n                                     [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"'Employee is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.empCd\">\r\n              {{emp.empName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/change/commonddl/commonddl.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/change/commonddl/commonddl.component.ts ***!
  \*********************************************************/
/*! exports provided: CommonddlComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonddlComponent", function() { return CommonddlComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommonddlComponent = /** @class */ (function () {
    function CommonddlComponent(_Service) {
        this._Service = _Service;
        this.onchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.question = 0;
        this.selectedIndex = 0;
        /** searching . */
        this.billCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.empCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.billFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.filteredBill = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    CommonddlComponent.prototype.ngOnInit = function () {
        var _this = this;
        debugger;
        this.username = sessionStorage.getItem('username');
        this.FindEmpCode(this.username);
        this.billFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterBill();
        });
    };
    CommonddlComponent.prototype.FindEmpCode = function (username) {
        var _this = this;
        debugger;
        this._Service.GetEmpCode(username).subscribe(function (data) {
            debugger;
            _this.ArrEmpCode = data[0];
            if (_this.ArrEmpCode.empcode != null) {
                _this.permDdoId = _this.ArrEmpCode.ddoid;
                sessionStorage.setItem('PermDdoId', _this.ArrEmpCode.ddoid);
                _this.BindDropDownBillGroup(_this.permDdoId); //paybill
                _this.BackForwordSearchDesignation("0");
                _this.BackForwordSearchEmployee("0");
            }
        });
    };
    CommonddlComponent.prototype.BindDropDownBillGroup = function (value) {
        var _this = this;
        debugger;
        this._Service.GetAllPayBillGroup(value, 'Change').subscribe(function (result) {
            debugger;
            _this.ddlBillGroup = result;
            _this.Bill = result;
            _this.billCtrl.setValue(_this.Bill);
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredBill.next(_this.Bill);
        });
    };
    CommonddlComponent.prototype.BindDropDownDesignation = function (value) {
        var _this = this;
        debugger;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, 'Change').subscribe(function (data) {
            _this.ddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredDesign.next(_this.Design);
            _this.tempdesignvar = _this.designCtrl.value;
            if (_this.designCtrl.value.length > 1) {
                _this.tempdesignvar = '';
                _this.BindDropDownEmployee(_this.tempdesignvar);
            }
            _this.onchange.emit('');
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    CommonddlComponent.prototype.BindDropDownEmployee = function (value) {
        var _this = this;
        debugger;
        this.designId = value;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        this.tempvar = this.billCtrl.value;
        if (this.billCtrl.value.length > 2) {
            this.tempvar = '';
        }
        this._Service.GetAllEmpByBillgrIDDesigID(this.designId, this.tempvar, this.permDdoId, 'Change').subscribe(function (data) {
            debugger;
            _this.ddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
            _this.onchange.emit('');
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    CommonddlComponent.prototype.BackForwordSearchDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, 'Change').subscribe(function (data) {
            debugger;
            _this.ddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    CommonddlComponent.prototype.BackForwordSearchEmployee = function (value) {
        var _this = this;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        this._Service.GetAllEmpByBillgrIDDesigID(value, this.billCtrl.value, this.permDdoId, 'Change').subscribe(function (data) {
            debugger;
            _this.ddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    CommonddlComponent.prototype.selectionChange = function ($event) {
        console.log('stepper.selectedIndex: ' + this.selectedIndex
            + '; $event.selectedIndex: ' + $event.selectedIndex);
        if ($event.selectedIndex === 0) {
            return;
        }
        this.selectedIndex = $event.selectedIndex;
    };
    CommonddlComponent.prototype.filterBill = function () {
        debugger;
        if (!this.Bill) {
            return;
        }
        var search = this.billFilterCtrl.value;
        if (!search) {
            this.filteredBill.next(this.Bill.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredBill.next(this.Bill.filter(function (Bill) { return Bill.billgrDesc.toLowerCase().indexOf(search) > -1; }));
    };
    CommonddlComponent.prototype.filterDesign = function () {
        debugger;
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    CommonddlComponent.prototype.filterEmp = function () {
        debugger;
        if (!this.EMP) {
            return;
        }
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmp.next(this.EMP.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredEmp.next(this.EMP.filter(function (EMP) { return EMP.empName.toLowerCase().indexOf(search) > -1; }));
    };
    CommonddlComponent.prototype.SelectedValue = function (value) {
        debugger;
        if (value != "") {
            var splitted = value.split("-", 3);
            this.empid = splitted[0];
            this.selectedEmp = Number(splitted[1]);
            this.selectedDesign = splitted[2].trim();
            this.designId = splitted[2].trim();
            this.onchange.emit(this.empid);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CommonddlComponent.prototype, "onchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CommonddlComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], CommonddlComponent.prototype, "singleSelect", void 0);
    CommonddlComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-commonddl',
            template: __webpack_require__(/*! ./commonddl.component.html */ "./src/app/change/commonddl/commonddl.component.html"),
            styles: [__webpack_require__(/*! ./commonddl.component.css */ "./src/app/change/commonddl/commonddl.component.css")]
        }),
        __metadata("design:paramtypes", [_services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_3__["CommanService"]])
    ], CommonddlComponent);
    return CommonddlComponent;
}());



/***/ }),

/***/ "./src/app/change/contact-details/contact-details.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/change/contact-details/contact-details.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2NvbnRhY3QtZGV0YWlscy9jb250YWN0LWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvY29udGFjdC1kZXRhaWxzL2NvbnRhY3QtZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/contact-details/contact-details.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/change/contact-details/contact-details.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <!--<mat-radio-group aria-labelledby=\"example-radio-group-label\"\r\n                   class=\"example-radio-group\"\r\n                   [(ngModel)]=\"contactDetails\"  (change)=\"contactDetailsChange($event.value)\">\r\n    <mat-radio-button class=\"example-radio-button\" *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n      {{ot.text1}}\r\n    </mat-radio-button>\r\n  </mat-radio-group>-->\r\n  <form [formGroup]=\"contForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">\r\n        Sanction Order Details\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n\r\n          <mat-error *ngIf=\"contForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"contForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"contForm.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"fom-title\">Select </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-radio-group formControlName=\"contactDetails\" required>\r\n          <!--<mat-radio-button value=\"MobileNo\" (click)=\"empCd && getcontactDetailsChange('MobileNo');\"> Mobile No. </mat-radio-button>\r\n          <mat-radio-button value=\"Email\" (click)=\"empCd && getcontactDetailsChange('Email');\"> E-mail </mat-radio-button>\r\n          <mat-radio-button value=\"EmpEcode\" (click)=\"empCd && getcontactDetailsChange('EmpEcode');\"> Emp.E-Code</mat-radio-button>-->\r\n\r\n          <mat-radio-button value=\"MobileNo\" (change)=\"getcontactDetailsChange($event.value)\"> Mobile No. </mat-radio-button>\r\n          <mat-radio-button value=\"Email\" (change)=\"getcontactDetailsChange($event.value)\"> E-mail </mat-radio-button>\r\n          <mat-radio-button value=\"EmpEcode\" (change)=\"getcontactDetailsChange($event.value)\"> Emp.E-Code</mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n\r\n\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"MobileNodiv\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Mobile No.\" formControlName=\"MobileNo\" required />\r\n          <mat-error *ngIf=\"contForm?.controls.MobileNo?.errors?.required\">Mobile No. Required !</mat-error>\r\n          <mat-error *ngIf=\"(contForm?.controls.MobileNo?.errors?.minlength) || (contForm?.controls.MobileNo?.errors?.maxlength)\">Minimum & Maximum Length required is 10 </mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.MobileNo?.errors?.pattern\">It only accept (0-9) characters.</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"Emaildiv\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Email Part 1\" formControlName=\"EmailPart1\" required />\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart1?.errors?.required\">Email Part1 Required !</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart1?.errors?.minlength\">Minimum Length required is 5 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart1?.errors?.maxlength\">Maximum Length required is 80 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Email Part 2\" formControlName=\"EmailPart2\" required />\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart2?.errors?.required\">Email Part2 Required !</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart2?.errors?.minlength\">Minimum Length required is 5 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart2?.errors?.maxlength\">Maximum Length required is 80 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmailPart2?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"EmpEcodediv\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Emp Ecode\" formControlName=\"EmpEcode\" required />\r\n\r\n          <mat-error *ngIf=\"contForm?.controls.EmpEcode?.errors?.required\">Emp Ecode Required !</mat-error>\r\n          <mat-error *ngIf=\"(contForm?.controls.EmpEcode?.errors?.minlength) || (contForm?.controls.EmpEcode?.errors?.maxlength)\">Minimum & Maximum Length required is 10 characters.</mat-error>\r\n          <mat-error *ngIf=\"contForm?.controls.EmpEcode?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- MobileNo Table Right--->\r\n<div *ngIf=\"MobileNodiv\">\r\n  <div class=\"col-md-12 col-lg-6 mb-15\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">Current Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFiltercurrentMobileNo1($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstMobileNo1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n              {{MobileNo1currentPagination.pageIndex * MobileNo1currentPagination.pageSize + i + 1}}\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"currentMobileNo\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Mobile No.\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.currentMobileNo}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"Action\">\r\n            <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n                 class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n                edit\r\n              </a>\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n                delete_forever\r\n              </a>\r\n              <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n                 class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n              <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n\r\n            </mat-cell>\r\n\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsMobileNo1\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsMobileNo1;\"></mat-row>\r\n\r\n        </mat-table>\r\n\r\n        <mat-paginator #MobileNo1currentPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n        <div *ngIf=\"dstMobileNo1.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-12 col-lg-6\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">History Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFilterhistoryMobileNo2($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstMobileNo2\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">  {{MobileNo2historyPagination.pageIndex * MobileNo2historyPagination.pageSize + i + 1}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"revisedMobileNo\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Mobile No.\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.revisedMobileNo}} </mat-cell>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsMobileNo2\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsMobileNo2;\"></mat-row>\r\n        </mat-table>\r\n        <mat-paginator #MobileNo2historyPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n        <div *ngIf=\"dstMobileNo2.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--- Email Table Right--->\r\n<div *ngIf=\"Emaildiv\">\r\n  <div class=\"col-md-12 col-lg-6 mb-15\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">Current Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFiltercurrentEmail1($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstEmail1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n              {{Email1currentPagination.pageIndex * Email1currentPagination.pageSize + i + 1}}\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"currentEmail\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Email\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.currentEmail}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"Action\">\r\n            <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n                 class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n                edit\r\n              </a>\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n                delete_forever\r\n              </a>\r\n              <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n                 class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n              <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n\r\n            </mat-cell>\r\n\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsEmail1\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsEmail1;\"></mat-row>\r\n\r\n        </mat-table>\r\n\r\n        <mat-paginator #Email1currentPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n        <div *ngIf=\"dstEmail1.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-12 col-lg-6\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">History Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFilterhistoryEmail2($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstEmail2\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">  {{Email2historyPagination.pageIndex * Email2historyPagination.pageSize + i + 1}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"revisedEmail\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Email\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.revisedEmail}} </mat-cell>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsEmail2\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsEmail2;\"></mat-row>\r\n        </mat-table>\r\n        <mat-paginator #Email2historyPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n        <div *ngIf=\"dstEmail2.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--- EmpEcode Table Right--->\r\n<div *ngIf=\"EmpEcodediv\">\r\n  <div class=\"col-md-12 col-lg-6 mb-15\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">Current Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFiltercurrentEmpCode1($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstEmpEcode1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n              {{EmpCode1currentPagination.pageIndex * EmpCode1currentPagination.pageSize + i + 1}}\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"currentEmpEcode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Ecode\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.currentEmpEcode}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"Action\">\r\n            <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n                 class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n                edit\r\n              </a>\r\n              <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n                delete_forever\r\n              </a>\r\n              <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n                 class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n              <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n                info\r\n              </a>\r\n\r\n            </mat-cell>\r\n\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsEmpCode1\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsEmpCode1;\"></mat-row>\r\n\r\n        </mat-table>\r\n\r\n        <mat-paginator #EmpCode1currentPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n        <div *ngIf=\"dstEmpEcode1.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-12 col-lg-6\">\r\n    <div class=\"example-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">History Details</div>\r\n      <mat-card>\r\n\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFilterhistoryEmpCode2($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <mat-table #table [dataSource]=\"dstEmpEcode2\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"Sr.No\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element ; let i = index;\">  {{EmpCode2historyPagination.pageIndex * EmpCode2historyPagination.pageSize + i + 1}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmpCode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Emp. Code\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"EmployeeName\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Name\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"revisedEmpEcode\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              Employee Ecode\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.revisedEmpEcode}} </mat-cell>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n              <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n              <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n              <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n              <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n              <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n            </mat-cell>\r\n          </ng-container>\r\n\r\n          <mat-header-row *matHeaderRowDef=\"displayedColumnsEmpCode2\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: displayedColumnsEmpCode2;\"></mat-row>\r\n        </mat-table>\r\n        <mat-paginator #EmpCode2historyPagination [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n        <div *ngIf=\"dstEmpEcode2.data.length === 0\">No records found</div>\r\n\r\n      </mat-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--dialog-->\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm.controls.rejectionRemark1.errors && !RejectPopupForm.controls.rejectionRemark1.errors.minlength\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls.rejectionRemark1.errors && RejectPopupForm.controls.rejectionRemark1.errors.minlength\">Minimum Length required is 3</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls.rejectionRemark1.errors && RejectPopupForm.controls.rejectionRemark1.errors.maxLength\">Maximum Length required is 200</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls.rejectionRemark1.errors && RejectPopupForm.controls.rejectionRemark1.errors.pattern\">Only characters,numbers & special characters (\"/, -, _\") allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/contact-details/contact-details.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/change/contact-details/contact-details.component.ts ***!
  \*********************************************************************/
/*! exports provided: ContactDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactDetailsComponent", function() { return ContactDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ContactDetailsComponent = /** @class */ (function () {
    function ContactDetailsComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.displayedColumnsMobileNo1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentMobileNo', 'Status', 'Action'];
        this.displayedColumnsMobileNo2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedMobileNo', 'Status'];
        this.displayedColumnsEmail1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentEmail', 'Status', 'Action'];
        this.displayedColumnsEmail2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedEmail', 'Status'];
        this.displayedColumnsEmpCode1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentEmpEcode', 'Status', 'Action'];
        this.displayedColumnsEmpCode2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedEmpEcode', 'Status'];
        this.list = [];
        this.dstMobileNo1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dstMobileNo2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.dstEmail1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dstEmail2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.dstEmpEcode1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dstEmpEcode2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.MobileNodiv = false;
        this.Emaildiv = false;
        this.EmpEcodediv = false;
        this.EDITpara = '';
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
        console.log(this.username);
    }
    ContactDetailsComponent.prototype.ngOnInit = function () {
        debugger;
        this.Btntxt = 'Save';
        //this.Bindddltype();
        this.RejectPopupcreateForm();
        this.createForm();
        this.infoScreen = true;
    };
    //Bindddltype() { use when the value takes from proc Mobile No./ E-mail /Emp.Code(Org.Code)
    //  debugger;
    //  this._service.GetDdlvalue('contactDetails').subscribe(res => {
    //    debugger;
    //    this.list = res;
    //  })
    //}
    //
    ContactDetailsComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.contForm.enable();
            this.contForm.patchValue({
                contactDetails: "MobileNo"
            });
            this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'MobileNo');
            this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'MobileNo');
        }
        else {
            this.contForm.disable();
            //this.contForm.controls.contactDetails.enable();
        }
        this.getcontactDetailsChange("MobileNo");
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.contForm.disable();
            this.contForm.controls.contactDetails.enable();
        }
        this.MobileNodiv = true;
        this.Emaildiv = false;
        this.EmpEcodediv = false;
    };
    ContactDetailsComponent.prototype.getcontactDetailsChange = function (value) {
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.editable = false;
        if (value == 'MobileNo') { //MobileNo
            this.MobileNodiv = true;
            this.Emaildiv = false;
            this.EmpEcodediv = false;
            //
            this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'MobileNo');
            this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'MobileNo');
            //
            this.contForm.controls["MobileNo"].setValidators([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[0-9]*$')
            ]);
            //
            this.contForm.controls['OrderNo'].setValue('');
            this.contForm.controls['OrderDate'].setValue('');
            this.contForm.controls['Remarks'].setValue('');
            this.contForm.controls['rejectionRemark'].setValue('');
            this.contForm.controls['oldOrderNo'].setValue('');
            this.contForm.controls['MobileNo'].setValue('');
            //
            this.contForm.controls['EmailPart1'].value == "";
            this.contForm.controls['EmailPart1'].disable();
            this.contForm.controls['EmailPart1'].clearValidators();
            this.contForm.controls['EmailPart2'].value == "";
            this.contForm.controls['EmailPart2'].disable();
            this.contForm.controls['EmailPart2'].clearValidators();
            this.contForm.controls['EmpEcode'].value == "";
            this.contForm.controls['EmpEcode'].disable();
            this.contForm.controls['EmpEcode'].clearValidators();
        }
        else if (value == 'Email') { //Email
            this.MobileNodiv = false;
            this.Emaildiv = true;
            this.EmpEcodediv = false;
            //
            this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'Email');
            this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'Email');
            //
            this.contForm.controls["EmailPart1"].setValidators([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(80),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-zA-Z0-9_.+-]+$")
            ]);
            this.contForm.controls["EmailPart2"].setValidators([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(80),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
            ]);
            //
            this.contForm.controls['OrderNo'].setValue('');
            this.contForm.controls['OrderDate'].setValue('');
            this.contForm.controls['Remarks'].setValue('');
            this.contForm.controls['rejectionRemark'].setValue('');
            this.contForm.controls['oldOrderNo'].setValue('');
            this.contForm.controls['EmailPart1'].setValue('');
            this.contForm.controls['EmailPart2'].setValue('');
            //
            this.contForm.controls['MobileNo'].value == "";
            this.contForm.controls['MobileNo'].disable();
            this.contForm.controls['MobileNo'].clearValidators();
            this.contForm.controls['EmpEcode'].value == "";
            this.contForm.controls['EmpEcode'].disable();
            this.contForm.controls['EmpEcode'].clearValidators();
        }
        else if (value == 'EmpEcode') { //EmpEcode
            this.MobileNodiv = false;
            this.Emaildiv = false;
            this.EmpEcodediv = true;
            //
            this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'EmpEcode');
            this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'EmpEcode');
            //
            this.contForm.controls["EmpEcode"].setValidators([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[0-9]*$')
            ]);
            //
            this.contForm.controls['OrderNo'].setValue('');
            this.contForm.controls['OrderDate'].setValue('');
            this.contForm.controls['Remarks'].setValue('');
            this.contForm.controls['rejectionRemark'].setValue('');
            this.contForm.controls['oldOrderNo'].setValue('');
            this.contForm.controls['EmpEcode'].setValue('');
            //
            this.contForm.controls['EmailPart1'].value == "";
            this.contForm.controls['EmailPart1'].disable();
            this.contForm.controls['EmailPart1'].clearValidators();
            this.contForm.controls['EmailPart2'].value == "";
            this.contForm.controls['EmailPart2'].disable();
            this.contForm.controls['EmailPart2'].clearValidators();
            this.contForm.controls['MobileNo'].value == "";
            this.contForm.controls['MobileNo'].disable();
            this.contForm.controls['MobileNo'].clearValidators();
        }
    };
    ContactDetailsComponent.prototype.createForm = function () {
        this.contForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            contactDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            MobileNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            EmailPart1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            EmailPart2: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            EmpEcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.contForm.disable();
    };
    ContactDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9/-_ ]*$')])
        });
    };
    ContactDetailsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        debugger;
        //console.log(contactDetails);
        this.contForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        if (!this.contForm.valid) {
            return false;
        }
        var contactDetails = this.contForm.controls.contactDetails.value;
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        //if (this.contForm.valid) {
        debugger;
        this._service.InsertMobileNoEmailEmpCodeDetails(this.contForm.value, flag, contactDetails).subscribe(function (result) {
            debugger;
            if (contactDetails == 'MobileNo') { //mobile_no
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', 'MobileNo');
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', 'MobileNo');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.contForm.disable();
            }
            else if (contactDetails == 'Email') { //e-mail
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', 'Email');
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', 'Email');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.contForm.disable();
            }
            else if (contactDetails == 'EmpEcode') { //org_code
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', 'EmpEcode');
                _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', 'EmpEcode');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.contForm.disable();
            }
        });
        //}
    };
    ContactDetailsComponent.prototype.getcontFormDetails1 = function (empCd, roleId, flag, SelectionOptFlag) {
        var _this = this;
        debugger;
        // let contactDetails = this.contForm.get('contactDetails').value;
        if (SelectionOptFlag == 'MobileNo') {
            this.pageNumber = 1;
            this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'MobileNo').subscribe(function (data) {
                debugger;
                var self = _this;
                if (flag == 'current') {
                    debugger;
                    _this.dstMobileNo1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstMobileNo1.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.currentMobileNo.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstMobileNo1.data.length;
                    _this.dstMobileNo1.paginator = _this.MobileNo1currentPagination;
                    //enable or disbale form using current status of orderNo user
                    var list_1 = [];
                    data.forEach(function (element) {
                        list_1.push(element.status);
                    });
                    if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                        debugger;
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.contForm.patchValue({
                            contactDetails: "MobileNo"
                        });
                        _this.infoScreen = true;
                    }
                    else {
                        self.contForm.enable();
                        _this.infoScreen = false;
                    }
                    if (_this.roleId == '5') {
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.infoScreen = false;
                    }
                }
                else if (flag == 'history') {
                    //debugger;
                    _this.dstMobileNo2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstMobileNo2.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.revisedMobileNo.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstMobileNo2.data.length;
                    _this.dstMobileNo2.paginator = _this.MobileNo2historyPagination;
                }
            });
        }
        else if (SelectionOptFlag == 'Email') {
            this.pageNumber = 1;
            this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'Email').subscribe(function (data) {
                debugger;
                var self = _this;
                if (flag == 'current') {
                    debugger;
                    _this.dstEmail1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstEmail1.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.currentEmail.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstEmail1.data.length;
                    _this.dstEmail1.paginator = _this.Email1currentPagination;
                    //enable or disbale form using current status of orderNo user
                    var list_2 = [];
                    data.forEach(function (element) {
                        list_2.push(element.status);
                    });
                    if (list_2[0] == 'E' || list_2[0] == 'U' || list_2[0] == 'P' || list_2[0] == 'R') {
                        debugger;
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.contForm.patchValue({
                            contactDetails: "Email"
                        });
                        _this.infoScreen = true;
                    }
                    else {
                        self.contForm.enable();
                        _this.infoScreen = false;
                    }
                    if (_this.roleId == '5') {
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.infoScreen = false;
                    }
                }
                else if (flag == 'history') {
                    //debugger;
                    _this.dstEmail2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstEmail2.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.revisedEmail.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstEmail2.data.length;
                    _this.dstEmail2.paginator = _this.Email2historyPagination;
                }
            });
        }
        else if (SelectionOptFlag == 'EmpEcode') {
            this.pageNumber = 1;
            this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'EmpEcode').subscribe(function (data) {
                debugger;
                var self = _this;
                if (flag == 'current') {
                    debugger;
                    _this.dstEmpEcode1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstEmpEcode1.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.currentEmpEcode.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstEmpEcode1.data.length;
                    _this.dstEmpEcode1.paginator = _this.EmpCode1currentPagination;
                    //enable or disbale form using current status of orderNo user
                    var list_3 = [];
                    data.forEach(function (element) {
                        list_3.push(element.status);
                    });
                    if (list_3[0] == 'E' || list_3[0] == 'U' || list_3[0] == 'P' || list_3[0] == 'R') {
                        debugger;
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.contForm.patchValue({
                            contactDetails: "EmpEcode"
                        });
                        _this.infoScreen = true;
                    }
                    else {
                        self.contForm.enable();
                        _this.infoScreen = false;
                    }
                    if (_this.roleId == '5') {
                        self.contForm.disable();
                        _this.contForm.controls.contactDetails.enable();
                        _this.infoScreen = false;
                    }
                }
                else if (flag == 'history') {
                    //debugger;
                    _this.dstEmpEcode2 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                    //filter in Data source        
                    _this.dstEmpEcode2.filterPredicate = function (data, filter) {
                        return data.empCd.toLowerCase().includes(filter) ||
                            data.empName.toLowerCase().includes(filter) ||
                            data.revisedEmpEcode.toLowerCase().includes(filter);
                    };
                    //END Of filter in Data source
                    _this.pageSize = _this.dstEmpEcode2.data.length;
                    _this.dstEmpEcode2.paginator = _this.EmpCode2historyPagination;
                }
            });
        }
    };
    ContactDetailsComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.contForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            contactDetails: obj.contactDetails,
            MobileNo: obj.mobileNo,
            EmailPart1: obj.emailPart1,
            EmailPart2: obj.emailPart2,
            EmpEcode: obj.empEcode,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        console.log(this.oldOrderNo);
        debugger;
        this.contForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var contactDetails = obj.contactDetails;
        if (contactDetails == 'MobileNo') {
            var empStatus = obj.status;
            if (empStatus == 'R') {
                this.EDITpara = 'EDIT';
                this.editable = true;
            }
            else if (empStatus == 'E' || empStatus == 'U') {
                this.EDITpara = 'EDIT';
                this.editable = false;
            }
            else {
                this.EDITpara = '';
                this.editable = false;
            }
            if (this.roleId == '6' && empStatus == 'U') {
                this.fwdtochker = true;
            }
            else {
                this.fwdtochker = false;
            }
        }
        else if (contactDetails == 'Email') {
            var empStatus = obj.status;
            if (empStatus == 'R') {
                this.EDITpara = 'EDIT';
                this.editable = true;
            }
            else if (empStatus == 'E' || empStatus == 'U') {
                this.EDITpara = 'EDIT';
                this.editable = false;
            }
            else {
                this.EDITpara = '';
                this.editable = false;
            }
            if (this.roleId == '6' && empStatus == 'U') {
                this.fwdtochker = true;
            }
            else {
                this.fwdtochker = false;
            }
        }
        else if (contactDetails == 'EmpEcode') {
            var empStatus = obj.status;
            if (empStatus == 'R') {
                this.EDITpara = 'EDIT';
                this.editable = true;
            }
            else if (empStatus == 'E' || empStatus == 'U') {
                this.EDITpara = 'EDIT';
                this.editable = false;
            }
            else {
                this.EDITpara = '';
                this.editable = false;
            }
            if (this.roleId == '6' && empStatus == 'U') {
                this.fwdtochker = true;
            }
            else {
                this.fwdtochker = false;
            }
        }
        //let empStatus = obj.status;
        //if (empStatus == 'R') {
        //  this.EDITpara = 'EDIT';
        //  this.editable = true;
        //}
        //else if (empStatus == 'E' || empStatus == 'U') {
        //  this.EDITpara = 'EDIT';
        //  this.editable = false;
        //}
        //else {
        //  this.EDITpara = '';
        //  this.editable = false;
        //}
        //if (this.roleId == '6' && empStatus == 'U') {
        //  this.fwdtochker = true;
        //}
        //else {
        //  this.fwdtochker = false;
        //}
    };
    //email
    ContactDetailsComponent.prototype.applyFiltercurrentEmail1 = function (value) {
        //debugger;
        this.Email1currentPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstEmail1.filter = value;
        if (this.dstEmail1.filteredData.length > 0) {
            this.dstEmail1.filter = value.trim().toLowerCase();
        }
        else {
            this.dstEmail1.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.applyFilterhistoryEmail2 = function (value) {
        //debugger;
        this.Email2historyPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstEmail2.filter = value;
        if (this.dstEmail2.filteredData.length > 0) {
            this.dstEmail2.filter = value.trim().toLowerCase();
        }
        else {
            this.dstEmail2.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.applyFiltercurrentMobileNo1 = function (value) {
        //debugger;
        this.MobileNo1currentPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstMobileNo1.filter = value;
        if (this.dstMobileNo1.filteredData.length > 0) {
            this.dstMobileNo1.filter = value.trim().toLowerCase();
        }
        else {
            this.dstMobileNo1.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.applyFilterhistoryMobileNo2 = function (value) {
        //debugger;
        this.MobileNo2historyPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstMobileNo2.filter = value;
        if (this.dstMobileNo2.filteredData.length > 0) {
            this.dstMobileNo2.filter = value.trim().toLowerCase();
        }
        else {
            this.dstMobileNo2.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.applyFiltercurrentEmpCode1 = function (value) {
        this.EmpCode1currentPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstEmpEcode1.filter = value;
        if (this.dstEmpEcode1.filteredData.length > 0) {
            this.dstEmpEcode1.filter = value.trim().toLowerCase();
        }
        else {
            this.dstEmpEcode1.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.applyFilterhistoryEmpCode2 = function (value) {
        //debugger;
        this.EmpCode2historyPagination.pageIndex = 0;
        value = value.trim(); // Remove whitespace
        value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dstEmpEcode2.filter = value;
        if (this.dstEmpEcode2.filteredData.length > 0) {
            this.dstEmpEcode2.filter = value.trim().toLowerCase();
        }
        else {
            this.dstEmpEcode2.filteredData.length = 0;
        }
    };
    ContactDetailsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    ContactDetailsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        var contactDetails = this.objToBeDeleted.contactDetails;
        this.contForm.patchValue({
            contactDetails: contactDetails
        });
    };
    ContactDetailsComponent.prototype.confirmDelete = function () {
        //debugger;
        var _this = this;
        var contactDetails = this.objToBeDeleted.contactDetails;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, contactDetails).subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.contForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.contForm.patchValue({
                contactDetails: contactDetails
            });
            _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', contactDetails);
            _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', contactDetails);
            _this.DeletePopup = false;
            _this.editable = false;
        });
    };
    ContactDetailsComponent.prototype.Cancel = function () {
        //debugger;
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    ContactDetailsComponent.prototype.cancelForm = function () {
        //debugger;
        this.formGroupDirective.resetForm();
        this.contForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    ContactDetailsComponent.prototype.btnInfoClick = function (obj) {
        //debugger;
        this.oldOrderNo = '';
        this.contForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            contactDetails: obj.contactDetails,
            MobileNo: obj.mobileNo,
            EmailPart1: obj.emailPart1,
            EmailPart2: obj.emailPart2,
            EmpEcode: obj.empEcode,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //debugger;
        var contactDetails = obj.contactDetails;
        if (contactDetails == 'MobileNo') {
            var empStatus = obj.status;
            this.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            if (this.roleId == '5' && empStatus == 'N') {
                this.contForm.enable();
            }
            else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
                this.infoScreen = false;
            }
            else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
                this.infoScreen = true;
                this.btnCancel = false;
                this.Btntxt = 'Save';
            }
            else {
                this.infoScreen = true;
                this.fwdtochker = false;
                this.Btntxt = 'Save';
            }
        }
        else if (contactDetails == 'Email') {
            var empStatus = obj.status;
            this.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            if (this.roleId == '5' && empStatus == 'N') {
                this.contForm.enable();
            }
            else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
                this.infoScreen = false;
            }
            else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
                this.infoScreen = true;
                this.btnCancel = false;
                this.Btntxt = 'Save';
            }
            else {
                this.infoScreen = true;
                this.fwdtochker = false;
                this.Btntxt = 'Save';
            }
        }
        else if (contactDetails == 'EmpEcode') {
            var empStatus = obj.status;
            this.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            if (this.roleId == '5' && empStatus == 'N') {
                this.contForm.enable();
            }
            else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
                this.infoScreen = false;
            }
            else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
                this.infoScreen = true;
                this.btnCancel = false;
                this.Btntxt = 'Save';
            }
            else {
                this.infoScreen = true;
                this.fwdtochker = false;
                this.Btntxt = 'Save';
            }
        }
    };
    //fwdtochecker
    ContactDetailsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.contForm.valid) {
            return false;
        }
        //debugger;
        var employeeId = this.empCd.trim();
        var orderNo = this.contForm.get('OrderNo').value;
        var contactDetails = this.contForm.controls.contactDetails.value;
        this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, null, null, contactDetails).subscribe(function (result) {
            //debugger;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.contForm.patchValue({
                contactDetails: contactDetails
            });
            _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', contactDetails);
            _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', contactDetails);
        });
        this.Btntxt = 'Save';
    };
    ContactDetailsComponent.prototype.updateStatus = function (status) {
        //debugger;    
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.contForm.get('OrderNo').value;
        var contactDetails = this.contForm.controls.contactDetails.value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, status, rejectionRemark1, contactDetails).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', contactDetails);
                        _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', contactDetails);
                        _this.formGroupDirective.reset();
                        _this.contForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, status, rejectionRemark1, contactDetails).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getcontFormDetails1(_this.empCd, _this.roleId, 'history', contactDetails);
                    _this.getcontFormDetails1(_this.empCd, _this.roleId, 'current', contactDetails);
                    _this.formGroupDirective.reset();
                    _this.contForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.contForm.patchValue({
                    contactDetails: contactDetails
                });
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ContactDetailsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('MobileNo1currentPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "MobileNo1currentPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('MobileNo2historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "MobileNo2historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('Email1currentPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "Email1currentPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('Email2historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "Email2historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('EmpCode1currentPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "EmpCode1currentPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('EmpCode2historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ContactDetailsComponent.prototype, "EmpCode2historyPagination", void 0);
    ContactDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-details',
            template: __webpack_require__(/*! ./contact-details.component.html */ "./src/app/change/contact-details/contact-details.component.html"),
            styles: [__webpack_require__(/*! ./contact-details.component.css */ "./src/app/change/contact-details/contact-details.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], ContactDetailsComponent);
    return ContactDetailsComponent;
}());



/***/ }),

/***/ "./src/app/change/dateofbirth/dateofbirth.component.css":
/*!**************************************************************!*\
  !*** ./src/app/change/dateofbirth/dateofbirth.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2RhdGVvZmJpcnRoL2RhdGVvZmJpcnRoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvY2hhbmdlL2RhdGVvZmJpcnRoL2RhdGVvZmJpcnRoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/change/dateofbirth/dateofbirth.component.html":
/*!***************************************************************!*\
  !*** ./src/app/change/dateofbirth/dateofbirth.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"DOBForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"DOBForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"DOBForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOBForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"DOBForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"DOB.open()\" [matDatepicker]=\"DOB\" [max]=\"maxDate\" formControlName=\"DOB\" placeholder=\"Revised DOB\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOB\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOB></mat-datepicker>\r\n          <mat-error>\r\n            Revised Date of Birth Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentAnnuationDate\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Old Super Annuation Date\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentAnnuationDate|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedAnnuationDate\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Old Super Annuation Date\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedAnnuationDate|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/dateofbirth/dateofbirth.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/change/dateofbirth/dateofbirth.component.ts ***!
  \*************************************************************/
/*! exports provided: DateofbirthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateofbirthComponent", function() { return DateofbirthComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_dateofbirth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/dateofbirth.service */ "./src/app/services/Change/dateofbirth.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DateofbirthComponent = /** @class */ (function () {
    function DateofbirthComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentAnnuationDate', 'currentDOB', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedAnnuationDate', 'revisedDOB', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    DateofbirthComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    DateofbirthComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.DOBForm.enable();
            //bind form
            this.getDOBDetails1(this.empCd, this.roleId, 'history');
            this.getDOBDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.DOBForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.DOBForm.disable();
        }
    };
    DateofbirthComponent.prototype.createForm = function () {
        this.DOBForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            DOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.DOBForm.disable();
    };
    DateofbirthComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    DateofbirthComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.DOBForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.DOBForm.valid) {
            this._service.InsertDOBDetails(this.DOBForm.value, flag).subscribe(function (result) {
                _this.getDOBDetails1(_this.empCd, _this.roleId, 'history');
                _this.getDOBDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.DOBForm.disable();
            });
        }
    };
    DateofbirthComponent.prototype.getDOBDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetDOBDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]('en');
                _this.dataSource.filterPredicate = function (data, filter) {
                    var currentAnnuationDate = _this.pipe.transform(data.currentAnnuationDate, 'dd/MM/yyyy');
                    var currentDOB = _this.pipe.transform(data.currentDOB, 'dd/MM/yyyy');
                    return currentAnnuationDate.indexOf(filter) >= 0 || currentDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.DOBForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.DOBForm.enable();
                }
                if (_this.roleId == '5') {
                    self.DOBForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]('en');
                _this.dataSource1.filterPredicate = function (data, filter) {
                    var revisedAnnuationDate = _this.pipe.transform(data.revisedAnnuationDate, 'dd/MM/yyyy');
                    var revisedDOB = _this.pipe.transform(data.revisedDOB, 'dd/MM/yyyy');
                    return revisedAnnuationDate.indexOf(filter) >= 0 || revisedDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    DateofbirthComponent.prototype.btnEditClick = function (obj) {
        this.DOBForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOB: obj.dob,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.DOBForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    DateofbirthComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    DateofbirthComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    DateofbirthComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    DateofbirthComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    DateofbirthComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'dob').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.DOBForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getDOBDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOBDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    DateofbirthComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    DateofbirthComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.DOBForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    DateofbirthComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.DOBForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOB: obj.dob,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.DOBForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.DOBForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    DateofbirthComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.DOBForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.DOBForm.get('OrderNo').value;
        this._service.ForwardToCheckerDOBUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getDOBDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOBDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    DateofbirthComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.DOBForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerDOBUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getDOBDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getDOBDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.DOBForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerDOBUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getDOBDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getDOBDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.DOBForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DateofbirthComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DateofbirthComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DateofbirthComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DateofbirthComponent.prototype, "sort", void 0);
    DateofbirthComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dateofbirth',
            template: __webpack_require__(/*! ./dateofbirth.component.html */ "./src/app/change/dateofbirth/dateofbirth.component.html"),
            styles: [__webpack_require__(/*! ./dateofbirth.component.css */ "./src/app/change/dateofbirth/dateofbirth.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_dateofbirth_service__WEBPACK_IMPORTED_MODULE_3__["DateofbirthService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], DateofbirthComponent);
    return DateofbirthComponent;
}());



/***/ }),

/***/ "./src/app/change/designation/designation.component.css":
/*!**************************************************************!*\
  !*** ./src/app/change/designation/designation.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2Rlc2lnbmF0aW9uL2Rlc2lnbmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvY2hhbmdlL2Rlc2lnbmF0aW9uL2Rlc2lnbmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/change/designation/designation.component.html":
/*!***************************************************************!*\
  !*** ./src/app/change/designation/designation.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"designationForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"designationForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"designationForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"designationForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"designationForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Designation\" formControlName=\"designation\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            Designation required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"WithEffectDate.open()\" [matDatepicker]=\"WithEffectDate\" [max]=\"maxDate\" formControlName=\"WithEffectDate\" placeholder=\"With Effect Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"WithEffectDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #WithEffectDate></mat-datepicker>\r\n          <mat-error>\r\n            With Effect Date Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentdesignation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Designation\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentdesignation}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\"> {{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"reviseddesignation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Designation\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.reviseddesignation}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/designation/designation.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/change/designation/designation.component.ts ***!
  \*************************************************************/
/*! exports provided: DesignationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DesignationComponent", function() { return DesignationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DesignationComponent = /** @class */ (function () {
    function DesignationComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentdesignation', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'reviseddesignation', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    DesignationComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        // this.empCd = 'A**013F91001';
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    DesignationComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('designation').subscribe(function (res) {
            _this.list = res;
        });
    };
    DesignationComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.designationForm.enable();
            //bind form
            this.getdesignationFormDetails1(this.empCd, this.roleId, 'history');
            this.getdesignationFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.designationForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.designationForm.disable();
        }
    };
    DesignationComponent.prototype.createForm = function () {
        this.designationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            designation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            WithEffectDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.designationForm.disable();
    };
    DesignationComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    DesignationComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.designationForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.designationForm.valid) {
            this._service.InsertdesignationFormDetails(this.designationForm.value, flag).subscribe(function (result) {
                _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.designationForm.disable();
            });
        }
    };
    DesignationComponent.prototype.getdesignationFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetdesignationFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentdesignation.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.designationForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.designationForm.enable();
                }
                if (_this.roleId == '5') {
                    self.designationForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.reviseddesignation.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    DesignationComponent.prototype.compare = function (o1, o2) {
        //
        if (o1 == o2)
            return true;
        else
            return false;
    };
    DesignationComponent.prototype.btnEditClick = function (obj) {
        this.designationForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            designation: obj.designation,
            WithEffectDate: obj.withEffectDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        //
        this.designationForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    DesignationComponent.prototype.applyFiltercurrent = function (filterValue) {
        //
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    DesignationComponent.prototype.applyFilterhistory = function (filterValue) {
        //
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    DesignationComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    DesignationComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    DesignationComponent.prototype.confirmDelete = function () {
        var _this = this;
        //
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'designation').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.designationForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    DesignationComponent.prototype.Cancel = function () {
        //
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    DesignationComponent.prototype.cancelForm = function () {
        //
        this.formGroupDirective.resetForm();
        this.designationForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    DesignationComponent.prototype.btnInfoClick = function (obj) {
        //
        this.oldOrderNo = '';
        this.designationForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            designation: obj.designation,
            WithEffectDate: obj.withEffectDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //
        var empStatus = obj.status;
        this.designationForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.designationForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    DesignationComponent.prototype.forwardToChecker = function () {
        var _this = this;
        ////debugger;
        if (!this.designationForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.designationForm.get('OrderNo').value;
        this._service.ForwardToCheckerdesignationUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            //
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    DesignationComponent.prototype.updateStatus = function (status) {
        //
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.designationForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerdesignationUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.designationForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerdesignationUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getdesignationFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.designationForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DesignationComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DesignationComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DesignationComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DesignationComponent.prototype, "sort", void 0);
    DesignationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-designation',
            template: __webpack_require__(/*! ./designation.component.html */ "./src/app/change/designation/designation.component.html"),
            styles: [__webpack_require__(/*! ./designation.component.css */ "./src/app/change/designation/designation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], DesignationComponent);
    return DesignationComponent;
}());



/***/ }),

/***/ "./src/app/change/doe-gov-service/doe-gov-service.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/change/doe-gov-service/doe-gov-service.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2RvZS1nb3Ytc2VydmljZS9kb2UtZ292LXNlcnZpY2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvZG9lLWdvdi1zZXJ2aWNlL2RvZS1nb3Ytc2VydmljZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/doe-gov-service/doe-gov-service.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/change/doe-gov-service/doe-gov-service.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"DOEForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"DOEForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"DOEForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOEForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"DOEForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"DOE.open()\" [matDatepicker]=\"DOE\" [max]=\"maxDate\" formControlName=\"DOE\" placeholder=\"Date of entry in Govt. Service\"\r\n                 autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOE\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOE></mat-datepicker>\r\n          <mat-error>\r\n            Date of entry in Govt. Service Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOE\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of entry in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOE|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOE\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of entry in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOE|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/doe-gov-service/doe-gov-service.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/change/doe-gov-service/doe-gov-service.component.ts ***!
  \*********************************************************************/
/*! exports provided: DoeGovServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoeGovServiceComponent", function() { return DoeGovServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DoeGovServiceComponent = /** @class */ (function () {
    function DoeGovServiceComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentDOB', 'currentDOE', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedDOB', 'revisedDOE', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    DoeGovServiceComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    DoeGovServiceComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.DOEForm.enable();
            //bind form
            this.getDOEDetails1(this.empCd, this.roleId, 'history');
            this.getDOEDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.DOEForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.DOEForm.disable();
        }
    };
    DoeGovServiceComponent.prototype.createForm = function () {
        this.DOEForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            DOE: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.DOEForm.disable();
    };
    DoeGovServiceComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    DoeGovServiceComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.DOEForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.DOEForm.valid) {
            this._service.InsertDOEDetails(this.DOEForm.value, flag).subscribe(function (result) {
                _this.getDOEDetails1(_this.empCd, _this.roleId, 'history');
                _this.getDOEDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.DOEForm.disable();
            });
        }
    };
    DoeGovServiceComponent.prototype.getDOEDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetDOEDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]('en');
                _this.dataSource.filterPredicate = function (data, filter) {
                    var currentDOE = _this.pipe.transform(data.currentDOE, 'dd/MM/yyyy');
                    var currentDOB = _this.pipe.transform(data.currentDOB, 'dd/MM/yyyy');
                    return currentDOE.indexOf(filter) >= 0 || currentDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.DOEForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.DOEForm.enable();
                }
                if (_this.roleId == '5') {
                    self.DOEForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]('en');
                _this.dataSource1.filterPredicate = function (data, filter) {
                    var revisedDOE = _this.pipe.transform(data.revisedDOE, 'dd/MM/yyyy');
                    var revisedDOB = _this.pipe.transform(data.revisedDOB, 'dd/MM/yyyy');
                    return revisedDOE.indexOf(filter) >= 0 || revisedDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    DoeGovServiceComponent.prototype.btnEditClick = function (obj) {
        this.DOEForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOE: obj.doe,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.DOEForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    DoeGovServiceComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    DoeGovServiceComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    DoeGovServiceComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    DoeGovServiceComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    DoeGovServiceComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'doe').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.DOEForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getDOEDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOEDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    DoeGovServiceComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    DoeGovServiceComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.DOEForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    DoeGovServiceComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.DOEForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOE: obj.doe,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.DOEForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.DOEForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    DoeGovServiceComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.DOEForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.DOEForm.get('OrderNo').value;
        this._service.ForwardToCheckerDOEUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getDOEDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOEDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    DoeGovServiceComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.DOEForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerDOEUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getDOEDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getDOEDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.DOEForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerDOEUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getDOEDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getDOEDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.DOEForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DoeGovServiceComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DoeGovServiceComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DoeGovServiceComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DoeGovServiceComponent.prototype, "sort", void 0);
    DoeGovServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-doe-gov-service',
            template: __webpack_require__(/*! ./doe-gov-service.component.html */ "./src/app/change/doe-gov-service/doe-gov-service.component.html"),
            styles: [__webpack_require__(/*! ./doe-gov-service.component.css */ "./src/app/change/doe-gov-service/doe-gov-service.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], DoeGovServiceComponent);
    return DoeGovServiceComponent;
}());



/***/ }),

/***/ "./src/app/change/doj-controller/doj-controller.component.css":
/*!********************************************************************!*\
  !*** ./src/app/change/doj-controller/doj-controller.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2Rvai1jb250cm9sbGVyL2Rvai1jb250cm9sbGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvY2hhbmdlL2Rvai1jb250cm9sbGVyL2Rvai1jb250cm9sbGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/change/doj-controller/doj-controller.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/change/doj-controller/doj-controller.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"DOJForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"DOJForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"DOJForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"DOJForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"DOJForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"DOJ.open()\" [matDatepicker]=\"DOJ\" [max]=\"maxDate\" formControlName=\"DOJ\" placeholder=\"Date of Joining Controller\"\r\n                 autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOJ></mat-datepicker>\r\n          <mat-error>\r\n            Revised Date of Joining Controller Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOJ\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Joining Controller\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOJ|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOG\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of entry in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOG|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOJ\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Joining Controller\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOJ|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOG\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of entry in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOG|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/doj-controller/doj-controller.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/change/doj-controller/doj-controller.component.ts ***!
  \*******************************************************************/
/*! exports provided: DojControllerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DojControllerComponent", function() { return DojControllerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DojControllerComponent = /** @class */ (function () {
    function DojControllerComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentDOJ', 'currentDOG', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedDOJ', 'revisedDOG', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    DojControllerComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    DojControllerComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.DOJForm.enable();
            //bind form
            this.getDOJDetails1(this.empCd, this.roleId, 'history');
            this.getDOJDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.DOJForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.DOJForm.disable();
        }
    };
    DojControllerComponent.prototype.createForm = function () {
        this.DOJForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            DOJ: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.DOJForm.disable();
    };
    DojControllerComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    DojControllerComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.DOJForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.DOJForm.valid) {
            this._service.InsertDOJDetails(this.DOJForm.value, flag).subscribe(function (result) {
                _this.getDOJDetails1(_this.empCd, _this.roleId, 'history');
                _this.getDOJDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.DOJForm.disable();
            });
        }
    };
    DojControllerComponent.prototype.getDOJDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetDOJDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]('en');
                _this.dataSource.filterPredicate = function (data, filter) {
                    var currentDOJ = _this.pipe.transform(data.currentDOJ, 'dd/MM/yyyy');
                    var currentDOG = _this.pipe.transform(data.currentDOG, 'dd/MM/yyyy');
                    return currentDOJ.indexOf(filter) >= 0 || currentDOG.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.DOJForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.DOJForm.enable();
                }
                if (_this.roleId == '5') {
                    self.DOJForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]('en');
                _this.dataSource1.filterPredicate = function (data, filter) {
                    var revisedDOJ = _this.pipe.transform(data.revisedDOJ, 'dd/MM/yyyy');
                    var revisedDOG = _this.pipe.transform(data.revisedDOG, 'dd/MM/yyyy');
                    return revisedDOJ.indexOf(filter) >= 0 || revisedDOG.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    DojControllerComponent.prototype.btnEditClick = function (obj) {
        this.DOJForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOJ: obj.doj,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.DOJForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    DojControllerComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    DojControllerComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    DojControllerComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    DojControllerComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    DojControllerComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'doj').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.DOJForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.deleteFailedMsg);
            }
            _this.getDOJDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOJDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    DojControllerComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    DojControllerComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.DOJForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    DojControllerComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.DOJForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOJ: obj.doj,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.DOJForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.DOJForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    DojControllerComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.DOJForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.DOJForm.get('OrderNo').value;
        this._service.ForwardToCheckerDOJUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getDOJDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDOJDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    DojControllerComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.DOJForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerDOJUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.DDORejectedByChecker);
                        _this.getDOJDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getDOJDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.DOJForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerDOJUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getDOJDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getDOJDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.DOJForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DojControllerComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DojControllerComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DojControllerComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DojControllerComponent.prototype, "sort", void 0);
    DojControllerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-doj-controller',
            template: __webpack_require__(/*! ./doj-controller.component.html */ "./src/app/change/doj-controller/doj-controller.component.html"),
            styles: [__webpack_require__(/*! ./doj-controller.component.css */ "./src/app/change/doj-controller/doj-controller.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]])
    ], DojControllerComponent);
    return DojControllerComponent;
}());



/***/ }),

/***/ "./src/app/change/dor-gov-service/dor-gov-service.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/change/dor-gov-service/dor-gov-service.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2Rvci1nb3Ytc2VydmljZS9kb3ItZ292LXNlcnZpY2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvZG9yLWdvdi1zZXJ2aWNlL2Rvci1nb3Ytc2VydmljZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/dor-gov-service/dor-gov-service.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/change/dor-gov-service/dor-gov-service.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"DORForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n\r\n          <mat-error *ngIf=\"DORForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"DORForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"DORForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"DORForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"DOR.open()\" [matDatepicker]=\"DOR\" [max]=\"maxDate\" formControlName=\"DOR\" placeholder=\"Date of Regularisation in Govt. Service\"\r\n                 autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOR\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOR></mat-datepicker>\r\n          <mat-error>\r\n            Date of Regularisation  in Govt. Service Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentDOR\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Regularisation in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDOR|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOB\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Birth\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOB|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedDOR\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Date of Regularisation in Govt. Service\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDOR|date:'dd/MM/yyyy'}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/dor-gov-service/dor-gov-service.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/change/dor-gov-service/dor-gov-service.component.ts ***!
  \*********************************************************************/
/*! exports provided: DorGovServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DorGovServiceComponent", function() { return DorGovServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DorGovServiceComponent = /** @class */ (function () {
    function DorGovServiceComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentDOB', 'currentDOR', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedDOB', 'revisedDOR', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    DorGovServiceComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    DorGovServiceComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.DORForm.enable();
            //bind form
            this.getDORDetails1(this.empCd, this.roleId, 'history');
            this.getDORDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.DORForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.DORForm.disable();
        }
    };
    DorGovServiceComponent.prototype.createForm = function () {
        this.DORForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            DOR: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.DORForm.disable();
    };
    DorGovServiceComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    DorGovServiceComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.DORForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.DORForm.valid) {
            this._service.InsertDORDetails(this.DORForm.value, flag).subscribe(function (result) {
                _this.getDORDetails1(_this.empCd, _this.roleId, 'history');
                _this.getDORDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.DORForm.disable();
            });
        }
    };
    DorGovServiceComponent.prototype.getDORDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetDORDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]('en');
                _this.dataSource.filterPredicate = function (data, filter) {
                    var currentDOR = _this.pipe.transform(data.currentDOR, 'dd/MM/yyyy');
                    var currentDOB = _this.pipe.transform(data.currentDOB, 'dd/MM/yyyy');
                    return currentDOR.indexOf(filter) >= 0 || currentDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.DORForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.DORForm.enable();
                }
                if (_this.roleId == '5') {
                    self.DORForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]('en');
                _this.dataSource1.filterPredicate = function (data, filter) {
                    var revisedDOR = _this.pipe.transform(data.revisedDOR, 'dd/MM/yyyy');
                    var revisedDOB = _this.pipe.transform(data.revisedDOB, 'dd/MM/yyyy');
                    return revisedDOR.indexOf(filter) >= 0 || revisedDOB.indexOf(filter) >= 0
                        || data.empName.indexOf(filter) >= 0 || data.empCd.indexOf(filter) >= 0;
                };
                //filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    DorGovServiceComponent.prototype.btnEditClick = function (obj) {
        this.DORForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOR: obj.dor,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        //
        this.DORForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    DorGovServiceComponent.prototype.applyFiltercurrent = function (filterValue) {
        //
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    DorGovServiceComponent.prototype.applyFilterhistory = function (filterValue) {
        //
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    DorGovServiceComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    DorGovServiceComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    DorGovServiceComponent.prototype.confirmDelete = function () {
        var _this = this;
        //
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'dor').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.DORForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.deleteFailedMsg);
            }
            _this.getDORDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDORDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    DorGovServiceComponent.prototype.Cancel = function () {
        //
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    DorGovServiceComponent.prototype.cancelForm = function () {
        //
        this.formGroupDirective.resetForm();
        this.DORForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    DorGovServiceComponent.prototype.btnInfoClick = function (obj) {
        //
        this.oldOrderNo = '';
        this.DORForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            DOR: obj.dor,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //
        var empStatus = obj.status;
        this.DORForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.DORForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    DorGovServiceComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.DORForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.DORForm.get('OrderNo').value;
        this._service.ForwardToCheckerDORUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getDORDetails1(_this.empCd, _this.roleId, 'history');
            _this.getDORDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    DorGovServiceComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.DORForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerDORUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.DDORejectedByChecker);
                        _this.getDORDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getDORDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.DORForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerDORUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getDORDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getDORDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.DORForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DorGovServiceComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DorGovServiceComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DorGovServiceComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DorGovServiceComponent.prototype, "sort", void 0);
    DorGovServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dor-gov-service',
            template: __webpack_require__(/*! ./dor-gov-service.component.html */ "./src/app/change/dor-gov-service/dor-gov-service.component.html"),
            styles: [__webpack_require__(/*! ./dor-gov-service.component.css */ "./src/app/change/dor-gov-service/dor-gov-service.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]])
    ], DorGovServiceComponent);
    return DorGovServiceComponent;
}());



/***/ }),

/***/ "./src/app/change/entitlement-office/entitlement-office.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/change/entitlement-office/entitlement-office.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2VudGl0bGVtZW50LW9mZmljZS9lbnRpdGxlbWVudC1vZmZpY2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvZW50aXRsZW1lbnQtb2ZmaWNlL2VudGl0bGVtZW50LW9mZmljZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/entitlement-office/entitlement-office.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/change/entitlement-office/entitlement-office.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n  <form [formGroup]=\"EntofficevehicleForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"EntofficevehicleForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"EntofficevehicleForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <label>Select Entitled for Office Vehicle :</label><br />\r\n\r\n        <mat-radio-group formControlName=\"entofficeVehicle\">\r\n          <mat-radio-button value=\"1\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"0\">No</mat-radio-button>\r\n        </mat-radio-group>\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && EntofficevehicleForm?.controls.entofficeVehicle?.errors?.required\">\r\n          Select Either Required Value\r\n        </mat-error>\r\n      </div>\r\n\r\n  \r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currententVehicle\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Entitled for Office Vehicle\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currententVehicle}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n       \r\n        <ng-container matColumnDef=\"revisedentVehicle\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Entitled for Office Vehicle\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedentVehicle}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/entitlement-office/entitlement-office.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/change/entitlement-office/entitlement-office.component.ts ***!
  \***************************************************************************/
/*! exports provided: EntitlementOfficeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntitlementOfficeComponent", function() { return EntitlementOfficeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EntitlementOfficeComponent = /** @class */ (function () {
    function EntitlementOfficeComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currententVehicle', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedentVehicle', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    EntitlementOfficeComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    EntitlementOfficeComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.EntofficevehicleForm.enable();
            //bind form
            this.getEntofficevehicleFormDetails1(this.empCd, this.roleId, 'history');
            this.getEntofficevehicleFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.EntofficevehicleForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.EntofficevehicleForm.disable();
        }
    };
    EntitlementOfficeComponent.prototype.createForm = function () {
        this.EntofficevehicleForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            entofficeVehicle: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.EntofficevehicleForm.disable();
    };
    EntitlementOfficeComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    EntitlementOfficeComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        this.EntofficevehicleForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.EntofficevehicleForm.valid) {
            this._service.InsertEntofficevehicleFormDetails(this.EntofficevehicleForm.value, flag).subscribe(function (result) {
                _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.EntofficevehicleForm.disable();
            });
        }
    };
    EntitlementOfficeComponent.prototype.getEntofficevehicleFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetEntofficevehicleFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currententVehicle.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.EntofficevehicleForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.EntofficevehicleForm.enable();
                }
                if (_this.roleId == '5') {
                    self.EntofficevehicleForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedentVehicle.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    EntitlementOfficeComponent.prototype.btnEditClick = function (obj) {
        this.EntofficevehicleForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            entofficeVehicle: obj.entofficeVehicle,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.EntofficevehicleForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    EntitlementOfficeComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    EntitlementOfficeComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    EntitlementOfficeComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    EntitlementOfficeComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    EntitlementOfficeComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'entofficevehicle').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.EntofficevehicleForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    EntitlementOfficeComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    EntitlementOfficeComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.EntofficevehicleForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    EntitlementOfficeComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.EntofficevehicleForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            entofficeVehicle: obj.entofficeVehicle,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.EntofficevehicleForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.EntofficevehicleForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    EntitlementOfficeComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.EntofficevehicleForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.EntofficevehicleForm.get('OrderNo').value;
        this._service.ForwardToCheckerEntofficevehicleFormUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    EntitlementOfficeComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.EntofficevehicleForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerEntofficevehicleFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.EntofficevehicleForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerEntofficevehicleFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getEntofficevehicleFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.EntofficevehicleForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], EntitlementOfficeComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], EntitlementOfficeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], EntitlementOfficeComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], EntitlementOfficeComponent.prototype, "sort", void 0);
    EntitlementOfficeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-entitlement-office',
            template: __webpack_require__(/*! ./entitlement-office.component.html */ "./src/app/change/entitlement-office/entitlement-office.component.html"),
            styles: [__webpack_require__(/*! ./entitlement-office.component.css */ "./src/app/change/entitlement-office/entitlement-office.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], EntitlementOfficeComponent);
    return EntitlementOfficeComponent;
}());



/***/ }),

/***/ "./src/app/change/exserviceman/exserviceman.component.css":
/*!****************************************************************!*\
  !*** ./src/app/change/exserviceman/exserviceman.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2V4c2VydmljZW1hbi9leHNlcnZpY2VtYW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvZXhzZXJ2aWNlbWFuL2V4c2VydmljZW1hbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/exserviceman/exserviceman.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/change/exserviceman/exserviceman.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"ExSerForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n         \r\n          <mat-error *ngIf=\"ExSerForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n\r\n          <mat-error *ngIf=\"ExSerForm?.controls.Remarks?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"ExSerForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"ExSerForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <label>Ex-servicemen</label><br />\r\n\r\n        <mat-radio-group formControlName=\"exservicemen\">\r\n          <mat-radio-button value=\"1040\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"1\">No</mat-radio-button>\r\n        </mat-radio-group>\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && ExSerForm?.controls.exservicemen?.errors?.required\">\r\n          Ex-servicemen Required !\r\n        </mat-error>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\"> {{paginator.pageIndex * paginator.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentexservicemen\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Ex-servicemen\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentexservicemen}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>    \r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n   \r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedexservicemen\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Ex-servicemen\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedexservicemen}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/exserviceman/exserviceman.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/change/exserviceman/exserviceman.component.ts ***!
  \***************************************************************/
/*! exports provided: ExservicemanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExservicemanComponent", function() { return ExservicemanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ExservicemanComponent = /** @class */ (function () {
    function ExservicemanComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentexservicemen', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedexservicemen', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    ExservicemanComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    ExservicemanComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.ExSerForm.enable();
            //bind form
            this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
            this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.ExSerForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.ExSerForm.disable();
        }
    };
    ExservicemanComponent.prototype.createForm = function () {
        this.ExSerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            exservicemen: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.ExSerForm.disable();
    };
    ExservicemanComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    ExservicemanComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        this.ExSerForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.ExSerForm.valid) {
            this._service.InsertexservicemenFormDetails(this.ExSerForm.value, flag).subscribe(function (result) {
                _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.ExSerForm.disable();
            });
        }
    };
    ExservicemanComponent.prototype.getexservicemenFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetexservicemenFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentexservicemen.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.ExSerForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.ExSerForm.enable();
                }
                if (_this.roleId == '5') {
                    self.ExSerForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedexservicemen.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    ExservicemanComponent.prototype.btnEditClick = function (obj) {
        this.ExSerForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            exservicemen: obj.exservicemen,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.ExSerForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    ExservicemanComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    ExservicemanComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    ExservicemanComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    ExservicemanComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    ExservicemanComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'exservicemen').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.ExSerForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    ExservicemanComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    ExservicemanComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.ExSerForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    ExservicemanComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.ExSerForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            exservicemen: obj.exservicemen,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.ExSerForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.ExSerForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    ExservicemanComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.ExSerForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.ExSerForm.get('OrderNo').value;
        this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    ExservicemanComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.ExSerForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.ExSerForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getexservicemenFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.ExSerForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ExservicemanComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ExservicemanComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ExservicemanComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], ExservicemanComponent.prototype, "sort", void 0);
    ExservicemanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-exserviceman',
            template: __webpack_require__(/*! ./exserviceman.component.html */ "./src/app/change/exserviceman/exserviceman.component.html"),
            styles: [__webpack_require__(/*! ./exserviceman.component.css */ "./src/app/change/exserviceman/exserviceman.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], ExservicemanComponent);
    return ExservicemanComponent;
}());



/***/ }),

/***/ "./src/app/change/extensionofservice/extensionofservice.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/change/extensionofservice/extensionofservice.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".select-drop-head[_ngcontent-c10] {\r\n  margin: 0 30px;\r\n}\r\n\r\n\r\n.dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2V4dGVuc2lvbm9mc2VydmljZS9leHRlbnNpb25vZnNlcnZpY2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7Q0FDaEI7OztBQUdEO0VBQ0UseUJBQXlCO0VBQ3pCLDhCQUE4QjtDQUMvQiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZS9leHRlbnNpb25vZnNlcnZpY2UvZXh0ZW5zaW9ub2ZzZXJ2aWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VsZWN0LWRyb3AtaGVhZFtfbmdjb250ZW50LWMxMF0ge1xyXG4gIG1hcmdpbjogMCAzMHB4O1xyXG59XHJcblxyXG5cclxuLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/extensionofservice/extensionofservice.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/change/extensionofservice/extensionofservice.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"col-md-12 col-lg-12\" style=\"text-align:center\">\r\n  <label>Select Extension Of Service Employee  </label>\r\n  <mat-form-field style=\"width:500px;text-align:center\">\r\n    <mat-select placeholder=\"Select\" (selectionChange)=\"GetcommonMethod($event.value)\" required>\r\n      <mat-option [value]=\"\">Select</mat-option>\r\n      <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n        {{ot.text1}}\r\n      </mat-option>\r\n    </mat-select>\r\n    <mat-error>\r\n      Extension Of Service Employee Required !\r\n    </mat-error>\r\n  </mat-form-field>\r\n\r\n</div>\r\n\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n\r\n  <form [formGroup]=\"extensionOfServiceForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Extension Of Service Order</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No.\" autocomplete=off [readonly]=\"editable\" required>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n         \r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\"\r\n                 [min]=\"extensionOfServiceForm.controls.JoiningDate.value\"\r\n                 placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"FromDate.open()\" [matDatepicker]=\"FromDate\" [max]=\"maxDate\" formControlName=\"FromDate\"\r\n                 [min]=\"extensionOfServiceForm.controls.JoiningDate.value\"\r\n                 (dateChange)=\"changeEvent($event)\"\r\n                 placeholder=\"From Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"FromDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #FromDate></mat-datepicker>\r\n          <mat-error>From Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"ToDate.open()\" [matDatepicker]=\"ToDate\" [min]=\"extensionOfServiceForm.controls.FromDate.value\" formControlName=\"ToDate\" placeholder=\"To Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"ToDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #ToDate></mat-datepicker>\r\n          <mat-error>To Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"JoiningDate\"\r\n                 formControlName=\"JoiningDate\" placeholder=\"Date of Joining\" [readonly]=\"JDreadonly\" disabled>\r\n\r\n          <!--<input matInput (click)=\"JoiningDate.open()\" [matDatepicker]=\"JoiningDate\"\r\n                 formControlName=\"JoiningDate\" placeholder=\"Date of Joining\" [readonly]=\"JDreadonly\" required disabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"JoiningDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #JoiningDate></mat-datepicker>-->\r\n          <mat-error>Date of Joining Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"extensionOfServiceForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"extensionOfServiceForm.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" [readonly]=\"editable\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n<div class=\"col-md-6 col-lg-6\">\r\n  <mat-card>\r\n    <div class=\"fom-title\"> Extension Of Service Order Details</div>\r\n\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" (keypress)=\"allowAlphaNumericSpace($event)\" [disabled]=\"!empCd\"  placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element ; let i = index;\">{{paginator.pageIndex * paginator.pageSize + i + 1}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"EmpCode\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Emp. Code\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"EmployeeName\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Employee Name\r\n\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Order No.\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Order Date\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.orderDate|date:'dd/MM/yyyy'}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"FromDate\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          From Date\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.fromDate|date:'dd/MM/yyyy'}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"ToDate\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          To Date\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.toDate|date:'dd/MM/yyyy'}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">\r\n          <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n          <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n          <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n          <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n          <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n          <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n        </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Action\">\r\n        <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n             class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n            edit\r\n          </a>\r\n          <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n            delete_forever\r\n          </a>\r\n          <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n             class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"(element.status == 'P' || element.status == 'V' || element.status == 'R' ) &&  roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n            info\r\n          </a>\r\n\r\n        </mat-cell>\r\n\r\n      </ng-container>\r\n\r\n      <!--<ng-container class=\"hiddenField\">\r\n        <mat-cell *matCellDef=\"let element\" class=\"hiddenField\"> {{element.remarks}} </mat-cell>\r\n\r\n        <mat-cell *matCellDef=\"let element\" class=\"hiddenField\"> {{element.joiningDate}} </mat-cell>\r\n      </ng-container>-->\r\n\r\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n    <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n    <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n  </mat-card>\r\n</div>\r\n\r\n<!--popup-->\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Order No. Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n       \r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/extensionofservice/extensionofservice.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/change/extensionofservice/extensionofservice.component.ts ***!
  \***************************************************************************/
/*! exports provided: ExtensionofserviceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionofserviceComponent", function() { return ExtensionofserviceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_Change_extensionofservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/Change/extensionofservice.service */ "./src/app/services/Change/extensionofservice.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ExtensionofserviceComponent = /** @class */ (function () {
    function ExtensionofserviceComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.JDreadonly = true;
        this.displayedColumns = ['S.No', 'EmpCode', 'EmployeeName', 'OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Status', 'Action'];
        this.list = [];
        this.allData = [];
        this.maxDate = new Date();
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"]();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    ExtensionofserviceComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.RejectPopupcreateForm();
        this.createForm();
        this.Bindddltype();
        this.infoScreen = true;
        this.JDreadonly = true;
    };
    ExtensionofserviceComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('extension').subscribe(function (res) {
            _this.list = res;
        });
    };
    ExtensionofserviceComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.extensionOfServiceForm.enable();
            //bind form
            this.getExtensionOfServiceDetails(this.empCd, this.roleId);
        }
        else {
            this.extensionOfServiceForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        //this.extensionOfServiceForm.patchValue({
        //  JoiningDate: this.joiningDate
        //});
        this.dataSource.filteredData.length = 0; //clr search text in different select emp
        if (this.roleId == '5') {
            this.extensionOfServiceForm.disable();
        }
    };
    ExtensionofserviceComponent.prototype.changeEvent = function (event) {
        this.extensionOfServiceForm.controls.ToDate.setValue(''); //clr todte value when selecting fromdt
        //this.extensionOfServiceForm.controls.JoiningDate.setValue(event.value);
    };
    ExtensionofserviceComponent.prototype.createForm = function () {
        this.extensionOfServiceForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            //  OrderNo: new FormControl('', [Validators.required, Validators.minLength(3), this.maxLength (100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            FromDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            ToDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            JoiningDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', { updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required] }),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.extensionOfServiceForm.disable();
    };
    ExtensionofserviceComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    ExtensionofserviceComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.extensionOfServiceForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.extensionOfServiceForm.valid) {
            this._service.InsertEOSDetails(this.extensionOfServiceForm.value, flag).subscribe(function (result) {
                //this.pageNumber = 1;
                _this.getExtensionOfServiceDetails(_this.empCd, _this.roleId);
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        //this.extensionOfServiceForm.reset();
                        //bind form
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.extensionOfServiceForm.disable();
            });
        }
    };
    //datatable data
    ExtensionofserviceComponent.prototype.getExtensionOfServiceDetails = function (empCd, roleId) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetExtensionOfServiceDetails(empCd, roleId).subscribe(function (data) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
            //filter in Data source
            _this.pipe = new _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]('en');
            var defaultPredicate = _this.dataSource.filterPredicate;
            _this.dataSource.filterPredicate = function (data, filter) {
                //const orderDate = this.pipe.transform(data.orderDate, 'dd/MM/yyyy');
                //const fromDate = this.pipe.transform(data.fromDate, 'dd/MM/yyyy');
                //// return orderDate.indexOf(filter) >= 0 || fromDate.indexOf(filter) >= 0 || defaultPredicate(data, filter);
                //if ((fromDate != null && fromDate.indexOf(filter) > -1) || (fromDate != null && fromDate.indexOf(filter) > -1)) {
                //  return orderDate.indexOf(filter) >= 0 || fromDate.indexOf(filter) >= 0 || data.empName.indexOf(filter) >= 0 || defaultPredicate(data, filter);
                //}
                //else {
                if (data.orderNo != null && data.orderNo.indexOf(filter) > -1) {
                    return data.orderNo.toLowerCase().includes(filter);
                }
                else {
                    return data.empCd.toLowerCase().includes(filter) || data.empName.toLowerCase().includes(filter);
                }
                //}
            };
            //END Of filter in Data source
            _this.pageSize = _this.dataSource.data.length;
            _this.dataSource.paginator = _this.paginator;
            var self = _this;
            var list = [];
            var list1 = [];
            //enable or disbale form using current status of orderNo user
            data.forEach(function (element) {
                list.push(element.joiningDate);
            });
            _this.extensionOfServiceForm.controls.JoiningDate.setValue(list[0]);
            //get joiningDtflag
            _this.joiningDate = list[0];
            //get status
            data.forEach(function (element) {
                list1.push(element.status);
            });
            if (list1[0] == 'E' || list1[0] == 'U' || list1[0] == 'P' || list1[0] == 'R') {
                self.extensionOfServiceForm.disable();
                _this.infoScreen = true;
            }
            else {
                self.extensionOfServiceForm.enable();
            }
            if (_this.roleId == '5') {
                self.extensionOfServiceForm.disable();
                _this.infoScreen = false;
            }
        });
    };
    //EDIT
    ExtensionofserviceComponent.prototype.btnEditClick = function (obj) {
        this.extensionOfServiceForm.setValue({
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            FromDate: obj.fromDate,
            ToDate: obj.toDate,
            JoiningDate: obj.joiningDate,
            Remarks: obj.remarks,
            empCd: this.empCd,
            rejectionRemark: obj.rejectionRemark,
            ddoId: this.ddoId,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.extensionOfServiceForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    //getEmpTransDetails(pageSize, pageNumber) {
    //  // this.getExtensionOfServiceDetails();
    //  let employeeId = this.empCd;
    //  this.totalCount = 0;
    //  this.dataSource = new MatTableDataSource<any>();
    //  this._service.FetchEOSDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(result => {
    //    
    //    if (result && result != undefined && result.length > 0) {
    //      this.dataSource = new MatTableDataSource(result);
    //      this.dataSource.paginator = this.paginator;
    //      this.dataSource.sort = this.sort;
    //      //this.allData = result;
    //      this.totalCount = result.length;
    //    }
    //  });
    //}
    ExtensionofserviceComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
            // this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    ExtensionofserviceComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    ExtensionofserviceComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    ExtensionofserviceComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'extension').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.extensionOfServiceForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getExtensionOfServiceDetails(_this.empCd, _this.roleId);
            _this.DeletePopup = false;
        });
    };
    ExtensionofserviceComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    ExtensionofserviceComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.extensionOfServiceForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    ExtensionofserviceComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.extensionOfServiceForm.setValue({
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            FromDate: obj.fromDate,
            ToDate: obj.toDate,
            JoiningDate: obj.joiningDate,
            Remarks: obj.remarks,
            empCd: this.empCd,
            rejectionRemark: obj.rejectionRemark,
            ddoId: this.ddoId,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.extensionOfServiceForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.extensionOfServiceForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    ExtensionofserviceComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.extensionOfServiceForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.extensionOfServiceForm.get('OrderNo').value;
        //
        this._service.ForwardToCheckerUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getExtensionOfServiceDetails(_this.empCd, _this.roleId);
        });
        this.Btntxt = 'Save';
    };
    ExtensionofserviceComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.extensionOfServiceForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getExtensionOfServiceDetails(_this.empCd, _this.roleId);
                        _this.formGroupDirective.reset();
                        _this.extensionOfServiceForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getExtensionOfServiceDetails(_this.empCd, _this.roleId);
                    _this.formGroupDirective.reset();
                    _this.extensionOfServiceForm.disable();
                    _this.infoScreen = false;
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ExtensionofserviceComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ExtensionofserviceComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ExtensionofserviceComponent.prototype, "sort", void 0);
    ExtensionofserviceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-extensionofservice',
            template: __webpack_require__(/*! ./extensionofservice.component.html */ "./src/app/change/extensionofservice/extensionofservice.component.html"),
            styles: [__webpack_require__(/*! ./extensionofservice.component.css */ "./src/app/change/extensionofservice/extensionofservice.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_extensionofservice_service__WEBPACK_IMPORTED_MODULE_2__["ExtensionofService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], ExtensionofserviceComponent);
    return ExtensionofserviceComponent;
}());



/***/ }),

/***/ "./src/app/change/hra-city-class/hra-city-class.component.css":
/*!********************************************************************!*\
  !*** ./src/app/change/hra-city-class/hra-city-class.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n  \r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2hyYS1jaXR5LWNsYXNzL2hyYS1jaXR5LWNsYXNzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCOztDQUUvQiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZS9ocmEtY2l0eS1jbGFzcy9ocmEtY2l0eS1jbGFzcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbiAgXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/hra-city-class/hra-city-class.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/change/hra-city-class/hra-city-class.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"HraForm\" (ngSubmit)=\"onSubmit()\" >\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"HraForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"HraForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"HraForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"HraForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n          \r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"City Class\" formControlName=\"HRAcc\" [compareWith]=\"compare\" required>\r\n            <!--<mat-option [value]=\"\">Select</mat-option>-->\r\n\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            City Class required\r\n          </mat-error>\r\n        </mat-form-field>\r\n        \r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"WithEffectDate.open()\" [matDatepicker]=\"WithEffectDate\" [max]=\"maxDate\" formControlName=\"WithEffectDate\" placeholder=\"With Effect Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"WithEffectDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #WithEffectDate></mat-datepicker>\r\n          <mat-error>\r\n            With Effect Date Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"!empCd\">Cancel</button>\r\n    </div>-->\r\n\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">        \r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n   \r\n    <mat-card>\r\n      <div class=\"fom-title\">Current Details</div>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"orderNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            orderNo\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentCityClass\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            City Class\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">{{element.currentHRAcc}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n \r\n    <mat-card>\r\n      <div class=\"fom-title\">History Details</div>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{historyPagination.pageIndex * historyPagination.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"orderNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            orderNo\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedCityClass\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            City Class\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">{{element.revisedHRAcc}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n \r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n    \r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/hra-city-class/hra-city-class.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/change/hra-city-class/hra-city-class.component.ts ***!
  \*******************************************************************/
/*! exports provided: HraCityClassComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HraCityClassComponent", function() { return HraCityClassComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HraCityClassComponent = /** @class */ (function () {
    function HraCityClassComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentCityClass', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedCityClass', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    HraCityClassComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        // this.empCd = 'A**013F91001';
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    HraCityClassComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('hra').subscribe(function (res) {
            _this.list = res;
        });
    };
    HraCityClassComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.HraForm.enable();
            //bind form
            this.getHraFormDetails1(this.empCd, this.roleId, 'history');
            this.getHraFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.HraForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.HraForm.disable();
        }
    };
    HraCityClassComponent.prototype.createForm = function () {
        this.HraForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            HRAcc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            WithEffectDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.HraForm.disable();
        //if (this.roleId == '5') {
        //  this.HraForm.disable();
        //}
        //this.HraForm.disable();
    };
    HraCityClassComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    HraCityClassComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.HraForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.HraForm.valid) { //change in proc of save & remove snackbar 
            this._service.InsertHraccFormDetails(this.HraForm.value, flag).subscribe(function (result) {
                //this.pageNumber = 1;
                _this.getHraFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getHraFormDetails1(_this.empCd, _this.roleId, 'current');
                //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
                //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.HraForm.disable();
            });
        }
    };
    HraCityClassComponent.prototype.getHraFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        //this.Bindddltype(); //set ddl
        this.pageNumber = 1;
        this._service.GetHraccFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentHRAcc.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.HraForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.HraForm.enable();
                }
                if (_this.roleId == '5') {
                    self.HraForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedHRAcc.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    HraCityClassComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    HraCityClassComponent.prototype.btnEditClick = function (obj) {
        this.HraForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            HRAcc: obj.hrAcc,
            WithEffectDate: obj.withEffectDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.HraForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        //if (empStatus == 'P' || empStatus == 'V' || empStatus == 'R') {
        //  this.editable = false;
        //}
        //else
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    //getEmpTransDetails(pageSize, pageNumber, flag1: any) {
    //  // this.getExtensionOfServiceDetails();
    //  let employeeId = this.empCd;
    //  this.totalCount = 0;
    //  this.totalCounthis = 0;
    //  if (flag1 == 'current') {
    //    this.dataSource = new MatTableDataSource<any>();
    //    this._service.FetchHraccsrchDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId, flag1).subscribe(result => {
    //      
    //      if (result && result != undefined && result.length > 0) {
    //        this.dataSource = new MatTableDataSource(result);
    //        this.dataSource.paginator = this.paginator;
    //        this.totalCount = result.length;
    //      }
    //    });
    //  }
    //  else if (flag1 == 'history') {
    //    this.dataSource1 = new MatTableDataSource<any>();
    //    this._service.FetchHraccsrchDetails(employeeId, pageNumber, pageSize, this.searchTermhis, this.roleId, flag1).subscribe(result => {
    //      
    //      if (result && result != undefined && result.length > 0) {
    //        this.dataSource1 = new MatTableDataSource(result);
    //        this.dataSource1.paginator = this.historyPagination;
    //        this.totalCounthis = result.length;
    //      }
    //    });
    //  }
    //}
    HraCityClassComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    HraCityClassComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    HraCityClassComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    HraCityClassComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    HraCityClassComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'hra').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.HraForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            //this.pageNumber = 1;
            _this.getHraFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getHraFormDetails1(_this.empCd, _this.roleId, 'current');
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');
            _this.DeletePopup = false;
        });
    };
    HraCityClassComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    HraCityClassComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.HraForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    HraCityClassComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.HraForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            HRAcc: obj.hrAcc,
            WithEffectDate: obj.withEffectDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.HraForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.HraForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    HraCityClassComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.HraForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.HraForm.get('OrderNo').value;
        this._service.ForwardToCheckerHraccUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getHraFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getHraFormDetails1(_this.empCd, _this.roleId, 'current');
            //this.pageNumber = 1;
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');
        });
        this.Btntxt = 'Save';
    };
    HraCityClassComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.HraForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerHraccUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getHraFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getHraFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.HraForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerHraccUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getHraFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getHraFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.HraForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], HraCityClassComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], HraCityClassComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], HraCityClassComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], HraCityClassComponent.prototype, "sort", void 0);
    HraCityClassComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hra-city-class',
            template: __webpack_require__(/*! ./hra-city-class.component.html */ "./src/app/change/hra-city-class/hra-city-class.component.html"),
            styles: [__webpack_require__(/*! ./hra-city-class.component.css */ "./src/app/change/hra-city-class/hra-city-class.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], HraCityClassComponent);
    return HraCityClassComponent;
}());



/***/ }),

/***/ "./src/app/change/joinin-mode/joinin-mode.component.css":
/*!**************************************************************!*\
  !*** ./src/app/change/joinin-mode/joinin-mode.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL2pvaW5pbi1tb2RlL2pvaW5pbi1tb2RlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7RUFDekIsOEJBQThCO0NBQy9CIiwiZmlsZSI6InNyYy9hcHAvY2hhbmdlL2pvaW5pbi1tb2RlL2pvaW5pbi1tb2RlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW4gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/change/joinin-mode/joinin-mode.component.html":
/*!***************************************************************!*\
  !*** ./src/app/change/joinin-mode/joinin-mode.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"JoiningModeForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"JoiningModeForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"JoiningModeForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Revised Joining Mode\" formControlName=\"JoiningMode\" [compareWith]=\"compare\" required>\r\n            <!--<mat-option [value]=\"\">Select</mat-option>-->\r\n\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            Revised Joining Mode Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n     \r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n   \r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n       \r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentJoiningMode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Joining Mode\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">{{element.currentJoiningMode}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{historyPagination.pageIndex * historyPagination.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n       \r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedJoiningMode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Joining Mode\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">{{element.revisedJoiningMode}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/joinin-mode/joinin-mode.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/change/joinin-mode/joinin-mode.component.ts ***!
  \*************************************************************/
/*! exports provided: JoininModeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoininModeComponent", function() { return JoininModeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var JoininModeComponent = /** @class */ (function () {
    function JoininModeComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentJoiningMode', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedJoiningMode', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    JoininModeComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    JoininModeComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('joiningmode').subscribe(function (res) {
            _this.list = res;
        });
    };
    JoininModeComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.JoiningModeForm.enable();
            //bind form
            this.getJoiningModeFormDetails1(this.empCd, this.roleId, 'history');
            this.getJoiningModeFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.JoiningModeForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.JoiningModeForm.disable();
        }
    };
    JoininModeComponent.prototype.createForm = function () {
        this.JoiningModeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            JoiningMode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.JoiningModeForm.disable();
    };
    JoininModeComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    JoininModeComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.JoiningModeForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.JoiningModeForm.valid) { //change in proc of save & remove snackbar 
            this._service.InsertJoiningModeFormDetails(this.JoiningModeForm.value, flag).subscribe(function (result) {
                _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.JoiningModeForm.disable();
            });
        }
    };
    JoininModeComponent.prototype.getJoiningModeFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetJoiningModeFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentJoiningMode.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.JoiningModeForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.JoiningModeForm.enable();
                }
                if (_this.roleId == '5') {
                    self.JoiningModeForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedJoiningMode.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    JoininModeComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    JoininModeComponent.prototype.btnEditClick = function (obj) {
        this.JoiningModeForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            JoiningMode: obj.joiningMode,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.JoiningModeForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    JoininModeComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    JoininModeComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    JoininModeComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    JoininModeComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    JoininModeComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'joiningmode').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.JoiningModeForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    JoininModeComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    JoininModeComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.JoiningModeForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    JoininModeComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.JoiningModeForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            JoiningMode: obj.joiningMode,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.JoiningModeForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.JoiningModeForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    JoininModeComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.JoiningModeForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.JoiningModeForm.get('OrderNo').value;
        this._service.ForwardToCheckerJoiningModeFormUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    JoininModeComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.JoiningModeForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerJoiningModeFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.JoiningModeForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerJoiningModeFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getJoiningModeFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.JoiningModeForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], JoininModeComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], JoininModeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], JoininModeComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], JoininModeComponent.prototype, "sort", void 0);
    JoininModeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-joinin-mode',
            template: __webpack_require__(/*! ./joinin-mode.component.html */ "./src/app/change/joinin-mode/joinin-mode.component.html"),
            styles: [__webpack_require__(/*! ./joinin-mode.component.css */ "./src/app/change/joinin-mode/joinin-mode.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], JoininModeComponent);
    return JoininModeComponent;
}());



/***/ }),

/***/ "./src/app/change/namegender/namegender.component.css":
/*!************************************************************!*\
  !*** ./src/app/change/namegender/namegender.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL25hbWVnZW5kZXIvbmFtZWdlbmRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQXlCO0VBQ3pCLDhCQUE4QjtDQUMvQiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZS9uYW1lZ2VuZGVyL25hbWVnZW5kZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kaWFsb2dfX2Nsb3NlLWJ0biB7XHJcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gIHZpc2liaWxpdHk6IGhpZGRlbiAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/change/namegender/namegender.component.html":
/*!*************************************************************!*\
  !*** ./src/app/change/namegender/namegender.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n<div class=\"col-md-6 col-lg-6\">\r\n\r\n  <form [formGroup]=\"NameGenderForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card>\r\n      <div class=\"fom-title\">\r\n        Sanction Order Details\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"NameGenderForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"fom-title\">\r\n        Revised Details\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"FirstName\" placeholder=\"First Name\" autocomplete=off required>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.FirstName?.errors?.required\">First Name Required !</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.FirstName?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.FirstName?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.FirstName?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"MiddleName\" placeholder=\"Middle Name\" autocomplete=off required>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.MiddleName?.errors?.required\">Middle Name Required !</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.MiddleName?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.MiddleName?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.MiddleName?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"LastName\" placeholder=\"Last Name\" autocomplete=off required>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.LastName?.errors?.required\">Last Name Required !</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.LastName?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.LastName?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"NameGenderForm?.controls.LastName?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <label>Gender</label><br />\r\n\r\n        <mat-radio-group formControlName=\"Gender\" required>\r\n          <mat-radio-button value=\"M\">Male</mat-radio-button>\r\n          <mat-radio-button value=\"F\">Female</mat-radio-button>\r\n        </mat-radio-group>\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && NameGenderForm?.controls.Gender?.errors?.required\">\r\n          Gender Required !\r\n        </mat-error>\r\n        <!--<mat-error *ngIf=\"NameGenderForm.controls.Gender.errors\">Gender Required!</mat-error>-->\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentEmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentEmpName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"currentDesignation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Designation\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentDesignation}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentGender\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Gender\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentGender}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" >Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" >Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" >Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" >Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" >Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" >Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedEmpName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedEmpName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedDesignation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Designation\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedDesignation}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedGender\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Gender\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedGender}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" >Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" >Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" >Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" >Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" >Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" >Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <!--<button type=\"button\" class=\"btn btn-info\" (click)=\"RejectPopup=!RejectPopup\">No</button>-->\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/namegender/namegender.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/change/namegender/namegender.component.ts ***!
  \***********************************************************/
/*! exports provided: NamegenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NamegenderComponent", function() { return NamegenderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NamegenderComponent = /** @class */ (function () {
    function NamegenderComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'currentEmpName', 'currentDesignation', 'currentGender', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'revisedEmpName', 'revisedDesignation', 'revisedGender', 'Status'];
        this.list = [];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.EDITpara = '';
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    NamegenderComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        //this.dataSource.paginator = this.paginator;
        // this.dataSource1.paginator = this.historyPagination;
        this.infoScreen = true;
    };
    NamegenderComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.NameGenderForm.enable();
            //bind form
            this.getGenderDetails1(this.empCd, this.roleId, 'history');
            this.getGenderDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.NameGenderForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.NameGenderForm.disable();
        }
    };
    NamegenderComponent.prototype.createForm = function () {
        this.NameGenderForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            FirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*$')]),
            MiddleName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*$')]),
            LastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*$')]),
            Gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.NameGenderForm.disable();
    };
    //RejectPopupForm
    NamegenderComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    //submit & edit
    NamegenderComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        this.NameGenderForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.NameGenderForm.valid) {
            this._service.InsertNameGenderDetails(this.NameGenderForm.value, flag).subscribe(function (result) {
                _this.getGenderDetails1(_this.empCd, _this.roleId, 'history');
                _this.getGenderDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.NameGenderForm.disable();
            });
        }
    };
    NamegenderComponent.prototype.getGenderDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetNameGenderDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source        
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.currentEmpName.toLowerCase().includes(filter) ||
                        data.currentDesignation.toLowerCase().includes(filter) ||
                        data.currentGender.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                var list_1 = [];
                //enable or disbale form using current status of orderNo user
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.NameGenderForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.NameGenderForm.enable();
                }
                if (_this.roleId == '5') {
                    self.NameGenderForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.revisedEmpName.toLowerCase().includes(filter) ||
                        data.revisedDesignation.toLowerCase().includes(filter) ||
                        data.revisedGender.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    NamegenderComponent.prototype.btnEditClick = function (obj) {
        this.NameGenderForm.setValue({
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            FirstName: obj.firstName,
            MiddleName: obj.middleName,
            LastName: obj.lastName,
            Gender: obj.gender,
            empCd: this.empCd,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.NameGenderForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    NamegenderComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    NamegenderComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    NamegenderComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    NamegenderComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    NamegenderComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'namegender').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.NameGenderForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getGenderDetails1(_this.empCd, _this.roleId, 'history');
            _this.getGenderDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    NamegenderComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    NamegenderComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.NameGenderForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
        this.NameGenderForm.patchValue({
            Gender: "M"
        });
    };
    NamegenderComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.NameGenderForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            FirstName: obj.firstName,
            MiddleName: obj.middleName,
            LastName: obj.lastName,
            Gender: obj.gender,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.NameGenderForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.NameGenderForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    NamegenderComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.NameGenderForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.NameGenderForm.get('OrderNo').value;
        this._service.ForwardToCheckerNameGenderUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getGenderDetails1(_this.empCd, _this.roleId, 'history');
            _this.getGenderDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    NamegenderComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.NameGenderForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerNameGenderUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getGenderDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getGenderDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.NameGenderForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerNameGenderUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getGenderDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getGenderDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.NameGenderForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], NamegenderComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], NamegenderComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], NamegenderComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], NamegenderComponent.prototype, "sort", void 0);
    NamegenderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-namegender',
            template: __webpack_require__(/*! ./namegender.component.html */ "./src/app/change/namegender/namegender.component.html"),
            styles: [__webpack_require__(/*! ./namegender.component.css */ "./src/app/change/namegender/namegender.component.css")],
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], NamegenderComponent);
    return NamegenderComponent;
}());



/***/ }),

/***/ "./src/app/change/pan-no/pan-no.component.css":
/*!****************************************************!*\
  !*** ./src/app/change/pan-no/pan-no.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL3Bhbi1uby9wYW4tbm8uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvcGFuLW5vL3Bhbi1uby5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/pan-no/pan-no.component.html":
/*!*****************************************************!*\
  !*** ./src/app/change/pan-no/pan-no.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n\r\n  <form [formGroup]=\"PanNoForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">\r\n        Sanction Order Details\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"PanNoForm.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"fom-title\">\r\n        Revised Details\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"PANNo\" placeholder=\"PAN No.\" autocomplete=off required>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.PANNo?.errors?.required\">PAN No. Required !</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.PANNo?.errors?.minlength\">Minimum Length required is 10 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.PANNo?.errors?.maxlength\">Maximum Length required is 10 characters.</mat-error>\r\n          <mat-error *ngIf=\"PanNoForm?.controls.PANNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentPANNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Pan No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentPANNo}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedPANNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Pan No.\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedPANNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/pan-no/pan-no.component.ts":
/*!***************************************************!*\
  !*** ./src/app/change/pan-no/pan-no.component.ts ***!
  \***************************************************/
/*! exports provided: PanNoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanNoComponent", function() { return PanNoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PanNoComponent = /** @class */ (function () {
    function PanNoComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentPANNo', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedPANNo', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    PanNoComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    PanNoComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.PanNoForm.enable();
            //bind form
            this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
            this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.PanNoForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.PanNoForm.disable();
        }
    };
    PanNoComponent.prototype.createForm = function () {
        this.PanNoForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            PANNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-zA-Z0-9]*$")]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.PanNoForm.disable();
    };
    PanNoComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    PanNoComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.PanNoForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.PanNoForm.valid) {
            this._service.InsertPanNoFormDetails(this.PanNoForm.value, flag).subscribe(function (result) {
                //this.pageNumber = 1;
                _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.PanNoForm.disable();
            });
        }
    };
    PanNoComponent.prototype.getPanNoFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        this.pageNumber = 1;
        this._service.GetPanNoFormDetails(empCd, roleId, flag).subscribe(function (data) {
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentPANNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                var list_1 = [];
                //enable or disbale form using current status of orderNo user
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.PanNoForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.PanNoForm.enable();
                }
                if (_this.roleId == '5') {
                    self.PanNoForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedPANNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    PanNoComponent.prototype.btnEditClick = function (obj) {
        this.PanNoForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            PANNo: obj.panNo,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        this.PanNoForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        //if (empStatus == 'P' || empStatus == 'V' || empStatus == 'R') {
        //  this.editable = false;
        //}
        //else
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    //getEmpTransDetails(pageSize, pageNumber, flag1: any) {
    //  // this.getExtensionOfServiceDetails();
    //  let employeeId = this.empCd;
    //  this.totalCount = 0;
    //  this.totalCounthis = 0;
    //  if (flag1 == 'current') {
    //    this.dataSource = new MatTableDataSource<any>();
    //    this._service.FetchPanNosrchDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId, flag1).subscribe(result => {
    //     
    //      if (result && result != undefined && result.length > 0) {
    //        this.dataSource = new MatTableDataSource(result);
    //        this.dataSource.paginator = this.paginator;
    //        this.totalCount = result.length;
    //      }
    //    });
    //  }
    //  else if (flag1 == 'history') {
    //    this.dataSource1 = new MatTableDataSource<any>();
    //    this._service.FetchPanNosrchDetails(employeeId, pageNumber, pageSize, this.searchTermhis, this.roleId, flag1).subscribe(result => {
    //     
    //      if (result && result != undefined && result.length > 0) {
    //        this.dataSource1 = new MatTableDataSource(result);
    //        this.dataSource1.paginator = this.historyPagination;
    //        this.totalCounthis = result.length;
    //      }
    //    });
    //  }
    //}
    PanNoComponent.prototype.applyFiltercurrent = function (filterValue) {
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    PanNoComponent.prototype.applyFilterhistory = function (filterValue) {
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    PanNoComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    PanNoComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    PanNoComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'panno').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.PanNoForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            //this.pageNumber = 1;
            _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'current');
            ////this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
            ////this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');
            _this.DeletePopup = false;
        });
    };
    PanNoComponent.prototype.Cancel = function () {
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    PanNoComponent.prototype.cancelForm = function () {
        this.formGroupDirective.resetForm();
        this.PanNoForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    PanNoComponent.prototype.btnInfoClick = function (obj) {
        this.oldOrderNo = '';
        this.PanNoForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            PANNo: obj.panNo,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        var empStatus = obj.status;
        this.PanNoForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.PanNoForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = true;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    PanNoComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.PanNoForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.PanNoForm.get('OrderNo').value;
        this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'current');
            //this.pageNumber = 1;
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
            //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');
        });
        this.Btntxt = 'Save';
    };
    PanNoComponent.prototype.updateStatus = function (status) {
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.PanNoForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.PanNoForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getPanNoFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.PanNoForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], PanNoComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PanNoComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PanNoComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], PanNoComponent.prototype, "sort", void 0);
    PanNoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pan-no',
            template: __webpack_require__(/*! ./pan-no.component.html */ "./src/app/change/pan-no/pan-no.component.html"),
            styles: [__webpack_require__(/*! ./pan-no.component.css */ "./src/app/change/pan-no/pan-no.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], PanNoComponent);
    return PanNoComponent;
}());



/***/ }),

/***/ "./src/app/change/pf-nps/pf-nps.component.css":
/*!****************************************************!*\
  !*** ./src/app/change/pf-nps/pf-nps.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL3BmLW5wcy9wZi1ucHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvcGYtbnBzL3BmLW5wcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/pf-nps/pf-nps.component.html":
/*!*****************************************************!*\
  !*** ./src/app/change/pf-nps/pf-nps.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"PfNpsForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"PfNpsForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"PF Type\" formControlName=\"Pftype\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            PF Type is required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"Pranno\" placeholder=\"PRAN No.\" autocomplete=off required>\r\n             <mat-error *ngIf=\"PfNpsForm?.controls.Pranno?.errors?.required\">PRAN No. Required !</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Pranno?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Pranno?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"PfNpsForm?.controls.Pranno?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"WithEffectDate.open()\" [matDatepicker]=\"WithEffectDate\" [max]=\"maxDate\" formControlName=\"WithEffectDate\" placeholder=\"With Effect Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"WithEffectDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #WithEffectDate></mat-datepicker>\r\n          <mat-error>\r\n            With Effect Date Required!\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentPFType\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            PF Type\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentPFType}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentPRANNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            PRAN No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentPRANNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" >Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" >Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" >Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" >Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" >Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{historyPagination.pageIndex * historyPagination.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedPFType\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            PF Type\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedPFType}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedPRANNo\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            PRAN No.\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedPRANNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/pf-nps/pf-nps.component.ts":
/*!***************************************************!*\
  !*** ./src/app/change/pf-nps/pf-nps.component.ts ***!
  \***************************************************/
/*! exports provided: PfNpsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PfNpsComponent", function() { return PfNpsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PfNpsComponent = /** @class */ (function () {
    function PfNpsComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.searchTerm = '';
        this.searchTermhis = '';
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentPFType', 'currentPRANNo', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedPFType', 'revisedPRANNo', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    PfNpsComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        // this.empCd = 'A**013F91001';
        this.BindPftype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    PfNpsComponent.prototype.BindPftype = function () {
        var _this = this;
        this._service.GetDdlvalue('pfnps').subscribe(function (res) {
            _this.list = res;
        });
    };
    PfNpsComponent.prototype.GetcommonMethod = function (value) {
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.PfNpsForm.enable();
            //bind form
            this.getPfNpsFormDetails1(this.empCd, this.roleId, 'history');
            this.getPfNpsFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.PfNpsForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.PfNpsForm.disable();
        }
    };
    PfNpsComponent.prototype.createForm = function () {
        this.PfNpsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Pftype: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            Pranno: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[0-9]*$")]),
            WithEffectDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.PfNpsForm.disable();
    };
    //RejectPopupForm
    PfNpsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    //submit & edit
    PfNpsComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.PfNpsForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (this.PfNpsForm.valid) {
            this._service.InsertPfNpsFormDetails(this.PfNpsForm.value, flag).subscribe(function (result) {
                //
                _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.PfNpsForm.disable();
            });
        }
    };
    PfNpsComponent.prototype.getPfNpsFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        //
        //this.Bindddltype(); //set ddl
        this.pageNumber = 1;
        this._service.GetPfNpsFormDetails(empCd, roleId, flag).subscribe(function (data) {
            //
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentPFType.toLowerCase().includes(filter) ||
                        data.currentPRANNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                var list_1 = [];
                //enable or disbale form using current status of orderNo user
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.PfNpsForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.PfNpsForm.enable();
                }
                if (_this.roleId == '5') {
                    self.PfNpsForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedPFType.toLowerCase().includes(filter) ||
                        data.revisedPRANNo.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    PfNpsComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    PfNpsComponent.prototype.btnEditClick = function (obj) {
        this.PfNpsForm.setValue({
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            Pftype: obj.pfType,
            Pranno: obj.pranNo,
            WithEffectDate: obj.withEffectDate,
            empCd: this.empCd,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        //
        this.PfNpsForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    PfNpsComponent.prototype.applyFiltercurrent = function (filterValue) {
        //
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    PfNpsComponent.prototype.applyFilterhistory = function (filterValue) {
        //
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    PfNpsComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    PfNpsComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    PfNpsComponent.prototype.confirmDelete = function () {
        var _this = this;
        //
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'pfnps').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.formGroupDirective.resetForm();
                _this.PfNpsForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    PfNpsComponent.prototype.Cancel = function () {
        //
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    PfNpsComponent.prototype.cancelForm = function () {
        //
        this.formGroupDirective.resetForm();
        this.PfNpsForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    PfNpsComponent.prototype.btnInfoClick = function (obj) {
        //
        this.oldOrderNo = '';
        this.PfNpsForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Pftype: obj.pfType,
            Pranno: obj.pranNo,
            WithEffectDate: obj.withEffectDate,
            Remarks: obj.remarks,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //
        var empStatus = obj.status;
        this.PfNpsForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.PfNpsForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    PfNpsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.PfNpsForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.PfNpsForm.get('OrderNo').value;
        this._service.ForwardToCheckerPfNpsUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    PfNpsComponent.prototype.updateStatus = function (status) {
        //
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.PfNpsForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerPfNpsUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.PfNpsForm.disable();
                        _this.infoScreen = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerPfNpsUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getPfNpsFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.PfNpsForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], PfNpsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PfNpsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PfNpsComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], PfNpsComponent.prototype, "sort", void 0);
    PfNpsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pf-nps',
            template: __webpack_require__(/*! ./pf-nps.component.html */ "./src/app/change/pf-nps/pf-nps.component.html"),
            styles: [__webpack_require__(/*! ./pf-nps.component.css */ "./src/app/change/pf-nps/pf-nps.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], PfNpsComponent);
    return PfNpsComponent;
}());



/***/ }),

/***/ "./src/app/change/state-gis/state-gis.component.css":
/*!**********************************************************!*\
  !*** ./src/app/change/state-gis/state-gis.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL3N0YXRlLWdpcy9zdGF0ZS1naXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2Uvc3RhdGUtZ2lzL3N0YXRlLWdpcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/state-gis/state-gis.component.html":
/*!***********************************************************!*\
  !*** ./src/app/change/state-gis/state-gis.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n\r\n  <form [formGroup]=\"StateGISForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">\r\n        Sanction Order Details\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n\r\n          <mat-error *ngIf=\"StateGISForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"StateGISForm.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"fom-title\">\r\n        Revised Details\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <label>State / Other GIS Applicable</label><br />\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-radio-group formControlName=\"StateOtherGIS\" required>\r\n          <mat-radio-button value=\"Y\" (change)=\"getStGIS1DetailsChange('Y');\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"N\" (change)=\"getStGIS1DetailsChange('N');\">No</mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && StateGISForm?.controls.StateOtherGIS?.errors?.required\">\r\n          Select Either Required Value\r\n        </mat-error>\r\n\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"StateGISAdjusted\">\r\n        <label>GIS Adjusted by Office</label><br />\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"StateGISAdjusted\">\r\n        <mat-radio-group formControlName=\"StateGISAdjusted\" required>\r\n          <mat-radio-button value=\"A\" (change)=\"getGIS2DetailsChange('A');\">AG</mat-radio-button>\r\n          <mat-radio-button value=\"P\" (change)=\"getGIS2DetailsChange('P');\">PAO </mat-radio-button>\r\n          <mat-radio-button value=\"O\" (change)=\"getGIS2DetailsChange('O');\">Other</mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n        <mat-error class=\"mat-error ng-star-inserted mat-form-field\" *ngIf=\"isSubmitted && StateGISForm?.controls.StateGISAdjusted?.errors?.required\">\r\n          Select Either Required Value\r\n        </mat-error>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\" *ngIf=\"StateGISForm.controls.StateGISAdjusted.value === 'A' && StateGISForm.controls.StateOtherGIS.value === 'Y'\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Office\" formControlName=\"StateGISAGOffice\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list1\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            AG Office required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\" *ngIf=\"StateGISForm.controls.StateGISAdjusted.value === 'P' && StateGISForm.controls.StateOtherGIS.value === 'Y'\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"StateGISPAOCode\" placeholder=\"Enter PAO Code\" autocomplete=off required>\r\n\r\n          <mat-error *ngIf=\"StateGISForm?.controls.StateGISPAOCode?.errors?.required\">PAO Code Required !</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.StateGISPAOCode?.errors?.maxlength\">Maximum Length required is 6 characters.</mat-error>\r\n          <mat-error *ngIf=\"StateGISForm?.controls.StateGISPAOCode?.errors?.pattern\">Only Numbers allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\" *ngIf=\"StateGISForm.controls.StateGISAdjusted.value === 'O' && StateGISForm.controls.StateOtherGIS.value === 'Y'\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Office\" formControlName=\"StateGISOtherOffice\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list2\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            Other Office required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentStateOtherGIS\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Eligibile for State GIS\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentStateOtherGIS}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{historyPagination.pageIndex * historyPagination.pageSize + i + 1}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"revisedStateOtherGIS\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Eligibile for State GIS\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedStateOtherGIS}} </mat-cell>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/state-gis/state-gis.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/change/state-gis/state-gis.component.ts ***!
  \*********************************************************/
/*! exports provided: StateGisComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateGisComponent", function() { return StateGisComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var StateGisComponent = /** @class */ (function () {
    function StateGisComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentStateOtherGIS', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedStateOtherGIS', 'Status'];
        this.list1 = [];
        this.list2 = [];
        this.EDITpara = '';
        this.StateGISAdjusted = false;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.isSubmitted = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    }
    StateGisComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
        //this.onChanges();//reset field by Y or N
    };
    StateGisComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        this.isSubmitted = false;
        if (this.empCd) {
            this.StateGISForm.enable();
            //bind form
            this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
            this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.StateGISForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.StateGISForm.disable();
        }
        this.StateGISAdjusted = false;
    };
    //
    StateGisComponent.prototype.createForm = function () {
        this.StateGISForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            StateOtherGIS: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            StateGISAdjusted: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            StateGISAGOffice: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            StateGISPAOCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            StateGISOtherOffice: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.StateGISForm.disable();
    };
    StateGisComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    //onChanges(): void {
    //  this.StateGISForm.get('StateOtherGIS')
    //    .valueChanges
    //    .subscribe(value => {
    //      let StateGISAdjusted = this.StateGISForm.get('StateGISAdjusted');
    //      let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
    //      let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
    //      let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');
    //      if (value === 'N') {
    //        debugger;
    //        this.StateGISAdjusted = false;
    //        StateGISAdjusted.setValue(null);
    //        StateGISAdjusted.disable()
    //        StateGISAdjusted.clearValidators();
    //        StateGISAGOffice.setValue(null);
    //        StateGISAGOffice.disable()
    //        StateGISAGOffice.clearValidators();
    //        StateGISPAOCode.setValue(null);
    //        StateGISPAOCode.disable()
    //        StateGISPAOCode.clearValidators();
    //        StateGISOtherOffice.setValue(null);
    //        StateGISOtherOffice.disable()
    //        StateGISOtherOffice.clearValidators();
    //      }
    //      else if (value === 'Y') {
    //        this.StateGISAdjusted = true;
    //        StateGISAdjusted.enable();
    //        StateGISAdjusted.setValidators([Validators.required]);
    //        StateGISAdjusted.setValue(null);
    //      }
    //    });
    //  this.StateGISForm.get('StateGISAdjusted')
    //    .valueChanges
    //    .subscribe(value => {
    //      let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
    //      let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
    //      let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');
    //      if (value === 'A') {
    //        debugger;
    //        StateGISAGOffice.enable();
    //        StateGISAGOffice.setValidators([Validators.required]);
    //        StateGISAGOffice.setValue(null);
    //      }
    //      else {
    //        StateGISAGOffice.disable()
    //        StateGISAGOffice.clearValidators();
    //      }
    //      if (value === 'P') {
    //        debugger;
    //        StateGISPAOCode.enable();
    //        StateGISPAOCode.setValidators([Validators.required, Validators.maxLength(6), Validators.pattern('^[0-9]*$')]);
    //        StateGISPAOCode.setValue(null);
    //      }
    //      else {
    //        StateGISPAOCode.disable()
    //        StateGISPAOCode.clearValidators();
    //      }
    //      if (value === 'O') {
    //        debugger;
    //        StateGISOtherOffice.enable();
    //        StateGISOtherOffice.setValidators([Validators.required]);
    //        StateGISOtherOffice.setValue(null);
    //      }
    //      else {
    //        StateGISOtherOffice.disable()
    //        StateGISOtherOffice.clearValidators();
    //      }
    //    });
    //}
    StateGisComponent.prototype.getStGIS1DetailsChange = function (value) {
        var StateGISAdjusted = this.StateGISForm.get('StateGISAdjusted');
        var StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
        var StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
        var StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');
        if (value === 'N') {
            debugger;
            this.StateGISAdjusted = false;
            StateGISAdjusted.setValue(null);
            StateGISAdjusted.disable();
            StateGISAdjusted.clearValidators();
            StateGISAGOffice.setValue(null);
            StateGISAGOffice.disable();
            StateGISAGOffice.clearValidators();
            StateGISPAOCode.setValue(null);
            StateGISPAOCode.disable();
            StateGISPAOCode.clearValidators();
            StateGISOtherOffice.setValue(null);
            StateGISOtherOffice.disable();
            StateGISOtherOffice.clearValidators();
        }
        else if (value === 'Y') {
            this.StateGISAdjusted = true;
            StateGISAdjusted.enable();
            StateGISAdjusted.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            StateGISAdjusted.setValue(null);
        }
    };
    StateGisComponent.prototype.getGIS2DetailsChange = function (value) {
        var StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
        var StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
        var StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');
        if (value === 'A') {
            debugger;
            StateGISAGOffice.enable();
            StateGISAGOffice.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            StateGISAGOffice.setValue(null);
        }
        else {
            StateGISAGOffice.disable();
            StateGISAGOffice.clearValidators();
        }
        if (value === 'P') {
            debugger;
            StateGISPAOCode.enable();
            StateGISPAOCode.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[0-9]*$')]);
            StateGISPAOCode.setValue(null);
        }
        else {
            StateGISPAOCode.disable();
            StateGISPAOCode.clearValidators();
        }
        if (value === 'O') {
            debugger;
            StateGISOtherOffice.enable();
            StateGISOtherOffice.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            StateGISOtherOffice.setValue(null);
        }
        else {
            StateGISOtherOffice.disable();
            StateGISOtherOffice.clearValidators();
        }
    };
    //
    StateGisComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('StateGISAGOffice').subscribe(function (res) {
            _this.list1 = res;
        });
        this._service.GetDdlvalue('StateGISOtherOffice').subscribe(function (res) {
            _this.list2 = res;
        });
    };
    StateGisComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        this.isSubmitted = true;
        //debugger;
        this.StateGISForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (!this.StateGISForm.valid) {
            return false;
        }
        //console.log(this.StateGISForm.value);
        if (this.StateGISForm.valid) { //change in proc of save & remove snackbar 
            debugger;
            this._service.InsertStateGISFormDetails(this.StateGISForm.value, flag).subscribe(function (result) {
                debugger;
                _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                        _this.StateGISAdjusted = false;
                        // this.isSubmitted = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                    else { //implement this in all forms
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                        _this.isSubmitted = false;
                    }
                }
                _this.isSubmitted = false;
                _this.fwdtochker = false;
                _this.StateGISForm.disable();
            });
        }
    };
    StateGisComponent.prototype.getStateGISFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        //debugger;
        //this.Bindddltype(); //set ddl
        this.pageNumber = 1;
        this._service.GetStateGISFormDetails(empCd, roleId, flag).subscribe(function (data) {
            //debugger;
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentStateOtherGIS.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    debugger;
                    self.StateGISForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.StateGISForm.enable();
                }
                if (_this.roleId == '5') {
                    self.StateGISForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //debugger;
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedStateOtherGIS.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    StateGisComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    StateGisComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.StateGISForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            StateOtherGIS: obj.stateOtherGIS,
            StateGISAdjusted: obj.stateGISAdjusted,
            StateGISAGOffice: obj.stateGISAGOffice,
            StateGISPAOCode: obj.stateGISPAOCode,
            StateGISOtherOffice: obj.stateGISOtherOffice,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        console.log(this.oldOrderNo);
        this.StateGISForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        debugger;
        var StateOtherGIS = obj.stateOtherGIS; //check Y/N value or not
        if (StateOtherGIS == 'Y') {
            this.StateGISAdjusted = true;
        }
        else if (StateOtherGIS == 'N') {
            this.StateGISAdjusted = false;
        }
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    StateGisComponent.prototype.applyFiltercurrent = function (filterValue) {
        //debugger;
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    StateGisComponent.prototype.applyFilterhistory = function (filterValue) {
        //debugger;
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    StateGisComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    StateGisComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.StateGISAdjusted = false;
    };
    StateGisComponent.prototype.confirmDelete = function () {
        var _this = this;
        //debugger;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'StateGIS').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
                _this.isSubmitted = false;
                _this.StateGISAdjusted = false;
                _this.StateGISForm.enable();
                _this.infoScreen = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            //this.pageNumber = 1;
            _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    StateGisComponent.prototype.Cancel = function () {
        //debugger;
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    StateGisComponent.prototype.cancelForm = function () {
        //debugger;
        this.formGroupDirective.resetForm();
        this.StateGISForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    StateGisComponent.prototype.btnInfoClick = function (obj) {
        //debugger;
        this.oldOrderNo = '';
        this.StateGISForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            StateOtherGIS: obj.stateOtherGIS,
            StateGISAdjusted: obj.stateGISAdjusted,
            StateGISAGOffice: obj.stateGISAGOffice,
            StateGISPAOCode: obj.stateGISPAOCode,
            StateGISOtherOffice: obj.stateGISOtherOffice,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //debugger;
        var StateOtherGIS = obj.stateOtherGIS; //check Y/N value or not
        if (StateOtherGIS == 'Y') {
            this.StateGISAdjusted = true;
        }
        else if (StateOtherGIS == 'N') {
            this.StateGISAdjusted = false;
        }
        var empStatus = obj.status;
        this.StateGISForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.StateGISForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    StateGisComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.StateGISForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.StateGISForm.get('OrderNo').value;
        this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            //debugger;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.isSubmitted = false;
                _this.StateGISAdjusted = false;
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    StateGisComponent.prototype.updateStatus = function (status) {
        //debugger;   
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.StateGISForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.isSubmitted = false;
                        _this.StateGISAdjusted = false;
                        _this.StateGISForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getStateGISFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.isSubmitted = false;
                    _this.StateGISAdjusted = false;
                    _this.StateGISForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], StateGisComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], StateGisComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], StateGisComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], StateGisComponent.prototype, "sort", void 0);
    StateGisComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-state-gis',
            template: __webpack_require__(/*! ./state-gis.component.html */ "./src/app/change/state-gis/state-gis.component.html"),
            styles: [__webpack_require__(/*! ./state-gis.component.css */ "./src/app/change/state-gis/state-gis.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], StateGisComponent);
    return StateGisComponent;
}());



/***/ }),

/***/ "./src/app/change/tpta-city-class/tpta-city-class.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/change/tpta-city-class/tpta-city-class.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dialog__close-btn {\r\n  display: none !important;\r\n  visibility: hidden !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlL3RwdGEtY2l0eS1jbGFzcy90cHRhLWNpdHktY2xhc3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6Qiw4QkFBOEI7Q0FDL0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2UvdHB0YS1jaXR5LWNsYXNzL3RwdGEtY2l0eS1jbGFzcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpYWxvZ19fY2xvc2UtYnRuIHtcclxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/change/tpta-city-class/tpta-city-class.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/change/tpta-city-class/tpta-city-class.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-commonddl (onchange)=\"GetcommonMethod($event)\"></app-commonddl>\r\n\r\n<div class=\"col-md-6 col-lg-6 \">\r\n\r\n  <form [formGroup]=\"TPTAForm\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput formControlName=\"OrderNo\" placeholder=\"Order No./Authority/Ref.Letter No.\" [readonly]=\"editable\" autocomplete=off required>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.OrderNo?.errors?.required\">Order No. Required !</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.OrderNo?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.OrderNo?.errors?.maxlength\">Maximum Length required is 100 characters.</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.OrderNo?.errors?.pattern\">Special characters not allowed</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"maxDate\" formControlName=\"OrderDate\" placeholder=\"Order Date\" autocomplete=off required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error>Order Date Required!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"Remarks\" rows=\"1\" placeholder=\"Remarks\" required></textarea>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.Remarks?.errors?.required\">Remarks Required !</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.Remarks?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.Remarks?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n          <mat-error *ngIf=\"TPTAForm?.controls.Remarks?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-12\" *ngIf=\"TPTAForm.controls.rejectionRemark.value \">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\"\r\n                    [readonly]=\"editable\"></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Revised Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"City Class TPTA\" formControlName=\"TPTA\" [compareWith]=\"compare\" required>\r\n            <mat-option *ngFor=\"let ot of list\" [value]=\"ot.value1\">\r\n              {{ot.text1}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n          <mat-error>\r\n            City Class TPTA required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\" style=\"display:none;\">\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"empCd\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"username\">\r\n        </mat-form-field>\r\n        <mat-form-field>\r\n          <input matInput formControlName=\"oldOrderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"roleId == 6\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd ||infoScreen\">{{Btntxt}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"infoScreen||!btnCancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!fwdtochker\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n</div>\r\n\r\n<!--- Table Right--->\r\n<div class=\"col-md-12 col-lg-6 mb-15\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">Current Details</div>\r\n    <mat-card>\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{paginator.pageIndex * paginator.pageSize + i + 1}}\r\n\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"currentTPTA\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            City Class TPTA\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.currentTPTA}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'R' || element.status == 'U')&& roleId == 6\"\r\n               class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">\r\n              edit\r\n            </a>\r\n            <a *ngIf=\"(element.status == 'E' || element.status == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteChangeDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.status != 'E' && element.status != 'R' && element.status != 'U' && element.status != 'N') && roleId == 6\"\r\n               class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element)\">\r\n              info\r\n            </a>\r\n\r\n          </mat-cell>\r\n\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n\r\n      <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">History Details</div>\r\n    <mat-card>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilterhistory($event.target.value)\" [disabled]=\"!empCd\" (keypress)=\"allowAlphaNumericSpace($event)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <mat-table #table [dataSource]=\"dataSource1\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"Sr.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">\r\n            {{historyPagination.pageIndex * historyPagination.pageSize + i + 1}}\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Emp. Code\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empCd}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmployeeName\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Employee Name\r\n\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.empName}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"revisedTPTA\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            City Class TPTA\r\n          </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.revisedTPTA}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label *ngIf=\"element.status == 'E'\" style=\"color: orange\">Entry</label>\r\n            <label *ngIf=\"element.status == 'U'\" style=\"color: darkorange\">Entry Updated</label>\r\n            <label *ngIf=\"element.status == 'P'\" style=\"color: green\">Sent to Checker</label>\r\n            <label *ngIf=\"element.status == 'V'\" style=\"color: blue\">Verified</label>\r\n            <label *ngIf=\"element.status == 'R'\" style=\"color: red\">Rejected</label>\r\n            <label *ngIf=\"element.status == 'N'\" style=\"color: red\">Exists Value</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns1\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns1;\"></mat-row>\r\n      </mat-table>\r\n      <mat-paginator #historyPagination [length]=\"totalCounthis\" [pageSizeOptions]=\"[10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n      <div *ngIf=\"dataSource1.data.length === 0 || dataSource1.filteredData.length === 0\">No records found</div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure to Delete the Record?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput formControlName=\"rejectionRemark1\" rows=\"2\" placeholder=\"Remarks\" required></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.required\">Remarks Required !</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.minlength\">Minimum Length required is 3 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.maxlength\">Maximum Length required is 200 characters.</mat-error>\r\n        <mat-error *ngIf=\"RejectPopupForm?.controls.rejectionRemark1?.errors?.pattern\">Special characters not allowed</mat-error>\r\n\r\n      </mat-form-field>\r\n      &nbsp;&nbsp;\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">No</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/change/tpta-city-class/tpta-city-class.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/change/tpta-city-class/tpta-city-class.component.ts ***!
  \*********************************************************************/
/*! exports provided: TptaCityClassComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TptaCityClassComponent", function() { return TptaCityClassComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/Change/ChangeSr.service */ "./src/app/services/Change/ChangeSr.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TptaCityClassComponent = /** @class */ (function () {
    function TptaCityClassComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        this.editable = true;
        this.fwdtochker = false;
        this.infoScreen = false;
        this.btnCancel = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.pageSize = 0;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.totalCounthis = 0;
        this.displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentTPTA', 'Status', 'Action'];
        this.displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedTPTA', 'Status'];
        this.list = [];
        this.EDITpara = '';
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](); //hist
        this.maxDate = new Date();
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.username = sessionStorage.getItem('username');
    }
    TptaCityClassComponent.prototype.ngOnInit = function () {
        this.Btntxt = 'Save';
        this.createForm();
        this.Bindddltype();
        this.RejectPopupcreateForm();
        this.infoScreen = true;
    };
    TptaCityClassComponent.prototype.GetcommonMethod = function (value) {
        debugger;
        this.formGroupDirective.resetForm();
        this.empCd = value;
        if (this.empCd) {
            this.TPTAForm.enable();
            //bind form
            this.getTPTAFormDetails1(this.empCd, this.roleId, 'history');
            this.getTPTAFormDetails1(this.empCd, this.roleId, 'current');
        }
        else {
            this.TPTAForm.disable();
        }
        this.editable = false;
        this.Btntxt = 'Save';
        this.fwdtochker = false;
        this.infoScreen = false;
        if (this.roleId == '5') {
            this.TPTAForm.disable();
        }
    };
    TptaCityClassComponent.prototype.createForm = function () {
        this.TPTAForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            OrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])),
            OrderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            Remarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')]),
            TPTA: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.username),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rejectionRemark),
            oldOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.TPTAForm.disable();
    };
    TptaCityClassComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(200), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9().,-/ ]*$')])
        });
    };
    TptaCityClassComponent.prototype.Bindddltype = function () {
        var _this = this;
        this._service.GetDdlvalue('tpta').subscribe(function (res) {
            _this.list = res;
        });
    };
    TptaCityClassComponent.prototype.onSubmit = function (flag) {
        var _this = this;
        //debugger;
        this.TPTAForm.patchValue({
            empCd: this.empCd,
            username: this.username
        });
        //check duplicate record then submit
        if (this.oldOrderNo != null && this.Btntxt == "Update") {
            flag = "EDIT";
        }
        if (!this.TPTAForm.valid) {
            return false;
        }
        //console.log(this.TPTAForm.value);
        if (this.TPTAForm.valid) { //change in proc of save & remove snackbar 
            debugger;
            this._service.InsertTPTAFormDetails(this.TPTAForm.value, flag).subscribe(function (result) {
                debugger;
                _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'history');
                _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'current');
                _this.Btntxt = 'Save';
                if (flag == 'EDIT') {
                    _this.Btntxt = 'Update';
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                        //bind form
                        _this.Btntxt = 'Save';
                        _this.editable = true;
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                }
                else {
                    if (result == '1') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                        _this.formGroupDirective.resetForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveFailedMsg);
                        _this.formGroupDirective.resetForm();
                    }
                }
                _this.fwdtochker = false;
                _this.TPTAForm.disable();
            });
        }
    };
    TptaCityClassComponent.prototype.getTPTAFormDetails1 = function (empCd, roleId, flag) {
        var _this = this;
        //debugger;
        //this.Bindddltype(); //set ddl
        this.pageNumber = 1;
        this._service.GetTPTAFormDetails(empCd, roleId, flag).subscribe(function (data) {
            //debugger;
            var self = _this;
            if (flag == 'current') {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.currentTPTA.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource.data.length;
                _this.dataSource.paginator = _this.paginator;
                //enable or disbale form using current status of orderNo user
                var list_1 = [];
                data.forEach(function (element) {
                    list_1.push(element.status);
                });
                if (list_1[0] == 'E' || list_1[0] == 'U' || list_1[0] == 'P' || list_1[0] == 'R') {
                    self.TPTAForm.disable();
                    _this.infoScreen = true;
                }
                else {
                    self.TPTAForm.enable();
                }
                if (_this.roleId == '5') {
                    self.TPTAForm.disable();
                    _this.infoScreen = false;
                }
            }
            else if (flag == 'history') {
                //debugger;
                _this.dataSource1 = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                //filter in Data source
                _this.dataSource1.filterPredicate = function (data, filter) {
                    return data.empCd.toLowerCase().includes(filter) ||
                        data.empName.toLowerCase().includes(filter) ||
                        data.revisedTPTA.toLowerCase().includes(filter);
                };
                //END Of filter in Data source
                _this.pageSize = _this.dataSource1.data.length;
                _this.dataSource1.paginator = _this.historyPagination;
            }
        });
    };
    //EDIT
    TptaCityClassComponent.prototype.compare = function (o1, o2) {
        if (o1 == o2)
            return true;
        else
            return false;
    };
    TptaCityClassComponent.prototype.btnEditClick = function (obj) {
        debugger;
        this.TPTAForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            TPTA: obj.tpta,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: obj.orderNo
        });
        this.oldOrderNo = obj.orderNo;
        console.log(this.oldOrderNo);
        //debugger;
        this.TPTAForm.enable();
        this.infoScreen = false;
        //this.fwdtochker = true;
        this.Btntxt = 'Update';
        var empStatus = obj.status;
        if (empStatus == 'R') {
            this.EDITpara = 'EDIT';
            this.editable = true;
        }
        else if (empStatus == 'E' || empStatus == 'U') {
            this.EDITpara = 'EDIT';
            this.editable = false;
        }
        else {
            this.EDITpara = '';
            this.editable = false;
        }
        if (this.roleId == '6' && empStatus == 'U') {
            this.fwdtochker = true;
        }
        else {
            this.fwdtochker = false;
        }
    };
    TptaCityClassComponent.prototype.applyFiltercurrent = function (filterValue) {
        //debugger;
        this.paginator.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.filteredData.length > 0) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource.filteredData.length = 0;
        }
    };
    TptaCityClassComponent.prototype.applyFilterhistory = function (filterValue) {
        //debugger;
        this.historyPagination.pageIndex = 0;
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource1.filter = filterValue;
        if (this.dataSource1.filteredData.length > 0) {
            this.dataSource1.filter = filterValue.trim().toLowerCase();
        }
        else {
            this.dataSource1.filteredData.length = 0;
        }
    };
    TptaCityClassComponent.prototype.allowAlphaNumericSpace = function (event) {
        if (event.target.value === "") {
            debugger;
            var charCode = (event.which) ? event.which : event.keyCode;
            if ((charCode !== 32) && // space
                (charCode >= 48 && charCode <= 57) || // numeric (0-9)
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            return false;
        }
    };
    TptaCityClassComponent.prototype.DeleteChangeDetails = function (obj) {
        this.objToBeDeleted = obj;
        this.formGroupDirective.resetForm();
        this.Btntxt = 'Save';
        this.fwdtochker = false;
    };
    TptaCityClassComponent.prototype.confirmDelete = function () {
        var _this = this;
        //debugger;
        this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'tpta').subscribe(function (result) {
            _this.objToBeDeleted = null;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                //this.editable = true;
                _this.formGroupDirective.resetForm();
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteFailedMsg);
            }
            //this.pageNumber = 1;
            _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'current');
            _this.DeletePopup = false;
        });
    };
    TptaCityClassComponent.prototype.Cancel = function () {
        //debugger;
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = null;
    };
    TptaCityClassComponent.prototype.cancelForm = function () {
        //debugger;
        this.formGroupDirective.resetForm();
        this.TPTAForm.enable();
        this.editable = false;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
    };
    TptaCityClassComponent.prototype.btnInfoClick = function (obj) {
        //debugger;
        this.oldOrderNo = '';
        this.TPTAForm.setValue({
            empCd: this.empCd,
            OrderNo: obj.orderNo,
            OrderDate: obj.orderDate,
            Remarks: obj.remarks,
            TPTA: obj.tpta,
            rejectionRemark: obj.rejectionRemark,
            username: this.username,
            oldOrderNo: this.oldOrderNo
        });
        //debugger;
        var empStatus = obj.status;
        this.TPTAForm.disable();
        if (this.roleId == '5' && empStatus == 'N') {
            this.TPTAForm.enable();
        }
        else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
            this.infoScreen = false;
        }
        else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
            this.infoScreen = true;
            this.btnCancel = false;
            this.Btntxt = 'Save';
        }
        else {
            this.infoScreen = true;
            this.fwdtochker = false;
            this.Btntxt = 'Save';
        }
    };
    //fwdtochecker
    TptaCityClassComponent.prototype.forwardToChecker = function () {
        var _this = this;
        //debugger;
        if (!this.TPTAForm.valid) {
            return false;
        }
        var employeeId = this.empCd.trim();
        var orderNo = this.TPTAForm.get('OrderNo').value;
        this._service.ForwardToCheckerTPTAUserDtls(employeeId, orderNo, null, null).subscribe(function (result) {
            //debugger;
            if (result == 1) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerMsg);
                _this.formGroupDirective.reset();
                _this.fwdtochker = false;
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.forwardDDOCheckerFailedMsg);
                _this.fwdtochker = true;
            }
            _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'history');
            _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'current');
        });
        this.Btntxt = 'Save';
    };
    TptaCityClassComponent.prototype.updateStatus = function (status) {
        //debugger;   
        var _this = this;
        var rejectionRemark1 = '';
        var employeeId = this.empCd;
        var orderNo = this.TPTAForm.get('OrderNo').value;
        if (status == 'R') {
            rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;
            if (this.RejectPopupForm.valid) {
                this._service.ForwardToCheckerTPTAUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                    if (result == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDORejectedByChecker);
                        _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'history');
                        _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'current');
                        _this.formGroupDirective.reset();
                        _this.TPTAForm.disable();
                        _this.infoScreen = false;
                        //this.createForm();
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                    }
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
                });
            }
        }
        else {
            this._service.ForwardToCheckerTPTAUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(function (result) {
                if (result == 1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.DDOVerifiedByChecker);
                    _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'history');
                    _this.getTPTAFormDetails1(_this.empCd, _this.roleId, 'current');
                    _this.formGroupDirective.reset();
                    _this.TPTAForm.disable();
                    _this.infoScreen = false;
                    //this.createForm();
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateFailedMsg);
                }
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], TptaCityClassComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], TptaCityClassComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('historyPagination', { read: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"] }),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], TptaCityClassComponent.prototype, "historyPagination", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], TptaCityClassComponent.prototype, "sort", void 0);
    TptaCityClassComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tpta-city-class',
            template: __webpack_require__(/*! ./tpta-city-class.component.html */ "./src/app/change/tpta-city-class/tpta-city-class.component.html"),
            styles: [__webpack_require__(/*! ./tpta-city-class.component.css */ "./src/app/change/tpta-city-class/tpta-city-class.component.css")]
        }),
        __metadata("design:paramtypes", [_services_Change_ChangeSr_service__WEBPACK_IMPORTED_MODULE_3__["ChangeSr"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], TptaCityClassComponent);
    return TptaCityClassComponent;
}());



/***/ }),

/***/ "./src/app/services/Change/ChangeSr.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/Change/ChangeSr.service.ts ***!
  \*****************************************************/
/*! exports provided: ChangeSr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeSr", function() { return ChangeSr; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ChangeSr = /** @class */ (function () {
    function ChangeSr(http, config) {
        this.http = http;
        this.config = config;
    }
    //Getddlvalue
    ChangeSr.prototype.GetDdlvalue = function (Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDdlvalue + Flag);
    };
    //common delete using flag
    ChangeSr.prototype.ChangeDeleteDetails = function (empCd, orderNo, flag) {
        return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag);
    };
    //NameGender
    ChangeSr.prototype.InsertNameGenderDetails = function (objPro, Flag) {
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertNameGenderDetails + "/InsertNameGenderDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetNameGenderDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetNameGenderDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerNameGenderUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerNameGenderUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //pfNps
    ChangeSr.prototype.InsertPfNpsFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertPfNpsFormDetails + "/InsertPfNpsFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetPfNpsFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetPfNpsFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerPfNpsUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerPfNpsUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //panno
    ChangeSr.prototype.InsertPanNoFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertPanNoFormDetails + "/InsertPanNoFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetPanNoFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetPanNoFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    //FetchPanNosrchDetails(employeeId, pageNumber, pageSize, searchTerm, roleId, flag1): Observable<any> {
    //  return this.http.get(this.config.api_base_url + this.config.FetchPanNosrchDetails + employeeId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&searchTerm=' + searchTerm + '&roleId=' + roleId + '&flag=' + flag1);
    //}
    ChangeSr.prototype.ForwardToCheckerPanNoUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerPanNoUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //HRA City Class
    ChangeSr.prototype.InsertHraccFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertHraccFormDetails + "/InsertHraccFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetHraccFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetHraccFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    //FetchHraccsrchDetails(employeeId, pageNumber, pageSize, searchTerm, roleId, flag1): Observable<any> {
    //  return this.http.get(this.config.api_base_url + this.config.FetchHraccsrchDetails + employeeId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&searchTerm=' + searchTerm + '&roleId=' + roleId + '&flag=' + flag1);
    //}
    ChangeSr.prototype.ForwardToCheckerHraccUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerHraccUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //designation
    ChangeSr.prototype.InsertdesignationFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertdesignationFormDetails + "/InsertdesignationFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetdesignationFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetdesignationFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerdesignationUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerdesignationUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    ////Exservicemen
    ChangeSr.prototype.InsertexservicemenFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertexservicemenFormDetails + "/InsertexservicemenFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetexservicemenFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetexservicemenFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerexservicemenUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerexservicemenUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //cghs
    ChangeSr.prototype.InsertCGHSFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertCGHSFormDetails + "/InsertCGHSFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetCGHSFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetCGHSFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerCGHSUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCGHSUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //CGEGIS
    ChangeSr.prototype.InsertCGEGISFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertCGEGISFormDetails + "/InsertCGEGISFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetCGEGISFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetCGEGISFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerCGEGISFormUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCGEGISFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //joiningmode
    ChangeSr.prototype.InsertJoiningModeFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertJoiningModeFormDetails + "/InsertJoiningModeFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetJoiningModeFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetJoiningModeFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerJoiningModeFormUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerJoiningModeFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //DOJ
    ChangeSr.prototype.InsertDOJDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertDOJDetails + "/InsertDOJDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetDOJDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDOJDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerDOJUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOJUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //DOE
    ChangeSr.prototype.InsertDOEDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertDOEDetails + "/InsertDOEDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetDOEDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDOEDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerDOEUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOEUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //DOR
    ChangeSr.prototype.InsertDORDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertDORDetails + "/InsertDORDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetDORDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDORDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerDORUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDORUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //CasteCategory
    ChangeSr.prototype.InsertCasteCategoryFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertCasteCategoryFormDetails + "/InsertCasteCategoryFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetCasteCategoryFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetCasteCategoryFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerCasteCategoryFormUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCasteCategoryFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //Entofficevehicle
    ChangeSr.prototype.InsertEntofficevehicleFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertEntofficevehicleFormDetails + "/InsertEntofficevehicleFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetEntofficevehicleFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetEntofficevehicleFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerEntofficevehicleFormUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerEntofficevehicleFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //bank
    ChangeSr.prototype.InsertBankFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertBankFormDetails + "/InsertBankFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetBankFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetBankFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerBankFormUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerBankFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //addtapd
    ChangeSr.prototype.InsertaddtapdFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertaddtapdFormDetails + "/InsertaddtapdFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetaddtapdFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetaddtapdFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckeraddtapdUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckeraddtapdUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //TPTAForm
    ChangeSr.prototype.InsertTPTAFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertTPTAFormDetails + "/InsertTPTAFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetTPTAFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetTPTAFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerTPTAUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerTPTAUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //StateGIS
    ChangeSr.prototype.InsertStateGISFormDetails = function (objPro, Flag) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertStateGISFormDetails + "/InsertStateGISFormDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetStateGISFormDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetStateGISFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    ChangeSr.prototype.ForwardToCheckerStateGISUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerStateGISUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //MobileNo_Email_EmpCode
    ChangeSr.prototype.InsertMobileNoEmailEmpCodeDetails = function (objPro, flag, Contactdetails) {
        debugger;
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('flag', flag).set('Contactdetails', Contactdetails);
        return this.http.post("" + this.config.api_base_url + this.config.InsertMobileNoEmailEmpCodeDetails + "/InsertMobileNoEmailEmpCodeDetails", objPro, { responseType: 'text', params: parameter });
    };
    ChangeSr.prototype.GetMobileNoEmailEmpCodeDetails = function (EmpCode, roleId, Flag, ContactdetailsSelectedOption) {
        return this.http.get(this.config.api_base_url + this.config.GetMobileNoEmailEmpCodeDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag + '&ContactdetailsSelectedOption=' + ContactdetailsSelectedOption);
    };
    ChangeSr.prototype.ForwardToCheckerMobileNoEmailEmpCodeUserDtls = function (empCd, orderNo, status, rejectionRemark, ContactdetailsSelectedOption) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerMobileNoEmailEmpCodeUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark + '&ContactdetailsSelectedOption=' + ContactdetailsSelectedOption, { responseType: 'text' });
    };
    ChangeSr = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ChangeSr);
    return ChangeSr;
}());



/***/ }),

/***/ "./src/app/services/Change/dateofbirth.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/Change/dateofbirth.service.ts ***!
  \********************************************************/
/*! exports provided: DateofbirthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateofbirthService", function() { return DateofbirthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DateofbirthService = /** @class */ (function () {
    function DateofbirthService(http, config) {
        this.http = http;
        this.config = config;
    }
    DateofbirthService.prototype.InsertDOBDetails = function (objPro, Flag) {
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.InsertDOBDetails + "/InsertDOBDetails", objPro, { responseType: 'text', params: parameter });
    };
    DateofbirthService.prototype.GetDOBDetails = function (EmpCode, roleId, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDOBDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
    };
    DateofbirthService.prototype.ForwardToCheckerDOBUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOBUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //common delete using flag
    DateofbirthService.prototype.ChangeDeleteDetails = function (empCd, orderNo, flag) {
        return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag);
    };
    DateofbirthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], DateofbirthService);
    return DateofbirthService;
}());



/***/ }),

/***/ "./src/app/services/Change/extensionofservice.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/Change/extensionofservice.service.ts ***!
  \***************************************************************/
/*! exports provided: ExtensionofService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionofService", function() { return ExtensionofService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ExtensionofService = /** @class */ (function () {
    function ExtensionofService(http, config) {
        this.http = http;
        this.config = config;
    }
    //common delete using flag
    ExtensionofService.prototype.ChangeDeleteDetails = function (empCd, orderNo, flag) {
        return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag);
    };
    //Getddlvalue
    ExtensionofService.prototype.GetDdlvalue = function (Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetDdlvalue + Flag);
    };
    ExtensionofService.prototype.InsertEOSDetails = function (objPro, Flag) {
        var parameter = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.EOSControllerName + "/InsertEOSDetails", objPro, { responseType: 'text', params: parameter });
    };
    ExtensionofService.prototype.GetExtensionOfServiceDetails = function (EmpCode, roleId) {
        return this.http.get(this.config.api_base_url + this.config.GetExtensionOfServiceDetails + EmpCode + '&roleId=' + roleId);
    };
    //fwdtochecker
    ExtensionofService.prototype.ForwardToCheckerUserDtls = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    ExtensionofService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ExtensionofService);
    return ExtensionofService;
}());



/***/ })

}]);
//# sourceMappingURL=change-change-module.js.map