(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/model/masters/PayscaleModel.ts":
/*!************************************************!*\
  !*** ./src/app/model/masters/PayscaleModel.ts ***!
  \************************************************/
/*! exports provided: PayscaleModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayscaleModel", function() { return PayscaleModel; });
var PayscaleModel = /** @class */ (function () {
    function PayscaleModel() {
    }
    return PayscaleModel;
}());



/***/ }),

/***/ "./src/app/services/PayBill/PayBillService.ts":
/*!****************************************************!*\
  !*** ./src/app/services/PayBill/PayBillService.ts ***!
  \****************************************************/
/*! exports provided: PayBillGroupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayBillGroupService", function() { return PayBillGroupService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PayBillGroupService = /** @class */ (function () {
    function PayBillGroupService(http, config) {
        this.http = http;
        this.config = config;
    }
    PayBillGroupService.prototype.BindAccountHeads = function (permDDoId) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.getAccountHeads + permDDoId));
    };
    PayBillGroupService.prototype.InsertUpdatepayBillGroup = function (objBillGroup) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.InsertUpdatePayBillGroup, objBillGroup, { responseType: 'text' });
    };
    //BindAccountHeads() {
    //  //return this.httpclient.get(`${this.config.api_base_url}/${this.config.getAccountHeads}`);
    //}
    PayBillGroupService.prototype.BindPayBillGroup = function (permDDoId) {
        return this.http.get("" + this.config.api_base_url + (this.config.BindPayBillGroup + permDDoId));
        //asdfsf
    };
    PayBillGroupService.prototype.GetPayBillGroupDetailsById = function (parentId) {
        debugger;
        return this.http
            .get("" + this.config.api_base_url + (this.config.GetPayBillGroupDetailsById + parentId));
    };
    PayBillGroupService.prototype.BindAttachEmployee = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetPayBillGroupDetailsById);
    };
    PayBillGroupService.prototype.getEmpAttachList = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetPayBillGroupEmpAttach);
    };
    PayBillGroupService.prototype.getEmpdEAttachList = function (BillGrpID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetPayBillGroupEmpDeAttach + BillGrpID));
    };
    PayBillGroupService.prototype.UpDatePayBillGroupId = function (empCode, BillGroupId) {
        return this.http.post(this.config.api_base_url + this.config.UpdatePayBillGroupIdByEmpCode + empCode + '&BillGroupId=' + BillGroupId, { responseType: 'text' });
    };
    PayBillGroupService.prototype.UpdatePayBillGroupNullbyEmpcode = function (empCode) {
        return this.http.post(this.config.api_base_url + this.config.updatePayBillGroupIdnullByEmpCode + empCode, { responseType: 'text' });
    };
    //amit
    PayBillGroupService.prototype.GetAccountHeadsOT1 = function (FinFromYr, FinToYr, Flag) {
        return this.http.get(this.config.api_base_url + this.config.getAccountHeadsOT1 + FinFromYr + '&FinToYr=' + FinToYr + '&Flag=' + Flag);
    };
    PayBillGroupService.prototype.GetAccountHeadOT1AttachList = function (DDOCode, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetAccountHeadOT1AttachList + DDOCode + '&Flag=' + Flag);
    };
    PayBillGroupService.prototype.UpdateaccountHeadOT1 = function (accountHeadvalue, DDOCode, Flag) {
        return this.http.post(this.config.api_base_url + this.config.UpdateaccountHeadOT1 + accountHeadvalue + '&DDOCode=' + DDOCode + '&Flag=' + Flag, { responseType: 'text' });
    };
    PayBillGroupService.prototype.GetAccountHeadOT1PaybillGroupStatus = function (DDOCode, Flag) {
        return this.http.get(this.config.api_base_url + this.config.GetAccountHeadOT1PaybillGroupStatus + DDOCode + '&Flag=' + Flag);
    };
    PayBillGroupService.prototype.UpdateAccountHeadOT1PaybillGroupStatus = function (accountHeadvalue, DDOCode, Flag) {
        return this.http.post(this.config.api_base_url + this.config.UpdateAccountHeadOT1PaybillGroupStatus + accountHeadvalue + '&DDOCode=' + DDOCode + '&Flag=' + Flag, { responseType: 'text' });
    };
    PayBillGroupService.prototype.GetNgRecovery = function (permDDoId) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.getNgRecovery + permDDoId));
    };
    PayBillGroupService.prototype.GetBankdetailsByIfsc = function (ifscCode) {
        debugger;
        return this.http
            .get("" + this.config.api_base_url + (this.config.GetBankDetailsByIfscCode + ifscCode));
    };
    PayBillGroupService.prototype.InsertUpdateNgRecovery = function (objNgRecovery) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.insertUpdateNgRecovery, objNgRecovery, { responseType: 'text' });
    };
    PayBillGroupService.prototype.ActiveDeactiveNgRecovery = function (objNgRecovery) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.activeDeactiveNgRecovery, objNgRecovery, { responseType: 'text' });
    };
    PayBillGroupService.prototype.GetFinancialYear = function () {
        debugger;
        return this.http.get(this.config.api_base_url + this.config.getfinancialYear);
    };
    PayBillGroupService.prototype.GetPayBillGroupByFinancialYear = function (finYearFrom, finYearTo, permDdoId) {
        return this.http.get(this.config.api_base_url + "PayBill/GetPayBillGroupByFinancialYear?FinYearFrom=" + finYearFrom + "&FinYearTo=" + finYearTo + "&PermDdoId=" + permDdoId);
    };
    PayBillGroupService.prototype.GetEmployeesByPayItemCode = function (PayItemCd) {
        return this.http.get(this.config.api_base_url + "PayBill/GetEmployeesByPayItemCode?PayItemCd=" + PayItemCd);
    };
    PayBillGroupService.prototype.GetPayItemsFromDesignationCode = function (DesigCode) {
        return this.http.get(this.config.api_base_url + "PayBill/GetPayItemsFromDesignationCode?DesigCode=" + DesigCode);
    };
    PayBillGroupService.prototype.UpdateEmpNonComputationalDuesDeductAmount = function (obj) {
        return this.http.post(this.config.api_base_url + "PayBill/UpdateEmpNonComputationalDuesDeductAmount", obj, { responseType: 'text' });
    };
    PayBillGroupService.prototype.GetDesignationByBillgrID = function (billGroupId, PermDdoId) {
        return this.http.get(this.config.api_base_url + this.config.SharedControllerName + "/GetDesignationByBillgrID?billGroupId=" + billGroupId + "&PermDdoId=" + PermDdoId);
    };
    PayBillGroupService.prototype.GetAllMonth = function () {
        debugger;
        return this.http.get(this.config.api_base_url + this.config.getAllMonth);
    };
    PayBillGroupService.prototype.GetEmployeeByPaybillAndDesig = function (PermDDOId, PayBillGroupId, DesigCd) {
        debugger;
        return this.http.get(this.config.api_base_url + this.config.GetEmployeeByPaybillAndDesig + '?PermDDOId=' + PermDDOId + '&PayBillGroupId=' + PayBillGroupId + '&DesigCd=' + DesigCd);
    };
    PayBillGroupService.prototype.GetNgRecoveryList = function (PermDDOId) {
        debugger;
        return this.http.get(this.config.api_base_url + this.config.GetNgRecoveryList + '?PermDDOId=' + PermDDOId);
    };
    PayBillGroupService.prototype.InsertNgrecoveryEntry = function (ngEntryList) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.InsertNgrecoveryEntry, ngEntryList, { responseType: 'text' });
    };
    PayBillGroupService.prototype.GetAllNgRecoveryandPaybillGrpDetails = function (permDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('permDdoId', permDdoId);
        return this.http.get(this.config.api_base_url + this.config.GetAllNgRecoveryandPaybillGrpDetails, { params: params });
    };
    PayBillGroupService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PayBillGroupService);
    return PayBillGroupService;
}());



/***/ }),

/***/ "./src/app/services/Recovery/recovery.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/Recovery/recovery.service.ts ***!
  \*******************************************************/
/*! exports provided: recoveryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recoveryService", function() { return recoveryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var recoveryService = /** @class */ (function () {
    function recoveryService(http, config) {
        this.http = http;
        this.config = config;
    }
    recoveryService.prototype.GetAllPayBillGroup = function (PermDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('permddoId', PermDdoId);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetPayBillGroup", { params: params });
    };
    recoveryService.prototype.GetAllDesignation = function (PayBillGrpId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('billGroupId', PayBillGrpId);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignation", { params: params });
    };
    recoveryService.prototype.GetAllEmp = function (DesigCode) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('desigId', DesigCode).set('pageCode', 'Leaves');
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmp", { params: params });
    };
    recoveryService.prototype.getTempPaymentRecovery = function () {
        return this.http.get("" + this.config.getTempPaymentRecovery);
    };
    recoveryService.prototype.SaveTempPayment = function (ObjTempData) {
        return this.http.post(this.config.SaveTempPayment, ObjTempData, { responseType: 'text' });
    };
    recoveryService.prototype.SaveExcessRecovery = function (ObjTempData) {
        return this.http.post(this.config.SaveRecoveryExcess, ObjTempData, { responseType: 'text' });
    };
    recoveryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], recoveryService);
    return recoveryService;
}());



/***/ }),

/***/ "./src/app/services/empdetails/posting-details.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/empdetails/posting-details.service.ts ***!
  \****************************************************************/
/*! exports provided: PostingDetailsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostingDetailsService", function() { return PostingDetailsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PostingDetailsService = /** @class */ (function () {
    function PostingDetailsService(http, config) {
        this.http = http;
        this.config = config;
    }
    PostingDetailsService.prototype.GetHRACity = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetHRACity);
    };
    PostingDetailsService.prototype.GetTACity = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetTACity);
    };
    PostingDetailsService.prototype.GetAllDesignation = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetAllDesignation);
    };
    PostingDetailsService.prototype.SaveUpdatePostingDetails = function (objPostingDetails) {
        return this.http.post(this.config.api_base_url + this.config.SaveUpdatePostingDetails, objPostingDetails, { responseType: 'text' });
    };
    PostingDetailsService.prototype.GetAllPostingDetails = function (empcode, roleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCd', empcode).set('roleId', roleId);
        return this.http.get("" + this.config.api_base_url + this.config.GetAllPostingDetails, { params: params });
    };
    PostingDetailsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PostingDetailsService);
    return PostingDetailsService;
}());



/***/ }),

/***/ "./src/app/services/payscale.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/payscale.service.ts ***!
  \**********************************************/
/*! exports provided: PayscaleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayscaleService", function() { return PayscaleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PayscaleService = /** @class */ (function () {
    //BaseUrl: any = [];
    function PayscaleService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        //this.BaseUrl = 'http://localhost:55424/api/';
    }
    PayscaleService.prototype.BindCommissionCode = function () {
        return this.httpclient
            .get("" + this.config.BindCommissionCode);
    };
    PayscaleService.prototype.BindPayScaleFormat = function () {
        return this.httpclient
            .get("" + this.config.BindPayScaleFormat);
    };
    PayscaleService.prototype.BindPayScaleGroup = function () {
        return this.httpclient.get("" + this.config.BindPayScaleGroup);
    };
    PayscaleService.prototype.GetPayScaleDetails = function () {
        //debugger;
        return this.httpclient.get("" + this.config.GetPayScaleDetails);
        // return this.httpclient.get<any>('${this.config.GetPayScaleDetails, {}}');
        //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/GetPayScaleDetails', {});
    };
    PayscaleService.prototype.EditPayScaleDetails = function (msPayScaleID) {
        return this.httpclient.get(this.config.EditPayScaleDetails + msPayScaleID, {});
        //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/EditPayScaleDetails?msPayScaleID=' + msPayScaleID, {});
    };
    PayscaleService.prototype.GetPayScaleDetailsBYComCode = function (CommCDID) {
        return this.httpclient.get(this.config.GetPayScaleDetailsBYComCode + CommCDID, {});
        //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/GetPayScaleDetailsBYComCode?CommCDID=' + CommCDID, {});
    };
    PayscaleService.prototype.GetPayScaleCode = function (CddirCodeValue) {
        return this.httpclient.get(this.config.GetPayScaleCode + CddirCodeValue, {});
        // return this.httpclient.get<any>(this.config.api_base_url + 'Payscale/GetPayScaleCode?CddirCodeValue=' + CddirCodeValue, {});
    };
    PayscaleService.prototype.Delete = function (msPayScaleID) {
        return this.httpclient.post(this.config.Delete + msPayScaleID, '', { responseType: 'text' });
        // return this.httpclient.post(this.config.api_base_url + 'Payscale/Delete?msPayScaleID='+ msPayScaleID,'', { responseType: 'text'});
    };
    PayscaleService.prototype.CreatePayScale = function (_pScaleObj) {
        return this.httpclient.post(this.config.CreatePayScale, _pScaleObj, { responseType: 'text' });
    };
    PayscaleService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PayscaleService);
    return PayscaleService;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map