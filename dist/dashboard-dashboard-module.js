(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/filter.js ***!
  \***************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/filter */ "./node_modules/rxjs-compat/_esm5/operator/filter.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.filter = _operator_filter__WEBPACK_IMPORTED_MODULE_1__["filter"];
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/filter.js":
/*!***********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/filter.js ***!
  \***********************************************************/
/*! exports provided: filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filter", function() { return filter; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

function filter(predicate, thisArg) {
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["filter"])(predicate, thisArg)(this);
}
//# sourceMappingURL=filter.js.map

/***/ }),

/***/ "./src/app/dashboard/chart/chart.component.css":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/chart/chart.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9jaGFydC9jaGFydC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dashboard/chart/chart.component.html":
/*!******************************************************!*\
  !*** ./src/app/dashboard/chart/chart.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <div style=\"display: block\" class=\"graph-body\">\r\n    <canvas baseChart\r\n            [datasets]=\"barChartData\"\r\n            [labels]=\"barChartLabels\"\r\n            [options]=\"barChartOptions\"\r\n            [legend]=\"barChartLegend\"\r\n            [chartType]=\"barChartType\"\r\n            (chartHover)=\"chartHovered($event)\"\r\n            (chartClick)=\"chartClicked($event)\"></canvas>\r\n  </div>\r\n  <!-- <button (click)=\"randomize()\">Update</button> -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/chart/chart.component.ts":
/*!****************************************************!*\
  !*** ./src/app/dashboard/chart/chart.component.ts ***!
  \****************************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ChartComponent = /** @class */ (function () {
    function ChartComponent() {
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
        ];
    }
    // events
    ChartComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    ChartComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    ChartComponent.prototype.randomize = function () {
        // Only Change 3 values
        var data = [
            Math.round(Math.random() * 100),
            59,
            80,
            (Math.random() * 100),
            56,
            (Math.random() * 100),
            40
        ];
        var clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        this.barChartData = clone;
        /**
         * (My guess), for Angular to recognize the change in the dataset
         * it has to change the dataset variable directly,
         * so one way around it, is to clone the data, change it and then
         * assign it;
         */
    };
    ChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! ./chart.component.html */ "./src/app/dashboard/chart/chart.component.html"),
            styles: [__webpack_require__(/*! ./chart.component.css */ "./src/app/dashboard/chart/chart.component.css")]
        })
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/dashboard/chart/chart.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pagenotfound/pagenotfound.component */ "./src/app/dashboard/pagenotfound/pagenotfound.component.ts");
/* harmony import */ var _underdevelopment_underdevelopment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./underdevelopment/underdevelopment.component */ "./src/app/dashboard/underdevelopment/underdevelopment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"], children: [
            { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
            { path: 'chart', component: _chart_chart_component__WEBPACK_IMPORTED_MODULE_2__["ChartComponent"] },
            {
                path: 'role', loadChildren: '../role-managment/role-managment.module#RoleManagmentModule'
            },
            // { path: 'test', loadChildren: '../master-mgmt/master-mgmt.module#MasterMgmtModule' },
            { path: 'onboarding', loadChildren: '../onboarding/onboarding.module#OnboardingModule' },
            { path: 'payroll', loadChildren: '../payroll/payroll.module#PayrollModule' },
            { path: 'loanmgt', loadChildren: '../loanmgt/loanmgt.module#LoanmgtModule' },
            { path: 'leavemgt', loadChildren: '../leaves-mgmt/leaves-mgmt.module#LeavesMgmtModule' },
            { path: 'employee', loadChildren: '../employee/employee.module#EmployeeModule' },
            { path: 'billGroup', loadChildren: '../bill-group-mgmt/bill-group-mgmt.module#BillGroupMgmtModule' },
            { path: 'master', loadChildren: '../masters/masters.module#MastersModule' },
            { path: 'suspension', loadChildren: '../suspension/suspension.module#SuspensionModule' },
            { path: 'commondesign', loadChildren: '../commondesign/commondesign.module#CommondesignModule' },
            { path: 'demodesign', loadChildren: '../commondesign/commondesign.module#CommondesignModule' },
            { path: 'promotion', loadChildren: '../promotion/promotion.module#PromotionModule' },
            { path: 'incometax', loadChildren: '../income-tax/income-tax.module#IncomeTaxModule' },
            //{ path: 'user', loadChildren: '../usermanagement/usermanagement.module#UsermanagementModule' },
            { path: 'recovery', loadChildren: '../recovery-excess-payment/recovery-excess-payment.module#RecoveryExcessPaymentModule' },
            { path: 'recoveryNonEPS', loadChildren: '../recovery-non-epspayment/recovery-non-epspayment.module#RecoveryNonEPSPaymentModule' },
            { path: 'deputation', loadChildren: '../deputation/deputation.module#DeputationModule' },
            { path: 'increment', loadChildren: '../increment/increment.module#IncrementModule' },
            { path: 'lienperiod', loadChildren: '../lienperiod/lienperiod.module#LienperiodModule' },
            { path: 'change', loadChildren: '../change/change.module#ChangeModule' },
            { path: 'profile', loadChildren: '../myprofile/myprofile.module#MyprofileModule' },
            { path: 'reports', loadChildren: '../reports/reports.module#ReportsModule' },
            { path: 'rejoiningofemployee', loadChildren: '../rejoiningofemployee/rejoiningofemployee.module#RejoiningofemployeeModule' },
            { path: 'endservice', loadChildren: '../endofservice/endofservice.module#EndofserviceModule' },
            { path: 'page', component: _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_5__["PagenotfoundComponent"] },
            { path: 'underdevelopment', component: _underdevelopment_underdevelopment_component__WEBPACK_IMPORTED_MODULE_6__["UnderdevelopmentComponent"] },
            { path: '**', redirectTo: 'page', pathMatch: 'full' },
        ]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/dashboard/chart/chart.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/header.component */ "./src/app/dashboard/header/header.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var _nav_menu_nav_menu_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./nav-menu/nav-menu.component */ "./src/app/dashboard/nav-menu/nav-menu.component.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _services_nav_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/nav.service */ "./src/app/services/nav.service.ts");
/* harmony import */ var _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pagenotfound/pagenotfound.component */ "./src/app/dashboard/pagenotfound/pagenotfound.component.ts");
/* harmony import */ var _underdevelopment_underdevelopment_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./underdevelopment/underdevelopment.component */ "./src/app/dashboard/underdevelopment/underdevelopment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_chart_chart_component__WEBPACK_IMPORTED_MODULE_3__["ChartComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"], _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"], _nav_menu_nav_menu_component__WEBPACK_IMPORTED_MODULE_8__["NavMenuComponent"], _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_11__["PagenotfoundComponent"], _underdevelopment_underdevelopment_component__WEBPACK_IMPORTED_MODULE_12__["UnderdevelopmentComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__["DashboardRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            providers: [_services_nav_service__WEBPACK_IMPORTED_MODULE_10__["NavService"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/header/header.component.css":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/header/header.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user { float: right; width: 20%; padding: 16px 16px 0 0;}\r\n.user .photo {float: right;margin-right:15px;}\r\n.user .photo img{border-radius: 50%;height: 50px;width: 50px;box-shadow: 0px 0px 28px -1px rgba(0,0,0,0.75);}\r\n.user .info {float: right;padding-top: 13px;}\r\n.user .info a{color: #fff;}\r\nbutton.user-profile.mat-icon-button { color: #fff;  float: right; width: auto;margin-top: 6px;}\r\n.cdk-overlay-connected-position-bounding-box{top: 97px; left: 1758.53px;}\r\n\r\n        \r\n            \r\n\r\n \r\n\r\n\r\n\r\n            \r\n        \r\n\r\n        \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxRQUFRLGFBQWEsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUM7QUFDMUQsY0FBYyxhQUFhLGtCQUFrQixDQUFDO0FBQzlDLGlCQUFpQixtQkFBbUIsYUFBYSxZQUFZLCtDQUErQyxDQUFDO0FBQzdHLGFBQWEsYUFBYSxrQkFBa0IsQ0FBQztBQUM3QyxjQUFjLFlBQVksQ0FBQztBQUUzQixzQ0FBc0MsWUFBWSxFQUFFLGFBQWEsQ0FBQyxZQUFZLGdCQUFnQixDQUFDO0FBQy9GLDZDQUE2QyxVQUFVLENBQUMsZ0JBQWdCLENBQUMiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXIgeyBmbG9hdDogcmlnaHQ7IHdpZHRoOiAyMCU7IHBhZGRpbmc6IDE2cHggMTZweCAwIDA7fVxyXG4udXNlciAucGhvdG8ge2Zsb2F0OiByaWdodDttYXJnaW4tcmlnaHQ6MTVweDt9XHJcbi51c2VyIC5waG90byBpbWd7Ym9yZGVyLXJhZGl1czogNTAlO2hlaWdodDogNTBweDt3aWR0aDogNTBweDtib3gtc2hhZG93OiAwcHggMHB4IDI4cHggLTFweCByZ2JhKDAsMCwwLDAuNzUpO31cclxuLnVzZXIgLmluZm8ge2Zsb2F0OiByaWdodDtwYWRkaW5nLXRvcDogMTNweDt9XHJcbi51c2VyIC5pbmZvIGF7Y29sb3I6ICNmZmY7fVxyXG5cclxuYnV0dG9uLnVzZXItcHJvZmlsZS5tYXQtaWNvbi1idXR0b24geyBjb2xvcjogI2ZmZjsgIGZsb2F0OiByaWdodDsgd2lkdGg6IGF1dG87bWFyZ2luLXRvcDogNnB4O31cclxuLmNkay1vdmVybGF5LWNvbm5lY3RlZC1wb3NpdGlvbi1ib3VuZGluZy1ib3h7dG9wOiA5N3B4OyBsZWZ0OiAxNzU4LjUzcHg7fVxyXG5cclxuICAgICAgICBcclxuICAgICAgICAgICAgXHJcblxyXG4gXHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIFxyXG5cclxuICAgICAgICAiXX0= */"

/***/ }),

/***/ "./src/app/dashboard/header/header.component.html":
/*!********************************************************!*\
  !*** ./src/app/dashboard/header/header.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid head-bg\">\r\n  \r\n  <div class=\"row\">         \r\n        <div class=\"logo\">\r\n        <img src=\"~/../assets/eps_logo.png\" />\r\n        <h1>Employee Payroll System</h1>\r\n        <span>Ministry of Finance, Government of India</span>\r\n      </div>\r\n      <div class=\"emblem \"><img src=\"~/../assets/emblem.png\"></div>\r\n\r\n      <div class=\"user\">\r\n        \r\n        <!-- <div class=\"info\">\r\n            <a class=\"collapsed\" data-toggle=\"collapse\" href=\"#collapseExample\">\r\n                <span>\r\n                    Tania Andrew\r\n                    <b class=\"caret\"></b>\r\n                </span>\r\n            </a>\r\n            <div class=\"collapse\" id=\"collapseExample\">\r\n                <ul class=\"nav\">\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\">\r\n                            <span class=\"sidebar-mini\">MP</span>\r\n                            <span class=\"sidebar-normal\">My Profile</span>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\">\r\n                            <span class=\"sidebar-mini\">EP</span>\r\n                            <span class=\"sidebar-normal\">Edit Profile</span>\r\n                        </a>\r\n                    </li>\r\n                    <li>\r\n                        <a href=\"javascript:void(0)\">\r\n                            <span class=\"sidebar-mini\">S</span>\r\n                            <span class=\"sidebar-normal\">Settings</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </div> -->\r\n       \r\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\" class=\"user-profile\">\r\n          <!--Employee Name-->Logged in as {{userRole}}\r\n          <!--{{username}}-->\r\n          <mat-icon>more_vert</mat-icon>\r\n        </button>\r\n        <mat-menu #menu=\"matMenu\">\r\n          <button mat-menu-item>\r\n            <mat-icon>assignment_ind</mat-icon>\r\n            <span>Profile</span>\r\n          </button>\r\n          <button mat-menu-item (click)=\"LogOut();\">\r\n            <mat-icon>play_for_work</mat-icon>\r\n            <span>Logout</span>\r\n          </button>\r\n        </mat-menu>\r\n        <div class=\"photo\">\r\n          <img src=\"~/../assets/emp.png\">\r\n      </div>\r\n    </div>\r\n\r\n    </div>\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/header/header.component.ts":
/*!******************************************************!*\
  !*** ./src/app/dashboard/header/header.component.ts ***!
  \******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, titleService) {
        this.router = router;
        this.titleService = titleService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.username = sessionStorage.getItem('username');
        this.userRole = sessionStorage.getItem('userRole');
    };
    HeaderComponent.prototype.LogOut = function () {
        sessionStorage.clear();
        this.titleService.setTitle('EPS');
        //alert('LogOut');
        this.router.navigate(['/login']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/dashboard/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/dashboard/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/home/home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content\">\r\n  <div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n        <div class=\"card card-stats\">\r\n          <div class=\"card-header card-header-warning card-header-icon\">\r\n            <div class=\"card-icon\">\r\n              <i class=\"material-icons\">content_copy</i>\r\n            </div>\r\n            <p class=\"card-category\">Total User-{{dashboard.totalUser}}</p>\r\n            <h3 class=\"card-title\">\r\n              {{dashboard.activeUser}}/{{dashboard.totalUser - dashboard.activeUser}}\r\n              <small></small>\r\n            </h3>\r\n          </div>\r\n          <div class=\"card-footer\">\r\n            <div class=\"stats\">\r\n              <i class=\"material-icons text-danger\">warning</i>\r\n              <a href=\"#pablo\">Know More...</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n        <div class=\"card card-stats\">\r\n          <div class=\"card-header card-header-success card-header-icon\">\r\n            <div class=\"card-icon\">\r\n              <i class=\"material-icons\">store</i>\r\n            </div>\r\n            <p class=\"card-category\">Total DDO</p>\r\n            <h3 class=\"card-title\"> {{dashboard.totalDDO}}</h3>\r\n          </div>\r\n          <div class=\"card-footer\">\r\n            <div class=\"stats\">\r\n              <i class=\"material-icons\">date_range</i> Last 24 Hours\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n        <div class=\"card card-stats\">\r\n          <div class=\"card-header card-header-danger card-header-icon\">\r\n            <div class=\"card-icon\">\r\n              <i class=\"material-icons\">info_outline</i>\r\n            </div>\r\n            <p class=\"card-category\">Fixed Issues</p>\r\n            <h3 class=\"card-title\">75</h3>\r\n          </div>\r\n          <div class=\"card-footer\">\r\n            <div class=\"stats\">\r\n              <i class=\"material-icons\">local_offer</i> Tracked from Github\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n        <div class=\"card card-stats\">\r\n          <div class=\"card-header card-header-info card-header-icon\">\r\n            <div class=\"card-icon\">\r\n                <i class=\"material-icons\">account_balance</i>\r\n            </div>\r\n            <p class=\"card-category\">Followers</p>\r\n            <h3 class=\"card-title\">+245</h3>\r\n          </div>\r\n          <div class=\"card-footer\">\r\n            <div class=\"stats\">\r\n              <i class=\"material-icons\">update</i> Just Updated\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>  \r\n    <div class=\"row mt-40\">\r\n      <div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n        <app-chart></app-chart>\r\n      </div>\r\n    </div>       \r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/dashboard/dashboard.service */ "./src/app/services/dashboard/dashboard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(dashboardService) {
        this.dashboardService = dashboardService;
        this.dashboard = {};
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.getDashboardDetail();
    };
    HomeComponent.prototype.getDashboardDetail = function () {
        var _this = this;
        //  alert(sessionStorage.getItem('UserID'));
        this.dashboardService.getDashboardDetail().subscribe(function (res) {
            _this.dashboard = res[0];
        });
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/dashboard/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/dashboard/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_1__["DashboardService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/nav-menu/nav-menu.component.html":
/*!************************************************************!*\
  !*** ./src/app/dashboard/nav-menu/nav-menu.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<a mat-list-item [ngStyle]=\"{'padding-left': (depth * 12) + 'px'}\" (click)=\"onItemSelected(item);generateNumber(item)\"\r\n   [ngClass]=\"{'active': item.route ? router.isActive(item.route, true): false, 'expanded': expanded }\" class=\"menu-list-item \" >\r\n  <mat-icon>{{item.iconName}}</mat-icon>\r\n  {{item.displayName}}\r\n  <span fxFlex *ngIf=\"item.children && item.children.length\">\r\n    <span fxFlex></span>\r\n    <mat-icon [@indicatorRotate]=\"expanded ? 'expanded': 'collapsed'\" class=\"dropdowon_icon\">\r\n      expand_more\r\n    </mat-icon>\r\n  </span>\r\n</a>\r\n<div *ngIf=\"expanded\">\r\n  <app-nav-menu *ngFor=\"let child of item.children;\" [item]=\"child\" [depth]=\"depth+1\"  (click)=\"generateNumber(child)\">\r\n  </app-nav-menu>\r\n</div>\r\n\r\n<!--<div [ngStyle]=\"textString.length > 20 && {'font-weight': 'bold', 'font-style': 'italic'}\">{{textString}}</div>-->\r\n"

/***/ }),

/***/ "./src/app/dashboard/nav-menu/nav-menu.component.scss":
/*!************************************************************!*\
  !*** ./src/app/dashboard/nav-menu/nav-menu.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*:host {\r\n  display: flex;\r\n  flex-direction: column;\r\n  outline: none;\r\n  width: 100%;\r\n\r\n  .mat-list-item.active {\r\n    background-color: #d9d9d9;\r\n  }\r\n\r\n  &:hover,\r\n  &:focus {\r\n    > .mat-list-item:not(.expanded) {\r\n      background-color: #d9d9d9 !important;\r\n      z-index: 9;\r\n    }\r\n  }\r\n}\r\n  \r\n  .mat-list-item {\r\n  \r\n    display: flex;\r\n    width: auto;\r\n  \r\n    .routeIcon {\r\n      margin-right: 40px;\r\n    }\r\n  }*/\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL25hdi1tZW51L006XFxnaXRFUFNcXGVwc2FwcFxcRVBTXFxFUFMuV2ViQXBwXFxDbGllbnRBcHAvc3JjXFxhcHBcXGRhc2hib2FyZFxcbmF2LW1lbnVcXG5hdi1tZW51LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0EyQksiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvbmF2LW1lbnUvbmF2LW1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKjpob3N0IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICB3aWR0aDogMTAwJTtcclxuXHJcbiAgLm1hdC1saXN0LWl0ZW0uYWN0aXZlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkOWQ5ZDk7XHJcbiAgfVxyXG5cclxuICAmOmhvdmVyLFxyXG4gICY6Zm9jdXMge1xyXG4gICAgPiAubWF0LWxpc3QtaXRlbTpub3QoLmV4cGFuZGVkKSB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkOWQ5ZDkgIWltcG9ydGFudDtcclxuICAgICAgei1pbmRleDogOTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuICBcclxuICAubWF0LWxpc3QtaXRlbSB7XHJcbiAgXHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgXHJcbiAgICAucm91dGVJY29uIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG4gIH0qL1xyXG5cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/dashboard/nav-menu/nav-menu.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/dashboard/nav-menu/nav-menu.component.ts ***!
  \**********************************************************/
/*! exports provided: NavMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavMenuComponent", function() { return NavMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_nav_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/nav.service */ "./src/app/services/nav.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../dashboard/dashboard.component */ "./src/app/dashboard/dashboard/dashboard.component.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavMenuComponent = /** @class */ (function () {
    function NavMenuComponent(navService, objDash, titleService, router) {
        this.navService = navService;
        this.objDash = objDash;
        this.titleService = titleService;
        this.router = router;
        this.ariaExpanded = this.expanded;
        this.numberGenerated = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.names = {};
        if (this.depth === undefined) {
            this.depth = 0;
        }
        localStorage.setItem('chk', 'false');
        //this.generateNumber(this.item);
    }
    NavMenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.titleService.setTitle('Home' + ':' + 'EPS');
        this.navService.currentUrl.subscribe(function (url) {
            if (localStorage.getItem('chk') == "false") {
                if (_this.item.route && url) {
                    // console.log(`Checking '/${this.item.route}' against '${url}'`);
                    _this.expanded = url.indexOf("/" + _this.item.route) === 0;
                    _this.ariaExpanded = _this.expanded;
                    // console.log(`${this.item.route} is expanded: ${this.expanded}`);
                }
            }
        });
    };
    NavMenuComponent.prototype.onItemSelected = function (item) {
        var _this = this;
        //this.generateNumber(item);
        if (!item.children || !item.children.length) {
            localStorage.setItem('chk', 'true');
            this.router.navigate([item.route]);
            // If same link is clicked again, then it reloads the component. ---begin
            var childrens = item.route.split('/');
            var currentUrl = this.router.url.split('/');
            if (childrens[childrens.length - 1] == currentUrl[currentUrl.length - 1]) {
                this.router.navigateByUrl('/', { skipLocationChange: true }).then(function () {
                    return _this.router.navigate([item.route]);
                });
            }
            // If same link is clicked again, then it reloads the component. ---end
            //this.navService.closeNav();
        }
        if (item.children && item.children.length) {
            this.expanded = !this.expanded;
        }
        //if (item !== undefined) {
        //  this.names.displayName = item.displayName;
        //  this.names.route = item.route
        //  // const randomNumber = Math.random();
        //  this.numberGenerated.emit(this.names);
        //}
    };
    NavMenuComponent.prototype.generateNumber = function (item) {
        // sessionStorage.setItem('menudisplayName',"");
        if (item.children.length === 0) {
            if (item !== undefined) {
                this.numberGenerated.emit(this.names);
                sessionStorage.setItem('menudisplayName', ' / ' + item.displayName);
                this.titleService.setTitle(item.displayName + ':' + 'EPS');
                sessionStorage.setItem('menuroute', item.route);
                this.objDash.onNumberGenerated();
            }
        }
        //this.numberGenerated.emit=item.displayName;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('attr.aria-expanded'),
        __metadata("design:type", Object)
    ], NavMenuComponent.prototype, "ariaExpanded", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NavMenuComponent.prototype, "item", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], NavMenuComponent.prototype, "depth", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], NavMenuComponent.prototype, "numberGenerated", void 0);
    NavMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav-menu',
            template: __webpack_require__(/*! ./nav-menu.component.html */ "./src/app/dashboard/nav-menu/nav-menu.component.html"),
            styles: [__webpack_require__(/*! ./nav-menu.component.scss */ "./src/app/dashboard/nav-menu/nav-menu.component.scss")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["trigger"])('indicatorRotate', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["state"])('collapsed', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'rotate(0deg)' })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["state"])('expanded', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'rotate(180deg)' })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])('expanded <=> collapsed', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('225ms cubic-bezier(0.4,0.0,0.2,1)')),
                ])
            ],
            providers: [_services_nav_service__WEBPACK_IMPORTED_MODULE_1__["NavService"]],
        }),
        __metadata("design:paramtypes", [_services_nav_service__WEBPACK_IMPORTED_MODULE_1__["NavService"], _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NavMenuComponent);
    return NavMenuComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/pagenotfound/pagenotfound.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/dashboard/pagenotfound/pagenotfound.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n#notfound {\r\n  position: relative;\r\n  height: 90%;\r\n  background:#000 !important;\r\n}\r\n\r\n#notfound .notfound {\r\n  position: absolute;\r\n  left: 50%;\r\n  top: 50%;\r\n  -webkit-transform: translate(-50%, -50%);\r\n          transform: translate(-50%, -50%);\r\n}\r\n\r\n.notfound {\r\n  max-width: 620px;\r\n  width: 100%;\r\n  line-height: 1.4;\r\n  text-align: center;\r\n}\r\n\r\n.notfound .notfound-404 {\r\n  position: relative;\r\n  height: 200px;\r\n  margin: 0px auto 20px;\r\n  z-index: -1;\r\n}\r\n\r\n.notfound .notfound-404 h1 {\r\n  font-family: 'Montserrat', sans-serif;\r\n  font-size: 170px;\r\n  font-weight: 200;\r\n  margin: 0px;\r\n  color: #232323;\r\n  text-transform: uppercase;\r\n  position: absolute;\r\n  left: 50%;\r\n  top: 50%;\r\n  -webkit-transform: translate(-50%, -50%);\r\n          transform: translate(-50%, -50%);\r\n}\r\n\r\n.notfound .notfound-404 h2 {\r\n  font-family: 'Montserrat', sans-serif;\r\n  font-size: 23px;\r\n  font-weight: 400;\r\n  text-transform: uppercase;\r\n  color: #ff6300;\r\n  background: #000;\r\n  padding: 18px 5px;\r\n  margin: auto;\r\n  display: inline-block;\r\n  position: absolute;\r\n  bottom: 0px;\r\n  left: 0;\r\n  right: 0;\r\n  opacity:0.7;\r\n}\r\n\r\n.notfound a {\r\n  font-family: 'Montserrat', sans-serif;\r\n  display: inline-block;\r\n  font-weight: 700;\r\n  text-decoration: none;\r\n  color: #979797;\r\n  text-transform: uppercase;\r\n  padding: 13px 23px;\r\n  background: #211b19;\r\n  font-size: 11px;\r\n  transition: 0.2s all;\r\n}\r\n\r\n.notfound a:hover {\r\n  color: #979797;\r\n  background: #454140;\r\n}\r\n\r\n@media only screen and (max-width: 767px) {\r\n  .notfound .notfound-404 h1 {\r\n    font-size: 148px;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 480px) {\r\n  .notfound .notfound-404 {\r\n    height: 148px;\r\n    margin: 0px auto 10px;\r\n  }\r\n  .notfound .notfound-404 h1 {\r\n    font-size: 86px;\r\n  }\r\n  .notfound .notfound-404 h2 {\r\n    font-size: 16px;\r\n  }\r\n  .notfound a {\r\n    padding: 7px 15px;\r\n    font-size: 14px;\r\n  }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3BhZ2Vub3Rmb3VuZC9wYWdlbm90Zm91bmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWiwyQkFBMkI7Q0FDNUI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFNBQVM7RUFDVCx5Q0FBeUM7VUFFakMsaUNBQWlDO0NBQzFDOztBQUNEO0VBQ0UsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsWUFBWTtDQUNiOztBQUVEO0VBQ0Usc0NBQXNDO0VBQ3RDLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGVBQWU7RUFDZiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixTQUFTO0VBQ1QseUNBQXlDO1VBRWpDLGlDQUFpQztDQUMxQzs7QUFFRDtFQUNFLHNDQUFzQztFQUN0QyxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osUUFBUTtFQUNSLFNBQVM7RUFDVCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxzQ0FBc0M7RUFDdEMsc0JBQXNCO0VBQ3RCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsZUFBZTtFQUNmLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUVoQixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2Ysb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0U7SUFDRSxpQkFBaUI7R0FDbEI7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsY0FBYztJQUNkLHNCQUFzQjtHQUN2QjtFQUNEO0lBQ0UsZ0JBQWdCO0dBQ2pCO0VBQ0Q7SUFDRSxnQkFBZ0I7R0FDakI7RUFDRDtJQUNFLGtCQUFrQjtJQUNsQixnQkFBZ0I7R0FDakI7Q0FDRiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wYWdlbm90Zm91bmQvcGFnZW5vdGZvdW5kLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbiNub3Rmb3VuZCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGhlaWdodDogOTAlO1xyXG4gIGJhY2tncm91bmQ6IzAwMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4jbm90Zm91bmQgLm5vdGZvdW5kIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIHRvcDogNTAlO1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG59XHJcbi5ub3Rmb3VuZCB7XHJcbiAgbWF4LXdpZHRoOiA2MjBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBsaW5lLWhlaWdodDogMS40O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIG1hcmdpbjogMHB4IGF1dG8gMjBweDtcclxuICB6LWluZGV4OiAtMTtcclxufVxyXG5cclxuLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDEge1xyXG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxNzBweDtcclxuICBmb250LXdlaWdodDogMjAwO1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIGNvbG9yOiAjMjMyMzIzO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDUwJTtcclxuICB0b3A6IDUwJTtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxufVxyXG5cclxuLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDIge1xyXG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAyM3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBjb2xvcjogI2ZmNjMwMDtcclxuICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gIHBhZGRpbmc6IDE4cHggNXB4O1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgb3BhY2l0eTowLjc7XHJcbn1cclxuXHJcbi5ub3Rmb3VuZCBhIHtcclxuICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnLCBzYW5zLXNlcmlmO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBjb2xvcjogIzk3OTc5NztcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIHBhZGRpbmc6IDEzcHggMjNweDtcclxuICBiYWNrZ3JvdW5kOiAjMjExYjE5O1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IDAuMnMgYWxsO1xyXG4gIHRyYW5zaXRpb246IDAuMnMgYWxsO1xyXG59XHJcblxyXG4ubm90Zm91bmQgYTpob3ZlciB7XHJcbiAgY29sb3I6ICM5Nzk3OTc7XHJcbiAgYmFja2dyb3VuZDogIzQ1NDE0MDtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gIC5ub3Rmb3VuZCAubm90Zm91bmQtNDA0IGgxIHtcclxuICAgIGZvbnQtc2l6ZTogMTQ4cHg7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQge1xyXG4gICAgaGVpZ2h0OiAxNDhweDtcclxuICAgIG1hcmdpbjogMHB4IGF1dG8gMTBweDtcclxuICB9XHJcbiAgLm5vdGZvdW5kIC5ub3Rmb3VuZC00MDQgaDEge1xyXG4gICAgZm9udC1zaXplOiA4NnB4O1xyXG4gIH1cclxuICAubm90Zm91bmQgLm5vdGZvdW5kLTQwNCBoMiB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgfVxyXG4gIC5ub3Rmb3VuZCBhIHtcclxuICAgIHBhZGRpbmc6IDdweCAxNXB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/dashboard/pagenotfound/pagenotfound.component.html":
/*!********************************************************************!*\
  !*** ./src/app/dashboard/pagenotfound/pagenotfound.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\t<div id=\"notfound\">\r\n    <div class=\"notfound\">\r\n      <div class=\"notfound-404\">\r\n        <h1>Oops!</h1>\r\n        <h2>404 - The Page can't be found</h2>\r\n      </div>\r\n      <!--<a href=\"#\">Go TO Homepage</a>-->\r\n    </div>\r\n\t</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/pagenotfound/pagenotfound.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/dashboard/pagenotfound/pagenotfound.component.ts ***!
  \******************************************************************/
/*! exports provided: PagenotfoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagenotfoundComponent", function() { return PagenotfoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PagenotfoundComponent = /** @class */ (function () {
    function PagenotfoundComponent() {
    }
    PagenotfoundComponent.prototype.ngOnInit = function () {
    };
    PagenotfoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagenotfound',
            template: __webpack_require__(/*! ./pagenotfound.component.html */ "./src/app/dashboard/pagenotfound/pagenotfound.component.html"),
            styles: [__webpack_require__(/*! ./pagenotfound.component.css */ "./src/app/dashboard/pagenotfound/pagenotfound.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PagenotfoundComponent);
    return PagenotfoundComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/underdevelopment/underdevelopment.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/dashboard/underdevelopment/underdevelopment.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC91bmRlcmRldmVsb3BtZW50L3VuZGVyZGV2ZWxvcG1lbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/underdevelopment/underdevelopment.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/dashboard/underdevelopment/underdevelopment.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\" style=\"margin-top:250px\">\r\n\r\n    <div style=\"text-align:center; font-size:x-large;height:50px;\">UNDER DEVELOPMENT OR MENU PERMISSIONS NOT ASSIGNED</div>\r\n\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/dashboard/underdevelopment/underdevelopment.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/dashboard/underdevelopment/underdevelopment.component.ts ***!
  \**************************************************************************/
/*! exports provided: UnderdevelopmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnderdevelopmentComponent", function() { return UnderdevelopmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UnderdevelopmentComponent = /** @class */ (function () {
    function UnderdevelopmentComponent() {
    }
    UnderdevelopmentComponent.prototype.ngOnInit = function () {
    };
    UnderdevelopmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-underdevelopment',
            template: __webpack_require__(/*! ./underdevelopment.component.html */ "./src/app/dashboard/underdevelopment/underdevelopment.component.html"),
            styles: [__webpack_require__(/*! ./underdevelopment.component.css */ "./src/app/dashboard/underdevelopment/underdevelopment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UnderdevelopmentComponent);
    return UnderdevelopmentComponent;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map