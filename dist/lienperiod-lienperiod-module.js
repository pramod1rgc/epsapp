(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["lienperiod-lienperiod-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/Subject.js":
/*!***************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/Subject.js ***!
  \***************************************************/
/*! exports provided: Subject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subject", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]; });


//# sourceMappingURL=Subject.js.map

/***/ }),

/***/ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.css":
/*!****************************************************************************************************!*\
  !*** ./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.css ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgorange {\r\n  color: orange;\r\n}\r\n\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n\r\n.bggreen {\r\n  color: green;\r\n}\r\n\r\n.bgblue {\r\n  color: blue;\r\n}\r\n\r\n.bgred {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGllbnBlcmlvZC9qb2luLWFmdGVyLWxpZW4tZXBzLWVtcGxveWVlL2pvaW4tYWZ0ZXItbGllbi1lcHMtZW1wbG95ZWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFdBQVc7Q0FDWiIsImZpbGUiOiJzcmMvYXBwL2xpZW5wZXJpb2Qvam9pbi1hZnRlci1saWVuLWVwcy1lbXBsb3llZS9qb2luLWFmdGVyLWxpZW4tZXBzLWVtcGxveWVlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmdvcmFuZ2Uge1xyXG4gIGNvbG9yOiBvcmFuZ2U7XHJcbn1cclxuXHJcbi5iZ2RhcmtvcmFuZ2Uge1xyXG4gIGNvbG9yOiBkYXJrb3JhbmdlO1xyXG59XHJcblxyXG4uYmdncmVlbiB7XHJcbiAgY29sb3I6IGdyZWVuO1xyXG59XHJcblxyXG4uYmdibHVlIHtcclxuICBjb2xvcjogYmx1ZTtcclxufVxyXG5cclxuLmJncmVkIHtcclxuICBjb2xvcjogcmVkO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-comman-mst (onchange)=\"GetcommanMethod($event)\" ></app-comman-mst>\r\n\r\n\r\n<form (ngSubmit)=\"form.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" novalidate>\r\n  <div class=\"col-md-12 col-lg-7\" [attr.disabled]=\"disableflag\">\r\n    <mat-card class=\"example-card emp1-form\">\r\n\r\n      <ng-template matStepLabel>Lien Period/Extension of Lien Period</ng-template>\r\n\r\n\r\n      <div class=\"fom-title\">Relieving Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieving Order Number\" formControlName=\"relievingOrderNo\" maxlength=\"50\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['relievingOrderNo'].valid || form.controls['relievingOrderNo'].dirty || form.controls['relievingOrderNo'].touched) && form.controls['relievingOrderNo'].invalid && form.controls['relievingOrderNo'].errors.required\">\r\n            Relieving Order Number is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['relievingOrderNo'].dirty || form.controls['relievingOrderNo'].touched) && form.controls['relievingOrderNo'].invalid && form.controls['relievingOrderNo'].errors.pattern\">\r\n        Relieving Order Number is invalid\r\n      </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"RelievingOrderDate\" placeholder=\"Relieving Order Date\"  formControlName=\"relievingOrderDate\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"RelievingOrderDate\" class=\"display-non\"></mat-datepicker-toggle>\r\n          <mat-datepicker #RelievingOrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['relievingOrderDate'].invalid || !form.controls['relievingOrderDate'].valid && form.controls['relievingOrderDate'].touched\">\r\n            Relieving Order Date is required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieving Office\" formControlName=\"relievingOffice\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['relievingOffice'].valid || form.controls['relievingOffice'].dirty || form.controls['relievingOffice'].touched) && form.controls['relievingOffice'].invalid && form.controls['relievingOffice'].errors.required\">\r\n            Relieving Office is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievingOffice'].dirty || form.controls['relievingOffice'].touched) && form.controls['relievingOffice'].invalid && form.controls['relievingOffice'].errors.pattern\">\r\n            Relieving Office is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieved by ( TRY - DDO )\" formControlName=\"relievingBy\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['relievingBy'].valid || form.controls['relievingBy'].dirty || form.controls['relievingBy'].touched) && form.controls['relievingBy'].invalid && form.controls['relievingBy'].errors.required\">\r\n            Relieved By is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievingBy'].dirty || form.controls['relievingBy'].touched) && form.controls['relievingBy'].invalid && form.controls['relievingBy'].errors.pattern\">\r\n            Relieved By is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieved On\" [matDatepicker]=\"RelievedOn\" formControlName=\"relievedOn\" required readonly>\r\n          <mat-datepicker-toggle matSuffix [for]=\"RelievedOn\" class=\"display-non\"></mat-datepicker-toggle>\r\n          <mat-datepicker #RelievedOn></mat-datepicker>\r\n          <mat-error *ngIf=\"(!form.controls['relievedOn'].valid || form.controls['relievedOn'].dirty || form.controls['relievedOn'].touched) && form.controls['relievedOn'].invalid && form.controls['relievedOn'].errors.required\">\r\n            Relieved On is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievedOn'].dirty || form.controls['relievedOn'].touched) && form.controls['relievedOn'].invalid && form.controls['relievedOn'].errors.pattern\">\r\n            Relieved On is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"On Designation\" formControlName=\"onRelievedDesignation\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['onRelievedDesignation'].valid || form.controls['onRelievedDesignation'].dirty || form.controls['onRelievedDesignation'].touched) && form.controls['onRelievedDesignation'].invalid && form.controls['onRelievedDesignation'].errors.required\">\r\n            On Designation is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['onRelievedDesignation'].dirty || form.controls['onRelievedDesignation'].touched) && form.controls['onRelievedDesignation'].invalid && form.controls['onRelievedDesignation'].errors.pattern\">\r\n            On Designation is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Pay Commission\" formControlName=\"payCommission\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['payCommission'].valid || form.controls['payCommission'].dirty || form.controls['payCommission'].touched) && form.controls['payCommission'].invalid && form.controls['payCommission'].errors.required\">\r\n            Pay Commission is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['payCommission'].dirty || form.controls['payCommission'].touched) && form.controls['payCommission'].invalid && form.controls['payCommission'].errors.pattern\">\r\n            Pay Commission is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Pay Scale\" formControlName=\"payScale\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['payScale'].valid || form.controls['payScale'].dirty || form.controls['payScale'].touched) && form.controls['payScale'].invalid && form.controls['payScale'].errors.required\">\r\n            Pay Scale is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['payScale'].dirty || form.controls['payScale'].touched) && form.controls['payScale'].invalid && form.controls['payScale'].errors.pattern\">\r\n            Pay Scale is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Basic Pay\" formControlName=\"basicPay\" required readonly>\r\n          <mat-error *ngIf=\"(!form.controls['basicPay'].valid || form.controls['basicPay'].dirty || form.controls['basicPay'].touched) && form.controls['basicPay'].invalid && form.controls['basicPay'].errors.required\">\r\n            Basic Pay is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['basicPay'].dirty || form.controls['basicPay'].touched) && form.controls['basicPay'].invalid && form.controls['basicPay'].errors.pattern\">\r\n            Basic Pay is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Joining Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\" Join Service As \" formControlName=\"joinServiceAs\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let joinservice of joinService\" [value]=\"joinservice.msEmpTypeID\">\r\n              {{joinservice.joiningText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joinServiceAs'].valid || form.controls['joinServiceAs'].dirty || form.controls['joinServiceAs'].touched) && form.controls['joinServiceAs'].invalid && form.controls['joinServiceAs'].errors.required\">\r\n            Join Service As is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['joinServiceAs'].dirty || form.controls['joinServiceAs'].touched) && form.controls['joinServiceAs'].invalid && form.controls['joinServiceAs'].errors.pattern\">\r\n        Join Service As is invalid\r\n      </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Joining on Account of\" formControlName=\"joiningonAccountof\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let getjoiningaccount of getJoiningAccount\" [value]=\"getjoiningaccount.cddirCodeValue\">\r\n              {{getjoiningaccount.cddirCodeText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joiningonAccountof'].valid || form.controls['joiningonAccountof'].dirty || form.controls['joiningonAccountof'].touched) && form.controls['joiningonAccountof'].invalid && form.controls['joiningonAccountof'].errors.required\">\r\n            Join Service As is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['joiningonAccountof'].dirty || form.controls['joiningonAccountof'].touched) && form.controls['joiningonAccountof'].invalid && form.controls['joiningonAccountof'].errors.pattern\">\r\n        Join Service As is invalid\r\n      </mat-error>-->\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Joining Order No.\" formControlName=\"joiningOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_]+\" required>\r\n          <mat-error *ngIf=\"(!form.controls['joiningOrderNo'].valid || form.controls['joiningOrderNo'].dirty || form.controls['joiningOrderNo'].touched) && form.controls['joiningOrderNo'].invalid && form.controls['joiningOrderNo'].errors.required\">\r\n            Joining Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['joiningOrderNo'].dirty || form.controls['joiningOrderNo'].touched) && form.controls['joiningOrderNo'].invalid && form.controls['joiningOrderNo'].errors.pattern\">\r\n            Joining Order Number is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"JoiningOrderDate\" placeholder=\"Joining Order Date\" [min]=\"form.controls['relievingOrderDate'].value\" (click)=\"JoiningOrderDate.open()\" formControlName=\"joiningOrderDate\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"JoiningOrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #JoiningOrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['joiningOrderDate'].invalid && form.controls['joiningOrderDate'].errors.required\">\r\n            Joining Order Date is required\r\n          </mat-error>\r\n\r\n          <!--<mat-error *ngIf=\"!form.controls['joiningOrderDate'].valid\">\r\n            Joining Order Date is invalid\r\n          </mat-error>-->\r\n    \r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n\r\n          <mat-select placeholder=\"Time\" formControlName=\"joiningTime\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let joiningTime of joiningTimeDdl\" [value]=\"joiningTime\">{{joiningTime}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joiningTime'].valid || form.controls['joiningTime'].dirty || form.controls['joiningTime'].touched) && form.controls['joiningTime'].invalid && form.controls['joiningTime'].errors.required\">\r\n            Time is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Office Name\" formControlName=\"officeId\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let getofficelists of getofficelist\" [value]=\"getofficelists.officeId\">{{getofficelists.officeName}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['officeId'].valid || form.controls['officeId'].dirty || form.controls['officeId'].touched) && form.controls['officeId'].invalid && form.controls['officeId'].errors.required\">\r\n            Office Name is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Office City Class\" formControlName=\"officeCityClass\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let officecityclass of officeCityClassList\" [value]=\"officecityclass.officeClassCityId\">{{officecityclass.officeClassCityName}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['officeCityClass'].valid || form.controls['officeCityClass'].dirty || form.controls['officeCityClass'].touched) && form.controls['officeCityClass'].invalid && form.controls['officeCityClass'].errors.required\">\r\n            Office City Class is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n\r\n          <mat-select placeholder=\"Designation\" formControlName=\"joiningDesigcode\" required >\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let designa of designation\" [value]=\"designa.desigId\">\r\n              {{designa.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <!--<mat-error *ngIf=\"!form.controls['joiningDesigcode'].valid\">\r\n            Office City Class is Required\r\n          </mat-error>-->\r\n          <mat-error *ngIf=\"(!form.controls['joiningDesigcode'].valid || form.controls['joiningDesigcode'].dirty || form.controls['joiningDesigcode'].touched) && form.controls['joiningDesigcode'].invalid && form.controls['joiningDesigcode'].errors.required\">\r\n            Designation is Required\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\"  [disabled]=\"form.controls['employeeCode'].invalid\">{{btnUpdateText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\" [disabled]=\"form.controls['employeeCode'].invalid\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"!disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <!--<mat-accordion>\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          Form value\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      <code>\r\n        {{form.value | json}}\r\n      </code>\r\n    </mat-expansion-panel>\r\n  </mat-accordion>-->\r\n\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" [(ngModel)]=\"tblfiltervalue\" placeholder=\"Search\" (keypress)=\"charaterOnlyNoSpace($event)\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"epsJoinData\" matSort class=\"mat-elevation-z8 even-odd-color\" matSortActive=\"joiningOrderNo\" matSortDirection=\"asc\">\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"joiningOrderNo\">\r\n\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Joining Order No </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element;\"> {{element.joiningOrderNo}}  </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"joiningOrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Joining Order Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.joiningOrderDate | date}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"relievingOrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Relieving Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"relievingOrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Relieving Order Date. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOrderDate | date}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"relievingOffice\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Office Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOffice}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <label *ngIf=\"element.veriFlag == 'N'\" class=\"bgred\">Lien Period</label>\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\">Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <mat-cell *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'N' || element.veriFlag == 'R' || element.veriFlag == 'U')&& roleId == 6\"  class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.msAfterLeinId)\">edit</a>\r\n  <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRecordClick(element.msAfterLeinId);DeletePopup=!DeletePopup\">\r\n    delete_forever\r\n  </a>\r\n  <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'N'  && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.msAfterLeinId)\">\r\n    info\r\n  </a>\r\n  <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.msAfterLeinId)\">\r\n    info\r\n  </a>\r\n</mat-cell>\r\n\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\" class=\"add-leave-btn\">\r\n\r\n      <font color=red> Record Not Found </font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5,10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure want to delete? <br /> <br /></h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to approve? <br /> <br /></h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form (ngSubmit)=\"form2.valid && forwardStatusUpdate('R')\" #freject=\"ngForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput [(ngModel)]=\"rejectionRemark\" name=\"rejectionRemark1\" #rejectionRemark1=\"ngModel\"  rows=\"2\" maxlength=\"50\" align=\"center\" placeholder=\"Remarks\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_,. ]+\" required></textarea>\r\n        <mat-error>\r\n          <span [hidden]=\"!rejectionRemark1.errors?.required\">Remarks is required</span>\r\n          <!--<span [hidden]=\"!rejectionRemark.errors?.required\">Service Type is required</span>-->\r\n          <!--<span *ngIf=\"!rejectionRemark.valid\">Remarks Required !</span>-->\r\n        </mat-error>\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: JoinAfterLienEpsEmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoinAfterLienEpsEmployeeComponent", function() { return JoinAfterLienEpsEmployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_lienperiod_epsemployeejoinafterlienperiod_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/lienperiod/epsemployeejoinafterlienperiod.service */ "./src/app/services/lienperiod/epsemployeejoinafterlienperiod.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var JoinAfterLienEpsEmployeeComponent = /** @class */ (function () {
    function JoinAfterLienEpsEmployeeComponent(master, _formBuilder, snackBar, epsemployeejoinafterlienperiodService) {
        this.master = master;
        this._formBuilder = _formBuilder;
        this.snackBar = snackBar;
        this.epsemployeejoinafterlienperiodService = epsemployeejoinafterlienperiodService;
        this.joiningTimeDdl = ['ForeNoon', 'AfterNoon'];
        this.designation = [];
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.rejectionRemark = '';
        this.tblfiltervalue = '';
        this.lienId = '';
        this.notFound = true;
        this.displayedColumns = ['joiningOrderNo', 'joiningOrderDate', 'relievingOrderNo', 'relievingOrderDate', 'relievingOffice', 'Status', 'action'];
        this.disableflag = true;
        this.disableForwardflag = false;
    }
    JoinAfterLienEpsEmployeeComponent.prototype.ngOnInit = function () {
        this.roleId = sessionStorage.getItem('userRoleID');
        this.btnUpdateText = 'Save';
        this.FormDetails();
        this.getAllDesignation(sessionStorage.getItem('controllerID'));
        this.getJoinServices();
        this.getJoiningAccountOf();
        this.getOfficeList();
        this.getOfficeCityClassList();
        // this.getAllDesignation(sessionStorage.getItem('controllerID'));
    };
    JoinAfterLienEpsEmployeeComponent.prototype.FormDetails = function () {
        this.form = this._formBuilder.group({
            'employeeCode': [this.empCd, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOrderNo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOrderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOffice': [null],
            'relievingBy': [null],
            'relievingDdoId': [null],
            'relievedOn': [null],
            'onRelievedDesignationId': [null],
            'onRelievedDesignation': [null],
            'payCommissionId': [null],
            'payCommission': [null],
            'payScaleId': [null],
            'payScale': [null],
            'basicPay': [null],
            'joinServiceAs': [null],
            'joiningonAccountof': [null],
            'joiningOrderNo': [null],
            'joiningOrderDate': [null],
            'joiningTime': [null],
            'officeId': [null],
            'officeCityClass': [null],
            'joiningDesigcode': [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'createdBy': [null],
            'existRecordAfterLien': [null],
            'flagUpdate': [null],
            'msAfterLeinId': [null],
            'rejectionRemark': [null],
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getJoinServices = function () {
        var _this = this;
        this.master.getJoiningMode().subscribe(function (res) {
            debugger;
            _this.joinService = res;
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getJoiningAccountOf = function () {
        var _this = this;
        debugger;
        this.master.getJoiningAccountOf().subscribe(function (res) {
            debugger;
            _this.getJoiningAccount = res;
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.charaterOnlyNoSpace = function (event) {
        debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getOfficeList = function () {
        var _this = this;
        this.master.getOfficeList(sessionStorage.getItem('controllerID'), sessionStorage.getItem('ddoid')).subscribe(function (res) {
            debugger;
            _this.getofficelist = res;
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getAllDesignation = function (controllerId) {
        var _this = this;
        this.master.GetAllDesignation(controllerId).subscribe(function (res) {
            debugger;
            _this.designation = res;
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getOfficeCityClassList = function () {
        var _this = this;
        this.master.getOfficeCityClassList().subscribe(function (res) {
            _this.officeCityClassList = res;
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.GetcommanMethod = function (value) {
        // alert(value);
        debugger;
        this.empCd = value;
        this.form.get('employeeCode').setValue(value);
        this.getEpsEmpJoinOffAfterLienPeriodDetails(value, this.roleId);
        this.form.get('employeeCode').setValue(this.empCd);
        // this.btnUpdateText = 'Save';
    };
    JoinAfterLienEpsEmployeeComponent.prototype.ltrim = function (tblfiltervalue) {
        return tblfiltervalue.replace(/^\s+/g, '');
    };
    JoinAfterLienEpsEmployeeComponent.prototype.applyFilter = function (filterValue) {
        debugger;
        this.tblfiltervalue = this.ltrim(this.tblfiltervalue);
        if (this.epsJoinData === null || this.epsJoinData === undefined) {
            this.notFound = false;
        }
        this.epsJoinData.filter = filterValue.trim().toLowerCase();
        ;
        this.epsJoinData.filterPredicate = function (data, filter) {
            debugger;
            var joiningOrderNo = "";
            if (data.joiningOrderNo != '' && data.joiningOrderNo != null) {
                joiningOrderNo = data.joiningOrderNo.toLowerCase();
                return data.relievingOrderNo.toLowerCase().includes(filter) || data.relievingOffice.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.joiningOrderDate.includes(filter);
            }
            else {
                return data.relievingOrderNo.toLowerCase().includes(filter) || joiningOrderNo.includes(filter) || data.relievingOffice.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter);
            }
        };
        if (this.epsJoinData.paginator) {
            this.epsJoinData.paginator.firstPage();
        }
        if (this.epsJoinData.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    JoinAfterLienEpsEmployeeComponent.prototype.btnEditClick = function (lienId) {
        debugger;
        this.formDirective.resetForm();
        var value = this.epsJoinData.filteredData.filter(function (x) { return x.msAfterLeinId == lienId; })[0];
        if (value.joiningDesigcode === 0) {
            value.joiningDesigcode = null;
            value.joiningonAccountof = null;
        }
        this.form.patchValue(value);
        this.form.enable();
        if (value.veriFlag == 'N') {
            this.btnUpdateText = 'Save';
        }
        else {
            this.btnUpdateText = 'Update';
        }
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableflag = true;
            if (value.veriFlag == 'N') {
                this.disableForwardflag = false;
            }
            else {
                this.disableForwardflag = true;
            }
            //this.Btntxt = 'Save';
        }
    };
    JoinAfterLienEpsEmployeeComponent.prototype.btnInfoClick = function (lienId) {
        var value = this.epsJoinData.filteredData.filter(function (x) { return x.msAfterLeinId == lienId; })[0];
        this.form.patchValue(value);
        this.form.disable();
        //let empStatus = value.veriFlag;
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableForwardflag = false;
            //this.Btntxt = 'Save';
        }
    };
    JoinAfterLienEpsEmployeeComponent.prototype.getEpsEmpJoinOffAfterLienPeriodDetails = function (Empcd, roleId) {
        var _this = this;
        this.epsemployeejoinafterlienperiodService.getEpsEmpJoinOffAfterLienPeriodDetailByEmp(Empcd, roleId).subscribe(function (result) {
            if (result.length == 0) {
                _this.formDirective.resetForm();
                _this.form.enable();
                _this.epsJoinData = null;
                _this.form.get('employeeCode').setValue(_this.empCd);
            }
            else {
                _this.epsJoinData = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result);
                _this.epsJoinData.paginator = _this.paginator;
                _this.epsJoinData.sort = _this.sort;
                _this.form.disable();
            }
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.ResetForm = function () {
        this.form.reset();
        this.formDirective.resetForm();
        this.btnUpdateText = 'Save';
        this.disableForwardflag = false;
        this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
    };
    JoinAfterLienEpsEmployeeComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    JoinAfterLienEpsEmployeeComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        debugger;
        var RejectionComment = '0';
        //let employeeId = this.empCd.trim();
        var joiningOrderNo = this.form.get('joiningOrderNo').value;
        if (statusFlag == 'R') {
            RejectionComment = this.rejectionRemark;
        }
        else {
            RejectionComment = '0';
        }
        this.epsemployeejoinafterlienperiodService.forwardStatusUpdate(this.empCd, joiningOrderNo, statusFlag, RejectionComment).subscribe(function (result1) {
            if (parseInt(result1) >= 1) {
                _this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
                _this.form.reset();
                _this.formDirective.resetForm();
                _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId);
                _this.btnUpdateText = 'Save';
                _this.Cancel();
                _this.infoScreen = false;
                _this.disableForwardflag = false;
            }
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.deleteRecordClick = function (lienId) {
        this.lienId = lienId;
    };
    JoinAfterLienEpsEmployeeComponent.prototype.confirmDelete = function () {
        var _this = this;
        this.epsemployeejoinafterlienperiodService.deleteEpsEmployeeJoinAfterLienPeriodById(this.lienId).subscribe(function (result1) {
            debugger;
            if (parseInt(result1) >= 1) {
                _this.snackBar.open('Record Successfully deleted', null, { duration: 4000 });
                _this.form.reset();
                _this.formDirective.resetForm();
                _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId);
                _this.btnUpdateText = 'Save';
                _this.Cancel();
            }
            else {
                _this.snackBar.open('Record not deleted', null, { duration: 4000 });
            }
        });
    };
    JoinAfterLienEpsEmployeeComponent.prototype.onSubmit = function () {
        var _this = this;
        debugger;
        //this.submitted = true;
        this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        else {
            if (this.btnUpdateText == 'Update') {
                this.form.get('flagUpdate').setValue('update');
            }
            else {
                this.form.get('flagUpdate').setValue('insert');
            }
            this.epsemployeejoinafterlienperiodService.InsertUpateEpsEmployeeJoinAfterLienPeriod(this.form.value).subscribe(function (result1) {
                var resultvalue = result1.split("-");
                if (parseInt(resultvalue[0]) >= 1) {
                    if (_this.btnUpdateText == 'Update') {
                        _this.snackBar.open('Update Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId);
                        _this.btnUpdateText = 'Save';
                    }
                    else {
                        _this.snackBar.open('Save Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId);
                        _this.btnUpdateText == 'Save';
                    }
                }
                else {
                    if (_this.btnUpdateText == 'Update') {
                        if (resultvalue[2] = 'Duplicate Record') {
                            _this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
                        }
                        else {
                            _this.snackBar.open('Update not Successfully', null, { duration: 4000 });
                        }
                    }
                    else if (resultvalue[2] = 'Duplicate Record') {
                        _this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Save not Successfully', null, { duration: 4000 });
                    }
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('freject'),
        __metadata("design:type", Object)
    ], JoinAfterLienEpsEmployeeComponent.prototype, "form2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], JoinAfterLienEpsEmployeeComponent.prototype, "formDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], JoinAfterLienEpsEmployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], JoinAfterLienEpsEmployeeComponent.prototype, "sort", void 0);
    JoinAfterLienEpsEmployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-join-after-lien-eps-employee',
            template: __webpack_require__(/*! ./join-after-lien-eps-employee.component.html */ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.html"),
            styles: [__webpack_require__(/*! ./join-after-lien-eps-employee.component.css */ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_2__["MasterService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"], _services_lienperiod_epsemployeejoinafterlienperiod_service__WEBPACK_IMPORTED_MODULE_3__["EpsemployeejoinafterlienperiodService"]])
    ], JoinAfterLienEpsEmployeeComponent);
    return JoinAfterLienEpsEmployeeComponent;
}());



/***/ }),

/***/ "./src/app/lienperiod/lienperiod-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/lienperiod/lienperiod-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: LienperiodRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LienperiodRoutingModule", function() { return LienperiodRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _transfer_office_lienperiod_transfer_office_lienperiod_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transfer-office-lienperiod/transfer-office-lienperiod.component */ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.ts");
/* harmony import */ var _join_after_lien_eps_employee_join_after_lien_eps_employee_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./join-after-lien-eps-employee/join-after-lien-eps-employee.component */ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.ts");
/* harmony import */ var _non_eps_employee_join_after_lien_non_eps_employee_join_after_lien_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component */ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.ts");
/* harmony import */ var _lienperiod_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./lienperiod.module */ "./src/app/lienperiod/lienperiod.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [{
        path: '', component: _lienperiod_module__WEBPACK_IMPORTED_MODULE_5__["LienperiodModule"], children: [{
                path: 'transferonlien', component: _transfer_office_lienperiod_transfer_office_lienperiod_component__WEBPACK_IMPORTED_MODULE_2__["TransferOfficeLienperiodComponent"],
            },
            {
                path: 'epsempjoinafterlien', component: _join_after_lien_eps_employee_join_after_lien_eps_employee_component__WEBPACK_IMPORTED_MODULE_3__["JoinAfterLienEpsEmployeeComponent"],
            },
            {
                path: 'nonepsempjoinafterlien', component: _non_eps_employee_join_after_lien_non_eps_employee_join_after_lien_component__WEBPACK_IMPORTED_MODULE_4__["NonEpsEmployeeJoinAfterLienComponent"],
            },
        ],
    }];
var LienperiodRoutingModule = /** @class */ (function () {
    function LienperiodRoutingModule() {
    }
    LienperiodRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LienperiodRoutingModule);
    return LienperiodRoutingModule;
}());



/***/ }),

/***/ "./src/app/lienperiod/lienperiod.module.ts":
/*!*************************************************!*\
  !*** ./src/app/lienperiod/lienperiod.module.ts ***!
  \*************************************************/
/*! exports provided: LienperiodModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LienperiodModule", function() { return LienperiodModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _lienperiod_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lienperiod-routing.module */ "./src/app/lienperiod/lienperiod-routing.module.ts");
/* harmony import */ var _transfer_office_lienperiod_transfer_office_lienperiod_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transfer-office-lienperiod/transfer-office-lienperiod.component */ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.ts");
/* harmony import */ var _join_after_lien_eps_employee_join_after_lien_eps_employee_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./join-after-lien-eps-employee/join-after-lien-eps-employee.component */ "./src/app/lienperiod/join-after-lien-eps-employee/join-after-lien-eps-employee.component.ts");
/* harmony import */ var _non_eps_employee_join_after_lien_non_eps_employee_join_after_lien_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component */ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../loanmgt/loanmgt.module */ "./src/app/loanmgt/loanmgt.module.ts");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var LienperiodModule = /** @class */ (function () {
    function LienperiodModule() {
    }
    LienperiodModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_transfer_office_lienperiod_transfer_office_lienperiod_component__WEBPACK_IMPORTED_MODULE_3__["TransferOfficeLienperiodComponent"], _join_after_lien_eps_employee_join_after_lien_eps_employee_component__WEBPACK_IMPORTED_MODULE_4__["JoinAfterLienEpsEmployeeComponent"], _non_eps_employee_join_after_lien_non_eps_employee_join_after_lien_component__WEBPACK_IMPORTED_MODULE_5__["NonEpsEmployeeJoinAfterLienComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _lienperiod_routing_module__WEBPACK_IMPORTED_MODULE_2__["LienperiodRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _material_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__["NgxMatSelectSearchModule"], _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_8__["LoanmgtModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__["SharedModule"]
            ]
        })
    ], LienperiodModule);
    return LienperiodModule;
}());



/***/ }),

/***/ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.css":
/*!************************************************************************************************************!*\
  !*** ./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.css ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgorange {\r\n  color: orange;\r\n}\r\n\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n\r\n.bggreen {\r\n  color: green;\r\n}\r\n\r\n.bgblue {\r\n  color: blue;\r\n}\r\n\r\n.bgred {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGllbnBlcmlvZC9ub24tZXBzLWVtcGxveWVlLWpvaW4tYWZ0ZXItbGllbi9ub24tZXBzLWVtcGxveWVlLWpvaW4tYWZ0ZXItbGllbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvbGllbnBlcmlvZC9ub24tZXBzLWVtcGxveWVlLWpvaW4tYWZ0ZXItbGllbi9ub24tZXBzLWVtcGxveWVlLWpvaW4tYWZ0ZXItbGllbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnb3JhbmdlIHtcclxuICBjb2xvcjogb3JhbmdlO1xyXG59XHJcblxyXG4uYmdkYXJrb3JhbmdlIHtcclxuICBjb2xvcjogZGFya29yYW5nZTtcclxufVxyXG5cclxuLmJnZ3JlZW4ge1xyXG4gIGNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuLmJnYmx1ZSB7XHJcbiAgY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5iZ3JlZCB7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\"></app-comman-mst>\r\n<form (ngSubmit)=\"form.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" novalidate>\r\n  <div class=\"col-md-12 col-lg-7\" [attr.disabled]=\"disableflag\">\r\n    <mat-card class=\"example-card emp1-form\">\r\n\r\n      <ng-template matStepLabel>Lien Period/Extension of Lien Period</ng-template>\r\n\r\n\r\n      <div class=\"fom-title\">Relieving Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieving Order Number\" formControlName=\"relievingOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_]+\" required>\r\n          <mat-error *ngIf=\"(form.controls['relievingOrderNo'].dirty ||!form.controls['relievingOrderNo'].valid || form.controls['relievingOrderNo'].touched) && form.controls['relievingOrderNo'].invalid && form.controls['relievingOrderNo'].errors.required\">\r\n            Relieving Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievingOrderNo'].dirty  || form.controls['relievingOrderNo'].touched) && form.controls['relievingOrderNo'].invalid && form.controls['relievingOrderNo'].errors.pattern\">\r\n            Relieving Order Number is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"RelievingOrderDate\" placeholder=\"Relieving Order Date\" formControlName=\"relievingOrderDate\" (click)=\"RelievingOrderDate.open()\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"RelievingOrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #RelievingOrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"!form.controls['relievingOrderDate'].valid || !form.controls['relievingOrderDate'].valid && form.controls['relievingOrderDate'].touched\">\r\n            Relieving Order Date is required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieving Office\" formControlName=\"relievingOffice\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_ ]+\" maxlength=\"70\" required>\r\n          <mat-error *ngIf=\"(!form.controls['relievingOffice'].valid || form.controls['relievingOffice'].dirty || form.controls['relievingOffice'].touched) && form.controls['relievingOffice'].invalid && form.controls['relievingOffice'].errors.required\">\r\n            Relieving Office is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievingOffice'].dirty || form.controls['relievingOffice'].touched) && form.controls['relievingOffice'].invalid && form.controls['relievingOffice'].errors.pattern\">\r\n            Relieving Office is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Relieved by ( TRY - DDO )\" formControlName=\"relievingBy\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_ ]+\" maxlength=\"50\" required>\r\n          <mat-error *ngIf=\"(!form.controls['relievingBy'].valid || form.controls['relievingBy'].dirty || form.controls['relievingBy'].touched) && form.controls['relievingBy'].invalid && form.controls['relievingBy'].errors.required\">\r\n            Relieved By is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['relievingBy'].dirty || form.controls['relievingBy'].touched) && form.controls['relievingBy'].invalid && form.controls['relievingBy'].errors.pattern\">\r\n            Relieved By is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"relievedOnDate\" placeholder=\"Relieved On\" formControlName=\"relievedOn\" (click)=\"relievedOnDate.open()\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"relievedOnDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #relievedOnDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['relievedOn'].invalid || !form.controls['relievedOn'].valid && form.controls['relievedOn'].touched\">\r\n            Relieved On Date is required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"On Designation\" formControlName=\"onRelievedDesignation\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_ ]+\" maxlength=\"50\" required>\r\n          <mat-error *ngIf=\"(!form.controls['onRelievedDesignation'].valid || form.controls['onRelievedDesignation'].dirty || form.controls['onRelievedDesignation'].touched) && form.controls['onRelievedDesignation'].invalid && form.controls['onRelievedDesignation'].errors.required\">\r\n            On Designation is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['onRelievedDesignation'].dirty || form.controls['onRelievedDesignation'].touched) && form.controls['onRelievedDesignation'].invalid && form.controls['onRelievedDesignation'].errors.pattern\">\r\n            On Designation is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\" Pay Commission \" formControlName=\"payCommissionId\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let getPayComm of getPayCommission\" [value]=\"getPayComm.payCommId\">\r\n              {{getPayComm.payCommDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['payCommissionId'].valid || form.controls['payCommissionId'].dirty || form.controls['payCommissionId'].touched) && form.controls['payCommissionId'].invalid && form.controls['payCommissionId'].errors.required\">\r\n            Join Service As is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['joinServiceAs'].dirty || form.controls['joinServiceAs'].touched) && form.controls['joinServiceAs'].invalid && form.controls['joinServiceAs'].errors.pattern\">\r\n            Join Service As is invalid\r\n          </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Pay Scale\" formControlName=\"payScale\" pattern=\"[1-9]{1}[0-9]{0,20}[-]{1}[0-9]+\" maxlength=\"50\" required>\r\n          <mat-error *ngIf=\"(!form.controls['payScale'].valid || form.controls['payScale'].dirty || form.controls['payScale'].touched) && form.controls['payScale'].invalid && form.controls['payScale'].errors.required\">\r\n            Pay Scale is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['payScale'].dirty || form.controls['payScale'].touched) && form.controls['payScale'].invalid && form.controls['payScale'].errors.pattern\">\r\n            Pay Scale is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Basic Pay\" formControlName=\"basicPay\" pattern=\"[0-9]+\" (keypress)=\"numberOnly($event)\" maxlength=\"5\" required>\r\n          <mat-error *ngIf=\"(!form.controls['basicPay'].valid || form.controls['basicPay'].dirty || form.controls['basicPay'].touched) && form.controls['basicPay'].invalid && form.controls['basicPay'].errors.required\">\r\n            Basic Pay is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['basicPay'].dirty || form.controls['basicPay'].touched) && form.controls['basicPay'].invalid && form.controls['basicPay'].errors.pattern\">\r\n            Basic Pay is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Joining Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\" Join Service As \" formControlName=\"joinServiceAs\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let joinservice of joinService\" [value]=\"joinservice.msEmpTypeID\">\r\n              {{joinservice.joiningText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joinServiceAs'].valid || form.controls['joinServiceAs'].dirty || form.controls['joinServiceAs'].touched) && form.controls['joinServiceAs'].invalid && form.controls['joinServiceAs'].errors.required\">\r\n            Join Service As is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['joinServiceAs'].dirty || form.controls['joinServiceAs'].touched) && form.controls['joinServiceAs'].invalid && form.controls['joinServiceAs'].errors.pattern\">\r\n            Join Service As is invalid\r\n          </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Joining on Account of\" formControlName=\"joiningonAccountof\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let getjoiningaccount of getJoiningAccount\" [value]=\"getjoiningaccount.cddirCodeValue\">\r\n              {{getjoiningaccount.cddirCodeText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joiningonAccountof'].valid ||form.controls['joiningonAccountof'].dirty || form.controls['joiningonAccountof'].touched) && form.controls['joiningonAccountof'].invalid && form.controls['joiningonAccountof'].errors.required\">\r\n            Join Service As is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"(form.controls['joiningonAccountof'].dirty || form.controls['joiningonAccountof'].touched) && form.controls['joiningonAccountof'].invalid && form.controls['joiningonAccountof'].errors.pattern\">\r\n            Join Service As is invalid\r\n          </mat-error>-->\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Joining Order No.\" formControlName=\"joiningOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_]+\" required>\r\n          <mat-error *ngIf=\"(!form.controls['joiningOrderNo'].valid || form.controls['joiningOrderNo'].dirty || form.controls['joiningOrderNo'].touched) && form.controls['joiningOrderNo'].invalid && form.controls['joiningOrderNo'].errors.required\">\r\n            Joining Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['joiningOrderNo'].dirty || form.controls['joiningOrderNo'].touched) && form.controls['joiningOrderNo'].invalid && form.controls['joiningOrderNo'].errors.pattern\">\r\n            Joining Order Number is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"JoiningOrderDate\" placeholder=\"Joining Order Date\" [min]=\"form.controls['relievingOrderDate'].value\" (click)=\"JoiningOrderDate.open()\" formControlName=\"joiningOrderDate\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"JoiningOrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #JoiningOrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['joiningOrderDate'].invalid && form.controls['joiningOrderDate'].errors.required\">\r\n            Joining Order Date is required\r\n          </mat-error>\r\n         \r\n          <!--<mat-error *ngIf=\"form.controls['joiningOrderDate'].errors?.mustMatch\">\r\n            Joining Order Date is invalid\r\n          </mat-error>-->\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n\r\n          <mat-select placeholder=\"Time\" formControlName=\"joiningTime\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let joiningTime of joiningTimeDdl\" [value]=\"joiningTime\">{{joiningTime}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joiningTime'].valid || form.controls['joiningTime'].dirty || form.controls['joiningTime'].touched) && form.controls['joiningTime'].invalid && form.controls['joiningTime'].errors.required\">\r\n            Time is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Office Name\" formControlName=\"officeId\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let getofficelists of getofficelist\" [value]=\"getofficelists.officeId\">{{getofficelists.officeName}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['officeId'].valid || form.controls['officeId'].dirty || form.controls['officeId'].touched) && form.controls['officeId'].invalid && form.controls['officeId'].errors.required\">\r\n            Office Name is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Office City Class\" formControlName=\"officeCityClass\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let officecityclass of officeCityClassList\" [value]=\"officecityclass.officeClassCityId\">{{officecityclass.officeClassCityName}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['officeCityClass'].valid  || form.controls['officeCityClass'].dirty || form.controls['officeCityClass'].touched) && form.controls['officeCityClass'].invalid && form.controls['officeCityClass'].errors.required\">\r\n            Office City Class is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n\r\n          <mat-select placeholder=\"Designation\" formControlName=\"joiningDesigcode\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let designa of designation\" [value]=\"designa.desigId\">\r\n              {{designa.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['joiningDesigcode'].valid || form.controls['joiningDesigcode'].dirty || form.controls['joiningDesigcode'].touched) && form.controls['joiningDesigcode'].invalid && form.controls['joiningDesigcode'].errors.required\">\r\n            Designation is Required\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"form.controls['employeeCode'].invalid\">{{btnUpdateText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" [disabled]=\"form.controls['employeeCode'].invalid\" (click)=\"ResetForm();\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"!disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n      <!--<div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n      //  <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"savebuttonstatus\" [disabled]=\"form.invalid\">{{btnUpdateText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>-->\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <!--<mat-accordion>\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          Form value\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      <code>\r\n        {{form.value | json}}\r\n      </code>\r\n    </mat-expansion-panel>\r\n  </mat-accordion>-->\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\" [(ngModel)]=\"tblfiltervalue\" (keypress)=\"charaterOnlyNoSpace($event)\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"nonEpsLienPeriodData\" matSort class=\"mat-elevation-z8 even-odd-color\" matSortActive=\"joiningOrderNo\" matSortDirection=\"asc\">\r\n      <!-- Name Column -->\r\n\r\n      <ng-container matColumnDef=\"joiningOrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Joining Order  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.joiningOrderNo}} </td>\r\n\r\n\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"joiningOrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Joining Order Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.joiningOrderDate | date}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"relievingOrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Relieving Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"relievingOrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Relieving Order Date. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOrderDate | date}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"relievingOffice\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Office Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOffice}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\" >Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef> Action </th>\r\n        <mat-cell *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'R' || element.veriFlag == 'U')&& roleId == 6\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.msAfterLeinId)\">\r\n            edit\r\n          </a>\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U' ||  element.veriFlag == 'R')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRecordClick(element.msAfterLeinId);DeletePopup=!DeletePopup\">\r\n            delete_forever\r\n          </a>\r\n          <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.msAfterLeinId)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.msAfterLeinId)\">\r\n            info\r\n          </a>\r\n        </mat-cell>\r\n\r\n      </ng-container>\r\n      <!--<ng-container matColumnDef=\"action\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n\r\n        <button type=\"button\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"this.getEpsEmpJoinOffAfterLienPeriodDetails(element.employeeCode,false,this.roleId,'E')\" [disabled]=\"disableflag\">\r\n          edit\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"this.deleteEpsEmpJoinAfterLienPeriod(element.employeeCode)\" [disabled]=\"disableflag\">delete_forever</button>\r\n      </td>\r\n    </ng-container>-->\r\n      <!--<tr *ngIf=\"nonEpsLienPeriodData?.length> 0\">\r\n        <td colspan=\"3\" class=\"no-data-available\">No data!</td>\r\n      </tr>-->\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\" class=\"add-leave-btn\">\r\n\r\n      <font color=red> Record Not Found </font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5,10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure want to delete?<br/> <br/></h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to approve? <br /> <br /></h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form (ngSubmit)=\"form2.valid && forwardStatusUpdate('R')\" #freject=\"ngForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput [(ngModel)]=\"rejectionRemark\" name=\"rejectionRemark1\" #rejectionRemark1=\"ngModel\" rows=\"2\" maxlength=\"50\" align=\"center\" placeholder=\"Remarks\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_,. ]+\" required></textarea>\r\n        <mat-error>\r\n          <span [hidden]=\"!rejectionRemark1.errors?.required\">Remarks is required</span>\r\n          <!--<span [hidden]=\"!rejectionRemark.errors?.required\">Service Type is required</span>-->\r\n          <!--<span *ngIf=\"!rejectionRemark.valid\">Remarks Required !</span>-->\r\n        </mat-error>\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: NonEpsEmployeeJoinAfterLienComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NonEpsEmployeeJoinAfterLienComponent", function() { return NonEpsEmployeeJoinAfterLienComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_lienperiod_nonepsemployeejoinafterlienperiod_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/lienperiod/nonepsemployeejoinafterlienperiod.service */ "./src/app/services/lienperiod/nonepsemployeejoinafterlienperiod.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NonEpsEmployeeJoinAfterLienComponent = /** @class */ (function () {
    function NonEpsEmployeeJoinAfterLienComponent(master, _formBuilder, snackBar, nonepsemployeejoinafterlienperiodService) {
        this.master = master;
        this._formBuilder = _formBuilder;
        this.snackBar = snackBar;
        this.nonepsemployeejoinafterlienperiodService = nonepsemployeejoinafterlienperiodService;
        this.joiningTimeDdl = ['ForeNoon', 'AfterNoon'];
        this.designation = [];
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.rejectionRemark = '';
        this.tblfiltervalue = '';
        this.lienId = '';
        this.notFound = true;
        this.displayedColumns = ['joiningOrderNo', 'joiningOrderDate', 'relievingOrderNo', 'relievingOrderDate', 'relievingOffice', 'Status', 'action'];
        this.disableflag = true;
        this.disableForwardflag = false;
        this.valuw = [];
    }
    NonEpsEmployeeJoinAfterLienComponent.prototype.ngOnInit = function () {
        this.roleId = sessionStorage.getItem('userRoleID');
        this.btnUpdateText = 'Save';
        this.FormDetails();
        this.getAllDesignation(sessionStorage.getItem('controllerID'));
        this.getJoinServices();
        this.getJoiningAccountOf();
        this.getOfficeList();
        this.getOfficeCityClassList();
        this.getPayCommissionLien();
        // this.getAllDesignation(sessionStorage.getItem('controllerID'));
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.FormDetails = function () {
        this.form = this._formBuilder.group({
            'employeeCode': [this.empCd, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOrderNo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOrderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingOffice': [null],
            'relievingBy': [null],
            'relievedOn': [null],
            'onRelievedDesignation': [null],
            'payCommissionId': [null],
            'payCommission': [null],
            'payScale': [null],
            'basicPay': [null],
            'joinServiceAs': [null],
            'joiningonAccountof': [null],
            'joiningOrderNo': [null],
            'joiningOrderDate': [null],
            'joiningTime': [null],
            'officeId': [null],
            'officeCityClass': [null],
            'joiningDesigcode': [null],
            'createdBy': [null],
            'existRecordAfterLien': [null],
            'flagUpdate': [null],
            'msAfterLeinId': [null],
            'rejectionRemark': [null],
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getJoinServices = function () {
        var _this = this;
        this.master.getJoiningMode().subscribe(function (res) {
            debugger;
            _this.joinService = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getJoiningAccountOf = function () {
        var _this = this;
        debugger;
        this.master.getJoiningAccountOf().subscribe(function (res) {
            debugger;
            _this.getJoiningAccount = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getPayCommissionLien = function () {
        var _this = this;
        this.master.getPayCommissionListLien().subscribe(function (res) {
            debugger;
            _this.getPayCommission = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getOfficeList = function () {
        var _this = this;
        this.master.getOfficeList(sessionStorage.getItem('controllerID'), sessionStorage.getItem('ddoid')).subscribe(function (res) {
            debugger;
            _this.getofficelist = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.ltrim = function (tblfiltervalue) {
        return tblfiltervalue.replace(/^\s+/g, '');
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.applyFilter = function (filterValue) {
        debugger;
        this.tblfiltervalue = this.ltrim(this.tblfiltervalue);
        if (this.nonEpsLienPeriodData === null || this.nonEpsLienPeriodData === undefined) {
            this.notFound = false;
        }
        this.nonEpsLienPeriodData.filter = filterValue.trim().toLowerCase();
        this.nonEpsLienPeriodData.filterPredicate = function (data, filter) {
            debugger;
            return data.joiningOrderNo.toLowerCase().includes(filter) || data.joiningOrderDate.includes(filter) || data.relievingOrderNo.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.relievingOffice.toLowerCase().includes(filter);
        };
        if (this.nonEpsLienPeriodData.paginator) {
            this.nonEpsLienPeriodData.paginator.firstPage();
        }
        if (this.nonEpsLienPeriodData.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getAllDesignation = function (controllerId) {
        var _this = this;
        this.master.GetAllDesignation(controllerId).subscribe(function (res) {
            debugger;
            _this.designation = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.charaterOnlyNoSpace = function (event) {
        debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getOfficeCityClassList = function () {
        var _this = this;
        this.master.getOfficeCityClassList().subscribe(function (res) {
            _this.officeCityClassList = res;
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.btnEditClick = function (lienId) {
        debugger;
        var value = this.nonEpsLienPeriodData.filteredData.filter(function (x) { return x.msAfterLeinId == lienId; })[0];
        this.form.patchValue(value);
        this.form.enable();
        this.btnUpdateText = 'Update';
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
            //this.Btntxt = 'Save';
        }
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.btnInfoClick = function (lienId) {
        debugger;
        var value = this.nonEpsLienPeriodData.filteredData.filter(function (x) { return x.msAfterLeinId == lienId; })[0];
        this.form.patchValue(value);
        this.form.disable();
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableForwardflag = false;
        }
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.confirmDelete = function () {
        var _this = this;
        debugger;
        debugger;
        var value = this.nonEpsLienPeriodData.filteredData.filter(function (x) { return x.msAfterLeinId == _this.lienId; })[0];
        var orderNo = value.joiningOrderNo;
        this.nonepsemployeejoinafterlienperiodService.deleteNonEpsTransferOffLienPeriod(this.empCd, orderNo).subscribe(function (result) {
            if (parseInt(result) >= 1) {
                _this.snackBar.open('Record Successfully Deleted ', null, { duration: 4000 });
                _this.formDirective.resetForm();
                _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId, true, "");
                _this.Cancel();
            }
            else {
                _this.snackBar.open('Record Not Deleted ', null, { duration: 4000 });
            }
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.deleteRecordClick = function (lienId) {
        this.lienId = lienId;
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.GetcommanMethod = function (value) {
        this.empCd = value;
        this.form.get('employeeCode').setValue(value);
        this.getEpsEmpJoinOffAfterLienPeriodDetails(value, this.roleId, true, "");
        //this.form.get('employeeCode').setValue(value);
        // this.btnUpdateText = 'Save';
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.getEpsEmpJoinOffAfterLienPeriodDetails = function (Empcd, roleId, Flag, insertEditFlag) {
        var _this = this;
        this.nonepsemployeejoinafterlienperiodService.getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(Empcd, roleId).subscribe(function (result) {
            debugger;
            if (result.length == 0) {
                _this.formDirective.resetForm();
                _this.form.enable();
                _this.nonEpsLienPeriodData = null;
                _this.form.get('employeeCode').setValue(_this.empCd);
            }
            else {
                _this.nonEpsLienPeriodData = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result);
                _this.nonEpsLienPeriodData.paginator = _this.paginator;
                _this.nonEpsLienPeriodData.sort = _this.sort;
                _this.form.get('employeeCode').setValue(_this.empCd);
                //this.disableForwardflag = true;
            }
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.ResetForm = function () {
        this.form.reset();
        this.formDirective.resetForm();
        this.form.enable();
        this.btnUpdateText = 'Save';
        this.disableForwardflag = false;
        this.form.get('employeeCode').setValue(this.empCd);
        //this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        debugger;
        debugger;
        var RejectionComment = '';
        var employeeId = this.empCd.trim();
        var joiningOrderNo = this.form.get('joiningOrderNo').value;
        if (statusFlag == 'R') {
            RejectionComment = this.rejectionRemark;
        }
        else {
            RejectionComment = '';
        }
        this.nonepsemployeejoinafterlienperiodService.forwardNonEpsEmployeeStatusUpdate(employeeId, joiningOrderNo, statusFlag, RejectionComment).subscribe(function (result1) {
            if (parseInt(result1) >= 1) {
                _this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
                _this.form.reset();
                _this.formDirective.resetForm();
                _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId, true, "");
                _this.infoScreen = false;
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
                _this.rejectionRemark = '';
                _this.btnUpdateText = 'Save';
                _this.disableForwardflag = false;
            }
        });
    };
    NonEpsEmployeeJoinAfterLienComponent.prototype.onSubmit = function () {
        var _this = this;
        debugger;
        //this.submitted = true;
        this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        else {
            if (this.btnUpdateText == 'Update') {
                this.form.get('flagUpdate').setValue('update');
            }
            else {
                this.form.get('flagUpdate').setValue('insert');
                var employeeid = this.form.get('employeeCode').value;
                if (employeeid == 0) {
                    alert("Select Employee");
                    return;
                }
            }
            this.nonepsemployeejoinafterlienperiodService.InsertUpateNonEpsEmployeeJoinAfterLienPeriod(this.form.value).subscribe(function (result1) {
                debugger;
                var resultvalue = result1.split("-");
                if (parseInt(resultvalue[0]) >= 1) {
                    if (_this.btnUpdateText == 'Update') {
                        _this.snackBar.open('Update Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId, true, "");
                        _this.btnUpdateText = 'Save';
                        _this.disableForwardflag = false;
                    }
                    else {
                        _this.snackBar.open('Save Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getEpsEmpJoinOffAfterLienPeriodDetails(_this.empCd, _this.roleId, true, "");
                        _this.btnUpdateText == 'Save';
                        _this.disableForwardflag = false;
                    }
                }
                else {
                    if (_this.btnUpdateText == 'Update') {
                        if (resultvalue[2] = 'Duplicate Record') {
                            _this.snackBar.open('Joining Order Number  Already Exits', null, { duration: 4000 });
                        }
                        else {
                            _this.snackBar.open('Update not Successfully', null, { duration: 4000 });
                        }
                    }
                    else if (resultvalue[2] = 'Duplicate Record') {
                        _this.snackBar.open('Joining Order Number  Already Exits', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Save not Successfully', null, { duration: 4000 });
                    }
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('freject'),
        __metadata("design:type", Object)
    ], NonEpsEmployeeJoinAfterLienComponent.prototype, "form2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], NonEpsEmployeeJoinAfterLienComponent.prototype, "formDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], NonEpsEmployeeJoinAfterLienComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], NonEpsEmployeeJoinAfterLienComponent.prototype, "sort", void 0);
    NonEpsEmployeeJoinAfterLienComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-non-eps-employee-join-after-lien',
            template: __webpack_require__(/*! ./non-eps-employee-join-after-lien.component.html */ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.html"),
            styles: [__webpack_require__(/*! ./non-eps-employee-join-after-lien.component.css */ "./src/app/lienperiod/non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_2__["MasterService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"], _services_lienperiod_nonepsemployeejoinafterlienperiod_service__WEBPACK_IMPORTED_MODULE_3__["NonepsemployeejoinafterlienperiodService"]])
    ], NonEpsEmployeeJoinAfterLienComponent);
    return NonEpsEmployeeJoinAfterLienComponent;
}());



/***/ }),

/***/ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgorange {\r\n  color: orange;\r\n}\r\n\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n\r\n.bggreen {\r\n  color: green;\r\n}\r\n\r\n.bgblue {\r\n  color: blue;\r\n}\r\n\r\n.bgred {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGllbnBlcmlvZC90cmFuc2Zlci1vZmZpY2UtbGllbnBlcmlvZC90cmFuc2Zlci1vZmZpY2UtbGllbnBlcmlvZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvbGllbnBlcmlvZC90cmFuc2Zlci1vZmZpY2UtbGllbnBlcmlvZC90cmFuc2Zlci1vZmZpY2UtbGllbnBlcmlvZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnb3JhbmdlIHtcclxuICBjb2xvcjogb3JhbmdlO1xyXG59XHJcblxyXG4uYmdkYXJrb3JhbmdlIHtcclxuICBjb2xvcjogZGFya29yYW5nZTtcclxufVxyXG5cclxuLmJnZ3JlZW4ge1xyXG4gIGNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuLmJnYmx1ZSB7XHJcbiAgY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5iZ3JlZCB7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [events]=\"eventsSubject.asObservable()\"></app-comman-mst>\r\n<form (ngSubmit)=\"form.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" novalidate>\r\n  <div class=\"col-md-12 col-lg-7\" [attr.disabled]=\"disableflag\">\r\n    <mat-card class=\"example-card emp1-form\">\r\n      <ng-template matStepLabel>Lien Period/Extension of Lien Period</ng-template>\r\n\r\n      <div class=\"fom-title\">Lien Period/Extension of Lien Period</div>\r\n      <div class=\"fom-title\">Order Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order Number\" formControlName=\"orderNo\" maxlength=\"50\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_]+\" required>\r\n          <mat-error *ngIf=\"(!form.controls['orderDate'].valid || form.controls['orderNo'].dirty || form.controls['orderNo'].touched) && form.controls['orderNo'].invalid && form.controls['orderNo'].errors.required\">\r\n            Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['orderNo'].dirty || form.controls['orderNo'].touched) && form.controls['orderNo'].invalid && form.controls['orderNo'].errors.pattern\">\r\n            Order Number is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" placeholder=\"Order Date\" (click)=\"OrderDate.open()\" formControlName=\"orderDate\" required readonly />\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['orderDate'].invalid || !form.controls['orderDate'].valid && form.controls['orderDate'].touched\">\r\n            Order Date is required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Reason for Transfer\" formControlName=\"reasonTransfer\" (selectionChange)=\"getTransferReasonType()\" required>\r\n            <mat-option value=\"\">--Select--</mat-option>\r\n            <mat-option *ngFor=\"let transferresond of reasonTransferDdl\" [value]=\"transferresond\">{{transferresond}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"(!form.controls['reasonTransfer'].valid || form.controls['reasonTransfer'].dirty || form.controls['reasonTransfer'].touched) && form.controls['reasonTransfer'].invalid && form.controls['reasonTransfer'].errors.required\">\r\n            Reason for Transfer is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\" formControlName=\"remarks\" maxlength=\"200\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_ ]+\" required></textarea>\r\n          <mat-error *ngIf=\"(!form.controls['remarks'].valid ||form.controls['remarks'].dirty || form.controls['remarks'].touched) && form.controls['remarks'].invalid && form.controls['remarks'].errors.required\">\r\n            Remarks is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['remarks'].dirty || form.controls['remarks'].touched) && form.controls['remarks'].invalid && form.controls['remarks'].errors.pattern\">\r\n            Remarks is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"wid-100\" *ngIf=\"relHideShow\">\r\n        <div class=\"fom-title\">Relieving Details</div>\r\n\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Relieving Order Number\" formControlName=\"relevingOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z0-9]{1}[A-Za-z0-9-/_]+\" required>\r\n            <mat-error *ngIf=\"(!form.controls['relevingOrderNo'].valid || form.controls['relevingOrderNo'].dirty || form.controls['relevingOrderNo'].touched) && form.controls['relevingOrderNo'].invalid && form.controls['relevingOrderNo'].errors.required\">\r\n              Relieving Order Number is Required\r\n            </mat-error>\r\n            <mat-error *ngIf=\"(form.controls['relevingOrderNo'].dirty || form.controls['relevingOrderNo'].touched) && form.controls['relevingOrderNo'].invalid && form.controls['relevingOrderNo'].errors.pattern\">\r\n              Relieving Order Number is invalid\r\n            </mat-error>\r\n           \r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"RelievingOrderDate\" placeholder=\"Order Date\" [min]=\"form.controls['orderDate'].value\" (click)=\"RelievingOrderDate.open();\" (Onblur)=\"CheckDate()\" formControlName=\"relievingOrderDate\" required readonly />\r\n            <mat-datepicker-toggle matSuffix [for]=\"RelievingOrderDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #RelievingOrderDate></mat-datepicker>\r\n            <mat-error *ngIf=\"form.controls['relievingOrderDate'].invalid && form.controls['relievingOrderDate'].errors.required\">\r\n              Relieving Order Date is required\r\n            </mat-error>\r\n\r\n            <!--<mat-error *ngIf=\"!form.controls['relievingOrderDate'].valid\">\r\n              Relieving Order Date is invalid\r\n            </mat-error>-->\r\n          </mat-form-field>\r\n\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n\r\n          <mat-form-field class=\"wid-100 example-full-width\">\r\n            <mat-select placeholder=\"Time\" formControlName=\"relievingTime\" required>\r\n              <mat-option value=\"\">--Select--</mat-option>\r\n              <mat-option *ngFor=\"let relievingTime of relievingTimeDdl\" [value]=\"relievingTime\">{{relievingTime}}</mat-option>\r\n            </mat-select>\r\n            <mat-error *ngIf=\"(!form.controls['relievingTime'].valid || form.controls['relievingTime'].dirty || form.controls['relievingTime'].touched) && form.controls['relievingTime'].invalid && form.controls['relievingTime'].errors.required\">\r\n              Time is Required\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n\r\n\r\n            <mat-select placeholder=\"Designation\" formControlName=\"relievingDesigcode\" required>\r\n              <mat-option value=\"\">--Select--</mat-option>\r\n              <mat-option *ngFor=\"let designa of designation\" [value]=\"designa.desigId\">\r\n                {{designa.desigDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n            <mat-error *ngIf=\"(!form.controls['relievingTime'].valid || form.controls['relievingDesigcode'].dirty || form.controls['relievingDesigcode'].touched) && form.controls['relievingDesigcode'].invalid && form.controls['relievingDesigcode'].errors.required\">\r\n              Designation is Required\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"fom-title\">Office Details</div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-radio-group formControlName=\"department\">\r\n          <mat-radio-button value=\"Civil\" (click)=\"showCivilValue = !showCivilValue; getDepartment('Civil')\"> Civil </mat-radio-button>\r\n          <mat-radio-button value=\"Other\" (click)=\"getDepartment('Other')\"> Other </mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Office Name\" [readonly]=\"this.isOfFEditable\"  formControlName=\"officeName\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-|/_(),.& ]+\" required>\r\n          <mat-error *ngIf=\"(form.controls['officeName'].dirty || !form.controls['officeName'].valid || form.controls['officeName'].touched) && form.controls['officeName'].invalid && form.controls['officeName'].errors.required\">\r\n            Office Name is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['officeName'].invalid && form.controls['officeName'].errors.pattern\">\r\n            Office Name is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"paoDdoHideShow\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"PAO Name\" formControlName=\"paoName\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-|/_(),.& ]+\" required readonly>\r\n          <mat-error *ngIf=\"(form.controls['paoName'].dirty || !form.controls['paoName'].valid || form.controls['paoName'].touched) && form.controls['paoName'].invalid && form.controls['paoName'].errors.required\">\r\n            PAO Name is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['paoName'].dirty  || form.controls['paoName'].touched) && form.controls['paoName'].invalid && form.controls['paoName'].errors.pattern\">\r\n            PAO Name is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"paoDdoHideShow\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"DDO Name\" formControlName=\"ddoName\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-|/_(),.& ]+\" required readonly>\r\n          <mat-error *ngIf=\"(form.controls['ddoName'].dirty || !form.controls['ddoName'].valid  || form.controls['ddoName'].touched) && form.controls['ddoName'].invalid && form.controls['ddoName'].errors.required\">\r\n            DDO Name is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['ddoName'].dirty || form.controls['ddoName'].touched) && form.controls['ddoName'].invalid && form.controls['ddoName'].errors.pattern\">\r\n            DDO Name is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"col-md-6 col-lg-4\" *ngIf=\"OffHideShow\">\r\n    <mat-form-field class=\"wid-100\">\r\n      <input matInput placeholder=\"Office Name\" formControlName=\"officeNameother\" required>\r\n      <mat-error *ngIf=\"(form.controls['officeNameother'].dirty || form.controls['officeNameother'].touched) && form.controls['officeNameother'].invalid && form.controls['officeNameother'].errors.required\">\r\n        Office Name is Required\r\n      </mat-error>\r\n      <mat-error *ngIf=\"(form.controls['officeNameother'].dirty || form.controls['officeNameother'].touched) && form.controls['officeNameother'].invalid && form.controls['officeNameother'].errors.pattern\">\r\n        Office Name is invalid\r\n      </mat-error>\r\n    </mat-form-field>\r\n  </div>-->\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"OffHideShow\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Office Address\"  formControlName=\"officeAddress\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_(), ]+\" required style=\"margin: -10px 0;\"></textarea>\r\n          <mat-error *ngIf=\"(form.controls['officeAddress'].dirty || !form.controls['officeAddress'].valid || form.controls['officeAddress'].touched) && form.controls['officeAddress'].invalid && form.controls['officeAddress'].errors.required\">\r\n            Office Address is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"(form.controls['officeAddress'].dirty || form.controls['officeAddress'].touched) && form.controls['officeAddress'].invalid && form.controls['officeAddress'].errors.pattern\">\r\n            Office Address is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_ ]+\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"savebuttonstatus\" [disabled]=\"form.controls['employeeCode'].invalid\">{{btnUpdateText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\" [disabled]=\"form.controls['employeeCode'].invalid\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"!disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <!--<mat-accordion>\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          Form value\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      <code>\r\n        {{form.value | json}}\r\n      </code>\r\n    </mat-expansion-panel>\r\n  </mat-accordion>-->\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" [(ngModel)]=\"tblfiltervalue\" placeholder=\"Search\" (keypress)=\"charaterOnlyNoSpace($event)\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"tranferLienPeriodData\" matSort class=\"mat-elevation-z8 even-odd-color\" matSortActive=\"orderNo\" matSortDirection=\"asc\">\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"orderNo\">\r\n\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order No </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element;\"> {{element.orderNo}}  </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"orderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.orderDate | date}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"relievingOrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Relieving Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relevingOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"relievingOrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Relieving Order Date. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.relievingOrderDate | date}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"officeName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Office Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.officeName}} </td>\r\n      </ng-container> \r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\">Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <mat-cell *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'R' || element.veriFlag == 'U')&& roleId == 6\"  class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.mslienId)\">edit</a>\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U' || element.veriFlag == 'R')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRecordClick(element.mslienId);DeletePopup=!DeletePopup\">\r\n            delete_forever\r\n          </a>\r\n          <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.mslienId)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.mslienId)\">\r\n            info\r\n          </a>\r\n        </mat-cell>\r\n\r\n      </ng-container>\r\n\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\" class=\"add-leave-btn\">\r\n\r\n      <font color=red> Record Not Found </font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5,10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n \r\n</div>\r\n<app-dialog [(visible)]=\"showCivilValue\">\r\n  <ng-template matStepLabel>Search to Select Office</ng-template>\r\n\r\n  <div class=\"fom-title\">Search to Select Office</div>\r\n  <form [formGroup]=\"formPopUp\" style=\"height:500px\" #formDirectivepop=\"ngForm\">\r\n\r\n    <legend>Search Office</legend>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n\r\n      <mat-form-field class=\"wid-100 example-full-width\">\r\n        <mat-select placeholder=\"Controller Name\" formControlName=\"controllerId\" (selectionChange)=\"bindddolist()\" required>\r\n          <mat-option value=\"\">--Select--</mat-option>\r\n          <mat-option *ngFor=\"let controllerListd of controllerList\" [value]=\"controllerListd.controllerId\">{{controllerListd.controllerName}}</mat-option>\r\n        </mat-select>\r\n        <mat-error *ngIf=\"(formPopUp.controls['controllerId'].dirty || !formPopUp.controls['controllerId'].valid || formPopUp.controls['controllerId'].touched) && formPopUp.controls['controllerId'].invalid && formPopUp.controls['controllerId'].errors.required\">\r\n          Controller Name is Required\r\n        </mat-error>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n\r\n      <mat-form-field class=\"wid-100 example-full-width\">\r\n        <mat-select placeholder=\"DDO Name\" formControlName=\"ddoId\" required>\r\n          <mat-option value=\"\">--Select--</mat-option>\r\n          <mat-option *ngFor=\"let ddoLists of ddoList\" [value]=\"ddoLists.ddoId\">{{ddoLists.ddoName}}</mat-option>\r\n        </mat-select>\r\n        <mat-error *ngIf=\"(formPopUp.controls['ddoId'].dirty || !formPopUp.controls['ddoId'].valid || formPopUp.controls['ddoId'].touched) && formPopUp.controls['ddoId'].invalid && formPopUp.controls['ddoId'].errors.required\">\r\n          DDO Name is Required\r\n        </mat-error>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Office Name\" formControlName=\"officeName\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_ ]+\">\r\n        <!--<mat-error *ngIf=\"(form.controls['officeAddress'].dirty || form.controls['officeAddress'].touched) && form.controls['officeAddress'].invalid && form.controls['officeAddress'].errors.required\">\r\n          Office Address is Required\r\n        </mat-error>\r\n        <mat-error *ngIf=\"(form.controls['officeAddress'].dirty || form.controls['officeAddress'].touched) && form.controls['officeAddress'].invalid && form.controls['officeAddress'].errors.pattern\">\r\n          Office Address is invalid\r\n        </mat-error>-->\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\" (click)=\"GetAllDetails()\">GO</button>\r\n      <button type=\"button\" class=\"btn btn-warning\" (click)=\"ClearPop()\">Clear</button>\r\n\r\n\r\n    </div>\r\n    <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n\r\n      <table mat-table [dataSource]=\"tranferOfficeData\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <div class=\"col-md-12 col-lg-3\">\r\n          <ng-container matColumnDef=\"checked\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Select </th>\r\n\r\n            <mat-cell *matCellDef=\"let element\">\r\n              <mat-radio-button value=\"{{element.officeId}}\" (click)=\"GetSelectCode(element.officeId)\"></mat-radio-button>\r\n              <!--<mat-checkbox [(ngModel)]=\"element.checked\"></mat-checkbox>-->\r\n            </mat-cell>\r\n          </ng-container>\r\n        </div>\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"OfficeName\">\r\n\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Office Name </th>\r\n\r\n          <td mat-cell *matCellDef=\"let element;\"> {{element.officeName}}  </td>\r\n        </ng-container>\r\n\r\n        <!-- Date Of Birth Column -->\r\n        <ng-container matColumnDef=\"OfficeAddress\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Office Address </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.officeAddress}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Date Of Joining Column -->\r\n        <ng-container matColumnDef=\"TelephoneNo\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Telephone No. </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.officeTelePhoneNO}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"E-MailId\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> E-Mail Id </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.officeEmailId}} </td>\r\n        </ng-container>\r\n\r\n\r\n        <!--<tr *ngIf=\"dataSource?.length> 0\">\r\n          <td colspan=\"3\" class=\"no-data-available\">No data!</td>\r\n        </tr>-->\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\"></tr>\r\n      </table>\r\n\r\n\r\n    </div>\r\n  </form>\r\n\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure want to delete? <br /> <br /></h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to approve? <br /> <br /></h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form  (ngSubmit)=\"form2.valid && forwardStatusUpdate('R')\" #freject=\"ngForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput [(ngModel)]=\"rejectionRemark\" name=\"rejectionRemark1\" #rejectionRemark1=\"ngModel\" rows=\"2\" maxlength=\"50\" align=\"center\" placeholder=\"Remarks\" pattern=\"[A-Za-z]{1}[A-Za-z0-9-/_., ]+\" required></textarea>\r\n        <mat-error>\r\n          <span [hidden]=\"!rejectionRemark1.errors?.required\">Remarks is required</span>\r\n        </mat-error>\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: TransferOfficeLienperiodComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferOfficeLienperiodComponent", function() { return TransferOfficeLienperiodComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_lienperiod_transfer_office_lienperiod_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/lienperiod/transfer-office-lienperiod.service */ "./src/app/services/lienperiod/transfer-office-lienperiod.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TransferOfficeLienperiodComponent = /** @class */ (function () {
    function TransferOfficeLienperiodComponent(master, _formBuilder, transferOfficeLienperiodService, snackBar) {
        this.master = master;
        this._formBuilder = _formBuilder;
        this.transferOfficeLienperiodService = transferOfficeLienperiodService;
        this.snackBar = snackBar;
        this.OffHideShow = false;
        this.paoDdoHideShow = true;
        this.designation = [];
        this.disableForwardflag = false;
        this.eventsSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.disableflag = true;
        this.displayedColumns = ['orderNo', 'orderDate', 'relievingOrderNo', 'relievingOrderDate', 'officeName', 'Status', 'action'];
        this.reasonTransferDdl = ['Technical Resignation With Lien Period'];
        this.relievingTimeDdl = ['ForeNoon', 'AfterNoon'];
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.rejectionRemark = '';
        this.tblfiltervalue = '';
        this.isOfFEditable = true;
        this.lienId = '';
        this.notFound = true;
        this.displayedColumns2 = ['checked', 'OfficeName', 'OfficeAddress', 'TelephoneNo', 'E-MailId'];
        //displayedColumns2: string[] = ['Office Name', 'Office Address', 'Telephone No', 'E-Mail Id'];
        this.officeAddressDetails = [];
    }
    TransferOfficeLienperiodComponent.prototype.ngOnInit = function () {
        this.roleId = sessionStorage.getItem('userRoleID');
        this.btnUpdateText = 'Save';
        this.savebuttonstatus = true;
        this.FormDetails();
        this.formPopUpDetails();
        this.getControllerList();
        this.getAllDesignation(sessionStorage.getItem('controllerID'));
    };
    TransferOfficeLienperiodComponent.prototype.GetcommanMethod = function (value) {
        debugger;
        this.empCd = value;
        this.form.get('employeeCode').setValue(value);
        this.getTransfer_office_lien_Period_Details(value, this.roleId);
        this.form.get('employeeCode').setValue(this.empCd);
        // this.btnUpdateText = 'Save';
    };
    TransferOfficeLienperiodComponent.prototype.getTransferReasonType = function () {
        if (this.form.get('reasonTransfer').value !== "") {
            this.relHideShow = true;
        }
        else {
            this.relHideShow = false;
        }
    };
    TransferOfficeLienperiodComponent.prototype.bindddolist = function () {
        this.getDdoListByControllerId(this.formPopUp.get('controllerId').value);
    };
    TransferOfficeLienperiodComponent.prototype.FormDetails = function () {
        this.form = this._formBuilder.group({
            'employeeCode': [this.empCd, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'orderNo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'orderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'reasonTransfer': [null],
            'remarks': [null],
            'relevingOrderNo': [null],
            'relievingOrderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingTime': [null],
            'relievingDesigcode': [null],
            'department': [null],
            'officeId': [null],
            'officeName': [null],
            'officeNameother': [null],
            'paoId': [null],
            'paoName': [null],
            'ddoId': [null],
            'ddoName': [null],
            'officeAddress': [null],
            'createdBy': [null],
            'rowNumber': [null],
            'flagUpdate': [null],
            'rejectionRemark': [null],
            'mslienId': [null]
        });
    };
    TransferOfficeLienperiodComponent.prototype.dateLessThan = function (from, to) {
        return function (group) {
            var f = group.controls[from];
            var t = group.controls[to];
            debugger;
            if (f.value > t.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()("Date from should be less than Date to");
            }
            return {};
        };
    };
    TransferOfficeLienperiodComponent.prototype.CheckDate = function () {
        debugger;
        if (this.form.get('relievingOrderDate').value >= this.form.get('orderDate').value) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()("Alert Hello");
        }
    };
    TransferOfficeLienperiodComponent.prototype.btnEditClick = function (lienId) {
        var value = this.tranferLienPeriodData.filteredData.filter(function (x) { return x.mslienId == lienId; })[0];
        this.getDepartment(value.department);
        this.form.patchValue(value);
        this.form.enable();
        this.btnUpdateText = 'Update';
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            this.disableflag = true;
            this.disableForwardflag = true;
            //this.Btntxt = 'Save';
        }
    };
    TransferOfficeLienperiodComponent.prototype.btnInfoClick = function (lienId) {
        var value = this.tranferLienPeriodData.filteredData.filter(function (x) { return x.mslienId == lienId; })[0];
        if (value.department == 'Other') {
            this.OffHideShow = true;
            this.paoDdoHideShow = false;
        }
        else {
            this.OffHideShow = false;
            this.paoDdoHideShow = true;
        }
        this.form.patchValue(value);
        this.form.disable();
        //let empStatus = value.veriFlag;
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            this.disableForwardflag = false;
            //this.Btntxt = 'Save';
        }
    };
    TransferOfficeLienperiodComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    TransferOfficeLienperiodComponent.prototype.getDepartment = function (valueId) {
        this.ClearPop();
        if (valueId == 'Other') {
            this.OffHideShow = true;
            this.paoDdoHideShow = false;
            this.form.get('paoId').clearValidators();
            this.form.get('paoId').updateValueAndValidity();
            this.form.get('paoId').setValue("");
            this.form.get('paoName').clearValidators();
            this.form.get('paoName').updateValueAndValidity();
            this.form.get('paoName').setValue("");
            this.form.get('ddoId').clearValidators();
            this.form.get('ddoId').updateValueAndValidity();
            this.form.get('ddoId').setValue("");
            this.form.get('ddoName').clearValidators();
            this.form.get('ddoName').updateValueAndValidity();
            this.form.get('ddoName').setValue("");
            this.form.get('officeId').clearValidators();
            this.form.get('officeId').updateValueAndValidity();
            this.form.get('officeId').setValue("");
            this.form.get('officeName').setValue("");
            this.isOfFEditable = false;
            this.showCivilValue = false;
        }
        else {
            this.form.get('officeAddress').clearValidators();
            this.form.get('officeAddress').updateValueAndValidity();
            this.form.get('officeAddress').setValue("");
            this.form.get('officeName').setValue("");
            this.OffHideShow = false;
            this.paoDdoHideShow = true;
            this.isOfFEditable = true;
        }
        //alert("test");
    };
    TransferOfficeLienperiodComponent.prototype.formPopUpDetails = function () {
        this.formPopUp = this._formBuilder.group({
            'controllerId': [null],
            'ddoId': [null],
            'officeName': [null]
        });
    };
    //get Controller List
    TransferOfficeLienperiodComponent.prototype.getControllerList = function () {
        var _this = this;
        this.transferOfficeLienperiodService.getControllerList().subscribe(function (res) {
            _this.controllerList = res;
        });
    };
    //Get DDO LIST Using ControllerId
    TransferOfficeLienperiodComponent.prototype.getDdoListByControllerId = function (controllerID) {
        var _this = this;
        this.transferOfficeLienperiodService.getDdoListbyControllerId(controllerID).subscribe(function (res) {
            _this.ddoList = res;
        });
    };
    TransferOfficeLienperiodComponent.prototype.GetAllDetails = function () {
        var Conrtollid = this.formPopUp.get('controllerId').value;
        var ddoid = this.formPopUp.get('ddoId').value;
        var officename = this.formPopUp.get('officeName').value;
        if (officename == null) {
            officename = 0;
        }
        this.getOfficeAddressDetails(Conrtollid, ddoid, officename);
    };
    TransferOfficeLienperiodComponent.prototype.ClearPop = function () {
        this.tranferOfficeData = null;
        this.formDirectivepop.resetForm();
    };
    TransferOfficeLienperiodComponent.prototype.getOfficeAddressDetails = function (controllerID, ddoId, officeName) {
        var _this = this;
        if (officeName == "") {
            officeName = 0;
        }
        debugger;
        if (this.formPopUp.invalid) {
            return;
        }
        else {
            this.transferOfficeLienperiodService.getOfficeAddressDetails(controllerID, ddoId, officeName).subscribe(function (res) {
                debugger;
                if (res.length == 0) {
                    _this.officeAddressDetails = null;
                    _this.tranferOfficeData = null;
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()("Data Not Found ?");
                }
                else {
                    _this.officeAddressDetails = res;
                    _this.tranferOfficeData = res;
                }
            });
        }
    };
    TransferOfficeLienperiodComponent.prototype.getTransfer_office_lien_Period_Details = function (Empcd, roleId) {
        var _this = this;
        this.transferOfficeLienperiodService.getTransfer_office_lien_Period_Details(Empcd, roleId).subscribe(function (result) {
            debugger;
            //if (Flag == true) {
            if (result.length == 0) {
                _this.formDirective.resetForm();
                _this.form.enable();
                _this.tranferLienPeriodData = null;
                _this.form.get('employeeCode').setValue(_this.empCd);
            }
            else {
                _this.tranferLienPeriodData = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](result);
                _this.tranferLienPeriodData.paginator = _this.paginator;
                _this.tranferLienPeriodData.sort = _this.sort;
                _this.form.get('employeeCode').setValue(Empcd);
                if (_this.form.get('reasonTransfer').value !== "select") {
                    _this.relHideShow = true;
                }
                else {
                    _this.relHideShow = false;
                }
            }
        });
    };
    TransferOfficeLienperiodComponent.prototype.charaterOnlyNoSpace = function (event) {
        debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    TransferOfficeLienperiodComponent.prototype.getAllDesignation = function (controllerId) {
        var _this = this;
        this.master.GetAllDesignation(controllerId).subscribe(function (res) {
            _this.designation = res;
        });
    };
    TransferOfficeLienperiodComponent.prototype.GetSelectCode = function (code) {
        this.officeAddressDetails;
        debugger;
        var groups = this.tranferOfficeData.filter(function (_a) {
            var officeId = _a.officeId;
            return code.includes(officeId);
        });
        this.form.get('officeId').setValue(groups[0].officeId);
        this.form.get('officeName').setValue(groups[0].officeName.trim());
        this.form.get('paoName').setValue(groups[0].paoName);
        this.form.get('ddoName').setValue(groups[0].ddoName);
        this.form.get('paoId').setValue(groups[0].paoId);
        this.form.get('ddoId').setValue(groups[0].ddoId);
        this.showCivilValue = false;
        this.ClearPop();
    };
    TransferOfficeLienperiodComponent.prototype.ltrim = function (tblfiltervalue) {
        return tblfiltervalue.replace(/^\s+/g, '');
    };
    TransferOfficeLienperiodComponent.prototype.applyFilter = function (filterValue) {
        debugger;
        this.tblfiltervalue = this.ltrim(this.tblfiltervalue);
        if (this.tranferLienPeriodData === null || this.tranferLienPeriodData === undefined) {
            this.notFound = false;
        }
        this.tranferLienPeriodData.filter = filterValue.trim().toLowerCase();
        this.tranferLienPeriodData.filterPredicate = function (data, filter) {
            debugger;
            return data.orderNo.toLowerCase().includes(filter) || data.orderDate.toLowerCase().includes(filter) || data.relevingOrderNo.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.officeName.toLowerCase().includes(filter);
        };
        if (this.tranferLienPeriodData.paginator) {
            this.tranferLienPeriodData.paginator.firstPage();
        }
        if (this.tranferLienPeriodData.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    TransferOfficeLienperiodComponent.prototype.confirmDelete = function () {
        var _this = this;
        var value = this.tranferLienPeriodData.filteredData.filter(function (x) { return x.mslienId == _this.lienId; })[0];
        var orderNo = value.orderNo;
        this.transferOfficeLienperiodService.deleteTransferOffLienPeriod(this.empCd, orderNo).subscribe(function (result) {
            if (parseInt(result) >= 1) {
                _this.snackBar.open('Record Successfully Deleted ', null, { duration: 4000 });
                _this.formDirective.reset();
                _this.getTransfer_office_lien_Period_Details(_this.empCd, _this.roleId);
                _this.Cancel();
            }
            else {
                _this.snackBar.open('Record Not Deleted ', null, { duration: 4000 });
            }
        });
    };
    TransferOfficeLienperiodComponent.prototype.deleteRecordClick = function (lienId) {
        this.lienId = lienId;
    };
    TransferOfficeLienperiodComponent.prototype.ResetForm = function () {
        this.form.reset();
        this.formDirective.resetForm();
        this.form.enable();
        this.btnUpdateText = 'Save';
        this.disableForwardflag = false;
        this.form.get('employeeCode').setValue(this.empCd.trim());
    };
    TransferOfficeLienperiodComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        var RejectionComment = '0';
        var joiningOrderNo = this.form.get('orderNo').value;
        if (statusFlag == 'R') {
            RejectionComment = this.rejectionRemark;
        }
        else {
            RejectionComment = '0';
        }
        this.transferOfficeLienperiodService.forwardStatusUpdate(this.empCd, joiningOrderNo, statusFlag, RejectionComment).subscribe(function (result1) {
            if (parseInt(result1) >= 1) {
                _this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
                _this.form.reset();
                _this.formDirective.resetForm();
                _this.getTransfer_office_lien_Period_Details(_this.empCd, _this.roleId);
                _this.btnUpdateText = 'Save';
                _this.infoScreen = false;
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
                _this.rejectionRemark = '';
                _this.disableForwardflag = false;
            }
            else {
                _this.snackBar.open('Status not Update Successfully', null, { duration: 4000 });
            }
        });
    };
    TransferOfficeLienperiodComponent.prototype.onSubmit = function () {
        //this.submitted = true;
        var _this = this;
        this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        else {
            if (this.btnUpdateText == 'Update') {
                this.form.get('flagUpdate').setValue('update');
            }
            else {
                this.form.get('flagUpdate').setValue('insert');
                var employeeid = this.form.get('employeeCode').value;
                if (employeeid == 0) {
                    alert("Select Employee");
                    return;
                }
            }
            this.transferOfficeLienperiodService.InsertUpateTransferOffLienPeriodService(this.form.value).subscribe(function (result1) {
                var resultvalue = result1.split("-");
                if (parseInt(resultvalue[0]) >= 1) {
                    if (_this.btnUpdateText == 'Update') {
                        _this.snackBar.open('Update Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getTransfer_office_lien_Period_Details(_this.empCd, _this.roleId);
                        _this.btnUpdateText = 'Save';
                    }
                    else {
                        _this.snackBar.open('Save Successfully', null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getTransfer_office_lien_Period_Details(_this.empCd, _this.roleId);
                        _this.btnUpdateText == 'Save';
                    }
                }
                else {
                    if (_this.btnUpdateText == 'Update') {
                        if (resultvalue[2] = 'Duplicate Record') {
                            _this.snackBar.open('Order Number  Already Exits', null, { duration: 4000 });
                        }
                        else {
                            _this.snackBar.open('Update not Successfully', null, { duration: 4000 });
                        }
                    }
                    else if (resultvalue[2] = 'Duplicate Record') {
                        _this.snackBar.open('Order Number Already Exits', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Save not Successfully', null, { duration: 4000 });
                    }
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], TransferOfficeLienperiodComponent.prototype, "formDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirectivepop'),
        __metadata("design:type", Object)
    ], TransferOfficeLienperiodComponent.prototype, "formDirectivepop", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('freject'),
        __metadata("design:type", Object)
    ], TransferOfficeLienperiodComponent.prototype, "form2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], TransferOfficeLienperiodComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], TransferOfficeLienperiodComponent.prototype, "sort", void 0);
    TransferOfficeLienperiodComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transfer-office-lienperiod',
            template: __webpack_require__(/*! ./transfer-office-lienperiod.component.html */ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.html"),
            styles: [__webpack_require__(/*! ./transfer-office-lienperiod.component.css */ "./src/app/lienperiod/transfer-office-lienperiod/transfer-office-lienperiod.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_lienperiod_transfer_office_lienperiod_service__WEBPACK_IMPORTED_MODULE_6__["TransferOfficeLienperiodService"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]])
    ], TransferOfficeLienperiodComponent);
    return TransferOfficeLienperiodComponent;
}());



/***/ }),

/***/ "./src/app/services/lienperiod/epsemployeejoinafterlienperiod.service.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/services/lienperiod/epsemployeejoinafterlienperiod.service.ts ***!
  \*******************************************************************************/
/*! exports provided: EpsemployeejoinafterlienperiodService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpsemployeejoinafterlienperiodService", function() { return EpsemployeejoinafterlienperiodService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var EpsemployeejoinafterlienperiodService = /** @class */ (function () {
    function EpsemployeejoinafterlienperiodService(http, config) {
        this.http = http;
        this.config = config;
    }
    EpsemployeejoinafterlienperiodService.prototype.getEpsEmpJoinOffAfterLienPeriodDetailByEmp = function (empCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getEpsEmpJoinOffAfterLienPeriodDetailByEmp + '?empCd= ' + empCd + '&roleId=' + roleId));
    };
    //Insert Update Eps Employee Join After Lien Period
    EpsemployeejoinafterlienperiodService.prototype.InsertUpateEpsEmployeeJoinAfterLienPeriod = function (epsEmployeeJoinLienPeriodDetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateEpsEmployeeJoinAfterLienPeriod, epsEmployeeJoinLienPeriodDetails, { responseType: 'text' });
    };
    //--end
    //Delete Eps Employee Join After Lien Period By EmployeeCode
    EpsemployeejoinafterlienperiodService.prototype.deleteEpsEmployeeJoinAfterLienPeriodById = function (lienId) {
        debugger;
        return this.http.get("" + this.config.api_base_url + (this.config.deleteEpsEmployeeJoinAfterLienPeriodById + '?lienId= ' + lienId));
    };
    //--end
    //
    EpsemployeejoinafterlienperiodService.prototype.forwardStatusUpdate = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.epsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    EpsemployeejoinafterlienperiodService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], EpsemployeejoinafterlienperiodService);
    return EpsemployeejoinafterlienperiodService;
}());



/***/ }),

/***/ "./src/app/services/lienperiod/nonepsemployeejoinafterlienperiod.service.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/services/lienperiod/nonepsemployeejoinafterlienperiod.service.ts ***!
  \**********************************************************************************/
/*! exports provided: NonepsemployeejoinafterlienperiodService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NonepsemployeejoinafterlienperiodService", function() { return NonepsemployeejoinafterlienperiodService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var NonepsemployeejoinafterlienperiodService = /** @class */ (function () {
    function NonepsemployeejoinafterlienperiodService(http, config) {
        this.http = http;
        this.config = config;
    }
    NonepsemployeejoinafterlienperiodService.prototype.getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp = function (empCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp + '?empCd= ' + empCd + '&roleId=' + roleId));
    };
    //Insert Update Non Eps Employee Join After Lien Period
    NonepsemployeejoinafterlienperiodService.prototype.InsertUpateNonEpsEmployeeJoinAfterLienPeriod = function (NonEpsEmpJoinLienPeriodDetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateNonEpsEmployeeJoinAfterLienPeriod, NonEpsEmpJoinLienPeriodDetails, { responseType: 'text' });
    };
    //--end
    //Delete Non Eps Employee Join After Lien Period 
    NonepsemployeejoinafterlienperiodService.prototype.deleteNonEpsTransferOffLienPeriod = function (empCd, orderNo) {
        debugger;
        return this.http.get(this.config.api_base_url + this.config.deleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo);
    };
    //--end
    //
    NonepsemployeejoinafterlienperiodService.prototype.forwardNonEpsEmployeeStatusUpdate = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.nonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    NonepsemployeejoinafterlienperiodService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], NonepsemployeejoinafterlienperiodService);
    return NonepsemployeejoinafterlienperiodService;
}());



/***/ }),

/***/ "./src/app/services/lienperiod/transfer-office-lienperiod.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/services/lienperiod/transfer-office-lienperiod.service.ts ***!
  \***************************************************************************/
/*! exports provided: TransferOfficeLienperiodService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferOfficeLienperiodService", function() { return TransferOfficeLienperiodService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var TransferOfficeLienperiodService = /** @class */ (function () {
    function TransferOfficeLienperiodService(http, config) {
        this.http = http;
        this.config = config;
    }
    //get Controller List
    TransferOfficeLienperiodService.prototype.getControllerList = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getControllerList);
    };
    //--end
    //Get DDOLIST Using Controller ID
    TransferOfficeLienperiodService.prototype.getDdoListbyControllerId = function (controllerId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getDdolistbyControllerid + controllerId));
    }; //--end
    //Get Office Address Details Using Controllerid& ddoId
    TransferOfficeLienperiodService.prototype.getOfficeAddressDetails = function (controllerID, ddoId, officeName) {
        return this.http.get("" + this.config.api_base_url + (this.config.getOfficeAddressDetails + '?controllerID= ' + controllerID + '&ddoId=' + ddoId + '&officeName=' + officeName));
    };
    TransferOfficeLienperiodService.prototype.getTransfer_office_lien_Period_Details = function (empCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getTransferofficelienPeriodDetails + '?empCd= ' + empCd + '&roleId=' + roleId));
    };
    //--end
    //Insert Update Transfer Office Lien Period
    TransferOfficeLienperiodService.prototype.InsertUpateTransferOffLienPeriodService = function (transferOffLienPeriodDetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateTransferOffLienPeriod, transferOffLienPeriodDetails, { responseType: 'text' });
    };
    //--end
    //
    TransferOfficeLienperiodService.prototype.forwardStatusUpdate = function (empCd, orderNo, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.lienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    //--end
    //Delete Record Transfer lien period Office
    TransferOfficeLienperiodService.prototype.deleteTransferOffLienPeriod = function (empCd, orderNo) {
        return this.http.get(this.config.api_base_url + this.config.deleteTransferOffLienPeriod + '?empCd= ' + empCd + '&orderNo=' + orderNo);
    };
    TransferOfficeLienperiodService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], TransferOfficeLienperiodService);
    return TransferOfficeLienperiodService;
}());



/***/ })

}]);
//# sourceMappingURL=lienperiod-lienperiod-module.js.map