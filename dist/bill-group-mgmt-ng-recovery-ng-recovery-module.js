(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bill-group-mgmt-ng-recovery-ng-recovery-module"],{

/***/ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.css":
/*!*********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.css ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9uZy1yZWNvdmVyeS9lZGl0LXZpZXctYWxsLWVtcGxveWVlL2VkaXQtdmlldy1hbGwtZW1wbG95ZWUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.html":
/*!**********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n  <div class=\"fom-title\">Enter NG Recovery Amount</div>\r\n\r\n\r\n\r\n  <!--<div class=\"col-md-12 col-lg-12 upload-container\">\r\n    <div class=\"col-sm-12 col-md-4 col-lg-2 pading-0\"><label>Upload File</label></div>\r\n    <div class=\"col-sm-12 col-md-8 col-lg-6\"><input type=\"file\"></div>\r\n    <button type=\"button\">Upload</button>\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-12\"><label>Download Template</label><a class=\"material-icons\">vertical_align_bottom</a></div>-->\r\n\r\n  <mat-form-field>\r\n    <input matInput placeholder=\"Search\">\r\n    <i class=\"material-icons icon-right\">search</i>\r\n  </mat-form-field>\r\n\r\n  <div class=\"table-title\">Attached Employees</div>\r\n  <table matSort class=\"mat-elevation-z8 even-odd-color tabel-form wid-100 recovery-tabel\" border=\"1\">\r\n    <tr class=\"table-head\">\r\n      <th>\r\n        <mat-checkbox></mat-checkbox>\r\n      </th>\r\n      <th>SI.No</th>\r\n      <th>Employee Code</th>\r\n      <th>Employee Name</th>\r\n      <th>Amount</th>\r\n      <th>No. of Installment</th>\r\n      <th>Recovered No. of Installment</th>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        1\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        2\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        3\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"200\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        4\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"200\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n    <tr class=\"tabel-total\">\r\n      <td colspan=\"4\"></td>\r\n      <td class=\"text-center\">Total</td>\r\n      <td>500</td>\r\n      <td></td>\r\n    </tr>\r\n  </table>\r\n</div>\r\n\r\n<!--Add Remove Button-->\r\n<div class=\"col-md-12 col-lg-12 pading-0 text-center mb-20 mt-40\">\r\n\r\n  <button class=\"btn btn-labeled btn-success add-list  text-center\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-up\"></i></span>Add to list\r\n  </button>\r\n\r\n  <button class=\"btn btn-labeled btn-success remove-list  text-center\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-down\"></i></span>Remove\r\n  </button>\r\n</div>\r\n\r\n<!--Add Remove Button End-->\r\n\r\n\r\n<div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n  \r\n\r\n\r\n\r\n  <!--<div class=\"col-md-12 col-lg-12 upload-container\">\r\n    <div class=\"col-sm-12 col-md-4 col-lg-2 pading-0\"><label>Upload File</label></div>\r\n    <div class=\"col-sm-12 col-md-8 col-lg-6\"><input type=\"file\"></div>\r\n    <button type=\"button\">Upload</button>\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-12\"><label>Download Template</label><a class=\"material-icons\">vertical_align_bottom</a></div>-->\r\n\r\n  <mat-form-field>\r\n    <input matInput placeholder=\"Search\">\r\n    <i class=\"material-icons icon-right\">search</i>\r\n  </mat-form-field>\r\n\r\n  <div class=\"table-title\">Un- Attached Employees</div>\r\n  <table matSort class=\"mat-elevation-z8 even-odd-color tabel-form wid-100 recovery-tabel\" border=\"1\">\r\n    <tr class=\"table-head\">\r\n      <th>\r\n        <mat-checkbox></mat-checkbox>\r\n      </th>\r\n      <th>SI.No</th>\r\n      <th>Employee Code</th>\r\n      <th>Employee Name</th>\r\n      <th>Amount</th>\r\n      <th>No. of Installment</th>\r\n      <th>Recovered No. of Installment</th>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        1\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        2\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        3\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"200\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td>\r\n        <mat-checkbox></mat-checkbox>\r\n      </td>\r\n      <td>\r\n        4\r\n      </td>\r\n      <td>\r\n        ABC123\r\n      </td>\r\n      <td>\r\n        Anik Khanna\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"100\">\r\n      </td>\r\n      <td>\r\n        <input matInput placeholder=\"Enter value\" value=\"200\">\r\n      </td>\r\n      <td>\r\n        -\r\n      </td>\r\n    </tr>\r\n    <tr class=\"tabel-total\">\r\n      <td colspan=\"4\"></td>\r\n      <td class=\"text-center\">Total</td>\r\n      <td>500</td>\r\n      <td></td>\r\n    </tr>\r\n  </table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: EditViewAllEmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditViewAllEmployeeComponent", function() { return EditViewAllEmployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EditViewAllEmployeeComponent = /** @class */ (function () {
    function EditViewAllEmployeeComponent() {
    }
    EditViewAllEmployeeComponent.prototype.ngOnInit = function () {
    };
    EditViewAllEmployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-view-all-employee',
            template: __webpack_require__(/*! ./edit-view-all-employee.component.html */ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.html"),
            styles: [__webpack_require__(/*! ./edit-view-all-employee.component.css */ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EditViewAllEmployeeComponent);
    return EditViewAllEmployeeComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.css":
/*!***************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.css ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9uZy1yZWNvdmVyeS9lbnRyeWZvcmFsbGVtcGxveWVlL2VudHJ5Zm9yYWxsZW1wbG95ZWUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.html":
/*!****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"col-md-6 col-lg-8\">\r\n  <div class=\"example-card mat-card\">\r\n    <app-headerforallemployee></app-headerforallemployee>\r\n    <div class=\"fom-title\">Enter NG Recovery for all Employees</div>\r\n    <form [formGroup]=\"EntryForAllEmployee\" #creationForm=\"ngForm\" (ngSubmit)=\"SaveNgDetails()\">\r\n      <mat-card class=\"example-card\">\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"NG Recovery\" formControlName=\"ngRecovery\">\r\n              <mat-option *ngFor=\"let ng of NgList\" [value]=\"ng.ngDedCd\">{{ng.ngDesc}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <!--<div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Scroll NGR from Previous Month?\" formControlName=\"previousMonth\" (selectionChange)=\"toggleRecovery($event.value)\">\r\n            <mat-option value=\"true\">Yes</mat-option>\r\n            <mat-option value=\"false\">No</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>-->\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <!--<mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Pay Bill Group\">\r\n            <mat-option>Select Pay Bill Group</mat-option>\r\n          </mat-select>\r\n\r\n        </mat-form-field>-->\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Pay Bill Group\" #singleSelect (selectionChange)=\"BindDropDownDesignation($event.value)\" [(value)]=\"selected2\" formControlName=\"billGrId\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"billFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option [value]=\"0\">\r\n                Select\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let Paybill of filteredBill | async\" [value]=\"Paybill.billgrId\">\r\n                {{Paybill.billgrDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-6\">\r\n\r\n          <mat-form-field class=\"wid-100\">\r\n\r\n            <mat-select placeholder=\"Find Designation\" #singleSelect (selectionChange)=\"FilterEmployeeByDesignation($event.value)\" [(value)]=\"selected\" formControlname=\"desigId\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option [value]=\"0\">\r\n                Select\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.desigId\">\r\n                {{emp.desigDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount\" formControlName=\"universalAmount\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6 margin-top-10px\">\r\n\r\n          <mat-checkbox (change)=\"onChange($event, i, item)\">Amount applicale to all Employees</mat-checkbox>\r\n\r\n        </div>\r\n\r\n\r\n        <div class=\"col-sm-12 col-md-12 col-lg-12\" *ngIf=\"toggleRecoveryBool == 'false'\">\r\n          <div class=\"fom-title\">NG Recoveries</div>\r\n\r\n\r\n\r\n          <!--<div class=\"col-md-12 col-lg-12 upload-container\">\r\n          <div class=\"col-sm-12 col-md-4 col-lg-2 pading-0\"><label>Upload File</label></div>\r\n          <div class=\"col-sm-12 col-md-8 col-lg-6\"><input type=\"file\"></div>\r\n          <button type=\"button\">Upload</button>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-12\"><label>Download Template</label><a class=\"material-icons\">vertical_align_bottom</a></div>-->\r\n\r\n          <div class=\"mat-card\">\r\n            <ng-container formArrayName=\"employeeList\">\r\n              <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color tabel-form wid-100 recovery-tabel\">\r\n\r\n                <ng-container matColumnDef=\"CheckBox\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n                    <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                                  [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n                  </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element ; let i = index;\" [formGroupName]=\"i\">\r\n                    <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(EntryForAllEmployee.controls.employeeList.controls[i].value) : null; AddCheckedObj()\"\r\n                                  [checked]=\"selectionAttach.isSelected(EntryForAllEmployee.controls.employeeList.controls[i].value)\">\r\n                    </mat-checkbox>\r\n                  </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"S.No\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> S.No </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Employee Code\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Employee Code </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\">\r\n                    <input matInput placeholder=\"\" formControlName=\"empCode\" readonly>\r\n\r\n                    <!--{{element.empCode}}-->\r\n                  </mat-cell>\r\n                  ma\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Employee Name\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\">\r\n                    <input matInput placeholder=\"\" formControlName=\"empName\" readonly>\r\n\r\n                    <!--{{element.empName}}-->\r\n                  </mat-cell>\r\n                  ma\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n\r\n                </ng-container>\r\n\r\n\r\n                <ng-container matColumnDef=\"SubsAmount\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Subscription Amount </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\"> <input matInput placeholder=\"Enter value\" formControlName=\"subscriptionAmt\" (keyup)=\"RowSubsSum($event.target.value, i);updateDatasource(element, $event.target.value)\"> </mat-cell>\r\n                  <!--<mat-footer-cell *matFooterCellDef> {{totalSumAmount}} </mat-footer-cell>-->\r\n                  el\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"RecovAmount\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Recovery Amount </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\"> <input matInput placeholder=\"Enter value\" formControlName=\"recoveryAmt\" (keyup)=\"RowRecovSum($event.target.value, i);updateDatasource(element, $event.target.value)\"> </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef> Total: </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"TotAmount\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Total Amount </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\"> <input matInput placeholder=\"Enter value\" formControlName=\"totalAmt\" readonly> </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef> {{totalSumAmount}} </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"No. of Installment\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> No. of Installment </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\"> <input matInput placeholder=\"Enter value\" formControlName=\"installmentCount\" (keyup)=\"updateInstallment(element,$event.target.value)\"> </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Recovered No. of Installment\">\r\n                  <mat-header-cell *matHeaderCellDef mat-sort-header> Recovered No. of Installment </mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let element; let i = index;\" [formGroupName]=\"i\">\r\n\r\n                    <input matInput placeholder=\"\" formControlName=\"recoveredInstallment\" readonly>\r\n\r\n                  </mat-cell>\r\n                  <mat-footer-cell *matFooterCellDef>  </mat-footer-cell>\r\n                </ng-container>\r\n                <!--<ng-container matColumnDef=\"Total\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Total </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> 50000 </mat-cell>\r\n                <mat-footer-cell *matFooterCellDef mat-sort-header> 50000 </mat-footer-cell>\r\n\r\n              </ng-container>-->\r\n                <!--<tr class=\"table-head\">\r\n                <th>\r\n                  <mat-checkbox></mat-checkbox>\r\n                </th>\r\n                <th>SI.No</th>\r\n                <th>Employee Code</th>\r\n                <th>Employee Name</th>\r\n                <th>Amount</th>\r\n                <th>No. of Installment</th>\r\n                <th>Recovered No. of Installment</th>\r\n              </tr>-->\r\n                <!--<tr>\r\n                <td>\r\n                  <mat-checkbox></mat-checkbox>\r\n                </td>\r\n                <td>\r\n                  1\r\n                </td>\r\n                <td>\r\n                  ABC123\r\n                </td>\r\n                <td>\r\n                  Anik Khanna\r\n                </td>\r\n                <td>\r\n                  <input matInput placeholder=\"Enter value\" value=\"100\">\r\n                </td>\r\n                <td>\r\n                  <input matInput placeholder=\"Enter value\" value=\"\">\r\n                </td>\r\n                <td>\r\n                  -\r\n                </td>\r\n              </tr>-->\r\n                <!--<tr  class=\"tabel-total\">\r\n                <td colspan=\"4\"></td>\r\n                <td class=\"text-center\">Total</td>\r\n                <td>500</td>\r\n                <td></td>\r\n              </tr>-->\r\n\r\n\r\n                <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                <mat-footer-row *matFooterRowDef=\"displayedColumns\"></mat-footer-row>\r\n              </mat-table>\r\n            </ng-container>\r\n            <!--<table class=\"mat-elevation-z8 tabel-form wid-100 recovery-tabel\">\r\n            <tr class=\"\">\r\n              <th></th>\r\n              <th></th>\r\n              <th></th>\r\n              <th></th>\r\n              <th></th>\r\n              <th></th>\r\n            </tr>\r\n            <tr class=\"tabel-total\">\r\n              <td colspan=\"4\"></td>\r\n              <td class=\"text-center\">Total:</td>\r\n              <td>500</td>\r\n              <td></td>\r\n            </tr>\r\n          </table>-->\r\n            <!--<div class=\"col-md-6 col-lg-6\">\r\n\r\n          </div>\r\n\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <div class=\"tabel-total\">\r\n              <label class=\"text-center\">Total: 500</label>\r\n            </div>\r\n          </div>-->\r\n            <!--<div class=\"col-md-12 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Amount \">\r\n            </mat-form-field>\r\n          </div>-->\r\n\r\n          </div>\r\n        </div>\r\n        <app-edit-view-all-employee *ngIf=\"toggleRecoveryBool == 'true'\"></app-edit-view-all-employee>\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n          <button type=\"reset\" class=\"btn btn-warning\">Cancel</button>\r\n        </div>\r\n      </mat-card>\r\n    </form>\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n<!--right table-->\r\n\r\n\r\n\r\n<div class=\"col-md-6 col-lg-4\">\r\n\r\n  <div class=\"fom-title\">Enter NG Recovery for all Employees</div>\r\n  <table class=\"w-100 \">\r\n    <tr class=\"tab-tr-bg\">\r\n      <td colspan=\"2\" class=\"tab-th\">S.No</td>\r\n      <td colspan=\"2\" class=\"tab-th\">NGR Code</td>\r\n      <td>NG Recovery Description</td>\r\n    </tr>\r\n  </table>\r\n\r\n  <mat-accordion class=\"accordion-wrap\">\r\n    <mat-expansion-panel *ngFor=\"let ng of dataSourceforgrid; let i = index\">\r\n      <mat-expansion-panel-header (click)=\"expansionHeader()\">\r\n        <mat-panel-title >\r\n         {{i+1}}\r\n        </mat-panel-title>\r\n        <mat-panel-title>\r\n          {{ng.ngDedCd}}\r\n        </mat-panel-title>\r\n        <mat-panel-description>\r\n          {{ng.ngDesc}}\r\n          <!--AGCR CO Opereative Credit-->\r\n        </mat-panel-description>\r\n      </mat-expansion-panel-header>\r\n\r\n      <table class=\"mat-elevation-z8 even-odd-color mat-table\">\r\n        <thead>\r\n          <tr class=\"mat-header-row ng-star-inserted\">\r\n            <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Pay Bill Group </th>\r\n            <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Month/Year </th>\r\n            <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Action</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody *ngFor=\"let item of ng.payBillDetails\">\r\n          <tr class=\"mat-row ng-star-inserted\">\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">{{item.billgrDesc}}</td>\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">{{item.monthName}}</td>\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n              <a class=\"material-icons i-info\" mattooltip=\"info\">info</a>\r\n              <a class=\"material-icons i-edit\" mattooltip=\"edit\">edit</a>\r\n              <a class=\"material-icons i-delet\" mattooltip=\"delete\">delete</a>\r\n            </td>\r\n          </tr>\r\n          <!--<tr class=\"mat-row ng-star-inserted\">\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">AAO NPS</td>\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">August/2019</td>\r\n            <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n              <a class=\"material-icons i-info\" mattooltip=\"info\">info</a>\r\n              <a class=\"material-icons i-edit\" mattooltip=\"edit\">edit</a>\r\n              <a class=\"material-icons i-delet\" mattooltip=\"delete\">delete</a>\r\n            </td>\r\n          </tr>-->\r\n        </tbody>\r\n      </table>\r\n    </mat-expansion-panel>\r\n\r\n\r\n    <!--<mat-expansion-panel>\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          2\r\n        </mat-panel-title>\r\n        <mat-panel-description>\r\n          Kerala State Fund\r\n        </mat-panel-description>\r\n      </mat-expansion-panel-header>\r\n      <table class=\"mat-elevation-z8 even-odd-color mat-table\">\r\n        <thead>\r\n          <tr class=\"mat-header-row ng-star-inserted\">\r\n            <th class=\"mat-sort-header\"> Pay Bill Group </th>\r\n            <th class=\"mat-sort-header\"> Month/Year </th>\r\n            <th class=\"mat-sort-header\">Action</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr class=\"mat-row ng-star-inserted\">\r\n            <td class=\"mat-cell cdk-column-id mat-column-id ng-star-inserted\">dgggdgg</td>\r\n            <td class=\"mat-cell cdk-column-id mat-column-id ng-star-inserted\">dgggdgg</td>\r\n            <td class=\"mat-cell cdk-column-id mat-column-id ng-star-inserted\">\r\n              <a class=\"material-icons i-info\" mattooltip=\"info\">info</a>\r\n              <a class=\"material-icons i-edit\" mattooltip=\"edit\">edit</a>\r\n              <a class=\"material-icons i-delet\" mattooltip=\"delete\">delete</a>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </mat-expansion-panel>-->\r\n\r\n  </mat-accordion>\r\n\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: EntryforallemployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntryforallemployeeComponent", function() { return EntryforallemployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { FormControl } from '@angular/forms';

//02-08-19 End
var EntryforallemployeeComponent = /** @class */ (function () {
    function EntryforallemployeeComponent(_Service, _CommanService, formBuilder) {
        this._Service = _Service;
        this._CommanService = _CommanService;
        this.formBuilder = formBuilder;
        this.displayedColumns = ['CheckBox', 'S.No', 'Employee Code', 'Employee Name', 'SubsAmount', 'RecovAmount', 'TotAmount', 'No. of Installment', 'Recovered No. of Installment'];
        this.NgList = [];
        this.NgRecoveryList = [];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"]();
        this.EmpList = [];
        this.toggleRecoveryBool = 'false';
        this.checkedObject = [];
        this.totalSumAmount = 0;
        this.RowWiseSumAmount = 0;
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_6__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_6__["SelectionModel"](true, []);
        this.flag = null;
        this.billCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.billFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredBill = new rxjs__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"](1);
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"](1);
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
    }
    EntryforallemployeeComponent.prototype.toggleRecovery = function (value) {
        this.toggleRecoveryBool = value;
        //console.log(this.toggleRecoveryBool);
    };
    EntryforallemployeeComponent.prototype.ngOnInit = function () {
        this.getNgrecovery('00003');
        this.getNgRecoveryList('00003');
        this.BindDropDownBillGroup('00003');
        //this.BindDropDownDesignation('00003');
        this.CreateForm();
        //this.getEmployee('00003', 12, 150);
    };
    EntryforallemployeeComponent.prototype.expansionHeader = function () {
        //alert("called");
    };
    EntryforallemployeeComponent.prototype.getNgrecovery = function (permDDOId) {
        //debugger;
        var _this = this;
        this._Service.GetNgRecovery(permDDOId).subscribe(function (data) {
            _this.NgList = data;
            //this.getNgrecoveryData = data;
            //this.dataSource = new MatTableDataSource(data);
            //this.dataSource.paginator = this.paginator;
            //this.dataSource.sort = this.sort;
        });
    };
    EntryforallemployeeComponent.prototype.getNgRecoveryList = function (permDDOId) {
        var _this = this;
        debugger;
        this._Service.GetAllNgRecoveryandPaybillGrpDetails(permDDOId).subscribe(function (data) {
            _this.NgRecoveryList = data;
            _this.dataSourceforgrid = data;
            //this.dataSourceforgrid = new MatTableDataSource(data);
            //this.dataSourceforgrid.paginator = this.paginator;
            //this.dataSourceforgrid.sort = this.sort;
            console.log(data);
            //this.getNgrecoveryData = data;
            //this.dataSource = new MatTableDataSource(data);
            //this.dataSource.paginator = this.paginator;
            //this.dataSource.sort = this.sort;
        });
    };
    EntryforallemployeeComponent.prototype.CreateForm = function () {
        this.EntryForAllEmployee = this.formBuilder.group({
            //FinYear
            //finYear: [null, ''],
            //month: [null, ''],
            ngRecovery: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            previousMonth: ["false", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            billGrId: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            desigid: [null, ''],
            universalAmount: [''],
            totalAmount: [''],
            //empCd: [null, Validators.required],
            //empName: [null, Validators.required],
            //amount: [0, Validators.required],
            //installmentCount: [null, Validators.required],
            //recoveredInstallment: [null, Validators.required],
            employeeList: this.formBuilder.array([])
        });
        //this.addEmpDetail();
    };
    EntryforallemployeeComponent.prototype.addEmpDetail = function () {
        this.GetEmployeeList.push(this.createItem());
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.GetEmployeeList.value);
    };
    EntryforallemployeeComponent.prototype.createItem = function () {
        return this.formBuilder.group({
            empCode: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            empName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            subscriptionAmt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            recoveryAmt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            totalAmt: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            installmentCount: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            recoveredInstallment: [0],
            ngDedCd: [0],
            permDdoId: [null]
        });
    };
    Object.defineProperty(EntryforallemployeeComponent.prototype, "GetEmployeeList", {
        get: function () {
            return this.EntryForAllEmployee.get('employeeList');
        },
        enumerable: true,
        configurable: true
    });
    EntryforallemployeeComponent.prototype.RowSubsSum = function (Value1, Value2) {
        debugger;
        this.RowWiseSumAmount = 0;
        //this.RowWiseSumAmount = Number(Value1) + this.GetEmployeeList.at(Value2).get('RecovAmount').value;
        //this.GetEmployeeList.at(Value2).get('SubsAmount').setValue(Number(Value1));
        this.GetEmployeeList.at(Value2).get('totalAmt').setValue(Number(Value1) + Number(this.GetEmployeeList.at(Value2).get('recoveryAmt').value));
    };
    EntryforallemployeeComponent.prototype.RowRecovSum = function (Value1, Value2) {
        debugger;
        //this.RowWiseSumAmount = 0;
        //this.GetEmployeeList.at(Value2).get('RecovAmount').setValue(Number(Value1));
        this.GetEmployeeList.at(Value2).get('totalAmt').setValue(Number(Value1) + Number(this.GetEmployeeList.at(Value2).get('subscriptionAmt').value));
    };
    EntryforallemployeeComponent.prototype.SumAmount = function () {
        this.totalSumAmount = 0;
        debugger;
        for (var i = 0; i < this.checkedObject.length; i++) {
            this.totalSumAmount += +this.checkedObject[i].totalAmt; //.at(i).get('totalAmt').value;
        }
        //this.totalSumAmount = this.EntryForAllEmployee.get('universalAmount').value * this.EmpList.length;
    };
    EntryforallemployeeComponent.prototype.updateDatasource = function (element, value) {
        //this.GetEmployeeList.reset();
        element.totalAmt = value;
        // this.SumAmount();
        //element.amount = value;
        // this.dataSource = new MatTableDataSource(this.GetEmployeeList.value);
        //this.GetEmployeeList.at(index).get('amount').setValue
    };
    EntryforallemployeeComponent.prototype.updateInstallment = function (element, value) {
        element.installmentCount = value;
    };
    EntryforallemployeeComponent.prototype.getEmployee = function (permDDOId, payBillGroupId, designationCd) {
        var _this = this;
        debugger;
        this._Service.GetEmployeeByPaybillAndDesig(permDDOId, payBillGroupId, designationCd).subscribe(function (data) {
            _this.EmpList = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](data);
            for (var i = 0; i < _this.EmpList.length; i++) {
                _this.GetEmployeeList.push(_this.formBuilder.group({
                    empCode: [_this.EmpList[i].empCode, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    empName: [_this.EmpList[i].empName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    subscriptionAmt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    recoveryAmt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    totalAmt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    installmentCount: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                    recoveredInstallment: [0]
                }));
            }
            console.log(_this.dataSource);
            //this.EmpList.forEach(emp => this.GetEmployeeList.push(this.formBuilder.group(emp)));
        });
    };
    EntryforallemployeeComponent.prototype.BindDropDownBillGroup = function (value) {
        var _this = this;
        debugger;
        this._CommanService.GetAllPayBillGroup(value, this.flag).subscribe(function (result) {
            _this.ArrddlBillGroup = result;
            _this.Bill = result;
            //console.log(result);
            _this.billCtrl.setValue(_this.Bill);
            _this.filteredBill.next(_this.Bill);
        });
    };
    EntryforallemployeeComponent.prototype.BindDropDownDesignation = function (value) {
        var _this = this;
        this.PayBillGroupId = value;
        this.getEmployee('00003', value, 0);
        this._CommanService.GetAllDesignation(value, this.flag).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            //console.log(data);
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
        this.selectionAttach.clear();
    };
    EntryforallemployeeComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        // get the search keyword
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    EntryforallemployeeComponent.prototype.filterBill = function () {
        if (!this.Bill) {
            return;
        }
        // get the search keyword
        var search = this.billFilterCtrl.value;
        if (!search) {
            this.filteredBill.next(this.Bill.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBill.next(this.Bill.filter(function (Bill) { return Bill.billgrDesc.toLowerCase().indexOf(search) > -1; }));
    };
    EntryforallemployeeComponent.prototype.FilterEmployeeByDesignation = function (value) {
        var _this = this;
        // this.getEmployee('00003', this.PayBillGroupId, value);
        this._Service.GetEmployeeByPaybillAndDesig('00003', this.PayBillGroupId, value).subscribe(function (data) {
            _this.EmpList = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](data);
            while (_this.GetEmployeeList.length != 0) {
                _this.GetEmployeeList.removeAt(0);
            }
            for (var i = 0; i < _this.EmpList.length; i++) {
                _this.GetEmployeeList.push(_this.createItem());
                _this.GetEmployeeList.at(i).get('empCode').setValue(_this.EmpList[i].empCode);
                _this.GetEmployeeList.at(i).get('empName').setValue(_this.EmpList[i].empName);
                _this.GetEmployeeList.at(i).get('subscriptionAmt').setValue(_this.checkBoxValue ? +_this.EntryForAllEmployee.get('subscriptionAmt').value : null);
                _this.GetEmployeeList.at(i).get('recoveryAmt').setValue(_this.checkBoxValue ? +_this.EntryForAllEmployee.get('recoveryAmt').value : null);
                _this.GetEmployeeList.at(i).get('totalAmt').setValue(_this.checkBoxValue ? +_this.EntryForAllEmployee.get('totalAmt').value : null);
                _this.GetEmployeeList.at(i).get('installmentCount').setValue(null);
                _this.GetEmployeeList.at(i).get('recoveredInstallment').setValue(null);
            }
            _this.selectionAttach.clear();
        });
    };
    EntryforallemployeeComponent.prototype.onChange = function (event, index, item) {
        debugger;
        this.checkBoxValue = event.checked;
        if (event.checked) {
            //this.GetEmployeeList;
            // var abc = this.EntryForAllEmployee.get('universalAmount').value * this.EmpList.length;
            for (var i = 0; i < this.EmpList.length; i++) {
                //this.GetEmployeeList.push(this.createItem());
                this.GetEmployeeList.at(i).get('subscriptionAmt').setValue(this.EntryForAllEmployee.get('universalAmount').value);
                this.RowSubsSum(this.EntryForAllEmployee.get('universalAmount').value, i);
                console.log(i);
            }
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.GetEmployeeList.value);
            //this.updateDatasource();
            //this.EntryForAllEmployee.get('totalAmount').setValue(this.EmpList.length * abc);
        }
        else {
            for (var i = 0; i < this.EmpList.length; i++) {
                //this.GetEmployeeList.push(this.createItem());
                this.GetEmployeeList.at(i).get('amount').setValue(0);
            }
        }
        //this.SumAmount();
    };
    EntryforallemployeeComponent.prototype.allChkChange = function (event) {
        if (event.checked) {
        }
    };
    EntryforallemployeeComponent.prototype.SaveNgDetails = function () {
        debugger;
        this.dataList = this.EntryForAllEmployee.value;
        this.checkedObject;
        var abcd = this.EntryForAllEmployee.get('ngRecovery').value;
        for (var i = 0; i < this.checkedObject.length; i++) {
            this.checkedObject[i].ngDedCd = this.EntryForAllEmployee.get('ngRecovery').value;
            this.checkedObject[i].permDdoId = '00003';
        }
        this._Service.InsertNgrecoveryEntry(this.checkedObject).subscribe(function (res) {
            //this.BindAllBillGroup('00003');
        });
        //var abcf = this.checkedObject;
        this.form.resetForm();
    };
    EntryforallemployeeComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        debugger;
        this.checkedObject = null;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.GetEmployeeList.value.forEach(function (row) { return _this.selectionAttach.select(row); });
        this.checkedObject = this.GetEmployeeList.value;
        this.checkedObject = this.selectionAttach.selected;
        this.SumAmount();
        // alert(this.selectionAttach.selected);
    };
    EntryforallemployeeComponent.prototype.isAllSelectedForAttach = function () {
        //debugger;
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    EntryforallemployeeComponent.prototype.AddCheckedObj = function () {
        debugger;
        this.checkedObject = null;
        this.checkedObject = this.selectionAttach.selected;
        this.SumAmount();
        //console.log(this.selectionAttach);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], EntryforallemployeeComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], EntryforallemployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], EntryforallemployeeComponent.prototype, "sort", void 0);
    EntryforallemployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-entryforallemployee',
            template: __webpack_require__(/*! ./entryforallemployee.component.html */ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.html"),
            styles: [__webpack_require__(/*! ./entryforallemployee.component.css */ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__["PayBillGroupService"], _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_3__["CommanService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], EntryforallemployeeComponent);
    return EntryforallemployeeComponent;
}());

var ELEMENT_DATA = [];


/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.css":
/*!*********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.css ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9uZy1yZWNvdmVyeS9lbnRyeWZvcnNpbmdsZWVtcGxveWVlL2VudHJ5Zm9yc2luZ2xlZW1wbG95ZWUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.html":
/*!**********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [data]=\"callTypevar\"></app-comman-mst>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-7\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Enter NG Recovery for all Employees</div>\r\n\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Financial Year\">\r\n          <!--<mat-option>2019-2020</mat-option>-->\r\n          <mat-option *ngFor=\"let financialYears of financialYearList\" [value]=\"financialYears.financialYear\">{{financialYears.fullDescription}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Month\">\r\n          <!--<mat-option>May</mat-option>-->\r\n          <mat-option *ngFor=\"let months of monthList\" [value]=\"months.msMonthid\">{{months.monthName}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    \r\n    <table matSort class=\"mat-elevation-z8 even-odd-color tabel-form wid-100 recovery-tabel\" border=\"1\">\r\n      <tr class=\"table-head\">\r\n        <th>Sl.No</th>\r\n        <th>\r\n          NG Recovery Code & Description\r\n        </th>\r\n        <th>Amount (Rs)</th>\r\n        <th>No. of Installments</th>\r\n        <th>Recovered no. of Installments</th>\r\n        <th>Function</th>\r\n      </tr>\r\n      <tr>\r\n        <td>\r\n          1\r\n        </td>\r\n        <td>\r\n          <mat-form-field>\r\n            <mat-select placeholder=\"\" required>\r\n              <mat-option value=\"option1\">Kerala State Fund-002 </mat-option>\r\n              <mat-option value=\"option2\">AAO Association-003</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </td>\r\n        <td>\r\n          <mat-form-field>\r\n            <input matInput placeholder=\"\" required>\r\n          </mat-form-field>\r\n        </td>\r\n        <td>\r\n          <mat-form-field>\r\n            <input matInput placeholder=\"\" required>\r\n          </mat-form-field>\r\n        </td>\r\n        <td>\r\n          \r\n        </td>\r\n        <td>\r\n          <i class=\"material-icons i-delet\">delete</i>\r\n        </td>\r\n      </tr>\r\n    </table>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-right mt-10 margin-top-10px\">\r\n      <a class=\"btn btn-success add-leave-btn \" matTooltip=\"Add\"><i class=\"material-icons\">playlist_add</i></a>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n      <button type=\"reset\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n<!-------------------------right table----------------------------------->\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">NG Recoveries</div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table matSort class=\"mat-elevation-z8 even-odd-color wid-100\">\r\n      <tr>\r\n        <th>S.No</th>\r\n        <th>NGR Code</th>\r\n        <th>NG Recovery Description</th>\r\n        <th>Amount (Rs)</th>\r\n        <th>Action</th>\r\n      </tr>\r\n      <tr>\r\n        <td>\r\n          1\r\n        </td>\r\n        <td>\r\n          001\r\n        </td>\r\n        <td>\r\n          AAO Association\r\n        </td>\r\n        <td>100</td>\r\n        <td>\r\n          <a class=\"material-icons i-info\" matTooltip=\"info\">info</a>\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Edit\">edit</a>\r\n          <a class=\"material-icons i-delet\" matTooltip=\"Edit\">delete</a>\r\n        </td>\r\n      </tr>\r\n      <tr>\r\n        <td>\r\n          2\r\n        </td>\r\n        <td>\r\n          002\r\n        </td>\r\n        <td>\r\n          AICAGRBGZ ASSOCIATION\r\n        </td>\r\n        <td>300          </td>\r\n        <td>\r\n          <a class=\"material-icons i-info\" matTooltip=\"info\">info</a>\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Edit\">edit</a>\r\n          <a class=\"material-icons i-delet\" matTooltip=\"Edit\">delete</a>\r\n        </td>\r\n      </tr>\r\n    </table>\r\n\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: EntryforsingleemployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntryforsingleemployeeComponent", function() { return EntryforsingleemployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EntryforsingleemployeeComponent = /** @class */ (function () {
    function EntryforsingleemployeeComponent(_Service, fb, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this.snackBar = snackBar;
        this.financialYearList = [];
        this.monthList = [];
    }
    EntryforsingleemployeeComponent.prototype.GetcommanMethod = function (value) {
        debugger;
        //alert(1);
        //this.GetAleadytakenLoandetails(value);
        localStorage.setItem('EmployeeCd', value);
    };
    EntryforsingleemployeeComponent.prototype.ngOnInit = function () {
        this.bindFinancialYear();
        this.bindAllMonth();
    };
    EntryforsingleemployeeComponent.prototype.bindFinancialYear = function () {
        var _this = this;
        debugger;
        this._Service.GetFinancialYear().subscribe(function (data) {
            _this.financialYearList = data;
        });
    };
    EntryforsingleemployeeComponent.prototype.bindAllMonth = function () {
        var _this = this;
        debugger;
        this._Service.GetAllMonth().subscribe(function (data) {
            _this.monthList = data;
            console.log(_this.monthList);
            //console.log(data[0].schemeCode);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], EntryforsingleemployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], EntryforsingleemployeeComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], EntryforsingleemployeeComponent.prototype, "sort", void 0);
    EntryforsingleemployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-entryforsingleemployee',
            template: __webpack_require__(/*! ./entryforsingleemployee.component.html */ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.html"),
            styles: [__webpack_require__(/*! ./entryforsingleemployee.component.css */ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], EntryforsingleemployeeComponent);
    return EntryforsingleemployeeComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.css":
/*!*************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9uZy1yZWNvdmVyeS9maWxldXBsb2FkZm9yYWxsZW1wbG95ZWUvZmlsZXVwbG9hZGZvcmFsbGVtcGxveWVlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"col-md-6 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <app-headerforallemployee></app-headerforallemployee>\r\n    <!--<div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"NG Recovery\">\r\n          <mat-option>Kerala State Relief Fund</mat-option>\r\n          <mat-option>Kerala State Relief Fund</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Scroll NGR from Previous Month?\">\r\n          <mat-option>Yes</mat-option>\r\n          <mat-option>No</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Pay Bill Group\">\r\n          <mat-option>Select Pay Bill Group</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Designation\">\r\n          <mat-option>Select Designation</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Amount \">\r\n      </mat-form-field>\r\n    </div>-->\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n      <div class=\"fom-title\">NG Recoveries</div>\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 upload-container\">\r\n        <div class=\"col-sm-12 col-md-4 col-lg-2 pading-0\"><label>Upload File</label></div>\r\n        <div class=\"col-sm-12 col-md-8 col-lg-6\"><input type=\"file\"></div>\r\n        <button type=\"button\">Upload</button>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12\"><label>Download Template</label><a class=\"material-icons\">vertical_align_bottom</a></div>\r\n\r\n\r\n\r\n      <table matSort class=\"mat-elevation-z8 even-odd-color tabel-form wid-100 recovery-tabel\" border=\"1\">\r\n        <tr class=\"table-head\">\r\n\r\n          <th>SI.No</th>\r\n          <th>Employee Code</th>\r\n          <th>Employee Name</th>\r\n          <th>NG Recovery</th>\r\n          <th>Amount</th>\r\n\r\n        </tr>\r\n\r\n        <tr>\r\n          <td>\r\n            1\r\n          </td>\r\n          <td>\r\n            ABC123\r\n          </td>\r\n          <td>\r\n            Anik Khanna\r\n          </td>\r\n          <td>\r\n            ewqewqe\r\n          </td>\r\n          <td>\r\n           45454\r\n          </td>\r\n\r\n        </tr>\r\n\r\n       \r\n\r\n        <tr class=\"tabel-total\">\r\n          <td colspan=\"4\" class=\"text-center \">Total</td>\r\n          <td>500</td>\r\n        </tr>\r\n\r\n      </table>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n      <button type=\"reset\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n\r\n<!--right table-->\r\n\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <div class=\"fom-title\">Enter NG Recovery for all Employees</div>\r\n\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: FileuploadforallemployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileuploadforallemployeeComponent", function() { return FileuploadforallemployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { Component, OnInit } from '@angular/core';




var FileuploadforallemployeeComponent = /** @class */ (function () {
    function FileuploadforallemployeeComponent(_Service, fb, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this.snackBar = snackBar;
    }
    //bindFinancialYear: any = [];
    FileuploadforallemployeeComponent.prototype.ngOnInit = function () {
        //this.bindFinancialYear();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], FileuploadforallemployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], FileuploadforallemployeeComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], FileuploadforallemployeeComponent.prototype, "sort", void 0);
    FileuploadforallemployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-fileuploadforallemployee',
            template: __webpack_require__(/*! ./fileuploadforallemployee.component.html */ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.html"),
            styles: [__webpack_require__(/*! ./fileuploadforallemployee.component.css */ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], FileuploadforallemployeeComponent);
    return FileuploadforallemployeeComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9uZy1yZWNvdmVyeS9oZWFkZXJmb3JhbGxlbXBsb3llZS9oZWFkZXJmb3JhbGxlbXBsb3llZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"col-md-12 col-lg-6\">\r\n  <mat-form-field class=\"wid-100\">\r\n    <mat-select placeholder=\"Financial Year\">\r\n      <mat-option *ngFor=\"let financialYears of financialYearList\" [value]=\"financialYears.financialYear\">{{financialYears.fullDescription}}</mat-option>\r\n    </mat-select>\r\n\r\n  </mat-form-field>\r\n</div>-->\r\n<!--<div class=\"col-md-12 col-lg-6\">\r\n  <mat-form-field class=\"wid-100\">\r\n    <mat-select placeholder=\"Month\">\r\n      <mat-option *ngFor=\"let months of monthList\" [value]=\"months.msMonthid\">{{months.monthName}}</mat-option>\r\n    </mat-select>\r\n  </mat-form-field>\r\n</div>-->\r\n<div class=\"col-md-12 col-lg-12 combo-col clear-both select-type\">\r\n  <mat-radio-group [(ngModel)]=\"selectedRadio\" name=\"RadioButton\" aria-label=\"Select an option\">\r\n    <mat-radio-button value=\"1\" (change)=\"toggleRecComp(1)\">Data Entry</mat-radio-button>\r\n    <mat-radio-button value=\"2\" (change)=\"toggleRecComp(2)\">File Upload</mat-radio-button>\r\n  </mat-radio-group>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: HeaderforallemployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderforallemployeeComponent", function() { return HeaderforallemployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HeaderforallemployeeComponent = /** @class */ (function () {
    function HeaderforallemployeeComponent(_Service, fb, snackBar, router) {
        this._Service = _Service;
        this.fb = fb;
        this.snackBar = snackBar;
        this.router = router;
        this.monthList = [];
        this.financialYearList = [];
    }
    HeaderforallemployeeComponent.prototype.ngOnInit = function () {
        this.bindFinancialYear();
        this.bindAllMonth();
        if (this.router.url === '/dashboard/billGroup/ngrecovery/fileuploadforallemployee')
            this.selectedRadio = '2';
        else
            this.selectedRadio = '1';
    };
    HeaderforallemployeeComponent.prototype.toggleRecComp = function (value) {
        if (value == "1") {
            this.router.navigate(['dashboard/billGroup/ngrecovery/entryforallemployee']);
        }
        else {
            this.router.navigate(['dashboard/billGroup/ngrecovery/fileuploadforallemployee']);
        }
    };
    HeaderforallemployeeComponent.prototype.bindFinancialYear = function () {
        var _this = this;
        debugger;
        this._Service.GetFinancialYear().subscribe(function (data) {
            _this.financialYearList = data;
            console.log(_this.financialYearList);
            //console.log(data[0].schemeCode);
        });
    };
    HeaderforallemployeeComponent.prototype.bindAllMonth = function () {
        var _this = this;
        debugger;
        this._Service.GetAllMonth().subscribe(function (data) {
            _this.monthList = data;
            console.log(_this.monthList);
            //console.log(data[0].schemeCode);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], HeaderforallemployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], HeaderforallemployeeComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], HeaderforallemployeeComponent.prototype, "sort", void 0);
    HeaderforallemployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-headerforallemployee',
            template: __webpack_require__(/*! ./headerforallemployee.component.html */ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.html"),
            styles: [__webpack_require__(/*! ./headerforallemployee.component.css */ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], HeaderforallemployeeComponent);
    return HeaderforallemployeeComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "tr > td:nth-child(6){text-align:center;}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlsbC1ncm91cC1tZ210L25nLXJlY292ZXJ5L25nLXJlY292ZXJ5LWNyZWF0aW9uL25nLXJlY292ZXJ5LWNyZWF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEscUJBQXFCLGtCQUFrQixDQUFDIiwiZmlsZSI6InNyYy9hcHAvYmlsbC1ncm91cC1tZ210L25nLXJlY292ZXJ5L25nLXJlY292ZXJ5LWNyZWF0aW9uL25nLXJlY292ZXJ5LWNyZWF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ciA+IHRkOm50aC1jaGlsZCg2KXt0ZXh0LWFsaWduOmNlbnRlcjt9XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Success!</strong> This alert box could indicate a successful or positive action.\r\n  </div>\r\n  <div class=\"alert alert-info alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        NG Recovery Creation Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <form [formGroup]=\"ngRecoveryCreationForm\" #creationForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"saveNgRecovery()\">\r\n\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"NG Recovery Description\" name=\"ngDesc\" [(ngModel)]=\"ngRecoveryCreation.ngDesc\" formControlName=\"ngDesc\" pattern=\"[-(),./a-zA-Z \\\\]*\">\r\n          <mat-error *ngIf=\"ngRecoveryCreationForm.controls.ngDesc.errors && ngRecoveryCreationForm.controls.ngDesc.errors.required\">\r\n            NgRecovery Description Required!\r\n          </mat-error>\r\n          <mat-error *ngIf=\"ngRecoveryCreationForm.controls.ngDesc.errors && ngRecoveryCreationForm.controls.ngDesc.errors.pattern\">\r\n            Invalid Text\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Address for Sending Schedules\" name=\"address\" [(ngModel)]=\"ngRecoveryCreation.address\" formControlName=\"address\" pattern=\"[-(),./a-zA-Z0-9 \\\\]*\">\r\n          <mat-error *ngIf=\"ngRecoveryCreationForm.controls.address.errors && ngRecoveryCreationForm.controls.address.errors.required\">\r\n            Address Required!\r\n          </mat-error>\r\n          <mat-error *ngIf=\"ngRecoveryCreationForm.controls.address.errors && ngRecoveryCreationForm.controls.address.errors.pattern\">\r\n            Invalid Text\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-4 col-lg-4 combo-col\">\r\n\r\n        <div class=\"col-md-6 col-lg-6 pading-0\">\r\n          <label>Does have Bank Details?</label>\r\n        </div>\r\n\r\n        <div class=\"col-md-6 col-lg-6\">\r\n          <mat-radio-group (change)=\"checkBankDetails($event.value)\" name=\"isBankDetails\" formControlName=\"isBankDetails\">\r\n            <mat-radio-button value=\"No\">No</mat-radio-button>\r\n            <mat-radio-button value=\"Yes\">Yes</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n\r\n        <!-- <mat-radio-group [(ngModel)]=\"billGroupCreation.billForGAR\" name=\"billForGAR\" formControlName=\"billForGAR\" [disabled]=\"disableFlag\">\r\n          <mat-radio-button value=\"true\" class=\"ml-10\">Yes</mat-radio-button>\r\n          <mat-radio-button value=\"false\" class=\"ml-10\">No</mat-radio-button>\r\n        </mat-radio-group> -->\r\n\r\n      </div>\r\n\r\n      <div class=\"col-lg-12 leave-wraper add-form\" *ngIf=\"showBankDetails\">\r\n        <div class=\"fom-title\">Bank Details</div>\r\n        <!--<div class=\"col-md-12 col-lg-12 pading-0\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"NG Recovery Description\">\r\n          </mat-form-field>\r\n        </div>-->\r\n        <div class=\"col-md-10 pading-0\">\r\n          <div class=\"col-md-6 col-lg-4 \">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"IFSC Code\" name=\"ifscCd\" [(ngModel)]=\"ngRecoveryCreation.ifscCd\" formControlName=\"ifscCd\">\r\n              <mat-error>\r\n                IFSC Code Required!\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.get('ifscCd').errors && ngRecoveryCreationForm.get('ifscCd').errors.incorrect == true\">\r\n              <strong>Please enter valid Ifsc Code</strong>\r\n            </mat-error>\r\n          </div>\r\n          <input type=\"button\" class=\"btn-search\" value=\"Go\" (click)=\"getBankdetailsByIfsc(ngRecoveryCreation.ifscCd)\">\r\n        </div>\r\n\r\n        <!--Go</input>-->\r\n\r\n\r\n        <div class=\"col-md-6 col-lg-4 display-non\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <!--<textarea matInput placeholder=\"Name\" required></textarea>-->\r\n            <input matInput name=\"bankId\" [(ngModel)]=\"ngRecoveryCreation.bankId\" formControlName=\"bankId\" [disabled]=\"disableFlag\">\r\n            <!--<textarea matInput placeholder=\"First Name\" required>-->\r\n\r\n          </mat-form-field>\r\n        </div>\r\n\r\n\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Bank Name\" disabled name=\"bankName\" [(ngModel)]=\"ngRecoveryCreation.bankName\" formControlName=\"bankName\" readonly>\r\n            <mat-error>\r\n              Bank Name Required!\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4 \">\r\n          <!--<mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Branch Name\" disabled name=\"branchName\" [(ngModel)]=\"ngRecoveryCreation.branchName\" formControlName=\"branchName\">\r\n          </mat-form-field>-->\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select Branch\" [(ngModel)]=\"ngRecoveryCreation.branchId\" (selectionChange)=\"onBranchSelection($event.value)\" name=\"branchName\" formControlName=\"branchId\">\r\n              <!--<mat-option value=\"option\">GPF</mat-option>\r\n              <mat-option value=\"option\">GPF</mat-option>-->\r\n              <mat-option *ngFor=\"let bankDetails of getBankDetails\" [value]=\"bankDetails.branchId\">{{bankDetails.branchName}}</mat-option>\r\n\r\n            </mat-select>\r\n            <mat-error>\r\n              Branch Name Required!\r\n            </mat-error>\r\n            <!--<mat-error>required</mat-error>-->\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4 \">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Beneficiary Name\" name=\"bnfName\" [(ngModel)]=\"ngRecoveryCreation.bnfName\" formControlName=\"bnfName\">\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bnfName.errors && ngRecoveryCreationForm.controls.bnfName.errors.required\">\r\n              Beneficiary Name Required!\r\n            </mat-error>\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bnfName.errors && ngRecoveryCreationForm.controls.bnfName.errors.pattern\">\r\n              Invalid Text\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Saving A/C Number\" name=\"bankAcNo\" [(ngModel)]=\"ngRecoveryCreation.bankAcNo\" formControlName=\"bankAcNo\" pattern=\"^(?=.*\\d)(?=.*[1-9]).{1,17}$\" autocomplete=\"off\">.\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bankAcNo.errors && ngRecoveryCreationForm.controls.bankAcNo.errors.required\">\r\n              Saving A/C Number Required!\r\n            </mat-error>\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bankAcNo.errors && ngRecoveryCreationForm.controls.bankAcNo.errors.pattern\">\r\n              Invalid Text\r\n            </mat-error>\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bankAcNo.errors && ngRecoveryCreationForm.controls.bankAcNo.errors.minlength\">\r\n              Minimum length of account number is 9\r\n            </mat-error>\r\n            <mat-error *ngIf=\"ngRecoveryCreationForm.controls.bankAcNo.errors && ngRecoveryCreationForm.controls.bankAcNo.errors.maxlength\">\r\n              Max length of account number is 17\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"Submit\" class=\"btn btn-success\">{{_btnText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"CancelClick()\">Cancel</button>\r\n      </div>\r\n\r\n\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!--right table-->\r\n<!--<div class=\"col-md-12 col-lg-8\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">NG Recovery Details</div>\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n  </div>\r\n</div>-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Edit Details\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFiltercurrent($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <!-- commonDesigSrNo Column -->\r\n      <!--<ng-container matColumnDef=\"commonDesigSrNo\">\r\n        <th mat-header-cell *matHeaderCellDef> Designation Code </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          {{element.commonDesigSrNo}}\r\n\r\n        </td>\r\n      </ng-container>-->\r\n      <!-- Designation Column -->\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> S.No </th>\r\n        <td mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n      <ng-container matColumnDef=\"NGR Code\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"width: 30px;\"> NGR Code </th>\r\n        <td mat-cell *matCellDef=\"let element ; let i = index;\">{{element.ngDedCd}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"PFMS  Unique Id\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> PFMS  Unique Id </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.pfmsUniqueCode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Superannuation Column -->\r\n      <ng-container matColumnDef=\"NG Recovery Description\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>NG Recovery Description</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.ngDesc}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"IFSC Code\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>IFSC Code</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.ifscCd}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Bank A/C No.\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Bank A/C No.</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.bankAcNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Vendor Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"width: 60px;\">Vendor Status</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.vendorStatus}} </td>\r\n      </ng-container>\r\n\r\n      <!--<ng-container matColumnDef=\"Activate/ Deactivate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Activate/ Deactivate</th>\r\n        <td mat-cell *matCellDef=\"let element\"> Deactivate </td>\r\n      </ng-container>-->\r\n\r\n\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!-- <a href=\"#\">Edit</a> -->\r\n          <!--<a class=\"material-icons i-info\" matTooltip=\"Info\">error</a>-->\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">edit</a>\r\n          <span *ngIf=\"!element.isActive\">\r\n            <a class=\"material-icons i-activate\" matTooltip=\"activate\" (click)=\"fillPopup(element.payNgRecoId, true);activatePopup = !activatePopup\">check_circle</a>\r\n          </span>\r\n          <span *ngIf=\"element.isActive\">\r\n            <a class=\"material-icons i-dactivate\" matTooltip=\"Deactivate\" (click)=\"fillPopup(element.payNgRecoId, false);deactivatePopup = !deactivatePopup\">\r\n              highlight_off\r\n            </a>\r\n          </span>\r\n          <!--<a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.commonDesigDesc,element.commonDesigGroupCD,element.commonDesigSrNo,element.commonDesigSuperannAge)\">edit</a>-->\r\n          <!--<a class=\"material-icons i-delet\" matTooltip=\"Delete\"> delete_forever </a>-->\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"rowHighlight(row)\"\r\n          [style.background]=\"highlightedRows.indexOf(row) != -1 ? 'lightgreen' : ''\"></tr>\r\n\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n    <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure to Deactivate?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"activateDeactivate();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"deactivatePopup = !deactivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"activatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure to Activate?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"activateDeactivate();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"activatePopup = !activatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: NgRecoveryCreationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgRecoveryCreationComponent", function() { return NgRecoveryCreationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { CommonMsg } from 'src/app/global/common-msg';

var NgRecoveryCreationComponent = /** @class */ (function () {
    function NgRecoveryCreationComponent(_Service, fb, _msg, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this._msg = _msg;
        this.snackBar = snackBar;
        this.is_btnStatus = true;
        this.showBankDetails = true;
        //applyFilter: any;
        this.editMode = false;
        this.displayedColumns = ['S.No', 'NGR Code', 'PFMS  Unique Id', 'NG Recovery Description', 'IFSC Code', 'Bank A/C No.', 'Vendor Status', 'Action'];
        this.highlightedRows = [];
        this.getBankDetails = [];
        this.ngRecoveryCreation = {
            payNgRecoId: 0,
            ngDesc: null,
            ngDedCd: null,
            address: null,
            isBankDetails: "Yes",
            ifscCd: null,
            bankId: null,
            bankName: null,
            branchId: null,
            branchName: null,
            bnfName: null,
            bankAcNo: null,
            isActive: null,
            permDDOId: null,
            pfmsUniqueCode: null,
            venderStatus: null,
        };
        this.allData = [];
        this.createForm();
    }
    NgRecoveryCreationComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    NgRecoveryCreationComponent.prototype.ngOnInit = function () {
        this.requiredvalidation = false;
        this.getNgrecovery('00003');
        this._btnText = 'Save';
    };
    NgRecoveryCreationComponent.prototype.createForm = function () {
        this.ngRecoveryCreationForm = this.fb.group({
            //ngDesc: ['', [Validators.required, Validators.pattern('[-(),./a-zA-Z ]*')]],
            //address: ['', [Validators.required, Validators.pattern('[-(),./a-zA-Z ]*')]],
            ngDesc: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            address: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            isBankDetails: ['Yes', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            ifscCd: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            bankId: ['', ''],
            bankName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            branchId: ['', ''],
            branchName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            bnfName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[a-zA-Z ]*')]],
            bankAcNo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(17), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[0-9]*$')]],
            payNgRecoId: [0],
            permDDOId: [''],
            isActive: [false],
            ngDedCd: [0],
        });
        console.log(this.ngRecoveryCreationForm);
    };
    NgRecoveryCreationComponent.prototype.getNgrecovery = function (permDDOId) {
        var _this = this;
        this._Service.GetNgRecovery(permDDOId).subscribe(function (data) {
            //this.getNgrecoveryData = data;
            _this.allData = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
            console.log(_this.dataSource);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    NgRecoveryCreationComponent.prototype.checkBankDetails = function (value) {
        this.showBankDetails = value == "No" ? false : true;
        if (value == "No") {
            debugger;
            //alert('No');
            //this.ngRecoveryCreationForm.get('ifscCd').setValidators([Validators.required]);
            // this.ngRecoveryCreationForm.controls['ifscCd'].setValidators(null);
            //================= by manoj pal================================
            this.ngRecoveryCreationForm.controls['ifscCd'].setErrors(null);
            this.ngRecoveryCreationForm.controls['bankName'].setErrors(null);
            this.ngRecoveryCreationForm.controls['branchName'].setErrors(null);
            this.ngRecoveryCreationForm.controls['bnfName'].setErrors(null);
            this.ngRecoveryCreationForm.controls['bankAcNo'].setErrors(null);
            //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();
            //this.ngRecoveryCreationForm.get("ifscCd").updateValueAndValidity();
            //this.ngRecoveryCreationForm.get("ifscCd").setValidators([Validators.required]);
        }
        if (value == "Yes") {
            this.ngRecoveryCreationForm.get('ifscCd').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
            this.ngRecoveryCreationForm.get('bankName').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
            this.ngRecoveryCreationForm.get('branchName').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
            this.ngRecoveryCreationForm.get('bnfName').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[a-zA-Z ]*')]);
            this.ngRecoveryCreationForm.get('bankAcNo').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(9), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(17)]);
            //alert('Yes');
            //this.ngRecoveryCreationForm.get('ifscCd').clearValidators();
            //this.ngRecoveryCreationForm.get("ifscCd").updateValueAndValidity();
            //this.ngRecoveryCreationForm.get("ifscCd").setValidators([Validators.required]);
            //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();
            //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();
        }
    };
    NgRecoveryCreationComponent.prototype.getBankdetailsByIfsc = function (IfscCode) {
        var _this = this;
        debugger;
        this.ngRecoveryCreation.bankName = null;
        this.ngRecoveryCreation.branchName = null;
        this._Service.GetBankdetailsByIfsc(IfscCode).subscribe(function (data) {
            _this.getBankDetails = data;
            if (data.length == 0) {
                //this.ngRecoveryCreationForm.get('ifscCd')
                _this.ngRecoveryCreationForm.get('ifscCd').setErrors({ 'incorrect': true });
            }
            else {
                _this.ngRecoveryCreation.bankName = data[0].bankName;
                _this.ngRecoveryCreation.bankId = data[0].bankId;
            }
            //console.log(data[0].schemeCode);
        });
    };
    NgRecoveryCreationComponent.prototype.onBranchSelection = function (branchId) {
        var selectedBranch = this.getBankDetails.filter(function (a) { return a.branchId == branchId; });
        if (selectedBranch.length > 0) {
            this.ngRecoveryCreationForm.controls.branchName.setValue(selectedBranch[0].branchName);
        }
    };
    NgRecoveryCreationComponent.prototype.saveNgRecovery = function () {
        var _this = this;
        debugger;
        //ngRecoveryCreation.groupIds = '';
        //for (let entry of newBillGroup.group) {
        //  if (newBillGroup.groupIds == '') {
        //    newBillGroup.groupIds = entry;
        //  }
        //  else
        //    newBillGroup.groupIds = newBillGroup.groupIds + "," + entry;
        //}
        //newBillGroup.group = "";
        //console.log(newBillGroup);
        //newBillGroup.permDDOId = '00003';
        ////newBillGroup.payBillGroupId = 0;
        //this.ngRecoveryCreationForm.controls.payNgRecoId.setValue(ngRecoveryCreation.payNgRecoId)
        //ngRecoveryCreation.payNgRecoId = ngRecoveryCreation.payNgRecoId;
        //ngRecoveryCreation.ngDedCd = 0;
        //ngRecoveryCreation.permDDOId = '00003';
        //ngRecoveryCreation.isActive = false;
        //var abc = this.ngRecoveryCreationForm.controls.branchId.value;
        this.ngRecoveryCreationForm.controls.ngDedCd.setValue(0);
        this.ngRecoveryCreationForm.controls.permDDOId.setValue('00003');
        this.ngRecoveryCreationForm.controls.isActive.setValue(false);
        this.onBranchSelection(this.ngRecoveryCreationForm.controls.branchId.value);
        //console.log(this.ngRecoveryCreationForm);
        if (this.ngRecoveryCreationForm.valid) {
            this._Service.InsertUpdateNgRecovery(this.ngRecoveryCreationForm.value).subscribe(function (res) {
                //this.BindAllBillGroup('00003');
                _this.getNgrecovery('00003');
                if (_this.editMode) {
                    _this.snackBar.open('Updated successfully', null, { duration: 4000 });
                }
                else {
                    _this.snackBar.open('Saved successfully', null, { duration: 4000 });
                }
                _this.editMode = false;
            });
            this.createForm();
            this.form.resetForm();
            this._btnText = 'Save';
            this.highlightedRows.pop();
            this.form.resetForm();
            //this.ngOnInit();
        }
        //this.form.resetForm();
        //newBillGroup.billGroupName = '';
        //document.getElementById('btn_save').innerHTML = 'Save';
        //this.billGroupCreation.billForGAR = "false";
        //this.disableFlag = false;
    };
    NgRecoveryCreationComponent.prototype.btnEditClick = function (ngRecoveryCreation) {
        debugger;
        this._btnText = 'Update';
        this.editMode = true;
        if (ngRecoveryCreation.ifscCd == "") {
            this.ngRecoveryCreationForm.controls.isBankDetails.setValue("No");
            this.checkBankDetails("No");
        }
        else {
            this.ngRecoveryCreationForm.controls.isBankDetails.setValue("Yes");
            this.getBankdetailsByIfsc(ngRecoveryCreation.ifscCd);
            this.ngRecoveryCreationForm.controls.branchId.setValue(ngRecoveryCreation.branchId);
            this.ngRecoveryCreationForm.controls.ifscCd.setValue(ngRecoveryCreation.ifscCd);
            this.ngRecoveryCreationForm.controls.bnfName.setValue(ngRecoveryCreation.bnfName);
            this.ngRecoveryCreationForm.controls.bankAcNo.setValue(ngRecoveryCreation.bankAcNo);
            //this.ngRecoveryCreationForm.controls.isBankDetails.setValue(ngRecoveryCreation.isBankDetails);
            //this.ngRecoveryCreation.branchId = ngRecoveryCreation.branchId;
            //this.ngRecoveryCreation.ifscCd = ngRecoveryCreation.ifscCd;
            //this.ngRecoveryCreation.bnfName = ngRecoveryCreation.bnfName;
            //this.ngRecoveryCreation.bankAcNo = ngRecoveryCreation.bankAcNo;
            //this.ngRecoveryCreation.isBankDetails = "Yes";
            this.checkBankDetails("Yes");
        }
        this.ngRecoveryCreationForm.controls.payNgRecoId.setValue(ngRecoveryCreation.payNgRecoId);
        this.ngRecoveryCreationForm.controls.ngDesc.setValue(ngRecoveryCreation.ngDesc);
        this.ngRecoveryCreationForm.controls.address.setValue(ngRecoveryCreation.address);
        //this.ngRecoveryCreation.payNgRecoId = ngRecoveryCreation.payNgRecoId;
        //this.ngRecoveryCreation.ngDesc = ngRecoveryCreation.ngDesc;
        //this.ngRecoveryCreation.address = ngRecoveryCreation.address;
        //this._btnText = 'Update';
        //let billgroups: any = []
        //this.disableFlag = true;
        //debugger;
        //for (let entry of group.split(',')) {
        //  billgroups.push(entry.trim());
        //}
        //this.billGroupCreation.accountHead = schemeId;
        //this.billGroupCreation.serviceType = serviceType;
        //this.billGroupCreation.group = billgroups;
        ////this.groups = 'A';
        //this.billGroupCreation.billGroupName = billGroupName.trim();
        //this.billGroupCreation.billForGAR = String(billForGAR);
        //this.billGroupCreation.payBillGroupId = payBillGroupId;
        ////billGroupCreation.group='A'
        ////this.billGroupCreation.billForGAR = billForGAR;
        //document.getElementById('btn_save').innerHTML = 'Update';
    };
    //btnActiveClick(ngRecoveryCreation: NgRecovery) {
    //}
    NgRecoveryCreationComponent.prototype.fillPopup = function (payNgRecoId, status) {
        debugger;
        this.ngRecoveryCreation.payNgRecoId = payNgRecoId;
        this.ngRecoveryCreation.isActive = status;
        this.ngRecoveryCreation.ngDedCd = 0;
    };
    NgRecoveryCreationComponent.prototype.CancelClick = function () {
        this._btnText = 'Save';
        this.form.resetForm();
        this.highlightedRows.pop();
        this.ngRecoveryCreation.isBankDetails = "Yes";
        this.checkBankDetails("Yes");
    };
    //Cancel() {
    //  $(".dialog__close-btn").click();
    //}
    NgRecoveryCreationComponent.prototype.activateDeactivate = function () {
        var _this = this;
        debugger;
        this._Service.ActiveDeactiveNgRecovery(this.ngRecoveryCreation).subscribe(function (res) {
            _this.getNgrecovery("00003");
            if (_this.ngRecoveryCreation.isActive) {
                _this.snackBar.open('Activated successfully', null, { duration: 4000 });
            }
            else {
                _this.snackBar.open('Deactivated successfully', null, { duration: 4000 });
            }
        });
        // this.getNgrecovery("00003");
        // $(".dialog__close-btn").click();
        //this.getNgrecovery('00003');
        this.deactivatePopup = false;
        this.activatePopup = false;
        this.highlightedRows.pop();
    };
    NgRecoveryCreationComponent.prototype.rowHighlight = function (row) {
        this.highlightedRows.pop();
        this.highlightedRows.push(row);
    };
    NgRecoveryCreationComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](this.allData.filter(function (a) { return a.status.indexOf(filterValue) > -1; }));
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    NgRecoveryCreationComponent.prototype.charaterOnlyNoSpace = function (event) {
        var msg = this._msg.charaterOnlyNoSpace(event);
        return msg;
    };
    NgRecoveryCreationComponent.prototype.applyFiltercurrent = function (filterValue) {
        debugger;
        this.paginator.pageIndex = 0;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](this.allData.filter(function (a) { return a.ngDesc.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.ngDedCd.toString().indexOf(filterValue) > -1 || a.pfmsUniqueCode.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.ifscCd.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.bankAcNo.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.vendorStatus.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1; }));
        this.dataSource.paginator = this.paginator;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
        //this.dataSource.filter = filterValue.trim().toLowerCase();
        //if (this.dataSource.filteredData.length > 0) {
        //  this.dataSource.filter = filterValue.trim().toLowerCase();
        //  // this.dataSource.filter = filterValue.trim().toLowerCase();
        //} else {
        //  this.dataSource.filteredData.length = 0;
        //}
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], NgRecoveryCreationComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], NgRecoveryCreationComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], NgRecoveryCreationComponent.prototype, "sort", void 0);
    NgRecoveryCreationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ng-recovery-creation',
            template: __webpack_require__(/*! ./ng-recovery-creation.component.html */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.html"),
            styles: [__webpack_require__(/*! ./ng-recovery-creation.component.css */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], NgRecoveryCreationComponent);
    return NgRecoveryCreationComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ng-recovery-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: NgRecoveryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgRecoveryRoutingModule", function() { return NgRecoveryRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_recovery_creation_ng_recovery_creation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ng-recovery-creation/ng-recovery-creation.component */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.ts");
/* harmony import */ var _ng_recovery_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ng-recovery.module */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery.module.ts");
/* harmony import */ var _entryforallemployee_entryforallemployee_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./entryforallemployee/entryforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.ts");
/* harmony import */ var _headerforallemployee_headerforallemployee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./headerforallemployee/headerforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.ts");
/* harmony import */ var _fileuploadforallemployee_fileuploadforallemployee_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fileuploadforallemployee/fileuploadforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.ts");
/* harmony import */ var _entryforsingleemployee_entryforsingleemployee_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./entryforsingleemployee/entryforsingleemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.ts");
/* harmony import */ var _ngrprocessing_ngrprocessing_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ngrprocessing/ngrprocessing.component */ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '', component: _ng_recovery_module__WEBPACK_IMPORTED_MODULE_3__["NgRecoveryModule"], children: [
            { path: 'ngrecoverycreation', component: _ng_recovery_creation_ng_recovery_creation_component__WEBPACK_IMPORTED_MODULE_2__["NgRecoveryCreationComponent"] },
            { path: 'entryforallemployee', component: _entryforallemployee_entryforallemployee_component__WEBPACK_IMPORTED_MODULE_4__["EntryforallemployeeComponent"] },
            { path: 'headerforallemployee', component: _headerforallemployee_headerforallemployee_component__WEBPACK_IMPORTED_MODULE_5__["HeaderforallemployeeComponent"] },
            { path: 'fileuploadforallemployee', component: _fileuploadforallemployee_fileuploadforallemployee_component__WEBPACK_IMPORTED_MODULE_6__["FileuploadforallemployeeComponent"] },
            { path: 'entryforsingleemployee', component: _entryforsingleemployee_entryforsingleemployee_component__WEBPACK_IMPORTED_MODULE_7__["EntryforsingleemployeeComponent"] },
            { path: 'ngrprocessing', component: _ngrprocessing_ngrprocessing_component__WEBPACK_IMPORTED_MODULE_8__["NGRprocessingComponent"] }
        ]
    }
];
var NgRecoveryRoutingModule = /** @class */ (function () {
    function NgRecoveryRoutingModule() {
    }
    NgRecoveryRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], NgRecoveryRoutingModule);
    return NgRecoveryRoutingModule;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ng-recovery.module.ts ***!
  \*******************************************************************/
/*! exports provided: NgRecoveryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgRecoveryModule", function() { return NgRecoveryModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_recovery_creation_ng_recovery_creation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ng-recovery-creation/ng-recovery-creation.component */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-creation/ng-recovery-creation.component.ts");
/* harmony import */ var _ng_recovery_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ng-recovery-routing.module */ "./src/app/bill-group-mgmt/ng-recovery/ng-recovery-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _entryforallemployee_entryforallemployee_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./entryforallemployee/entryforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/entryforallemployee/entryforallemployee.component.ts");
/* harmony import */ var _headerforallemployee_headerforallemployee_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./headerforallemployee/headerforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/headerforallemployee/headerforallemployee.component.ts");
/* harmony import */ var _fileuploadforallemployee_fileuploadforallemployee_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./fileuploadforallemployee/fileuploadforallemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/fileuploadforallemployee/fileuploadforallemployee.component.ts");
/* harmony import */ var _entryforsingleemployee_entryforsingleemployee_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./entryforsingleemployee/entryforsingleemployee.component */ "./src/app/bill-group-mgmt/ng-recovery/entryforsingleemployee/entryforsingleemployee.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../loanmgt/loanmgt.module */ "./src/app/loanmgt/loanmgt.module.ts");
/* harmony import */ var _edit_view_all_employee_edit_view_all_employee_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./edit-view-all-employee/edit-view-all-employee.component */ "./src/app/bill-group-mgmt/ng-recovery/edit-view-all-employee/edit-view-all-employee.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _ngrprocessing_ngrprocessing_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ngrprocessing/ngrprocessing.component */ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//import { DialogComponent } from '../../dialog/dialog.component';













var NgRecoveryModule = /** @class */ (function () {
    function NgRecoveryModule() {
    }
    NgRecoveryModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_ng_recovery_creation_ng_recovery_creation_component__WEBPACK_IMPORTED_MODULE_2__["NgRecoveryCreationComponent"], _entryforallemployee_entryforallemployee_component__WEBPACK_IMPORTED_MODULE_6__["EntryforallemployeeComponent"], _headerforallemployee_headerforallemployee_component__WEBPACK_IMPORTED_MODULE_7__["HeaderforallemployeeComponent"], _fileuploadforallemployee_fileuploadforallemployee_component__WEBPACK_IMPORTED_MODULE_8__["FileuploadforallemployeeComponent"], _entryforsingleemployee_entryforsingleemployee_component__WEBPACK_IMPORTED_MODULE_9__["EntryforsingleemployeeComponent"], _edit_view_all_employee_edit_view_all_employee_component__WEBPACK_IMPORTED_MODULE_12__["EditViewAllEmployeeComponent"], _ngrprocessing_ngrprocessing_component__WEBPACK_IMPORTED_MODULE_14__["NGRprocessingComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ng_recovery_routing_module__WEBPACK_IMPORTED_MODULE_3__["NgRecoveryRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__["NgxMatSelectSearchModule"], _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_11__["LoanmgtModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_13__["SharedModule"]
            ]
        })
    ], NgRecoveryModule);
    return NgRecoveryModule;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n.mat-form-field {\r\n  font-size: 12px;\r\n  width: 100%;\r\n}\r\n\r\ntd, th {\r\n  width: 25%;\r\n  border-bottom: 0px !important;\r\n  margin: 5px;\r\n}\r\n\r\n.col-md-5 {\r\n  width:45%!important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlsbC1ncm91cC1tZ210L25nLXJlY292ZXJ5L25ncnByb2Nlc3NpbmcvbmdycHJvY2Vzc2luZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFdBQVc7RUFDWCw4QkFBOEI7RUFDOUIsWUFBWTtDQUNiOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCIiwiZmlsZSI6InNyYy9hcHAvYmlsbC1ncm91cC1tZ210L25nLXJlY292ZXJ5L25ncnByb2Nlc3NpbmcvbmdycHJvY2Vzc2luZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQsIHRoIHtcclxuICB3aWR0aDogMjUlO1xyXG4gIGJvcmRlci1ib3R0b206IDBweCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbjogNXB4O1xyXG59XHJcblxyXG4uY29sLW1kLTUge1xyXG4gIHdpZHRoOjQ1JSFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n  <!--------------- Employee Details Start\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">\r\n        Employee Details\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-7 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Employee Type\" required>\r\n            <mat-option value=\"option0\">Regular</mat-option>\r\n            <mat-option value=\"option1\">Co-Terminus</mat-option>\r\n            <mat-option value=\"option2\">Re-Employeer Pensioner</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n   Employee Details End --------------->\r\n  <!----Right Form Start-->\r\n  \r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <div class=\"alert alert-info\"><strong>To process the bill, please click on the bill to select.</strong></div>\r\n    <div class=\"example-card mat-card tabel-wraper even-odd-color\">\r\n      <div class=\"fom-title\">\r\n        NG Recovery Process\r\n      </div>\r\n      <!--\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n        -->\r\n      <mat-form-field class=\"col-md-12\">\r\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\"> <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- ID Column -->\r\n        <ng-container matColumnDef=\"SrNo\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr. No. </th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.SrNo}} </td>\r\n        </ng-container>\r\n        <!-- Progress Column -->\r\n        <ng-container matColumnDef=\"PayBillName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Pay Bill Group Name & Description\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.PayBillName}}</td>\r\n        </ng-container>\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"BillId\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Bill Id\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.BillId}} </td>\r\n        </ng-container>\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"BillDate\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Bill Date</th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.BillDate}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Noofemployee\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> No. of Employees in PayBill Group</th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.Noofemployee}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"BillType\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Bill Date\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\"> {{row.BillType}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Bill Type\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\">\r\n            <i class=\"material-icons i-info\" matTooltip=\"Information\">info</i>\r\n            <i class=\"material-icons i-edit\" matTooltip=\"Edit\">edit</i>\r\n            <i class=\"material-icons i-delet\" matTooltip=\"Delete\">delete</i>\r\n          </td>\r\n        </ng-container>\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n      </table>\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"><button class=\"btn btn-success\" type=\"button\">Process</button>\r\n  </div>\r\n  <!----Right Form End-->\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.ts ***!
  \**************************************************************************************/
/*! exports provided: NGRprocessingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NGRprocessingComponent", function() { return NGRprocessingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*import { Component, OnInit } from '@angular/core';*/


var NGRprocessingComponent = /** @class */ (function () {
    function NGRprocessingComponent() {
        this.displayedColumns = ['SrNo', 'PayBillName', 'BillId', 'BillDate', 'Noofemployee', 'BillType'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](NAMES);
    }
    NGRprocessingComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    NGRprocessingComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], NGRprocessingComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], NGRprocessingComponent.prototype, "sort", void 0);
    NGRprocessingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ngrprocessing',
            template: __webpack_require__(/*! ./ngrprocessing.component.html */ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.html"),
            styles: [__webpack_require__(/*! ./ngrprocessing.component.css */ "./src/app/bill-group-mgmt/ng-recovery/ngrprocessing/ngrprocessing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NGRprocessingComponent);
    return NGRprocessingComponent;
}());

/** Constants used to fill up our data base. */
var NAMES = [
    { SrNo: 1, PayBillName: 'This is a dummy text.', BillId: '3', BillDate: 4, Noofemployee: 343, BillType: 447 },
];
/**
 * @title Data table with sorting, pagination, and filtering.
 */
/** Builds and returns a new User. */


/***/ })

}]);
//# sourceMappingURL=bill-group-mgmt-ng-recovery-ng-recovery-module.js.map