(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["income-tax-income-tax-module"],{

/***/ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/income-tax/exemption-deduction/exemption-deduction.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-table {\r\n  border-collapse:collapse;\r\n}\r\n\r\n/*mat-row {\r\n  border: 1px solid #000000;\r\n}*/\r\n\r\nmat-cell {\r\n  border: 1px solid #000000;\r\n}\r\n\r\nmat-header-cell {\r\n  border: 1px solid #000000;\r\n}\r\n\r\nmat-header-row {\r\n  background-color: #3268ab;\r\n}\r\n\r\nmat-cell:last-of-type, mat-footer-cell:last-of-type, mat-header-cell:last-of-type {\r\n  padding-right: 0px;\r\n}\r\n\r\n.record-not-found {\r\n  text-align: center;\r\n  display: inline-block;\r\n  height: 33px;\r\n}\r\n\r\n.form-field-width {\r\n  width: 90px;\r\n}\r\n\r\n.bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jb21lLXRheC9leGVtcHRpb24tZGVkdWN0aW9uL2V4ZW1wdGlvbi1kZWR1Y3Rpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtDQUMxQjs7QUFFRDs7R0FFRzs7QUFFSDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFDRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixzQkFBc0I7RUFDdEIsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsNEJBQTRCO0NBQzdCIiwiZmlsZSI6InNyYy9hcHAvaW5jb21lLXRheC9leGVtcHRpb24tZGVkdWN0aW9uL2V4ZW1wdGlvbi1kZWR1Y3Rpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC10YWJsZSB7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOmNvbGxhcHNlO1xyXG59XHJcblxyXG4vKm1hdC1yb3cge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbn0qL1xyXG5cclxubWF0LWNlbGwge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbn1cclxubWF0LWhlYWRlci1jZWxsIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG59XHJcblxyXG5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMyNjhhYjtcclxufVxyXG5cclxubWF0LWNlbGw6bGFzdC1vZi10eXBlLCBtYXQtZm9vdGVyLWNlbGw6bGFzdC1vZi10eXBlLCBtYXQtaGVhZGVyLWNlbGw6bGFzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbn1cclxuXHJcbi5yZWNvcmQtbm90LWZvdW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGhlaWdodDogMzNweDtcclxufVxyXG5cclxuLmZvcm0tZmllbGQtd2lkdGgge1xyXG4gIHdpZHRoOiA5MHB4O1xyXG59XHJcblxyXG4uYmdjb2xvciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmJkYTk5O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/income-tax/exemption-deduction/exemption-deduction.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccessStatus\" (click)=\"isSuccessStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"isWarningStatus\" (click)=\"isWarningStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n\r\n<div class=\"col-md-12 mb-20\">\r\n  <mat-accordion>\r\n    <mat-expansion-panel [expanded]=\"isRateDetailPanel\" [ngClass]=\"bgColor\">\r\n      <mat-expansion-panel-header class=\"panel-header btm-m15\" (click)=\"resetRateDetailPanel()\" >\r\n        <mat-panel-title class=\"acordion-heading\">\r\n          Section Details\r\n        </mat-panel-title>\r\n\r\n      </mat-expansion-panel-header>\r\n           \r\n      <form [formGroup]=\"incomeTaxRatesForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"majorSectionCode\" (selectionChange)=\"BindSectionCode($event.value)\" placeholder=\"Major Section Code\" required>\r\n              <mat-option>Please Select</mat-option>\r\n              <mat-option *ngFor=\"let code of ArrddlMajSecCode\" value=\"{{code.majorSecCode}}\">{{code.majorSecDesc}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Major Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"sectionCode\" (selectionChange)=\"BindSubSectionCode()\" placeholder=\"Section Code\" required>\r\n              <mat-option>Please Select</mat-option>\r\n              <mat-option *ngFor=\"let code of ArrddlSecCode\" value=\"{{code.secCode}}\">{{code.secCode}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"subSectionCode\" (selectionChange)=\"BindSectionCodeDescription()\" placeholder=\"Sub Section Code\" required>\r\n              <mat-option>Please Select</mat-option>\r\n              <mat-option *ngFor=\"let code of ArrddlSubSecCode\" value=\"{{code.subSecCode}}\">{{code.subSecCode}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Sub Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput formControlName=\"sectionDesc\" placeholder=\"Section Description\" autocomplete=off readonly></textarea>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <mat-card class=\"example-card mat-card margin-top-10px\" [ngClass]=\"bgColor\">\r\n          <div class=\"fom-title\">Rate Years</div>\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <!--<input matInput [matDatepicker]=\"picker2\" placeholder=\"Financial Year From \">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker2></mat-datepicker>-->\r\n              <mat-select formControlName=\"financialYearFrom\" placeholder=\"Financial Year From\" required>\r\n                <mat-option value=\"\">Please Select</mat-option>\r\n                <mat-option *ngFor=\"let code of ArrddlYearFrom\" value=\"{{code.financialYear}}\">{{code.fullDescription}}</mat-option>\r\n              </mat-select>\r\n              <!-- <input matInput formControlName=\"financialYearFrom\" placeholder=\"Financial Year From\" (paste)=\"$event.preventDefault()\" autocomplete=off maxlength=\"9\"  onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)\">-->\r\n              <mat-error>Please select From Financial Year</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <!--<input matInput [matDatepicker]=\"picker3\" placeholder=\"Financial Year To \">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker3\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker3></mat-datepicker>-->\r\n              <mat-select formControlName=\"financialYearTo\" placeholder=\"Financial Year To\">\r\n                <mat-option value=\"\">Please Select</mat-option>\r\n                <mat-option *ngFor=\"let code of ArrddlYearTo\" value=\"{{code.financialYear}}\">{{code.fullDescription}}</mat-option>\r\n              </mat-select>\r\n              <!--<input matInput formControlName=\"financialYearTo\" placeholder=\"Financial Year To\" (paste)=\"$event.preventDefault()\" autocomplete=off maxlength=\"9\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)\">-->\r\n              <mat-error>Please select To Financial Year</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"slabLimitsFor\" placeholder=\"Slab Limit For\" required>\r\n                <mat-option value=\"\">Please Select</mat-option>\r\n                <mat-option *ngFor=\"let code of ArrddlSlabLimit\" value=\"{{code.slabLimitCode}}\">{{code.slabLimitDesc}}</mat-option>\r\n              </mat-select>\r\n              <mat-error>Please select Slab Limit</mat-error>\r\n            </mat-form-field>\r\n\r\n          </div>\r\n        </mat-card>\r\n\r\n        <mat-card class=\"example-card mat-card margin-top-10px\" [ngClass]=\"bgColor\">\r\n          <div class=\"fom-title\">Rate Details</div>\r\n          <div class=\"tabel-wraper wid-100\">\r\n            <table class=\"mat-elevation-z8 col-md-12 col-lg-12 table2 even-odd-color\" formArrayName=\"rateDetails\">\r\n              <tr>\r\n                <th>Lower Limit</th>\r\n                <th>Upper Limit</th>\r\n                <th>Value / %</th>\r\n                <th>Rebate (Normal)</th>\r\n                <th>Rebate (Special)</th>\r\n                <th>Max Saving</th>\r\n                <th>Action</th>\r\n              </tr>\r\n              <tr *ngFor=\"let rate of incomeTaxRatesForm.controls.rateDetails.controls; let i = index;\" [formGroupName]=\"i\">\r\n                <td>\r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <input matInput formControlName=\"lowerLimit\" placeholder=\"Lower Limit\" autocomplete=off maxlength=\"6\" (keyup)=\"validateLowerLimit(i)\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors.required == true\">Please enter Lower Limit</mat-error>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors.invalidError == true\">Should be less than Upper Limit</mat-error>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.lowerLimit.errors.limitOverlapError == true\">Lower Limit & Upper Limit value should not be overlap</mat-error>\r\n                    <!--<mat-error *ngIf=\"limitOverlapError == true\">Lower Limit & Upper Limit value should not be overlap</mat-error>-->\r\n                  </mat-form-field>\r\n                </td>\r\n                <td> \r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <input matInput formControlName=\"upperLimit\" placeholder=\"Upper Limit\" autocomplete=off maxlength=\"6\" (keyup)=\"validateLowerLimit(i)\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                    <mat-error>Please enter Upper Limit</mat-error>\r\n                  </mat-form-field>\r\n                </td>\r\n                <td>\r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <mat-select formControlName=\"percentageValue\" (selectionChange)=\"PercentValueChange(i)\">\r\n                      <mat-option value=\"\">Please Select</mat-option>\r\n                      <mat-option value=\"P\">Percent</mat-option>\r\n                      <mat-option value=\"V\">Value</mat-option>\r\n                    </mat-select>\r\n                    <mat-error>Please select % value</mat-error>\r\n                  </mat-form-field>\r\n                </td>\r\n                <td>\r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <input matInput formControlName=\"normalRebate\" placeholder=\"Normal Rebate\" autocomplete=off maxlength=\"{{maxLength}}\" (keyup)=\"validateLowerLimit(i)\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails.controls[i].controls.normalRebate.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.normalRebate.errors.required\">Please enter normal rebate</mat-error>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails.controls[i].controls.normalRebate.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.normalRebate.errors.pattern\">Please enter valid normal rebate</mat-error>\r\n                  </mat-form-field>\r\n                </td>\r\n                <td>\r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <input matInput formControlName=\"spclRebate\" name=\"spclRebate\" placeholder=\"Special Rebate\" autocomplete=off maxlength=\"{{maxLength}}\" oninput=\"this.value=Math.abs(this.value)\" pattern=\"{{percentValuePattern}}\" (keyup)=\"validateLowerLimit(i)\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails.controls[i].controls.spclRebate.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.spclRebate.errors.required\">Please enter special rebate</mat-error>\r\n                    <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails.controls[i].controls.spclRebate.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.spclRebate.errors.pattern\">Please enter valid special rebate</mat-error>\r\n                  </mat-form-field>\r\n                </td>\r\n                <td>\r\n                  <mat-form-field class=\"form-field-width\">\r\n                    <input matInput formControlName=\"maxSaving\" placeholder=\"Max saving\" autocomplete=off maxlength=\"6\" (keyup)=\"validateLowerLimit(i)\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\">\r\n                    <mat-error>Please enter Max Saving</mat-error>\r\n                  </mat-form-field>\r\n                </td>\r\n                <td>\r\n                  <a *ngIf=\"rateDetails.length > 1\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRateDetail(i)\">\r\n                    delete_forever\r\n                  </a>\r\n                </td>\r\n              </tr>\r\n            </table>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12 text-right btn-wraper\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"addRateDetail()\">Add Row</button>\r\n          </div>\r\n        </mat-card>\r\n\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" [ngClass]=\"btnCssClass\" *ngIf=\"submitButtonText\">{{submitButtonText}}</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\">Cancel</button>\r\n        </div>\r\n\r\n      </form>\r\n\r\n    </mat-expansion-panel>\r\n  </mat-accordion>\r\n</div>\r\n\r\n<div class=\"col-md-12\">\r\n  <mat-accordion class=\"mb-20\">\r\n    <mat-expansion-panel [expanded]=\"true\">\r\n      <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n        <mat-panel-title class=\"acordion-heading\">\r\n          Income Tax Details\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      \r\n      <mat-form-field>\r\n        <input matInput (keyup.enter)=\"applyFilter($event.target.value)\" [(ngModel)]=\"searchTerm\" placeholder=\"Search\">\r\n        <span (click)=\"applyFilter(searchTerm)\"><i class=\"material-icons icon-right\">search</i></span>\r\n      </mat-form-field>\r\n      <div class=\"tabel-wraper\">\r\n        <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"serialNo\">\r\n            <th mat-header-cell *matHeaderCellDef>S.No</th>\r\n            <td mat-cell *matCellDef=\"let element; let i = index;\">{{ i+1 }} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"sectionCode\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Section </th>\r\n            <td mat-cell *matCellDef=\"let element\"> <label *ngIf=\"element.subSectionCode != 'NA'\">{{element.sectionCode + ' ' + element.subSectionCode}}</label> <label *ngIf=\"element.subSectionCode == 'NA'\">{{element.sectionCode}}</label>  </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"financialYearFrom\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Financial Year From</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.financialYearFromDesc}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"financialYearTo\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Financial Year To</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.financialYearToDesc}} </td>\r\n          </ng-container>\r\n          <!--<ng-container matColumnDef=\"slabLimitFor\">-->\r\n          <ng-container matColumnDef=\"slDesc\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Slab Limit For</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.slDesc}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef class=\"actionWidth\"> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">edit</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteDetails(element);DeletePopup = !DeletePopup\">\r\n                delete_forever\r\n              </a>\r\n            </td>\r\n          </ng-container>\r\n\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n\r\n        </table>\r\n      </div>\r\n      <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"5\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n      <mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"dataSource.data.length == 0\">{{noRecordMsg}}</mat-toolbar>\r\n    </mat-expansion-panel>\r\n  </mat-accordion>\r\n    \r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete record?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = !DeletePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/income-tax/exemption-deduction/exemption-deduction.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ExemptionDeductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExemptionDeductionComponent", function() { return ExemptionDeductionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_income_tax_exemption_deduction_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/income-tax/exemption-deduction.service */ "./src/app/services/income-tax/exemption-deduction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExemptionDeductionComponent = /** @class */ (function () {
    function ExemptionDeductionComponent(formBuilder, _Service, _msg, cdRef) {
        this.formBuilder = formBuilder;
        this._Service = _Service;
        this._msg = _msg;
        this.cdRef = cdRef;
        this.displayedColumnsForForm = ['lowerLimit', 'upperLimit', 'percentageValue', 'normalRebate', 'spclRebate', 'maxSaving', 'action'];
        //displayedColumns: string[] = ['lowerLimit', 'upperLimit', 'percentageValue', 'normalRebate', 'spclRebate', 'maxSaving', 'action'];
        //displayedColumns: string[] = ['serialNo', 'sectionCode', 'financialYearFrom', 'financialYearTo', 'slDesc', 'action']; removed serial no
        this.displayedColumns = ['sectionCode', 'financialYearFrom', 'financialYearTo', 'slDesc', 'action'];
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.data = [];
        this.DeletePopup = false;
        this.allData = [];
        this.ArrddlMajSecCode = [];
        this.ArrddlSecCode = [];
        this.ArrddlSubSecCode = [];
        this.ArrddlSecCodeDesc = [];
        this.ArrddlSlabLimit = [];
        this.ArrddlPayCommission = [];
        this.ArrddlYearFrom = [];
        this.ArrddlYearTo = [];
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.userName = '';
        this.incomeTaxDetails = [];
        this.incomeTaxDetailsSearch = [];
        this.maxLength = 6;
        this.percentValuePattern = '^[0-9]{1,6}$';
        this.submitButtonText = 'Save';
        this.Iscondition = false;
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
        this.isOnInit = false;
        this.isRateDetailPanel = false;
        this.btnCssClass = 'btn btn-success';
    }
    ExemptionDeductionComponent_1 = ExemptionDeductionComponent;
    ExemptionDeductionComponent.prototype.ngOnChanges = function () {
    };
    ExemptionDeductionComponent.prototype.createForm = function () {
        this.incomeTaxRatesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            majorSectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            sectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            subSectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            msEdrId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            sectionDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            financialYearFrom: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            financialYearTo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            payCommission: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            slabLimitsFor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            rateDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)])
        });
        this.addRateDetail();
    };
    ExemptionDeductionComponent.prototype.createRateDetail = function () {
        return this.formBuilder.group({
            id2: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            exmDedRebateId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            lowerLimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            upperLimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            percentageValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            normalRebate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.percentValuePattern)]),
            spclRebate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.percentValuePattern)]),
            maxSaving: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            limitOverlapError: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false),
        }, { validators: this.LimitValidators.bind(this) });
    };
    ExemptionDeductionComponent.prototype.addRateDetail = function () {
        this.rateDetails.push(this.createRateDetail());
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
    };
    Object.defineProperty(ExemptionDeductionComponent.prototype, "rateDetails", {
        get: function () {
            return this.incomeTaxRatesForm.get("rateDetails");
        },
        enumerable: true,
        configurable: true
    });
    ExemptionDeductionComponent.prototype.deleteRateDetail = function (index) {
        if (this.rateDetails.length > 1) {
            this.rateDetails.removeAt(index);
            this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
        }
    };
    ExemptionDeductionComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.incomeTaxRatesForm.valid) {
            this.incomeTaxRatesForm.controls.userName.setValue(this.userName);
            this._Service.UpsertExemptionDeductionDetails(this.incomeTaxRatesForm.value).subscribe(function (response) {
                if (response != undefined || response != null) {
                    _this.getExempDedDetails(_this.pageSize, _this.pageNumber);
                    _this.incomeTaxRatesForm.reset();
                    //this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
                    //swal(response);
                    _this.formGroupDirective.resetForm();
                    _this.createForm();
                    _this.submitButtonText = 'Save';
                    _this.isSuccessStatus = true;
                    _this.responseMessage = response;
                }
                else {
                    _this.isSuccessStatus = false;
                    _this.isWarningStatus = true;
                    _this.responseMessage = _this._msg.saveFailedMsg;
                }
                _this.bgColor = '';
                _this.btnCssClass = 'btn btn-success';
                setTimeout(function () {
                    _this.isSuccessStatus = false;
                    _this.isWarningStatus = false;
                    _this.responseMessage = '';
                }, _this._msg.messageTimer);
            });
        }
    };
    ExemptionDeductionComponent.prototype.cancelForm = function () {
        this.incomeTaxRatesForm.reset();
        this.formGroupDirective.resetForm();
        this.createForm();
        this.submitButtonText = 'Save';
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
    };
    ExemptionDeductionComponent.prototype.applyFilterPrevious = function (value) {
        var data = this.dataSource.data;
        //if (value && data.length > 0) {
        if (value) {
            value = value.toLowerCase();
            //let data = this.dataSource.data as any[];
            var filteredData = data.filter(function (a) { return a.financialYearFrom.toLowerCase().includes(value) || a.financialYearTo.toLowerCase().includes(value) || a.majorSectionCode.toLowerCase().includes(value) || a.sectionCode.toLowerCase().includes(value) || a.slDesc.toLowerCase().includes(value); });
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](filteredData);
        }
        else {
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.incomeTaxDetails);
        }
        this.totalCount = this.dataSource.data.length;
    };
    ExemptionDeductionComponent.prototype.applyFilter = function (value) {
        var _this = this;
        if (value) {
            this.totalCount = 0;
            this._Service.GetExemptionDeductionDetails(this.pageNumber, this.pageSize, this.searchTerm).subscribe(function (filteredData) {
                if (filteredData && filteredData != undefined && filteredData.length > 0) {
                    _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](filteredData);
                    _this.dataSource.sort = _this.sort;
                    _this.allData = filteredData;
                    _this.totalCount = filteredData[0].totalCount;
                }
                else {
                    _this.getExempDedDetails(_this.pageSize, 1);
                }
            });
        }
        else {
            //this.dataSource = new MatTableDataSource(this.incomeTaxDetailsSearch);
            this.getExempDedDetails(this.pageSize, 1);
        }
        this.totalCount = this.dataSource.data.length;
    };
    ExemptionDeductionComponent.prototype.getPaginationData = function (event) {
        this.pageSize = event.pageSize;
        this.pageNumber = event.pageIndex + 1;
        this.getExempDedDetails(this.pageSize, this.pageNumber);
    };
    ExemptionDeductionComponent.prototype.getExempDedDetails = function (pageSize, pageNumber) {
        var _this = this;
        this.totalCount = 0;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this._Service.GetExemptionDeductionDetails(pageNumber, pageSize, this.searchTerm).subscribe(function (result) {
            if (result && result != undefined && result.length > 0) {
                _this.incomeTaxDetails = result;
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
                _this.dataSource.sort = _this.sort;
                _this.allData = result;
                _this.totalCount = result[0].totalCount;
            }
        });
    };
    ExemptionDeductionComponent.prototype.btnEditClick = function (obj) {
        var _this = this;
        while (this.rateDetails.length != 0) {
            this.rateDetails.removeAt(0);
        }
        this.incomeTaxRatesForm.patchValue(obj);
        this.rateDetails.removeAt(0);
        obj.rateDetails.forEach(function (rate) { return _this.rateDetails.push(_this.formBuilder.group(rate)); });
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
        this.BindSectionCode(obj.majorSectionCode);
        this.BindSubSectionCodeOnEdit(obj.majorSectionCode, obj.sectionCode);
        this.submitButtonText = 'Update';
        this.isRateDetailPanel = true;
        this.bgColor = 'bgcolor';
        this.btnCssClass = 'btn btn-info';
    };
    ExemptionDeductionComponent.prototype.deleteDetails = function (element) {
        this.elementToBeDeleted = element;
    };
    ExemptionDeductionComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._Service.DeleteExemptionDeductionDetails(this.elementToBeDeleted.drrID, this.elementToBeDeleted.drrID2).subscribe(function (response) {
            if ((response != undefined || response != null) && response > 0) {
                var data = _this.dataSource.data;
                var index = data.indexOf(_this.elementToBeDeleted);
                data.splice(index, 1);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                _this.elementToBeDeleted = null;
                _this.totalCount = _this.totalCount - 1;
                //this.getExempDedDetails(10, 1);
                _this.createForm();
                //this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
                //swal(this._msg.deleteMsg);
                //$(".dialog__close-btn").click();
                _this.isSuccessStatus = false;
                _this.isWarningStatus = true;
                _this.responseMessage = _this._msg.deleteMsg;
            }
            else {
                //swal(this._msg.deleteFailedMsg);
                _this.isSuccessStatus = false;
                _this.isWarningStatus = true;
                _this.responseMessage = _this._msg.deleteFailedMsg;
            }
            _this.DeletePopup = false;
            setTimeout(function () {
                _this.isSuccessStatus = false;
                _this.isWarningStatus = false;
                _this.responseMessage = '';
            }, _this._msg.messageTimer);
        });
    };
    ExemptionDeductionComponent.prototype.ngOnInit = function () {
        this.isOnInit = true;
        this.createForm();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.data);
        this.totalCount = this.data.length;
        this.BindMajorSectionCode();
        this.BindSlabLimit(7);
        //this.BindFromFinancialYear();
        //this.BindToFinancialYear();
        this.GetFinancialYear();
        this.userName = sessionStorage.getItem('username');
        this.getExempDedDetails(this.pageSize, this.pageNumber);
        this.noRecordMsg = this._msg.noRecordMsg;
        this.submitButtonText = 'Save';
    };
    ExemptionDeductionComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.sort = this.sort;
    };
    ExemptionDeductionComponent.prototype.BindMajorSectionCode = function () {
        var _this = this;
        this._Service.GetMajorSectionCode().subscribe(function (data) {
            _this.ArrddlMajSecCode = data;
        });
    };
    ExemptionDeductionComponent.prototype.BindSectionCode = function (sectionCode) {
        var _this = this;
        this._Service.GetSectionCode(sectionCode).subscribe(function (data) {
            _this.ArrddlSecCode = data;
        });
    };
    ExemptionDeductionComponent.prototype.BindSubSectionCode = function () {
        var _this = this;
        var selectedMajSecCode = this.incomeTaxRatesForm.controls.majorSectionCode.value;
        var selectedSecCode = this.incomeTaxRatesForm.controls.sectionCode.value;
        this.ArrddlSubSecCode = [];
        this._Service.GetSubSectionCode(selectedMajSecCode, selectedSecCode).subscribe(function (data) {
            _this.ArrddlSubSecCode = data;
            if (_this.ArrddlSubSecCode.length == 1) {
                _this.incomeTaxRatesForm.controls.subSectionCode.setValue(_this.ArrddlSubSecCode[0].subSecCode);
                _this.BindSectionCodeDescription();
            }
            else {
                _this.incomeTaxRatesForm.controls.subSectionCode.setValue('');
                _this.incomeTaxRatesForm.controls.sectionDesc.setValue('');
            }
        });
    };
    ExemptionDeductionComponent.prototype.BindSubSectionCodeOnEdit = function (majSecCode, secCode) {
        var _this = this;
        this._Service.GetSubSectionCode(majSecCode, secCode).subscribe(function (data) {
            _this.ArrddlSubSecCode = data;
        });
    };
    ExemptionDeductionComponent.prototype.BindSectionCodeDescription = function () {
        var _this = this;
        var selectedMajSecCode = this.incomeTaxRatesForm.controls.majorSectionCode.value;
        var selectedSecCode = this.incomeTaxRatesForm.controls.sectionCode.value;
        var selectedSubSecCode = this.incomeTaxRatesForm.controls.subSectionCode.value;
        this._Service.GetSectionCodeDescription(selectedMajSecCode, selectedSecCode, selectedSubSecCode).subscribe(function (data) {
            _this.ArrddlSecCodeDesc = data;
            _this.incomeTaxRatesForm.controls.sectionDesc.setValue(_this.ArrddlSecCodeDesc[0].secDesc);
            _this.incomeTaxRatesForm.controls.msEdrId.setValue(_this.ArrddlSecCodeDesc[0].secId);
        });
    };
    ExemptionDeductionComponent.prototype.BindSlabLimit = function (payCommission) {
        var _this = this;
        this._Service.GetSlabLimit(payCommission).subscribe(function (data) {
            _this.ArrddlSlabLimit = data;
        });
    };
    ExemptionDeductionComponent.prototype.BindPayCommission = function () {
        var _this = this;
        this._Service.GetPayCommission().subscribe(function (data) {
            _this.ArrddlPayCommission = data;
        });
    };
    ExemptionDeductionComponent.prototype.GetFinancialYear = function () {
        var _this = this;
        this._Service.GetFinancialYear().subscribe(function (data) {
            _this.ArrddlYearFrom = data;
            _this.ArrddlYearTo = data;
        });
    };
    ExemptionDeductionComponent.prototype.LimitValidators = function (frm) {
        var lLimit = frm.controls.lowerLimit.value;
        var uLimit = frm.controls.upperLimit.value;
        if (+lLimit > +uLimit) {
            frm.controls.lowerLimit.setErrors({ 'invalidError': true });
            return { 'invalidError': true };
        }
        else {
            frm.controls.lowerLimit.setErrors(null);
            if (!lLimit) {
                frm.controls.lowerLimit.setErrors({ 'required': true });
            }
            if (!this.isOnInit) {
                if (this.Iscondition) {
                    frm.controls.lowerLimit.setErrors({ 'limitOverlapError': true });
                    return { 'limitOverlapError': true };
                }
                else {
                    frm.controls.lowerLimit.setErrors(null);
                }
            }
            return null;
        }
    };
    ExemptionDeductionComponent.prototype.PercentValueChange = function (index) {
        var percentValue = this.rateDetails.value[index].percentageValue;
        if (percentValue == 'P') {
            this.maxLength = 3;
            this.percentValuePattern = '^(?!0)(100|[1-9]?[0-9])(\.\d{1,2})?$';
        }
        else {
            this.maxLength = 6;
            this.percentValuePattern = '^[0-9]{1,6}$';
        }
        this.rateDetails.controls[index].get("spclRebate").setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.percentValuePattern)]);
        this.rateDetails.controls[index].get("normalRebate").setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.percentValuePattern)]);
        this.validateLowerLimit(index);
        //alert(this.rateDetails.value[index].limitOverlapError);
    };
    ExemptionDeductionComponent.prototype.validateLowerLimit = function (index) {
        if (+index > 0) {
            var lLimit = this.rateDetails.value[index].lowerLimit;
            var preLimit = this.rateDetails.value[index - 1].upperLimit;
            if (+lLimit <= +preLimit) {
                this.rateDetails.controls[index].get("lowerLimit").setErrors({ 'limitOverlapError': true });
                this.rateDetails.controls[index].value.limitOverlapError = true;
                this.Iscondition = true;
            }
            else {
                this.Iscondition = false;
            }
        }
    };
    ExemptionDeductionComponent.prototype.resetRateDetailPanel = function () {
        this.isRateDetailPanel = false;
        this.bgColor = '';
    };
    var ExemptionDeductionComponent_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ExemptionDeductionComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], ExemptionDeductionComponent.prototype, "sort", void 0);
    ExemptionDeductionComponent = ExemptionDeductionComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-exemption-deduction',
            template: __webpack_require__(/*! ./exemption-deduction.component.html */ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.html"),
            styles: [__webpack_require__(/*! ./exemption-deduction.component.css */ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.css")],
            providers: [{
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ExemptionDeductionComponent_1; }),
                    multi: true,
                }]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_income_tax_exemption_deduction_service__WEBPACK_IMPORTED_MODULE_4__["ExemptionDeductionService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_3__["CommonMsg"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], ExemptionDeductionComponent);
    return ExemptionDeductionComponent;
}());



/***/ }),

/***/ "./src/app/income-tax/income-tax-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/income-tax/income-tax-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: IncomeTaxRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncomeTaxRoutingModule", function() { return IncomeTaxRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _income_tax_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./income-tax.module */ "./src/app/income-tax/income-tax.module.ts");
/* harmony import */ var _exemption_deduction_exemption_deduction_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./exemption-deduction/exemption-deduction.component */ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.ts");
/* harmony import */ var _standard_deduction_standard_deduction_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./standard-deduction/standard-deduction.component */ "./src/app/income-tax/standard-deduction/standard-deduction.component.ts");
/* harmony import */ var _it_rates_it_rates_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./it-rates/it-rates.component */ "./src/app/income-tax/it-rates/it-rates.component.ts");
/* harmony import */ var _other_dues_other_dues_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./other-dues/other-dues.component */ "./src/app/income-tax/other-dues/other-dues.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _income_tax_module__WEBPACK_IMPORTED_MODULE_2__["IncomeTaxModule"], children: [
            { path: 'exemptiondeduction', component: _exemption_deduction_exemption_deduction_component__WEBPACK_IMPORTED_MODULE_3__["ExemptionDeductionComponent"] },
            { path: 'standarddeduction', component: _standard_deduction_standard_deduction_component__WEBPACK_IMPORTED_MODULE_4__["StandardDeductionComponent"], data: { breadcrumb: 'Standard Deduction' } },
            { path: 'itrates', component: _it_rates_it_rates_component__WEBPACK_IMPORTED_MODULE_5__["ItRatesComponent"], data: { breadcrumb: 'IT Rates' } },
            { path: 'otherdues', component: _other_dues_other_dues_component__WEBPACK_IMPORTED_MODULE_6__["OtherDuesComponent"] }
        ]
    }
];
var IncomeTaxRoutingModule = /** @class */ (function () {
    function IncomeTaxRoutingModule() {
    }
    IncomeTaxRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], IncomeTaxRoutingModule);
    return IncomeTaxRoutingModule;
}());



/***/ }),

/***/ "./src/app/income-tax/income-tax.module.ts":
/*!*************************************************!*\
  !*** ./src/app/income-tax/income-tax.module.ts ***!
  \*************************************************/
/*! exports provided: IncomeTaxModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncomeTaxModule", function() { return IncomeTaxModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _income_tax_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./income-tax-routing.module */ "./src/app/income-tax/income-tax-routing.module.ts");
/* harmony import */ var _exemption_deduction_exemption_deduction_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./exemption-deduction/exemption-deduction.component */ "./src/app/income-tax/exemption-deduction/exemption-deduction.component.ts");
/* harmony import */ var _standard_deduction_standard_deduction_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./standard-deduction/standard-deduction.component */ "./src/app/income-tax/standard-deduction/standard-deduction.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _services_income_tax_exemption_deduction_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/income-tax/exemption-deduction.service */ "./src/app/services/income-tax/exemption-deduction.service.ts");
/* harmony import */ var _services_income_tax_standard_deduction_services__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/income-tax/standard-deduction.services */ "./src/app/services/income-tax/standard-deduction.services.ts");
/* harmony import */ var _it_rates_it_rates_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./it-rates/it-rates.component */ "./src/app/income-tax/it-rates/it-rates.component.ts");
/* harmony import */ var _other_dues_other_dues_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./other-dues/other-dues.component */ "./src/app/income-tax/other-dues/other-dues.component.ts");
/* harmony import */ var _services_income_tax_it_rates_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../services/income-tax/it-rates.service */ "./src/app/services/income-tax/it-rates.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var IncomeTaxModule = /** @class */ (function () {
    function IncomeTaxModule() {
    }
    IncomeTaxModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_exemption_deduction_exemption_deduction_component__WEBPACK_IMPORTED_MODULE_3__["ExemptionDeductionComponent"], _standard_deduction_standard_deduction_component__WEBPACK_IMPORTED_MODULE_4__["StandardDeductionComponent"], _it_rates_it_rates_component__WEBPACK_IMPORTED_MODULE_11__["ItRatesComponent"], _other_dues_other_dues_component__WEBPACK_IMPORTED_MODULE_12__["OtherDuesComponent"]],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatTooltipModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _income_tax_routing_module__WEBPACK_IMPORTED_MODULE_2__["IncomeTaxRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__["NgxMatSelectSearchModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]
            ],
            providers: [_services_income_tax_exemption_deduction_service__WEBPACK_IMPORTED_MODULE_9__["ExemptionDeductionService"], _services_income_tax_standard_deduction_services__WEBPACK_IMPORTED_MODULE_10__["StandardDeductionService"], _services_income_tax_it_rates_service__WEBPACK_IMPORTED_MODULE_13__["ITRatesService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_14__["CommonMsg"]]
        })
    ], IncomeTaxModule);
    return IncomeTaxModule;
}());



/***/ }),

/***/ "./src/app/income-tax/it-rates/it-rates.component.css":
/*!************************************************************!*\
  !*** ./src/app/income-tax/it-rates/it-rates.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*mat-table {\r\n  border-collapse: collapse;\r\n}\r\n\r\nmat-row {\r\n  border-right: 1px solid #000000;\r\n}\r\n\r\nmat-cell {\r\n  border-right: 1px solid #000000;\r\n}\r\n\r\nmat-header-cell {\r\n  border-right: 1px solid #000000;\r\n  color: white;\r\n  justify-content: center;\r\n}\r\n\r\nmat-header-row {\r\n  background-color: #3268ab;\r\n  color: white;\r\n}\r\n\r\n.customWidthClassForm {\r\n  flex: 0 0 30px;\r\n}\r\n\r\n.customWidthClass {\r\n  flex: 0 0 70px;\r\n}\r\n\r\n.alignTextCentre {\r\n  display: flex;\r\n  justify-content: center\r\n}*/\r\n\r\n.form-field-width{\r\n  width:150px;\r\n}\r\n\r\n.record-not-found {\r\n  text-align: center;\r\n  display: inline-block\r\n}\r\n\r\n.customWidthClass {\r\n  flex: 0 0 75px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jb21lLXRheC9pdC1yYXRlcy9pdC1yYXRlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBa0NHOztBQUVIO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLGVBQWU7Q0FDaEIiLCJmaWxlIjoic3JjL2FwcC9pbmNvbWUtdGF4L2l0LXJhdGVzL2l0LXJhdGVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKm1hdC10YWJsZSB7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxufVxyXG5cclxubWF0LXJvdyB7XHJcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzAwMDAwMDtcclxufVxyXG5cclxubWF0LWNlbGwge1xyXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICMwMDAwMDA7XHJcbn1cclxuXHJcbm1hdC1oZWFkZXItY2VsbCB7XHJcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzAwMDAwMDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzI2OGFiO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmN1c3RvbVdpZHRoQ2xhc3NGb3JtIHtcclxuICBmbGV4OiAwIDAgMzBweDtcclxufVxyXG5cclxuLmN1c3RvbVdpZHRoQ2xhc3Mge1xyXG4gIGZsZXg6IDAgMCA3MHB4O1xyXG59XHJcblxyXG4uYWxpZ25UZXh0Q2VudHJlIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyXHJcbn0qL1xyXG5cclxuLmZvcm0tZmllbGQtd2lkdGh7XHJcbiAgd2lkdGg6MTUwcHg7XHJcbn1cclxuXHJcbi5yZWNvcmQtbm90LWZvdW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbn1cclxuXHJcbi5jdXN0b21XaWR0aENsYXNzIHtcclxuICBmbGV4OiAwIDAgNzVweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/income-tax/it-rates/it-rates.component.html":
/*!*************************************************************!*\
  !*** ./src/app/income-tax/it-rates/it-rates.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccessStatus\" (click)=\"isSuccessStatus=false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>{{  responseMessage }}</strong>\r\n  </div>\r\n  <!--<div class=\"alert alert-info alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>-->\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"isItRatePanel\"  [ngClass]=\"bgColor\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\" (click)=\"isItRatePanel=false\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Income Tax Rates\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <form [formGroup]=\"itRatesForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n      <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"isrFinYr\" placeholder=\"Financial Year\" required>\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let fin of financialYears\" value=\"{{fin.financialYear}}\">{{fin.fullDescription}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Please select Financial Year!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"isrRateType\" placeholder=\"Rate Type\" required>\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let rt of rateType\" value=\"{{rt.codeValue.trim()}}\">{{rt.codeValue+'-'+rt.codeType}}</mat-option>\r\n            <!--<mat-option value=\"I\">I - Income Tax Rates</mat-option>\r\n            <mat-option value=\"S\">S - Surcharge Rates</mat-option>-->\r\n          </mat-select>\r\n          <mat-error>Please select Rate Type!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"isrRateFor\" placeholder=\"Rate For\" required>\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let rt of rateFor\" value=\"{{rt.codeValue.trim()}}\">{{rt.codeValue+'-Rates for '+rt.codeType}}</mat-option>\r\n            <!--<mat-option value=\"\">Select</mat-option>\r\n            <mat-option value=\"M\">M - Rates for Male</mat-option>\r\n            <mat-option value=\"F\">F- Rates for Female</mat-option>\r\n            <mat-option value=\"C\">C - Rates for Common</mat-option>\r\n            <mat-option value=\"S\">S - Rates for Senior Citizens</mat-option>-->\r\n          </mat-select>\r\n          <mat-error>Please select Rates for!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <mat-card class=\" margin-top-10px\" [ngClass]=\"bgColor\">\r\n        <div class=\"fom-title\">Rate Details</div>\r\n\r\n        <table class=\"col-md-12 col-lg-12 table2 even-odd-color \" formArrayName=\"rateDetails\">\r\n          <tr>\r\n            <th>Lower Limit</th>\r\n            <th> Upper Limit </th>\r\n            <th>Added Amount</th>\r\n            <th>Percentage Value %</th>\r\n            <th>Action</th>\r\n          </tr>\r\n          <tbody>\r\n            <tr *ngFor=\"let rate of itRatesForm.controls.rateDetails.controls; let i = index;\" [formGroupName]=\"i\">\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput formControlName=\"isrLlimit\" required class=\"mat-input-element\" maxlength=\"6\" (keyup)=\"validateLowerLimit(i)\" placeholder=\"Lower Limit Value\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\">\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrLlimit.errors && rate.controls.isrLlimit.errors.required == true\">Please enter Lower Limit!</mat-error>\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrLlimit.errors && rate.controls.isrLlimit.errors.collapse == true\">Lower limit and upper limit value should not be overlapped!</mat-error>\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrLlimit.errors && rate.controls.isrLlimit.errors.invalidError == true\">Should be less than Upper Limit!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput formControlName=\"isrUlimit\" required class=\"mat-input-element\" maxlength=\"6\" (keyup)=\"validateUpperLimit(i)\" placeholder=\"Upper Limit Value\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\">\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrUlimit.errors && rate.controls.isrUlimit.errors.required == true\">Please enter Upper Limit!</mat-error>\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrUlimit.errors && rate.controls.isrUlimit.errors.invalidError == true\">Should be greater than lower Limit!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput formControlName=\"isrAddedAmt\" required class=\"mat-input-element\" maxlength=\"6\" placeholder=\"Added Amount\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\">\r\n                  <mat-error>Please enter Added Amount!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput formControlName=\"isrPercVal\" required class=\"mat-input-element\" maxlength=\"3\" pattern=\"^(?!0)(100|[1-9]?[0-9])(\\.\\d{1,2})?$\" placeholder=\"Percentage Value %\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\">\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrPercVal.errors && rate.controls.isrPercVal.errors.required\">Please enter Percentage Value %!</mat-error>\r\n                  <mat-error *ngIf=\"itRatesForm.controls.rateDetails && rate.controls.isrPercVal.errors && rate.controls.isrPercVal.errors.pattern\">Please enter valid Percentage Value %!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <a *ngIf=\"rateDetails.length > 1\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRateDetail(i, element)\">\r\n                  delete_forever\r\n                </a>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n        <div class=\"col-md-12 col-lg-12 text-right btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" [disabled]=\"isDisableAddRow\" (click)=\"addRateDetail()\">Add Row</button>\r\n        </div>\r\n      </mat-card>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" [ngClass]=\"btnCssClass\">{{lblSubmitStatus}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm(-1)\">Cancel</button>\r\n      </div>\r\n    </form>\r\n    <div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n      <img style=\"height:70px;width:70px\" src=\"~/../assets/Loder12.gif\">\r\n    </div>\r\n\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<!--<mat-card>-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"isItRateDetailPanel\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Income Tax Rate Detail\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n    <form [formGroup]=\"filterRatesForm\">\r\n      <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"finYear\" placeholder=\"Financial Year\">\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let fin of financialYears\" value=\"{{fin.financialYear}}\">{{fin.fullDescription}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"rateType\" placeholder=\"Rate Type\">\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let rt of rateType\" value=\"{{rt.codeValue}}\">{{rt.codeValue+'-'+rt.codeType}}</mat-option>\r\n            <!--<mat-option value=\"I\">I - Income Tax Rates</mat-option>\r\n          <mat-option value=\"S\">S - Surcharge Rates</mat-option>-->\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"rateFor\" placeholder=\"Rates For\">\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let rt of rateFor\" value=\"{{rt.codeValue}}\">{{rt.codeValue+'-Rates for '+rt.codeType}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"search(-1)\">Search</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm(1)\">Cancel</button>\r\n      </div>\r\n    </form>\r\n    <br /><br />\r\n          <div class=\"tabel-wraper  col-sm-12\">\r\n            <mat-table [dataSource]=\"dataSource\" matSort (matSortChange)=\"sortData($event)\" class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"dataSource.data.length > 0\">\r\n\r\n              <ng-container matColumnDef=\"finYear\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Financial Year </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.fullDescription}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"rateType\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Rate Type </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.isrRateType}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"rateFor\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Rate For </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.isrRateFor}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"lowerLimit\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Lower Limit </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.isrLlimit}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"upperLimit\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header> Upper Limit </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> {{element.isrUlimit}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"addedAmount\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header>Added Amount</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> {{element.isrAddedAmt}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <ng-container matColumnDef=\"percentageValue\">\r\n                <mat-header-cell *matHeaderCellDef mat-sort-header>Percentage Value</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> {{element.isrPercVal}} </mat-cell>\r\n              </ng-container>\r\n\r\n\r\n              <ng-container matColumnDef=\"action\">\r\n                <mat-header-cell *matHeaderCellDef [ngClass]=\"'customWidthClass'\"> Action</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\" [ngClass]=\"'customWidthClass'\">\r\n                  <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">edit</a>\r\n                  <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteDetails(element\r\n             );DeletePopup = !DeletePopup\">\r\n                    delete_forever\r\n                  </a>\r\n                </mat-cell>\r\n              </ng-container>\r\n\r\n              <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n              <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n            </mat-table>\r\n            <mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" #paginator [length]=\"totalCount\" [pageSize]=\"pageSize\" [pageIndex]=\"pageNumber\" [pageSizeOptions]=\"[5, 10, 20, 50]\" showFirstLastButtons></mat-paginator>\r\n            <mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"dataSource.data.length == 0\">Record is not found</mat-toolbar>\r\n          </div>\r\n\r\n  \r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img style=\"height:70px;width:70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = false;\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"IsDMLStatus\">\r\n  <h3 class=\"card-title text-center\">Your Record Status</h3>\r\n  <mat-card>\r\n    <mat-table [dataSource]=\"DMLStatusList\" class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"DMLStatusList.data.length > 0\">\r\n\r\n      <ng-container matColumnDef=\"finYear\">\r\n        <mat-header-cell *matHeaderCellDef> Financial Year </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">{{element.isrFinYr}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"rateType\">\r\n        <mat-header-cell *matHeaderCellDef> Rate Type </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">{{element.isrRateType}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"rateFor\">\r\n        <mat-header-cell *matHeaderCellDef> Rate For </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">{{element.isrRateFor}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"lowerLimit\">\r\n        <mat-header-cell *matHeaderCellDef>Lower Limit </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\">{{element.isrLlimit}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"upperLimit\">\r\n        <mat-header-cell *matHeaderCellDef>Upper Limit</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.isrUlimit}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"addedAmount\">\r\n        <mat-header-cell *matHeaderCellDef>Amount</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.isrAddedAmt}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"percentageValue\">\r\n        <mat-header-cell *matHeaderCellDef>% Value</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.isrPercVal}} </mat-cell>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"message\">\r\n        <mat-header-cell *matHeaderCellDef>Status</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.message}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <mat-header-row *matHeaderRowDef=\"dspColumnStatusHeader\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: dspColumnStatusHeader;\"></mat-row>\r\n    </mat-table>\r\n    <br />\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"IsDMLStatus=false\">OK</button>\r\n    </div>\r\n  </mat-card>\r\n\r\n</app-dialog>\r\n\r\n\r\n<!--</mat-card>-->\r\n"

/***/ }),

/***/ "./src/app/income-tax/it-rates/it-rates.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/income-tax/it-rates/it-rates.component.ts ***!
  \***********************************************************/
/*! exports provided: ItRatesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItRatesComponent", function() { return ItRatesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_income_tax_it_rates_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/income-tax/it-rates.service */ "./src/app/services/income-tax/it-rates.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ItRatesComponent = /** @class */ (function () {
    function ItRatesComponent(formBuilder, _service, _msg) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._msg = _msg;
        this.is_btnStatus = true;
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.DMLStatusList = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.displayedColumnsForForm = ['lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'action'];
        this.displayedColumns = ['finYear', 'rateType', 'rateFor', 'lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'action'];
        this.dspColumnStatusHeader = ['finYear', 'rateType', 'rateFor', 'lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'message'];
        this.financialYears = [];
        this.pageSize = 5;
        this.pageNumber = 0;
        this.totalCount = 0;
        this.DeletePopup = false;
        this.lblSubmitStatus = 'Save';
        this.isLoading = false;
        this.rateType = [];
        this.rateFor = [];
        this.isDisableAddRow = false;
        this.IsDMLStatus = false;
        this.isSuccessStatus = false;
        this.isItRatePanel = false;
        this.isItRateDetailPanel = false;
        this.responseMessage = '';
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
        this.ddoId = sessionStorage.getItem('ddoid');
        this.createForm();
        this.createFilterForm();
    }
    ItRatesComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    ItRatesComponent.prototype.createForm = function () {
        this.itRatesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            isrFinYr: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrRateType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrRateFor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            rateDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)])
        });
        this.addRateDetail();
    };
    ItRatesComponent.prototype.createFilterForm = function () {
        this.filterRatesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            finYear: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            rateType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            rateFor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
        });
    };
    ItRatesComponent.prototype.createRateDetail = function () {
        return new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rateDetails.value.length + 1),
            itSurcharge_RateID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            isrLlimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrUlimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrAddedAmt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrPercVal: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^0*(?:[1-9][0-9]?|100)$")]),
        });
    };
    ItRatesComponent.prototype.addRateDetail = function () {
        this.rateDetails.push(this.createRateDetail());
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
    };
    Object.defineProperty(ItRatesComponent.prototype, "rateDetails", {
        get: function () {
            return this.itRatesForm.get("rateDetails");
        },
        enumerable: true,
        configurable: true
    });
    ItRatesComponent.prototype.deleteRateDetail = function (index, obj) {
        if (this.rateDetails.length > 1) {
            this.rateDetails.removeAt(index);
            this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
        }
    };
    ItRatesComponent.prototype.onSubmit = function () {
        var _this = this;
        this.isLoading = true;
        if (this.itRatesForm.valid) {
            this.filterRatesForm.reset();
            var status_1 = this.itRatesForm.value.rateDetails[0].itSurcharge_RateID > 0 ? true : false;
            this._service.upsertItRates(this.itRatesForm.value).subscribe(function (response) {
                _this.IsDMLStatus = true;
                if (response && response.length > 0) {
                    _this.formGroupDirective.resetForm();
                    _this.createForm();
                    _this.createFilterForm();
                    _this.search(-1);
                    _this.lblSubmitStatus = 'Save';
                    _this.DMLStatusList = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](response);
                    _this.isItRatePanel = false;
                    _this.bgColor = '';
                }
            }, function (error) {
                _this.isSuccessStatus = true;
                _this.responseMessage = _this._msg.apiErrorMsg;
                _this.isLoading = false;
            });
        }
        else {
            this.isLoading = false;
        }
    };
    ItRatesComponent.prototype.DeleteDetails = function (element) {
        this.elementToBeDeleted = element;
    };
    ItRatesComponent.prototype.confirmDelete = function () {
        var _this = this;
        this.isSuccessStatus = true;
        this._service.deleteItRates(this.elementToBeDeleted.itSurcharge_RateID, this.ddoId).subscribe(function (response) {
            _this.elementToBeDeleted = null;
            _this.responseMessage = _this._msg.deleteMsg;
            _this.createForm();
            _this.DeletePopup = false;
            _this.createFilterForm();
            _this.search(-1);
            setTimeout(function () { return _this.isSuccessStatus = false; }, _this._msg.messageTimer);
        });
    };
    ItRatesComponent.prototype.btnEditClick = function (element) {
        this.isDisableAddRow = true;
        this.isItRatePanel = true;
        this.rateDetails.removeAt(0);
        this.lblSubmitStatus = 'Update';
        this.btnCssClass = 'btn btn-info';
        this.bgColor = 'bgcolor';
        this.itRatesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            isrFinYr: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrFinYr.trim(), [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrRateType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrRateType.trim(), [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrRateFor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrRateFor.trim(), [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            rateDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)])
        });
        this.rateDetails.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rateDetails.value.length + 1),
            itSurcharge_RateID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.itSurcharge_RateID),
            isrLlimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrLlimit, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,]),
            isrUlimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrUlimit, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrAddedAmt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrAddedAmt, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            isrPercVal: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](element.isrPercVal, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
        }));
    };
    ItRatesComponent.prototype.search = function (e) {
        var _this = this;
        this.isLoading = true;
        this.totalCount = 0;
        var ffm = this.filterRatesForm.value;
        this._service.getItRates(ffm.finYear == null ? '' : ffm.finYear, ffm.rateType == null ? '' : ffm.rateType, ffm.rateFor == null ? '' : ffm.rateFor).subscribe(function (response) {
            _this.isLoading = false;
            if (response.length > 0) {
                _this.totalCount = response[0].totalCount;
                _this.isItRateDetailPanel = true;
            }
            else {
                _this.isItRatePanel = e == 0 ? true : false;
            }
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](response);
            _this.dataSource.paginator = _this.paginator;
            _this.cancelForm(0);
        });
    };
    ItRatesComponent.prototype.searchForReset = function () {
        var _this = this;
        this.isLoading = true;
        this.totalCount = 0;
        var ffm = this.filterRatesForm.value;
        this._service.getItRates(ffm.finYear == null ? '' : ffm.finYear, ffm.rateType == null ? '' : ffm.rateType, ffm.rateFor == null ? '' : ffm.rateFor).subscribe(function (response) {
            _this.isLoading = false;
            if (response.length > 0) {
                _this.totalCount = response[0].totalCount;
            }
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](response);
            _this.dataSource.paginator = _this.paginator;
        });
    };
    ItRatesComponent.prototype.cancelForm = function (i) {
        this.lblSubmitStatus = 'Save';
        this.isDisableAddRow = false;
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
        if (i == 1) {
            this.filterRatesForm.reset();
            this.createFilterForm();
            this.searchForReset();
        }
        else if (i == -1) {
            this.filterRatesForm.reset();
            this.createFilterForm();
            this.searchForReset();
            this.itRatesForm.reset();
            this.formGroupDirective.resetForm();
            this.createForm();
        }
        else {
            this.itRatesForm.reset();
            this.formGroupDirective.resetForm();
            this.createForm();
        }
    };
    ItRatesComponent.prototype.validateUpperLimit = function (i) {
        var lLmt = this.itRatesForm.controls.rateDetails.value[i].isrLlimit;
        var uLmt = this.itRatesForm.controls.rateDetails.value[i].isrUlimit;
        if (this.checkUpperLimitExists(lLmt, uLmt)) {
            this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': true });
            return { 'collapse': true };
        }
        else {
            if (lLmt == '' || uLmt == '') {
                return null;
            }
            else {
                if (+lLmt > +uLmt) {
                    this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'invalidError': true });
                    this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
                    return { 'invalidError': true };
                }
                else {
                    this.rateDetails.controls[i].get("isrUlimit").setErrors(null);
                    this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
                    return null;
                }
            }
        }
    };
    ItRatesComponent.prototype.validateLowerLimit = function (i) {
        var lLmt = this.itRatesForm.controls.rateDetails.value[i].isrLlimit;
        var uLmt = this.itRatesForm.controls.rateDetails.value[i].isrUlimit;
        if (this.checkUpperLimitExists(lLmt, uLmt)) {
            this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': true });
            return { 'collapse': true };
        }
        else {
            this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': false });
            if (lLmt == '' || uLmt == '') {
                lLmt == '' ? this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'required': true }) : null;
                uLmt == '' ? this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'required': true }) : null;
                return null;
            }
            else {
                if (+lLmt > +uLmt) {
                    this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'invalidError': true });
                    this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'invalidError': false });
                    return { 'invalidError': true };
                }
                else {
                    this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
                    this.rateDetails.controls[i].get("isrUlimit").setErrors(null);
                    return null;
                }
            }
        }
    };
    ItRatesComponent.prototype.checkUpperLimitExists = function (lowerLimt, ulimit) {
        if (this.itRatesForm.controls.rateDetails.value != undefined && this.itRatesForm.controls.rateDetails.value.length > 0) {
            if (this.itRatesForm.controls.rateDetails.value.length == 1) {
                return false;
            }
            else {
                var status_2 = this.itRatesForm.controls.rateDetails.value.filter(function (p) { return (parseInt(p.isrLlimit) <= parseInt(lowerLimt) && parseInt(p.isrUlimit) >= lowerLimt) || (parseInt(p.isrLlimit) <= parseInt(ulimit) && parseInt(p.isrUlimit) >= ulimit); });
                if (status_2 == undefined || status_2 == null || status_2 == '') {
                    return false;
                }
                else {
                    if (status_2.length == 1 && parseInt(status_2[0].isrLlimit) == lowerLimt && parseInt(status_2[0].isrUlimit) == ulimit) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
        }
    };
    ItRatesComponent.prototype.sortData = function (sort) {
        var _this = this;
        var data = this.dataSource.data.slice();
        if (!sort.active || sort.direction === '') {
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
            this.dataSource.paginator = this.paginator;
            return;
        }
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data.sort(function (a, b) {
            var isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'finYear': return _this.compare(a.isrFinYr, b.isrFinYr, isAsc);
                case 'rateType': return _this.compare(a.isrRateType, b.isrRateType, isAsc);
                case 'rateFor': return _this.compare(a.isrRateFor, b.isrRateFor, isAsc);
                case 'lowerLimit': return _this.compare(a.isrLlimit, b.isrLlimit, isAsc);
                case 'upperLimit': return _this.compare(a.isrUlimit, b.isrUlimit, isAsc);
                case 'addedAmount': return _this.compare(a.isrAddedAmt, b.isrAddedAmt, isAsc);
                case 'percentageValue': return _this.compare(a.isrPercVal, b.isrPercVal, isAsc);
                default: return 0;
            }
        }));
        this.dataSource.paginator = this.paginator;
    };
    ItRatesComponent.prototype.compare = function (a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    };
    ItRatesComponent.prototype.GetFinancialYear = function () {
        var _this = this;
        this._service.getFinancialYear().subscribe(function (result) {
            _this.financialYears = result;
        });
    };
    ItRatesComponent.prototype.GetAllRateTypeAndRateFor = function () {
        var _this = this;
        this._service.getAllRateTypeAndRateFor('').subscribe(function (result) {
            if (result && result.length > 0) {
                _this.rateType = result.filter(function (p) { return p.codeText == 'RT'; });
                _this.rateFor = result.filter(function (p) { return p.codeText == 'RF'; });
            }
        });
    };
    ItRatesComponent.prototype.ngOnInit = function () {
        this.GetFinancialYear();
        this.GetAllRateTypeAndRateFor();
        this.search(0);
        this.lblSubmitStatus = 'Save';
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ItRatesComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ItRatesComponent.prototype, "formGroupDirective", void 0);
    ItRatesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-it-rates',
            template: __webpack_require__(/*! ./it-rates.component.html */ "./src/app/income-tax/it-rates/it-rates.component.html"),
            styles: [__webpack_require__(/*! ./it-rates.component.css */ "./src/app/income-tax/it-rates/it-rates.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_income_tax_it_rates_service__WEBPACK_IMPORTED_MODULE_4__["ITRatesService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_3__["CommonMsg"]])
    ], ItRatesComponent);
    return ItRatesComponent;
}());



/***/ }),

/***/ "./src/app/income-tax/other-dues/other-dues.component.css":
/*!****************************************************************!*\
  !*** ./src/app/income-tax/other-dues/other-dues.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-table {\r\n  border-collapse: collapse;\r\n}\r\n\r\nmat-cell {\r\n  border: 1px solid #000000;\r\n}\r\n\r\nmat-header-cell {\r\n  border: 1px solid #000000;\r\n}\r\n\r\nmat-header-row {\r\n  background-color: #3268ab;\r\n}\r\n\r\nmat-cell:last-of-type, mat-footer-cell:last-of-type, mat-header-cell:last-of-type {\r\n  padding-right: 0px;\r\n}\r\n\r\n.actionWidth {\r\n  width: 90px;\r\n}\r\n\r\n.bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n.record-not-found {\r\n  text-align: center;\r\n  display: inline-block;\r\n  height: 33px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jb21lLXRheC9vdGhlci1kdWVzL290aGVyLWR1ZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLDRCQUE0QjtDQUM3Qjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixzQkFBc0I7RUFDdEIsYUFBYTtDQUNkIiwiZmlsZSI6InNyYy9hcHAvaW5jb21lLXRheC9vdGhlci1kdWVzL290aGVyLWR1ZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC10YWJsZSB7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxufVxyXG5cclxubWF0LWNlbGwge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbn1cclxuXHJcbm1hdC1oZWFkZXItY2VsbCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDAwMDtcclxufVxyXG5cclxubWF0LWhlYWRlci1yb3cge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzMjY4YWI7XHJcbn1cclxuXHJcbm1hdC1jZWxsOmxhc3Qtb2YtdHlwZSwgbWF0LWZvb3Rlci1jZWxsOmxhc3Qtb2YtdHlwZSwgbWF0LWhlYWRlci1jZWxsOmxhc3Qtb2YtdHlwZSB7XHJcbiAgcGFkZGluZy1yaWdodDogMHB4O1xyXG59XHJcblxyXG4uYWN0aW9uV2lkdGgge1xyXG4gIHdpZHRoOiA5MHB4O1xyXG59XHJcblxyXG4uYmdjb2xvciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmJkYTk5O1xyXG59XHJcblxyXG4ucmVjb3JkLW5vdC1mb3VuZCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBoZWlnaHQ6IDMzcHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/income-tax/other-dues/other-dues.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/income-tax/other-dues/other-dues.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccessStatus\" (click)=\"isSuccessStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"isWarningStatus\" (click)=\"isWarningStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"isRateDetailPanel\" [ngClass]=\"bgColor\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\" (click)=\"resetRateDetailPanel()\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Other Dues (to be added to salary)\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n\r\n    <form [formGroup]=\"otherDuesForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"duesId\" placeholder=\"Please Select Dues\" [disabled]=\"onEdit\" required>\r\n            <!--<mat-option value=\"\">Please Select</mat-option>-->\r\n            <mat-option *ngFor=\"let code of ArrddlDues\" value=\"{{code.duesId}}\">{{code.duesDesc}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Please select Dues</mat-error>\r\n          <!--<mat-error *ngIf=\"otherDuesForm.controls.duesId.value == 0\">Please select other dues</mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <mat-card class=\"example-card mat-card margin-top-10px\" [ngClass]=\"bgColor\">\r\n        <div class=\"fom-title\">Section Details</div>\r\n        <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"majorSectionCode\" (selectionChange)=\"BindSectionCode($event.value)\" placeholder=\"Please Select Major Section Code\" required>\r\n              <!--<mat-option value=\"\">Please Select</mat-option>-->\r\n              <mat-option *ngFor=\"let code of ArrddlMajSecCode\" value=\"{{code.majorSectionCode}}\">{{code.majorSectionDesc}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Major Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"sectionCode\" (selectionChange)=\"BindSubSectionCode()\" placeholder=\"Please Select Section Code\" required>\r\n              <!--<mat-option value=\"\">Please Select</mat-option>-->\r\n              <mat-option *ngFor=\"let code of ArrddlSecCode\" value=\"{{code.secCode}}\">{{code.secCode}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <!--<mat-select formControlName=\"subSectionCode\" (selectionChange)=\"BindSectionCodeDescription()\" placeholder=\"Sub Section Code\" [(value)]=\"ArrddlSubSecCode.subSecCode\">-->\r\n            <mat-select formControlName=\"subSectionCode\" (selectionChange)=\"BindSectionCodeDescription()\" placeholder=\"Please Select Sub Section Code\" required>\r\n              <!--<mat-option value=\"\">Please Select</mat-option>-->\r\n              <mat-option *ngFor=\"let code of ArrddlSubSecCode\" value=\"{{code.subSecCode}}\">{{code.subSecCode}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please select Sub Section Code</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput formControlName=\"sectionDesc\" (paste)=\"$event.preventDefault()\" placeholder=\"Section Description\" autocomplete=off readonly></textarea>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" [ngClass]=\"btnCssClass\" *ngIf=\"submitButtonText\">{{submitButtonText}}</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\">Cancel</button>\r\n        </div>\r\n\r\n      </mat-card>\r\n\r\n    </form>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Other Dues Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    \r\n    <mat-form-field>\r\n      <input matInput (keyup.enter)=\"applyFilter($event.target.value)\" [(ngModel)]=\"searchTerm\" placeholder=\"Search\">\r\n      <span (click)=\"applyFilter(searchTerm)\"><i class=\"material-icons icon-right\">search</i></span>\r\n    </mat-form-field>\r\n    <div class=\"tabel-wraper\">\r\n      <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"serialNo\">\r\n          <th mat-header-cell mat-header-cell *matHeaderCellDef>S.No</th>\r\n          <td mat-cell mat-cell *matCellDef=\"let element; let i = index;\">{{ i+1 }}</td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"duesId\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Dues Id</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.duesId}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"duesDesc\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Dues Description</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.duesDesc}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"sectionCode\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Section </th>\r\n          <td mat-cell *matCellDef=\"let element\"> <label *ngIf=\"element.subSectionCode != 'NA'\">{{element.sectionCode + ' ' + element.subSectionCode}}</label> <label *ngIf=\"element.subSectionCode == 'NA'\">{{element.sectionCode}}</label>  </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"action\">\r\n          <th mat-header-cell *matHeaderCellDef class=\"actionWidth\"> Action </th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">edit</a>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n          </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n\r\n      </table>\r\n    </div>\r\n    <mat-paginator #paginator [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"5\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n    <mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"dataSource.data.length == 0\">{{noRecordMsg}}</mat-toolbar>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img style=\"height:70px;width:70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete record?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = !DeletePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/income-tax/other-dues/other-dues.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/income-tax/other-dues/other-dues.component.ts ***!
  \***************************************************************/
/*! exports provided: OtherDuesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherDuesComponent", function() { return OtherDuesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_income_tax_other_dues_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/income-tax/other-dues.service */ "./src/app/services/income-tax/other-dues.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OtherDuesComponent = /** @class */ (function () {
    function OtherDuesComponent(_service, _msg) {
        this._service = _service;
        this._msg = _msg;
        //displayedColumns: string[] = ['serialNo', 'duesDesc', 'majorSectionCode', 'sectionCode', 'subSectionCode', 'sectionCombined', 'action'];
        //displayedColumns: string[] = ['serialNo', 'duesDesc', 'sectionCode', 'action'];
        this.displayedColumns = ['duesDesc', 'sectionCode', 'action'];
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.data = [];
        this.DeletePopup = false;
        this.allData = [];
        this.ArrddlMajSecCode = [];
        this.ArrddlSecCode = [];
        this.ArrddlSubSecCode = [];
        this.ArrddlSecCodeDesc = [];
        this.ArrddlDues = [];
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.userName = '';
        this.otherDuesDetails = [];
        this.onEdit = false;
        this.submitButtonText = 'Save';
        this.isRateDetailPanel = false;
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
        this.isLoading = false;
        this.btnCssClass = 'btn btn-success';
        this.isUpdated = false;
    }
    OtherDuesComponent.prototype.createForm = function () {
        this.otherDuesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            msEdrId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            majorSectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            sectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            subSectionCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            sectionDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            duesId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            duesDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
    };
    OtherDuesComponent.prototype.ngOnInit = function () {
        this.isLoading = true;
        this.onEdit = false;
        this.createForm();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.data);
        this.totalCount = this.data.length;
        this.BindOtherDues(false);
        this.BindMajorSectionCode();
        this.userName = sessionStorage.getItem('username');
        this.getOtherDuesDetails(this.pageSize, this.pageNumber);
        this.noRecordMsg = this._msg.noRecordMsg;
        this.submitButtonText = 'Save';
        this.isLoading = false;
        this.isUpdated = false;
    };
    OtherDuesComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.otherDuesForm.valid) {
            this.isLoading = true;
            this.otherDuesForm.controls.userName.setValue(this.userName);
            this._service.SaveOtherDuesDetails(this.otherDuesForm.value).subscribe(function (response) {
                if (response && response != undefined) {
                    _this.isSuccessStatus = true;
                    if (_this.isUpdated) {
                        _this.responseMessage = _this._msg.updateMsg;
                    }
                    else {
                        _this.responseMessage = _this._msg.saveMsg;
                    }
                    _this.getOtherDuesDetails(_this.pageSize, _this.pageNumber);
                    _this.BindOtherDues(false);
                    _this.otherDuesForm.reset();
                    //this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
                    //swal(this._msg.saveMsg);
                    _this.formGroupDirective.resetForm();
                    _this.createForm();
                    _this.submitButtonText = 'Save';
                    _this.onEdit = false;
                }
                else {
                    if (_this.isUpdated) {
                        _this.responseMessage = _this._msg.updateFailedMsg;
                    }
                    else {
                        _this.responseMessage = _this._msg.saveFailedMsg;
                    }
                    _this.isWarningStatus = true;
                }
                _this.bgColor = '';
                _this.isRateDetailPanel = false;
                _this.btnCssClass = 'btn btn-success';
                _this.isUpdated = false;
            });
            this.isLoading = false;
            setTimeout(function () {
                _this.isSuccessStatus = false;
                _this.isWarningStatus = false;
                _this.responseMessage = '';
            }, this._msg.messageTimer);
        }
    };
    OtherDuesComponent.prototype.cancelForm = function () {
        this.onEdit = false;
        this.otherDuesForm.reset();
        this.formGroupDirective.resetForm();
        this.createForm();
        this.BindOtherDues(false);
        this.submitButtonText = 'Save';
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
        this.isUpdated = false;
    };
    OtherDuesComponent.prototype.applyFilterPrevious = function (value) {
        var data = this.dataSource.data;
        //if (value && data.length > 0) {
        if (value) {
            value = value.toLowerCase();
            //let data = this.dataSource.data as any[];
            var filteredData = data.filter(function (a) { return a.duesDesc.toLowerCase().includes(value) || a.majorSectionCode.toLowerCase().includes(value) || a.sectionCode.toLowerCase().includes(value); });
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](filteredData);
        }
        else {
            this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.otherDuesDetails);
        }
        this.totalCount = this.dataSource.data.length;
    };
    OtherDuesComponent.prototype.applyFilter = function (value) {
        var _this = this;
        if (value) {
            this.totalCount = 0;
            this._service.getOtherDuesDetails(this.pageNumber, this.pageSize, this.searchTerm).subscribe(function (filteredData) {
                if (filteredData && filteredData != undefined && filteredData.length > 0) {
                    _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](filteredData);
                    _this.dataSource.sort = _this.sort;
                    _this.allData = filteredData;
                    _this.totalCount = filteredData[0].totalCount;
                }
                else {
                    _this.getOtherDuesDetails(_this.pageSize, 1);
                }
            });
        }
        else {
            this.getOtherDuesDetails(this.pageSize, 1);
        }
        this.totalCount = this.dataSource.data.length;
    };
    OtherDuesComponent.prototype.getPaginationData = function (event) {
        this.pageSize = event.pageSize;
        this.pageNumber = event.pageIndex + 1;
        this.getOtherDuesDetails(this.pageSize, this.pageNumber);
    };
    OtherDuesComponent.prototype.getOtherDuesDetails = function (pageSize, pageNumber) {
        var _this = this;
        this.totalCount = 0;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this._service.getOtherDuesDetails(pageNumber, pageSize, this.searchTerm).subscribe(function (result) {
            if (result && result != undefined && result.length > 0) {
                _this.otherDuesDetails = result;
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
                _this.dataSource.sort = _this.sort;
                _this.allData = result;
                _this.totalCount = result[0].totalCount;
            }
        });
    };
    OtherDuesComponent.prototype.btnEditClick = function (obj) {
        this.isLoading = true;
        this.onEdit = true;
        this.BindOtherDues(true);
        this.otherDuesForm.patchValue(obj);
        this.otherDuesForm.controls.duesId.setValue('' + obj.duesId + '');
        this.BindSectionCode(obj.majorSectionCode);
        this.BindSubSectionCodeOnEdit(obj.majorSectionCode, obj.sectionCode);
        this.submitButtonText = 'Update';
        this.bgColor = 'bgcolor';
        this.isRateDetailPanel = true;
        this.btnCssClass = 'btn btn-info';
        this.isUpdated = true;
    };
    OtherDuesComponent.prototype.deleteDetails = function (element) {
        this.elementToBeDeleted = element;
    };
    OtherDuesComponent.prototype.confirmDelete = function () {
        var _this = this;
        this.isUpdated = false;
        this._service.DeleteOtherDuesDetails(this.elementToBeDeleted.duesId, this.userName).subscribe(function (response) {
            if ((response != undefined || response != null) && response > 0) {
                var data = _this.dataSource.data;
                var index = data.indexOf(_this.elementToBeDeleted);
                data.splice(index, 1);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                _this.elementToBeDeleted = null;
                _this.totalCount = _this.totalCount - 1;
                _this.createForm();
                _this.BindOtherDues(false);
                //this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
                //swal(this._msg.deleteMsg);
                _this.isSuccessStatus = false;
                _this.isWarningStatus = true;
                _this.responseMessage = _this._msg.deleteMsg;
            }
            else {
                //swal(this._msg.deleteFailedMsg);
                _this.isSuccessStatus = false;
                _this.isWarningStatus = true;
                _this.responseMessage = _this._msg.deleteFailedMsg;
            }
            _this.DeletePopup = false;
            setTimeout(function () {
                _this.isSuccessStatus = false;
                _this.isWarningStatus = false;
                _this.responseMessage = '';
            }, _this._msg.messageTimer);
        });
    };
    //cancel() {
    //  $(".dialog__close-btn").click();
    //}
    OtherDuesComponent.prototype.BindOtherDues = function (allDues) {
        var _this = this;
        this._service.GetOtherDues(allDues).subscribe(function (data) {
            _this.ArrddlDues = data;
        });
    };
    OtherDuesComponent.prototype.BindMajorSectionCode = function () {
        var _this = this;
        this._service.GetMajorSectionCode().subscribe(function (data) {
            _this.ArrddlMajSecCode = data;
        });
        this.otherDuesForm.controls.sectionCode.setValue('');
        this.otherDuesForm.controls.subSectionCode.setValue('');
        this.otherDuesForm.controls.sectionDesc.setValue('');
    };
    OtherDuesComponent.prototype.BindSectionCode = function (sectionCode) {
        var _this = this;
        this._service.GetSectionCode(sectionCode).subscribe(function (data) {
            _this.ArrddlSecCode = data;
        });
        this.BindSubSectionCode();
        this.otherDuesForm.controls.sectionDesc.setValue('');
        this.otherDuesForm.controls.msEdrId.setValue(0);
    };
    OtherDuesComponent.prototype.BindSubSectionCode = function () {
        var _this = this;
        debugger;
        var selectedMajSecCode = this.otherDuesForm.controls.majorSectionCode.value;
        var selectedSecCode = this.otherDuesForm.controls.sectionCode.value;
        this._service.GetSubSectionCode(selectedMajSecCode, selectedSecCode).subscribe(function (data) {
            _this.ArrddlSubSecCode = data;
            if (data.length == 1) {
                _this.otherDuesForm.controls.subSectionCode.setValue(data[0].subSecCode);
            }
            else {
                if (!_this.onEdit) {
                    //this.otherDuesForm.controls.subSectionCode.setValue('');
                }
            }
            _this.otherDuesForm.controls.sectionDesc.setValue('');
            if (_this.onEdit) {
                _this.BindSectionCodeDescription();
                _this.onEdit = false;
            }
            else {
                if (_this.ArrddlSubSecCode.length == 1) {
                    _this.otherDuesForm.controls.subSectionCode.setValue(_this.ArrddlSubSecCode[0].subSecCode);
                    _this.BindSectionCodeDescription();
                }
                else {
                    _this.otherDuesForm.controls.subSectionCode.setValue('');
                    _this.otherDuesForm.controls.sectionDesc.setValue('');
                }
            }
        });
    };
    OtherDuesComponent.prototype.BindSubSectionCodeOnEdit = function (majSecCode, secCode) {
        var _this = this;
        debugger;
        this._service.GetSubSectionCode(majSecCode, secCode).subscribe(function (data) {
            _this.ArrddlSubSecCode = data;
        });
        this.BindSectionCodeDescription();
    };
    OtherDuesComponent.prototype.BindSectionCodeDescription = function () {
        var _this = this;
        debugger;
        var selectedMajSecCode = this.otherDuesForm.controls.majorSectionCode.value;
        var selectedSecCode = this.otherDuesForm.controls.sectionCode.value;
        var selectedSubSecCode = this.otherDuesForm.controls.subSectionCode.value;
        this._service.GetSectionCodeDescription(selectedMajSecCode, selectedSecCode, selectedSubSecCode).subscribe(function (data) {
            if ((data != null || data != undefined) && data.length > 0) {
                _this.ArrddlSecCodeDesc = data;
                _this.otherDuesForm.controls.sectionDesc.setValue(data[0].secDesc);
                _this.otherDuesForm.controls.msEdrId.setValue(data[0].secId);
            }
            _this.isLoading = false;
        });
    };
    OtherDuesComponent.prototype.resetRateDetailPanel = function () {
        this.isRateDetailPanel = false;
        this.bgColor = '';
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], OtherDuesComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], OtherDuesComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], OtherDuesComponent.prototype, "paginator", void 0);
    OtherDuesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-other-dues',
            template: __webpack_require__(/*! ./other-dues.component.html */ "./src/app/income-tax/other-dues/other-dues.component.html"),
            styles: [__webpack_require__(/*! ./other-dues.component.css */ "./src/app/income-tax/other-dues/other-dues.component.css")]
        }),
        __metadata("design:paramtypes", [_services_income_tax_other_dues_service__WEBPACK_IMPORTED_MODULE_4__["OtherDuesService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_3__["CommonMsg"]])
    ], OtherDuesComponent);
    return OtherDuesComponent;
}());



/***/ }),

/***/ "./src/app/income-tax/standard-deduction/standard-deduction.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/income-tax/standard-deduction/standard-deduction.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*mat-table {\r\n  border-collapse: collapse;\r\n}\r\n\r\nmat-row {\r\n  border-right: 1px solid #000000;\r\n}\r\n\r\nmat-cell {\r\n  border-right: 1px solid #000000;\r\n}\r\n\r\nmat-header-cell {\r\n  border-right: 1px solid #000000;\r\n  color: white;\r\n  justify-content:center;\r\n}\r\n\r\nmat-header-row {\r\n  background-color: #3268ab;\r\n  color: white;\r\n}\r\n.customWidthClassForm {\r\n  flex: 0 0 30px;\r\n}\r\n.customWidthClass {\r\n  flex: 0 0 70px;\r\n}\r\n.alignTextCentre{\r\n  display:flex;\r\n  justify-content:center\r\n}*/\r\n\r\n.record-not-found {\r\n  text-align: center;\r\n  display: inline-block;\r\n  height: 33px;\r\n}\r\n\r\n.form-field-width {\r\n  width: 150px;\r\n}\r\n\r\n.customWidthClass {\r\n  flex: 0 0 75px;\r\n}\r\n\r\n.bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jb21lLXRheC9zdGFuZGFyZC1kZWR1Y3Rpb24vc3RhbmRhcmQtZGVkdWN0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0ErQkc7O0FBRUg7RUFDRSxtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLGFBQWE7Q0FDZDs7QUFDRDtFQUNFLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSw0QkFBNEI7Q0FDN0IiLCJmaWxlIjoic3JjL2FwcC9pbmNvbWUtdGF4L3N0YW5kYXJkLWRlZHVjdGlvbi9zdGFuZGFyZC1kZWR1Y3Rpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qbWF0LXRhYmxlIHtcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcblxyXG5tYXQtcm93IHtcclxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG59XHJcblxyXG5tYXQtY2VsbCB7XHJcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzAwMDAwMDtcclxufVxyXG5cclxubWF0LWhlYWRlci1jZWxsIHtcclxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjMDAwMDAwO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG59XHJcblxyXG5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMyNjhhYjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLmN1c3RvbVdpZHRoQ2xhc3NGb3JtIHtcclxuICBmbGV4OiAwIDAgMzBweDtcclxufVxyXG4uY3VzdG9tV2lkdGhDbGFzcyB7XHJcbiAgZmxleDogMCAwIDcwcHg7XHJcbn1cclxuLmFsaWduVGV4dENlbnRyZXtcclxuICBkaXNwbGF5OmZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OmNlbnRlclxyXG59Ki9cclxuXHJcbi5yZWNvcmQtbm90LWZvdW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGhlaWdodDogMzNweDtcclxufVxyXG4uZm9ybS1maWVsZC13aWR0aCB7XHJcbiAgd2lkdGg6IDE1MHB4O1xyXG59XHJcblxyXG4uY3VzdG9tV2lkdGhDbGFzcyB7XHJcbiAgZmxleDogMCAwIDc1cHg7XHJcbn1cclxuXHJcbi5iZ2NvbG9yIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmYmRhOTk7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/income-tax/standard-deduction/standard-deduction.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/income-tax/standard-deduction/standard-deduction.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccessStatus\" (click)=\"isSuccessStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"isWarningStatus\" (click)=\"isWarningStatus = false\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong> {{responseMessage}} </strong>\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"isRateDetailPanel\" [ngClass]=\"bgColor\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\" (click)=\"resetRateDetailPanel()\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Section Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <form [formGroup]=\"incomeTaxRatesForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"serSecCode\" placeholder=\"Section Code\" required>\r\n            <mat-option value=\"\" (click)=\"bindEntertainMasters('')\">Select</mat-option>\r\n            <mat-option *ngFor=\"let ele of entertainMaster\" value=\"{{ele.sdeSecCode}}\" (click)=\"bindEntertainMasters(ele.msSdrId)\">{{ele.sdeSecCode}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Please select a Section Code!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"serSecDesc\" placeholder=\"Section Description\" [disabled]=\"true\">\r\n            <mat-option value=\"\" (click)=\"bindEntertainMasters('')\">Select</mat-option>\r\n            <mat-option *ngFor=\"let ele of entertainMaster\" value=\"{{ele.sdeDesc}}\" (click)=\"bindEntertainMasters(ele.msSdrId)\">{{ele.sdeDesc}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Please select a Section Description!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select formControlName=\"serSlab\" placeholder=\"Slab\" [disabled]=\"true\">\r\n            <mat-option value=\"\" (click)=\"bindEntertainMasters('')\">Select</mat-option>\r\n            <mat-option *ngFor=\"let ele of entertainMaster\" value=\"{{ele.sdeSlab}}\" (click)=\"bindEntertainMasters(ele.msSdrId)\">{{ele.sdeSlab}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Please select a Slab!</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <mat-card class=\"example-card mat-card margin-top-10px\" [ngClass]=\"bgColor\">\r\n        <div class=\"fom-title\">Financial Year Details</div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"serFinFyr\" placeholder=\"Financial Year From\" (selectionChange)=\"onFinYearChange()\" required>\r\n              <mat-option value=\"\">Select</mat-option>\r\n              <mat-option *ngFor=\"let fin of financialYears\" value=\"{{fin}}\">{{fin}}</mat-option>\r\n            </mat-select>\r\n            <!--<input matInput formControlName=\"serFinFyr\" placeholder=\"Financial Year From\" autocomplete=off maxlength=\"9\" (keyup)=\"modifyFinancialYearFromInput($event, 'from')\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57)\">-->\r\n            <mat-error *ngIf=\"incomeTaxRatesForm.controls.serFinFyr && incomeTaxRatesForm.controls.serFinFyr.errors && incomeTaxRatesForm.controls.serFinFyr.errors.required\">Please enter Financial Year From!</mat-error>\r\n            <mat-error *ngIf=\"incomeTaxRatesForm.controls.serFinFyr && incomeTaxRatesForm.controls.serFinFyr.errors && incomeTaxRatesForm.controls.serFinFyr.errors.invalidError\">Financial Year From should be less than Financial Year To!</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"serFinTyr\" placeholder=\"Financial Year To\" (selectionChange)=\"onFinYearChange()\" required>\r\n              <mat-option value=\"\">Select</mat-option>\r\n              <mat-option *ngFor=\"let fin of financialYears\" value=\"{{fin}}\">{{fin}}</mat-option>\r\n            </mat-select>\r\n            <!--<input matInput formControlName=\"serFinTyr\" placeholder=\"Financial Year To\" autocomplete=off maxlength=\"9\" (keyup)=\"modifyFinancialYearFromInput($event, 'to')\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57)\">-->  <!--|| (event.charCode == 45)-->\r\n            <mat-error>Please enter Financial Year To!</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"serRateFor\" placeholder=\"Rates For\" required>\r\n              <mat-option value=\"\">Select</mat-option>\r\n              <mat-option value=\"M\">M - Male</mat-option>\r\n              <mat-option value=\"F\">F- Female</mat-option>\r\n              <mat-option value=\"C\">C - Common</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please enter Rates For!</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n      </mat-card>\r\n      <mat-card class=\"example-card mat-card margin-top-10px\" [ngClass]=\"bgColor\">\r\n        <div class=\"fom-title\">Rate Details</div>\r\n        <div class=\"tabel-wraper wid-100\">\r\n          <table class=\"mat-elevation-z8 col-md-12 col-lg-12 table2\" formArrayName=\"rateDetails\">\r\n            <tr>\r\n              <th>Lower Limit</th>\r\n              <th>Upper Limit</th>\r\n              <th>Percentage Value %</th>\r\n              <th>Max Admissible</th>\r\n              <th>Action</th>\r\n            </tr>\r\n            <tr *ngFor=\"let rate of incomeTaxRatesForm.controls.rateDetails.controls; let i = index;\" [formGroupName]=\"i\">\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput class=\"mat-input-element\" formControlName=\"serLLimit\" (keyup)=\"validateLowerLimit(i)\" maxlength=\"6\" placeholder=\"Lower Limit\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors.required == true\">Please enter Lower Limit!</mat-error>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors.collapse == true\">Lower limit and upper limit value should not be overlapped!</mat-error>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serLLimit.errors.invalidError == true\">Should be less than Upper Limit!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput class=\"mat-input-element\" formControlName=\"serULimit\" (keyup)=\"validateUpperLimit(i)\" maxlength=\"6\" placeholder=\"Upper Limit\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serULimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serULimit.errors.required == true\">Please enter Upper Limit!</mat-error>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serULimit.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serULimit.errors.invalidError == true\">Should be greater than lower Limit!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput class=\"mat-input-element\" formControlName=\"serPercValue\" pattern=\"^(?!0)(100|[1-9]?[0-9])(\\.\\d{1,2})?$\" maxlength=\"3\" placeholder=\"Percentage value %\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serPercValue.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serPercValue.errors.required\">Please enter Percentage value %!</mat-error>\r\n                  <mat-error *ngIf=\"incomeTaxRatesForm.controls.rateDetails && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serPercValue.errors && incomeTaxRatesForm.controls.rateDetails.controls[i].controls.serPercValue.errors.pattern\">Please enter valid percentage value %!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <mat-form-field class=\"form-field-width\">\r\n                  <input matInput class=\"mat-input-element\" formControlName=\"serValue\" maxlength=\"6\" placeholder=\"Max Admissible\" autocomplete=off onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\" required>\r\n                  <mat-error>Please enter Max Admissible!</mat-error>\r\n                </mat-form-field>\r\n              </td>\r\n              <td>\r\n                <a *ngIf=\"rateDetails.length > 1\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"deleteRateDetail(i)\">\r\n                  delete_forever\r\n                </a>\r\n              </td>\r\n            </tr>\r\n          </table>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12 text-right btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" (click)=\"addRateDetail()\">Add Row</button>\r\n        </div>\r\n      </mat-card>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" [ngClass]=\"btnCssClass\" *ngIf=\"submitButtonText\">{{submitButtonText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\">Cancel</button>\r\n      </div>\r\n    </form>\r\n    \r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Rate Detail History\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter()\" [(ngModel)]=\"searchTerm\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <div class=\"tabel-wraper\">\r\n      <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"dataSource.data.length > 0\">\r\n\r\n        <ng-container matColumnDef=\"sno\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header [ngClass]=\"'customWidthClass'\"> Sno. </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\" [ngClass]=\"'customWidthClass'\">{{ i + 1 }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"serSecCode\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> Section Code </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\" [ngClass]=\"'alignTextCentre'\"> {{element.serSecCode }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"serFinFyr\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Financial Year From</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\" [ngClass]=\"'alignTextCentre'\"> {{element.serFinFyr}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"serFinTyr\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Financial Year To</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\" [ngClass]=\"'alignTextCentre'\"> {{element.serFinTyr}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"serRateFor\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header [ngClass]=\"'customWidthClass'\">Rate for</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\" [ngClass]=\"'alignTextCentre customWidthClass'\"> {{element.serRateFor}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"action\">\r\n          <mat-header-cell *matHeaderCellDef [ngClass]=\"'customWidthClass'\">Action</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\" [ngClass]=\"'customWidthClass'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element)\">edit</a>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n    </div>\r\n    <!--<mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" #paginator [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10, 20, 50]\" (page)=\"pageEvent = getPaginationData($event)\" showFirstLastButtons></mat-paginator>-->\r\n    <mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" #paginator [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"pageSize\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"dataSource.data.length == 0\">Record is not found</mat-toolbar>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = false\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/income-tax/standard-deduction/standard-deduction.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/income-tax/standard-deduction/standard-deduction.component.ts ***!
  \*******************************************************************************/
/*! exports provided: StandardDeductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StandardDeductionComponent", function() { return StandardDeductionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_income_tax_standard_deduction_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/income-tax/standard-deduction.services */ "./src/app/services/income-tax/standard-deduction.services.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StandardDeductionComponent = /** @class */ (function () {
    function StandardDeductionComponent(formBuilder, _service, _msg) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._msg = _msg;
        this.is_btnStatus = false;
        this.displayedColumnsForForm = ['lowerLimit', 'upperLimit', 'percentageValue', 'maxAdmissible', 'action'];
        //displayedColumns: string[] = ['sno', 'serSecCode', 'serFinFyr', 'serFinTyr', 'serRateFor', 'action'];
        this.displayedColumns = ['serSecCode', 'serFinFyr', 'serFinTyr', 'serRateFor', 'action'];
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.searchTerm = '';
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.DeletePopup = false;
        this.entertainMaster = [];
        this.standardDeductionRules = [];
        this.financialYears = [];
        this.submitButtonText = 'Save';
        this.isRateDetailPanel = false;
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
        this.btnCssClass = 'btn btn-success';
        this.ddoId = sessionStorage.getItem('ddoid');
        for (var i = 1999; i < 2021; i++) {
            var j = i + 1;
            var str = i + "-" + j;
            this.financialYears.push(str);
        }
    }
    StandardDeductionComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    StandardDeductionComponent.prototype.createForm = function () {
        this.incomeTaxRatesForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            msSdrID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serSecCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serSecDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            serSlab: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            serFinFyr: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serFinTyr: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serRateFor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            rateDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)])
        });
        this.addRateDetail();
    };
    StandardDeductionComponent.prototype.createRateDetail = function () {
        return this.formBuilder.group({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.rateDetails.value.length + 1),
            msSdentRuleID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            serLLimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serULimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            serPercValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^0*(?:[1-9][0-9]?|100)$")]),
            serValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
        });
    };
    //customValidators(frm: FormGroup): any {
    //  let lLimit = frm.controls.serLLimit.value;
    //  let uLimit = frm.controls.serULimit.value;
    //  if (+lLimit > +uLimit) {
    //    frm.controls.serLLimit.setErrors({ 'invalidError': true });
    //    return { 'invalidError': true };
    //  }
    //  else {
    //    frm.controls.serLLimit.setErrors(null);
    //    if (!lLimit) {
    //      frm.controls.serLLimit.setErrors({ 'required': true })
    //    }
    //    return null;
    //  }
    //}
    StandardDeductionComponent.prototype.onFinYearChange = function () {
        var finFyr = this.incomeTaxRatesForm.controls.serFinFyr.value;
        var finTyr = this.incomeTaxRatesForm.controls.serFinTyr.value;
        var splitFyr = finFyr.split('-');
        var splitTyr = finTyr.split('-');
        if (splitTyr.length > 1 && splitFyr.length > 1) {
            if (splitFyr[1] > splitTyr[0]) {
                this.incomeTaxRatesForm.controls.serFinFyr.setErrors({ 'invalidError': true });
            }
            else {
                this.incomeTaxRatesForm.controls.serFinFyr.setErrors(null);
            }
        }
    };
    StandardDeductionComponent.prototype.addRateDetail = function () {
        this.rateDetails.push(this.createRateDetail());
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
    };
    Object.defineProperty(StandardDeductionComponent.prototype, "rateDetails", {
        get: function () {
            return this.incomeTaxRatesForm.get("rateDetails");
        },
        enumerable: true,
        configurable: true
    });
    StandardDeductionComponent.prototype.deleteRateDetail = function (index) {
        if (this.rateDetails.length > 1) {
            //if (obj.msSdentRuleID && obj.msSdentRuleID > 0) {
            //  this._service.deleteStandardDeductionRateDetail(obj.msSdentRuleID).subscribe(response => {
            //    this.snackBar.open("Entry Deleted", null, { duration: 4000 });
            //  });
            //}
            this.rateDetails.removeAt(index);
            this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
        }
    };
    StandardDeductionComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.incomeTaxRatesForm.valid) {
            this._service.upsertStandardDeductionRule(this.incomeTaxRatesForm.value).subscribe(function (response) {
                if (response > 0) {
                    //this.getStandardDeductionRules();
                    _this.incomeTaxRatesForm.reset();
                    if (_this.submitButtonText == 'Save') {
                        //swal(this._msg.saveMsg);
                        _this.responseMessage = _this._msg.saveMsg;
                    }
                    else {
                        //swal(this._msg.updateMsg);
                        _this.responseMessage = _this._msg.updateMsg;
                    }
                    _this.isSuccessStatus = true;
                    _this.isWarningStatus = false;
                    _this.formGroupDirective.resetForm();
                    _this.createForm();
                    _this.getStandardDeductionRules();
                }
                else {
                    //swal(this._msg.alreadyExistMsg);
                    _this.isSuccessStatus = false;
                    _this.isWarningStatus = true;
                    _this.responseMessage = _this._msg.alreadyExistMsg;
                }
                _this.submitButtonText = 'Save';
                setTimeout(function () {
                    _this.isSuccessStatus = false;
                    _this.isWarningStatus = false;
                    _this.responseMessage = '';
                }, _this._msg.messageTimer);
            });
            this.bgColor = '';
            this.btnCssClass = 'btn btn-success';
        }
    };
    StandardDeductionComponent.prototype.cancelForm = function () {
        this.incomeTaxRatesForm.reset();
        this.formGroupDirective.resetForm();
        this.createForm();
        this.searchTerm = '';
        this.getStandardDeductionRules();
        this.submitButtonText = 'Save';
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
    };
    StandardDeductionComponent.prototype.applyFilter = function () {
        this.totalCount = 0;
        this.pageNumber = 1;
        this.getStandardDeductionRules();
    };
    StandardDeductionComponent.prototype.getPaginationData = function (e) {
        this.pageNumber = e.pageIndex + 1;
        this.pageSize = e.pageSize;
        this.getStandardDeductionRules();
    };
    StandardDeductionComponent.prototype.btnEditClick = function (element) {
        var _this = this;
        while (this.rateDetails.length != 0) {
            this.rateDetails.removeAt(0);
        }
        // this.isRateDetailPanel = false;
        this.incomeTaxRatesForm.patchValue(element);
        this.incomeTaxRatesForm.controls.ddoId.setValue(this.ddoId);
        this.bindEntertainMasters(element.msSdrID);
        this.rateDetails.removeAt(0);
        element.rateDetails.forEach(function (rate) { return _this.rateDetails.push(_this.formBuilder.group(rate)); });
        //for (var i = 0; i < this.rateDetails.length; i++) {
        //  this.rateDetails.controls[i].setValidators(this.customValidators);
        //}
        this.isRateDetailPanel = true;
        this.formDataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.rateDetails.value);
        this.submitButtonText = 'Update';
        this.bgColor = 'bgcolor';
        this.btnCssClass = 'btn btn-info';
    };
    StandardDeductionComponent.prototype.resetRateDetailPanel = function () {
        this.isRateDetailPanel = false;
        this.bgColor = '';
    };
    StandardDeductionComponent.prototype.DeleteDetails = function (element) {
        this.elementToBeDeleted = element;
    };
    StandardDeductionComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.deleteStandardDeductionRule(this.elementToBeDeleted).subscribe(function (response) {
            _this.elementToBeDeleted = null;
            _this.pageNumber = 1;
            _this.paginator.firstPage();
            _this.dataSource.paginator = _this.paginator;
            _this.getStandardDeductionRules();
            _this.createForm();
            _this.incomeTaxRatesForm.reset();
            _this.formGroupDirective.resetForm();
            //swal(this._msg.deleteMsg);
            _this.isWarningStatus = true;
            _this.isSuccessStatus = false;
            _this.responseMessage = _this._msg.deleteMsg;
            _this.DeletePopup = false;
            setTimeout(function () {
                _this.isSuccessStatus = false;
                _this.isWarningStatus = false;
                _this.responseMessage = '';
            }, _this._msg.messageTimer);
        });
    };
    StandardDeductionComponent.prototype.getEntertainMaster = function () {
        var _this = this;
        this._service.getEntertainMaster().subscribe(function (response) {
            _this.entertainMaster = response;
        });
    };
    StandardDeductionComponent.prototype.getStandardDeductionRules = function () {
        var _this = this;
        this._service.getStandardDeductionRule(this.pageNumber, this.pageSize, this.searchTerm).subscribe(function (response) {
            _this.standardDeductionRules = response;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](response);
            _this.totalCount = response[0].totalCount;
        });
    };
    StandardDeductionComponent.prototype.bindEntertainMasters = function (id) {
        if (id) {
            var selectedData = this.entertainMaster.filter(function (a) { return a.msSdrId == id; });
            this.incomeTaxRatesForm.controls.msSdrID.setValue(selectedData[0].msSdrId);
            this.incomeTaxRatesForm.controls.serSecCode.setValue(selectedData[0].sdeSecCode);
            this.incomeTaxRatesForm.controls.serSecDesc.setValue(selectedData[0].sdeDesc);
            this.incomeTaxRatesForm.controls.serSlab.setValue(selectedData[0].sdeSlab);
        }
        else {
            this.incomeTaxRatesForm.controls.msSdrID.setValue('');
            this.incomeTaxRatesForm.controls.serSecCode.setValue('');
            this.incomeTaxRatesForm.controls.serSecDesc.setValue('');
            this.incomeTaxRatesForm.controls.serSlab.setValue('');
        }
    };
    StandardDeductionComponent.prototype.ngOnInit = function () {
        this.createForm();
        this.getEntertainMaster();
        this.getStandardDeductionRules();
        this.submitButtonText = 'Save';
        this.isRateDetailPanel = false;
    };
    //modifyFinancialYearFromInput(event: any, finYr: string) {
    //  if (finYr == 'from') {
    //    if (event.key != "Backspace") {
    //      let value = this.incomeTaxRatesForm.controls.serFinFyr.value;
    //      if (value.length == 4) {
    //        this.incomeTaxRatesForm.controls.serFinFyr.setValue(value + '-');
    //      }
    //    }
    //  }
    //  else {
    //    if (event.key != "Backspace") {
    //      let value = this.incomeTaxRatesForm.controls.serFinTyr.value;
    //      if (value.length == 4) {
    //        this.incomeTaxRatesForm.controls.serFinTyr.setValue(value + '-');
    //      }
    //    }
    //  }
    //}
    StandardDeductionComponent.prototype.validateUpperLimit = function (index) {
        debugger;
        var lLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serLLimit;
        var uLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serULimit;
        if (this.checkUpperLimitExists(lLmt, uLmt)) {
            this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': true });
            return { 'collapse': true };
        }
        else {
            if (lLmt == '' || uLmt == '') {
                return null;
            }
            else {
                if (+lLmt > +uLmt) {
                    this.rateDetails.controls[index].get("serULimit").setErrors({ 'invalidError': true });
                    this.rateDetails.controls[index].get("serLLimit").setErrors(null);
                    return { 'invalidError': true };
                }
                else {
                    //this.rateDetails.controls[index].get("serULimit").setErrors(null);
                    //this.rateDetails.controls[index].get("serLLimit").setErrors(null);
                    this.rateDetails.controls.forEach(function (key) {
                        key.get("serULimit").setErrors(null);
                        key.get("serLLimit").setErrors(null);
                    });
                    return null;
                }
            }
        }
    };
    StandardDeductionComponent.prototype.validateLowerLimit = function (index) {
        debugger;
        var lLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serLLimit;
        var uLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serULimit;
        if (this.checkUpperLimitExists(lLmt, uLmt)) {
            this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': true });
            return { 'collapse': true };
        }
        else {
            //this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': false });
            this.rateDetails.controls.forEach(function (key) {
                console.log(key);
                key.get("serLLimit").setErrors({ 'collapse': false });
                key.get("serLLimit").setErrors(null);
            });
            //this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': false });
            //this.rateDetails.controls[0].get("serLLimit").setErrors({ 'collapse': false });
            if (lLmt == '' || uLmt == '') {
                return null;
            }
            else {
                if (+lLmt > +uLmt) {
                    this.rateDetails.controls[index].get("serLLimit").setErrors({ 'invalidError': true });
                    this.rateDetails.controls[index].get("serULimit").setErrors({ 'invalidError': false });
                    return { 'invalidError': true };
                }
                else {
                    this.rateDetails.controls[index].get("serLLimit").setErrors(null);
                    this.rateDetails.controls[index].get("serULimit").setErrors(null);
                    return null;
                }
            }
        }
    };
    StandardDeductionComponent.prototype.checkUpperLimitExists = function (lowerLimt, upperLimit) {
        if (this.incomeTaxRatesForm.controls.rateDetails.value != undefined && this.incomeTaxRatesForm.controls.rateDetails.value.length > 0) {
            if (this.incomeTaxRatesForm.controls.rateDetails.value.length == 1) {
                return false;
            }
            else {
                var status_1 = this.incomeTaxRatesForm.controls.rateDetails.value.filter(function (p) { return (parseInt(p.serLLimit) <= parseInt(lowerLimt) && parseInt(p.serULimit) >= lowerLimt) || (parseInt(p.serLLimit) <= parseInt(upperLimit) && parseInt(p.serULimit) >= upperLimit); });
                if (status_1 == undefined || status_1 == null || status_1 == '') {
                    return false;
                }
                else {
                    if (status_1.length == 1 && parseInt(status_1[0].serLLimit) == lowerLimt && parseInt(status_1[0].serULimit) == upperLimit) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], StandardDeductionComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], StandardDeductionComponent.prototype, "paginator", void 0);
    StandardDeductionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-standard-deduction',
            template: __webpack_require__(/*! ./standard-deduction.component.html */ "./src/app/income-tax/standard-deduction/standard-deduction.component.html"),
            styles: [__webpack_require__(/*! ./standard-deduction.component.css */ "./src/app/income-tax/standard-deduction/standard-deduction.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_income_tax_standard_deduction_services__WEBPACK_IMPORTED_MODULE_4__["StandardDeductionService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_3__["CommonMsg"]])
    ], StandardDeductionComponent);
    return StandardDeductionComponent;
}());



/***/ }),

/***/ "./src/app/services/income-tax/exemption-deduction.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/services/income-tax/exemption-deduction.service.ts ***!
  \********************************************************************/
/*! exports provided: ExemptionDeductionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExemptionDeductionService", function() { return ExemptionDeductionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ExemptionDeductionService = /** @class */ (function () {
    function ExemptionDeductionService(http, config) {
        this.http = http;
        this.config = config;
    }
    ExemptionDeductionService.prototype.GetMajorSectionCode = function () {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetMajorSectionCode");
    };
    ExemptionDeductionService.prototype.GetSectionCode = function (majorSecCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSectionCode?majorSecCode=" + majorSecCode);
    };
    ExemptionDeductionService.prototype.GetSubSectionCode = function (majorSecCode, sectionCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSubSectionCode?majorSecCode=" + majorSecCode + "&sectionCode=" + sectionCode);
    };
    ExemptionDeductionService.prototype.GetSectionCodeDescription = function (majorSectionCode, sectionCode, subSectionCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSectionCodeDescription?majorSectionCode=" + majorSectionCode + "&sectionCode=" + sectionCode + "&subSectionCode=" + subSectionCode);
    };
    ExemptionDeductionService.prototype.GetSlabLimit = function (payCommission) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSlabLimit?payCommission=" + payCommission);
    };
    ExemptionDeductionService.prototype.GetPayCommission = function () {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetPayCommission");
    };
    ExemptionDeductionService.prototype.GetExemptionDeductionDetails = function (pageNumber, pageSize, searchTerm) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetExemptionDeductionDetails?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm);
    };
    ExemptionDeductionService.prototype.UpsertExemptionDeductionDetails = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/UpsertExemptionDeductionDetails", obj, { responseType: 'text' });
    };
    ExemptionDeductionService.prototype.DeleteExemptionDeductionDetails = function (drrId, drrId2) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/DeleteExemptionDeductionDetails?drrId=" + drrId + "&drrId2=" + drrId2, { responseType: 'text' });
    };
    ExemptionDeductionService.prototype.GetFinancialYear = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getfinancialYear);
    };
    ExemptionDeductionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ExemptionDeductionService);
    return ExemptionDeductionService;
}());



/***/ }),

/***/ "./src/app/services/income-tax/it-rates.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/income-tax/it-rates.service.ts ***!
  \*********************************************************/
/*! exports provided: ITRatesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ITRatesService", function() { return ITRatesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ITRatesService = /** @class */ (function () {
    function ITRatesService(http, config) {
        this.http = http;
        this.config = config;
    }
    ITRatesService.prototype.getItRates = function (finYear, rateType, rateFor) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetITRates?FinYear=" + finYear + "&RateType=" + rateType + "&RateFor=" + rateFor);
    };
    ITRatesService.prototype.upsertItRates = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/UpsertITRates", obj).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    ITRatesService.prototype.deleteItRates = function (id, ddoId) {
        return this.http.delete("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/DeleteITRates/" + id + "/" + ddoId);
    };
    ITRatesService.prototype.getFinancialYear = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getfinancialYear);
    };
    ITRatesService.prototype.getAllRateTypeAndRateFor = function (type) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetRateTypeAndRateFor/" + type);
    };
    ITRatesService.prototype.handleError = function (errorResponce) {
        if (errorResponce.error instanceof ErrorEvent) {
            console.log('client side error');
        }
        else {
            console.log('server side error');
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    };
    ITRatesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ITRatesService);
    return ITRatesService;
}());



/***/ }),

/***/ "./src/app/services/income-tax/other-dues.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/income-tax/other-dues.service.ts ***!
  \***********************************************************/
/*! exports provided: OtherDuesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherDuesService", function() { return OtherDuesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var OtherDuesService = /** @class */ (function () {
    //constructor() { }
    function OtherDuesService(http, config) {
        this.http = http;
        this.config = config;
    }
    OtherDuesService.prototype.GetOtherDues = function (allDues) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetOtherDues?allDues=" + allDues);
    };
    OtherDuesService.prototype.GetMajorSectionCode = function () {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetMajorSectionCodeForDues");
    };
    OtherDuesService.prototype.GetSectionCode = function (majorSecCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSectionCode?majorSecCode=" + majorSecCode);
    };
    OtherDuesService.prototype.GetSubSectionCode = function (majorSecCode, sectionCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSubSectionCode?majorSecCode=" + majorSecCode + "&sectionCode=" + sectionCode);
    };
    OtherDuesService.prototype.GetSectionCodeDescription = function (majorSectionCode, sectionCode, subSectionCode) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetSectionCodeDescription?majorSectionCode=" + majorSectionCode + "&sectionCode=" + sectionCode + "&subSectionCode=" + subSectionCode);
    };
    OtherDuesService.prototype.getOtherDuesDetails = function (pageNumber, pageSize, searchTerm) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetOtherDuesDetails?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm);
    };
    OtherDuesService.prototype.SaveOtherDuesDetails = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/SaveOtherDuesDetails", obj, { responseType: 'text' });
    };
    OtherDuesService.prototype.DeleteOtherDuesDetails = function (duesId, userName) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/DeleteOtherDuesDetails?duesId=" + duesId + "&userName=" + userName, { responseType: 'text' });
    };
    OtherDuesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], OtherDuesService);
    return OtherDuesService;
}());



/***/ }),

/***/ "./src/app/services/income-tax/standard-deduction.services.ts":
/*!********************************************************************!*\
  !*** ./src/app/services/income-tax/standard-deduction.services.ts ***!
  \********************************************************************/
/*! exports provided: StandardDeductionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StandardDeductionService", function() { return StandardDeductionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var StandardDeductionService = /** @class */ (function () {
    function StandardDeductionService(http, config) {
        this.http = http;
        this.config = config;
    }
    StandardDeductionService.prototype.getEntertainMaster = function () {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetStandardDeductionEntertainMaster");
    };
    StandardDeductionService.prototype.getStandardDeductionRule = function (pageNumber, pageSize, searchTerm) {
        return this.http.get("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/GetStandardDeductionRule?PageNumber=" + pageNumber + "&PageSize=" + pageSize + "&SearchTerm=" + searchTerm);
    };
    StandardDeductionService.prototype.upsertStandardDeductionRule = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/UpsertStandardDeductionRule", obj, { responseType: 'text' });
    };
    StandardDeductionService.prototype.deleteStandardDeductionRule = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/DeleteStandardDeductionRule", obj, { responseType: 'text' });
    };
    StandardDeductionService.prototype.deleteStandardDeductionRateDetail = function (id) {
        return this.http.delete("" + this.config.api_base_url + this.config.IncomeTaxControllerName + "/DeleteStandardDeductionRateDetail/" + id);
    };
    StandardDeductionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], StandardDeductionService);
    return StandardDeductionService;
}());



/***/ })

}]);
//# sourceMappingURL=income-tax-income-tax-module.js.map