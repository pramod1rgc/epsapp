(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bill-group-mgmt-bill-group-mgmt-module"],{

/***/ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-attach/bill-attach.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ml-10 {\r\n  margin-left: 15px !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlsbC1ncm91cC1tZ210L2JpbGwtYXR0YWNoL2JpbGwtYXR0YWNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2QkFBNkI7Q0FDOUIiLCJmaWxlIjoic3JjL2FwcC9iaWxsLWdyb3VwLW1nbXQvYmlsbC1hdHRhY2gvYmlsbC1hdHRhY2guY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tbC0xMCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.html":
/*!************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-attach/bill-attach.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n       \r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n\r\n\r\n    <ng-template matStepLabel>Pay Bill Group Attach</ng-template>\r\n\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12 combo-col clear-both select-type\">\r\n      <div class=\"col-md-6 col-lg-3 pading-16\">\r\n        <label>Select Pay Bill Group</label>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Pay Bill Group\" name=\"billgroupId\" #billgroupId=\"ngModel\"\r\n                      [(ngModel)]=\"billAttach.billgroupId\" (selectionChange)=\"GetPayBillGroupId($event.value)\">\r\n            <mat-option *ngFor=\"let Bill of getPayBillGroup\" [value]=\"Bill.billgrId\">\r\n              {{Bill.billgrId +'('+Bill.billgrDesc+')'}}\r\n\r\n\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"AccountHead\" [disabled]=disbleflag [(ngModel)]=\"billAttach.schemeId\"\r\n               [value]=\"billAttach.schemeId\" name=\"schemeId\" #schemeId=\"ngModel\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"SeriveType\" [disabled]=disbleflag [(ngModel)]=\"billAttach.serviceType\"\r\n               [value]=\"billAttach.serviceType\" name=\"serviceType\" #serviceType=\"ngModel\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"GroupId\" [(ngModel)]=\"billAttach.groupId\" [value]=\"billAttach.groupId\"\r\n               name=\"groupId\" #groupId=\"ngModel\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Pay BillGroup Name\" [disabled]=disbleflag [(ngModel)]=\"billAttach.billgrDesc\"\r\n               [value]=\"billAttach.billgrDesc\" name=\"billgrDesc\" #billgrDesc=\"ngModel\" class=\"width-fill\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Bill For GAR 15\" [disabled]=disbleflag [(ngModel)]=\"billAttach.billgrFor\"\r\n               [value]=\"billAttach.billgrFor\" name=\"billgrDesc\" #billgrFor=\"ngModel\" class=\"width-fill\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Employee Type\" [(ngModel)]=\"getEmpById.emp_type\" name=\"emp_type\" #emp_type=\"ngModel\"\r\n                    (selectionChange)=\"getEmployeeTypeId($event.value)\">\r\n          <mat-option *ngFor=\"let employeeType of employeeTypes\" [value]=\"employeeType.codeValue\">\r\n            {{employeeType.codeText}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <!-- <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Employee Type\" [formControl]=\"toppings\" multiple>\r\n            <mat-select-trigger>\r\n              {{toppings.value ? toppings.value[0] : ''}}\r\n              <span *ngIf=\"toppings.value?.length > 1\" class=\"example-additional-selection\">\r\n                (+{{toppings.value.length - 1}} {{toppings.value?.length === 2 ? 'other' : 'others'}})\r\n              </span>\r\n            </mat-select-trigger>\r\n            <mat-option *ngFor=\"let topping of toppingList\" [value]=\"topping\">{{topping}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n\r\n\r\n  </div> -->\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Employee Sub-Type\" [(ngModel)]=\"getEmpById.empSubType\" name=\"empSubType\"\r\n                    #empSubType=\"ngModel\">\r\n          <mat-option *ngFor=\"let employeeSubType of employeeSubTypes\" [value]=\"employeeSubType.codeValue\">\r\n            {{employeeSubType.codeText}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" (click)=\"saveMethod(billAttach)\" class=\"btn btn-success\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-12\">\r\n\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n    <div class=\"fom-title\">Employee not Attached to any Bill Group</div>\r\n    \r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"index\">\r\n        <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n      </ng-container>\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"empCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"empName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"desc\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desc}} </td>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"desigFieldDeptCD\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> DepttEmpCode </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desigFieldDeptCD}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                        [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n        </th>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null\"\r\n                        [checked]=\"selectionAttach.isSelected(row)\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns; let i = index\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasData\">\r\n      No Data Found\r\n    </div>\r\n\r\n\r\n  </div>\r\n\r\n  <div class=\"col-lg-12 text-center attach-dattach-wraper\">\r\n    <button mat-raised-button  class=\"attach-btn\" (click)=\"AttachSelectedRows()\">\r\n      Attach\r\n    </button>\r\n   \r\n\r\n    <button mat-raised-button  class=\"dattach-btn\" (click)=\"DeAttachSelectedRows()\">\r\n      DeAttach\r\n    </button>\r\n\r\n\r\n  </div>\r\n\r\n  \r\n  \r\n    <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n      <div class=\"fom-title\">Employee Attached to selected Bill Group</div>\r\n\r\n\r\n      <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"empCode\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Date Of Birth Column -->\r\n        <ng-container matColumnDef=\"empName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"desc\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desc}} </td>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"desigFieldDeptCD\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> DepttEmpCode </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desigFieldDeptCD}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Date Of Joining Column -->\r\n\r\n        <ng-container matColumnDef=\"select\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                          [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n          </th>\r\n          <mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                          [checked]=\"selectionDeAttach.isSelected(row)\">\r\n            </mat-checkbox>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n      <div [hidden]=\"isTableHasData\">\r\n        No Data Found\r\n      </div>\r\n\r\n\r\n\r\n    </div>\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-attach/bill-attach.component.ts ***!
  \**********************************************************************/
/*! exports provided: BillAttachComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillAttachComponent", function() { return BillAttachComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BillAttachComponent = /** @class */ (function () {
    function BillAttachComponent(_Service, fb, masterServices, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this.masterServices = masterServices;
        this.snackBar = snackBar;
        this.disbleflag = false;
        this.isTableHasData = true;
        this.getEmpById = {};
        // sfgdfgd
        this.displayedColumns = ['empCode', 'empName', 'desc', 'desigFieldDeptCD', 'select',];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.billAttach = {
            payBillGroupId: null,
            schemeId: null,
            serviceType: null,
            group: null,
            groupId: null,
            billGroupName: null,
            billForGAR: null,
            isActive: null,
            EmployeeType: null,
            EmployeeSubType: null,
            Designation: null,
            billgroupId: null,
            billgrDesc: null,
            billgrFor: null,
            empDOB: null
        };
        this.getPayBillGroup = [];
        // toppings = new FormControl();
        // toppingList: string[] = ['Regular', 'Co-terminus', 'Deputation In', 'Consultant', 'On-Contract', 'Member of Parliament'];
        this.EmployeeSybType = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.EmployeeSubTypeList = [''];
    }
    BillAttachComponent.prototype.ngOnInit = function () {
        this.disbleflag = true;
        this.getPayBillGroups('00003');
        this.getEmployeetype();
        this.getEmpAttachList();
    };
    BillAttachComponent.prototype.getEmpAttachList = function () {
        var _this = this;
        this._Service.getEmpAttachList().subscribe(function (res) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](res);
            _this.data = Object.assign(res);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.data);
        });
    };
    BillAttachComponent.prototype.getEmpDeAttachList = function (BillGrpId) {
        var _this = this;
        this._Service.getEmpdEAttachList(BillGrpId).subscribe(function (res) {
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](res);
            _this.dataDeAttach = Object.assign(res);
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
        });
    };
    BillAttachComponent.prototype.getEmployeetype = function () {
        var _this = this;
        this.masterServices.getEmployeeType().subscribe(function (res) {
            _this.employeeTypes = res;
        });
    };
    BillAttachComponent.prototype.getEmployeeSubType = function (parentId) {
        var _this = this;
        this.masterServices.getEmployeeSubType(parentId).subscribe(function (res) {
            _this.employeeSubTypes = res;
        });
    };
    BillAttachComponent.prototype.getEmployeeTypeId = function (value) {
        if (value === 'undefined' || null) {
        }
        this.getEmployeeSubType(value);
        this.getEmpById.employeeSubTypes = 0;
    };
    BillAttachComponent.prototype.GetPayBillGroupId = function (value) {
        if (value === 'undefined' || null) {
        }
        this.GetPayBillGroupDetailsById(value);
        this.getEmpDeAttachList(value);
    };
    BillAttachComponent.prototype.GetPayBillGroupDetailsById = function (parentId) {
        var _this = this;
        this._Service.GetPayBillGroupDetailsById(parentId).subscribe(function (res) {
            debugger;
            _this.billAttach = res;
            if (_this.billAttach.serviceType == "" && _this.billAttach.billForGAR == "") {
                _this.billAttach.serviceType = "No servie Type";
                _this.billAttach.billForGAR = "  this value is empty";
                _this.billAttach.billgroupId = parentId;
            }
            _this.billAttach.billgroupId = parentId;
            console.log(_this.billAttach);
        });
    };
    BillAttachComponent.prototype.getPayBillGroups = function (permDDOId) {
        var _this = this;
        this._Service.BindPayBillGroup(permDDOId).subscribe(function (data) {
            _this.getPayBillGroup = data;
            console.log(data[0].schemeCode);
        });
    };
    BillAttachComponent.prototype.isAllSelectedForDeAttach = function () {
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    BillAttachComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    BillAttachComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
    };
    BillAttachComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
    };
    BillAttachComponent.prototype.DeAttachSelectedRows = function () {
        var _this = this;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                _this.data.push(item);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        }
        else {
            alert("Please DeAttach at least one Employee ");
        }
    };
    BillAttachComponent.prototype.AttachSelectedRows = function () {
        var _this = this;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                _this.dataDeAttach.push(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        }
        else {
            alert("Please  Attach at least one Employee");
        }
    };
    BillAttachComponent.prototype.saveMethod = function (newBillGroup) {
        var _this = this;
        debugger;
        var DeAttach = this.dataSourceDeAttach;
        var Attach = this.dataSource;
        if (confirm("Are you sure to Save  ")) {
            var BillGrpId_1 = String(this.billAttach.billgroupId).trim();
            alert(BillGrpId_1);
            this.dataSource.data.forEach(function (element) {
                var EmployeeCode = element.empCode;
                _this._Service.UpDatePayBillGroupId(EmployeeCode, BillGrpId_1).subscribe(function (result) {
                    if (parseInt(result) >= 1) {
                        _this.snackBar.open('BillGroup Update Successfully', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('BillGroup not Successfully', null, { duration: 4000 });
                    }
                });
                //  this.objPostingDetailsService.SaveUpdatePostingDetails(this.objPostingDetails).subscribe(result => {
                //   console.log(result);
                //   // tslint:disable-next-line:radix
                //   if (parseInt(result) >= 1) {
                //    this.GetAllPostingDetails(this.comoonEmoCode.Empcode);
                //   this.resetPostingdetailsForm();
                //     this.snackBar.open('Save Successfully', null, { duration: 4000 });
                //   } else {
                //     this.snackBar.open('Save not Successfully', null, { duration: 4000 });
                //   }
                // });
            });
        }
    };
    BillAttachComponent.prototype.ResetForm = function () {
        this.form.resetForm();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], BillAttachComponent.prototype, "form", void 0);
    BillAttachComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bill-attach',
            template: __webpack_require__(/*! ./bill-attach.component.html */ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.html"),
            styles: [__webpack_require__(/*! ./bill-attach.component.css */ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__["MasterService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], BillAttachComponent);
    return BillAttachComponent;
}());

var ELEMENT_DeAttachdata = [];
var ELEMENT_DATA = [];


/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ml-10 {\r\n  margin-left: 15px !important;\r\n}\r\n.mar-pad-0 {  margin: 0;  padding: 0;}\r\n.checkbox > .mat-list-option { height: auto !important;}\r\n/*12-03-19*/\r\ntable[_ngcontent-c10] {\r\n  width: 100%;\r\n}\r\n.mat-elevation-z8[_ngcontent-c10] {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlsbC1ncm91cC1tZ210L2JpbGwtZ3JvdXAtY3JlYXRpb24vYmlsbC1ncm91cC1jcmVhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNkJBQTZCO0NBQzlCO0FBQ0QsY0FBYyxVQUFVLEVBQUUsV0FBVyxDQUFDO0FBQ3RDLCtCQUErQix3QkFBd0IsQ0FBQztBQUl4RCxZQUFZO0FBQ1o7RUFDRSxZQUFZO0NBQ2I7QUFDRDtFQUNFLG9HQUFvRztDQUNyRyIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9iaWxsLWdyb3VwLWNyZWF0aW9uL2JpbGwtZ3JvdXAtY3JlYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tbC0xMCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG4ubWFyLXBhZC0wIHsgIG1hcmdpbjogMDsgIHBhZGRpbmc6IDA7fVxyXG4uY2hlY2tib3ggPiAubWF0LWxpc3Qtb3B0aW9uIHsgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7fVxyXG5cclxuXHJcblxyXG4vKjEyLTAzLTE5Ki9cclxudGFibGVbX25nY29udGVudC1jMTBdIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4ubWF0LWVsZXZhdGlvbi16OFtfbmdjb250ZW50LWMxMF0ge1xyXG4gIGJveC1zaGFkb3c6IDAgMnB4IDFweCAtMXB4IHJnYmEoMCwwLDAsLjIpLCAwIDFweCAxcHggMCByZ2JhKDAsMCwwLC4xNCksIDAgMXB4IDNweCAwIHJnYmEoMCwwLDAsLjEyKTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Success!</strong> This alert box could indicate a successful or positive action.\r\n  </div>\r\n  <div class=\"alert alert-info alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Pay bill Group Creation\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <form [formGroup]=\"billGroupCreationForm\" #creationForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"saveBillGroup(billGroupCreation)\">\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Account Head *\" [(ngModel)]=\"billGroupCreation.accountHead\" name=\"accountHead\" formControlName=\"accountHead\">\r\n            <!--<mat-option value=\"option\">GPF</mat-option>\r\n            <mat-option value=\"option\">GPF</mat-option>-->\r\n            <mat-option *ngFor=\"let accountheads of getAccountHead\" [value]=\"accountheads.msPSchemeID\">{{accountheads.schemeCode}}</mat-option>\r\n\r\n          </mat-select>\r\n          <mat-error>required</mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Service Type *\" [(ngModel)]=\"billGroupCreation.serviceType\" name=\"serviceType\" formControlName=\"serviceType\" [disabled]=\"disableFlag\">\r\n            <mat-option *ngFor=\"let serviceTypes of serviceType\" [value]=\"serviceTypes.serviceTypeId\">{{serviceTypes.serviceText}}</mat-option>\r\n\r\n          </mat-select>\r\n          <mat-error>required</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--</div>-->\r\n      <!--<div class=\"row\">-->\r\n      <div class=\"col-md-12 col-lg-4 mat-form-field-wrapper combo-col-2\">\r\n        <!-- <button md-raised-button color=\"primary\" (click)=\"fileInput.click()\">Upload</button> -->\r\n\r\n        <div class=\"col-md-4 col-lg-2 pading-0\"><label>Group *</label></div>\r\n        <div class=\"col-md-8 col-lg-10 pading-0\">\r\n\r\n          <mat-selection-list class=\"checkbox mar-pad-0\" [(ngModel)]=\"billGroupCreation.group\" name=\"group\" formControlName=\"group\">\r\n            <mat-list-option *ngFor=\"let grops of group \" [value]=\"grops.id\" (click)=\"groupSelection(billGroupCreation);\" class=\"brd0\">\r\n              {{grops.name}}\r\n            </mat-list-option>\r\n          </mat-selection-list>\r\n\r\n          <!--<mat-error *ngIf=\"billGroupCreationForm.controls.group.errors && billGroupCreationForm.controls.group.errors.required\">\r\n            required\r\n          </mat-error>-->\r\n          <!--Options selected: {{grops.selectedOptions.selected.length}}-->\r\n\r\n\r\n        </div>\r\n\r\n        <!--<span class=\"help-block\" *ngIf=\"group.invalid && group.touched\">Phone number is required</span>-->\r\n      </div>\r\n      <span class=\"ml-10\">\r\n        <span *ngIf=\"checkBoxValidation\" class=\"txtred\">required</span>\r\n      </span>\r\n      <!--</div>-->\r\n      <!--<div class=\"row\">-->\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!--<textarea matInput placeholder=\"Name\" required></textarea>-->\r\n          <input matInput placeholder=\"Pay Bill Group Name *\" name=\"billGroupName\" (keypress)=\"charaterOnlyNoSpace($event)\" [(ngModel)]=\"billGroupCreation.billGroupName\" formControlName=\"billGroupName\" pattern=\"[-(),./a-zA-Z \\\\]*\">\r\n          <!--<textarea matInput placeholder=\"First Name\" required>-->\r\n          <mat-error *ngIf=\"billGroupCreationForm.controls.billGroupName.errors && billGroupCreationForm.controls.billGroupName.errors.required\">\r\n            required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"billGroupCreationForm.controls.billGroupName.errors && billGroupCreationForm.controls.billGroupName.errors.pattern\">\r\n            Invalid Text\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-6 col-lg-4 display-non\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <!--<textarea matInput placeholder=\"Name\" required></textarea>-->\r\n          <input matInput name=\"payBillGroupId\" [(ngModel)]=\"billGroupCreation.payBillGroupId\" formControlName=\"payBillGroupId\" [disabled]=\"disableFlag\">\r\n          <!--<textarea matInput placeholder=\"First Name\" required>-->\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <!--</div>-->\r\n      <!--<div class=\"row\">-->\r\n      <div class=\"col-md-6 col-lg-4 mat-form-field-wrapper combo-col-2\">\r\n        <!-- <button md-raised-button color=\"primary\" (click)=\"fileInput.click()\">Upload</button> -->\r\n\r\n        <div class=\"col-md-6 col-lg-4 pading-0\"><label>Bill for GAR 15</label></div>\r\n        <div class=\"col-md-6 col-lg-8 pading-0\">\r\n\r\n          <mat-radio-group [(ngModel)]=\"billGroupCreation.billForGAR\" name=\"billForGAR\" formControlName=\"billForGAR\" [disabled]=\"disableFlag\">\r\n            <mat-radio-button value=\"true\" class=\"ml-10\">Yes</mat-radio-button>\r\n            <mat-radio-button value=\"false\" class=\"ml-10\">No</mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n          <!--<label class=\"radio-inline\">\r\n            <input type=\"radio\" value=\"yes\" required #billForGAR=\"ngModel\" name=\"billForGAR\" [(ngModel)]=\"billGroupCreation.billForGAR\">\r\n            Yes\r\n          </label>\r\n          <label class=\"radio-inline\">\r\n            <input type=\"radio\" value=\"no\" required #billForGAR=\"ngModel\" name=\"billForGAR\" [(ngModel)]=\"billGroupCreation.billForGAR\">\r\n            No\r\n          </label>-->\r\n          <!--<mat-checkbox value=\"always\" class=\"ml-10\">B</mat-checkbox>\r\n          <mat-checkbox value=\"auto\" class=\"ml-10\">BN-Gz</mat-checkbox>\r\n          <mat-checkbox value=\"always\" class=\"ml-10\">C</mat-checkbox>\r\n          <mat-checkbox value=\"always\" class=\"ml-10\">D</mat-checkbox>-->\r\n          <!--</div>-->\r\n\r\n        </div>\r\n      </div>\r\n      <!--<div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Employee Type\"  (selectionChange)=\"getEmployeeTypeId($event.value)\">\r\n              <mat-option ></mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n      </div>-->\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"Submit\" id=\"btn_save\" class=\"btn btn-success\">{{_btnText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm()\">Cancel</button>\r\n      </div>\r\n\r\n    </form>\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n<!--Angular Generated Form Model : {{billGroupCreationForm.value | json}}-->\r\n<!--Our Employee Model : {{ billGroupCreationForm | json }}-->\r\n<!--<div class=\"col-md-12 col-lg-8\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons\">search</i>\r\n    </mat-form-field>\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8\">\r\n\r\n\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n        <td mat-cell *matCellDef=\"let element\">  </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"dob\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Date Of Birth </th>\r\n        <td mat-cell *matCellDef=\"let element\">  </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"doj\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Date Of Joining </th>\r\n        <td mat-cell *matCellDef=\"let element\">  </td>\r\n        <th mat-header-cell *matHeaderCellDef> Symbol </th>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a>\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Edit\">edit</a>\r\n          <a class=\"material-icons i-delet\" matTooltip=\"Delete\"> delete_forever </a>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>-->\r\n<!--right table-->\r\n\r\n\r\n<div class=\"col-md-12 col-lg-12\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Edit Details</div>\r\n    <mat-form-field>\r\n      <input matInput (keypress)=\"charaterOnlyNoSpace($event)\" (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\" pattern=\"^[A-Za-z]\\w*$\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <div class=\"tabel-wraper\">\r\n      <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n        <!-- commonDesigSrNo Column -->\r\n        <!--<ng-container matColumnDef=\"commonDesigSrNo\">\r\n          <th mat-header-cell *matHeaderCellDef> Designation Code </th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            {{element.commonDesigSrNo}}\r\n\r\n          </td>\r\n        </ng-container>-->\r\n        <!-- Designation Column -->\r\n        <ng-container matColumnDef=\"S.No\">\r\n          <th mat-header-cell *matHeaderCellDef> S.No </th>\r\n          f=\r\n          <td mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Group Column -->\r\n        <ng-container matColumnDef=\"billgrDesc\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay bill GroupId & Name </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.billgrDesc}}-{{ element.billgrId }} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"schemeCode\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Account Head </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.schemeCode}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Superannuation Column -->\r\n        <ng-container matColumnDef=\"serviceType\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Service Type</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.serviceType}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"groups\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Groups</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.groups}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Scheme\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Scheme</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.schemeId}} </td>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"Action\">\r\n          <th mat-header-cell *matHeaderCellDef> Action </th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <!-- <a href=\"#\">Edit</a> -->\r\n            <!--<a class=\"material-icons i-info\" matTooltip=\"Info\">error</a>-->\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.schemeId ,element.serviceTypeId,element.groups,element.billgrDesc, element.billForGAR, element.payBillGroupId)\">edit</a>\r\n            <!--<a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.commonDesigDesc,element.commonDesigGroupCD,element.commonDesigSrNo,element.commonDesigSuperannAge)\">edit</a>-->\r\n            <!--<a class=\"material-icons i-delet\" matTooltip=\"Delete\"> delete_forever </a>-->\r\n          </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"rowHighlight(row)\"\r\n            [style.background]=\"highlightedRows.indexOf(row) != -1 ? 'lightgreen' : ''\"></tr>\r\n\r\n      </table>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n    <div *ngIf=\"dataSource.data.length === 0 || dataSource.filteredData.length === 0\">No records found</div>\r\n  </div>\r\n</div>\r\n\r\n<!--right table End-->\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.ts ***!
  \**************************************************************************************/
/*! exports provided: BillGroupCreationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillGroupCreationComponent", function() { return BillGroupCreationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/promotions/promotions.service */ "./src/app/services/promotions/promotions.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//import { FormControl } from '@angular/forms';
//import { Observable } from 'rxjs';
var BillGroupCreationComponent = /** @class */ (function () {
    function BillGroupCreationComponent(_Service, fb, masterServices, promotionServices, _msg, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this.masterServices = masterServices;
        this.promotionServices = promotionServices;
        this._msg = _msg;
        this.snackBar = snackBar;
        this.is_btnStatus = true;
        this.displayedColumns = ['S.No', 'billgrDesc', 'schemeCode', 'serviceType', 'groups', 'Action'];
        this.highlightedRows = [];
        this.billGroupCreation = {
            payBillGroupId: null,
            schemeId: null,
            schemeCode: null,
            accountHead: null,
            serviceType: null,
            group: null,
            groupIds: null,
            billGroupName: null,
            billForGAR: "false",
            isActive: null,
            serviceTypeId: null,
            permDDOId: null
        };
        //abc: string = "true";
        // abc = "false";
        this.checked = false;
        this.group = [
            { id: 'A', name: 'A' },
            { id: 'B', name: 'B', checked: false },
            { id: 'BN-Gz', name: 'BN-Gz', checked: false },
            { id: 'C', name: 'C', checked: false },
            { id: 'D', name: 'D', checked: false }
        ];
        this.getAccountHead = [];
        this.editMode = false;
        this.serviceType = [];
        this.billGroup = [];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"]();
        this.tempGroup = [];
        this.allData = [];
        this.checkBoxValidation = false;
        this.createForm();
    }
    BillGroupCreationComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    BillGroupCreationComponent.prototype.ngOnInit = function () {
        debugger;
        this._btnText = 'Save';
        console.log(this.billGroupCreation.billForGAR);
        this.getAccountHeads('00003');
        this.getServiceType();
        this.BindAllBillGroup('00003');
        this.billGroupCreation.payBillGroupId = 0;
    };
    BillGroupCreationComponent.prototype.createForm = function () {
        this.billGroupCreationForm = this.fb.group({
            accountHead: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            serviceType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            group: [''],
            billGroupName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            billForGAR: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            payBillGroupId: ['', '']
        });
    };
    BillGroupCreationComponent.prototype.getAccountHeads = function (permDDOId) {
        var _this = this;
        this._Service.BindAccountHeads(permDDOId).subscribe(function (data) {
            _this.getAccountHead = data;
            console.log(data[0].schemeCode);
        });
    };
    BillGroupCreationComponent.prototype.getServiceType = function () {
        var _this = this;
        this.masterServices.getServiceType().subscribe(function (data) {
            _this.serviceType = data;
            console.log(data[0].serviceText);
        });
    };
    BillGroupCreationComponent.prototype.BindAllBillGroup = function (permDDOId) {
        var _this = this;
        this.flag = null;
        this._Service.BindPayBillGroup(permDDOId).subscribe(function (result) {
            _this.billGroup = result;
            _this.allData = result;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    BillGroupCreationComponent.prototype.saveBillGroup = function (newBillGroup) {
        var _this = this;
        debugger;
        if (newBillGroup.group != null && newBillGroup.group.length != 0) {
            newBillGroup.groupIds = '';
            for (var _i = 0, _a = newBillGroup.group; _i < _a.length; _i++) {
                var entry = _a[_i];
                if (newBillGroup.groupIds == '') {
                    newBillGroup.groupIds = entry;
                }
                else
                    newBillGroup.groupIds = newBillGroup.groupIds + "," + entry;
            }
            this.tempGroup = newBillGroup.group;
            newBillGroup.group = "";
            console.log(newBillGroup);
            newBillGroup.permDDOId = '00003';
            //newBillGroup.payBillGroupId = 0;
            if (this.billGroupCreationForm.valid) {
                this._Service.InsertUpdatepayBillGroup(newBillGroup).subscribe(function (res) {
                    if (res != "") {
                        newBillGroup.group = _this.tempGroup;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(res);
                    }
                    else {
                        if (_this.editMode) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()("Updated Successfully");
                        }
                        else {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()("Saved Successfully");
                        }
                        //swal("Saved Successfully");
                        _this.form.resetForm();
                        _this.ngOnInit();
                        _this.billGroupCreationForm.controls.billForGAR.setValue("false");
                        //this.billGroupCreation.billForGAR = "false";
                        //this.createForm();
                    }
                    //return false;
                    //this.snackBar.open(res, null, { duration: 4000 });
                    //this.BindAllBillGroup('00003');
                });
                //this.snackBar.open('Save successfully', null, { duration: 4000 });
                //newBillGroup.billGroupName = '';
                //document.getElementById('btn_save').innerHTML = 'Save';
                this._btnText = 'Save';
                this.disableFlag = false;
            }
            else {
                newBillGroup.group = this.tempGroup;
            }
        }
        else {
            this.checkBoxValidation = true;
        }
    };
    BillGroupCreationComponent.prototype.groupSelection = function (newBillGroup) {
        if (newBillGroup.group.length != 0) {
            this.checkBoxValidation = false;
        }
        //else {
        //  this.checkBoxValidation = true;
        //}
    };
    BillGroupCreationComponent.prototype.btnEditClick = function (schemeId, serviceType, group, billGroupName, billForGAR, payBillGroupId) {
        this.editMode = true;
        var billgroups = [];
        this.disableFlag = true;
        for (var _i = 0, _a = group.split(','); _i < _a.length; _i++) {
            var entry = _a[_i];
            billgroups.push(entry.trim());
        }
        this.billGroupCreation.accountHead = schemeId;
        this.billGroupCreation.serviceType = String(serviceType);
        this.billGroupCreation.group = billgroups;
        //this.groups = 'A';
        this.billGroupCreation.billGroupName = billGroupName.trim();
        this.billGroupCreation.billForGAR = String(billForGAR);
        this.billGroupCreation.payBillGroupId = payBillGroupId;
        //billGroupCreation.group='A'
        //this.billGroupCreation.billForGAR = billForGAR;
        //document.getElementById('btn_save').innerHTML = 'Update';
        this._btnText = 'Update';
    };
    BillGroupCreationComponent.prototype.rowHighlight = function (row) {
        this.highlightedRows.pop();
        this.highlightedRows.push(row);
    };
    BillGroupCreationComponent.prototype.resetForm = function () {
        this.disableFlag = false;
        var a = this.billGroupCreation.billForGAR;
        this.billGroupCreation.accountHead = null;
        this.billGroupCreation.serviceType = null;
        this.billGroupCreation.group = null;
        this.billGroupCreation.billGroupName = null;
        this.billGroupCreation.billForGAR = "false";
        this.highlightedRows.pop();
        this._btnText = 'Save';
        //this.form.resetForm();
        //if (a) {
        //  this.billGroupCreation.billForGAR = "false";
        //}
    };
    //data = [
    //  { label: 'one', checked: false },
    //  { label: 'two', checked: true },
    //  { label: 'three', checked: false },
    //  { label: 'four', checked: false },
    //  { label: 'five', checked: false }
    //];
    BillGroupCreationComponent.prototype.applyFilter = function (filterValue) {
        //this.dataSource.filter = filterValue.trim().toLowerCase();
        this.paginator.pageIndex = 0;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](this.allData.filter(function (a) { return a.billgrDesc.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.schemeCode.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.serviceType.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.groups.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.billgrId.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1; }));
        this.dataSource.paginator = this.paginator;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    BillGroupCreationComponent.prototype.charaterOnlyNoSpace = function (event) {
        var msg = this._msg.charaterOnlyNoSpace(event);
        return msg;
    };
    BillGroupCreationComponent.prototype.onChange = function (event, index, item) {
        item.checked = !item.checked;
        this.lastAction = 'index: ' + index + ', label: ' + item.label + ', checked: ' + item.checked;
        console.log(index, event, item);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], BillGroupCreationComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], BillGroupCreationComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], BillGroupCreationComponent.prototype, "form", void 0);
    BillGroupCreationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bill-group-creation',
            template: __webpack_require__(/*! ./bill-group-creation.component.html */ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.html"),
            styles: [__webpack_require__(/*! ./bill-group-creation.component.css */ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.css")],
            providers: [_services_master_master_service__WEBPACK_IMPORTED_MODULE_4__["MasterService"]]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__["MasterService"], _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_5__["PromotionsService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], BillGroupCreationComponent);
    return BillGroupCreationComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-group-mgmt-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-group-mgmt-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: BillGroupMgmtRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillGroupMgmtRoutingModule", function() { return BillGroupMgmtRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _bill_group_mgmt_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bill-group-mgmt.module */ "./src/app/bill-group-mgmt/bill-group-mgmt.module.ts");
/* harmony import */ var _bill_group_creation_bill_group_creation_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bill-group-creation/bill-group-creation.component */ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.ts");
/* harmony import */ var _bill_attach_bill_attach_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bill-attach/bill-attach.component */ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.ts");
/* harmony import */ var _paybillprocess_paybillprocess_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paybillprocess/paybillprocess.component */ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.ts");
/* harmony import */ var _mappingaccounthead_mappingaccounthead_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mappingaccounthead/mappingaccounthead.component */ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.ts");
/* harmony import */ var _mappingaccounthead_actdeactmapacoth1_actdeactmapacoth1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component */ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.ts");
/* harmony import */ var _dues_deductions_dues_deductions_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dues-deductions/dues-deductions.component */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.ts");
/* harmony import */ var _duesanddeductionsemployeewise_duesanddeductionsemployeewise_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./duesanddeductionsemployeewise/duesanddeductionsemployeewise.component */ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.ts");
/* harmony import */ var _temporarypbrprocess_temporarypbrprocess_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./temporarypbrprocess/temporarypbrprocess.component */ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.ts");
/* harmony import */ var _paybillgeneration_paybillgeneration_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./paybillgeneration/paybillgeneration.component */ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.ts");
/* harmony import */ var _unfreezeanddeletebill_unfreezeanddeletebill_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./unfreezeanddeletebill/unfreezeanddeletebill.component */ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.ts");
/* harmony import */ var _viewandforwardbill_viewandforwardbill_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./viewandforwardbill/viewandforwardbill.component */ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var routes = [
    {
        path: '', component: _bill_group_mgmt_module__WEBPACK_IMPORTED_MODULE_2__["BillGroupMgmtModule"], children: [
            { path: 'billgroupcreation', component: _bill_group_creation_bill_group_creation_component__WEBPACK_IMPORTED_MODULE_3__["BillGroupCreationComponent"] },
            { path: 'billattach', component: _bill_attach_bill_attach_component__WEBPACK_IMPORTED_MODULE_4__["BillAttachComponent"] },
            { path: 'payBillProcess', component: _paybillprocess_paybillprocess_component__WEBPACK_IMPORTED_MODULE_5__["PaybillprocessComponent"] },
            { path: 'ngrecovery', loadChildren: '../bill-group-mgmt/ng-recovery/ng-recovery.module#NgRecoveryModule' },
            { path: 'mappingaccountheadotherthan1', component: _mappingaccounthead_mappingaccounthead_component__WEBPACK_IMPORTED_MODULE_6__["MappingaccountheadComponent"] },
            { path: 'actdeactmapacoth1', component: _mappingaccounthead_actdeactmapacoth1_actdeactmapacoth1_component__WEBPACK_IMPORTED_MODULE_7__["Actdeactmapacoth1Component"] },
            { path: 'DuesDductions', component: _dues_deductions_dues_deductions_component__WEBPACK_IMPORTED_MODULE_8__["DuesDeductionsComponent"] },
            { path: 'DuesanddeductionsemployeewiseComponent', component: _duesanddeductionsemployeewise_duesanddeductionsemployeewise_component__WEBPACK_IMPORTED_MODULE_9__["DuesanddeductionsemployeewiseComponent"] },
            { path: 'temporarypbrprocessComponent', component: _temporarypbrprocess_temporarypbrprocess_component__WEBPACK_IMPORTED_MODULE_10__["TemporarypbrprocessComponent"] },
            { path: 'paybillgenerationComponent', component: _paybillgeneration_paybillgeneration_component__WEBPACK_IMPORTED_MODULE_11__["PaybillgenerationComponent"] },
            { path: 'unfreezeanddeletebillComponent', component: _unfreezeanddeletebill_unfreezeanddeletebill_component__WEBPACK_IMPORTED_MODULE_12__["UnfreezeanddeletebillComponent"] },
            { path: 'viewandforwardbillComponent', component: _viewandforwardbill_viewandforwardbill_component__WEBPACK_IMPORTED_MODULE_13__["ViewandforwardbillComponent"] }
        ]
    }
];
var BillGroupMgmtRoutingModule = /** @class */ (function () {
    function BillGroupMgmtRoutingModule() {
    }
    BillGroupMgmtRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BillGroupMgmtRoutingModule);
    return BillGroupMgmtRoutingModule;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/bill-group-mgmt.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/bill-group-mgmt/bill-group-mgmt.module.ts ***!
  \***********************************************************/
/*! exports provided: BillGroupMgmtModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillGroupMgmtModule", function() { return BillGroupMgmtModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _bill_group_mgmt_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bill-group-mgmt-routing.module */ "./src/app/bill-group-mgmt/bill-group-mgmt-routing.module.ts");
/* harmony import */ var _bill_group_creation_bill_group_creation_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bill-group-creation/bill-group-creation.component */ "./src/app/bill-group-mgmt/bill-group-creation/bill-group-creation.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _bill_attach_bill_attach_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bill-attach/bill-attach.component */ "./src/app/bill-group-mgmt/bill-attach/bill-attach.component.ts");
/* harmony import */ var _paybillprocess_paybillprocess_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./paybillprocess/paybillprocess.component */ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.ts");
/* harmony import */ var _mappingaccounthead_mappingaccounthead_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./mappingaccounthead/mappingaccounthead.component */ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.ts");
/* harmony import */ var _mappingaccounthead_actdeactmapacoth1_actdeactmapacoth1_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component */ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.ts");
/* harmony import */ var _dues_deductions_dues_deductions_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dues-deductions/dues-deductions.component */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _dues_deductions_dues_deductions_header_dues_deductions_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dues-deductions/dues-deductions-header/dues-deductions-header.component */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _duesanddeductionsemployeewise_duesanddeductionsemployeewise_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./duesanddeductionsemployeewise/duesanddeductionsemployeewise.component */ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.ts");
/* harmony import */ var _temporarypbrprocess_temporarypbrprocess_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./temporarypbrprocess/temporarypbrprocess.component */ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.ts");
/* harmony import */ var _paybillgeneration_paybillgeneration_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./paybillgeneration/paybillgeneration.component */ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.ts");
/* harmony import */ var _unfreezeanddeletebill_unfreezeanddeletebill_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./unfreezeanddeletebill/unfreezeanddeletebill.component */ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.ts");
/* harmony import */ var _viewandforwardbill_viewandforwardbill_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./viewandforwardbill/viewandforwardbill.component */ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var BillGroupMgmtModule = /** @class */ (function () {
    function BillGroupMgmtModule() {
    }
    BillGroupMgmtModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_bill_group_creation_bill_group_creation_component__WEBPACK_IMPORTED_MODULE_4__["BillGroupCreationComponent"], _bill_attach_bill_attach_component__WEBPACK_IMPORTED_MODULE_6__["BillAttachComponent"], _paybillprocess_paybillprocess_component__WEBPACK_IMPORTED_MODULE_7__["PaybillprocessComponent"], _mappingaccounthead_mappingaccounthead_component__WEBPACK_IMPORTED_MODULE_8__["MappingaccountheadComponent"], _mappingaccounthead_actdeactmapacoth1_actdeactmapacoth1_component__WEBPACK_IMPORTED_MODULE_9__["Actdeactmapacoth1Component"], _dues_deductions_dues_deductions_component__WEBPACK_IMPORTED_MODULE_10__["DuesDeductionsComponent"], _dues_deductions_dues_deductions_header_dues_deductions_header_component__WEBPACK_IMPORTED_MODULE_12__["DuesDeductionsHeaderComponent"], _duesanddeductionsemployeewise_duesanddeductionsemployeewise_component__WEBPACK_IMPORTED_MODULE_14__["DuesanddeductionsemployeewiseComponent"], _temporarypbrprocess_temporarypbrprocess_component__WEBPACK_IMPORTED_MODULE_15__["TemporarypbrprocessComponent"], _paybillgeneration_paybillgeneration_component__WEBPACK_IMPORTED_MODULE_16__["PaybillgenerationComponent"], _unfreezeanddeletebill_unfreezeanddeletebill_component__WEBPACK_IMPORTED_MODULE_17__["UnfreezeanddeletebillComponent"], _viewandforwardbill_viewandforwardbill_component__WEBPACK_IMPORTED_MODULE_18__["ViewandforwardbillComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _bill_group_mgmt_routing_module__WEBPACK_IMPORTED_MODULE_3__["BillGroupMgmtRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__["SharedModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_13__["NgxMatSelectSearchModule"]
            ],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_19__["CommonMsg"]]
        })
    ], BillGroupMgmtModule);
    return BillGroupMgmtModule;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.css":
/*!*************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9kdWVzLWRlZHVjdGlvbnMvZHVlcy1kZWR1Y3Rpb25zLWhlYWRlci9kdWVzLWRlZHVjdGlvbnMtaGVhZGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.html":
/*!**************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-10 col-lg-10 col-md-offset-1\">\r\n  <div class=\"basic-container select-drop-head\">\r\n    <form [formGroup]=\"headersForm\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"row  selection-hed\">\r\n        <div class=\"col-md-3 col-lg-3\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"financial_year\" (selectionChange)=\"getPayBillGroups()\" placeholder=\"Financial Year\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"finYearFilterCtrl\" [placeholderLabel]=\"'Find Financial Year...'\" [noEntriesFoundLabel]=\"'Financial Year is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option>Select</mat-option>\r\n              <mat-option *ngFor=\"let fin of filteredFinYear | async\" value=\"{{fin.financialYear}}\">{{fin.fullDescription}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please Select Financial Year</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-3 col-lg-3\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"pay_bill_group\" (selectionChange)=\"getDesignations()\" placeholder=\"Pay Bill Group\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"billFilterCtrl\" [placeholderLabel]=\"'Find Bill group...'\" [noEntriesFoundLabel]=\"'Pay Bill Group is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option>Select</mat-option>\r\n              <mat-option *ngFor=\"let Bill of filteredBill | async\" value=\"{{Bill.payBillGroupId}}\">{{Bill.billGrDesc}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please Select Pay Bill Group</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-3 col-lg-3\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"designation\" placeholder=\"Designation\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"desigFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option>Select</mat-option>\r\n              <mat-option *ngFor=\"let Desig of filteredDesig | async\" value=\"{{Desig.desigId}}\">{{Desig.desigId}} - {{Desig.desigDesc}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please Select Designation</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-3 col-lg-3\">\r\n          <button class=\"btn btn-success mt-10\" type=\"submit\">Go</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: DuesDeductionsHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DuesDeductionsHeaderComponent", function() { return DuesDeductionsHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _dues_deductions_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../dues-deductions.component */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DuesDeductionsHeaderComponent = /** @class */ (function () {
    function DuesDeductionsHeaderComponent(_service, duesComponent) {
        this._service = _service;
        this.duesComponent = duesComponent;
        this.allDesignation = [];
        this.allFinYear = [];
        this.billFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.desigFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.finYearFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.filteredBill = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredDesig = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredFinYear = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.controllerId = sessionStorage.getItem('controllerID');
        this.ddoId = sessionStorage.getItem('ddoid');
    }
    DuesDeductionsHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.billFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterBill();
        });
        this.desigFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesig();
        });
        this.finYearFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterFinYear();
        });
        this.getAllFinYears();
        //if (this.router.url === '/dashboard/promotion/reversions')
        //  this.selectedRadio = '2';
        //else
        //  this.selectedRadio = '1';
        this.headerDataForms();
    };
    DuesDeductionsHeaderComponent.prototype.filterBill = function () {
        if (!this.BillGroup) {
            return;
        }
        // get the search keyword
        var search = this.billFilterCtrl.value;
        if (!search) {
            this.filteredBill.next(this.BillGroup.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBill.next(this.BillGroup.filter(function (Bill) { return Bill.billGrDesc.toLowerCase().indexOf(search) > -1; }));
    };
    DuesDeductionsHeaderComponent.prototype.filterDesig = function () {
        if (!this.allDesignation) {
            return;
        }
        // get the search keyword
        var search = this.desigFilterCtrl.value;
        if (!search) {
            this.filteredDesig.next(this.allDesignation.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredDesig.next(this.allDesignation.filter(function (desig) { return desig.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    DuesDeductionsHeaderComponent.prototype.filterFinYear = function () {
        if (!this.allFinYear) {
            return;
        }
        // get the search keyword
        var search = this.finYearFilterCtrl.value;
        if (!search) {
            this.filteredFinYear.next(this.allFinYear.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredFinYear.next(this.allFinYear.filter(function (fin) { return fin.fullDescription.toLowerCase().indexOf(search) > -1; }));
    };
    DuesDeductionsHeaderComponent.prototype.getAllFinYears = function () {
        var _this = this;
        this._service.GetFinancialYear().subscribe(function (response) {
            _this.allFinYear = response;
            _this.filteredFinYear.next(_this.allFinYear);
        });
    };
    DuesDeductionsHeaderComponent.prototype.headerDataForms = function () {
        this.headersForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            financial_year: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            pay_bill_group: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            designation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
        });
    };
    DuesDeductionsHeaderComponent.prototype.getPayBillGroups = function () {
        var _this = this;
        var finYearTo = +this.headersForm.controls.financial_year.value;
        var finYearFrom = +finYearTo - 1;
        this._service.GetPayBillGroupByFinancialYear(finYearFrom.toString(), finYearTo.toString(), "00003").subscribe(function (response) {
            _this.BillGroup = response;
            _this.filteredBill.next(_this.BillGroup);
        });
    };
    DuesDeductionsHeaderComponent.prototype.getDesignations = function () {
        var _this = this;
        this._service.GetDesignationByBillgrID(this.headersForm.controls.pay_bill_group.value, "00003").subscribe(function (response) {
            _this.allDesignation = response;
            _this.filteredDesig.next(_this.allDesignation);
        });
    };
    DuesDeductionsHeaderComponent.prototype.onSubmit = function () {
        var _this = this;
        this._service.GetPayItemsFromDesignationCode(this.headersForm.controls.designation.value).subscribe(function (response) {
            _this.duesComponent.allPayItems = response;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], DuesDeductionsHeaderComponent.prototype, "formGroupDirective", void 0);
    DuesDeductionsHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dues-deductions-header',
            template: __webpack_require__(/*! ./dues-deductions-header.component.html */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.html"),
            styles: [__webpack_require__(/*! ./dues-deductions-header.component.css */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions-header/dues-deductions-header.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_4__["PayBillGroupService"], _dues_deductions_component__WEBPACK_IMPORTED_MODULE_5__["DuesDeductionsComponent"]])
    ], DuesDeductionsHeaderComponent);
    return DuesDeductionsHeaderComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9kdWVzLWRlZHVjdGlvbnMvZHVlcy1kZWR1Y3Rpb25zLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-dues-deductions-header></app-dues-deductions-header>\r\n<mat-accordion class=\"col-md-12\">\r\n\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Dues & Deduction Details Bulk Employees\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <form [formGroup]=\"duesDeductForm\">\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Category\" formControlName=\"category\" (selectionChange)=\"setPayItems()\">\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option value=\"dues\">Non Computational Dues</mat-option>\r\n            <mat-option value=\"deduction\">Non- Computational Deductions</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Pay-Item\" formControlName=\"payItem\">\r\n            <mat-option value=\"\">Select</mat-option>\r\n            <mat-option *ngFor=\"let item of payItems\" value=\"{{item.payItemsCd}}\">{{item.payItemsName}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n        <button type=\"submit\" class=\"btn btn-success mt-10\" (click)=\"goButton()\">Go</button>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 pading-0\">\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount\" formControlName=\"amount\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4 pt-10\">\r\n          <mat-checkbox (change)=\"setAmount($event)\">Applicable to all Employees</mat-checkbox>\r\n        </div>\r\n      </div>\r\n\r\n\r\n      <div *ngIf=\"showTable\">\r\n        <!--table-->\r\n        <div class=\"fom-title mt-40\">Dues</div>\r\n        <!-- Search -->\r\n        <mat-form-field>\r\n          <input matInput placeholder=\"Search\" (keyup)=\"applyFilter($event.target.value)\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n\r\n        <div class=\"example-card mat-card\">\r\n          <ng-container formArrayName=\"empDetails\">\r\n            <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"totalCount > 0\">\r\n\r\n              <!-- Checkbox Column -->\r\n              <ng-container matColumnDef=\"select\">\r\n                <th mat-header-cell *matHeaderCellDef>\r\n                  <mat-checkbox (change)=\"selectAll($event)\"></mat-checkbox>\r\n                  <!--<mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                                [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                                [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n                  </mat-checkbox>-->\r\n                </th>\r\n                <td mat-cell *matCellDef=\"let element; let i = index\" [formGroupName]=\"i\">\r\n                  <mat-checkbox [checked]=\"element.isSelected\" formControlName=\"isSelected\"></mat-checkbox>\r\n                  <!--<mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                                (change)=\"$event ? selection.toggle(row) : null\"\r\n                                [checked]=\"selection.isSelected(row)\">\r\n                  </mat-checkbox>-->\r\n                </td>\r\n              </ng-container>\r\n\r\n              <!-- Position Column -->\r\n              <ng-container matColumnDef=\"position\">\r\n                <th mat-header-cell *matHeaderCellDef> No. </th>\r\n                <td mat-cell *matCellDef=\"let element; let i = index\"> {{i + 1}} </td>\r\n              </ng-container>\r\n\r\n              <!-- Name Column -->\r\n              <ng-container matColumnDef=\"name\">\r\n                <th mat-header-cell *matHeaderCellDef> Employee Name </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n              </ng-container>\r\n\r\n              <!-- Weight Column -->\r\n              <ng-container matColumnDef=\"code\">\r\n                <th mat-header-cell *matHeaderCellDef> Employee Code </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n\r\n              <!-- Symbol Column -->\r\n              <ng-container matColumnDef=\"symbol\">\r\n                <th mat-header-cell *matHeaderCellDef> Amount </th>\r\n                <td mat-cell *matCellDef=\"let element; let i = index\" [formGroupName]=\"i\"><input type=\"text\" formControlName=\"amount\"> </td>\r\n              </ng-container>\r\n\r\n\r\n\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n          </ng-container>\r\n          <mat-paginator [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" (click)=\"onSubmit()\" class=\"btn btn-success\" [disabled]=\"totalCount == 0\">Save</button>\r\n        <button type=\"button\" (click)=\"cancelForm()\" class=\"btn btn-warning\" [disabled]=\"totalCount == 0\">Cancel</button>\r\n      </div>\r\n\r\n    </form>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.ts ***!
  \******************************************************************************/
/*! exports provided: DuesDeductionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DuesDeductionsComponent", function() { return DuesDeductionsComponent; });
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//const ELEMENT_DATA: PeriodicElement[] = [
//  { position: 1, name: 'Hydrogen', code: 1.0079, amount: 'H', isSelected: false },
//  { position: 2, name: 'Helium', code: 4.0026, amount: 'He', isSelected: false },
//  { position: 3, name: 'Lithium', code: 6.941, amount: 'Li', isSelected: false },
//  { position: 4, name: 'Beryllium', code: 9.0122, amount: 'Be', isSelected: false },
//  { position: 5, name: 'Boron', code: 10.811, amount: 'B', isSelected: false },
//  { position: 6, name: 'Carbon', code: 12.0107, amount: 'C', isSelected: false },
//  { position: 7, name: 'Nitrogen', code: 14.0067, amount: 'N', isSelected: false },
//  { position: 8, name: 'Oxygen', code: 15.9994, amount: 'O', isSelected: false },
//  { position: 9, name: 'Fluorine', code: 18.9984, amount: 'F', isSelected: false },
//  { position: 10, name: 'Neon', code: 20.1797, amount: 'Ne', isSelected: false },
//];
/**
 * @title Table with selection
 */
var DuesDeductionsComponent = /** @class */ (function () {
    function DuesDeductionsComponent(formBuilder, _service, snackBar, _msg) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this.snackBar = snackBar;
        this._msg = _msg;
        this.displayedColumns = ['select', 'position', 'name', 'code', 'symbol'];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_0__["MatTableDataSource"]();
        //selection = new SelectionModel<PeriodicElement>(true, []);
        this.totalCount = 0;
        this.allPayItems = [];
        this.payItems = [];
        this.showTable = false;
        this.ddoId = sessionStorage.getItem('ddoid');
    }
    DuesDeductionsComponent.prototype.createForm = function () {
        this.duesDeductForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            category: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            payItem: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.ddoId),
            empDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormArray"]([])
        });
        this.addEmployeeDetail();
    };
    DuesDeductionsComponent.prototype.createEmployeeForm = function () {
        return this.formBuilder.group({
            sNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.employeeDetails.value.length + 1),
            msEmpDuesId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            payItemCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            empId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            empName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            isSelected: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](false)
        });
    };
    DuesDeductionsComponent.prototype.addEmployeeDetail = function () {
        this.employeeDetails.push(this.createEmployeeForm());
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_0__["MatTableDataSource"](this.employeeDetails.value);
    };
    Object.defineProperty(DuesDeductionsComponent.prototype, "employeeDetails", {
        get: function () {
            return this.duesDeductForm.get("empDetails");
        },
        enumerable: true,
        configurable: true
    });
    DuesDeductionsComponent.prototype.setPayItems = function () {
        if (this.duesDeductForm.controls.category.value == 'dues') {
            this.payItems = this.allPayItems.filter(function (a) { return a.isDeus_Deduct == 77; });
        }
        else {
            this.payItems = this.allPayItems.filter(function (a) { return a.isDeus_Deduct == 78; });
        }
    };
    DuesDeductionsComponent.prototype.getEmployees = function () {
        var _this = this;
        while (this.employeeDetails.length > 0) {
            this.employeeDetails.removeAt(0);
        }
        this._service.GetEmployeesByPayItemCode(this.duesDeductForm.controls.payItem.value).subscribe(function (response) {
            for (var i = 0; i < response.length; i++) {
                _this.employeeDetails.push(_this.formBuilder.group({
                    sNo: _this.employeeDetails.value.length + 1,
                    msEmpDuesId: response[i].msEmpDuesId,
                    empCd: response[i].empCd,
                    payItemCd: response[i].payItemsCd,
                    empId: response[i].empId,
                    empName: response[i].empName,
                    amount: response[i].amount,
                    isSelected: false
                }));
            }
            //response.forEach(emp => this.employeeDetails.push(this.formBuilder.group(emp)));
            _this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_0__["MatTableDataSource"](_this.employeeDetails.value);
            _this.dataSource.paginator = _this.paginator;
            _this.totalCount = _this.dataSource.data.length;
        });
    };
    DuesDeductionsComponent.prototype.selectAll = function (event) {
        this.employeeDetails.controls.forEach(function (el) { return el.get("isSelected").setValue(event.checked); });
        //this.dataSource = new MatTableDataSource(this.employeeDetails.value);
        //this.dataSource.paginator = this.paginator;
    };
    ///** Whether the number of selected elements matches the total number of rows. */
    //isAllSelected() {
    //  const numSelected = this.selection.selected.length;
    //  const numRows = this.dataSource.data.length;
    //  return numSelected === numRows;
    //}
    ///** Selects all rows if they are not all selected; otherwise clear selection. */
    //masterToggle() {
    //  this.isAllSelected() ?
    //    this.selection.clear() :
    //    this.dataSource.data.forEach(row => this.selection.select(row));
    //}
    DuesDeductionsComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.employeeDetails.value.filter(function (a) { return a.isSelected == true; }).length > 0) {
            var obj = { DDOId: this.ddoId, EmpDetails: this.employeeDetails.value.filter(function (a) { return a.isSelected == true; }) };
            this._service.UpdateEmpNonComputationalDuesDeductAmount(obj).subscribe(function (response) {
                while (_this.employeeDetails.length > 0) {
                    _this.employeeDetails.removeAt(0);
                }
                _this.createForm();
                _this.snackBar.open(_this._msg.saveMsg, null, { duration: 4000 });
                _this.showTable = false;
            });
        }
        else {
            this.snackBar.open("Please Select a Employee.", null, { duration: 4000 });
        }
    };
    DuesDeductionsComponent.prototype.cancelForm = function () {
        while (this.employeeDetails.length > 0) {
            this.employeeDetails.removeAt(0);
        }
        this.createForm();
        this.showTable = false;
    };
    DuesDeductionsComponent.prototype.goButton = function () {
        this.getEmployees();
        this.showTable = true;
    };
    DuesDeductionsComponent.prototype.setAmount = function (event) {
        var _this = this;
        if (event.checked) {
            this.employeeDetails.controls.forEach(function (el) { return el.get("amount").setValue(_this.duesDeductForm.controls.amount.value); });
        }
        else {
            this.employeeDetails.controls.forEach(function (el) { return el.get("amount").setValue(''); });
        }
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_0__["MatTableDataSource"](this.employeeDetails.value);
        this.dataSource.paginator = this.paginator;
    };
    DuesDeductionsComponent.prototype.applyFilter = function (searchTerm) {
        this.dataSource.filter = searchTerm.trim();
        this.paginator.firstPage();
        this.totalCount = this.dataSource.filteredData.length;
    };
    DuesDeductionsComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.createForm();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"], {}),
        __metadata("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], DuesDeductionsComponent.prototype, "paginator", void 0);
    DuesDeductionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-dues-deductions',
            template: __webpack_require__(/*! ./dues-deductions.component.html */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.html"),
            styles: [__webpack_require__(/*! ./dues-deductions.component.css */ "./src/app/bill-group-mgmt/dues-deductions/dues-deductions.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_4__["PayBillGroupService"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]])
    ], DuesDeductionsComponent);
    return DuesDeductionsComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.css":
/*!***********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".selection-border {\r\n  border: 1px solid #d2cccc;\r\n  padding: 1px;\r\n}\r\n.textaria {\r\n  height: 24px;\r\n  width: 100%;\r\n  max-height:60px;\r\n  max-width:300px;\r\n}\r\n.lbl-input {\r\n  margin-right: 10px;\r\n  font-weight: bold !important;\r\n}\r\n.select-wid {\r\n  width: 182px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlsbC1ncm91cC1tZ210L2R1ZXNhbmRkZWR1Y3Rpb25zZW1wbG95ZWV3aXNlL2R1ZXNhbmRkZWR1Y3Rpb25zZW1wbG95ZWV3aXNlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwwQkFBMEI7RUFDMUIsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixnQkFBZ0I7Q0FDakI7QUFDRDtFQUNFLG1CQUFtQjtFQUNuQiw2QkFBNkI7Q0FDOUI7QUFDRDtFQUNFLGFBQWE7Q0FDZCIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9kdWVzYW5kZGVkdWN0aW9uc2VtcGxveWVld2lzZS9kdWVzYW5kZGVkdWN0aW9uc2VtcGxveWVld2lzZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlbGVjdGlvbi1ib3JkZXIge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkMmNjY2M7XHJcbiAgcGFkZGluZzogMXB4O1xyXG59XHJcbi50ZXh0YXJpYSB7XHJcbiAgaGVpZ2h0OiAyNHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6NjBweDtcclxuICBtYXgtd2lkdGg6MzAwcHg7XHJcbn1cclxuLmxibC1pbnB1dCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbn1cclxuLnNlbGVjdC13aWQge1xyXG4gIHdpZHRoOiAxODJweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12 mb-30\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Dues & Deduction Details Employee-wise</div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Branch\" disabled>\r\n          <mat-option>Select</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Year\" disabled>\r\n          <mat-option>2018</mat-option>\r\n          <mat-option>2019</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Month \" disabled>\r\n          <mat-option>1</mat-option>\r\n          <mat-option>2</mat-option>\r\n          <mat-option>3</mat-option>\r\n          <mat-option>4</mat-option>\r\n          <mat-option>5</mat-option>\r\n          <mat-option>6</mat-option>\r\n          <mat-option>7</mat-option>\r\n          <mat-option>8</mat-option>\r\n          <mat-option>9</mat-option>\r\n          <mat-option>10</mat-option>\r\n          <mat-option>11</mat-option>\r\n          <mat-option>12</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Pay Commission\" disabled>\r\n          <mat-option>4th CPC</mat-option>\r\n          <mat-option>5th CPC</mat-option>\r\n          <mat-option>6th CPC</mat-option>\r\n          <mat-option>7th CPC</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Pay Level\" disabled>\r\n          <mat-option>1</mat-option>\r\n          <mat-option>2</mat-option>\r\n          <mat-option>3</mat-option>\r\n          <mat-option>4</mat-option>\r\n          <mat-option>5</mat-option>\r\n          <mat-option>6</mat-option>\r\n          <mat-option>7</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Pay Index\" disabled>\r\n          <mat-option>1</mat-option>\r\n          <mat-option>2</mat-option>\r\n          <mat-option>3</mat-option>\r\n          <mat-option>4</mat-option>\r\n          <mat-option>5</mat-option>\r\n          <mat-option>6</mat-option>\r\n          <mat-option>7</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Basic Pay\" disabled>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn\">View Draft Salary</button>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n<!----------------------Dues tab Start-------------------------->\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Dues\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <mat-tab-group>\r\n      <mat-tab label=\"Computational Dues\">\r\n        <div class=\"mat-elevation-z8\">\r\n          <table class=\"table even-odd-color\">\r\n            <thead>\r\n              <tr>\r\n                <th>Description of Computational Dues</th>\r\n                <th>Rate</th>\r\n                <th>Amount</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr>\r\n                <td>DA</td>\r\n                <td><input type=\"text\" disabled></td>\r\n                <td><input type=\"text\" disabled></td>\r\n              </tr>\r\n              <tr>\r\n                <td>HRA</td>\r\n                <td><input type=\"text\" disabled></td>\r\n                <td><input type=\"text\" disabled></td>\r\n              </tr>\r\n              <tr>\r\n                <td>TPTA</td>\r\n                <td><input type=\"text\" disabled></td>\r\n                <td><input type=\"text\" disabled></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"Non-Computational Dues\">\r\n        <div class=\"mat-elevation-z8\">\r\n          <table mat-table [dataSource]=\"dataSource\" class=\"even-odd-color\">\r\n            <!-- Name Column -->\r\n            <ng-container matColumnDef=\"name\">\r\n              <th mat-header-cell *matHeaderCellDef>Description of <br />Non- Computational Dues </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n            </ng-container>\r\n            <!-- Weight Column -->\r\n            <ng-container matColumnDef=\"weight\">\r\n              <th mat-header-cell *matHeaderCellDef> Rate </th>\r\n              <td mat-cell *matCellDef=\"let element\">  <input type=\"text\"> </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"remark\">\r\n              <th mat-header-cell *matHeaderCellDef> remark </th>\r\n              <td mat-cell *matCellDef=\"let element\">  <input type=\"text\"> </td>\r\n            </ng-container>\r\n            <!-- Symbol Column -->\r\n            <ng-container matColumnDef=\"symbol\">\r\n              <th mat-header-cell *matHeaderCellDef> Amount </th>\r\n              <td mat-cell *matCellDef=\"let element\">  <input type=\"text\"></td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"periodicity\">\r\n              <th mat-header-cell *matHeaderCellDef> Periodicity </th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\">\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>Monthly</mat-option>\r\n                  <mat-option>Quarterley</mat-option>\r\n                  <mat-option>Half Yearly</mat-option>\r\n                  <mat-option>Annually</mat-option>\r\n                </mat-select>\r\n              </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"month\">\r\n              <th mat-header-cell *matHeaderCellDef> Month/Months </th>\r\n              <td mat-cell *matCellDef=\"let element\">  <input type=\"text\"></td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<!----------------------Dues tab End-------------------------->\r\n<!----------------------Deductions tab Start-------------------------->\r\n<mat-accordion class=\"col-md-12 mt-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Deductions\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <mat-tab-group>\r\n      <mat-tab label=\"Computational Deductions\">\r\n        <table class=\"table even-odd-color\">\r\n          <thead>\r\n            <tr>\r\n              <th>Description of Deductions</th>\r\n              <th>Amount</th>\r\n              <th>PAO Code</th>\r\n              <th>Remarks</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr>\r\n              <td>CGHS</td>\r\n              <td><input type=\"text\" disabled></td>\r\n              <td>\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\" disabled>\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                </mat-select>\r\n              </td>\r\n              <td>\r\n                <textarea class=\"textaria\" disabled></textarea>\r\n              </td>\r\n            </tr>\r\n\r\n            <tr>\r\n              <td>CGEGIS</td>\r\n              <td><input type=\"text\" disabled></td>\r\n              <td>\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\" disabled>\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                </mat-select>\r\n              </td>\r\n              <td>\r\n                <textarea class=\"textaria\" disabled></textarea>\r\n              </td>\r\n            </tr>\r\n\r\n            <tr>\r\n              <td>NPS</td>\r\n              <td><input type=\"text\" disabled></td>\r\n              <td>\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\" disabled>\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                </mat-select>\r\n              </td>\r\n              <td>\r\n                <textarea class=\"textaria\" disabled></textarea>\r\n              </td>\r\n            </tr>\r\n\r\n          </tbody>\r\n        </table>\r\n      </mat-tab>\r\n      <mat-tab label=\"Non- Computational Deduction\">\r\n        <table class=\"table even-odd-color\">\r\n          <thead>\r\n            <tr>\r\n              <th>Description of Deductions</th>\r\n              <th>Amount</th>\r\n              <th>PAO Code/ PF Agency</th>\r\n            </tr>\r\n          </thead>\r\n\r\n          <tbody>\r\n            <tr>\r\n              <td>GPF</td>\r\n              <td><input type=\"text\"></td>\r\n              <td>\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\">\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                </mat-select>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </mat-tab>\r\n      <mat-tab label=\"Dedcution-Loans\">\r\n        <table class=\"table even-odd-color\">\r\n          <thead>\r\n            <tr>\r\n              <th>Description of Deductions</th>\r\n              <th>Sanctioned Amount</th>\r\n              <th>Total No. of Installments</th>\r\n              <th>Current Month Recovery Amount</th>\r\n              <th>No of Installments completed</th>\r\n              <th>Amount Recovered so far</th>\r\n              <th>PAO Code</th>\r\n              <th>Recovery start month</th>\r\n            </tr>\r\n          </thead>\r\n\r\n          <tbody>\r\n            <tr>\r\n              <td>Computer Advance</td>\r\n              <td><input type=\"text\"></td>\r\n              <td><input type=\"text\"></td>\r\n              <td><input type=\"text\"></td>\r\n              <td><input type=\"text\"></td>\r\n              <td><input type=\"text\"></td>\r\n              <td>\r\n                <mat-select placeholder=\"Select\" class=\"selection-border select-wid\">\r\n                  <mat-option>Select</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                  <mat-option>033195</mat-option>\r\n                </mat-select>\r\n              </td>\r\n              <td><input type=\"text\"></td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<!----------------------Deductions tab End-------------------------->\r\n<!----------------------Salary Amount Part Payment  in currencies tab Start-------------------------->\r\n<mat-accordion class=\"col-md-12 mt-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Salary Amount Part Payment  in currencies\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <div class=\"col-md-12 text-center mb-15\">\r\n      <label class=\"lbl-input\">Net Salary </label><input type=\"text\" disabled>\r\n    </div>\r\n    <table class=\"table even-odd-color\">\r\n      <thead>\r\n        <tr>\r\n          <th>Currency</th>\r\n          <th>Select(if Hard Currency)</th>\r\n          <th>Amount (INR)</th>\r\n          <th>Converted Amount</th>\r\n          <th>Balance Net Payable (INR)</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr>\r\n          <td>\r\n            <mat-select placeholder=\"Select\" class=\"selection-border select-wid\">\r\n              <mat-option>Select</mat-option>\r\n              <mat-option>Hard Currency</mat-option>\r\n              <mat-option>Local Currency</mat-option>\r\n            </mat-select>\r\n          </td>\r\n          <td>\r\n            <mat-select placeholder=\"Select\" class=\"selection-border select-wid\">\r\n              <mat-option>Select</mat-option>\r\n              <mat-option>EURO</mat-option>\r\n              <mat-option>UK Pound Sterling</mat-option>\r\n              <mat-option>US Doller</mat-option>\r\n            </mat-select>\r\n          </td>\r\n          <td><input type=\"text\"></td>\r\n          <td><input type=\"text\"></td>\r\n          <td><input type=\"text\" disabled></td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn\">View Draft Salary</button>\r\n      <button type=\"button\" class=\"btn btn-info\">Re-Calculate</button>\r\n      <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n<!----------------------Salary Amount Part Payment  in currencies tab End-------------------------->\r\n<!--right table-->\r\n\r\n<mat-accordion class=\"col-md-12 mt-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Salary Summary\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n        <!-- Search -->\r\n        <mat-form-field>\r\n          <input matInput placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n\r\n        <div class=\"mat-elevation-z8\">\r\n          <table class=\"table even-odd-color\">\r\n            <thead>\r\n              <tr>\r\n                <th>Month / Year</th>\r\n                <th>Gross Amount</th>\r\n                <th>Deductions</th>\r\n                <th>Amount Payable</th>\r\n                <th>Net Pay</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr>\r\n                <td>05 / 2019</td>\r\n                <td>39324</td>\r\n                <td>5000</td>\r\n                <td>34324</td>\r\n                <td>34324</td>\r\n              </tr>\r\n              <tr>\r\n                <td>05 / 2019</td>\r\n                <td>39324</td>\r\n                <td>5000</td>\r\n                <td>34324</td>\r\n                <td>34324</td>\r\n              </tr>\r\n              <tr>\r\n                <td>05 / 2019</td>\r\n                <td>39324</td>\r\n                <td>5000</td>\r\n                <td>34324</td>\r\n                <td>34324</td>\r\n              </tr>\r\n              <tr>\r\n                <td>05 / 2019</td>\r\n                <td>39324</td>\r\n                <td>5000</td>\r\n                <td>34324</td>\r\n                <td>34324</td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: DuesanddeductionsemployeewiseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DuesanddeductionsemployeewiseComponent", function() { return DuesanddeductionsemployeewiseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DuesanddeductionsemployeewiseComponent = /** @class */ (function () {
    function DuesanddeductionsemployeewiseComponent() {
        this.displayedColumns = ['name', 'weight', 'symbol', 'remark', 'periodicity', 'month',];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
    }
    DuesanddeductionsemployeewiseComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], DuesanddeductionsemployeewiseComponent.prototype, "paginator", void 0);
    DuesanddeductionsemployeewiseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-duesanddeductionsemployeewise',
            template: __webpack_require__(/*! ./duesanddeductionsemployeewise.component.html */ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.html"),
            styles: [__webpack_require__(/*! ./duesanddeductionsemployeewise.component.css */ "./src/app/bill-group-mgmt/duesanddeductionsemployeewise/duesanddeductionsemployeewise.component.css")]
        })
    ], DuesanddeductionsemployeewiseComponent);
    return DuesanddeductionsemployeewiseComponent;
}());

var ELEMENT_DATA = [
    { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
    { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
    { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
    { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
];


/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.css":
/*!******************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9tYXBwaW5nYWNjb3VudGhlYWQvYWN0ZGVhY3RtYXBhY290aDEvYWN0ZGVhY3RtYXBhY290aDEuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n\r\n  <mat-card class=\"example-card\">\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12 combo-col clear-both select-type\">\r\n      <div class=\"col-md-12 col-lg-8 pading-16\">\r\n        <label>DDO Code </label>\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select\" [(ngModel)]=\"mappingAccountHeadOT1.ddoCode\" name=\"ddoCode\" #ddoCode=\"ngModel\" (selectionChange)=\"GetAccountHeadOT1ActDedata($event.value)\">\r\n            <mat-option *ngFor=\"let accountheadsOT1 of getAccountHeadOT1\" [value]=\"accountheadsOT1.ddoCode\">\r\n              {{accountheadsOT1.ddoCode+'('+accountheadsOT1.ddoCodeddoName+')'}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4 pading-16\">\r\n        <label>Financial Year</label>\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [(ngModel)]=\"modeselect\" placeholder=\"Select\">\r\n            <mat-option value=\"2018\">2018-2019</mat-option>\r\n            <!--<mat-option value=\"2019\">2019-2020</mat-option>-->\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n  </mat-card>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">\r\n      Details of Activated  Account Head (Other than 1)\r\n\r\n\r\n    </div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          S.No\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n\r\n      <ng-container matColumnDef=\"AccountHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"ObjectHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Object Head </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.objectHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Category\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Category</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.category}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"AccountHeadName\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head Name </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHeadName}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"AccountHeadStatus\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head Status </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.descriptions}} </mat-cell>\r\n      </ng-container>\r\n      <!--<ng-container matColumnDef=\"PaybillGroupStatus\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> paybill Group Status </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.paybillGroupStatus}} </mat-cell>\r\n      </ng-container>-->\r\n\r\n      <ng-container matColumnDef=\"select\">\r\n        <mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle($event) : null\" [checked]=\"selection.hasValue() && isAllSelected() == true\" [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n          </mat-checkbox>\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selection.toggle(row) : null;\" [checked]=\"selection.isSelected(row) == true\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n    </mat-table>\r\n    <!--<mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>-->\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"Submit\" id=\"btn_De_Active\" class=\"btn btn-success\" (click)=\"De_ActivecheckOptions()\" [disabled]=\"!disabled\">\r\n        De-Active\r\n\r\n      </button>\r\n      <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm()\">Reset</button>-->\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">\r\n      Details of De-Activated  Account Head (Other than 1)\r\n\r\n    </div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"MappedapplyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <mat-table #table [dataSource]=\"MappeddataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> S.No </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n\r\n      <ng-container matColumnDef=\"AccountHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Account Head\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"ObjectHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Object Head\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.objectHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Category\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Category\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.category}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"AccountHeadName\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Account Head Name\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHeadName}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"AccountHeadStatus\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head Status </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.descriptions}} </mat-cell>\r\n      </ng-container>\r\n      <!--<ng-container matColumnDef=\"PaybillGroupStatus\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> paybill Group Status </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.paybillGroupStatus}} </mat-cell>\r\n      </ng-container>-->\r\n      <ng-container matColumnDef=\"Mappedselect\">\r\n        <mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? MappedmasterToggle($event) : null\" [checked]=\"Mappedselection.hasValue() && MappedisAllSelected() == true\" [indeterminate]=\"Mappedselection.hasValue() && !MappedisAllSelected()\">\r\n          </mat-checkbox>\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ?  Mappedselection.toggle(row) : null;\" [checked]=\" Mappedselection.isSelected(row) == true\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n\r\n      <mat-header-row *matHeaderRowDef=\"MappeddisplayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: MappeddisplayedColumns;\"></mat-row>\r\n\r\n    </mat-table>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"Submit\" id=\"btn_Active\" class=\"btn btn-success\" (click)=\"ActivecheckOptions()\" [disabled]=\"!disabled\">Active</button>\r\n      <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm()\">Reset</button>-->\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: Actdeactmapacoth1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Actdeactmapacoth1Component", function() { return Actdeactmapacoth1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Actdeactmapacoth1Component = /** @class */ (function () {
    function Actdeactmapacoth1Component(_Service, snackBar) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.mappingAccountHeadOT1 = {
            ddoCode: null,
            ddoCodeddoName: null
        };
        this.getAccountHeadOT1 = [];
        this.modeselect = '2018';
        this.displayedColumns = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'AccountHeadStatus', 'select'];
        this.MappeddisplayedColumns = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'AccountHeadStatus', 'Mappedselect'];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__["SelectionModel"](true, []);
        this.Mappedselection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__["SelectionModel"](true, []);
    }
    Actdeactmapacoth1Component.prototype.ngOnInit = function () {
        this.getAccountHeadsOT1();
        this.disabled = false;
    };
    //Get DDO Info 
    Actdeactmapacoth1Component.prototype.getAccountHeadsOT1 = function () {
        var _this = this;
        //debugger;
        if (this.modeselect === '2018') {
            this.FinFromYr = '2018';
            this.FinToYr = '2019';
        }
        this._Service.GetAccountHeadsOT1(this.FinFromYr, this.FinToYr, "ACT_DEACTIVE").subscribe(function (data) {
            _this.getAccountHeadOT1 = data;
            console.log(data[0].ddoCode);
        });
    };
    //on click of DDL_ddocode   
    Actdeactmapacoth1Component.prototype.GetAccountHeadOT1ActDedata = function (value) {
        debugger;
        if (value === 'undefined' || null) {
            this.disabled = true;
        }
        this.getAccountActivatedHeadOT1(value);
        this.getAccountDeActivatedHeadOT1(value);
    };
    // #region <Activated   Mapping of account head OT 1>
    Actdeactmapacoth1Component.prototype.getAccountActivatedHeadOT1 = function (DDOCode) {
        var _this = this;
        this._Service.GetAccountHeadOT1PaybillGroupStatus(DDOCode, 'Y').subscribe(function (res) {
            debugger;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res);
            _this.dataSource.sort = _this.sort;
            _this.disabled = true;
        });
    };
    Actdeactmapacoth1Component.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    Actdeactmapacoth1Component.prototype.isAllSelected = function ($event) {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    Actdeactmapacoth1Component.prototype.masterToggle = function ($event) {
        var _this = this;
        debugger;
        this.isAllSelected($event) ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    Actdeactmapacoth1Component.prototype.De_ActivecheckOptions = function () {
        debugger;
        var numSelected1 = this.selection.selected.length;
        var self = this;
        var isSelected = numSelected1;
        var ddoCodeSelected = this.mappingAccountHeadOT1.ddoCode;
        if (isSelected > 0) {
            //At least one is selected
            //alert(numSelected1);      
            this.selection.selected.forEach(function (value) {
                var accountHeadvalue = value.hiddenAccountHead;
                //billgroups.push(value.accountHead);
                self._Service.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, ddoCodeSelected, 'ACTIVE').subscribe(function (result) {
                    if (parseInt(result) >= 1) {
                        self.snackBar.open('Deactive Update Successfully', null, { duration: 4000 });
                        self.GetAccountHeadOT1ActDedata(ddoCodeSelected);
                    }
                    else {
                        self.snackBar.open('Deactive Not Update Successfully', null, { duration: 4000 });
                    }
                });
                //console.log(value.accountHead);
            });
            //mapped account head
            this.getAccountDeActivatedHeadOT1(ddoCodeSelected);
            self.selection.clear();
            self.Mappedselection.clear();
            //alert(billgroups);
        }
        else {
            alert("select at least one for Deactive");
        }
    };
    //onCompleteRow(dataSource) {
    //  dataSource.data.forEach(element => {
    //    console.log(element.accountHead);
    //  });
    //}
    // #endregion
    // #region <De-Activated Mapped of account head OT 1>
    Actdeactmapacoth1Component.prototype.MappedapplyFilter = function (filterValue) {
        this.MappeddataSource.filter = filterValue.trim().toLowerCase();
        if (this.MappeddataSource.paginator) {
            this.MappeddataSource.paginator.firstPage();
        }
    };
    Actdeactmapacoth1Component.prototype.getAccountDeActivatedHeadOT1 = function (DDOCode) {
        var _this = this;
        this._Service.GetAccountHeadOT1PaybillGroupStatus(DDOCode, 'N').subscribe(function (res) {
            debugger;
            _this.MappeddataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res);
            _this.MappeddataSource.sort = _this.sort;
        });
    };
    Actdeactmapacoth1Component.prototype.MappedisAllSelected = function ($event) {
        var numSelected = this.Mappedselection.selected.length;
        var numRows = this.MappeddataSource.data.length;
        return numSelected === numRows;
    };
    Actdeactmapacoth1Component.prototype.MappedmasterToggle = function ($event) {
        var _this = this;
        debugger;
        this.MappedisAllSelected($event) ?
            this.Mappedselection.clear() :
            this.MappeddataSource.data.forEach(function (row) { return _this.Mappedselection.select(row); });
    };
    Actdeactmapacoth1Component.prototype.ActivecheckOptions = function () {
        debugger;
        var numSelected1 = this.Mappedselection.selected.length;
        var self = this;
        var isSelected = numSelected1;
        var ddoCodeSelected = this.mappingAccountHeadOT1.ddoCode;
        //let paybillGroupStatusSelected: any = this.Mappedselection.selected[0].paybillGroupStatus;
        if (isSelected > 0) {
            //At least one is selected
            //alert(numSelected1);
            // if (paybillGroupStatusSelected == 'Yes') {
            this.Mappedselection.selected.forEach(function (value) {
                var accountHeadvalue = value.hiddenAccountHead;
                //billgroups.push(value.accountHead);
                self._Service.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, ddoCodeSelected, null).subscribe(function (result) {
                    if (parseInt(result) >= 1) {
                        self.snackBar.open('Active Update Successfully', null, { duration: 4000 });
                        self.GetAccountHeadOT1ActDedata(ddoCodeSelected);
                    }
                    else {
                        self.snackBar.open('Active Not Update Successfully', null, { duration: 4000 });
                    }
                });
                //console.log(value.accountHead);
            });
            //mapped account head
            this.getAccountDeActivatedHeadOT1(ddoCodeSelected);
            //}
            //else {
            //  alert("You Can only De-Activate the Account Head as Bill is Already Processed");
            //}
            self.selection.clear();
            self.Mappedselection.clear();
            //alert(billgroups);
        }
        else {
            alert("select at least one for Active");
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], Actdeactmapacoth1Component.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], Actdeactmapacoth1Component.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], Actdeactmapacoth1Component.prototype, "form", void 0);
    Actdeactmapacoth1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-actdeactmapacoth1',
            template: __webpack_require__(/*! ./actdeactmapacoth1.component.html */ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.html"),
            styles: [__webpack_require__(/*! ./actdeactmapacoth1.component.css */ "./src/app/bill-group-mgmt/mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component.css")]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__["PayBillGroupService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], Actdeactmapacoth1Component);
    return Actdeactmapacoth1Component;
}());

var ELEMENT_DATA = [];


/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9tYXBwaW5nYWNjb3VudGhlYWQvbWFwcGluZ2FjY291bnRoZWFkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n\r\n  <mat-card class=\"example-card\">\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12 combo-col clear-both select-type\">\r\n      <div class=\"col-md-12 col-lg-8 pading-16\">\r\n        <label>DDO Code </label>\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select\" [(ngModel)]=\"mappingAccountHeadOT1.ddoCode\" name=\"ddoCode\" #ddoCode=\"ngModel\" (selectionChange)=\"GetAccountHeadOT1data($event.value)\">\r\n            <mat-option *ngFor=\"let accountheadsOT1 of getAccountHeadOT1\" [value]=\"accountheadsOT1.ddoCode\">\r\n              {{accountheadsOT1.ddoCode+'('+accountheadsOT1.ddoCodeddoName+')'}}\r\n            </mat-option>\r\n          </mat-select>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4 pading-16\">\r\n        <label>Financial Year</label>\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [(ngModel)]=\"modeselect\" placeholder=\"Select\">\r\n            <mat-option value=\"2018\">2018-2019</mat-option>\r\n            <!--<mat-option value=\"2019\">2019-2020</mat-option>-->\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n  </mat-card>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">\r\n      Mapping of Account Head (Other than 1)\r\n    </div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          S.No\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n\r\n      <ng-container matColumnDef=\"AccountHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"ObjectHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Object Head </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.objectHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Category\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>Category</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.category}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"AccountHeadName\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Account Head Name </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHeadName}} </mat-cell>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"select\">\r\n        <mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle($event) : null\" [checked]=\"selection.hasValue() && isAllSelected() == true\" [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n          </mat-checkbox>\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selection.toggle(row) : null;\" [checked]=\"selection.isSelected(row) == true\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n    </mat-table>\r\n    <!--<mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>-->\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"Submit\" id=\"btn_save\" class=\"btn btn-success\" (click)=\"checkOptions()\" [disabled]=\"!disabled\">Save</button>\r\n      <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm()\">Reset</button>-->\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">\r\n      Mapped Account Head\r\n\r\n    </div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"MappedapplyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <mat-table #table [dataSource]=\"MappeddataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"S.No\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> S.No </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <!-- Group Column -->\r\n\r\n      <ng-container matColumnDef=\"AccountHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Account Head\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"ObjectHead\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Object Head\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.objectHead}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Category\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Category\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.category}} </mat-cell>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"AccountHeadName\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Account Head Name\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.accountHeadName}} </mat-cell>\r\n\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PaybillGroupStatus\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          Paybill Group Status\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.paybillGroupStatus}} </mat-cell>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Mappedselect\">\r\n        <mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? MappedmasterToggle($event) : null\" [checked]=\"Mappedselection.hasValue() && MappedisAllSelected() == true\" [indeterminate]=\"Mappedselection.hasValue() && !MappedisAllSelected()\">\r\n          </mat-checkbox>\r\n        </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ?  Mappedselection.toggle(row) : null;\" [checked]=\" Mappedselection.isSelected(row) == true\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n\r\n      <mat-header-row *matHeaderRowDef=\"MappeddisplayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: MappeddisplayedColumns;\"></mat-row>\r\n\r\n    </mat-table>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"Submit\" id=\"btn_UnMapped\" class=\"btn btn-success\" (click)=\"UnMappedcheckOptions()\" [disabled]=\"!disabled\">Un-Map</button>\r\n      <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm()\">Reset</button>-->\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.ts ***!
  \************************************************************************************/
/*! exports provided: MappingaccountheadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MappingaccountheadComponent", function() { return MappingaccountheadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MappingaccountheadComponent = /** @class */ (function () {
    function MappingaccountheadComponent(_Service, snackBar) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.mappingAccountHeadOT1 = {
            ddoCode: null,
            ddoCodeddoName: null
        };
        this.getAccountHeadOT1 = [];
        this.modeselect = '2018';
        this.displayedColumns = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'select'];
        this.MappeddisplayedColumns = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'PaybillGroupStatus', 'Mappedselect'];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__["SelectionModel"](true, []);
        this.Mappedselection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_3__["SelectionModel"](true, []);
    }
    MappingaccountheadComponent.prototype.ngOnInit = function () {
        this.getAccountHeadsOT1();
        this.disabled = false;
    };
    MappingaccountheadComponent.prototype.getAccountHeadsOT1 = function () {
        var _this = this;
        //debugger;
        if (this.modeselect === '2018') {
            this.FinFromYr = '2018';
            this.FinToYr = '2019';
        }
        this._Service.GetAccountHeadsOT1(this.FinFromYr, this.FinToYr, null).subscribe(function (data) {
            _this.getAccountHeadOT1 = data;
            console.log(data[0].ddoCode);
        });
    };
    //on click of DDL_ddocode
    MappingaccountheadComponent.prototype.GetAccountHeadOT1data = function (value) {
        debugger;
        if (value === 'undefined' || null) {
            this.disabled = true;
        }
        this.getAccountHeadOT1AttachList(value);
        this.getAccountHeadOT1MappedAttachList(value);
    };
    // #region <Mapping of account head OT 1>
    MappingaccountheadComponent.prototype.getAccountHeadOT1AttachList = function (DDOCode) {
        var _this = this;
        this._Service.GetAccountHeadOT1AttachList(DDOCode, 'N').subscribe(function (res) {
            debugger;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res);
            _this.dataSource.sort = _this.sort;
            _this.disabled = true;
        });
    };
    MappingaccountheadComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    MappingaccountheadComponent.prototype.isAllSelected = function ($event) {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    MappingaccountheadComponent.prototype.masterToggle = function ($event) {
        var _this = this;
        debugger;
        this.isAllSelected($event) ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    MappingaccountheadComponent.prototype.checkOptions = function () {
        debugger;
        var numSelected1 = this.selection.selected.length;
        var self = this;
        var isSelected = numSelected1;
        var ddoCodeSelected = this.mappingAccountHeadOT1.ddoCode;
        if (isSelected > 0) {
            //At least one is selected
            //alert(numSelected1);      
            this.selection.selected.forEach(function (value) {
                var accountHeadvalue = value.hiddenAccountHead;
                //billgroups.push(value.accountHead);
                self._Service.UpdateaccountHeadOT1(accountHeadvalue, ddoCodeSelected, 'Map').subscribe(function (result) {
                    if (parseInt(result) >= 1) {
                        self.snackBar.open('Update Successfully', null, { duration: 4000 });
                        self.GetAccountHeadOT1data(ddoCodeSelected);
                    }
                    else {
                        self.snackBar.open('Not Update Successfully', null, { duration: 4000 });
                    }
                });
                //console.log(value.accountHead);
            });
            //mapped account head
            this.getAccountHeadOT1MappedAttachList(ddoCodeSelected);
            self.selection.clear();
            self.Mappedselection.clear();
            //alert(billgroups);
        }
        else {
            alert("select at least one");
        }
    };
    //onCompleteRow(dataSource) {
    //  dataSource.data.forEach(element => {
    //    console.log(element.accountHead);
    //  });
    //}
    // #endregion
    // #region <Mapped of account head OT 1>
    MappingaccountheadComponent.prototype.MappedapplyFilter = function (filterValue) {
        this.MappeddataSource.filter = filterValue.trim().toLowerCase();
        if (this.MappeddataSource.paginator) {
            this.MappeddataSource.paginator.firstPage();
        }
    };
    MappingaccountheadComponent.prototype.getAccountHeadOT1MappedAttachList = function (DDOCode) {
        var _this = this;
        this._Service.GetAccountHeadOT1AttachList(DDOCode, 'Y').subscribe(function (res) {
            debugger;
            _this.MappeddataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res);
            _this.MappeddataSource.sort = _this.sort;
        });
    };
    MappingaccountheadComponent.prototype.MappedisAllSelected = function ($event) {
        var numSelected = this.Mappedselection.selected.length;
        var numRows = this.MappeddataSource.data.length;
        return numSelected === numRows;
    };
    MappingaccountheadComponent.prototype.MappedmasterToggle = function ($event) {
        var _this = this;
        debugger;
        this.MappedisAllSelected($event) ?
            this.Mappedselection.clear() :
            this.MappeddataSource.data.forEach(function (row) { return _this.Mappedselection.select(row); });
    };
    MappingaccountheadComponent.prototype.UnMappedcheckOptions = function () {
        debugger;
        var numSelected1 = this.Mappedselection.selected.length;
        var self = this;
        var isSelected = numSelected1;
        var ddoCodeSelected = this.mappingAccountHeadOT1.ddoCode;
        // let paybillGroupStatusSelected: any = this.Mappedselection.selected[0].paybillGroupStatus;
        //this.Mappedselection.selected.forEach(function (value) {
        //  let accountHeadvalue: string = value.hiddenAccountHead;
        //})
        if (isSelected > 0) {
            //At least one is selected
            //alert(numSelected1);
            this.Mappedselection.selected.forEach(function (value) {
                debugger;
                var accountHeadvalue = value.hiddenAccountHead;
                var paybillGroupStatusSelected = value.paybillGroupStatus;
                if (paybillGroupStatusSelected == 'No') {
                    self._Service.UpdateaccountHeadOT1(accountHeadvalue, ddoCodeSelected, 'Unmap').subscribe(function (result) {
                        if (parseInt(result) >= 1) {
                            self.snackBar.open('Update Un-Map Successfully', null, { duration: 4000 });
                            self.GetAccountHeadOT1data(ddoCodeSelected);
                        }
                        else {
                            self.snackBar.open('Update Un-Map Not Successfully', null, { duration: 4000 });
                        }
                    });
                }
                else {
                    alert("You Can only De-Activate the Account Head as Bill is Already Processed");
                }
                //console.log(value.accountHead);
            });
            //mapped account head
            this.getAccountHeadOT1MappedAttachList(ddoCodeSelected);
            self.selection.clear();
            self.Mappedselection.clear();
            //alert(billgroups);
        }
        else {
            alert("select at least one for Un-mapped");
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], MappingaccountheadComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], MappingaccountheadComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('creationForm'),
        __metadata("design:type", Object)
    ], MappingaccountheadComponent.prototype, "form", void 0);
    MappingaccountheadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-mappingaccounthead',
            template: __webpack_require__(/*! ./mappingaccounthead.component.html */ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.html"),
            styles: [__webpack_require__(/*! ./mappingaccounthead.component.css */ "./src/app/bill-group-mgmt/mappingaccounthead/mappingaccounthead.component.css")],
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_2__["PayBillGroupService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], MappingaccountheadComponent);
    return MappingaccountheadComponent;
}());

var ELEMENT_DATA = [];


/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9wYXliaWxsZ2VuZXJhdGlvbi9wYXliaWxsZ2VuZXJhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12 mb-30\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Temporary PBR Process</div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Pay Bill Group\">\r\n          <mat-option>Select</mat-option>\r\n          <mat-option>ABC</mat-option>\r\n          <mat-option>XYZ</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Sort By\">\r\n          <mat-option>Select</mat-option>\r\n          <mat-option>Alphabetically</mat-option>\r\n          <mat-option>Designation wise</mat-option>\r\n          <mat-option>Pay Level wise</mat-option>\r\n          <mat-option>EPS Employee Code</mat-option>\r\n          <mat-option>Departmental Employee Code</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Pay Bill Format\">\r\n          <mat-option>Select</mat-option>\r\n          <mat-option>A4</mat-option>\r\n          <mat-option>A4-Double-sided</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Freezed Employees\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Checkbox Column -->\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                        [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                        [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\r\n                        [aria-label]=\"checkboxLabel()\">\r\n          </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                        (change)=\"$event ? selection.toggle(row) : null\"\r\n                        [checked]=\"selection.isSelected(row)\"\r\n                        [aria-label]=\"checkboxLabel(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"weight\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Symbol Column -->\r\n      <ng-container matColumnDef=\"symbol\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"empcode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Departmental Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empcode}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n          (click)=\"selection.toggle(row)\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n\r\n\r\n    <div class=\"col-lg-12 text-center attach-dattach-wraper\">\r\n      <button class=\"attach-btn mat-raised-button\"> Attach </button>\r\n      <button class=\"dattach-btn mat-raised-button\">  DeAttach  </button>\r\n    </div>\r\n\r\n\r\n    <!--table-2-->\r\n    <div class=\"fom-title\">Selected Freezed Employees for Pay Bill generation</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Checkbox Column -->\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                        [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                        [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\r\n                        [aria-label]=\"checkboxLabel()\">\r\n          </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                        (change)=\"$event ? selection.toggle(row) : null\"\r\n                        [checked]=\"selection.isSelected(row)\"\r\n                        [aria-label]=\"checkboxLabel(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"weight\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n      </ng-container>\r\n\r\n    \r\n\r\n      \r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n          (click)=\"selection.toggle(row)\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\">Generate Pay Bill</button>\r\n      <button type=\"button\" class=\"btn btn-info\">Print Report</button>\r\n    </div>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.ts ***!
  \**********************************************************************************/
/*! exports provided: PaybillgenerationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaybillgenerationComponent", function() { return PaybillgenerationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA = [
    { position: 1, name: 'Rahul Khanna', weight: 121 },
    { position: 1, name: 'Rahul Khanna', weight: 121 },
    { position: 1, name: 'Rahul Khanna', weight: 121 },
    { position: 1, name: 'Rahul Khanna', weight: 121 },
    { position: 1, name: 'Rahul Khanna', weight: 121 },
    { position: 1, name: 'Rahul Khanna', weight: 121 },
];
var PaybillgenerationComponent = /** @class */ (function () {
    function PaybillgenerationComponent() {
        this.displayedColumns = ['select', 'position', 'weight', 'name'];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__["SelectionModel"](true, []);
    }
    /** Whether the number of selected elements matches the total number of rows. */
    PaybillgenerationComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    PaybillgenerationComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    /** The label for the checkbox on the passed row */
    PaybillgenerationComponent.prototype.checkboxLabel = function (row) {
        if (!row) {
            return (this.isAllSelected() ? 'select' : 'deselect') + " all";
        }
        return (this.selection.isSelected(row) ? 'deselect' : 'select') + " row " + (row.position + 1);
    };
    PaybillgenerationComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    PaybillgenerationComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], PaybillgenerationComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], PaybillgenerationComponent.prototype, "sort", void 0);
    PaybillgenerationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-paybillgeneration',
            template: __webpack_require__(/*! ./paybillgeneration.component.html */ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.html"),
            styles: [__webpack_require__(/*! ./paybillgeneration.component.css */ "./src/app/bill-group-mgmt/paybillgeneration/paybillgeneration.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PaybillgenerationComponent);
    return PaybillgenerationComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillprocess/date.adapter.ts":
/*!****************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillprocess/date.adapter.ts ***!
  \****************************************************************/
/*! exports provided: AppDateAdapter, APP_DATE_FORMATS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDateAdapter", function() { return AppDateAdapter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_DATE_FORMATS", function() { return APP_DATE_FORMATS; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var AppDateAdapter = /** @class */ (function (_super) {
    __extends(AppDateAdapter, _super);
    function AppDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppDateAdapter.prototype.parse = function (value) {
        if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
            var str = value.split('/');
            var year = Number(str[2]);
            var month = Number(str[1]) - 1;
            var date = Number(str[0]);
            return new Date(year, month, date);
        }
        var timestamp = typeof value === 'number' ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    };
    AppDateAdapter.prototype.format = function (date, displayFormat) {
        if (displayFormat == "input") {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
        }
        else if (displayFormat == "inputMonth") {
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return this._to2digit(month) + '/' + year;
        }
        else {
            return date.toDateString();
        }
    };
    AppDateAdapter.prototype._to2digit = function (n) {
        return ('00' + n).slice(-2);
    };
    return AppDateAdapter;
}(_angular_material__WEBPACK_IMPORTED_MODULE_0__["NativeDateAdapter"]));

var APP_DATE_FORMATS = {
    parse: {
        dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
    },
    display: {
        // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
        dateInput: 'input',
        // monthYearLabel: { month: 'short', year: 'numeric', day: 'numeric' },
        monthYearLabel: 'inputMonth',
        dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
        monthYearA11yLabel: { year: 'numeric', month: 'long' },
    }
};


/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC9wYXliaWxscHJvY2Vzcy9wYXliaWxscHJvY2Vzcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Success!</strong> This alert box could indicate a successful or positive action.\r\n  </div>\r\n  <div class=\"alert alert-info alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n\r\n    <ng-template matStepLabel>Pay Bill Group Attach</ng-template>\r\n\r\n\r\n    <div class=\"col-md-12 combo-col clear-both select-type \">\r\n      <div class=\"col-md-6 col-lg-3 pading-16\">\r\n        <label>Financial Year</label>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-3\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-label>Select Financial Year</mat-label>\r\n          <mat-select [(value)]=\"selected\">\r\n\r\n            <mat-option value=\"option1\">2018-2019</mat-option>\r\n            <mat-option value=\"option2\">2019-2020</mat-option>\r\n\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n\r\n      </div>\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-3\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput (click)=\"DOB.open()\" [matDatepicker]=\"DOB\" placeholder=\"Select pay month\"\r\n               [(ngModel)]=\"billAttach.empDOB\" name=\"empDOB\" readonly>\r\n        <mat-datepicker-toggle matSuffix [for]=\"DOB\"></mat-datepicker-toggle>\r\n        <mat-datepicker #DOB></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-3\">\r\n      <mat-form-field class=\"wid-100\">\r\n\r\n\r\n        <mat-select placeholder=\"Pay Bill Group\" name=\"billgroupId\" #billgroupId=\"ngModel\"\r\n                    [(ngModel)]=\"billAttach.billgroupId\" (selectionChange)=\"GetPayBillGroupId($event.value)\">\r\n          <mat-option *ngFor=\"let Bill of getPayBillGroup\" [value]=\"Bill.billgrId\">\r\n            {{Bill.billgrId +'('+Bill.billgrDesc+')'}}\r\n\r\n\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-6 combo-col-2\">\r\n\r\n      <div class=\"col-md-6 col-lg-3 pading-0\">\r\n        <label>Generate GAR inner By</label>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-9\">\r\n        <mat-radio-group aria-label=\"Select an option\">\r\n          <mat-radio-button value=\"auto\" checked>Alphabetically</mat-radio-button>\r\n          <mat-radio-button value=\"Paylevelwise\" class=\"gender-mr\">Pay level wise</mat-radio-button>\r\n          <mat-radio-button value=\"Designationwise\">Designation wise</mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n      </div>\r\n    </div>\r\n\r\n\r\n\r\n\r\n    <div class=\"col-md-6 col-lg-3\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-label>Bill Type</mat-label>\r\n        <mat-select [(value)]=\"selected1\">\r\n\r\n          <mat-option value=\"option1\">Bill Type 1</mat-option>\r\n          <mat-option value=\"option2\">Bill Type 2</mat-option>\r\n          <mat-option value=\"option2\">Test</mat-option>\r\n\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" (click)=\"saveMethod('name')\" class=\"btn btn-success\">Process</button>\r\n      <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-12\">\r\n\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n    <div class=\"fom-title\">Employees of selected  Bill Group</div>\r\n\r\n\r\n    <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n\r\n\r\n      <ng-container matColumnDef=\"empCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"empName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"desc\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desc}} </td>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"desigFieldDeptCD\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> DepttEmpCode </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desigFieldDeptCD}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                        [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n        </th>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                        [checked]=\"selectionDeAttach.isSelected(row)\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasData\">\r\n      No Data Found\r\n    </div>\r\n\r\n\r\n\r\n\r\n  </div>\r\n\r\n  <div class=\"col-lg-12 text-center attach-dattach-wraper\">\r\n    <button mat-raised-button class=\"attach-btn\" (click)=\"DeAttachSelectedRows()\">\r\n      Select\r\n    </button>\r\n\r\n\r\n    <button mat-raised-button class=\"dattach-btn\" (click)=\"AttachSelectedRows()\">\r\n\r\n      UnSelect\r\n    </button>\r\n\r\n\r\n  </div>\r\n\r\n\r\n\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n    <div class=\"fom-title\">Attached Employees Whoose Bill To be processed</div>\r\n\r\n\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"index\">\r\n        <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n      </ng-container>\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"empCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"empName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"desc\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desc}} </td>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"desigFieldDeptCD\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> DepttEmpCode </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desigFieldDeptCD}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                        [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n        </th>\r\n        <mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null\"\r\n                        [checked]=\"selectionAttach.isSelected(row)\">\r\n          </mat-checkbox>\r\n        </mat-cell>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns; let i = index\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasData\">\r\n      No Data Found\r\n    </div>\r\n\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.ts ***!
  \****************************************************************************/
/*! exports provided: PaybillprocessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaybillprocessComponent", function() { return PaybillprocessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/PayBill/PayBillService */ "./src/app/services/PayBill/PayBillService.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _date_adapter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./date.adapter */ "./src/app/bill-group-mgmt/paybillprocess/date.adapter.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PaybillprocessComponent = /** @class */ (function () {
    function PaybillprocessComponent(_Service, fb, masterServices, snackBar) {
        this._Service = _Service;
        this.fb = fb;
        this.masterServices = masterServices;
        this.snackBar = snackBar;
        this.is_btnStatus = true;
        this.disbleflag = false;
        this.isTableHasData = true;
        this.getEmpById = {};
        // sfgdfgd
        this.displayedColumns = ['empCode', 'empName', 'desc', 'desigFieldDeptCD', 'select',];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.billAttach = {
            payBillGroupId: null,
            schemeId: null,
            serviceType: null,
            group: null,
            groupId: null,
            billGroupName: null,
            billForGAR: null,
            isActive: null,
            EmployeeType: null,
            EmployeeSubType: null,
            Designation: null,
            billgroupId: null,
            billgrDesc: null,
            billgrFor: null,
            empDOB: null
        };
        this.getPayBillGroup = [];
        // toppings = new FormControl();
        // toppingList: string[] = ['Regular', 'Co-terminus', 'Deputation In', 'Consultant', 'On-Contract', 'Member of Parliament'];
        this.EmployeeSybType = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.EmployeeSubTypeList = [''];
    }
    PaybillprocessComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    PaybillprocessComponent.prototype.ngOnInit = function () {
        this.disbleflag = true;
        this.getPayBillGroups('00003');
        this.getEmployeetype();
        this.getEmpAttachList();
    };
    PaybillprocessComponent.prototype.getEmpAttachList = function () {
        var _this = this;
        this._Service.getEmpAttachList().subscribe(function (res) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](res);
            _this.data = Object.assign(res);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.data);
        });
    };
    PaybillprocessComponent.prototype.getEmpDeAttachList = function (BillGrpId) {
        var _this = this;
        this._Service.getEmpdEAttachList(BillGrpId).subscribe(function (res) {
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](res);
            _this.dataDeAttach = Object.assign(res);
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
        });
    };
    PaybillprocessComponent.prototype.getEmployeetype = function () {
        var _this = this;
        this.masterServices.getEmployeeType().subscribe(function (res) {
            _this.employeeTypes = res;
        });
    };
    PaybillprocessComponent.prototype.getEmployeeSubType = function (parentId) {
        var _this = this;
        this.masterServices.getEmployeeSubType(parentId).subscribe(function (res) {
            _this.employeeSubTypes = res;
        });
    };
    PaybillprocessComponent.prototype.getEmployeeTypeId = function (value) {
        if (value === 'undefined' || null) {
        }
        this.getEmployeeSubType(value);
        this.getEmpById.employeeSubTypes = 0;
    };
    PaybillprocessComponent.prototype.GetPayBillGroupId = function (value) {
        if (value === 'undefined' || null) {
        }
        this.GetPayBillGroupDetailsById(value);
        this.getEmpDeAttachList(value);
    };
    PaybillprocessComponent.prototype.GetPayBillGroupDetailsById = function (parentId) {
        var _this = this;
        this._Service.GetPayBillGroupDetailsById(parentId).subscribe(function (res) {
            _this.billAttach = res;
            if (_this.billAttach.serviceType == "" && _this.billAttach.billForGAR == "") {
                _this.billAttach.serviceType = "No servie Type";
                _this.billAttach.billForGAR = "  this value is empty";
            }
            _this.billAttach.billgroupId = parentId;
            console.log(_this.billAttach);
        });
    };
    PaybillprocessComponent.prototype.getPayBillGroups = function (permDDOId) {
        var _this = this;
        this._Service.BindPayBillGroup(permDDOId).subscribe(function (data) {
            _this.getPayBillGroup = data;
            console.log(data[0].schemeCode);
        });
    };
    PaybillprocessComponent.prototype.isAllSelectedForDeAttach = function () {
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    PaybillprocessComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    PaybillprocessComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
    };
    PaybillprocessComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
    };
    PaybillprocessComponent.prototype.DeAttachSelectedRows = function () {
        var _this = this;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                _this.data.push(item);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        }
        else {
            alert("Please Unselect at least one Employee ");
        }
    };
    PaybillprocessComponent.prototype.AttachSelectedRows = function () {
        var _this = this;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                _this.dataDeAttach.push(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        }
        else {
            alert("Please  select at least one Employee");
        }
    };
    PaybillprocessComponent.prototype.saveMethod = function (name) {
        if (confirm("Are you sure to process pay Bill   ")) {
            console.log("Implement process functionality here");
        }
    };
    PaybillprocessComponent.prototype.ResetForm = function () {
        this.form.resetForm();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], PaybillprocessComponent.prototype, "form", void 0);
    PaybillprocessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-paybillprocess',
            template: __webpack_require__(/*! ./paybillprocess.component.html */ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.html"),
            styles: [__webpack_require__(/*! ./paybillprocess.component.css */ "./src/app/bill-group-mgmt/paybillprocess/paybillprocess.component.css")],
            providers: [
                {
                    provide: _angular_material__WEBPACK_IMPORTED_MODULE_3__["DateAdapter"], useClass: _date_adapter__WEBPACK_IMPORTED_MODULE_6__["AppDateAdapter"]
                },
                {
                    provide: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DATE_FORMATS"], useValue: _date_adapter__WEBPACK_IMPORTED_MODULE_6__["APP_DATE_FORMATS"]
                }
            ]
        }),
        __metadata("design:paramtypes", [_services_PayBill_PayBillService__WEBPACK_IMPORTED_MODULE_1__["PayBillGroupService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__["MasterService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], PaybillprocessComponent);
    return PaybillprocessComponent;
}());

var ELEMENT_DeAttachdata = [];
var ELEMENT_DATA = [];


/***/ }),

/***/ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC90ZW1wb3JhcnlwYnJwcm9jZXNzL3RlbXBvcmFyeXBicnByb2Nlc3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12 mb-30\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Temporary PBR Process</div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Financial Year\" disabled>\r\n          <mat-option>2019-2020</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Pay Month\" disabled>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"w-100\">\r\n        <mat-select placeholder=\"Pay Bill Type\">\r\n          <mat-option>Regular Pay Bill - REG</mat-option>\r\n          <mat-option>Supplementary Bill</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"w-100\">\r\n        <mat-select placeholder=\"Pay Bill Type\">\r\n          <mat-option>ABC</mat-option>\r\n          <mat-option>XYZ</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Select Employees\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Checkbox Column -->\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                        [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                        [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\r\n                        [aria-label]=\"checkboxLabel()\">\r\n          </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                        (change)=\"$event ? selection.toggle(row) : null\"\r\n                        [checked]=\"selection.isSelected(row)\"\r\n                        [aria-label]=\"checkboxLabel(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"weight\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Symbol Column -->\r\n      <ng-container matColumnDef=\"symbol\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"empcode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Departmental Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empcode}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n          (click)=\"selection.toggle(row)\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n\r\n\r\n    <div class=\"col-lg-12 text-center attach-dattach-wraper\">\r\n      <button class=\"attach-btn mat-raised-button\"> Attach </button>\r\n      <button class=\"dattach-btn mat-raised-button\">  DeAttach  </button>\r\n    </div>\r\n\r\n\r\n    <!--table-2-->\r\n    <div class=\"fom-title\">Attached employees whose Temporary PBR to be processed</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Checkbox Column -->\r\n      <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                        [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                        [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\r\n                        [aria-label]=\"checkboxLabel()\">\r\n          </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                        (change)=\"$event ? selection.toggle(row) : null\"\r\n                        [checked]=\"selection.isSelected(row)\"\r\n                        [aria-label]=\"checkboxLabel(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"weight\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Symbol Column -->\r\n      <ng-container matColumnDef=\"symbol\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"empcode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Departmental Employee Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empcode}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n          (click)=\"selection.toggle(row)\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\">Process</button>\r\n      <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n    </div>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.ts ***!
  \**************************************************************************************/
/*! exports provided: TemporarypbrprocessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemporarypbrprocessComponent", function() { return TemporarypbrprocessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA = [
    { position: 1, name: 'Rahul Khanna', weight: 121, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Vinay Khanna', weight: 122, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Bhima Mathur', weight: 123, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Divyansh Kumar', weight: 124, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Shashi Kumar', weight: 125, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Ram', weight: 126, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Shyam', weight: 127, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Nitin', weight: 128, symbol: 'H', empcode: 'DEC1' },
    { position: 1, name: 'Toni', weight: 129, symbol: 'H', empcode: 'DEC1' },
];
var TemporarypbrprocessComponent = /** @class */ (function () {
    function TemporarypbrprocessComponent() {
        this.displayedColumns = ['select', 'position', 'weight', 'name', 'symbol', 'empcode'];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__["SelectionModel"](true, []);
    }
    /** Whether the number of selected elements matches the total number of rows. */
    TemporarypbrprocessComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    TemporarypbrprocessComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    /** The label for the checkbox on the passed row */
    TemporarypbrprocessComponent.prototype.checkboxLabel = function (row) {
        if (!row) {
            return (this.isAllSelected() ? 'select' : 'deselect') + " all";
        }
        return (this.selection.isSelected(row) ? 'deselect' : 'select') + " row " + (row.position + 1);
    };
    TemporarypbrprocessComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    TemporarypbrprocessComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], TemporarypbrprocessComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], TemporarypbrprocessComponent.prototype, "sort", void 0);
    TemporarypbrprocessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-temporarypbrprocess',
            template: __webpack_require__(/*! ./temporarypbrprocess.component.html */ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.html"),
            styles: [__webpack_require__(/*! ./temporarypbrprocess.component.css */ "./src/app/bill-group-mgmt/temporarypbrprocess/temporarypbrprocess.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TemporarypbrprocessComponent);
    return TemporarypbrprocessComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC91bmZyZWV6ZWFuZGRlbGV0ZWJpbGwvdW5mcmVlemVhbmRkZWxldGViaWxsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Delete Pay Bill </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Pay Bill Group\">\r\n          <mat-option>Select</mat-option>\r\n          <mat-option>ABC</mat-option>\r\n          <mat-option>XYZ</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Bill No.\">\r\n          <mat-option>201910_001(R)</mat-option>\r\n          <mat-option>201910_002(R)</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <textarea matInput placeholder=\"Remark\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper mb-30\">\r\n      <button type=\"submit\" class=\"btn btn-danger\">Delete</button>\r\n    </div>\r\n    <div class=\"fom-title\">Un- Freeze Pay Bill</div>\r\n    <div class=\"col-md-12 mb-20\">\r\n      <div class=\"col-md-4 mb-20\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Deleted Bill No\">\r\n            <mat-option>201910_001(R)</mat-option>\r\n            <mat-option>201910_002(R)</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      </div>\r\n      <div class=\"col-lg-12 \">\r\n        <div class=\"col-md-4 checkbox-wraper\">\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n        </div>\r\n\r\n        <div class=\"col-md-4 text-center\">\r\n          <div class=\"col-md-12 mt-10 mb-20\"><button type=\"button\" class=\"btn btn-primary btn-cons\">Select All</button></div>\r\n          <div class=\"col-md-12\"><button type=\"button\" class=\"btn btn-info btn-cons\">Un-Select All</button></div>\r\n        </div>\r\n\r\n        <div class=\"col-md-4 checkbox-wraper\">\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n          <mat-checkbox>Check me!</mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-warning btn-cons\">Un-Freeze</button>\r\n      </div>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.ts ***!
  \******************************************************************************************/
/*! exports provided: UnfreezeanddeletebillComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnfreezeanddeletebillComponent", function() { return UnfreezeanddeletebillComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UnfreezeanddeletebillComponent = /** @class */ (function () {
    function UnfreezeanddeletebillComponent() {
    }
    UnfreezeanddeletebillComponent.prototype.ngOnInit = function () {
    };
    UnfreezeanddeletebillComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-unfreezeanddeletebill',
            template: __webpack_require__(/*! ./unfreezeanddeletebill.component.html */ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.html"),
            styles: [__webpack_require__(/*! ./unfreezeanddeletebill.component.css */ "./src/app/bill-group-mgmt/unfreezeanddeletebill/unfreezeanddeletebill.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UnfreezeanddeletebillComponent);
    return UnfreezeanddeletebillComponent;
}());



/***/ }),

/***/ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JpbGwtZ3JvdXAtbWdtdC92aWV3YW5kZm9yd2FyZGJpbGwvdmlld2FuZGZvcndhcmRiaWxsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">View And Forward Paybill </div>\r\n\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n   <!--Table One-->\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr.No </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Scheme Code <br />(Bill Description)</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"weight\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Bill Id </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Symbol Column -->\r\n      <ng-container matColumnDef=\"schemecode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Bill Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.schemecode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"billbd\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Month </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.billbd}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"billtype\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Bill Status </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.billtype}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n          (click)=\"selection.toggle(row)\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button class=\"btn btn-primary\" type=\"button\">Forward to Checker</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<mat-accordion class=\"col-md-12 margin-top-10px\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Regular bill for the month of April,2019\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n    <!--Table Two-->\r\n    <table mat-table [dataSource]=\"dataSource2\" matSort class=\"even-odd-color\">\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"position\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr.No </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"name\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Select the Report to View/Print</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\">     </tr>\r\n\r\n    </table>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button class=\"btn btn-info\" type=\"button\">View/Print</button>\r\n    </div>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.ts ***!
  \************************************************************************************/
/*! exports provided: ViewandforwardbillComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewandforwardbillComponent", function() { return ViewandforwardbillComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA = [
    { position: 1, weight: 121, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
    { position: 2, weight: 122, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
    { position: 3, weight: 123, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
    { position: 4, weight: 124, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
];
var ELEMENT_DATA2 = [
    { position: 1, name: 'GAR 13 (Outer)', },
    { position: 1, name: 'MTR 13-A(Inner)', },
];
//Table 2 end
var ViewandforwardbillComponent = /** @class */ (function () {
    function ViewandforwardbillComponent() {
        this.displayedColumns = ['position', 'weight', 'name', 'schemecode', 'billbd', 'billtype',];
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        //Table 2
        this.displayedColumns2 = ['position', 'name'];
        this.dataSource2 = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA2);
        //Table 2 end
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__["SelectionModel"](true, []);
    }
    ViewandforwardbillComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource2.paginator = this.paginator;
        this.dataSource2.sort = this.sort;
    };
    ViewandforwardbillComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ViewandforwardbillComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material_sort__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], ViewandforwardbillComponent.prototype, "sort", void 0);
    ViewandforwardbillComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewandforwardbill',
            template: __webpack_require__(/*! ./viewandforwardbill.component.html */ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.html"),
            styles: [__webpack_require__(/*! ./viewandforwardbill.component.css */ "./src/app/bill-group-mgmt/viewandforwardbill/viewandforwardbill.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ViewandforwardbillComponent);
    return ViewandforwardbillComponent;
}());



/***/ }),

/***/ "./src/app/services/master/master.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/master/master.service.ts ***!
  \***************************************************/
/*! exports provided: MasterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MasterService", function() { return MasterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var MasterService = /** @class */ (function () {
    function MasterService(http, config) {
        this.http = http;
        this.config = config;
    }
    MasterService.prototype.getSalutation = function () {
        return this.http
            .get("" + this.config.api_base_url + this.config.getSalutation);
    };
    //getEmployeeType1(): Observable<EmployeeTypeMaster> {
    //  return this.http
    //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeType}`);
    //}
    MasterService.prototype.getEmployeeType = function (serviceId, deputId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('serviceId', serviceId).set('deputationId', deputId);
        return this.http
            .get("" + this.config.api_base_url + this.config.getEmployeeType, { params: params });
    };
    //getEmployeeSubType1(parentId: any): Observable<EmployeeTypeMaster> {
    //  debugger
    //  const params = new HttpParams().set('parentTypeID', parentId)
    //  return this.http
    //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`,{ params });
    //}
    MasterService.prototype.getEmployeeSubType = function (parentId, isDeput) {
        //  const params = new HttpParams().set('parentTypeID', parentId).set('IsDeput', isDeput);
        //   return this.http.get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`, { params });
        return this.http.get(this.config.api_base_url + this.config.getEmployeeSubType + '?parentTypeId=' + parentId + ' &IsDeput=' + isDeput, {});
    };
    //getJoiningMode1(): Observable<any> {
    //  return this.http
    //    .get<any>(`${this.config.api_base_url}${this.config.getJoiningMode}`);
    //}
    MasterService.prototype.getJoiningMode = function (isDeput, empSubTypeId) {
        return this.http.get(this.config.api_base_url + this.config.getJoiningMode + '?isDeput=' + isDeput + '&MsEmpSubTypeID=' + empSubTypeId, {});
    };
    MasterService.prototype.getJoiningCategory = function (parentId, isDeput) {
        return this.http.get(this.config.api_base_url + 'master/getJoiningCategory' + '?parentTypeId=' + parentId + '&isDeput=' + isDeput, {});
    };
    //getDeputationType1(): Observable<MasterModel> {
    //  return this.http
    //    .get<MasterModel>(`${this.config.api_base_url}${this.config.getDeputationType}`);
    //}
    MasterService.prototype.getDeputationType = function (serviceId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('serviceId', serviceId);
        return this.http
            .get("" + this.config.api_base_url + this.config.getDeputationType, { params: params });
    };
    //getServiceType1(): Observable<any> {
    //  return this.http
    //    .get<any>(`${this.config.api_base_url}${this.config.getServiceType}`);
    //}
    MasterService.prototype.getServiceType = function (isDeput) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('isDeput', isDeput);
        return this.http
            .get("" + this.config.api_base_url + this.config.getServiceType, { params: params });
    };
    MasterService.prototype.getGender = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getGender);
    };
    MasterService.prototype.getState = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetState);
    };
    MasterService.prototype.uploadFile = function (uploadFiles) {
        return this.http.post(this.config.api_base_url + this.config.uploadFiles, uploadFiles, { responseType: 'text' });
    };
    MasterService.prototype.getRelation = function () {
        return this.http
            .get("" + this.config.api_base_url + this.config.getRelation);
    };
    MasterService.prototype.GetAllDesignation = function (controllerId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getDesignationAll + controllerId));
    };
    MasterService.prototype.getJoiningAccountOf = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getJoiningAccountOf);
    };
    MasterService.prototype.getOfficeList = function (controllerId, ddoId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getOfficeList + '?controllerId= ' + controllerId + '&ddoId=' + ddoId));
    };
    MasterService.prototype.getOfficeCityClassList = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getOfficeCityClass);
    };
    MasterService.prototype.getPayCommissionListLien = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getPayCommissionListLien);
    };
    MasterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], MasterService);
    return MasterService;
}());



/***/ }),

/***/ "./src/app/services/promotions/promotions.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/promotions/promotions.service.ts ***!
  \***********************************************************/
/*! exports provided: PromotionsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsService", function() { return PromotionsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PromotionsService = /** @class */ (function () {
    function PromotionsService(http, config) {
        this.http = http;
        this.config = config;
    }
    PromotionsService.prototype.BindDropDownBillGroup = function () {
        // return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
        // return this.http.get('http://localhost:55424/api/PayBill/GetAccountHeads');
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetPayBillGroup?PermDdoId=00003");
    };
    PromotionsService.prototype.BindDropDownDesigGroup = function (selectedBilgrp) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignation?billGroupId=" + selectedBilgrp);
    };
    PromotionsService.prototype.BindDropDownEmpGroup = function (selectedDesig) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmp?desigId=" + selectedDesig + "&pageCode=Leaves");
    };
    PromotionsService.prototype.GetAllDesignation = function (controllerId) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetAllDesignationOfDepartment?id=" + controllerId);
    };
    PromotionsService.prototype.GetAllEmpWithDesignationDetails = function (suspendedEmployee, controllerId, ddoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('suspendedEmployees', suspendedEmployee.toString()).set('controllerId', "013").set('DDOId', ddoId); // controllerId);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmpWithDesignationDetails", { params: params });
    };
    PromotionsService.prototype.FetchEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetPromotionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdatePromotionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdatePromotionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeletePromotionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/" + employeeId);
    };
    PromotionsService.prototype.ForwardTransferDetailsToChecker = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/ForwardTransferDetailToChecker", obj, { responseType: 'text' });
    };
    PromotionsService.prototype.UpdateStatus = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateStatus", obj, { responseType: 'text' });
    };
    // Reversion API's
    PromotionsService.prototype.FetchReversionEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetReversionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdateReversionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateReversionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeleteReversionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/reversion/" + employeeId);
    };
    PromotionsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PromotionsService);
    return PromotionsService;
}());



/***/ })

}]);
//# sourceMappingURL=bill-group-mgmt-bill-group-mgmt-module.js.map