(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rejoiningofemployee-rejoiningofemployee-module"],{

/***/ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.css":
/*!***********************************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlam9pbmluZ29mZW1wbG95ZWUvcmVqb2luaW5nb2YtZW1wbG95ZWUtaGVhZGVyL3Jlam9pbmluZ29mLWVtcGxveWVlLWhlYWRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-10 col-lg-10 col-md-offset-1\">\r\n  <div class=\"basic-container select-drop-head\">\r\n    <form [formGroup]=\"headersForm\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"row  selection-hed\">\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"searchEmployeeType\" placeholder=\"Search Employees\">\r\n              <mat-option value=\"name\">Employee Name/Code</mat-option>\r\n              <mat-option value=\"pan\">Employee PAN No.</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\" *ngIf=\"headersForm.controls.searchEmployeeType.value == 'name'\">\r\n            <mat-select formControlName=\"searchEmployee\" placeholder=\"Employee Name and Code\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"searchEmployee\" [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"'Employee is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredEmp | async\" value=\"{{emp.msEmpId}}\">{{emp.empName}} ({{emp.empCd}})</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please Select a Employee</mat-error>\r\n          </mat-form-field>\r\n\r\n          <mat-form-field class=\"wid-100\" *ngIf=\"headersForm.controls.searchEmployeeType.value == 'pan'\">\r\n            <mat-select formControlName=\"searchEmployee\" placeholder=\"Employee PAN no.\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"searchEmployeePAN\" [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"'Employee is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredPAN | async\" value=\"{{emp.msEmpId}}\">{{emp.empPanNo}}</mat-option>\r\n            </mat-select>\r\n            <mat-error>Please Select a Employee</mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <button class=\"btn btn-success\" type=\"submit\">GO</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: RejoiningofEmployeeHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RejoiningofEmployeeHeaderComponent", function() { return RejoiningofEmployeeHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_rejoiningofEmployee_rejoiningofEmployee_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/rejoiningofEmployee/rejoiningofEmployee.service */ "./src/app/services/rejoiningofEmployee/rejoiningofEmployee.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../rejoiningofemployee/rejoiningofemployee.component */ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RejoiningofEmployeeHeaderComponent = /** @class */ (function () {
    function RejoiningofEmployeeHeaderComponent(_service, _rejoiningComponent) {
        this._service = _service;
        this._rejoiningComponent = _rejoiningComponent;
        this.employeeList = [];
        this.searchEmployee = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.searchEmployeePAN = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
        this.filteredPAN = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
    }
    RejoiningofEmployeeHeaderComponent.prototype.createForm = function () {
        this.headersForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            searchEmployeeType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('name'),
            searchEmployee: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
    };
    RejoiningofEmployeeHeaderComponent.prototype.resetForm = function () {
        this.createForm();
        this._rejoiningComponent.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"]();
        this._rejoiningComponent.totalCount = 0;
    };
    RejoiningofEmployeeHeaderComponent.prototype.getServiceEndEmployees = function () {
        var _this = this;
        this._service.GetServiceEndEmployees('', '', '').subscribe(function (response) {
            _this.employeeList = response;
            _this.filteredEmp.next(_this.employeeList);
            _this.filteredPAN.next(_this.employeeList);
        });
    };
    RejoiningofEmployeeHeaderComponent.prototype.onSubmit = function () {
        var _this = this;
        var employee = this.employeeList.filter(function (a) { return a.msEmpId == _this.headersForm.controls.searchEmployee.value; });
        if (employee.length > 0) {
            this._rejoiningComponent.setEmployeeDetails(employee[0]);
        }
    };
    RejoiningofEmployeeHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.getServiceEndEmployees();
        this.searchEmployee.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
        this.searchEmployeePAN.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmpPAN();
        });
    };
    RejoiningofEmployeeHeaderComponent.prototype.filterEmp = function () {
        if (!this.employeeList) {
            return;
        }
        // get the search keyword
        var search = this.searchEmployee.value;
        if (!search) {
            this.filteredEmp.next(this.employeeList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredEmp.next(this.employeeList.filter(function (emp) { return emp.empName.toLowerCase().indexOf(search) > -1 || emp.empCd.toLowerCase().indexOf(search) > -1; }));
    };
    RejoiningofEmployeeHeaderComponent.prototype.filterEmpPAN = function () {
        if (!this.employeeList) {
            return;
        }
        // get the search keyword
        var search = this.searchEmployeePAN.value;
        if (!search) {
            this.filteredPAN.next(this.employeeList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredPAN.next(this.employeeList.filter(function (emp) { return emp.empPanNo ? emp.empPanNo.toLowerCase().indexOf(search) > -1 : false; }));
    };
    RejoiningofEmployeeHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rejoiningof-employee-header',
            template: __webpack_require__(/*! ./rejoiningof-employee-header.component.html */ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.html"),
            styles: [__webpack_require__(/*! ./rejoiningof-employee-header.component.css */ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.css")]
        }),
        __metadata("design:paramtypes", [_services_rejoiningofEmployee_rejoiningofEmployee_service__WEBPACK_IMPORTED_MODULE_2__["RejoiningOfEmployeeService"], _rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_5__["RejoiningofemployeeComponent"]])
    ], RejoiningofEmployeeHeaderComponent);
    return RejoiningofEmployeeHeaderComponent;
}());



/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningofemployee-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningofemployee-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: RejoiningofemployeeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RejoiningofemployeeRoutingModule", function() { return RejoiningofemployeeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _rejoiningofemployee_rejoiningofemployee_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rejoiningofemployee/rejoiningofemployee.module */ "./src/app/rejoiningofemployee/rejoiningofemployee.module.ts");
/* harmony import */ var _rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rejoiningofemployee/rejoiningofemployee.component */ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _rejoiningofemployee_rejoiningofemployee_module__WEBPACK_IMPORTED_MODULE_2__["RejoiningofemployeeModule"], children: [
            {
                path: 'rejoining', component: _rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_3__["RejoiningofemployeeComponent"]
            }
        ]
    }
];
var RejoiningofemployeeRoutingModule = /** @class */ (function () {
    function RejoiningofemployeeRoutingModule() {
    }
    RejoiningofemployeeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RejoiningofemployeeRoutingModule);
    return RejoiningofemployeeRoutingModule;
}());



/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningofemployee.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningofemployee.module.ts ***!
  \*******************************************************************/
/*! exports provided: RejoiningofemployeeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RejoiningofemployeeModule", function() { return RejoiningofemployeeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rejoiningofemployee_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./rejoiningofemployee-routing.module */ "./src/app/rejoiningofemployee/rejoiningofemployee-routing.module.ts");
/* harmony import */ var _rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rejoiningofemployee/rejoiningofemployee.component */ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.ts");
/* harmony import */ var _rejoiningof_employee_header_rejoiningof_employee_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rejoiningof-employee-header/rejoiningof-employee-header.component */ "./src/app/rejoiningofemployee/rejoiningof-employee-header/rejoiningof-employee-header.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var RejoiningofemployeeModule = /** @class */ (function () {
    function RejoiningofemployeeModule() {
    }
    RejoiningofemployeeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_rejoiningofemployee_rejoiningofemployee_component__WEBPACK_IMPORTED_MODULE_5__["RejoiningofemployeeComponent"], _rejoiningof_employee_header_rejoiningof_employee_header_component__WEBPACK_IMPORTED_MODULE_6__["RejoiningofEmployeeHeaderComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_2__["MaterialModule"],
                _rejoiningofemployee_routing_module__WEBPACK_IMPORTED_MODULE_4__["RejoiningofemployeeRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__["NgxMatSelectSearchModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]
            ],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_9__["CommonMsg"]]
        })
    ], RejoiningofemployeeModule);
    return RejoiningofemployeeModule;
}());







/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n.mat-form-field {\r\n  font-size: 12px;\r\n  width: 100%;\r\n}\r\n\r\ntd, th {\r\n  width: 25%;\r\n  border-bottom:0px!important;\r\n  margin:5px;\r\n}\r\n \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVqb2luaW5nb2ZlbXBsb3llZS9yZWpvaW5pbmdvZmVtcGxveWVlL3Jlam9pbmluZ29mZW1wbG95ZWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGdCQUFnQjtFQUNoQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsNEJBQTRCO0VBQzVCLFdBQVc7Q0FDWiIsImZpbGUiOiJzcmMvYXBwL3Jlam9pbmluZ29mZW1wbG95ZWUvcmVqb2luaW5nb2ZlbXBsb3llZS9yZWpvaW5pbmdvZmVtcGxveWVlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG50ZCwgdGgge1xyXG4gIHdpZHRoOiAyNSU7XHJcbiAgYm9yZGVyLWJvdHRvbTowcHghaW1wb3J0YW50O1xyXG4gIG1hcmdpbjo1cHg7XHJcbn1cclxuIFxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--------------- Left Form Start --------------->\r\n<app-rejoiningof-employee-header #header></app-rejoiningof-employee-header>\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <!--------------- Employee Details Start --------------->\r\n  <mat-card class=\"example-card\">\r\n    <div class=\"fom-title\">\r\n      Employee Details\r\n    </div>\r\n    <form [formGroup]=\"rejoiningForm\" (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"col-sm-12 col-md-7 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Employee Name\" formControlName=\"empName\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Employee Type\" formControlName=\"currentType\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker\" formControlName=\"empDOB\" placeholder=\"Date of Birth\" [disabled]=\"true\" autocomplete=off>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Gender\" formControlName=\"empGender\" [disabled]=\"true\">\r\n            <mat-option value=\"M\">Male</mat-option>\r\n            <mat-option value=\"F\">Female</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"PAN No.\" formControlName=\"empPanNo\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"End of Service Date\" formControlName=\"endServDt\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Reason for End of Service\" formControlName=\"empEndReason\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--------------- Re-Joining Details Start --------------->\r\n      <div class=\"fom-title\">\r\n        Re-Joining Details\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 combo-col\">\r\n        <div class=\"col-md-12 col-lg-5 pading-0\">\r\n          <label>Consider Previous Govt Service</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-4\">\r\n          <mat-radio-group formControlName=\"isPreviousGovService\" (change)=\"setPreviousJoiningDetails($event)\">\r\n            <mat-radio-button value=\"y\">Yes</mat-radio-button>\r\n            <mat-radio-button value=\"n\">No</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Employee Type\" formControlName=\"reJoiningEmployeeTypeId\" [disabled]=\"(rejoiningForm && rejoiningForm.controls.isPreviousGovService.value == 'y') || infoMode || roleId == 5\">\r\n            <mat-option value=\"1\">Regular</mat-option>\r\n            <mat-option value=\"2\">Co-Terminus</mat-option>\r\n            <mat-option value=\"33\">Re-Employeer Pensioner</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"picker3.open()\" [matDatepicker]=\"picker3\" placeholder=\"Date of Re-Joining\" formControlName=\"empRejoiningDate\" [disabled]=\"(rejoiningForm && rejoiningForm.controls.isPreviousGovService.value == 'y') || infoMode || roleId == 5\" autocomplete=off readonly>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker3\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker3></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Time\" formControlName=\"empJoiningTime\" [disabled]=\"(rejoiningForm && rejoiningForm.controls.isPreviousGovService.value == 'y') || infoMode || roleId == 5\">\r\n            <mat-option value=\"B\">Forenoon</mat-option>\r\n            <mat-option value=\"A\">Afternoon</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"mat-card\" *ngIf=\"rejoiningForm.controls.reJoiningEmployeeTypeId.value == 33\">\r\n        <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Pension Amount\" formControlName=\"pensionAmount\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"DA taken from\" formControlName=\"daTakenFrom\">\r\n              <mat-option value=\"Parent\">Parent DDO</mat-option>\r\n              <mat-option value=\"Current\">Current DDO</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"NOC Order No.\" formControlName=\"nocOrderNo\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput (click)=\"picker5.open()\" [matDatepicker]=\"picker5\" placeholder=\"NOC Order Date\" formControlName=\"nocOrderDt\" autocomplete=off readonly>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker5\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker5></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <!--------------- Re-Joining Details End --------------->\r\n      <!--------------- Order Details Start --------------->\r\n      <div class=\"fom-title\">\r\n        Order Details\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order No.\" formControlName=\"orderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput (click)=\"picker4.open()\" [matDatepicker]=\"picker4\" placeholder=\"Order Date\" formControlName=\"orderDt\" readonly>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker4\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker4></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\" formControlName=\"remark\">\r\n      </textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-12\" *ngIf=\"rejoiningForm.controls.verifFlag.value == 'R' || rejoiningForm.controls.verifFlag.value == 'V'\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remark from DDO Checker\" formControlName=\"reasonForRej\" readonly>\r\n      </textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"rejoiningForm.controls.empId.value == 0 || infoMode\">Save</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm();header.resetForm()\" [disabled]=\"rejoiningForm.controls.empId.value == 0\">Cancel</button>\r\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"changeStatus('P')\" [disabled]=\"rejoiningForm.controls.empId.value == 0 || !editMode || infoMode\">Forward to Checker</button>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 5\">\r\n        <button type=\"button\" class=\"btn btn-success\" [disabled]=\"(rejoiningForm.controls.verifFlag.value == 'V' && infoMode) || rejoiningForm.controls.msEmpReinstateDetID.value == 0\" (click)=\"ApprovePopup = !ApprovePopup\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" [disabled]=\"(rejoiningForm.controls.verifFlag.value == 'V' && infoMode) || rejoiningForm.controls.msEmpReinstateDetID.value == 0\" (click)=\"RejectPopup = !RejectPopup\">Reject</button>\r\n      </div>\r\n\r\n    </form>\r\n    <!--------------- Order Details End --------------->\r\n\r\n  </mat-card>\r\n\r\n  <!--------------- Employee Details End --------------->\r\n\r\n</div>\r\n<!---Left Form End--->\r\n<!----Right Form Start-->\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card tabel-wraper even-odd-color\">\r\n    <div class=\"fom-title\">\r\n      Re-Joining of End of Service Employee Details\r\n    </div>\r\n    <!--\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n        -->\r\n    <mat-form-field class=\"col-md-10\">\r\n      <input matInput (keyup)=\"applyFilter()\" [(ngModel)]=\"searchTerm\" placeholder=\"Filter\"> <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"totalCount > 0\">\r\n      <!-- ID Column -->\r\n      <ng-container matColumnDef=\"OrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order No. </th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.orderNo}} </td>\r\n      </ng-container>\r\n      <!-- Progress Column -->\r\n      <ng-container matColumnDef=\"OrderDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date</th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.orderDt | date: 'dd-MM-yyyy'}}</td>\r\n      </ng-container>\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"EmployeeType\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Type </th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.rejoiningType}} </td>\r\n      </ng-container>\r\n      <!-- Color Column -->\r\n      <ng-container matColumnDef=\"EndofServiceDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> End of Service Date </th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.endServDt | date: 'dd-MM-yyyy'}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PANNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> PAN No. </th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.empPanNo}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let row\"> {{row.status}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action</th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <i class=\"material-icons i-info\" matTooltip=\"Information\" (click)=\"formInfo(row)\">info</i>\r\n          <i class=\"material-icons i-edit\" (click)=\"editForm(row)\" matTooltip=\"Edit\" *ngIf=\"row.verifFlag != 'P' && row.verifFlag != 'V' && roleId == 6\">edit</i>\r\n          <i class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteRejoiningDetails(row);DeletePopup = !DeletePopup\" *ngIf=\"row.verifFlag != 'P' && row.verifFlag != 'V' && roleId == 6\">delete</i>\r\n        </td>\r\n      </ng-container>\r\n      <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n      <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </table>\r\n    <mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10, 25, 100]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n    <mat-toolbar color=\"warning\" style=\"text-align:center;display:inline-block\" *ngIf=\"totalCount == 0\">Record is not found</mat-toolbar>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"reasonForRej\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"changeStatus('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"reasonForRej\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"changeStatus('R')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.ts ***!
  \******************************************************************************************/
/*! exports provided: RejoiningofemployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RejoiningofemployeeComponent", function() { return RejoiningofemployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_rejoiningofEmployee_rejoiningofEmployee_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/rejoiningofEmployee/rejoiningofEmployee.service */ "./src/app/services/rejoiningofEmployee/rejoiningofEmployee.service.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RejoiningofemployeeComponent = /** @class */ (function () {
    function RejoiningofemployeeComponent(_service, snackBar, _msg) {
        this._service = _service;
        this.snackBar = snackBar;
        this._msg = _msg;
        this.displayedColumns = ['OrderNo', 'OrderDate', 'EmployeeType', 'EndofServiceDate', 'PANNo', 'Status', 'Action'];
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.editMode = false;
        this.infoMode = false;
        this.ddoId = sessionStorage.getItem('ddoid');
        this.roleId = sessionStorage.getItem('userRoleID');
    }
    RejoiningofemployeeComponent.prototype.createForm = function () {
        this.rejoiningForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            msEmpReinstateDetID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            isPreviousGovService: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('n'),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            empRejoiningDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empJoiningTime: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            pensionAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            reJoiningEmployeeType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            reJoiningEmployeeTypeId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            toDDOId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            daTakenFrom: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            nocOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            nocOrderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            orderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            orderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            remark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            verifFlag: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empPanNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            endServDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empGender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empDOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empEndReason: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            empTypeID: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            currentType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            reasonForRej: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        if (this.roleId == '5') {
            this.rejoiningForm.disable();
        }
    };
    RejoiningofemployeeComponent.prototype.setEmployeeDetails = function (employee) {
        this.employeeDetails = employee;
        if (this.roleId == '6') {
            this.rejoiningForm.enable();
        }
        this.createForm();
        this.editMode = false;
        this.infoMode = false;
        this.rejoiningForm.controls.empCd.setValue(this.employeeDetails.empCd);
        this.rejoiningForm.controls.empId.setValue(this.employeeDetails.msEmpId);
        this.rejoiningForm.controls.empPanNo.setValue(this.employeeDetails.empPanNo);
        this.rejoiningForm.controls.endServDt.setValue(this.employeeDetails.endServDt);
        this.rejoiningForm.controls.empName.setValue(this.employeeDetails.empName);
        this.rejoiningForm.controls.empGender.setValue(this.employeeDetails.empGender);
        this.rejoiningForm.controls.empDOB.setValue(this.employeeDetails.empDOB);
        this.rejoiningForm.controls.empEndReason.setValue(this.employeeDetails.empEndReason);
        this.rejoiningForm.controls.empTypeID.setValue(this.employeeDetails.empApptTypeId);
        this.rejoiningForm.controls.currentType.setValue(this.employeeDetails.empType);
        this.getRejoiningDetails();
    };
    RejoiningofemployeeComponent.prototype.setPreviousJoiningDetails = function (evnt) {
        if (evnt.value == 'y') {
            if (this.employeeDetails) {
                this.rejoiningForm.controls.empRejoiningDate.setValue(this.employeeDetails.joiningDate);
                this.rejoiningForm.controls.empJoiningTime.setValue(this.employeeDetails.joiningTiming);
                this.rejoiningForm.controls.reJoiningEmployeeTypeId.setValue(this.employeeDetails.empApptTypeId.toString());
            }
        }
        else {
            this.rejoiningForm.controls.empRejoiningDate.setValue('');
            this.rejoiningForm.controls.empJoiningTime.setValue('');
            this.rejoiningForm.controls.reJoiningEmployeeTypeId.setValue(0);
        }
    };
    RejoiningofemployeeComponent.prototype.onSubmit = function () {
        var _this = this;
        this._service.UpsertReJoiningServiceDetails(this.rejoiningForm.value).subscribe(function (result) {
            var response = result;
            if (response) {
                if (response > 0) {
                    _this.rejoiningForm.reset();
                    _this.snackBar.open(_this._msg.saveMsg, null, { duration: 4000 });
                    _this.formGroupDirective.resetForm();
                    _this.createForm();
                    _this.getRejoiningDetails();
                }
                if (response == -1) {
                    _this.snackBar.open(_this._msg.saveFailedMsg, null, { duration: 4000 });
                }
            }
        });
    };
    RejoiningofemployeeComponent.prototype.getRejoiningDetails = function () {
        var _this = this;
        this.totalCount = 0;
        this._service.GetReJoiningServiceEmployees(this.pageNumber, this.pageSize, this.searchTerm, this.employeeDetails.empCd, this.roleId).subscribe(function (response) {
            if (response.length > 0) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](response);
                _this.totalCount = response[0].totalCount;
                _this.dataSource.sort = _this.sort;
            }
        });
    };
    RejoiningofemployeeComponent.prototype.getPaginationData = function (event) {
        this.pageSize = event.pageSize;
        this.pageNumber = event.pageIndex + 1;
        console.log(this.pageNumber);
        this.getRejoiningDetails();
    };
    RejoiningofemployeeComponent.prototype.cancelForm = function () {
        this.rejoiningForm.enable();
        this.rejoiningForm.reset();
        this.formGroupDirective.resetForm();
        this.createForm();
        this.employeeDetails = null;
        this.elementToBeDeleted = null;
        this.editMode = false;
        this.infoMode = false;
    };
    RejoiningofemployeeComponent.prototype.editForm = function (element) {
        this.rejoiningForm.setValue({
            msEmpReinstateDetID: element.msEmpReinstateDetID,
            isPreviousGovService: element.isPreviousGovService,
            empCd: element.empCd,
            empId: element.msEmpID,
            empRejoiningDate: element.empRejoiningDt,
            empJoiningTime: element.empJoiningTime,
            pensionAmount: element.pensionAmount,
            reJoiningEmployeeType: element.relApptType,
            reJoiningEmployeeTypeId: element.reEmpTypeID.toString(),
            toDDOId: this.ddoId,
            daTakenFrom: element.daTakenFrom,
            nocOrderNo: element.nocOrderNo,
            nocOrderDt: element.nocOrderDt,
            orderNo: element.orderNo,
            orderDt: element.orderDt,
            remark: element.remark,
            verifFlag: element.verifFlag,
            empPanNo: element.empPanNo,
            endServDt: element.endServDt,
            empName: element.empName,
            empGender: element.empGender,
            empDOB: element.empDOB,
            empEndReason: element.empEndReason,
            empTypeID: element.empTypeID,
            currentType: element.currentType,
            reasonForRej: element.reasonForRej
        });
        this.rejoiningForm.enable();
        this.editMode = true;
        this.infoMode = false;
    };
    RejoiningofemployeeComponent.prototype.formInfo = function (element) {
        this.rejoiningForm.setValue({
            msEmpReinstateDetID: element.msEmpReinstateDetID,
            isPreviousGovService: element.isPreviousGovService,
            empCd: element.empCd,
            empId: element.msEmpID,
            empRejoiningDate: element.empRejoiningDt,
            empJoiningTime: element.empJoiningTime,
            pensionAmount: element.pensionAmount,
            reJoiningEmployeeType: element.relApptType,
            reJoiningEmployeeTypeId: element.reEmpTypeID.toString(),
            toDDOId: this.ddoId,
            daTakenFrom: element.daTakenFrom,
            nocOrderNo: element.nocOrderNo,
            nocOrderDt: element.nocOrderDt,
            orderNo: element.orderNo,
            orderDt: element.orderDt,
            remark: element.remark,
            verifFlag: element.verifFlag,
            empPanNo: element.empPanNo,
            endServDt: element.endServDt,
            empName: element.empName,
            empGender: element.empGender,
            empDOB: element.empDOB,
            empEndReason: element.empEndReason,
            empTypeID: element.empTypeID,
            currentType: element.currentType,
            reasonForRej: element.reasonForRej
        });
        this.rejoiningForm.disable();
        this.infoMode = true;
        this.editMode = false;
    };
    RejoiningofemployeeComponent.prototype.DeleteRejoiningDetails = function (element) {
        this.elementToBeDeleted = element;
    };
    RejoiningofemployeeComponent.prototype.Cancel = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
        this.reasonForRej = '';
    };
    RejoiningofemployeeComponent.prototype.confirmDelete = function () {
        var _this = this;
        var obj = {
            msEmpReinstateDetID: this.elementToBeDeleted.msEmpReinstateDetID,
            empCd: this.elementToBeDeleted.empCd,
            toDDOId: this.ddoId,
            ipAddress: ""
        };
        this._service.DeleteReJoiningServiceDetails(obj).subscribe(function (result) {
            _this.snackBar.open(_this._msg.deleteMsg, null, { duration: 4000 });
            _this.pageNumber = 1;
            _this.paginator.firstPage();
            _this.dataSource.paginator = _this.paginator;
            _this.getRejoiningDetails();
            jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
            _this.reasonForRej = '';
            _this.elementToBeDeleted = null;
            _this.cancelForm();
        });
    };
    RejoiningofemployeeComponent.prototype.changeStatus = function (status) {
        var _this = this;
        var obj = {
            msEmpReinstateDetID: this.rejoiningForm.controls.msEmpReinstateDetID.value,
            empCd: this.rejoiningForm.controls.empCd.value,
            toDDOId: this.ddoId,
            ipAddress: "",
            status: status,
            reason: this.reasonForRej
        };
        this._service.UpdateReJoiningStatusDetails(obj).subscribe(function (response) {
            _this.rejoiningForm.reset();
            _this.snackBar.open(_this._msg.updateMsg, null, { duration: 4000 });
            _this.formGroupDirective.resetForm();
            _this.createForm();
            _this.getRejoiningDetails();
            jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
            _this.reasonForRej = '';
        });
    };
    RejoiningofemployeeComponent.prototype.applyFilter = function () {
        this.pageNumber = 1;
        this.getRejoiningDetails();
        this.paginator.firstPage();
        this.dataSource.paginator = this.paginator;
    };
    RejoiningofemployeeComponent.prototype.ngOnInit = function () {
        this.createForm();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], RejoiningofemployeeComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], RejoiningofemployeeComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], RejoiningofemployeeComponent.prototype, "formGroupDirective", void 0);
    RejoiningofemployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./rejoiningofemployee.component.html */ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.html"),
            styles: [__webpack_require__(/*! ./rejoiningofemployee.component.css */ "./src/app/rejoiningofemployee/rejoiningofemployee/rejoiningofemployee.component.css")]
        }),
        __metadata("design:paramtypes", [_services_rejoiningofEmployee_rejoiningofEmployee_service__WEBPACK_IMPORTED_MODULE_3__["RejoiningOfEmployeeService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], RejoiningofemployeeComponent);
    return RejoiningofemployeeComponent;
}());

//export interface UserData {
//  OrderNo: number;
//  OrderDate: number;
//  EmployeeType: string;
//  EndofServiceDate: number;
//  PANNo: number;
//}
///** Constants used to fill up our data base. */
//const NAMES: UserData[] = [
//  { OrderNo: 1, OrderDate: 2, EmployeeType: '3', EndofServiceDate: 4, PANNo: 343 },
//  { OrderNo: 34, OrderDate: 56, EmployeeType: 'kasjdlaf', EndofServiceDate: 56, PANNo: 556 },
//  { OrderNo: 1, OrderDate: 77, EmployeeType: 'asdfladk', EndofServiceDate: 77, PANNo: 34 },
//  { OrderNo: 777, OrderDate: 2, EmployeeType: 'jjoolkkk', EndofServiceDate: 11, PANNo: 34343 },
//  { OrderNo: 55, OrderDate: 45, EmployeeType: '3', EndofServiceDate: 33, PANNo: 333 },
//  { OrderNo: 33, OrderDate: 12, EmployeeType: '3', EndofServiceDate: 12, PANNo: 3434222 },
//  { OrderNo: 888, OrderDate: 21, EmployeeType: '3', EndofServiceDate: 88, PANNo: 34322 }
//];
/**
 * @title Data table with sorting, pagination, and filtering.
 */
/** Builds and returns a new User. */


/***/ }),

/***/ "./src/app/services/rejoiningofEmployee/rejoiningofEmployee.service.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/services/rejoiningofEmployee/rejoiningofEmployee.service.ts ***!
  \*****************************************************************************/
/*! exports provided: RejoiningOfEmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RejoiningOfEmployeeService", function() { return RejoiningOfEmployeeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var RejoiningOfEmployeeService = /** @class */ (function () {
    function RejoiningOfEmployeeService(http, config) {
        this.http = http;
        this.config = config;
    }
    RejoiningOfEmployeeService.prototype.GetServiceEndEmployees = function (empCode, empName, empPanNo) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('empCode', empCode).set('empName', empName).set('empPanNo', empPanNo); // controllerId);
        return this.http.get("" + this.config.api_base_url + this.config.ReJoiningServiceControllerName + "/GetServiceEndEmployees", { params: params });
    };
    RejoiningOfEmployeeService.prototype.GetReJoiningServiceEmployees = function (pageNumber, pageSize, SearchTerm, empCd, roleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('pageNumber', pageNumber.toString()).set('pageSize', pageSize.toString()).set('SearchTerm', SearchTerm.toString()).set('empCd', empCd.toString()).set('roleId', roleId); // controllerId);
        return this.http.get("" + this.config.api_base_url + this.config.ReJoiningServiceControllerName + "/GetReJoiningServiceEmployees", { params: params });
    };
    RejoiningOfEmployeeService.prototype.UpsertReJoiningServiceDetails = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.ReJoiningServiceControllerName + "/UpsertReJoiningServiceDetails", obj, { responseType: 'text' });
    };
    RejoiningOfEmployeeService.prototype.DeleteReJoiningServiceDetails = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.ReJoiningServiceControllerName + "/DeleteReJoiningServiceDetails", obj, { responseType: 'text' });
    };
    RejoiningOfEmployeeService.prototype.UpdateReJoiningStatusDetails = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.ReJoiningServiceControllerName + "/UpdateReJoiningStatusDetails", obj, { responseType: 'text' });
    };
    RejoiningOfEmployeeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], RejoiningOfEmployeeService);
    return RejoiningOfEmployeeService;
}());



/***/ })

}]);
//# sourceMappingURL=rejoiningofemployee-rejoiningofemployee-module.js.map