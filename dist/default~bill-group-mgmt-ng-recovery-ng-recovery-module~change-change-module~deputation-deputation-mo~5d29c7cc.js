(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc"],{

/***/ "./node_modules/file-saver/dist/FileSaver.min.js":
/*!*******************************************************!*\
  !*** ./node_modules/file-saver/dist/FileSaver.min.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function(a,b){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (b),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}})(this,function(){"use strict";function b(a,b){return"undefined"==typeof b?b={autoBom:!1}:"object"!=typeof b&&(console.warn("Deprecated: Expected third argument to be a object"),b={autoBom:!b}),b.autoBom&&/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)?new Blob(["\uFEFF",a],{type:a.type}):a}function c(b,c,d){var e=new XMLHttpRequest;e.open("GET",b),e.responseType="blob",e.onload=function(){a(e.response,c,d)},e.onerror=function(){console.error("could not download file")},e.send()}function d(a){var b=new XMLHttpRequest;b.open("HEAD",a,!1);try{b.send()}catch(a){}return 200<=b.status&&299>=b.status}function e(a){try{a.dispatchEvent(new MouseEvent("click"))}catch(c){var b=document.createEvent("MouseEvents");b.initMouseEvent("click",!0,!0,window,0,0,0,80,20,!1,!1,!1,!1,0,null),a.dispatchEvent(b)}}var f="object"==typeof window&&window.window===window?window:"object"==typeof self&&self.self===self?self:"object"==typeof global&&global.global===global?global:void 0,a=f.saveAs||("object"!=typeof window||window!==f?function(){}:"download"in HTMLAnchorElement.prototype?function(b,g,h){var i=f.URL||f.webkitURL,j=document.createElement("a");g=g||b.name||"download",j.download=g,j.rel="noopener","string"==typeof b?(j.href=b,j.origin===location.origin?e(j):d(j.href)?c(b,g,h):e(j,j.target="_blank")):(j.href=i.createObjectURL(b),setTimeout(function(){i.revokeObjectURL(j.href)},4E4),setTimeout(function(){e(j)},0))}:"msSaveOrOpenBlob"in navigator?function(f,g,h){if(g=g||f.name||"download","string"!=typeof f)navigator.msSaveOrOpenBlob(b(f,h),g);else if(d(f))c(f,g,h);else{var i=document.createElement("a");i.href=f,i.target="_blank",setTimeout(function(){e(i)})}}:function(a,b,d,e){if(e=e||open("","_blank"),e&&(e.document.title=e.document.body.innerText="downloading..."),"string"==typeof a)return c(a,b,d);var g="application/octet-stream"===a.type,h=/constructor/i.test(f.HTMLElement)||f.safari,i=/CriOS\/[\d]+/.test(navigator.userAgent);if((i||g&&h)&&"object"==typeof FileReader){var j=new FileReader;j.onloadend=function(){var a=j.result;a=i?a:a.replace(/^data:[^;]*;/,"data:attachment/file;"),e?e.location.href=a:location=a,e=null},j.readAsDataURL(a)}else{var k=f.URL||f.webkitURL,l=k.createObjectURL(a);e?e.location=l:location.href=l,e=null,setTimeout(function(){k.revokeObjectURL(l)},4E4)}});f.saveAs=a.saveAs=a,"undefined"!=typeof module&&(module.exports=a)});

//# sourceMappingURL=FileSaver.min.js.map

/***/ }),

/***/ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvYWR2LWJpbGwtcHJvYy9hZHYtYmlsbC1wcm9jLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.html":
/*!********************************************************************!*\
  !*** ./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  adv-bill-proc works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.ts ***!
  \******************************************************************/
/*! exports provided: AdvBillProcComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvBillProcComponent", function() { return AdvBillProcComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdvBillProcComponent = /** @class */ (function () {
    function AdvBillProcComponent() {
    }
    AdvBillProcComponent.prototype.ngOnInit = function () {
    };
    AdvBillProcComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-adv-bill-proc',
            template: __webpack_require__(/*! ./adv-bill-proc.component.html */ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.html"),
            styles: [__webpack_require__(/*! ./adv-bill-proc.component.css */ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdvBillProcComponent);
    return AdvBillProcComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvY2huZy1sYXN0LWluc3QtcGFpZC9jaG5nLWxhc3QtaW5zdC1wYWlkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  chng-last-inst-paid works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.ts ***!
  \******************************************************************************/
/*! exports provided: ChngLastInstPaidComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChngLastInstPaidComponent", function() { return ChngLastInstPaidComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChngLastInstPaidComponent = /** @class */ (function () {
    function ChngLastInstPaidComponent() {
    }
    ChngLastInstPaidComponent.prototype.ngOnInit = function () {
    };
    ChngLastInstPaidComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chng-last-inst-paid',
            template: __webpack_require__(/*! ./chng-last-inst-paid.component.html */ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.html"),
            styles: [__webpack_require__(/*! ./chng-last-inst-paid.component.css */ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ChngLastInstPaidComponent);
    return ChngLastInstPaidComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/loanmgt/chng-loan-type/chng-loan-type.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvY2huZy1sb2FuLXR5cGUvY2huZy1sb2FuLXR5cGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/loanmgt/chng-loan-type/chng-loan-type.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  chng-loan-type works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/loanmgt/chng-loan-type/chng-loan-type.component.ts ***!
  \********************************************************************/
/*! exports provided: ChngLoanTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChngLoanTypeComponent", function() { return ChngLoanTypeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChngLoanTypeComponent = /** @class */ (function () {
    function ChngLoanTypeComponent() {
    }
    ChngLoanTypeComponent.prototype.ngOnInit = function () {
    };
    ChngLoanTypeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chng-loan-type',
            template: __webpack_require__(/*! ./chng-loan-type.component.html */ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.html"),
            styles: [__webpack_require__(/*! ./chng-loan-type.component.css */ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ChngLoanTypeComponent);
    return ChngLoanTypeComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-tooltip {\r\n  color: yellow !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbm1ndC9jb21wLWFkdi12ZXJpZnkvY29tcC1hZHYtdmVyaWZ5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7Q0FDMUIiLCJmaWxlIjoic3JjL2FwcC9sb2FubWd0L2NvbXAtYWR2LXZlcmlmeS9jb21wLWFkdi12ZXJpZnkuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtdG9vbHRpcCB7XHJcbiAgY29sb3I6IHllbGxvdyAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.html":
/*!************************************************************************!*\
  !*** ./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"empstatus\">\r\n<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [data]=\"callTypevar\"></app-comman-mst>\r\n</div>\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form (ngSubmit)=\"NewLaon.valid && btnsubmit()\" #NewLaon=\"ngForm\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Employe Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empFullName\" placeholder=\"User name\" #empFullName=\"ngModel\" [(ngModel)]=\"_loandetails.empFullName\" disabled='true'>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empApptType\" placeholder=\"Post Held\" #empApptType=\"ngModel\" [(ngModel)]=\"_loandetails.empApptType\" disabled='true'>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"pay_basic\" placeholder=\"Present Pay as defined in Rule 4(b) and scale of pay\"\r\n                 [(ngModel)]=\"_loandetails.pay_basic\" #pay_basic=\"ngModel\" disabled='true'>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6 combo-col-2\">\r\n        <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n          <label>Whether governed by Pension Rules</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-5\">\r\n          <mat-radio-group [(ngModel)]=\"_loandetails.empPfType\" name=\"empPfType\" #empPfType=\"ngModel\" [disabled]=disbleflag disabled='true'>\r\n            <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n            <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"desigDesc\" placeholder=\"Designation\" #desigDesc=\"ngModel\" [(ngModel)]=\"_loandetails.desigDesc\" [disabled]=disbleflag disabled='true'>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <!--<mat-form-field class=\"wid-100\">\r\n      <input matInput [matDatepicker]=\"picker\" placeholder=\"Date of superannuation or retirement or date of expiry of contract in case of a Contract Officer :\"\r\n             #empSupanDt=\"ngModel\" [(ngModel)]=\"_loandetails.empSupanDt \" name=\"empSupanDt\" disabled='true'>\r\n    </mat-form-field>-->\r\n\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"empSupanDt\" (click)=\"empSupanDt.open()\" [(ngModel)]=\"_loandetails.empSupanDt  \" name=\"empSupanDt\" #empSupanDt=\"ngModel\"\r\n                 placeholder=\"Date of superannuation or retirement or date of expiry of contract in case of a Contract Officer\" readonly [disabled]=disbleflag required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"empSupanDt\"></mat-datepicker-toggle>\r\n          <mat-datepicker #empSupanDt></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!empSupanDt.errors?.required\">Date of superannuation or retirement or date of expiry is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n\r\n\r\n\r\n      </div>\r\n      <div class=\"fom-title\">Loan Application</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\" (selectionChange)=\"getLoanpurposeSelectedPayloanCode($event.value)\"\r\n                      [(ngModel)]=\"_loandetails.payLoanRefLoanCD\" name=\"payLoanRefLoanCD\"\r\n                      #payLoanRefLoanCD=\"ngModel\" required [disabled]=disbleflag>\r\n            <mat-option label=\"Select Loan Type\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of arrddlLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span *ngIf=\"payLoanRefLoanCD.submitted && payLoanRefLoanCD.invalid\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"col-md-12 col-lg-6\">\r\n    <mat-form-field class=\"wid-100\">\r\n      <mat-select placeholder=\"Select Loan Purpose\" matNativeControl (selectionChange)=\"getvalues($event.value)\" name=\"payLoanpurposeCode\"\r\n                  #payLoanpurposeCode=\"ngModel\" [(ngModel)]=\"_loandetails.payLoanpurposeCode\" [disabled]=disbleflag required>\r\n        <mat-option label=\"Select Loan Purpose Type\">Select Loan Purpose Type</mat-option>\r\n        <mat-option *ngFor=\"let LoanPurpose of ArrLoanPurpose\" [value]=\"LoanPurpose.payLoanPurposeCode\">\r\n          {{LoanPurpose.payLoanPurposeDescription}}\r\n        </mat-option>\r\n      </mat-select>\r\n      <mat-error>\r\n        <span *ngIf=\"NewLaon.submitted && payLoanpurposeCode.invalid\">Loan Purpose is required</span>\r\n      </mat-error>\r\n    </mat-form-field>\r\n  </div>-->\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"{{labelmsg}}\" matNativeControl (selectionChange)=\"getvalues($event.value)\" name=\"payLoanPurposeCode\"\r\n                      #payLoanPurposeCode=\"ngModel\" [(ngModel)]=\"_loandetails.payLoanPurposeCode\" [disabled]=disbleflag required>\r\n            <mat-option label=\"this.labelmsg\">{{labelmsg}}</mat-option>\r\n            <mat-option *ngFor=\"let LoanPurpose of arrLoanPurpose\" [value]=\"LoanPurpose.payLoanPurposeCode\">\r\n              {{LoanPurpose.payLoanPurposeDescription}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n\r\n            <span [hidden]=\"!payLoanPurposeCode.errors?.required\">Loan Purpose is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"LoanAmtSanc\" placeholder=\"Loan Amount (Rs.)\" #loanAmtSanc=\"ngModel\" [(ngModel)]=\"_loandetails.loanAmtSanc\"\r\n                 [disabled]=disbleflag (keypress)=\"numberOnly($event)\" (change)=\"checkvaluecLoanAmout($event.target.value)\" required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Loan Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"PriInstAmt\" placeholder=\"Installment Amount\" #priInstAmt=\"ngModel\" [(ngModel)]=\"_loandetails.priInstAmt\"\r\n                 (keypress)=\"numberOnly($event)\" (change)=\"valuechange($event.target.value)\" [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!priInstAmt.errors?.required\">Installment Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Total No. of Installment\" name=\"IntTotInst\" #intTotInst=\"ngModel\" [(ngModel)]=\"_loandetails.intTotInst \" readonly disabled='true' required>\r\n          <mat-error>\r\n            <span [hidden]=\"!intTotInst.errors?.required\">Total No. of Installment is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"No. of Odd Installment\" name=\"OddInstNoInt\" #oddInstNoInt=\"ngModel\" [(ngModel)]=\"_loandetails.oddInstNoInt \" (keypress)=\"numberOnly($event)\" disabled='true' required>\r\n          <mat-error>required</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Odd Installment Amount\" name=\"OddInstAmtInt\" #oddInstAmtInt=\"ngModel\" [(ngModel)]=\"_loandetails.oddInstAmtInt\" (keypress)=\"numberOnly($event)\" disabled='true' required>\r\n          <mat-error>\r\n            <span [hidden]=\"!oddInstAmtInt.errors?.required\">Odd Installment Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div *ngIf=\"loanCode=='21'\">\r\n        <div class=\"fom-title\">Property Owner Details</div>\r\n\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Property Owner Name\" [(ngModel)]=\"_loandetails.ownerName\" title=\"Property Owner Name\" name=\"ownerName\" #ownerName=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!ownerName.errors?.required\">Property Owner Name  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Description\" title=\"Description\" [(ngModel)]=\"_loandetails.descriptions\" name=\"descriptions\" #descriptions=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!descriptions.errors?.required\">Description  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Property Owner's PAN No\" title=\"Property Owner's PAN No\" [(ngModel)]=\"_loandetails.ownerPanNo\" name=\"panno\" maxlength=\"10\" #ownerPanNo=\"ngModel\" [disabled]=disbleflag required class=\"upperCase\">\r\n            <mat-error>\r\n              <span [hidden]=\"!ownerPanNo.errors?.required\">Property Owner's PAN No  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"IFSC Code\" [(ngModel)]=\"_loandetails.bankIFSCCODE\" #bankIFSCCODE=\"ngModel\" name=\"bankIFSCCODE\" (blur)=\"getBankDetailsByIFSC(_loandetails.bankIFSCCODE)\" [disabled]=disbleflag required class=\"upperCase\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Bank Name\" [(ngModel)]=\"objBankDetails.bankName\" name=\"bankName\" #bankName=\"ngModel\" [disabled]=disbleflag required>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Branch Name\" [(ngModel)]=\"objBankDetails.branchName\" name=\"branchName\" #branchName=\"ngModel\" [disabled]=disbleflag required>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Saving A/c No\" [(ngModel)]=\"_loandetails.bankAccountNo\" (keypress)=\"numberOnly($event)\" maxlength=\"10\" name=\"bankAccountNo\" #bankAccountNo=\"ngModel\" [disabled]=disbleflag required>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n\r\n      <!---------------------Purchase of a Plot---------------------->\r\n      <div *ngIf=\"loanCode=='21'  && PurposeCode == '1'\">\r\n        <div class=\"fom-title\">Purchase of a Plot</div>\r\n\r\n        <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6 pading-0\">\r\n            <label> Select Area</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6\">\r\n\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.selct_Area\" name=\"selct_Area\" #selct_Area=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Rural</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">Urban</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label> Is it clearly demarcated and developed</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-5\">\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.demar_Dev\" name=\"demar_Dev\" #demar_Dev=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Approximate area (in sq. )\" [(ngModel)]=\"_loandetails.area_Sq_Ft\" title=\"Approximate area (in sq. )\" name=\"area_Sq_Ft\" #area_Sq_Ft=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!area_Sq_Ft.errors?.required\">Approximate area  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost\" title=\"Cost\" [(ngModel)]=\"_loandetails.cost\" (keypress)=\"numberOnly($event)\" name=\"cost\" #cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!cost.errors?.required\">Cost  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount actually paid\" title=\"Amount actually paid\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_loandetails.amt_Act_Pay\" name=\"amt_Act_Pay\" #amt_Act_Pay=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Act_Pay.errors?.required\">Amount actually paid  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"If not purchased when proposed to be acquired\" [(ngModel)]=\"_loandetails.purp_Acquire\"\r\n                   title=\"If not purchased when proposed to be acquired\" name=\"purp_Acquire\" #purp_Acquire=\"ngModel\" required [disabled]=disbleflag>\r\n            <mat-error>\r\n              <span [hidden]=\"!purp_Acquire.errors?.required\">proposed to be acquired  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Unexpired portion of lease if not freehold\"\r\n                   [(ngModel)]=\"_loandetails.unexp_Port_Lease\" title=\"Unexpired portion of lease if not freehold\" name=\"unexp_Port_Lease\" #unexp_Port_Lease=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!unexp_Port_Lease.errors?.required\">Unexpired portion of lease if not freehold  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Group</label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"purchageFlag = !purchageFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Construction---------------------->\r\n      <div *ngIf=\"loanCode=='21'  && PurposeCode == '2'\">\r\n        <div class=\"fom-title\">Construction</div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <!--<mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Floor - wise area to be constructed in (sq.mtrs.)\"\r\n                    [(ngModel)]=\"_loandetails.flr_Area\" title=\"Floor - wise area to be constructed in (sq.mtrs.)\" name=\"flr_Area\" #flr_Area=\"ngModel\" [disabled]=disbleflag required>\r\n          <mat-option value=\"1\">Select Floor</mat-option>\r\n          <mat-option value=\"2\">Ground  Floor</mat-option>\r\n          <mat-option value=\"3\">First Floor</mat-option>\r\n          <mat-option value=\"4\">Second Floor</mat-option>\r\n        </mat-select>\r\n        <mat-error>\r\n          <span [hidden]=\"!flr_Area.errors?.required\">Floor - wise area to be constructed is required</span>\r\n        </mat-error>\r\n      </mat-form-field>-->\r\n\r\n\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Floor - wise area to be constructed in (sq.mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.flr_Area\" title=\"Floor - wise area to be constructed in (sq.mtrs.)\" name=\"flr_Area\" #flr_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!flr_Area.errors?.required\">Floor - wise area to be constructed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n\r\n\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Estimated Cost\" title=\"Estimated Cost\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_loandetails.est_Cost\" name=\"est_Cost\" #est_Cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!est_Cost.errors?.required\">Estimated Cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Amount of advance required (for land/construction/both)\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_loandetails.amt_Adv_Req\"\r\n                   title=\"Amount of advance required (for land/construction/both)\" name=\"amt_Adv_Req\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount of advance is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled\r\n                   placeholder=\"No. of instalments for repayment\"\r\n                   [(ngModel)]=\"_loandetails.num_Inst\"\r\n                   title=\"No. of instalments for repayment\" name=\"num_Inst\" #num_Inst=\"ngModel\" (keypress)=\"numberOnly($event)\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">No. of instalments for repayment is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      title=\"Location with address\" [(ngModel)]=\"_loandetails.address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n\r\n        <!--<div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n      <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n        <label> Select Area</label>\r\n      </div>\r\n      <div class=\"col-sm-6 col-md-6 col-lg-5\">\r\n        <mat-radio-group [(ngModel)]=\"_loandetails.selct_Area\" name=\"selct_Area\" #selct_Area=\"ngModel\">\r\n          <mat-radio-button [value]=\"true\">Rural</mat-radio-button>\r\n          <mat-radio-button [value]=\"false\">Urban</mat-radio-button>\r\n\r\n        </mat-radio-group>\r\n      </div>\r\n    </div>-->\r\n\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6 pading-0\">\r\n            <label> Select Area</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6\">\r\n\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.selct_Area\" name=\"selct_Area\" #selct_Area=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Rural</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">Urban</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n\r\n\r\n          </div>\r\n        </div>\r\n\r\n\r\n\r\n\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label> Is it clearly demarcated and developed</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-5\">\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.demar_Dev\" name=\"demar_Dev\" #demar_Dev=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Approximate area (in sq. )\"\r\n                   [(ngModel)]=\"_loandetails.area_Sq_Ft\" title=\"Approximate area (in sq. )\" name=\"area_Sq_Ft\" #area_Sq_Ft=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!area_Sq_Ft.errors?.required\">Approximate area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost \" title=\"Cost \" [(ngModel)]=\"_loandetails.cost\" (keypress)=\"numberOnly($event)\" name=\"cost\" #cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!cost.errors?.required\">Cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Amount actually paid \" title=\"Amount actually paid \"\r\n                   [(ngModel)]=\"_loandetails.amt_Act_Pay\" name=\"amt_Act_Pay\" #amt_Act_Pay=\"ngModel\" (keypress)=\"numberOnly($event)\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Act_Pay.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"If not purchased when proposed to be acquired \"\r\n                   [(ngModel)]=\"_loandetails.purp_Acquire\"\r\n                   title=\"If not purchased when proposed to be acquired \" name=\"purp_Acquire\" #purp_Acquire=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!purp_Acquire.errors?.required\">purchased is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Unexpired portion of lease if not freehold \"\r\n                   [(ngModel)]=\"_loandetails.unexp_Port_Lease\"\r\n                   title=\"Unexpired portion of lease if not freehold\" name=\"unexp_Port_Lease\" #unexp_Port_Lease=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!unexp_Port_Lease.errors?.required\">Unexpired portion of lease is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Group</label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"constructionFlag = !constructionFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n\r\n      <!---------------------Details for enlarging the existing House---------------------->\r\n      <div *ngIf=\"loanCode=='21'  && PurposeCode == '3'\">\r\n        <div class=\"fom-title\">Details for enlarging the existing House</div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area(in sq.mtrs.)\" title=\"Plinth area(in sq.mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.plint_Area\" name=\"plint_Area\" #plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plint_Area.errors?.required\">Plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area proposed for enlargement(in sq. mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.plth_Prop_Enlarge\" title=\"Plinth area proposed for enlargement(in sq. mtrs.)\"\r\n                   name=\"plth_Prop_Enlarge\" #plth_Prop_Enlarge=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plth_Prop_Enlarge.errors?.required\">Plinth area proposed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost of construction/acqisition of existing house\"\r\n                   [(ngModel)]=\"_loandetails.coc\" title=\"Cost of construction/acqisition of existing house\" (keypress)=\"numberOnly($event)\" name=\"coc\" #coc=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!coc.errors?.required\">Cost of construction is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost of proposed enlargement\"\r\n                   [(ngModel)]=\"_loandetails.cop_Enlarge\" title=\"Cost of proposed enlargement\" (keypress)=\"numberOnly($event)\" name=\"cop_Enlarge\" #cop_Enlarge=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!cop_Enlarge.errors?.required\">Cost of proposed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Total plinth area\"\r\n                   [(ngModel)]=\"_loandetails.tot_Plint_Area\" title=\"Total plinth area\" name=\"tot_Plint_Area\" #tot_Plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!tot_Plint_Area.errors?.required\">Total plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Total Cost\"\r\n                   [(ngModel)]=\"_loandetails.tot_Cost\" title=\"Total Cost\" (keypress)=\"numberOnly($event)\" name=\"tot_Cost\" #tot_Cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!tot_Cost.errors?.required\">Total cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance required \"\r\n                   [(ngModel)]=\"_loandetails.amt_Adv_Req\" title=\"Amount of advance required \" (keypress)=\"numberOnly($event)\" name=\"amt_Adv_Req\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount of advance is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"No. of Installments for Repayment\"\r\n                   [(ngModel)]=\"_loandetails.num_Inst\" title=\"No. of Installments for Repayment\" (keypress)=\"numberOnly($event)\" name=\"num_Inst\" #num_Inst=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">No. of installments is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Group</label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"enlargingFlag = !enlargingFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Details for purchasing a ready-built house/flat---------------------->\r\n      <div *ngIf=\"loanCode=='21'  && PurposeCode == '4'\">\r\n        <div class=\"fom-title\">Details for purchasing a ready-built house/flat</div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area (in sq.mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.plint_Area\"\r\n                   name=\"plint_Area\" #plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plint_Area.errors?.required\">Plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <!--<mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"When constructed\" [(ngModel)]=\"_loandetails.Const_Date\"\r\n               title=\"When constructed\" name=\"Const_Date\" #Const_Date=\"ngModel\" [disabled]=disbleflag required />\r\n        <mat-error>\r\n          <span [hidden]=\"!Const_Date.errors?.required\">Constructed date is required</span>\r\n        </mat-error>\r\n      </mat-form-field>-->\r\n\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker2\" placeholder=\"When constructed\"\r\n                   #const_Date=\"ngModel\" [(ngModel)]=\"_loandetails.const_Date \" readonly (click)=\"picker2.open()\" name=\"const_Date\" required [disabled]=disbleflag>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker2></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!const_Date.errors?.required\">When constructed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n\r\n\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Price settled\" title=\"Price settled\"\r\n                   [(ngModel)]=\"_loandetails.prc_Settled\" name=\"prc_Settled\" (keypress)=\"numberOnly($event)\" #prc_Settled=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!prc_Settled.errors?.required\">Price settled is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Agency from whom to be purchased\" [(ngModel)]=\"_loandetails.agency_Whom_Pur\"\r\n                   title=\"Agency from whom to be purchased\" name=\"agency_Whom_Pur\" #agency_Whom_Pur=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!agency_Whom_Pur.errors?.required\">Purchased is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount already paid \" title=\"Amount already paid\"\r\n                   [(ngModel)]=\"_loandetails.amt_Paid\" name=\"amt_Paid\" (keypress)=\"numberOnly($event)\" #amt_Paid=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Paid.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance required\" [(ngModel)]=\"_loandetails.amt_Adv_Req\"\r\n                   title=\"Amount of advance required\" name=\"amt_Adv_Req\" (keypress)=\"numberOnly($event)\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"No. of Installments for Repayment\" [(ngModel)]=\"_loandetails.num_Inst\"\r\n                   title=\"No. of Installments for Repayment\" name=\"num_Inst\" (keypress)=\"numberOnly($event)\" #num_Inst=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">Repayment is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Group</label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"readybuiltFlag = !readybuiltFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Computer Adv---------------------->\r\n      <div *ngIf=\"loanCode=='100'\">\r\n        <div class=\"fom-title\">Computer Advance</div>\r\n\r\n        <div class=\"col-md-6 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Purchase Personal Computer</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.intPurPrsnComp\" name=\"intPurPrsnComp\" #intPurPrsnComp=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Sanction is as per Rule 18 & Rules 1964</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.rule_18_1964_Pur\" name=\"rule_18_1964_Pur\" #rule_18_1964_Pur=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Basic pay\" [(ngModel)]=\"_loandetails.pay_basic\"\r\n                   name=\"pay_basic\" #pay_basic=\"ngModel\" (keypress)=\"numberOnly($event)\" disabled='true' required>\r\n            <mat-error>\r\n              <span [hidden]=\"!pay_basic.errors?.required\">Basic pay is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Anticipated price of Personal Computer \" title=\"Anticipated price of Personal Computer\"\r\n                   [(ngModel)]=\"_loandetails.anti_Price_Pc\" (keypress)=\"numberOnly($event)\" name=\"Anti_Price_Pc\" #anti_Price_Pc=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!anti_Price_Pc.errors?.required\">Anticipated price of Personal Computer is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker\" disabled placeholder=\"Superannuation / Contract Expiry Date (in case of contract officer)\"\r\n                   title=\"Superannuation / Contract Expiry Date (in case of contract officer)\"\r\n                   [(ngModel)]=\"_loandetails.empSupanDt\" (click)=\"picker.open()\" name=\"empSupanDt\" #empSupanDt=\"ngModel\" required disabled='true'>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!empSupanDt.errors?.required\">Contract Expiry Date is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Desired No of Installments to be repaid for advance\" (keypress)=\"numberOnly($event)\" title=\"Desired No of Installments to be repaid for advance\"\r\n                   [(ngModel)]=\"_loandetails.desNoInsRePaidForAdv\" name=\"desNoInsRePaidForAdv\" #desNoInsRePaidForAdv=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!desNoInsRePaidForAdv.errors?.required\">Desired No of Installments is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Whether the officer is on leave</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.off_On_Leave\" name=\"off_On_Leave\" #off_On_Leave=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Advance taken previously for similar purpose</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.pre_Adv_Simi_Pur\" name=\"pre_Adv_Simi_Pur\" #pre_Adv_Simi_Pur=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker1\" placeholder=\"Date of drawal of the advance\" title=\"Date of drawal of the advance\"\r\n                   [(ngModel)]=\"_loandetails.adv_Draw_Date\" (click)=\"picker1.open()\" name=\"adv_Draw_Date\" #adv_Draw_Date=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker1></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!adv_Draw_Date.errors?.required\">Date of drawal is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance / interest outstanding\" (keypress)=\"numberOnly($event)\" title=\"Amount of advance / interest outstanding\"\r\n                   [(ngModel)]=\"_loandetails.adv_Amount\" name=\"adv_Amount\" #adv_Amount=\"ngModel\" [disabled]=disbleflag required>\r\n\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Group</label></div>\r\n          <div class=\"col-md-12 col-lg-10 pading-0\">\r\n\r\n            <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"compAdvFlag = !compAdvFlag\" [disabled]=disbleflag></mat-checkbox>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"col-sm-12 col-md-6\"><label>Upload e-sign</label></div>\r\n            <input id=\"idfile\" class=\"form-control\" name=\"file\" [(ngModel)]=\"_loandetails.upload_Sign\" type=\"file\"\r\n                   #file accept=\"file/*\" (change)=\"handleFileinput($event.target.files)\"><br />\r\n            <a href={{_loandetails.upload_Sign}} target=\"_blank\" *ngIf=disbledwnloadflag>{{_loandetails.upload_Sign}}</a><br /><br />\r\n            <a class=\"material-icons\" *ngIf=disbledwnloadflag (click)=\"DownloadFile(_loandetails.upload_Sign)\">vertical_align_bottom</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br /> <br />\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"_loandetails.remarks!=''\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_loandetails.remarks\" title=\"Remarks\" name=\"remarks\" #remarks=\"ngModel\" [disabled]=disbleflag></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"disableSaveFlage\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"disableRejectFlag\" (click)=\"deletepopup = !deletepopup\">Reject</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" *ngIf=\"disableCancelFlage\" style=\"display:none;\">Cancel</button>\r\n        <button class=\"btn btn-primary\" type=\"button\" *ngIf=\"disableCheckerFlag\" (click)=\"fwdhoochecker()\"> Forward to HOO Checker</button>\r\n      </div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n    </mat-card>\r\n\r\n  </form>\r\n  <app-dialog [(visible)]=\"purchageFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_1\" name=\"pop_Decl_1\" #pop_Decl_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('1',_loandetails.Pop_Decl_1)\"-->\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief.\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_2\" name=\"pop_Decl_2\" #pop_Decl_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('2',_loandetails.Pop_Decl_2)\"-->\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_3\" name=\"pop_Decl_3\" #pop_Decl_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('3',_loandetails.Pop_Decl_3)\"-->\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\"  [disabled]=disbleflag class=\"btn btn-warning center\" (click)=\"popupclose()\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"enlargingFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_1\" name=\"const_Dec_1\" #const_Dec_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('4',_loandetails.const_Dec_1)\"-->\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_2\" name=\"const_Dec_2\" #const_Dec_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('5',_loandetails.const_Dec_2)\"-->\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_3\" name=\"const_Dec_3\" #const_Dec_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('6',_loandetails.const_Dec_3)\"-->\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\"  [disabled]=disbleflag class=\"btn btn-warning center\" (click)=\"popupclose()\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"constructionFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_1\" name=\"dtls_Enlarg_1\" #dtls_Enlarg_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief.\r\n          <!--(click)=\"ChangeForm('7',_loandetails.dtls_Enlarg_1)\"-->\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_2\" name=\"dtls_Enlarg_2\" #dtls_Enlarg_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('8',_loandetails.dtls_Enlarg_2)\"-->\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_3\" name=\"dtls_Enlarg_3\" #dtls_Enlarg_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('9',_loandetails.dtls_Enlarg_3)\"-->\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" [disabled]=disbleflag class=\"btn btn-warning center\" (click)=\"popupclose()\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"ReadybuiltFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_Flat_1\" name=\"dtls_Built_Flat_1\" [disabled]=disbleflag #dtls_Built_Flat_1=\"ngModel\"></mat-checkbox>\r\n\r\n          <!--(click)=\"ChangeForm('10',_loandetails.dtls_Built_Flat_1)\"-->\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best\r\n          of my knowledge and belief\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_flat_2\" name=\"dtls_Built_flat_2\" [disabled]=disbleflag #dtls_Built_flat_2=\"ngModel\"></mat-checkbox>\r\n          <!-- (click)=\"ChangeForm('11',_loandetails.dtls_Built_Flat_2)\"-->\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc.\r\n          and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_flat_3\" name=\"dtls_Built_flat_3\" [disabled]=disbleflag #dtls_Built_flat_3=\"ngModel\"></mat-checkbox>\r\n          <!-- ChangeForm('12',_loandetails.dtls_Built_Flat_3)-->\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or\r\n              Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for\r\n              acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" [disabled]=disbleflag class=\"btn btn-warning center\" (click)=\"popupclose()\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"compAdvFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.comp_Certi_Info_1\" name=\"comp_Certi_Info_1\" [disabled]=disbleflag #comp_Certi_Info_1=\"ngModel\"></mat-checkbox>\r\n          Certified that the information given above is complete and true.\r\n          <!--(click)=\"ChangeForm('13',_loandetails.comp_Certi_Info_1)\"-->\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.comp_Certi_Info_2\" name=\"comp_Certi_Info_2\" [disabled]=disbleflag #comp_Certi_Info_2=\"ngModel\"></mat-checkbox>\r\n          <!--(click)=\"ChangeForm('14',_loandetails.comp_Certi_Info_2)\"-->\r\n          Certified that i have not taken delivery of the personal computer on account of which i apply for the advance that i shall complete negotiations for the purchase of pay finally and take possession of the same before the expiry of one month from the date of drawal of the advance\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"popupclose()\">Ok</button></div>\r\n  </app-dialog>\r\n</div>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"EMPId\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> ID </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.id}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"empCode\" style=\"width:120px;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> EMP Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"loanCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.loanCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"purposeCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Purpose  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.purposeCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n       \r\n          <a  matTooltip=\"{{element.moduleStatus}}\">   {{element.status}}</a>\r\n\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n        \r\n\r\n          <a class=\"material-icons i-info\" matTooltip=\"view details\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">error</a>\r\n\r\n          <span *ngIf=\"element.statusId == '24' && userRoleID=='8'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == '22' && userRoleID=='8'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <!--<span *ngIf=\"element.statusId == '31' && userRoleID=='9'\">\r\n    <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n  </span>-->\r\n          <span *ngIf=\"element.statusId == '22' || '24'\">\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"SetDeleteId(element.id);deletepopup = !deletepopup\" style=\"display:none;\"> delete_forever </a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n<!--(click)=\"deleteEmployeeById(element.id)\"-->\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Please Enter Remarks.</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_loandetails.remarks\" title=\"Remarks\" name=\"remarks\" #remarks=\"ngModel\" required></textarea>\r\n          <mat-error>\r\n            <span [hidden]=\"!remarks.errors?.required\">Remarks is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"forwardhoochecker()\">Ok</button>\r\n        <button type=\"button\" class=\"btn btn-info center\"(click)=\"deletePopup = !deletePopup\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.ts ***!
  \**********************************************************************/
/*! exports provided: CompAdvVerifyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompAdvVerifyComponent", function() { return CompAdvVerifyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/new-loan.service */ "./src/app/services/loan-mgmt/new-loan.service.ts");
/* harmony import */ var _model_newloanModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/newloanModel */ "./src/app/model/newloanModel.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_empdetails_empdetails_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/empdetails/empdetails.service */ "./src/app/services/empdetails/empdetails.service.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var CompAdvVerifyComponent = /** @class */ (function () {
    function CompAdvVerifyComponent(master, objBankService, empDetails, _msg, _Service, objCommanService) {
        this.master = master;
        this.objBankService = objBankService;
        this.empDetails = empDetails;
        this._msg = _msg;
        this._Service = _Service;
        this.objCommanService = objCommanService;
        this.fileToupload = null;
        this.imageUrl = "/assets/uploadfile/";
        this.purchageFlag = false;
        this.enlargingFlag = false;
        this.constructionFlag = false;
        this.readybuiltFlag = false;
        this.compAdvFlag = false;
        this.disableflag = false;
        this.disableSaveFlage = false;
        this.disableCancelFlage = false;
        this.disableFwdCheckerFlag = false;
        this.disableRejectFlag = false;
        this.disableCheckerFlag = false;
        this.Message = null;
        this.disbleflag = false;
        this.disbleflagdownload = false;
        this.disbledwnloadflag = false;
        this.callTypevar = 2;
        this.buttonText = 'Save';
        this.submitted = false;
        this.objBankDetails = {};
        this.labelmsg = 'Select Purpose Type';
        this.displayedColumns = ['EMPId', 'empCode', 'loanCode', 'purposeCode', 'Status', 'action'];
        this.selectedIndex = 0;
        this.empstatus = false;
        this.flag = false;
        this.formatsDateTest = [
            'dd/MM/yyyy',
        ];
        this.dateNow = new Date();
        this.dateNowISO = this.dateNow.toISOString();
        this.dateNowMilliseconds = this.dateNow.getTime();
        this.urls = [];
        this._loandetails = new _model_newloanModel__WEBPACK_IMPORTED_MODULE_3__["newloanModel"]();
    }
    CompAdvVerifyComponent.prototype.ngOnInit = function () {
        debugger;
        this.empstatus = false;
        this.flag = false;
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        if (this.userRoleID == 17) {
            this.FindEmpCode(this.username);
        }
        else if (this.userRoleID == 8) {
            this.FindEmpCode(this.username);
        }
        else if (this.userRoleID == 9) {
            this.FindEmpCode(this.username);
        }
        this.BindLoanStatus(this.UserID);
        this.group = false;
        this.BindLoanType();
    };
    CompAdvVerifyComponent.prototype.FindEmpCode = function (username) {
        var _this = this;
        this.objCommanService.GetEmpCode(username).subscribe(function (data) {
            _this.ArrEmpCode = data[0];
            //if (this.ArrEmpCode.serviceTime < '5') {
            // swal("At least 5Yrs. continuous in govt service for apply loan");    
            //}
            //if (this.ArrEmpCode.serviceType != 'R') {
            //  swal("only Permanent employee apply for the loan");
            //}   
            if (_this.ArrEmpCode.empcode != null) {
                _this.getLoanDetails(_this.ArrEmpCode.empcode);
            }
        });
    };
    CompAdvVerifyComponent.prototype.GetcommanMethod = function (value) {
        this.getLoanDetails(value);
    };
    CompAdvVerifyComponent.prototype.BindLoanType = function () {
        var _this = this;
        this.loantypeFlag = '2';
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this.arrddlLoanType = data;
            console.log(_this.arrddlLoanType);
        });
    };
    CompAdvVerifyComponent.prototype.valuechange = function (value) {
        this._loandetails.intTotInst = Math.floor(((this._loandetails.loanAmtSanc) / value));
        this.temp = Math.floor((this._loandetails.priInstAmt) * this._loandetails.intTotInst);
        this._loandetails.oddInstAmtInt = Math.floor(((this._loandetails.loanAmtSanc) - this.temp));
        if (this._loandetails.oddInstAmtInt == 0) {
            this._loandetails.oddInstNoInt = 0;
        }
        else {
            this._loandetails.oddInstNoInt = 1;
        }
    };
    CompAdvVerifyComponent.prototype.checkvaluecLoanAmout = function (value) {
        if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.maxLimitLoanAmountMsg);
            //swal("Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower");
            this._loandetails.loanAmtSanc = null;
        }
        if (Math.floor(this._loandetails.loanAmtSanc) > 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.maximumLimitLoanAmountMsg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Number(this._Service.PurposeCode) == 3 && Math.floor(this._loandetails.loanAmtSanc) > 180000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.maxLimitLoanAmount1LK80Msg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Number(this._Service.PurposeCode) == 2 && Math.floor(this._loandetails.loanAmtSanc) > 250000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.maxLimitLoanAmount2LK50Msg);
            this._loandetails.loanAmtSanc = null;
        }
    };
    CompAdvVerifyComponent.prototype.getLoanDetails = function (value) {
        this.empid = value;
        this.designid = "0";
        this.getEmployeeDeatils(this.empid, this.designid);
    };
    CompAdvVerifyComponent.prototype.getEmployeeDeatils = function (empCode, msDesignID) {
        var _this = this;
        this._Service.EmployeeInfo(empCode, msDesignID).subscribe(function (data) {
            _this._loandetails = data[0];
            _this.getLoanpurposeSelectedPayloanCode(_this._loandetails.payLoanRefLoanCD);
        });
    };
    CompAdvVerifyComponent.prototype.getloantypeandpurposestatus = function (LoanBillID) {
        var _this = this;
        this._Service.Loantypeandpurposestatus(LoanBillID).subscribe(function (data) {
            _this.loantypeandpurposes = data;
            console.log(_this.loantypeandpurposes);
        });
    };
    CompAdvVerifyComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    CompAdvVerifyComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    CompAdvVerifyComponent.prototype.uploadfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var formData, _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        formData = new FormData();
                        formData.append(this.fileToupload.name, this.fileToupload);
                        _a = this;
                        return [4 /*yield*/, this.master.uploadFile(formData).subscribe(function (res) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.asyncResult = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CompAdvVerifyComponent.prototype.resetLoanDdetailsForm = function () {
        this.NewLaonForm.resetForm();
    };
    CompAdvVerifyComponent.prototype.getLoanpurposeSelectedPayloanCode = function (value) {
        var _this = this;
        debugger;
        if (value == "21") {
            this.labelmsg = 'Select Purpose Type';
        }
        if (value == "100") {
            this.labelmsg = 'Select No. of Computer Loan Sanctioned';
        }
        this._Service.LoanCode = value;
        this.temploanCode = value;
        this._Service.EMPLoanPurposeByLoanCode(value, this.ArrEmpCode.empcode).subscribe(function (data) {
            _this.arrLoanPurpose = data;
            console.log(data);
        });
    };
    CompAdvVerifyComponent.prototype.getvalues = function (value) {
        debugger;
        this._Service.PurposeCode = value;
        this.loanCode = this.temploanCode;
        this.PurposeCode = value;
    };
    CompAdvVerifyComponent.prototype.BindLoanStatus = function (value) {
        var _this = this;
        debugger;
        this.mode = 2;
        this._Service.DdoId = value;
        this._Service.BindLoanStatus(this.mode, value).subscribe(function (data) {
            _this.arrddlLoanStatus = data;
            console.log(data);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    CompAdvVerifyComponent.prototype.ViewEditLoanDetailsById = function (id, flag) {
        var _this = this;
        debugger;
        this.objBankDetails.branchName = '';
        this.objBankDetails.bankName = '';
        this._loandetails.bankIFSCCODE = '';
        this._Service.EditLoanDetailsByEMPID(id).subscribe(function (data) {
            _this._loandetails = data[0];
            console.log(data[0]);
            _this.tempId = id;
            _this.buttonText = "Update";
            if (_this._loandetails.priVerifFlag == 22) {
                _this.disableSaveFlage = false;
                _this.disableCancelFlage = false;
                _this.disableFwdCheckerFlag = true;
            }
            if (_this._loandetails.priVerifFlag == 8) {
                _this.disableSaveFlage = true;
                _this.disableCancelFlage = true;
                _this.disableCheckerFlag = true;
                _this.disableRejectFlag = true;
            }
            if (_this._loandetails.priVerifFlag == 24) {
                _this.disableSaveFlage = true;
                _this.disableCancelFlage = true;
                _this.disableCheckerFlag = true;
                _this.disableRejectFlag = true;
            }
            _this.getLoanpurposeSelectedPayloanCode(_this._loandetails.payLoanRefLoanCD);
            _this.getvalues(_this._loandetails.payLoanPurposeCode);
            if (_this._Service.PurposeCode == '4') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '1') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '2') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '3') {
                _this.group = true;
            }
            if (_this._Service.LoanCode == '100') {
                if (_this._loandetails.comp_Certi_Info_1 == true) {
                    _this.group = true;
                }
            }
            if (flag === 1) {
                _this.disbleflag = true;
                _this.disbledwnloadflag = false;
                _this.disableSaveFlage = false;
                _this.disableCancelFlage = false;
                _this.disableFwdCheckerFlag = false;
                _this.disableRejectFlag = false;
            }
            else {
                _this.disbledwnloadflag = true;
                _this.disbleflag = false;
                _this.disableSaveFlage = true;
                _this.disableRejectFlag = true;
            }
            if (_this._loandetails.bankIFSCCODE != "" && _this._loandetails.bankIFSCCODE != undefined) {
                _this.objBankService.getBankDetailsByIFSC(_this._loandetails.bankIFSCCODE).subscribe(function (res) {
                    _this.objBankDetails = res;
                });
            }
            else {
                _this.objBankDetails.branchName = '';
                _this.objBankDetails.bankName = '';
                _this._loandetails.bankIFSCCODE = '';
            }
        });
    };
    CompAdvVerifyComponent.prototype.commanfunction = function () {
        this.resetLoanDdetailsForm();
        if (this._Service.PurposeCode == '4') {
            this.readybuiltFlag = false;
        }
        if (this._Service.PurposeCode == '1') {
            this.purchageFlag = false;
        }
        if (this._Service.PurposeCode == '2') {
            this.constructionFlag = false;
        }
        if (this._Service.PurposeCode == '3') {
            this.enlargingFlag = false;
        }
        if (this._Service.LoanCode == '100') {
            this.compAdvFlag = false;
            if (this._loandetails.comp_Certi_Info_1 == true) {
                this.group = true;
            }
        }
    };
    CompAdvVerifyComponent.prototype.SetDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    //Cancel() {
    //  $(".dialog__close-btn").click();
    //}
    CompAdvVerifyComponent.prototype.popupclose = function () {
        this.disableSaveFlage = true;
        this.disableCancelFlage = true;
        this.disableFwdCheckerFlag = false;
        this.purchageFlag = false;
        this.enlargingFlag = false;
        this.constructionFlag = false;
        this.readybuiltFlag = false;
        this.compAdvFlag = false;
    };
    CompAdvVerifyComponent.prototype.deleteEmployeeById = function (id) {
        var _this = this;
        this.mode = 2;
        this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(function (data) {
            _this.Message = data;
            if (_this.Message != null) {
                _this.deletePopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.resetLoanDdetailsForm();
            }
        });
    };
    CompAdvVerifyComponent.prototype.handleFileinput = function (file) {
        var _this = this;
        this.fileToupload = file.item(0);
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToupload);
    };
    CompAdvVerifyComponent.prototype.onSelectFile = function (event) {
        debugger;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    CompAdvVerifyComponent.prototype.DownloadFile = function (url) {
        var _this = this;
        this.empDetails.getdownloadDetails(url).subscribe(function (res) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_5__["saveAs"])(new Blob([res], { type: _this._msg.formatfileMsg }), "");
        });
    };
    CompAdvVerifyComponent.prototype.btnsubmit = function () {
        var _this = this;
        debugger;
        this._loandetails.createdby = this.username;
        this._loandetails.permDdoId = this.ddoid;
        this._loandetails.msEmpId = this.tempId;
        this.mode = 2;
        if (this.userRoleID == 8 && this._loandetails.priVerifFlag == 22) {
            this._loandetails.priVerifFlag = 23;
        }
        else {
            // this._loandetails.priVerifFlag = 31;
        }
        this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(function (data) {
            _this.flag = true;
            if (_this._loandetails.priVerifFlag == 22) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.VerifyHOOChecker);
                //this.snackBar.open('Verified by HOO checker', null, { duration: 5000 });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.VerifyHOOChecker);
                //this.snackBar.open('Verified by HOO checker', null, { duration: 5000 });
            }
            _this.BindLoanStatus(_this._Service.DdoId);
            _this.commanfunction();
        });
    };
    CompAdvVerifyComponent.prototype.fwdhoochecker = function () {
        var _this = this;
        if (this.tempId != null) {
            this.mode = 1;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.VerifyHOOChecker);
                //this.snackBar.open('HOO Checker Verify', null, { duration: 5000 });
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.commanfunction();
            });
        }
    };
    CompAdvVerifyComponent.prototype.forwardhoochecker = function () {
        var _this = this;
        debugger;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 3;
            this.remarks = this._loandetails.remarks;
            if (this.userRoleID == 8) {
                this._loandetails.priVerifFlag = 31;
            }
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.Rejected);
                // this.snackBar.open('Rejected', null, { duration: 5000 });
                // $(".dialog__close-btn").click();
                _this.deletePopup = false;
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.commanfunction();
            });
        }
    };
    CompAdvVerifyComponent.prototype.getBankDetailsByIFSC = function (bankIFSCCODE) {
        var _this = this;
        this.objBankService.getBankDetailsByIFSC(bankIFSCCODE).subscribe(function (res) {
            _this.objBankDetails = res;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('loandetailsForm'),
        __metadata("design:type", Object)
    ], CompAdvVerifyComponent.prototype, "loanDetailsForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('NewLaon'),
        __metadata("design:type", Object)
    ], CompAdvVerifyComponent.prototype, "NewLaonForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], CompAdvVerifyComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], CompAdvVerifyComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pdetails'),
        __metadata("design:type", Object)
    ], CompAdvVerifyComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('EmPDdetailsForm'),
        __metadata("design:type", Object)
    ], CompAdvVerifyComponent.prototype, "EmPDdetailsForm", void 0);
    CompAdvVerifyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-comp-adv-verify',
            template: __webpack_require__(/*! ./comp-adv-verify.component.html */ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.html"),
            styles: [__webpack_require__(/*! ./comp-adv-verify.component.css */ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_10__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_6__["MasterService"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_9__["BankDetailsService"], _services_empdetails_empdetails_service__WEBPACK_IMPORTED_MODULE_7__["EmpdetailsService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_10__["CommonMsg"],
            _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__["NewLoanService"], _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_8__["CommanService"]])
    ], CompAdvVerifyComponent);
    return CompAdvVerifyComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/existingloan/existingloan.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/loanmgt/existingloan/existingloan.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  /*lebel dropdown */\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbm1ndC9leGlzdGluZ2xvYW4vZXhpc3Rpbmdsb2FuLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0NBQ3hCOztFQUVDO0lBQ0UsWUFBWTtHQUNiOztFQUVIO0VBQ0Usa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7Q0FDWjs7RUFFRDtFQUNFLG9GQUFvRjtFQUNwRix1QkFBdUI7Q0FDeEI7O0VBRUQsZUFBZTs7RUFFZjtFQUNFLFlBQVk7Q0FDYjs7RUFFRDtFQUNFLHFCQUFxQjtDQUN0Qjs7RUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7RUFFRDtFQUNFLG9HQUFvRztDQUNyRzs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0VBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBR0QsZ0JBQWdCOztFQUNoQjtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0VBRUQ7RUFDRTtJQUNFLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFdBQVc7R0FDWjtDQUNGOztFQUNELGFBQWE7O0VBQ2I7RUFDRSxvQ0FBb0M7Q0FDckM7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBR0Q7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0NBQ2hCOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztFQUNELG1CQUFtQjs7RUFDbkI7RUFDRSxZQUFZO0NBQ2IiLCJmaWxlIjoic3JjL2FwcC9sb2FubWd0L2V4aXN0aW5nbG9hbi9leGlzdGluZ2xvYW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxucCB7XHJcbiAgZm9udC1mYW1pbHk6IExhdG87XHJcbn1cclxuXHJcbi5leGFtcGxlLWNhcmQge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiA0cHhcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4vKiAyOS1qYW4tMTkgKi9cclxuXHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGwubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubXQtMTAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtZHJvcC1oZWFkIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLypsZWJlbCBkcm9wZG93biAqL1xyXG4uc2VsZWN0LWxibCB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/loanmgt/existingloan/existingloan.component.html":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/existingloan/existingloan.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [data]=\"callTypevar\"></app-comman-mst>\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form #ExitLoan=\"ngForm\" (ngSubmit)=\"ExitLoan.valid && save(ArrLoanSection);\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Capture Running Loans/Advances</div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <h4>Loan Sanctioned Details</h4>\r\n      </div>\r\n      <!--payLoanRefLoanCD-->\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select matNativeControl placeholder=\"Loan Type\" #Loantype=\"ngModel\" [(ngModel)]=\"ArrLoanSection.payLoanRefLoanCD\" name=\"Loantype\" (selectionChange)=\"GetLoanCode($event.value)\" [disabled]=IsDisabled required>\r\n            <mat-option *ngFor=\"let Loantype of ArrddlLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}-{{Loantype.payLoanRefLoanCD}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span [hidden]=\"!Loantype.errors?.required\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input type=\"number\" matInput placeholder=\"Account Head\" [(ngModel)]=\"ArrLoanSection.payLoanRefLoanHeadACP\"\r\n                 maxlength=\"17\" name=\"payLoanRefLoanHeadACP\" #payLoanRefLoanHeadACP=\"ngModel\" required disabled>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker2\" placeholder=\"Sanction Order Date\"\r\n                 #sancOrdDT=\"ngModel\" [(ngModel)]=\"ArrLoanSection.sancOrdDT \" readonly (click)=\"picker2.open()\" name=\"sancOrdDT\" required [disabled]=IsDisabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker2></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdDT.errors?.required\">Sanction Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Amount Sanctioned (Rs.)\" [(ngModel)]=\"ArrLoanSection.loanAmtSanc\" (keypress)=\"numberOnly($event)\" maxlength=\"7\"\r\n                 name=\"loanAmtSanc\" #loanAmtSanc=\"ngModel\" required [disabled]=\"IsDisabled\" (keyup)=\"CompareValue()\" (blur)=\"CalculateValue()\">\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Amount Sanction required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 mb-15\">\r\n        <h4>Loan Recovery Details</h4>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"text\" matInput placeholder=\"Sanction Order No\" [(ngModel)]=\"ArrLoanSection.sancOrdNo\" maxlength=\"10\" name=\"sancOrdNo\" #sancOrdNo=\"ngModel\" [disabled]=\"IsDisabled\" pattern=\"^$|^[A-Za-z0-9]+\" (keypress)=\"charaterOnlyNoSpace($event)\" required>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdNo.errors?.required\">Sanction Order No is required</span>\r\n          </mat-error>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdNo.errors?.pattern\">Only Alphanumeric character's are allowed</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n \r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Total Loan Disbursed (Rs.)\" maxlength=\"6\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"ArrLoanSection.loanAmtDisbursed\" name=\"loanAmtDisbursed\"\r\n                 #loanAmtDisbursed=\"ngModel\" required [disabled]=\"IsDisabled\" (blur)=\"CalculateValue()\"> <!--(keyup)=\"CompareValue()\"-->\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtDisbursed.errors?.required\"> Loan Disbursed is required</span>\r\n          </mat-error>\r\n          <mat-error>\r\n            <span *ngIf=\"loanAmtDisbursed.errors?.min\">New WEF should be greater then current WEF!</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Installment Amount \" maxlength=\"7\" (blur)=\"CalculateValue()\" [(ngModel)]=\"ArrLoanSection.priInstAmt\" name=\"priInstAmt\" #priInstAmt=\"ngModel\"\r\n                 max=\"99999\" (keypress)=\"numberOnly($event)\" required [disabled]=\"IsDisabled\">\r\n          <mat-error>\r\n            <span [hidden]=\"!priInstAmt.errors?.required\">Installment Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Odd Installment Amount\" maxlength=\"7\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"ArrLoanSection.oddInstAmtPri\" name=\"oddInstAmtPri\" #oddInstAmtPri=\"ngModel\"\r\n                 required [disabled]=\"IsFieldDisabled\">\r\n          <mat-error>\r\n            <span [hidden]=\"!oddInstAmtPri.errors?.required\">Odd Inst Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"No. Of Installments Paid\" maxlength=\"7\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"ArrLoanSection.priTotInst\" name=\"priTotInst\" #priTotInst=\"ngModel\"\r\n                 required [disabled]=\"IsFieldDisabled\">\r\n          <mat-error>\r\n            <span [hidden]=\"!priTotInst.errors?.required\">No of Odd Inst is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"No. of Inst. incl. Odd instalment\" maxlength=\"7\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"ArrLoanSection.totalInstallment\" name=\"totalInstallment\" #totalInstallment=\"ngModel\"\r\n                 [disabled]=\"IsFieldDisabled\">\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Odd Installment No.\" maxlength=\"7\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"ArrLoanSection.oddInstNoPri\" name=\"oddInstNoPri\" #oddInstNoPri=\"ngModel\"\r\n                 [disabled]=\"IsFieldDisabled\">\r\n          <mat-error>\r\n            <span [hidden]=\"!oddInstNoPri.errors?.required\">This field is required</span>\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"FlagSave\" name=\"save\">{{buttonText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancel()\" *ngIf=\"FlagCancel\" name=\"Cancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwordDDOChecker()\" name=\"ForwardtoDDOChecker\" *ngIf=\"FlagForwordChecker\">Forward to DDO Checker</button>\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"accept()\" name=\"accept\" *ngIf=\"FlageAccept\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"FlagReject\" (click)=\"Reject()\">Reject</button>\r\n      </div>\r\n\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Loan Sanctioned Details</div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" [(ngModel)]=\"searchvalue\"  placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <ng-container matColumnDef=\"empCD\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n        <td mat-cell *matCellDef=\"let element\">{{element.empCD}} </td>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"description\">\r\n        <th mat-header-cell *matHeaderCellDef>Description </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.description}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"payLoanRefLoanShortDesc\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\">{{element.payLoanRefLoanShortDesc}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"empPersVerifFlag\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\">{{element.empPersVerifFlag}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"symbol\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          With E\r\n          ffect From\r\n        </th>\r\n        E\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <mat-form-field>\r\n            <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker></mat-datepicker>\r\n          </mat-form-field>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <span>\r\n            <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"ViewLoanDetailsById(element.empCD,element.msPayAdvEmpdetID,element.loanCd)\">error</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == '64' && userRoleID=='6'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditLoanDetailsById(element.empCD,element.msPayAdvEmpdetID,element.loanCd)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == '63' && userRoleID=='6'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditLoanDetailsById(element.empCD,element.msPayAdvEmpdetID,element.loanCd)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"(element.statusId == '64' || element.statusId == '61') && userRoleID=='6'\">\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" value=\"delete\" (click)=\"SetDeleteId(element.empCD,element.msPayAdvEmpdetID);deletepopup = !deletepopup\">delete_forever</a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteDetails(CtrCode,msPayAdvEmpdetID)\">OK</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/loanmgt/existingloan/existingloan.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/loanmgt/existingloan/existingloan.component.ts ***!
  \****************************************************************/
/*! exports provided: ExistingloanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExistingloanComponent", function() { return ExistingloanComponent; });
/* harmony import */ var _services_loan_mgmt_exist_loan_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/loan-mgmt/exist-loan.service */ "./src/app/services/loan-mgmt/exist-loan.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _model_LoanModel_existloanModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/LoanModel/existloanModel */ "./src/app/model/LoanModel/existloanModel.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ExistingloanComponent = /** @class */ (function () {
    function ExistingloanComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.callTypevar = 1;
        this.disbleflag = false;
        this.FlagSave = false;
        this.FlagCancel = false;
        this.FlagForwordChecker = false;
        this.FlageAccept = false;
        this.FlagReject = false;
        this.buttonText = 'Save';
        this.IsDisabled = false;
        this.IsFieldDisabled = true;
        this.comparisonError = '';
        this.Compvalue = false;
        this.displayedColumns = ['empCD', 'description', 'payLoanRefLoanShortDesc', 'empPersVerifFlag', 'Action'];
        this.selectedIndex = 0;
        this.ArrLoanSection = new _model_LoanModel_existloanModel__WEBPACK_IMPORTED_MODULE_4__["ExistloanModel"]();
    }
    ExistingloanComponent.prototype.ngOnInit = function () {
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.BindExistingLoanStatus(this.ddoid, this.UserID);
        this.BindLoanType();
        this.buttonText = "Save";
    };
    ExistingloanComponent.prototype.GetcommanMethod = function (value) {
        debugger;
        this.GetAleadytakenLoandetails(value);
    };
    ExistingloanComponent.prototype.changecalltype = function (value) {
        debugger;
        this.callTypevar = value;
    };
    ExistingloanComponent.prototype.GetAleadytakenLoandetails = function (value) {
        var _this = this;
        debugger;
        this.FlagSave = false;
        this.FlagCancel = false;
        this.FlagForwordChecker = false;
        this.FlageAccept = false;
        this.FlagReject = false;
        this.buttonText = "Save";
        sessionStorage.removeItem("MSPayAdvEmpdetID");
        sessionStorage.removeItem("EmployeeCd");
        sessionStorage.setItem('EmployeeCd', value);
        this.MsDesignID = '0';
        this._Service.LoanSection(this.ddoid, value).subscribe(function (data) {
            _this.ArrLoanSection = data;
            console.log(data);
            _this.tempId = _this.ArrLoanSection.msPayAdvEmpdetID;
            sessionStorage.setItem('MSPayAdvEmpdetID', String(_this.tempId));
            if (sessionStorage.userRoleID != 5) {
                if ((data.empCd == null || data.priVerifFlag == null || data == null)) {
                    _this.FlagSave = true;
                    _this.FlagCancel = true;
                    _this.FlagForwordChecker = false;
                    _this.IsDisabled = false;
                }
                if (data.priVerifFlag == 61 || data.priVerifFlag == 62) {
                    _this.FlagSave = false;
                    _this.FlagCancel = false;
                    _this.FlagForwordChecker = false;
                    _this.IsDisabled = true;
                }
                if (data.priVerifFlag == 64 || data.priVerifFlag == 63) {
                    _this.FlagSave = false;
                    _this.FlagCancel = false;
                    _this.FlagForwordChecker = true;
                    _this.IsDisabled = true;
                }
            }
            else {
                if (data.priVerifFlag == 61) {
                    _this.FlageAccept = true;
                    _this.FlagReject = true;
                    _this.FlagSave = false;
                    _this.FlagCancel = false;
                    _this.FlagForwordChecker = false;
                    _this.IsDisabled = true;
                }
            }
        });
    };
    ExistingloanComponent.prototype.BindExistingLoanStatus = function (ddoid, UserID) {
        var _this = this;
        this.IsDisabled = false;
        this._Service.BindExistingLoanStatus(ddoid, UserID, this.userRoleID.toString()).subscribe(function (data) {
            _this.ArrddlLoanStatus = data;
            if (_this.ArrddlLoanStatus != null) {
                if (data.empPersVerifFlag == 'EE') {
                    _this.IsDisabled = true;
                }
            }
            _this.tempdata = _this.ArrddlLoanStatus.PayLoanRefLoanShortDesc;
            _this.ArrddlLoanStatus.payLoanRefLoanCD = _this.tempdata;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    ExistingloanComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    ExistingloanComponent.prototype.CompareValue = function () {
        this.Compvalue = false;
        this.comparisonError = '';
        if (this.ArrLoanSection.loanAmtDisbursed != null && this.ArrLoanSection.loanAmtSanc != null) {
            this.Compvalue = false;
            if (this.ArrLoanSection.loanAmtDisbursed > this.ArrLoanSection.loanAmtSanc) {
                this.comparisonError = 'loanAmtDisbursed should less than sanction';
                this.Compvalue = true;
            }
            else {
                this.Compvalue = false;
            }
        }
    };
    ExistingloanComponent.prototype.BindLoanType = function () {
        var _this = this;
        this._Service.BindLoanType().subscribe(function (data) {
            _this.ArrddlLoanType = data;
        });
    };
    ExistingloanComponent.prototype.BindHeadAccount = function (value) {
        var _this = this;
        this._Service.BindHeadAccount(value).subscribe(function (data) {
            _this.ArrLoanSection.payLoanRefLoanHeadACP = data.payLoanRefLoanHeadACP;
        });
    };
    ExistingloanComponent.prototype.save = function () {
        var _this = this;
        debugger;
        var SchemeCode = this.ArrLoanSection.payLoanRefLoanHeadACP;
        var EmployeeCode = sessionStorage.getItem('EmployeeCd');
        //  var EmployeeCode = Number(sessionStorage.getItem('EmployeeCd'));
        var LoanCd = sessionStorage.getItem('LoanCode');
        var buttonName = document.activeElement.getAttribute("Name");
        if (buttonName == 'ForwardtoDDOChecker') {
            this.forwordDDOChecker();
        }
        else {
            if (this.buttonText == 'Save') {
                this.ArrLoanSection.EmpCd = EmployeeCode;
                this.ArrLoanSection.mode = 1;
                this.ArrLoanSection.schemeId = SchemeCode;
                this.ArrLoanSection.loanCD = LoanCd;
                this.ArrLoanSection.ddoId = this.ddoid;
                this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
                if (EmployeeCode != "") {
                    this._Service.PostData(this.ArrLoanSection).subscribe(function (data) {
                        if (data != "0") {
                            _this.ExitLoanForm.resetForm();
                            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.saveMsg);
                            _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                            _this.clear();
                        }
                        if (data == "-1") {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.alreadyExistMsg);
                        }
                    });
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(this.commonMsg.EmpCdSelectMsg);
                }
                EmployeeCode == "";
            }
            if (this.buttonText == 'Update') {
                debugger;
                this.ArrLoanSection.mode = 2;
                this.ArrLoanSection.EmpCd = EmployeeCode;
                this.ArrLoanSection.schemeId = SchemeCode;
                this.ArrLoanSection.loanCD = LoanCd;
                this.ArrLoanSection.ddoId = this.ddoid;
                this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
                if (EmployeeCode != "") {
                    this._Service.PostData(this.ArrLoanSection).subscribe(function (data) {
                        if (data != "0") {
                            _this.ExitLoanForm.resetForm();
                            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.updateMsg);
                            _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                            _this.clear();
                        }
                        if (data == "-1") {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.alreadyExistMsg);
                        }
                    });
                }
            }
        }
    };
    ExistingloanComponent.prototype.forwordDDOChecker = function () {
        var _this = this;
        debugger;
        this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd');
        this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
        this.ArrLoanSection.loanCD = sessionStorage.getItem('LoanCode');
        this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
        this.ArrLoanSection.mode = 1;
        this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(function (data) {
            if (data != "0") {
                _this.ExitLoanForm.resetForm();
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.forwardCheckerMsg);
                _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                _this.clear();
            }
        });
    };
    ExistingloanComponent.prototype.accept = function () {
        var _this = this;
        this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd');
        this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
        this.ArrLoanSection.loanCD = sessionStorage.getItem('LoanCode');
        this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
        this.ArrLoanSection.mode = 2;
        this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(function (data) {
            if (data != "0") {
                _this.ExitLoanForm.resetForm();
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.VerifyedByChecker);
                _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                _this.clear();
            }
        });
    };
    ExistingloanComponent.prototype.Reject = function () {
        var _this = this;
        this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd');
        this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
        this.ArrLoanSection.LoanCD = sessionStorage.getItem('LoanCode');
        this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
        this.ArrLoanSection.mode = 3;
        this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(function (data) {
            if (data != "0") {
                _this.ExitLoanForm.resetForm();
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.RejectedByChecker);
                _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                _this.clear();
            }
        });
    };
    ExistingloanComponent.prototype.GetLoanCode = function (value) {
        sessionStorage.setItem('LoanCode', value);
        this.BindHeadAccount(value);
    };
    ExistingloanComponent.prototype.ViewLoanDetailsById = function (EmpCd, msPayAdvEmpdetID) {
        var _this = this;
        debugger;
        this._Service.GetExistLoanDetailsByID(msPayAdvEmpdetID).subscribe(function (data) {
            _this.ArrLoanSection = data[0];
            var recovData = data[0].priVerifFlag;
            if (recovData == "61") {
                _this.FlagForwordChecker = false;
            }
            if (recovData == "63") {
                _this.FlagForwordChecker = false;
                _this.FlagSave = false;
            }
            if (recovData == "64" && _this.userRoleID == 6) {
                _this.FlagForwordChecker = false;
            }
            if (data != null || data != undefined) {
                _this.FlagSave = false;
                _this.FlagCancel = false;
                _this.IsDisabled = true;
            }
            else {
                _this.FlagForwordChecker = true;
            }
        });
    };
    ExistingloanComponent.prototype.EditLoanDetailsById = function (empCD, msPayAdvEmpdetID, LoanCd) {
        var _this = this;
        debugger;
        sessionStorage.removeItem("MSPayAdvEmpdetID");
        sessionStorage.removeItem("EmployeeCd");
        sessionStorage.removeItem("LoanCode");
        sessionStorage.setItem('MSPayAdvEmpdetID', msPayAdvEmpdetID);
        sessionStorage.setItem('EmployeeCd', empCD);
        sessionStorage.setItem('LoanCode', LoanCd);
        this.buttonText = "Update";
        this._Service.GetExistLoanDetailsByID(msPayAdvEmpdetID).subscribe(function (data) {
            _this.ArrLoanSection = data[0];
            var recovData = data[0].priVerifFlag;
            if (data != null || data != undefined) {
                _this.FlagSave = true;
                _this.FlagCancel = false;
                _this.IsDisabled = false;
            }
            if (recovData == 62) {
                _this.FlagForwordChecker = false;
            }
            if (_this.ArrLoanSection.priVerifFlag == 61 && _this.userRoleID == 5) {
                _this.FlagSave = false;
                _this.FlageAccept = true;
                _this.FlagReject = true;
            }
            else {
                _this.FlagForwordChecker = true;
            }
            _this.BindLoanType();
        });
    };
    ExistingloanComponent.prototype.clear = function () {
        this.ExitLoanForm.resetForm();
    };
    ExistingloanComponent.prototype.cancel = function () {
        this.buttonText = "Save";
        this.ExitLoanForm.resetForm();
    };
    ExistingloanComponent.prototype.charaterOnlyNoSpace = function (event) {
        debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    ExistingloanComponent.prototype.CalculateValue = function () {
        debugger;
        if (this.ArrLoanSection.loanAmtDisbursed != undefined && this.ArrLoanSection.priInstAmt != undefined) {
            var IsFloat = (this.ArrLoanSection.loanAmtDisbursed) % (this.ArrLoanSection.priInstAmt);
            if (IsFloat != 0) {
                this.ArrLoanSection.oddInstNoPri = 1;
            }
            else {
                this.ArrLoanSection.oddInstNoPri = 0;
            }
            this.ArrLoanSection.priTotInst = Math.floor(((this.ArrLoanSection.loanAmtDisbursed) / (this.ArrLoanSection.priInstAmt)));
            this.temp = Math.floor((this.ArrLoanSection.priInstAmt) * this.ArrLoanSection.priTotInst);
            this.ArrLoanSection.oddInstAmtPri = Math.floor(this.ArrLoanSection.loanAmtDisbursed - (this.temp));
            this.ArrLoanSection.totalInstallment = Math.floor((this.ArrLoanSection.oddInstNoPri) + (this.ArrLoanSection.priTotInst));
        }
    };
    ExistingloanComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    ExistingloanComponent.prototype.SetDeleteId = function (CtrCode, msPayAdvEmpdetID) {
        this.CtrCode = CtrCode;
        this.msPayAdvEmpdetID = msPayAdvEmpdetID;
    };
    ExistingloanComponent.prototype.DeleteDetails = function (CtrlCode, msPayAdvEmpdetID) {
        var _this = this;
        debugger;
        this._Service.DeleteEmpDetails(CtrlCode, msPayAdvEmpdetID, 4).subscribe(function (data) {
            if (data != null) {
                _this.BindExistingLoanStatus(_this.ddoid, _this.UserID);
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.commonMsg.deleteMsg);
                _this.deletepopup = false;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ExistingloanComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], ExistingloanComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ExitLoan'),
        __metadata("design:type", Object)
    ], ExistingloanComponent.prototype, "ExitLoanForm", void 0);
    ExistingloanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-existingloan',
            template: __webpack_require__(/*! ./existingloan.component.html */ "./src/app/loanmgt/existingloan/existingloan.component.html"),
            styles: [__webpack_require__(/*! ./existingloan.component.css */ "./src/app/loanmgt/existingloan/existingloan.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_loan_mgmt_exist_loan_service__WEBPACK_IMPORTED_MODULE_0__["ExistLoanService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], ExistingloanComponent);
    return ExistingloanComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/floodadv/floodadv.component.css":
/*!*********************************************************!*\
  !*** ./src/app/loanmgt/floodadv/floodadv.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#lbl {\r\n  background-color: #bfbfbf;\r\n  line-height: 0px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbm1ndC9mbG9vZGFkdi9mbG9vZGFkdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMEJBQTBCO0VBQzFCLGlCQUFpQjtDQUNsQiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvZmxvb2RhZHYvZmxvb2RhZHYuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsYmwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNiZmJmYmY7XHJcbiAgbGluZS1oZWlnaHQ6IDBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/loanmgt/floodadv/floodadv.component.html":
/*!**********************************************************!*\
  !*** ./src/app/loanmgt/floodadv/floodadv.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--maxlength=\"10\"-->\r\n\r\n<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-4 col-lg-5\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select (selectionChange)=\"onBillGrpSel($event.value)\" placeholder=\"Select Pay Bill Group\">\r\n          <mat-option *ngFor=\"let Bill of BillGroup\" value=\"{{Bill.payBillGroupId}}\">{{Bill.billgrDesc}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-4 col-lg-5\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <mat-select placeholder=\"Select Designation\" (selectionChange)=\"GetDetailsForFlood($event.value)\">\r\n          <!--<mat-option  value=\"SelectAll\" >Select All</mat-option>-->\r\n          <mat-option *ngFor=\"let Desig of DesigGroup\" value=\"{{Desig.desigId}}\">{{Desig.desigDesc}}{{Desig.desigId}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"col-md-6\">\r\n  <div class=\"col-md-12 col-lg-12 col-xs-12\">\r\n      <form #FloodLaon=\"ngForm\" novalidate>\r\n\r\n        <mat-card>\r\n          <div class=\"fom-title\">Sanction Order Details for Flood Advance</div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Sanction Order No.\" name=\"SancOrdNo\" [(ngModel)]=\"ArrLoanData.SancOrdNo\" #SancOrdNo=\"ngModel\"  maxlength=\"20\" required>\r\n                <!--<mat-error>\r\n                <span [hidden]=\"!SancOrdNo.errors?.required\">Sanction Order No is required</span>\r\n              </mat-error>-->\r\n\r\n                <span *ngIf=\"FloodLaon.submitted && SancOrdNo.invalid\" class=\"invalid-feedback\">\r\n                  <span *ngIf=\"!SancOrdNo.errors?.required\">SanctionNo is required</span>\r\n                </span>\r\n              </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput [matDatepicker]=\"picker2\" placeholder=\"Sanction Order Date\"\r\n                       readonly (click)=\"picker2.open()\" name=\"SancOrdDT\" [(ngModel)]=\"ArrLoanData.SancOrdDT\" #SancOrdDT=\"ngModel\" required>\r\n                <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n                <mat-datepicker #picker2></mat-datepicker>\r\n                <mat-error>\r\n                  <span [hidden]=\"!SancOrdDT.errors?.required\">Sanction Date is required</span>\r\n                </mat-error>\r\n\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Amount Disbursed\" name=\"LoanAmtDisbursed\" value=\"7500\" disabled>\r\n\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Recovered under Scheme\" name=\"SchemeId\" [(ngModel)]=\"ArrLoanData.SchemeId\" #SchemeId=\"ngModel\" maxlength=\"17\" required>\r\n\r\n                <mat-error>\r\n                  <span [hidden]=\"!SchemeId.errors?.required\"> Account Head is required </span>\r\n                </mat-error>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div class=\"fom-title\">Principal Recovery Schedule</div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"No of Installments\" name=\"PriTotInst\" [(ngModel)]=\"ArrLoanData.PriTotInst\" #PriTotInst=\"ngModel\" (keypress)=\"numberOnly($event)\" (blur)=\"CalculateValue($event.target.value)\" maxlength=\"4\" required>\r\n                <mat-error>\r\n                  <span [hidden]=\"!PriTotInst.errors?.required\">No of Installments is required</span>\r\n                </mat-error>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Installment Amount\" name=\"PriInstAmt\" [(ngModel)]=\"ArrLoanData.PriInstAmt\" #PriInstAmt=\"ngModel\" (keypress)=\"numberOnly($event)\" disabled required>\r\n                <mat-error>\r\n                  <span [hidden]=\"!PriInstAmt.errors?.required\">Installment Amount is required</span>\r\n                </mat-error>\r\n\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Last no. of Installment Paid\" name=\"PriLstInstRec\" [(ngModel)]=\"ArrLoanData.PriLstInstRec\" #PriLstInstRec=\"ngModel\" (keypress)=\"numberOnly($event)\" (blur)=\"CalculatePriBal($event.target.value)\" maxlength=\"4\" required>\r\n                <mat-error>\r\n                  <span [hidden]=\"!PriLstInstRec.errors?.required\">Last no. of Installment is required</span>\r\n                </mat-error>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button class=\"btn btn-success\" (click)=\"FloodLaon.onSubmit();FloodLaon.valid && SaveRecords(ArrLoanData)()\" *ngIf=\"FlagSave\">Save</button>\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancel()\" name=\"Cancel\" *ngIf=\"FlagCancel\">Cancel</button>\r\n            <button type=\"submit\" class=\"btn btn-warning\" (click)=\"FloodLaon.onSubmit();FloodLaon.valid && ForwardtoDDOChecker(ArrLoanData)\" name=\"ForwardToDDOChecker\" *ngIf=\"FlagForwordChecker\">Forward to DDO Checker</button>\r\n\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"FloodLaon.onSubmit();FloodLaon.valid && accept()\" name=\"Accept\" *ngIf=\"FlageAccept\">Accept</button>\r\n            <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"FlagReject\" (click)=\"FloodLaon.onSubmit();FloodLaon.valid && Reject()\">Reject</button>\r\n\r\n          </div>\r\n        </mat-card>\r\n\r\n      </form>\r\n  </div>\r\n  </div>\r\n    <div class=\"col-md-6\">\r\n      <div class=\"col-md-12 col-lg-12 col-xs-12\">\r\n        <mat-card>\r\n          <div class=\"fom-title\">Select employees from the table and click on 'Attach' (or 'Detach') to add (or remove) employees to/from the current sanction order.</div>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n            <ng-container matColumnDef=\"index\">\r\n              <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n              <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n            </ng-container>\r\n\r\n\r\n            <ng-container matColumnDef=\"select\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n                <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                              [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n              </th>\r\n              <mat-cell *matCellDef=\"let row\">\r\n                <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null; AddCheckedObj()\"\r\n                              [checked]=\"selectionAttach.isSelected(row)\">\r\n                </mat-checkbox>\r\n              </mat-cell>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"EmpFirstName\" style=\"width:120px;\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empFirstName}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"EmpCd\" style=\"width:120px;\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Design Desc  </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.desigDesc}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"priVerifFlag\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.priVerifFlag}}</td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns; let i = index\"></tr>\r\n          </table>\r\n\r\n          <mat-paginator [pageSizeOptions]=\"[3, 10]\" showFirstLastButtons></mat-paginator>\r\n          <div class=\"col-lg-12 text-center attach-dattach-wraper\">\r\n            <button mat-raised-button class=\"attach-btn\" (click)=\"DeAttachSelectedRows()\">\r\n              Attach\r\n            </button>\r\n\r\n            <button mat-raised-button class=\"dattach-btn\" (click)=\"AttachSelectedRows()\">\r\n              DeAttach\r\n            </button>\r\n\r\n          </div>\r\n        </mat-card>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 col-xs-12\">\r\n        <mat-card>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpFirstName\" style=\"width:120px;\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empFirstName}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"EmpCd\" style=\"width:120px;\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Design Desc  </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.desigDesc}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"priVerifFlag\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.priVerifFlag}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"select\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n                <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                              [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n              </th>\r\n              <mat-cell *matCellDef=\"let row\">\r\n                <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                              [checked]=\"selectionDeAttach.isSelected(row)\">\r\n                </mat-checkbox>\r\n              </mat-cell>\r\n            </ng-container>\r\n\r\n\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <mat-paginator [pageSizeOptions]=\"[3, 10]\" showFirstLastButtons></mat-paginator>\r\n          <div [hidden]=\"isTableHasData\">\r\n            No Data Found\r\n          </div>\r\n        </mat-card>\r\n      </div>\r\n      </div>\r\n    \r\n  \r\n"

/***/ }),

/***/ "./src/app/loanmgt/floodadv/floodadv.component.ts":
/*!********************************************************!*\
  !*** ./src/app/loanmgt/floodadv/floodadv.component.ts ***!
  \********************************************************/
/*! exports provided: FloodadvComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FloodadvComponent", function() { return FloodadvComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/promotions/promotions.service */ "./src/app/services/promotions/promotions.service.ts");
/* harmony import */ var _services_loan_mgmt_flood_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/flood-service */ "./src/app/services/loan-mgmt/flood-service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _model_LoanModel_floodModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../model/LoanModel/floodModel */ "./src/app/model/LoanModel/floodModel.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { ExistLoanService } from '../../services/loan-mgmt/exist-loan.service';
//import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
//import { Router } from '@angular/router';
//import { ReplaySubject } from 'rxjs';
//import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
//import { LeavesSanctionMainModel } from '../../model/LeavesModel/leavesSanctionModel';


//import { locale } from 'moment';





//import { and } from '@angular/router/src/utils/collection';
//interface BillCode {
//  id: string;
//  name: string;
//}
var FloodadvComponent = /** @class */ (function () {
    //showErrors: boolean = false;
    function FloodadvComponent(_PromotionsService, _FloodService, commonMsg) {
        this._PromotionsService = _PromotionsService;
        this._FloodService = _FloodService;
        this.commonMsg = commonMsg;
        this.dataDeAttach = [];
        this.isTableHasData = true;
        this.checkedObject = [];
        this.DcheckedObject = [];
        this.totalEmpcode = [];
        this.DisbursedAmnt = 7500;
        this.btnHideShow = false;
        this.FlagSave = false;
        this.FlagCancel = false;
        this.FlagForwordChecker = false;
        this.FlageAccept = false;
        this.FlagReject = false;
        this.displayedColumns = ['select', 'EmpFirstName', 'EmpCd', 'DesigDesc', 'priVerifFlag',];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
        this.ArrLoanData = new _model_LoanModel_floodModel__WEBPACK_IMPORTED_MODULE_5__["FloodModel"]();
    }
    FloodadvComponent.prototype.ngOnInit = function () {
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        //this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.username = sessionStorage.getItem('username');
        this.BindDropDownBillGroup();
    };
    FloodadvComponent.prototype.SaveRecords = function () {
        var _this = this;
        debugger;
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(this.commonMsg.AttachAtleastOne);
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.totalEmpcode.push(d.empCd);
                });
            }
            this.ArrLoanData.totalEmpcode = this.totalEmpcode;
            this._FloodService.InsertFloodData(this.ArrLoanData).subscribe(function (result) {
                _this.EmpDetails = result;
                //alert(this.EmpDetails);
                if (_this.EmpDetails != 0 && _this.EmpDetails != -1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.saveMsg);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.alreadyExistMsg);
                }
            });
        }
        //this.checkedObject = null;
        this.totalEmpcode = [];
    };
    FloodadvComponent.prototype.ForwardtoDDOChecker = function () {
        var _this = this;
        if (this.checkedObject.length == 0 || this.checkedObject == null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(this.commonMsg.AttachAtleastOne);
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.totalEmpcode.push(d.empCd);
                });
            }
            this.ArrLoanData.totalEmpcode = this.totalEmpcode;
            this.ArrLoanData.Mode = 1;
            this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(function (result) {
                _this.EmpDetails = result;
                //alert(this.EmpDetails);
                if (_this.EmpDetails != 0 && _this.EmpDetails != -1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.forwardCheckerMsg);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.noRecordMsg);
                }
            });
        }
        this.totalEmpcode = [];
    };
    FloodadvComponent.prototype.accept = function () {
        var _this = this;
        if (this.checkedObject.length == 0 || this.checkedObject == null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(this.commonMsg.AttachAtleastOne);
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.totalEmpcode.push(d.empCd);
                });
            }
            this.ArrLoanData.totalEmpcode = this.totalEmpcode;
            this.ArrLoanData.Mode = 2;
            this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(function (result) {
                _this.EmpDetails = result;
                //alert(this.EmpDetails);
                if (_this.EmpDetails != 0 && _this.EmpDetails != -1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.VerifyedByChecker);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.noRecordMsg);
                }
            });
        }
        this.totalEmpcode = [];
    };
    FloodadvComponent.prototype.Reject = function () {
        var _this = this;
        if (this.checkedObject.length == 0 || this.checkedObject == null) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(this.commonMsg.AttachAtleastOne);
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.totalEmpcode.push(d.empCd);
                });
            }
            this.ArrLoanData.totalEmpcode = this.totalEmpcode;
            this.ArrLoanData.Mode = 3;
            this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(function (result) {
                _this.EmpDetails = result;
                //alert(this.EmpDetails);
                if (_this.EmpDetails != 0 && _this.EmpDetails != -1) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.RejectedByChecker);
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.commonMsg.noRecordMsg);
                }
            });
        }
        this.totalEmpcode = [];
    };
    //toggleErrors() {
    //  this.showErrors = !this.showErrors;
    //}
    FloodadvComponent.prototype.BindDropDownBillGroup = function () {
        var _this = this;
        this._PromotionsService.BindDropDownBillGroup().subscribe(function (result) {
            _this.BillGroup = result;
        });
    };
    FloodadvComponent.prototype.BindDropDownDesigGroup = function (selectedBilgrp) {
        var _this = this;
        localStorage.setItem('BillGrpId', selectedBilgrp);
        this._PromotionsService.BindDropDownDesigGroup(selectedBilgrp).subscribe(function (result) {
            _this.DesigGroup = result;
            //console.log(this.DesigGroup);
        });
    };
    FloodadvComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    FloodadvComponent.prototype.onBillGrpSel = function (selectedBilgrp) {
        this.BindDropDownDesigGroup(selectedBilgrp);
    };
    FloodadvComponent.prototype.GetDetailsForFlood = function (desigId) {
        var _this = this;
        this.btnHideShow = true;
        this.FlagSave = true;
        this.FlagCancel = true;
        this.FlagForwordChecker = true;
        if (this.userRoleID == 5) {
            this.FlageAccept = true;
            this.FlagReject = true;
            this.FlagSave = true;
            this.FlagCancel = true;
            this.FlagForwordChecker = true;
            this.mode = "2";
        }
        else {
            this.mode = "1";
        }
        var billGrID = localStorage.getItem('BillGrpId');
        this._FloodService.GetEmpDetails(billGrID, desigId, this.UserID, this.mode).subscribe(function (result) {
            _this.EmpDetails = result;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](result);
            _this.data = Object.assign(result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    FloodadvComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    FloodadvComponent.prototype.isAllSelectedForDeAttach = function () {
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    FloodadvComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
        this.checkedObject = this.dataSource.data;
        this.checkedObject = this.selectionAttach.selected;
        // alert(this.selectionAttach.selected);
    };
    //working
    FloodadvComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
        // this.checkedObject = this.dataSourceDeAttach.data;
        this.DcheckedObject = this.dataSourceDeAttach.data;
        // this.DcheckedObject = this.selectionDeAttach.selected;
    };
    FloodadvComponent.prototype.AddCheckedObj = function () {
        this.checkedObject = null;
        this.checkedObject = this.selectionAttach.selected;
    };
    FloodadvComponent.prototype.AttachSelectedRows = function () {
        var _this = this;
        this.checkedObject = null;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                // this.dataDeAttach.push(item);
                _this.dataDeAttach.unshift(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSource.paginator = _this.paginator;
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(this.commonMsg.AttachAtleastOne);
        }
    };
    FloodadvComponent.prototype.DeAttachSelectedRows = function () {
        var _this = this;
        this.checkedObject = null;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                //this.data.push(item);
                _this.data.unshift(item);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_4__["SelectionModel"](true, []);
            this.dataSource.paginator = this.paginator;
        }
        else {
            alert(this.commonMsg.DeAttachAtleastOne);
        }
    };
    FloodadvComponent.prototype.cancel = function () {
        this.FloodLaon.resetForm();
    };
    FloodadvComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    FloodadvComponent.prototype.CalculateValue = function (value) {
        this.ArrLoanData.PriInstAmt = Math.round(7500 / value);
        if (this.ArrLoanData.PriInstAmt == Infinity) {
            this.ArrLoanData.PriInstAmt = 0;
        }
    };
    FloodadvComponent.prototype.CalculatePriBal = function (value) {
        //var a = parseInt(value);
        //console.log(a);
        this.ArrLoanData.PriBalance = 0;
        var paidAmt = this.ArrLoanData.PriInstAmt * parseInt(value);
        var PriBalance = 7500 - paidAmt;
        this.ArrLoanData.PriBalance = PriBalance;
        // alert(PriBalance);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], FloodadvComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], FloodadvComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('FloodLaon'),
        __metadata("design:type", Object)
    ], FloodadvComponent.prototype, "FloodLaon", void 0);
    FloodadvComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-floodadv',
            template: __webpack_require__(/*! ./floodadv.component.html */ "./src/app/loanmgt/floodadv/floodadv.component.html"),
            styles: [__webpack_require__(/*! ./floodadv.component.css */ "./src/app/loanmgt/floodadv/floodadv.component.css")],
            providers: [_services_loan_mgmt_flood_service__WEBPACK_IMPORTED_MODULE_2__["FloodService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_1__["PromotionsService"], _services_loan_mgmt_flood_service__WEBPACK_IMPORTED_MODULE_2__["FloodService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]])
    ], FloodadvComponent);
    return FloodadvComponent;
}());

//const ELEMENT_DeAttachdata: EmpDeAttach[] = [];
//const ELEMENT_DATA: EmpAttach[] = [];
//export interface EmpAttach {
//  EmpFirstName: string;
//  EmpCd: string;
//  DesigDesc: string;
//}
//export interface EmpDeAttach {
//  EmpFirstName: string;
//  EmpCd: string;
//  DesigDesc: string;
//}


/***/ }),

/***/ "./src/app/loanmgt/hba-verify/hba-verify.component.css":
/*!*************************************************************!*\
  !*** ./src/app/loanmgt/hba-verify/hba-verify.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvaGJhLXZlcmlmeS9oYmEtdmVyaWZ5LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/loanmgt/hba-verify/hba-verify.component.html":
/*!**************************************************************!*\
  !*** ./src/app/loanmgt/hba-verify/hba-verify.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst></app-comman-mst>\r\n\r\n<div class=\"col-sm-12 col-md-12 col-lg-7\">\r\n  <mat-card class=\"example-card\">\r\n    <div class=\"fom-title\">Details</div>\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n      <p class=\"peragraph-input\">I have Scrutinized the applicationn of Shri/Smt./Kumari</p>\r\n      <mat-form-field class=\"float-left\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">may be approved in relaxation of rule 4(b) of House Building Advance Rules.</p>\r\n    </div>\r\n\r\n\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput [matDatepicker]=\"DOA\" disabled name=\"Date of appointment\" placeholder=\"Date of appointment\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"DOA\"></mat-datepicker-toggle>\r\n        <mat-datepicker #DOA></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput [matDatepicker]=\"DOR\" disabled name=\"Date of retirement\" placeholder=\"Date of retirement\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"DOR\"></mat-datepicker-toggle>\r\n        <mat-datepicker #DOR></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Pay\" title=\"Pay\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Pay\" title=\"Pay\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Pay\" title=\"Pay\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"50 Months Pay\" title=\"50 Months Pay\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Cost Ceiling for Construction\" title=\"Cost Ceiling for Construction\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Amount of GP Fund Withdrawal / other advance to be adjusted\" title=\"Amount of GP Fund Withdrawal / other advance to be adjusted\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Advance Entitled\" title=\"Advance Entitled\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Estimated / Assessed cost of construction \" title=\"Estimated / Assessed cost of construction \">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Expected date of starting recovery \" title=\"Expected date of starting recovery \">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"No. of Monthly Installment\" title=\"No. of Monthly Installment\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"fom-title\">Repaying Capacity Details</div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Proposed rate of recovery\" title=\"Proposed rate of recovery\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Amount recoverable at the proposed rate of recovery\" title=\"Amount recoverable at the proposed rate of recovery\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Amount Adjustable from DCRG \" title=\"Amount Adjustable from DCRG \">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Total\" title=\"Total\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Apporximate Amount of Intrest\" title=\"Apporximate Amount of Intrest\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Amount of advance that can be sanctioned based on repaying capacity\" title=\"Amount of advance that can be sanctioned based on repaying capacity\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-4\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput disabled placeholder=\"Amount of HBA recommended for sanctioned\" title=\"Amount of HBA recommended for sanctioned\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n\r\n    <div class=\"fom-title\">Rate of Recovery Details</div>\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n      <p class=\"peragraph-input\">Principal (Rs.) </p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">in</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\"> installments of Rs</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">  each</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n      <p class=\"peragraph-input\">Principal (Rs.) </p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">in</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\"> installments of Rs</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">  each</p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n      <p class=\"peragraph-input\">An Advance Rs </p>\r\n      <mat-form-field class=\"float-left fillinthe-blanks\">\r\n        <input matInput placeholder=\"\" title=\"\">\r\n      </mat-form-field>\r\n      <p class=\"peragraph-input\">may be approved in relaxation of rule 4(b) of House Building Advance Rules.</p>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-danger\">Reject</button>\r\n      <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n      <button class=\"btn btn-primary\" type=\"button\"> Forward to HOO Checker </button>\r\n    </div>\r\n\r\n  </mat-card>\r\n</div>\r\n\r\n\r\n\r\n\r\n<!--right table-->\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n    <div class=\"fom-title\">Tabel Details</div>\r\n\r\n\r\n\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/loanmgt/hba-verify/hba-verify.component.ts":
/*!************************************************************!*\
  !*** ./src/app/loanmgt/hba-verify/hba-verify.component.ts ***!
  \************************************************************/
/*! exports provided: HbaVerifyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HbaVerifyComponent", function() { return HbaVerifyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HbaVerifyComponent = /** @class */ (function () {
    function HbaVerifyComponent() {
    }
    HbaVerifyComponent.prototype.ngOnInit = function () {
    };
    HbaVerifyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hba-verify',
            template: __webpack_require__(/*! ./hba-verify.component.html */ "./src/app/loanmgt/hba-verify/hba-verify.component.html"),
            styles: [__webpack_require__(/*! ./hba-verify.component.css */ "./src/app/loanmgt/hba-verify/hba-verify.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HbaVerifyComponent);
    return HbaVerifyComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/inst-recovry/inst-recovry.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/loanmgt/inst-recovry/inst-recovry.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvaW5zdC1yZWNvdnJ5L2luc3QtcmVjb3ZyeS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/loanmgt/inst-recovry/inst-recovry.component.html":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/inst-recovry/inst-recovry.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\"></app-comman-mst>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n  <form #InstRecover=\"ngForm\" (ngSubmit)=\"InstRecover.valid && Save(InstEmpDetais);\" novalidate>\r\n    <div class=\"mat-card\">\r\n      <div class=\"fom-title\"> Loan Recovery  Details </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\" [(ngModel)]=\"RecoveryDetails.loanCD\" name=\"loanCD\" #loanCD=\"ngModel\" (selectionChange)=\"BindSanctionDetails($event.value)\">\r\n            <mat-option label=\"Select Loan Type\" disabled=\"disabled\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of RecoveryDetails\" [value]=\"Loantype.loanCD\">\r\n              {{Loantype.payloanrefloandesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\" Pay Month\" title=\" Pay Month\" [(ngModel)]=\"InstEmpDetais.billMonth\" name=\"billMonth\" #billMonth=\"ngModel\" disabled=\"disabled\">\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order No./ Letter No\" title=\"Order No./ Letter No\" [(ngModel)]=\"InstEmpDetais.orderNo\" name=\"orderNo\" #OrderNo=\"ngModel\" required [disabled]=IsDisabled>\r\n          <!--<mat-error>\r\n            <span [hidden]=\"!orderNo.errors?.required\">Order No is required</span>\r\n          </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker2\" placeholder=\"Order Date\" [(ngModel)]=\"InstEmpDetais.orderDt\" name=\"orderDt\" #OrderDt=\"ngModel\" required [disabled]=IsDisabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker2></mat-datepicker>\r\n          <!--<mat-error>\r\n            <span [hidden]=\"!OrderDate.errors?.required\">Order Date is required</span>\r\n          </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"No. of Installments to be recovered\" (keypress)=\"numberOnly($event)\" title=\"No. of Installments to be recovered\" maxlength=\"3\" [(ngModel)]=\"InstEmpDetais.totAmtRecov\" #TotAmtRecov=\"ngModel\" name=\"TotAmtRecov\" required [disabled]=IsDisabled>\r\n          <!--<mat-error>\r\n             <span [hidden]=\"!recoveredInstNo.errors?.required\">Installments recovered is required</span>\r\n          </mat-error>-->\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Amount to be deducted\" (keypress)=\"numberOnly($event)\" title=\"Amount to be deducted\" disabled=\"disabled\">\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"FlagSave\">{{btnname}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" *ngIf=\"FlagCancel\" (click)=\"cancel()\" name=\"Cancel\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" *ngIf=\"FlagForwordChecker\" (click)=\"ForwardtoDDOChecker(InstEmpDetais)\" name=\"ForwardtoDDOChecker\">Forward to DDO Checker</button>\r\n\r\n        <!--<button type=\"submit\" class=\"btn btn-primary\"  name=\"Update\" *ngIf=\"FlagUpdate\"  (click)=\"Update()\" >Update</button>-->\r\n        <button type=\"button\" class=\"btn btn-success\" *ngIf=\"FlageAccept\" (click)=\"Accept()\" name=\"accept\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"FlagReject\" (click)=\"Reject()\" name=\"Reject\">Reject</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n\r\n\r\n<div class=\"col-md-6 col-lg-6\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <div class=\"fom-title\">Current Details </div>\r\n    <!--<mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>-->\r\n\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <!--<table>table</table>-->\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"loanType\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\">{{element.payloanrefloandesc}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"sancOrdNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order No </th>\r\n        <td mat-cell *matCellDef=\"let element\">{{element.sancOrdNo}}</td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"billMonth\">\r\n        <th mat-header-cell *matHeaderCellDef>Pay Month</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.billMonth}} </td>\r\n      </ng-container>\r\n\r\n      <!--<ng-container matColumnDef=\"empCd\" hidden>\r\n        <th mat-header-cell *matHeaderCellDef>Emp Cd</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n      </ng-container>-->\r\n\r\n\r\n\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <span *ngIf=\"element.statusId == '64' && userRoleID=='6'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditLoanDetailsById(element.msEmpLoanMultiInstId,element.empCD)\">edit</a>\r\n          </span>\r\n\r\n\r\n          <span *ngIf=\"element.statusId == '64' && userRoleID=='6'\">\r\n            <a class=\"material-icons i-delete\" matTooltip=\"delete\" (click)=\"setDeleteId(element.msEmpLoanMultiInstId,element.empCD);deletepopup = !deletepopup\">delete</a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n\r\n    <div class=\"fom-title margin-top-10px\">Details </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Sanctioned Order Date\" title=\"Sanctioned Order Date\" [(ngModel)]=\"SanctionDetails.sancOrdDT\" name=\"sancOrdDT\" #sancOrdDT=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Sanctioned Order No\" [(ngModel)]=\"SanctionDetails.sancOrdNo\" name=\"sancOrdNo\" #sancOrdNo=\"ngModel\" title=\"Sanctioned Order No\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Loan Amount Disbursed\" title=\"Loan Amount Disbursed\" [(ngModel)]=\"SanctionDetails.loanAmtDisbursed\" name=\"loanAmtDisbursed\" #loanAmtDisbursed=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Total No. of Installments\" title=\"Total No. of Installments\" [(ngModel)]=\"SanctionDetails.priTotInst\" name=\"priTotInst\" #priTotInst=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Installment Amount\" title=\"Installment Amount\" [(ngModel)]=\"SanctionDetails.priTotInst\" name=\"priTotInst\" #priTotInst=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Odd Installment No\" title=\"Odd Installment No\" [(ngModel)]=\"SanctionDetails.oddInstNoPri\" name=\"oddInstNoPri\" #oddInstNoPri=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Odd Installment Amount\" title=\"Odd Installment Amount\" [(ngModel)]=\"SanctionDetails.oddInstAmtPri\" name=\"oddInstAmtPri\" #oddInstAmtPri=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Last Installment No. Recovered\" title=\"Last Installment No. Recovered\" [(ngModel)]=\"SanctionDetails.priLstInstRec\" name=\"priLstInstRec\" #priLstInstRec=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Outstanding Amount\" title=\"Outstanding Amount\" [(ngModel)]=\"SanctionDetails.outstandingAmt\" name=\"outstandingAmt\" #outstandingAmt=\"ngModel\" disabled=\"disabled\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteLoanDetailsById(setDeletIDOnPopup,setDeletIDOnPopup2)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/inst-recovry/inst-recovry.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/loanmgt/inst-recovry/inst-recovry.component.ts ***!
  \****************************************************************/
/*! exports provided: InstRecovryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstRecovryComponent", function() { return InstRecovryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_loan_mgmt_inst_recovery_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/loan-mgmt/inst-recovery-service */ "./src/app/services/loan-mgmt/inst-recovery-service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _model_LoanModel_EmpSanctionModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/LoanModel/EmpSanctionModel */ "./src/app/model/LoanModel/EmpSanctionModel.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InstRecovryComponent = /** @class */ (function () {
    function InstRecovryComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.callTypevar = 1;
        this.RecoveryDetails = [];
        this.SanctionDetails = new _model_LoanModel_EmpSanctionModel__WEBPACK_IMPORTED_MODULE_3__["EmpSanction"]();
        this.InstEmpDetais = new _model_LoanModel_EmpSanctionModel__WEBPACK_IMPORTED_MODULE_3__["InstEmpModel"]();
        this.IsDisabled = false;
        this.FlagSave = false;
        this.FlagCancel = false;
        this.FlagForwordChecker = false;
        this.FlageAccept = false;
        this.FlagReject = false;
        this.FlagUpdate = false;
        this.displayedColumns = ['loanType', 'sancOrdNo', 'billMonth', 'action'];
        //this.RecoveryDetails = new ExistloanModel();
        //this.SanctionDetails = new EmpSanction();
    }
    InstRecovryComponent.prototype.ngOnInit = function () {
        this.btnname = 'Save';
        this.EmpSanc = new _model_LoanModel_EmpSanctionModel__WEBPACK_IMPORTED_MODULE_3__["EmpSancModel"]();
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.deletepopup = false;
    };
    InstRecovryComponent.prototype.GetcommanMethod = function (value) {
        this.GetRecoveryEmpDetails(value);
    };
    InstRecovryComponent.prototype.GetRecoveryEmpDetails = function (value) {
        var _this = this;
        this.FlagSave = false;
        this.FlageAccept = false;
        this.FlagReject = false;
        this.FlagForwordChecker = false;
        this.FlagCancel = false;
        this.dataSource = null;
        //this.cancel();
        this.InstRecoverForm.resetForm();
        this.RecoveryDetails = [];
        sessionStorage.setItem("EmpCd", value);
        this._Service.GetRecoveryEmpDetails(value).subscribe(function (data) {
            _this.RecoveryDetails = data;
            // console.log(this.RecoveryDetails);
            if (data != null || data != undefined) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSource.sort = _this.sort;
            }
            else {
                _this.dataSource = null;
            }
        });
    };
    InstRecovryComponent.prototype.BindSanctionDetails = function (value) {
        var _this = this;
        // this.cancel();
        this.InstRecoverForm.resetForm();
        this.EmpCD = sessionStorage.getItem("EmpCd");
        this._Service.BindSanctionDetails(value, this.EmpCD).subscribe(function (data) {
            if (data[0] == null) {
                _this.btnname = 'Save';
                _this.FlagSave = true;
                _this.FlagCancel = true;
            }
            else {
                _this.SanctionDetails = data[0];
                var empStatus = data[0].statusId;
            }
            // console.log(this.SanctionDetails);
            if (_this.userRoleID == 5) {
                _this.GetInstEmpDetails(_this.EmpCD);
                if (empStatus == 61) {
                    _this.FlagSave = false;
                    _this.IsDisabled = true;
                    _this.FlageAccept = true;
                    _this.FlagReject = true;
                    _this.FlagForwordChecker = false;
                }
                if (empStatus == 62) {
                    _this.FlagSave = false;
                    _this.FlagCancel = false;
                    _this.IsDisabled = false;
                    _this.FlagForwordChecker = false;
                }
                if (empStatus == 64) {
                    _this.FlagSave = false;
                    _this.IsDisabled = true;
                    _this.FlageAccept = true;
                    _this.FlagReject = true;
                    _this.FlagForwordChecker = false;
                }
            }
            else {
                _this.GetInstEmpDetails(_this.EmpCD);
                if (empStatus == 61) {
                    //this.FlagSave = true;
                    //this.FlagCancel = true;
                    _this.IsDisabled = true;
                    _this.FlagForwordChecker = false;
                }
                if (empStatus == 64) {
                    _this.FlagSave = true;
                    _this.FlagCancel = false;
                    // this.FlagUpdate = true;
                    _this.FlagForwordChecker = true;
                    _this.btnname = 'Update';
                }
            }
        });
    };
    InstRecovryComponent.prototype.GetInstEmpDetails = function (EmpCD) {
        var _this = this;
        this._Service.GetInstEmpDetails(EmpCD).subscribe(function (data) {
            _this.InstEmpDetais = data[0];
            // console.log(this.InstEmpDetais);
        });
    };
    InstRecovryComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    InstRecovryComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    InstRecovryComponent.prototype.Save = function (InstEmpDetais) {
        var _this = this;
        this.EmpCD = sessionStorage.getItem("EmpCd");
        InstEmpDetais.EmpCD = this.EmpCD;
        InstEmpDetais.mode = 1;
        var buttonName = document.activeElement.getAttribute("Name");
        if (this.btnname == 'Save') {
            this._Service.SaveData(this.InstEmpDetais).subscribe(function (data) {
                if (data == "1") {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.saveMsg);
                    _this.GetRecoveryEmpDetails(_this.EmpCD);
                    _this.InstRecoverForm.resetForm();
                }
            });
        }
        else if (this.btnname == 'Update') {
            this._Service.EditLoanDetailsById(this.InstEmpDetais).subscribe(function (data) {
                if (data == "1") {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.updateMsg);
                }
            });
        }
        else {
            if (buttonName == 'ForwardtoDDOChecker') {
                this.ForwardtoDDOChecker(InstEmpDetais);
            }
        }
    };
    InstRecovryComponent.prototype.ForwardtoDDOChecker = function (InstEmpDetais) {
        var _this = this;
        this.EmpCD = sessionStorage.getItem("EmpCd");
        InstEmpDetais.EmpCD = this.EmpCD;
        InstEmpDetais.mode = 2;
        this._Service.ForwardtoDDOChecker(this.InstEmpDetais).subscribe(function (data) {
            if (data == "1") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.forwardCheckerMsg);
            }
            // console.log(this.SanctionDetails);
        });
    };
    InstRecovryComponent.prototype.Accept = function () {
        var _this = this;
        this.EmpCD = sessionStorage.getItem("EmpCd");
        //  EmpSanc.EmpCD = this.EmpCD;
        //EmpSanc.mode = 1;
        this._Service.Accept(this.EmpCD).subscribe(function (data) {
            if (data == "1") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.VerifyedByChecker);
            }
        });
    };
    InstRecovryComponent.prototype.Reject = function () {
        var _this = this;
        this.EmpCD = sessionStorage.getItem("EmpCd");
        // EmpSanc.EmpCD = this.EmpCD;
        //EmpSanc.mode = 2;
        this._Service.Reject(this.EmpCD).subscribe(function (data) {
            if (data == "1") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.RejectedByChecker);
            }
        });
    };
    InstRecovryComponent.prototype.EditLoanDetailsById = function (msEmpLoanMultiInstId, EmpCd) {
        // this.FlagUpdate = true;
        this.btnname = 'Update';
        this.FlagSave = true;
        sessionStorage.setItem('msEmpLoanMultiInstId', msEmpLoanMultiInstId);
        sessionStorage.setItem('EmpCd', EmpCd);
        this.GetInstEmpDetails(EmpCd);
    };
    InstRecovryComponent.prototype.Update = function (InstEmpDetais) {
        var _this = this;
        this.FlagForwordChecker = true;
        InstEmpDetais.MsEmpLoanMultiInstId = sessionStorage.getItem('msEmpLoanMultiInstId');
        InstEmpDetais.EmpCD = sessionStorage.getItem('EmpCd');
        this._Service.EditLoanDetailsById(this.InstEmpDetais).subscribe(function (data) {
            if (data == "1") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.updateMsg);
            }
        });
    };
    InstRecovryComponent.prototype.DeleteLoanDetailsById = function (msEmpLoanMultiInstId, empCD) {
        var _this = this;
        this._Service.DeleteLoanDetailsById(msEmpLoanMultiInstId, empCD).subscribe(function (data) {
            if (data == "1") {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this.commonMsg.deleteMsg);
            }
        });
    };
    //cancel() {
    //  this.InstRecoverForm.resetForm();
    //}
    InstRecovryComponent.prototype.setDeleteId = function (loanId, empCd) {
        this.setDeletIDOnPopup = loanId;
        this.setDeletIDOnPopup2 = empCd;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], InstRecovryComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], InstRecovryComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('InstRecover'),
        __metadata("design:type", Object)
    ], InstRecovryComponent.prototype, "InstRecoverForm", void 0);
    InstRecovryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inst-recovry',
            template: __webpack_require__(/*! ./inst-recovry.component.html */ "./src/app/loanmgt/inst-recovry/inst-recovry.component.html"),
            styles: [__webpack_require__(/*! ./inst-recovry.component.css */ "./src/app/loanmgt/inst-recovry/inst-recovry.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_loan_mgmt_inst_recovery_service__WEBPACK_IMPORTED_MODULE_1__["InstRecoveryService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_5__["CommonMsg"]])
    ], InstRecovryComponent);
    return InstRecovryComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/loanmgt-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/loanmgt/loanmgt-routing.module.ts ***!
  \***************************************************/
/*! exports provided: LoanmgtRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanmgtRoutingModule", function() { return LoanmgtRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _existingloan_existingloan_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./existingloan/existingloan.component */ "./src/app/loanmgt/existingloan/existingloan.component.ts");
/* harmony import */ var _newloanapp_newloanapp_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./newloanapp/newloanapp.component */ "./src/app/loanmgt/newloanapp/newloanapp.component.ts");
/* harmony import */ var _sanction_sanction_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sanction/sanction.component */ "./src/app/loanmgt/sanction/sanction.component.ts");
/* harmony import */ var _loanmgt_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./loanmgt.module */ "./src/app/loanmgt/loanmgt.module.ts");
/* harmony import */ var _floodadv_floodadv_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./floodadv/floodadv.component */ "./src/app/loanmgt/floodadv/floodadv.component.ts");
/* harmony import */ var _inst_recovry_inst_recovry_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./inst-recovry/inst-recovry.component */ "./src/app/loanmgt/inst-recovry/inst-recovry.component.ts");
/* harmony import */ var _recovry_challan_recovry_challan_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./recovry-challan/recovry-challan.component */ "./src/app/loanmgt/recovry-challan/recovry-challan.component.ts");
/* harmony import */ var _recovy_schedule_recovy_schedule_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./recovy-schedule/recovy-schedule.component */ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.ts");
/* harmony import */ var _pay_past_perd_pay_past_perd_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pay-past-perd/pay-past-perd.component */ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.ts");
/* harmony import */ var _chng_loan_type_chng_loan_type_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./chng-loan-type/chng-loan-type.component */ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.ts");
/* harmony import */ var _chng_last_inst_paid_chng_last_inst_paid_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./chng-last-inst-paid/chng-last-inst-paid.component */ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.ts");
/* harmony import */ var _adv_bill_proc_adv_bill_proc_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./adv-bill-proc/adv-bill-proc.component */ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.ts");
/* harmony import */ var _comp_adv_verify_comp_adv_verify_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./comp-adv-verify/comp-adv-verify.component */ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.ts");
/* harmony import */ var _hba_verify_hba_verify_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./hba-verify/hba-verify.component */ "./src/app/loanmgt/hba-verify/hba-verify.component.ts");
/* harmony import */ var _rels_emp_lvl_rels_emp_lvl_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./rels-emp-lvl/rels-emp-lvl.component */ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.ts");
/* harmony import */ var _rels_hoo_lvl_rels_hoo_lvl_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./rels-hoo-lvl/rels-hoo-lvl.component */ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var routes = [
    {
        path: '', component: _loanmgt_module__WEBPACK_IMPORTED_MODULE_5__["LoanmgtModule"], data: {
            breadcrumb: 'Loan Management'
        }, children: [
            {
                path: ' ', component: _loanmgt_module__WEBPACK_IMPORTED_MODULE_5__["LoanmgtModule"], data: {
                    breadcrumb: 'Loan Application'
                }
            },
            {
                path: 'loanapplication', component: _newloanapp_newloanapp_component__WEBPACK_IMPORTED_MODULE_3__["NewloanappComponent"], data: {
                    breadcrumb: 'Loan Application Form'
                }
            },
            {
                path: 'comptadvncverfy', component: _comp_adv_verify_comp_adv_verify_component__WEBPACK_IMPORTED_MODULE_14__["CompAdvVerifyComponent"], data: {
                    breadcrumb: 'Compt Adv Verify'
                }
            },
            {
                path: 'sanction', component: _sanction_sanction_component__WEBPACK_IMPORTED_MODULE_4__["SanctionComponent"], data: {
                    breadcrumb: 'Sanction'
                }
            },
            {
                path: 'releasedetailshodlevel', component: _rels_hoo_lvl_rels_hoo_lvl_component__WEBPACK_IMPORTED_MODULE_17__["RelsHooLvlComponent"], data: {
                    breadcrumb: 'Release Details'
                }
            },
        ]
    },
    { path: 'existingloan', component: _existingloan_existingloan_component__WEBPACK_IMPORTED_MODULE_2__["ExistingloanComponent"] },
    { path: 'floodadv', component: _floodadv_floodadv_component__WEBPACK_IMPORTED_MODULE_6__["FloodadvComponent"] },
    { path: 'instrecovy', component: _inst_recovry_inst_recovry_component__WEBPACK_IMPORTED_MODULE_7__["InstRecovryComponent"] },
    { path: 'recovychallan', component: _recovry_challan_recovry_challan_component__WEBPACK_IMPORTED_MODULE_8__["RecovryChallanComponent"] },
    { path: 'paypastperiod', component: _pay_past_perd_pay_past_perd_component__WEBPACK_IMPORTED_MODULE_10__["PayPastPerdComponent"] },
    { path: 'recovyschedule', component: _recovy_schedule_recovy_schedule_component__WEBPACK_IMPORTED_MODULE_9__["RecovyScheduleComponent"] },
    { path: 'changloantype', component: _chng_loan_type_chng_loan_type_component__WEBPACK_IMPORTED_MODULE_11__["ChngLoanTypeComponent"] },
    { path: 'changelastinstpaid', component: _chng_last_inst_paid_chng_last_inst_paid_component__WEBPACK_IMPORTED_MODULE_12__["ChngLastInstPaidComponent"] },
    { path: 'billprocess', component: _adv_bill_proc_adv_bill_proc_component__WEBPACK_IMPORTED_MODULE_13__["AdvBillProcComponent"] },
    { path: 'hbaverify', component: _hba_verify_hba_verify_component__WEBPACK_IMPORTED_MODULE_15__["HbaVerifyComponent"] },
    { path: 'releasedetailsemplevel', component: _rels_emp_lvl_rels_emp_lvl_component__WEBPACK_IMPORTED_MODULE_16__["RelsEmpLvlComponent"] },
    { path: 'releasedetailshodlevel', component: _rels_hoo_lvl_rels_hoo_lvl_component__WEBPACK_IMPORTED_MODULE_17__["RelsHooLvlComponent"] }
    // { path: 'commanmaster', component: CommanMstComponent }
];
var LoanmgtRoutingModule = /** @class */ (function () {
    function LoanmgtRoutingModule() {
    }
    LoanmgtRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LoanmgtRoutingModule);
    return LoanmgtRoutingModule;
}());



/***/ }),

/***/ "./src/app/loanmgt/loanmgt.module.ts":
/*!*******************************************!*\
  !*** ./src/app/loanmgt/loanmgt.module.ts ***!
  \*******************************************/
/*! exports provided: LoanmgtModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanmgtModule", function() { return LoanmgtModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _loanmgt_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loanmgt-routing.module */ "./src/app/loanmgt/loanmgt-routing.module.ts");
/* harmony import */ var _existingloan_existingloan_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./existingloan/existingloan.component */ "./src/app/loanmgt/existingloan/existingloan.component.ts");
/* harmony import */ var _newloanapp_newloanapp_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./newloanapp/newloanapp.component */ "./src/app/loanmgt/newloanapp/newloanapp.component.ts");
/* harmony import */ var _sanction_sanction_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sanction/sanction.component */ "./src/app/loanmgt/sanction/sanction.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _floodadv_floodadv_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./floodadv/floodadv.component */ "./src/app/loanmgt/floodadv/floodadv.component.ts");
/* harmony import */ var _inst_recovry_inst_recovry_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./inst-recovry/inst-recovry.component */ "./src/app/loanmgt/inst-recovry/inst-recovry.component.ts");
/* harmony import */ var _recovry_challan_recovry_challan_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./recovry-challan/recovry-challan.component */ "./src/app/loanmgt/recovry-challan/recovry-challan.component.ts");
/* harmony import */ var _recovy_schedule_recovy_schedule_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./recovy-schedule/recovy-schedule.component */ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.ts");
/* harmony import */ var _pay_past_perd_pay_past_perd_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pay-past-perd/pay-past-perd.component */ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.ts");
/* harmony import */ var _chng_loan_type_chng_loan_type_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./chng-loan-type/chng-loan-type.component */ "./src/app/loanmgt/chng-loan-type/chng-loan-type.component.ts");
/* harmony import */ var _chng_last_inst_paid_chng_last_inst_paid_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./chng-last-inst-paid/chng-last-inst-paid.component */ "./src/app/loanmgt/chng-last-inst-paid/chng-last-inst-paid.component.ts");
/* harmony import */ var _adv_bill_proc_adv_bill_proc_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./adv-bill-proc/adv-bill-proc.component */ "./src/app/loanmgt/adv-bill-proc/adv-bill-proc.component.ts");
/* harmony import */ var _comp_adv_verify_comp_adv_verify_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./comp-adv-verify/comp-adv-verify.component */ "./src/app/loanmgt/comp-adv-verify/comp-adv-verify.component.ts");
/* harmony import */ var _hba_verify_hba_verify_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./hba-verify/hba-verify.component */ "./src/app/loanmgt/hba-verify/hba-verify.component.ts");
/* harmony import */ var _rels_emp_lvl_rels_emp_lvl_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./rels-emp-lvl/rels-emp-lvl.component */ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.ts");
/* harmony import */ var _rels_hoo_lvl_rels_hoo_lvl_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./rels-hoo-lvl/rels-hoo-lvl.component */ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _shared_module_comman_mst_comman_mst_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../shared-module/comman-mst/comman-mst.component */ "./src/app/shared-module/comman-mst/comman-mst.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























//import { ReleaseComponent } from './release/release.component';


var LoanmgtModule = /** @class */ (function () {
    function LoanmgtModule() {
    }
    LoanmgtModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_existingloan_existingloan_component__WEBPACK_IMPORTED_MODULE_2__["ExistingloanComponent"], _newloanapp_newloanapp_component__WEBPACK_IMPORTED_MODULE_3__["NewloanappComponent"], _sanction_sanction_component__WEBPACK_IMPORTED_MODULE_4__["SanctionComponent"],
                _floodadv_floodadv_component__WEBPACK_IMPORTED_MODULE_7__["FloodadvComponent"], _inst_recovry_inst_recovry_component__WEBPACK_IMPORTED_MODULE_8__["InstRecovryComponent"], _recovry_challan_recovry_challan_component__WEBPACK_IMPORTED_MODULE_9__["RecovryChallanComponent"], _recovy_schedule_recovy_schedule_component__WEBPACK_IMPORTED_MODULE_10__["RecovyScheduleComponent"],
                _pay_past_perd_pay_past_perd_component__WEBPACK_IMPORTED_MODULE_11__["PayPastPerdComponent"], _chng_loan_type_chng_loan_type_component__WEBPACK_IMPORTED_MODULE_12__["ChngLoanTypeComponent"], _chng_last_inst_paid_chng_last_inst_paid_component__WEBPACK_IMPORTED_MODULE_13__["ChngLastInstPaidComponent"], _adv_bill_proc_adv_bill_proc_component__WEBPACK_IMPORTED_MODULE_14__["AdvBillProcComponent"],
                _comp_adv_verify_comp_adv_verify_component__WEBPACK_IMPORTED_MODULE_15__["CompAdvVerifyComponent"], _hba_verify_hba_verify_component__WEBPACK_IMPORTED_MODULE_16__["HbaVerifyComponent"], _rels_emp_lvl_rels_emp_lvl_component__WEBPACK_IMPORTED_MODULE_17__["RelsEmpLvlComponent"], _rels_hoo_lvl_rels_hoo_lvl_component__WEBPACK_IMPORTED_MODULE_18__["RelsHooLvlComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_24__["CommonModule"],
                _loanmgt_routing_module__WEBPACK_IMPORTED_MODULE_1__["LoanmgtRoutingModule"], _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_19__["RouterModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_20__["MatTooltipModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_21__["NgxMatSelectSearchModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_22__["SharedModule"]
            ],
            exports: [_shared_module_comman_mst_comman_mst_component__WEBPACK_IMPORTED_MODULE_23__["CommanMstComponent"]]
        })
    ], LoanmgtModule);
    return LoanmgtModule;
}());



/***/ }),

/***/ "./src/app/loanmgt/newloanapp/newloanapp.component.css":
/*!*************************************************************!*\
  !*** ./src/app/loanmgt/newloanapp/newloanapp.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  /*lebel dropdown */\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n  .upperCase {\r\n  text-transform: uppercase;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbm1ndC9uZXdsb2FuYXBwL25ld2xvYW5hcHAuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7Q0FDeEI7O0VBRUM7SUFDRSxZQUFZO0dBQ2I7O0VBRUg7RUFDRSxrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztDQUNaOztFQUVEO0VBQ0Usb0ZBQW9GO0VBQ3BGLHVCQUF1QjtDQUN4Qjs7RUFFRCxlQUFlOztFQUVmO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UscUJBQXFCO0NBQ3RCOztFQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztFQUVEO0VBQ0Usb0dBQW9HO0NBQ3JHOztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7RUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFHRCxnQkFBZ0I7O0VBQ2hCO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7Q0FDRjs7RUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsV0FBVztHQUNaO0NBQ0Y7O0VBQ0QsYUFBYTs7RUFDYjtFQUNFLG9DQUFvQztDQUNyQzs7RUFFRDtFQUNFLFlBQVk7Q0FDYjs7RUFHRDtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsaUJBQWlCO0NBQ2xCOztFQUVEO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7O0VBQ0QsbUJBQW1COztFQUNuQjtFQUNFLFlBQVk7Q0FDYjs7RUFDRDtFQUNFLDBCQUEwQjtDQUMzQiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvbmV3bG9hbmFwcC9uZXdsb2FuYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIG1hcmdpbjogNHB4XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vYXNzZXRzL2ltZy9leGFtcGxlcy9zaGliYTEuanBnJyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLyogMjktamFuLTE5ICovXHJcblxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXJnaW4tcmJsIHtcclxuICBtYXJnaW46IDAgNTBweCAxMHB4IDBcclxufVxyXG5cclxuLmZpbGQtb25lIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OCB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMXB4IC0xcHggcmdiYSgwLDAsMCwuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwwLDAsLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLDAsMCwuMTIpO1xyXG59XHJcblxyXG4ubWF0LWNhcmQge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U0ZTJlMjtcclxufVxyXG5cclxuLmV4YW1wbGUtY2FyZCB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDE4MXB4O1xyXG59XHJcblxyXG5cclxuLyogUmVzcG9uc2l2ZSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjhweCkgYW5kIChtYXgtd2lkdGggOiAxMDI0cHgpIHtcclxuICAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX29uZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMTAyNXB4KSBhbmQgKG1heC13aWR0aCA6IDEzOTdweCkge1xyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX3R3byB7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gIH1cclxufVxyXG4vKjMxL2phbi8xOSovXHJcbi5tLTIwIHtcclxuICBtYXJnaW46IDAgMjBweCAyMHB4IDIwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIG1pbi13aWR0aDogMzAwcHg7XHJcbn1cclxuXHJcbi5tYXQtdGFibGUge1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIG1heC1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxsLm1hdC1zb3J0LWhlYWRlci1zb3J0ZWQge1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm10LTEwIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi8qbGViZWwgZHJvcGRvd24gKi9cclxuLnNlbGVjdC1sYmwge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi51cHBlckNhc2Uge1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/loanmgt/newloanapp/newloanapp.component.html":
/*!**************************************************************!*\
  !*** ./src/app/loanmgt/newloanapp/newloanapp.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"userRoleID !=17\">\r\n  <app-comman-mst (onchange)=\"GetcommanMethod($event)\"></app-comman-mst>\r\n</div>\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form (ngSubmit)=\"NewLaon.valid && btnsubmit()\" #NewLaon=\"ngForm\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Employee Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empFullName\" placeholder=\"Name\" #empFullName=\"ngModel\" [(ngModel)]=\"_loandetails.empFullName\" disabled='true'>\r\n          <input matInput name=\"ddoid\" #permDdoId=\"ngModel\" [(ngModel)]=\"_loandetails.permDdoId\" disabled='true' style=\"display:none;\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empApptType\" placeholder=\"Post Held\" #empApptType=\"ngModel\" [(ngModel)]=\"_loandetails.empApptType\" disabled='true'>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"pay_basic\" placeholder=\"Present Pay as defined in Rule 4(b) and scale of pay\"\r\n                 [(ngModel)]=\"_loandetails.pay_basic\" #pay_basic=\"ngModel\" disabled='true'>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6 combo-col-2\">\r\n        <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n          <label>Whether governed by Pension Rules</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-5\">\r\n          <mat-radio-group [(ngModel)]=\"_loandetails.empPfType\" name=\"empPfType\" #empPfType=\"ngModel\" disabled='true'>\r\n            <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n            <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"desigDesc\" placeholder=\"Designation\" #desigDesc=\"ngModel\" [(ngModel)]=\"_loandetails.desigDesc\" disabled='true'>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"empSupanDt\" (click)=\"empSupanDt.open()\" [(ngModel)]=\"_loandetails.empSupanDt  \" name=\"empSupanDt\" #empSupanDt=\"ngModel\"\r\n                 placeholder=\"Date of superannuation or retirement or date of expiry of contract in case of a Contract Officer\" disabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"empSupanDt\"></mat-datepicker-toggle>\r\n          <mat-datepicker #empSupanDt></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!empSupanDt.errors?.required\">Date of superannuation or retirement or date of expiry is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Loan Application</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\" (selectionChange)=\"getLoanpurposeSelectedPayLoanCode($event.value)\"\r\n                      [(ngModel)]=\"_loandetails.payLoanRefLoanCD\" name=\"payLoanRefLoanCD\"\r\n                      #payLoanRefLoanCD=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-option label=\"Select Loan Type\" [value]=\"\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of ArrddlLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span [hidden]=\"!payLoanRefLoanCD.errors?.required\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"{{labelmsg}}\" matNativeControl (selectionChange)=\"getvalues($event.value)\" name=\"payLoanPurposeCode\"\r\n                      #payLoanPurposeCode=\"ngModel\" [(ngModel)]=\"_loandetails.payLoanPurposeCode\" [disabled]=disbleflag required>\r\n            <mat-option label=\"this.labelmsg\">{{labelmsg}}</mat-option>\r\n            <mat-option *ngFor=\"let LoanPurpose of ArrLoanPurpose\" [value]=\"LoanPurpose.payLoanPurposeCode\">\r\n              {{LoanPurpose.payLoanPurposeDescription}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span [hidden]=\"!payLoanPurposeCode.errors?.required\">Loan Purpose is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"LoanAmtSanc\" placeholder=\"Loan Amount (Rs.)\" #loanAmtSanc=\"ngModel\" [(ngModel)]=\"_loandetails.loanAmtSanc\"\r\n                 [disabled]=disbleflag pattern=\"^[1-9][0-9]*$\" (change)=\"checkvaluecLoanAmout($event.target.value)\" required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Loan Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"PriInstAmt\" placeholder=\"Installment Amount\" #priInstAmt=\"ngModel\" [(ngModel)]=\"_loandetails.priInstAmt\"\r\n                 pattern=\"^[1-9][0-9]*$\" (change)=\"valuechange($event.target.value)\" [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!priInstAmt.errors?.required\">Installment Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Total No. of Installment\" name=\"IntTotInst\" #intTotInst=\"ngModel\" [(ngModel)]=\"_loandetails.intTotInst \" readonly disabled='true' required>\r\n          <mat-error>\r\n            <span [hidden]=\"!intTotInst.errors?.required\">Total No. of Installment is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"No. of Odd Installment\" name=\"OddInstNoInt\" #oddInstNoInt=\"ngModel\" [(ngModel)]=\"_loandetails.oddInstNoInt \" pattern=\"^[1-9][0-9]*$\" disabled='true' required>\r\n          <mat-error>required</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Odd Installment Amount\" name=\"OddInstAmtInt\" #oddInstAmtInt=\"ngModel\" [(ngModel)]=\"_loandetails.oddInstAmtInt\" pattern=\"^[1-9][0-9]*$\" disabled='true' required>\r\n          <mat-error>\r\n            <span [hidden]=\"!oddInstAmtInt.errors?.required\">Odd Installment Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <!---------------------Property Owner Details---------------------->\r\n      <div *ngIf=\"LoanCode=='21'\">\r\n        <div class=\"fom-title\">Property Owner Details</div>\r\n\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Property Owner Name\" [(ngModel)]=\"_loandetails.ownerName\" title=\"Property Owner Name\" name=\"ownerName\" #ownerName=\"ngModel\"\r\n                   pattern=\"^[a-zA-Z. ]*$\" onKeyPress=\"if(this.value.length==100) return false;\"\r\n                   [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!ownerName.errors?.required\">Property Owner Name  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Description\" title=\"Description\" [(ngModel)]=\"_loandetails.descriptions\" onKeyPress=\"if(this.value.length==200) return false;\" name=\"descriptions\" #descriptions=\"ngModel\" [disabled]=disbleflag\r\n                   pattern=\"^[a-zA-Z0-9,./: ]*$\" required>\r\n            <mat-error>\r\n              <span [hidden]=\"!descriptions.errors?.required\">Description  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Property Owner's PAN No\" title=\"Property Owner's PAN No\" (paste)=\"$event.preventDefault()\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" [(ngModel)]=\"_loandetails.ownerPanNo\" name=\"panno\" maxlength=\"10\" #ownerPanNo=\"ngModel\" [disabled]=disbleflag required class=\"upperCase\">\r\n            <mat-error>\r\n              <span [hidden]=\"!ownerPanNo.errors?.required\">Property Owner's PAN No  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"IFSC Code\" [(ngModel)]=\"_loandetails.bankIFSCCODE\" name=\"bankIFSCCODE\" #bankIFSCCODE=\"ngModel\" (change)=\"getBankDetailsByIFSC(_loandetails.bankIFSCCODE)\" [disabled]=disbleflag required pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\">\r\n            <mat-error>\r\n              <span [hidden]=\"!bankIFSCCODE.errors?.required\">Bank IFSC Code  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Bank Name\" [(ngModel)]=\"objBankDetails.bankName\" name=\"bankName\" #bankName=\"ngModel\" (paste)=\"$event.preventDefault()\" readonly [disabled]=disbleflag pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Branch Name\" [(ngModel)]=\"objBankDetails.branchName\" name=\"branchName\" maxlength=\"50\" readonly #branchName=\"ngModel\" [disabled]=disbleflag pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Saving A/c No\" [(ngModel)]=\"_loandetails.bankAccountNo\" pattern=\"^(?!0)[0-9]{1,}\" min=\"9\" maxlength=\"18\"\r\n                   name=\"bankAccountNo\" #bankAccountNo=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!bankAccountNo.errors?.required\">Saving A/c No is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <!---------------------Purchase of a Plot---------------------->\r\n      <div *ngIf=\"LoanCode=='21'  && PurposeCode == '1'\">\r\n        <div class=\"fom-title\">Purchase of a Plot</div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" pattern=\"^[a-zA-Z0-9,./: ]*$\" onKeyPress=\"if(this.value.length==200) return false;\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6 pading-0\">\r\n            <label> Select Area</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6\">\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.selct_Area\" name=\"selct_Area\" #selct_Area=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Rural</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">Urban</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label> Is it clearly demarcated and developed</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-5\">\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.demar_Dev\" name=\"demar_Dev\" #demar_Dev=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Approximate area (in sq. )\" [(ngModel)]=\"_loandetails.area_Sq_Ft\" pattern=\"[0-9]+(\\.[0-9][0-9]?)?\" type=\"number\" onKeyPress=\"if(this.value.length==20 ) return false;\" title=\"Approximate area (in sq. )\" name=\"area_Sq_Ft\" #area_Sq_Ft=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!area_Sq_Ft.errors?.required\">Approximate area  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost\" title=\"Cost\" [(ngModel)]=\"_loandetails.cost\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" onKeyPress=\"if(this.value.length==20 ) return false;\" type=\"number\" name=\"cost\" #cost=\"ngModel\" [disabled]=disbleflag required>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount actually paid\" title=\"Amount actually paid\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" type=\"number\" onKeyPress=\"if(this.value.length==20 ) return false;\"\r\n                   [(ngModel)]=\"_loandetails.amt_Act_Pay\" name=\"amt_Act_Pay\" #amt_Act_Pay=\"ngModel\" [disabled]=disbleflag (blur)=\"onAmtActuallyPaidValidate()\" required>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"If not purchased when proposed to be acquired\" [(ngModel)]=\"_loandetails.purp_Acquire\"\r\n                   title=\"If not purchased when proposed to be acquired\" name=\"purp_Acquire\" #purp_Acquire=\"ngModel\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" required [disabled]=disbleflag>\r\n            <mat-error>\r\n              <span [hidden]=\"!purp_Acquire.errors?.required\">proposed to be acquired  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Unexpired portion of lease if not freehold\"\r\n                   [(ngModel)]=\"_loandetails.unexp_Port_Lease\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" title=\"Unexpired portion of lease if not freehold\" name=\"unexp_Port_Lease\" #unexp_Port_Lease=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!unexp_Port_Lease.errors?.required\">Unexpired portion of lease if not freehold  is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-5 pading-0\"><label>Terms and Conditions </label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"PurchageFlag = !PurchageFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Construction---------------------->\r\n      <div *ngIf=\"LoanCode=='21'  && PurposeCode == '2'\">\r\n        <div class=\"fom-title\">Construction</div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Floor - wise area to be constructed in (sq.mtrs.)\" onKeyPress=\"if(this.value.length==20) return false;\" type=\"number\" maxlength=\"3\"\r\n                   [(ngModel)]=\"_loandetails.flr_Area\" title=\"Floor - wise area to be constructed in (sq.mtrs.)\" name=\"flr_Area\" #flr_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!flr_Area.errors?.required\">Floor - wise area to be constructed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Estimated Cost\" title=\"Estimated Cost\" onKeyPress=\"if(this.value.length==12 ) return false;\" type=\"number\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" [(ngModel)]=\"_loandetails.est_Cost\" name=\"est_Cost\" #est_Cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!est_Cost.errors?.required\">Estimated Cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Amount of advance required (for land/construction/both)\" pattern=\"^[1-9][0-9]*$\" [(ngModel)]=\"_loandetails.amt_Adv_Req\"\r\n                   title=\"Amount of advance required (for land/construction/both)\" name=\"amt_Adv_Req\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount of advance is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled\r\n                   placeholder=\"No. of instalments for repayment\"\r\n                   [(ngModel)]=\"_loandetails.num_Inst\"\r\n                   title=\"No. of instalments for repayment\" name=\"num_Inst\" #num_Inst=\"ngModel\" pattern=\"([1-9]|[1-8][0-9]|9[0-9]|1[0-4][0-9]|150)\" type=\"number\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">No. of instalments for repayment is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\"\r\n                      title=\"Location with address\" [(ngModel)]=\"_loandetails.address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag pattern=\"^[a-zA-Z0-9,./: ]*$\" onKeyPress=\"if(this.value.length==200) return false;\" required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6 pading-0\">\r\n            <label> Select Area</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-6\">\r\n\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.selct_Area\" name=\"selct_Area\" #selct_Area=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Rural</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">Urban</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label> Is it clearly demarcated and developed</label>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-6 col-lg-5\">\r\n            <mat-radio-group [(ngModel)]=\"_loandetails.demar_Dev\" name=\"demar_Dev\" #demar_Dev=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Approximate area (in sq. )\" pattern=\"[0-9]+(\\.[0-9][0-9]?)?\" type=\"number\"\r\n                   [(ngModel)]=\"_loandetails.area_Sq_Ft\" title=\"Approximate area (in sq. )\" name=\"area_Sq_Ft\" #area_Sq_Ft=\"ngModel\" maxlength=\"30\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!area_Sq_Ft.errors?.required\">Approximate area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost \" title=\"Cost \" [(ngModel)]=\"_loandetails.cost\" pattern=\"^[1-9][0-9]*$\" maxlength=\"20\" name=\"cost\" #cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!cost.errors?.required\">Cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Amount actually paid \" title=\"Amount actually paid \"\r\n                   [(ngModel)]=\"_loandetails.amt_Act_Pay\" name=\"amt_Act_Pay\" #amt_Act_Pay=\"ngModel\" pattern=\"^[1-9][0-9]*$\" (change)=\"validatechange($event.target.value)\"\r\n                   [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Act_Pay.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"If not purchased when proposed to be acquired \"\r\n                   [(ngModel)]=\"_loandetails.purp_Acquire\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\"\r\n                   title=\"If not purchased when proposed to be acquired \" name=\"purp_Acquire\" #purp_Acquire=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!purp_Acquire.errors?.required\">purchased is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Unexpired portion of lease if not freehold \" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\"\r\n                   [(ngModel)]=\"_loandetails.unexp_Port_Lease\"\r\n                   title=\"Unexpired portion of lease if not freehold\" name=\"unexp_Port_Lease\" #unexp_Port_Lease=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!unexp_Port_Lease.errors?.required\">Unexpired portion of lease is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Terms and Conditions </label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"ConstructionFlag = !ConstructionFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Details for enlarging the existing House- (click)=\"ConstructionFlag = !ConstructionFlag\" --------------------->\r\n      <div *ngIf=\"LoanCode=='21'  && PurposeCode == '3'\">\r\n        <div class=\"fom-title\">Details for enlarging the existing House</div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\" pattern=\"^[a-zA-Z0-9,./: ]*$\" onKeyPress=\"if(this.value.length==200) return false;\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area(in sq.mtrs.)\" title=\"Plinth area(in sq.mtrs.)\" type=\"number\" pattern=\"^[1-9][0-9]*$\"\r\n                   [(ngModel)]=\"_loandetails.plint_Area\" name=\"plint_Area\" #plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plint_Area.errors?.required\">Plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area proposed for enlargement(in sq. mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.plth_Prop_Enlarge\" title=\"Plinth area proposed for enlargement(in sq. mtrs.)\"\r\n                   name=\"plth_Prop_Enlarge\" #plth_Prop_Enlarge=\"ngModel\" pattern=\"^[0-9]*(?:\\.[0-9]{0,4})?$\" maxlength=\"12\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plth_Prop_Enlarge.errors?.required\">Plinth area proposed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost of construction/acqisition of existing house\"\r\n                   [(ngModel)]=\"_loandetails.coc\" title=\"Cost of construction/acqisition of existing house\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" name=\"coc\" #coc=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!coc.errors?.required\">Cost of construction is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Cost of proposed enlargement\"\r\n                   [(ngModel)]=\"_loandetails.cop_Enlarge\" title=\"Cost of proposed enlargement\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" name=\"cop_Enlarge\" #cop_Enlarge=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!cop_Enlarge.errors?.required\">Cost of proposed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Total plinth area\" pattern=\"^[0-9]*(?:\\.[0-9]{0,4})?$\"\r\n                   [(ngModel)]=\"_loandetails.tot_Plint_Area\" title=\"Total plinth area\" name=\"tot_Plint_Area\" maxlength=\"12\" #tot_Plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!tot_Plint_Area.errors?.required\">Total plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Total Cost\"\r\n                   [(ngModel)]=\"_loandetails.tot_Cost\" title=\"Total Cost\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" name=\"tot_Cost\" #tot_Cost=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!tot_Cost.errors?.required\">Total cost is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance required \"\r\n                   [(ngModel)]=\"_loandetails.amt_Adv_Req\" title=\"Amount of advance required \" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" name=\"amt_Adv_Req\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount of advance is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"No. of Installments for Repayment\"\r\n                   [(ngModel)]=\"_loandetails.num_Inst\" title=\"No. of Installments for Repayment\" pattern=\"([1-9]|[1-8][0-9]|9[0-9]|1[0-4][0-9]|150)\" name=\"num_Inst\" #num_Inst=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">No. of installments is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Terms and Conditions </label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"EnlargingFlag = !EnlargingFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Details for purchasing a ready-built house/flat (click)=\"EnlargingFlag = !EnlargingFlag\"---------------------->\r\n      <div *ngIf=\"LoanCode=='21'  && PurposeCode == '4'\">\r\n        <div class=\"fom-title\">Details for purchasing a ready-built house/flat</div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <textarea matInput placeholder=\"Location with address\" pattern=\"^[a-zA-Z0-9,./: ]*$\" onKeyPress=\"if(this.value.length==200) return false;\"\r\n                      [(ngModel)]=\"_loandetails.address\" title=\"Location with address\" name=\"address\" #address=\"ngModel\" [disabled]=disbleflag required></textarea>\r\n            <mat-error>\r\n              <span [hidden]=\"!address.errors?.required\">Location with address is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Plinth area (in sq.mtrs.)\"\r\n                   [(ngModel)]=\"_loandetails.plint_Area\" type=\"number\" pattern=\"^[0-9]*(?:\\.[0-9]{0,4})?$\" maxlength=\"12\"\r\n                   name=\"plint_Area\" #plint_Area=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!plint_Area.errors?.required\">Plinth area is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker2\" placeholder=\"When constructed\"\r\n                   #const_Date=\"ngModel\" [(ngModel)]=\"_loandetails.const_Date \" readonly (click)=\"picker2.open()\" name=\"const_Date\" required [disabled]=disbleflag>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker2></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!const_Date.errors?.required\">When constructed is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Price settled\" title=\"Price settled\"\r\n                   [(ngModel)]=\"_loandetails.prc_Settled\" name=\"prc_Settled\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" #prc_Settled=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!prc_Settled.errors?.required\">Price settled is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Agency from whom to be purchased\" [(ngModel)]=\"_loandetails.agency_Whom_Pur\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\"\r\n                   title=\"Agency from whom to be purchased\" name=\"agency_Whom_Pur\" #agency_Whom_Pur=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!agency_Whom_Pur.errors?.required\">Purchased is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount already paid \" title=\"Amount already paid\"\r\n                   [(ngModel)]=\"_loandetails.amt_Paid\" name=\"amt_Paid\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" #amt_Paid=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Paid.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance required\" [(ngModel)]=\"_loandetails.amt_Adv_Req\"\r\n                   title=\"Amount of advance required\" name=\"amt_Adv_Req\" pattern=\"^[0-9]*(?:\\.[0-9]{0,2})?$\" maxlength=\"12\" #amt_Adv_Req=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!amt_Adv_Req.errors?.required\">Amount is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"No. of Installments for Repayment\" [(ngModel)]=\"_loandetails.num_Inst\" pattern=\"([1-9]|[1-8][0-9]|9[0-9]|1[0-4][0-9]|150)\"\r\n                   title=\"No. of Installments for Repayment\" name=\"num_Inst\" #num_Inst=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!num_Inst.errors?.required\">Repayment is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Terms and Conditions </label></div>\r\n          <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"ReadybuiltFlag = !ReadybuiltFlag\" [disabled]=disbleflag></mat-checkbox>\r\n        </div>\r\n      </div>\r\n      <!---------------------Computer Adv--(click)=\"ReadybuiltFlag = !ReadybuiltFlag\" -------------------->\r\n      <div *ngIf=\"LoanCode=='100'\">\r\n        <div class=\"fom-title\">Computer Advance</div>\r\n\r\n        <div class=\"col-md-6 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Whether the intention is to purchase Personal Computer</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.intPurPrsnComp\" name=\"intPurPrsnComp\" #intPurPrsnComp=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">New</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">Old</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Sanction is as per Rule 18 & Rules 1964</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.rule_18_1964_Pur\" name=\"rule_18_1964_Pur\" #rule_18_1964_Pur=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Basic pay\" [(ngModel)]=\"_loandetails.pay_basic\"\r\n                   name=\"pay_basic\" #pay_basic=\"ngModel\" pattern=\"^[1-9][0-9]*$\" disabled='true' required>\r\n            <mat-error>\r\n              <span [hidden]=\"!pay_basic.errors?.required\">Basic pay is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Anticipated price of Personal Computer \" title=\"Anticipated price of Personal Computer\" maxlength=\"10\" onkeypress=\"return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)\"\r\n                   [(ngModel)]=\"_loandetails.anti_Price_Pc\" pattern=\"^[1-9][0-9]*$\" name=\"Anti_Price_Pc\" #anti_Price_Pc=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-error>\r\n              <span [hidden]=\"!anti_Price_Pc.errors?.required\">Anticipated price of Personal Computer is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker\" disabled placeholder=\"Superannuation / Contract Expiry Date (in case of contract officer)\"\r\n                   title=\"Superannuation / Contract Expiry Date (in case of contract officer)\"\r\n                   [(ngModel)]=\"_loandetails.empSupanDt\" (click)=\"picker.open()\" name=\"empSupanDt\" #empSupanDt=\"ngModel\" required disabled='true'>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!empSupanDt.errors?.required\">Contract Expiry Date is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput disabled placeholder=\"Desired No of Installments to be repaid for advance\" title=\"Desired No of Installments to be repaid for advance\"\r\n                   [(ngModel)]=\"_loandetails.desNoInsRePaidForAdv\" name=\"desNoInsRePaidForAdv\" #desNoInsRePaidForAdv=\"ngModel\" [disabled]=disbleflag oninput=\"this.value=Math.abs(this.value)\" type=\"number\" required>\r\n            <mat-error>\r\n              <span [hidden]=\"!desNoInsRePaidForAdv.errors?.required\">Desired No of Installments is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Whether the officer is on leave</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.off_On_Leave\" name=\"off_On_Leave\" #off_On_Leave=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6 combo-col\">\r\n          <div class=\"col-sm-6 col-md-6 col-lg-7 pading-0\">\r\n            <label>Advance taken previously for similar purpose</label>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-5\">\r\n            <mat-radio-group aria-label=\"Select an option\"\r\n                             [(ngModel)]=\"_loandetails.pre_Adv_Simi_Pur\" name=\"pre_Adv_Simi_Pur\" #pre_Adv_Simi_Pur=\"ngModel\" [disabled]=disbleflag>\r\n              <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n              <mat-radio-button [value]=\"false\">No</mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"picker1\" placeholder=\"Date of drawal of the advance\" title=\"Date of drawal of the advance\" readonly\r\n                   [(ngModel)]=\"_loandetails.adv_Draw_Date\" (click)=\"picker1.open()\" [max]=\"_loandetails.empSupanDt\" [min]=\"_loandetails.empJoinDt\"\r\n                   name=\"adv_Draw_Date\" #adv_Draw_Date=\"ngModel\" [disabled]=disbleflag required>\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker1></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!adv_Draw_Date.errors?.required\">Date of drawal is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount of advance / interest outstanding\" maxlength=\"10\" type=\"number\" title=\"Amount of advance / interest outstanding\"\r\n                   (blur)=\"onValidate()\"\r\n                   [(ngModel)]=\"_loandetails.adv_Amount\" name=\"adv_Amount\" #adv_Amount=\"ngModel\" [disabled]=disbleflag oninput=\"this.value=Math.abs(this.value)\" required>\r\n\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"col-md-12 col-lg-2 pading-0\"><label>Terms and Conditions </label></div>\r\n          <div class=\"col-md-12 col-lg-10 pading-0\">\r\n\r\n            <mat-checkbox [(ngModel)]=\"group\" name=\"group\" (click)=\"CompAdvFlag = !CompAdvFlag\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"col-sm-12 col-md-6\"><label>Upload e-sign</label></div>\r\n            <input id=\"idfile\" class=\"form-control\" name=\"file\" [(ngModel)]=\"_loandetails.upload_Sign\" type=\"file\" accept=\"application/pdf\"\r\n                   #file (change)=\"handleFileinput($event.target.files)\"><br />\r\n            <a href={{_loandetails.upload_Sign}} target=\"_blank\" *ngIf=disbledwnloadflag>{{_loandetails.upload_Sign}}</a><br /><br />\r\n            <a class=\"material-icons\" *ngIf=disbledwnloadflag (click)=\"DownloadFile(_loandetails.upload_Sign)\">vertical_align_bottom</a>\r\n            <h6 style=\"color:red\">{{msg}}</h6>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"userRoleID  !=17\">\r\n        <mat-form-field class=\"wid-100\" style=\"color:red;\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_loandetails.loanApp_Remarks\" title=\"Remarks\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" name=\"loanApp_Remarks\" #loanApp_Remarks=\"ngModel\" [disabled]=disbleRemarksflag></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\"></div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" id=\"btn_save\" class=\"btn btn-primary\" *ngIf=\"disableSaveFlage\">\r\n          <i class=\"fa fa-floppy-o\"></i>{{buttonText}}\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"resetLoanDdetailsForm()\" *ngIf=\"disableCancelFlage\">Cancel</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarHooMaker()\" *ngIf=\"disableFwdMakerFlag\">Forward to HOO Maker</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarHooChecker()\" *ngIf=\"disableFwdCheckerFlag\">Forward to HOO Checker</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n  <app-dialog [(visible)]=\"PurchageFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_1\" name=\"pop_Decl_1\" #pop_Decl_1=\"ngModel\" [disabled]=disbleflag>\r\n          </mat-checkbox>\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief.\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_2\" name=\"pop_Decl_2\" #pop_Decl_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.pop_Decl_3\" name=\"pop_Decl_3\" #pop_Decl_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"PurchageFlag=!PurchageFlag\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"EnlargingFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_1\" name=\"const_Dec_1\" #const_Dec_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_2\" name=\"const_Dec_2\" #const_Dec_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.const_Dec_3\" name=\"const_Dec_3\" #const_Dec_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"EnlargingFlag=!EnlargingFlag\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"ConstructionFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_1\" name=\"dtls_Enlarg_1\" #dtls_Enlarg_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best of my knowledge and belief.\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_2\" name=\"dtls_Enlarg_2\" #dtls_Enlarg_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc. and agree to abide by the terms and conditions stipulated therein\r\n\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Enlarg_3\" name=\"dtls_Enlarg_3\" #dtls_Enlarg_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n          </ol>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"ConstructionFlag=!ConstructionFlag\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"ReadybuiltFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_Flat_1\" name=\"dtls_Built_Flat_1\" #dtls_Built_Flat_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I solemnly declare that the information furnished by me in reply to the various items indicated above is true to the best\r\n          of my knowledge and belief\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_flat_2\" name=\"dtls_Built_flat_2\" #dtls_Built_flat_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          I have read the rules regulating the grant of advances to Central Government servants for purchase of land and purchase/construction of buildings,etc.\r\n          and agree to abide by the terms and conditions stipulated therein\r\n        </li>\r\n        <li class=\"dispp-block\">\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.dtls_Built_flat_3\" name=\"dtls_Built_flat_3\" #dtls_Built_flat_3=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          <span>I certify that :</span>\r\n          <ol class=\"lower-roman\">\r\n            <li>\r\n              My  wife/husband is not a Central Government servant/ My wife/husband who is Central Govenment Servant, has not applied for and/or\r\n              Obtained and advance under these rules;\r\n            </li>\r\n            <li>\r\n              Neither I nor my wife/husband/minor child has applied for and/or obtained any loan or advance for\r\n              acquisition of a house in the past from any Government source(e.g. Ministry of Rehabilitaion or under any Central or State Housing Scheme.\r\n            </li>\r\n            <li>\r\n              The Construction of house for which the advance has been applied for,has not yet been commenced.\r\n            </li>\r\n          </ol>\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"ReadybuiltFlag=!ReadybuiltFlag\">Ok</button></div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"CompAdvFlag\">\r\n    <div class=\"declaration-fom\">\r\n      <h3 class=\"access-lbl-with text-center\">Declaration</h3>\r\n      <ul>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.comp_Certi_Info_1\" name=\"comp_Certi_Info_1\" #comp_Certi_Info_1=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n          Certified that the information given above is complete and true.\r\n\r\n        </li>\r\n        <li>\r\n          <mat-checkbox [(ngModel)]=\"_loandetails.comp_Certi_Info_2\" name=\"comp_Certi_Info_2\" #comp_Certi_Info_2=\"ngModel\" [disabled]=disbleflag></mat-checkbox>\r\n\r\n          Certified that i have not taken delivery of the personal computer on account of which i apply for the advance that i shall complete negotiations for the purchase of pay finally and take possession of the same before the expiry of one month from the date of drawal of the advance\r\n        </li>\r\n      </ul>\r\n\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"> <button type=\"button\" class=\"btn btn-warning center\" [disabled]=disbleflag (click)=\"CompAdvFlag=!CompAdvFlag\">Ok</button></div>\r\n  </app-dialog>\r\n</div>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"EMPId\" style=\"display:none;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"display:none;\"> ID </th>\r\n        <td mat-cell *matCellDef=\"let element\" style=\"display:none;\"> {{element.id}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"EMPCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> EMP Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"LoanCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.loanCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PurposeCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Purpose  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.purposeCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a matTooltip=\"{{element.moduleStatus}}\"> {{element.status}}</a>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <a class=\"material-icons i-info\" matTooltip=\"view details\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">error</a>\r\n          <span *ngIf=\"element.statusId == 8 &&  this.userRoleID==17\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == 22 &&  this.userRoleID==8\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == 24 &&  this.userRoleID==9\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == 33 &&  this.userRoleID==17\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == 31 &&  this.userRoleID==8\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == 31 && userRoleID==9\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == 33 && userRoleID==9\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == 25 &&  this.userRoleID==9\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == 8 &&  this.userRoleID==9\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == 8  &&  this.userRoleID==17\">\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"SetDeleteId(element.id);deletePopup = !deletePopup\"> delete_forever </a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasData\">\r\n      <span style=\"color:red\"> No Data Found</span>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n  <app-dialog [(visible)]=\"deletePopup\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n      <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteEmployeeById(setDeletIDOnPopup)\">Delete</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletePopup = !deletePopup\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/newloanapp/newloanapp.component.ts":
/*!************************************************************!*\
  !*** ./src/app/loanmgt/newloanapp/newloanapp.component.ts ***!
  \************************************************************/
/*! exports provided: NewloanappComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewloanappComponent", function() { return NewloanappComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/loan-mgmt/new-loan.service */ "./src/app/services/loan-mgmt/new-loan.service.ts");
/* harmony import */ var _model_LoanModel_new_loan_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/LoanModel/new-loan-model */ "./src/app/model/LoanModel/new-loan-model.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_empdetails_empdetails_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/empdetails/empdetails.service */ "./src/app/services/empdetails/empdetails.service.ts");
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _model_LoanModel_propertyownerdetails__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../model/LoanModel/propertyownerdetails */ "./src/app/model/LoanModel/propertyownerdetails.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};















var NewloanappComponent = /** @class */ (function () {
    function NewloanappComponent(master, empDetails, _Service, _msg, snackBar, objBankService, objCommanService) {
        this.master = master;
        this.empDetails = empDetails;
        this._Service = _Service;
        this._msg = _msg;
        this.snackBar = snackBar;
        this.objBankService = objBankService;
        this.objCommanService = objCommanService;
        this.eventsSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.ifscCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormControl"]();
        this.ifscFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormControl"]();
        this.fileToupload = null;
        this.imageUrl = "/assets/uploadfile/";
        this.PurchageFlag = false;
        this.EnlargingFlag = false;
        this.ConstructionFlag = false;
        this.ReadybuiltFlag = false;
        this.CompAdvFlag = false;
        this.disableflag = false;
        this.disableSaveFlage = true;
        this.disableCancelFlage = true;
        this.disableFwdCheckerFlag = false;
        this.disableFwdMakerFlag = false;
        this.isSubmitted = false;
        this.Message = null;
        this.disbleflag = false;
        this.disbleRemarksflag = false;
        this.disbleflagdownload = false;
        this.disbledwnloadflag = false;
        this.callTypevar = 2;
        this.submitted = false;
        this.remarks = '';
        this.coolingPeriod = false;
        this.myDate = new Date();
        this.labelmsg = 'Select Purpose Type';
        this.displayedColumns = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
        this.selectedIndex = 0;
        this.empstatus = false;
        this.flag = false;
        this.objBankDetails = {};
        this.objAllIFSCCode = {};
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.filteredIfsc = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.urls = [];
        this._loandetails = new _model_LoanModel_new_loan_model__WEBPACK_IMPORTED_MODULE_4__["NewLoanModel"]();
        this._propdetlsmodels = new _model_LoanModel_propertyownerdetails__WEBPACK_IMPORTED_MODULE_11__["PropOwnerDetlsModel"]();
    }
    NewloanappComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.buttonText = 'Save';
        this.empstatus = false;
        this.flag = false;
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.BindLoanStatus(this.UserID);
        this.group = false;
        this.BindLoanType();
        if (this.userRoleID == 17) {
            this.FindEmpCode(this.username);
        }
        else if (this.userRoleID == 9) {
            this.FindEmpCode(this.username);
        }
        this.getAllIFSC();
        this.ifscFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterIfsc();
        });
    };
    NewloanappComponent.prototype.filterIfsc = function () {
        if (!this.Ifsc) {
            return;
        }
        var search = this.ifscFilterCtrl.value;
        if (!search) {
            this.filteredIfsc.next(this.Ifsc.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredIfsc.next(this.Ifsc.filter(function (Ifsc) { return Ifsc.ifscCD.toLowerCase().indexOf(search) > -1; }));
    };
    NewloanappComponent.prototype.onValidate = function () {
        this.tempcost = Number(this._loandetails.adv_Amount);
        this.tempamt_Act_Pay = Number(this._loandetails.loanAmtSanc);
        if (Math.floor(this.tempamt_Act_Pay) < Math.floor(this.tempcost)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.amountLessEqualloanAmountMsg);
            this._loandetails.adv_Amount = null;
        }
    };
    NewloanappComponent.prototype.onAmtActuallyPaidValidate = function () {
        this.tempcost = Number(this._loandetails.cost);
        this.tempamt_Act_Pay = Number(this._loandetails.amt_Act_Pay);
        if (Math.floor(this.tempcost) < Math.floor(this.tempamt_Act_Pay)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.amountActuallyPaidMsg);
            this._loandetails.amt_Act_Pay = null;
        }
    };
    NewloanappComponent.prototype.GetcommanMethod = function (value) {
        this.getLoanDetails(value);
    };
    NewloanappComponent.prototype.BindLoanType = function () {
        var _this = this;
        this.loantypeFlag = '2'; //new loan type dropdownlist
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this.ArrddlLoanType = data;
        });
    };
    NewloanappComponent.prototype.FindEmpCode = function (username) {
        var _this = this;
        debugger;
        this.objCommanService.GetEmpCode(username).subscribe(function (data) {
            _this.ArrEmpCode = data[0];
            // uncomment to validate for the loan requet before 5 year 
            //if (this.ArrEmpCode.serviceTime < '5') {
            // swal("At least 5Yrs. continuous in govt service for apply loan");    
            //}
            //if (this.ArrEmpCode.serviceType != 'R') {
            //  swal("only Permanent employee apply for the loan");
            //}   
            if (_this.ArrEmpCode.empcode != null) {
                _this.getLoanDetails(_this.ArrEmpCode.empcode);
            }
        });
    };
    NewloanappComponent.prototype.validatechange = function () {
        this.tempamtActPay = Number(this._loandetails.amt_Act_Pay);
        this.tempcost = Number(this._loandetails.cost);
        if (Math.floor(this.tempcost) < this.tempamtActPay) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.costGreateThenAmountActuallyPaid);
        }
    };
    NewloanappComponent.prototype.validatechangeInstallmentsRepayment = function () {
        this.tempNum_Inst = Number(this._loandetails.num_Inst);
        if (Math.floor(this.tempNum_Inst) >= 151) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.noInstallmentsRepaymentAmountshouldbelessthanorequal150);
            this._loandetails.num_Inst = null;
        }
    };
    NewloanappComponent.prototype.valuechange = function (value) {
        this.temppriInstAmt = Number(this._loandetails.priInstAmt);
        this.temploanAmtSanc = Number(this._loandetails.loanAmtSanc);
        this._loandetails.intTotInst = Math.floor(((this._loandetails.loanAmtSanc) / value));
        this.temp = Math.floor((this._loandetails.priInstAmt) * this._loandetails.intTotInst);
        this._loandetails.oddInstAmtInt = Math.floor(((this._loandetails.loanAmtSanc) - this.temp));
        if (Math.floor(this.temppriInstAmt) == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.enterCorrectValueMsg);
            this._loandetails.priInstAmt = null;
        }
        if (this._loandetails.oddInstAmtInt == 0) {
            this._loandetails.oddInstNoInt = 0;
        }
        else {
            this._loandetails.oddInstNoInt = 1;
        }
        if (Math.floor(this.temploanAmtSanc) < Math.floor(this.temppriInstAmt)) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.loanAmountGreaterthenInstallMsg);
            this._loandetails.priInstAmt = null;
        }
    };
    NewloanappComponent.prototype.checkvaluecLoanAmout = function (value) {
        this.tempLoanAmtSanc = Number(value);
        this.loanamt = Math.floor(Number(this._loandetails.pay_basic));
        if (Math.floor(this.loanamt * 34) >= 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maximumLimitLoanAmountMsg);
        }
        if (this.tempLoanAmtSanc >= 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maximumLimitLoanAmountMsg);
        }
        if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maxLimitLoanAmountMsg);
            this._loandetails.loanAmtSanc = null;
        }
        if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maxLimitLoanAmountMsg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Math.floor(this._loandetails.loanAmtSanc) > 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maximumLimitLoanAmountMsg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Number(this._Service.PurposeCode) == 3 && Math.floor(this._loandetails.loanAmtSanc) > 180000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maxLimitLoanAmount1LK80Msg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Number(this._Service.PurposeCode) == 2 && Math.floor(this._loandetails.loanAmtSanc) > 250000) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.maxLimitLoanAmount2LK50Msg);
            this._loandetails.loanAmtSanc = null;
        }
        if (Math.floor(this.tempLoanAmtSanc) == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.enterCorrectValueMsg);
            this._loandetails.loanAmtSanc = null;
        }
    };
    NewloanappComponent.prototype.getLoanDetails = function (value) {
        this.empid = value;
        this.designid = "0";
        this.getEmployeeDeatils(this.empid, this.designid);
    };
    NewloanappComponent.prototype.getEmployeeDeatils = function (EmpCode, MsDesignID) {
        var _this = this;
        this._Service.EmployeeInfo(EmpCode, MsDesignID).subscribe(function (data) {
            _this._loandetails = data[0];
            _this._loandetails.payLoanRefLoanCD = '';
            _this._loandetails.payLoanPurposeCode = '';
            if (_this._loandetails == undefined) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.noRecordMsg);
                _this._loandetails = new _model_LoanModel_new_loan_model__WEBPACK_IMPORTED_MODULE_4__["NewLoanModel"]();
            }
            else {
                _this.PurposeCode = String(_this._loandetails.payLoanPurposeID);
                _this.LoanCode = String(_this._loandetails.payLoanRefLoanCD);
                _this.getLoanpurposeSelectedPayLoanCode(_this._loandetails.payLoanRefLoanCD);
            }
        });
    };
    NewloanappComponent.prototype.getLoantypeandpurposestatus = function (LoanBillID) {
        var _this = this;
        this._Service.Loantypeandpurposestatus(LoanBillID).subscribe(function (data) {
            _this.Loantypeandpurposes = data;
        });
    };
    NewloanappComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
            if (this.dataSource.filteredData.length > 0) {
                this.isTableHasData = true;
            }
            else {
                this.isTableHasData = false;
            }
        }
    };
    NewloanappComponent.prototype.btnsubmit = function () {
        var _this = this;
        var pop_Decl_1 = this._loandetails.pop_Decl_1;
        var pop_Decl_2 = this._loandetails.pop_Decl_2;
        var pop_Decl_3 = this._loandetails.pop_Decl_3;
        var const_Dec_1 = this._loandetails.const_Dec_1;
        var const_Dec_2 = this._loandetails.const_Dec_2;
        var const_Dec_3 = this._loandetails.const_Dec_3;
        var dtls_Enlarg_1 = this._loandetails.dtls_Enlarg_1;
        var dtls_Enlarg_2 = this._loandetails.dtls_Enlarg_2;
        var dtls_Enlarg_3 = this._loandetails.dtls_Enlarg_3;
        var dtls_Built_Flat_1 = this._loandetails.dtls_Built_Flat_1;
        var dtls_Built_Flat_2 = this._loandetails.dtls_Built_flat_2;
        var dtls_Built_Flat_3 = this._loandetails.dtls_Built_flat_3;
        var comp_Certi_Info_1 = this._loandetails.comp_Certi_Info_1;
        var comp_Certi_Info_2 = this._loandetails.comp_Certi_Info_2;
        this._loandetails.createdBy = this.username;
        this._loandetails.bankName = this.objBankDetails.bankName;
        this._loandetails.branchName = this.objBankDetails.branchName;
        this._loandetails.adv_Draw_Date = new Date();
        if (this.userRoleID == 9) {
            this._loandetails.priVerifFlag = 33;
        }
        else {
            this._loandetails.priVerifFlag = 8;
        }
        if (this.LoanCode == '100' && this._loandetails.upload_Sign != "0") {
            this._loandetails.upload_Sign = this.fileToupload.name;
        }
        if (pop_Decl_1 == true && pop_Decl_2 == true && pop_Decl_3 == true || const_Dec_1 == true && const_Dec_2 == true && const_Dec_3 == true ||
            dtls_Enlarg_1 == true && dtls_Enlarg_2 == true && dtls_Enlarg_3 == true || dtls_Built_Flat_1 == true && dtls_Built_Flat_2 == true && dtls_Built_Flat_3 == true
            || comp_Certi_Info_1 == true && comp_Certi_Info_2) {
            if (this.buttonText == 'Save') {
                this.ValidateCoolingPeriod(this._loandetails.payLoanRefLoanCD, this._loandetails.payLoanPurposeCode, this._loandetails.empCd);
                if (this.coolingPeriod == true) {
                    this._Service.EmployeeSaveInfo(this._loandetails).subscribe(function (data) {
                        _this._loandetails = data[0];
                        _this._Service.HiddenId = data[0];
                        _this.uploadfile();
                        if (data == "-1") {
                            _this.BindLoanStatus(_this._Service.DdoId);
                            // this.snackBar.open(this._msg.alreadyExistMsg, null, { duration: 5000 });
                            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.alreadyExistMsg);
                            _this.commanfunction();
                            _this.form.resetForm();
                        }
                        else {
                            _this.BindLoanStatus(_this._Service.DdoId);
                            _this.snackBar.open(_this._msg.saveMsg, null, { duration: 5000 });
                            _this.commanfunction();
                            _this.form.resetForm();
                            _this.disableSaveFlage = false;
                            _this.disableFwdCheckerFlag = false;
                            _this.form.resetForm();
                        }
                    });
                }
                else {
                    alert(this._msg.coolingPeriodMsg);
                }
            }
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(this._msg.checkDeclarationMsg);
        }
        if (pop_Decl_1 == true && pop_Decl_2 == true && pop_Decl_3 == true || const_Dec_1 == true && const_Dec_2 == true && const_Dec_3 == true ||
            dtls_Enlarg_1 == true && dtls_Enlarg_2 == true && dtls_Enlarg_3 == true || dtls_Built_Flat_1 == true && dtls_Built_Flat_2 == true && dtls_Built_Flat_3 == true
            || comp_Certi_Info_1 == true && comp_Certi_Info_2) {
            if (this.buttonText == 'Update') {
                if (this.userRoleID == 9) {
                    this.disableFwdCheckerFlag = true;
                    this._loandetails.priVerifFlag = 33;
                }
                else {
                    this.disableFwdCheckerFlag = false;
                    this.disableFwdMakerFlag = true;
                    this._loandetails.priVerifFlag = 8;
                }
                this._loandetails.msEmpId = this.tempId;
                this._Service.EmployeeUpdateInfo(this._loandetails).subscribe(function (data) {
                    _this._loandetails = data[0];
                    _this.uploadfile();
                    _this.snackBar.open(_this._msg.updateMsg, null, { duration: 5000 });
                    _this.form.resetForm();
                    _this.disableSaveFlage = true;
                    _this.disableFwdCheckerFlag = false;
                    _this.BindLoanStatus(_this._Service.DdoId);
                    _this.commanfunction();
                });
            }
        }
        else {
            this.snackBar.open(this._msg.checkDeclarationMsg, null, { duration: 5000 });
        }
    };
    NewloanappComponent.prototype.uploadfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var formData, _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        formData = new FormData();
                        formData.append(this.fileToupload.name, this.fileToupload);
                        _a = this;
                        return [4 /*yield*/, this.master.uploadFile(formData).subscribe(function (res) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 1:
                        _a.asyncResult = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NewloanappComponent.prototype.LoanDdetailsForm = function () {
        this.objBankDetails.bankName = '';
        this.objBankDetails.branchName = '';
        this._loandetails.loanAmtSanc = null;
        this._loandetails.priInstAmt = null;
        this._loandetails.intTotInst = null;
        this._loandetails.oddInstNoInt = null;
        this._loandetails.oddInstAmtInt = null;
        this._loandetails.ownerName = '';
        this._loandetails.descriptions = '';
        this._loandetails.ownerPanNo = '';
        this._loandetails.bankIFSCCODE = '';
        this.objBankDetails.bankName = '';
        this.objBankDetails.branchName = '';
        this._loandetails.bankAccountNo = '';
        this.PurposeCode = undefined;
        this._loandetails.address = '';
        this._loandetails.area_Sq_Ft = null;
        this._loandetails.cost = null;
        this._loandetails.amt_Act_Pay = null;
        this._loandetails.purp_Acquire = null;
        this._loandetails.unexp_Port_Lease = '';
        this._loandetails.flr_Area = null;
        this._loandetails.amt_Adv_Req = null;
        this._loandetails.num_Inst = null;
        this._loandetails.address = null;
        this._loandetails.area_Sq_Ft = null;
        this._loandetails.cost = null;
        this._loandetails.amt_Act_Pay = null;
        this._loandetails.purp_Acquire = null;
        this._loandetails.unexp_Port_Lease = null;
        this._loandetails.address = '';
        this._loandetails.plint_Area = null;
        this._loandetails.plth_Prop_Enlarge = null;
        this._loandetails.coc = null;
        this._loandetails.cop_Enlarge = null;
        this._loandetails.tot_Plint_Area = null;
        this._loandetails.tot_Cost = null;
        this._loandetails.address = null;
        this._loandetails.plint_Area = null;
        this._loandetails.prc_Settled = null;
        this._loandetails.amt_Paid = null;
        this._loandetails.anti_Price_Pc = null;
        this._loandetails.desNoInsRePaidForAdv = null;
        this._loandetails.adv_Amount = null;
        this._loandetails.adv_Draw_Date = null;
        this._loandetails.const_Date = null;
        this._loandetails.payLoanRefLoanCD = 0;
        this._loandetails.payLoanPurposeCode = 0;
    };
    NewloanappComponent.prototype.getLoanpurposeSelectedPayLoanCode = function (value) {
        var _this = this;
        debugger;
        //using this table name [ODS].[tblMsPayLoanRef] 
        if (value == "21") {
            this.labelmsg = 'Select Purpose Type';
        }
        if (value == "100") {
            this.labelmsg = 'Select No. of Computer Loan Sanctioned';
        }
        this._Service.LoanCode = value;
        this.TempLoanCode = value;
        this.LoanCode = this.TempLoanCode;
        this._Service.EMPLoanPurposeByLoanCode(value, this.ArrEmpCode.empcode).subscribe(function (data) {
            _this.ArrLoanPurpose = data;
        });
    };
    NewloanappComponent.prototype.getvalues = function (value) {
        this._Service.PurposeCode = value;
        this.LoanCode = this.TempLoanCode;
        this.PurposeCode = value;
    };
    NewloanappComponent.prototype.BindLoanStatus = function (value) {
        var _this = this;
        debugger;
        this.mode = 1; //new loan select data in the table
        this._Service.DdoId = value;
        this._Service.BindLoanStatus(this.mode, value).subscribe(function (data) {
            _this.ArrddlLoanStatus = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.dataSource.data.length > 0) {
                _this.isTableHasData = true;
            }
            else {
                _this.isTableHasData = false;
            }
        });
    };
    NewloanappComponent.prototype.ViewEditLoanDetailsById = function (id, flag) {
        var _this = this;
        this._Service.EditLoanDetailsByEMPID(id).subscribe(function (data) {
            _this._loandetails = data[0];
            _this.tempId = id;
            if (flag == 0) {
                _this.buttonText = "Update";
            }
            if (_this._loandetails.priVerifFlag == 31) {
                _this.disableSaveFlage = true;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._loandetails.priVerifFlag == 33) {
                _this.disableSaveFlage = true;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._loandetails.priVerifFlag == 22) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._loandetails.priVerifFlag == 8) {
                _this.disableSaveFlage = true;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._loandetails.priVerifFlag == 24) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._loandetails.priVerifFlag == 25) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            _this.getLoanpurposeSelectedPayLoanCode(_this._loandetails.payLoanRefLoanCD);
            _this.getvalues(_this._loandetails.payLoanPurposeCode);
            if (_this._Service.PurposeCode == '4') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '1') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '2') {
                _this.group = true;
            }
            if (_this._Service.PurposeCode == '3') {
                _this.group = true;
            }
            if (_this._Service.LoanCode == '100') {
                if (_this._loandetails.comp_Certi_Info_1 == true) {
                    _this.group = true;
                }
            }
            if (flag === 1) {
                _this.disbleflag = true;
                _this.disbledwnloadflag = false;
                _this.disableSaveFlage = false;
                _this.disableCancelFlage = false;
                if (_this.userRoleID == 9) {
                    _this.disableFwdCheckerFlag = false;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = false;
                }
            }
            else {
                _this.disbledwnloadflag = true;
                _this.disbleflag = false;
                _this.disableCancelFlage = false;
            }
            if (_this._loandetails.bankIFSCCODE != "" && _this._loandetails.bankIFSCCODE != undefined) {
                _this.objBankService.getBankDetailsByIFSC(_this._loandetails.bankIFSCCODE).subscribe(function (res) {
                    _this.objBankDetails = res;
                });
            }
            else {
                _this.objBankDetails.branchName = '';
                _this.objBankDetails.bankName = '';
                _this._loandetails.bankIFSCCODE = '';
            }
        });
    };
    NewloanappComponent.prototype.commanfunction = function () {
        this.LoanDdetailsForm();
        if (this._Service.PurposeCode == '4') {
            this.ReadybuiltFlag = false;
        }
        if (this._Service.PurposeCode == '1') {
            this.PurchageFlag = false;
        }
        if (this._Service.PurposeCode == '2') {
            this.ConstructionFlag = false;
        }
        if (this._Service.PurposeCode == '3') {
            this.EnlargingFlag = false;
        }
        if (this._Service.LoanCode == '100') {
            this.CompAdvFlag = false;
            if (this._loandetails.comp_Certi_Info_1 == true) {
                this.group = true;
            }
        }
    };
    NewloanappComponent.prototype.forwarHooMaker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._loandetails.priVerifFlag = 24;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                //this.snackBar.open(this._msg.forwardHooMakerMsg, null, { duration: 5000 });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardHooMakerMsg);
                _this.disableSaveFlage = false;
                _this.disableFwdMakerFlag = false;
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.commanfunction();
            });
        }
    };
    NewloanappComponent.prototype.forwarHooChecker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._loandetails.priVerifFlag = 22;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                // this.snackBar.open(this._msg.forwardHooCheckerMsg, null, { duration: 5000 });
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.forwardHooCheckerMsg);
                _this.disableSaveFlage = false;
                _this.disableFwdCheckerFlag = false;
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.commanfunction();
            });
        }
    };
    NewloanappComponent.prototype.SetDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    NewloanappComponent.prototype.deleteEmployeeById = function (id) {
        var _this = this;
        this.mode = 1;
        this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(function (data) {
            _this.Message = data;
            if (_this.Message != null) {
                _this.deletePopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this._msg.deleteMsg);
                _this.BindLoanStatus(_this._Service.DdoId);
                _this.LoanDdetailsForm();
            }
        });
    };
    NewloanappComponent.prototype.handleFileinput = function (file) {
        var _this = this;
        this.fileToupload = file.item(0);
        if (file.length != 0 && this.LoanCode == '100') {
            if (file[0].type != this._msg.apppdffileMsg) {
                this.msg = this._msg.pdffileMsg;
                return false;
            }
        }
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToupload);
    };
    NewloanappComponent.prototype.onSelectFile = function (event) {
        debugger;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    NewloanappComponent.prototype.DownloadFile = function (url) {
        var _this = this;
        this.empDetails.getdownloadDetails(url).subscribe(function (res) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_6__["saveAs"])(new Blob([res], { type: _this._msg.formatfileMsg }), "");
        });
    };
    NewloanappComponent.prototype.getAllIFSC = function () {
        var _this = this;
        this.objBankService.getAllIFSC().subscribe(function (res) {
            _this.objAllIFSCCode = res;
            _this.Ifsc = res;
            _this.ifscCtrl.setValue(_this.Ifsc);
            _this.filteredIfsc.next(_this.Ifsc);
        });
    };
    NewloanappComponent.prototype.getBankDetailsByIFSC = function (bankIFSCCODE) {
        var _this = this;
        this.objBankService.getBankDetailsByIFSC(bankIFSCCODE).subscribe(function (res) {
            _this.objBankDetails.bankId = '';
            _this.objBankDetails.branchId = '';
            _this.objBankDetails = res;
        });
    };
    NewloanappComponent.prototype.resetLoanDdetailsForm = function () {
        this.LoanDdetailsForm();
        this.eventsSubject.next(this.data);
    };
    NewloanappComponent.prototype.ValidateCoolingPeriod = function (LoanCode, PurposeCode, EmpCd) {
        var _this = this;
        this._Service.ValidateCoolingPeriod(LoanCode, PurposeCode, EmpCd).subscribe(function (res) {
            _this.coolingPeriod = res;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('NewLaon'),
        __metadata("design:type", Object)
    ], NewloanappComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], NewloanappComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], NewloanappComponent.prototype, "sort", void 0);
    NewloanappComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-newloanapp',
            template: __webpack_require__(/*! ./newloanapp.component.html */ "./src/app/loanmgt/newloanapp/newloanapp.component.html"),
            styles: [__webpack_require__(/*! ./newloanapp.component.css */ "./src/app/loanmgt/newloanapp/newloanapp.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_12__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_7__["MasterService"], _services_empdetails_empdetails_service__WEBPACK_IMPORTED_MODULE_8__["EmpdetailsService"], _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_3__["NewLoanService"],
            _global_common_msg__WEBPACK_IMPORTED_MODULE_12__["CommonMsg"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_9__["BankDetailsService"], _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_10__["CommanService"]])
    ], NewloanappComponent);
    return NewloanappComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/loanmgt/pay-past-perd/pay-past-perd.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvcGF5LXBhc3QtcGVyZC9wYXktcGFzdC1wZXJkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.html":
/*!********************************************************************!*\
  !*** ./src/app/loanmgt/pay-past-perd/pay-past-perd.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  pay-past-perd works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/pay-past-perd/pay-past-perd.component.ts ***!
  \******************************************************************/
/*! exports provided: PayPastPerdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayPastPerdComponent", function() { return PayPastPerdComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PayPastPerdComponent = /** @class */ (function () {
    function PayPastPerdComponent() {
    }
    PayPastPerdComponent.prototype.ngOnInit = function () {
    };
    PayPastPerdComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pay-past-perd',
            template: __webpack_require__(/*! ./pay-past-perd.component.html */ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.html"),
            styles: [__webpack_require__(/*! ./pay-past-perd.component.css */ "./src/app/loanmgt/pay-past-perd/pay-past-perd.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PayPastPerdComponent);
    return PayPastPerdComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/recovry-challan/recovry-challan.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/loanmgt/recovry-challan/recovry-challan.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvcmVjb3ZyeS1jaGFsbGFuL3JlY292cnktY2hhbGxhbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/loanmgt/recovry-challan/recovry-challan.component.html":
/*!************************************************************************!*\
  !*** ./src/app/loanmgt/recovry-challan/recovry-challan.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--------------- Left Form --------------->\r\n<div *ngIf=\"userRoleID !=17\">\r\n  <app-comman-mst (onchange)=\"GetcommanMethod($event)\"></app-comman-mst>\r\n</div>\r\n<div >\r\n  <form (ngSubmit)=\"formDirective.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" >\r\n    <mat-accordion class=\"col-md-12\" >\r\n      <mat-expansion-panel [expanded]=\"panelOpenState\" [ngClass]=\"bgcolor\">\r\n        <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n          <mat-panel-title class=\"acordion-heading\">\r\n            Recovery of Challan Details\r\n          </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Loan Type\" formControlName=\"payLoanRefLoanCD\">\r\n              <mat-option value=\"\">--Select--</mat-option>\r\n              <mat-option *ngFor=\"let loantypename of _loanType\" [value]=\"loantypename.payLoanRefLoanCD\">\r\n                {{loantypename.payLoanRefLoanShortDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Amount Disbursed\" formControlName=\"amountDisbursed\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Installment Amount \" formControlName=\"installmentAmount\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Total No. of Installments\" formControlName=\"totalNoInstalments\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Odd Installment No\" formControlName=\"oddInstlNo\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Odd Installment Amount\" formControlName=\"oddInstlAmt\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Outstanding Amount\" formControlName=\"outstandingAmount\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Last Instalment No. Recovered\" formControlName=\"lastInstalmentNoRecovered\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 col-md-4 col-lg-4\" style=\"display:none;\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput formControlName=\"createdBy\">\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"fom-title mt-40\">Part of Loan Amount deposited through Challan</div>\r\n        <div class=\"col-sm-12 col-md-12 mt-10 mb-30\">\r\n          <div class=\"col-md-4 pading-0\"><label class=\"radio-btn-lbl\">Payment made through</label></div>\r\n          <mat-radio-group aria-label=\"Select an option\" class=\"multi-radio-btn\"\r\n                           formControlName=\"paymentmadethrough\" (change)=\"modeChange($event.value)\">\r\n            <mat-radio-button [value]=\"1\" checked>PFMS</mat-radio-button>\r\n            <mat-radio-button [value]=\"2\">NTRP</mat-radio-button>\r\n            <mat-radio-button [value]=\"3\">Provisional</mat-radio-button>\r\n            <mat-radio-button [value]=\"4\">Transfer Entry</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n        <div *ngIf=\"showAndHideMode == 1\">\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Challan No\" formControlName=\"PFMSchallanNo\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">102345678912</mat-option>\r\n                <mat-option value=\"2\">192345678922</mat-option>\r\n                <mat-option value=\"3\">172345678932</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4\">\r\n            <mat-form-field class=\"w-100\">\r\n              <input matInput name=\"PFMSchallanAmt\" placeholder=\" Challan Amount \" formControlName=\"PFMSchallanAmt\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"picker2\" placeholder=\"Challan Date\" formControlName=\"PFMSchallanDate\" [value]=\"currentDate\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker2></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Payment Mode\" formControlName=\"PFMSpaymentMode\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">Cash</mat-option>\r\n                <mat-option value=\"2\">Cheque</mat-option>\r\n                <mat-option value=\"3\">DD</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Cheque No./DD No\" formControlName=\"chequeNoDDNo\" type=\"number\" onKeyPress=\"if(this.value.length==6 ) return false;\">\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Outstanding Amount\" formControlName=\"PFMSupdatedOutstandingAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Last Instalment No. Recovered\" formControlName=\"PFMSupdatedLastInstalmentNoRecovered\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div *ngIf=\"showAndHideMode == 2\">\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\" NTRP Transaction Id\" formControlName=\"NTRPTransactionId\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">102345678912</mat-option>\r\n                <mat-option value=\"2\">192345678922</mat-option>\r\n                <mat-option value=\"3\">172345678932</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4\">\r\n            <mat-form-field class=\"w-100\">\r\n              <input matInput placeholder=\" Challan Amount \" formControlName=\"NTRPchallanAmt\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"picker2\" placeholder=\"Challan Date\" formControlName=\"NTRPchallanDate\" [value]=\"currentDate\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker2></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Payment Mode\" formControlName=\"NTRPpaymentMode\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">Cash</mat-option>\r\n                <mat-option value=\"2\">Cheque</mat-option>\r\n                <mat-option value=\"3\">DD</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"NTRP Receipt Details\" formControlName=\"NTRPReceiptDetails\">\r\n            </mat-form-field>\r\n\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Outstanding Amount\" formControlName=\"NTRPupdatedOutstandingAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Last Instalment No. Recovered\" formControlName=\"NTRPupdatedLastInstalmentNoRecovered\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n        </div>\r\n\r\n\r\n        <div *ngIf=\"showAndHideMode == 3\">\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Challan No\" formControlName=\"ProchallanNo\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">102345678912</mat-option>\r\n                <mat-option value=\"2\">192345678922</mat-option>\r\n                <mat-option value=\"3\">172345678932</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4\">\r\n            <mat-form-field class=\"w-100\">\r\n              <input matInput placeholder=\" Challan Amount \" formControlName=\"ProchallanAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"picker2\" placeholder=\"Challan Date\" formControlName=\"ProchallanDate\" [value]=\"currentDate\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker2></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Payment Mode\" formControlName=\"PropaymentMode\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">Cash</mat-option>\r\n                <mat-option value=\"2\">Cheque</mat-option>\r\n                <mat-option value=\"3\">DD</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Remarks\" formControlName=\"Proremarks\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Outstanding Amount\" formControlName=\"ProupdatedOutstandingAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Last Instalment No. Recovered\" formControlName=\"ProupdatedLastInstalmentNoRecovered\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div *ngIf=\"showAndHideMode == 4\">\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"TE No\" formControlName=\"TENo\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">102345678912</mat-option>\r\n                <mat-option value=\"2\">192345678922</mat-option>\r\n                <mat-option value=\"3\">172345678932</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4\">\r\n            <mat-form-field class=\"w-100\" >\r\n              <input matInput placeholder=\" TE Amount \"  formControlName=\"TEAmounts\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"picker2\" placeholder=\"TE Date\" formControlName=\"TEDate\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker2></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Payment Mode\" formControlName=\"TEpaymentMode\">\r\n                <mat-option value=\"0\">Select</mat-option>\r\n                <mat-option value=\"1\">Cash</mat-option>\r\n                <mat-option value=\"2\">Cheque</mat-option>\r\n                <mat-option value=\"3\">DD</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"remarks\" formControlName=\"TEremarks\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Outstanding Amount\" formControlName=\"TEupdatedOutstandingAmount\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-4 col-lg-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Updated Last Instalment No. Recovered\" formControlName=\"TEupdatedLastInstalmentNoRecovered\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n        </div>\r\n\r\n\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\">{{btnUpdateText}}</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\">Cancel</button>\r\n          <button class=\"btn btn-primary\" type=\"button\">Forward to HOO Checker</button>\r\n        </div>\r\n      </mat-expansion-panel>\r\n    </mat-accordion>\r\n\r\n  </form>\r\n\r\n</div>\r\n\r\n\r\n\r\n  <div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n    <div class=\"example-card mat-card\">\r\n      <div class=\"fom-title\">Select In-Process Loan Details</div>\r\n      <!-- Search -->\r\n      <mat-form-field>\r\n        <input matInput placeholder=\"Search\" (keyup)=\"applyFilter($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <table mat-table [dataSource]=\"RecoveryChallanData\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <ng-container matColumnDef=\"LoanType\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.loanType}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"AmountDisbursed\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Amount Disbursed </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.amountDisbursed}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"CurrentRecoveryof\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Current Recovery of </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.currentRecoveryof}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"TotalNoInstalments\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Total No.Instalments </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.totalNoInstalments}} </td>\r\n        </ng-container>\r\n        <!--<ng-container matColumnDef=\"Paymentmadethrough\" style=\"display:none;\">\r\n    <th mat-header-cell *matHeaderCellDef mat-sort-header> Payment made through </th>\r\n    <td mat-cell *matCellDef=\"let element\"> {{element.Paymentmadethrough}} </td>\r\n  </ng-container>-->\r\n        <ng-container matColumnDef=\"status\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Status </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"createdBy\" style=\"display:none;\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header  style=\"display:none;\">createdBy </th>\r\n          <td mat-cell *matCellDef=\"let element\"  style=\"display:none;\"> {{element.createdBy}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"symbol\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"ViewRevocationDetails()\">error</a>\r\n          </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-right view-btn-wraper\">\r\n        <button  class=\"btn view-detail-btn\" (click)=\"RevocationFlag = !RevocationFlag\">View New Captured Revocation Order Details</button>\r\n\r\n      </div>\r\n\r\n      <div *ngIf=\"RevocationFlag\">\r\n        <div class=\"fom-title\">New captured Recovery of Challan Details<i class=\"material-icons close-icon\" (click)=\"RevocationFlag = !RevocationFlag\">clear</i></div>\r\n\r\n\r\n        <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"LoanType\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.LoanType}} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"AmountDisbursed\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Amount deposited via Challan </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.AmountDisbursed}} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"CurrentRecoveryof\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Current Recovery of </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.CurrentRecoveryof}} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"TotalNoInstalments\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Total No.Instalments </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.TotalNoInstalments}} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"Paymentmadethrough\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Payment made through </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.Paymentmadethrough}} </td>\r\n          </ng-container>\r\n          <ng-container matColumnDef=\"status\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Status </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"symbol\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a>\r\n            </td>\r\n          </ng-container>\r\n\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n        </table>\r\n\r\n        <mat-paginator [pageSizeOptions]=\"[5, 10]\"></mat-paginator>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/loanmgt/recovry-challan/recovry-challan.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/loanmgt/recovry-challan/recovry-challan.component.ts ***!
  \**********************************************************************/
/*! exports provided: RecovryChallanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecovryChallanComponent", function() { return RecovryChallanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_loan_mgmt_recovery_challan_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/recovery-challan.service */ "./src/app/services/loan-mgmt/recovery-challan.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
    { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
    { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
];
var ELEMENT_DATA2 = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
];
var RecovryChallanComponent = /** @class */ (function () {
    function RecovryChallanComponent(snackBar, _Service, objCommanService) {
        this.snackBar = snackBar;
        this._Service = _Service;
        this.objCommanService = objCommanService;
        this._loanType = [];
        this.RevocationFlag = false;
        this.getPaymentById = {};
        this.panelOpenState = false;
        this.displayedColumns = ['LoanType', 'AmountDisbursed', 'CurrentRecoveryof', 'TotalNoInstalments', 'createdBy', 'status', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](ELEMENT_DATA);
        //@ViewChild('formDirective') private formDirective;
        this.displayedColumns2 = ['LoanType', 'AmountDisbursed', 'CurrentRecoveryof', 'TotalNoInstalments', 'status', 'symbol'];
        this.dataSource2 = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](ELEMENT_DATA2);
        this.recoveryArr = [];
        this.isSubmitted = false;
        this.createForm();
    }
    RecovryChallanComponent.prototype.ngOnInit = function () {
        this.btnUpdateText = 'Save';
        this.showAndHideMode = 1;
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.dataSource.sort = this.sort;
        this.BindLoanType();
    };
    RecovryChallanComponent.prototype.modeChange = function (val) {
        debugger;
        if (val == 1) {
            this.showAndHideMode = 1;
            //this.form.controls['challanNo'].setValidators([Validators.required]);
            //this.form.controls['challanAmount'].setValidators([Validators.required]);
            //this.form.controls['challanDate'].setValidators([Validators.required]);
            //this.form.controls['paymentMode'].setValidators([Validators.required]);
            //this.form.controls['chequeNoDDNo'].setValidators([Validators.required]);
            //this.form.controls['outstandingAmount'].setValidators([Validators.required]);
            //this.form.controls['lastInstalmentNoRecovered'].setValidators([Validators.required]); 
        }
        if (val == 2) {
            this.showAndHideMode = 2;
            //this.form.controls['NTRPTransactionId'].setValidators([Validators.required]);
            //this.form.controls['NTRPchallanAmt'].setValidators([Validators.required]); 
            //this.form.controls['NTRPchallanDate'].setValidators([Validators.required]);
            //this.form.controls['NTRPpaymentMode'].setValidators([Validators.required]);
            //this.form.controls['NTRPReceiptDetails'].setValidators([Validators.required]);
            //this.form.controls['NTRPupdatedOutstandingAmount'].setValidators([Validators.required]);
            //this.form.controls['NTRPupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
        }
        if (val == 3) {
            this.showAndHideMode = 3;
            //this.form.controls['ProchallanNo'].setValidators([Validators.required]);
            //this.form.controls['ProchallanAmount'].setValidators([Validators.required]);
            //this.form.controls['ProchallanDate'].setValidators([Validators.required]);
            //this.form.controls['PropaymentMode'].setValidators([Validators.required]);
            //this.form.controls['ProupdatedOutstandingAmount'].setValidators([Validators.required]);
            //this.form.controls['ProupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]); 
            //this.form.controls['Proremarks'].setValidators([Validators.required]); 
        }
        if (val == 4) {
            this.showAndHideMode = 4;
            //this.form.controls['TENo'].setValidators([Validators.required]);
            //this.form.controls['TEAmounts'].setValidators([Validators.required]);
            //this.form.controls['TEDate'].setValidators([Validators.required]);
            //this.form.controls['TEpaymentMode'].setValidators([Validators.required]);
            //this.form.controls['TEupdatedOutstandingAmount'].setValidators([Validators.required]);
            //this.form.controls['TEupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
            //this.form.controls['TEremarks'].setValidators([Validators.required]); 
            //this.form.controls['ProchallanNo'].setValidators(null);
            //this.form.controls.ProchallanNo.setErrors(null);
            //this.form.controls.ProchallanAmount.setValidators(null);
            //this.form.controls.ProchallanAmount.setErrors(null);
            //this.form.controls.ProchallanDate.setValidators(null);
            //this.form.controls.ProchallanDate.setErrors(null);
            //this.form.controls.PropaymentMode.setValidators(null);
            //this.form.controls.PropaymentMode.setErrors(null);
            //this.form.controls.ProupdatedOutstandingAmount.setValidators(null);
            //this.form.controls.ProupdatedOutstandingAmount.setErrors(null);
            //this.form.controls.ProupdatedLastInstalmentNoRecovered.setValidators(null);
            //this.form.controls.ProupdatedLastInstalmentNoRecovered.setErrors(null);
            //this.form.controls.Proremarks.setValidators(null);
            //this.form.controls.Proremarks.setErrors(null);
            //this.form.controls.NTRPTransactionId.setValidators(null);
            //this.form.controls.NTRPTransactionId.setErrors(null);
            //this.form.controls.NTRPchallanAmt.setValidators(null);
            //this.form.controls.NTRPchallanAmt.setErrors(null);
            //this.form.controls.NTRPchallanDate.setValidators(null);
            //this.form.controls.NTRPchallanDate.setErrors(null);
            //this.form.controls.NTRPpaymentMode.setValidators(null);
            //this.form.controls.NTRPpaymentMode.setErrors(null);
            //this.form.controls.NTRPReceiptDetails.setValidators(null);
            //this.form.controls.NTRPReceiptDetails.setErrors(null);
            //this.form.controls.NTRPupdatedOutstandingAmount.setValidators(null);
            //this.form.controls.NTRPupdatedOutstandingAmount.setErrors(null);
            //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setValidators(null);
            //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setErrors(null);
            //// this.form.controls.challanNo.setValidators(null);
            //// this.form.controls.challanNo.setErrors(null);
            // this.form.controls.challanAmount.setValidators(null);
            // this.form.controls.challanAmount.setErrors(null);
            // this.form.controls.challanDate.setValidators(null);
            // this.form.controls.challanDate.setErrors(null);
            // this.form.controls.chequeNoDDNo.setValidators(null);
            // this.form.controls.chequeNoDDNo.setErrors(null);
            // this.form.controls.outstandingAmount.setValidators(null);
            // this.form.controls.outstandingAmount.setErrors(null);
            // this.form.controls.lastInstalmentNoRecovered.setValidators(null);
            // this.form.controls.lastInstalmentNoRecovered.setErrors(null);
        }
    };
    RecovryChallanComponent.prototype.validateMethod = function (val) {
        debugger;
        if (val == 1) {
            this.showAndHideMode = 1;
            this.form.controls['challanNo'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['challanAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['challanDate'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['paymentMode'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['chequeNoDDNo'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['outstandingAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['lastInstalmentNoRecovered'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        }
        if (val == 2) {
            this.showAndHideMode = 2;
            this.form.controls['NTRPTransactionId'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPchallanAmt'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPchallanDate'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPpaymentMode'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPReceiptDetails'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPupdatedOutstandingAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['NTRPupdatedLastInstalmentNoRecovered'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        }
        if (val == 3) {
            this.showAndHideMode = 3;
            this.form.controls['ProchallanNo'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['ProchallanAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['ProchallanDate'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['PropaymentMode'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['ProupdatedOutstandingAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['ProupdatedLastInstalmentNoRecovered'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['Proremarks'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        }
        if (val == 4) {
            this.showAndHideMode = 4;
            this.form.controls['TENo'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEAmounts'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEDate'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEpaymentMode'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEupdatedOutstandingAmount'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEupdatedLastInstalmentNoRecovered'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            this.form.controls['TEremarks'].setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
            //this.form.controls['ProchallanNo'].setValidators(null);
            //this.form.controls.ProchallanNo.setErrors(null);
            //this.form.controls.ProchallanAmount.setValidators(null);
            //this.form.controls.ProchallanAmount.setErrors(null);
            //this.form.controls.ProchallanDate.setValidators(null);
            //this.form.controls.ProchallanDate.setErrors(null);
            //this.form.controls.PropaymentMode.setValidators(null);
            //this.form.controls.PropaymentMode.setErrors(null);
            //this.form.controls.ProupdatedOutstandingAmount.setValidators(null);
            //this.form.controls.ProupdatedOutstandingAmount.setErrors(null);
            //this.form.controls.ProupdatedLastInstalmentNoRecovered.setValidators(null);
            //this.form.controls.ProupdatedLastInstalmentNoRecovered.setErrors(null);
            //this.form.controls.Proremarks.setValidators(null);
            //this.form.controls.Proremarks.setErrors(null);
            //this.form.controls.NTRPTransactionId.setValidators(null);
            //this.form.controls.NTRPTransactionId.setErrors(null);
            //this.form.controls.NTRPchallanAmt.setValidators(null);
            //this.form.controls.NTRPchallanAmt.setErrors(null);
            //this.form.controls.NTRPchallanDate.setValidators(null);
            //this.form.controls.NTRPchallanDate.setErrors(null);
            //this.form.controls.NTRPpaymentMode.setValidators(null);
            //this.form.controls.NTRPpaymentMode.setErrors(null);
            //this.form.controls.NTRPReceiptDetails.setValidators(null);
            //this.form.controls.NTRPReceiptDetails.setErrors(null);
            //this.form.controls.NTRPupdatedOutstandingAmount.setValidators(null);
            //this.form.controls.NTRPupdatedOutstandingAmount.setErrors(null);
            //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setValidators(null);
            //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setErrors(null);
            //// this.form.controls.challanNo.setValidators(null);
            //// this.form.controls.challanNo.setErrors(null);
            // this.form.controls.challanAmount.setValidators(null);
            // this.form.controls.challanAmount.setErrors(null);
            // this.form.controls.challanDate.setValidators(null);
            // this.form.controls.challanDate.setErrors(null);
            // this.form.controls.chequeNoDDNo.setValidators(null);
            // this.form.controls.chequeNoDDNo.setErrors(null);
            // this.form.controls.outstandingAmount.setValidators(null);
            // this.form.controls.outstandingAmount.setErrors(null);
            // this.form.controls.lastInstalmentNoRecovered.setValidators(null);
            // this.form.controls.lastInstalmentNoRecovered.setErrors(null);
        }
    };
    RecovryChallanComponent.prototype.createForm = function () {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            loanType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            createdBy: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            payLoanRefLoanCD: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            amountDisbursed: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            installmentAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            totalNoInstalments: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            oddInstlNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            oddInstlAmt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            outstandingAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            lastInstalmentNoRecovered: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            paymentmadethrough: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            PFMSchallanNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PFMSchallanAmt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PFMSchallanDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]({ value: this.currentDate }, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PFMSpaymentMode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            chequeNoDDNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PFMSupdatedOutstandingAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            PFMSupdatedLastInstalmentNoRecovered: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            NTRPTransactionId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPchallanAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPReceiptDetails: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPchallanAmt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPchallanDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPpaymentMode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPupdatedOutstandingAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            NTRPupdatedLastInstalmentNoRecovered: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ProchallanNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ProchallanAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ProchallanDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            PropaymentMode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ProupdatedOutstandingAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ProupdatedLastInstalmentNoRecovered: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            Proremarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TENo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEAmounts: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEpaymentMode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEupdatedOutstandingAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEupdatedLastInstalmentNoRecovered: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            TEremarks: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            flag: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0)
        });
    };
    RecovryChallanComponent.prototype.onRadiobtnClick = function () {
    };
    RecovryChallanComponent.prototype.onSubmit = function () {
        debugger;
        if (!this.form.valid) {
            return false;
        }
        else {
            debugger;
            this.tempValue = this.form.get('paymentmadethrough').value;
            this.validateMethod(this.tempValue);
            this.form.controls.flag.setValue(1);
            //this.form.controls.get('')
            this.form.get('flag').value;
            //this._Service.InsertUpateRecoveryChallan(this.form.value).subscribe(result1 => {
            //  let resultvalue = result1.split("-");
            //  if (parseInt(resultvalue[0]) >= 1) {
            //    if (this.btnUpdateText == 'Update') {
            //      this.snackBar.open('Update Successfully', null, { duration: 4000 });
            //      this.form.reset();
            //      this.formDirective.resetForm();
            //      this.btnUpdateText = 'Save';
            //    }
            //    else {
            //      this.snackBar.open('Save Successfully', null, { duration: 4000 });
            //      this.form.reset();
            //      this.formDirective.resetForm();
            //      // this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
            //      this.btnUpdateText == 'Save';
            //    }
            //  }
            //  //else {
            //  //  if (this.btnUpdateText == 'Update') {
            //  //    if (resultvalue[2] = 'Duplicate Record') {
            //  //      this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
            //  //    }
            //  //    else {
            //  //      this.snackBar.open('Update not Successfully', null, { duration: 4000 });
            //  //    }
            //  //  }
            //  //  else if (resultvalue[2] = 'Duplicate Record') {
            //  //    this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
            //  //  }
            //  //  else {
            //  //    this.snackBar.open('Save not Successfully', null, { duration: 4000 });
            //  //  }
            //  //}
            //})
        }
        //this.form.controls.ProchallanAmount.setValidators(null);
        this.form.get('ProchallanAmount').clearValidators();
        //this.form.get('ProchallanAmount').setErrors(null);
        //this.form.get('ProchallanAmount').updateValueAndValidity();
    };
    RecovryChallanComponent.prototype.BindLoanType = function () {
        var _this = this;
        debugger;
        this.loantypeFlag = '4';
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this._loanType = data;
        });
    };
    RecovryChallanComponent.prototype.GetcommanMethod = function (value) {
        this.getEmployeeDeatils(value, "0");
    };
    RecovryChallanComponent.prototype.applyFilter = function (filterValue) {
    };
    RecovryChallanComponent.prototype.getEmployeeDeatils = function (EmpCode, MsDesignID) {
        var _this = this;
        debugger;
        this._Service.RecoveryChallan(EmpCode, MsDesignID).subscribe(function (result) {
            if (result.length == 0) {
                _this.RecoveryChallanData = null;
            }
            else {
                debugger;
                _this.recoveryArr = result;
                console.log(_this.recoveryArr);
                _this.RecoveryChallanData = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](result);
                _this.RecoveryChallanData.paginator = _this.paginator;
                _this.RecoveryChallanData.sort = _this.sort;
            }
        });
    };
    RecovryChallanComponent.prototype.charaterOnlyNoSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    RecovryChallanComponent.prototype.ViewRevocationDetails = function () {
        debugger;
        this.panelOpenState = true;
        console.log(this.recoveryArr);
        this.form.patchValue(this.recoveryArr[0]);
    };
    RecovryChallanComponent.prototype.ResetForm = function () {
        this.panelOpenState = false;
        // this.form.reset();
        this.formDirective.resetForm();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], RecovryChallanComponent.prototype, "formDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], RecovryChallanComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], RecovryChallanComponent.prototype, "paginator", void 0);
    RecovryChallanComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-recovry-challan',
            template: __webpack_require__(/*! ./recovry-challan.component.html */ "./src/app/loanmgt/recovry-challan/recovry-challan.component.html"),
            styles: [__webpack_require__(/*! ./recovry-challan.component.css */ "./src/app/loanmgt/recovry-challan/recovry-challan.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _services_loan_mgmt_recovery_challan_service__WEBPACK_IMPORTED_MODULE_2__["RecoveryChallanService"], _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_4__["CommanService"]])
    ], RecovryChallanComponent);
    return RecovryChallanComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/loanmgt/recovy-schedule/recovy-schedule.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvcmVjb3Z5LXNjaGVkdWxlL3JlY292eS1zY2hlZHVsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.html":
/*!************************************************************************!*\
  !*** ./src/app/loanmgt/recovy-schedule/recovy-schedule.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  recovy-schedule works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/loanmgt/recovy-schedule/recovy-schedule.component.ts ***!
  \**********************************************************************/
/*! exports provided: RecovyScheduleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecovyScheduleComponent", function() { return RecovyScheduleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RecovyScheduleComponent = /** @class */ (function () {
    function RecovyScheduleComponent() {
    }
    RecovyScheduleComponent.prototype.ngOnInit = function () {
    };
    RecovyScheduleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-recovy-schedule',
            template: __webpack_require__(/*! ./recovy-schedule.component.html */ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.html"),
            styles: [__webpack_require__(/*! ./recovy-schedule.component.css */ "./src/app/loanmgt/recovy-schedule/recovy-schedule.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RecovyScheduleComponent);
    return RecovyScheduleComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3QvcmVscy1lbXAtbHZsL3JlbHMtZW1wLWx2bC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.html":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\" ></app-comman-mst>\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form (ngSubmit)=\"Sanction.valid && btnsubmit()\" #Sanction=\"ngForm\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Release Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\"\r\n                      [(ngModel)]=\"_sanctiondetails.payLoanRefLoanCD\" name=\"payLoanRefLoanCD\"\r\n                      #payLoanRefLoanCD=\"ngModel\" required [disabled]=disbleflag>\r\n            <mat-option label=\"Select Loan Type\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of ArrLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span *ngIf=\"payLoanRefLoanCD.submitted && payLoanRefLoanCD.invalid\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\"> Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput name=\"sancOrdNo\" placeholder=\"Order No\" #sancOrdNo=\"ngModel\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_sanctiondetails.sancOrdNo\" BlockCopyPaste\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdNo.errors?.required\">Order Number is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker1\" placeholder=\"Order Date\" title=\"Order Date\"\r\n                 [(ngModel)]=\"_sanctiondetails.sancOrdDT\" (click)=\"picker1.open()\" name=\"sancOrdDT\" #sancOrdDT=\"ngModel\" [disabled]=disbleflag required BlockCopyPaste=\"enter\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker1></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdDT.errors?.required\">Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Sanctioned Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtDisbursed\" placeholder=\"Amount Request Loan\" #loanAmtDisbursed=\"ngModel\" BlockCopyPaste (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_sanctiondetails.loanAmtDisbursed\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtDisbursed.errors?.required\">Amount Request Loan is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtSanc\" placeholder=\"Sanction Amount\" (keypress)=\"numberOnly($event)\" #loanAmtSanc=\"ngModel\" [(ngModel)]=\"_sanctiondetails.loanAmtSanc\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Sanction Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12  col-lg-12\">\r\n        <mat-radio-group [(ngModel)]=\"_sanctiondetails.empApptType\" name=\"empApptType\" #empApptType=\"ngModel\" [disabled]=disbleflag (click)=\"setempApptType(_sanctiondetails.empApptType)\">\r\n          <mat-radio-button value=\"R\">Employee</mat-radio-button>\r\n          <mat-radio-button value=\"T\">Vendor</mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empName\" placeholder=\"Name\" #empName=\"ngModel\" [(ngModel)]=\"_sanctiondetails.empName\"\r\n                 [disabled]=EmpTypeFlag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!empName.errors?.required\">Name is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"description\" placeholder=\"Description \" #description=\"ngModel\" [(ngModel)]=\"_sanctiondetails.description\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!description.errors?.required\">Description is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Bank Details</div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"IFSC Code\" required [(ngModel)]=\"_sanctiondetails.ifscCD\" name=\"ifscCD\" (blur)=\"getBankDetailsByIFSC(_sanctiondetails.ifscCD)\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Bank Name\" required [(ngModel)]=\"objBankDetails.bankName\" name=\"bankName\" #bankName=\"ngModel\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Branch Name\" required [(ngModel)]=\"objBankDetails.branchName\" name=\"branchName\" #branchName=\"ngModel\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Saving A/c No\" required [(ngModel)]=\"_sanctiondetails.bnkAcNo\" #showhideinput type=\"{{type}}\" maxlength=\"15\" name=\"bnkAcNo\"\r\n                 BlockCopyPaste (keypress)=\"numberOnly($event)\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n        <a (click)=\"toggleShow()\"><i class=\"fa fa-eye-slash\" ></i></a>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Confirm Saving A/c No\" required [(ngModel)]=\"_sanctiondetails.ConfirmbnkAcNo\" maxlength=\"15\" name=\"bnkAcNo\" #bnkAcNo=\"ngModel\" BlockCopyPaste (keypress)=\"numberOnly($event)\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" id=\"btn_save\" class=\"btn btn-primary\" *ngIf=\"btndisable\">\r\n          <i class=\"fa fa-floppy-o\"></i>{{buttonText}}\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetLoanDdetailsForm()\" *ngIf=\"btndisable\">Cancel</button>\r\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"forwardddochecker()\" *ngIf=\"btndisable\">Forward to Checker</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"EMPId\" style=\"display:none;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"display:none;\"> ID </th>\r\n        <td mat-cell *matCellDef=\"let element\" style=\"display:none;\"> {{element.id}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"EMPCode\" style=\"width:120px;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> EMP Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"LoanCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.loanCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PurposeCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Purpose  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.purposeCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">error</a>\r\n          <span>\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">edit</a>\r\n          </span>\r\n          <span>\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"SetDeleteId(element.id);deletepopup = !deletepopup\"> delete_forever </a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteEmployeeById(setDeletIDOnPopup)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup=!deletepopup\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.ts ***!
  \****************************************************************/
/*! exports provided: RelsEmpLvlComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RelsEmpLvlComponent", function() { return RelsEmpLvlComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/new-loan.service */ "./src/app/services/loan-mgmt/new-loan.service.ts");
/* harmony import */ var _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/LoanModel/sanction.model */ "./src/app/model/LoanModel/sanction.model.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/loan-mgmt/sanction.service */ "./src/app/services/loan-mgmt/sanction.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RelsEmpLvlComponent = /** @class */ (function () {
    function RelsEmpLvlComponent(_msg, _Service, objSanctionService, objCommanService, objBankService) {
        this._msg = _msg;
        this._Service = _Service;
        this.objSanctionService = objSanctionService;
        this.objCommanService = objCommanService;
        this.objBankService = objBankService;
        this.buttonText = 'Save';
        this.objBankDetails = {};
        this.displayedColumns = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
        this.selectedIndex = 0;
        this.btndisable = true;
        this.show = false;
        this.type = "password";
        this._sanctiondetails = new _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__["Sanction"]();
    }
    RelsEmpLvlComponent.prototype.ngOnInit = function () {
        //  this.resetLoanDdetailsForm();
        this.BindLoanType();
        this.username = sessionStorage.getItem('username');
        this.FindEmpCode(this.username);
    };
    RelsEmpLvlComponent.prototype.FindEmpCode = function (username) {
        var _this = this;
        this.objCommanService.GetEmpCode(username).subscribe(function (data) {
            _this.ArrEmpCode = data[0];
            _this._Service.DdoId = _this.ArrEmpCode.ddoid;
            if (_this._Service.DdoId != null || _this._Service.DdoId != NaN) {
                _this.BindGridLoanSanction(_this._Service.DdoId);
            }
            if (_this.ArrEmpCode.serviceType != 'R') {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.PermanentEmpeApplyLoan);
            }
            if (_this._Service.RoleId == 8 || _this._Service.RoleId == 9) {
                _this.empstatus = true;
            }
            if (_this.ArrEmpCode.empcode != null) {
                _this.getLoanDetails(_this.ArrEmpCode.empcode);
            }
        });
    };
    RelsEmpLvlComponent.prototype.GetcommanMethod = function (value) {
        this.getLoanDetails(value);
    };
    RelsEmpLvlComponent.prototype.BindLoanType = function () {
        var _this = this;
        this.loantypeFlag = '2';
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this.ArrLoanType = data;
        });
    };
    RelsEmpLvlComponent.prototype.getLoanDetails = function (value) {
        this.getEmployeeDeatils(value, '0');
    };
    RelsEmpLvlComponent.prototype.getEmployeeDeatils = function (EmpCode, MsDesignID) {
        var _this = this;
        this.mode = 1;
        this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(function (data) {
            _this._sanctiondetails = data[0];
            if (_this._sanctiondetails != undefined) {
                _this.getBankDetailsByIFSC(_this._sanctiondetails.ifscCD);
            }
            if (_this._sanctiondetails == undefined) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.noRecordMsg);
            }
            if (_this._sanctiondetails.empApptType == 'R') {
                _this.EmpTypeFlag = true;
            }
            else {
                _this.EmpTypeFlag = false;
            }
        });
    };
    RelsEmpLvlComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    RelsEmpLvlComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    RelsEmpLvlComponent.prototype.btnsubmit = function () {
        var _this = this;
        if (this.buttonText == 'Save') {
            if (this._sanctiondetails.ConfirmbnkAcNo == this._sanctiondetails.bnkAcNo) {
                this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(function (data) {
                    _this._sanctiondetails = data;
                    _this._Service.HiddenId = data;
                    if (data == "-1") {
                        _this.BindGridLoanSanction(_this._Service.DdoId);
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.alreadyExistMsg);
                    }
                    else {
                        _this.BindGridLoanSanction(_this._Service.DdoId);
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                    }
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.AccountNumberNotMatched);
                this._sanctiondetails.ConfirmbnkAcNo = '';
                this._sanctiondetails.bnkAcNo = '';
            }
        }
        if (this.buttonText == 'Update') {
            this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(function (data) {
                _this._sanctiondetails = data;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                _this.BindGridLoanSanction(_this._Service.DdoId);
            });
        }
    };
    RelsEmpLvlComponent.prototype.BindGridLoanSanction = function (value) {
        var _this = this;
        this.mode = 3;
        this._Service.DdoId = value;
        this._Service.BindLoanStatus(this.mode, this._Service.DdoId).subscribe(function (data) {
            _this.ArrSanction = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    RelsEmpLvlComponent.prototype.ViewEditLoanDetailsById = function (id, flag) {
        var _this = this;
        this.mode = 2;
        this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(function (data) {
            _this._sanctiondetails = data[0];
            _this.buttonText = "Update";
            if (flag == 1) {
                _this.disbleflag = true;
                _this.btndisable = false;
            }
            if (flag == 0) {
                _this.disbleflag = false;
                _this.btndisable = true;
            }
        });
    };
    RelsEmpLvlComponent.prototype.setempApptType = function (value) {
        if (value == "R") {
            this.EmpTypeFlag = false;
        }
        if (value == "T") {
            this.EmpTypeFlag = true;
        }
    };
    RelsEmpLvlComponent.prototype.getBankDetailsByIFSC = function (ifscCD) {
        var _this = this;
        this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(function (res) {
            _this.objBankDetails = res;
        });
    };
    RelsEmpLvlComponent.prototype.SetDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    RelsEmpLvlComponent.prototype.deleteEmployeeById = function (id) {
        var _this = this;
        this.mode = 2;
        this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(function (data) {
            _this._sanctiondetails = data;
            if (_this._sanctiondetails != null) {
                _this.deletepopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.BindGridLoanSanction(_this._Service.DdoId);
                _this.SanctionForm.resetForm();
            }
        });
    };
    RelsEmpLvlComponent.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('Sanction'),
        __metadata("design:type", Object)
    ], RelsEmpLvlComponent.prototype, "SanctionForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], RelsEmpLvlComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RelsEmpLvlComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"])('showhideinput'),
        __metadata("design:type", Object)
    ], RelsEmpLvlComponent.prototype, "input", void 0);
    RelsEmpLvlComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rels-emp-lvl',
            template: __webpack_require__(/*! ./rels-emp-lvl.component.html */ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.html"),
            styles: [__webpack_require__(/*! ./rels-emp-lvl.component.css */ "./src/app/loanmgt/rels-emp-lvl/rels-emp-lvl.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"], _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__["NewLoanService"], _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_8__["SanctionService"],
            _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_7__["CommanService"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__["BankDetailsService"]])
    ], RelsEmpLvlComponent);
    return RelsEmpLvlComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-tooltip {\r\n  color: yellow !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9hbm1ndC9yZWxzLWhvby1sdmwvcmVscy1ob28tbHZsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBeUI7Q0FDMUIiLCJmaWxlIjoic3JjL2FwcC9sb2FubWd0L3JlbHMtaG9vLWx2bC9yZWxzLWhvby1sdmwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtdG9vbHRpcCB7XHJcbiAgY29sb3I6IHllbGxvdyAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.html":
/*!******************************************************************!*\
  !*** ./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"userRoleID !=17\">\r\n  <app-comman-mst (onchange)=\"GetcommanMethod($event)\" ></app-comman-mst>\r\n</div>\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form (ngSubmit)=\"Release.valid && btnsubmit()\" #Release=\"ngForm\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Release Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\"\r\n                      [(ngModel)]=\"_releasedetails.payLoanRefLoanCD\" name=\"payLoanRefLoanCD\"\r\n                      #payLoanRefLoanCD=\"ngModel\" required [disabled]=disbleflag>\r\n            <mat-option label=\"Select Loan Type\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of ArrLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span *ngIf=\"payLoanRefLoanCD.submitted && payLoanRefLoanCD.invalid\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\"> Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput name=\"sancOrdNo\" placeholder=\"Order No\" #sancOrdNo=\"ngModel\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_releasedetails.sancOrdNo\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdNo.errors?.required\">Order Number is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker1\" placeholder=\"Order Date\" title=\"Order Date\" [max]=\"maxDate\"\r\n                 [(ngModel)]=\"_releasedetails.sancOrdDT\" (click)=\"picker1.open()\" name=\"sancOrdDT\" #sancOrdDT=\"ngModel\" [disabled]=disbleflag required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker1></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdDT.errors?.required\">Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Sanctioned Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtDisbursed\" placeholder=\"Amount Request Loan\"\r\n                 #loanAmtDisbursed=\"ngModel\" BlockCopyPaste (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_releasedetails.loanAmtDisbursed\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtDisbursed.errors?.required\">Amount Request Loan is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtSanc\" placeholder=\"Sanction Amount\" (keypress)=\"numberOnly($event)\"\r\n                 #loanAmtSanc=\"ngModel\" [(ngModel)]=\"_releasedetails.loanAmtSanc\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Sanction Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empName\" placeholder=\"User Name\" #empName=\"ngModel\" [(ngModel)]=\"_releasedetails.empName\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!empName.errors?.required\">User Name is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"description\" placeholder=\"Description \" #description=\"ngModel\" [(ngModel)]=\"_releasedetails.description\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!description.errors?.required\">Description is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Bank Details</div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"IFSC Code\" [(ngModel)]=\"_releasedetails.ifscCD\" name=\"ifscCD\" (blur)=\"getBankDetailsByIFSC(_releasedetails.ifscCD)\" [disabled]=disbleflag required>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Bank Name\" [(ngModel)]=\"objBankDetails.bankName\" name=\"bankName\" #bankName=\"ngModel\" [disabled]=disbleflag required>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Branch Name\" [(ngModel)]=\"objBankDetails.branchName\" name=\"branchName\" #branchName=\"ngModel\" [disabled]=disbleflag required>\r\n        </mat-form-field>\r\n      </div>\r\n       <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Saving A/c No\" [(ngModel)]=\"_releasedetails.bnkAcNo\" type=\"{{type}}\" maxlength=\"15\" name=\"bnkAcNo\"\r\n                 (keypress)=\"numberOnly($event)\" [disabled]=disbleflag required>\r\n        </mat-form-field>\r\n        <a (click)=\"toggleShow()\">show/hide</a>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Confirm Saving A/c No\" required [(ngModel)]=\"_releasedetails.ConfirmbnkAcNo\" maxlength=\"15\" name=\"bnkAcNo\" #bnkAcNo=\"ngModel\" BlockCopyPaste (keypress)=\"numberOnly($event)\" [disabled]=EmpTypeFlag>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" id=\"btn_save\" class=\"btn btn-primary\" *ngIf=\"btndisable\">\r\n          <i class=\"fa fa-floppy-o\"></i>{{buttonText}}\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetLoanDdetailsForm()\" *ngIf=\"btndisable\">Cancel</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarDDOMaker()\" *ngIf=\"disableFwdMakerFlag\">Forward to DDO Maker</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarDDOChecker()\" *ngIf=\"disableFwdCheckerFlag\">Forward to DDO Checker</button>\r\n        <button class=\"btn btn-success\" type=\"button\" (click)=\"VerifyChecker()\" *ngIf=\"disableVerifyCheckerFlag\">Verify</button>\r\n        <button class=\"btn btn-danger\" type=\"button\" *ngIf=\"disableRejectCheckerFlag\" (click)=\"deletePopup = !deletePopup\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"EMPId\" style=\"display:none;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"display:none;\"> ID </th>\r\n        <td mat-cell *matCellDef=\"let element\" style=\"display:none;\"> {{element.id}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"EMPCode\" style=\"width:120px;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> EMP Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"LoanCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.loanCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PurposeCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Purpose  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.purposeCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a  matTooltip=\"{{element.moduleStatus}}\"> {{element.status}}</a>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a class=\"material-icons i-info\" matTooltip=\"view details\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">error</a>\r\n          <span *ngIf=\"element.statusId == '54' && userRoleID==6\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == '55' && userRoleID==6\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == '58' && userRoleID==6\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == '56' && userRoleID==5\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteEmployeeById(setDeletIDOnPopup)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletePopup = !deletePopup\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"deletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Please Enter Remarks.</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_releasedetails.release_Remarks\" title=\"Remarks\" name=\"release_Remarks\" #release_Remarks=\"ngModel\" required></textarea>\r\n          <mat-error>\r\n            <span [hidden]=\"!release_Remarks.errors?.required\">Remarks is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"Reject()\">Ok</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletePopup = !deletePopup\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.ts ***!
  \****************************************************************/
/*! exports provided: RelsHooLvlComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RelsHooLvlComponent", function() { return RelsHooLvlComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/new-loan.service */ "./src/app/services/loan-mgmt/new-loan.service.ts");
/* harmony import */ var _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/LoanModel/sanction.model */ "./src/app/model/LoanModel/sanction.model.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/loan-mgmt/sanction.service */ "./src/app/services/loan-mgmt/sanction.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RelsHooLvlComponent = /** @class */ (function () {
    function RelsHooLvlComponent(_msg, _Service, objSanctionService, objCommanService, objBankService) {
        this._msg = _msg;
        this._Service = _Service;
        this.objSanctionService = objSanctionService;
        this.objCommanService = objCommanService;
        this.objBankService = objBankService;
        this.maxDate = new Date();
        this.buttonText = 'Save';
        this.objBankDetails = {};
        this.displayedColumns = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
        this.selectedIndex = 0;
        this.btndisable = false;
        this.show = false;
        this.type = "password";
        this.disableVerifyCheckerFlag = false;
        this.disableRejectCheckerFlag = false;
        this._releasedetails = new _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__["Sanction"]();
    }
    RelsHooLvlComponent.prototype.ngOnInit = function () {
        this.resetReleaseForm();
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.BindLoanType();
        this.BindGridLoanSanction(this.UserID);
    };
    RelsHooLvlComponent.prototype.GetcommanMethod = function (value) {
        this.getLoanDetails(value);
    };
    RelsHooLvlComponent.prototype.BindLoanType = function () {
        var _this = this;
        this.loantypeFlag = '2';
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this.ArrLoanType = data;
        });
    };
    RelsHooLvlComponent.prototype.getLoanDetails = function (value) {
        this.getEmployeeDeatils(value, '0');
    };
    RelsHooLvlComponent.prototype.getEmployeeDeatils = function (EmpCode, MsDesignID) {
        var _this = this;
        this.mode = 1;
        this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(function (data) {
            _this._releasedetails = data[0];
            if (_this._releasedetails != undefined) {
                _this.getBankDetailsByIFSC(_this._releasedetails.ifscCD);
            }
            if (_this._releasedetails == undefined) {
                _this._releasedetails = new _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__["Sanction"]();
            }
            if (_this._releasedetails.empApptType == 'R') {
                _this.EmpTypeFlag = true;
            }
            else {
                _this.EmpTypeFlag = false;
            }
        });
    };
    RelsHooLvlComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    RelsHooLvlComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    RelsHooLvlComponent.prototype.resetReleaseForm = function () {
        this.ReleaseForm.resetForm();
        this.objBankDetails.branchName = '';
        this._releasedetails.bnkAcNo = '';
        this.objBankDetails.branchName = '';
        this._releasedetails.ConfirmbnkAcNo = '';
    };
    RelsHooLvlComponent.prototype.btnsubmit = function () {
        var _this = this;
        if (this.buttonText == 'Save') {
            if (this.userRoleID == 6) {
                this._releasedetails.priVerifFlag = 55;
            }
            else {
                this._releasedetails.priVerifFlag = 8;
            }
            this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(function (data) {
                _this._releasedetails = data;
                _this._Service.HiddenId = data;
                if (data == "-1") {
                    _this._Service.DdoId = _this.UserID;
                    _this.BindGridLoanSanction(_this._Service.DdoId);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.alreadyExistMsg);
                }
                else {
                    _this._Service.DdoId = _this.UserID;
                    _this.BindGridLoanSanction(_this._Service.DdoId);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                    _this.ReleaseForm.resetForm();
                }
            });
        }
        if (this.buttonText == 'Update') {
            if (this.userRoleID == 6) {
                this.disableFwdCheckerFlag = true;
                this._releasedetails.priVerifFlag = 55;
            }
            else {
                this.disableFwdCheckerFlag = false;
                this.disableFwdMakerFlag = true;
                this._releasedetails.priVerifFlag = 8;
            }
            this._releasedetails.msEmpId = this.tempId;
            this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(function (data) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                _this._Service.DdoId = _this.UserID;
                _this.BindGridLoanSanction(_this._Service.DdoId);
                _this.ReleaseForm.resetForm();
            });
        }
    };
    RelsHooLvlComponent.prototype.resetLoanDdetailsForm = function () {
        this.ReleaseForm.resetForm();
    };
    RelsHooLvlComponent.prototype.BindGridLoanSanction = function (value) {
        var _this = this;
        this.mode = 3; //loan sanction and release details(HOO Label) ,select data in the table
        this.UserID = value;
        this._Service.BindLoanStatus(this.mode, this.UserID).subscribe(function (data) {
            _this.ArrSanction = data;
            _this._releasedetails = data;
            _this.DescripStatus = _this.ArrSanction.moduleStatus;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    RelsHooLvlComponent.prototype.ViewEditLoanDetailsById = function (id, flag) {
        var _this = this;
        debugger;
        this.mode = 2;
        this.tempId = id;
        this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(function (data) {
            debugger;
            _this._releasedetails = data[0];
            _this._releasedetails.bnkAcNo = data[0].bnkAcNo;
            _this._releasedetails.confirmbnkAcNo = data[0].bnkAcNo;
            _this.buttonText = "Update";
            if (_this._releasedetails.priVerifFlag == 23) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._releasedetails.priVerifFlag == 35) {
                _this.btndisable = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdMakerFlag = false;
                    _this.disableFwdCheckerFlag = false;
                }
                else {
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._releasedetails.priVerifFlag == 31) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._releasedetails.priVerifFlag == 33) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._releasedetails.priVerifFlag == 22) {
                _this.btndisable = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._releasedetails.priVerifFlag == 8) {
                _this.disableSaveFlage = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._releasedetails.priVerifFlag == 24) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._releasedetails.priVerifFlag == 25) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._releasedetails.priVerifFlag == 34) {
                _this.btndisable = true;
                if (_this.userRoleID == 5) {
                    _this.disableVerifyCheckerFlag = true;
                    _this.disableRejectCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._releasedetails.priVerifFlag == 56) {
                _this.btndisable = true;
                if (_this.userRoleID == 5) {
                    _this.disableVerifyCheckerFlag = true;
                    _this.disableRejectCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (flag == 1) {
                _this.disbleflag = false;
                _this.btndisable = true;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = true;
            }
            if (flag == 0) {
                _this.disbleflag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = true;
            }
            if (_this._releasedetails.ifscCD != "" && _this._releasedetails.ifscCD != undefined) {
                _this.objBankService.getBankDetailsByIFSC(_this._releasedetails.ifscCD).subscribe(function (res) {
                    _this.objBankDetails = res;
                });
            }
            else {
                _this.objBankDetails.branchName = '';
                _this.objBankDetails.bankName = '';
                _this._releasedetails.ifscCD = '';
            }
            if (flag == 1) {
                _this.disbleflag = false;
                _this.btndisable = true;
            }
            if (flag == 0) {
                _this.disbleflag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = false;
            }
            if (flag == 2 && _this.userRoleID == 6 && _this._releasedetails.priVerifFlag == 34) {
                _this.disableVerifyCheckerFlag = true;
                _this.disableRejectCheckerFlag = true;
                _this.disbleflag = true;
                _this.btndisable = false;
            }
            if (flag == 1 && _this.userRoleID == 5 && _this._releasedetails.priVerifFlag == 56) {
                _this.disableVerifyCheckerFlag = true;
                _this.disableRejectCheckerFlag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = false;
                _this.disbleflag = false;
                _this.btndisable = false;
            }
        });
    };
    RelsHooLvlComponent.prototype.getBankDetailsByIFSC = function (ifscCD) {
        var _this = this;
        this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(function (res) {
            _this.objBankDetails = res;
        });
    };
    RelsHooLvlComponent.prototype.SetDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    RelsHooLvlComponent.prototype.deleteEmployeeById = function (id) {
        var _this = this;
        this.mode = 2;
        this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(function (data) {
            _this._releasedetails = data;
            if (_this._releasedetails != null) {
                _this.deletePopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this._Service.DdoId = _this.UserID;
                _this.BindGridLoanSanction(_this._Service.DdoId);
                _this.ReleaseForm.resetForm();
            }
        });
    };
    RelsHooLvlComponent.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    };
    RelsHooLvlComponent.prototype.forwarDDOMaker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._releasedetails.priVerifFlag = 34;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.ForwardToDDOMaker);
                _this.disableSaveFlage = false;
                _this.disableFwdMakerFlag = false;
                _this._Service.DdoId = _this.UserID;
                _this.BindGridLoanSanction(_this.UserID);
                _this.ReleaseForm.resetForm();
            });
        }
    };
    RelsHooLvlComponent.prototype.forwarDDOChecker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._releasedetails.priVerifFlag = 56;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.ForwardToDDOChecker);
                _this.disableSaveFlage = false;
                _this.disableFwdCheckerFlag = false;
                _this.BindGridLoanSanction(_this.UserID);
                _this.ReleaseForm.resetForm();
            });
        }
    };
    RelsHooLvlComponent.prototype.VerifyChecker = function () {
        var _this = this;
        if (this.userRoleID == 5) {
            this._releasedetails.priVerifFlag = 57;
        }
        this._releasedetails.msEmpId = this.tempId;
        this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(function (data) {
            _this._releasedetails = data;
            _this._Service.HiddenId = data;
            if (data == "-1") {
                _this.BindGridLoanSanction(_this.UserID);
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.alreadyExistMsg);
            }
            else {
                _this.BindGridLoanSanction(_this.UserID);
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.VerifyDDOChecker);
                _this.ReleaseForm.resetForm();
            }
        });
    };
    RelsHooLvlComponent.prototype.Reject = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 5;
            this.remarks = this._releasedetails.sanction_Remarks;
            if (this.userRoleID == 5) {
                this._releasedetails.priVerifFlag = 58;
            }
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.Rejected);
                _this.deletePopup = false;
                _this.BindGridLoanSanction(_this._Service.DdoId);
                _this.ReleaseForm.resetForm();
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('Release'),
        __metadata("design:type", Object)
    ], RelsHooLvlComponent.prototype, "ReleaseForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], RelsHooLvlComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RelsHooLvlComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"])('showhideinput'),
        __metadata("design:type", Object)
    ], RelsHooLvlComponent.prototype, "input", void 0);
    RelsHooLvlComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-rels-hoo-lvl',
            template: __webpack_require__(/*! ./rels-hoo-lvl.component.html */ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.html"),
            styles: [__webpack_require__(/*! ./rels-hoo-lvl.component.css */ "./src/app/loanmgt/rels-hoo-lvl/rels-hoo-lvl.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"], _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__["NewLoanService"], _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_7__["SanctionService"],
            _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_6__["CommanService"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__["BankDetailsService"]])
    ], RelsHooLvlComponent);
    return RelsHooLvlComponent;
}());



/***/ }),

/***/ "./src/app/loanmgt/sanction/sanction.component.css":
/*!*********************************************************!*\
  !*** ./src/app/loanmgt/sanction/sanction.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvYW5tZ3Qvc2FuY3Rpb24vc2FuY3Rpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/loanmgt/sanction/sanction.component.html":
/*!**********************************************************!*\
  !*** ./src/app/loanmgt/sanction/sanction.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [data]=\"callTypevar\"></app-comman-mst>-->\r\n\r\n<div *ngIf=\"userRoleID !=17\">\r\n  <app-comman-mst (onchange)=\"GetcommanMethod($event)\" [data]=\"callTypevar\" [events]=\"eventsSubject.asObservable()\"></app-comman-mst>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form (ngSubmit)=\"Sanction.valid && btnsubmit()\" #Sanction=\"ngForm\" novalidate>\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Details</div>\r\n      <div class=\"fom-title\">A.Loan Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Loan Type\" title=\"Loan Type\"\r\n                      [(ngModel)]=\"_sanctiondetails.payLoanRefLoanCD\" name=\"payLoanRefLoanCD\"\r\n                      #payLoanRefLoanCD=\"ngModel\" required [disabled]=disbleflag>\r\n            <mat-option label=\"Select Loan Type\">Select Loan Type</mat-option>\r\n            <mat-option *ngFor=\"let Loantype of ArrLoanType\" [value]=\"Loantype.payLoanRefLoanCD\">\r\n              {{Loantype.payLoanRefLoanShortDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span *ngIf=\"payLoanRefLoanCD.submitted && payLoanRefLoanCD.invalid\">Loan Type is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\"> B. Sanction Order Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput name=\"sancOrdNo\" title=\"Order No\" placeholder=\"Order No\" #sancOrdNo=\"ngModel\" (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_sanctiondetails.sancOrdNo\" BlockCopyPaste\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdNo.errors?.required\">Order Number is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker1\" placeholder=\"Order Date\" title=\"Order Date\"\r\n                 [(ngModel)]=\"_sanctiondetails.sancOrdDT\" (click)=\"picker1.open()\" name=\"sancOrdDT\" #sancOrdDT=\"ngModel\" [disabled]=disbleflag required BlockCopyPaste=\"enter\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker1></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!sancOrdDT.errors?.required\">Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">C. Sanctioned Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtSanc\" title=\"Amount Requested for Loan\" placeholder=\"Amount Requested for Loan (In Rs.)  \" (keypress)=\"numberOnly($event)\" #loanAmtSanc=\"ngModel\" [(ngModel)]=\"_sanctiondetails.loanAmtSanc\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtSanc.errors?.required\">Amount Requested is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"loanAmtDisbursed\" title=\"Total Sanctioned Amount\" placeholder=\"Total Sanctioned Amount (In Rs.) \" (blur)=\"valuechange($event.target.value)\" #loanAmtDisbursed=\"ngModel\" BlockCopyPaste (keypress)=\"numberOnly($event)\" [(ngModel)]=\"_sanctiondetails.loanAmtDisbursed\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!loanAmtDisbursed.errors?.required\">Sanctioned Amount is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12  col-lg-12\">\r\n        <mat-radio-group [(ngModel)]=\"_sanctiondetails.empApptType\" name=\"empApptType\" #empApptType=\"ngModel\" [disabled]=disbleflag required (click)=\"setempApptType(_sanctiondetails.empApptType)\">\r\n          <mat-radio-button value=\"R\">Employee</mat-radio-button>\r\n          <mat-radio-button value=\"T\">Vendor</mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"empName\" title=\"User Name\" placeholder=\"User Name\" #empName=\"ngModel\" [(ngModel)]=\"_sanctiondetails.empName\"\r\n                 [disabled]=disableUserBankDetailsFlag required>\r\n          <!--[disabled]=EmpTypeFlag required-->\r\n          <mat-error>\r\n            <span [hidden]=\"!empName.errors?.required\">In Favour is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput name=\"description\" title=\"Description\" placeholder=\"Description \" #description=\"ngModel\" [(ngModel)]=\"_sanctiondetails.description\"\r\n                 [disabled]=disbleflag required>\r\n          <mat-error>\r\n            <span [hidden]=\"!description.errors?.required\">Description is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Bank Details</div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"IFSC Code\" title=\"IFSC Code\" [(ngModel)]=\"_sanctiondetails.ifscCD\" name=\"ifscCD\" (blur)=\"getBankDetailsByIFSC(_sanctiondetails.ifscCD)\" [disabled]=disableUserBankDetailsFlag required>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Bank Name\" title=\"Bank Name\" [(ngModel)]=\"objBankDetails.bankName\" name=\"bankName\" #bankName=\"ngModel\" [disabled]=disableUserBankDetailsFlag required>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Branch Name\" title=\"Branch Name\" [(ngModel)]=\"objBankDetails.branchName\" name=\"branchName\" #branchName=\"ngModel\" [disabled]=disableUserBankDetailsFlag required>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Saving A/c No\" title=\"Saving A/c No\" [(ngModel)]=\"_sanctiondetails.bnkAcNo\" maxlength=\"15\" name=\"bnkAcNo\"\r\n                 (keypress)=\"numberOnly($event)\" [disabled]=disableUserBankDetailsFlag required>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\" *ngIf=\"_sanctiondetails.sanction_Remarks!=''\">\r\n\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_sanctiondetails.sanction_Remarks\" title=\"Remarks\" name=\"sanction_Remarks\" #sanction_Remarks=\"ngModel\" [disabled]=disbleflag></textarea>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" id=\"btn_save\" class=\"btn btn-primary\" *ngIf=\"btndisable\">\r\n          <i class=\"fa fa-floppy-o\"></i>{{buttonText}}\r\n        </button>\r\n      \r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetLoanDdetailsForm()\" *ngIf=\"btndisable\">Cancel</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarDDOMaker()\" *ngIf=\"disableFwdMakerFlag\">Forward to DDO Maker</button>\r\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"forwarDDOChecker()\" *ngIf=\"disableFwdCheckerFlag\">Forward to DDO Checker</button>\r\n        <button class=\"btn btn-success\" type=\"button\" (click)=\"VerifyChecker()\" *ngIf=\"disableVerifyCheckerFlag\">Verify</button>\r\n        <button class=\"btn btn-danger\" type=\"button\" *ngIf=\"disableRejectCheckerFlag\" (click)=\"deletePopup = !deletePopup\">Reject</button>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <ng-container matColumnDef=\"EMPId\" style=\"display:none;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header style=\"display:none;\"> ID </th>\r\n        <td mat-cell *matCellDef=\"let element\" style=\"display:none;\"> {{element.id}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"EMPCode\" style=\"width:120px;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> EMP Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"LoanCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Loan Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.loanCode}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PurposeCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Purpose  </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.purposeCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <a matTooltip=\"{{element.moduleStatus}}\"> {{element.status}}</a>\r\n\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a class=\"material-icons i-info\" matTooltip=\"view details\" (click)=\"ViewEditLoanDetailsById(element.id,0)\">error</a>\r\n          <span *ngIf=\"element.statusId == '23'  && userRoleID !=17\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"element.statusId == '34'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n\r\n          <span *ngIf=\"element.statusId == '32' \">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,1)\">edit</a>\r\n          </span>\r\n          <span *ngIf=\"userRoleID==5  && element.statusId == '35'\">\r\n            <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"ViewEditLoanDetailsById(element.id,2)\">edit</a>\r\n          </span>\r\n\r\n          <!--<span *ngIf=\"element.status == 'E'\">\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"SetDeleteId(element.id);deletepopup = !deletepopup\"> delete_forever </a>\r\n          </span>\r\n          <span *ngIf=\"element.status == 'ME'\">\r\n            <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"SetDeleteId(element.id);deletepopup = !deletepopup\"> delete_forever </a>\r\n          </span>-->\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Please Enter Remarks.</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remarks\"\r\n                    [(ngModel)]=\"_sanctiondetails.sanction_Remarks\" title=\"Remarks\" name=\"sanction_Remarks\" #sanction_Remarks=\"ngModel\" required></textarea>\r\n          <mat-error>\r\n            <span [hidden]=\"!sanction_Remarks.errors?.required\">Remarks is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"Reject()\">Ok</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletePopup = !deletePopup\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/loanmgt/sanction/sanction.component.ts":
/*!********************************************************!*\
  !*** ./src/app/loanmgt/sanction/sanction.component.ts ***!
  \********************************************************/
/*! exports provided: SanctionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SanctionComponent", function() { return SanctionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/loan-mgmt/new-loan.service */ "./src/app/services/loan-mgmt/new-loan.service.ts");
/* harmony import */ var _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/LoanModel/sanction.model */ "./src/app/model/LoanModel/sanction.model.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/empdetails/bank-details.service */ "./src/app/services/empdetails/bank-details.service.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
/* harmony import */ var _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/loan-mgmt/sanction.service */ "./src/app/services/loan-mgmt/sanction.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var SanctionComponent = /** @class */ (function () {
    function SanctionComponent(_msg, _Service, objCommanService, objSanctionService, objBankService) {
        this._msg = _msg;
        this._Service = _Service;
        this.objCommanService = objCommanService;
        this.objSanctionService = objSanctionService;
        this.objBankService = objBankService;
        this.eventsSubject = new rxjs__WEBPACK_IMPORTED_MODULE_9__["Subject"]();
        this.buttonText = 'Save';
        this.objBankDetails = {};
        this.displayedColumns = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
        this.selectedIndex = 0;
        this.btndisable = false;
        this.disbleflag = false;
        this.show = false;
        this.jwt = {};
        this.type = "password";
        this.disableSaveFlage = false;
        this.disableCancelFlage = false;
        this.disableFwdCheckerFlag = false;
        this.disableFwdMakerFlag = false;
        this.disableVerifyCheckerFlag = false;
        this.disableRejectCheckerFlag = false;
        this.disableUserBankDetailsFlag = false;
        this._sanctiondetails = new _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__["Sanction"]();
    }
    SanctionComponent.prototype.ngOnInit = function () {
        this.username = sessionStorage.getItem('username');
        this.ddoid = Number(sessionStorage.getItem('ddoid'));
        this.UserID = Number(sessionStorage.getItem('UserID'));
        this.userRole = sessionStorage.getItem('userRole');
        this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
        this.BindLoanType();
        this.BindGridLoanSanction(this.UserID);
    };
    SanctionComponent.prototype.GetcommanMethod = function (value) {
        this.getEmployeeDeatils(value, '0');
    };
    SanctionComponent.prototype.BindLoanType = function () {
        var _this = this;
        this.loantypeFlag = '2';
        this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(function (data) {
            _this.ArrLoanType = data;
        });
    };
    SanctionComponent.prototype.getEmployeeDeatils = function (EmpCode, MsDesignID) {
        var _this = this;
        this.mode = 1;
        this._sanctiondetails.payLoanRefLoanCD = 0;
        this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(function (data) {
            _this._sanctiondetails = data[0];
            if (_this._sanctiondetails != undefined) {
                _this.getBankDetailsByIFSC(_this._sanctiondetails.ifscCD);
            }
            if (_this._sanctiondetails == undefined) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.noRecordMsg);
                _this._sanctiondetails = new _model_LoanModel_sanction_model__WEBPACK_IMPORTED_MODULE_3__["Sanction"]();
            }
            if (_this._sanctiondetails.empApptType == 'R') {
                _this.EmpTypeFlag = true;
                _this.disableUserBankDetailsFlag = false;
            }
            else {
                _this.EmpTypeFlag = false;
            }
        });
    };
    SanctionComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    SanctionComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    SanctionComponent.prototype.btnsubmit = function () {
        var _this = this;
        if (this.buttonText == 'Save') {
            if (this.userRoleID == 6) {
                this.disableFwdCheckerFlag = true;
                this._sanctiondetails.priVerifFlag = 34;
            }
            if (this.userRoleID == 5) {
                this.disableFwdCheckerFlag = true;
                this._sanctiondetails.priVerifFlag = 35;
            }
            this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(function (data) {
                _this._sanctiondetails = data;
                _this._Service.HiddenId = data;
                if (data == "-1") {
                    _this.BindGridLoanSanction(_this.UserID);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.alreadyExistMsg);
                }
                else {
                    _this.BindGridLoanSanction(_this.UserID);
                    sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.saveMsg);
                }
            });
            this.SanctionForm.resetForm();
        }
        if (this.buttonText == 'Update') {
            if (this.userRoleID == 6) {
                this.disableFwdCheckerFlag = true;
                this._sanctiondetails.priVerifFlag = 34;
            }
            if (this.userRoleID == 5) {
                this.disableFwdCheckerFlag = true;
                this._sanctiondetails.priVerifFlag = 35;
            }
            this._sanctiondetails.msEmpId = this.tempId;
            this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(function (data) {
                _this._sanctiondetails = data;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.updateMsg);
                _this.BindGridLoanSanction(_this.UserID);
                _this.SanctionForm.resetForm();
            });
        }
    };
    SanctionComponent.prototype.resetLoanDdetailsForm = function () {
        debugger;
        this.SanctionForm.resetForm();
        this.eventsSubject.next(this.data);
    };
    SanctionComponent.prototype.BindGridLoanSanction = function (value) {
        var _this = this;
        this.mode = 3; //loan sanction and release details(HOO Label) ,select data in the table
        this.UserID = value;
        this._Service.BindLoanStatus(this.mode, this.UserID).subscribe(function (data) {
            _this.ArrSanction = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
    };
    SanctionComponent.prototype.valuechange = function (value) {
        if (this._sanctiondetails.loanAmtSanc >= value) {
            this._sanctiondetails.loanAmtDisbursed;
        }
        else {
            this._sanctiondetails.loanAmtDisbursed;
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(this._msg.AmountShouldbelessthanRequestedAmount);
        }
    };
    SanctionComponent.prototype.ViewEditLoanDetailsById = function (id, flag) {
        var _this = this;
        this.mode = 3;
        this.tempId = id;
        this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(function (data) {
            _this._sanctiondetails = data[0];
            _this._sanctiondetails.ifscCD;
            _this.getBankDetailsByIFSC(_this._sanctiondetails.ifscCD);
            _this.buttonText = "Update";
            if (_this._sanctiondetails.priVerifFlag == 23) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 35) {
                _this.btndisable = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdMakerFlag = false;
                    _this.disableFwdCheckerFlag = false;
                }
                else {
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 31) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 33) {
                _this.btndisable = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 22) {
                _this.btndisable = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 8) {
                _this.disableSaveFlage = true;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 24) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 25) {
                _this.disableSaveFlage = false;
                if (_this.userRoleID == 6) {
                    _this.disableFwdCheckerFlag = true;
                }
                else {
                    _this.disableFwdCheckerFlag = false;
                    _this.disableFwdMakerFlag = true;
                }
            }
            if (_this._sanctiondetails.priVerifFlag == 34) {
                _this.btndisable = true;
                if (_this.userRoleID == 5) {
                    _this.disableVerifyCheckerFlag = true;
                    _this.disableRejectCheckerFlag = true;
                }
                else {
                    _this.disableFwdMakerFlag = false;
                }
            }
            if (_this._sanctiondetails.empApptType == 'R' || _this._sanctiondetails.empApptType == 'T') {
                _this.EmpTypeFlag = true;
                _this.disableUserBankDetailsFlag = true;
            }
            else {
                _this.EmpTypeFlag = true;
                _this.disableUserBankDetailsFlag = false;
            }
            if (flag == 1) {
                _this.disbleflag = false;
                _this.btndisable = true;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = true;
            }
            if (flag == 0) {
                _this.disbleflag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = true;
            }
            if (_this._sanctiondetails.ifscCD != "" && _this._sanctiondetails.ifscCD != undefined) {
                _this.objBankService.getBankDetailsByIFSC(_this._sanctiondetails.ifscCD).subscribe(function (res) {
                    _this.objBankDetails = res;
                });
            }
            else {
                _this.objBankDetails.branchName = '';
                _this.objBankDetails.bankName = '';
                _this._sanctiondetails.ifscCD = '';
            }
            if (flag == 2 && _this.userRoleID == 6 && _this._sanctiondetails.priVerifFlag == 34) {
                _this.disableVerifyCheckerFlag = true;
                _this.disableRejectCheckerFlag = true;
                _this.disbleflag = true;
                _this.btndisable = false;
            }
            if (flag == 2 && _this.userRoleID == 5 && _this._sanctiondetails.priVerifFlag == 35) {
                _this.disableVerifyCheckerFlag = true;
                _this.disableRejectCheckerFlag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = false;
            }
            if (flag == 1 && _this.userRoleID == 6) {
                _this.disbleflag = false;
                _this.btndisable = true;
            }
            else if (flag == 1 && _this.userRoleID == 5) {
                _this.disbleflag = false;
                _this.btndisable = false;
                _this.disableFwdCheckerFlag = false;
            }
            if (flag == 0) {
                _this.disbleflag = true;
                _this.btndisable = false;
                _this.disableFwdMakerFlag = false;
                _this.disableFwdCheckerFlag = false;
                _this.disableUserBankDetailsFlag = true;
            }
        });
    };
    SanctionComponent.prototype.getBankDetailsByIFSC = function (ifscCD) {
        var _this = this;
        this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(function (res) {
            _this.objBankDetails = res;
        });
    };
    SanctionComponent.prototype.SetDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    SanctionComponent.prototype.deleteEmployeeById = function (id) {
        var _this = this;
        this.mode = 2;
        this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(function (data) {
            _this._sanctiondetails = data;
            if (_this._sanctiondetails != null) {
                _this.deletePopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.deleteMsg);
                _this.BindGridLoanSanction(_this.UserID);
                _this.resetLoanDdetailsForm();
            }
        });
    };
    SanctionComponent.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    };
    SanctionComponent.prototype.forwarDDOMaker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._sanctiondetails.priVerifFlag = 34;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.ForwardToDDOMaker);
                _this.disableSaveFlage = false;
                _this.disableFwdMakerFlag = false;
                _this.BindGridLoanSanction(_this.UserID);
                _this.SanctionForm.resetForm();
            });
        }
    };
    SanctionComponent.prototype.forwarDDOChecker = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 2;
            this._sanctiondetails.priVerifFlag = 35;
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.ForwardToDDOChecker);
                _this.disableSaveFlage = false;
                _this.disableFwdCheckerFlag = false;
                _this.BindGridLoanSanction(_this.UserID);
                _this.SanctionForm.resetForm();
            });
        }
    };
    SanctionComponent.prototype.VerifyChecker = function () {
        var _this = this;
        this._sanctiondetails.priVerifFlag = 54;
        this._sanctiondetails.msEmpId = this.tempId;
        this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(function (data) {
            _this._sanctiondetails = data;
            _this._Service.HiddenId = data;
            if (data == "-1") {
                _this.BindGridLoanSanction(_this.UserID);
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.alreadyExistMsg);
            }
            else {
                _this.BindGridLoanSanction(_this.UserID);
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.VerifyDDOChecker);
            }
        });
    };
    SanctionComponent.prototype.Cancel = function () {
        this.deletePopup = false;
    };
    SanctionComponent.prototype.Reject = function () {
        var _this = this;
        if (this._Service.HiddenId != null) {
        }
        if (this.tempId != null) {
            this.mode = 4;
            this.remarks = this._sanctiondetails.sanction_Remarks;
            if (this.userRoleID == 5) {
                this._sanctiondetails.priVerifFlag = 32;
            }
            else {
                this._sanctiondetails.priVerifFlag = 35;
            }
            this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(function (data) {
                _this.flag = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()(_this._msg.Rejected);
                _this.deletePopup = false;
            });
        }
    };
    SanctionComponent.prototype.setempApptType = function (value) {
        if (value == "R") {
            this.disableUserBankDetailsFlag = false;
        }
        if (value == "T") {
            this.disableUserBankDetailsFlag = true;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('Sanction'),
        __metadata("design:type", Object)
    ], SanctionComponent.prototype, "SanctionForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], SanctionComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], SanctionComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"])('showhideinput'),
        __metadata("design:type", Object)
    ], SanctionComponent.prototype, "input", void 0);
    SanctionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sanction',
            template: __webpack_require__(/*! ./sanction.component.html */ "./src/app/loanmgt/sanction/sanction.component.html"),
            styles: [__webpack_require__(/*! ./sanction.component.css */ "./src/app/loanmgt/sanction/sanction.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"], _services_loan_mgmt_new_loan_service__WEBPACK_IMPORTED_MODULE_2__["NewLoanService"], _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_6__["CommanService"],
            _services_loan_mgmt_sanction_service__WEBPACK_IMPORTED_MODULE_7__["SanctionService"], _services_empdetails_bank_details_service__WEBPACK_IMPORTED_MODULE_5__["BankDetailsService"]])
    ], SanctionComponent);
    return SanctionComponent;
}());



/***/ }),

/***/ "./src/app/model/LoanModel/EmpSanctionModel.ts":
/*!*****************************************************!*\
  !*** ./src/app/model/LoanModel/EmpSanctionModel.ts ***!
  \*****************************************************/
/*! exports provided: EmpSanction, EmpSancModel, InstEmpModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpSanction", function() { return EmpSanction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpSancModel", function() { return EmpSancModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstEmpModel", function() { return InstEmpModel; });
var EmpSanction = /** @class */ (function () {
    function EmpSanction() {
    }
    return EmpSanction;
}());

var EmpSancModel = /** @class */ (function () {
    function EmpSancModel() {
    }
    return EmpSancModel;
}());

var InstEmpModel = /** @class */ (function () {
    function InstEmpModel() {
    }
    return InstEmpModel;
}());



/***/ }),

/***/ "./src/app/model/LoanModel/existloanModel.ts":
/*!***************************************************!*\
  !*** ./src/app/model/LoanModel/existloanModel.ts ***!
  \***************************************************/
/*! exports provided: ExistloanModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExistloanModel", function() { return ExistloanModel; });
var ExistloanModel = /** @class */ (function () {
    function ExistloanModel() {
    }
    return ExistloanModel;
}());

//export interface PeriodicElement {
//  position: number;
//  EMPCode: string;
//  name: string;
//  status: string;
//  symbol: string
//}


/***/ }),

/***/ "./src/app/model/LoanModel/floodModel.ts":
/*!***********************************************!*\
  !*** ./src/app/model/LoanModel/floodModel.ts ***!
  \***********************************************/
/*! exports provided: FloodModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FloodModel", function() { return FloodModel; });
var FloodModel = /** @class */ (function () {
    function FloodModel() {
        this.totalEmpcode = [];
    }
    return FloodModel;
}());



/***/ }),

/***/ "./src/app/model/LoanModel/new-loan-model.ts":
/*!***************************************************!*\
  !*** ./src/app/model/LoanModel/new-loan-model.ts ***!
  \***************************************************/
/*! exports provided: NewLoanModel, employee */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewLoanModel", function() { return NewLoanModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employee", function() { return employee; });
var NewLoanModel = /** @class */ (function () {
    function NewLoanModel() {
        this.permDdoId = 0;
        this.createdBy = '';
        this.pay_basic = '';
        this.loanAmtSanc = 0;
        this.priInstAmt = 0;
        this.intTotInst = 0;
        this.oddInstNoInt = 0;
        this.oddInstAmtInt = 0;
        this.payLoanRefLoanCD = '';
        this.payLoanPurposeCode = '';
        this.msEmpId = 0;
        this.tot_Plint_Area = 0;
        this.payLoanPurposeID = 0;
        this.anti_Price_Pc = '';
        this.priVerifFlag = 0;
        this.adv_Amount = 0;
        this.upload_Sign = '';
        this.address = '';
        this.area_Sq_Ft = 0;
        this.desNoInsRePaidForAdv = 0;
        this.cost = 0;
        this.amt_Act_Pay = 0;
        this.flr_Area = 0;
        this.est_Cost = 0;
        this.amt_Adv_Req = 0;
        this.num_Inst = 0;
        this.pur_Area = 0;
        this.plth_Prop_Enlarge = 0;
        this.coc = 0;
        this.cop_Enlarge = 0;
        this.tot_Cost = 0;
        this.aOD_Req = 0;
        this.prc_Settled = 0;
        this.amt_Paid = 0;
        this.bankName = '';
        this.branchName = '';
    }
    return NewLoanModel;
}());

var employee = /** @class */ (function () {
    function employee() {
        this.userid = 0;
        this.roleid = 0;
        this.ddoid = 0;
    }
    return employee;
}());



/***/ }),

/***/ "./src/app/model/LoanModel/propertyownerdetails.ts":
/*!*********************************************************!*\
  !*** ./src/app/model/LoanModel/propertyownerdetails.ts ***!
  \*********************************************************/
/*! exports provided: PropOwnerDetlsModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropOwnerDetlsModel", function() { return PropOwnerDetlsModel; });
var PropOwnerDetlsModel = /** @class */ (function () {
    function PropOwnerDetlsModel() {
    }
    return PropOwnerDetlsModel;
}());



/***/ }),

/***/ "./src/app/model/LoanModel/sanction.model.ts":
/*!***************************************************!*\
  !*** ./src/app/model/LoanModel/sanction.model.ts ***!
  \***************************************************/
/*! exports provided: Sanction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sanction", function() { return Sanction; });
var Sanction = /** @class */ (function () {
    function Sanction() {
        this.payLoanRefLoanCD = 0;
        this.sanction_Remarks = '';
        this.release_Remarks = '';
    }
    return Sanction;
}());



/***/ }),

/***/ "./src/app/model/newloanModel.ts":
/*!***************************************!*\
  !*** ./src/app/model/newloanModel.ts ***!
  \***************************************/
/*! exports provided: newloanModel, employee */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newloanModel", function() { return newloanModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employee", function() { return employee; });
var newloanModel = /** @class */ (function () {
    function newloanModel() {
        this.permDdoId = 0;
        this.createdby = '';
        this.pay_basic = 0;
        this.loanAmtSanc = 0;
        this.priInstAmt = 0;
        this.intTotInst = 0;
        this.oddInstNoInt = 0;
        this.oddInstAmtInt = 0;
        this.payLoanRefLoanCD = 0;
        this.payLoanPurposeCode = 0;
        this.msEmpId = 0;
        this.tot_Plint_Area = 0;
        this.payLoanPurposeID = 0;
        this.anti_Price_Pc = '0';
        this.priVerifFlag = 0;
        this.adv_Amount = 0;
        this.area_Sq_Ft = 0;
        this.desNoInsRePaidForAdv = 0;
        this.cost = 0;
        this.amt_Act_Pay = 0;
        this.flr_Area = 0;
        this.est_Cost = 0;
        this.amt_Adv_Req = 0;
        this.num_Inst = 0;
        this.pur_Area = 0;
        this.plth_Prop_Enlarge = 0;
        this.coc = 0;
        this.cop_Enlarge = 0;
        this.tot_Cost = 0;
        this.aOD_Req = 0;
        this.prc_Settled = 0;
        this.amt_Paid = 0;
        this.bankName = '';
        this.branchName = '';
    }
    return newloanModel;
}());

var employee = /** @class */ (function () {
    function employee() {
        this.userid = 0;
        this.roleid = 0;
        this.ddoid = 0;
    }
    return employee;
}());



/***/ }),

/***/ "./src/app/services/empdetails/bank-details.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/empdetails/bank-details.service.ts ***!
  \*************************************************************/
/*! exports provided: BankDetailsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankDetailsService", function() { return BankDetailsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var BankDetailsService = /** @class */ (function () {
    function BankDetailsService(http, config) {
        this.http = http;
        this.config = config;
    }
    BankDetailsService.prototype.getBankDetails = function (empcode, roleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCd', empcode).set('RoleId', roleId);
        return this.http.get("" + this.config.api_base_url + this.config.getBankDetails, { params: params });
    };
    BankDetailsService.prototype.getBankDetailsByIFSC = function (ifscCD) {
        // tslint:disable-next-line:max-line-length
        return this.http.get("" + this.config.api_base_url + (this.config.getBankDetailsByIFSC + '?ifscCD=' + ifscCD));
    };
    BankDetailsService.prototype.getAllIFSC = function () {
        // tslint:disable-next-line:max-line-length
        return this.http.get("" + this.config.api_base_url + this.config.getAllIFSC);
    };
    BankDetailsService.prototype.UpdateBankDetails = function (objBankDetails) {
        return this.http.post(this.config.api_base_url + this.config.UpdateBankDetails, objBankDetails, { responseType: 'text' });
    };
    BankDetailsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], BankDetailsService);
    return BankDetailsService;
}());



/***/ }),

/***/ "./src/app/services/empdetails/empdetails.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/empdetails/empdetails.service.ts ***!
  \***********************************************************/
/*! exports provided: EmpdetailsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpdetailsService", function() { return EmpdetailsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var EmpdetailsService = /** @class */ (function () {
    function EmpdetailsService(http, config) {
        this.http = http;
        this.config = config;
    }
    //private handleError(err) {
    //  let body;
    //  if (err instanceof Response) {
    //    body = err.json() || '';
    //  }
    //  return Observable.throw(body.error['message']);
    //}
    EmpdetailsService.prototype.GetVerifyEmpCode = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getverifiedEmpCode);
    };
    EmpdetailsService.prototype.GetEmpPersonalDetailsByID = function (EmpCd, roleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCd', EmpCd).set('RoleId', roleId);
        return this.http.get("" + this.config.api_base_url + this.config.GetEmpPersonalDetailsByID, { params: params });
    };
    EmpdetailsService.prototype.UpdateEmpDetails = function (empdetails) {
        return this.http.post(this.config.api_base_url + this.config.UpdateEmpDetails, empdetails, { responseType: 'text' });
    };
    EmpdetailsService.prototype.getMakerEmpList = function (roleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('roleId', roleId);
        return this.http.get("" + this.config.api_base_url + this.config.getMakerEmpList, { params: params });
    };
    EmpdetailsService.prototype.InsertUpdateEmpDetails = function (empdetails) {
        return this.http.post(this.config.api_base_url + this.config.InsertUpdateEmployeeDetails, empdetails);
    };
    EmpdetailsService.prototype.GetEmpPHDetails = function (EmpCd) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.GetEmpPHDetails + EmpCd));
    };
    EmpdetailsService.prototype.GetPhysicalDisabilityTypes = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetPhysicalDisabilityTypes);
    };
    EmpdetailsService.prototype.SavePHDetails = function (empPHDetails) {
        return this.http.post(this.config.api_base_url + this.config.SavePHDetails, empPHDetails, { responseType: 'text' });
    };
    EmpdetailsService.prototype.DeleteEmpDetails = function (EmpCd) {
        return this.http.post(this.config.api_base_url + 'EmployeeDetails/DeleteEmployeeDetails?EmpCd=' + EmpCd, { responseType: 'text' });
        // .get<EmployeeDetailsModel>(`${this.config.api_base_url}${this.config.GetEmpPHDetails + EmpCd}`);
    };
    EmpdetailsService.prototype.getdownloadDetails = function (url) {
        debugger;
        return this.http.get(this.config.api_base_url + 'EmployeeDetails/DownloadFile?url=' + url, { responseType: 'arraybuffer' });
    };
    EmpdetailsService.prototype.GetIsDeputEmp = function (EmpCd) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCd', EmpCd);
        return this.http
            .get("" + this.config.api_base_url + this.config.GetIsDeputEmp, { params: params });
    };
    EmpdetailsService.prototype.VerifyRejectionEmpDetails = function (empdetails) {
        return this.http.post(this.config.api_base_url + 'EmployeeDetails/VerifyRejectionEmployeeDtl', empdetails);
    };
    EmpdetailsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], EmpdetailsService);
    return EmpdetailsService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/exist-loan.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/loan-mgmt/exist-loan.service.ts ***!
  \**********************************************************/
/*! exports provided: ExistLoanService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExistLoanService", function() { return ExistLoanService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ExistLoanService = /** @class */ (function () {
    function ExistLoanService(http, config) {
        this.http = http;
        this.config = config;
    }
    Object.defineProperty(ExistLoanService.prototype, "Empcode", {
        get: function () {
            return this._id;
        },
        set: function (val) {
            this._id = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistLoanService.prototype, "RoleId", {
        get: function () {
            return this._roleid;
        },
        set: function (val) {
            this._roleid = val;
        },
        enumerable: true,
        configurable: true
    });
    ExistLoanService.prototype.BindDropDownDesign = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownDesign);
    };
    ExistLoanService.prototype.BindDropDownBillGroup = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownBillGroup);
    };
    ExistLoanService.prototype.BindDropDownEmp = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownEmp);
    };
    ExistLoanService.prototype.BindLoanType = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getExistingLoanType);
    };
    ExistLoanService.prototype.BindHeadAccount = function (LoanCD) {
        return this.http.get("" + this.config.api_base_url + (this.config.getBindHeadAccount + LoanCD));
    };
    ExistLoanService.prototype.GetExistLoanDetailsByID = function (MSPayAdvEmpdetID) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.GetExistLoanDetailsByID + MSPayAdvEmpdetID));
    };
    ExistLoanService.prototype.PostData = function (ArrLoanSection) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.ExistingLoanInsertData, ArrLoanSection);
    };
    ExistLoanService.prototype.EmployeeInfo = function (MsDesignID) {
        return this.http.get("" + this.config.api_base_url + (this.config.getAllEmpNameSelectedDesignId + MsDesignID));
    };
    ExistLoanService.prototype.LoanSection = function (ddoid, EmpCode) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCode', EmpCode).set('PermDdoId', ddoid.toString());
        return this.http.get("" + this.config.api_base_url + this.config.getLoanAdvanceDetails + "/LoanAdvanceDetails", { params: params });
        //return this.http.get<any>(`${this.config.api_base_url}${this.config.getLoanAdvanceDetails + ddoid +'&EmpCode=' + EmpCode}`);
    };
    // For GetList Data
    ExistLoanService.prototype.GetEmpLoanDetails = function (PayBillGpID) {
        return this.http.get("" + this.config.api_base_url + this.config.GetExistingEmpLoanDetails);
    };
    ExistLoanService.prototype.BindExistingLoanStatus = function (ddoId, UserID, userRoleID) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('UserID', UserID).set('userRoleID', userRoleID).set('ddoId', ddoId.toString());
        // return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus + ddoId + '&UserID=' + UserID }`);
        return this.http.get("" + this.config.api_base_url + this.config.ExistingLoanStatus, { params: params });
    };
    //BindExistingLoanStatus(userid: string): Observable<any> {
    //  const params = new HttpParams().set('Userid', userid);
    //  return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus}/BindExistingLoanStatus`, { params });
    //}
    ExistLoanService.prototype.ExistingLoanForwordToDDO = function (ArrLoanSection) {
        return this.http.post(this.config.api_base_url + this.config.ExistingLoanForwordToDDO, ArrLoanSection);
    };
    ExistLoanService.prototype.PopulateEmpData = function (desigId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getBindHeadAccount + desigId));
    };
    ExistLoanService.prototype.DeleteEmpDetails = function (CtrlCode, msPayAdvEmpdetID, Mode) {
        debugger;
        //return this.http.get(this.config.ViewEditOrDeletePrao + "?CtrlCode=" + CtrlCode + "&controllerID=" + controllerID + "&Mode=" + Mode, {});
        //return this.http.post(this.config.api_base_url + this.config.ViewEditOrDeletePrao, PraoModel);
        return this.http.get(this.config.DeleteEmpDetails + "?CtrlCode=" + CtrlCode + "&msPayAdvEmpdetID=" + msPayAdvEmpdetID + "&Mode=" + Mode, {});
    };
    ExistLoanService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ExistLoanService);
    return ExistLoanService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/flood-service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/loan-mgmt/flood-service.ts ***!
  \*****************************************************/
/*! exports provided: FloodService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FloodService", function() { return FloodService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var FloodService = /** @class */ (function () {
    function FloodService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
        //this.BaseUrl = 'http://localhost:55424/api/';
    }
    //constructor(private http: HttpClient,
    //  @Inject(APP_CONFIG) private config: AppConfig
    //) { }
    //GetEmpDetails(BillGrID: string, desigId: string, UserID:number ): Observable<any> {
    //  return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus + BillGrID + desigId + UserID}`);
    //}
    FloodService.prototype.GetEmpDetails = function (BillGrID, desigId, UserID, mode) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('BillGrID', BillGrID).set('desigId', desigId).set('UserID', UserID).set('mode', mode);
        return this.httpclient.get(this.config.FloodController + "/GetEmpDetails", { params: params });
        //return null;
    };
    //InsertFloodData(ArrLoanData) {
    //  debugger;
    //  const params = new HttpParams().set('ArrLoanData', ArrLoanData);
    //  return this.httpclient.post<any>(`${this.config.FloodController}/InsertFloodData`, { params });
    //}
    FloodService.prototype.InsertFloodData = function (ArrLoanData) {
        debugger;
        //const paramtr = new HttpParams().set('EmployeeCode', EmployeeCode);
        // const params = new HttpParams().set('EmployeeCode', EmployeeCode);
        return this.httpclient.post(this.config.FloodController + "/InsertFloodData", ArrLoanData, { responseType: 'text' });
    };
    FloodService.prototype.ForwordToDdo = function (ArrLoanData) {
        debugger;
        //const paramtr = new HttpParams().set('EmployeeCode', EmployeeCode);
        // const params = new HttpParams().set('EmployeeCode', EmployeeCode);
        return this.httpclient.post(this.config.FloodController + "/ForwordToDdo", ArrLoanData, { responseType: 'text' });
    };
    FloodService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], FloodService);
    return FloodService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/inst-recovery-service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/loan-mgmt/inst-recovery-service.ts ***!
  \*************************************************************/
/*! exports provided: InstRecoveryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstRecoveryService", function() { return InstRecoveryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var InstRecoveryService = /** @class */ (function () {
    function InstRecoveryService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
        //this.BaseUrl = 'http://localhost:55424/api/';
    }
    InstRecoveryService.prototype.GetRecoveryEmpDetails = function (value) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCd', value);
        return this.httpclient.get(this.config.MultipleInstRecoveryController + "/GetRecoveryEmpDetails", { params: params });
    };
    InstRecoveryService.prototype.BindSanctionDetails = function (value, EmpCD) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('LoanCd', value).set('EmpCD', EmpCD);
        return this.httpclient.get(this.config.MultipleInstRecoveryController + "/BindSanctionDetails", { params: params });
    };
    InstRecoveryService.prototype.SaveData = function (EmpSanc) {
        debugger;
        return this.httpclient.post(this.config.MultipleInstRecoveryController + "/SaveSanctionData", EmpSanc);
    };
    InstRecoveryService.prototype.ForwardtoDDOChecker = function (EmpSanc) {
        return this.httpclient.post(this.config.MultipleInstRecoveryController + "/ForwardtoDDOChecker", EmpSanc);
    };
    InstRecoveryService.prototype.GetInstEmpDetails = function (EmpCd) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCD', EmpCd);
        return this.httpclient.get(this.config.MultipleInstRecoveryController + "/GetInstEmpDetails", { params: params });
    };
    InstRecoveryService.prototype.Accept = function (EmpCD) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('EmpCD', EmpCD);
        //return this.httpclient.post(`${this.config.MultipleInstRecoveryController}/Accept`, EmpCD, { responseType:'text' });
        return this.httpclient.get(this.config.MultipleInstRecoveryController + "/Accept", { params: params });
    };
    InstRecoveryService.prototype.Reject = function (EmpCD) {
        return this.httpclient.post(this.config.MultipleInstRecoveryController + "/Reject", EmpCD);
    };
    InstRecoveryService.prototype.EditLoanDetailsById = function (InstEmpDetais) {
        //const params = new HttpParams().set('msPayAdvEmpdetID', msPayAdvEmpdetID).set('EmpCd', EmpCd);
        return this.httpclient.post(this.config.MultipleInstRecoveryController + "/EditLoanDetailsById", InstEmpDetais);
    };
    InstRecoveryService.prototype.DeleteLoanDetailsById = function (msEmpLoanMultiInstId, empCD) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('msEmpLoanMultiInstId', msEmpLoanMultiInstId).set('empCD', empCD);
        return this.httpclient.delete(this.config.MultipleInstRecoveryController + "/DeleteLoanDetailsById", { params: params });
    };
    InstRecoveryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], InstRecoveryService);
    return InstRecoveryService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/new-loan.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/loan-mgmt/new-loan.service.ts ***!
  \********************************************************/
/*! exports provided: NewLoanService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewLoanService", function() { return NewLoanService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var NewLoanService = /** @class */ (function () {
    function NewLoanService(http, config) {
        this.http = http;
        this.config = config;
    }
    Object.defineProperty(NewLoanService.prototype, "list", {
        get: function () {
            return this._list;
        },
        set: function (val) {
            this._list = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewLoanService.prototype, "LoanCode", {
        get: function () {
            return this._id;
        },
        set: function (val) {
            this._id = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewLoanService.prototype, "PurposeCode", {
        get: function () {
            return this._type;
        },
        set: function (val) {
            this._type = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewLoanService.prototype, "RoleId", {
        get: function () {
            return this._roleid;
        },
        set: function (val) {
            this._roleid = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewLoanService.prototype, "DdoId", {
        get: function () {
            return this._ddoid;
        },
        set: function (val) {
            this._ddoid = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewLoanService.prototype, "HiddenId", {
        get: function () {
            return this._hiddenid;
        },
        set: function (val) {
            this._hiddenid = val;
        },
        enumerable: true,
        configurable: true
    });
    NewLoanService.prototype.BindLoanStatus = function (mode, userid) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetLoanDetails + mode + '&userid=' + userid));
    };
    NewLoanService.prototype.FwdtochkerDetails = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetFwdtochkerDetails);
    };
    NewLoanService.prototype.GetEmpLoanDetailsByID = function (EmpCd) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.GetEmpLoanDetailsByID + EmpCd));
    };
    NewLoanService.prototype.EditLoanDetailsByEMPID = function (id) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.EditLoanDetailsByEMPID + id));
    };
    NewLoanService.prototype.UpdateLoanDetailsbyID = function (id, mode, remarks, priVerifFlag) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.UpdateLoanDetailsbyID + id + '&mode=' + mode + '&remarks=' + remarks + '&priVerifFlag=' + priVerifFlag));
    };
    //DeleteLoanDetailsByEMPID(id: number, mode: number): Observable<any> {
    //  return this.http
    //    .get<any>(`${this.config.api_base_url}${this.config.DeleteLoanDetailsByEMPID + id + '&mode=' + mode}`);
    //}
    NewLoanService.prototype.EMPLoanPurposeByLoanCode = function (payLoanCode, empCode) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetLoanpurposebyLoanCode + payLoanCode + '&empCode=' + empCode));
    };
    NewLoanService.prototype.EMPLoanPurposeCode = function (payLoanCode, purposecode) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetDetailsbyLoanCodePurposecode + payLoanCode + '&purposecode=' + purposecode));
    };
    NewLoanService.prototype.EmployeeName = function (MsDesignID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetAllEmpNameSelectedByDesignId + MsDesignID));
    };
    NewLoanService.prototype.EmployeeInfo = function (EmpCode, MsDesignID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetEmpDetailsbyEmpcode + EmpCode + '&MsDesignID=' + MsDesignID));
    };
    NewLoanService.prototype.Loantypeandpurposestatus = function (LoanBillID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetPayLoanPurposeStatus + LoanBillID));
    };
    NewLoanService.prototype.EmployeeSaveInfo = function (formData) {
        return this.http.post(this.config.api_base_url + this.config.PostLoanDetails, formData, { responseType: 'text' });
    };
    NewLoanService.prototype.PropertySaveInfo = function (formData) {
        return this.http.post(this.config.api_base_url + this.config.PostPropertyOwnerDetails, formData, { responseType: 'text' });
    };
    NewLoanService.prototype.EmployeeUpdateInfo = function (employeedetails) {
        return this.http.put(this.config.api_base_url + this.config.UpdateLoanDetails, employeedetails, { responseType: 'text' });
    };
    NewLoanService.prototype.ValidateCoolingPeriod = function (LoanCode, PurposeCode, EmpCd) {
        return this.http.get("" + this.config.api_base_url + (this.config.ValidateCoolingPeriod + LoanCode + '&PurposeCode=' + PurposeCode + '&EmpCd=' + EmpCd));
    };
    NewLoanService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], NewLoanService);
    return NewLoanService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/recovery-challan.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/loan-mgmt/recovery-challan.service.ts ***!
  \****************************************************************/
/*! exports provided: RecoveryChallanService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoveryChallanService", function() { return RecoveryChallanService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var RecoveryChallanService = /** @class */ (function () {
    function RecoveryChallanService(http, config) {
        this.http = http;
        this.config = config;
    }
    RecoveryChallanService.prototype.RecoveryChallan = function (EmpCode, MsDesignID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetRecoveryChallan + EmpCode + '&MsDesignID=' + MsDesignID));
    };
    //Insert Update recovery challan
    RecoveryChallanService.prototype.InsertUpateRecoveryChallan = function (recoveryChallanDetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateRecoveryChallan, recoveryChallanDetails, { responseType: 'text' });
    };
    RecoveryChallanService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], RecoveryChallanService);
    return RecoveryChallanService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/sanction.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/loan-mgmt/sanction.service.ts ***!
  \********************************************************/
/*! exports provided: SanctionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SanctionService", function() { return SanctionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var SanctionService = /** @class */ (function () {
    function SanctionService(http, config) {
        this.http = http;
        this.config = config;
    }
    SanctionService.prototype.UpdateLoanSanctionDetails = function (loandetails) {
        return this.http.put(this.config.api_base_url + this.config.UpdateSanctionDetails, loandetails, { responseType: 'text' });
    };
    SanctionService.prototype.GetSanctionDetails = function (EmpCode, MsDesignID, mode) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetSanctionDetailsbyEmpcode + EmpCode + '&MsDesignID=' + MsDesignID + '&mode=' + mode));
    };
    SanctionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], SanctionService);
    return SanctionService;
}());



/***/ }),

/***/ "./src/app/services/master/master.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/master/master.service.ts ***!
  \***************************************************/
/*! exports provided: MasterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MasterService", function() { return MasterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var MasterService = /** @class */ (function () {
    function MasterService(http, config) {
        this.http = http;
        this.config = config;
    }
    MasterService.prototype.getSalutation = function () {
        return this.http
            .get("" + this.config.api_base_url + this.config.getSalutation);
    };
    //getEmployeeType1(): Observable<EmployeeTypeMaster> {
    //  return this.http
    //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeType}`);
    //}
    MasterService.prototype.getEmployeeType = function (serviceId, deputId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('serviceId', serviceId).set('deputationId', deputId);
        return this.http
            .get("" + this.config.api_base_url + this.config.getEmployeeType, { params: params });
    };
    //getEmployeeSubType1(parentId: any): Observable<EmployeeTypeMaster> {
    //  debugger
    //  const params = new HttpParams().set('parentTypeID', parentId)
    //  return this.http
    //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`,{ params });
    //}
    MasterService.prototype.getEmployeeSubType = function (parentId, isDeput) {
        //  const params = new HttpParams().set('parentTypeID', parentId).set('IsDeput', isDeput);
        //   return this.http.get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`, { params });
        return this.http.get(this.config.api_base_url + this.config.getEmployeeSubType + '?parentTypeId=' + parentId + ' &IsDeput=' + isDeput, {});
    };
    //getJoiningMode1(): Observable<any> {
    //  return this.http
    //    .get<any>(`${this.config.api_base_url}${this.config.getJoiningMode}`);
    //}
    MasterService.prototype.getJoiningMode = function (isDeput, empSubTypeId) {
        return this.http.get(this.config.api_base_url + this.config.getJoiningMode + '?isDeput=' + isDeput + '&MsEmpSubTypeID=' + empSubTypeId, {});
    };
    MasterService.prototype.getJoiningCategory = function (parentId, isDeput) {
        return this.http.get(this.config.api_base_url + 'master/getJoiningCategory' + '?parentTypeId=' + parentId + '&isDeput=' + isDeput, {});
    };
    //getDeputationType1(): Observable<MasterModel> {
    //  return this.http
    //    .get<MasterModel>(`${this.config.api_base_url}${this.config.getDeputationType}`);
    //}
    MasterService.prototype.getDeputationType = function (serviceId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('serviceId', serviceId);
        return this.http
            .get("" + this.config.api_base_url + this.config.getDeputationType, { params: params });
    };
    //getServiceType1(): Observable<any> {
    //  return this.http
    //    .get<any>(`${this.config.api_base_url}${this.config.getServiceType}`);
    //}
    MasterService.prototype.getServiceType = function (isDeput) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('isDeput', isDeput);
        return this.http
            .get("" + this.config.api_base_url + this.config.getServiceType, { params: params });
    };
    MasterService.prototype.getGender = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getGender);
    };
    MasterService.prototype.getState = function () {
        return this.http.get("" + this.config.api_base_url + this.config.GetState);
    };
    MasterService.prototype.uploadFile = function (uploadFiles) {
        return this.http.post(this.config.api_base_url + this.config.uploadFiles, uploadFiles, { responseType: 'text' });
    };
    MasterService.prototype.getRelation = function () {
        return this.http
            .get("" + this.config.api_base_url + this.config.getRelation);
    };
    MasterService.prototype.GetAllDesignation = function (controllerId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getDesignationAll + controllerId));
    };
    MasterService.prototype.getJoiningAccountOf = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getJoiningAccountOf);
    };
    MasterService.prototype.getOfficeList = function (controllerId, ddoId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getOfficeList + '?controllerId= ' + controllerId + '&ddoId=' + ddoId));
    };
    MasterService.prototype.getOfficeCityClassList = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getOfficeCityClass);
    };
    MasterService.prototype.getPayCommissionListLien = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getPayCommissionListLien);
    };
    MasterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], MasterService);
    return MasterService;
}());



/***/ }),

/***/ "./src/app/services/promotions/promotions.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/promotions/promotions.service.ts ***!
  \***********************************************************/
/*! exports provided: PromotionsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsService", function() { return PromotionsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PromotionsService = /** @class */ (function () {
    function PromotionsService(http, config) {
        this.http = http;
        this.config = config;
    }
    PromotionsService.prototype.BindDropDownBillGroup = function () {
        // return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
        // return this.http.get('http://localhost:55424/api/PayBill/GetAccountHeads');
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetPayBillGroup?PermDdoId=00003");
    };
    PromotionsService.prototype.BindDropDownDesigGroup = function (selectedBilgrp) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignation?billGroupId=" + selectedBilgrp);
    };
    PromotionsService.prototype.BindDropDownEmpGroup = function (selectedDesig) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmp?desigId=" + selectedDesig + "&pageCode=Leaves");
    };
    PromotionsService.prototype.GetAllDesignation = function (controllerId) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetAllDesignationOfDepartment?id=" + controllerId);
    };
    PromotionsService.prototype.GetAllEmpWithDesignationDetails = function (suspendedEmployee, controllerId, ddoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('suspendedEmployees', suspendedEmployee.toString()).set('controllerId', "013").set('DDOId', ddoId); // controllerId);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmpWithDesignationDetails", { params: params });
    };
    PromotionsService.prototype.FetchEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetPromotionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdatePromotionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdatePromotionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeletePromotionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/" + employeeId);
    };
    PromotionsService.prototype.ForwardTransferDetailsToChecker = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/ForwardTransferDetailToChecker", obj, { responseType: 'text' });
    };
    PromotionsService.prototype.UpdateStatus = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateStatus", obj, { responseType: 'text' });
    };
    // Reversion API's
    PromotionsService.prototype.FetchReversionEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetReversionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdateReversionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateReversionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeleteReversionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/reversion/" + employeeId);
    };
    PromotionsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PromotionsService);
    return PromotionsService;
}());



/***/ })

}]);
//# sourceMappingURL=default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc.js.map