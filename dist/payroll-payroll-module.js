(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["payroll-payroll-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/finally.js":
/*!****************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/finally.js ***!
  \****************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_finally__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/finally */ "./node_modules/rxjs-compat/_esm5/operator/finally.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.finally = _operator_finally__WEBPACK_IMPORTED_MODULE_1__["_finally"];
rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype._finally = _operator_finally__WEBPACK_IMPORTED_MODULE_1__["_finally"];
//# sourceMappingURL=finally.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/finally.js":
/*!************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/finally.js ***!
  \************************************************************/
/*! exports provided: _finally */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_finally", function() { return _finally; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

function _finally(callback) {
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["finalize"])(callback)(this);
}
//# sourceMappingURL=finally.js.map

/***/ }),

/***/ "./src/app/payroll/controllerrole/controllerrole.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/payroll/controllerrole/controllerrole.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r{float:right !important;}\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n.margin-bottom-9 {margin-bottom:9px\r\n}\r\n.EmptyMessage{\r\n  color:red;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9jb250cm9sbGVycm9sZS9jb250cm9sbGVycm9sZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFNBQVMsdUJBQXVCLENBQUM7QUFDakM7RUFDRSxxREFBcUQ7RUFDckQsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCO0FBQ0Qsa0JBQWtCLGlCQUFpQjtDQUNsQztBQUVEO0VBQ0UsVUFBVTtDQUNYIiwiZmlsZSI6InNyYy9hcHAvcGF5cm9sbC9jb250cm9sbGVycm9sZS9jb250cm9sbGVycm9sZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZsb2F0LXJ7ZmxvYXQ6cmlnaHQgIWltcG9ydGFudDt9XHJcbi5tYXQtY2FyZCB7XHJcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyODBtcyBjdWJpYy1iZXppZXIoLjQsMCwuMiwxKTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZzogMTZweDtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuLm1hcmdpbi1ib3R0b20tOSB7bWFyZ2luLWJvdHRvbTo5cHhcclxufVxyXG5cclxuLkVtcHR5TWVzc2FnZXtcclxuICBjb2xvcjpyZWQ7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/payroll/controllerrole/controllerrole.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/payroll/controllerrole/controllerrole.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n\r\n    <!--<form #controllerDetail=\"ngForm\" class=\"rowdasa\" novalidate>-->\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <form #controllerDetail=\"ngForm\"   class=\"rowdasa\" novalidate>\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Select controller\" required #controller=\"ngModel\" [(ngModel)]=\"MsControllerID\" (selectionChange)=\"ControllerChnage($event.value)\" name=\"controller\">\r\n                <mat-option>\r\n                  <ngx-mat-select-search [formControl]=\"controllerFilterCtrl\" [placeholderLabel]=\"'Find controller...'\" [noEntriesFoundLabel]=\"'Controller is not found'\"></ngx-mat-select-search>\r\n                </mat-option>\r\n                <mat-option *ngFor=\"let controller of filteredcontroller |async \" [value]=\"controller.values\">\r\n                  {{controller.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!controller.errors?.required\">Please select Controller</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-2\">\r\n            <div *ngIf=\"btnFlageView\">\r\n              <button type=\"button\" class=\"btn btn-success\" (click)='View()'>View Assigned Controller</button>\r\n            </div>\r\n            <div *ngIf=\"btnFlage\">\r\n              <button type=\"submit\" class=\"btn btn-info\" (click)=\"controllerDetail.onSubmit();controllerDetail.valid && View()\">Go</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n\r\n      <!--=======================two drop down List=======================-->\r\n      <form #DDLDetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n        <div class=\"example-card mat-card margin-bottom-9\" *ngIf=\"DDLHideShowFlage\">\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select PAO\" required #pao=\"ngModel\" [(ngModel)]=\"PAOID\" (selectionChange)=\"onpaoSelectchanged($event.value)\" name=\"pao\">\r\n                <mat-option *ngFor=\"let pao of ddlPAOList\" [value]=\"pao.values\">\r\n                  {{pao.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!pao.errors?.required\">Please select PAO</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n            \r\n          </div>\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select DDO\" required #ddo=\"ngModel\" [(ngModel)]=\"DDOID\" (selectionChange)=\"DDOSelectChange($event.value)\" name=\"ddo\">\r\n                <mat-option *ngFor=\"let ddo of ddlDDOList\" [value]=\"ddo.values\">\r\n                  {{ddo.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!ddo.errors?.required\">Please select DDO</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n           \r\n          </div>\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <button type=\"submit\" class=\"btn btn-info\" (click)='DDLDetail.onSubmit();DDLDetail.valid && go()'>Get Employee</button>\r\n          </div>\r\n        </div>\r\n      </form>\r\n\r\n      <!--=======================Assign Employee List=======================-->\r\n      <form  #empDetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n        <div *ngIf=\"IsFlagUnAssigned\">\r\n          <div *ngIf=\"AssignedEmpList.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n            <div class=\"fom-title\">Assigned Controller</div>\r\n            <table mat-table [dataSource]=\"AssignedEmpList\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"PAOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                    <div *ngIf=\"element.activeStatus =='Controller'; else Assign\">\r\n                      <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n            <div *ngIf=\"AssignedEmpList.length === 0\" class=\"EmptyMessage\">No controller assigned</div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </div>\r\n    <!--</form>-->\r\n    <!--===========form first end===================-->\r\n    <!-- ----------  For List of Employees  ---------------->\r\n    <form  class=\"rowdasa\">\r\n      <div *ngIf=\"IsFlagAssign\">\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <div class=\"example-card mat-card\">\r\n            <div class=\"fom-title\">Employee List</div>\r\n            <mat-form-field>\r\n              <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n              <i class=\"material-icons icon-right\">search</i>\r\n            </mat-form-field>\r\n            <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"PAOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                    <div *ngIf=\"element.activeStatus =='Controller'; else Assign\">\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                    <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);activatePopup = !activatePopup\">check_circle_outline</a>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n            <div [hidden]=\"MessageDataFound\">\r\n              <font color=\"red\">No employee exists</font>\r\n            </div>\r\n            <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to Unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"PaosAssigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"activatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to Assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"PaosAssigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/payroll/controllerrole/controllerrole.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/payroll/controllerrole/controllerrole.component.ts ***!
  \********************************************************************/
/*! exports provided: ControllerroleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ControllerroleComponent", function() { return ControllerroleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/controller.service */ "./src/app/services/UserManagement/controller.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_add_operator_finally__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/finally */ "./node_modules/rxjs-compat/_esm5/add/operator/finally.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var ControllerroleComponent = /** @class */ (function () {
    function ControllerroleComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.paosList = [];
        this.ddlPAOList = [];
        this.ddlDDOList = [];
        this.checkStatus = "NO";
        this.AssignedEmp = [];
        this.MessageDataFound = true;
        this.AssignedEmpList = [];
        this.AssignedEmpListChanged = [];
        this.AssignedEmpFlag = [];
        this.displayedColumns = ['EmpCode', 'DDOName', 'PAOName', 'EmpName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.paosList);
        this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.AssignedEmp);
        this.controllerCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.controllerFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.filteredcontroller = new rxjs__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
    }
    ControllerroleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.btnFlage = true;
        this.IsFlagAssign = false;
        this.DDLHideShowFlage = false;
        this.getallcontrollers(sessionStorage.getItem('controllerID'));
        this.controllerFilterCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._onDestroy)).subscribe(function () { _this.filtercontroller(); });
    };
    //Search functionality
    ControllerroleComponent.prototype.filtercontroller = function () {
        if (!this.controllerroleList) {
            return;
        }
        // get the search keyword
        var search = this.controllerFilterCtrl.value;
        if (!search) {
            this.filteredcontroller.next(this.controllerroleList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredcontroller.next(this.controllerroleList.filter(function (controllerroleList) { return controllerroleList.text.toLowerCase().indexOf(search) > -1; }));
    };
    ControllerroleComponent.prototype.ControllerChnage = function (MsControllerID) {
        var _this = this;
        this.AssignedEmpList = null;
        this.dataSource = null;
        this.MessageDataFound = true;
        this.MsControllerID = MsControllerID;
        this.getallpaos(MsControllerID);
        this.IsFlagUnAssigned = false;
        this.IsFlagAssign = false;
        this.DDLHideShowFlage = false;
        this._Service.AssignedEmp(this.MsControllerID).subscribe(function (data) {
            _this.AssignedEmpListChanged = data;
            if (_this.AssignedEmpListChanged.length == 0) {
                _this.btnFlage = true;
                _this.btnFlageView = false;
            }
            else if (_this.AssignedEmpListChanged.length != 0) {
                _this.btnFlage = false;
                _this.btnFlageView = true;
            }
        });
    };
    ControllerroleComponent.prototype.getallcontrollers = function (ControllerID) {
        var _this = this;
        if (ControllerID == 0) {
            this.Query = this.commonMsg.getController;
            this._Service.getAllControllerServiceclient(ControllerID, this.Query).subscribe(function (data) {
                _this.controllerroleList = data;
                _this.controllerCtrl.setValue(_this.controllerroleList);
                _this.filteredcontroller.next(_this.controllerroleList);
            });
        }
    };
    ControllerroleComponent.prototype.getallpaos = function (ControllerID) {
        var _this = this;
        this.Query = this.commonMsg.getPAO;
        this._Service.GetAllpaos(ControllerID, this.Query).subscribe(function (data) {
            _this.ddlPAOList = data;
        });
    };
    ControllerroleComponent.prototype.getallDDO = function (PAOID) {
        var _this = this;
        this.Query = this.commonMsg.getDDO;
        this._Service.GetAllDDO(PAOID, this.Query).subscribe(function (data) {
            _this.ddlDDOList = data;
        });
    };
    ControllerroleComponent.prototype.onpaoSelectchanged = function (MsPAOID) {
        this.ddlDDOList = null;
        this.PAOID = MsPAOID;
        this.getallDDO(MsPAOID);
    };
    ControllerroleComponent.prototype.DDOSelectChange = function (MsDDOID) {
        this.DDOID = MsDDOID;
    };
    ControllerroleComponent.prototype.View = function () {
        var _this = this;
        this._Service.AssignedEmp(this.MsControllerID).subscribe(function (data) {
            _this.AssignedEmpList = data;
            if (_this.AssignedEmpList.length == 0) {
                _this.btnFlage = true;
                _this.btnFlageView = false;
                _this.DDLHideShowFlage = true;
            }
            else if (_this.AssignedEmpList.length != 0) {
                _this.IsFlagUnAssigned = true;
                _this.btnFlage = false;
                _this.btnFlageView = true;
                _this.DDLHideShowFlage = false;
            }
            var num = 0;
            for (num = 0; num < _this.AssignedEmpList.length; num++) {
                if (_this.AssignedEmpList[num]['activeStatus'] == _this.commonMsg.Controller) {
                    _this.checkStatus = "Yes";
                    _this.btnFlageView = true;
                    break;
                }
            }
        });
    };
    ControllerroleComponent.prototype.go = function () {
        var _this = this;
        this.IsFlagAssign = true;
        //this.MsControllerID = '13';
        //this.PAOID = '144';
        //this.DDOID = '00003';
        this._Service.oncontrollerroleSelectchanged(this.MsControllerID, this.PAOID, this.DDOID).subscribe(function (data) {
            _this.paosList = data;
            _this.AssignedEmp = _this.paosList.filter(function (emp) { return emp.activeStatus == _this.commonMsg.Controller; });
            _this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.AssignedEmp);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.paosList.filter(function (emp) { return emp.activeStatus != _this.commonMsg.Controller; }));
            _this.dataSource.sort = _this.sort;
            _this.dataSource.paginator = _this.paginator;
            if (_this.dataSource.data.length > 0) {
                _this.MessageDataFound = true;
            }
            else {
                _this.MessageDataFound = false;
            }
            var num = 0;
            for (num = 0; num < _this.paosList.length; num++) {
                if (_this.paosList[num]['activeStatus'] == _this.commonMsg.Controller) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
        this.View();
    };
    ControllerroleComponent.prototype.PaosAssigned = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.isLoading = true;
                this._Service.PaosAssigned(this.empCd, this.MsControllerID, this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
                    _this.Messages = result.toLowerCase();
                    if (_this.Messages == _this.commonMsg.USN.toLowerCase()) {
                        _this.IsFlagUnAssigned = false;
                        _this.dataSource = null;
                        _this.AssignedEmp = null;
                        _this.Messages = _this.commonMsg.USN;
                        _this.btnFlageView = false;
                        _this.btnFlage = true;
                        _this.IsFlagAssign = false;
                        _this.DDLHideShowFlage = false;
                        _this.getallcontrollers(sessionStorage.getItem('controllerID'));
                    }
                    if (_this.Messages == _this.commonMsg.ASN.toLowerCase()) {
                        _this.Messages = _this.commonMsg.EmailMsg;
                        _this.btnFlageView = false;
                        _this.btnFlage = true;
                        _this.IsFlagAssign = false;
                        _this.DDLHideShowFlage = false;
                        _this.getallcontrollers(sessionStorage.getItem('controllerID'));
                        _this.IsFlagUnAssigned = true;
                        _this.View();
                    }
                    if (_this.Messages === _this.commonMsg.USN) {
                        _this.checkStatus = "NO";
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                        _this.deactivatePopup = false;
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                        _this.activatePopup = false;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ControllerroleComponent.prototype.oncontrollerroleSelectchanged = function (MsControllerID) {
        this.MsControllerID = MsControllerID;
        this.getallpaos(MsControllerID);
    };
    ControllerroleComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.Controller) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    ControllerroleComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], ControllerroleComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], ControllerroleComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('controllerDetail'),
        __metadata("design:type", Object)
    ], ControllerroleComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('DDLDetail'),
        __metadata("design:type", Object)
    ], ControllerroleComponent.prototype, "DDLDetailform", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('empDetail'),
        __metadata("design:type", Object)
    ], ControllerroleComponent.prototype, "empDetailform", void 0);
    ControllerroleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-controllerrole',
            template: __webpack_require__(/*! ./controllerrole.component.html */ "./src/app/payroll/controllerrole/controllerrole.component.html"),
            styles: [__webpack_require__(/*! ./controllerrole.component.css */ "./src/app/payroll/controllerrole/controllerrole.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_3__["controllerservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_8__["CommonMsg"]])
    ], ControllerroleComponent);
    return ControllerroleComponent;
}());



/***/ }),

/***/ "./src/app/payroll/ddoroll/ddoroll.component.css":
/*!*******************************************************!*\
  !*** ./src/app/payroll/ddoroll/ddoroll.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.float-r {\r\n  float: right !important;\r\n}\r\n\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n\r\n.EmptyMessage {\r\n  color: red;\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9kZG9yb2xsL2Rkb3JvbGwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSx3QkFBd0I7Q0FDekI7O0FBRUQ7RUFDRSxxREFBcUQ7RUFDckQsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUNEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvcGF5cm9sbC9kZG9yb2xsL2Rkb3JvbGwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4uZmxvYXQtciB7XHJcbiAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyODBtcyBjdWJpYy1iZXppZXIoLjQsMCwuMiwxKTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZzogMTZweDtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuXHJcbi5tYXJnaW4tYm90dG9tLTkge1xyXG4gIG1hcmdpbi1ib3R0b206IDlweFxyXG59XHJcbi5FbXB0eU1lc3NhZ2Uge1xyXG4gIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/payroll/ddoroll/ddoroll.component.html":
/*!********************************************************!*\
  !*** ./src/app/payroll/ddoroll/ddoroll.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form #DDODetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select DDO\" required #ddo=\"ngModel\" [(ngModel)]=\"DDOID\" name=\"ddo\" (selectionChange)=\"AssignedDDO($event.value)\">\r\n                <mat-option *ngFor=\"let ddo of DDOList\" [value]=\"ddo.values\">\r\n                  {{ddo.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!ddo.errors?.required\">Please select PAO</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-2\">\r\n            <div *ngIf=\"btnFlageView\">\r\n              <button type=\"button\" class=\"btn btn-success\" (click)='Go()'>View Assigned DDO</button>\r\n            </div>\r\n            <div *ngIf=\"btnFlage\">\r\n              <button type=\"submit\" class=\"btn btn-warning\" (click)='DDODetail.onSubmit();DDODetail.valid && Go()'>Select Employee</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div *ngIf=\"IsFlagUnAssigned\">\r\n          <div *ngIf=\"AssignedEmp && AssignedEmp.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n            <div class=\"fom-title\">Assigned DDO</div>\r\n            <table mat-table [dataSource]=\"AssignedEmp\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\" style=\"align-content:center\">\r\n                    <div *ngIf=\"element.activeStatus =='DDO Admin'; else Assign\">\r\n                      <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n            <div *ngIf=\"AssignedEmp && AssignedEmp.length === 0\" class=\"EmptyMessage\">No PAO assigned</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <form class=\"rowdasa\">\r\n      <div *ngIf=\"IsFlagAssign\">\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <div class=\"example-card mat-card\">\r\n            <div class=\"fom-title\">Employee List</div>\r\n            <mat-form-field>\r\n              <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n              <i class=\"material-icons icon-right\">search</i>\r\n            </mat-form-field>\r\n            <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                    <div *ngIf=\"element.activeStatus =='DDO Admin'; else Assign\">\r\n                      <!--<a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);deactivatePopup = !deactivatePopup\">edit</a>-->\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                    <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n\r\n            <div *ngIf=\"EmpList && EmpList.length==0\">\r\n              <font color=\"red\">No employee exist</font>\r\n            </div>\r\n            <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/ddoroll/ddoroll.component.ts":
/*!******************************************************!*\
  !*** ./src/app/payroll/ddoroll/ddoroll.component.ts ***!
  \******************************************************/
/*! exports provided: DdorollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DdorollComponent", function() { return DdorollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_ddo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/ddo.service */ "./src/app/services/UserManagement/ddo.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DdorollComponent = /** @class */ (function () {
    function DdorollComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.DDOList = [];
        this.userroleList = [];
        this.EmpList = [];
        this.EmpListTemp = [];
        this.checkStatus = "NO";
        this.AssignedEmp = [];
        this.MessageDataFound = true;
        this.EmpListAssigned = [];
        this.displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.EmpList);
        this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.AssignedEmp);
    }
    DdorollComponent.prototype.ngOnInit = function () {
        this.btnFlage = true;
        this.getallDDO(sessionStorage.getItem('paoid'));
    };
    DdorollComponent.prototype.AssignedDDO = function (DDOID) {
        var _this = this;
        this.IsFlagAssign = false;
        this.IsFlagUnAssigned = false;
        this.AssignedEmp = null;
        this.dataSource = null;
        this.MessageDataFound = false;
        this.DDOID = DDOID;
        this.isLoading = true;
        this._Service.AssignedDDO(this.DDOID).subscribe(function (data) {
            _this.EmpListAssigned = data;
            if (_this.EmpListAssigned.length == 0) {
                _this.btnFlage = true;
                _this.btnFlageView = false;
                _this.IsFlagAssign = false;
                _this.IsFlagUnAssigned = true;
            }
            else if (_this.EmpListAssigned.length != 0) {
                _this.btnFlage = false;
                _this.btnFlageView = true;
                _this.IsFlagAssign = false;
                _this.IsFlagUnAssigned = true;
            }
            _this.isLoading = false;
        });
    };
    DdorollComponent.prototype.Go = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.AssignedEmpDDO(this.DDOID).subscribe(function (data) {
            _this.AssignedEmp = data;
            if (_this.AssignedEmp.length == 0) {
                _this.IsFlagAssign = true;
                _this.GoEmpList();
            }
            else if (_this.AssignedEmp.length != 0) {
                _this.isLoading = false;
                _this.IsFlagUnAssigned = true;
                _this.IsFlagAssign = false;
            }
            else {
                _this.IsFlagAssign = true;
                _this.IsFlagUnAssigned = false;
            }
            var num = 0;
            for (num = 0; num < _this.AssignedEmp.length; num++) {
                if (_this.AssignedEmp[num]['activeStatus'] == _this.commonMsg.dDOAdmin) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
    };
    DdorollComponent.prototype.GoEmpList = function () {
        var _this = this;
        this.checkStatus = "NO";
        this.isLoading = true;
        this._Service.EmployeeList(this.DDOID).subscribe(function (data) {
            _this.EmpList = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.EmpList.filter(function (emp) { return emp.activeStatus != _this.commonMsg.dDOAdmin; }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.EmpList.length > 0) {
                _this.MessageDataFound = true;
            }
            else {
                _this.MessageDataFound = false;
            }
            var num = 0;
            for (num = 0; num < _this.EmpList.length; num++) {
                if (_this.EmpList[num]['activeStatus'] == _this.commonMsg.dDOAdmin) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
            _this.isLoading = false;
        });
    };
    DdorollComponent.prototype.EmployeeList = function (DDOID) {
        var _this = this;
        this.DDOID = DDOID;
        this.btnFlage = false;
        this.btnFlageView = false;
        this.isLoading = true;
        this._Service.EmployeeList(this.DDOID).finally(function () { return _this.isLoading = false; }).subscribe(function (data) {
            _this.EmpListTemp = data;
            if (_this.EmpListTemp.length == 0) {
            }
            else if (_this.EmpListTemp.length != 0) {
                _this.btnFlage = false;
                _this.btnFlageView = true;
            }
        });
    };
    DdorollComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.dDOAdmin) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    DdorollComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    DdorollComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.Assigned(this.empCd, this.DDOID, this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages == _this.commonMsg.USN) {
                _this.IsFlagUnAssigned = false;
                _this.btnFlageView = false;
                _this.btnFlage = true;
                _this.deactivatePopup = false;
            }
            if (_this.Messages == _this.commonMsg.ASN) {
                _this.Messages = _this.commonMsg.EmailMsg;
                _this.btnFlageView = true;
                _this.btnFlage = false;
                _this.ActivatePopup = false;
            }
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                _this.Go();
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
            }
        });
    };
    DdorollComponent.prototype.GetAllUserRoles = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.GetAllUserRoles(sessionStorage.getItem('username')).finally(function () { return _this.isLoading = false; }).subscribe(function (data) {
            _this.userroleList = data;
        });
    };
    DdorollComponent.prototype.getallDDO = function (PAOID) {
        var _this = this;
        this.isLoading = true;
        this.Query = this.commonMsg.getDDO;
        this._Service.GetAllDDO(PAOID, this.Query).finally(function () { return _this.isLoading = false; }).subscribe(function (data) {
            _this.DDOList = data;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], DdorollComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('DDODetail'),
        __metadata("design:type", Object)
    ], DdorollComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], DdorollComponent.prototype, "paginator", void 0);
    DdorollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ddoroll',
            template: __webpack_require__(/*! ./ddoroll.component.html */ "./src/app/payroll/ddoroll/ddoroll.component.html"),
            styles: [__webpack_require__(/*! ./ddoroll.component.css */ "./src/app/payroll/ddoroll/ddoroll.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_ddo_service__WEBPACK_IMPORTED_MODULE_3__["ddoservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], DdorollComponent);
    return DdorollComponent;
}());



/***/ }),

/***/ "./src/app/payroll/hoo-checker/hoo-checker.component.css":
/*!***************************************************************!*\
  !*** ./src/app/payroll/hoo-checker/hoo-checker.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r {\r\n  float: right !important;\r\n}\r\n\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n\r\n.EmptyMessage {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9ob28tY2hlY2tlci9ob28tY2hlY2tlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UscURBQXFEO0VBQ3JELGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFDRDtFQUNFLFdBQVc7Q0FDWiIsImZpbGUiOiJzcmMvYXBwL3BheXJvbGwvaG9vLWNoZWNrZXIvaG9vLWNoZWNrZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mbG9hdC1yIHtcclxuICBmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1jYXJkIHtcclxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDI4MG1zIGN1YmljLWJlemllciguNCwwLC4yLDEpO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nOiAxNnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG5cclxuLm1hcmdpbi1ib3R0b20tOSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogOXB4XHJcbn1cclxuLkVtcHR5TWVzc2FnZSB7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/payroll/hoo-checker/hoo-checker.component.html":
/*!****************************************************************!*\
  !*** ./src/app/payroll/hoo-checker/hoo-checker.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <!--====================  Assigned HOO Employee List ===================-->\r\n        <div *ngIf=\"HooCheckerListAssigned.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"fom-title\">Assigned HOO Checker</div>\r\n          <table mat-table [dataSource]=\"HooCheckerListAssigned\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div *ngIf=\"HooCheckerListAssigned.length === 0\" style=\"color:red;\">No employee exists</div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n    <!--====================    HOO Employee List ===================-->\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card\">\r\n          <div class=\"fom-title\">Employee List</div>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                  <div *ngIf=\"element.activeStatus =='DDO Checker'; else Assign\">\r\n                  </div>\r\n                </div>\r\n                <ng-template #NotAssign>\r\n                  <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                </ng-template>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div [hidden]=\"MessageDataFound\">\r\n            <font class=\"EmptyMessage\">No employee exists</font>\r\n          </div>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n          <div *ngIf=\"SelfCheck\">\r\n            <section class=\"example-section\">\r\n              <mat-checkbox class=\"example-margin\" name=\"checked\" (change)=\"SelfAssign(checked)\" [checked]=\"checked\" [(ngModel)]=\"checked\" style=\"font-weight:bolder\"><span style=\"font-weight:bolder; color:black\">Check the box to assign self as DDO checker</span></mat-checkbox>\r\n            </section>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button> &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"deactivatePopup =!deactivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button> &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ActivatePopup =!ActivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/hoo-checker/hoo-checker.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/payroll/hoo-checker/hoo-checker.component.ts ***!
  \**************************************************************/
/*! exports provided: HOOCheckerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOOCheckerComponent", function() { return HOOCheckerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_hoo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/hoo.service */ "./src/app/services/UserManagement/hoo.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HOOCheckerComponent = /** @class */ (function () {
    function HOOCheckerComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.Active = null;
        this.MessageDataFound = true;
        this.displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
    }
    HOOCheckerComponent.prototype.ngOnInit = function () {
        this.hooCheckerEmpList();
        this.SelfAssignHOOCheckerRole();
    };
    // employee list asssign and unassign
    HOOCheckerComponent.prototype.hooCheckerEmpList = function () {
        var _this = this;
        this._Service.hooCheckerEmpList(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.HooCheckerList = data['listEmployeeModel'];
            _this.HooCheckerListAssigned = data['listEmployeeModelAsigned'];
            _this.AssignedEmp = _this.HooCheckerList.filter(function (emp) { return (emp.activeStatus == _this.commonMsg.HOOChecker); });
            _this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.AssignedEmp);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.HooCheckerList.filter(function (emp) { return (emp.activeStatus != _this.commonMsg.HOOChecker); }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.dataSource.data.length > 0) {
                _this.MessageDataFound = true;
                _this.SelfCheck = false;
            }
            else {
                _this.MessageDataFound = false;
                _this.SelfCheck = true;
            }
            var num = 0;
            for (num = 0; num < _this.HooCheckerList.length; num++) {
                if (_this.HooCheckerList[num]['activeStatus'] == _this.commonMsg.HOOChecker) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
    };
    HOOCheckerComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.AssignedHOOChecker(this.empCd, sessionStorage.getItem('ddoid'), this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                _this.hooCheckerEmpList();
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                _this.ActivatePopup = false;
                _this.deactivatePopup = false;
            }
        });
    };
    HOOCheckerComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.HOOChecker) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    HOOCheckerComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    HOOCheckerComponent.prototype.SelfAssign = function (IsAssign) {
        var _this = this;
        debugger;
        this.checked = IsAssign;
        this._Service.SelfAssignHOOChecker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(function (result) {
            _this.Messages = result;
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
            _this.ActivatePopup = false;
        });
    };
    HOOCheckerComponent.prototype.SelfAssignHOOCheckerRole = function () {
        var _this = this;
        this.checked = true;
        this._Service.SelfAssignHOOCheckerRole(sessionStorage.getItem('UserID')).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages == '[]') {
                _this.checked = false;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], HOOCheckerComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], HOOCheckerComponent.prototype, "sort", void 0);
    HOOCheckerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hoo-checker',
            template: __webpack_require__(/*! ./hoo-checker.component.html */ "./src/app/payroll/hoo-checker/hoo-checker.component.html"),
            styles: [__webpack_require__(/*! ./hoo-checker.component.css */ "./src/app/payroll/hoo-checker/hoo-checker.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_hoo_service__WEBPACK_IMPORTED_MODULE_3__["hooservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], HOOCheckerComponent);
    return HOOCheckerComponent;
}());



/***/ }),

/***/ "./src/app/payroll/hoo-maker/hoo-maker.component.css":
/*!***********************************************************!*\
  !*** ./src/app/payroll/hoo-maker/hoo-maker.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r {\r\n  float: right !important;\r\n}\r\n\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9ob28tbWFrZXIvaG9vLW1ha2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx3QkFBd0I7Q0FDekI7O0FBRUQ7RUFDRSxxREFBcUQ7RUFDckQsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25CIiwiZmlsZSI6InNyYy9hcHAvcGF5cm9sbC9ob28tbWFrZXIvaG9vLW1ha2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmxvYXQtciB7XHJcbiAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyODBtcyBjdWJpYy1iZXppZXIoLjQsMCwuMiwxKTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZzogMTZweDtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuXHJcbi5tYXJnaW4tYm90dG9tLTkge1xyXG4gIG1hcmdpbi1ib3R0b206IDlweFxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/payroll/hoo-maker/hoo-maker.component.html":
/*!************************************************************!*\
  !*** ./src/app/payroll/hoo-maker/hoo-maker.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"fom-title\">Assigned HOO Checker</div>\r\n          <table mat-table [dataSource]=\"HooMakerListAssigned\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div *ngIf=\"HooMakerListAssigned.length === 0\" style=\"color:red;\">No HOO Maker assigned</div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card\">\r\n          <div class=\"fom-title\">Employee List</div>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                  <div *ngIf=\"element.activeStatus =='DDO Checker'; else Assign\">\r\n                    <!--<button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\">\r\n                      <i class=\"material-icons\">delete</i>\r\n                    </button>-->\r\n                  </div>\r\n                </div>\r\n                <ng-template #NotAssign>\r\n                  <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                </ng-template>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"deactivatePopup = !deactivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ActivatePopup = !ActivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/hoo-maker/hoo-maker.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/payroll/hoo-maker/hoo-maker.component.ts ***!
  \**********************************************************/
/*! exports provided: HOOMakerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HOOMakerComponent", function() { return HOOMakerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_hoo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/hoo.service */ "./src/app/services/UserManagement/hoo.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HOOMakerComponent = /** @class */ (function () {
    function HOOMakerComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.Active = null;
        this.displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
    }
    HOOMakerComponent.prototype.ngOnInit = function () {
        this.hooMakerEmpList();
    };
    HOOMakerComponent.prototype.hooMakerEmpList = function () {
        var _this = this;
        this._Service.hooMakerEmpList(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.HooMakerList = data['listEmployeeModel'];
            _this.HooMakerListAssigned = data['listEmployeeModelAsigned'];
            _this.AssignedEmp = _this.HooMakerList.filter(function (emp) { return (emp.activeStatus == _this.commonMsg.HOOMaker); });
            _this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.AssignedEmp);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.HooMakerList.filter(function (emp) { return (emp.activeStatus != _this.commonMsg.HOOMaker); }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            var num = 0;
            for (num = 0; num < _this.HooMakerList.length; num++) {
                if (_this.HooMakerList[num]['activeStatus'] == _this.commonMsg.HOOMaker) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
    };
    HOOMakerComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.AssignedHOOMaker(this.empCd, sessionStorage.getItem('ddoid'), this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                _this.hooMakerEmpList();
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                _this.ActivatePopup = false;
                _this.deactivatePopup = false;
            }
        });
    };
    HOOMakerComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.HOOMaker) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    HOOMakerComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], HOOMakerComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], HOOMakerComponent.prototype, "sort", void 0);
    HOOMakerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-hoo-maker',
            template: __webpack_require__(/*! ./hoo-maker.component.html */ "./src/app/payroll/hoo-maker/hoo-maker.component.html"),
            styles: [__webpack_require__(/*! ./hoo-maker.component.css */ "./src/app/payroll/hoo-maker/hoo-maker.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_hoo_service__WEBPACK_IMPORTED_MODULE_3__["hooservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], HOOMakerComponent);
    return HOOMakerComponent;
}());



/***/ }),

/***/ "./src/app/payroll/login-activate/login-activate.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/payroll/login-activate/login-activate.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheXJvbGwvbG9naW4tYWN0aXZhdGUvbG9naW4tYWN0aXZhdGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/payroll/login-activate/login-activate.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/payroll/login-activate/login-activate.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  Account Activated successfully!----------{{param1}}----------{{param2}}\r\n</p>\r\n<div>\r\n\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/payroll/login-activate/login-activate.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/payroll/login-activate/login-activate.component.ts ***!
  \********************************************************************/
/*! exports provided: LoginActivateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginActivateComponent", function() { return LoginActivateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginActivateComponent = /** @class */ (function () {
    function LoginActivateComponent(route) {
        this.route = route;
    }
    LoginActivateComponent.prototype.ngOnInit = function () {
        var _this = this;
        debugger;
        this.route.queryParams.subscribe(function (params) {
            _this.param1 = params['param1'];
            _this.param2 = params['param2'];
            alert(_this.param1);
            debugger;
        });
    };
    LoginActivateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-activate',
            template: __webpack_require__(/*! ./login-activate.component.html */ "./src/app/payroll/login-activate/login-activate.component.html"),
            styles: [__webpack_require__(/*! ./login-activate.component.css */ "./src/app/payroll/login-activate/login-activate.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], LoginActivateComponent);
    return LoginActivateComponent;
}());



/***/ }),

/***/ "./src/app/payroll/makerroll/makerroll.component.css":
/*!***********************************************************!*\
  !*** ./src/app/payroll/makerroll/makerroll.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r {\r\n  float: right !important;\r\n}\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n.EmptyMessage {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9tYWtlcnJvbGwvbWFrZXJyb2xsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx3QkFBd0I7Q0FDekI7QUFDRDtFQUNFLHFEQUFxRDtFQUNyRCxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxtQkFBbUI7Q0FDcEI7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjtBQUVEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvcGF5cm9sbC9tYWtlcnJvbGwvbWFrZXJyb2xsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmxvYXQtciB7XHJcbiAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hdC1jYXJkIHtcclxuICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDI4MG1zIGN1YmljLWJlemllciguNCwwLC4yLDEpO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nOiAxNnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG5cclxuLm1hcmdpbi1ib3R0b20tOSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogOXB4XHJcbn1cclxuXHJcbi5FbXB0eU1lc3NhZ2Uge1xyXG4gIGNvbG9yOiByZWQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/payroll/makerroll/makerroll.component.html":
/*!************************************************************!*\
  !*** ./src/app/payroll/makerroll/makerroll.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div *ngIf=\"AssignedMakerList.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"fom-title\">Assigned DDO Maker</div>\r\n          <table mat-table [dataSource]=\"AssignedMakerList\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div *ngIf=\"AssignedMakerList.length === 0\" style=\"color:red;\">No employee exists</div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card\">\r\n          <div class=\"fom-title\">Employee List</div>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                  <div *ngIf=\"element.activeStatus =='DDO Checker'; else Assign\">\r\n                    <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\">\r\n                      <i class=\"material-icons\">delete</i>\r\n                    </button>\r\n                  </div>\r\n                </div>\r\n                <ng-template #NotAssign>\r\n                  <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                </ng-template>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div [hidden]=\"MessageDataFound\">\r\n            <font class=\"EmptyMessage\">No employee exists</font>\r\n          </div>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n          <div *ngIf=\"SelfCheck\">\r\n            <section class=\"example-section\">\r\n              <mat-checkbox class=\"example-margin\" name=\"checked\" (change)=\"SelfAssign(checked)\" [checked]=\"checked\" [(ngModel)]=\"checked\" style=\"font-weight:bolder\"><span style=\"font-weight:bolder; color:black\">Check the box to assign self as DDO checker</span></mat-checkbox>\r\n            </section>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>&nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"deactivatePopup = !deactivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" [disabled]=\"AssignedBtnDisabled\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>&nbsp;\r\n      <button type=\"button\" [disabled]=\"CancelDisabled\" class=\"btn btn-info\" (click)=\"deactivatePopup = !deactivatePopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/makerroll/makerroll.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/payroll/makerroll/makerroll.component.ts ***!
  \**********************************************************/
/*! exports provided: MakerrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MakerrollComponent", function() { return MakerrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_UserManagement_maker_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/UserManagement/maker.service */ "./src/app/services/UserManagement/maker.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MakerrollComponent = /** @class */ (function () {
    function MakerrollComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.Active = null;
        this.length = null;
        this.MessageDataFound = true;
        this.displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
    }
    MakerrollComponent.prototype.ngOnInit = function () {
        this.makerEmpList();
        this.AssignedBtnDisabled = false;
        this.CancelDisabled = false;
        this.CheckedFlagSelfAssignDDOMaker();
        this.AssignedmakerEmpList();
    };
    MakerrollComponent.prototype.AssignedmakerEmpList = function () {
        var _this = this;
        this._Service.AssignedMakerEmpList(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.AssignedMakerList = data;
        });
    };
    MakerrollComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this.AssignedBtnDisabled = true;
        this.CancelDisabled = true;
        this._Service.AssignedMaker(this.empCd, sessionStorage.getItem('ddoid'), sessionStorage.getItem('UserID'), this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result.toLowerCase();
            if (_this.Messages == _this.commonMsg.USN.toLowerCase()) {
                _this.Messages = _this.commonMsg.USN;
                _this.makerEmpList();
                _this.AssignedmakerEmpList();
            }
            if (_this.Messages == _this.commonMsg.ASN.toLowerCase()) {
                _this.Messages = _this.commonMsg.EmailMsg;
                _this.makerEmpList();
                _this.AssignedmakerEmpList();
            }
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.Messages);
                _this.ActivatePopup = false;
                _this.deactivatePopup = false;
            }
        });
    };
    MakerrollComponent.prototype.makerEmpList = function () {
        var _this = this;
        this.AssignedBtnDisabled = false;
        this.CancelDisabled = false;
        this._Service.makerEmpList(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.makerList = data;
            _this.AssignedEmp = _this.makerList.filter(function (emp) { return (emp.activeStatus == _this.commonMsg.DDOChecker) || (emp.activeStatus == _this.commonMsg.DDOMaker); });
            _this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.AssignedEmp);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.makerList.filter(function (emp) { return (emp.activeStatus != _this.commonMsg.DDOChecker) && (emp.activeStatus != _this.commonMsg.DDOMaker); }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.dataSource.data.length > 0) {
                _this.MessageDataFound = true;
                _this.SelfCheck = false;
            }
            else {
                _this.MessageDataFound = false;
                _this.SelfCheck = true;
            }
        });
    };
    MakerrollComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.DDOChecker || activeStatus == this.commonMsg.DDOMaker) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    MakerrollComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    MakerrollComponent.prototype.SelfAssign = function (IsAssign) {
        var _this = this;
        this.checked = IsAssign;
        this._Service.SelfAssignedDDOMaker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(function (result) {
            _this.Messages = result;
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.Messages);
            _this.ActivatePopup = false;
        });
    };
    MakerrollComponent.prototype.CheckedFlagSelfAssignDDOMaker = function () {
        var _this = this;
        this.checked = true;
        this._Service.SelfAssignMakerRole(sessionStorage.getItem('UserID')).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages == '[]') {
                _this.checked = false;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], MakerrollComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], MakerrollComponent.prototype, "sort", void 0);
    MakerrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-makerroll',
            template: __webpack_require__(/*! ./makerroll.component.html */ "./src/app/payroll/makerroll/makerroll.component.html"),
            styles: [__webpack_require__(/*! ./makerroll.component.css */ "./src/app/payroll/makerroll/makerroll.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_maker_service__WEBPACK_IMPORTED_MODULE_1__["makerservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], MakerrollComponent);
    return MakerrollComponent;
}());



/***/ }),

/***/ "./src/app/payroll/paoroll/paoroll.component.css":
/*!*******************************************************!*\
  !*** ./src/app/payroll/paoroll/paoroll.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r {\r\n  float: right !important;\r\n}\r\n\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC9wYW9yb2xsL3Bhb3JvbGwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHdCQUF3QjtDQUN6Qjs7QUFFRDtFQUNFLHFEQUFxRDtFQUNyRCxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxrQkFBa0I7Q0FDbkIiLCJmaWxlIjoic3JjL2FwcC9wYXlyb2xsL3Bhb3JvbGwvcGFvcm9sbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZsb2F0LXIge1xyXG4gIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWNhcmQge1xyXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMjgwbXMgY3ViaWMtYmV6aWVyKC40LDAsLjIsMSk7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHBhZGRpbmc6IDE2cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ubWFyZ2luLWJvdHRvbS05IHtcclxuICBtYXJnaW4tYm90dG9tOiA5cHhcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/payroll/paoroll/paoroll.component.html":
/*!********************************************************!*\
  !*** ./src/app/payroll/paoroll/paoroll.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <!--<form #PAODetail=\"ngForm\" class=\"rowdasa\" novalidate>-->\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <form #PAODetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Please select PAO\" required #pao=\"ngModel\" [(ngModel)]=\"PAOID\" name=\"pao\" (selectionChange)=\"onpaoSelectchanged($event.value)\">\r\n                <mat-option *ngFor=\"let pao of paosList\" [value]=\"pao.values\">\r\n                  {{pao.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!pao.errors?.required\">Please select PAO</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-2\">\r\n            <div *ngIf=\"btnFlageView\">\r\n              <button type=\"button\" class=\"btn btn-success\" (click)='View()'>View Assigned PAO</button>\r\n            </div>\r\n            <div *ngIf=\"btnFlage\">\r\n              <button type=\"submit\" class=\"btn btn-warning\" (click)='PAODetail.onSubmit();PAODetail.valid && View()'>Select Employee</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!--=======================DDO drop down =======================-->\r\n      <form #DDODetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n        <div class=\"example-card mat-card margin-bottom-9\" *ngIf=\"DDLHideShow\">\r\n          <div class=\"col-sm-4 col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Please select DDO\" required #ddo=\"ngModel\" [(ngModel)]=\"DDOID\" name=\"ddo\" (selectionChange)=\"DDOSelectChange($event.value)\">\r\n                <mat-option *ngFor=\"let ddo of ddlDDOList\" [value]=\"ddo.values\">\r\n                  {{ddo.text}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span [hidden]=\"!ddo.errors?.required\">Please select DDO</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-2\">\r\n            <button mat-mini-fab color=\"primary\" type=\"submit\" class=\"go-btn\" (click)='DDODetail.onSubmit();DDODetail.valid && Go()'>Go</button>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!--=======================Assign Employee List=======================-->\r\n      <form #empDetail=\"ngForm\" class=\"rowdasa\" novalidate>\r\n        <div *ngIf=\"IsFlagUnAssigned\">\r\n          <div *ngIf=\"AssignedEmp.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n            <div class=\"fom-title\">Assigned PAO</div>\r\n            <table mat-table [dataSource]=\"AssignedEmp\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                    <div *ngIf=\"element.activeStatus =='PAO'; else Assign\">\r\n                      <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                    <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);ActivatePopup = !ActivatePopup\">\r\n                      <i class=\"material-icons\">edit</i>\r\n                    </button>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </div>\r\n    \r\n    <!--=======================unassign Employee List=======================-->\r\n    <form class=\"rowdasa\">\r\n      <div *ngIf=\"IsFlagAssign\">\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <div class=\"example-card mat-card\">\r\n            <div class=\"fom-title\">Employee List</div>\r\n            <mat-form-field>\r\n              <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n              <i class=\"material-icons icon-right\">search</i>\r\n            </mat-form-field>\r\n            <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n              <ng-container matColumnDef=\"EmpCode\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"DDOName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"EmpName\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n                <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"CommonDesigDesc\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Designation </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"Email\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\r\n                <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n              </ng-container>\r\n              <ng-container matColumnDef=\"ActiveDeactive\">\r\n                <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n                <td mat-cell *matCellDef=\"let element\">\r\n                  <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                    <div *ngIf=\"element.activeStatus =='PAO'; else Assign\">\r\n                      <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);deactivatePopup = !deactivatePopup\">edit</a>\r\n                    </div>\r\n                  </div>\r\n                  <ng-template #NotAssign>\r\n                    <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                  </ng-template>\r\n                </td>\r\n              </ng-container>\r\n              <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n              <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n            <div [hidden]=\"MessageDataFound\">\r\n              <font color=\"red\">No employee exist</font>\r\n            </div>\r\n            <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons></mat-paginator>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/paoroll/paoroll.component.ts":
/*!******************************************************!*\
  !*** ./src/app/payroll/paoroll/paoroll.component.ts ***!
  \******************************************************/
/*! exports provided: PaorollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaorollComponent", function() { return PaorollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_pao_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/pao.service */ "./src/app/services/UserManagement/pao.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaorollComponent = /** @class */ (function () {
    function PaorollComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        //userroleList: any = [];
        this.paosList = [];
        this.EmpList = [];
        this.checkStatus = "NO";
        this.AssignedEmp = [];
        this.AssignedEmpTemp = [];
        this.ddlDDOList = [];
        this.MessageDataFound = true;
        this.AssignedEmpList = [];
        this.AssignedEmpListTemp = [];
        this.displayedColumns = ['EmpCode', 'DDOName', 'EmpName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.EmpList);
        this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.AssignedEmp);
    }
    PaorollComponent.prototype.ngOnInit = function () {
        this.btnFlage = true;
        this.DDLHideShow = false;
        this.getallpaos(sessionStorage.getItem('controllerID'));
    };
    PaorollComponent.prototype.PAODDLChange = function () {
        this.DDLHideShow = true;
    };
    PaorollComponent.prototype.Go = function () {
        var _this = this;
        alert('go');
        this._Service.AssignedPAOEmp(this.MsPAOID).subscribe(function (data) {
            _this.AssignedEmpList = data;
            _this.AssignedEmpListTemp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.AssignedEmpList);
            if (_this.AssignedEmpListTemp.data.length > 0) {
                _this.IsFlagAssign = false;
            }
            else {
                _this.employeeList();
                _this.IsFlagAssign = true;
            }
        });
    };
    PaorollComponent.prototype.employeeList = function () {
        var _this = this;
        this._Service.onpaoSelectchanged(this.MsPAOID).subscribe(function (data) {
            _this.EmpList = data;
            _this.AssignedEmp = _this.EmpList.filter(function (emp) { return emp.activeStatus == _this.commonMsg.PAO; });
            _this.datasourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.AssignedEmp);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.EmpList.filter(function (emp) { return emp.activeStatus != _this.commonMsg.PAO; }));
            _this.dataSource.paginator = _this.paginator;
            if (_this.dataSource.data.length > 0) {
                _this.MessageDataFound = true;
            }
            else {
                _this.MessageDataFound = false;
            }
            var num = 0;
            for (num = 0; num < _this.EmpList.length; num++) {
                if (_this.EmpList[num]['activeStatus'] == _this.commonMsg.PAO) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
    };
    PaorollComponent.prototype.View = function () {
        var _this = this;
        // this.ddlDDOList = null;
        this._Service.AssignedEmpList(this.MsPAOID).subscribe(function (data) {
            _this.AssignedEmp = data;
            if (_this.AssignedEmp.length == 0) {
                _this.IsFlagAssign = true;
                _this.btnFlageView = false;
                _this.btnFlage = true;
                _this.IsFlagAssign = false;
                _this.DDLHideShow = true;
            }
            else if (_this.AssignedEmp.length != 0) {
                _this.IsFlagUnAssigned = true;
                _this.IsFlagAssign = false;
                _this.btnFlageView = true;
                _this.btnFlage = false;
                _this.DDLHideShow = false;
            }
            else {
                _this.IsFlagAssign = true;
                _this.IsFlagUnAssigned = false;
            }
            var num = 0;
            for (num = 0; num < _this.AssignedEmp.length; num++) {
                if (_this.AssignedEmp[num]['activeStatus'] == _this.commonMsg.PAO) {
                    _this.checkStatus = "Yes";
                    break;
                }
            }
        });
    };
    PaorollComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.Assigned(this.empCd, this.MsPAOID, this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages == _this.commonMsg.USN) {
                _this.IsFlagUnAssigned = false;
                _this.IsFlagAssign = true;
                _this.btnFlageView = false;
                _this.btnFlage = true;
                _this.dataSource = null;
                _this.AssignedEmp = null;
                _this.IsFlagAssign = false;
            }
            if (_this.Messages == _this.commonMsg.ASN) {
                _this.IsFlagUnAssigned = true;
                _this.IsFlagAssign = false;
                _this.btnFlageView = true;
                _this.btnFlage = false;
                _this.Messages = _this.commonMsg.EmailMsg;
                _this.View();
            }
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                _this.ActivatePopup = false;
                _this.deactivatePopup = false;
            }
        });
    };
    PaorollComponent.prototype.onpaoSelectchanged = function (MsPAOID) {
        var _this = this;
        //alert('Change')
        // this.ddlDDOList = null;
        this.MsPAOID = MsPAOID;
        this.getallDDO(this.MsPAOID);
        this.IsFlagUnAssigned = false;
        this.IsFlagAssign = false;
        this.dataSource = null;
        this.AssignedEmp = null;
        this.MessageDataFound = true;
        this.DDLHideShow = false;
        this.btnFlage = false;
        this.btnFlageView = false;
        this._Service.AssignedEmpList(this.MsPAOID).subscribe(function (data) {
            _this.AssignedEmpTemp = data;
            if (_this.AssignedEmpTemp.length == 0) {
                _this.btnFlage = true;
                _this.btnFlageView = false;
            }
            else if (_this.AssignedEmpTemp.length != 0) {
                _this.btnFlage = false;
                _this.btnFlageView = true;
            }
        });
    };
    PaorollComponent.prototype.getallpaos = function (ControllerID) {
        var _this = this;
        // this.paosList = null;
        //this.ddlDDOList = null;
        this.Query = 'getPAO';
        this._Service.GetAllpaos(ControllerID, this.Query).subscribe(function (data) {
            _this.paosList = data;
        });
    };
    PaorollComponent.prototype.getallDDO = function (PAOID) {
        var _this = this;
        //this.ddlDDOList = null;
        this.Query = 'getDDO';
        this._Service.GetAllDDO(PAOID, this.Query).subscribe(function (data) {
            _this.ddlDDOList = data;
        });
    };
    PaorollComponent.prototype.DDOSelectChange = function (MsDDOID) {
        this.DDOID = MsDDOID;
    };
    PaorollComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.PAO) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    PaorollComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], PaorollComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], PaorollComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('PAODetail'),
        __metadata("design:type", Object)
    ], PaorollComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('DDODetail'),
        __metadata("design:type", Object)
    ], PaorollComponent.prototype, "DDLDetailform", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('empDetail'),
        __metadata("design:type", Object)
    ], PaorollComponent.prototype, "empDetailform", void 0);
    PaorollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-paoroll',
            template: __webpack_require__(/*! ./paoroll.component.html */ "./src/app/payroll/paoroll/paoroll.component.html"),
            styles: [__webpack_require__(/*! ./paoroll.component.css */ "./src/app/payroll/paoroll/paoroll.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_pao_service__WEBPACK_IMPORTED_MODULE_3__["paoservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], PaorollComponent);
    return PaorollComponent;
}());



/***/ }),

/***/ "./src/app/payroll/payroll-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/payroll/payroll-routing.module.ts ***!
  \***************************************************/
/*! exports provided: PayrollRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollRoutingModule", function() { return PayrollRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _payroll_payroll_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../payroll/payroll.module */ "./src/app/payroll/payroll.module.ts");
/* harmony import */ var _ddoroll_ddoroll_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ddoroll/ddoroll.component */ "./src/app/payroll/ddoroll/ddoroll.component.ts");
/* harmony import */ var _paoroll_paoroll_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./paoroll/paoroll.component */ "./src/app/payroll/paoroll/paoroll.component.ts");
/* harmony import */ var _controllerrole_controllerrole_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./controllerrole/controllerrole.component */ "./src/app/payroll/controllerrole/controllerrole.component.ts");
/* harmony import */ var _usersbyddo_usersbyddo_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./usersbyddo/usersbyddo.component */ "./src/app/payroll/usersbyddo/usersbyddo.component.ts");
/* harmony import */ var _makerroll_makerroll_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./makerroll/makerroll.component */ "./src/app/payroll/makerroll/makerroll.component.ts");
/* harmony import */ var _hoo_checker_hoo_checker_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./hoo-checker/hoo-checker.component */ "./src/app/payroll/hoo-checker/hoo-checker.component.ts");
/* harmony import */ var _hoo_maker_hoo_maker_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./hoo-maker/hoo-maker.component */ "./src/app/payroll/hoo-maker/hoo-maker.component.ts");
/* harmony import */ var _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login-activate/login-activate.component */ "./src/app/payroll/login-activate/login-activate.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    {
        path: '', component: _payroll_payroll_module__WEBPACK_IMPORTED_MODULE_2__["PayrollModule"], data: {
            breadcrumb: 'User Management/ Role-User Management'
        }, children: [
            {
                path: 'controller', component: _controllerrole_controllerrole_component__WEBPACK_IMPORTED_MODULE_5__["ControllerroleComponent"], data: {
                    breadcrumb: 'Pre-defined Role (Controller)'
                }
            },
            {
                path: 'ddo', component: _ddoroll_ddoroll_component__WEBPACK_IMPORTED_MODULE_3__["DdorollComponent"], data: {
                    breadcrumb: 'Pre-defined Role (DDO)'
                }
            },
            {
                path: 'pao', component: _paoroll_paoroll_component__WEBPACK_IMPORTED_MODULE_4__["PaorollComponent"], data: {
                    breadcrumb: 'Pre-defined Role (PAO)'
                }
            },
            {
                path: 'usersbyddo', component: _usersbyddo_usersbyddo_component__WEBPACK_IMPORTED_MODULE_6__["UsersbyddoComponent"], data: {
                    breadcrumb: 'Pre-defined Role (DDO Checker)'
                }
            },
            {
                path: 'maker', component: _makerroll_makerroll_component__WEBPACK_IMPORTED_MODULE_7__["MakerrollComponent"], data: {
                    breadcrumb: 'Pre-defined Role (DDO Maker)'
                }
            },
            {
                path: 'hoochecker', component: _hoo_checker_hoo_checker_component__WEBPACK_IMPORTED_MODULE_8__["HOOCheckerComponent"], data: {
                    breadcrumb: 'Pre-defined Role (HOO Checker)'
                }
            },
            {
                path: 'hoomaker', component: _hoo_maker_hoo_maker_component__WEBPACK_IMPORTED_MODULE_9__["HOOMakerComponent"], data: {
                    breadcrumb: 'Pre-defined Role (HOO Maker)'
                }
            },
            { path: 'activate', component: _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_10__["LoginActivateComponent"] }
        ]
    }
];
var PayrollRoutingModule = /** @class */ (function () {
    function PayrollRoutingModule() {
    }
    PayrollRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PayrollRoutingModule);
    return PayrollRoutingModule;
}());



/***/ }),

/***/ "./src/app/payroll/payroll.module.ts":
/*!*******************************************!*\
  !*** ./src/app/payroll/payroll.module.ts ***!
  \*******************************************/
/*! exports provided: PayrollModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollModule", function() { return PayrollModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _payroll_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payroll-routing.module */ "./src/app/payroll/payroll-routing.module.ts");
/* harmony import */ var _ddoroll_ddoroll_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ddoroll/ddoroll.component */ "./src/app/payroll/ddoroll/ddoroll.component.ts");
/* harmony import */ var _paoroll_paoroll_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paoroll/paoroll.component */ "./src/app/payroll/paoroll/paoroll.component.ts");
/* harmony import */ var _controllerrole_controllerrole_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./controllerrole/controllerrole.component */ "./src/app/payroll/controllerrole/controllerrole.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _usersbyddo_usersbyddo_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./usersbyddo/usersbyddo.component */ "./src/app/payroll/usersbyddo/usersbyddo.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _makerroll_makerroll_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./makerroll/makerroll.component */ "./src/app/payroll/makerroll/makerroll.component.ts");
/* harmony import */ var _hoo_maker_hoo_maker_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./hoo-maker/hoo-maker.component */ "./src/app/payroll/hoo-maker/hoo-maker.component.ts");
/* harmony import */ var _hoo_checker_hoo_checker_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./hoo-checker/hoo-checker.component */ "./src/app/payroll/hoo-checker/hoo-checker.component.ts");
/* harmony import */ var _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./login-activate/login-activate.component */ "./src/app/payroll/login-activate/login-activate.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var PayrollModule = /** @class */ (function () {
    function PayrollModule() {
    }
    PayrollModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_ddoroll_ddoroll_component__WEBPACK_IMPORTED_MODULE_4__["DdorollComponent"], _paoroll_paoroll_component__WEBPACK_IMPORTED_MODULE_5__["PaorollComponent"], _controllerrole_controllerrole_component__WEBPACK_IMPORTED_MODULE_6__["ControllerroleComponent"], _usersbyddo_usersbyddo_component__WEBPACK_IMPORTED_MODULE_8__["UsersbyddoComponent"], _makerroll_makerroll_component__WEBPACK_IMPORTED_MODULE_11__["MakerrollComponent"], _hoo_maker_hoo_maker_component__WEBPACK_IMPORTED_MODULE_12__["HOOMakerComponent"], _hoo_checker_hoo_checker_component__WEBPACK_IMPORTED_MODULE_13__["HOOCheckerComponent"], _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_14__["LoginActivateComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _payroll_routing_module__WEBPACK_IMPORTED_MODULE_3__["PayrollRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__["NgxMatSelectSearchModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]
            ]
        })
    ], PayrollModule);
    return PayrollModule;
}());



/***/ }),

/***/ "./src/app/payroll/usersbyddo/usersbyddo.component.css":
/*!*************************************************************!*\
  !*** ./src/app/payroll/usersbyddo/usersbyddo.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".float-r {\r\n  float: right !important;\r\n}\r\n.mat-card {\r\n  transition: box-shadow 280ms cubic-bezier(.4,0,.2,1);\r\n  display: block;\r\n  position: relative;\r\n  padding: 16px;\r\n  border-radius: 4px;\r\n}\r\n.margin-bottom-9 {\r\n  margin-bottom: 9px\r\n}\r\n.EmptyMessage {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5cm9sbC91c2Vyc2J5ZGRvL3VzZXJzYnlkZG8uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHdCQUF3QjtDQUN6QjtBQUNEO0VBQ0UscURBQXFEO0VBQ3JELGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLG1CQUFtQjtDQUNwQjtBQUVEO0VBQ0Usa0JBQWtCO0NBQ25CO0FBRUQ7RUFDRSxXQUFXO0NBQ1oiLCJmaWxlIjoic3JjL2FwcC9wYXlyb2xsL3VzZXJzYnlkZG8vdXNlcnNieWRkby5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZsb2F0LXIge1xyXG4gIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXQtY2FyZCB7XHJcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyODBtcyBjdWJpYy1iZXppZXIoLjQsMCwuMiwxKTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZzogMTZweDtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuXHJcbi5tYXJnaW4tYm90dG9tLTkge1xyXG4gIG1hcmdpbi1ib3R0b206IDlweFxyXG59XHJcblxyXG4uRW1wdHlNZXNzYWdlIHtcclxuICBjb2xvcjogcmVkO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/payroll/usersbyddo/usersbyddo.component.html":
/*!**************************************************************!*\
  !*** ./src/app/payroll/usersbyddo/usersbyddo.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div *ngIf=\"datasourceEmp.length != 0\" class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"fom-title\">Assigned DDO Checker</div>\r\n          <table mat-table [dataSource]=\"datasourceEmp\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Unassign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <a class=\"material-icons i-dactivate\" matTooltip=\"Delete\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\"> cancel </a>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div *ngIf=\"datasourceEmp.length === 0\" style=\"color:red;\">No employee exists</div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <form class=\"rowdasa\">\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <div class=\"example-card mat-card\">\r\n          <div class=\"fom-title\">Employee List</div>\r\n          <mat-form-field>\r\n            <input matInput (keyup)=\"filter($event.target.value)\" placeholder=\"Search\">\r\n            <i class=\"material-icons icon-right\">search</i>\r\n          </mat-form-field>\r\n          <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n            <ng-container matColumnDef=\"EmpCode\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Emp Code</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"DDOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> DDO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.ddoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"PAOName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> PAO Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.paoName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Name </th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{element.empName}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"CommonDesigDesc\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.commonDesigDesc}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"Email\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Email</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{element.email}}</td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"ActiveDeactive\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Assign</th>\r\n              <td mat-cell *matCellDef=\"let element\">\r\n                <div *ngIf=\"checkStatus =='Yes'; else NotAssign\">\r\n                  <div *ngIf=\"element.activeStatus =='DDO Checkerf'; else Assign\">\r\n                    <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"fillPopup(element.empCd,element.empName,element.activeStatus);deactivatePopup = !deactivatePopup\">\r\n                      <i class=\"material-icons\">delete</i>\r\n                    </button>\r\n                  </div>\r\n                </div>\r\n                <ng-template #NotAssign>\r\n                  <a class=\"material-icons i-activate\" matTooltip=\"Edit\" (click)=\"fillPopup(element.empCd,element.empName,element.empDOB,element.activeStatus);ActivatePopup = !ActivatePopup\">check_circle_outline</a>\r\n                </ng-template>\r\n              </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n          </table>\r\n          <div [hidden]=\"!MessageUnassign\">\r\n            <font class=\"EmptyMessage\">No employee</font>\r\n          </div>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10]\" showFirstLastButtons ></mat-paginator>\r\n          <div *ngIf=\"SelfCheck\">           \r\n            <section class=\"example-section\">\r\n              <mat-checkbox class=\"example-margin\" name=\"checked\" (change)=\"SelfAssign(checked)\" [checked]=\"checked\" [(ngModel)]=\"checked\" style=\"font-weight:bolder\"><span style=\"font-weight:bolder; color:black\">Check the box to assign self as DDO checker</span></mat-checkbox>\r\n            </section>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"deactivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to unassign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"ActivatePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Do you want to assign?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Assigned();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/payroll/usersbyddo/usersbyddo.component.ts":
/*!************************************************************!*\
  !*** ./src/app/payroll/usersbyddo/usersbyddo.component.ts ***!
  \************************************************************/
/*! exports provided: UsersbyddoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersbyddoComponent", function() { return UsersbyddoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_UserManagement_usersbyddo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/UserManagement/usersbyddo.service */ "./src/app/services/UserManagement/usersbyddo.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsersbyddoComponent = /** @class */ (function () {
    function UsersbyddoComponent(_Service, commonMsg) {
        this._Service = _Service;
        this.commonMsg = commonMsg;
        this.EmpList = [];
        this.checkStatus = "NO";
        this.MessageDataFound = true;
        this.MessageUnassign = true;
        this.displayedColumns = ['EmpCode', 'DDOName', 'PAOName', 'EmpName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
    }
    UsersbyddoComponent.prototype.ngOnInit = function () {
        this.CheckedFlagSelfAssignDDOChecker();
        this.AssignedDDOCheckerEmpList();
        this.EmployeeListOfDDO();
    };
    UsersbyddoComponent.prototype.AssignedDDOCheckerEmpList = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.AssignedDDOCheckerEmpList(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.datasourceEmp = data;
            if (_this.datasourceEmp.length > 0) {
            }
            else {
                _this.SelfCheck = true;
            }
        });
    };
    UsersbyddoComponent.prototype.Assigned = function () {
        var _this = this;
        this.isLoading = true;
        this._Service.AssignedCMD(this.empCd, this.RoleID, sessionStorage.getItem('ddoid'), sessionStorage.getItem('UserID'), this.Active).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages != null) {
                _this.checkStatus = "NO";
                _this.EmployeeListOfDDO();
                _this.AssignedDDOCheckerEmpList();
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
                _this.ActivatePopup = false;
                _this.deactivatePopup = false;
            }
        });
    };
    UsersbyddoComponent.prototype.EmployeeListOfDDO = function () {
        var _this = this;
        this._Service.EmployeeListOfDDO(sessionStorage.getItem('ddoid')).subscribe(function (data) {
            _this.EmpList = data;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.EmpList.filter(function (emp) { return (emp.activeStatus != _this.commonMsg.DDOChecker) && (emp.activeStatus != _this.commonMsg.DDOMaker); }));
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.dataSource.data.length > 0) {
                _this.MessageUnassign = false;
                _this.SelfCheck = false;
            }
            else {
                _this.MessageUnassign = true;
                _this.SelfCheck = true;
            }
            _this.isLoading = false;
        });
    };
    UsersbyddoComponent.prototype.fillPopup = function (empCd, empName, activeStatus) {
        this.empCd = empCd;
        this.empName = empName;
        this.activeStatus = activeStatus;
        if (activeStatus == this.commonMsg.DDOChecker || activeStatus == this.commonMsg.DDOMaker) {
            this.Active = true;
        }
        else {
            this.Active = false;
        }
    };
    UsersbyddoComponent.prototype.filter = function (filter) {
        filter = filter.trim();
        filter = filter.toLocaleLowerCase();
        this.dataSource.filter = filter;
    };
    UsersbyddoComponent.prototype.SelfAssign = function (IsAssign) {
        var _this = this;
        this.checked = IsAssign;
        this._Service.SelfAssignDDOChecker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(function (result) {
            _this.Messages = result;
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default()(_this.Messages);
            _this.ActivatePopup = false;
            _this.deactivatePopup = false;
        });
    };
    UsersbyddoComponent.prototype.CheckedFlagSelfAssignDDOChecker = function () {
        var _this = this;
        this.checked = true;
        this._Service.SelfAssignCheck(sessionStorage.getItem('UserID')).subscribe(function (result) {
            _this.Messages = result;
            if (_this.Messages == '[]') {
                _this.checked = false;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], UsersbyddoComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], UsersbyddoComponent.prototype, "sort", void 0);
    UsersbyddoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-usersbyddo',
            template: __webpack_require__(/*! ./usersbyddo.component.html */ "./src/app/payroll/usersbyddo/usersbyddo.component.html"),
            styles: [__webpack_require__(/*! ./usersbyddo.component.css */ "./src/app/payroll/usersbyddo/usersbyddo.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_usersbyddo_service__WEBPACK_IMPORTED_MODULE_3__["usersbyddoservice"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], UsersbyddoComponent);
    return UsersbyddoComponent;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/ddo.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/UserManagement/ddo.service.ts ***!
  \********************************************************/
/*! exports provided: ddoservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ddoservice", function() { return ddoservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ddoservice = /** @class */ (function () {
    function ddoservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    //getAllControllerServiceclient(controllerID, Query) {
    //  // debugger;
    //  //return this.httpclient.get(this.BaseUrl + 'Controllerrole/GetAllControllers?controllerID=' + controllerID + '&Query=' + Query, {});
    //  return this.httpclient.get(this.config.getAllControllers + controllerID + '&Query=' + Query, {});
    //}
    ddoservice.prototype.GetAllDDO = function (PAOID, Query) {
        //debugger;
        // return this.httpclient.get(this.BaseUrl + 'ddorole/GetAllDDO?PAOID=' + PAOID + '&Query=' + Query, {});
        return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
    };
    ddoservice.prototype.EmployeeList = function (DDOID) {
        // debugger;
        //return this.httpclient.get(this.BaseUrl + 'ddorole/EmployeeList?DDOID=' + DDOID, {});
        return this.httpclient.get(this.config.EmployeeList + DDOID, {});
    };
    ddoservice.prototype.Assigned = function (empCd, DDOID, Active) {
        //debugger;
        //alert(Active)
        //return this.httpclient.get(this.BaseUrl + 'ddorole/Assigned?empCd=' + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
        return this.httpclient.get(this.config.Assigned + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
    };
    ddoservice.prototype.GetAllUserRoles = function (username) {
        //debugger;
        //return this.httpclient.get(this.BaseUrl + 'Controllerrole/GetAllUserRoles?username=' + username, {});
        return this.httpclient.get(this.config.GetAllUserRoles + username, {});
    };
    ddoservice.prototype.AssignedEmpDDO = function (DDOID) {
        return this.httpclient.get(this.config.AssignedEmpDDO + DDOID, {});
    };
    ddoservice.prototype.AssignedDDO = function (DDOID) {
        return this.httpclient.get(this.config.AssignedDDO + DDOID, {});
    };
    ddoservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], ddoservice);
    return ddoservice;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/hoo.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/UserManagement/hoo.service.ts ***!
  \********************************************************/
/*! exports provided: hooservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hooservice", function() { return hooservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var hooservice = /** @class */ (function () {
    function hooservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    hooservice.prototype.hooCheckerEmpList = function (DDOID) {
        return this.httpclient.get(this.config.HooCheckerEmpList + DDOID, {});
    };
    hooservice.prototype.AssignedHOOChecker = function (empCd, DDOID, Active) {
        return this.httpclient.get(this.config.AssignedHOOChecker + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
    };
    hooservice.prototype.hooMakerEmpList = function (DDOID) {
        return this.httpclient.get(this.config.hooMakerEmpList + DDOID, {});
    };
    hooservice.prototype.AssignedHOOMaker = function (empCd, DDOID, Active) {
        return this.httpclient.get(this.config.AssignedHOOMaker + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
    };
    hooservice.prototype.SelfAssignHOOChecker = function (UserID, username, EmpPermDDOId, ddoid, IsAssign) {
        return this.httpclient.get(this.config.SelfAssignHOOChecker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
    };
    hooservice.prototype.SelfAssignHOOCheckerRole = function (UserID) {
        return this.httpclient.get(this.config.SelfAssignHOOCheckerRole + UserID, { responseType: 'text' });
    };
    hooservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], hooservice);
    return hooservice;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/maker.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/UserManagement/maker.service.ts ***!
  \**********************************************************/
/*! exports provided: makerservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "makerservice", function() { return makerservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var makerservice = /** @class */ (function () {
    function makerservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    makerservice.prototype.makerEmpList = function (DDOID) {
        return this.httpclient.get(this.config.makerEmpList + DDOID, {});
    };
    makerservice.prototype.AssignedMakerEmpList = function (DDOID) {
        return this.httpclient.get(this.config.AssignedMakerEmpList + DDOID, {});
    };
    makerservice.prototype.AssignedMaker = function (empCd, DDOID, UserID, Active) {
        return this.httpclient.get(this.config.AssignedMaker + empCd + '&DDOID=' + DDOID + '&UserID=' + UserID + '&Active=' + Active, { responseType: 'text' });
    };
    makerservice.prototype.SelfAssignedDDOMaker = function (UserID, username, EmpPermDDOId, ddoid, IsAssign) {
        return this.httpclient.get(this.config.SelfAssignedDDOMaker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
    };
    makerservice.prototype.SelfAssignMakerRole = function (UserID) {
        return this.httpclient.get(this.config.SelfAssignMakerRole + UserID, { responseType: 'text' });
    };
    makerservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], makerservice);
    return makerservice;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/pao.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/UserManagement/pao.service.ts ***!
  \********************************************************/
/*! exports provided: paoservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "paoservice", function() { return paoservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var paoservice = /** @class */ (function () {
    function paoservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    paoservice.prototype.GetAllpaos = function (controllerID, Query) {
        return this.httpclient.get(this.config.GetAllpaos + controllerID + '&Query=' + Query, {});
    };
    paoservice.prototype.onpaoSelectchanged = function (MsPAOID) {
        return this.httpclient.get(this.config.onpaoSelectchanged + MsPAOID, {});
    };
    paoservice.prototype.GetAllDDO = function (PAOID, Query) {
        return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
    };
    paoservice.prototype.Assigned = function (empCd, MsPAOID, Active) {
        return this.httpclient.get(this.config.AssignedPAO + empCd + '&MsPAOID=' + MsPAOID + '&Active=' + Active, { responseType: 'text' });
    };
    paoservice.prototype.AssignedEmpList = function (MsPAOID) {
        return this.httpclient.get(this.config.AssignedEmpList + MsPAOID, {});
    };
    paoservice.prototype.AssignedPAOEmp = function (MsPAOID) {
        return this.httpclient.get(this.config.AssignedPAOEmp + MsPAOID, {});
    };
    paoservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], paoservice);
    return paoservice;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/usersbyddo.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/UserManagement/usersbyddo.service.ts ***!
  \***************************************************************/
/*! exports provided: usersbyddoservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usersbyddoservice", function() { return usersbyddoservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var usersbyddoservice = /** @class */ (function () {
    function usersbyddoservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    usersbyddoservice.prototype.GetAllpaos = function (controllerID, Query) {
        return this.httpclient.get(this.config.GetAllpaos + controllerID + '&Query=' + Query, {});
    };
    usersbyddoservice.prototype.onpaoSelectchanged = function (MsPAOID) {
        return this.httpclient.get(this.config.onpaoSelectchanged + MsPAOID, {});
    };
    usersbyddoservice.prototype.AssignedCMD = function (empCd, RoleID, DDOID, UserID, Active) {
        return this.httpclient.get(this.config.AssignedCMD + empCd + '&RoleID=' + RoleID + '&DDOID=' + DDOID + '&UserID=' + UserID + '&Active=' + Active, { responseType: 'text' });
    };
    usersbyddoservice.prototype.GetAllUserRolesByDDO = function (username) {
        return this.httpclient.get(this.config.GetAllUserRolesByDDO + username, {});
    };
    //For Assigned Employee
    usersbyddoservice.prototype.AssignedDDOCheckerEmpList = function (DDOID) {
        return this.httpclient.get(this.config.AssignedDDOCheckerEmpList + DDOID, {});
    };
    //For  Employee list
    usersbyddoservice.prototype.EmployeeListOfDDO = function (DDOID) {
        return this.httpclient.get(this.config.EmployeeListOfDDO + DDOID, {});
    };
    usersbyddoservice.prototype.AssignChecker = function (empCd, RoleID, DDOID, Active) {
        return this.httpclient.get(this.config.AssignChecker + empCd + '&RoleID=' + RoleID + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
    };
    //AssingedDDOCheckerMaker() {
    //  return this.httpclient.get(this.config.AssingedDDOCheckerMaker, {});
    //}
    usersbyddoservice.prototype.SelfAssignDDOChecker = function (UserID, username, EmpPermDDOId, ddoid, IsAssign) {
        return this.httpclient.get(this.config.SelfAssignDDOChecker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
    };
    usersbyddoservice.prototype.SelfAssignCheck = function (UserID) {
        return this.httpclient.get(this.config.SelfAssignCheck + UserID, { responseType: 'text' });
    };
    usersbyddoservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], usersbyddoservice);
    return usersbyddoservice;
}());



/***/ })

}]);
//# sourceMappingURL=payroll-payroll-module.js.map