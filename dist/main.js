(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../bill-group-mgmt/bill-group-mgmt.module": [
		"./src/app/bill-group-mgmt/bill-group-mgmt.module.ts",
		"common",
		"bill-group-mgmt-bill-group-mgmt-module"
	],
	"../bill-group-mgmt/ng-recovery/ng-recovery.module": [
		"./src/app/bill-group-mgmt/ng-recovery/ng-recovery.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"common",
		"bill-group-mgmt-ng-recovery-ng-recovery-module"
	],
	"../change/change.module": [
		"./src/app/change/change.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"common",
		"change-change-module"
	],
	"../commondesign/commondesign.module": [
		"./src/app/commondesign/commondesign.module.ts",
		"commondesign-commondesign-module"
	],
	"../deputation/deputation.module": [
		"./src/app/deputation/deputation.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"default~deputation-deputation-module~employee-employee-module",
		"common"
	],
	"../employee/employee.module": [
		"./src/app/employee/employee.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"default~deputation-deputation-module~employee-employee-module",
		"default~employee-employee-module~role-managment-role-managment-module",
		"common",
		"employee-employee-module"
	],
	"../endofservice/endofservice.module": [
		"./src/app/endofservice/endofservice.module.ts",
		"common",
		"endofservice-endofservice-module"
	],
	"../income-tax/income-tax.module": [
		"./src/app/income-tax/income-tax.module.ts",
		"income-tax-income-tax-module"
	],
	"../increment/increment.module": [
		"./src/app/increment/increment.module.ts",
		"common",
		"increment-increment-module"
	],
	"../leaves-mgmt/leaves-mgmt.module": [
		"./src/app/leaves-mgmt/leaves-mgmt.module.ts",
		"default~leaves-mgmt-leaves-mgmt-module~masters-masters-module~rejoiningofemployee-rejoiningofemploye~b86703c9",
		"common",
		"leaves-mgmt-leaves-mgmt-module"
	],
	"../lienperiod/lienperiod.module": [
		"./src/app/lienperiod/lienperiod.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"common",
		"lienperiod-lienperiod-module"
	],
	"../loanmgt/loanmgt.module": [
		"./src/app/loanmgt/loanmgt.module.ts",
		"default~bill-group-mgmt-ng-recovery-ng-recovery-module~change-change-module~deputation-deputation-mo~5d29c7cc",
		"common"
	],
	"../masters/masters.module": [
		"./src/app/masters/masters.module.ts",
		"default~leaves-mgmt-leaves-mgmt-module~masters-masters-module~rejoiningofemployee-rejoiningofemploye~b86703c9",
		"common",
		"masters-masters-module"
	],
	"../myprofile/myprofile.module": [
		"./src/app/myprofile/myprofile.module.ts",
		"myprofile-myprofile-module"
	],
	"../onboarding/onboarding.module": [
		"./src/app/onboarding/onboarding.module.ts",
		"onboarding-onboarding-module"
	],
	"../payroll/payroll.module": [
		"./src/app/payroll/payroll.module.ts",
		"common",
		"payroll-payroll-module"
	],
	"../promotion/promotion.module": [
		"./src/app/promotion/promotion.module.ts",
		"common",
		"promotion-promotion-module"
	],
	"../recovery-excess-payment/recovery-excess-payment.module": [
		"./src/app/recovery-excess-payment/recovery-excess-payment.module.ts",
		"common",
		"recovery-excess-payment-recovery-excess-payment-module"
	],
	"../recovery-non-epspayment/recovery-non-epspayment.module": [
		"./src/app/recovery-non-epspayment/recovery-non-epspayment.module.ts",
		"common",
		"recovery-non-epspayment-recovery-non-epspayment-module"
	],
	"../rejoiningofemployee/rejoiningofemployee.module": [
		"./src/app/rejoiningofemployee/rejoiningofemployee.module.ts",
		"default~leaves-mgmt-leaves-mgmt-module~masters-masters-module~rejoiningofemployee-rejoiningofemploye~b86703c9",
		"common",
		"rejoiningofemployee-rejoiningofemployee-module"
	],
	"../reports/reports.module": [
		"./src/app/reports/reports.module.ts",
		"reports-reports-module"
	],
	"../role-managment/role-managment.module": [
		"./src/app/role-managment/role-managment.module.ts",
		"default~leaves-mgmt-leaves-mgmt-module~masters-masters-module~rejoiningofemployee-rejoiningofemploye~b86703c9",
		"default~employee-employee-module~role-managment-role-managment-module",
		"default~dashboard-dashboard-module~role-managment-role-managment-module",
		"common",
		"role-managment-role-managment-module"
	],
	"../suspension/suspension.module": [
		"./src/app/suspension/suspension.module.ts",
		"suspension-suspension-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/dashboard/dashboard.module.ts",
		"default~dashboard-dashboard-module~role-managment-role-managment-module",
		"common",
		"dashboard-dashboard-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-header></app-header> -->\r\n<!-- <mat-sidenav-container class=\"example-container\" autosize>\r\n    <mat-sidenav class=\"example-sidenav\" #sideNav mode=\"side\" opened=\"true\">\r\n      <mat-nav-list>\r\n        <app-nav-menu *ngFor=\"let item of navItems\" [item]=\"item\" class=\"mg_b\" >\r\n        </app-nav-menu>\r\n      </mat-nav-list>\r\n    </mat-sidenav>\r\n  -->\r\n<!-- <div class='main-panel body-content main-background'> -->\r\n<!-- <button mat-icon-button (click)=\"sideNav.toggle()\">\r\n    <mat-icon class=\"material-icons md-24\">menu</mat-icon>\r\n  </button>￼\r\n  <ul class=\"breadcrumb\">\r\n      <li><a href=\"#\">Home</a></li>\r\n      <li>Dashboard</li>\r\n    </ul>\r\n     -->\r\n\r\n\r\n<router-outlet></router-outlet>\r\n<!-- </mat-sidenav-container>  -->\r\n\r\n<div #spinnerElement style=\"display:none;\">\r\n  <div class=\"loading\">\r\n    <h2>Loading...</h2>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-sidenav {\n  width: 260px; }\n\n.mat-sidenav-container {\n  background-color: white !important;\n  height: 100vh; }\n\n.sidenav-toolbar {\n  height: 64px;\n  background-color: #3f51b5;\n  display: flex;\n  flex-direction: row; }\n\n@media (max-width: 600px) {\n    .sidenav-toolbar {\n      height: 56px; } }\n\n.mat-nav-list {\n  padding-top: 0; }\n\n.fixed-topnav {\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 2;\n  width: 100% !important; }\n\n.mat-drawer-container,\n.mat-drawer {\n  padding-top: 56px; }\n\n.menu-list-item.mat-list-item .mat-list-item-content {\n  flex: 1 1 100%;\n  font-size: 94%; }\n\n.version-info {\n  font-size: 8pt;\n  float: right;\n  padding: 8px; }\n\n/*breadcrumb*/\n\nul.breadcrumb {\n  width: calc(100% - 100px);\n  float: right;\n  background: #fff;\n  margin-top: 4px; }\n\n/*pramod*/\n\n.loading-bar {\n  position: fixed;\n  z-index: 10000;\n  top: 0px;\n  left: 0px;\n  background-color: rgba(255, 255, 255, 0.97);\n  width: 100%;\n  height: 100%;\n  filter: Alpha(Opacity=60);\n  opacity: 0.6;\n  -moz-opacity: 0.6;\n  /* <img class=\"logo\" src=\"assets/images/loading\" /> */ }\n\n.modalprogress1 {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 100px;\n  height: 100px;\n  margin-left: -50px;\n  margin-top: -50px;\n  color: #990000;\n  font-weight: bold;\n  font-size: 14px; }\n\n.loading {\n  position: fixed;\n  height: 100%;\n  width: 100%;\n  background: #fff;\n  text-align: center;\n  padding-top: 20%; }\n\n.loading > h2 {\n  color: #ccc;\n  margin: 0;\n  font: .8em verdana;\n  text-transform: uppercase;\n  letter-spacing: .1em; }\n\n/*\r\n * Loading Dots\r\n * Can we use pseudo elements here instead :after?\r\n */\n\n.loading span {\n  display: inline-block;\n  vertical-align: middle;\n  width: .6em;\n  height: .6em;\n  margin: .19em;\n  background: #007DB6;\n  border-radius: .6em;\n  -webkit-animation: loading 1s infinite alternate;\n  animation: loading 1s infinite alternate; }\n\n/*\r\n * Dots Colors\r\n * Smarter targeting vs nth-of-type?\r\n */\n\n.loading span:nth-of-type(2) {\n  background: #008FB2;\n  -webkit-animation-delay: 0.2s;\n  animation-delay: 0.2s; }\n\n.loading span:nth-of-type(3) {\n  background: #009B9E;\n  -webkit-animation-delay: 0.4s;\n  animation-delay: 0.4s; }\n\n.loading span:nth-of-type(4) {\n  background: #00A77D;\n  -webkit-animation-delay: 0.6s;\n  animation-delay: 0.6s; }\n\n.loading span:nth-of-type(5) {\n  background: #00B247;\n  -webkit-animation-delay: 0.8s;\n  animation-delay: 0.8s; }\n\n.loading span:nth-of-type(6) {\n  background: #5AB027;\n  -webkit-animation-delay: 1.0s;\n  animation-delay: 1.0s; }\n\n.loading span:nth-of-type(7) {\n  background: #A0B61E;\n  -webkit-animation-delay: 1.2s;\n  animation-delay: 1.2s; }\n\n@media (max-width: 767px) {\n  ._fxContainer {\n    position: relative;\n    width: auto;\n    height: auto; }\n  .mat-calendar {\n    width: 240px !important; }\n  .container h2::after {\n    width: 0px;\n    height: 0px;\n    background: #ffffff; }\n  .loading {\n    padding-top: 75%; } }\n\n/*\r\n * Animation keyframes\r\n * Use transition opacity instead of keyframes?\r\n */\n\n@-webkit-keyframes loading {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n@keyframes loading {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n.loader {\n  position: absolute;\n  height: 50%;\n  width: 100%;\n  background: #000;\n  text-align: center;\n  padding-top: 10%;\n  opacity: 0.5; }\n\n.loader > h3 {\n  color: #ccc;\n  margin: 0;\n  font: .8em verdana;\n  text-transform: uppercase;\n  letter-spacing: .1em; }\n\n/*\r\n * Loading Dots\r\n * Can we use pseudo elements here instead :after?\r\n */\n\n.loader span {\n  display: inline-block;\n  vertical-align: middle;\n  width: .6em;\n  height: .6em;\n  margin: .19em;\n  background: #007DB6;\n  border-radius: .6em;\n  -webkit-animation: loader 1s infinite alternate;\n  animation: loader 1s infinite alternate; }\n\n/*\r\n * Dots Colors\r\n * Smarter targeting vs nth-of-type?\r\n */\n\n.loader span:nth-of-type(2) {\n  background: #008FB2;\n  -webkit-animation-delay: 0.2s;\n  animation-delay: 0.2s; }\n\n.loader span:nth-of-type(3) {\n  background: #009B9E;\n  -webkit-animation-delay: 0.4s;\n  animation-delay: 0.4s; }\n\n.loader span:nth-of-type(4) {\n  background: #00A77D;\n  -webkit-animation-delay: 0.6s;\n  animation-delay: 0.6s; }\n\n.loader span:nth-of-type(5) {\n  background: #00B247;\n  -webkit-animation-delay: 0.8s;\n  animation-delay: 0.8s; }\n\n.loader span:nth-of-type(6) {\n  background: #5AB027;\n  -webkit-animation-delay: 1.0s;\n  animation-delay: 1.0s; }\n\n.loader span:nth-of-type(7) {\n  background: #A0B61E;\n  -webkit-animation-delay: 1.2s;\n  animation-delay: 1.2s; }\n\na {\n  text-decoration: none;\n  color: #666666; }\n\ninput[readonly],\ntextarea[readonly] {\n  color: gray; }\n\n.copyright {\n  color: #555;\n  font-size: 12px;\n  text-align: center;\n  position: absolute;\n  bottom: 0px;\n  text-align: center;\n  width: 100%; }\n\n.featresAccordion .slimscroll-wrapper {\n  width: 100% !important; }\n\n.mat-accent .mat-slider-thumb, .mat-accent .mat-slider-thumb-label, .mat-accent .mat-slider-track-fill {\n  background-color: #419bf9 !important; }\n\n/*\r\n * Animation keyframes\r\n * Use transition opacity instead of keyframes?\r\n */\n\n@-webkit-keyframes loader {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n@keyframes loader {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTTpcXGdpdEVQU1xcZXBzYXBwXFxFUFNcXEVQUy5XZWJBcHBcXENsaWVudEFwcC9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBWSxFQUNiOztBQUNEO0VBQ0UsbUNBQWtDO0VBQ2xDLGNBQWEsRUFDZDs7QUFDRDtFQUNFLGFBQVk7RUFDWiwwQkFBeUI7RUFDekIsY0FBYTtFQUNiLG9CQUFtQixFQUtwQjs7QUFIQztJQU5GO01BT0ksYUFBWSxFQUVmLEVBQUE7O0FBQ0Q7RUFDRSxlQUFjLEVBQ2Y7O0FBRUM7RUFDRSxnQkFBZTtFQUNmLE9BQU07RUFDTixRQUFPO0VBQ1AsV0FBVTtFQUNWLHVCQUFzQixFQUN2Qjs7QUFFRDs7RUFFRSxrQkFBaUIsRUFDbEI7O0FBRUg7RUFFSSxlQUFjO0VBQ2QsZUFBYyxFQUNmOztBQUVIO0VBQ0UsZUFBYztFQUNkLGFBQVk7RUFDWixhQUFZLEVBQ2I7O0FBRUQsY0FBYzs7QUFDZDtFQUFnQiwwQkFBeUI7RUFBRSxhQUFZO0VBQUUsaUJBQWdCO0VBQUUsZ0JBQWUsRUFBRzs7QUFFN0YsVUFBVTs7QUFFVjtFQUNFLGdCQUFlO0VBQ2YsZUFBYztFQUNkLFNBQVE7RUFDUixVQUFTO0VBQ1QsNENBQTJDO0VBQzNDLFlBQVc7RUFDWCxhQUFZO0VBQ1osMEJBQXlCO0VBQ3pCLGFBQVk7RUFDWixrQkFBaUI7RUFDakIsc0RBQXNELEVBQ3ZEOztBQUVEO0VBQ0UsbUJBQWtCO0VBQ2xCLFNBQVE7RUFDUixVQUFTO0VBQ1QsYUFBWTtFQUNaLGNBQWE7RUFDYixtQkFBa0I7RUFDbEIsa0JBQWlCO0VBQ2pCLGVBQWM7RUFDZCxrQkFBaUI7RUFDakIsZ0JBQWUsRUFDaEI7O0FBRUQ7RUFDRSxnQkFBZTtFQUNmLGFBQVk7RUFDWixZQUFXO0VBQ1gsaUJBQWdCO0VBQ2hCLG1CQUFrQjtFQUNsQixpQkFBZ0IsRUFDakI7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsVUFBUztFQUNULG1CQUFrQjtFQUNsQiwwQkFBeUI7RUFDekIscUJBQW9CLEVBQ3JCOztBQUVEOzs7R0FHRzs7QUFDSDtFQUNFLHNCQUFxQjtFQUNyQix1QkFBc0I7RUFDdEIsWUFBVztFQUNYLGFBQVk7RUFDWixjQUFhO0VBQ2Isb0JBQW1CO0VBQ25CLG9CQUFtQjtFQUNuQixpREFBZ0Q7RUFDaEQseUNBQXdDLEVBQ3pDOztBQUVEOzs7R0FHRzs7QUFDSDtFQUNFLG9CQUFtQjtFQUNuQiw4QkFBNkI7RUFDN0Isc0JBQXFCLEVBQ3RCOztBQUVEO0VBQ0Usb0JBQW1CO0VBQ25CLDhCQUE2QjtFQUM3QixzQkFBcUIsRUFDdEI7O0FBRUQ7RUFDRSxvQkFBbUI7RUFDbkIsOEJBQTZCO0VBQzdCLHNCQUFxQixFQUN0Qjs7QUFFRDtFQUNFLG9CQUFtQjtFQUNuQiw4QkFBNkI7RUFDN0Isc0JBQXFCLEVBQ3RCOztBQUVEO0VBQ0Usb0JBQW1CO0VBQ25CLDhCQUE2QjtFQUM3QixzQkFBcUIsRUFDdEI7O0FBRUQ7RUFDRSxvQkFBbUI7RUFDbkIsOEJBQTZCO0VBQzdCLHNCQUFxQixFQUN0Qjs7QUFFRDtFQUNFO0lBQ0UsbUJBQWtCO0lBQ2xCLFlBQVc7SUFDWCxhQUFZLEVBQ2I7RUFFRDtJQUNFLHdCQUF1QixFQUN4QjtFQUVEO0lBQ0UsV0FBVTtJQUNWLFlBQVc7SUFDWCxvQkFBbUIsRUFDcEI7RUFFRDtJQUNFLGlCQUFnQixFQUNqQixFQUFBOztBQUVIOzs7R0FHRzs7QUFDSDtFQUNFO0lBQ0UsV0FBVSxFQUFBO0VBR1o7SUFDRSxXQUFVLEVBQUEsRUFBQTs7QUFJZDtFQUNFO0lBQ0UsV0FBVSxFQUFBO0VBR1o7SUFDRSxXQUFVLEVBQUEsRUFBQTs7QUFJZDtFQUNFLG1CQUFrQjtFQUNsQixZQUFXO0VBQ1gsWUFBVztFQUNYLGlCQUFnQjtFQUNoQixtQkFBa0I7RUFDbEIsaUJBQWdCO0VBQ2hCLGFBQVksRUFDYjs7QUFFRDtFQUNFLFlBQVc7RUFDWCxVQUFTO0VBQ1QsbUJBQWtCO0VBQ2xCLDBCQUF5QjtFQUN6QixxQkFBb0IsRUFDckI7O0FBRUQ7OztHQUdHOztBQUNIO0VBQ0Usc0JBQXFCO0VBQ3JCLHVCQUFzQjtFQUN0QixZQUFXO0VBQ1gsYUFBWTtFQUNaLGNBQWE7RUFDYixvQkFBbUI7RUFDbkIsb0JBQW1CO0VBQ25CLGdEQUErQztFQUMvQyx3Q0FBdUMsRUFDeEM7O0FBRUQ7OztHQUdHOztBQUNIO0VBQ0Usb0JBQW1CO0VBQ25CLDhCQUE2QjtFQUM3QixzQkFBcUIsRUFDdEI7O0FBRUQ7RUFDRSxvQkFBbUI7RUFDbkIsOEJBQTZCO0VBQzdCLHNCQUFxQixFQUN0Qjs7QUFFRDtFQUNFLG9CQUFtQjtFQUNuQiw4QkFBNkI7RUFDN0Isc0JBQXFCLEVBQ3RCOztBQUVEO0VBQ0Usb0JBQW1CO0VBQ25CLDhCQUE2QjtFQUM3QixzQkFBcUIsRUFDdEI7O0FBRUQ7RUFDRSxvQkFBbUI7RUFDbkIsOEJBQTZCO0VBQzdCLHNCQUFxQixFQUN0Qjs7QUFFRDtFQUNFLG9CQUFtQjtFQUNuQiw4QkFBNkI7RUFDN0Isc0JBQXFCLEVBQ3RCOztBQUdEO0VBQ0Usc0JBQXFCO0VBQ3JCLGVBQWMsRUFDZjs7QUFFRDs7RUFFRSxZQUFXLEVBQ1o7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsZ0JBQWU7RUFDZixtQkFBa0I7RUFDbEIsbUJBQWtCO0VBQ2xCLFlBQVc7RUFDWCxtQkFBa0I7RUFDbEIsWUFBVyxFQUNaOztBQUVEO0VBQ0UsdUJBQXNCLEVBQ3ZCOztBQUVEO0VBQ0UscUNBQW9DLEVBQ3JDOztBQUNEOzs7R0FHRzs7QUFDSDtFQUNFO0lBQ0UsV0FBVSxFQUFBO0VBR1o7SUFDRSxXQUFVLEVBQUEsRUFBQTs7QUFJZDtFQUNFO0lBQ0UsV0FBVSxFQUFBO0VBR1o7SUFDRSxXQUFVLEVBQUEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtc2lkZW5hdiB7XHJcbiAgd2lkdGg6IDI2MHB4O1xyXG59XHJcbi5tYXQtc2lkZW5hdi1jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxufVxyXG4uc2lkZW5hdi10b29sYmFyIHtcclxuICBoZWlnaHQ6IDY0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNmNTFiNTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgaGVpZ2h0OiA1NnB4O1xyXG4gIH1cclxufVxyXG4ubWF0LW5hdi1saXN0IHtcclxuICBwYWRkaW5nLXRvcDogMDtcclxufVxyXG5cclxuICAuZml4ZWQtdG9wbmF2IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OiAyO1xyXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5tYXQtZHJhd2VyLWNvbnRhaW5lcixcclxuICAubWF0LWRyYXdlciB7XHJcbiAgICBwYWRkaW5nLXRvcDogNTZweDtcclxuICB9XHJcblxyXG4ubWVudS1saXN0LWl0ZW0ubWF0LWxpc3QtaXRlbSB7XHJcbiAgLm1hdC1saXN0LWl0ZW0tY29udGVudCB7XHJcbiAgICBmbGV4OiAxIDEgMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogOTQlO1xyXG4gIH1cclxufVxyXG4udmVyc2lvbi1pbmZvIHtcclxuICBmb250LXNpemU6IDhwdDtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgcGFkZGluZzogOHB4O1xyXG59XHJcblxyXG4vKmJyZWFkY3J1bWIqL1xyXG51bC5icmVhZGNydW1iIHsgd2lkdGg6IGNhbGMoMTAwJSAtIDEwMHB4KTsgZmxvYXQ6IHJpZ2h0OyBiYWNrZ3JvdW5kOiAjZmZmOyBtYXJnaW4tdG9wOiA0cHg7fVxyXG5cclxuLypwcmFtb2QqL1xyXG5cclxuLmxvYWRpbmctYmFyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMTAwMDA7XHJcbiAgdG9wOiAwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC45Nyk7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZpbHRlcjogQWxwaGEoT3BhY2l0eT02MCk7XHJcbiAgb3BhY2l0eTogMC42O1xyXG4gIC1tb3otb3BhY2l0eTogMC42O1xyXG4gIC8qIDxpbWcgY2xhc3M9XCJsb2dvXCIgc3JjPVwiYXNzZXRzL2ltYWdlcy9sb2FkaW5nXCIgLz4gKi9cclxufVxyXG5cclxuLm1vZGFscHJvZ3Jlc3MxIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA1MCU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtNTBweDtcclxuICBtYXJnaW4tdG9wOiAtNTBweDtcclxuICBjb2xvcjogIzk5MDAwMDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuXHJcbi5sb2FkaW5nIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctdG9wOiAyMCU7XHJcbn1cclxuXHJcbi5sb2FkaW5nID4gaDIge1xyXG4gIGNvbG9yOiAjY2NjO1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250OiAuOGVtIHZlcmRhbmE7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBsZXR0ZXItc3BhY2luZzogLjFlbTtcclxufVxyXG5cclxuLypcclxuICogTG9hZGluZyBEb3RzXHJcbiAqIENhbiB3ZSB1c2UgcHNldWRvIGVsZW1lbnRzIGhlcmUgaW5zdGVhZCA6YWZ0ZXI/XHJcbiAqL1xyXG4ubG9hZGluZyBzcGFuIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICB3aWR0aDogLjZlbTtcclxuICBoZWlnaHQ6IC42ZW07XHJcbiAgbWFyZ2luOiAuMTllbTtcclxuICBiYWNrZ3JvdW5kOiAjMDA3REI2O1xyXG4gIGJvcmRlci1yYWRpdXM6IC42ZW07XHJcbiAgLXdlYmtpdC1hbmltYXRpb246IGxvYWRpbmcgMXMgaW5maW5pdGUgYWx0ZXJuYXRlO1xyXG4gIGFuaW1hdGlvbjogbG9hZGluZyAxcyBpbmZpbml0ZSBhbHRlcm5hdGU7XHJcbn1cclxuXHJcbi8qXHJcbiAqIERvdHMgQ29sb3JzXHJcbiAqIFNtYXJ0ZXIgdGFyZ2V0aW5nIHZzIG50aC1vZi10eXBlP1xyXG4gKi9cclxuLmxvYWRpbmcgc3BhbjpudGgtb2YtdHlwZSgyKSB7XHJcbiAgYmFja2dyb3VuZDogIzAwOEZCMjtcclxuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogMC4ycztcclxuICBhbmltYXRpb24tZGVsYXk6IDAuMnM7XHJcbn1cclxuXHJcbi5sb2FkaW5nIHNwYW46bnRoLW9mLXR5cGUoMykge1xyXG4gIGJhY2tncm91bmQ6ICMwMDlCOUU7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDAuNHM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAwLjRzO1xyXG59XHJcblxyXG4ubG9hZGluZyBzcGFuOm50aC1vZi10eXBlKDQpIHtcclxuICBiYWNrZ3JvdW5kOiAjMDBBNzdEO1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAwLjZzO1xyXG4gIGFuaW1hdGlvbi1kZWxheTogMC42cztcclxufVxyXG5cclxuLmxvYWRpbmcgc3BhbjpudGgtb2YtdHlwZSg1KSB7XHJcbiAgYmFja2dyb3VuZDogIzAwQjI0NztcclxuICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogMC44cztcclxuICBhbmltYXRpb24tZGVsYXk6IDAuOHM7XHJcbn1cclxuXHJcbi5sb2FkaW5nIHNwYW46bnRoLW9mLXR5cGUoNikge1xyXG4gIGJhY2tncm91bmQ6ICM1QUIwMjc7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDEuMHM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAxLjBzO1xyXG59XHJcblxyXG4ubG9hZGluZyBzcGFuOm50aC1vZi10eXBlKDcpIHtcclxuICBiYWNrZ3JvdW5kOiAjQTBCNjFFO1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAxLjJzO1xyXG4gIGFuaW1hdGlvbi1kZWxheTogMS4ycztcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgLl9meENvbnRhaW5lciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIGhlaWdodDogYXV0bztcclxuICB9XHJcblxyXG4gIC5tYXQtY2FsZW5kYXIge1xyXG4gICAgd2lkdGg6IDI0MHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAuY29udGFpbmVyIGgyOjphZnRlciB7XHJcbiAgICB3aWR0aDogMHB4O1xyXG4gICAgaGVpZ2h0OiAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gIH1cclxuXHJcbiAgLmxvYWRpbmcge1xyXG4gICAgcGFkZGluZy10b3A6IDc1JTtcclxuICB9XHJcbn1cclxuLypcclxuICogQW5pbWF0aW9uIGtleWZyYW1lc1xyXG4gKiBVc2UgdHJhbnNpdGlvbiBvcGFjaXR5IGluc3RlYWQgb2Yga2V5ZnJhbWVzP1xyXG4gKi9cclxuQC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmcge1xyXG4gIDAlIHtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG5cclxuICAxMDAlIHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGxvYWRpbmcge1xyXG4gIDAlIHtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG5cclxuICAxMDAlIHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfVxyXG59XHJcblxyXG4ubG9hZGVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgaGVpZ2h0OiA1MCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogIzAwMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy10b3A6IDEwJTtcclxuICBvcGFjaXR5OiAwLjU7XHJcbn1cclxuXHJcbi5sb2FkZXIgPiBoMyB7XHJcbiAgY29sb3I6ICNjY2M7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGZvbnQ6IC44ZW0gdmVyZGFuYTtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIGxldHRlci1zcGFjaW5nOiAuMWVtO1xyXG59XHJcblxyXG4vKlxyXG4gKiBMb2FkaW5nIERvdHNcclxuICogQ2FuIHdlIHVzZSBwc2V1ZG8gZWxlbWVudHMgaGVyZSBpbnN0ZWFkIDphZnRlcj9cclxuICovXHJcbi5sb2FkZXIgc3BhbiB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgd2lkdGg6IC42ZW07XHJcbiAgaGVpZ2h0OiAuNmVtO1xyXG4gIG1hcmdpbjogLjE5ZW07XHJcbiAgYmFja2dyb3VuZDogIzAwN0RCNjtcclxuICBib3JkZXItcmFkaXVzOiAuNmVtO1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBsb2FkZXIgMXMgaW5maW5pdGUgYWx0ZXJuYXRlO1xyXG4gIGFuaW1hdGlvbjogbG9hZGVyIDFzIGluZmluaXRlIGFsdGVybmF0ZTtcclxufVxyXG5cclxuLypcclxuICogRG90cyBDb2xvcnNcclxuICogU21hcnRlciB0YXJnZXRpbmcgdnMgbnRoLW9mLXR5cGU/XHJcbiAqL1xyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoMikge1xyXG4gIGJhY2tncm91bmQ6ICMwMDhGQjI7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDAuMnM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAwLjJzO1xyXG59XHJcblxyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoMykge1xyXG4gIGJhY2tncm91bmQ6ICMwMDlCOUU7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDAuNHM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAwLjRzO1xyXG59XHJcblxyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoNCkge1xyXG4gIGJhY2tncm91bmQ6ICMwMEE3N0Q7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDAuNnM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAwLjZzO1xyXG59XHJcblxyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoNSkge1xyXG4gIGJhY2tncm91bmQ6ICMwMEIyNDc7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDAuOHM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAwLjhzO1xyXG59XHJcblxyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoNikge1xyXG4gIGJhY2tncm91bmQ6ICM1QUIwMjc7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDEuMHM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAxLjBzO1xyXG59XHJcblxyXG4ubG9hZGVyIHNwYW46bnRoLW9mLXR5cGUoNykge1xyXG4gIGJhY2tncm91bmQ6ICNBMEI2MUU7XHJcbiAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDEuMnM7XHJcbiAgYW5pbWF0aW9uLWRlbGF5OiAxLjJzO1xyXG59XHJcblxyXG5cclxuYSB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGNvbG9yOiAjNjY2NjY2O1xyXG59XHJcblxyXG5pbnB1dFtyZWFkb25seV0sXHJcbnRleHRhcmVhW3JlYWRvbmx5XSB7XHJcbiAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi5jb3B5cmlnaHQge1xyXG4gIGNvbG9yOiAjNTU1O1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmZlYXRyZXNBY2NvcmRpb24gLnNsaW1zY3JvbGwtd3JhcHBlciB7XHJcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1hY2NlbnQgLm1hdC1zbGlkZXItdGh1bWIsIC5tYXQtYWNjZW50IC5tYXQtc2xpZGVyLXRodW1iLWxhYmVsLCAubWF0LWFjY2VudCAubWF0LXNsaWRlci10cmFjay1maWxsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDE5YmY5ICFpbXBvcnRhbnQ7XHJcbn1cclxuLypcclxuICogQW5pbWF0aW9uIGtleWZyYW1lc1xyXG4gKiBVc2UgdHJhbnNpdGlvbiBvcGFjaXR5IGluc3RlYWQgb2Yga2V5ZnJhbWVzP1xyXG4gKi9cclxuQC13ZWJraXQta2V5ZnJhbWVzIGxvYWRlciB7XHJcbiAgMCUge1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcblxyXG4gIDEwMCUge1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgbG9hZGVyIHtcclxuICAwJSB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuXHJcbiAgMTAwJSB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(router, ngZone, renderer, titleService) {
        var _this = this;
        this.router = router;
        this.ngZone = ngZone;
        this.renderer = renderer;
        this.titleService = titleService;
        router.events.subscribe(function (event) {
            _this._navigationInterceptor(event);
            //    this.titleService.setTitle('EPS' + ':' + event);
            // this.titleService.setTitle('EPS' + ':' + sessionStorage.getItem('menudisplayName').split('/')[1]);
        });
    }
    AppComponent.prototype._navigationInterceptor = function (event) {
        var _this = this;
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
            this.ngZone.runOutsideAngular(function () {
                _this.renderer.setElementStyle(_this.spinnerElement.nativeElement, 'display', 'inline');
            });
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
            this._hideSpinner();
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationCancel"]) {
            this._hideSpinner();
        }
        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationError"]) {
            this._hideSpinner();
        }
    };
    AppComponent.prototype._hideSpinner = function () {
        var _this = this;
        this.ngZone.runOutsideAngular(function () {
            _this.renderer.setElementStyle(_this.spinnerElement.nativeElement, 'display', 'none');
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('spinnerElement'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AppComponent.prototype, "spinnerElement", void 0);
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _authguard_authguard_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authguard/authguard.guard */ "./src/app/authguard/authguard.guard.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm5/tree.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _logintoken_logintoken_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./logintoken/logintoken.component */ "./src/app/logintoken/logintoken.component.ts");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _global_error_interceptor__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./global/error.interceptor */ "./src/app/global/error.interceptor.ts");
/* harmony import */ var _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./changepassword/changepassword.component */ "./src/app/changepassword/changepassword.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./login-activate/login-activate.component */ "./src/app/login-activate/login-activate.component.ts");
/* harmony import */ var _login_loginservice__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./login/loginservice */ "./src/app/login/loginservice.ts");
/* harmony import */ var _new_onboarding_new_onboarding_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./new-onboarding/new-onboarding.component */ "./src/app/new-onboarding/new-onboarding.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _logintoken_httpInterceptor__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./logintoken/httpInterceptor */ "./src/app/logintoken/httpInterceptor.ts");
/* harmony import */ var _new_on_boarding_status_new_on_boarding_status_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./new-on-boarding-status/new-on-boarding-status.component */ "./src/app/new-on-boarding-status/new-on-boarding-status.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _logintoken_logintoken_component__WEBPACK_IMPORTED_MODULE_13__["LogintokenComponent"],
                _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_16__["ChangepasswordComponent"],
                _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_18__["LoginActivateComponent"],
                _new_onboarding_new_onboarding_component__WEBPACK_IMPORTED_MODULE_20__["NewOnboardingComponent"],
                _new_on_boarding_status_new_on_boarding_status_component__WEBPACK_IMPORTED_MODULE_23__["NewOnBoardingStatusComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'ng-cli-universal' }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_21__["NgxMatSelectSearchModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_17__["SharedModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_8__["MaterialModule"],
                _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_9__["CdkTreeModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_11__["ChartsModule"],
                _global_global_module__WEBPACK_IMPORTED_MODULE_14__["GlobalModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot([
                    { path: '', redirectTo: 'login', pathMatch: 'full' },
                    //{ path: 'login', component: LoginComponent },
                    { path: 'login', component: _logintoken_logintoken_component__WEBPACK_IMPORTED_MODULE_13__["LogintokenComponent"] },
                    { path: 'NewOnboarding', component: _new_onboarding_new_onboarding_component__WEBPACK_IMPORTED_MODULE_20__["NewOnboardingComponent"] },
                    { path: 'NewOnBoardingStatus', component: _new_on_boarding_status_new_on_boarding_status_component__WEBPACK_IMPORTED_MODULE_23__["NewOnBoardingStatusComponent"] },
                    { path: 'changepassword', component: _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_16__["ChangepasswordComponent"] },
                    { path: 'activate', component: _login_activate_login_activate_component__WEBPACK_IMPORTED_MODULE_18__["LoginActivateComponent"] },
                    { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [_authguard_authguard_guard__WEBPACK_IMPORTED_MODULE_1__["AuthguardGuard"]] },
                    { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
                ])
            ],
            providers: [
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_5__["HashLocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_5__["LocationStrategy"] },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], useClass: _logintoken_httpInterceptor__WEBPACK_IMPORTED_MODULE_22__["httpInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], useClass: _global_error_interceptor__WEBPACK_IMPORTED_MODULE_15__["ErrorInterceptor"], multi: true },
                [_authguard_authguard_guard__WEBPACK_IMPORTED_MODULE_1__["AuthguardGuard"], _login_loginservice__WEBPACK_IMPORTED_MODULE_19__["LoginServices"]],
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/authguard/authguard.guard.ts":
/*!**********************************************!*\
  !*** ./src/app/authguard/authguard.guard.ts ***!
  \**********************************************/
/*! exports provided: AuthguardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthguardGuard", function() { return AuthguardGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthguardGuard = /** @class */ (function () {
    function AuthguardGuard(routes) {
        this.routes = routes;
    }
    AuthguardGuard.prototype.canActivate = function (next, state) {
        if (sessionStorage.getItem('username') != null) {
            return true;
        }
        else {
            this.routes.navigate(['/login']);
            return false;
        }
        //return true;
    };
    AuthguardGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthguardGuard);
    return AuthguardGuard;
}());



/***/ }),

/***/ "./src/app/changepassword/changepassword.component.css":
/*!*************************************************************!*\
  !*** ./src/app/changepassword/changepassword.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".is-invalid {\r\n  border-left: solid 6px red;\r\n}\r\n\r\n.material-icons {\r\n  display: inline-flex;\r\n  vertical-align: middle;\r\n}\r\n\r\n.mat-app-background {\r\n  background: #fff !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhbmdlcGFzc3dvcmQvY2hhbmdlcGFzc3dvcmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQix1QkFBdUI7Q0FDeEI7O0FBQ0Q7RUFDRSw0QkFBNEI7Q0FDN0IiLCJmaWxlIjoic3JjL2FwcC9jaGFuZ2VwYXNzd29yZC9jaGFuZ2VwYXNzd29yZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmlzLWludmFsaWQge1xyXG4gIGJvcmRlci1sZWZ0OiBzb2xpZCA2cHggcmVkO1xyXG59XHJcblxyXG4ubWF0ZXJpYWwtaWNvbnMge1xyXG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuLm1hdC1hcHAtYmFja2dyb3VuZCB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/changepassword/changepassword.component.html":
/*!**************************************************************!*\
  !*** ./src/app/changepassword/changepassword.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<app-dialog [(visible)]=\"DeleteMenu\">-->\r\n\r\n\r\n  <!--<div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">First LogIn  Change Password </h3>\r\n    <div class=\"md-form form-lg\">\r\n      <label for=\"inputLGEx\">User ID</label>\r\n      <input type=\"text\" id=\"inputLGEx\" class=\"form-control form-control-lg\" disabled [(ngModel)]=\"ObjPwdDetails.username\" name=\"username\">\r\n    </div>\r\n    <div class=\"md-form form-lg\">\r\n      <label for=\"inputLGEx\">Old Password</label>\r\n      <input type=\"text\" id=\"inputLGEx\" class=\"form-control form-control-lg\" [(ngModel)]=\"ObjPwdDetails.OldPassword\" name=\"OldPassword\">\r\n    </div>\r\n    <div class=\"md-form\">\r\n      <label for=\"inputMDEx\">New Password</label>\r\n      <input type=\"text\" id=\"inputMDEx\" class=\"form-control\" [(ngModel)]=\"ObjPwdDetails.NewPassword\" name=\"NewPassword\" (blur)=\"restrictNumeric(ObjPwdDetails.NewPassword)\" maxlength=\"15\">\r\n    </div>\r\n    <div class=\"md-form form-sm\">\r\n      <label for=\"inputSMEx\">Confirm Password</label>\r\n      <input type=\"text\" id=\"inputSMEx\" class=\"form-control form-control-sm\" [(ngModel)]=\"ObjPwdDetails.ConfirmPassword\" (blur)=\"restrictNumeric(ObjPwdDetails.ConfirmPassword)\" name=\"ConfirmPassword\" maxlength=\"15\">\r\n    </div>\r\n    <br />\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Save()\">LogIn</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"clear()\">clear</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>-->\r\n  <!--//----Test------------------>\r\n  <!-- <div class=\"container p-3\">\r\n     <div class=\"offset-3 col-6\">\r\n       <div class=\"card\">\r\n         <div class=\"card-header  font-weight-bold\">\r\n           Sign Up Form\r\n         </div>-->\r\n<!--<div class=\"col-md-8\">-->\r\n<div class=\"card card-body div-center\">\r\n  <h3  style=\"text-align:center;\">First LogIn  Change Password </h3>\r\n  <form [formGroup]=\"frmSignup\" (submit)=\"submit()\">\r\n    <div class=\"form-group\">\r\n      <!--<label for=\"email\" [ngClass]=\"frmSignup.controls['email'].invalid ? 'text-danger' : ''\">Email Address</label>-->\r\n      <input id=\"email\" formControlName=\"email\" type=\"email\" disabled [(ngModel)]=\"ObjPwdDetails.username\" class=\"form-control\">\r\n\r\n      <!--<input type=\"text\" id=\"inputLGEx\" class=\"form-control form-control-lg\" disabled [(ngModel)]=\"ObjPwdDetails.username\" name=\"username\">-->\r\n\r\n      <label class=\"text-danger\" *ngIf=\"frmSignup.controls['email'].hasError('required')\">\r\n        Email is Required!\r\n      </label>\r\n      <label class=\"text-danger\" *ngIf=\"frmSignup.controls['email'].hasError('email')\">\r\n        Enter a valid email address!\r\n      </label>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label for=\"password\" [ngClass]=\"frmSignup.controls['password'].invalid ? 'text-danger' : ''\">Password:</label>\r\n      <input id=\"password\" formControlName=\"password\" type=\"password\" class=\"form-control\" [ngClass]=\"frmSignup.controls['password'].invalid ? 'is-invalid' : ''\">\r\n\r\n      <label class=\"col-md-12 mt-10 pading-0\" [ngClass]=\"frmSignup.controls['password'].hasError('required') || frmSignup.controls['password'].hasError('minlength')  ? 'text-danger' : 'text-success'\">\r\n        <i class=\"material-icons\">\r\n          {{\r\n              frmSignup.controls['password'].hasError('required') ||\r\n                frmSignup.controls['password'].hasError('minlength') ? 'cancel' :\r\n                'check_circle'\r\n          }}\r\n        </i>\r\n        Must be at least 8 characters!\r\n      </label>\r\n      <label class=\"col-md-12 pading-0\" [ngClass]=\"frmSignup.controls['password'].hasError('required') || frmSignup.controls['password'].hasError('hasNumber')  ? 'text-danger' : 'text-success'\">\r\n        <i class=\"material-icons\">\r\n          {{\r\n               frmSignup.controls['password'].hasError('required') ||\r\n                frmSignup.controls['password'].hasError('hasNumber') ? 'cancel' :\r\n                'check_circle'\r\n          }}\r\n        </i>\r\n        Must contain at least 1 number!\r\n      </label>\r\n      <label class=\"col-md-12 pading-0\" [ngClass]=\"frmSignup.controls['password'].hasError('required') || frmSignup.controls['password'].hasError('hasCapitalCase')  ? 'text-danger' : 'text-success'\">\r\n        <i class=\"material-icons\">\r\n          {{\r\n              frmSignup.controls['password'].hasError('required') ||\r\n                frmSignup.controls['password'].hasError('hasCapitalCase') ? 'cancel' :\r\n                'check_circle'\r\n          }}\r\n        </i>\r\n        Must contain at least 1 in Capital Case!\r\n      </label>\r\n      <label class=\"col-md-12 pading-0\" [ngClass]=\"frmSignup.controls['password'].hasError('required') || frmSignup.controls['password'].hasError('hasSmallCase')  ? 'text-danger' : 'text-success'\">\r\n        <i class=\"material-icons\">\r\n          {{\r\n              frmSignup.controls['password'].hasError('required') ||\r\n                frmSignup.controls['password'].hasError('hasSmallCase') ? 'cancel' :\r\n                'check_circle'\r\n          }}\r\n        </i>\r\n        Must contain at least 1 Letter in Small Case!\r\n      </label>\r\n      <label class=\"col-md-12 mb-10 pading-0\" [ngClass]=\"frmSignup.controls['password'].hasError('required') || frmSignup.controls['password'].hasError('hasSpecialCharacters') ? 'text-danger' : 'text-success'\">\r\n        <i class=\"material-icons\">\r\n          {{\r\n              frmSignup.controls['password'].hasError('required') ||\r\n                frmSignup.controls['password'].hasError('hasSpecialCharacters') ? 'cancel' :\r\n                'check_circle'\r\n          }}\r\n        </i>\r\n        Must contain at least 1 Special Character!\r\n      </label>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <label for=\"confirmPassword\" [ngClass]=\"frmSignup.controls['confirmPassword'].invalid ? 'text-danger' : ''\">\r\n        Confirm\r\n        Password:\r\n      </label>\r\n      <input id=\"confirmPassword\" formControlName=\"confirmPassword\" type=\"password\" class=\"form-control\"\r\n             [ngClass]=\"frmSignup.controls['confirmPassword'].invalid ? 'is-invalid' : ''\">\r\n      <label class=\"text-danger\" *ngIf=\"frmSignup.controls['confirmPassword'].hasError('required')\">\r\n        Password is Required!\r\n      </label>\r\n      <label class=\"text-danger\" *ngIf=\"frmSignup.controls['confirmPassword'].hasError('NoPassswordMatch')\">\r\n        Password do not match\r\n      </label>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <!--<div class=\"col-md-12\">-->\r\n        <!--<button [disabled]=\"frmSignup.invalid\" type=\"submit\" class=\"btn btn-primary btn-block font-weight-bold ml-0 col-md-3\">Sign up</button>-->\r\n      <div class=\"col text-center\">\r\n        <button [disabled]=\"frmSignup.invalid\" type=\"submit\" class=\"btn btn-primary text-center\" (click)=\"clear()\">Submit</button>\r\n        <button type=\"button\" class=\"btn btn-info text-center\" (click)=\"clear()\">Clear</button>\r\n        <button type=\"button\" class=\"btn btn-warning text-center\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n        \r\n      \r\n       \r\n      <!--</div>-->\r\n      \r\n    </div>\r\n  </form>\r\n</div>\r\n<!--</div>-->\r\n  \r\n  <!--</div>\r\n    </div>\r\n  </div>-->\r\n<!--</app-dialog>-->\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/changepassword/changepassword.component.ts":
/*!************************************************************!*\
  !*** ./src/app/changepassword/changepassword.component.ts ***!
  \************************************************************/
/*! exports provided: ChangepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangepasswordComponent", function() { return ChangepasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_loginservice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/loginservice */ "./src/app/login/loginservice.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _custom_validators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./custom-validators */ "./src/app/changepassword/custom-validators.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChangepasswordComponent = /** @class */ (function () {
    function ChangepasswordComponent(router, _LoginServices, fb) {
        this.router = router;
        this._LoginServices = _LoginServices;
        this.fb = fb;
        this.ObjPwdDetails = {};
        this.frmSignup = this.createSignupForm();
    }
    ChangepasswordComponent.prototype.ngOnInit = function () {
        this.DeleteMenu = true;
        this.ObjPwdDetails.username = sessionStorage.getItem('NewUser');
    };
    ChangepasswordComponent.prototype.Save = function () {
        var _this = this;
        if (this.ObjPwdDetails.OldPassword == null) {
            alert("Please enter OldPassword");
            return;
        }
        if (this.ObjPwdDetails.NewPassword == null) {
            alert("Please enter New Password");
            return;
        }
        if (this.ObjPwdDetails.ConfirmPassword == null) {
            alert("Please enter Confirm Password");
            return;
        }
        if (this.ObjPwdDetails.ConfirmPassword != this.ObjPwdDetails.NewPassword) {
            alert("Please enter match New Password and Confirm Password");
            return;
        }
        this._LoginServices.ChangePassword(this.ObjPwdDetails).subscribe(function (data) {
            _this.PasswordChangeStatus = data;
            debugger;
            if (_this.PasswordChangeStatus == 'Pass') {
                alert("Password changed successfully, Please login!");
                _this.router.navigate(['/login']);
            }
            if (_this.PasswordChangeStatus == 'change') {
                alert("Please change Password");
            }
        });
        this.clear();
    };
    ChangepasswordComponent.prototype.clear = function () {
        this.ObjPwdDetails.OldPassword = "";
        this.ObjPwdDetails.NewPassword = "";
        this.ObjPwdDetails.ConfirmPassword = "";
    };
    ChangepasswordComponent.prototype.Cancel = function () {
        this.DeleteMenu = false;
        this.router.navigate(['/login']);
    };
    ChangepasswordComponent.prototype.restrictNumeric = function (password) {
        debugger;
        //alert('Blur')
        var minMaxLength = /^[\s\S]{8,15}$/, upper = /[A-Z]/, lower = /[a-z]/, number = /[0-9]/, special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;
        if (minMaxLength.test(password) &&
            upper.test(password) &&
            lower.test(password) &&
            number.test(password) &&
            special.test(password)) {
            return true;
        }
        else {
            alert('Passwords must contain at least eight characters, including uppercase, lowercase letters,operator and numbers.');
            return false;
        }
    };
    ChangepasswordComponent.prototype.createSignupForm = function () {
        return this.fb.group({
            email: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
            ],
            password: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    // check whether the entered password has a number
                    _custom_validators__WEBPACK_IMPORTED_MODULE_4__["CustomValidators"].patternValidator(/\d/, {
                        hasNumber: true
                    }),
                    // check whether the entered password has upper case letter
                    _custom_validators__WEBPACK_IMPORTED_MODULE_4__["CustomValidators"].patternValidator(/[A-Z]/, {
                        hasCapitalCase: true
                    }),
                    // check whether the entered password has a lower case letter
                    _custom_validators__WEBPACK_IMPORTED_MODULE_4__["CustomValidators"].patternValidator(/[a-z]/, {
                        hasSmallCase: true
                    }),
                    // check whether the entered password has a special character
                    _custom_validators__WEBPACK_IMPORTED_MODULE_4__["CustomValidators"].patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
                        hasSpecialCharacters: true
                    }),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8)
                ])
            ],
            confirmPassword: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])]
        }, {
            // check whether our password and confirm password match
            validator: _custom_validators__WEBPACK_IMPORTED_MODULE_4__["CustomValidators"].passwordMatchValidator
        });
    };
    ChangepasswordComponent.prototype.submit = function () {
        var _this = this;
        //console.log(this.frmSignup.value);
        this.ObjPwdDetails.username = this.frmSignup.value.email;
        this.ObjPwdDetails.NewPassword = this.frmSignup.value.password;
        this._LoginServices.ChangePassword(this.ObjPwdDetails).subscribe(function (data) {
            _this.PasswordChangeStatus = data;
            debugger;
            if (_this.PasswordChangeStatus == 'Pass') {
                alert("Password changed successfully, Please login!");
                _this.router.navigate(['/login']);
            }
            if (_this.PasswordChangeStatus == 'change') {
                alert("Please change Password");
            }
        });
        this.clear();
    };
    ChangepasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-changepassword',
            template: __webpack_require__(/*! ./changepassword.component.html */ "./src/app/changepassword/changepassword.component.html"),
            styles: [__webpack_require__(/*! ./changepassword.component.css */ "./src/app/changepassword/changepassword.component.css")],
            providers: [_login_loginservice__WEBPACK_IMPORTED_MODULE_1__["LoginServices"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _login_loginservice__WEBPACK_IMPORTED_MODULE_1__["LoginServices"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], ChangepasswordComponent);
    return ChangepasswordComponent;
}());



/***/ }),

/***/ "./src/app/changepassword/custom-validators.ts":
/*!*****************************************************!*\
  !*** ./src/app/changepassword/custom-validators.ts ***!
  \*****************************************************/
/*! exports provided: CustomValidators */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomValidators", function() { return CustomValidators; });
var CustomValidators = /** @class */ (function () {
    function CustomValidators() {
    }
    CustomValidators.patternValidator = function (regex, error) {
        return function (control) {
            if (!control.value) {
                // if control is empty return no error
                return null;
            }
            // test the value of the control against the regexp supplied
            var valid = regex.test(control.value);
            // if true, return no error (no error), else return error passed in the second parameter
            return valid ? null : error;
        };
    };
    CustomValidators.passwordMatchValidator = function (control) {
        var password = control.get('password').value; // get password from our password form control
        var confirmPassword = control.get('confirmPassword').value; // get password from our confirmPassword form control
        // compare is the password math
        if (password !== confirmPassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('confirmPassword').setErrors({ NoPassswordMatch: true });
        }
    };
    return CustomValidators;
}());



/***/ }),

/***/ "./src/app/global/common-msg.ts":
/*!**************************************!*\
  !*** ./src/app/global/common-msg.ts ***!
  \**************************************/
/*! exports provided: CommonMsg */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonMsg", function() { return CommonMsg; });
var CommonMsg = /** @class */ (function () {
    function CommonMsg() {
        /* common messages use in application */
        this.saveMsg = 'Record saved successfully.';
        this.updateMsg = 'Record updated successfully.';
        this.deleteMsg = 'Record deleted successfully.';
        this.alreadyExistMsg = 'Record already exist.';
        this.noRecordMsg = 'No Record Found.';
        this.enterCorrectValueMsg = 'Please enter correct value.';
        this.checkDeclarationMsg = 'Please check Declaration.';
        this.forwardHooMakerMsg = 'Forward to HOO Maker';
        this.forwardHooCheckerMsg = 'Forward to HOO Checker';
        this.updateFailedMsg = 'Failed to update Record.';
        this.alreadyForwardedMsg = 'Record Already Forwarded';
        this.deleteFailedMsg = 'Failed to Delete Record.';
        this.forwardCheckerMsg = 'Record Forwarded to Checker Successfully';
        this.DDOVerifiedByChecker = 'Record Verified Successfully';
        this.DDORejectedByChecker = 'Record Rejected Successfully';
        this.AttachAtleastOne = 'Please attach at least one record';
        this.DeAttachAtleastOne = 'Please DeAttach at least one Employee';
        this.saveFailedMsg = 'Failed to Save Record.';
        this.formatfileMsg = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.pdffileMsg = 'only pdf file upload';
        this.apppdffileMsg = 'application/pdf';
        this.errorMsg = 'An error has occurred. Please contact your system administrator.';
        this.apiErrorMsg = 'server api error occure.';
        this.messageTimer = 15000;
        /* common messages use in application */
        /* New Loan message */
        this.amountLessEqualloanAmountMsg = 'Entered Amount should be less than or equal to Loan Amount';
        this.amountActuallyPaidMsg = 'Entered Actually Paid Amount should be less than or equal to Cost Amount';
        this.loanAmountGreaterthenInstallMsg = 'Loan Amount should be greater than installment Amount';
        this.maximumLimitLoanAmountMsg = 'Maximum Limit for Loan Amount is Rs. 25 Lakhs';
        this.maxLimitLoanAmountMsg = 'Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower';
        this.maxLimitLoanAmount1LK80Msg = 'Maximum Limit for Loan Amount is Rs. 1,80,000.00';
        this.maxLimitLoanAmount2LK50Msg = 'Maximum Limit for Loan Amount is Rs. 2,50,000.00';
        this.costGreateThenAmountActuallyPaid = 'Cost should be greater then Amount actually paid.';
        this.noInstallmentsRepaymentAmountshouldbelessthanorequal150 = 'No. of Installments for Repayment Amount should be less than or equal 150.';
        this.VerifyedByChecker = 'Verifyed by DDO Checker';
        this.RejectedByChecker = 'Rejected by DDO Checker';
        this.ForwardToDDOMaker = 'Forward to DDO Maker';
        this.ForwardToDDOChecker = 'Forward to DDO Checker';
        this.VerifyDDOChecker = 'Verify DDO Checker';
        this.VerifyHOOChecker = 'Verified HOO checker';
        this.Rejected = 'Rejected';
        this.PermanentEmpeApplyLoan = 'only Permanent employee apply for the loan';
        this.AmountShouldbelessthanRequestedAmount = 'Amount should be less than Amount requested';
        this.AccountNumberNotMatched = 'Account Number not Matched Please try Again.';
        this.forwardDDOCheckerMsg = 'Record Forwarded to DDO Checker Successfully';
        this.forwardDDOCheckerFailedMsg = 'Failed to Forwarded Record to DDO Checker';
        this.coolingPeriodMsg = 'Minimum cooling period is 3 years for computer advance loan';
        this.EmpCdSelectMsg = 'Please Select Employee';
        this.sessionExpiredMsg = 'Your session has been expired!';
        /* New Loan message */
        /*Deputation  */
        this.depRepaAlreadyExits = 'Repatriation Order Number  Already Exits';
        /*Deputation  */
        /*   User Management/Payroll  */
        this.getController = 'getController';
        this.getPAO = 'getPAO';
        this.getDDO = 'getDDO';
        this.Controller = 'Controller';
        this.USN = 'Unassigned Successfully';
        this.ASN = 'Assigned Successfully';
        this.EmailMsg = 'Assigned successfully,Role activation link has been sent to your email.';
        this.dDOAdmin = 'DDO Admin';
        this.HOOChecker = 'HOO Checker';
        this.HOOMaker = 'HOO Maker';
        this.DDOChecker = 'DDO Checker';
        this.DDOMaker = 'DDO Maker';
        this.PAO = 'PAO';
        /*   User Management/Payroll  */
        /* Common Component Message */
        this.billGroupNotFoundMsg = 'Pay Bill Group is not found';
        this.desgNotFoundMsg = 'Designation is not found';
        this.empNotFoundMsg = 'Employee is not found';
        this.numberNotAllowed = 'Number Not Allowed';
    }
    /* Common Component Message */
    CommonMsg.prototype.charaterOnlyNoSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    return CommonMsg;
}());



/***/ }),

/***/ "./src/app/global/error.interceptor.ts":
/*!*********************************************!*\
  !*** ./src/app/global/error.interceptor.ts ***!
  \*********************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ErrorInterceptor = /** @class */ (function () {
    function ErrorInterceptor(router) {
        this.router = router;
    }
    ErrorInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) {
            if (err.status === 401) {
                if (sessionStorage.length > 0) {
                    alert('Your session has been expired!');
                    sessionStorage.clear();
                    _this.router.navigate(['/login']);
                }
            }
            else if (err.status === 400) {
                var errorObj = JSON.parse(err.error);
                var alertStr = '';
                for (var k in errorObj) {
                    alertStr += errorObj[k] + '\n';
                }
                alert(alertStr);
            }
            else if (err.status === 404) {
                alert(err.statusText || err.error.message);
            }
            else if (err.status === 500) {
                alert(err.statusText || err.error.message);
            }
            else if (err.status === 504) {
                alert(err.statusText || err.error.message);
            }
            else if (err.status === 200) {
                alert(err.error.message || err.statusText);
            }
            var error = err.error.message || err.statusText;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(error);
        }));
    };
    ErrorInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ErrorInterceptor);
    return ErrorInterceptor;
}());



/***/ }),

/***/ "./src/app/global/global.module.ts":
/*!*****************************************!*\
  !*** ./src/app/global/global.module.ts ***!
  \*****************************************/
/*! exports provided: APP_CONFIG, AppConfig, APP_DI_CONFIG, GlobalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_CONFIG", function() { return APP_CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConfig", function() { return AppConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_DI_CONFIG", function() { return APP_DI_CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalModule", function() { return GlobalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var APP_CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('app.config');
var AppConfig = /** @class */ (function () {
    function AppConfig() {
    }
    return AppConfig;
}());

var getHostName = window.location.hostname;
var apiHost;
if (getHostName === 'localhost') {
    //apiHost = 'http://localhost:55424/api';
    apiHost = 'http://localhost:55424/api/';
}
else {
    apiHost = 'http://10.199.72.53:82/api/';
}
var APP_DI_CONFIG = {
    api_base_url: apiHost,
    //==============DashBoard=======
    getAllMenusByUser: apiHost + 'Dashboard/getAllMenusByUser',
    getDashboardDetal: 'Dashboard/GetDashboardDetails',
    //============End Of DashBoard======
    //=========Masters======================
    //=======  City Class ============
    GeCityClassMasterList: 'CityMaster/GetCitymasterList',
    // SaveCityClass:'CityMaster/SaveCityClass',
    //========= End City Class ============
    //============ Dues Master ===============
    InsertUpdateDuesDefinationMaster: 'DuesDefinationandDuesMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
    GetDuesDefinationmaster: 'DuesDefinationandDuesMaster/GetDuesdefinationMasterList',
    GetDuesDefinationMasterDetailsByID: 'DuesDefinationandDuesMaster/GetEmpPersonalDetailsByID?DuesCD=',
    GetAutoGenratedDuesCode: 'DuesDefinationandDuesMaster/GetAutoGenratedDuesCode',
    GetOrgnaztionTypeForDuesRate: 'DuesDefinationandDuesMaster/GetOrgnazationTypeForDues',
    GetStateForDuesRate: 'DuesDefinationandDuesMaster/GetStateForDuesRate',
    GetPayCommForDuesRate: 'DuesDefinationandDuesMaster/GetPayCommForDuesRate',
    GetCityClassForDuesRate: 'DuesDefinationandDuesMaster/GetCityClassForDuesRate?payCommId=',
    GetDuesCodeDuesDefination: 'DuesDefinationandDuesMaster/GetDuesCodeDuesDefination',
    AddDuesDetails: 'DuesDefinationandDuesMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
    GetSlabTypeByPayCommId: 'DuesDefinationandDuesMaster/GetSlabtypeByPayCommId?payCommId=',
    saveUpdateDuesRateDetails: 'DuesDefinationandDuesMaster/SaveUpdateDuesRateDetails',
    GetDuesRateList: 'DuesDefinationandDuesMaster/GetDuesCodeDuesRate',
    DeleteDuesRatemaster: apiHost + 'DuesDefinationandDuesMaster/DeleteDuesRateDetails?msduesratedetailsid=',
    //=============== end
    //=========leavetype master======
    getMstLeaveTypeList: apiHost + 'Leavetype/GetLeavetypelist',
    getMstLeavetypeMaxid: apiHost + 'Leavetype/GetLeavetypeMaxid',
    upadateMstLeavetype: apiHost + 'Leavetype/upadateLeavetype',
    insertLeavetype: apiHost + 'Leavetype/insertLeavetype',
    deleteLeavetype: apiHost + 'Leavetype/DeleteLeavetype',
    //=========Design master======
    getMsdesignlist: apiHost + 'DesignMaster/GetMsdesignlist',
    getMsdesignMaxid: apiHost + 'DesignMaster/GetMsdesignMaxid',
    updateMsdesign: apiHost + 'DesignMaster/UpdateMsdesign',
    insertMsdesign: apiHost + 'DesignMaster/InsertMsdesign',
    deleteMsdesign: apiHost + 'DesignMaster/DeleteMsdesign',
    //=========Ms State Ag master======
    getMsPfTypeList: apiHost + 'MsStateAg/GetMsPfTypeList',
    getMsStateList: apiHost + 'MsStateAg/GetMsStateList',
    getMsStateAglist: apiHost + 'MsStateAg/GetMsStateAglist',
    insertMsStateAg: apiHost + 'MsStateAg/InsertMsStateAg',
    upadateMsStateAg: apiHost + 'MsStateAg/UpadateMsStateAg',
    //=========Ms PAO Master======
    getMSPAODetails: apiHost + 'PAOMaster/GetMSPAODetails',
    updateMSPAO: apiHost + 'PAOMaster/UpdateMSPAO',
    insertMSPAO: apiHost + 'PAOMaster/InsertMSPAO',
    deleteMSPAO: apiHost + 'PAOMaster/DeleteMSPAO',
    //============payscale==========
    BindCommissionCode: apiHost + 'payscale/BindCommissionCode',
    BindPayScaleFormat: apiHost + 'payscale/BindPayScaleFormat',
    BindPayScaleGroup: apiHost + 'payscale/BindPayScaleGroup',
    CreatePayScale: apiHost + 'payscale/CreatePayScale',
    GetPayScaleDetails: apiHost + 'payscale/GetPayScaleDetails',
    EditPayScaleDetails: apiHost + 'payscale/EditPayScaleDetails?msPayScaleID=',
    GetPayScaleDetailsBYComCode: apiHost + 'payscale/GetPayScaleDetailsBYComCode?CommCDID=',
    GetPayScaleCode: apiHost + 'payscale/GetPayScaleCode?CddirCodeValue=',
    Delete: apiHost + 'payscale/Delete?msPayScaleID=',
    //==================================================
    InsertandUpdate: apiHost + 'DaStateCenteral/InsertandUpdate',
    GetDADetails: apiHost + 'DaStateCenteral/GetDADetails',
    GetDACurrent_Rate_Wef: apiHost + 'DaStateCenteral/GetDACurrent_Rate_Wef?DaType=',
    DeleteDaMaster: apiHost + 'DaStateCenteral/Delete?msDaMasterID=',
    GetDACurrent_Rate_Wef_state: apiHost + 'DaStateCenteral/GetDACurrent_Rate_Wef_state?DaType=',
    //============Hra Master==========
    BindPayScaleCode: apiHost + 'HraMaster/BindPayScaleCode',
    BindPayLevel: apiHost + 'HraMaster/BindPayLevel',
    GetCityDetails: apiHost + 'HraMaster/GetCityDetails?StateID=',
    GetCityClass: apiHost + 'HraMaster/GetCityClass?payCommId=',
    CreateHraMaster: apiHost + 'HraMaster/CreateHraMaster',
    UpdateHraMaster: apiHost + 'HraMaster/UpdateHraMaster',
    GetHraMasterDetails: apiHost + 'HraMaster/GetHraMasterDetails',
    EditHraMasterDetails: apiHost + 'HraMaster/EditHraMasterDetails?hraMasterID=',
    DeleteHraMaster: apiHost + 'HraMaster/DeleteHraMaster?hraMasterID=',
    GetPayCommissionByEmployeeType: apiHost + 'HraMaster/GetPayCommissionByEmployeeType?employeeType=',
    //================PraoMaster===============
    GetControllerCode: 'PraoMaster/GetControllerCode',
    BindCtrlName: 'PraoMaster/BindCtrlName?CtrlrCode=',
    BindState: 'PraoMaster/BindState',
    BindDistrict: 'PraoMaster/BindDistrict?stateId=',
    SaveOrUpdate: 'PraoMaster/SaveOrUpdate?PraoModel=',
    BindPraoMasterDetails: 'PraoMaster/BindPraoMasterDetails',
    ViewEditOrDeletePrao: apiHost + 'PraoMaster/ViewEditOrDeletePrao',
    //============Tpta Master==========
    BindUnionTerritory: apiHost + 'TptaMaster/BindUnionTerritory',
    CreateTptaMaster: apiHost + 'TptaMaster/CreateTptaMaster',
    GetTptaMasterDetails: apiHost + 'TptaMaster/GetTptaMasterDetails',
    EditTptaMasterDetails: apiHost + 'TptaMaster/EditTptaMasterDetails?masterID=',
    GetTptaMasterDetailsByID: apiHost + 'TptaMaster/GetTptaMasterDetailsByID?masterID=',
    DeleteTptaMaster: apiHost + 'TptaMaster/DeleteTptaMaster?masterID=',
    //====Family Definition GPF Master==========
    CreatefdGpfMaster: apiHost + 'FdGpfMaster/CreatefdGpfMaster',
    GetFdGpfMasterDetails: apiHost + 'FdGpfMaster/GetFdGpfMasterDetails',
    EditFdGpfMasterDetails: apiHost + 'FdGpfMaster/EditFdGpfMasterDetails?fdMasterID=',
    DeleteFdGpfMaster: apiHost + 'FdGpfMaster/DeleteFdGpfMaster?fdMasterID=',
    //====Family Definition Pension Master==========
    //CreatefdPensionMaster: apiHost + 'FdGpfMaster/CreatefdPensionMaster',
    //GetFdPensionMasterDetails: apiHost + 'FdGpfMaster/GetFdPensionMasterDetails',
    //EditFdPensionMasterDetails: apiHost + 'FdGpfMaster/EditFdPensionMasterDetails?fdMasterID=',
    //DeleteFdPensionMaster: apiHost + 'FdGpfMaster/DeleteFdPensionMaster?fdMasterID=',
    //====Commutation table Pension Master==========
    CreateCommPensionMaster: apiHost + 'CommutationTable/CreateCommPensionMaster',
    GetCommPensionMasterDetails: apiHost + 'CommutationTable/GetCommPensionMasterDetails',
    //=====================Gpf Interest Rate==========================================
    DeleteGpfInterestRate: apiHost + 'GPFInterestRate/Delete?gpfInterestID=',
    GetGPFInterestRateDetails: apiHost + 'GPFInterestRate/GetGPFInterestRateDetails',
    GpfInterestRateInsertandUpdate: apiHost + 'GPFInterestRate/InsertandUpdate',
    //======================Gpf Advance Reasons=================================
    BindMainReason: apiHost + 'GpfAdvanceReasons/BindMainReason',
    GpfAdvancereasonsInsertandUpdate: apiHost + 'GpfAdvanceReasons/GpfAdvancereasonsInsertandUpdate',
    GetPfType: apiHost + 'Master/GetPfType',
    GetGpfAdvancereasonsDetails: apiHost + 'GpfAdvanceReasons/GetGpfAdvancereasonsDetails',
    GpfAdvancereasonsDelete: apiHost + 'GpfAdvanceReasons/GpfAdvancereasonsDelete?GpfAdvanceReasonID=',
    //======================Gpf Withdrawal Rules=================================
    GpfWithdrawalRules: 'GpfWithdrawRules/GetGpfWithdrawRules',
    InsertUpdateGpfWithdrawalRules: 'GpfWithdrawRules/InsertUpdateGpfWithdrawRules',
    GpfWithdrawalRulesByID: 'GpfWithdrawRules/GetGpfWithdrawRulesById',
    DeleteGpfWithdrawalRules: 'GpfWithdrawRules/DeleteGpfWithdrawRules?msGpfWithdrawRulesRefID=',
    //======================Gpf Advance Rules=================================
    GpfAdvanceRules: 'GpfAdvanceRules/GetGpfAdvanceRules',
    InsertUpdateGpfAdvanceRules: 'GpfAdvanceRules/InsertUpdateGpfAdvanceRules',
    GpfAdvanceRulesByID: 'GpfAdvanceRules/GetGpfAdvanceRulesById',
    DeleteGpfAdvanceRules: 'GpfAdvanceRules/DeleteGpfAdvanceRules?msGpfAdvanceRulesRefID=',
    //======================Gpf Recovery Rules=================================
    GpfRecoveryRules: 'GpfRecoveryRules/GetGpfRecoveryRules',
    InsertUpdateGpfRecoveryRules: 'GpfRecoveryRules/InsertUpdateGpfRecoveryRules',
    GpfRecoveryRulesByID: 'GpfRecoveryRules/GetGpfRecoveryRulesById',
    DeleteGpfRecoveryRules: 'GpfRecoveryRules/DeleteGpfRecoveryRules?msGpfRecoveryRulesRefID=',
    //End Of Masters
    //Role Manegement
    getAllRole: apiHost + 'Role/GetAlRoles',
    getAllActiveRollList: apiHost + 'Role/GetAllActiveRollList',
    GetAllCustomeRollList: apiHost + 'Role/GetAllCustomeRollList',
    getAssignRoleForUser: apiHost + 'Role/GetAssignRoleForUser?roleID=',
    GetBindUserListddl: apiHost + 'Role/GetBindUserListddl',
    saveNewRole: apiHost + 'Role/CreateNewRole',
    getUserDetails: apiHost + 'Role/GetUserDetails',
    revokeUser: apiHost + 'Role/RevokeUser',
    getUserDetailsByPan: apiHost + 'Role/GetUserDetailsByPan',
    saveAssignRoleForUser: apiHost + 'Role/SaveAssignRoleForUser',
    getAllMenuList: apiHost + 'Role/GetAllMenuList',
    saveMenuPerMission: apiHost + 'Role/SaveMenuPerMission',
    checkMenuPerMission: apiHost + 'Role/CheckMenuPerMission',
    saveMenu: apiHost + 'Menu/SaveMenu?hdnMenuId=',
    FillUpdateMenu: apiHost + 'Menu/FillUpdateMenu?MenuId=',
    bindDropDownMenu: apiHost + 'Menu/BindDropDownMenu',
    bindMenuInGride: apiHost + 'Menu/BindMenuInGride',
    activeDeactiveMenu: apiHost + 'Menu/ActiveDeactiveMenu',
    GetPredefineRole: apiHost + 'Menu/GetPredefineRole',
    //End Of RoleManagmentModule
    // Employee Data Capture
    getSalutation: 'master/getsalutation',
    getRelation: 'master/getRelation',
    getEmployeeType: 'master/getEmpType',
    getEmployeeSubType: 'master/getEmpSubType',
    getJoiningMode: 'master/getJoiningMode',
    getJoiningCategory: 'master/getJoiningCategory?parentTypeId=',
    getverifiedEmpCode: 'EmployeeDetails/GetVerifyEmpCode',
    GetEmpPersonalDetailsByID: 'EmployeeDetails/GetEmpPersonalDetailsByID',
    GetIsDeputEmp: 'EmployeeDetails/GetIsDeputEmp',
    getMakerEmpList: 'EmployeeDetails/MakerEmpList',
    UpdateEmpDetails: 'EmployeeDetails/UpdateEmpDetails',
    SavePHDetails: 'EmployeeDetails/SavePHDetails',
    getDeputationType: 'master/getdeputationtype',
    getServiceType: 'master/getservicetype',
    getGender: 'master/getgender',
    InsertUpdateEmployeeDetails: 'EmployeeDetails/InsertUpdateEmpDetails',
    GetEmpPHDetails: 'EmployeeDetails/GetEmpPHDetails?EmpCd=',
    GetPhysicalDisabilityTypes: 'EmployeeDetails/GetPhysicalDisabilityTypes',
    GetState: 'master/getstate',
    uploadFiles: 'master/uploadFiles',
    GetMaritalStatus: 'FamilyDetails/GetMaritalStatus',
    getFamilyDetails: 'FamilyDetails/getFamilyDetails',
    getAllFamilyDetails: 'FamilyDetails/getAllFamilyDetails',
    UpdateEmpFamilyDetails: 'FamilyDetails/UpdateEmpFamilyDetails',
    DeleteFamilyDetails: 'FamilyDetails/DeleteFamilyDetails',
    getAllNomineeDetails: 'NomineeDetails/getAllNomineeDetails',
    getNomineeDetails: 'NomineeDetails/getNomineeDetails',
    UpdateNomineeDetails: 'NomineeDetails/UpdateNomineeDetails',
    DeleteNomineeDetails: 'NomineeDetails/DeleteNomineeDetails',
    getBankDetails: 'BankDetails/GetBankDetails',
    getBankDetailsByIFSC: 'BankDetails/GetBankDetailsByIFSC',
    getAllIFSC: 'BankDetails/GetAllIFSC',
    UpdateBankDetails: 'BankDetails/UpdateBankDetails',
    GetHRACity: 'PostingDetails/GetHRACity',
    GetTACity: 'PostingDetails/GetTACity',
    GetAllDesignation: 'PostingDetails/GetAllDesignation',
    SaveUpdatePostingDetails: 'PostingDetails/SaveUpdatePostingDetails',
    GetAllPostingDetails: 'PostingDetails/GetAllPostingDetails',
    getMaintainByOfc: 'ServiceDetails/getMaintainByOfc',
    getServiceDetails: 'ServiceDetails/getAllServiceDetails',
    SaveUpdateServiceDetails: 'ServiceDetails/SaveUpdateServiceDetails',
    getOrganisationType: 'PayDetails/getOrganisationType',
    getNonComputationalDues: 'PayDetails/GetNonComputationalDues',
    getNonComputationalDeductions: 'PayDetails/GetNonComputationalDeductions',
    getPayLevel: 'PayDetails/getPayLevel',
    getPayIndex: 'PayDetails/getPayIndex',
    getBasicDetails: 'PayDetails/getBasicDetails',
    getGradePay: 'PayDetails/getGradePay',
    getPayScale: 'PayDetails/getPayScale',
    SaveUpdatePayDetails: 'PayDetails/SaveUpdatePayDetails',
    getPayDetails: 'PayDetails/getPayDetails',
    getEntitledOffVeh: 'PayDetails/getEntitledOffVeh',
    getInsuranceApplicable: 'CGEGIS/GetInsuranceApplicable',
    getCGEGISCategory: 'CGEGIS/GetCGEGISCategory',
    SaveUpdateCGEGISDetails: 'CGEGIS/SaveUpdateCGEGISDetails',
    getCGEGISDetails: 'CGEGIS/GetCGEGISDetails',
    SaveUpdateCGHSDetails: 'CGHSDetails/SaveUpdateCGHSDetails',
    getCGHSDetails: 'CGHSDetails/getCGHSDetails',
    getQuarterOwnedby: 'QuarterDetails/getQuarterOwnedby',
    getAllottedTo: 'QuarterDetails/getAllottedTo',
    getRentStatus: 'QuarterDetails/getRentStatus',
    getQuarterType: 'QuarterDetails/getQuarterType',
    GetCustodian: 'QuarterDetails/GetCustodian',
    GetGPRACityLocation: 'QuarterDetails/GetGPRACityLocation',
    SaveUpdateQuarterDetails: 'QuarterDetails/SaveUpdateQuarterDetails',
    QuarterAllDetails: 'QuarterDetails/QuarterAllDetails',
    QuarterDetails: 'QuarterDetails/QuarterDetails',
    DeleteQuarterDetails: 'QuarterDetails/DeleteQuarterDetails',
    getAllEmpDetails: 'ForwardToChecker/getAllEmpDetails',
    getAllEmployeeCompleteDetails: 'ForwardToChecker/getAllEmployeeCompleteDetails',
    ForwardToCheckerEmpDetails: 'ForwardToChecker/ForwardToCheckerEmpDetails',
    verifyEmpData: 'ForwardToChecker/verifyEmpData',
    // End Of Employee Data Capture
    // Login
    LoginDetails: 'LoginRole/LoginCheck',
    UserDetails: apiHost + 'LoginRole/UserDetails?username=',
    LoginNewUser: 'LoginRole/LoginNewUser',
    RoleActivation: apiHost + 'LoginRole/RoleActivation?ID=',
    currentrUrl: apiHost + 'LoginRole/currentrUrl?url=',
    IsMenuPermissionAssigned: apiHost + 'LoginRole/IsMenuPermissionAssigned',
    //-----------------User creation---------------------
    getAllControllers: apiHost + 'Controllerrole/GetAllControllers?controllerID=',
    controllerchanged: apiHost + 'Controllerrole/oncontrollerroleSelectchanged?MsControllerID=',
    PaosAssigned: apiHost + 'Controllerrole/PaosAssigned?empCd=',
    GetAllUserRoles: apiHost + 'Controllerrole/GetAllUserRoles?username=',
    GetAllDDO: apiHost + 'ddorole/GetAllDDO?PAOID=',
    EmployeeList: apiHost + 'ddorole/EmployeeList?DDOID=',
    Assigned: apiHost + 'ddorole/Assigned?empCd=',
    //GetAllUserRoles: apiHost + 'Controllerrole/GetAllUserRoles?username='
    GetAllpaos: apiHost + 'paoroll/GetAllpaos?controllerID=',
    onpaoSelectchanged: apiHost + 'paoroll/GetAllDDOsUnderSelectedPAOs?MsPAOID=',
    AssignedPAO: apiHost + 'paoroll/Assigned?empCd=',
    login: apiHost + 'Login/Login?userId=',
    GetAllUserRolesByDDO: apiHost + 'UsersByDDO/GetAllUserRolesByDDO?username=',
    EmployeeListOfDDO: apiHost + 'UsersByDDO/EmployeeListOfDDO?DDOID=',
    AssignedCMD: apiHost + 'UsersByDDO/AssignedCMD?empCd=',
    AssignChecker: apiHost + 'UsersByDDO/AssignChecker?empCd=',
    //AssingedDDOChecker: apiHost + 'UsersByDDO/AssingedDDOChecker',
    //AssingedDDOCheckerMaker: apiHost + 'UsersByDDO/AssingedDDOCheckerMaker',
    makerEmpList: apiHost + 'maker/makerEmpList?DDOID=',
    AssignedMaker: apiHost + 'maker/AssignedMaker?empCd=',
    HooCheckerEmpList: apiHost + 'HOO/HooCheckerEmpList?DDOID=',
    AssignedHOOChecker: apiHost + 'HOO/AssignedHOOChecker?empCd=',
    hooMakerEmpList: apiHost + 'HOO/hooMakerEmpList?DDOID=',
    AssignedHOOMaker: apiHost + 'HOO/AssignedHOOMaker?empCd=',
    SelfAssignDDOChecker: apiHost + 'UsersByDDO/SelfAssignDDOChecker?UserID=',
    SelfAssignCheck: apiHost + 'UsersByDDO/SelfAssignCheck?UserID=',
    SelfAssignedDDOMaker: apiHost + 'maker/SelfAssignedDDOMaker?UserID=',
    SelfAssignMakerRole: apiHost + 'maker/SelfAssignMakerRole?UserID=',
    SelfAssignHOOChecker: apiHost + 'HOO/SelfAssignedHOOChecker?UserID=',
    SelfAssignHOOCheckerRole: apiHost + 'HOO/SelfAssignHOOCheckerRole?UserID=',
    AssignedEmp: apiHost + 'Controllerrole/AssignedEmp?controllerID=',
    AssignedEmpList: apiHost + 'paoroll/AssignedEmpList?PAOID=',
    AssignedEmpDDO: apiHost + 'ddorole/AssignedEmpDDO?DDOID=',
    AssignedDDO: apiHost + 'ddorole/AssignedDDO?DDOID=',
    AssignedPAOEmp: apiHost + 'paoroll/AssignedPAOEmp?PAOID=',
    AssignedDDOCheckerEmpList: apiHost + 'UsersByDDO/AssignedDDOCheckerEmpList?DDOID=',
    AssignedMakerEmpList: apiHost + 'maker/AssignedMakerEmpList?DDOID=',
    OnBoardingSubmit: apiHost + 'OnBoarding/OnBoardingSubmit',
    GetAllReuestNoOfOnboarding: apiHost + 'OnBoarding/GetAllReuestNoOfOnboarding',
    FetchRequestLetterNoRecord: apiHost + 'OnBoarding/FetchRequestLetterNoRecord?OnboardingId=',
    //------------------
    /*Start PayBillGroup*/
    getAccountHeads: 'PayBill/GetAccountHeads?PermDDOId=',
    InsertUpdatePayBillGroup: 'PayBill/InsertUpdatePayBillGroup',
    BindPayBillGroup: 'Shared/GetPayBillGroup?PermDdoId=',
    GetPayBillGroupDetailsById: 'PayBill/GetPayBillGroupDetailsById?BillGroupId=',
    GetPayBillGroupEmpAttach: 'PayBill/GetpayBillGroupEmpAttach?EmpPermDDOId=00003',
    GetPayBillGroupEmpDeAttach: 'Paybill/GetpayBillGroupEmpDeAttach?EmpPermDDOId=00003&BillGrpID=',
    UpdatePayBillGroupIdByEmpCode: 'Paybill/updatePayBillGroupbyEmpCodeandBillGroupId?empCode=',
    updatePayBillGroupIdnullByEmpCode: 'Paybill/updatePayBillGroupNullbyEmpCode?empCode=',
    getAccountHeadsOT1: 'PayBill/GetAccountHeadsOT1?FinFromYr=',
    GetAccountHeadOT1AttachList: 'PayBill/GetAccountHeadOT1AttachList?DDOCode=',
    UpdateaccountHeadOT1: 'PayBill/UpdateaccountHeadOT1?accountHeadvalue=',
    GetAccountHeadOT1PaybillGroupStatus: 'PayBill/GetAccountHeadOT1PaybillGroupStatus?DDOCode=',
    UpdateAccountHeadOT1PaybillGroupStatus: 'PayBill/UpdateAccountHeadOT1PaybillGroupStatus?accountHeadvalue=',
    getNgRecovery: 'PayBill/GetNgRecovery?PermDDOId=',
    GetBankDetailsByIfscCode: 'PayBill/GetBankdetailsByIfsc?IfscCode=',
    insertUpdateNgRecovery: 'PayBill/InsertUpdateNgRecovery',
    activeDeactiveNgRecovery: 'PayBill/ActiveDeactiveNgRecovery',
    getfinancialYear: 'PayBill/GetFinancialYear',
    getAllMonth: 'PayBill/GetAllMonth',
    GetEmployeeByPaybillAndDesig: 'PayBill/GetEmployeeByPaybillAndDesig',
    InsertNgrecoveryEntry: 'PayBill/InsertAllNgEntry',
    GetNgRecoveryList: 'PayBill/GetNgRecoveryList',
    GetAllNgRecoveryandPaybillGrpDetails: 'PayBill/GetAllNgRecoveryandPaybillGrpDetails',
    /*End PayBillGroup*/
    // -------Suspension Module--------------
    suspensionControllerName: 'Suspension',
    // -------Suspension Module End--------
    psuMasterControllerName: 'PsuMaster',
    // -------------leave management-----------
    LeavesControllerName: apiHost + 'Leaves',
    //End leave management
    // -------------leave management-----------
    DDOMasterControllerName: apiHost + 'DDOMaster',
    //End leave management
    //-----------existing-loan-------------------
    FloodController: apiHost + 'Flood',
    MultipleInstRecoveryController: apiHost + 'MultipleInstRecovery',
    getBindDropDownDesign: 'ExistingLoan/BindDropDownDesign',
    getBindDropDownBillGroup: 'ExistingLoan/BindDropDownBillGroup',
    getBindDropDownEmp: 'existingloan/BindDropDownEmp',
    getLoanAdvanceDetails: 'ExistingLoan/LoanAdvanceDetails?ddoid=',
    GetLoanType: 'LoanApplication/GetLoanType?loantypeFlag=',
    getAllEmpNameSelectedDesignId: 'existingloan/GetAllEmpNameSelectedDesignId?MsDesignID=',
    GetExistingEmpLoanDetails: 'existingloan/GetEmpLoanListDetails?PayBillGpID=',
    ExistingLoanStatus: 'ExistingLoan/BindExistingLoanStatus',
    GetExistLoanDetailsByID: 'ExistingLoan/GetExistLoanDetailsByID?MSPayAdvEmpdetID=',
    ExistingLoanForwordToDDO: 'ExistingLoan/ExistingLoanForwordToDDO',
    DeleteEmpDetails: apiHost + 'ExistingLoan/DeleteEmpDetails',
    //----------------RecoveryChallan------------------------------
    GetRecoveryChallan: 'RecoveryChallan/GetRecoveryChallan?EmpCode=',
    saveUpdateRecoveryChallan: 'RecoveryChallan/SaveUpdateRecoveryChallan',
    //-------------------------------------------------------------------------
    //----------------new loan application------------------------------
    GetLoanpurposebyLoanCode: 'LoanApplication/GetLoanpurposebyLoanCode?payLoanCode=',
    GetDetailsbyLoanCodePurposecode: 'LoanApplication/GetDetailsbyLoanCodePurposecode?payLoanCode=',
    GetAllEmpNameSelectedByDesignId: 'existingloan/GetAllEmpNameSelectedDesignId?MsDesignID=',
    GetEmpDetailsbyEmpcode: 'LoanApplication/GetEmpDetailsbyEmpcode?EmpCode=',
    GetSanctionDetailsbyEmpcode: 'LoanApplication/GetSanctionDetailsbyEmpcode?EmpCode=',
    GetPayLoanPurposeStatus: 'LoanApplication/GetPayLoanPurposeStatus?LoanBillID=',
    PostLoanDetails: 'LoanApplication/SaveLoanDetails',
    PostPropertyOwnerDetails: 'LoanApplication/SavePropertyOwnerDetails',
    UpdateLoanDetails: 'LoanApplication/UpdateLoanDetails',
    getExistingLoanType: 'ExistingLoan/BindDropDownLoanType',
    getBindHeadAccount: 'existingloan/BindHeadAccount?LoanCD=',
    ExistingLoanInsertData: 'existingloan/SaveExistingLoanData',
    GetLoanDetails: 'LoanApplication/GetLoanDetails?mode=',
    GetFwdtochkerDetails: 'LoanApplication/GetFwdtochkerDetails',
    GetEmpLoanDetailsByID: 'LoanApplication/GetEmpLoanDetailsByID?EmpCd=',
    EditLoanDetailsByEMPID: 'LoanApplication/EditLoanDetailsByEmpID?id=',
    UpdateLoanDetailsbyID: 'LoanApplication/UpdateLoanDetailsbyID?id=',
    DeleteLoanDetailsByEMPID: 'LoanApplication/DeleteLoanDetailsByEMPID?id=',
    GetEmpDetails: 'LoanApplication/GetEmpDetails?EmpCd=',
    GetEmpfilterbydesignBillgroup: 'LoanApplication',
    GetBillgroupfilterbydesignID: 'LoanApplication',
    UpdateSanctionDetails: 'LoanApplication/UpdateSanctionDetails',
    ValidateCoolingPeriod: 'LoanApplication/ValidateCoolingPeriod?LoanCode=',
    //end
    // --Shared----
    SharedControllerName: 'Shared',
    //--end
    // --Recovery Excess Payment
    GetFinancialYears: apiHost + 'ExcessRecoveryPayment/GetFinancialYears',
    GetRecoveryLoanType: apiHost + 'ExcessRecoveryPayment/GetLoanPaymentType',
    GetRecoveryElement: apiHost + 'ExcessRecoveryPayment/GetRecoveryElement',
    GetAccountHead: apiHost + 'ExcessRecoveryPayment/GetAccountHead',
    GetRecoveryComponent: apiHost + 'ExcessRecoveryPayment/GetRecoveryComponent',
    InsertUpdateExRecovery: apiHost + 'ExcessRecoveryPayment/InsertUpdateExRecovery',
    GetExcessRecoveryDetail: apiHost + 'ExcessRecoveryPayment/GetExcessRecoveryDetail',
    EditExcessRecoveryDetail: apiHost + 'ExcessRecoveryPayment/EditExcessRecoveryDetail',
    DeleteExcessRecoveryData: apiHost + 'ExcessRecoveryPayment/DeleteExcessRecoveryData?RecExcessId=',
    ForwardExRecoveryData: apiHost + 'ExcessRecoveryPayment/ForwardExRecoveryData',
    insertUpdateComponentAmt: apiHost + 'ExcessRecoveryPayment/insertUpdateComponentAmt',
    getComponentAmtDetails: apiHost + 'ExcessRecoveryPayment/getComponentAmtDetails',
    deleteComponentAmt: apiHost + 'ExcessRecoveryPayment/deleteComponentAmt?CmpId=',
    SaveTempPayment: apiHost + 'RecoveryPayment/SaveTempPayment',
    SaveRecoveryExcess: apiHost + 'RecoveryPayment/SaveRecoveryExcess',
    getTempPaymentRecovery: apiHost + 'RecoveryPayment/getTempPaymentRecovery',
    //--end
    //Depuatation
    getPayItems: 'DeputationIn/GetPayItems',
    getDesignationAll: 'DeputationIn/GetAllDesignation?controllerId=',
    getDepuatationCommunticationAddress: 'DeputationIn/GetCommunicationAddress',
    getDeductionScheduleDetailsbyEmployee: 'DeputationIn/GetDeductionScheduleDetails',
    saveUpdateDeputationInDetails: 'DeputationIn/SaveUpdateDeputationInDetails',
    getScheme_CodeByMsEmpDuesID: 'DeputationIn/GetScheme_CodeByMsEmpDuesID?MsEmpDuesID=',
    getAllDepuatationOutDetailsByEmployee: 'DeputationOut/GetAllDepuatationOutDetailsByEmployee',
    saveUpdateDeputationOutDetails: 'DeputationOut/SaveUpdateDeputationOutDetails',
    getAllRepatriationDetailsByEmployee: 'RepatriationDetails/GetAllRepatriationDetailsByEmployee',
    saveUpdateRepatriationDetails: 'RepatriationDetails/SaveUpdateRepatriationDetails',
    updateforwardStatusUpdateByDepID: 'DeputationIn/UpdateforwardStatusUpdateByDepId',
    updateforwardStatusUpdateByEmpCode: 'DeputationIn/UpdateforwardStatusUpdateByEmpCode',
    getCheckDuputationInEmployee: 'RepatriationDetails/CheckDepuatationinEmployee',
    //--end
    //Promotions
    PromotionControllerName: 'Promotion',
    //--end
    //ReJoiningService
    ReJoiningServiceControllerName: 'ReJoiningService',
    //--end
    //Lien Period
    getControllerList: 'Transferofflienperiod/GetControllList',
    getDdolistbyControllerid: 'Transferofflienperiod/GetDdolistbyControllerid?controllerId=',
    getOfficeAddressDetails: 'Transferofflienperiod/GetOfficeAddressDetails',
    saveUpdateTransferOffLienPeriod: 'Transferofflienperiod/SaveUpdateTransferOffLienPeriod',
    deleteTransferOffLienPeriod: 'Transferofflienperiod/DeleteTransferOffLienPeriod',
    getTransferofficelienPeriodDetails: 'Transferofflienperiod/getTransferofficelienPeriodDetails',
    lienPeriodUpdateForwardStatusUpdateByEmpCode: 'Transferofflienperiod/UpdateforwardStatusUpdateByEmpCode',
    getEpsEmpJoinOffAfterLienPeriodDetailByEmp: 'EpsEmpJoinOffAfterLienPeriod/GetEpsEmpJoinOffAfterLienPeriodDetailByEmp',
    getJoiningAccountOf: 'master/GetJoiningAccount',
    getOfficeList: 'master/GetOfficeList',
    getOfficeCityClass: 'master/GetOfficeCityClassList',
    saveUpdateEpsEmployeeJoinAfterLienPeriod: 'EpsEmpJoinOffAfterLienPeriod/SaveUpdateEpsEmployeeJoinAfterLienPeriod',
    deleteEpsEmployeeJoinAfterLienPeriodById: 'EpsEmpJoinOffAfterLienPeriod/DeleteEpsEmployeeJoinById',
    epsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: 'EpsEmpJoinOffAfterLienPeriod/UpdateforwardStatusUpdateByEmpCode',
    getPayCommissionListLien: 'master/GetPayCommissionList',
    getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp: 'NonEpsEmpAfterLienPeriod/GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp',
    saveUpdateNonEpsEmployeeJoinAfterLienPeriod: 'NonEpsEmpAfterLienPeriod/SaveUpdateNonEpsEmployeeJoinAfterLienPeriod',
    deleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode: 'NonEpsEmpAfterLienPeriod/DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode',
    nonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: 'NonEpsEmpAfterLienPeriod/NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode',
    //--end
    //Increment
    //Advance Increment
    CreateAdvIncrement: apiHost + 'AdvIncrement/CreateAdvIncrement',
    GetIncrementDetails: apiHost + 'AdvIncrement/GetIncrementDetails?empcode=',
    EditAdvIncrement: apiHost + 'AdvIncrement/EditAdvIncrement?incID=',
    DeleteAdvDetails: apiHost + 'AdvIncrement/DeleteAdvDetails?incID=',
    Forwardtochecker: apiHost + 'AdvIncrement/Forwardtochecker',
    GetEmployeeByDesigPayComm: apiHost + 'AdvIncrement/GetEmployeeByDesigPayComm',
    // Regular Incremnt
    CreateOrderNo: apiHost + 'Regincrement/CreateOrderNo',
    GetOrderDetails: apiHost + 'Regincrement/GetOrderDetails',
    deleteOrderDetails: apiHost + 'Regincrement/deleteOrderDetails',
    GetEmployeeForIncrement: apiHost + 'Regincrement/GetEmployeeForIncrement',
    GetEmployeeForNonIncrement: apiHost + 'Regincrement/GetEmployeeForNonIncrement',
    AddEmpForIncrement: apiHost + 'Regincrement/InsertRegIncrementData',
    ForwardtocheckerForInc: apiHost + 'Regincrement/ForwardtocheckerForInc',
    GetEmployeeDesignation: apiHost + 'Regincrement',
    // Stop Increment
    GetEmployeeStopIncrement: apiHost + 'StopIncrement/GetEmployeeStopIncrement',
    GetEmployeeForNotStop: apiHost + 'StopIncrement/GetEmployeeForNotStop',
    InsertStopIncrementData: apiHost + 'StopIncrement/InsertStopIncrementData',
    ForwardcheckerForStopInc: apiHost + 'StopIncrement/ForwardcheckerForStopInc',
    // Annual Increment Report
    GetSalaryMonth: apiHost + 'AnnualIncrement/GetSalaryMonth',
    GetOrderWithEmployee: apiHost + 'AnnualIncrement/GetOrderWithEmployee',
    GetAnnualIncReportData: apiHost + 'AnnualIncrement/GetAnnualIncReportData',
    GetEmpDueForIncReport: apiHost + 'AnnualIncrement/GetEmpDueForIncReport',
    //ENd
    //change
    ChangeDeleteDetails: 'Change/ChangeDeleteDetails?empCd=',
    GetDdlvalue: 'Change/GetDdlvalue?Flag=',
    EOSControllerName: 'EOS',
    GetExtensionOfServiceDetails: 'EOS/GetExtensionOfServiceDetails?empcode=',
    //FetchEOSDetails: 'EOS/FetchEOSDetails?employeeId=',
    ForwardToCheckerUserDtls: 'EOS/ForwardToCheckerUserDtls?empCd=',
    InsertDOBDetails: 'Change',
    GetDOBDetails: 'Change/GetDOBDetails?empcode=',
    ForwardToCheckerDOBUserDtls: 'Change/ForwardToCheckerDOBUserDtls?empCd=',
    InsertNameGenderDetails: 'Change',
    GetNameGenderDetails: 'Change/GetNameGenderDetails?empcode=',
    ForwardToCheckerNameGenderUserDtls: 'Change/ForwardToCheckerNameGenderUserDtls?empCd=',
    InsertPfNpsFormDetails: 'Change',
    GetPfNpsFormDetails: 'Change/GetPfNpsFormDetails?empcode=',
    ForwardToCheckerPfNpsUserDtls: 'Change/ForwardToCheckerPfNpsUserDtls?empCd=',
    InsertPanNoFormDetails: 'Change',
    GetPanNoFormDetails: 'Change/GetPanNoFormDetails?empcode=',
    //FetchPanNosrchDetails: 'Change/FetchPanNosrchDetails?employeeId=',
    ForwardToCheckerPanNoUserDtls: 'Change/ForwardToCheckerPanNoUserDtls?empCd=',
    InsertHraccFormDetails: 'Change',
    GetHraccFormDetails: 'Change/GetHraccFormDetails?empcode=',
    //FetchHraccsrchDetails: 'Change/FetchHraccsrchDetails?employeeId=',  
    ForwardToCheckerHraccUserDtls: 'Change/ForwardToCheckerHraccUserDtls?empCd=',
    InsertdesignationFormDetails: 'Change',
    GetdesignationFormDetails: 'Change/GetdesignationFormDetails?empcode=',
    ForwardToCheckerdesignationUserDtls: 'Change/ForwardToCheckerdesignationUserDtls?empCd=',
    InsertexservicemenFormDetails: 'Change',
    GetexservicemenFormDetails: 'Change/GetexservicemenFormDetails?empcode=',
    ForwardToCheckerexservicemenUserDtls: 'Change/ForwardToCheckerexservicemenUserDtls?empCd=',
    InsertCGHSFormDetails: 'Change',
    GetCGHSFormDetails: 'Change/GetCGHSFormDetails?empcode=',
    ForwardToCheckerCGHSUserDtls: 'Change/ForwardToCheckerCGHSUserDtls?empCd=',
    InsertCGEGISFormDetails: 'Change',
    GetCGEGISFormDetails: 'Change/GetCGEGISFormDetails?empcode=',
    ForwardToCheckerCGEGISFormUserDtls: 'Change/ForwardToCheckerCGEGISFormUserDtls?empCd=',
    InsertJoiningModeFormDetails: 'Change',
    GetJoiningModeFormDetails: 'Change/GetJoiningModeFormDetails?empcode=',
    ForwardToCheckerJoiningModeFormUserDtls: 'Change/ForwardToCheckerJoiningModeFormUserDtls?empCd=',
    InsertDOJDetails: 'Change',
    GetDOJDetails: 'Change/GetDOJDetails?empcode=',
    ForwardToCheckerDOJUserDtls: 'Change/ForwardToCheckerDOJUserDtls?empCd=',
    InsertDOEDetails: 'Change',
    GetDOEDetails: 'Change/GetDOEDetails?empcode=',
    ForwardToCheckerDOEUserDtls: 'Change/ForwardToCheckerDOEUserDtls?empCd=',
    InsertDORDetails: 'Change',
    GetDORDetails: 'Change/GetDORDetails?empcode=',
    ForwardToCheckerDORUserDtls: 'Change/ForwardToCheckerDORUserDtls?empCd=',
    InsertCasteCategoryFormDetails: 'Change',
    GetCasteCategoryFormDetails: 'Change/GetCasteCategoryFormDetails?empcode=',
    ForwardToCheckerCasteCategoryFormUserDtls: 'Change/ForwardToCheckerCasteCategoryFormUserDtls?empCd=',
    InsertEntofficevehicleFormDetails: 'Change',
    GetEntofficevehicleFormDetails: 'Change/GetEntofficevehicleFormDetails?empcode=',
    ForwardToCheckerEntofficevehicleFormUserDtls: 'Change/ForwardToCheckerEntofficevehicleFormUserDtls?empCd=',
    InsertBankFormDetails: 'Change',
    GetBankFormDetails: 'Change/GetBankFormDetails?empcode=',
    ForwardToCheckerBankFormUserDtls: 'Change/ForwardToCheckerBankFormUserDtls?empCd=',
    InsertaddtapdFormDetails: 'Change',
    GetaddtapdFormDetails: 'Change/GetaddtapdFormDetails?empcode=',
    ForwardToCheckeraddtapdUserDtls: 'Change/ForwardToCheckeraddtapdUserDtls?empCd=',
    InsertTPTAFormDetails: 'Change',
    GetTPTAFormDetails: 'Change/GetTPTAFormDetails?empcode=',
    ForwardToCheckerTPTAUserDtls: 'Change/ForwardToCheckerTPTAUserDtls?empCd=',
    InsertStateGISFormDetails: 'Change',
    GetStateGISFormDetails: 'Change/GetStateGISFormDetails?empcode=',
    ForwardToCheckerStateGISUserDtls: 'Change/ForwardToCheckerStateGISUserDtls?empCd=',
    InsertMobileNoEmailEmpCodeDetails: 'Change',
    GetMobileNoEmailEmpCodeDetails: 'Change/GetMobileNoEmailEmpCodeDetails?empcode=',
    ForwardToCheckerMobileNoEmailEmpCodeUserDtls: 'Change/ForwardToCheckerMobileNoEmailEmpCodeUserDtls?empCd=',
    //--end
    // -------End of Service Module--------------
    EndofServiceGetEmployee: 'EndofService/GetEndofServiceEmployees',
    EndofServiceReason: 'EndofService/EndofServiceReasonDetails',
    EmpCurrentEndDeta: 'EndofService/EmpCurrentEndDetails',
    EndofServiceGetDetailByEmp: 'EndofService/EndofServiceListDetails',
    EndofServiceInsert: 'EndofService/EndofServiceInsert',
    EndofServiceUpdate: 'EndofService/EndofServiceUpdate',
    EndofServiceDelete: 'EndofService/DeleteEndofService',
    // -------End of Service Module--------
    //User Profile
    GetUserProfileDetails: apiHost + 'UserProfile/GetUserdetailsProfile',
    //End Of User Profile
    IncomeTaxControllerName: 'IncomeTax',
    CityMasterController: 'CityMaster',
    //============================== Deduction Master ========================================
    categoryList: apiHost + 'DeductionMaster/CategoryList',
    deductionDescription: apiHost + 'DeductionMaster/DeductionDescription',
    deductionDescriptionChange: apiHost + 'DeductionMaster/DeductionDescriptionChange?payItemsCD=',
    getAutoGenratedDeductionCode: apiHost + 'DeductionMaster/GetAutoGenratedDeductionCode',
    getDeductionDefinitionList: apiHost + 'DeductionMaster/GetDeductionDefinitionList',
    deductionDefinitionSubmit: apiHost + 'DeductionMaster/DeductionDefinitionSubmit',
    //=============Deduction Rate Master==========
    GetOrgnaztionTypeForDeductionRate: 'DeductionMaster/GetOrgnazationTypeForDues',
    GetStateForDeductionRate: 'DeductionMaster/GetStateForDuesRate',
    GetPayCommForDeductionRate: 'DeductionMaster/GetPayCommForDuesRate',
    GetCityClassForDeductionRate: 'DeductionMaster/GetCityClassForDuesRate?payCommId=',
    GetDeductionCodeDeductionDefination: 'DeductionMaster/GetDuesCodeDuesDefination',
    AddDeductionDetails: 'DeductionMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
    GetDeductionSlabTypeByPayCommId: 'DeductionMaster/GetSlabtypeByPayCommId?payCommId=',
    saveUpdateDeductionRateDetails: 'DeductionMaster/SaveUpdateDuesRateDetails',
    GetDeductionRateList: 'DeductionMaster/GetDuesCodeDuesRate',
    DeleteDeductionRatemaster: apiHost + 'DeductionMaster/DeleteDuesRateDetails?msduesratedetailsid=',
};
var GlobalModule = /** @class */ (function () {
    function GlobalModule() {
    }
    GlobalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]
            ],
            declarations: [],
            exports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]
            ],
            providers: [{
                    provide: APP_CONFIG,
                    useValue: APP_DI_CONFIG
                }]
        })
    ], GlobalModule);
    return GlobalModule;
}());



/***/ }),

/***/ "./src/app/login-activate/login-activate.component.css":
/*!*************************************************************!*\
  !*** ./src/app/login-activate/login-activate.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luLWFjdGl2YXRlL2xvZ2luLWFjdGl2YXRlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login-activate/login-activate.component.html":
/*!**************************************************************!*\
  !*** ./src/app/login-activate/login-activate.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!--<div align=\"center\">\r\n  <p>\r\n    Account :  {{message}}\r\n  </p>\r\n  <div>\r\n    <button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Please click here for Login</button>\r\n  </div>\r\n</div>-->\r\n\r\n<app-dialog [(visible)]=\"DeleteMenu\">\r\n\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Login confirmation status.</h3>\r\n    <!-- Large input -->\r\n    <div class=\"md-form form-lg\">\r\n      <b>Account :  {{message}}</b>\r\n    </div>\r\n    <br />\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <!--<button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Please click here for Login.</button>-->\r\n      <p> Please click <a href=\"#\" (click)='redirectto()'>here</a> for Login!</p>\r\n     \r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/login-activate/login-activate.component.ts":
/*!************************************************************!*\
  !*** ./src/app/login-activate/login-activate.component.ts ***!
  \************************************************************/
/*! exports provided: LoginActivateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginActivateComponent", function() { return LoginActivateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_loginservice__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/loginservice */ "./src/app/login/loginservice.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginActivateComponent = /** @class */ (function () {
    function LoginActivateComponent(router, route, _Service, _snackBar) {
        this.router = router;
        this.route = route;
        this._Service = _Service;
        this._snackBar = _snackBar;
        this.userroleActivate = [];
    }
    LoginActivateComponent.prototype.ngOnInit = function () {
        this.DeleteMenu = true;
        this.ActivateUser();
    };
    LoginActivateComponent.prototype.ActivateUser = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.genId = params['id'];
            if (_this.genId != null) {
                _this._Service.RoleActivation(_this.genId).subscribe(function (data) {
                    _this.userroleActivate = data;
                    _this.message = _this.userroleActivate;
                });
            }
        });
    };
    LoginActivateComponent.prototype.redirectto = function () {
        this.router.navigate(['dashboard/login']);
    };
    LoginActivateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-activate',
            template: __webpack_require__(/*! ./login-activate.component.html */ "./src/app/login-activate/login-activate.component.html"),
            styles: [__webpack_require__(/*! ./login-activate.component.css */ "./src/app/login-activate/login-activate.component.css")],
            providers: [_login_loginservice__WEBPACK_IMPORTED_MODULE_2__["LoginServices"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _login_loginservice__WEBPACK_IMPORTED_MODULE_2__["LoginServices"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], LoginActivateComponent);
    return LoginActivateComponent;
}());



/***/ }),

/***/ "./src/app/login/getLoginUserDetails.ts":
/*!**********************************************!*\
  !*** ./src/app/login/getLoginUserDetails.ts ***!
  \**********************************************/
/*! exports provided: getLoginUserDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLoginUserDetails", function() { return getLoginUserDetails; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var getLoginUserDetails = /** @class */ (function () {
    function getLoginUserDetails() {
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    getLoginUserDetails.prototype.SendLoginUserDetails = function (Username, paoid, ddoid, controllerID, msUserID, empPermDDOId) {
        this.subject.next({ Username: Username, paoid: paoid, ddoid: ddoid, controllerID: controllerID, msUserID: msUserID, empPermDDOId: empPermDDOId });
    };
    getLoginUserDetails.prototype.getLoginUserDetails = function () {
        return this.subject.asObservable();
    };
    getLoginUserDetails = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], getLoginUserDetails);
    return getLoginUserDetails;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\r\n\r\n  <div class=\"full-page login-page\" filter-color=\"black\">\r\n    <div class=\"content\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\r\n          <form action=\"#\" method=\"#\" novalidate=\"\" class=\"ng-untouched ng-pristine ng-valid slideInDown\">\r\n            <div class=\"card card-login\">\r\n              <div class=\"card-header text-center\" data-background-color=\"rose\">\r\n                <h4>Login</h4>\r\n              </div>\r\n\r\n            \r\n              <!--<div class=\"input-group\">\r\n                  <mat-radio-group [(ngModel)]=\"selectedRadio\" name=\"RadioButton\">\r\n                    <mat-radio-button value=\"Y\">All</mat-radio-button>\r\n                    <mat-radio-button value=\"N\">Specific</mat-radio-button>\r\n                  </mat-radio-group>\r\n              </div>-->\r\n              <div class=\"input-group\">\r\n                <span class=\"input-group-addon\">\r\n                  <i class=\"material-icons\">email</i>\r\n                </span>\r\n                <mat-form-field class=\"example-full-width\">\r\n                  <input matInput placeholder=\"Email Address\" [(ngModel)]=\"loginModel.Username\" name=\"username\" required>\r\n                </mat-form-field>\r\n              </div>\r\n              <div class=\"input-group\">\r\n                <span class=\"input-group-addon\">\r\n                  <i class=\"material-icons\">lock_outline</i>\r\n                </span>\r\n                <mat-form-field class=\"example-full-width\">\r\n                  <input matInput placeholder=\"Password\" type=\"password\" [(ngModel)]=\"loginModel.Password\" name=\"password\" required>\r\n                </mat-form-field>\r\n              </div>\r\n\r\n\r\n\r\n            </div>\r\n      <div class=\"footer text-center\">\r\n        <button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Login</button>\r\n      </div>\r\n    \r\n  </form>\r\n         \r\n            \r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n    <!-- <div class=\"footer text-center\">\r\n      <button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Let's go</button>\r\n    </div> -->\r\n    <!--<div class=\"full-page-background\" style=\"background-image: url('../../assets/img/login.jpeg')\"></div>-->\r\n  </div>\r\n</div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n<app-dialog [(visible)]=\"CheckLoginRole\">\r\n\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h4 class=\"card-title text-center\">Please select Role </h4>\r\n    <!-- Large input -->\r\n    <div class=\"col-md-12\">\r\n      <div class=\"col-md-4\">\r\n        <div class=\"md-form form-lg\">\r\n          <label for=\"inputLGEx\">User ID</label>\r\n          {{UserName}}\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"md-form form-lg\">\r\n          <mat-form-field class=\"wid-50\">           \r\n            <mat-select matNativeControl placeholder=\"Select Role\" [(ngModel)]=\"msRoleIDs\">\r\n              <mat-option label=\"Select Role\">Select Role</mat-option>\r\n              <mat-option *ngFor=\"let role of userDetails; let i=index\"  (click)=\"RoleSelectchanged(i)\" [value]=\"role.msRoleID\"> {{role.userRole}} </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n\r\n        </div>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-4\">\r\n        <button type=\"button\" class=\"btn btn-info text-center btn-sm\" (click)=\"RedirecToHome()\">GO</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control, .form-group .form-control {\n  border: 0;\n  background-image: linear-gradient(#9c27b0, #9c27b0), linear-gradient(#D2D2D2, #D2D2D2);\n  background-size: 0 2px, 100% 1px;\n  background-repeat: no-repeat;\n  background-position: center bottom, center calc(100% - 1px);\n  background-color: rgba(0, 0, 0, 0);\n  transition: background 0s ease-out;\n  float: none;\n  box-shadow: none;\n  border-radius: 0;\n  font-weight: 400; }\n\n.login-page > .content,\n.lock-page > .content {\n  padding-top: 18vh; }\n\n.form-group {\n  padding-bottom: 10px;\n  margin: 20px 0 0 0;\n  position: relative; }\n\n.login-page .card-login .btn-wd {\n  min-width: 180px; }\n\n.login-page .card-login .card-header {\n  margin-top: -40px;\n  margin-bottom: 20px; }\n\n.login-page .card-login .card-header .title {\n  margin-top: 10px; }\n\n.login-page > .content,\n.lock-page > .content {\n  padding-top: 18vh; }\n\n.login-page .card-login {\n  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);\n  -webkit-transform: translateY(0px);\n          transform: translateY(0px);\n  transition: width .35s ease-in-out; }\n\n.login-page .card-login.card-hidden {\n    opacity: 0; }\n\n.login-page .card-login .btn-wd {\n    min-width: 180px; }\n\n.login-page .card-login .card-header {\n    margin-top: -40px;\n    margin-bottom: 20px; }\n\n.login-page .card-login .card-header .title {\n      margin-top: 10px; }\n\n.full-page[filter-color=\"rose\"].lock-page .form-group .form-control {\n  background-image: linear-gradient(#e91e63, #e91e63), linear-gradient(#D2D2D2, #D2D2D2); }\n\n.full-page[data-image]:after {\n  opacity: .8; }\n\n.full-page > .content,\n.full-page > .footer {\n  position: relative;\n  z-index: 4; }\n\n.full-page > .content {\n  min-height: calc(100vh - 80px); }\n\n.full-page .full-page-background {\n  position: absolute;\n  z-index: 1;\n  height: 100%;\n  width: 100%;\n  display: block;\n  top: 0;\n  left: 0;\n  background-size: cover;\n  background-position: center center; }\n\n.card {\n  display: inline-block;\n  position: relative;\n  width: 100%;\n  margin: 25px 0;\n  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);\n  border-radius: 6px;\n  color: rgba(0, 0, 0, 0.87);\n  background: #fff; }\n\n.card [data-background-color] {\n  box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);\n  margin: -20px 15px 0;\n  border-radius: 3px;\n  padding: 15px;\n  background-color: #999999;\n  position: relative; }\n\n.card [data-background-color=\"rose\"] {\n  background: linear-gradient(60deg, #3d3f6f, #3d3f6f);\n  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px #3d3f6f 233, 30, 99, 0.4; }\n\n.card [data-background-color] {\n  color: #FFFFFF; }\n\n.input-group .input-group-btn {\n  padding: 0 12px; }\n\n.input-group .input-group-addon {\n  border: 0;\n  background: transparent;\n  padding: 6px 15px 0px; }\n\n.input-group {\n  position: relative;\n  display: table;\n  border-collapse: separate; }\n\n.btn.btn-rose.btn-simple, .navbar .navbar-nav > li > a.btn.btn-rose.btn-simple {\n  background-color: transparent;\n  color: #3d3f6f;\n  box-shadow: none; }\n\n.example-full-width {\n  width: 250px; }\n\n.modal.fade {\n  opacity: 1; }\n\n.modal.fade .modal-dialog {\n  -webkit-transform: translate(0);\n  transform: translate(0); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vTTpcXGdpdEVQU1xcZXBzYXBwXFxFUFNcXEVQUy5XZWJBcHBcXENsaWVudEFwcC9zcmNcXGFwcFxcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsVUFBUztFQUNULHVGQUFzRjtFQUN0RixpQ0FBZ0M7RUFDaEMsNkJBQTRCO0VBQzVCLDREQUEyRDtFQUMzRCxtQ0FBa0M7RUFDbEMsbUNBQWtDO0VBQ2xDLFlBQVc7RUFDWCxpQkFBZ0I7RUFDaEIsaUJBQWdCO0VBQ2hCLGlCQUFnQixFQUNqQjs7QUFFRDs7RUFFRSxrQkFBaUIsRUFDbEI7O0FBQ0Q7RUFDRSxxQkFBb0I7RUFDcEIsbUJBQWtCO0VBQ2xCLG1CQUFrQixFQUNuQjs7QUFvQkQ7RUFDRSxpQkFBZ0IsRUFDakI7O0FBQ0Q7RUFDRSxrQkFBaUI7RUFDakIsb0JBQW1CLEVBQ3BCOztBQUNEO0VBQ0UsaUJBQWdCLEVBQ2pCOztBQUlEOztFQUdRLGtCQUFpQixFQUNwQjs7QUFHTDtFQUVRLDRDQUF3QztFQUN4QyxtQ0FBMEI7VUFBMUIsMkJBQTBCO0VBQzFCLG1DQUFrQyxFQWdCckM7O0FBcEJMO0lBTVksV0FBVSxFQUNiOztBQVBUO0lBU1ksaUJBQWdCLEVBQ25COztBQVZUO0lBYVksa0JBQWdCO0lBQ2hCLG9CQUFtQixFQUt0Qjs7QUFuQlQ7TUFpQmdCLGlCQUFnQixFQUNuQjs7QUFNYjtFQUNFLHVGQUFzRixFQUN2Rjs7QUFDRDtFQUNFLFlBQVcsRUFDWjs7QUFDRDs7RUFFRSxtQkFBa0I7RUFDbEIsV0FBVSxFQUNYOztBQUNEO0VBQ0UsK0JBQThCLEVBQy9COztBQUNEO0VBQ0UsbUJBQWtCO0VBQ2xCLFdBQVU7RUFDVixhQUFZO0VBQ1osWUFBVztFQUNYLGVBQWM7RUFDZCxPQUFNO0VBQ04sUUFBTztFQUNQLHVCQUFzQjtFQUN0QixtQ0FBa0MsRUFDbkM7O0FBR0Q7RUFDRSxzQkFBcUI7RUFDckIsbUJBQWtCO0VBQ2xCLFlBQVc7RUFDWCxlQUFjO0VBQ2QsNENBQTJDO0VBQzNDLG1CQUFrQjtFQUNsQiwyQkFBd0I7RUFDeEIsaUJBQWdCLEVBQ2pCOztBQUVEO0VBQ0UsMEhBQXlIO0VBQ3pILHFCQUFvQjtFQUNwQixtQkFBa0I7RUFDbEIsY0FBYTtFQUNiLDBCQUF5QjtFQUN6QixtQkFBa0IsRUFDbkI7O0FBR0Q7RUFDRSxxREFBb0Q7RUFDcEQseUZBQXdGLEVBQ3pGOztBQUVEO0VBQ0UsZUFBYyxFQUNmOztBQUVEO0VBQ0UsZ0JBQWUsRUFDaEI7O0FBQ0Q7RUFDRSxVQUFTO0VBQ1Qsd0JBQXVCO0VBQ3ZCLHNCQUFxQixFQUN0Qjs7QUFFRDtFQUNFLG1CQUFrQjtFQUNsQixlQUFjO0VBQ2QsMEJBQXlCLEVBQzFCOztBQUVEO0VBQ0UsOEJBQTZCO0VBQzdCLGVBQWM7RUFDZCxpQkFBZ0IsRUFDakI7O0FBRUQ7RUFDRSxhQUFXLEVBQ1o7O0FBR0Q7RUFDRSxXQUFTLEVBQ1Y7O0FBQ0Q7RUFDRyxnQ0FBK0I7RUFFL0Isd0JBQXVCLEVBQ3pCIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmZvcm0tY29udHJvbCwgLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbCB7XHJcbiAgYm9yZGVyOiAwO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgjOWMyN2IwLCAjOWMyN2IwKSwgbGluZWFyLWdyYWRpZW50KCNEMkQyRDIsICNEMkQyRDIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogMCAycHgsIDEwMCUgMXB4O1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGJvdHRvbSwgY2VudGVyIGNhbGMoMTAwJSAtIDFweCk7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwKTtcclxuICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIDBzIGVhc2Utb3V0O1xyXG4gIGZsb2F0OiBub25lO1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMDtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4ubG9naW4tcGFnZSA+IC5jb250ZW50LFxyXG4ubG9jay1wYWdlID4gLmNvbnRlbnQge1xyXG4gIHBhZGRpbmctdG9wOiAxOHZoO1xyXG59XHJcbi5mb3JtLWdyb3VwIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW46IDIwcHggMCAwIDA7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4vLyAubG9naW4tcGFnZSAuY2FyZC1sb2dpbiB7XHJcbi8vICAgYm94LXNoYWRvdzogMCAxcHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KTtcclxuLy8gICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbi8vICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbi8vICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xyXG4vLyAgIC1tb3otdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcclxuLy8gICAtby10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xyXG4vLyAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xyXG4vLyAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XHJcbi8vIH1cclxuLy8gLmxvZ2luLXBhZ2UgLmNhcmQtbG9naW4uY2FyZC1oaWRkZW4ge1xyXG4vLyAgIG9wYWNpdHk6IDA7XHJcbi8vICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC02MHB4LCAwKTtcclxuLy8gICAtbW96LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTYwcHgsIDApO1xyXG4vLyAgIC1vLXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTYwcHgsIDApO1xyXG4vLyAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC02MHB4LCAwKTtcclxuLy8gICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC02MHB4LCAwKTtcclxuLy8gfVxyXG4ubG9naW4tcGFnZSAuY2FyZC1sb2dpbiAuYnRuLXdkIHtcclxuICBtaW4td2lkdGg6IDE4MHB4O1xyXG59XHJcbi5sb2dpbi1wYWdlIC5jYXJkLWxvZ2luIC5jYXJkLWhlYWRlciB7XHJcbiAgbWFyZ2luLXRvcDogLTQwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4ubG9naW4tcGFnZSAuY2FyZC1sb2dpbiAuY2FyZC1oZWFkZXIgLnRpdGxlIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcbi5sb2dpbi1wYWdlLFxyXG4ubG9jay1wYWdle1xyXG4gICAgPiAuY29udGVudHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTh2aDtcclxuICAgIH1cclxufVxyXG5cclxuLmxvZ2luLXBhZ2V7XHJcbiAuY2FyZC1sb2dpbntcclxuICAgICAgICBib3gtc2hhZG93OiAwIDFweCA0cHggMCByZ2JhKDAsMCwwLDAuMTQpO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgwcHgpO1xyXG4gICAgICAgIHRyYW5zaXRpb246IHdpZHRoIC4zNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgJi5jYXJkLWhpZGRlbntcclxuICAgICAgICAgICAgb3BhY2l0eTogMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ0bi13ZHtcclxuICAgICAgICAgICAgbWluLXdpZHRoOiAxODBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkLWhlYWRlcntcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDotNDBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuXHJcbiAgICAgICAgICAgIC50aXRsZXtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIFxyXG5cclxuLmZ1bGwtcGFnZVtmaWx0ZXItY29sb3I9XCJyb3NlXCJdLmxvY2stcGFnZSAuZm9ybS1ncm91cCAuZm9ybS1jb250cm9sIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoI2U5MWU2MywgI2U5MWU2MyksIGxpbmVhci1ncmFkaWVudCgjRDJEMkQyLCAjRDJEMkQyKTtcclxufVxyXG4uZnVsbC1wYWdlW2RhdGEtaW1hZ2VdOmFmdGVyIHtcclxuICBvcGFjaXR5OiAuODtcclxufVxyXG4uZnVsbC1wYWdlID4gLmNvbnRlbnQsXHJcbi5mdWxsLXBhZ2UgPiAuZm9vdGVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogNDtcclxufVxyXG4uZnVsbC1wYWdlID4gLmNvbnRlbnQge1xyXG4gIG1pbi1oZWlnaHQ6IGNhbGMoMTAwdmggLSA4MHB4KTtcclxufVxyXG4uZnVsbC1wYWdlIC5mdWxsLXBhZ2UtYmFja2dyb3VuZCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxufVxyXG4gIFxyXG5cclxuLmNhcmQge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiAyNXB4IDA7XHJcbiAgYm94LXNoYWRvdzogMCAxcHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KTtcclxuICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgY29sb3I6IHJnYmEoMCwwLDAsIDAuODcpO1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuICAgICAgXHJcbi5jYXJkIFtkYXRhLWJhY2tncm91bmQtY29sb3JdIHtcclxuICBib3gtc2hhZG93OiAwIDEwcHggMzBweCAtMTJweCByZ2JhKDAsIDAsIDAsIDAuNDIpLCAwIDRweCAyNXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMTIpLCAwIDhweCAxMHB4IC01cHggcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gIG1hcmdpbjogLTIwcHggMTVweCAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICBwYWRkaW5nOiAxNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM5OTk5OTk7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59IFxyXG5cclxuICBcclxuLmNhcmQgW2RhdGEtYmFja2dyb3VuZC1jb2xvcj1cInJvc2VcIl0ge1xyXG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg2MGRlZywgIzNkM2Y2ZiwgIzNkM2Y2Zik7XHJcbiAgYm94LXNoYWRvdzogMCA0cHggMjBweCAwcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCA3cHggMTBweCAtNXB4ICMzZDNmNmYoMjMzLCAzMCwgOTksIDAuNCk7XHJcbn1cclxuICAgXHJcbi5jYXJkIFtkYXRhLWJhY2tncm91bmQtY29sb3JdIHtcclxuICBjb2xvcjogI0ZGRkZGRjtcclxufSBcclxuIFxyXG4uaW5wdXQtZ3JvdXAgLmlucHV0LWdyb3VwLWJ0biB7XHJcbiAgcGFkZGluZzogMCAxMnB4O1xyXG59XHJcbi5pbnB1dC1ncm91cCAuaW5wdXQtZ3JvdXAtYWRkb24ge1xyXG4gIGJvcmRlcjogMDtcclxuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICBwYWRkaW5nOiA2cHggMTVweCAwcHg7XHJcbn1cclxuXHJcbi5pbnB1dC1ncm91cCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IHRhYmxlO1xyXG4gIGJvcmRlci1jb2xsYXBzZTogc2VwYXJhdGU7XHJcbn1cclxuXHJcbi5idG4uYnRuLXJvc2UuYnRuLXNpbXBsZSwgLm5hdmJhciAubmF2YmFyLW5hdiA+IGxpID4gYS5idG4uYnRuLXJvc2UuYnRuLXNpbXBsZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgY29sb3I6ICMzZDNmNmY7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aHtcclxuICB3aWR0aDoyNTBweDtcclxufVxyXG5cclxuXHJcbi5tb2RhbC5mYWRle1xyXG4gIG9wYWNpdHk6MTtcclxufVxyXG4ubW9kYWwuZmFkZSAubW9kYWwtZGlhbG9nIHtcclxuICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwKTtcclxuICAgLW1vei10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwKTtcclxuICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCk7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _model_loginModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/loginModel */ "./src/app/model/loginModel.ts");
/* harmony import */ var _loginservice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loginservice */ "./src/app/login/loginservice.ts");
/* harmony import */ var _payroll_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../payroll/common */ "./src/app/payroll/common.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _login_getLoginUserDetails__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/getLoginUserDetails */ "./src/app/login/getLoginUserDetails.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = /** @class */ (function () {
    //logins: Observable<loginModel[]>;
    function LoginComponent(router, _Service, _common, _snackBar, route, _LoginUserDetails) {
        this.router = router;
        this._Service = _Service;
        this._common = _common;
        this._snackBar = _snackBar;
        this.route = route;
        this._LoginUserDetails = _LoginUserDetails;
        this.loginDetails = [];
        this.userDetails = [];
        this.userroleActivate = [];
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginModel = new _model_loginModel__WEBPACK_IMPORTED_MODULE_2__["loginModel"]();
        this.CheckLoginRole = false;
    };
    // genId: string;
    // param2: string;
    //ActivateUser() {
    //  this.route.queryParams.subscribe(params => {
    //    this.genId = params['id'];
    //    alert(this.genId)
    //    if (this.genId != null) {
    //      this._Service.RoleActivation(this.genId).subscribe(data => {
    //        this.userroleActivate = data;
    //        if (this.userroleActivate != null) {
    //          this._snackBar.open('Role Activated Successfully, Please Login!', null, { duration: 4000 });
    //        }
    //      });
    //    }
    //  });
    //}
    LoginComponent.prototype.UserDetails = function () {
        var _this = this;
        this._Service.UserDetails(this.loginModel.Username).subscribe(function (data) {
            _this.userDetails = data;
            //const logins = new loginModel();
            //logins.Username = this.loginModel.Username;
            _this._LoginUserDetails.SendLoginUserDetails(_this.loginModel.Username, _this.userDetails[0].paoid, _this.userDetails[0].ddoid, _this.userDetails[0].controllerID, _this.userDetails[0].msUserID, _this.userDetails[0].empPermDDOId);
            sessionStorage.setItem('username', _this.loginModel.Username);
            sessionStorage.setItem('paoid', _this.userDetails[0].paoid);
            sessionStorage.setItem('ddoid', _this.userDetails[0].ddoid);
            sessionStorage.setItem('controllerID', _this.userDetails[0].controllerID);
            sessionStorage.setItem('UserID', _this.userDetails[0].msUserID);
            sessionStorage.setItem('EmpPermDDOId', _this.userDetails[0].empPermDDOId);
            if (_this.userDetails.length == "1") {
                sessionStorage.setItem('UserID', _this.userDetails[0].msUserID);
                sessionStorage.setItem('userRole', _this.userDetails[0].userRole);
                sessionStorage.setItem('userRoleID', _this.userDetails[0].msRoleID);
                sessionStorage.setItem('EmpPermDDOId', _this.userDetails[0].empPermDDOId);
                //this.router.navigate(['dashboard/home']);
            }
            if (_this.userDetails.length > 1) {
                _this.UserName = sessionStorage.getItem('username');
                _this.CheckLoginRole = true;
            }
        });
    };
    LoginComponent.prototype.redirectto = function () {
        var _this = this;
        if (this.loginModel.Username == null) {
            this.username = this.loginModel.Username;
            this._common.username = this.username;
            alert("Please enter username/password");
            return;
        }
        if (this.loginModel.Username != null || this.loginModel.Username != '') {
            this.username = this.loginModel.Username;
            this._common.username = this.username;
        }
        if (this.loginModel.Password == null) {
            alert("Please enter username/password");
            return;
        }
        this._Service.Login(this.loginModel).subscribe(function (data) {
            _this.loginDetails = data;
            var Arrrolestatus = _this.loginDetails.split('_');
            if (Arrrolestatus[0] == 'yes') {
                if (Arrrolestatus[1] == 'O') {
                    _this.UserDetails();
                }
                if (Arrrolestatus[1] == 'N') {
                    sessionStorage.setItem('NewUser', _this.loginModel.Username);
                    _this.router.navigate(['/changepassword']);
                }
                if (Arrrolestatus[1] == 'NF') {
                    alert('Please activate your account.');
                }
            }
            else {
                alert('Please enter valid email/password.');
            }
        });
    };
    LoginComponent.prototype.RoleSelectchanged = function (RowIndex) {
        sessionStorage.setItem('userRole', this.userDetails[RowIndex].userRole);
        sessionStorage.setItem('userRoleID', this.userDetails[RowIndex].msRoleID);
    };
    LoginComponent.prototype.RedirecToHome = function () {
        if (this.msRoleIDs == undefined) {
            alert("Please Select Role");
            return;
        }
        //this.router.navigate(['dashboard/home']);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")],
            providers: [_loginservice__WEBPACK_IMPORTED_MODULE_3__["LoginServices"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _loginservice__WEBPACK_IMPORTED_MODULE_3__["LoginServices"], _payroll_common__WEBPACK_IMPORTED_MODULE_4__["common"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _login_getLoginUserDetails__WEBPACK_IMPORTED_MODULE_6__["getLoginUserDetails"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/loginservice.ts":
/*!***************************************!*\
  !*** ./src/app/login/loginservice.ts ***!
  \***************************************/
/*! exports provided: LoginServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginServices", function() { return LoginServices; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var LoginServices = /** @class */ (function () {
    function LoginServices(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
        this.BaseUrl = 'http://localhost:55424/api/';
    }
    LoginServices.prototype.Login = function (loginModel) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('Username', loginModel.Username).set('Password', loginModel.Password);
        return this.httpclient
            .get("" + this.config.api_base_url + this.config.LoginDetails, { params: params });
    };
    LoginServices.prototype.UserDetails = function (username) {
        return this.httpclient.get(this.config.UserDetails + username, {});
    };
    LoginServices.prototype.ChangePassword = function (ObjPwdDetails) {
        return this.httpclient.post(this.config.api_base_url + this.config.LoginNewUser, ObjPwdDetails, { responseType: 'text' });
    };
    LoginServices.prototype.RoleActivation = function (ID) {
        return this.httpclient.get(this.config.RoleActivation + ID, { responseType: 'text' });
    };
    LoginServices.prototype.currentrUrl = function (url) {
        return this.httpclient.get(this.config.currentrUrl + url, { responseType: 'text' });
    };
    LoginServices.prototype.IsMenuPermissionAssigned = function (UserName, msRoleIDs) {
        return this.httpclient.get(this.config.IsMenuPermissionAssigned + "?username=" + UserName + "&uroleid=" + msRoleIDs, { responseType: 'text' });
        //return this.http.post(`${this.config.api_base_url}${this.config.InsertPanNoFormDetails}/InsertPanNoFormDetails`,
        //  objPro, { responseType: 'text', params: parameter });
    };
    LoginServices = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], LoginServices);
    return LoginServices;
}());



/***/ }),

/***/ "./src/app/logintoken/httpInterceptor.ts":
/*!***********************************************!*\
  !*** ./src/app/logintoken/httpInterceptor.ts ***!
  \***********************************************/
/*! exports provided: httpInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "httpInterceptor", function() { return httpInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var httpInterceptor = /** @class */ (function () {
    function httpInterceptor() {
    }
    httpInterceptor.prototype.intercept = function (request, newRequest) {
        var tokenInfo = JSON.parse(sessionStorage.getItem('token'));
        if (tokenInfo) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + tokenInfo
                    //'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
        return newRequest.handle(request);
    };
    httpInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], httpInterceptor);
    return httpInterceptor;
}());



/***/ }),

/***/ "./src/app/logintoken/logintoken.component.css":
/*!*****************************************************!*\
  !*** ./src/app/logintoken/logintoken.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control, .form-group .form-control {\r\n  border: 0;\r\n  background-image: linear-gradient(#9c27b0, #9c27b0), linear-gradient(#D2D2D2, #D2D2D2);\r\n  background-size: 0 2px, 100% 1px;\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom, center calc(100% - 1px);\r\n  background-color: rgba(0, 0, 0, 0);\r\n  transition: background 0s ease-out;\r\n  float: none;\r\n  box-shadow: none;\r\n  border-radius: 0;\r\n  font-weight: 400;\r\n}\r\n\r\n.login-page > .content,\r\n.lock-page > .content {\r\n  padding-top: 18vh;\r\n}\r\n\r\n.form-group {\r\n  padding-bottom: 10px;\r\n  margin: 20px 0 0 0;\r\n  position: relative;\r\n}\r\n\r\n.login-page .card-login .btn-wd {\r\n  min-width: 180px;\r\n}\r\n\r\n.login-page .card-login .card-header {\r\n  margin-top: -40px;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.login-page .card-login .card-header .title {\r\n    margin-top: 10px;\r\n  }\r\n\r\n.login-page,\r\n.lock-page {\r\n  > .content\r\n\r\n{\r\n  padding-top: 18vh;\r\n}\r\n\r\n}\r\n\r\n.login-page {\r\n  .card-login\r\n\r\n{\r\n  box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);\r\n  -webkit-transform: translateY(0px);\r\n          transform: translateY(0px);\r\n  transition: width .35s ease-in-out;\r\n  &.card-hidden\r\n\r\n{\r\n  opacity: 0;\r\n}\r\n\r\n.btn-wd {\r\n  min-width: 180px;\r\n}\r\n\r\n.card-header {\r\n  margin-top: -40px;\r\n  margin-bottom: 20px;\r\n  .title\r\n\r\n{\r\n  margin-top: 10px;\r\n}\r\n\r\n}\r\n}\r\n}\r\n\r\n.full-page[filter-color=\"rose\"].lock-page .form-group .form-control {\r\n  background-image: linear-gradient(#e91e63, #e91e63), linear-gradient(#D2D2D2, #D2D2D2);\r\n}\r\n\r\n.full-page[data-image]:after {\r\n  opacity: .8;\r\n}\r\n\r\n.full-page > .content,\r\n.full-page > .footer {\r\n  position: relative;\r\n  z-index: 4;\r\n}\r\n\r\n.full-page > .content {\r\n  min-height: calc(100vh - 80px);\r\n}\r\n\r\n.full-page .full-page-background {\r\n  position: absolute;\r\n  z-index: 1;\r\n  height: 100%;\r\n  width: 100%;\r\n  display: block;\r\n  top: 0;\r\n  left: 0;\r\n  background-size: cover;\r\n  background-position: center center;\r\n}\r\n\r\n.card {\r\n  display: inline-block;\r\n  position: relative;\r\n  width: 100%;\r\n  margin: 25px 0;\r\n  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);\r\n  border-radius: 6px;\r\n  color: rgba(0,0,0, 0.87);\r\n  background: #fff;\r\n}\r\n\r\n.card [data-background-color] {\r\n    box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);\r\n    margin: -20px 15px 0;\r\n    border-radius: 3px;\r\n    padding: 15px;\r\n    background-color: #999999;\r\n    position: relative;\r\n  }\r\n\r\n.card [data-background-color=\"rose\"] {\r\n    background: linear-gradient(60deg, #3d3f6f, #3d3f6f);\r\n    box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px #3d3f6f(233, 30, 99, 0.4);\r\n  }\r\n\r\n.card [data-background-color] {\r\n    color: #FFFFFF;\r\n  }\r\n\r\n.input-group .input-group-btn {\r\n  padding: 0 12px;\r\n}\r\n\r\n.input-group .input-group-addon {\r\n  border: 0;\r\n  background: transparent;\r\n  padding: 6px 15px 0px;\r\n}\r\n\r\n.input-group {\r\n  position: relative;\r\n  display: table;\r\n  border-collapse: separate;\r\n}\r\n\r\n.btn.btn-rose.btn-simple, .navbar .navbar-nav > li > a.btn.btn-rose.btn-simple {\r\n  background-color: transparent;\r\n  color: #3d3f6f;\r\n  box-shadow: none;\r\n}\r\n\r\n.example-full-width {\r\n  width: 250px;\r\n}\r\n\r\n.modal.fade {\r\n  opacity: 1;\r\n}\r\n\r\n.modal.fade .modal-dialog {\r\n    -webkit-transform: translate(0);\r\n    transform: translate(0);\r\n  }\r\n\r\n@media only screen and (max-width: 1199px) and (min-width: 320px) {\r\n  .example-full-width {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW50b2tlbi9sb2dpbnRva2VuLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxVQUFVO0VBQ1YsdUZBQXVGO0VBQ3ZGLGlDQUFpQztFQUNqQyw2QkFBNkI7RUFDN0IsNERBQTREO0VBQzVELG1DQUFtQztFQUNuQyxtQ0FBbUM7RUFDbkMsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsaUJBQWlCO0NBQ2xCOztBQUVEOztFQUVFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsbUJBQW1CO0NBQ3BCOztBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG9CQUFvQjtDQUNyQjs7QUFFQztJQUNFLGlCQUFpQjtHQUNsQjs7QUFJSDs7RUFFRTs7O0VBR0Esa0JBQWtCO0NBQ25COztDQUVBOztBQUVEO0VBQ0U7OztFQUdBLHlDQUF5QztFQUN6QyxtQ0FBMkI7VUFBM0IsMkJBQTJCO0VBQzNCLG1DQUFtQztFQUNuQzs7O0VBR0EsV0FBVztDQUNaOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQjs7O0VBR0EsaUJBQWlCO0NBQ2xCOztDQUVBO0NBQ0E7Q0FDQTs7QUFHRDtFQUNFLHVGQUF1RjtDQUN4Rjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDs7RUFFRSxtQkFBbUI7RUFDbkIsV0FBVztDQUNaOztBQUVEO0VBQ0UsK0JBQStCO0NBQ2hDOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGVBQWU7RUFDZixPQUFPO0VBQ1AsUUFBUTtFQUNSLHVCQUF1QjtFQUN2QixtQ0FBbUM7Q0FDcEM7O0FBR0Q7RUFDRSxzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixlQUFlO0VBQ2YsNENBQTRDO0VBQzVDLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsaUJBQWlCO0NBQ2xCOztBQUVDO0lBQ0UsMEhBQTBIO0lBQzFILHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLDBCQUEwQjtJQUMxQixtQkFBbUI7R0FDcEI7O0FBR0Q7SUFDRSxxREFBcUQ7SUFDckQsMEZBQTBGO0dBQzNGOztBQUVEO0lBQ0UsZUFBZTtHQUNoQjs7QUFFSDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLFVBQVU7RUFDVix3QkFBd0I7RUFDeEIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSw4QkFBOEI7RUFDOUIsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLGFBQWE7Q0FDZDs7QUFHRDtFQUNFLFdBQVc7Q0FDWjs7QUFFQztJQUNFLGdDQUFnQztJQUVoQyx3QkFBd0I7R0FDekI7O0FBQ0g7RUFDRTtJQUNFLFlBQVk7R0FDYjtDQUNGIiwiZmlsZSI6InNyYy9hcHAvbG9naW50b2tlbi9sb2dpbnRva2VuLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250cm9sLCAuZm9ybS1ncm91cCAuZm9ybS1jb250cm9sIHtcclxuICBib3JkZXI6IDA7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCM5YzI3YjAsICM5YzI3YjApLCBsaW5lYXItZ3JhZGllbnQoI0QyRDJEMiwgI0QyRDJEMik7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAwIDJweCwgMTAwJSAxcHg7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgYm90dG9tLCBjZW50ZXIgY2FsYygxMDAlIC0gMXB4KTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApO1xyXG4gIHRyYW5zaXRpb246IGJhY2tncm91bmQgMHMgZWFzZS1vdXQ7XHJcbiAgZmxvYXQ6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5sb2dpbi1wYWdlID4gLmNvbnRlbnQsXHJcbi5sb2NrLXBhZ2UgPiAuY29udGVudCB7XHJcbiAgcGFkZGluZy10b3A6IDE4dmg7XHJcbn1cclxuXHJcbi5mb3JtLWdyb3VwIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW46IDIwcHggMCAwIDA7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5sb2dpbi1wYWdlIC5jYXJkLWxvZ2luIC5idG4td2Qge1xyXG4gIG1pbi13aWR0aDogMTgwcHg7XHJcbn1cclxuXHJcbi5sb2dpbi1wYWdlIC5jYXJkLWxvZ2luIC5jYXJkLWhlYWRlciB7XHJcbiAgbWFyZ2luLXRvcDogLTQwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuICAubG9naW4tcGFnZSAuY2FyZC1sb2dpbiAuY2FyZC1oZWFkZXIgLnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4ubG9naW4tcGFnZSxcclxuLmxvY2stcGFnZSB7XHJcbiAgPiAuY29udGVudFxyXG5cclxue1xyXG4gIHBhZGRpbmctdG9wOiAxOHZoO1xyXG59XHJcblxyXG59XHJcblxyXG4ubG9naW4tcGFnZSB7XHJcbiAgLmNhcmQtbG9naW5cclxuXHJcbntcclxuICBib3gtc2hhZG93OiAwIDFweCA0cHggMCByZ2JhKDAsMCwwLDAuMTQpO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgwcHgpO1xyXG4gIHRyYW5zaXRpb246IHdpZHRoIC4zNXMgZWFzZS1pbi1vdXQ7XHJcbiAgJi5jYXJkLWhpZGRlblxyXG5cclxue1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbi5idG4td2Qge1xyXG4gIG1pbi13aWR0aDogMTgwcHg7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlciB7XHJcbiAgbWFyZ2luLXRvcDogLTQwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAudGl0bGVcclxuXHJcbntcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG59XHJcbn1cclxufVxyXG5cclxuXHJcbi5mdWxsLXBhZ2VbZmlsdGVyLWNvbG9yPVwicm9zZVwiXS5sb2NrLXBhZ2UgLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbCB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCNlOTFlNjMsICNlOTFlNjMpLCBsaW5lYXItZ3JhZGllbnQoI0QyRDJEMiwgI0QyRDJEMik7XHJcbn1cclxuXHJcbi5mdWxsLXBhZ2VbZGF0YS1pbWFnZV06YWZ0ZXIge1xyXG4gIG9wYWNpdHk6IC44O1xyXG59XHJcblxyXG4uZnVsbC1wYWdlID4gLmNvbnRlbnQsXHJcbi5mdWxsLXBhZ2UgPiAuZm9vdGVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogNDtcclxufVxyXG5cclxuLmZ1bGwtcGFnZSA+IC5jb250ZW50IHtcclxuICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gODBweCk7XHJcbn1cclxuXHJcbi5mdWxsLXBhZ2UgLmZ1bGwtcGFnZS1iYWNrZ3JvdW5kIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogMTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xyXG59XHJcblxyXG5cclxuLmNhcmQge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiAyNXB4IDA7XHJcbiAgYm94LXNoYWRvdzogMCAxcHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KTtcclxuICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgY29sb3I6IHJnYmEoMCwwLDAsIDAuODcpO1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuXHJcbiAgLmNhcmQgW2RhdGEtYmFja2dyb3VuZC1jb2xvcl0ge1xyXG4gICAgYm94LXNoYWRvdzogMCAxMHB4IDMwcHggLTEycHggcmdiYSgwLCAwLCAwLCAwLjQyKSwgMCA0cHggMjVweCAwcHggcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCA4cHggMTBweCAtNXB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgIG1hcmdpbjogLTIwcHggMTVweCAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgcGFkZGluZzogMTVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5OTk5OTk7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG5cclxuXHJcbiAgLmNhcmQgW2RhdGEtYmFja2dyb3VuZC1jb2xvcj1cInJvc2VcIl0ge1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDYwZGVnLCAjM2QzZjZmLCAjM2QzZjZmKTtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDIwcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgN3B4IDEwcHggLTVweCAjM2QzZjZmKDIzMywgMzAsIDk5LCAwLjQpO1xyXG4gIH1cclxuXHJcbiAgLmNhcmQgW2RhdGEtYmFja2dyb3VuZC1jb2xvcl0ge1xyXG4gICAgY29sb3I6ICNGRkZGRkY7XHJcbiAgfVxyXG5cclxuLmlucHV0LWdyb3VwIC5pbnB1dC1ncm91cC1idG4ge1xyXG4gIHBhZGRpbmc6IDAgMTJweDtcclxufVxyXG5cclxuLmlucHV0LWdyb3VwIC5pbnB1dC1ncm91cC1hZGRvbiB7XHJcbiAgYm9yZGVyOiAwO1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gIHBhZGRpbmc6IDZweCAxNXB4IDBweDtcclxufVxyXG5cclxuLmlucHV0LWdyb3VwIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogdGFibGU7XHJcbiAgYm9yZGVyLWNvbGxhcHNlOiBzZXBhcmF0ZTtcclxufVxyXG5cclxuLmJ0bi5idG4tcm9zZS5idG4tc2ltcGxlLCAubmF2YmFyIC5uYXZiYXItbmF2ID4gbGkgPiBhLmJ0bi5idG4tcm9zZS5idG4tc2ltcGxlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICBjb2xvcjogIzNkM2Y2ZjtcclxuICBib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG4uZXhhbXBsZS1mdWxsLXdpZHRoIHtcclxuICB3aWR0aDogMjUwcHg7XHJcbn1cclxuXHJcblxyXG4ubW9kYWwuZmFkZSB7XHJcbiAgb3BhY2l0eTogMTtcclxufVxyXG5cclxuICAubW9kYWwuZmFkZSAubW9kYWwtZGlhbG9nIHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCk7XHJcbiAgICAtbW96LXRyYW5zZm9ybTogdHJhbnNsYXRlKDApO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCk7XHJcbiAgfVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDExOTlweCkgYW5kIChtaW4td2lkdGg6IDMyMHB4KSB7XHJcbiAgLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/logintoken/logintoken.component.html":
/*!******************************************************!*\
  !*** ./src/app/logintoken/logintoken.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\r\n\r\n  <div class=\"full-page login-page\" filter-color=\"black\">\r\n    <div class=\"content\">\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\r\n            <form action=\"#\" method=\"#\" novalidate=\"\" class=\"ng-untouched ng-pristine ng-valid slideInDown\">\r\n              <div class=\"card card-login\">\r\n                <div class=\"card-header text-center\" data-background-color=\"rose\">\r\n                  <h4>Login</h4>\r\n                </div>\r\n\r\n\r\n                <!--<div class=\"input-group\">\r\n            <mat-radio-group [(ngModel)]=\"selectedRadio\" name=\"RadioButton\">\r\n              <mat-radio-button value=\"Y\">All</mat-radio-button>\r\n              <mat-radio-button value=\"N\">Specific</mat-radio-button>\r\n            </mat-radio-group>\r\n        </div>-->\r\n                <div class=\"input-group\">\r\n                  <span class=\"input-group-addon\">\r\n                    <i class=\"material-icons\">email</i>\r\n                  </span>\r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"Email Address\" [(ngModel)]=\"loginModel.Username\" name=\"username\" required>\r\n                  </mat-form-field>\r\n                </div>\r\n                <div class=\"input-group\">\r\n                  <span class=\"input-group-addon\">\r\n                    <i class=\"material-icons\">lock_outline</i>\r\n                  </span>\r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"Password\" type=\"password\" [(ngModel)]=\"loginModel.Password\" name=\"password\" required>\r\n                  </mat-form-field>\r\n                </div>\r\n\r\n\r\n\r\n              </div>\r\n              <div class=\"footer text-center\">\r\n                <button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Login</button>\r\n              </div>\r\n\r\n            </form>\r\n\r\n\r\n\r\n          </div>\r\n          <a (click)='OnBoarding()'>New On-Boarding</a>\r\n          <br />\r\n          <a (click)='NewOnBoardingStatus()'>New OnBoarding Status</a>\r\n          \r\n        </div>\r\n      </div>\r\n      <!-- <div class=\"footer text-center\">\r\n        <button mat-button class=\"btn btn-rose btn-simple btn-wd btn-lg\" (click)='redirectto()'>Let's go</button>\r\n      </div> -->\r\n      <!--<div class=\"full-page-background\" style=\"background-image: url('../../assets/img/login.jpeg')\"></div>-->\r\n\r\n      \r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n<app-dialog [(visible)]=\"CheckLoginRole\">\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h4 class=\"card-title text-center\">Please select Role </h4>\r\n    <!-- Large input -->\r\n    <div class=\"col-md-12\">\r\n      <div class=\"col-md-4\">\r\n        <div class=\"md-form form-lg\">\r\n          <label for=\"inputLGEx\">User ID</label>\r\n          {{UserName}}\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"md-form form-lg\">\r\n          <mat-form-field class=\"wid-50\">\r\n            <mat-select matNativeControl placeholder=\"Select Role\" [(ngModel)]=\"msRoleIDs\">\r\n              <mat-option label=\"Select Role\">Select Role</mat-option>\r\n              <mat-option *ngFor=\"let role of userDetails; let i=index\" (click)=\"RoleSelectchanged(i)\" [value]=\"role.msRoleID\"> {{role.userRole}} </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <button type=\"button\" class=\"btn btn-info text-center btn-sm\" (click)=\"RedirecToHome()\">GO</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/logintoken/logintoken.component.ts":
/*!****************************************************!*\
  !*** ./src/app/logintoken/logintoken.component.ts ***!
  \****************************************************/
/*! exports provided: LogintokenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogintokenComponent", function() { return LogintokenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _model_loginModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/loginModel */ "./src/app/model/loginModel.ts");
/* harmony import */ var _login_loginservice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login/loginservice */ "./src/app/login/loginservice.ts");
/* harmony import */ var _payroll_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../payroll/common */ "./src/app/payroll/common.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _login_getLoginUserDetails__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/getLoginUserDetails */ "./src/app/login/getLoginUserDetails.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LogintokenComponent = /** @class */ (function () {
    function LogintokenComponent(location, router, _Service, _common, _snackBar, route, _LoginUserDetails) {
        this.location = location;
        this.router = router;
        this._Service = _Service;
        this._common = _common;
        this._snackBar = _snackBar;
        this.route = route;
        this._LoginUserDetails = _LoginUserDetails;
        this.currentURL = '';
        this.userDetails = [];
        this.userroleActivate = [];
        this.token = [];
        this.tokenSub = [];
        history.pushState(null, null, window.location.href);
        this.location.onPopState(function () {
            history.pushState(null, null, window.location.href);
        });
        this.currentURL = window.location.href;
    }
    LogintokenComponent.prototype.ngOnInit = function () {
        //alert(this.currentURL);
        //var ArrUrl = this.currentURL.split('/');//Abs.split('/');'http://10.199.72.53:8181/login'
        // alert(ArrUrl+''+ArrUrl[1] + '' + ArrUrl[2]);
        //this.url = ArrUrl[2] + '/';
        this.baseURL(this.currentURL);
        this.loginModel = new _model_loginModel__WEBPACK_IMPORTED_MODULE_2__["loginModel"]();
        this.CheckLoginRole = false;
    };
    LogintokenComponent.prototype.UserDetails = function () {
        var _this = this;
        this._Service.UserDetails(this.loginModel.Username).subscribe(function (data) {
            _this.userDetails = data;
            _this._LoginUserDetails.SendLoginUserDetails(_this.loginModel.Username, _this.userDetails[0].paoid, _this.userDetails[0].ddoid, _this.userDetails[0].controllerID, _this.userDetails[0].msUserID, _this.userDetails[0].empPermDDOId);
            sessionStorage.setItem('username', _this.loginModel.Username);
            sessionStorage.setItem('paoid', _this.userDetails[0].paoid);
            sessionStorage.setItem('ddoid', _this.userDetails[0].ddoid);
            sessionStorage.setItem('controllerID', _this.userDetails[0].controllerID);
            sessionStorage.setItem('UserID', _this.userDetails[0].msUserID);
            sessionStorage.setItem('EmpPermDDOId', _this.userDetails[0].empPermDDOId);
            if (_this.userDetails.length == "1") {
                sessionStorage.setItem('UserID', _this.userDetails[0].msUserID);
                sessionStorage.setItem('userRole', _this.userDetails[0].userRole);
                sessionStorage.setItem('userRoleID', _this.userDetails[0].msRoleID);
                sessionStorage.setItem('EmpPermDDOId', _this.userDetails[0].empPermDDOId);
                //   this.router.navigate(['dashboard/home']);
            }
            if (_this.userDetails.length > 1) {
                _this.UserName = sessionStorage.getItem('username');
                _this.CheckLoginRole = true;
            }
        });
    };
    LogintokenComponent.prototype.redirectto = function () {
        var _this = this;
        if (this.loginModel.Username == null) {
            this.username = this.loginModel.Username;
            this._common.username = this.username;
            alert("Please enter username/password");
            return;
        }
        if (this.loginModel.Username != null || this.loginModel.Username != '') {
            this.username = this.loginModel.Username;
            this._common.username = this.username;
        }
        if (this.loginModel.Password == null) {
            alert("Please enter username/password");
            return;
        }
        this._Service.Login(this.loginModel).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["first"])()).subscribe(function (data) {
            _this.loginDetails = data;
            sessionStorage.setItem('token', JSON.stringify(_this.loginDetails.jwT_Secret));
            if (_this.loginDetails.status == 'yes') {
                if (_this.loginDetails.userType == 'O') {
                    _this.UserDetails();
                }
                if (_this.loginDetails.userType == 'N') {
                    sessionStorage.setItem('NewUser', _this.loginModel.Username);
                    _this.router.navigate(['/changepassword']);
                }
                if (_this.loginDetails.userType == 'NF') {
                    alert('Please activate your account.');
                }
            }
            else {
                alert('Please enter valid email/password.');
            }
        });
    };
    LogintokenComponent.prototype.RoleSelectchanged = function (RowIndex) {
        sessionStorage.setItem('userRole', this.userDetails[RowIndex].userRole);
        sessionStorage.setItem('userRoleID', this.userDetails[RowIndex].msRoleID);
    };
    LogintokenComponent.prototype.RedirecToHome = function () {
        if (this.msRoleIDs == undefined) {
            alert("Please Select Role");
            return;
        }
        this.IsMenuPermissionAssigned(this.loginModel.Username, this.msRoleIDs);
    };
    LogintokenComponent.prototype.OnBoarding = function () {
        this.router.navigate(['NewOnboarding']);
    };
    LogintokenComponent.prototype.NewOnBoardingStatus = function () {
        this.router.navigate(['NewOnBoardingStatus']);
    };
    LogintokenComponent.prototype.baseURL = function (url) {
        var _this = this;
        this._Service.currentrUrl(url).subscribe(function (data) {
            _this.urlResult = data;
        });
    };
    LogintokenComponent.prototype.IsMenuPermissionAssigned = function (UserName, msRoleIDs) {
        var _this = this;
        this._Service.IsMenuPermissionAssigned(UserName, msRoleIDs).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["first"])()).subscribe(function (data) {
            _this.isMenuPermissionAssigned = Boolean(JSON.parse(data));
            if (_this.isMenuPermissionAssigned) {
                _this.router.navigate(['dashboard/home']);
            }
            else {
                _this.router.navigate(['dashboard/underdevelopment']);
            }
        });
    };
    LogintokenComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logintoken',
            template: __webpack_require__(/*! ./logintoken.component.html */ "./src/app/logintoken/logintoken.component.html"),
            styles: [__webpack_require__(/*! ./logintoken.component.css */ "./src/app/logintoken/logintoken.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_7__["LocationStrategy"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _login_loginservice__WEBPACK_IMPORTED_MODULE_3__["LoginServices"], _payroll_common__WEBPACK_IMPORTED_MODULE_4__["common"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _login_getLoginUserDetails__WEBPACK_IMPORTED_MODULE_6__["getLoginUserDetails"]])
    ], LogintokenComponent);
    return LogintokenComponent;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material-moment-adapter */ "./node_modules/@angular/material-moment-adapter/esm5/material-moment-adapter.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_3__["MatMomentDateModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_3__["MatMomentDateModule"]
            ],
            providers: [
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_2__["ErrorStateMatcher"], useClass: _angular_material__WEBPACK_IMPORTED_MODULE_2__["ShowOnDirtyErrorStateMatcher"] },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DATE_LOCALE"], useValue: 'en-GB' },
                { provide: _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_3__["MAT_MOMENT_DATE_ADAPTER_OPTIONS"], useValue: { useUtc: true } },
            ],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/model/UserManagementModel/OnBoardingModel.ts":
/*!**************************************************************!*\
  !*** ./src/app/model/UserManagementModel/OnBoardingModel.ts ***!
  \**************************************************************/
/*! exports provided: OnBoardingModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnBoardingModel", function() { return OnBoardingModel; });
var OnBoardingModel = /** @class */ (function () {
    function OnBoardingModel() {
    }
    return OnBoardingModel;
}());



/***/ }),

/***/ "./src/app/model/loginModel.ts":
/*!*************************************!*\
  !*** ./src/app/model/loginModel.ts ***!
  \*************************************/
/*! exports provided: loginModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginModel", function() { return loginModel; });
var loginModel = /** @class */ (function () {
    function loginModel() {
    }
    return loginModel;
}());



/***/ }),

/***/ "./src/app/new-on-boarding-status/new-on-boarding-status.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/new-on-boarding-status/new-on-boarding-status.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ldy1vbi1ib2FyZGluZy1zdGF0dXMvbmV3LW9uLWJvYXJkaW5nLXN0YXR1cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/new-on-boarding-status/new-on-boarding-status.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/new-on-boarding-status/new-on-boarding-status.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\" (ngSubmit)=\"OnBoarding.valid && OnBoardingSubmit(ObjOnBoarding);\" #OnBoarding=\"ngForm\" novalidate>\r\n      <div class=\"col-md-8 col-lg-8\">\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\"><p class=\"text-center\">New Office On-Boarding</p></div>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Request letter no\" required [(ngModel)]=\"ObjOnBoarding.RequestLetterNo\" name=\"RequestLetterNo\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput [matDatepicker]=\"picker1\" placeholder=\"Request letter date\" [(ngModel)]=\"ObjOnBoarding.RequestLetterDate\" name=\"RequestLetterDate\"\r\n                       required (click)=\"picker1.open()\" readonly [max]=\"maxDate\">\r\n                <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n                <mat-datepicker #picker1></mat-datepicker>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Date of birth\" required [(ngModel)]=\"ObjOnBoarding.RequestLetterNo\" name=\"RequestLetterNo\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <label>Status:</label>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n              <button type=\"submit\" class=\"btn btn-success\" (click)=\"SubmitForStatus()\" >Submit</button>\r\n              <button type=\"button\" class=\"btn btn-warning\" (click)=\"Cancel()\">Cancel</button>\r\n\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/new-on-boarding-status/new-on-boarding-status.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/new-on-boarding-status/new-on-boarding-status.component.ts ***!
  \****************************************************************************/
/*! exports provided: NewOnBoardingStatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewOnBoardingStatusComponent", function() { return NewOnBoardingStatusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_UserManagementModel_OnBoardingModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/UserManagementModel/OnBoardingModel */ "./src/app/model/UserManagementModel/OnBoardingModel.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NewOnBoardingStatusComponent = /** @class */ (function () {
    function NewOnBoardingStatusComponent(router) {
        this.router = router;
    }
    NewOnBoardingStatusComponent.prototype.ngOnInit = function () {
        this.ObjOnBoarding = new _model_UserManagementModel_OnBoardingModel__WEBPACK_IMPORTED_MODULE_1__["OnBoardingModel"]();
    };
    NewOnBoardingStatusComponent.prototype.Cancel = function () {
        this.router.navigate(['login']);
    };
    NewOnBoardingStatusComponent.prototype.SubmitForStatus = function () {
    };
    NewOnBoardingStatusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-on-boarding-status',
            template: __webpack_require__(/*! ./new-on-boarding-status.component.html */ "./src/app/new-on-boarding-status/new-on-boarding-status.component.html"),
            styles: [__webpack_require__(/*! ./new-on-boarding-status.component.css */ "./src/app/new-on-boarding-status/new-on-boarding-status.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NewOnBoardingStatusComponent);
    return NewOnBoardingStatusComponent;
}());



/***/ }),

/***/ "./src/app/new-onboarding/new-onboarding.component.css":
/*!*************************************************************!*\
  !*** ./src/app/new-onboarding/new-onboarding.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ldy1vbmJvYXJkaW5nL25ldy1vbmJvYXJkaW5nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/new-onboarding/new-onboarding.component.html":
/*!**************************************************************!*\
  !*** ./src/app/new-onboarding/new-onboarding.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<div class=\"main-content\">\r\n  <div class=\"container-fluid\">\r\n    <form class=\"rowdasa\" (ngSubmit)=\"OnBoarding.valid && OnBoardingSubmit(ObjOnBoarding);\" #OnBoarding=\"ngForm\" novalidate>\r\n      <div class=\"col-md-8 col-lg-8\">\r\n        <div class=\"example-card mat-card margin-bottom-9\">\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\"><p class=\"text-center\">New Office On-Boarding</p></div>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Request letter no\" required [(ngModel)]=\"ObjOnBoarding.RequestLetterNo\" name=\"RequestLetterNo\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput [matDatepicker]=\"picker1\" placeholder=\"Request letter date\" [(ngModel)]=\"ObjOnBoarding.RequestLetterDate\" name=\"RequestLetterDate\"\r\n                       required (click)=\"picker1.open()\" readonly [max]=\"maxDate\">\r\n                <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n                <mat-datepicker #picker1></mat-datepicker>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <!---------------------------Applicant details---------------------------------------->          \r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\">Applicant Details</div>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"First Name\" required [(ngModel)]=\"ObjOnBoarding.FirstName\" name=\"FirstName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Last Name\" [(ngModel)]=\"ObjOnBoarding.LastName\" name=\"LastName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Designation\" [(ngModel)]=\"ObjOnBoarding.Designation\" name=\"Designation\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Email\" type=\"email\" pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" required [(ngModel)]=\"ObjOnBoarding.EmailID\" name=\"EmailID\">\r\n              </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Mobile No\" [(ngModel)]=\"ObjOnBoarding.MobileNo\" required pattern=\"^[6-9]\\d{9}$\" name=\"MobileNo\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          \r\n          \r\n          <!--------------------------------------------Controller--------------------------->\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\">Controller Details</div>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <mat-select placeholder=\"Select controller\" [(ngModel)]=\"ObjOnBoarding.ControllerID\" name=\"ControllerID\" (selectionChange)=\"ControllerChnage($event.value)\" required>\r\n                  <mat-option>\r\n                    <ngx-mat-select-search [formControl]=\"controllerFilterCtrl\" [placeholderLabel]=\"'Find controller...'\"></ngx-mat-select-search>\r\n                  </mat-option>\r\n                  <mat-option *ngFor=\"let emp of filteredcontroller |async \" [value]=\"emp.values\">\r\n                    {{emp.text}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"First Name\" required [(ngModel)]=\"ObjOnBoarding.CFirstName\" name=\"CFirstName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Last Name\" [(ngModel)]=\"ObjOnBoarding.CLastName\" name=\"CLastName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Designation\" [(ngModel)]=\"ObjOnBoarding.CDesignation\" name=\"CDesignation\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Email\" required pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" [(ngModel)]=\"ObjOnBoarding.CEmailID\" name=\"CEmailID\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Mobile No\" required [(ngModel)]=\"ObjOnBoarding.CMobileNo\" pattern=\"^[6-9]\\d{9}$\" name=\"CMobileNo\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <!-----------------------------------PAO ---------------------------------- -->\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\">PAO Details</div>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n\r\n            <div class=\"col-md-12 col-lg-4\">\r\n\r\n              <mat-form-field class=\"wid-100\">\r\n                <mat-select matNativeControl placeholder=\"Select PAO\" [(ngModel)]=\"ObjOnBoarding.PAOID\" name=\"PAOID\" (selectionChange)=\"PAOSelectchanged($event.value)\" required>\r\n                  <mat-option *ngFor=\"let pao of PAOList\" [value]=\"pao.values\">\r\n                    {{pao.text}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"First Name\" required [(ngModel)]=\"ObjOnBoarding.PFirstName\" name=\"PFirstName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Last Name\" [(ngModel)]=\"ObjOnBoarding.PLastName\" name=\"PLastName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Designation\" [(ngModel)]=\"ObjOnBoarding.PDesignation\" name=\"PDesignation\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Email\" required pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" [(ngModel)]=\"ObjOnBoarding.PEmailID\" name=\"PEmailID\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Mobile No\" required [(ngModel)]=\"ObjOnBoarding.PMobileNo\" pattern=\"^[6-9]\\d{9}$\" name=\"PMobileNo\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <!---------------------------DDO---------------------------------------->\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <div class=\"fom-title\">DDO Details</div>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <mat-select matNativeControl placeholder=\"Select DDO\" required [(ngModel)]=\"ObjOnBoarding.DDOID\" name=\"DDOID\" (selectionChange)=\"DDOSelectChange($event.value)\">\r\n                  <mat-option *ngFor=\"let ddo of DDOList\" [value]=\"ddo.values\">\r\n                    {{ddo.text}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"First Name\" required [(ngModel)]=\"ObjOnBoarding.DFirstName\" name=\"DFirstName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Last Name\" [(ngModel)]=\"ObjOnBoarding.DLastName\" name=\"DLastName\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Designation\" [(ngModel)]=\"ObjOnBoarding.DDesignation\" name=\"DDesignation\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Email\" required pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" [(ngModel)]=\"ObjOnBoarding.DEmailID\" name=\"DEmailID\">\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-4\">\r\n              <mat-form-field class=\"wid-100\">\r\n                <input matInput placeholder=\"Mobile No\" required [(ngModel)]=\"ObjOnBoarding.DMobileNo\" pattern=\"^[6-9]\\d{9}$\" name=\"DMobileNo\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"OnBoarding.invalid\">Submit</button>\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"Reset()\">Cancel</button>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n    <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"RedirectToLogin()\">Login</button>-->\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/new-onboarding/new-onboarding.component.ts":
/*!************************************************************!*\
  !*** ./src/app/new-onboarding/new-onboarding.component.ts ***!
  \************************************************************/
/*! exports provided: NewOnboardingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewOnboardingComponent", function() { return NewOnboardingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/UserManagement/controller.service */ "./src/app/services/UserManagement/controller.service.ts");
/* harmony import */ var _services_UserManagement_OnBoarding_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/UserManagement/OnBoarding.service */ "./src/app/services/UserManagement/OnBoarding.service.ts");
/* harmony import */ var _model_UserManagementModel_OnBoardingModel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../model/UserManagementModel/OnBoardingModel */ "./src/app/model/UserManagementModel/OnBoardingModel.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var NewOnboardingComponent = /** @class */ (function () {
    function NewOnboardingComponent(_Service, Service, snackBar, router) {
        this._Service = _Service;
        this.Service = Service;
        this.snackBar = snackBar;
        this.router = router;
        this.controllerCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.controllerFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredcontroller = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.PAOList = [];
        this.DDOList = [];
    }
    NewOnboardingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ObjOnBoarding = new _model_UserManagementModel_OnBoardingModel__WEBPACK_IMPORTED_MODULE_6__["OnBoardingModel"]();
        this.getallcontrollers(sessionStorage.getItem('controllerID'));
        this.controllerFilterCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy)).subscribe(function () { _this.filtercontroller(); });
    };
    //Search functionality
    NewOnboardingComponent.prototype.filtercontroller = function () {
        if (!this.controllerroleList) {
            return;
        }
        // get the search keyword
        var search = this.controllerFilterCtrl.value;
        if (!search) {
            this.filteredcontroller.next(this.controllerroleList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredcontroller.next(this.controllerroleList.filter(function (controllerroleList) { return controllerroleList.text.toLowerCase().indexOf(search) > -1; }));
    };
    //For controller fetch
    NewOnboardingComponent.prototype.getallcontrollers = function (ControllerID) {
        var _this = this;
        this.Query = 'getController';
        this._Service.getAllControllerServiceclient(ControllerID, this.Query).subscribe(function (data) {
            _this.controllerroleList = data;
            _this.controllerCtrl.setValue(_this.controllerroleList);
            _this.filteredcontroller.next(_this.controllerroleList);
        });
    };
    NewOnboardingComponent.prototype.ControllerChnage = function (MsControllerID) {
        this.ControllerID = MsControllerID;
        if (this.ControllerID == null) {
            alert("Please select Controller");
            return;
        }
        this.getallpaos(MsControllerID);
    };
    NewOnboardingComponent.prototype.getallpaos = function (ControllerID) {
        var _this = this;
        this.DDOList = null;
        this.Query = 'getPAO';
        this._Service.GetAllpaos(ControllerID, this.Query).subscribe(function (data) {
            _this.PAOList = data;
        });
    };
    NewOnboardingComponent.prototype.PAOSelectchanged = function (PAOID) {
        this.DDOList = null;
        this.PAOID = PAOID;
        this.GetAllDDO(PAOID);
    };
    NewOnboardingComponent.prototype.GetAllDDO = function (PAOID) {
        var _this = this;
        this.Query = 'getDDO';
        this._Service.GetAllDDO(PAOID, this.Query).subscribe(function (data) {
            _this.DDOList = data;
        });
    };
    NewOnboardingComponent.prototype.OnBoardingSubmit = function (ObjOnBoarding) {
        var _this = this;
        ObjOnBoarding.ControllerID = this.ControllerID;
        ObjOnBoarding.PAOID = this.PAOID;
        ObjOnBoarding.DDOID = this.DDOID;
        debugger;
        this.Service.OnBoardingSubmit(ObjOnBoarding).subscribe(function (data) {
            debugger;
            _this.result = data;
            if (_this.result == '1') {
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default()('Submitted to EPS Admin for approval');
                _this.router.navigate(['login']);
            }
            if (_this.result == '0') {
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default()('On-Boarding request already exist');
            }
            _this.resetOnBoardingForm();
        });
    };
    NewOnboardingComponent.prototype.DDOSelectChange = function (DDOID) {
        this.DDOID = DDOID;
    };
    NewOnboardingComponent.prototype.resetOnBoardingForm = function () {
        this.OnBoardingValues.resetForm();
    };
    NewOnboardingComponent.prototype.Reset = function () {
        this.resetOnBoardingForm();
        this.router.navigate(['login']);
    };
    NewOnboardingComponent.prototype.RedirectToLogin = function () {
        this.router.navigate(['login']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('OnBoarding'),
        __metadata("design:type", Object)
    ], NewOnboardingComponent.prototype, "OnBoardingValues", void 0);
    NewOnboardingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-onboarding',
            template: __webpack_require__(/*! ./new-onboarding.component.html */ "./src/app/new-onboarding/new-onboarding.component.html"),
            styles: [__webpack_require__(/*! ./new-onboarding.component.css */ "./src/app/new-onboarding/new-onboarding.component.css")]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_4__["controllerservice"], _services_UserManagement_OnBoarding_service__WEBPACK_IMPORTED_MODULE_5__["OnBoardingService"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"]])
    ], NewOnboardingComponent);
    return NewOnboardingComponent;
}());



/***/ }),

/***/ "./src/app/payroll/common.ts":
/*!***********************************!*\
  !*** ./src/app/payroll/common.ts ***!
  \***********************************/
/*! exports provided: common */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "common", function() { return common; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var common = /** @class */ (function () {
    function common() {
    }
    common = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], common);
    return common;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/OnBoarding.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/UserManagement/OnBoarding.service.ts ***!
  \***************************************************************/
/*! exports provided: OnBoardingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnBoardingService", function() { return OnBoardingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var OnBoardingService = /** @class */ (function () {
    function OnBoardingService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    OnBoardingService.prototype.OnBoardingSubmit = function (ObjOnBoarding) {
        return this.httpclient.post(this.config.OnBoardingSubmit, ObjOnBoarding, { responseType: 'text' });
    };
    OnBoardingService.prototype.GetAllReuestNoOfOnboarding = function () {
        return this.httpclient.get(this.config.GetAllReuestNoOfOnboarding, {});
    };
    OnBoardingService.prototype.FetchReuestLetterNoRecord = function (OnboardingId) {
        return this.httpclient.get(this.config.FetchRequestLetterNoRecord + OnboardingId, {});
    };
    OnBoardingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], OnBoardingService);
    return OnBoardingService;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/controller.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/UserManagement/controller.service.ts ***!
  \***************************************************************/
/*! exports provided: controllerservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "controllerservice", function() { return controllerservice; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var controllerservice = /** @class */ (function () {
    function controllerservice(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    controllerservice.prototype.getAllControllerServiceclient = function (controllerID, Query) {
        return this.httpclient.get(this.config.getAllControllers + controllerID + '&Query=' + Query, {});
    };
    controllerservice.prototype.GetAllpaos = function (controllerID, Query) {
        return this.httpclient.get(this.config.GetAllpaos + controllerID + '&Query=' + Query, {});
    };
    controllerservice.prototype.GetAllDDO = function (PAOID, Query) {
        return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
    };
    controllerservice.prototype.oncontrollerroleSelectchanged = function (MsControllerID, PAOID, DDOID) {
        return this.httpclient.get(this.config.controllerchanged + MsControllerID + '&PAOID=' + PAOID + '&DDOID=' + DDOID, {});
    };
    controllerservice.prototype.PaosAssigned = function (empCd, MsControllerID, Active) {
        return this.httpclient.get(this.config.PaosAssigned + empCd + '&MsControllerID=' + MsControllerID + '&Active=' + Active, { responseType: 'text' });
    };
    controllerservice.prototype.AssignedEmp = function (ControllerID) {
        return this.httpclient.get(this.config.AssignedEmp + ControllerID, {});
    };
    controllerservice = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], controllerservice);
    return controllerservice;
}());



/***/ }),

/***/ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/leaves-mgmt/leaves-mgmt.service.ts ***!
  \*************************************************************/
/*! exports provided: LeavesMgmtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesMgmtService", function() { return LeavesMgmtService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



//import { EmpDesigModel } from '../../model/EmpDesigModel';
//import { EmpDesigModel } from '../../model/EmpDesigModel';
//import { EmpDesigModel } from '../../model/EmpDesigModel';
var LeavesMgmtService = /** @class */ (function () {
    //BaseUrl: any = [];
    function LeavesMgmtService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        //this.BaseUrl = 'http://localhost:55424/api/';
    }
    LeavesMgmtService.prototype.GetAllDesignation = function (PermDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('PermDdoId', PermDdoId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetAllDesignation", { params: params });
    };
    LeavesMgmtService.prototype.GetAllEmp = function (msDesigMastID) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('MsDesignID', msDesigMastID);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetEmployeeByDesig", { params: params });
    };
    LeavesMgmtService.prototype.GetOrderbyEmp = function (empCd, userRoleId, pageCode) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('EmpCd', empCd).set('userRoleId', userRoleId).set('pageCode', pageCode);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetOrderByEmployee", { params: params });
    };
    LeavesMgmtService.prototype.GetLeavesType = function () {
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesType");
    };
    LeavesMgmtService.prototype.GetLeavesReason = function () {
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesReason");
    };
    LeavesMgmtService.prototype.GetLeavesSanction = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesSanction", { params: params });
    };
    LeavesMgmtService.prototype.GetLeavesSanctionHistory = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesSanctionHistory", { params: params });
    };
    LeavesMgmtService.prototype.SaveLeavesSanction = function (_objdamaster, _status) {
        var paramtr = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('Status', _status);
        return this.httpclient.post(this.config.LeavesControllerName + "/SaveLeavesSanction", _objdamaster, { responseType: 'text', params: paramtr });
    };
    LeavesMgmtService.prototype.GetLeavesCurtailDetail = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesCurtailDetail", { params: params });
    };
    LeavesMgmtService.prototype.GetLeavesCurtailHistory = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesCurtailHistory", { params: params });
    };
    LeavesMgmtService.prototype.GetVerifiedLeavesSanction = function (permDdoId, ordNo) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('ordId', ordNo);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetVerifiedLeavesSanction", { params: params });
    };
    //GetVerifiedLeavesSanction(permDdoId: string, ordNo: string, userRoleId: string): Observable<any> {
    //  const params = new HttpParams().set('permDdoId', permDdoId).set('ordId', ordNo).set('userRoleId', userRoleId);
    //  return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetVerifiedLeavesSanction`, { params });
    //}
    LeavesMgmtService.prototype.SaveLeavescurtExten = function (_objdamaster, _status) {
        debugger;
        var paramtr = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('Status', _status);
        return this.httpclient.post(this.config.LeavesControllerName + "/SaveLeavesCurtExten", _objdamaster, { responseType: 'text', params: paramtr });
    };
    LeavesMgmtService.prototype.GetLeavesCancel = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesCancel", { params: params });
    };
    LeavesMgmtService.prototype.GetLeavesCancelHistory = function (permDdoId, empCode, userRoleId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesCancelHistory", { params: params });
    };
    LeavesMgmtService.prototype.GetVerifiedLeaveForCancel = function (permDdoId, ordNo) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('permDdoId', permDdoId).set('ordId', ordNo);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetVerifiedLeaveForCancel", { params: params });
    };
    LeavesMgmtService.prototype.SaveLeavesCancel = function (_objdamaster, _status) {
        debugger;
        var paramtr = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('Status', _status);
        return this.httpclient.post(this.config.LeavesControllerName + "/SaveLeavesCancel", _objdamaster, { responseType: 'text', params: paramtr });
    };
    LeavesMgmtService.prototype.DeleteLeavesSanction = function (id, pageCode) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('id', id).set('pageCode', pageCode);
        return this.httpclient.delete(this.config.LeavesControllerName + "/DeleteLeavesSanction", { params: params });
    };
    LeavesMgmtService.prototype.VerifyReturnSanction = function (Id, status, pagecode, reason) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('Id', Id).set('Status', status).set('PageCd', pagecode).set('Reason', reason);
        return this.httpclient.post(this.config.LeavesControllerName + "/VerifyReturnSanction", null, { responseType: 'text', params: params });
    };
    LeavesMgmtService.prototype.SaveTotalLeaveAvailed = function (empCode, TotalLeaveAvailed) {
        debugger;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('EmpCd', empCode).set('TotalLeaveAvailed', TotalLeaveAvailed);
        return this.httpclient.post(this.config.LeavesControllerName + "/SaveTotalLeaveAvailed", null, { responseType: 'text', params: params });
    };
    LeavesMgmtService.prototype.GetLeavesTypeBalance = function (empCode) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]().set('EmpCd', empCode);
        return this.httpclient.get(this.config.LeavesControllerName + "/GetLeavesTypeBalance", { params: params });
    };
    LeavesMgmtService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], LeavesMgmtService);
    return LeavesMgmtService;
}());



/***/ }),

/***/ "./src/app/services/loan-mgmt/comman.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/loan-mgmt/comman.service.ts ***!
  \******************************************************/
/*! exports provided: CommanService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommanService", function() { return CommanService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var CommanService = /** @class */ (function () {
    function CommanService(http, config) {
        this.http = http;
        this.config = config;
    }
    CommanService.prototype.GetAllPayBillGroup = function (PermDdoId, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('permddoId', PermDdoId).set('Flag', Flag);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetPayBillGroupByPermDDOId", { params: params });
    };
    CommanService.prototype.GetAllDesignation = function (PayBillGrpId, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('billGroupId', PayBillGrpId).set('Flag', Flag);
        ;
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignation", { params: params });
    };
    CommanService.prototype.GetAllDesignationByBillgrID = function (PayBillGrpId, PermDdoId, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('billGroupId', PayBillGrpId).set('PermDdoId', PermDdoId).set('Flag', Flag);
        ;
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignationByBillgrID", { params: params });
    };
    CommanService.prototype.GetAllEmp = function (DesigCode, BillgrID, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('desigId', DesigCode).set('BillgrID', BillgrID).set('Flag', Flag);
        ;
        return this.http.get("" + this.config.api_base_url + this.config.GetEmpfilterbydesignBillgroup + "/GetEmpfilterbydesignBillgroup", { params: params });
    };
    CommanService.prototype.GetAllEmpByBillgrIDDesigID = function (DesigCode, BillgrID, PermDdoId, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('desigId', DesigCode).set('BillgrID', BillgrID).set('PermDdoId', PermDdoId).set('Flag', Flag);
        ;
        return this.http.get("" + this.config.api_base_url + this.config.GetEmpfilterbydesignBillgroup + "/GetEmpfilterbydesignBillID", { params: params });
    };
    CommanService.prototype.GetBillgroupfilterbydesignID = function (DesigCode, Flag) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('desigId', DesigCode).set('DesigCode', DesigCode).set('Flag', Flag);
        ;
        return this.http.get("" + this.config.api_base_url + this.config.GetBillgroupfilterbydesignID + "/GetBillgroupfilterbydesignID", { params: params });
    };
    CommanService.prototype.BindDesignation = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownDesign);
    };
    CommanService.prototype.BindBillGroup = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownBillGroup);
    };
    CommanService.prototype.BindDropDownEmp = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getBindDropDownEmp);
    };
    CommanService.prototype.BindLoanType = function (loantypeFlag) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetLoanType + loantypeFlag));
    };
    CommanService.prototype.GetEmpCode = function (username) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetEmpDetails + username));
    };
    CommanService.prototype.Loantypeandpurposestatus = function (LoanBillID) {
        return this.http.get("" + this.config.api_base_url + (this.config.GetPayLoanPurposeStatus + LoanBillID));
    };
    CommanService.prototype.DeleteLoanDetailsByEMPID = function (id, mode) {
        return this.http
            .get("" + this.config.api_base_url + (this.config.DeleteLoanDetailsByEMPID + id + '&mode=' + mode));
    };
    CommanService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], CommanService);
    return CommanService;
}());



/***/ }),

/***/ "./src/app/shared-module/comman-mst/comman-mst.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/shared-module/comman-mst/comman-mst.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  /*lebel dropdown */\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkLW1vZHVsZS9jb21tYW4tbXN0L2NvbW1hbi1tc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7Q0FDeEI7O0VBRUM7SUFDRSxZQUFZO0dBQ2I7O0VBRUg7RUFDRSxrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztDQUNaOztFQUVEO0VBQ0Usb0ZBQW9GO0VBQ3BGLHVCQUF1QjtDQUN4Qjs7RUFFRCxlQUFlOztFQUVmO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UscUJBQXFCO0NBQ3RCOztFQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztFQUVEO0VBQ0Usb0dBQW9HO0NBQ3JHOztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLDBCQUEwQjtDQUMzQjs7RUFFRDtFQUNFLDJCQUEyQjtDQUM1Qjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFHRCxnQkFBZ0I7O0VBQ2hCO0VBQ0U7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7O0VBRUQ7SUFDRSxZQUFZO0dBQ2I7Q0FDRjs7RUFFRDtFQUNFO0lBQ0UsV0FBVztHQUNaOztFQUVEO0lBQ0UsV0FBVztHQUNaO0NBQ0Y7O0VBQ0QsYUFBYTs7RUFDYjtFQUNFLG9DQUFvQztDQUNyQzs7RUFFRDtFQUNFLFlBQVk7Q0FDYjs7RUFHRDtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsaUJBQWlCO0NBQ2xCOztFQUVEO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7RUFFRDtFQUNFLGFBQWE7Q0FDZDs7RUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7Q0FDaEI7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7O0VBQ0QsbUJBQW1COztFQUNuQjtFQUNFLFlBQVk7Q0FDYiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC1tb2R1bGUvY29tbWFuLW1zdC9jb21tYW4tbXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIG1hcmdpbjogNHB4XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vYXNzZXRzL2ltZy9leGFtcGxlcy9zaGliYTEuanBnJyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLyogMjktamFuLTE5ICovXHJcblxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXJnaW4tcmJsIHtcclxuICBtYXJnaW46IDAgNTBweCAxMHB4IDBcclxufVxyXG5cclxuLmZpbGQtb25lIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OCB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMXB4IC0xcHggcmdiYSgwLDAsMCwuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwwLDAsLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLDAsMCwuMTIpO1xyXG59XHJcblxyXG4ubWF0LWNhcmQge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U0ZTJlMjtcclxufVxyXG5cclxuLmV4YW1wbGUtY2FyZCB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDE4MXB4O1xyXG59XHJcblxyXG5cclxuLyogUmVzcG9uc2l2ZSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjhweCkgYW5kIChtYXgtd2lkdGggOiAxMDI0cHgpIHtcclxuICAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX29uZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMTAyNXB4KSBhbmQgKG1heC13aWR0aCA6IDEzOTdweCkge1xyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX3R3byB7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gIH1cclxufVxyXG4vKjMxL2phbi8xOSovXHJcbi5tLTIwIHtcclxuICBtYXJnaW46IDAgMjBweCAyMHB4IDIwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIG1pbi13aWR0aDogMzAwcHg7XHJcbn1cclxuXHJcbi5tYXQtdGFibGUge1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIG1heC1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxsLm1hdC1zb3J0LWhlYWRlci1zb3J0ZWQge1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm10LTEwIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi8qbGViZWwgZHJvcGRvd24gKi9cclxuLnNlbGVjdC1sYmwge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/shared-module/comman-mst/comman-mst.component.html":
/*!********************************************************************!*\
  !*** ./src/app/shared-module/comman-mst/comman-mst.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n\r\n    <form #commanForm=\"ngForm\" novalidate>\r\n      <div class=\"col-md-3\">\r\n        <label class=\"select-lbl\">Pay Bill Group:</label>\r\n        <div class=\"col-md-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select [formControl]=\"billCtrl\" placeholder=\"Find Pay Bill Group\" #singleSelect (selectionChange)=\"BindDropDownDesignation($event.value)\" [(value)]=\"selectedDesign\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"billFilterCtrl\" [placeholderLabel]=\"'Find Pay Bill Group...'\" (keypress)=\"allowAlphaNumericSpace($event)\" [noEntriesFoundLabel]=\"billGroupMsg\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredBill | async\" [value]=\"emp.billgrId\">\r\n                {{emp.billgrDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <label class=\"select-lbl\">Designation:</label>\r\n        <div class=\"col-md-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select [formControl]=\"designCtrl\" placeholder=\"Find Designation\" #singleSelect (selectionChange)=\"BindDropDownEmployee($event.value)\" [(value)]=\"selectedEmp\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\" (keypress)=\"allowAlphaNumericSpace($event)\" [noEntriesFoundLabel]=\"desgMsg\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.desigId\">\r\n                {{emp.desigDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <label class=\"select-lbl\">Employee:</label>\r\n        <div class=\"col-md-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select [formControl]=\"empCtrl\" placeholder=\"Find Employee\" #singleSelect (selectionChange)=\"SelectedValue($event.value)\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"empMsg\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.empCd\">\r\n                {{emp.empName}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img style=\"height:70px;width:70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/shared-module/comman-mst/comman-mst.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared-module/comman-mst/comman-mst.component.ts ***!
  \******************************************************************/
/*! exports provided: CommanMstComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommanMstComponent", function() { return CommanMstComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/loan-mgmt/comman.service */ "./src/app/services/loan-mgmt/comman.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommanMstComponent = /** @class */ (function () {
    function CommanMstComponent(_Service, _msg, cd) {
        this._Service = _Service;
        this._msg = _msg;
        this.cd = cd;
        this.onchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.question = 0;
        this.selectedIndex = 0;
        this.flag = null;
        /** searching . */
        this.billCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.empCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.billFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredBill = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    CommanMstComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.billGroupMsg = this._msg.billGroupNotFoundMsg;
        this.desgMsg = this._msg.desgNotFoundMsg;
        this.empMsg = this._msg.empNotFoundMsg;
        this.username = sessionStorage.getItem('username');
        this.FindEmpCode(this.username);
        this.billFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterBill();
        });
        this.eventsSubscription = this.events.subscribe(function (data) {
            _this.isLoading = true;
            _this.resetCommonControl(data);
            //console.log(data);
        });
    };
    //ngOnDestroy() {
    //  this.eventsSubscription.unsubscribe();
    //}
    CommanMstComponent.prototype.resetCommonControl = function (data) {
        this.commanForm.resetForm();
        this.cd.detectChanges();
        this.isLoading = false;
        //this.billCtrl.setValue(0);
        // this.designCtrl.setValue(0);
        //this.empCtrl.setValue(0);
        this.selectedDesign = '';
        this.selectedEmp = '';
        this.loadCommonMethod();
    };
    CommanMstComponent.prototype.loadCommonMethod = function () {
        var _this = this;
        this.billGroupMsg = this._msg.billGroupNotFoundMsg;
        this.desgMsg = this._msg.desgNotFoundMsg;
        this.empMsg = this._msg.empNotFoundMsg;
        this.username = sessionStorage.getItem('username');
        this.FindEmpCode(this.username);
        this.billFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterBill();
        });
    };
    CommanMstComponent.prototype.FindEmpCode = function (username) {
        var _this = this;
        //debugger;
        this._Service.GetEmpCode(username).subscribe(function (data) {
            _this.ArrEmpCode = data[0];
            if (_this.ArrEmpCode.empcode != null) {
                _this.permDdoId = _this.ArrEmpCode.ddoid;
                sessionStorage.setItem('PermDdoId', _this.ArrEmpCode.ddoid);
                _this.BindDropDownBillGroup(_this.permDdoId);
                _this.BackForwordSearchDesignation("0");
                _this.BackForwordSearchEmployee("0");
            }
        });
    };
    CommanMstComponent.prototype.BindDropDownBillGroup = function (value) {
        var _this = this;
        //debugger;
        this.isLoading = true;
        this._Service.GetAllPayBillGroup(value, this.flag).subscribe(function (result) {
            _this.ddlBillGroup = result;
            _this.isLoading = false;
            _this.Bill = result;
            _this.billCtrl.setValue(_this.Bill);
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredBill.next(_this.Bill);
        });
    };
    CommanMstComponent.prototype.allowAlphaNumericSpace = function (event) {
        //console.log(event.target.value);
        var charCode = (event.which) ? event.which : event.keyCode;
        if (event.target.value === "") {
            if ((charCode !== 32) && // space        
                (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
                (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
                return true;
            }
            else {
                if ((charCode !== 32) && // space        
                    (charCode >= 48 && charCode <= 57)) { // number (0-9)
                    alert(this._msg.numberNotAllowed);
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        if ((charCode !== 32) && (charCode >= 48 && charCode <= 57)) { // number (0-9)
            alert(this._msg.numberNotAllowed);
            return true;
        }
    };
    CommanMstComponent.prototype.BindDropDownDesignation = function (value) {
        var _this = this;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, this.flag).subscribe(function (data) {
            _this.ddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredDesign.next(_this.Design);
            _this.tempdesignvar = _this.designCtrl.value;
            if (_this.designCtrl.value.length > 1) {
                _this.tempdesignvar = '';
                _this.BindDropDownEmployee(_this.tempdesignvar);
            }
            _this.onchange.emit('');
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    CommanMstComponent.prototype.BindDropDownEmployee = function (value) {
        var _this = this;
        this.designId = value;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        this.tempvar = this.billCtrl.value;
        if (this.billCtrl.value.length > 2) {
            this.tempvar = '';
        }
        this._Service.GetAllEmpByBillgrIDDesigID(this.designId, this.tempvar, this.permDdoId, this.flag).subscribe(function (data) {
            _this.ddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
            _this.onchange.emit('');
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    CommanMstComponent.prototype.BackForwordSearchDesignation = function (value) {
        var _this = this;
        this.isLoading = true;
        this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, this.flag).subscribe(function (data) {
            _this.ddlDesign = data;
            _this.isLoading = false;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    CommanMstComponent.prototype.BackForwordSearchEmployee = function (value) {
        var _this = this;
        //debugger;
        this.isLoading = true;
        this.permDdoId = sessionStorage.getItem('PermDdoId');
        if (this.billCtrl.value == null) {
            this.billFlag = this.billCtrl.value;
        }
        else if (this.billCtrl.value.length > 0) {
            this.billFlag = null;
        }
        else {
            this.billFlag = this.billCtrl.value;
        }
        this._Service.GetAllEmpByBillgrIDDesigID(value, this.billFlag, this.permDdoId, this.flag).subscribe(function (data) {
            _this.ddlEmployee = data;
            _this.isLoading = false;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    CommanMstComponent.prototype.selectionChange = function ($event) {
        console.log('stepper.selectedIndex: ' + this.selectedIndex
            + '; $event.selectedIndex: ' + $event.selectedIndex);
        if ($event.selectedIndex === 0) {
            return;
        }
        this.selectedIndex = $event.selectedIndex;
    };
    CommanMstComponent.prototype.filterBill = function () {
        if (!this.Bill) {
            return;
        }
        var search = this.billFilterCtrl.value;
        if (!search) {
            this.filteredBill.next(this.Bill.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredBill.next(this.Bill.filter(function (Bill) { return Bill.billgrDesc.toLowerCase().indexOf(search) > -1; }));
    };
    CommanMstComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    CommanMstComponent.prototype.filterEmp = function () {
        if (!this.EMP) {
            return;
        }
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmp.next(this.EMP.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredEmp.next(this.EMP.filter(function (EMP) { return EMP.empName.toLowerCase().indexOf(search) > -1; }));
    };
    CommanMstComponent.prototype.SelectedValue = function (value) {
        debugger;
        this.isLoading = true;
        if (value != "") {
            //alert('value' + value);
            var splitted = value.split("-", 3);
            this.empid = splitted[0].trim();
            this.selectedDesign = splitted[2].trim();
            //alert('bill' + this.selectedDesign);
            //this.designId = splitted[2].trim();
            this.selectedEmp = Number(splitted[1]);
            //alert('desig' + this.selectedEmp);
            this.cd.detectChanges();
            this.onchange.emit(this.empid);
            this.isLoading = false;
        }
        //else {
        //  //to reset common component in dropdownlist 
        //  this.billCtrl.setValue('');
        //  this.designCtrl.setValue('');
        //  this.empCtrl.setValue('');
        //  //this.loadCommonMethod();
        //  this.BackForwordSearchEmployee("0");
        //  this.BackForwordSearchDesignation("0");
        //  this.BindDropDownBillGroup(this.permDdoId);
        //}
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('commanForm'),
        __metadata("design:type", Object)
    ], CommanMstComponent.prototype, "commanForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], CommanMstComponent.prototype, "events", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CommanMstComponent.prototype, "onchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CommanMstComponent.prototype, "data", void 0);
    CommanMstComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-comman-mst',
            template: __webpack_require__(/*! ./comman-mst.component.html */ "./src/app/shared-module/comman-mst/comman-mst.component.html"),
            styles: [__webpack_require__(/*! ./comman-mst.component.css */ "./src/app/shared-module/comman-mst/comman-mst.component.css")]
        }),
        __metadata("design:paramtypes", [_services_loan_mgmt_comman_service__WEBPACK_IMPORTED_MODULE_5__["CommanService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], CommanMstComponent);
    return CommanMstComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC1tb2R1bGUvZGVzaWctZW1wLW9yZG5vL2Rlc2lnLWVtcC1vcmRuby5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"designCtrl\" [placeholder]=\"placeholdervalue_desig\" #singleSelect (selectionChange)=\"BindDropDownEmployee($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [noEntriesFoundLabel]=\"'No Records Found'\" [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Search Designation...'\" (keypress)=\"charaterOnly($event)\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n              {{emp.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Employee:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"empCtrl\" [placeholder]=\"placeholdervalue_emp\" #singleSelect (selectionChange)=\"BindDropDownOrder($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [noEntriesFoundLabel]=\"'No Records Found'\" [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Search Employee...'\" (keypress)=\"charaterOnly($event)\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.empCd\">\r\n              {{emp.empName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Order No:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"orderNoCtrl\" [placeholder]=\"placeholdervalue_ord\" #singleSelect (selectionChange)=\"GetOrderdetails($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [noEntriesFoundLabel]=\"'No Records Found'\" [formControl]=\"orderNoFilterCtrl\" [placeholderLabel]=\"'Search Order Numer...'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredOrder | async\" [value]=\"emp.orderId\">\r\n              {{emp.orderNo}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.ts ***!
  \****************************************************************************/
/*! exports provided: DesigEmpOrdnoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DesigEmpOrdnoComponent", function() { return DesigEmpOrdnoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DesigEmpOrdnoComponent = /** @class */ (function () {
    function DesigEmpOrdnoComponent(_Service, snackBar) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.placeholdervalue_desig = "Select Designation";
        this.placeholdervalue_emp = "Select Employee";
        this.placeholdervalue_ord = "Select Order Numer(Order Date)";
        this.desigchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.empchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.orderchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectedIndex = 0;
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.empCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.orderNoCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.orderNoFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredOrder = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.question = 0;
    }
    DesigEmpOrdnoComponent.prototype.ngOnInit = function () {
        var _this = this;
        debugger;
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.BindDropDownDesignation(this.PermDdoId);
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
        this.BackForwordSearchEmployee("0");
        this.BackForwordSearchOrder("0");
    };
    DesigEmpOrdnoComponent.prototype.ngOnChanges = function () {
    };
    DesigEmpOrdnoComponent.prototype.BindDropDownDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    DesigEmpOrdnoComponent.prototype.BindDropDownEmployee = function (value) {
        var _this = this;
        this.desigchange.emit();
        this._Service.GetAllEmp(value).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
        this.ArrddlOrder = null;
        this.Order = null;
        this.orderNoCtrl.setValue(this.Order);
        this.filteredOrder.next(this.Order);
        this.placeholdervalue_desig = "";
    };
    DesigEmpOrdnoComponent.prototype.BindDropDownOrder = function (value) {
        var _this = this;
        debugger;
        this.empchange.emit(value);
        this._Service.GetOrderbyEmp(value, sessionStorage.getItem('userRoleID'), this.PageCode).subscribe(function (data) {
            _this.ArrddlOrder = data;
            _this.Order = data;
            _this.orderNoCtrl.setValue(_this.Order);
            _this.filteredOrder.next(_this.Order);
        });
        this.orderNoFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterOrder();
        });
        this.placeholdervalue_emp = "";
    };
    DesigEmpOrdnoComponent.prototype.BackForwordSearchDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            // set initial selection
            _this.designCtrl.setValue(_this.Design);
            // load the initial Design list
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    DesigEmpOrdnoComponent.prototype.BackForwordSearchEmployee = function (value) {
        var _this = this;
        this._Service.GetAllEmp(value).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            // set initial selection
            _this.empCtrl.setValue(_this.EMP);
            // load the initial EMP list
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    DesigEmpOrdnoComponent.prototype.BackForwordSearchOrder = function (value) {
        var _this = this;
        debugger;
        this._Service.GetOrderbyEmp(value, sessionStorage.getItem('userRoleID'), this.PageCode).subscribe(function (data) {
            _this.ArrddlOrder = data;
            _this.Order = data;
            // set initial selection
            _this.orderNoCtrl.setValue(_this.Order);
            // load the initial EMP list
            _this.filteredOrder.next(_this.Order);
        });
        this.orderNoFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterOrder();
        });
    };
    DesigEmpOrdnoComponent.prototype.selectionChange = function ($event) {
        console.log('stepper.selectedIndex: ' + this.selectedIndex
            + '; $event.selectedIndex: ' + $event.selectedIndex);
        if ($event.selectedIndex === 0) {
            return;
        } // First step is still selected
        this.selectedIndex = $event.selectedIndex;
    };
    DesigEmpOrdnoComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    DesigEmpOrdnoComponent.prototype.filterEmp = function () {
        if (!this.EMP) {
            return;
        }
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmp.next(this.EMP.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredEmp.next(this.EMP.filter(function (EMP) { return EMP.empName.toLowerCase().indexOf(search) > -1; }));
    };
    DesigEmpOrdnoComponent.prototype.filterOrder = function () {
        if (!this.Order) {
            return;
        }
        var search = this.orderNoFilterCtrl.value;
        if (!search) {
            this.filteredOrder.next(this.Order.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredOrder.next(this.Order.filter(function (Order) { return Order.ordernName.toLowerCase().indexOf(search) > -1; }));
    };
    DesigEmpOrdnoComponent.prototype.GetOrderdetails = function (value) {
        debugger;
        this.placeholdervalue_ord = "";
        this.orderchange.emit(value);
    };
    DesigEmpOrdnoComponent.prototype.charaterOnly = function (event) {
        debugger;
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8) {
            return true;
        }
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "desigchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "empchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "orderchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "ArrddlOrder", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], DesigEmpOrdnoComponent.prototype, "PageCode", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pdetails'),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('EmPDdetailsForm'),
        __metadata("design:type", Object)
    ], DesigEmpOrdnoComponent.prototype, "EmPDdetailsForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], DesigEmpOrdnoComponent.prototype, "singleSelect", void 0);
    DesigEmpOrdnoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-desig-emp-ordno',
            template: __webpack_require__(/*! ./desig-emp-ordno.component.html */ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.html"),
            styles: [__webpack_require__(/*! ./desig-emp-ordno.component.css */ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.css")]
        }),
        __metadata("design:paramtypes", [_services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_5__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], DesigEmpOrdnoComponent);
    return DesigEmpOrdnoComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/desig-emp/desig-emp.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/shared-module/desig-emp/desig-emp.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC1tb2R1bGUvZGVzaWctZW1wL2Rlc2lnLWVtcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared-module/desig-emp/desig-emp.component.html":
/*!******************************************************************!*\
  !*** ./src/app/shared-module/desig-emp/desig-emp.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"basic-container select-drop-head\">\r\n  <div class=\"row  selection-hed\">\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Designation:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"designCtrl\" [placeholder]=\"placeholdervalue_desig\" #singleSelect (selectionChange)=\"BindDropDownEmployee($event.value)\" >\r\n            <mat-option>\r\n              <ngx-mat-select-search [noEntriesFoundLabel]=\"'No Records Found'\" [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Search Designation...'\" (keypress)=\"charaterOnly($event)\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n              {{emp.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <label class=\"select-lbl\">Employee:</label>\r\n      <div class=\"col-md-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"empCtrl\" [placeholder]=\"placeholdervalue_emp\" #singleSelect (selectionChange)=\"GetEmployeedetails($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [noEntriesFoundLabel]=\"'No Records Found'\" [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Search Employee...'\" (keypress)=\"charaterOnly($event)\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <!--<mat-option [value]=\"0\">\r\n              Select\r\n            </mat-option>-->\r\n            <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.empCd\">\r\n              {{emp.empName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared-module/desig-emp/desig-emp.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared-module/desig-emp/desig-emp.component.ts ***!
  \****************************************************************/
/*! exports provided: DesigEmpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DesigEmpComponent", function() { return DesigEmpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DesigEmpComponent = /** @class */ (function () {
    function DesigEmpComponent(_Service, snackBar) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.onchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.desigchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectedIndex = 0;
        this.placeholdervalue_desig = "Select Designation";
        this.placeholdervalue_emp = "Select Employee";
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.empCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.question = 0;
    }
    DesigEmpComponent.prototype.ngOnInit = function () {
        var _this = this;
        debugger;
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.BindDropDownDesignation(this.PermDdoId);
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
        this.BackForwordSearchEmployee("0");
    };
    DesigEmpComponent.prototype.ngOnChanges = function () {
    };
    DesigEmpComponent.prototype.BindDropDownDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    DesigEmpComponent.prototype.BindDropDownEmployee = function (value) {
        var _this = this;
        this.desigchange.emit();
        this._Service.GetAllEmp(value).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
        this.placeholdervalue_desig = "";
    };
    DesigEmpComponent.prototype.BackForwordSearchDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            // set initial selection
            _this.designCtrl.setValue(_this.Design);
            // load the initial Design list
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    DesigEmpComponent.prototype.BackForwordSearchEmployee = function (value) {
        var _this = this;
        this._Service.GetAllEmp(value).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            // set initial selection
            _this.empCtrl.setValue(_this.EMP);
            // load the initial EMP list
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    DesigEmpComponent.prototype.selectionChange = function ($event) {
        console.log('stepper.selectedIndex: ' + this.selectedIndex
            + '; $event.selectedIndex: ' + $event.selectedIndex);
        if ($event.selectedIndex === 0) {
            return;
        } // First step is still selected
        this.selectedIndex = $event.selectedIndex;
    };
    DesigEmpComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    DesigEmpComponent.prototype.filterEmp = function () {
        if (!this.EMP) {
            return;
        }
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmp.next(this.EMP.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredEmp.next(this.EMP.filter(function (EMP) { return EMP.empName.toLowerCase().indexOf(search) > -1; }));
    };
    DesigEmpComponent.prototype.GetEmployeedetails = function (value) {
        this.placeholdervalue_emp = "";
        this.onchange.emit(value);
    };
    DesigEmpComponent.prototype.charaterOnly = function (event) {
        debugger;
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8) {
            return true;
        }
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DesigEmpComponent.prototype, "onchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DesigEmpComponent.prototype, "desigchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('pdetails'),
        __metadata("design:type", Object)
    ], DesigEmpComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('EmPDdetailsForm'),
        __metadata("design:type", Object)
    ], DesigEmpComponent.prototype, "EmPDdetailsForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], DesigEmpComponent.prototype, "singleSelect", void 0);
    DesigEmpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-desig-emp',
            template: __webpack_require__(/*! ./desig-emp.component.html */ "./src/app/shared-module/desig-emp/desig-emp.component.html"),
            styles: [__webpack_require__(/*! ./desig-emp.component.css */ "./src/app/shared-module/desig-emp/desig-emp.component.css")]
        }),
        __metadata("design:paramtypes", [_services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_5__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], DesigEmpComponent);
    return DesigEmpComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/dialog/dialog.component.css":
/*!***********************************************************!*\
  !*** ./src/app/shared-module/dialog/dialog.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".overlay {\r\n  position: fixed;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 999;\r\n}\r\n\r\n.dialog {\r\n  z-index: 1000;\r\n  position: fixed;\r\n  right: 0;\r\n  left: 0;\r\n  top: 80px;\r\n  margin-right: auto;\r\n  margin-left: auto;\r\n  min-height: 200px;\r\n  width: 90%;\r\n  max-width: 40%;\r\n  background-color: #fff;\r\n  padding: 12px;\r\n  box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12);\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .dialog {\r\n    top: 180px;\r\n    border-radius: 5px;\r\n  }\r\n}\r\n\r\n.dialog__close-btn {\r\n  border: 0;\r\n  background:rgba(0, 0, 0, 0.16);\r\n  color: #fff;\r\n  position: absolute;\r\n  top: 8px;\r\n  right: 8px;\r\n  font-size: 1.3em;\r\n  border-radius: 5px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkLW1vZHVsZS9kaWFsb2cvZGlhbG9nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsT0FBTztFQUNQLFVBQVU7RUFDVixRQUFRO0VBQ1IsU0FBUztFQUNULHFDQUFxQztFQUNyQyxhQUFhO0NBQ2Q7O0FBRUQ7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxlQUFlO0VBQ2YsdUJBQXVCO0VBQ3ZCLGNBQWM7RUFDZCx1SEFBdUg7Q0FDeEg7O0FBRUQ7RUFDRTtJQUNFLFdBQVc7SUFDWCxtQkFBbUI7R0FDcEI7Q0FDRjs7QUFFRDtFQUNFLFVBQVU7RUFDViwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsV0FBVztFQUNYLGlCQUFpQjtFQUNqQixtQkFBbUI7Q0FDcEIiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQtbW9kdWxlL2RpYWxvZy9kaWFsb2cuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdmVybGF5IHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiAwO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICB6LWluZGV4OiA5OTk7XHJcbn1cclxuXHJcbi5kaWFsb2cge1xyXG4gIHotaW5kZXg6IDEwMDA7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiA4MHB4O1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtaW4taGVpZ2h0OiAyMDBweDtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1heC13aWR0aDogNDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogMTJweDtcclxuICBib3gtc2hhZG93OiAwIDdweCA4cHggLTRweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMTNweCAxOXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDVweCAyNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAuZGlhbG9nIHtcclxuICAgIHRvcDogMTgwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGJvcmRlcjogMDtcclxuICBiYWNrZ3JvdW5kOnJnYmEoMCwgMCwgMCwgMC4xNik7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogOHB4O1xyXG4gIHJpZ2h0OiA4cHg7XHJcbiAgZm9udC1zaXplOiAxLjNlbTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shared-module/dialog/dialog.component.html":
/*!************************************************************!*\
  !*** ./src/app/shared-module/dialog/dialog.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\r\n  <ng-content></ng-content>\r\n  <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared-module/dialog/dialog.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared-module/dialog/dialog.component.ts ***!
  \**********************************************************/
/*! exports provided: DialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogComponent", function() { return DialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DialogComponent = /** @class */ (function () {
    function DialogComponent() {
        this.closable = true;
        this.visibleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    DialogComponent.prototype.ngOnInit = function () { };
    DialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DialogComponent.prototype, "closable", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], DialogComponent.prototype, "visible", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], DialogComponent.prototype, "visibleChange", void 0);
    DialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dialog',
            template: __webpack_require__(/*! ./dialog.component.html */ "./src/app/shared-module/dialog/dialog.component.html"),
            styles: [__webpack_require__(/*! ./dialog.component.css */ "./src/app/shared-module/dialog/dialog.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('dialog', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('void => *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'scale3d(.3, .3, .3)' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])(100)
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => void', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])(100, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'scale3d(.0, .0, .0)' }))
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [])
    ], DialogComponent);
    return DialogComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/header/header.component.css":
/*!***********************************************************!*\
  !*** ./src/app/shared-module/header/header.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user {\r\n  float: right;\r\n  width: 20%;\r\n  padding: 16px 16px 0 0;\r\n}\r\n\r\n  .user .photo {\r\n    float: right;\r\n    margin-right: 15px;\r\n  }\r\n\r\n  .user .photo img {\r\n      border-radius: 50%;\r\n      height: 50px;\r\n      width: 50px;\r\n      box-shadow: 0px 0px 28px -1px rgba(0,0,0,0.75);\r\n    }\r\n\r\n  .user .info {\r\n    float: right;\r\n    padding-top: 13px;\r\n  }\r\n\r\n  .user .info a {\r\n      color: #fff;\r\n    }\r\n\r\n  button.user-profile.mat-icon-button {\r\n  color: #fff;\r\n  float: right;\r\n  width: auto;\r\n  margin-top: 6px;\r\n}\r\n\r\n  .cdk-overlay-connected-position-bounding-box {\r\n  top: 97px;\r\n  left: 1758.53px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkLW1vZHVsZS9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLHVCQUF1QjtDQUN4Qjs7RUFFQztJQUNFLGFBQWE7SUFDYixtQkFBbUI7R0FDcEI7O0VBRUM7TUFDRSxtQkFBbUI7TUFDbkIsYUFBYTtNQUNiLFlBQVk7TUFDWiwrQ0FBK0M7S0FDaEQ7O0VBRUg7SUFDRSxhQUFhO0lBQ2Isa0JBQWtCO0dBQ25COztFQUVDO01BQ0UsWUFBWTtLQUNiOztFQUVMO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0NBQ2pCOztFQUVEO0VBQ0UsVUFBVTtFQUNWLGdCQUFnQjtDQUNqQiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC1tb2R1bGUvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXIge1xyXG4gIGZsb2F0OiByaWdodDtcclxuICB3aWR0aDogMjAlO1xyXG4gIHBhZGRpbmc6IDE2cHggMTZweCAwIDA7XHJcbn1cclxuXHJcbiAgLnVzZXIgLnBob3RvIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxuICB9XHJcblxyXG4gICAgLnVzZXIgLnBob3RvIGltZyB7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICB3aWR0aDogNTBweDtcclxuICAgICAgYm94LXNoYWRvdzogMHB4IDBweCAyOHB4IC0xcHggcmdiYSgwLDAsMCwwLjc1KTtcclxuICAgIH1cclxuXHJcbiAgLnVzZXIgLmluZm8ge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgcGFkZGluZy10b3A6IDEzcHg7XHJcbiAgfVxyXG5cclxuICAgIC51c2VyIC5pbmZvIGEge1xyXG4gICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbmJ1dHRvbi51c2VyLXByb2ZpbGUubWF0LWljb24tYnV0dG9uIHtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogNnB4O1xyXG59XHJcblxyXG4uY2RrLW92ZXJsYXktY29ubmVjdGVkLXBvc2l0aW9uLWJvdW5kaW5nLWJveCB7XHJcbiAgdG9wOiA5N3B4O1xyXG4gIGxlZnQ6IDE3NTguNTNweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/shared-module/header/header.component.html":
/*!************************************************************!*\
  !*** ./src/app/shared-module/header/header.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid head-bg\">\r\n\r\n  <div class=\"row\">\r\n    <div class=\"logo\">\r\n      <img src=\"~/../assets/eps_logo.png\" />\r\n      <h1>Employee Payroll System</h1>\r\n      <span>Ministry of Finance, Government of India</span>\r\n    </div>\r\n    <div class=\"emblem \"><img src=\"~/../assets/emblem.png\"></div>\r\n    <div class=\"user\">\r\n      <button type=\"button\" class=\"btn btn-warning\" (click)=\"RedirectToLogin()\">Login</button>\r\n    </div>\r\n    \r\n    <div *ngIf=\"userRole\">\r\n      <div class=\"user\">\r\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\" class=\"user-profile\">\r\n          Logged in as {{userRole}}\r\n          <mat-icon>more_vert</mat-icon>\r\n        </button>\r\n        <mat-menu #menu=\"matMenu\">\r\n          <button mat-menu-item>\r\n            <mat-icon>assignment_ind</mat-icon>\r\n            <span>Profile</span>\r\n          </button>\r\n          <button mat-menu-item (click)=\"LogOut();\">\r\n            <mat-icon>play_for_work</mat-icon>\r\n            <span>Logout</span>\r\n          </button>\r\n        </mat-menu>\r\n        <div class=\"photo\">\r\n          <img src=\"~/../assets/emp.png\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared-module/header/header.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared-module/header/header.component.ts ***!
  \**********************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.username = sessionStorage.getItem('username');
        this.userRole = sessionStorage.getItem('userRole');
    };
    HeaderComponent.prototype.LogOut = function () {
        sessionStorage.clear();
        //alert('LogOut');
        this.router.navigate(['/login']);
    };
    HeaderComponent.prototype.RedirectToLogin = function () {
        this.router.navigate(['/login']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/shared-module/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/shared-module/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/shared-module/shared-module.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared-module/shared-module.module.ts ***!
  \*******************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dialog/dialog.component */ "./src/app/shared-module/dialog/dialog.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _desig_emp_desig_emp_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./desig-emp/desig-emp.component */ "./src/app/shared-module/desig-emp/desig-emp.component.ts");
/* harmony import */ var _desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./desig-emp-ordno/desig-emp-ordno.component */ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_header_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared-module/header/header.component */ "./src/app/shared-module/header/header.component.ts");
/* harmony import */ var _shared_module_comman_mst_comman_mst_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared-module/comman-mst/comman-mst.component */ "./src/app/shared-module/comman-mst/comman-mst.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_2__["DialogComponent"], _desig_emp_desig_emp_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpComponent"], _desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_5__["DesigEmpOrdnoComponent"], _shared_module_header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"], _shared_module_comman_mst_comman_mst_component__WEBPACK_IMPORTED_MODULE_9__["CommanMstComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_3__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__["NgxMatSelectSearchModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTooltipModule"]
            ],
            exports: [_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_2__["DialogComponent"], _desig_emp_desig_emp_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpComponent"], _desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_5__["DesigEmpOrdnoComponent"], _shared_module_header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"], _shared_module_comman_mst_comman_mst_component__WEBPACK_IMPORTED_MODULE_9__["CommanMstComponent"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! exports provided: getBaseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBaseUrl", function() { return getBaseUrl; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment.prod */ "./src/environments/environment.prod.ts");




function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
var providers = [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] }
];
if (_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])(providers).bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! M:\gitEPS\epsapp\EPS\EPS.WebApp\ClientApp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map