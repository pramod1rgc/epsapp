(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["promotion-promotion-module"],{

/***/ "./src/app/promotion/promotion-header/promotion-header.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/promotion/promotion-header/promotion-header.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".toggle-pro-rev {\r\n  text-align: center;\r\n  padding: 10px;\r\n  background-color: #a4bdbd;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvbW90aW9uL3Byb21vdGlvbi1oZWFkZXIvcHJvbW90aW9uLWhlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCwwQkFBMEI7RUFDMUIsb0JBQW9CO0NBQ3JCIiwiZmlsZSI6InNyYy9hcHAvcHJvbW90aW9uL3Byb21vdGlvbi1oZWFkZXIvcHJvbW90aW9uLWhlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvZ2dsZS1wcm8tcmV2IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTRiZGJkO1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/promotion/promotion-header/promotion-header.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/promotion/promotion-header/promotion-header.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-10 col-lg-10 col-md-offset-1\">\r\n  <div class=\"basic-container select-drop-head\">\r\n    <form [formGroup]=\"headersForm\">\r\n      <div class=\"row  selection-hed\">\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"pay_bill_group\" (selectionChange)=\"BindDropDownDesigGroup($event.value)\" placeholder=\"Pay Bill Group\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"billFilterCtrl\" [placeholderLabel]=\"'Find Bill group...'\" [noEntriesFoundLabel]=\"'Pay Bill Group is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let Bill of filteredBill | async\" value=\"{{Bill.payBillGroupId}}\">{{Bill.billgrDesc}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"designation\" (selectionChange)=\"onBillDesgSel($event.value)\" placeholder=\"Designation\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"desigFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let Desig of filteredDesig | async\" value=\"{{Desig.desigId}}\">{{Desig.desigDesc}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-4 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select formControlName=\"employee\" (selectionChange)=\"getEmpTransDetails()\" placeholder=\"Employee\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\" [noEntriesFoundLabel]=\"'Employee is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredEmployee | async\" value=\"{{emp.empCd}}\">{{emp.empName}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n\r\n<!--<mat-radio-group [(ngModel)]=\"selectedRadio\" name=\"RadioButton\" class=\"col-md-10 col-lg-10 col-md-offset-1 toggle-pro-rev\"> \r\n  <mat-radio-button value=\"1\" (change)=\"toggleComponent(1)\" >Promotion</mat-radio-button>\r\n  <mat-radio-button value=\"2\" (change)=\"toggleComponent(2)\" >Reversion</mat-radio-button>\r\n</mat-radio-group>-->\r\n\r\n"

/***/ }),

/***/ "./src/app/promotion/promotion-header/promotion-header.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/promotion/promotion-header/promotion-header.component.ts ***!
  \**************************************************************************/
/*! exports provided: PromotionHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionHeaderComponent", function() { return PromotionHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/promotions/promotions.service */ "./src/app/services/promotions/promotions.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _promotions_promotions_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../promotions/promotions.component */ "./src/app/promotion/promotions/promotions.component.ts");
/* harmony import */ var _reversions_reversions_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../reversions/reversions.component */ "./src/app/promotion/reversions/reversions.component.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PromotionHeaderComponent = /** @class */ (function () {
    function PromotionHeaderComponent(_Service, _PromoComp, _ReverComp, route) {
        this._Service = _Service;
        this._PromoComp = _PromoComp;
        this._ReverComp = _ReverComp;
        this.route = route;
        this.allDesignation = [];
        this.allEmployees = [];
        this.billFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.desigFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.filteredBill = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.filteredDesig = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.filteredEmployee = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.suspendedEmployees = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.ddoId = sessionStorage.getItem('ddoid');
    }
    PromotionHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.BindDropDownBillGroup();
        this.billFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterBill();
        });
        this.desigFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesig();
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
        this.getAllDesignation();
        this.getAllEmployees();
        //if (this.router.url === '/dashboard/promotion/reversions')
        //  this.selectedRadio = '2';
        //else
        //  this.selectedRadio = '1';
        this.headerDataForms();
    };
    PromotionHeaderComponent.prototype.filterBill = function () {
        if (!this.BillGroup) {
            return;
        }
        // get the search keyword
        var search = this.billFilterCtrl.value;
        if (!search) {
            this.filteredBill.next(this.BillGroup.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBill.next(this.BillGroup.filter(function (Bill) { return Bill.billgrDesc.toLowerCase().indexOf(search) > -1; }));
    };
    PromotionHeaderComponent.prototype.filterDesig = function () {
        if (!this.allDesignation) {
            return;
        }
        // get the search keyword
        var search = this.desigFilterCtrl.value;
        if (!search) {
            this.filteredDesig.next(this.allDesignation.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredDesig.next(this.allDesignation.filter(function (desig) { return desig.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    PromotionHeaderComponent.prototype.filterEmp = function () {
        if (!this.allEmployees) {
            return;
        }
        // get the search keyword
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmployee.next(this.allEmployees.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredEmployee.next(this.allEmployees.filter(function (emp) { return emp.empName.toLowerCase().indexOf(search) > -1 || emp.empCd.toLowerCase().indexOf(search) > -1; }));
    };
    PromotionHeaderComponent.prototype.getAllDesignation = function () {
        var _this = this;
        this._Service.GetAllDesignation(this.controllerId).subscribe(function (result) {
            _this.allDesignation = result;
            _this.allDesignation.sort(function (a, b) {
                if (a.desigDesc < b.desigDesc)
                    return -1;
                else if (a.desigDesc > b.desigDesc)
                    return 1;
                else
                    return 0;
            });
            _this.filteredDesig.next(_this.allDesignation);
        });
    };
    PromotionHeaderComponent.prototype.getAllEmployees = function () {
        var _this = this;
        this._Service.GetAllEmpWithDesignationDetails(this.suspendedEmployees, this.controllerId, this.ddoId).subscribe(function (result) {
            _this.allEmployees = result;
            _this.allEmployees.sort(function (a, b) {
                if (a.empName < b.empName)
                    return -1;
                else if (a.empName > b.empName)
                    return 1;
                else
                    return 0;
            });
            _this.filteredEmployee.next(_this.allEmployees);
        });
    };
    PromotionHeaderComponent.prototype.setSuspendedEmployees = function (isSuspended) {
        if (isSuspended) {
            this.headerDataForms();
            this.formGroupDirective.resetForm();
            this.getEmpTransDetails();
            this._ReverComp.createForm();
            this._ReverComp.reversionsForm.controls.reversionType.setValue('penalty');
        }
        this.suspendedEmployees = isSuspended;
        this.getAllEmployees();
    };
    PromotionHeaderComponent.prototype.headerDataForms = function () {
        this.headersForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            pay_bill_group: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            designation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            employee: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
    };
    PromotionHeaderComponent.prototype.resetForm = function () {
        this.headerDataForms();
        //this.headersForm.reset();
        this.formGroupDirective.resetForm();
    };
    PromotionHeaderComponent.prototype.BindDropDownBillGroup = function () {
        var _this = this;
        this._Service.BindDropDownBillGroup().subscribe(function (result) {
            _this.BillGroup = result;
            _this.BillGroup.sort(function (a, b) {
                if (a.billgrDesc < b.billgrDesc)
                    return -1;
                else if (a.billgrDesc > b.billgrDesc)
                    return 1;
                else
                    return 0;
            });
            _this.filteredBill.next(_this.BillGroup);
        });
    };
    PromotionHeaderComponent.prototype.BindDropDownDesigGroup = function (selectedBilgrp) {
        var _this = this;
        if (selectedBilgrp) {
            this._Service.BindDropDownDesigGroup(selectedBilgrp).subscribe(function (result) {
                _this.DesigGroup = result;
                _this.DesigGroup.sort(function (a, b) {
                    if (a.desigDesc < b.desigDesc)
                        return -1;
                    else if (a.desigDesc > b.desigDesc)
                        return 1;
                    else
                        return 0;
                });
                _this.filteredDesig.next(_this.DesigGroup);
            });
        }
    };
    PromotionHeaderComponent.prototype.onBillDesgSel = function (selectedDesig) {
        var _this = this;
        if (selectedDesig) {
            this._Service.BindDropDownEmpGroup(selectedDesig).subscribe(function (result) {
                _this.EmpGroup = result;
                _this.EmpGroup.sort(function (a, b) {
                    if (a.empName < b.empName)
                        return -1;
                    else if (a.empName > b.empName)
                        return 1;
                    else
                        return 0;
                });
                _this.filteredEmployee.next(_this.EmpGroup);
            });
        }
    };
    PromotionHeaderComponent.prototype.getEmpTransDetails = function () {
        for (var i = 0; i < this.allEmployees.length; i++) {
            if (this.allEmployees[i].empCd == this.headersForm.value.employee) {
                this.headersForm.controls.designation.setValue(this.allEmployees[i].designationCd);
                break;
            }
        }
        if (this.route.snapshot.routeConfig.path == 'promotions') {
            this._PromoComp.fetchTableData(this.headersForm.value.employee);
        }
        if (this.route.snapshot.routeConfig.path == 'reversionadhoc') {
            this._ReverComp.fetchTableData(this.headersForm.value.employee);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"])
    ], PromotionHeaderComponent.prototype, "formGroupDirective", void 0);
    PromotionHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-promotion-header',
            template: __webpack_require__(/*! ./promotion-header.component.html */ "./src/app/promotion/promotion-header/promotion-header.component.html"),
            styles: [__webpack_require__(/*! ./promotion-header.component.css */ "./src/app/promotion/promotion-header/promotion-header.component.css")]
        }),
        __metadata("design:paramtypes", [_services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_1__["PromotionsService"], _promotions_promotions_component__WEBPACK_IMPORTED_MODULE_4__["PromotionsComponent"], _reversions_reversions_component__WEBPACK_IMPORTED_MODULE_5__["ReversionsComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], PromotionHeaderComponent);
    return PromotionHeaderComponent;
}());



/***/ }),

/***/ "./src/app/promotion/promotion-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/promotion/promotion-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PromotionRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionRoutingModule", function() { return PromotionRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _promotion_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./promotion.module */ "./src/app/promotion/promotion.module.ts");
/* harmony import */ var _promotion_promotions_promotions_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../promotion/promotions/promotions.component */ "./src/app/promotion/promotions/promotions.component.ts");
/* harmony import */ var _promotion_reversions_reversions_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../promotion/reversions/reversions.component */ "./src/app/promotion/reversions/reversions.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _promotion_module__WEBPACK_IMPORTED_MODULE_2__["PromotionModule"], children: [
            {
                path: 'promotions', component: _promotion_promotions_promotions_component__WEBPACK_IMPORTED_MODULE_3__["PromotionsComponent"], data: {
                    breadcrumb: 'Promotion Details'
                }
            },
            {
                path: 'reversionadhoc', component: _promotion_reversions_reversions_component__WEBPACK_IMPORTED_MODULE_4__["ReversionsComponent"], data: {
                    breadcrumb: 'Reversion Details'
                }
            }
        ]
    }
];
var PromotionRoutingModule = /** @class */ (function () {
    function PromotionRoutingModule() {
    }
    PromotionRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PromotionRoutingModule);
    return PromotionRoutingModule;
}());



/***/ }),

/***/ "./src/app/promotion/promotion.module.ts":
/*!***********************************************!*\
  !*** ./src/app/promotion/promotion.module.ts ***!
  \***********************************************/
/*! exports provided: PromotionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionModule", function() { return PromotionModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _promotion_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./promotion-routing.module */ "./src/app/promotion/promotion-routing.module.ts");
/* harmony import */ var _promotions_promotions_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./promotions/promotions.component */ "./src/app/promotion/promotions/promotions.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _promotion_header_promotion_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./promotion-header/promotion-header.component */ "./src/app/promotion/promotion-header/promotion-header.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _reversions_reversions_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./reversions/reversions.component */ "./src/app/promotion/reversions/reversions.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var PromotionModule = /** @class */ (function () {
    function PromotionModule() {
    }
    PromotionModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_promotions_promotions_component__WEBPACK_IMPORTED_MODULE_3__["PromotionsComponent"], _promotion_header_promotion_header_component__WEBPACK_IMPORTED_MODULE_5__["PromotionHeaderComponent"], _reversions_reversions_component__WEBPACK_IMPORTED_MODULE_8__["ReversionsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _promotion_routing_module__WEBPACK_IMPORTED_MODULE_2__["PromotionRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_7__["NgxMatSelectSearchModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"]
            ],
            providers: [_reversions_reversions_component__WEBPACK_IMPORTED_MODULE_8__["ReversionsComponent"], _promotions_promotions_component__WEBPACK_IMPORTED_MODULE_3__["PromotionsComponent"], _global_common_msg__WEBPACK_IMPORTED_MODULE_10__["CommonMsg"]]
        })
    ], PromotionModule);
    return PromotionModule;
}());



/***/ }),

/***/ "./src/app/promotion/promotions/promotions.component.css":
/*!***************************************************************!*\
  !*** ./src/app/promotion/promotions/promotions.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".select-drop-head[_ngcontent-c10] {\r\n  margin: 0 30px;\r\n}\r\n.pay-fixation-margin {\r\n  margin-top: 22px;\r\n  margin-bottom: 22px;\r\n}\r\n.record-not-found {\r\n  text-align: center;\r\n  display: inline-block\r\n}\r\n.header-button-style {\r\n  display: none\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvbW90aW9uL3Byb21vdGlvbnMvcHJvbW90aW9ucy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtDQUNoQjtBQUNEO0VBQ0UsaUJBQWlCO0VBQ2pCLG9CQUFvQjtDQUNyQjtBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLHFCQUFxQjtDQUN0QjtBQUNEO0VBQ0UsYUFBYTtDQUNkIiwiZmlsZSI6InNyYy9hcHAvcHJvbW90aW9uL3Byb21vdGlvbnMvcHJvbW90aW9ucy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlbGVjdC1kcm9wLWhlYWRbX25nY29udGVudC1jMTBdIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG4ucGF5LWZpeGF0aW9uLW1hcmdpbiB7XHJcbiAgbWFyZ2luLXRvcDogMjJweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMnB4O1xyXG59XHJcbi5yZWNvcmQtbm90LWZvdW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbn1cclxuLmhlYWRlci1idXR0b24tc3R5bGUge1xyXG4gIGRpc3BsYXk6IG5vbmVcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/promotion/promotions/promotions.component.html":
/*!****************************************************************!*\
  !*** ./src/app/promotion/promotions/promotions.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n  <app-promotion-header #header></app-promotion-header>\r\n  <div class=\"col-md-6 col-lg-6\">\r\n    <mat-card>\r\n      <form [formGroup]=\"promotionsForm\" (ngSubmit)=\"onSubmit()\">\r\n        <div class=\"fom-title\">Promotion Details</div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"promotionType\" placeholder=\"Promotion Type\">\r\n                <mat-option value=\"adhoc\">Adhoc Promotion</mat-option>\r\n                <mat-option value=\"proforma\">Proforma Promotion</mat-option>\r\n                <mat-option value=\"regular\">Regular Promotion</mat-option>\r\n              </mat-select>\r\n              <mat-error>Please select a promotion type!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput formControlName=\"orderNo\" placeholder=\"Order No.\" autocomplete=off onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"50\">\r\n              <mat-error *ngIf=\"promotionsForm.controls.orderNo.errors && !promotionsForm.controls.orderNo.errors.minlength\">Order No. Required !</mat-error>\r\n              <mat-error *ngIf=\"promotionsForm.controls.orderNo.errors && promotionsForm.controls.orderNo.errors.minlength\">Minimum Length required is 3</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"tomorrow\" formControlName=\"orderDt\" placeholder=\"Order Date\" autocomplete=off readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #OrderDate></mat-datepicker>\r\n              <mat-error>Order Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input #ref matInput (click)=\"promotionWef.open()\" [matDatepicker]=\"promotionWef\" [min]=\"promotionsForm.controls.orderDt.value\" formControlName=\"promotionWefDt\" placeholder=\"Promotion Wef Date\" autocomplete=off (blur)=\"setJoiningDate()\" readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"promotionWef\"></mat-datepicker-toggle>\r\n              <mat-datepicker #promotionWef></mat-datepicker>\r\n              <mat-error>Promotion Wef Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <textarea matInput formControlName=\"remark\" rows=\"1\" placeholder=\"Remarks\" onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"70\"></textarea>\r\n              <mat-error>Remarks Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\" *ngIf=\"promotionsForm.controls.rejectionRemark.value\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" disabled></textarea>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div class=\"fom-title\">Joining Details</div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput formControlName=\"joinOrderNo\" placeholder=\"Order No.\" autocomplete=off onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"50\">\r\n              <mat-error>Order No. Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"joiningDate.open()\" [matDatepicker]=\"joiningDate\" [max]=\"promotionsForm.controls.joinedDt.value\" formControlName=\"joinOrderDt\" placeholder=\"Order Date\" autocomplete=off readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"joiningDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #joiningDate></mat-datepicker>\r\n              <mat-error>Order Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"dateOfjoining.open()\" [matDatepicker]=\"dateOfjoining\" [value]=\"promotionsForm ? promotionsForm.controls.promotionWefDt.value : ''\" formControlName=\"joinedDt\" placeholder=\"Date of joining\" autocomplete=off disabled readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"dateOfjoining\"></mat-datepicker-toggle>\r\n              <mat-datepicker #dateOfjoining></mat-datepicker>\r\n              <mat-error>Date of joining Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <!--<span>Timing: </span>-->\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"joinedAnBn\" placeholder=\"Timing\">\r\n                <mat-option value=\"Bn\">Fore Noon</mat-option>\r\n                <mat-option value=\"An\">After Noon</mat-option>\r\n              </mat-select>\r\n              <mat-error>Timing Required !</mat-error>\r\n            </mat-form-field>\r\n            <!--<mat-radio-group formControlName=\"timing\">\r\n              <mat-radio-button value=\"fore-noon\" class=\"ml-10\" checked>Fore Noon</mat-radio-button>&nbsp;\r\n              <mat-radio-button value=\"after-noon\" class=\"ml-10\">After Noon</mat-radio-button>\r\n            </mat-radio-group>-->\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12 pay-fixation-margin\" *ngIf=\"promotionsForm.controls.promotionType.value == 'adhoc'\">\r\n            <span>Pay Fixation effect On :</span>\r\n            <mat-radio-group>\r\n              <mat-radio-button value=\"promotion-date\" class=\"ml-10\" checked>Date of Promotion</mat-radio-button>&nbsp;\r\n              <!--<mat-radio-button *ngIf=\"promotionsForm.controls.promotionType.value == 'proforma'\" value=\"increment-date\" class=\"ml-10\">Increment Date</mat-radio-button>-->\r\n            </mat-radio-group>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"toDesigCd\" placeholder=\"Designation\" autocomplete=off>\r\n                <mat-option *ngFor=\"let desig of designations\" [value]=\"desig.desigId\">{{desig.desigDesc}}</mat-option>\r\n              </mat-select>\r\n              <mat-error>Designation Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div *ngIf=\"roleId == '6'\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"!editScreen\" [disabled]=\"!empCd || infoScreen\">Save</button>\r\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd || infoScreen\" *ngIf=\"editScreen\">Update</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"!empCd\">Cancel</button>\r\n          <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!empCd || !editScreen\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n        </div>\r\n        <div *ngIf=\"roleId == '5'\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" [disabled]=\"promotionsForm.controls.status.value == 'V' && infoScreen\" (click)=\"ApprovePopup = !ApprovePopup\">Accept</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" [disabled]=\"promotionsForm.controls.status.value == 'V' && infoScreen\" (click)=\"RejectPopup = !RejectPopup\">Reject</button>\r\n          <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n        </div>\r\n      </form>\r\n    </mat-card>\r\n  </div>\r\n  <div class=\"col-md-6 col-lg-6\">\r\n    <mat-card>\r\n      <div class=\"fom-title\">Promotions Details</div>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilter()\" [(ngModel)]=\"searchTerm\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"totalCount > 0\">\r\n\r\n        <ng-container matColumnDef=\"S.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1 }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"PromotionType\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> Promotion Type </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.promotionType | titlecase }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"OrderNo.\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Order No.</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Designation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Designation</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.designation}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label>{{element.statusDesc}}</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.verifyFlag == 'E' || element.verifyFlag == 'R' || element.verifyFlag == 'U') && roleId == '6'\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element, true)\">edit</a>\r\n            <a *ngIf=\"(element.verifyFlag == 'E' || element.verifyFlag == 'R') && roleId == '6'\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeletePromotionDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.verifyFlag != 'E' && element.verifyFlag != 'R' && element.verifyFlag != 'U') && roleId == '6'\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnEditClick(element, false)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == '5'\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnEditClick(element, false)\">\r\n              info\r\n            </a>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n      <mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10, 20, 50]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n\r\n      <mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"totalCount == 0\">Record is not found</mat-toolbar>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<button id=\"headerButton\" class=\"header-button-style\" (click)=\"header.resetForm()\"></button>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = !DeletePopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ApprovePopup = !ApprovePopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"RejectPopup = !RejectPopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ForwardToMakerPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Forward this to Maker?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('E')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup; rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/promotion/promotions/promotions.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/promotion/promotions/promotions.component.ts ***!
  \**************************************************************/
/*! exports provided: PromotionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsComponent", function() { return PromotionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/promotions/promotions.service */ "./src/app/services/promotions/promotions.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PromotionsComponent = /** @class */ (function () {
    function PromotionsComponent(_service, snackBar, _msg) {
        this._service = _service;
        this.snackBar = snackBar;
        this._msg = _msg;
        //dataSource: any[] = [];
        this.allData = [];
        this.designations = [];
        this.displayedColumns = ['S.No', 'PromotionType', 'OrderNo.', 'Designation', 'Status', 'Action'];
        this.tomorrow = new Date();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"]();
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.infoScreen = false;
        this.editScreen = false;
        this.rejectionRemark = '';
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.DeletePopup = false;
        this.ForwardToMakerPopup = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
        this.getAllDesignation();
    }
    PromotionsComponent.prototype.createForm = function () {
        this.promotionsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            transferDetailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            promotionType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            orderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]),
            orderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            promotionWefDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinOrderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinedDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinedAnBn: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            toDesigCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            remark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            trnGround: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](1),
            dueToCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("711"),
            permDdoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            toOfficeCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("00003000"),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        if (this.roleId == '5') {
            this.promotionsForm.disable();
        }
    };
    PromotionsComponent.prototype.ngOnInit = function () {
        this.createForm();
    };
    PromotionsComponent.prototype.getAllDesignation = function () {
        var _this = this;
        this._service.GetAllDesignation(this.controllerId).subscribe(function (result) {
            _this.designations = result;
            _this.designations.sort(function (a, b) {
                if (a.desigDesc < b.desigDesc)
                    return -1;
                else if (a.desigDesc > b.desigDesc)
                    return 1;
                else
                    return 0;
            });
        });
    };
    PromotionsComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.promotionsForm.valid) {
            this._service.UpdatePromotionDetails(this.promotionsForm.value).subscribe(function (result) {
                var response = result;
                if (response) {
                    if (response > 0) {
                        _this.promotionsForm.reset();
                        //this.getEmpTransDetails(this.pageSize, this.pageNumber);
                        _this.snackBar.open(_this._msg.saveMsg, null, { duration: 4000 });
                        _this.formGroupDirective.resetForm();
                        _this.createForm();
                        _this.transferDetailId = null;
                        _this.editScreen = false;
                        var btn = document.getElementById('headerButton');
                        btn.click();
                        _this.empCd = null;
                        _this.getEmpTransDetails(10, 1);
                    }
                    if (response == -1) {
                        _this.snackBar.open("Order number already exist!", null, { duration: 4000 });
                    }
                }
            });
            if (this.transferDetailId && this.promotionsForm.controls.status.value != 'E') {
                this.updateStatus("U");
            }
        }
    };
    PromotionsComponent.prototype.getPaginationData = function (event) {
        this.pageSize = event.pageSize;
        this.pageNumber = event.pageIndex + 1;
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
    };
    PromotionsComponent.prototype.setJoiningDate = function () {
        this.promotionsForm.controls.joinedDt.setValue(this.promotionsForm.controls.promotionWefDt.value);
    };
    PromotionsComponent.prototype.getEmpTransDetails = function (pageSize, pageNumber) {
        var _this = this;
        var employeeId = this.empCd;
        this.totalCount = 0;
        this.dataSource.data = []; // = new MatTableDataSource<any>();
        this._service.FetchEmployeeDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(function (result) {
            if (result && result != undefined && result.length > 0) {
                // this.dataSource = new MatTableDataSource(result);
                _this.dataSource.data = result;
                //this.dataSource.paginator = this.paginator;
                _this.dataSource.sort = _this.sort;
                _this.allData = result;
                _this.totalCount = result[0].totalCount;
            }
        });
    };
    PromotionsComponent.prototype.fetchTableData = function (empCd) {
        this.empCd = empCd;
        this.createForm();
        this.promotionsForm.controls.empCd.setValue(this.empCd);
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        if (this.roleId != '5') {
            if (this.empCd) {
                this.promotionsForm.enable();
            }
            else {
                this.promotionsForm.disable();
            }
        }
    };
    PromotionsComponent.prototype.applyFilter = function () {
        this.pageNumber = 1;
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        //this.dataSource.filter = value;
        //if (this.dataSource.paginator) {
        //  this.dataSource.paginator.firstPage();
        //}
    };
    PromotionsComponent.prototype.btnEditClick = function (obj, isEdit) {
        if (isEdit) {
            this.transferDetailId = obj.transDetailId;
            this.promotionsForm.setValue({
                transferDetailId: obj.transDetailId,
                promotionType: obj.promotionType,
                orderNo: obj.orderNo,
                orderDt: obj.orderDt,
                promotionWefDt: obj.promotionWefDt,
                joinOrderNo: obj.joinOrderNo,
                joinOrderDt: obj.joinOrderDt,
                joinedDt: obj.joinedDt,
                joinedAnBn: obj.joinedAnBn,
                toDesigCd: obj.toDesigCd,
                remark: obj.remark,
                rejectionRemark: obj.rejectionRemark,
                trnGround: 1,
                dueToCd: "711",
                permDdoId: "",
                toOfficeCd: "00003000",
                empCd: this.empCd,
                ddoId: this.ddoId,
                status: obj.verifyFlag
            });
            this.promotionsForm.enable();
            this.infoScreen = false;
            this.editScreen = true;
        }
        else {
            this.transferDetailId = obj.transDetailId;
            this.promotionsForm.setValue({
                transferDetailId: obj.transDetailId,
                promotionType: obj.promotionType,
                orderNo: obj.orderNo,
                orderDt: obj.orderDt,
                promotionWefDt: obj.promotionWefDt,
                joinOrderNo: obj.joinOrderNo,
                joinOrderDt: obj.joinOrderDt,
                joinedDt: obj.joinedDt,
                joinedAnBn: obj.joinedAnBn,
                toDesigCd: obj.toDesigCd,
                remark: obj.remark,
                rejectionRemark: obj.rejectionRemark,
                trnGround: 1,
                dueToCd: "711",
                permDdoId: "",
                toOfficeCd: "00003000",
                empCd: this.empCd,
                ddoId: this.ddoId,
                status: obj.verifyFlag
            });
            this.promotionsForm.disable();
            this.infoScreen = true;
            this.editScreen = false;
        }
    };
    PromotionsComponent.prototype.DeletePromotionDetails = function (obj) {
        this.objToBeDeleted = obj;
    };
    PromotionsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.DeletePromotionDetails(this.objToBeDeleted.transDetailId).subscribe(function (result) {
            _this.snackBar.open(_this._msg.deleteMsg, null, { duration: 4000 });
            _this.pageNumber = 1;
            _this.paginator.firstPage();
            _this.dataSource.paginator = _this.paginator;
            _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
            // $(".dialog__close-btn").click();
            _this.DeletePopup = false;
            _this.objToBeDeleted = null;
        });
    };
    PromotionsComponent.prototype.cancelForm = function () {
        this.promotionsForm.reset();
        this.promotionsForm.enable();
        this.createForm();
        this.infoScreen = false;
        this.editScreen = false;
        this.formGroupDirective.resetForm();
    };
    PromotionsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        if (this.transferDetailId) {
            var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": "P", "PermDdoId": "", "DDOId": this.ddoId };
            this._service.ForwardTransferDetailsToChecker(forwardObj).subscribe(function (result) {
                _this.snackBar.open(_this._msg.forwardCheckerMsg, null, { duration: 4000 });
                _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
                _this.transferDetailId = null;
                _this.promotionsForm.reset();
                _this.createForm();
            });
        }
    };
    PromotionsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        if (this.transferDetailId) {
            var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": status, "PermDdoId": "", "RejectionRemark": this.rejectionRemark, "DDOId": this.ddoId };
            this._service.UpdateStatus(forwardObj).subscribe(function (result) {
                _this.snackBar.open(_this._msg.updateMsg, null, { duration: 4000 });
                _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
                _this.transferDetailId = null;
                _this.promotionsForm.reset();
                _this.createForm();
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
                _this.rejectionRemark = '';
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], PromotionsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], PromotionsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], PromotionsComponent.prototype, "sort", void 0);
    PromotionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-promotions',
            template: __webpack_require__(/*! ./promotions.component.html */ "./src/app/promotion/promotions/promotions.component.html"),
            styles: [__webpack_require__(/*! ./promotions.component.css */ "./src/app/promotion/promotions/promotions.component.css")]
        }),
        __metadata("design:paramtypes", [_services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_2__["PromotionsService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], PromotionsComponent);
    return PromotionsComponent;
}());



/***/ }),

/***/ "./src/app/promotion/reversions/reversions.component.css":
/*!***************************************************************!*\
  !*** ./src/app/promotion/reversions/reversions.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".record-not-found {\r\n  text-align: center;\r\n  display: inline-block\r\n}\r\n\r\n.header-button-style {\r\n  display: none\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvbW90aW9uL3JldmVyc2lvbnMvcmV2ZXJzaW9ucy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLHFCQUFxQjtDQUN0Qjs7QUFFRDtFQUNFLGFBQWE7Q0FDZCIsImZpbGUiOiJzcmMvYXBwL3Byb21vdGlvbi9yZXZlcnNpb25zL3JldmVyc2lvbnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yZWNvcmQtbm90LWZvdW5kIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbn1cclxuXHJcbi5oZWFkZXItYnV0dG9uLXN0eWxlIHtcclxuICBkaXNwbGF5OiBub25lXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/promotion/reversions/reversions.component.html":
/*!****************************************************************!*\
  !*** ./src/app/promotion/reversions/reversions.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n  <app-promotion-header #header></app-promotion-header>\r\n  <div class=\"clearfix\"></div> \r\n  <div class=\"alert addDefinitionMessage alert-dismissable alert-danger\" *ngIf=\"reversionsForm && reversionsForm.controls.reversionType.value == 'penalty'\" role=\"alert\">\r\n    Note: Listed Employees are those whose entry has been made for suspension.\r\n    <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\"> <span aria-hidden=\"true\" (click)=\"btnclose();\">&times;</span></button>\r\n  </div>\r\n  <div class=\"col-md-6 col-lg-6\">\r\n    <mat-card>\r\n      <form [formGroup]=\"reversionsForm\" (ngSubmit)=\"onSubmit()\">\r\n        <div class=\"fom-title\">Reversion Details</div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select (selectionChange)=\"header.setSuspendedEmployees($event.value == 'penalty' ? true : false)\" formControlName=\"reversionType\" placeholder=\"Reversion Type\">\r\n                <mat-option value=\"adhoc\">Reversion after ad-hoc promotion</mat-option>\r\n                <mat-option value=\"penalty\">Reversion on penalty</mat-option>\r\n                <mat-option value=\"administrative\">Reversion due to administrative reason</mat-option>\r\n              </mat-select>\r\n              <mat-error>Please select a reversion type!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput formControlName=\"orderNo\" placeholder=\"Order No.\" autocomplete=off onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"50\">\r\n              <mat-error *ngIf=\"reversionsForm.controls.orderNo.errors && !reversionsForm.controls.orderNo.errors.minlength\">Order No. Required !</mat-error>\r\n              <mat-error *ngIf=\"reversionsForm.controls.orderNo.errors && reversionsForm.controls.orderNo.errors.minlength\">Minimum Length required is 3</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"OrderDate.open()\" [matDatepicker]=\"OrderDate\" [max]=\"tomorrow\" formControlName=\"orderDt\" placeholder=\"Order Date\" autocomplete=off readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #OrderDate></mat-datepicker>\r\n              <mat-error>Order Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input #ref matInput (click)=\"reversionWef.open()\" [matDatepicker]=\"reversionWef\" [min]=\"reversionsForm.controls.orderDt.value\" formControlName=\"reversionWefDt\" placeholder=\"Reversion Wef Date\" autocomplete=off (blur)=\"setJoiningDate()\" readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"reversionWef\"></mat-datepicker-toggle>\r\n              <mat-datepicker #reversionWef></mat-datepicker>\r\n              <mat-error>Reversion Wef Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <textarea matInput formControlName=\"remark\" rows=\"1\" placeholder=\"Remarks\" onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"70\"></textarea>\r\n              <mat-error>Remarks Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\" *ngIf=\"reversionsForm.controls.rejectionRemark.value\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" disabled></textarea>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div class=\"fom-title\">Joining Details</div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput formControlName=\"joinOrderNo\" placeholder=\"Order No.\" autocomplete=off onkeypress=\"return (event.target.selectionStart == 0 && event.charCode == 32 ? false : (event.charCode >= 44 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122) || event.charCode == 32)\" maxlength=\"50\">\r\n              <mat-error>Order No. Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"joiningDate.open()\" [matDatepicker]=\"joiningDate\" [max]=\"reversionsForm.controls.joinedDt.value\" formControlName=\"joinOrderDt\" placeholder=\"Order Date\" autocomplete=off readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"joiningDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #joiningDate></mat-datepicker>\r\n              <mat-error>Order Date Required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput (click)=\"dateOfjoining.open()\" [matDatepicker]=\"dateOfjoining\" [value]=\"reversionsForm ? reversionsForm.controls.reversionWefDt.value : ''\" formControlName=\"joinedDt\" placeholder=\"Date of joining\" autocomplete=off disabled readonly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"dateOfjoining\"></mat-datepicker-toggle>\r\n              <mat-datepicker #dateOfjoining></mat-datepicker>\r\n              <mat-error>Date of joining Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-6 col-lg-6\">\r\n            <!--<span>Timing: </span>-->\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"joinedAnBn\" placeholder=\"Timing\">\r\n                <mat-option value=\"Bn\">Fore Noon</mat-option>\r\n                <mat-option value=\"An\">After Noon</mat-option>\r\n              </mat-select>\r\n              <mat-error>Timing Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select formControlName=\"toDesigCd\" placeholder=\"Designation\" autocomplete=off>\r\n                <mat-option *ngFor=\"let desig of designations\" [value]=\"desig.desigId\">{{desig.desigDesc}}</mat-option>\r\n              </mat-select>\r\n              <mat-error>Designation Required !</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div *ngIf=\"roleId == '6'\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"!editScreen\" [disabled]=\"!empCd || infoScreen\">Save</button>\r\n          <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!empCd || infoScreen\" *ngIf=\"editScreen\">Update</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" (click)=\"cancelForm()\" [disabled]=\"!empCd\">Cancel</button>\r\n          <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"!empCd || !editScreen\" (click)=\"forwardToChecker()\">Forward To Checker</button>\r\n        </div>\r\n        <div *ngIf=\"roleId == '5'\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"button\" class=\"btn btn-success\" [disabled]=\"reversionsForm.controls.status.value == 'V' && infoScreen\" (click)=\"ApprovePopup = !ApprovePopup\">Accept</button>\r\n          <button type=\"button\" class=\"btn btn-warning\" [disabled]=\"reversionsForm.controls.status.value == 'V' && infoScreen\" (click)=\"RejectPopup = !RejectPopup\">Reject</button>\r\n          <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n        </div>\r\n      </form>\r\n    </mat-card>\r\n  </div>\r\n  <div class=\"col-md-6 col-lg-6\">\r\n    <mat-card>\r\n      <div class=\"fom-title\">Reversions Details</div>\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" [(ngModel)]=\"searchTerm\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n\r\n      <mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\" *ngIf=\"totalCount > 0\">\r\n\r\n        <ng-container matColumnDef=\"S.No\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> S.No </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element ; let i = index;\">{{ i+1 }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"ReversionType\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header> Reversion Type </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.reversionType | titlecase }} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"OrderNo.\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Order No.</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Designation\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Designation</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\"> {{element.designation}} </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Status\">\r\n          <mat-header-cell *matHeaderCellDef mat-sort-header>Status</mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <label>{{element.statusDesc}}</label>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Action\">\r\n          <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element\">\r\n            <a *ngIf=\"(element.verifyFlag == 'E' || element.verifyFlag == 'R' || element.verifyFlag == 'U') && roleId == '6'\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element, true)\">edit</a>\r\n            <a *ngIf=\"(element.verifyFlag == 'E' || element.verifyFlag == 'R') && roleId == '6'\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteReversionDetails(element);DeletePopup = !DeletePopup\">\r\n              delete_forever\r\n            </a>\r\n            <a *ngIf=\"(element.verifyFlag != 'E' && element.verifyFlag != 'R' && element.verifyFlag != 'U') && roleId == '6'\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnEditClick(element, false)\">\r\n              info\r\n            </a>\r\n            <a *ngIf=\"roleId == '5'\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnEditClick(element, false)\">\r\n              info\r\n            </a>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n      </mat-table>\r\n      <mat-paginator [ngStyle]=\"{display: totalCount > 0 ? 'block' : 'none'}\" [length]=\"totalCount\" [pageSizeOptions]=\"[5, 10, 20, 50]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n\r\n      <mat-toolbar color=\"warning\" class=\"record-not-found\" *ngIf=\"totalCount == 0\">Record is not found</mat-toolbar>\r\n    </mat-card>\r\n  </div>\r\n</div>\r\n\r\n<button id=\"headerButton\" class=\"header-button-style\" (click)=\"header.resetForm()\"></button>\r\n\r\n<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"DeletePopup = !DeletePopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ApprovePopup = !ApprovePopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('R')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"RejectPopup = !RejectPopup;rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"ForwardToMakerPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Forward this to Maker?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"updateStatus('E')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup; rejectionRemark = ''\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/promotion/reversions/reversions.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/promotion/reversions/reversions.component.ts ***!
  \**************************************************************/
/*! exports provided: ReversionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReversionsComponent", function() { return ReversionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/promotions/promotions.service */ "./src/app/services/promotions/promotions.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReversionsComponent = /** @class */ (function () {
    function ReversionsComponent(_service, snackBar, _msg) {
        this._service = _service;
        this.snackBar = snackBar;
        this._msg = _msg;
        this.empCd = null;
        this.designations = [];
        this.displayedColumns = ['S.No', 'ReversionType', 'OrderNo.', 'Designation', 'Status', 'Action'];
        this.tomorrow = new Date();
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"]();
        this.objToBeDeleted = null;
        this.pageSize = 5;
        this.pageNumber = 1;
        this.totalCount = 0;
        this.searchTerm = '';
        this.infoScreen = false;
        this.editScreen = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.ForwardToMakerPopup = false;
        this.controllerId = sessionStorage.getItem('controllerID');
        this.roleId = sessionStorage.getItem('userRoleID');
        this.ddoId = sessionStorage.getItem('ddoid');
    }
    ReversionsComponent.prototype.ngOnInit = function () {
        this.createForm();
        this.getAllDesignation();
    };
    ReversionsComponent.prototype.createForm = function () {
        this.reversionsForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            transferDetailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            reversionType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            orderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]),
            orderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            reversionWefDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinOrderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinOrderDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinedDt: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            joinedAnBn: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            toDesigCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            remark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            rejectionRemark: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            trnGround: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](2),
            dueToCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("712"),
            permDdoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            toOfficeCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("00003000"),
            empCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.empCd, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            ddoId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.ddoId),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        if (this.roleId == '5') {
            this.reversionsForm.disable();
        }
    };
    ReversionsComponent.prototype.fetchTableData = function (empCd) {
        if (empCd) {
            this.empCd = empCd;
            //this.createForm();
            this.reversionsForm.controls.empCd.setValue(this.empCd);
            this.getEmpTransDetails(this.pageSize, this.pageNumber);
            if (this.roleId != '5') {
                if (this.empCd) {
                    this.reversionsForm.enable();
                }
                else {
                    this.reversionsForm.disable();
                }
            }
        }
        else {
            this.empCd = empCd;
            //this.createForm();
            this.reversionsForm.controls.empCd.setValue(this.empCd);
            this.dataSource.data = [];
            this.totalCount = 0;
        }
    };
    ReversionsComponent.prototype.getEmpTransDetails = function (pageSize, pageNumber) {
        var _this = this;
        var employeeId = this.empCd;
        this.totalCount = 0;
        this.dataSource.data = []; // = new MatTableDataSource<any>();
        this._service.FetchReversionEmployeeDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(function (result) {
            if (result && result != undefined && result.length > 0) {
                _this.dataSource.data = result; // = new MatTableDataSource(result);
                // this.dataSource.paginator = this.paginator;
                _this.dataSource.sort = _this.sort;
                _this.totalCount = result[0].totalCount;
            }
        });
    };
    ReversionsComponent.prototype.cancelForm = function () {
        this.reversionsForm.reset();
        this.reversionsForm.enable();
        this.createForm();
        this.infoScreen = false;
        this.editScreen = false;
        this.formGroupDirective.resetForm();
    };
    ReversionsComponent.prototype.getAllDesignation = function () {
        var _this = this;
        this._service.GetAllDesignation(this.controllerId).subscribe(function (result) {
            _this.designations = result;
            _this.designations.sort(function (a, b) {
                if (a.desigDesc < b.desigDesc)
                    return -1;
                else if (a.desigDesc > b.desigDesc)
                    return 1;
                else
                    return 0;
            });
        });
    };
    ReversionsComponent.prototype.DeleteReversionDetails = function (obj) {
        this.objToBeDeleted = obj;
    };
    ReversionsComponent.prototype.confirmDelete = function () {
        var _this = this;
        this._service.DeleteReversionDetails(this.objToBeDeleted.transDetailId).subscribe(function (result) {
            _this.snackBar.open(_this._msg.deleteMsg, null, { duration: 4000 });
            _this.pageNumber = 1;
            _this.paginator.firstPage();
            _this.dataSource.paginator = _this.paginator;
            _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
            _this.DeletePopup = false;
            _this.objToBeDeleted = null;
        });
    };
    ReversionsComponent.prototype.btnEditClick = function (obj, isEdit) {
        if (isEdit) {
            this.transferDetailId = obj.transDetailId;
            this.reversionsForm.setValue({
                transferDetailId: obj.transDetailId,
                reversionType: obj.reversionType,
                orderNo: obj.orderNo,
                orderDt: obj.orderDt,
                reversionWefDt: obj.reversionWefDt,
                joinOrderNo: obj.joinOrderNo,
                joinOrderDt: obj.joinOrderDt,
                joinedDt: obj.joinedDt,
                joinedAnBn: obj.joinedAnBn,
                toDesigCd: obj.toDesigCd,
                remark: obj.remark,
                rejectionRemark: obj.rejectionRemark,
                trnGround: 2,
                dueToCd: "712",
                permDdoId: "",
                toOfficeCd: "00003000",
                empCd: this.empCd,
                ddoId: this.ddoId,
                status: obj.verifyFlag
            });
            this.reversionsForm.enable();
            this.infoScreen = false;
            this.editScreen = true;
        }
        else {
            this.transferDetailId = obj.transDetailId;
            this.reversionsForm.setValue({
                transferDetailId: obj.transDetailId,
                reversionType: obj.reversionType,
                orderNo: obj.orderNo,
                orderDt: obj.orderDt,
                reversionWefDt: obj.reversionWefDt,
                joinOrderNo: obj.joinOrderNo,
                joinOrderDt: obj.joinOrderDt,
                joinedDt: obj.joinedDt,
                joinedAnBn: obj.joinedAnBn,
                toDesigCd: obj.toDesigCd,
                remark: obj.remark,
                rejectionRemark: obj.rejectionRemark,
                trnGround: 2,
                dueToCd: "712",
                permDdoId: "",
                toOfficeCd: "00003000",
                empCd: this.empCd,
                ddoId: this.ddoId,
                status: obj.verifyFlag
            });
            this.reversionsForm.disable();
            this.infoScreen = true;
            this.editScreen = false;
        }
    };
    ReversionsComponent.prototype.applyFilter = function (value) {
        this.pageNumber = 1;
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        //this.dataSource.filter = value;
        //if (this.dataSource.paginator) {
        //  this.dataSource.paginator.firstPage();
        //}
    };
    ReversionsComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.reversionsForm.valid) {
            this._service.UpdateReversionDetails(this.reversionsForm.value).subscribe(function (result) {
                var response = result;
                if (response) {
                    if (response > 0) {
                        _this.reversionsForm.reset();
                        //this.getEmpTransDetails(this.pageSize, this.pageNumber);
                        _this.snackBar.open(_this._msg.saveMsg, null, { duration: 4000 });
                        _this.formGroupDirective.resetForm();
                        _this.createForm();
                        _this.transferDetailId = null;
                        _this.editScreen = false;
                        var btn = document.getElementById('headerButton');
                        btn.click();
                        _this.empCd = null;
                        _this.getEmpTransDetails(10, 1);
                    }
                    if (response == -1) {
                        _this.snackBar.open("Order number already exist!", null, { duration: 4000 });
                    }
                }
            });
            if (this.transferDetailId && this.reversionsForm.controls.status.value != 'E') {
                this.updateStatus("U");
            }
        }
    };
    ReversionsComponent.prototype.setJoiningDate = function () {
        this.reversionsForm.controls.joinedDt.setValue(this.reversionsForm.controls.reversionWefDt.value);
    };
    ReversionsComponent.prototype.getPaginationData = function (event) {
        this.pageSize = event.pageSize;
        this.pageNumber = event.pageIndex + 1;
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
    };
    ReversionsComponent.prototype.forwardToChecker = function () {
        var _this = this;
        if (this.transferDetailId) {
            var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": "P", "PermDdoId": "", "DDOId": this.ddoId };
            this._service.ForwardTransferDetailsToChecker(forwardObj).subscribe(function (result) {
                _this.snackBar.open(_this._msg.forwardCheckerMsg, null, { duration: 4000 });
                _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
                _this.transferDetailId = null;
                _this.reversionsForm.reset();
                _this.createForm();
            });
        }
    };
    ReversionsComponent.prototype.updateStatus = function (status) {
        var _this = this;
        if (this.transferDetailId) {
            var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": status, "PermDdoId": "", "RejectionRemark": this.rejectionRemark, "DDOId": this.ddoId };
            this._service.UpdateStatus(forwardObj).subscribe(function (result) {
                _this.snackBar.open(_this._msg.updateMsg, null, { duration: 4000 });
                _this.getEmpTransDetails(_this.pageSize, _this.pageNumber);
                _this.transferDetailId = null;
                _this.reversionsForm.reset();
                _this.createForm();
                _this.ApprovePopup = false;
                _this.RejectPopup = false;
                _this.rejectionRemark = '';
            });
        }
    };
    ReversionsComponent.prototype.refresh = function () {
        //this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
        //  this.router.navigate([item.route]));
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"]),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"])
    ], ReversionsComponent.prototype, "formGroupDirective", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ReversionsComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ReversionsComponent.prototype, "sort", void 0);
    ReversionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reversions',
            template: __webpack_require__(/*! ./reversions.component.html */ "./src/app/promotion/reversions/reversions.component.html"),
            styles: [__webpack_require__(/*! ./reversions.component.css */ "./src/app/promotion/reversions/reversions.component.css")]
        }),
        __metadata("design:paramtypes", [_services_promotions_promotions_service__WEBPACK_IMPORTED_MODULE_2__["PromotionsService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _global_common_msg__WEBPACK_IMPORTED_MODULE_4__["CommonMsg"]])
    ], ReversionsComponent);
    return ReversionsComponent;
}());



/***/ }),

/***/ "./src/app/services/promotions/promotions.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/promotions/promotions.service.ts ***!
  \***********************************************************/
/*! exports provided: PromotionsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsService", function() { return PromotionsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PromotionsService = /** @class */ (function () {
    function PromotionsService(http, config) {
        this.http = http;
        this.config = config;
    }
    PromotionsService.prototype.BindDropDownBillGroup = function () {
        // return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
        // return this.http.get('http://localhost:55424/api/PayBill/GetAccountHeads');
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetPayBillGroup?PermDdoId=00003");
    };
    PromotionsService.prototype.BindDropDownDesigGroup = function (selectedBilgrp) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetDesignation?billGroupId=" + selectedBilgrp);
    };
    PromotionsService.prototype.BindDropDownEmpGroup = function (selectedDesig) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmp?desigId=" + selectedDesig + "&pageCode=Leaves");
    };
    PromotionsService.prototype.GetAllDesignation = function (controllerId) {
        //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetAllDesignationOfDepartment?id=" + controllerId);
    };
    PromotionsService.prototype.GetAllEmpWithDesignationDetails = function (suspendedEmployee, controllerId, ddoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('suspendedEmployees', suspendedEmployee.toString()).set('controllerId', "013").set('DDOId', ddoId); // controllerId);
        return this.http.get("" + this.config.api_base_url + this.config.SharedControllerName + "/GetEmpWithDesignationDetails", { params: params });
    };
    PromotionsService.prototype.FetchEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetPromotionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdatePromotionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdatePromotionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeletePromotionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/" + employeeId);
    };
    PromotionsService.prototype.ForwardTransferDetailsToChecker = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/ForwardTransferDetailToChecker", obj, { responseType: 'text' });
    };
    PromotionsService.prototype.UpdateStatus = function (obj) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateStatus", obj, { responseType: 'text' });
    };
    // Reversion API's
    PromotionsService.prototype.FetchReversionEmployeeDetails = function (employeeId, pageNumber, pageSize, searchTerm, roleId) {
        return this.http.get("" + this.config.api_base_url + this.config.PromotionControllerName + "/GetReversionDetailsWithoutTransfer?empCode=" + employeeId + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&searchTerm=" + searchTerm + "&roleId=" + roleId);
    };
    PromotionsService.prototype.UpdateReversionDetails = function (objPro) {
        return this.http.post("" + this.config.api_base_url + this.config.PromotionControllerName + "/UpdateReversionDetailsWithoutTransfer", objPro, { responseType: 'text' });
    };
    PromotionsService.prototype.DeleteReversionDetails = function (employeeId) {
        return this.http.delete("" + this.config.api_base_url + this.config.PromotionControllerName + "/reversion/" + employeeId);
    };
    PromotionsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], PromotionsService);
    return PromotionsService;
}());



/***/ })

}]);
//# sourceMappingURL=promotion-promotion-module.js.map