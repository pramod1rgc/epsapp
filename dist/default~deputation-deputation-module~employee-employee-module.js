(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~deputation-deputation-module~employee-employee-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/Subject.js":
/*!***************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/Subject.js ***!
  \***************************************************/
/*! exports provided: Subject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subject", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["Subject"]; });


//# sourceMappingURL=Subject.js.map

/***/ }),

/***/ "./src/app/deputation/deputation-in/deputation-in.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/deputation/deputation-in/deputation-in.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*tr > td:nth-child(2) {\r\n  word-break: break-all;\r\n  max-width: 110px;\r\n  padding-right: 15px;\r\n}*/\r\n.bgorange {\r\n  color: orange;\r\n}\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n.bggreen {\r\n  color: green;\r\n}\r\n.bgblue {\r\n  color: blue;\r\n}\r\n.bgred {\r\n  color: red;\r\n}\r\ntr.mat-row > td {\r\n  padding-right: 15px;\r\n}\r\ntr.mat-row > td {\r\n  word-break: break-all;\r\n  max-width: 110px;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVwdXRhdGlvbi9kZXB1dGF0aW9uLWluL2RlcHV0YXRpb24taW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztHQUlHO0FBQ0g7RUFDRSxjQUFjO0NBQ2Y7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjtBQUVEO0VBQ0UsYUFBYTtDQUNkO0FBRUQ7RUFDRSxZQUFZO0NBQ2I7QUFFRDtFQUNFLFdBQVc7Q0FDWjtBQUNEO0VBQ0Usb0JBQW9CO0NBQ3JCO0FBQ0Q7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCO0NBQ2xCIiwiZmlsZSI6InNyYy9hcHAvZGVwdXRhdGlvbi9kZXB1dGF0aW9uLWluL2RlcHV0YXRpb24taW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qdHIgPiB0ZDpudGgtY2hpbGQoMikge1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXgtd2lkdGg6IDExMHB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbn0qL1xyXG4uYmdvcmFuZ2Uge1xyXG4gIGNvbG9yOiBvcmFuZ2U7XHJcbn1cclxuXHJcbi5iZ2RhcmtvcmFuZ2Uge1xyXG4gIGNvbG9yOiBkYXJrb3JhbmdlO1xyXG59XHJcblxyXG4uYmdncmVlbiB7XHJcbiAgY29sb3I6IGdyZWVuO1xyXG59XHJcblxyXG4uYmdibHVlIHtcclxuICBjb2xvcjogYmx1ZTtcclxufVxyXG5cclxuLmJncmVkIHtcclxuICBjb2xvcjogcmVkO1xyXG59XHJcbnRyLm1hdC1yb3cgPiB0ZCB7XHJcbiAgcGFkZGluZy1yaWdodDogMTVweDtcclxufVxyXG50ci5tYXQtcm93ID4gdGQge1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXgtd2lkdGg6IDExMHB4O1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/deputation/deputation-in/deputation-in.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/deputation/deputation-in/deputation-in.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\" *ngIf=\"empPageDeputation\" [events]=\"eventsSubject.asObservable()\"></app-comman-mst>\r\n<form (ngSubmit)=\"onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" novalidate>\r\n\r\n  <div class=\"alert\" [ngClass]=\"divbgcolor\" *ngIf=\"is_btnStatus\">\r\n    <strong> {{Message}}</strong>\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n      <span aria-hidden=\"true\" (click)=\"is_btnStatus=false\">&times;</span>\r\n    </button>&nbsp;\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-7\" [attr.disabled]=\"disableflag\">\r\n    <mat-card class=\"example-card\">\r\n      <ng-template matStepLabel>Deputation Details</ng-template>\r\n\r\n      <div class=\"fom-title\">Deputation In Details</div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Deputation Type\" formControlName=\"deputation_Type\" required [errorStateMatcher]=\"matcher\">\r\n\r\n            <mat-option *ngFor=\"let deputationType of deputationTypes\" [value]=\"deputationType.cddirCodeValue\">\r\n              {{deputationType.cddirCodeText}}\r\n            </mat-option>\r\n\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['deputation_Type'].errors?.required\">\r\n            Deputation Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Service Type\" formControlName=\"service_Type\" required [errorStateMatcher]=\"matcher\">\r\n\r\n            <mat-option *ngFor=\"let serviceType of serviceType\" [value]=\"serviceType.serviceValue\">\r\n              {{serviceType.serviceText}}\r\n            </mat-option>\r\n\r\n          </mat-select>\r\n\r\n          <mat-error *ngIf=\"form.controls['service_Type'].errors?.required\">\r\n            Service Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Deputation Order Number\" formControlName=\"deputation_Order\" required pattern=\"[A-Za-z1-9]{1}|[A-Za-z][A-Za-z0-9-/_]+\" maxlength=\"50\" [errorStateMatcher]=\"matcher\">\r\n          <mat-error *ngIf=\"form.controls['deputation_Order'].errors?.required\">\r\n            Deputation Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['deputation_Order'].errors?.pattern\">\r\n            Deputation Order Number is Invalid\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"Deputation\" placeholder=\"Deputation Order Date \" (dateChange)=\"Checkdate()\" (click)=\"Deputation.open()\" formControlName=\"deputation_Order_Date\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"Deputation\"></mat-datepicker-toggle>\r\n          <mat-datepicker #Deputation></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['deputation_Order_Date'].errors?.required\">\r\n            Deputation Order Date is Required\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Deputation From Office\" formControlName=\"deputation_From_Office\" required pattern=\"[A-Za-z]{1}[A-Za-z0-9- ]+\" maxlength=\"50\" [errorStateMatcher]=\"matcher\">\r\n          <mat-error *ngIf=\"form.controls['deputation_From_Office'].errors?.required\">\r\n            Deputation From Office is Required\r\n          </mat-error>\r\n          <!--<mat-error *ngIf=\"form.controls['deputation_From_Office'].errors?.minlength\">\r\n            Deputation From Office min length 2\r\n          </mat-error>-->\r\n          <mat-error *ngIf=\"form.controls['deputation_From_Office'].errors?.pattern\">\r\n            Deputation From Office is Invalid\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"State\" formControlName=\"deputation_State\" required [errorStateMatcher]=\"matcher\">\r\n\r\n            <mat-option *ngFor=\"let state of states\" [value]=\"state.stateId\">{{state.stateName}}</mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['deputation_State'].errors?.required\">\r\n            State is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"deputationEffetive1\" (click)=\"deputationEffetive1.open()\" [min]=\"form.controls['deputation_Order_Date'].value\"\r\n                 placeholder=\"Deputation w.e.f \" formControlName=\"deputation_Effective_Date\" required readonly [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"deputationEffetive1\"></mat-datepicker-toggle>\r\n          <mat-datepicker #deputationEffetive1></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['deputation_Effective_Date'].errors?.required\">\r\n            Deputation w.e.f is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-placeholder [ngClass]=\"flag ? 'actplacholder' : 'dsbplaceholder'\"> Repatriation Date </mat-placeholder>\r\n\r\n          <input matInput [matDatepicker]=\"repatriation\" (click)=\"repatriation.open()\" formControlName=\"deputation_Repatration_Date\" [min]=\"form.controls['deputation_Effective_Date'].value\" readonly>\r\n          <mat-datepicker-toggle matSuffix [for]=\"repatriation\"></mat-datepicker-toggle>\r\n          <mat-datepicker #repatriation></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Designation Before Deputation\" formControlName=\"desig_Before_Deputation\" (selectionChange)=\"updateValidation()\" required #singleSelect [errorStateMatcher]=\"matcher\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation Before Deputation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n\r\n            <mat-option *ngFor=\"let designa of filteredDesignation | async\" [value]=\"designa.desigId\">\r\n              {{designa.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['desig_Before_Deputation'].errors?.required\">\r\n            Designation Before Deputation is Required\r\n          </mat-error>\r\n\r\n\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" *ngIf=\"this.form.get('desig_Before_Deputation').value == 99\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Designation Before  Name\" formControlName=\"desig_Before_Remark\" required maxlength=\"150\" [errorStateMatcher]=\"matcher\">\r\n          <mat-error *ngIf=\"form.submitted || !form.controls['desig_Before_Remark'].valid || form.controls['desig_Before_Remark'].touched\">\r\n            Designation Before Name is Required\r\n          </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100 example-full-width\">\r\n          <mat-select placeholder=\"Designation After Deputation\" formControlName=\"desig_After_Deputation\" required #singleSelect [errorStateMatcher]=\"matcher\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation Before Deputation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <mat-option *ngFor=\"let designa of filteredDesignation | async\" [value]=\"designa.desigId\">\r\n              {{designa.desigDesc}}\r\n            </mat-option>\r\n\r\n          </mat-select>\r\n\r\n          <mat-error *ngIf=\"form.controls['desig_After_Deputation'].errors?.required\">\r\n            Designation After Deputation is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"1\" placeholder=\"Checker Remarks\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12\">\r\n        <!--*ngIf=\"!dedutionExist\"-->\r\n        <div class=\"fom-title\">\r\n          Add Deduction and Schedule Details\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <mat-table [dataSource]=\"deputation_Deduction.controls\">\r\n\r\n\r\n            <div class=\"col-md-12 col-lg-3\">\r\n              <ng-container matColumnDef=\"payItemsDeductionName\" class=\"col-lg-3\">\r\n                <mat-header-cell *matHeaderCellDef> Deduction Item </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element;\" [formGroup]=\"element\">\r\n                  <mat-form-field floatLabel=\"never\">\r\n                    <input matInput placeholder=\"Deduction Item\" formControlName=\"payItemsDeductionName\" readonly required>\r\n                    <!--<mat-error *ngIf=\"form.submitted || !form.controls['deputation_order_date'].valid || form.controls['deputation_order_date'].touched\">-->\r\n                    <!--Deputation w.e.f is required\r\n                    </mat-error>-->\r\n                  </mat-form-field>\r\n\r\n                </mat-cell>\r\n              </ng-container>\r\n\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-3\">\r\n              <!-- User Column -->\r\n              <ng-container matColumnDef=\"deduction_Schedule\" class=\"col-lg-3\">\r\n                <mat-header-cell *matHeaderCellDef> Deduction Schedule </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\" [formGroup]=\"element\">\r\n                  <mat-form-field floatLabel=\"never\">\r\n                    <mat-select formControlName=\"deduction_Schedule\" placeholder=\"Deduction Schedule\" (click)=\"getvalue(element)\" (selectionChange)=\"onUserChange(element)\" required [errorStateMatcher]=\"matcher\">\r\n                      <mat-option *ngFor=\"let dedutionpao of deductionSchedule\" [value]=\"dedutionpao\">\r\n                        {{ dedutionpao }}\r\n                      </mat-option>\r\n\r\n                    </mat-select>\r\n                    <!--<mat-error *ngIf=\"element.get('deduction_Schedule').errors?.required\">Deduction Schedule is required</mat-error>-->\r\n\r\n                  </mat-form-field>\r\n\r\n                </mat-cell>\r\n              </ng-container>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-3\">\r\n              <!-- Title Column -->\r\n              <ng-container matColumnDef=\"payItemsDeductionAccCdScheme\" class=\"col-lg-3\">\r\n                <mat-header-cell *matHeaderCellDef> Account Head </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element;\" [formGroup]=\"element\">\r\n                  <mat-form-field floatLabel=\"never\">\r\n                    <input matInput placeholder=\"Account Head\" formControlName=\"payItemsDeductionAccCdScheme\" maxlength=\"80\" required readonly [errorStateMatcher]=\"matcher\">\r\n                  </mat-form-field>\r\n                </mat-cell>\r\n              </ng-container>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-3\">\r\n              <ng-container matColumnDef=\"communication_Address\" class=\"col-lg-3\">\r\n                <mat-header-cell *matHeaderCellDef required> Communication Address </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\" [formGroup]=\"element\">\r\n                  <mat-form-field floatLabel=\"never\">\r\n                    <mat-select placeholder=\"Communication Address\" formControlName=\"communication_Address\" required #singleSelect [errorStateMatcher]=\"matcher\">\r\n                      <mat-option>\r\n                        <ngx-mat-select-search [formControl]=\"communicationFilterCtrl\" [placeholderLabel]=\"'Find Designation Before Deputation...'\" [noEntriesFoundLabel]=\"'Communication Address is not found'\"></ngx-mat-select-search>\r\n                      </mat-option>\r\n                      <mat-option *ngFor=\"let commu of filteredCommunication | async\" [value]=\"commu.pfId\">\r\n                        {{ commu.pfAgency_Desc }}\r\n                      </mat-option>\r\n\r\n                    </mat-select>\r\n                    <!--<mat-error *ngIf=\"element.get('communication_Address').errors?.required\">Communication Address is required</mat-error>-->\r\n                  </mat-form-field>\r\n                </mat-cell>\r\n              </ng-container>\r\n            </div>\r\n            <div class=\"col-md-12 col-lg-3\">\r\n              <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n              <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n            </div>\r\n          </mat-table>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6 || roleId == 9\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"btndisabled\" *ngIf=\"roleId == 6 || roleId == 9\">{{btnUpdateText}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm();\" *ngIf=\"roleId == 6 || roleId == 9\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"disableForwardflag\" *ngIf=\"roleId == 6\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n\r\n    </mat-card>\r\n  </div>\r\n  <!--<mat-accordion>\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n          Form value\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      <code>\r\n        {{form.value | json}}\r\n      </code>\r\n    </mat-expansion-panel>\r\n  </mat-accordion>-->\r\n</form>\r\n<!--right table-->\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"srNo\">\r\n\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr.No. </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element; let i = index;\"> {{i+1}}  </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"deputeFrom\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Deputation Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.deputation_Type_Name}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"orderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.deputation_Order}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\">Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'N' || element.veriFlag == 'R' || element.veriFlag == 'U')&& (roleId == 6 || roleId == 9)\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.empDeputDetailsId)\">edit</a>\r\n          <!--<a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteEOSDetails(element);DeletePopup = !DeletePopup\">\r\n            delete_forever\r\n          </a>-->\r\n          <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'N'  && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n        </td>\r\n\r\n      </ng-container>\r\n\r\n      <tr *ngIf=\"dataSource?.length> 0\">\r\n        <td colspan=\"3\" class=\"no-data-available\">No data!</td>\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\"></tr>\r\n    </table>\r\n\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput spellcheck=\"false\"   [(ngModel)]=\"rejectionRemark\" formControlName=\"rejectionRemark1\" rows=\"2\" maxlength=\"100\" align=\"center\" placeholder=\"Remarks\" required inputmode=\"full-width-latin\"></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.required\">Remarks is Required </mat-error>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.pattern\">Remarks is Invalid</mat-error>\r\n        \r\n\r\n\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n<!--<app-dialog [(visible)]=\"RejectPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" maxlength=\"50\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('R')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>-->\r\n"

/***/ }),

/***/ "./src/app/deputation/deputation-in/deputation-in.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/deputation/deputation-in/deputation-in.component.ts ***!
  \*********************************************************************/
/*! exports provided: DeputationInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationInComponent", function() { return DeputationInComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/deputation/deputationin.service */ "./src/app/services/deputation/deputationin.service.ts");
/* harmony import */ var _model_DeptationModel_depuationin_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../model/DeptationModel/depuationin-model */ "./src/app/model/DeptationModel/depuationin-model.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_empdetails_getempcode_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/empdetails/getempcode.service */ "./src/app/services/empdetails/getempcode.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../global/error-state-matcher */ "./src/app/global/error-state-matcher.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













var DeputationInComponent = /** @class */ (function () {
    function DeputationInComponent(master, deputationInService, snackBar, objEmpGetCodeService, _formBuilder, _message) {
        var _this = this;
        this.master = master;
        this.deputationInService = deputationInService;
        this.snackBar = snackBar;
        this.objEmpGetCodeService = objEmpGetCodeService;
        this._formBuilder = _formBuilder;
        this._message = _message;
        //displayedColumns = ['id', 'userId', 'title','userIdss']
        this.displayedColumns = ['payItemsDeductionName', 'deduction_Schedule', 'payItemsDeductionAccCdScheme', 'communication_Address'];
        this.displayedColumns2 = ['srNo', 'deputeFrom', 'orderNo', 'Status', 'action'];
        this.submitted = false;
        this.deputationTypes = [];
        this.serviceType = [];
        this.states = [];
        this.dedutionItems = [];
        this.designation = [];
        this.deductionSchedule = ['Current Organitation PAO', 'Other PAO'];
        this.deputationCommAddress = [];
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.rejectionRemark = '';
        this.flag = false;
        this.eventsSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.disableflag = true;
        // dedutionExist: boolean = false;
        this.disableForwardflag = false;
        this.btndisabled = false;
        this.btnCssClass = 'btn btn-success';
        this.designationCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredDesignation = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.communicationCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.communicationFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredCommunication = new rxjs__WEBPACK_IMPORTED_MODULE_8__["ReplaySubject"](1);
        // @ViewChild('f') forms: Form;
        /* ==================for Deputation Details---------------------------*/
        this._onDestroy = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.matcher = new _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_11__["MyErrorStateMatcher"]();
        this.empPageDeputation = true;
        this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(// tslint:disable-next-line: no-non-null-assertion
        // tslint:disable-next-line: max-line-length
        function (// tslint:disable-next-line: no-non-null-assertion
        commonEmpCode) {
            debugger;
            if (commonEmpCode.selectedIndex === 11) {
                // debugger
                _this.GetcommanMethod(commonEmpCode.empcode);
                _this.empPageDeputation = false;
            }
        });
    }
    DeputationInComponent.prototype.ngOnInit = function () {
        var _this = this;
        //registerForm: FormGroup;
        this.roleId = sessionStorage.getItem('userRoleID');
        this.btnUpdateText = 'Save';
        this.savebuttonstatus = true;
        this.FormDeatils();
        this.RejectPopupcreateForm();
        //this.setChangeValidate();
        this.form.get('controllerId').setValue(sessionStorage.getItem('controllerID'));
        RejectPopupForm: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"];
        this.getDeputationType();
        this.getServiceType();
        this.getState();
        this.getDedutionItems();
        //debugger;
        //this.getAllDesignation(33);
        this.getAllDesignation(sessionStorage.getItem('controllerID'));
        this.getDepuatationComm_Address();
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesignation();
        });
        this.communicationFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterCommunication();
        });
        this.btndisabled = true;
        this.disableForwardflag = true;
        this.form.disable();
        this.flag = true;
    };
    DeputationInComponent.prototype.GetcommanMethod = function (value) {
        //debugger;
        if (value == "") {
            this.btndisabled = true;
            this.empCd = value;
            this.btnUpdateText = 'Save';
            this.deputationDetail = [];
            var mapFormGroupValue = this.deputationDetail.map(_model_DeptationModel_depuationin_model__WEBPACK_IMPORTED_MODULE_7__["Deputation_Deduction"].asFormGroup);
            this.form.setControl('deputation_Deduction', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"](mapFormGroupValue));
            this.formDirective.resetForm();
            this.dataSource = null;
            this.form.disable();
        }
        else {
            this.empCd = value;
            this.btnUpdateText = 'Save';
            this.formDirective.resetForm();
            this.getDeputaiondetails(this.empCd, this.roleId);
            this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
            this.form.get('controllerId').setValue(sessionStorage.getItem('controllerID'));
            this.form.get('empCd').setValue(this.empCd);
        }
    };
    DeputationInComponent.prototype.FormDeatils = function () {
        this.form = this._formBuilder.group({
            'deputation_Type': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_Type_Name': [null],
            'service_Type': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_Order': [null, [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
                ]],
            'deputation_Order_Date': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_From_Office': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_State': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_Effective_Date': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'deputation_Repatration_Date': [null],
            'desig_Before_Deputation': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'desig_Before_Remark': [null],
            'desig_After_Deputation': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'empCd': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'empDeputDetailsId': [null],
            'flagUpdate': [null],
            'rejectionRemark': [null],
            'createdBy': [null],
            'controllerId': [null],
            deputation_Deduction: this._formBuilder.array([]),
            'validate': ''
        });
    };
    DeputationInComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
        });
    };
    DeputationInComponent.prototype.getDeputaiondetails = function (Empcd, roleId) {
        var _this = this;
        this.deputationInService.getDeputaionDetails(Empcd, roleId).subscribe(function (result) {
            var myArrayResult = [];
            myArrayResult.push(result);
            _this.deputationDetail = result.deputation_Deduction;
            var mapFormGroupValue = _this.deputationDetail.map(_model_DeptationModel_depuationin_model__WEBPACK_IMPORTED_MODULE_7__["Deputation_Deduction"].asFormGroup);
            _this.form.setControl('deputation_Deduction', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"](mapFormGroupValue));
            // debugger;
            if (result.empDeputDetailsId == null) {
                //this.form.reset();
                _this.dataSource = null;
                _this.form.enable();
                _this.form.clearValidators;
                _this.disableflag = true;
                _this.disableForwardflag = true;
                _this.infoScreen = false;
                _this.flag = true;
                if (_this.roleId == '5') {
                    _this.form.disable();
                }
                if (_this.empCd == null || _this.empCd == undefined || _this.empCd == "") {
                    _this.btndisabled = true;
                }
                else {
                    _this.btndisabled = false;
                }
                _this.form.get('empCd').setValue(_this.empCd);
            }
            else {
                //if (mapFormGroupValue.length == 0) {
                //  this.dedutionExist = true;
                //}
                //else {
                //  this.dedutionExist = false;
                //}
                _this.form.patchValue(result);
                _this.dataSource = myArrayResult;
                _this.form.disable();
                _this.flag = false;
                _this.btndisabled = true;
                _this.infoScreen = true;
                if (result.veriFlag == 'E' || result.veriFlag == 'U') {
                    _this.disableForwardflag = false;
                }
                else {
                    _this.disableForwardflag = true;
                }
            }
            if (_this.roleId == '5' && (result.veriFlag == 'V' || result.veriFlag == 'R')) {
                _this.infoScreen = false;
            }
            _this.updateValidation();
        });
    };
    DeputationInComponent.prototype.btnEditClick = function (empDeputId) {
        this.formDirective.resetForm();
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.form.patchValue(value);
        this.form.enable();
        this.flag = true;
        if (value.veriFlag == 'N') {
            this.btnUpdateText = 'Save';
        }
        else {
            this.btnUpdateText = 'Update';
        }
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            // debugger;
            this.btndisabled = false;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            this.btndisabled = false;
            // debugger;
            this.disableflag = true;
            if (value.veriFlag == 'N') {
                this.disableForwardflag = true;
            }
            else {
                this.disableForwardflag = true;
            }
            //this.Btntxt = 'Save';
        }
    };
    DeputationInComponent.prototype.btnInfoClick = function (empDeputId) {
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.form.patchValue(value);
        this.form.disable();
        //let empStatus = value.veriFlag;
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            //debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            //debugger;
            this.disableForwardflag = true;
            //this.Btntxt = 'Save';
        }
    };
    DeputationInComponent.prototype.updateValidation = function () {
        //debugger;
        if (this.form.get('desig_Before_Deputation').value !== 99) {
            this.form.get('desig_Before_Remark').clearValidators();
            this.form.get('desig_Before_Remark').updateValueAndValidity();
            this.form.get('desig_Before_Remark').setValue("");
        }
    };
    DeputationInComponent.prototype.getDeputationType = function () {
        var _this = this;
        this.master.getDeputationType().subscribe(function (res) {
            // debugger;
            _this.deputationTypes = res;
        });
    };
    DeputationInComponent.prototype.getServiceType = function () {
        var _this = this;
        this.master.getServiceType().subscribe(function (res) {
            _this.serviceType = res;
        });
    };
    DeputationInComponent.prototype.getState = function () {
        var _this = this;
        this.master.getState().subscribe(function (res) {
            _this.states = res;
        });
    };
    DeputationInComponent.prototype.getDedutionItems = function () {
        var _this = this;
        this.deputationInService.GetPayItems().subscribe(function (res) {
            _this.dedutionItems = res;
        });
    };
    DeputationInComponent.prototype.getAllDesignation = function (controllerId) {
        var _this = this;
        this.deputationInService.GetAllDesignation(controllerId).subscribe(function (res) {
            _this.designation = res;
            _this.designationCtrl.setValue(_this.designation);
            _this.filteredDesignation.next(_this.designation);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesignation();
        });
    };
    DeputationInComponent.prototype.getDepuatationComm_Address = function () {
        var _this = this;
        this.deputationInService.GetCommunicationAddress().subscribe(function (res) {
            _this.deputationCommAddress = res;
            _this.communicationCtrl.setValue(_this.deputationCommAddress);
            _this.filteredCommunication.next(_this.deputationCommAddress);
        });
        this.communicationFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterCommunication();
        });
    };
    DeputationInComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    DeputationInComponent.prototype.filterDesignation = function () {
        if (!this.designation) {
            return;
        }
        // get the search keyword
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesignation.next(this.designation.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredDesignation.next(this.designation.filter(function (designation) { return designation.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    DeputationInComponent.prototype.filterCommunication = function () {
        if (!this.deputationCommAddress) {
            return;
        }
        // get the search keyword
        var search = this.communicationFilterCtrl.value;
        if (!search) {
            this.filteredCommunication.next(this.deputationCommAddress.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredCommunication.next(this.deputationCommAddress.filter(function (deputationCommAddress) { return deputationCommAddress.pfAgency_Desc.toLowerCase().indexOf(search) > -1; }));
    };
    DeputationInComponent.prototype.resetForm = function () {
        //location.reload(true);
        this.form.reset();
        this.formDirective.resetForm();
        this.btnUpdateText = 'Save';
        this.disableForwardflag = true;
        this.btndisabled = true;
        this.dataSource = null;
        this.flag = true;
        this.form.disable();
        //if (this.empCd != null && this.empCd != undefined && this.empCd != "") {
        //  this.form.disable();
        //  this.getDeputaiondetails(this.empCd, this.roleId);
        //}
        this.eventsSubject.next(this.data);
    };
    Object.defineProperty(DeputationInComponent.prototype, "deputation_Deduction", {
        get: function () {
            return this.form.get('deputation_Deduction');
        },
        enumerable: true,
        configurable: true
    });
    DeputationInComponent.prototype.Checkdate = function () {
        debugger;
        if (this.form.get('deputation_Order_Date').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('deputation_Effective_Date').value || this.form.get('deputation_Order_Date').value > this.form.get('deputation_Effective_Date').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('deputation_Effective_Date').setValue("");
        }
        if (this.form.get('deputation_Effective_Date').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('deputation_Repatration_Date').value || this.form.get('deputation_Effective_Date').value > this.form.get('deputation_Repatration_Date').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('deputation_Repatration_Date').setValue("");
        }
        //alert(dd)
    };
    // On user change fill the deduction item scheme 
    DeputationInComponent.prototype.onUserChange = function (deputation_Deduction) {
        return __awaiter(this, void 0, void 0, function () {
            var deduc_Sche, controlArray;
            return __generator(this, function (_a) {
                deduc_Sche = deputation_Deduction.get('deduction_Schedule');
                controlArray = this.form.get('deputation_Deduction');
                this.GetDeatisl(controlArray, deduc_Sche.value);
                return [2 /*return*/];
            });
        });
    };
    DeputationInComponent.prototype.getvalue = function (deputation_Deduction) {
        var deduc_Sche = deputation_Deduction.get('deduction_Schedule');
        if (deduc_Sche.value == "") {
            deduc_Sche.setValue("");
            deduc_Sche.errors.required;
            this.deductioniinvalid = "in";
        }
    };
    DeputationInComponent.prototype.GetDeatisl = function (controlArray, deductionSchedule) {
        return __awaiter(this, void 0, void 0, function () {
            var _loop_1, this_1, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _loop_1 = function (i) {
                            var msEmpDuesID;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        msEmpDuesID = controlArray.controls[i].get('msEmpDuesId').value;
                                        console.log(i);
                                        if (!(deductionSchedule == "Other PAO")) return [3 /*break*/, 2];
                                        return [4 /*yield*/, this_1.deputationInService.GetScheme_Code("0").subscribe(function (res) {
                                                controlArray.controls[i].get('deduction_Schedule').setValue(deductionSchedule);
                                                controlArray.controls[i].get('payItemsDeductionAccCdScheme').setValue(res.payItemsDeductionAccCdScheme);
                                                // this.deputationCommAddress = res;
                                            })];
                                    case 1:
                                        _a.sent();
                                        return [3 /*break*/, 4];
                                    case 2: return [4 /*yield*/, this_1.deputationInService.GetScheme_Code(msEmpDuesID).subscribe(function (res) {
                                            controlArray.controls[i].get('deduction_Schedule').setValue(deductionSchedule);
                                            controlArray.controls[i].get('payItemsDeductionAccCdScheme').setValue(res.payItemsDeductionAccCdScheme);
                                            //DeduAccCdSche.markAsUntouched();
                                            // this.deputationCommAddress = res;
                                        })];
                                    case 3:
                                        _a.sent();
                                        _a.label = 4;
                                    case 4: return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < controlArray.length)) return [3 /*break*/, 4];
                        return [5 /*yield**/, _loop_1(i)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    //get f() { return this.form.controls; }
    DeputationInComponent.prototype.getvalidateDeduction = function (deputation_Deduction) {
        // debugger;
        for (var i = 0; i < deputation_Deduction.controls.length; i++) {
            //debugger;
            var deductionSchedule = deputation_Deduction.value[i].deduction_Schedule;
            var communicationAddress = deputation_Deduction.value[i].communication_Address;
            if (deductionSchedule == "" && communicationAddress == 0) {
                alert("Select deduction schedule And communication address");
                return false;
            }
            if (deductionSchedule == "" && communicationAddress != 0) {
                alert("Select deduction schedule");
                return false;
            }
            if (deductionSchedule != "" && communicationAddress == 0) {
                alert("Select communication address");
                return false;
            }
            //const msEmpDuesID = deputation_Deduction.controls[i].get('msEmpDuesId').value;
        }
    };
    DeputationInComponent.prototype.onSubmit = function () {
        var _this = this;
        //debugger;
        this.submitted = true;
        this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
        // stop here if form is invalid
        var deductionvalidation = this.getvalidateDeduction(this.deputation_Deduction);
        if (this.form.invalid) {
            return false;
        }
        else {
            if (deductionvalidation == false) {
                return false;
            }
            else {
                this.deputationInService.InsertUpateDeputationDetails(this.form.value).subscribe(function (result1) {
                    if (parseInt(result1) >= 1) {
                        if (_this.btnUpdateText == 'Update') {
                            _this.is_btnStatus = true;
                            _this.divbgcolor = "alert-success";
                            _this.Message = _this._message.updateMsg;
                            _this.form.reset();
                            _this.formDirective.resetForm();
                            _this.getDeputaiondetails(_this.empCd, _this.roleId);
                            _this.btnUpdateText = 'Save';
                        }
                        else {
                            _this.Message = _this._message.saveMsg;
                            _this.form.reset();
                            _this.formDirective.resetForm();
                            _this.getDeputaiondetails(_this.empCd, _this.roleId);
                            _this.btnUpdateText = 'Save';
                        }
                    }
                    else {
                        if (_this.btnUpdateText == 'Update') {
                            _this.Message = _this._message.updateFailedMsg;
                        }
                        else {
                            _this.Message = _this._message.saveFailedMsg;
                        }
                    }
                    setTimeout(function () {
                        _this.is_btnStatus = false;
                        _this.Message = '';
                    }, 8000);
                });
            }
        }
    };
    DeputationInComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    DeputationInComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        debugger;
        var RejectionComment = '';
        if (this.RejectPopupForm.invalid && statusFlag == 'R') {
            this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
            return false;
        }
        else {
            var empDeputDetailsId = this.form.get('empDeputDetailsId').value;
            if (statusFlag == 'R') {
                RejectionComment = this.rejectionRemark;
            }
            else {
                RejectionComment = '';
            }
            this.deputationInService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(function (result1) {
                if (parseInt(result1) >= 1) {
                    _this.is_btnStatus = true;
                    _this.divbgcolor = "alert-success";
                    if (statusFlag == 0) {
                        _this.Message = _this._message.forwardDDOCheckerMsg;
                    }
                    if (statusFlag == 'V') {
                        _this.Message = _this._message.VerifyedByChecker;
                    }
                    if (statusFlag == 'R') {
                        _this.Message = _this._message.RejectedByChecker;
                    }
                    _this.form.reset();
                    _this.formDirective.resetForm();
                    _this.btnUpdateText = 'Save';
                    _this.getDeputaiondetails(_this.empCd, _this.roleId);
                    _this.infoScreen = false;
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.rejectionRemark = '';
                }
                setTimeout(function () {
                    _this.is_btnStatus = false;
                    _this.Message = '';
                }, 8000);
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DeputationInComponent.prototype, "empPageDeputation", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], DeputationInComponent.prototype, "formDirective", void 0);
    DeputationInComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-deputation-in',
            template: __webpack_require__(/*! ./deputation-in.component.html */ "./src/app/deputation/deputation-in/deputation-in.component.html"),
            styles: [__webpack_require__(/*! ./deputation-in.component.css */ "./src/app/deputation/deputation-in/deputation-in.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_10__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_services_master_master_service__WEBPACK_IMPORTED_MODULE_5__["MasterService"], _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_6__["DeputationinService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _services_empdetails_getempcode_service__WEBPACK_IMPORTED_MODULE_9__["GetempcodeService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _global_common_msg__WEBPACK_IMPORTED_MODULE_10__["CommonMsg"]])
    ], DeputationInComponent);
    return DeputationInComponent;
}());



/***/ }),

/***/ "./src/app/deputation/deputation-out/deputation-out.component.css":
/*!************************************************************************!*\
  !*** ./src/app/deputation/deputation-out/deputation-out.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgorange {\r\n  color: orange;\r\n}\r\n\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n\r\n.bggreen {\r\n  color: green;\r\n}\r\n\r\n.bgblue {\r\n  color: blue;\r\n}\r\n\r\n.bgred {\r\n  color: red;\r\n}\r\n\r\n.primary {\r\n  color: red;\r\n}\r\n\r\n.secondary {\r\n  color: green;\r\n}\r\n\r\n.mat-focused .mat-form-field-label {\r\n  /*change color of label*/\r\n  color: red !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVwdXRhdGlvbi9kZXB1dGF0aW9uLW91dC9kZXB1dGF0aW9uLW91dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsV0FBVztDQUNaOztBQUNEO0VBQ0UsV0FBVztDQUNaOztBQUVEO0VBQ0UsYUFBYTtDQUNkOztBQUdEO0VBQ0UseUJBQXlCO0VBQ3pCLHNCQUFzQjtDQUN2QiIsImZpbGUiOiJzcmMvYXBwL2RlcHV0YXRpb24vZGVwdXRhdGlvbi1vdXQvZGVwdXRhdGlvbi1vdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZ29yYW5nZSB7XHJcbiAgY29sb3I6IG9yYW5nZTtcclxufVxyXG5cclxuLmJnZGFya29yYW5nZSB7XHJcbiAgY29sb3I6IGRhcmtvcmFuZ2U7XHJcbn1cclxuXHJcbi5iZ2dyZWVuIHtcclxuICBjb2xvcjogZ3JlZW47XHJcbn1cclxuXHJcbi5iZ2JsdWUge1xyXG4gIGNvbG9yOiBibHVlO1xyXG59XHJcblxyXG4uYmdyZWQge1xyXG4gIGNvbG9yOiByZWQ7XHJcbn1cclxuLnByaW1hcnkge1xyXG4gIGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5zZWNvbmRhcnkge1xyXG4gIGNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuXHJcbi5tYXQtZm9jdXNlZCAubWF0LWZvcm0tZmllbGQtbGFiZWwge1xyXG4gIC8qY2hhbmdlIGNvbG9yIG9mIGxhYmVsKi9cclxuICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/deputation/deputation-out/deputation-out.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/deputation/deputation-out/deputation-out.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [events]=\"eventsSubject.asObservable()\"></app-comman-mst>\r\n<form (ngSubmit)=\"form.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" >\r\n  <div class=\"col-md-12 col-lg-7\" >\r\n    <mat-card class=\"example-card\">\r\n      <ng-template matStepLabel>Deputation Details</ng-template>\r\n\r\n      <div class=\"fom-title\">Deputation Out Details</div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Deputation Type\" formControlName=\"deputedTypeId\" required [errorStateMatcher]=\"matcher\">\r\n             <mat-option *ngFor=\"let deputationType of deputationTypes\" [value]=\"deputationType.cddirCodeValue\">\r\n              {{deputationType.cddirCodeText}}\r\n            </mat-option>\r\n\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['deputedTypeId'].errors?.required\">\r\n            Deputation Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Service Type\" formControlName=\"serviceTypeId\" required [errorStateMatcher]=\"matcher\">\r\n           <mat-option *ngFor=\"let serviceType of serviceType\" [value]=\"serviceType.serviceValue\">\r\n              {{serviceType.serviceText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['serviceTypeId'].errors?.required\">\r\n            Service Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Deputation Order Number\" formControlName=\"depuOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z1-9]{1}|[A-Za-z][A-Za-z0-9-/_]+\" required [errorStateMatcher]=\"matcher\" >\r\n          <mat-error *ngIf=\" form.controls['depuOrderNo'].errors?.required\">\r\n            Deputation Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['depuOrderNo'].errors?.pattern\">\r\n            Deputation Order Number is Invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"deputationDate\" placeholder=\"Deputation Order Date \" (click)=\"deputationDate.open()\"  (dateChange)=\"Checkdate()\" formControlName=\"depuOrderDate\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"deputationDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #deputationDate></mat-datepicker>\r\n          <mat-error *ngIf=\" form.controls['depuOrderDate'].errors?.required\">\r\n            Deputation Order Date is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <input matInput placeholder=\"Deputation To Office\" formControlName=\"depuedToOffice\" pattern=\"[A-Za-z]{1}[A-Za-z- ]+\" required maxlength=\"50\" [errorStateMatcher]=\"matcher\">\r\n          <mat-error *ngIf=\"form.controls['depuedToOffice'].errors?.required\">\r\n            Deputation To Office is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['depuedToOffice'].errors?.pattern\">\r\n            Deputation To Office is Invalid\r\n          </mat-error>\r\n\r\n\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Deputation On Designation\" formControlName=\"depuOnDesignation\" required #singleSelect [errorStateMatcher]=\"matcher\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation Before Deputation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n          \r\n            <mat-option *ngFor=\"let designa of filteredDesignation | async\" [value]=\"designa.desigId\">\r\n              {{designa.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['depuOnDesignation'].errors?.required\">\r\n            Deputation On Designation is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"dateofDeputation\" (click)=\"dateofDeputation.open()\" placeholder=\"Date Of Deputation\" formControlName=\"dateOfDeputataion\" [min]=\"form.controls['depuOrderDate'].value\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"dateofDeputation\"></mat-datepicker-toggle>\r\n          <mat-datepicker #dateofDeputation></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['dateOfDeputataion'].errors?.required\">\r\n            Date Of Deputation is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"relievingDate\" (click)=\"relievingDate.open()\" placeholder=\"Relieved On Date \" formControlName=\"relievingDate\" [min]=\"form.controls['depuOrderDate'].value\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"relievingDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #relievingDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['relievingDate'].errors?.required\">\r\n            Relieved On Date is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-placeholder [ngClass]=\"flag ? 'actplacholder' : 'dsbplaceholder'\"> Tenure Of Deputation(Years) </mat-placeholder>\r\n\r\n          <input matInput  (keypress)=\"numberOnly($event)\" formControlName=\"depuTenureYear\" pattern=\"([1-9]|[1-5][0-9]|60)\" maxlength=\"2\" [ngClass]=\"flag ? 'label-disabled' : 'label-active'\">\r\n          <mat-error *ngIf=\"form.controls['depuTenureYear'].errors?.pattern\">\r\n            Tenure Of Deputation(Year) is Invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-placeholder [ngClass]=\"flag ? 'actplacholder' : 'dsbplaceholder'\"> Tenure Of Deputation(Months) </mat-placeholder>\r\n\r\n          <input matInput  (keypress)=\"numberOnly($event)\" formControlName=\"depuTenureMonth\" pattern=\"([1-9]|1[0-2])\" maxlength=\"2\">\r\n          <mat-error *ngIf=\"form.controls['depuTenureMonth'].errors?.pattern\">\r\n            Tenure Of Deputation(Months) is Invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      \r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-placeholder [ngClass]=\"flag ? 'actplacholder' : 'dsbplaceholder'\"> Tenure Of Deputation(Days) </mat-placeholder>\r\n\r\n          <input matInput (keypress)=\"numberOnly($event)\" formControlName=\"depuTenureDays\" pattern=\"([1-9]|[12][0-9]|3[01])\" maxlength=\"2\">\r\n          <mat-error *ngIf=\"form.controls['depuTenureDays'].errors?.pattern\">\r\n            Tenure Of Deputation(Days) is Invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"2\" placeholder=\"Checker Remarks\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\"  [disabled]=\"btndisabled\">{{btnUpdatetext}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n\r\n\r\n\r\n\r\n\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <!--<mat-accordion>\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header>\r\n          <mat-panel-title>\r\n            Form value\r\n          </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <code>\r\n          {{form.value | json}}\r\n        </code>\r\n      </mat-expansion-panel>\r\n    </mat-accordion>-->\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"srNo\">\r\n\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr.No. </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element; let i = index;\"> {{i+1}}  </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"deputeFrom\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Deputation Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.depuTypeName}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Joining Column -->\r\n      <ng-container matColumnDef=\"orderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.depuOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\" >Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td *matCellDef=\"let element\">\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'N' || element.veriFlag == 'R' || element.veriFlag == 'U')&& roleId == 6\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.empDeputDetailsId)\">edit</a>\r\n          <!--<a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteEOSDetails(element);DeletePopup = !DeletePopup\">\r\n    delete_forever\r\n  </a>-->\r\n          <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'N'  && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n        </td>\r\n\r\n</ng-container>\r\n      <tr *ngIf=\"dataSource?.length> 0\">\r\n        <td colspan=\"3\" class=\"no-data-available\">No data!</td>\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\"></tr>\r\n    </table>\r\n\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput spellcheck=\"false\" [(ngModel)]=\"rejectionRemark\" formControlName=\"rejectionRemark1\" rows=\"2\" maxlength=\"100\" align=\"center\" placeholder=\"Remarks\" required inputmode=\"full-width-latin\"></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.required\">Remarks is Required </mat-error>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.pattern\">Remarks is Invalid</mat-error>\r\n\r\n\r\n\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/deputation/deputation-out/deputation-out.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/deputation/deputation-out/deputation-out.component.ts ***!
  \***********************************************************************/
/*! exports provided: DeputationOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationOutComponent", function() { return DeputationOutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/deputation/deputationin.service */ "./src/app/services/deputation/deputationin.service.ts");
/* harmony import */ var _services_deputation_deputationout_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/deputation/deputationout.service */ "./src/app/services/deputation/deputationout.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../global/error-state-matcher */ "./src/app/global/error-state-matcher.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var DeputationOutComponent = /** @class */ (function () {
    function DeputationOutComponent(_formBuilder, master, deputationInService, deputationOutService, snackBar, _message) {
        this._formBuilder = _formBuilder;
        this.master = master;
        this.deputationInService = deputationInService;
        this.deputationOutService = deputationOutService;
        this.snackBar = snackBar;
        this._message = _message;
        this.deputationTypes = [];
        this.serviceType = [];
        this.designation = [];
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.rejectionRemark = '';
        this.disableForwardflag = true;
        this.btndisabled = false;
        this.disableflag = true;
        this.flag = false;
        this.displayedColumns2 = ['srNo', 'deputeFrom', 'orderNo', 'Status', 'action'];
        this.eventsSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.designationCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredDesignation = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.submitted = false;
        this._onDestroy = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.matcher = new _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_8__["MyErrorStateMatcher"]();
    }
    DeputationOutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.btnUpdatetext = 'Save';
        this.roleId = sessionStorage.getItem('userRoleID');
        this.savebuttonstatus = true;
        this.FormDeatils();
        this.RejectPopupcreateForm();
        this.getDeputationType();
        this.getServiceType();
        this.getAllDesignation();
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesignation();
        });
        this.btndisabled = true;
        this.form.disable();
        this.flag = true;
    };
    DeputationOutComponent.prototype.GetcommanMethod = function (value) {
        //this.getLoanDetails(value);
        debugger;
        this.formDirective.resetForm();
        this.btnUpdatetext = 'Save';
        this.empCd = value;
        if (value == "") {
            this.disableForwardflag = true;
            this.btndisabled = true;
            this.dataSource = null;
            this.form.disable();
        }
        else {
            this.getDeputaiondetails(value, this.roleId);
        }
        this.form.get('employeeCode').setValue(this.empCd);
    };
    //someDateToBlock: number = 3;
    //dateFilter  = (d: Date): boolean => {
    //  const day = d.getDay();
    //  debugger;
    //  // THIS FUNCTION CANNOT ACCESS THE VARIABLE 'someDateToBlock'
    //  return day !== 0 && day !== 6;
    //}
    //dateFilter = (d: Date = new Date()) => d.getDate() == 1;
    DeputationOutComponent.prototype.getDeputaiondetails = function (Empcd, roleId) {
        var _this = this;
        debugger;
        this.deputationOutService.getAllDepuatationOutDetails(Empcd, roleId).subscribe(function (result) {
            if (result.length == 0) {
                _this.formDirective.resetForm();
                _this.form.enable();
                _this.dataSource = null;
                _this.infoScreen = false;
                _this.form.get('employeeCode').setValue(_this.empCd);
                _this.btndisabled = false;
                _this.flag = true;
                if (_this.roleId == '5') {
                    _this.form.disable();
                }
            }
            else {
                _this.dataSource = result;
                // this.form.enable();
                _this.form.patchValue(result[0]);
                _this.form.disable();
                _this.btndisabled = true;
                _this.infoScreen = true;
                _this.flag = false;
                if (result[0].veriFlag == 'E' || result[0].veriFlag == 'U') {
                    _this.disableForwardflag = false;
                }
                else {
                    _this.disableForwardflag = true;
                }
            }
            if (_this.roleId == '5' && (result[0].veriFlag == 'V' || result[0].veriFlag == 'R')) {
                _this.infoScreen = false;
            }
        });
    };
    DeputationOutComponent.prototype.FormDeatils = function () {
        this.form = this._formBuilder.group({
            'deputedTypeId': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'depuTypeName': [null],
            'serviceTypeId': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'depuOrderNo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'depuOrderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'depuedToOffice': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'depuOnDesignation': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'dateOfDeputataion': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'relievingDate': [null],
            'depuTenureYear': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(4)],
            'depuTenureMonth': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(2)],
            'depuTenureDays': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(2)],
            'employeeCode': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'empDeputDetailsId': [null],
            'flagUpdate': [null],
            'rejectionRemark': [null]
        });
    };
    DeputationOutComponent.prototype.btnEditClick = function (empDeputId) {
        debugger;
        this.formDirective.resetForm();
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.flag = true;
        this.form.patchValue(value);
        this.form.enable();
        //if (value.veriFlag == 'N') {
        //}
        //else {
        //}
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.btndisabled = false;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableflag = true;
            this.btndisabled = false;
            if (value.veriFlag == 'N') {
                this.btnUpdatetext = 'Save';
                this.disableForwardflag = true;
            }
            else {
                this.btnUpdatetext = 'Update';
                this.disableForwardflag = true;
            }
            //this.Btntxt = 'Save';
        }
    };
    DeputationOutComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
        });
    };
    DeputationOutComponent.prototype.btnInfoClick = function (empDeputId) {
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.form.patchValue(value);
        this.form.disable();
        //let empStatus = value.veriFlag;
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            this.disableForwardflag = true;
            //this.Btntxt = 'Save';
        }
    };
    DeputationOutComponent.prototype.numberOnly = function (event) {
        debugger;
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    DeputationOutComponent.prototype.Checkdate = function () {
        if (this.form.get('depuOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('dateOfDeputataion').value || this.form.get('depuOrderDate').value > this.form.get('dateOfDeputataion').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('dateOfDeputataion').setValue("");
        }
        if (this.form.get('depuOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('relievingDate').value || this.form.get('depuOrderDate').value > this.form.get('relievingDate').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('relievingDate').setValue("");
        }
        //alert(dd)
    };
    DeputationOutComponent.prototype.getDeputationType = function () {
        var _this = this;
        this.master.getDeputationType().subscribe(function (res) {
            _this.deputationTypes = res;
        });
    };
    DeputationOutComponent.prototype.getServiceType = function () {
        var _this = this;
        this.master.getServiceType().subscribe(function (res) {
            _this.serviceType = res;
        });
    };
    DeputationOutComponent.prototype.getAllDesignation = function () {
        var _this = this;
        this.deputationInService.GetAllDesignation(sessionStorage.getItem('controllerID')).subscribe(function (res) {
            _this.designation = res;
            _this.designationCtrl.setValue(_this.designation);
            _this.filteredDesignation.next(_this.designation);
        });
    };
    DeputationOutComponent.prototype.filterDesignation = function () {
        if (!this.designation) {
            return;
        }
        // get the search keyword
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesignation.next(this.designation.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredDesignation.next(this.designation.filter(function (designation) { return designation.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    //get f() { return this.form.controls; }
    DeputationOutComponent.prototype.onSubmit = function () {
        var _this = this;
        //   debugger;
        this.submitted = true;
        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        else {
            if (this.btnUpdatetext == 'Update') {
                this.form.get('flagUpdate').setValue('update');
            }
            else {
                this.form.get('flagUpdate').setValue('insert');
            }
            this.deputationOutService.InsertUpateDeputationOutDetails(this.form.value).subscribe(function (result1) {
                var resultvalue = result1.split("-");
                if (parseInt(resultvalue[0]) >= 1) {
                    if (_this.btnUpdatetext == 'Update') {
                        _this.snackBar.open(_this._message.updateMsg, null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getDeputaiondetails(_this.empCd, _this.roleId);
                        _this.btnUpdatetext = 'Save';
                        _this.disableForwardflag = false;
                    }
                    else {
                        _this.snackBar.open(_this._message.saveMsg, null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getDeputaiondetails(_this.empCd, _this.roleId);
                        _this.btnUpdatetext == 'Save';
                        _this.disableForwardflag = false;
                    }
                }
                else {
                    if (_this.btnUpdatetext == 'Update') {
                        if (resultvalue[2] = 'Duplicate Record') {
                            _this.snackBar.open(_this._message.depRepaAlreadyExits, null, { duration: 4000 });
                        }
                        else {
                            _this.snackBar.open(_this._message.updateFailedMsg, null, { duration: 4000 });
                        }
                    }
                    else if (resultvalue[2] = 'Duplicate Record') {
                        _this.snackBar.open(_this._message.depRepaAlreadyExits, null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open(_this._message.saveFailedMsg, null, { duration: 4000 });
                    }
                }
            });
        }
    };
    DeputationOutComponent.prototype.ResetForm = function () {
        debugger;
        // location.reload(true);
        this.eventsSubject.next(this.data);
        this.form.reset();
        this.formDirective.resetForm();
        this.btnUpdatetext = 'Save';
        this.disableForwardflag = true;
        this.btndisabled = true;
        this.dataSource = null;
        this.flag = true;
        this.form.disable();
        //if (this.empCd == null || this.empCd == undefined) {
        //  this.form.disable();
        //  // this.getDeputaiondetails(this.empCd, this.roleId);
        //  this.form.get('employeeCode').setValue(this.empCd);
        //}
        //else {
        //  this.form.get('employeeCode').setValue(this.empCd);
        //  if (this.dataSource != null && this.dataSource != undefined) {
        //    let value = this.dataSource[0]
        //    if (this.dataSource[0].veriFlag == 'E' || this.dataSource[0].veriFlag == 'U') {
        //      this.disableForwardflag = false;
        //    }
        //    else {
        //      this.disableForwardflag = true;
        //    }
        //}
        //  }
        //this.form.disable();
    };
    DeputationOutComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    DeputationOutComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        var RejectionComment = '';
        if (this.RejectPopupForm.invalid && statusFlag == 'R') {
            this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
            return false;
        }
        else {
            var empDeputDetailsId = this.form.get('empDeputDetailsId').value;
            if (statusFlag == 'R') {
                RejectionComment = this.rejectionRemark;
            }
            else {
                RejectionComment = '';
            }
            this.deputationInService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(function (result1) {
                if (parseInt(result1) >= 1) {
                    if (statusFlag == 0) {
                        _this.snackBar.open(_this._message.forwardDDOCheckerMsg, null, { duration: 4000 });
                    }
                    if (statusFlag == 'V') {
                        _this.snackBar.open(_this._message.VerifyedByChecker, null, { duration: 4000 });
                    }
                    if (statusFlag == 'R') {
                        _this.snackBar.open(_this._message.RejectedByChecker, null, { duration: 4000 });
                    }
                    _this.form.reset();
                    _this.formDirective.resetForm();
                    _this.btnUpdatetext = 'Save';
                    _this.getDeputaiondetails(_this.empCd, _this.roleId);
                    _this.infoScreen = false;
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.rejectionRemark = '';
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], DeputationOutComponent.prototype, "formDirective", void 0);
    DeputationOutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-deputation-out',
            template: __webpack_require__(/*! ./deputation-out.component.html */ "./src/app/deputation/deputation-out/deputation-out.component.html"),
            styles: [__webpack_require__(/*! ./deputation-out.component.css */ "./src/app/deputation/deputation-out/deputation-out.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_9__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_master_master_service__WEBPACK_IMPORTED_MODULE_4__["MasterService"], _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_5__["DeputationinService"], _services_deputation_deputationout_service__WEBPACK_IMPORTED_MODULE_6__["DeputationoutService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _global_common_msg__WEBPACK_IMPORTED_MODULE_9__["CommonMsg"]])
    ], DeputationOutComponent);
    return DeputationOutComponent;
}());



/***/ }),

/***/ "./src/app/deputation/deputation-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/deputation/deputation-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: DeputationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationRoutingModule", function() { return DeputationRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _deputation_in_deputation_in_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deputation-in/deputation-in.component */ "./src/app/deputation/deputation-in/deputation-in.component.ts");
/* harmony import */ var _deputation_out_deputation_out_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./deputation-out/deputation-out.component */ "./src/app/deputation/deputation-out/deputation-out.component.ts");
/* harmony import */ var _repatriation_details_repatriation_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./repatriation-details/repatriation-details.component */ "./src/app/deputation/repatriation-details/repatriation-details.component.ts");
/* harmony import */ var _deputation_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./deputation.module */ "./src/app/deputation/deputation.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//import { CommanMstComponent } from '../loanmgt/comman-mst/comman-mst.component';
var routes = [
    {
        path: '', component: _deputation_module__WEBPACK_IMPORTED_MODULE_5__["DeputationModule"], children: [
            {
                path: 'deputationin', component: _deputation_in_deputation_in_component__WEBPACK_IMPORTED_MODULE_2__["DeputationInComponent"],
            }, {
                path: 'deputationout', component: _deputation_out_deputation_out_component__WEBPACK_IMPORTED_MODULE_3__["DeputationOutComponent"],
            },
            {
                path: 'repatriationdetails', component: _repatriation_details_repatriation_details_component__WEBPACK_IMPORTED_MODULE_4__["RepatriationDetailsComponent"],
            },
        ]
    }
];
var DeputationRoutingModule = /** @class */ (function () {
    function DeputationRoutingModule() {
    }
    DeputationRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DeputationRoutingModule);
    return DeputationRoutingModule;
}());



/***/ }),

/***/ "./src/app/deputation/deputation.module.ts":
/*!*************************************************!*\
  !*** ./src/app/deputation/deputation.module.ts ***!
  \*************************************************/
/*! exports provided: DeputationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationModule", function() { return DeputationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _deputation_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deputation-routing.module */ "./src/app/deputation/deputation-routing.module.ts");
/* harmony import */ var _deputation_in_deputation_in_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./deputation-in/deputation-in.component */ "./src/app/deputation/deputation-in/deputation-in.component.ts");
/* harmony import */ var _deputation_out_deputation_out_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./deputation-out/deputation-out.component */ "./src/app/deputation/deputation-out/deputation-out.component.ts");
/* harmony import */ var _repatriation_details_repatriation_details_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./repatriation-details/repatriation-details.component */ "./src/app/deputation/repatriation-details/repatriation-details.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../loanmgt/loanmgt.module */ "./src/app/loanmgt/loanmgt.module.ts");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var DeputationModule = /** @class */ (function () {
    function DeputationModule() {
    }
    DeputationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_deputation_in_deputation_in_component__WEBPACK_IMPORTED_MODULE_3__["DeputationInComponent"], _deputation_out_deputation_out_component__WEBPACK_IMPORTED_MODULE_4__["DeputationOutComponent"], _repatriation_details_repatriation_details_component__WEBPACK_IMPORTED_MODULE_5__["RepatriationDetailsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _deputation_routing_module__WEBPACK_IMPORTED_MODULE_2__["DeputationRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"], _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_9__["MatTooltipModule"], ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_10__["NgxMatSelectSearchModule"], _loanmgt_loanmgt_module__WEBPACK_IMPORTED_MODULE_8__["LoanmgtModule"], _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_11__["SharedModule"]
            ],
            exports: [_deputation_in_deputation_in_component__WEBPACK_IMPORTED_MODULE_3__["DeputationInComponent"]]
        })
    ], DeputationModule);
    return DeputationModule;
}());



/***/ }),

/***/ "./src/app/deputation/repatriation-details/repatriation-details.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/deputation/repatriation-details/repatriation-details.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgorange {\r\n  color: orange;\r\n}\r\n\r\n.bgdarkorange {\r\n  color: darkorange;\r\n}\r\n\r\n.bggreen {\r\n  color: green;\r\n}\r\n\r\n.bgblue {\r\n  color: blue;\r\n}\r\n\r\n.bgred {\r\n  color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVwdXRhdGlvbi9yZXBhdHJpYXRpb24tZGV0YWlscy9yZXBhdHJpYXRpb24tZGV0YWlscy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0Usa0JBQWtCO0NBQ25COztBQUVEO0VBQ0UsYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsV0FBVztDQUNaIiwiZmlsZSI6InNyYy9hcHAvZGVwdXRhdGlvbi9yZXBhdHJpYXRpb24tZGV0YWlscy9yZXBhdHJpYXRpb24tZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnb3JhbmdlIHtcclxuICBjb2xvcjogb3JhbmdlO1xyXG59XHJcblxyXG4uYmdkYXJrb3JhbmdlIHtcclxuICBjb2xvcjogZGFya29yYW5nZTtcclxufVxyXG5cclxuLmJnZ3JlZW4ge1xyXG4gIGNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuLmJnYmx1ZSB7XHJcbiAgY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5iZ3JlZCB7XHJcbiAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/deputation/repatriation-details/repatriation-details.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/deputation/repatriation-details/repatriation-details.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<app-comman-mst (onchange)=\"GetcommanMethod($event)\" [events]=\"eventsSubject.asObservable()\"></app-comman-mst>\r\n<form (ngSubmit)=\"form.valid && onSubmit()\" [formGroup]=\"form\" #formDirective=\"ngForm\" novalidate>\r\n  <div class=\"col-md-12 col-lg-7\" [attr.disabled]=\"disableflag\">\r\n    <mat-card class=\"example-card\">\r\n      <ng-template matStepLabel>Repatriation Details</ng-template>\r\n\r\n      <div class=\"fom-title\">Repatriation Details</div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n          <mat-select placeholder=\"Deputation Type\" formControlName=\"repatDeputedTypeId\" required [errorStateMatcher]=\"matcher\">\r\n\r\n            <mat-option *ngFor=\"let deputationType of deputationTypes\" [value]=\"deputationType.cddirCodeValue\">\r\n              {{deputationType.cddirCodeText}}\r\n            </mat-option>\r\n\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['repatDeputedTypeId'].errors?.required\">\r\n            Deputation Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n\r\n\r\n          <mat-select placeholder=\"Service Type\" formControlName=\"repatServiceTypeId\" required [errorStateMatcher]=\"matcher\">\r\n\r\n            <mat-option *ngFor=\"let serviceType of serviceType\" [value]=\"serviceType.serviceValue\">\r\n              {{serviceType.serviceText}}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error *ngIf=\"form.controls['repatServiceTypeId'].errors?.required\">\r\n            Service Type is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Repatriation Order No\" formControlName=\"repatOrderNo\" maxlength=\"50\" pattern=\"[A-Za-z1-9]{1}|[A-Za-z][A-Za-z0-9-/_]+\" required [errorStateMatcher]=\"matcher\">\r\n\r\n          <mat-error *ngIf=\"form.controls['repatOrderNo'].errors?.required\">\r\n            Repatriation Order Number is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['repatOrderNo'].errors?.pattern\">\r\n            Repatriation Order Number is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"repatOrderDate\" placeholder=\"Repatriation  Order Date \" (dateChange)=\"Checkdate()\" (click)=\"repatOrderDate.open()\"   formControlName=\"repatOrderDate\" [min]=\"form.controls['depuInDate'].value\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"repatOrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #repatOrderDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['repatOrderDate'].errors?.required\">\r\n            Repatriation  Order Date is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"repatEffecDate\" placeholder=\"Repatriation Effective Date \" (click)=\"repatEffecDate.open()\" formControlName=\"repatEffecDate\" [min]=\"form.controls['repatOrderDate'].value\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"repatEffecDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #repatEffecDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['repatEffecDate'].errors?.required\">\r\n            Repatriation  Effective Date is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"repatRelievingDate\" placeholder=\"Repatriation Relieving Date \" (click)=\"repatRelievingDate.open()\" formControlName=\"repatRelievingDate\" [min]=\"form.controls['repatOrderDate'].value\" readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"repatRelievingDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #repatRelievingDate></mat-datepicker>\r\n          <mat-error *ngIf=\"form.controls['repatRelievingDate'].errors?.required\">\r\n            Repatriation Relieving Date  is Required\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-placeholder [ngClass]=\"flag ? 'actplacholder' : 'dsbplaceholder'\"> Remark (If Any) </mat-placeholder>\r\n\r\n          <textarea matInput formControlName=\"repatRemarks\" maxlength=\"100\" pattern=\"[A-Za-z0-9-/ ]+\"></textarea>\r\n          <mat-error *ngIf=\"form.controls['repatRemarks'].errors?.required\">\r\n            Remark (If Any) is Required\r\n          </mat-error>\r\n          <mat-error *ngIf=\"form.controls['repatRemarks'].errors?.pattern\">\r\n            Remark (If Any) is invalid\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\" *ngIf=\"form.controls.rejectionRemark.value\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput formControlName=\"rejectionRemark\" rows=\"2\" placeholder=\"Checker Remarks\" readonly></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\" *ngIf=\"false\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"depuInDate\" placeholder=\"Repatriation Effective Date \" formControlName=\"depuInDate\"  readonly required [errorStateMatcher]=\"matcher\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"repatEffecDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #repatEffecDate></mat-datepicker>\r\n          \r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"roleId == 6\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"btndisabled\">{{btnUpdatetext}}</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetForm();\">Cancel</button>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"forwardStatusUpdate('0');\" [disabled]=\"disableForwardflag\">Forward to DDO Checker</button>\r\n\r\n      </div>\r\n      <div *ngIf=\"roleId == 5\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n\r\n        <button type=\"button\" class=\"btn btn-success\" (click)=\"ApprovePopup = !ApprovePopup\" [disabled]=\"!infoScreen\">Accept</button>\r\n        <button type=\"button\" class=\"btn btn-warning\" (click)=\"RejectPopup = !RejectPopup\" [disabled]=\"!infoScreen\">Reject</button>\r\n\r\n        <!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToMakerPopup = !ForwardToMakerPopup\">Forward To Maker</button>-->\r\n      </div>\r\n\r\n\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n<!--right table-->\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <!--<mat-accordion>\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header>\r\n          <mat-panel-title>\r\n            Form value\r\n          </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <code>\r\n          {{form.value | json}}\r\n        </code>\r\n      </mat-expansion-panel>\r\n    </mat-accordion>-->\r\n\r\n\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"srNo\">\r\n\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Sr.No. </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element; let i = index;\"> {{i+1}}  </td>\r\n      </ng-container>\r\n\r\n      <!-- Date Of Birth Column -->\r\n      <ng-container matColumnDef=\"deputeFrom\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Deputed Type </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.repatDepuTypeName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"orderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>  Order No. </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.repatOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Status\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <label *ngIf=\"element.veriFlag == 'E'\" class=\"bgorange\">Entry</label>\r\n          <label *ngIf=\"element.veriFlag == 'U'\" class=\"bgdarkorange\">Entry Updated</label>\r\n          <label *ngIf=\"element.veriFlag == 'F'\" class=\"bggreen\">Sent to Checker</label>\r\n          <label *ngIf=\"element.veriFlag == 'V'\" class=\"bgblue\">Verified</label>\r\n          <label *ngIf=\"element.veriFlag == 'R'\" class=\"bgred\">Rejected</label>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        \r\n        <td *matCellDef=\"let element\">\r\n\r\n          <a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'N' || element.veriFlag == 'R' || element.veriFlag == 'U')&& roleId == 6\" type=\"button\" class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"btnEditClick(element.empDeputDetailsId)\">edit</a>\r\n          <!--<a *ngIf=\"(element.veriFlag == 'E' || element.veriFlag == 'U')\" class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteEOSDetails(element);DeletePopup = !DeletePopup\">\r\n    delete_forever\r\n  </a>-->\r\n          <a *ngIf=\"(element.veriFlag != 'E' && element.veriFlag != 'N'  && element.veriFlag != 'R' && element.veriFlag != 'U') && roleId == 6\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n          <a *ngIf=\"roleId == 5\" class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"btnInfoClick(element.empDeputDetailsId)\">\r\n            info\r\n          </a>\r\n        </td>\r\n\r\n      </ng-container>\r\n      <tr *ngIf=\"dataSource?.length> 0\">\r\n        <td colspan=\"3\" class=\"no-data-available\">No data!</td>\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\"></tr>\r\n    </table>\r\n\r\n\r\n  </div>\r\n\r\n</div>\r\n<!--<app-dialog [(visible)]=\"DeletePopup\">\r\n  <div class=\"card-content panel panel-body panel-default \">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Delete?</h3>\r\n\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"confirmDelete()\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>-->\r\n\r\n\r\n<app-dialog [(visible)]=\"ApprovePopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Approve?</h3>\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('V')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"RejectPopup\">\r\n  <form [formGroup]=\"RejectPopupForm\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure want to Reject? <br /> <br /></h3>\r\n      <div class=\"col-md-3\"></div>\r\n      <mat-form-field class=\"col-md-6 card-title text-center\">\r\n        <textarea matInput spellcheck=\"false\" [(ngModel)]=\"rejectionRemark\" formControlName=\"rejectionRemark1\" rows=\"2\" maxlength=\"100\" align=\"center\" placeholder=\"Remarks\" required inputmode=\"full-width-latin\"></textarea>\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.required\">Remarks is Required </mat-error>\r\n\r\n        <mat-error *ngIf=\"RejectPopupForm.controls['rejectionRemark1'].errors?.pattern\">Remarks is Invalid</mat-error>\r\n\r\n\r\n\r\n      </mat-form-field>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n\r\n        <button type=\"submit\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('R')\">Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n<!--<app-dialog [(visible)]=\"RejectPopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure want to Reject?</h3>\r\n    <mat-form-field class=\"wid-100\">\r\n      <textarea matInput [(ngModel)]=\"rejectionRemark\" rows=\"2\" maxlength=\"50\" placeholder=\"Remarks\"></textarea>\r\n      <mat-error>Remarks Required !</mat-error>\r\n    </mat-form-field>\r\n    <div class=\"form-group text-center col-md-12\">\r\n\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"forwardStatusUpdate('R')\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>-->\r\n"

/***/ }),

/***/ "./src/app/deputation/repatriation-details/repatriation-details.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/deputation/repatriation-details/repatriation-details.component.ts ***!
  \***********************************************************************************/
/*! exports provided: RepatriationDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepatriationDetailsComponent", function() { return RepatriationDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_master_master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/master/master.service */ "./src/app/services/master/master.service.ts");
/* harmony import */ var _services_deputation_repatriationdetails_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/deputation/repatriationdetails.service */ "./src/app/services/deputation/repatriationdetails.service.ts");
/* harmony import */ var _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/deputation/deputationin.service */ "./src/app/services/deputation/deputationin.service.ts");
/* harmony import */ var _global_common_msg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../global/common-msg */ "./src/app/global/common-msg.ts");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../global/error-state-matcher */ "./src/app/global/error-state-matcher.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RepatriationDetailsComponent = /** @class */ (function () {
    function RepatriationDetailsComponent(_formBuilder, master, repatriationDetailsService, snackBar, deputationinService, _message) {
        this._formBuilder = _formBuilder;
        this.master = master;
        this.repatriationDetailsService = repatriationDetailsService;
        this.snackBar = snackBar;
        this.deputationinService = deputationinService;
        this._message = _message;
        this.deputationTypes = [];
        this.serviceType = [];
        this.disableflag = true;
        this.disableForwardflag = true;
        this.eventsSubject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.flag = false;
        this.DeletePopup = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.infoScreen = false;
        this.btndisabled = false;
        this.rejectionRemark = '';
        this.displayedColumns2 = ['srNo', 'deputeFrom', 'orderNo', 'Status', 'action'];
        this.submitted = false;
        this.matcher = new _global_error_state_matcher__WEBPACK_IMPORTED_MODULE_8__["MyErrorStateMatcher"]();
    }
    RepatriationDetailsComponent.prototype.ngOnInit = function () {
        this.FormDeatils();
        this.RejectPopupcreateForm();
        this.btnUpdatetext = 'Save';
        this.roleId = sessionStorage.getItem('userRoleID');
        this.savebuttonstatus = true;
        this.getDeputationType();
        this.getServiceType();
        this.btndisabled = true;
        this.form.disable();
        this.flag = true;
    };
    RepatriationDetailsComponent.prototype.GetcommanMethod = function (value) {
        this.formDirective.resetForm();
        this.btnUpdatetext = 'Save';
        this.empCd = value;
        if (value == "") {
            this.btndisabled = true;
            this.dataSource = null;
            this.form.disable();
        }
        else {
            this.getRepatriationDeputaionDetails(this.empCd, this.roleId);
            this.getCheckDuputationInEmployee(this.empCd, this.roleId);
        }
        //this.getLoanDetails(value);
        this.form.get('employeeCode').setValue(this.empCd);
    };
    RepatriationDetailsComponent.prototype.getCheckDuputationInEmployee = function (Empcd, roleId) {
        var _this = this;
        debugger;
        this.repatriationDetailsService.getCheckDuputationInEmployee(Empcd, roleId).subscribe(function (result) {
            debugger;
            //if (result.length != 0) {
            if (result.deputationEffectiveDate == null) {
                _this.formDirective.resetForm();
                _this.btndisabled = true;
                _this.form.get('employeeCode').setValue(_this.empCd);
                _this.dataSource = null;
                _this.form.disable();
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()("Employee Is Not Eligible For Repatriation");
            }
            else {
                _this.form.get('depuInDate').setValue(result.deputationEffectiveDate);
                _this.depuInDate = result.deputationEffectiveDate;
            }
            //}
        });
    };
    RepatriationDetailsComponent.prototype.getRepatriationDeputaionDetails = function (Empcd, roleId) {
        var _this = this;
        debugger;
        this.repatriationDetailsService.getRepatriationDeputaionDetails(Empcd, roleId).subscribe(function (result) {
            debugger;
            if (result.length == 0) {
                _this.formDirective.resetForm();
                _this.form.enable();
                _this.dataSource = null;
                _this.infoScreen = false;
                _this.form.get('employeeCode').setValue(_this.empCd);
                _this.btndisabled = false;
                _this.flag = true;
                if (_this.roleId == '5') {
                    _this.form.disable();
                }
            }
            else {
                debugger;
                _this.dataSource = result;
                _this.form.patchValue(result[0]);
                _this.form.disable();
                _this.infoScreen = true;
                _this.btndisabled = true;
                _this.flag = false;
                if (result[0].veriFlag == 'E' || result[0].veriFlag == 'U') {
                    _this.disableForwardflag = false;
                }
                else {
                    _this.disableForwardflag = true;
                }
            }
            if (_this.roleId == '5' && (result[0].veriFlag == 'V' || result[0].veriFlag == 'R')) {
                _this.infoScreen = false;
            }
        });
    };
    RepatriationDetailsComponent.prototype.Checkdate = function () {
        if (this.form.get('repatOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('repatEffecDate').value || this.form.get('repatOrderDate').value > this.form.get('repatEffecDate').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('repatEffecDate').setValue("");
        }
        if (this.form.get('repatOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('repatRelievingDate').value || this.form.get('repatOrderDate').value > this.form.get('repatRelievingDate').value) {
            //this.form.get('deputation_Effective_Date').value
            this.form.get('repatRelievingDate').setValue("");
        }
        //alert(dd)
    };
    RepatriationDetailsComponent.prototype.RejectPopupcreateForm = function () {
        this.RejectPopupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            rejectionRemark1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
        });
    };
    RepatriationDetailsComponent.prototype.btnEditClick = function (empDeputId) {
        debugger;
        this.formDirective.resetForm();
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.form.patchValue(value);
        this.form.get('depuInDate').setValue(this.depuInDate);
        this.form.enable();
        this.flag = true;
        if (value.veriFlag == 'N') {
            this.btnUpdatetext = 'Save';
        }
        else {
            this.btnUpdatetext = 'Update';
        }
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.btndisabled = false;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.btndisabled = false;
            this.disableflag = true;
            if (value.veriFlag == 'N') {
                this.disableForwardflag = true;
            }
            else {
                this.disableForwardflag = true;
            }
            //this.Btntxt = 'Save';
        }
    };
    RepatriationDetailsComponent.prototype.btnInfoClick = function (empDeputId) {
        var value = this.dataSource.filter(function (x) { return x.empDeputDetailsId == empDeputId; })[0];
        this.form.patchValue(value);
        this.form.disable();
        this.form.get('depuInDate').setValue(this.depuInDate);
        //let empStatus = value.veriFlag;
        if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
            this.infoScreen = false;
            debugger;
            this.disableflag = true;
            this.disableForwardflag = true;
        }
        else {
            this.infoScreen = true;
            debugger;
            this.disableForwardflag = true;
            //this.Btntxt = 'Save';
        }
    };
    RepatriationDetailsComponent.prototype.getDeputationType = function () {
        var _this = this;
        this.master.getDeputationType().subscribe(function (res) {
            debugger;
            _this.deputationTypes = res;
        });
    };
    RepatriationDetailsComponent.prototype.getServiceType = function () {
        var _this = this;
        this.master.getServiceType().subscribe(function (res) {
            debugger;
            _this.serviceType = res;
        });
    };
    RepatriationDetailsComponent.prototype.ResetForm = function () {
        debugger;
        this.eventsSubject.next(this.data);
        this.form.reset();
        this.formDirective.resetForm();
        this.btnUpdatetext = 'Save';
        this.disableForwardflag = true;
        this.btndisabled = true;
        this.dataSource = null;
        this.form.disable();
        this.flag = true;
        // this.formDirective.resetForm();
        //// this.form.enable();
        // this.btnUpdatetext = 'Save';
        // this.disableForwardflag = true;
        // this.form.get('employeeCode').setValue(this.empCd.trim());
        // let value = this.dataSource[0];
        // if (this.dataSource[0].veriFlag == 'E' || this.dataSource[0].veriFlag == 'U') {
        //   this.disableForwardflag = false;
        // }
        // else {
        //   this.disableForwardflag = true;
        // }
        // this.form.patchValue(value);
        // this.form.disable();
    };
    RepatriationDetailsComponent.prototype.Cancel = function () {
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.DeletePopup = false;
    };
    RepatriationDetailsComponent.prototype.FormDeatils = function () {
        this.form = this._formBuilder.group({
            'repatDeputedTypeId': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatDepuTypeName': [null],
            'repatServiceTypeId': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatOrderNo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatOrderDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatEffecDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatRelievingDate': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'repatRemarks': [null],
            'employeeCode': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'empDeputDetailsId': [null],
            'flagUpdate': [null],
            'rejectionRemark': [null],
            'depuInDate': [null],
        });
    };
    RepatriationDetailsComponent.prototype.forwardStatusUpdate = function (statusFlag) {
        var _this = this;
        var RejectionComment = '';
        if (this.RejectPopupForm.invalid && statusFlag == 'R') {
            this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
            return false;
        }
        else {
            var empDeputDetailsId = this.form.get('empDeputDetailsId').value;
            if (statusFlag == 'R') {
                RejectionComment = this.rejectionRemark;
            }
            else {
                RejectionComment = '';
            }
            debugger;
            this.deputationinService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(function (result1) {
                if (parseInt(result1) >= 1) {
                    if (statusFlag == 0) {
                        _this.snackBar.open(_this._message.forwardDDOCheckerMsg, null, { duration: 4000 });
                    }
                    if (statusFlag == 'V') {
                        _this.snackBar.open(_this._message.VerifyedByChecker, null, { duration: 4000 });
                    }
                    if (statusFlag == 'R') {
                        _this.snackBar.open(_this._message.RejectedByChecker, null, { duration: 4000 });
                    }
                    _this.form.reset();
                    _this.formDirective.resetForm();
                    _this.getRepatriationDeputaionDetails(_this.empCd, _this.roleId);
                    _this.btnUpdatetext = 'Save';
                    _this.infoScreen = false;
                    _this.ApprovePopup = false;
                    _this.RejectPopup = false;
                    _this.rejectionRemark = '';
                }
            });
        }
    };
    RepatriationDetailsComponent.prototype.onSubmit = function () {
        var _this = this;
        debugger;
        this.submitted = true;
        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
        else {
            if (this.btnUpdatetext == 'Update') {
                this.form.get('flagUpdate').setValue('update');
            }
            else {
                this.form.get('flagUpdate').setValue('insert');
            }
            this.repatriationDetailsService.InsertUpateRepatriationDetails(this.form.value).subscribe(function (result1) {
                var resultvalue = result1.split("-");
                if (parseInt(resultvalue[0]) >= 1) {
                    if (_this.btnUpdatetext == 'Update') {
                        _this.snackBar.open(_this._message.updateMsg, null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getRepatriationDeputaionDetails(_this.empCd, _this.roleId);
                        _this.btnUpdatetext = 'Save';
                        _this.disableForwardflag = false;
                    }
                    else {
                        _this.snackBar.open(_this._message.saveMsg, null, { duration: 4000 });
                        _this.form.reset();
                        _this.formDirective.resetForm();
                        _this.getRepatriationDeputaionDetails(_this.empCd, _this.roleId);
                        _this.btnUpdatetext == 'Save';
                        _this.disableForwardflag = true;
                    }
                }
                else {
                    if (_this.btnUpdatetext == 'Update') {
                        if (resultvalue[2] = 'Duplicate Record') {
                            _this.snackBar.open(_this._message.depRepaAlreadyExits, null, { duration: 4000 });
                        }
                        else {
                            _this.snackBar.open(_this._message.updateFailedMsg, null, { duration: 4000 });
                        }
                    }
                    else if (resultvalue[2] = 'Duplicate Record') {
                        _this.snackBar.open(_this._message.depRepaAlreadyExits, null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open(_this._message.saveFailedMsg, null, { duration: 4000 });
                    }
                }
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('formDirective'),
        __metadata("design:type", Object)
    ], RepatriationDetailsComponent.prototype, "formDirective", void 0);
    RepatriationDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-repatriation-details',
            template: __webpack_require__(/*! ./repatriation-details.component.html */ "./src/app/deputation/repatriation-details/repatriation-details.component.html"),
            styles: [__webpack_require__(/*! ./repatriation-details.component.css */ "./src/app/deputation/repatriation-details/repatriation-details.component.css")],
            providers: [_global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_master_master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"], _services_deputation_repatriationdetails_service__WEBPACK_IMPORTED_MODULE_4__["RepatriationdetailsService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _services_deputation_deputationin_service__WEBPACK_IMPORTED_MODULE_5__["DeputationinService"], _global_common_msg__WEBPACK_IMPORTED_MODULE_6__["CommonMsg"]])
    ], RepatriationDetailsComponent);
    return RepatriationDetailsComponent;
}());



/***/ }),

/***/ "./src/app/global/error-state-matcher.ts":
/*!***********************************************!*\
  !*** ./src/app/global/error-state-matcher.ts ***!
  \***********************************************/
/*! exports provided: MyErrorStateMatcher */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyErrorStateMatcher", function() { return MyErrorStateMatcher; });
/** Error when invalid control is dirty, touched, or submitted. */
var MyErrorStateMatcher = /** @class */ (function () {
    function MyErrorStateMatcher() {
    }
    MyErrorStateMatcher.prototype.isErrorState = function (control, form) {
        var isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    };
    return MyErrorStateMatcher;
}());



/***/ }),

/***/ "./src/app/model/DeptationModel/depuationin-model.ts":
/*!***********************************************************!*\
  !*** ./src/app/model/DeptationModel/depuationin-model.ts ***!
  \***********************************************************/
/*! exports provided: Deputation_Deduction, SecemeCode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Deputation_Deduction", function() { return Deputation_Deduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecemeCode", function() { return SecemeCode; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

var Deputation_Deduction = /** @class */ (function () {
    function Deputation_Deduction() {
    }
    Deputation_Deduction.asFormGroup = function (deputation_Deduction) {
        var fg = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroup"]({
            payItemsDeductionName: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.payItemsDeductionName, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required),
            payItemsDeductionAccCdScheme: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.payItemsDeductionAccCdScheme, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required),
            deduction_Schedule: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.deduction_Schedule, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required),
            communication_Address: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.communication_Address, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required),
            payItemsCd: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.payItemsCd),
            msEmpDuesId: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](deputation_Deduction.msEmpDuesId)
        });
        return fg;
    };
    return Deputation_Deduction;
}());

var SecemeCode = /** @class */ (function () {
    function SecemeCode() {
    }
    return SecemeCode;
}());

//export class Designation {
//  desigId: string;
//  desigDesc: string;
//}
//export interface CommunicationAddress {
//  id: number;
//  username: string;
//}
//export interface State {
//  stateId: number;
//  stateName: string
//}


/***/ }),

/***/ "./src/app/services/deputation/deputationin.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/deputation/deputationin.service.ts ***!
  \*************************************************************/
/*! exports provided: DeputationinService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationinService", function() { return DeputationinService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var DeputationinService = /** @class */ (function () {
    function DeputationinService(http, config) {
        this.http = http;
        this.config = config;
    }
    DeputationinService.prototype.GetPayItems = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getPayItems);
    };
    DeputationinService.prototype.GetAllDesignation = function (controllerId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getDesignationAll + controllerId));
    };
    DeputationinService.prototype.GetCommunicationAddress = function () {
        return this.http.get("" + this.config.api_base_url + this.config.getDepuatationCommunticationAddress);
    };
    DeputationinService.prototype.GetDeductionDetails = function (EmpCd, roleId) {
        debugger;
        return this.http.get("" + this.config.api_base_url + (this.config.getDeductionScheduleDetailsbyEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (deputation_Deduction) {
            debugger;
            return deputation_Deduction;
        }));
    };
    DeputationinService.prototype.getAllAsFormArray2 = function (EmpCd, roleId) {
        debugger;
        return this.http.get("" + this.config.api_base_url + (this.config.getDeductionScheduleDetailsbyEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (deputation_Deduction) {
            debugger;
            return deputation_Deduction;
        }));
    };
    DeputationinService.prototype.GetScheme_Code = function (MsEmpDuesID) {
        var id = JSON.stringify(MsEmpDuesID);
        debugger;
        return this.http.get("" + this.config.api_base_url + (this.config.getScheme_CodeByMsEmpDuesID + id))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (schemecode) {
            debugger;
            return schemecode;
        }));
    };
    DeputationinService.prototype.InsertUpateDeputationDetails = function (deputationdetails) {
        return this.http.post(this.config.api_base_url + this.config.saveUpdateDeputationInDetails, deputationdetails);
    };
    DeputationinService.prototype.getDeputaionDetails = function (EmpCd, roleId) {
        return this.GetDeductionDetails(EmpCd, roleId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (deputation_Deduction) {
            debugger;
            return deputation_Deduction;
        }));
    };
    DeputationinService.prototype.forwardStatusUpdate = function (empDeputDetailsId, status, rejectionRemark) {
        return this.http.post(this.config.api_base_url + this.config.updateforwardStatusUpdateByDepID + '?empDeputDetailsId= ' + empDeputDetailsId + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' });
    };
    DeputationinService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], DeputationinService);
    return DeputationinService;
}());



/***/ }),

/***/ "./src/app/services/deputation/deputationout.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/deputation/deputationout.service.ts ***!
  \**************************************************************/
/*! exports provided: DeputationoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeputationoutService", function() { return DeputationoutService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DeputationoutService = /** @class */ (function () {
    function DeputationoutService(http, config) {
        this.http = http;
        this.config = config;
    }
    DeputationoutService.prototype.getAllDepuatationOutDetails = function (EmpCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getAllDepuatationOutDetailsByEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId));
    };
    DeputationoutService.prototype.InsertUpateDeputationOutDetails = function (deputationoutdetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateDeputationOutDetails, deputationoutdetails, { responseType: 'text' });
    };
    DeputationoutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], DeputationoutService);
    return DeputationoutService;
}());



/***/ }),

/***/ "./src/app/services/deputation/repatriationdetails.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/services/deputation/repatriationdetails.service.ts ***!
  \********************************************************************/
/*! exports provided: RepatriationdetailsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepatriationdetailsService", function() { return RepatriationdetailsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var RepatriationdetailsService = /** @class */ (function () {
    function RepatriationdetailsService(http, config) {
        this.http = http;
        this.config = config;
    }
    RepatriationdetailsService.prototype.getRepatriationDeputaionDetails = function (EmpCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getAllRepatriationDetailsByEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId));
    };
    RepatriationdetailsService.prototype.getCheckDuputationInEmployee = function (EmpCd, roleId) {
        return this.http.get("" + this.config.api_base_url + (this.config.getCheckDuputationInEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId));
    };
    RepatriationdetailsService.prototype.InsertUpateRepatriationDetails = function (repatriationDetails) {
        debugger;
        return this.http.post(this.config.api_base_url + this.config.saveUpdateRepatriationDetails, repatriationDetails, { responseType: 'text' });
    };
    RepatriationdetailsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], RepatriationdetailsService);
    return RepatriationdetailsService;
}());



/***/ }),

/***/ "./src/app/services/empdetails/getempcode.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/empdetails/getempcode.service.ts ***!
  \***********************************************************/
/*! exports provided: GetempcodeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetempcodeService", function() { return GetempcodeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GetempcodeService = /** @class */ (function () {
    function GetempcodeService() {
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    GetempcodeService.prototype.SendEmpCode = function (commonEmpCode, matselectedIndex) {
        this.subject.next({ empcode: commonEmpCode, selectedIndex: matselectedIndex });
    };
    GetempcodeService.prototype.getEmpCode = function () {
        return this.subject.asObservable();
    };
    GetempcodeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], GetempcodeService);
    return GetempcodeService;
}());



/***/ })

}]);
//# sourceMappingURL=default~deputation-deputation-module~employee-employee-module.js.map