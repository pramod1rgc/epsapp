(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["leaves-mgmt-leaves-mgmt-module"],{

/***/ "./node_modules/ng2-order-pipe/dist/index.js":
/*!***************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
/**
 * Created by vadimdez on 20/01/2017.
 */
__export(__webpack_require__(/*! ./src/ng2-order.module */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js"));
__export(__webpack_require__(/*! ./src/ng2-order.pipe */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js":
/*!******************************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/src/ng2-order.module.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by vadimdez on 20/01/2017.
 */
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ng2_order_pipe_1 = __webpack_require__(/*! ./ng2-order.pipe */ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js");
var Ng2OrderModule = (function () {
    function Ng2OrderModule() {
    }
    return Ng2OrderModule;
}());
Ng2OrderModule = __decorate([
    core_1.NgModule({
        declarations: [ng2_order_pipe_1.Ng2OrderPipe],
        exports: [ng2_order_pipe_1.Ng2OrderPipe]
    })
], Ng2OrderModule);
exports.Ng2OrderModule = Ng2OrderModule;
//# sourceMappingURL=ng2-order.module.js.map

/***/ }),

/***/ "./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js":
/*!****************************************************************!*\
  !*** ./node_modules/ng2-order-pipe/dist/src/ng2-order.pipe.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var Ng2OrderPipe = Ng2OrderPipe_1 = (function () {
    function Ng2OrderPipe() {
    }
    Ng2OrderPipe.prototype.transform = function (value, expression, reverse) {
        if (!value) {
            return value;
        }
        var isArray = value instanceof Array;
        if (isArray) {
            return this.sortArray(value, expression, reverse);
        }
        if (typeof value === 'object') {
            return this.transformObject(value, expression, reverse);
        }
        return value;
    };
    /**
     * Sort array
     *
     * @param value
     * @param expression
     * @param reverse
     * @returns {any[]}
     */
    Ng2OrderPipe.prototype.sortArray = function (value, expression, reverse) {
        var array = value.sort(function (a, b) {
            if (!expression) {
                return a > b ? 1 : -1;
            }
            return a[expression] > b[expression] ? 1 : -1;
        });
        if (reverse) {
            return array.reverse();
        }
        return array;
    };
    /**
     * Transform Object
     *
     * @param value
     * @param expression
     * @param reverse
     * @returns {any[]}
     */
    Ng2OrderPipe.prototype.transformObject = function (value, expression, reverse) {
        var parsedExpression = Ng2OrderPipe_1.parseExpression(expression);
        var lastPredicate = parsedExpression.pop();
        var oldValue = Ng2OrderPipe_1.getValue(value, parsedExpression);
        if (!(oldValue instanceof Array)) {
            parsedExpression.push(lastPredicate);
            lastPredicate = null;
            oldValue = Ng2OrderPipe_1.getValue(value, parsedExpression);
        }
        if (!oldValue) {
            return value;
        }
        var newValue = this.transform(oldValue, lastPredicate, reverse);
        Ng2OrderPipe_1.setValue(value, newValue, parsedExpression);
        return value;
    };
    /**
     * Parse expression, split into items
     * @param expression
     * @returns {string[]}
     */
    Ng2OrderPipe.parseExpression = function (expression) {
        expression = expression.replace(/\[(\w+)\]/g, '.$1');
        expression = expression.replace(/^\./, '');
        return expression.split('.');
    };
    /**
     * Get value by expression
     *
     * @param object
     * @param expression
     * @returns {any}
     */
    Ng2OrderPipe.getValue = function (object, expression) {
        for (var i = 0, n = expression.length; i < n; ++i) {
            var k = expression[i];
            if (!(k in object)) {
                return;
            }
            object = object[k];
        }
        return object;
    };
    /**
     * Set value by expression
     *
     * @param object
     * @param value
     * @param expression
     */
    Ng2OrderPipe.setValue = function (object, value, expression) {
        var i;
        for (i = 0; i < expression.length - 1; i++) {
            object = object[expression[i]];
        }
        object[expression[i]] = value;
    };
    return Ng2OrderPipe;
}());
Ng2OrderPipe = Ng2OrderPipe_1 = __decorate([
    core_1.Pipe({
        name: 'orderBy'
    })
], Ng2OrderPipe);
exports.Ng2OrderPipe = Ng2OrderPipe;
var Ng2OrderPipe_1;
//# sourceMappingURL=ng2-order.pipe.js.map

/***/ }),

/***/ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js":
/*!*****************************************************************!*\
  !*** ./node_modules/ng2-search-filter/ng2-search-filter.es5.js ***!
  \*****************************************************************/
/*! exports provided: Ng2SearchPipeModule, Ng2SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipeModule", function() { return Ng2SearchPipeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipe", function() { return Ng2SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var Ng2SearchPipe = (function () {
    function Ng2SearchPipe() {
    }
    /**
     * @param {?} items object from array
     * @param {?} term term's search
     * @return {?}
     */
    Ng2SearchPipe.prototype.transform = function (items, term) {
        if (!term || !items)
            return items;
        return Ng2SearchPipe.filter(items, term);
    };
    /**
     *
     * @param {?} items List of items to filter
     * @param {?} term  a string term to compare with every property of the list
     *
     * @return {?}
     */
    Ng2SearchPipe.filter = function (items, term) {
        var /** @type {?} */ toCompare = term.toLowerCase();
        /**
         * @param {?} item
         * @param {?} term
         * @return {?}
         */
        function checkInside(item, term) {
            for (var /** @type {?} */ property in item) {
                if (item[property] === null || item[property] == undefined) {
                    continue;
                }
                if (typeof item[property] === 'object') {
                    if (checkInside(item[property], term)) {
                        return true;
                    }
                }
                if (item[property].toString().toLowerCase().includes(toCompare)) {
                    return true;
                }
            }
            return false;
        }
        return items.filter(function (item) {
            return checkInside(item, term);
        });
    };
    return Ng2SearchPipe;
}());
Ng2SearchPipe.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                name: 'filter',
                pure: false
            },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
Ng2SearchPipe.ctorParameters = function () { return []; };
var Ng2SearchPipeModule = (function () {
    function Ng2SearchPipeModule() {
    }
    return Ng2SearchPipeModule;
}());
Ng2SearchPipeModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [Ng2SearchPipe],
                exports: [Ng2SearchPipe]
            },] },
];
/**
 * @nocollapse
 */
Ng2SearchPipeModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=ng2-search-filter.es5.js.map


/***/ }),

/***/ "./node_modules/ngx-pagination/dist/ngx-pagination.js":
/*!************************************************************!*\
  !*** ./node_modules/ngx-pagination/dist/ngx-pagination.js ***!
  \************************************************************/
/*! exports provided: ɵb, ɵa, NgxPaginationModule, PaginationService, PaginationControlsComponent, PaginationControlsDirective, PaginatePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return DEFAULT_STYLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return DEFAULT_TEMPLATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxPaginationModule", function() { return NgxPaginationModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationService", function() { return PaginationService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationControlsComponent", function() { return PaginationControlsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationControlsDirective", function() { return PaginationControlsDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginatePipe", function() { return PaginatePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var PaginationService = /** @class */ (function () {
    function PaginationService() {
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.instances = {};
        this.DEFAULT_ID = 'DEFAULT_PAGINATION_ID';
    }
    PaginationService.prototype.defaultId = function () { return this.DEFAULT_ID; };
    /**
     * Register a PaginationInstance with this service. Returns a
     * boolean value signifying whether the instance is new or
     * updated (true = new or updated, false = unchanged).
     */
    PaginationService.prototype.register = function (instance) {
        if (instance.id == null) {
            instance.id = this.DEFAULT_ID;
        }
        if (!this.instances[instance.id]) {
            this.instances[instance.id] = instance;
            return true;
        }
        else {
            return this.updateInstance(instance);
        }
    };
    /**
     * Check each property of the instance and update any that have changed. Return
     * true if any changes were made, else return false.
     */
    PaginationService.prototype.updateInstance = function (instance) {
        var changed = false;
        for (var prop in this.instances[instance.id]) {
            if (instance[prop] !== this.instances[instance.id][prop]) {
                this.instances[instance.id][prop] = instance[prop];
                changed = true;
            }
        }
        return changed;
    };
    /**
     * Returns the current page number.
     */
    PaginationService.prototype.getCurrentPage = function (id) {
        if (this.instances[id]) {
            return this.instances[id].currentPage;
        }
    };
    /**
     * Sets the current page number.
     */
    PaginationService.prototype.setCurrentPage = function (id, page) {
        if (this.instances[id]) {
            var instance = this.instances[id];
            var maxPage = Math.ceil(instance.totalItems / instance.itemsPerPage);
            if (page <= maxPage && 1 <= page) {
                this.instances[id].currentPage = page;
                this.change.emit(id);
            }
        }
    };
    /**
     * Sets the value of instance.totalItems
     */
    PaginationService.prototype.setTotalItems = function (id, totalItems) {
        if (this.instances[id] && 0 <= totalItems) {
            this.instances[id].totalItems = totalItems;
            this.change.emit(id);
        }
    };
    /**
     * Sets the value of instance.itemsPerPage.
     */
    PaginationService.prototype.setItemsPerPage = function (id, itemsPerPage) {
        if (this.instances[id]) {
            this.instances[id].itemsPerPage = itemsPerPage;
            this.change.emit(id);
        }
    };
    /**
     * Returns a clone of the pagination instance object matching the id. If no
     * id specified, returns the instance corresponding to the default id.
     */
    PaginationService.prototype.getInstance = function (id) {
        if (id === void 0) { id = this.DEFAULT_ID; }
        if (this.instances[id]) {
            return this.clone(this.instances[id]);
        }
        return {};
    };
    /**
     * Perform a shallow clone of an object.
     */
    PaginationService.prototype.clone = function (obj) {
        var target = {};
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                target[i] = obj[i];
            }
        }
        return target;
    };
    return PaginationService;
}());

var __decorate$1 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var LARGE_NUMBER = Number.MAX_SAFE_INTEGER;
var PaginatePipe = /** @class */ (function () {
    function PaginatePipe(service) {
        this.service = service;
        // store the values from the last time the pipe was invoked
        this.state = {};
    }
    PaginatePipe.prototype.transform = function (collection, args) {
        // When an observable is passed through the AsyncPipe, it will output
        // `null` until the subscription resolves. In this case, we want to
        // use the cached data from the `state` object to prevent the NgFor
        // from flashing empty until the real values arrive.
        if (!(collection instanceof Array)) {
            var _id = args.id || this.service.defaultId();
            if (this.state[_id]) {
                return this.state[_id].slice;
            }
            else {
                return collection;
            }
        }
        var serverSideMode = args.totalItems && args.totalItems !== collection.length;
        var instance = this.createInstance(collection, args);
        var id = instance.id;
        var start, end;
        var perPage = instance.itemsPerPage;
        var emitChange = this.service.register(instance);
        if (!serverSideMode && collection instanceof Array) {
            perPage = +perPage || LARGE_NUMBER;
            start = (instance.currentPage - 1) * perPage;
            end = start + perPage;
            var isIdentical = this.stateIsIdentical(id, collection, start, end);
            if (isIdentical) {
                return this.state[id].slice;
            }
            else {
                var slice = collection.slice(start, end);
                this.saveState(id, collection, slice, start, end);
                this.service.change.emit(id);
                return slice;
            }
        }
        else {
            if (emitChange) {
                this.service.change.emit(id);
            }
            // save the state for server-side collection to avoid null
            // flash as new data loads.
            this.saveState(id, collection, collection, start, end);
            return collection;
        }
    };
    /**
     * Create an PaginationInstance object, using defaults for any optional properties not supplied.
     */
    PaginatePipe.prototype.createInstance = function (collection, config) {
        this.checkConfig(config);
        return {
            id: config.id != null ? config.id : this.service.defaultId(),
            itemsPerPage: +config.itemsPerPage || 0,
            currentPage: +config.currentPage || 1,
            totalItems: +config.totalItems || collection.length
        };
    };
    /**
     * Ensure the argument passed to the filter contains the required properties.
     */
    PaginatePipe.prototype.checkConfig = function (config) {
        var required = ['itemsPerPage', 'currentPage'];
        var missing = required.filter(function (prop) { return !(prop in config); });
        if (0 < missing.length) {
            throw new Error("PaginatePipe: Argument is missing the following required properties: " + missing.join(', '));
        }
    };
    /**
     * To avoid returning a brand new array each time the pipe is run, we store the state of the sliced
     * array for a given id. This means that the next time the pipe is run on this collection & id, we just
     * need to check that the collection, start and end points are all identical, and if so, return the
     * last sliced array.
     */
    PaginatePipe.prototype.saveState = function (id, collection, slice, start, end) {
        this.state[id] = {
            collection: collection,
            size: collection.length,
            slice: slice,
            start: start,
            end: end
        };
    };
    /**
     * For a given id, returns true if the collection, size, start and end values are identical.
     */
    PaginatePipe.prototype.stateIsIdentical = function (id, collection, start, end) {
        var state = this.state[id];
        if (!state) {
            return false;
        }
        var isMetaDataIdentical = state.size === collection.length &&
            state.start === start &&
            state.end === end;
        if (!isMetaDataIdentical) {
            return false;
        }
        return state.slice.every(function (element, index) { return element === collection[start + index]; });
    };
    PaginatePipe = __decorate$1([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'paginate',
            pure: false
        }),
        __metadata("design:paramtypes", [PaginationService])
    ], PaginatePipe);
    return PaginatePipe;
}());

/**
 * The default template and styles for the pagination links are borrowed directly
 * from Zurb Foundation 6: http://foundation.zurb.com/sites/docs/pagination.html
 */
var DEFAULT_TEMPLATE = "\n    <pagination-template  #p=\"paginationApi\"\n                         [id]=\"id\"\n                         [maxSize]=\"maxSize\"\n                         (pageChange)=\"pageChange.emit($event)\"\n                         (pageBoundsCorrection)=\"pageBoundsCorrection.emit($event)\">\n    <ul class=\"ngx-pagination\" \n        role=\"navigation\" \n        [attr.aria-label]=\"screenReaderPaginationLabel\" \n        [class.responsive]=\"responsive\"\n        *ngIf=\"!(autoHide && p.pages.length <= 1)\">\n\n        <li class=\"pagination-previous\" [class.disabled]=\"p.isFirstPage()\" *ngIf=\"directionLinks\"> \n            <a tabindex=\"0\" *ngIf=\"1 < p.getCurrent()\" (keyup.enter)=\"p.previous()\" (click)=\"p.previous()\" [attr.aria-label]=\"previousLabel + ' ' + screenReaderPageLabel\">\n                {{ previousLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </a>\n            <span *ngIf=\"p.isFirstPage()\">\n                {{ previousLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </span>\n        </li> \n\n        <li class=\"small-screen\">\n            {{ p.getCurrent() }} / {{ p.getLastPage() }}\n        </li>\n\n        <li [class.current]=\"p.getCurrent() === page.value\" \n            [class.ellipsis]=\"page.label === '...'\"\n            *ngFor=\"let page of p.pages\">\n            <a tabindex=\"0\" (keyup.enter)=\"p.setCurrent(page.value)\" (click)=\"p.setCurrent(page.value)\" *ngIf=\"p.getCurrent() !== page.value\">\n                <span class=\"show-for-sr\">{{ screenReaderPageLabel }} </span>\n                <span>{{ (page.label === '...') ? page.label : (page.label | number:'') }}</span>\n            </a>\n            <ng-container *ngIf=\"p.getCurrent() === page.value\">\n                <span class=\"show-for-sr\">{{ screenReaderCurrentLabel }} </span>\n                <span>{{ (page.label === '...') ? page.label : (page.label | number:'') }}</span> \n            </ng-container>\n        </li>\n\n        <li class=\"pagination-next\" [class.disabled]=\"p.isLastPage()\" *ngIf=\"directionLinks\">\n            <a tabindex=\"0\" *ngIf=\"!p.isLastPage()\" (keyup.enter)=\"p.next()\" (click)=\"p.next()\" [attr.aria-label]=\"nextLabel + ' ' + screenReaderPageLabel\">\n                 {{ nextLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </a>\n            <span *ngIf=\"p.isLastPage()\">\n                 {{ nextLabel }} <span class=\"show-for-sr\">{{ screenReaderPageLabel }}</span>\n            </span>\n        </li>\n\n    </ul>\n    </pagination-template>\n    ";
var DEFAULT_STYLES = "\n.ngx-pagination {\n  margin-left: 0;\n  margin-bottom: 1rem; }\n  .ngx-pagination::before, .ngx-pagination::after {\n    content: ' ';\n    display: table; }\n  .ngx-pagination::after {\n    clear: both; }\n  .ngx-pagination li {\n    -moz-user-select: none;\n    -webkit-user-select: none;\n    -ms-user-select: none;\n    margin-right: 0.0625rem;\n    border-radius: 0; }\n  .ngx-pagination li {\n    display: inline-block; }\n  .ngx-pagination a,\n  .ngx-pagination button {\n    color: #0a0a0a; \n    display: block;\n    padding: 0.1875rem 0.625rem;\n    border-radius: 0; }\n    .ngx-pagination a:hover,\n    .ngx-pagination button:hover {\n      background: #e6e6e6; }\n  .ngx-pagination .current {\n    padding: 0.1875rem 0.625rem;\n    background: #2199e8;\n    color: #fefefe;\n    cursor: default; }\n  .ngx-pagination .disabled {\n    padding: 0.1875rem 0.625rem;\n    color: #cacaca;\n    cursor: default; } \n    .ngx-pagination .disabled:hover {\n      background: transparent; }\n  .ngx-pagination a, .ngx-pagination button {\n    cursor: pointer; }\n\n.ngx-pagination .pagination-previous a::before,\n.ngx-pagination .pagination-previous.disabled::before { \n  content: '\u00AB';\n  display: inline-block;\n  margin-right: 0.5rem; }\n\n.ngx-pagination .pagination-next a::after,\n.ngx-pagination .pagination-next.disabled::after {\n  content: '\u00BB';\n  display: inline-block;\n  margin-left: 0.5rem; }\n\n.ngx-pagination .show-for-sr {\n  position: absolute !important;\n  width: 1px;\n  height: 1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0); }\n.ngx-pagination .small-screen {\n  display: none; }\n@media screen and (max-width: 601px) {\n  .ngx-pagination.responsive .small-screen {\n    display: inline-block; } \n  .ngx-pagination.responsive li:not(.small-screen):not(.pagination-previous):not(.pagination-next) {\n    display: none; }\n}\n  ";

var __decorate$2 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$1 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
function coerceToBoolean(input) {
    return !!input && input !== 'false';
}
/**
 * The default pagination controls component. Actually just a default implementation of a custom template.
 */
var PaginationControlsComponent = /** @class */ (function () {
    function PaginationControlsComponent() {
        this.maxSize = 7;
        this.previousLabel = 'Previous';
        this.nextLabel = 'Next';
        this.screenReaderPaginationLabel = 'Pagination';
        this.screenReaderPageLabel = 'page';
        this.screenReaderCurrentLabel = "You're on page";
        this.pageChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pageBoundsCorrection = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this._directionLinks = true;
        this._autoHide = false;
        this._responsive = false;
    }
    Object.defineProperty(PaginationControlsComponent.prototype, "directionLinks", {
        get: function () {
            return this._directionLinks;
        },
        set: function (value) {
            this._directionLinks = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationControlsComponent.prototype, "autoHide", {
        get: function () {
            return this._autoHide;
        },
        set: function (value) {
            this._autoHide = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationControlsComponent.prototype, "responsive", {
        get: function () {
            return this._responsive;
        },
        set: function (value) {
            this._responsive = coerceToBoolean(value);
        },
        enumerable: true,
        configurable: true
    });
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "id", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Number)
    ], PaginationControlsComponent.prototype, "maxSize", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "directionLinks", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "autoHide", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", Boolean),
        __metadata$1("design:paramtypes", [Boolean])
    ], PaginationControlsComponent.prototype, "responsive", null);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "previousLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "nextLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderPaginationLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderPageLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$1("design:type", String)
    ], PaginationControlsComponent.prototype, "screenReaderCurrentLabel", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$1("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsComponent.prototype, "pageChange", void 0);
    __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$1("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsComponent.prototype, "pageBoundsCorrection", void 0);
    PaginationControlsComponent = __decorate$2([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pagination-controls',
            template: DEFAULT_TEMPLATE,
            styles: [DEFAULT_STYLES],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        })
    ], PaginationControlsComponent);
    return PaginationControlsComponent;
}());

var __decorate$3 = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata$2 = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * This directive is what powers all pagination controls components, including the default one.
 * It exposes an API which is hooked up to the PaginationService to keep the PaginatePipe in sync
 * with the pagination controls.
 */
var PaginationControlsDirective = /** @class */ (function () {
    function PaginationControlsDirective(service, changeDetectorRef) {
        var _this = this;
        this.service = service;
        this.changeDetectorRef = changeDetectorRef;
        this.maxSize = 7;
        this.pageChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pageBoundsCorrection = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.pages = [];
        this.changeSub = this.service.change
            .subscribe(function (id) {
            if (_this.id === id) {
                _this.updatePageLinks();
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
            }
        });
    }
    PaginationControlsDirective.prototype.ngOnInit = function () {
        if (this.id === undefined) {
            this.id = this.service.defaultId();
        }
        this.updatePageLinks();
    };
    PaginationControlsDirective.prototype.ngOnChanges = function (changes) {
        this.updatePageLinks();
    };
    PaginationControlsDirective.prototype.ngOnDestroy = function () {
        this.changeSub.unsubscribe();
    };
    /**
     * Go to the previous page
     */
    PaginationControlsDirective.prototype.previous = function () {
        this.checkValidId();
        this.setCurrent(this.getCurrent() - 1);
    };
    /**
     * Go to the next page
     */
    PaginationControlsDirective.prototype.next = function () {
        this.checkValidId();
        this.setCurrent(this.getCurrent() + 1);
    };
    /**
     * Returns true if current page is first page
     */
    PaginationControlsDirective.prototype.isFirstPage = function () {
        return this.getCurrent() === 1;
    };
    /**
     * Returns true if current page is last page
     */
    PaginationControlsDirective.prototype.isLastPage = function () {
        return this.getLastPage() === this.getCurrent();
    };
    /**
     * Set the current page number.
     */
    PaginationControlsDirective.prototype.setCurrent = function (page) {
        this.pageChange.emit(page);
    };
    /**
     * Get the current page number.
     */
    PaginationControlsDirective.prototype.getCurrent = function () {
        return this.service.getCurrentPage(this.id);
    };
    /**
     * Returns the last page number
     */
    PaginationControlsDirective.prototype.getLastPage = function () {
        var inst = this.service.getInstance(this.id);
        if (inst.totalItems < 1) {
            // when there are 0 or fewer (an error case) items, there are no "pages" as such,
            // but it makes sense to consider a single, empty page as the last page.
            return 1;
        }
        return Math.ceil(inst.totalItems / inst.itemsPerPage);
    };
    PaginationControlsDirective.prototype.getTotalItems = function () {
        return this.service.getInstance(this.id).totalItems;
    };
    PaginationControlsDirective.prototype.checkValidId = function () {
        if (this.service.getInstance(this.id).id == null) {
            console.warn("PaginationControlsDirective: the specified id \"" + this.id + "\" does not match any registered PaginationInstance");
        }
    };
    /**
     * Updates the page links and checks that the current page is valid. Should run whenever the
     * PaginationService.change stream emits a value matching the current ID, or when any of the
     * input values changes.
     */
    PaginationControlsDirective.prototype.updatePageLinks = function () {
        var _this = this;
        var inst = this.service.getInstance(this.id);
        var correctedCurrentPage = this.outOfBoundCorrection(inst);
        if (correctedCurrentPage !== inst.currentPage) {
            setTimeout(function () {
                _this.pageBoundsCorrection.emit(correctedCurrentPage);
                _this.pages = _this.createPageArray(inst.currentPage, inst.itemsPerPage, inst.totalItems, _this.maxSize);
            });
        }
        else {
            this.pages = this.createPageArray(inst.currentPage, inst.itemsPerPage, inst.totalItems, this.maxSize);
        }
    };
    /**
     * Checks that the instance.currentPage property is within bounds for the current page range.
     * If not, return a correct value for currentPage, or the current value if OK.
     */
    PaginationControlsDirective.prototype.outOfBoundCorrection = function (instance) {
        var totalPages = Math.ceil(instance.totalItems / instance.itemsPerPage);
        if (totalPages < instance.currentPage && 0 < totalPages) {
            return totalPages;
        }
        else if (instance.currentPage < 1) {
            return 1;
        }
        return instance.currentPage;
    };
    /**
     * Returns an array of Page objects to use in the pagination controls.
     */
    PaginationControlsDirective.prototype.createPageArray = function (currentPage, itemsPerPage, totalItems, paginationRange) {
        // paginationRange could be a string if passed from attribute, so cast to number.
        paginationRange = +paginationRange;
        var pages = [];
        var totalPages = Math.ceil(totalItems / itemsPerPage);
        var halfWay = Math.ceil(paginationRange / 2);
        var isStart = currentPage <= halfWay;
        var isEnd = totalPages - halfWay < currentPage;
        var isMiddle = !isStart && !isEnd;
        var ellipsesNeeded = paginationRange < totalPages;
        var i = 1;
        while (i <= totalPages && i <= paginationRange) {
            var label = void 0;
            var pageNumber = this.calculatePageNumber(i, currentPage, paginationRange, totalPages);
            var openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
            var closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));
            if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                label = '...';
            }
            else {
                label = pageNumber;
            }
            pages.push({
                label: label,
                value: pageNumber
            });
            i++;
        }
        return pages;
    };
    /**
     * Given the position in the sequence of pagination links [i],
     * figure out what page number corresponds to that position.
     */
    PaginationControlsDirective.prototype.calculatePageNumber = function (i, currentPage, paginationRange, totalPages) {
        var halfWay = Math.ceil(paginationRange / 2);
        if (i === paginationRange) {
            return totalPages;
        }
        else if (i === 1) {
            return i;
        }
        else if (paginationRange < totalPages) {
            if (totalPages - halfWay < currentPage) {
                return totalPages - paginationRange + i;
            }
            else if (halfWay < currentPage) {
                return currentPage - halfWay + i;
            }
            else {
                return i;
            }
        }
        else {
            return i;
        }
    };
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$2("design:type", String)
    ], PaginationControlsDirective.prototype, "id", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata$2("design:type", Number)
    ], PaginationControlsDirective.prototype, "maxSize", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$2("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsDirective.prototype, "pageChange", void 0);
    __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata$2("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PaginationControlsDirective.prototype, "pageBoundsCorrection", void 0);
    PaginationControlsDirective = __decorate$3([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: 'pagination-template,[pagination-template]',
            exportAs: 'paginationApi'
        }),
        __metadata$2("design:paramtypes", [PaginationService,
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], PaginationControlsDirective);
    return PaginationControlsDirective;
}());

var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var NgxPaginationModule = /** @class */ (function () {
    function NgxPaginationModule() {
    }
    NgxPaginationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [
                PaginatePipe,
                PaginationControlsComponent,
                PaginationControlsDirective
            ],
            providers: [PaginationService],
            exports: [PaginatePipe, PaginationControlsComponent, PaginationControlsDirective]
        })
    ], NgxPaginationModule);
    return NgxPaginationModule;
}());

/**
 * Generated bundle index. Do not edit.
 */




/***/ }),

/***/ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xlYXZlcy1tZ210L2NvbW1vbi1sZWF2ZXMtc2FuY3Rpb24vY29tbW9uLWxlYXZlcy1zYW5jdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-card mat-card\">\r\n  <div class=\"fom-title\">Leave Sanction Details</div>\r\n  <mat-form-field>\r\n    <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n    <i class=\"material-icons icon-right\">search</i>\r\n  </mat-form-field>\r\n\r\n  <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n\r\n\r\n    <!-- Leave Type -->\r\n    <ng-container matColumnDef=\"SanctionOrderNo\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Order No</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.orderNo}}</td>\r\n    </ng-container>\r\n    <!-- From Date -->\r\n    <ng-container matColumnDef=\"SanctionOrderDate\">\r\n      <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.orderDT}} </td>\r\n    </ng-container>\r\n    <!-- To Date Order No. -->\r\n    <ng-container matColumnDef=\"LeaveReason\">\r\n      <th mat-header-cell *matHeaderCellDef>Leave Reason</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.leaveReasonDesc}} </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"LeaveCategory\">\r\n      <th mat-header-cell *matHeaderCellDef>Leave Category</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.leaveCategory}} </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"action\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <div  *ngIf=\"ShowinfoButton\"><a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"DissableLeaves(element.leaveMainId,element.orderNo,element.orderDT,element.leaveCategory,element.leaveReasonCD,element.remarks,element.leaveSanctionDetails)\">error</a></div>\r\n        <div  *ngIf=\"ShoweditButton\"><a  class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditLeaves(element.leaveMainId,element.orderNo,element.orderDT,element.leaveCategory,element.leaveReasonCD,element.remarks,element.leaveSanctionDetails)\">edit</a></div>\r\n        <div  *ngIf=\"ShowdeleteButton\"><a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteLeaves(element.leaveMainId)\"> delete_forever </a></div>\r\n</td>\r\n    </ng-container>\r\n\r\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"rowHighlight(row)\"\r\n        [style.background]=\"highlightedRows.indexOf(row) != -1 ? 'lightgreen' : ''\"></tr>\r\n  </table>\r\n  <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.ts ***!
  \****************************************************************************************/
/*! exports provided: CommonLeavesSanctionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonLeavesSanctionComponent", function() { return CommonLeavesSanctionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ELEMENT_DATA;
var CommonLeavesSanctionComponent = /** @class */ (function () {
    function CommonLeavesSanctionComponent() {
        this.displayedColumns = ['SanctionOrderNo', 'SanctionOrderDate', 'LeaveReason', 'LeaveCategory', 'action'];
        this.highlightedRows = [];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
        this.EditLeavesSanction = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.DissableLeavesSanction = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.DeleteLeavesSanction = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    CommonLeavesSanctionComponent.prototype.ngOnInit = function () {
        //this.dataSource.sort = this.sort;
        //this.dataSource.paginator = this.paginator;
    };
    CommonLeavesSanctionComponent.prototype.ngAfterViewInit = function () {
        //this.dataSource.sort = this.sort;
    };
    CommonLeavesSanctionComponent.prototype.applyFilter = function (filterValue) {
        debugger;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    CommonLeavesSanctionComponent.prototype.EditLeaves = function (leaveMainId, orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails) {
        this.EditLeavesSanction.emit({ leaveMainId: leaveMainId, orderNo: orderNo, orderDT: orderDT, leaveCategory: leaveCategory, leaveReasonCD: leaveReasonCD, remarks: remarks, leaveSanctionDetails: leaveSanctionDetails });
    };
    CommonLeavesSanctionComponent.prototype.DissableLeaves = function (leaveMainId, orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails) {
        this.DissableLeavesSanction.emit({ leaveMainId: leaveMainId, orderNo: orderNo, orderDT: orderDT, leaveCategory: leaveCategory, leaveReasonCD: leaveReasonCD, remarks: remarks, leaveSanctionDetails: leaveSanctionDetails });
        //this.EditLeavesSanction(orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails);
        //this.leaveSanctionCreationForm.disable();
        //this.showCancel = true;
        //this.showForward = true;
        //this.showSave = true;
    };
    CommonLeavesSanctionComponent.prototype.DeleteLeaves = function (value) {
        this.DeleteLeavesSanction.emit(value);
        //this._Service.DeleteLeavesSanction(value).subscribe(result => {
        //  this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
        //  this.ResetLeavesSanction();
        //  this.GetLeavesSanction(this.objlsmain.empCd);
        //})
    };
    CommonLeavesSanctionComponent.prototype.rowHighlight = function (row) {
        this.highlightedRows.pop();
        this.highlightedRows.push(row);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CommonLeavesSanctionComponent.prototype, "dataSource", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], CommonLeavesSanctionComponent.prototype, "ShowinfoButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], CommonLeavesSanctionComponent.prototype, "ShoweditButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], CommonLeavesSanctionComponent.prototype, "ShowdeleteButton", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CommonLeavesSanctionComponent.prototype, "EditLeavesSanction", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CommonLeavesSanctionComponent.prototype, "DissableLeavesSanction", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CommonLeavesSanctionComponent.prototype, "DeleteLeavesSanction", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], CommonLeavesSanctionComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], CommonLeavesSanctionComponent.prototype, "sort", void 0);
    CommonLeavesSanctionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-common-leaves-sanction',
            template: __webpack_require__(/*! ./common-leaves-sanction.component.html */ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.html"),
            styles: [__webpack_require__(/*! ./common-leaves-sanction.component.css */ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CommonLeavesSanctionComponent);
    return CommonLeavesSanctionComponent;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padLeave th, td {padding: 5px 2px 5px 0px;padding-left: 10px!important; }\r\ntextarea {\r\n  max-height: 35px!important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGVhdmVzLW1nbXQvbGVhdmVzLWNhbmNlbGwvbGVhdmVzLWNhbmNlbGwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxrQkFBa0IseUJBQXlCLDZCQUE2QixFQUFFO0FBQzFFO0VBQ0UsMkJBQTJCO0NBQzVCIiwiZmlsZSI6InNyYy9hcHAvbGVhdmVzLW1nbXQvbGVhdmVzLWNhbmNlbGwvbGVhdmVzLWNhbmNlbGwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYWRMZWF2ZSB0aCwgdGQge3BhZGRpbmc6IDVweCAycHggNXB4IDBweDtwYWRkaW5nLWxlZnQ6IDEwcHghaW1wb3J0YW50OyB9XHJcbnRleHRhcmVhIHtcclxuICBtYXgtaGVpZ2h0OiAzNXB4IWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!--<app-desig-emp-ordno [PageCode]=\"Cancel\" (desigchange)=\"designationChange()\" (empchange)=\"employechange($event)\" (orderchange)=\"GetLeavesSanction($event)\"></app-desig-emp-ordno>-->\r\n\r\n<div *ngIf=\"UserRoleId=='9'\">\r\n  <app-desig-emp-ordno [PageCode]=\"Cancel\" [ArrddlOrder]=\"orderList\" (desigchange)=\"designationChange()\" (empchange)=\"employechange($event)\" (orderchange)=\"GetLeavesSanction($event)\"></app-desig-emp-ordno>\r\n</div>\r\n<div *ngIf=\"UserRoleId=='8'\">\r\n  <app-desig-emp (desigchange)=\"designationChange()\" (onchange)=\"employechange($event)\"></app-desig-emp>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form [formGroup]=\"leaveSanctionDetailsForm\" #LeaveSanctionForm=\"ngForm\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Leave Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Sanction Order No\" formControlName=\"orderNo\" >\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" placeholder=\"Sanction Order Date\" formControlName=\"orderDT\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"fom-title\">Leave Details</div>-->\r\n      <div class=\"col-md-12 col-lg-6 combo-col\">\r\n\r\n        <div class=\"col-md-12 col-lg-3 pading-0\">\r\n          <label>Leave Type</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-9\">\r\n          <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"leaveCategory\">\r\n            <mat-radio-button [checked]='radiochecked' value=\"Single\" class=\"ml-10\">Single</mat-radio-button>\r\n            <mat-radio-button value=\"Combined\" class=\"ml-10\">Combined</mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select matNativeControl placeholder=\"Select Leave Reason\" formControlName=\"leaveReasonCD\">\r\n            <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"leave-wraper\" formArrayName=\"leaveSanctionDetails\" *ngFor=\"let item1 of leaveSanctionDetailsForm.get('leaveSanctionDetails')['controls']; let i=index\">\r\n        <div [formGroupName]=\"i\">\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"leaveCd\">\r\n                <mat-option *ngFor=\"let item of ArrddlLeavesType; let j=index\" (click)=\"LeaveTypeItemChange(i,item.leaveMaxAtATime)\" [value]=\"item.leaveTypeLeaveCd\">{{item.leaveTypeLeaveDesc}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"FDate\" readonly placeholder=\"From Date\" formControlName=\"fromDT\" (dateChange)=\"fillnoofDays('s',i)\" />\r\n              <mat-datepicker-toggle matSuffix [for]=\"FDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #FDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"TDate\" readonly placeholder=\"To Date\" formControlName=\"toDT\" (dateChange)=\"fillnoofDays('e',i)\" />\r\n              <!--(input)=\"alert('saga')\" onclick=\"alert('saga')\">-->\r\n              <mat-datepicker-toggle matSuffix [for]=\"TDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #TDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"No. of Days\" type=\"number\" formControlName=\"noDays\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </form>\r\n\r\n  <form [formGroup]=\"leaveCancelForm\" #LeaveCancelForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"SaveLeavesCancel('SI')\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Leave Cancellation Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Sanction Order No\" formControlName=\"cancelOrderNo\" (keypress)=\"validateOrdNo($event)\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"DOB\" placeholder=\"Sanction Order Date\" formControlName=\"cancelOrderDt\" [min]=\"leaveSanctionDetailsForm.controls.orderDT.value\" [max]=\"currentdate\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOB\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOB></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Reason For Cancellation\" formControlName=\"cancelReason\">\r\n          <!--<mat-select matNativeControl placeholder=\"Select Leave Reason\"  formControlName=\"leaveReasonCD\">\r\n            <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n          </mat-select>-->\r\n        </mat-form-field>\r\n      </div>\r\n      <div *ngIf=\"showReturnReason && UserRoleId=='9'\" class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Returned Reason\" formControlName=\"returnReason\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <textarea rows=\"5\" placeholder=\"Enter remarks (if any)\" class=\"wid-100\" formControlName=\"cancelRemark\"></textarea>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"submit\" class=\"btn btn-success\" (click)=\"checkValidation()\" [disabled]=\"leaveCancelForm.invalid\">Save</button>\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-warning\" (click)=\"ResetLeavesSanction()\" [disabled]=\"showCancel\">Cancel</button>\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-primary\" (click)=\"SaveLeavesCancel('SF')\" [disabled]=\"leaveCancelForm.invalid || showForward\">Save And Forward</button>\r\n        <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-success\" (click)=\"VerifiedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveCancelForm.invalid\">Verify</button>\r\n        <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-danger\" (click)=\"RejectedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveCancelForm.invalid\">Return</button>\r\n      </div>\r\n\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n\r\n<!--<div class=\"col-md-12 col-lg-7\">\r\n\r\n</div>-->\r\n<!--right table-->\r\n\r\n\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Leave Cancellation Details</div>\r\n\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <table class=\"w-100 tab-tr-bg\">\r\n        <tr>\r\n          <th class=\"tab-th\">Order No.</th>\r\n          <th>Order Date</th>\r\n          <th>Leave Status</th>\r\n          <th>Action</th>\r\n        </tr>\r\n      </table>\r\n      <mat-accordion class=\"accordion-wrap\">\r\n        <mat-expansion-panel *ngFor=\"let item of dataSourceCancelgrid \">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title class=\"col-md-3\">\r\n              {{item.cancelOrderNo}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              {{item.cancelOrderDt | date:'dd/MM/yyyy'}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              {{item.cancelVerifyFlg}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              <div>\r\n                <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"EditCancelLeaves(item,'dis')\">error</a>\r\n              </div>\r\n              <div *ngIf=\"UserRoleId=='9' && item.cancelVerifyFlg=='Entered'|| item.cancelVerifyFlg=='Returned'\">\r\n                <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditCancelLeaves(item,'ena')\">edit</a>\r\n              </div>\r\n              <div *ngIf=\"UserRoleId=='9' && item.cancelVerifyFlg=='Entered'|| item.cancelVerifyFlg=='Returned'\">\r\n                <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteCancelLeaves(item.leavesSanctionMainModel.leaveMainId,true);deletepopup = !deletepopup\">delete_forever </a>\r\n              </div>\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <table class=\"mat-elevation-z8 mat-table padLeave\">\r\n            <thead>\r\n              <tr class=\"mat-header-row ng-star-inserted\">\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leavesSanctionMainModel.leaveSanctionDetails\">\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.leaveCdDesc}}\r\n                </td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.toDT | date:'dd/MM/yyyy'}}\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </mat-expansion-panel>\r\n\r\n\r\n      </mat-accordion>\r\n\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <!--<div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Leave Cancel History</div>\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <table class=\"w-100 tab-tr-bg\">\r\n        <tr>\r\n          <th class=\"tab-th\">Order No.</th>\r\n          <th>Order Date</th>\r\n          <th>Reason</th>\r\n          <th>Status</th>\r\n        </tr>\r\n      </table>\r\n      <mat-accordion class=\"accordion-wrap\">\r\n        <mat-expansion-panel *ngFor=\"let item of dataSourceCancelHistorygrid\">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title class=\"col-md-3\">\r\n              {{item.cancelOrderNo}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              {{item.cancelOrderDt | date:'dd/MM/yyyy'}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              {{item.cancelReason}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4\">\r\n              {{item.cancelVerifyFlg}}\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <table class=\"mat-elevation-z8 mat-table padLeave\">\r\n            <thead>\r\n              <tr class=\"mat-header-row ng-star-inserted\">\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leavesSanctionMainModel.leaveSanctionDetails\">\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.leaveCdDesc}}\r\n                </td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.toDT | date:'dd/MM/yyyy'}}\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </mat-expansion-panel>\r\n\r\n\r\n      </mat-accordion>\r\n\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n      <mat-paginator #b [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData1($event)\" showFirstLastButtons></mat-paginator>\r\n    </div>\r\n  </div>-->\r\n\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\" DeleteCancelLeaves(leaveMainid,false)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"hooCheckerLogin\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure you want to verify?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure you want to reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">Please enter the reason</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VerifiedSanc(true)\">YES</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedSanc(true)\">YES</button>\r\n      <!--<button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">Close</button>-->\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.ts ***!
  \************************************************************************/
/*! exports provided: LeavesCancellComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesCancellComponent", function() { return LeavesCancellComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
/* harmony import */ var _shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared-module/desig-emp-ordno/desig-emp-ordno.component */ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//A 15-feb+




//import dayjs from 'dayjs';


var ELEMENT_DATA;
var LeavesCancellComponent = /** @class */ (function () {
    function LeavesCancellComponent(_Service, snackBar, formBuilder) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.formBuilder = formBuilder;
        this.displayedCancel = ['CancelOrderNo', 'CancelOrderDate', 'LeaveType', 'FromDate', 'ToDate', 'NoOfDays', 'CancelReason', 'Status'];
        this.highlightedRows = [];
        this.dataSourceCancel = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.dataSourceCancelgrid = [];
        this.dataSourceCancelHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.dataSourceCancelHistorygrid = [];
        this.currentdate = new Date();
        this.showAddButton = false;
        this.showDeleteButton = false;
        this.showCheckBox0 = false;
        this.radiochecked = true;
        this.Cancel = "Cancel";
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.UserRoleId = sessionStorage.getItem('userRoleID');
        this.showReturnReason = false;
        this.DetailsForm();
        this.CancelForm();
    }
    LeavesCancellComponent.prototype.ngOnInit = function () {
        debugger;
        this.BindDropDownLeavesReason();
        this.BindDropDownLeavesType();
        this.leaveSanctionDetailsForm.disable();
        this.leaveCancelForm.disable();
        this.leaveCancelForm.get('cancelOrderDt').disable();
        this.isVarified = true;
        this.isReasonEmpty = false;
    };
    LeavesCancellComponent.prototype.DetailsForm = function () {
        this.leaveSanctionDetailsForm = this.formBuilder.group({
            empCd: [this.employeeCode],
            empName: [null],
            permDDOId: [this.PermDdoId],
            orderNo: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveCategory: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveReasonCD: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            remarks: [null],
            verifyFlg: [null],
            leaveReasonDesc: [null],
            leaveMainId: [0],
            leaveSanctionDetails: this.formBuilder.array([this.createItem()])
        });
    };
    LeavesCancellComponent.prototype.createItem = function () {
        return this.formBuilder.group({
            //leaveDetailsId: [{ value: null, disabled: false }, Validators.required],
            leaveCd: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            fromDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            toDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            noDays: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            maxLeave: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            isMaxLeave: [{ value: false, disabled: false }]
        });
    };
    LeavesCancellComponent.prototype.CancelForm = function () {
        this.leaveCancelForm = this.formBuilder.group({
            preLeaveMainId: [this.leaveMainid],
            cancelOrderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            cancelOrderDt: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            cancelReason: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            returnReason: [null],
            cancelRemark: [null],
            cancelVerifyFlg: [null],
        });
    };
    LeavesCancellComponent.prototype.BindDropDownLeavesReason = function () {
        var _this = this;
        this._Service.GetLeavesReason().subscribe(function (data) {
            _this.ArrddlReason = data;
        });
    };
    LeavesCancellComponent.prototype.BindDropDownLeavesType = function () {
        var _this = this;
        this._Service.GetLeavesType().subscribe(function (data) {
            _this.ArrddlLeavesType = data;
        });
    };
    //Search Form
    LeavesCancellComponent.prototype.designationChange = function () {
        debugger;
        this.leaveSanctionDetailsForm.disable();
        this.leaveCancelForm.disable();
        this.showReturnReason = false;
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.CancelForm();
        //this.leaveCancelForm.get('cancelOrderDt').disable();
        if (this.UserRoleId == "8") { }
        else {
        }
        this.leaveSanctionDetailsForm.disable();
        this.leaveCancelForm.disable();
    };
    LeavesCancellComponent.prototype.employechange = function (value) {
        var _this = this;
        debugger;
        this.leaveSanctionDetailsForm.disable();
        this.leaveCancelForm.disable();
        this.showReturnReason = false;
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        // this.leaveSanctionDetailsForm.disable();
        this.CancelForm();
        // this.leaveCancelForm.disable();
        //this.leaveCancelForm.get('cancelOrderDt').disable();
        if (this.UserRoleId == "8") {
        }
        else {
        }
        this._Service.GetLeavesCancel(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceCancelgrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData(event);
            _this.dataSourceCancel = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
            _this.dataSourceCancel.paginator = _this.paginator;
            _this.dataSourceCancel.sort = _this.sort;
            //this.dataSourceCancelgrid = result;
            //this.dataSourceCancel = new MatTableDataSource(result);
            //this.dataSourceCancel.paginator = this.paginator;
            //this.dataSourceCancel.sort = this.sort;
        });
        this._Service.GetLeavesCancelHistory(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceCancelHistorygrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData1(event);
            _this.dataSourceCancelHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
            _this.dataSourceCancelHistory.paginator = _this.paginator1;
            _this.dataSourceCancelHistory.sort = _this.sort1;
            //this.dataSourceCancelHistorygrid = result;
            //this.dataSourceCancelHistory = new MatTableDataSource(result);
            //this.dataSourceCancel.paginator = this.paginator;
            //this.dataSourceCancel.sort = this.sort;
        });
        this.leaveSanctionDetailsForm.disable();
        this.leaveCancelForm.disable();
    };
    LeavesCancellComponent.prototype.getPaginationData = function (event) {
        debugger;
        this.dataSourceCancelgrid = this.alldataSourceCancelgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesCancellComponent.prototype.getPaginationData1 = function (event) {
        debugger;
        this.dataSourceCancelHistorygrid = this.alldataSourceCancelHistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesCancellComponent.prototype.GetLeavesSanction = function (value) {
        var _this = this;
        debugger;
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionDetailsForm.disable();
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.CancelForm();
        this.leaveCancelForm.disable();
        if (this.UserRoleId == "8") {
        }
        else {
        }
        //this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this._Service.GetVerifiedLeaveForCancel(this.PermDdoId, value).subscribe(function (result) {
            debugger;
            _this.leaveMainid = result.leavesSanctionMainModel.leaveMainId;
            _this.leaveSanctionDetailsForm.get('leaveMainId').setValue(_this.leaveMainid);
            _this.leaveSanctionDetailsForm.get('empCd').setValue(result.leavesSanctionMainModel.empCd);
            _this.leaveSanctionDetailsForm.get('permDDOId').setValue(result.leavesSanctionMainModel.permDDOId);
            _this.leaveSanctionDetailsForm.get('orderNo').setValue(result.leavesSanctionMainModel.orderNo);
            _this.leaveSanctionDetailsForm.get('orderDT').setValue(result.leavesSanctionMainModel.orderDT);
            _this.leaveSanctionDetailsForm.get('leaveCategory').setValue(result.leavesSanctionMainModel.leaveCategory);
            _this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(result.leavesSanctionMainModel.leaveReasonCD);
            _this.leaveCancelForm.get('preLeaveMainId').setValue(_this.leaveMainid);
            _this.leaveCancelForm.get('cancelOrderNo').setValue(result.cancelOrderNo);
            _this.leaveCancelForm.get('cancelOrderDt').setValue(result.cancelOrderDt);
            _this.leaveCancelForm.get('cancelReason').setValue(result.cancelReason);
            debugger;
            if (result.returnReason != null) {
                _this.showReturnReason = true;
                _this.leaveCancelForm.get('returnReason').setValue(result.returnReason);
            }
            else {
                _this.showReturnReason = false;
            }
            _this.leaveCancelForm.get('cancelRemark').setValue(result.cancelRemark);
            for (var i = 0; i < result.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(_this.createItem());
                //(<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveDetailsId').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveDetailsId);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
            }
            _this.leaveSanctionDetailsForm.disable();
            if (_this.UserRoleId == "8") {
                _this.leaveCancelForm.disable();
                _this.leaveCancelForm.get('cancelOrderDt').disable();
            }
            else {
                _this.leaveCancelForm.enable();
                _this.leaveCancelForm.get('cancelOrderDt').enable();
            }
        });
    };
    Object.defineProperty(LeavesCancellComponent.prototype, "getleaveDetails", {
        get: function () {
            return this.leaveSanctionDetailsForm.get('leaveSanctionDetails');
        },
        enumerable: true,
        configurable: true
    });
    //operational Form
    LeavesCancellComponent.prototype.LeaveTypeItemChange = function (i, value) {
        debugger;
        //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
        //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);
    };
    LeavesCancellComponent.prototype.ChangeLeavesType = function (value, index) {
        debugger;
        //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
        //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
    };
    LeavesCancellComponent.prototype.ChangeLeaveCategory = function (value) {
        //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
        //this.showDeleteButton = value == 'Single' ? false : true;
        //this.showAddButton = value == 'Single' ? false : true;
        //alert(this.findInvalidControls());
    };
    LeavesCancellComponent.prototype.fillnoofDays = function (value, index) {
        debugger;
        //if (this.leaveSanctionDetailsForm.get('fromDT_' + index).value != "undefined" || this.leaveSanctionDetailsForm.get('toDT_' + index).value) {
        //  this.firstdate = new Date(this.leaveSanctionDetailsForm.get('fromDT_' + index).value);
        //  this.lastdate = new Date(this.leaveSanctionDetailsForm.get('toDT_' + index).value);
        //  if (this.firstdate > this.lastdate) {
        //    alert('From date should be less than to date');
        //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(0);
        //    value == "s" ? this.leaveSanctionDetailsForm.get('fromDT_' + index).setValue("") : this.leaveSanctionDetailsForm.get('toDT_' + index).setValue("");
        //  }
        //  else {
        //    this.datedif = Math.abs(this.firstdate.getTime() - this.lastdate.getTime());
        //    this.datedif = Math.ceil(this.datedif / (1000 * 3600 * 24)) + 1;
        //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(this.datedif);
        //  }
        //}
    };
    LeavesCancellComponent.prototype.SaveLeavesCancel = function (value) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCancelForm.get('cancelOrderNo').value) {
            this.leaveCancelForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
            this._Service.SaveLeavesCancel(this.leaveCancelForm.value, value).subscribe(function (result) {
                if (parseInt(result) >= 1) {
                    if (value == "SI") {
                        _this.snackBar.open('Saved Successfully', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
                    }
                    _this.leaveCancelForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    _this.child.BindDropDownOrder(_this.employeeCode);
                }
                else {
                    if (value == "SI") {
                        _this.snackBar.open('Save  not Successfull', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
                    }
                }
            });
        }
        else {
            alert('Sanction orderNo and Cancel Order No Cant be same.');
        }
        this.leaveSanctionDetailsForm.disable();
    };
    //ForwardToChecker() {
    //  this.leaveCancelForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
    //  this.leaveCancelForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
    //  this.leaveCancelForm.get('cancelEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
    //  this.leaveCancelForm.get('cancelPermDdoId').setValue(this.PermDdoId);
    //  if (this.leaveCancelForm.get('cancelId').value == null) {
    //    this.leaveCancelForm.get('cancelId').setValue(0);
    //  }
    //  debugger;
    //  this._Service.SaveLeavescurtExten(this.leaveCancelForm.value, "F").subscribe(result => {
    //    this.snackBar.open('Save Successfully', null, { duration: 5000 });
    //    this.GetLeavesSanction(this.employeeCode);
    //  })
    //}
    LeavesCancellComponent.prototype.VerifiedSanc = function (flag) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (flag) {
            this._Service.VerifyReturnSanction(this.leaveSanctionDetailsForm.get('leaveMainId').value, "SV", "Cance", null).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Verified Successfully', null, { duration: 4000 });
                    _this.leaveCancelForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    _this.employechange(_this.employeeCode);
                }
                else {
                    _this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
                }
            });
            jquery__WEBPACK_IMPORTED_MODULE_5__('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = true;
        }
    };
    LeavesCancellComponent.prototype.RejectedSanc = function (flag) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (flag) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                return false;
            }
            this._Service.VerifyReturnSanction(this.leaveSanctionDetailsForm.get('leaveMainId').value, "SR", "Cance", this.reasonText).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Returned Successfully', null, { duration: 4000 });
                    _this.leaveCancelForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    _this.employechange(_this.employeeCode);
                    _this.reasonText = null;
                    _this.hooCheckerLogin = false;
                    ;
                }
                else {
                    _this.snackBar.open('Return not Successfull', null, { duration: 4000 });
                }
            });
            jquery__WEBPACK_IMPORTED_MODULE_5__('.dialog__close-btn').click();
        }
        else {
            this.isVarified = false;
            return false;
        }
    };
    LeavesCancellComponent.prototype.ResetLeavesSanction = function () {
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    };
    //Grid operation
    LeavesCancellComponent.prototype.ShowLeavesSanction = function (value) {
        debugger;
        this.leaveMainid = value.leaveMainId;
        this.SanctionOrderNo = value.orderNo;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.form2.resetForm();
        this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
        this.showAddButton = value.leaveCategory == 'Single' ? false : true;
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
        this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
        for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
        }
    };
    LeavesCancellComponent.prototype.EditCancelLeaves = function (element, flg) {
        debugger;
        this.leaveMainid = element.leavesSanctionMainModel.leaveMainId;
        this.employeeCode = element.leavesSanctionMainModel.empCd;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.showDeleteButton = element.leaveCategory == 'Single' ? false : true;
        this.showAddButton = element.leaveCategory == 'Single' ? false : true;
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
        this.leaveCancelForm.get('cancelOrderNo').setValue(element.cancelOrderNo);
        this.leaveCancelForm.get('cancelReason').setValue(element.cancelReason);
        this.leaveCancelForm.get('cancelRemark').setValue(element.cancelRemark);
        this.leaveCancelForm.get('cancelOrderDt').setValue(element.cancelOrderDt);
        if (element.leavesSanctionMainModel.returnReason != null) {
            this.showReturnReason = true;
            this.leaveCancelForm.get('returnReason').setValue(element.leavesSanctionMainModel.returnReason);
        }
        else {
            this.showReturnReason = false;
        }
        for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
        }
        this.leaveSanctionDetailsForm.disable();
        if (flg == "dis") {
            this.leaveCancelForm.disable();
        }
        else {
            this.leaveCancelForm.enable();
            //this.leaveCancelForm.get('curtLeaveCd').disable();
            //this.leaveCancelForm.get('curtFromDT').disable();
            //this.leaveCurtailForm.get('curtNoDays').setValue('50');
            //this.showForward = false;
            //this.showCancel = false;
            //this.showSave = false;
        }
    };
    LeavesCancellComponent.prototype.DeleteCancelLeaves = function (leaveMainId, flag) {
        var _this = this;
        debugger;
        this.leaveMainid = flag ? leaveMainId : "0";
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (!flag) {
            this._Service.DeleteLeavesSanction(leaveMainId, 'Canc').subscribe(function (result) {
                _this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
                _this.child.BindDropDownOrder(_this.employeeCode);
            });
            this.leaveSanctionDetailsForm.disable();
        }
        this.deletepopup = false;
    };
    LeavesCancellComponent.prototype.checkValidation = function () {
    };
    LeavesCancellComponent.prototype.validateOrdNo = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48)) {
            return true;
        }
        return false;
    };
    LeavesCancellComponent.prototype.rowHighlight = function (row) {
        this.highlightedRows.pop();
        this.highlightedRows.push(row);
    };
    LeavesCancellComponent.prototype.abc = function () {
        alert(this.findInvalidControls());
    };
    //Extra methods
    LeavesCancellComponent.prototype.findInvalidControls = function () {
        debugger;
        var invalid = [];
        var controls = this.leaveSanctionDetailsForm.controls;
        for (var name_1 in controls) {
            if (controls[name_1].invalid) {
                invalid.push(name_1);
            }
        }
        return invalid;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], LeavesCancellComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], LeavesCancellComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('b'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], LeavesCancellComponent.prototype, "paginator1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('a'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], LeavesCancellComponent.prototype, "sort1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveSanctionForm'),
        __metadata("design:type", Object)
    ], LeavesCancellComponent.prototype, "form1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveCancelForm'),
        __metadata("design:type", Object)
    ], LeavesCancellComponent.prototype, "form2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpOrdnoComponent"]),
        __metadata("design:type", _shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpOrdnoComponent"])
    ], LeavesCancellComponent.prototype, "child", void 0);
    LeavesCancellComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-leaves-cancell',
            template: __webpack_require__(/*! ./leaves-cancell.component.html */ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.html"),
            styles: [__webpack_require__(/*! ./leaves-cancell.component.css */ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.css")]
        }),
        __metadata("design:paramtypes", [_services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], LeavesCancellComponent);
    return LeavesCancellComponent;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "textarea {\r\n  max-height: 35px!important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGVhdmVzLW1nbXQvbGVhdmVzLWN1cnRhaWwvbGVhdmVzLWN1cnRhaWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJCQUEyQjtDQUM1QiIsImZpbGUiOiJzcmMvYXBwL2xlYXZlcy1tZ210L2xlYXZlcy1jdXJ0YWlsL2xlYXZlcy1jdXJ0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ZXh0YXJlYSB7XHJcbiAgbWF4LWhlaWdodDogMzVweCFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Start Alert message for delet, info-->\r\n<div class=\"col-md-12\">\r\n\r\n  <div class=\"alert alert-success alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Success!</strong> This alert box could indicate a successful or positive action.\r\n  </div>\r\n  <div class=\"alert alert-info alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n  <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"is_btnStatus==true\" (click)=\"btnclose();\">\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n\r\n</div>\r\n<!--End Alert message for delet, info-->\r\n\r\n\r\n<!--<app-desig-emp (desigchange)=\"designationChange()\"  (onchange)=\"GetLeavesSanction($event)\"></app-desig-emp>-->\r\n<div *ngIf=\"UserRoleId=='9'\">\r\n  <app-desig-emp-ordno [PageCode]=\"Curtail\" [ArrddlOrder]=\"orderList\" (desigchange)=\"designationChange()\" (empchange)=\"employechange($event)\" (orderchange)=\"GetLeavesSanction($event)\"></app-desig-emp-ordno>\r\n</div>\r\n<div *ngIf=\"UserRoleId=='8'\">\r\n  <app-desig-emp (desigchange)=\"designationChange()\" (onchange)=\"employechange($event)\"></app-desig-emp>\r\n</div>\r\n\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Leave Details\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n\r\n\r\n    <form [formGroup]=\"leaveSanctionDetailsForm\" #LeaveSanctionForm=\"ngForm\">\r\n\r\n\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Sanction Order No\" formControlName=\"orderNo\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" placeholder=\"Sanction Order Date\" formControlName=\"orderDT\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"fom-title\">Leave Details</div>-->\r\n      <div class=\"col-md-6 col-lg-4 combo-col-2\">\r\n\r\n        <div class=\"col-md-6 col-lg-3 pading-0\">\r\n          <label>Leave Type</label>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-9\">\r\n          <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"leaveCategory\">\r\n            <mat-radio-button [checked]='radiochecked' value=\"Single\" class=\"ml-10\">Single</mat-radio-button>\r\n            <mat-radio-button value=\"Combined\" class=\"ml-10\">Combined</mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-6 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select matNativeControl placeholder=\"Select Leave Reason\" formControlName=\"leaveReasonCD\">\r\n            <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"leave-wraper\" formArrayName=\"leaveSanctionDetails\" *ngFor=\"let item1 of leaveSanctionDetailsForm.get('leaveSanctionDetails')['controls']; let i=index\">\r\n        <div [formGroupName]=\"i\">\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"leaveCd\">\r\n                <mat-option *ngFor=\"let item of ArrddlLeavesType; let j=index\" (click)=\"LeaveTypeItemChange(i,item.leaveMaxAtATime)\" [value]=\"item.leaveTypeLeaveCd\">{{item.leaveTypeLeaveDesc}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"FDate\" readonly placeholder=\"From Date\" formControlName=\"fromDT\" (dateChange)=\"fillnoofDays('s',i)\" />\r\n              <mat-datepicker-toggle matSuffix [for]=\"FDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #FDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"TDate\" readonly placeholder=\"To Date\" formControlName=\"toDT\" (dateChange)=\"fillnoofDays('e',i)\" />\r\n              <!--(input)=\"alert('saga')\" onclick=\"alert('saga')\">-->\r\n              <mat-datepicker-toggle matSuffix [for]=\"TDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #TDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"No. of Days\" type=\"number\" formControlName=\"noDays\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </form>\r\n\r\n    <form [formGroup]=\"leaveCurtailForm\" #LeaveCurtailForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"SaveLeavesCurtExten('I')\">\r\n      <mat-card class=\"example-card\">\r\n        <div class=\"fom-title\">Leave Curtail/ Extension Details</div>\r\n\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Sanction Order No\" formControlName=\"curtOrderNo\" (keypress)=\"validateOrdNo($event)\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"DOB\" placeholder=\"Sanction Order Date\" formControlName=\"curtOrderDT\" [min]=\"leaveSanctionDetailsForm.controls.orderDT.value\" [max]=\"currentdate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"DOB\"></mat-datepicker-toggle>\r\n            <mat-datepicker #DOB></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n        <!--<div class=\"fom-title\">Leave Details</div>-->\r\n        <div class=\"col-md-6 col-lg-4 combo-col-2\">\r\n\r\n          <!--<div class=\"col-md-12 col-lg-3 pading-0\">-->\r\n          <!--<label>Leave Type</label>-->\r\n          <!--</div>-->\r\n          <div class=\"col-md-12 col-lg-12\">\r\n            <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"isCurtleave\">\r\n              <mat-radio-button [checked]='radiochecked' value=\"Curtail\" class=\"ml-10\">Leave Curtail</mat-radio-button>\r\n              <mat-radio-button value=\"Extension\" class=\"ml-10\">Leave Extension</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Reason for curtail/extension\" formControlName=\"curtReason\">\r\n            <!--<mat-select matNativeControl placeholder=\"Select Leave Reason\"  formControlName=\"leaveReasonCD\">\r\n              <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n            </mat-select>-->\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"curtLeaveCd\">\r\n              <mat-option *ngFor=\"let item of ArrddlLeavesType; let j=index\" [value]=\"item.leaveTypeLeaveCd\">{{item.leaveTypeLeaveDesc}}</mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"FDate\" readonly placeholder=\"From Date\" formControlName=\"curtFromDT\" (dateChange)=\"fillnoofDays('s')\" />\r\n            <mat-datepicker-toggle matSuffix [for]=\"FDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #FDate></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"TDate\" readonly placeholder=\"To Date\" formControlName=\"curtToDT\" [max]=\"mxcutaildate\" [min]=\"mncutaildate\" (dateChange)=\"fillnoofDays('e')\" />\r\n            <mat-datepicker-toggle matSuffix [for]=\"TDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #TDate></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6 col-lg-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"No. of Days\" type=\"number\" formControlName=\"curtNoDays\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n        <div *ngIf=\"showReturnReason && UserRoleId=='9'\" class=\"col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Returned Reason\" formControlName=\"returnReason\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <textarea rows=\"5\" placeholder=\"Enter remarks (if any)\" class=\"wid-100\" formControlName=\"curtRemarks\"></textarea>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button *ngIf=\"UserRoleId=='9'\" type=\"submit\" class=\"btn btn-success\" (click)=\"checkValidation(event)\" [disabled]=\"leaveCurtailForm.invalid||showSave\">Save</button>\r\n          <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-warning\" (click)=\"ResetLeavesSanction()\" [disabled]=\"showCancel\">Cancel</button>\r\n          <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-primary\" (click)=\"SaveLeavesCurtExten('F')\" [disabled]=\"leaveCurtailForm.invalid||showForward \">Save And Forward</button>\r\n          <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-success\" (click)=\"VerifiedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveCurtailForm.invalid\">Verify</button>\r\n          <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-danger\" (click)=\"RejectedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveCurtailForm.invalid\">Return</button>\r\n        </div>\r\n      </mat-card>\r\n    </form>\r\n\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n<!--<div class=\"col-md-12 col-lg-7\">\r\n\r\n</div>-->\r\n<!--right table-->\r\n<mat-accordion class=\"col-md-12 mb-20\">\r\n  <mat-expansion-panel [expanded]=\"true\">\r\n    <mat-expansion-panel-header class=\"panel-header btm-m15\">\r\n      <mat-panel-title class=\"acordion-heading\">\r\n        Leave Curtail/ Extension Details\r\n      </mat-panel-title>\r\n\r\n    </mat-expansion-panel-header>\r\n\r\n\r\n\r\n\r\n\r\n    <table class=\"w-100 tab-tr-bg\">\r\n      <tr>\r\n        <th class=\"tbl-2\">Order No.</th>\r\n        <th class=\"tbl-2\">Order Date</th>\r\n        <th class=\"tbl-2\">Leave Curtail/Extension</th>\r\n        <th class=\"tbl-2\">Leave Status</th>\r\n        <th class=\"tbl-2\">Action</th>\r\n      </tr>\r\n    </table>\r\n    <mat-accordion class=\"accordion-wrap\">\r\n      <mat-expansion-panel *ngFor=\"let item of dataSourceforgrid\">\r\n        <mat-expansion-panel-header>\r\n          <mat-panel-title class=\"col-md-3 tbl-2\">\r\n            {{item.curtOrderNo}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4 tbl-2\">\r\n            {{item.curtOrderDT | date:'dd/MM/yyyy'}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4 tbl-2\">\r\n            {{item.isCurtleave}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4 tbl-2\">\r\n            {{item.verifyflg}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4 tbl-2\">\r\n            <!--<div *ngIf=\"UserRoleId=='8'\">-->\r\n            <div>\r\n              <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"EditCurtailLeaves(item,'dis')\">error</a>\r\n            </div>\r\n            <div *ngIf=\"UserRoleId=='9' && item.verifyflg=='Entered'|| item.verifyflg=='Returned'\">\r\n              <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditCurtailLeaves(item,'ena')\">edit</a>\r\n            </div>\r\n            <div *ngIf=\"UserRoleId=='9'&& item.verifyflg=='Entered' || item.verifyflg=='Returned'\">\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteCurtailLeaves(item.leavesSanctionMainModel.leaveMainId,true);deletepopup = !deletepopup\">delete_forever </a>\r\n            </div>\r\n          </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <table class=\"mat-elevation-z8 mat-table padLeave even-odd-color\">\r\n          <thead>\r\n            <tr class=\"mat-header-row ng-star-inserted\">\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leavesSanctionMainModel.leaveSanctionDetails\">\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                {{item1.leaveCdDesc}}\r\n              </td>\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                {{item1.toDT | date:'dd/MM/yyyy'}}\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </mat-expansion-panel>\r\n\r\n\r\n    </mat-accordion>\r\n\r\n    <div class=\"col-md-12\">\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n    </div>\r\n\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n\r\n\r\n\r\n<!--<div class=\"example-card mat-card\">\r\n  <div class=\"fom-title\">Leave Curtail/Extension History</div>\r\n\r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <table class=\"w-100 tab-tr-bg\">\r\n      <tr>\r\n        <th class=\"tab-th\">Order No.</th>\r\n        <th>Order Date</th>\r\n        <th>Leave Curtail/Extension</th>\r\n        <th>Leave Status</th>\r\n      </tr>\r\n    </table>\r\n    <mat-accordion class=\"accordion-wrap\">\r\n      <mat-expansion-panel *ngFor=\"let item of dataSourceCurtailHistorygrid \">\r\n        <mat-expansion-panel-header>\r\n          <mat-panel-title class=\"col-md-3\">\r\n            {{item.curtOrderNo}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4\">\r\n            {{item.curtOrderDT | date:'dd/MM/yyyy'}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4\">\r\n            {{item.isCurtleave}}\r\n          </mat-panel-title>\r\n          <mat-panel-title class=\"col-md-4\">\r\n            {{item.verifyflg}}\r\n          </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <table class=\"mat-elevation-z8 mat-table padLeave\">\r\n          <thead>\r\n            <tr class=\"mat-header-row ng-star-inserted\">\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n              <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leavesSanctionMainModel.leaveSanctionDetails\">\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                {{item1.leaveCdDesc}}\r\n              </td>\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n              <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                {{item1.toDT | date:'dd/MM/yyyy'}}\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </mat-expansion-panel>\r\n\r\n\r\n    </mat-accordion>\r\n\r\n  </div>\r\n  <div class=\"col-md-12\">\r\n    <mat-paginator  #b [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData1($event)\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>-->\r\n\r\n\r\n\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteCurtailLeaves(leaveMainid,false)\">Delete</button>\r\n      <!--<button type=\"button\" class=\"btn btn-info center\" (click)=\"DeleteCurtailLeaves(leaveMainId,false)\">Delete</button>-->\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n<app-dialog [(visible)]=\"hooCheckerLogin\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure you want to verify?</h3>\r\n    <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure you want to reject?</h3>\r\n    <div *ngIf=\"!isVarified\">\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n      </mat-card>\r\n      <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">Please enter the reason</label>\r\n    </div>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VerifiedSanc(true)\">YES</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedSanc(true)\">YES</button>\r\n      <!--<button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">Close</button>-->\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">No</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.ts ***!
  \************************************************************************/
/*! exports provided: LeavesCurtailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesCurtailComponent", function() { return LeavesCurtailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
/* harmony import */ var _shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared-module/desig-emp-ordno/desig-emp-ordno.component */ "./src/app/shared-module/desig-emp-ordno/desig-emp-ordno.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//A 15-feb+




//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';



var ELEMENT_DATA;
var ELEMENT_DATA1;
var LeavesCurtailComponent = /** @class */ (function () {
    function LeavesCurtailComponent(datePipe, _Service, snackBar, formBuilder) {
        this.datePipe = datePipe;
        this._Service = _Service;
        this.snackBar = snackBar;
        this.formBuilder = formBuilder;
        this.is_btnStatus = true;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.dataSourceforgrid = [];
        this.dataSourceCurtailHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA1);
        this.dataSourceCurtailHistorygrid = [];
        this.currentdate = new Date();
        this.radiochecked = true;
        this.Curtail = "Curtail";
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.UserRoleId = sessionStorage.getItem('userRoleID');
        this.showCancel = true;
        this.showForward = true;
        this.showSave = true;
        this.showReturnReason = false;
        this.DetailsForm();
        this.CurtailForm();
    }
    LeavesCurtailComponent.prototype.btnclose = function () {
        this.is_btnStatus = false;
    };
    LeavesCurtailComponent.prototype.ngOnInit = function () {
        debugger;
        this.BindDropDownLeavesReason();
        this.BindDropDownLeavesType();
        this.leaveSanctionDetailsForm.disable();
        this.leaveCurtailForm.disable();
        debugger;
        if (this.UserRoleId == "8") {
        }
        else {
            this.buttonText = "Save";
            this.showForward = true;
            this.showCancel = true;
            this.showSave = true;
        }
        this.isVarified = true;
        this.isReasonEmpty = false;
    };
    LeavesCurtailComponent.prototype.DetailsForm = function () {
        this.leaveSanctionDetailsForm = this.formBuilder.group({
            leaveMainId: [0],
            empCd: [this.employeeCode],
            permDDOId: [this.PermDdoId],
            orderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveCategory: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveReasonCD: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveReasonDesc: [null],
            remarks: [null],
            leaveSanctionDetails: this.formBuilder.array([this.createItem()])
        });
    };
    LeavesCurtailComponent.prototype.createItem = function () {
        return this.formBuilder.group({
            leaveDetailsId: [0],
            leaveCd: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            fromDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            toDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            noDays: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    };
    LeavesCurtailComponent.prototype.CurtailForm = function () {
        this.leaveCurtailForm = this.formBuilder.group({
            curtId: [0],
            curtOrderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtOrderDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            isCurtleave: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtReason: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtRemarks: [null],
            returnReason: [null],
            curtLeaveDetailsId: [0],
            curtLeaveCd: [{ value: null, disabled: true }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtFromDT: [{ value: null, disabled: true }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtToDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtNoDays: [{ value: null, disabled: true }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtEmpCd: [null],
            curtPermDdoId: [null],
            preOrderNo: [null],
            preLeaveMainId: [null],
        });
    };
    LeavesCurtailComponent.prototype.BindDropDownLeavesReason = function () {
        var _this = this;
        this._Service.GetLeavesReason().subscribe(function (data) {
            _this.ArrddlReason = data;
        });
    };
    LeavesCurtailComponent.prototype.BindDropDownLeavesType = function () {
        var _this = this;
        this._Service.GetLeavesType().subscribe(function (data) {
            _this.ArrddlLeavesType = data;
        });
    };
    //Search Form
    LeavesCurtailComponent.prototype.designationChange = function () {
        debugger;
        this.leaveSanctionDetailsForm.disable();
        this.leaveCurtailForm.disable();
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.CurtailForm();
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        //this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
        //this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
        //this.leaveCurtailForm.get('curtFromDT').setValue(result.curtFromDT);
        //this.leaveCurtailForm.get('curtToDT').setValue(result.curtToDT);
        this.showReturnReason = false;
        if (this.UserRoleId == "8") {
        }
        else {
            this.buttonText = "Save";
            this.showForward = true;
            this.showCancel = true;
            this.showSave = true;
        }
        this.leaveSanctionDetailsForm.disable();
        this.leaveCurtailForm.disable();
    };
    LeavesCurtailComponent.prototype.employechange = function (value) {
        var _this = this;
        debugger;
        this.leaveSanctionDetailsForm.disable();
        this.leaveCurtailForm.disable();
        this.showReturnReason = false;
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.CurtailForm();
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
        this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
        this.leaveSanctionDetailsForm.disable();
        this.leaveCurtailForm.disable();
        if (this.UserRoleId == "8") {
        }
        else {
            this.buttonText = "Save";
            this.showForward = true;
            this.showCancel = true;
            this.showSave = true;
        }
        this._Service.GetLeavesCurtailDetail(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceforgrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData(event);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this._Service.GetLeavesCurtailHistory(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceCurtailHistorygrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData1(event);
            //this.dataSourceHistory = new MatTableDataSource(result);
            //this.dataSourceHistory.paginator = this.paginator1;
            //this.dataSourceHistory.sort = this.sort1;
            //this.alldataSourceCurtailHistorygrid = result;
            //let event = { pageIndex: 0, pageSize: 5 };
            //this.getPaginationData1(event);
            _this.dataSourceCurtailHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](result);
            _this.dataSourceCurtailHistory.paginator = _this.paginator1;
            _this.dataSourceCurtailHistory.sort = _this.sort1;
        });
    };
    LeavesCurtailComponent.prototype.GetLeavesSanction = function (value) {
        var _this = this;
        debugger;
        this.DetailsForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionDetailsForm.disable();
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.CurtailForm();
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.leaveCurtailForm.disable();
        //if (this.UserRoleId == "8") {
        //}
        //else {
        this.buttonText = "Save";
        this.showForward = true;
        this.showCancel = true;
        this.showSave = true;
        //}
        //this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this._Service.GetVerifiedLeavesSanction(this.PermDdoId, value).subscribe(function (result) {
            debugger;
            _this.leaveMainid = result.leavesSanctionMainModel.leaveMainId;
            _this.leaveSanctionDetailsForm.get('leaveMainId').setValue(_this.leaveMainid);
            _this.leaveSanctionDetailsForm.get('empCd').setValue(result.leavesSanctionMainModel.empCd);
            _this.leaveSanctionDetailsForm.get('permDDOId').setValue(result.leavesSanctionMainModel.permDdoId);
            _this.leaveSanctionDetailsForm.get('orderNo').setValue(result.leavesSanctionMainModel.orderNo);
            _this.leaveSanctionDetailsForm.get('orderDT').setValue(result.leavesSanctionMainModel.orderDT);
            _this.leaveSanctionDetailsForm.get('leaveCategory').setValue(result.leavesSanctionMainModel.leaveCategory);
            _this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(result.leavesSanctionMainModel.leaveReasonCD);
            for (var i = 0; i < result.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(_this.createItem());
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveDetailsId').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveDetailsId);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
                _this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
            }
            _this.leaveCurtailid = result.curtId;
            _this.leaveCurtailForm.get('curtId').setValue(result.curtId);
            _this.leaveCurtailForm.get('curtOrderNo').setValue(result.curtOrderNo);
            _this.leaveCurtailForm.get('curtOrderDT').setValue(result.curtOrderDT);
            if (result.isCurtleave == null || result.isCurtleave == 'Curtail') {
                _this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
                _this.mxcutaildate = result.curtToDT;
                _this.mncutaildate = result.curtFromDT;
            }
            else {
                _this.leaveCurtailForm.get('isCurtleave').setValue(result.isCurtleave);
                _this.mxcutaildate = _this.currentdate;
                _this.mncutaildate = result.curtToDT;
            }
            _this.leaveCurtailForm.get('curtReason').setValue(result.curtReason);
            _this.leaveCurtailForm.get('curtRemarks').setValue(result.curtRemarks);
            debugger;
            if (result.returnReason != null) {
                _this.showReturnReason = true;
                _this.leaveCurtailForm.get('returnReason').setValue(result.returnReason);
            }
            else {
                _this.showReturnReason = false;
            }
            _this.leaveCurtailForm.get('curtLeaveDetailsId').setValue(result.curtLeaveDetailsId);
            _this.leaveCurtailForm.get('curtLeaveCd').setValue(result.curtLeaveCd);
            _this.leaveCurtailForm.get('curtFromDT').setValue(result.curtFromDT);
            _this.leaveCurtailForm.get('curtToDT').setValue(result.curtToDT);
            _this.leaveCurtailForm.get('curtNoDays').setValue(result.curtNoDays);
            _this.leaveCurtailForm.get('curtEmpCd').setValue(result.leavesSanctionMainModel.empCd);
            _this.leaveCurtailForm.get('curtPermDdoId').setValue(result.leavesSanctionMainModel.permDdoId);
            _this.leaveCurtailForm.get('preOrderNo').setValue(result.leavesSanctionMainModel.orderNo);
            _this.leaveCurtailForm.get('preLeaveMainId').setValue(_this.leaveMainid);
            //result.curtId == 0 ? this.buttonText = "Save" : this.buttonText = "Update";
            _this.leaveSanctionDetailsForm.disable();
            //if (this.UserRoleId == "8") {
            //  this.leaveCurtailForm.disable();
            //}
            //else {
            _this.leaveCurtailForm.enable();
            _this.leaveCurtailForm.get('curtLeaveCd').disable();
            _this.leaveCurtailForm.get('curtFromDT').disable();
            //this.leaveCurtailForm.get('curtNoDays').setValue('50');
            _this.showForward = false;
            _this.showCancel = false;
            _this.showSave = false;
            //}
        });
        //this.abc();
    };
    Object.defineProperty(LeavesCurtailComponent.prototype, "getleaveDetails", {
        get: function () {
            return this.leaveSanctionDetailsForm.get('leaveSanctionDetails');
        },
        enumerable: true,
        configurable: true
    });
    //operational Form
    LeavesCurtailComponent.prototype.LeaveTypeItemChange = function (i, value) {
        debugger;
        //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
        //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);
    };
    LeavesCurtailComponent.prototype.ChangeLeavesType = function (value, index) {
        debugger;
        //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
        //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
    };
    LeavesCurtailComponent.prototype.ChangeLeaveCategory = function (value) {
        if (value == 'Curtail') {
            this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
            this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
        }
        else {
            this.mxcutaildate = this.currentdate;
            this.mncutaildate = this.leaveCurtailForm.get('curtToDT').value;
        }
    };
    LeavesCurtailComponent.prototype.getPaginationData = function (event) {
        this.dataSourceforgrid = this.alldataSourceforgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesCurtailComponent.prototype.getPaginationData1 = function (event) {
        this.dataSourceCurtailHistorygrid = this.alldataSourceCurtailHistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesCurtailComponent.prototype.SaveLeavesCurtExten = function (value) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        var detailsFormLength = this.leaveSanctionDetailsForm.get('leaveSanctionDetails').value.length - 1;
        var sancdays = parseInt(this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(detailsFormLength).get('noDays').value);
        var curtdays = this.leaveCurtailForm.get('curtNoDays').value;
        if (sancdays != curtdays) 
        //if (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);)
        {
            sancdays > curtdays ? this.leaveCurtailForm.get('isCurtleave').setValue('Curtail') : this.leaveCurtailForm.get('isCurtleave').setValue('Extension');
        }
        else {
            alert('please Extend or curtail the leave');
            return false;
        }
        if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCurtailForm.get('curtOrderNo').value) {
            this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
            this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
            this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
            this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
            if (this.leaveCurtailForm.get('curtId').value == null) {
                this.leaveCurtailForm.get('curtId').setValue(0);
            }
            this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, value).subscribe(function (result) {
                if (parseInt(result) >= 1) {
                    if (value == "I") {
                        _this.snackBar.open('Saved Successfully', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
                    }
                    _this.snackBar.open('Saved Successfully', null, { duration: 4000 });
                    _this.child.BindDropDownOrder(_this.employeeCode);
                    _this.leaveCurtailForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    //this.GetLeavesSanction("0");
                }
                else {
                    if (value == "I") {
                        _this.snackBar.open('Save  not Successfull', null, { duration: 4000 });
                    }
                    else {
                        _this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
                    }
                }
            });
        }
        else {
            alert('Sanction orderNo and Curtail/Extension Order No Cant be same.');
        }
        this.leaveSanctionDetailsForm.disable();
    };
    LeavesCurtailComponent.prototype.ForwardToChecker = function () {
        var _this = this;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        debugger;
        var detailsFormLength = this.leaveSanctionDetailsForm.get('leaveSanctionDetails').value.length - 1;
        var sancdays = parseInt(this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(detailsFormLength).get('noDays').value);
        var curtdays = this.leaveCurtailForm.get('curtNoDays').value;
        if (sancdays != curtdays) 
        //if (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);)
        {
            sancdays > curtdays ? this.leaveCurtailForm.get('isCurtleave').setValue('Curtail') : this.leaveCurtailForm.get('isCurtleave').setValue('Extension');
        }
        else {
            alert('please Extend or curtail the leave');
            return false;
        }
        if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCurtailForm.get('curtOrderNo').value) {
            this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
            this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
            this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
            this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
            if (this.leaveCurtailForm.get('curtId').value == null) {
                this.leaveCurtailForm.get('curtId').setValue(0);
            }
            if (this.UserRoleId == "9") {
                this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "F").subscribe(function (result) {
                    if (parseInt(result) >= 1) {
                        _this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
                        _this.leaveCurtailForm.reset();
                        _this.leaveSanctionDetailsForm.reset();
                        _this.form1.resetForm();
                        _this.form2.resetForm();
                        _this.child.BindDropDownOrder(_this.employeeCode);
                        //this.GetLeavesSanction("0");
                    }
                    else {
                        _this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
                    }
                });
            }
        }
        else {
            alert('Sanction orderNo and Curtail/Extension Order No Cant be same.');
        }
        if (this.UserRoleId == "8") {
            this.leaveCurtailForm.disable();
        }
        //this.child.BindDropDownOrder(this.leaveSanctionDetailsForm.get('empCd').value);
        this.leaveSanctionDetailsForm.disable();
    };
    LeavesCurtailComponent.prototype.ResetLeavesSanction = function () {
        this.showReturnReason = false;
        this.leaveCurtailForm.reset();
        this.leaveSanctionDetailsForm.reset();
        this.form1.resetForm();
        this.form2.resetForm();
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionDetailsForm.disable();
        //if (this.UserRoleId == "8") {
        //  this.leaveCurtailForm.disable();
        //}
    };
    LeavesCurtailComponent.prototype.VerifiedSanc = function (flag) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (flag) {
            this._Service.VerifyReturnSanction(this.leaveCurtailForm.get('curtId').value, "V", "CurtE", null).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Verified Successfully', null, { duration: 4000 });
                    _this.leaveCurtailForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    _this.employechange(_this.employeeCode);
                }
                else {
                    _this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
                }
            });
            jquery__WEBPACK_IMPORTED_MODULE_6__('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = true;
        }
    };
    LeavesCurtailComponent.prototype.RejectedSanc = function (flag) {
        var _this = this;
        debugger;
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (flag) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                return false;
            }
            this._Service.VerifyReturnSanction(this.leaveCurtailForm.get('curtId').value, "R", "CurtE", this.reasonText).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Returned Successfully', null, { duration: 4000 });
                    _this.leaveCurtailForm.reset();
                    _this.leaveSanctionDetailsForm.reset();
                    _this.form1.resetForm();
                    _this.form2.resetForm();
                    _this.employechange(_this.employeeCode);
                    ;
                    _this.reasonText = null;
                    _this.hooCheckerLogin = false;
                    // $('.dialog__close-btn').click();
                }
                else {
                    _this.snackBar.open('Return not Successfull', null, { duration: 4000 });
                }
            });
            jquery__WEBPACK_IMPORTED_MODULE_6__('.dialog__close-btn').click();
        }
        else {
            this.isVarified = false;
            return false;
        }
    };
    //Grid operation
    LeavesCurtailComponent.prototype.ShowLeavesSanction = function (value) {
        debugger;
        this.leaveMainid = value.leaveMainId;
        this.SanctionOrderNo = value.orderNo;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.form2.resetForm();
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
        this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
        //this.leaveCurtailForm.setControl('leaveSanctionDetails', new FormArray([]));
        //this.leaveCurtailForm.reset();
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.buttonText = "Save";
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
        this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
        for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
        }
        this.leaveSanctionDetailsForm.disable();
        if (this.UserRoleId == "8") {
            this.leaveCurtailForm.disable();
        }
    };
    LeavesCurtailComponent.prototype.EditCurtailLeaves = function (element, flg) {
        debugger;
        this.leaveMainid = element.leavesSanctionMainModel.leaveMainId;
        this.employeeCode = element.curtEmpCd;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        //this.buttonText = "Update";
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
        this.leaveCurtailForm.get('curtOrderNo').setValue(element.curtOrderNo);
        this.leaveCurtailForm.get('curtOrderDT').setValue(element.curtOrderDT);
        this.leaveCurtailForm.get('curtFromDT').setValue(element.curtFromDT);
        this.leaveCurtailForm.get('curtToDT').setValue(element.curtToDT);
        if (element.isCurtleave == null || element.isCurtleave == 'Curtail') {
            this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
            this.mxcutaildate = element.curtToDT;
            this.mncutaildate = element.curtFromDT;
        }
        else {
            this.leaveCurtailForm.get('isCurtleave').setValue(element.isCurtleave);
            this.mxcutaildate = this.currentdate;
            this.mncutaildate = element.curtToDT;
        }
        this.leaveCurtailForm.get('curtReason').setValue(element.curtReason);
        this.leaveCurtailForm.get('curtRemarks').setValue(element.curtRemarks);
        this.leaveCurtailForm.get('curtLeaveCd').setValue(element.curtLeaveCd);
        this.leaveCurtailForm.get('curtNoDays').setValue(element.curtNoDays);
        this.leaveCurtailForm.get('curtLeaveDetailsId').setValue(element.curtLeaveDetailsId);
        if (element.returnReason != null) {
            this.showReturnReason = true;
            this.leaveCurtailForm.get('returnReason').setValue(element.returnReason);
        }
        else {
            this.showReturnReason = false;
        }
        this.leaveCurtailForm.get('curtId').setValue(element.curtId);
        for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
        }
        this.leaveSanctionDetailsForm.disable();
        if (flg == "dis") {
            this.leaveCurtailForm.disable();
        }
        else {
            this.leaveCurtailForm.enable();
            this.leaveCurtailForm.get('curtLeaveCd').disable();
            this.leaveCurtailForm.get('curtFromDT').disable();
            //this.leaveCurtailForm.get('curtNoDays').setValue('50');
            this.showForward = false;
            this.showCancel = false;
            this.showSave = false;
        }
    };
    LeavesCurtailComponent.prototype.DeleteCurtailLeaves = function (leaveMainId, flag) {
        var _this = this;
        debugger;
        this.leaveMainid = flag ? leaveMainId : "0";
        this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
        if (!flag) {
            this._Service.DeleteLeavesSanction(leaveMainId, 'Curt').subscribe(function (result) {
                _this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
                _this.child.BindDropDownOrder(_this.employeeCode);
            });
            this.leaveSanctionDetailsForm.disable();
        }
        this.deletepopup = false;
    };
    LeavesCurtailComponent.prototype.checkValidation = function () {
        //if (this.leaveSanctionDetailsForm.get("leaveCategory").value == "Combined") {
        //  if (this.objlsmain.leaveSanctionDetails.length == 1) {
        //    alert('Please select single leave Type');
        //    return false;
        //  }
        //}
    };
    LeavesCurtailComponent.prototype.fillnoofDays = function (value) {
        debugger;
        this.firstdate = this.datePipe.transform(this.leaveCurtailForm.get('curtFromDT').value, 'yyyy-MM-dd');
        this.lastdate = this.datePipe.transform(this.leaveCurtailForm.get('curtToDT').value, 'yyyy-MM-dd');
        if ((this.firstdate != null) && (this.lastdate != null)) {
            debugger;
            if (this.leaveCurtailForm.get('curtFromDT').value > this.leaveCurtailForm.get('curtToDT').value) {
                alert('From date should be less than to date');
                this.leaveCurtailForm.get('curtToDT').setValue("");
            }
            else {
                this.datedif = this.leaveCurtailForm.get('curtToDT').value.diff(this.leaveCurtailForm.get('curtFromDT').value, 'days');
                this.leaveCurtailForm.get('curtNoDays').setValue(this.datedif + 1);
            }
        }
    };
    LeavesCurtailComponent.prototype.validateOrdNo = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48)) {
            return true;
        }
        return false;
    };
    LeavesCurtailComponent.prototype.rowHighlight = function (row) {
        debugger;
        //this.highlightedRows.pop();
        //this.highlightedRows.push(row)
    };
    LeavesCurtailComponent.prototype.abc = function () {
        alert(this.findInvalidControls());
    };
    //Extra methods
    LeavesCurtailComponent.prototype.findInvalidControls = function () {
        debugger;
        var invalid = [];
        var controls = this.leaveCurtailForm.controls;
        for (var name_1 in controls) {
            if (controls[name_1].invalid) {
                invalid.push(name_1);
            }
        }
        return invalid;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], LeavesCurtailComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], LeavesCurtailComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('b'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], LeavesCurtailComponent.prototype, "paginator1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('a'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], LeavesCurtailComponent.prototype, "sort1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveSanctionForm'),
        __metadata("design:type", Object)
    ], LeavesCurtailComponent.prototype, "form1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveCurtailForm'),
        __metadata("design:type", Object)
    ], LeavesCurtailComponent.prototype, "form2", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpOrdnoComponent"]),
        __metadata("design:type", _shared_module_desig_emp_ordno_desig_emp_ordno_component__WEBPACK_IMPORTED_MODULE_4__["DesigEmpOrdnoComponent"])
    ], LeavesCurtailComponent.prototype, "child", void 0);
    LeavesCurtailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-leaves-curtail',
            template: __webpack_require__(/*! ./leaves-curtail.component.html */ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.html"),
            styles: [__webpack_require__(/*! ./leaves-curtail.component.css */ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"], _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], LeavesCurtailComponent);
    return LeavesCurtailComponent;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-mgmt-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-mgmt-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: LeavesMgmtRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesMgmtRoutingModule", function() { return LeavesMgmtRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _leaves_mgmt_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leaves-mgmt.module */ "./src/app/leaves-mgmt/leaves-mgmt.module.ts");
/* harmony import */ var _leaves_mgmt_leaves_sanction_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../leaves-mgmt/leaves-sanction/leaves-sanction.component */ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.ts");
/* harmony import */ var _leaves_mgmt_leaves_curtail_leaves_curtail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../leaves-mgmt/leaves-curtail/leaves-curtail.component */ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.ts");
/* harmony import */ var _leaves_mgmt_leaves_cancell_leaves_cancell_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../leaves-mgmt/leaves-cancell/leaves-cancell.component */ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.ts");
/* harmony import */ var _leaves_mgmt_test_test_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../leaves-mgmt/test/test.component */ "./src/app/leaves-mgmt/test/test.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [{
        path: '', component: _leaves_mgmt_module__WEBPACK_IMPORTED_MODULE_2__["LeavesMgmtModule"], children: [
            {
                path: 'leavessanction', component: _leaves_mgmt_leaves_sanction_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_3__["LeavesSanctionComponent"], data: {
                    breadcrumb: 'LeavesSanction'
                }
            },
            {
                path: 'leavescurtail', component: _leaves_mgmt_leaves_curtail_leaves_curtail_component__WEBPACK_IMPORTED_MODULE_4__["LeavesCurtailComponent"], data: {
                    breadcrumb: 'LeavesCurtail'
                }
            },
            {
                path: 'leavescancell', component: _leaves_mgmt_leaves_cancell_leaves_cancell_component__WEBPACK_IMPORTED_MODULE_5__["LeavesCancellComponent"], data: {
                    breadcrumb: 'LeavesCancell'
                }
            },
            {
                path: 'leavestest', component: _leaves_mgmt_test_test_component__WEBPACK_IMPORTED_MODULE_6__["TestComponent"], data: {
                    breadcrumb: 'Leavestest'
                }
            },
        ]
    }];
var LeavesMgmtRoutingModule = /** @class */ (function () {
    function LeavesMgmtRoutingModule() {
    }
    LeavesMgmtRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], LeavesMgmtRoutingModule);
    return LeavesMgmtRoutingModule;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-mgmt.module.ts":
/*!***************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-mgmt.module.ts ***!
  \***************************************************/
/*! exports provided: LeavesMgmtModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesMgmtModule", function() { return LeavesMgmtModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _leaves_mgmt_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./leaves-mgmt-routing.module */ "./src/app/leaves-mgmt/leaves-mgmt-routing.module.ts");
/* harmony import */ var _leaves_sanction_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./leaves-sanction/leaves-sanction.component */ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.ts");
/* harmony import */ var _leaves_curtail_leaves_curtail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./leaves-curtail/leaves-curtail.component */ "./src/app/leaves-mgmt/leaves-curtail/leaves-curtail.component.ts");
/* harmony import */ var _leaves_cancell_leaves_cancell_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./leaves-cancell/leaves-cancell.component */ "./src/app/leaves-mgmt/leaves-cancell/leaves-cancell.component.ts");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./test/test.component */ "./src/app/leaves-mgmt/test/test.component.ts");
/* harmony import */ var _common_leaves_sanction_common_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./common-leaves-sanction/common-leaves-sanction.component */ "./src/app/leaves-mgmt/common-leaves-sanction/common-leaves-sanction.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var ng2_order_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-order-pipe */ "./node_modules/ng2-order-pipe/dist/index.js");
/* harmony import */ var ng2_order_pipe__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ng2_order_pipe__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















//import { Ng2SearchPipeModule } from '@angular/core/npm_package.es6/';
var LeavesMgmtModule = /** @class */ (function () {
    function LeavesMgmtModule() {
    }
    LeavesMgmtModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_leaves_sanction_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_4__["LeavesSanctionComponent"], _leaves_curtail_leaves_curtail_component__WEBPACK_IMPORTED_MODULE_5__["LeavesCurtailComponent"], _leaves_cancell_leaves_cancell_component__WEBPACK_IMPORTED_MODULE_6__["LeavesCancellComponent"], _test_test_component__WEBPACK_IMPORTED_MODULE_7__["TestComponent"],
                _common_leaves_sanction_common_leaves_sanction_component__WEBPACK_IMPORTED_MODULE_8__["CommonLeavesSanctionComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _leaves_mgmt_routing_module__WEBPACK_IMPORTED_MODULE_3__["LeavesMgmtRoutingModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_10__["MaterialModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_11__["MatTooltipModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_12__["Ng2SearchPipeModule"],
                ng2_order_pipe__WEBPACK_IMPORTED_MODULE_13__["Ng2OrderModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_14__["NgxPaginationModule"]
                //MatTableModule
            ]
        })
    ], LeavesMgmtModule);
    return LeavesMgmtModule;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\ntextarea {\r\n  max-height: 35px!important;\r\n}\r\n.padLeave thead tr {\r\n  height: 56px;\r\n}\r\n.padLeave tbody tr {\r\n  height: 48px;\r\n}\r\n.padLeave td {\r\n  }\r\n.padLeave th {\r\n    background: #e3e3e3;\r\n  }\r\n.padLeave td:first-of-type, th.mat-header-cell:first-of-type {\r\n  padding-left: 24px;\r\n}\r\n.tbl-2 {\r\n  width: 20% !important;\r\n  padding: 10px 5px 10px 8px !important;\r\n}\r\n.td-padding {\r\n  padding: 10px 5px 10px 8px !important;\r\n}\r\ntr .tbl-2:first-child {\r\npadding-left:14px !important;}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGVhdmVzLW1nbXQvbGVhdmVzLXNhbmN0aW9uL2xlYXZlcy1zYW5jdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTtFQUNFLDJCQUEyQjtDQUM1QjtBQUNEO0VBQ0UsYUFBYTtDQUNkO0FBQ0Q7RUFDRSxhQUFhO0NBQ2Q7QUFDQztHQUNDO0FBQ0Q7SUFDRSxvQkFBb0I7R0FDckI7QUFHSDtFQUNFLG1CQUFtQjtDQUNwQjtBQUVEO0VBQ0Usc0JBQXNCO0VBQ3RCLHNDQUFzQztDQUN2QztBQUNEO0VBQ0Usc0NBQXNDO0NBQ3ZDO0FBQ0Q7QUFDQSw2QkFBNkIsQ0FBQyIsImZpbGUiOiJzcmMvYXBwL2xlYXZlcy1tZ210L2xlYXZlcy1zYW5jdGlvbi9sZWF2ZXMtc2FuY3Rpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG50ZXh0YXJlYSB7XHJcbiAgbWF4LWhlaWdodDogMzVweCFpbXBvcnRhbnQ7XHJcbn1cclxuLnBhZExlYXZlIHRoZWFkIHRyIHtcclxuICBoZWlnaHQ6IDU2cHg7XHJcbn1cclxuLnBhZExlYXZlIHRib2R5IHRyIHtcclxuICBoZWlnaHQ6IDQ4cHg7XHJcbn1cclxuICAucGFkTGVhdmUgdGQge1xyXG4gIH1cclxuICAucGFkTGVhdmUgdGgge1xyXG4gICAgYmFja2dyb3VuZDogI2UzZTNlMztcclxuICB9XHJcblxyXG5cclxuLnBhZExlYXZlIHRkOmZpcnN0LW9mLXR5cGUsIHRoLm1hdC1oZWFkZXItY2VsbDpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDI0cHg7XHJcbn1cclxuXHJcbi50YmwtMiB7XHJcbiAgd2lkdGg6IDIwJSAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDEwcHggNXB4IDEwcHggOHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnRkLXBhZGRpbmcge1xyXG4gIHBhZGRpbmc6IDEwcHggNXB4IDEwcHggOHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxudHIgLnRibC0yOmZpcnN0LWNoaWxkIHtcclxucGFkZGluZy1sZWZ0OjE0cHggIWltcG9ydGFudDt9XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-desig-emp (desigchange)=\"designationChange()\" (onchange)=\"GetLeavesSanction($event)\"></app-desig-emp>\r\n<!--<app-desig-emp (onchange)=\"GetLeavesSanction($event)\" [data]=\"callTypevar\"></app-desig-emp>-->\r\n<div class=\"col-md-12 col-lg-7\">\r\n  <form [formGroup]=\"leaveSanctionCreationForm\" #sanctionCreationForm=\"ngForm\" ngNativeValidate (ngSubmit)=\"SaveLeavesSanction()\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Sanction Order Details</div>\r\n\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Sanction Order No\" formControlName=\"orderNo\" (keypress)=\"validateOrdNo($event)\">\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"DOB\" readonly placeholder=\"Sanction Order Date\" formControlName=\"orderDT\" [max]=\"currentdate\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOB\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOB></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"fom-title\">Leave Details</div>\r\n      <div class=\"col-md-12 col-lg-6 combo-col\">\r\n\r\n        <div class=\"col-md-12 col-lg-3 pading-0\">\r\n          <label>Leave Type</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-9\">\r\n          <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"leaveCategory\">\r\n            <mat-radio-button [checked]='radiochecked' value=\"Single\" class=\"ml-10\">Single</mat-radio-button>\r\n            <mat-radio-button value=\"Combined\" class=\"ml-10\">Combined</mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select matNativeControl placeholder=\"Select Leave Reason\" formControlName=\"leaveReasonCD\">\r\n            <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"leave-wraper\" formArrayName=\"leaveSanctionDetails\" *ngFor=\"let item1 of leaveSanctionCreationForm.get('leaveSanctionDetails')['controls']; let i=index\">\r\n        <div [formGroupName]=\"i\">\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"leaveCd\">\r\n                <mat-option *ngFor=\"let item of ArrddlLeavesType; let j=index\" (click)=\"LeaveTypeItemChange(i,item.leaveMaxAtATime);\" [value]=\"item.leaveTypeLeaveCd\">{{item.leaveTypeLeaveDesc}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"FDate\" readonly placeholder=\"From Date\" formControlName=\"fromDT\" (dateChange)=\"fillnoofDays('s',i)\" />\r\n              <mat-datepicker-toggle matSuffix [for]=\"FDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #FDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"TDate\" readonly placeholder=\"To Date\" formControlName=\"toDT\" [min]=\"item1.get('fromDT').value\" (dateChange)=\"fillnoofDays('e',i)\" />\r\n              <!--(input)=\"alert('saga')\" onclick=\"alert('saga')\">-->\r\n              <mat-datepicker-toggle matSuffix [for]=\"TDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #TDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Max at a time\" type=\"number\" formControlName=\"maxLeave\" readonly>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"No. of Days\" type=\"number\" formControlName=\"noDays\" readonly>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-6 col-lg-4\" *ngIf=\"leaveSanctionCreationForm && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls.length > 0 && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.leaveCd.value=='CHL'\">\r\n            <mat-checkbox formControlName=\"isMaxLeave\">Leave period more than 365 days</mat-checkbox>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\" *ngIf=\"leaveSanctionCreationForm && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls.length > 0 && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.leaveCd.value=='CHL' && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.isMaxLeave.value\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"80% pay entitled w.e.f date\" [matDatepicker]=\"PayEntDT\" formControlName=\"payEntDT\" />\r\n              <mat-datepicker-toggle matSuffix [for]=\"PayEntDT\"></mat-datepicker-toggle>\r\n              <mat-datepicker #PayEntDT></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\" *ngIf=\"leaveSanctionCreationForm && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls.length > 0 && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.leaveCd.value=='CL'\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"isHalfDay\">\r\n                <mat-option *ngFor=\"let item of group \" [value]=\"item.id\" (click)=\"ChangeDayChange()\">{{item.name}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md12 col-lg-4\" *ngIf=\"leaveSanctionCreationForm && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls.length > 0 && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.leaveCd.value=='CL' && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.isHalfDay.value\">\r\n            <mat-radio-group (change)=\"ChangeTime($event.value)\" formControlName=\"isAfterNoon\">\r\n              <mat-radio-button [checked]='radioafterchecked' value=\"ForeNoon\" class=\"ml-10\">ForeNoon</mat-radio-button>\r\n              <mat-radio-button value=\"AfterNoon\" class=\"ml-10\">AfterNoon</mat-radio-button>\r\n            </mat-radio-group>\r\n\r\n          </div>\r\n          <!--<div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-checkbox formControlName=\"isHalfPay\" *ngIf=\"leaveSanctionCreationForm && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls.length > 0 && leaveSanctionCreationForm.controls.leaveSanctionDetails.controls[i].controls.leaveCd.value=='CL'\">Half Day</mat-checkbox>\r\n          </div>-->\r\n          <div class=\"col-md-12 col-lg-12 text-right\" *ngIf=\"UserRoleId=='9' && (showDeleteButton||  leaveSanctionCreationForm.controls.leaveCategory.value=='Combined')\">\r\n            <a (click)=\"DeleteRow(i)\" class=\"btn btn-danger delete-leave-btn\" matTooltip=\"Delete\"><i class=\"material-icons\">delete_forever</i></a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-right\" *ngIf=\"showAddButton\">\r\n        <a (click)=\"AddRow()\" class=\"btn btn-success add-leave-btn \" matTooltip=\"Add\"><i class=\"material-icons\">playlist_add</i></a>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <textarea rows=\"5\" placeholder=\"Enter remarks (if any)\" class=\"wid-100\" formControlName=\"remarks\"></textarea>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 combo-col\">\r\n\r\n        <div class=\"col-md-7 col-lg-7 pading-0\">\r\n          <label>Is there any recovery to be made from the current month's salary?</label>\r\n        </div>\r\n        <div class=\"col-md-5 col-lg-5\">\r\n          <mat-radio-group (change)=\"IsAnyRecovery($event.value)\" formControlName=\"isRecovery\">\r\n            <mat-radio-button value=\"Yes\" class=\"ml-10\">Yes</mat-radio-button>\r\n            <mat-radio-button [checked]='radiochecked' value=\"No\" class=\"ml-10\">No</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"showReturnReason && UserRoleId=='9'\" class=\"col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Returned Reason\" formControlName=\"returnReason\" readonly>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"submit\" class=\"btn btn-success\" (click)=\"checkValidation()\" [disabled]=\"leaveSanctionCreationForm.invalid || showSave\">Save</button>\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-warning\" (click)=\"ResetLeavesSanction()\" [disabled]=\"showCancel\">Cancel</button>\r\n        <button *ngIf=\"UserRoleId=='9'\" type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToChecker();\" [disabled]=\"leaveSanctionCreationForm.invalid || showForward\">Save And Forward</button>\r\n        <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-success\" (click)=\"VerifiedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveSanctionCreationForm.invalid || showSave\">Verify</button>\r\n        <button *ngIf=\"UserRoleId=='8'\" type=\"button\" class=\"btn btn-danger\" (click)=\"RejectedSanc(false);hooCheckerLogin = !hooCheckerLogin\" [disabled]=\"leaveSanctionCreationForm.invalid || showSave\">Return</button>\r\n      </div>\r\n\r\n    </mat-card>\r\n  </form>\r\n</div>\r\n\r\n\r\n\r\n\r\n<!--right table-->\r\n\r\n<div class=\"col-md-12 col-lg-5\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Leave Sanction Details</div>\r\n\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <table class=\"w-100 tab-tr-bg\">\r\n        <tr>\r\n          <th class=\"tbl-2\">Order No.</th>\r\n          <th class=\"tbl-2\">Order Date</th>\r\n          <th class=\"tbl-2\">Action</th>\r\n        </tr>\r\n      </table>\r\n      <mat-accordion class=\"accordion-wrap\">\r\n        <mat-expansion-panel *ngFor=\"let item of dataSourceforgrid\">\r\n          <!--<mat-expansion-panel *ngFor=\"let item of dataSourceforgrid | filter:searchText | orderBy: key : reverse | paginate: { itemsPerPage: 5, currentPage: p };\">-->\r\n          <mat-expansion-panel-header class=\"td-padding\">\r\n            <mat-panel-title class=\"col-md-3 tbl-2\" (click)=\"sortk('orderNo')\">\r\n              {{item.orderNo}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4 tbl-2\" (click)=\"sortk('orderDT')\">\r\n              {{item.orderDT | date:'dd/MM/yyyy'}}\r\n            </mat-panel-title>\r\n            <mat-panel-title class=\"col-md-4 tbl-2\">\r\n              <div>\r\n                <!--<div *ngIf=\"UserRoleId=='8'\">-->\r\n                <a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"DissableLeaves(item)\">error</a>\r\n              </div>\r\n              <div *ngIf=\"UserRoleId=='9'\">\r\n                <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditLeavesSanction(item)\">edit</a>\r\n              </div>\r\n              <div *ngIf=\"UserRoleId=='9'\">\r\n                <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteLeavesSanction(item.leaveMainId,true);deletepopup = !deletepopup\">delete_forever </a>\r\n              </div>\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <table class=\"mat-elevation-z8 mat-table padLeave even-odd-color\">\r\n            <thead>\r\n              <tr class=\"mat-header-row ng-star-inserted\">\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n                <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leaveSanctionDetails\">\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.leaveCdDesc}}\r\n                </td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n                <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                  {{item1.toDT | date:'dd/MM/yyyy'}}\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </mat-expansion-panel>\r\n\r\n\r\n      </mat-accordion>\r\n\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n      <!--<pagination-controls (pageChange)=\"p = $event\"></pagination-controls>-->\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData($event)\" showFirstLastButtons></mat-paginator>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-12 text-center btn-wraper mb-20\">\r\n    <button mat-button class=\"btn-primary\" (click)=\"ShowHistorygrid(true)\">\r\n      View History\r\n    </button>\r\n  </div>\r\n  <div [hidden]=\"!ShowHistory\">\r\n    <div>\r\n      <div class=\"example-card mat-card\">\r\n        <div class=\"fom-title\">\r\n          Leave Sanction History\r\n          <i _ngcontent-c18=\"\" class=\"material-icons close-icon\" (click)=\"ShowHistorygrid(false)\"> clear </i>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-12\">\r\n          <table class=\"w-100 tab-tr-bg\">\r\n            <tr>\r\n              <th class=\"tbl-2\">Order No.</th>\r\n              <th class=\"tbl-2\">Order Date</th>\r\n              <th class=\"tbl-2\">Status</th>\r\n            </tr>\r\n          </table>\r\n          <mat-accordion class=\"accordion-wrap\">\r\n            <mat-expansion-panel *ngFor=\"let item of dataSourceforhistorygrid \">\r\n              <mat-expansion-panel-header class=\"td-padding\">\r\n                <mat-panel-title class=\"col-md-3 tbl-2\">\r\n                  {{item.orderNo}}\r\n                </mat-panel-title>\r\n                <mat-panel-title class=\"col-md-4 tbl-2\">\r\n                  {{item.orderDT | date:'dd/MM/yyyy'}}\r\n                </mat-panel-title>\r\n                <mat-panel-title class=\"col-md-4 tbl-2\">\r\n                  {{item.verifyFlg}}\r\n                </mat-panel-title>\r\n\r\n\r\n              </mat-expansion-panel-header>\r\n              <table class=\"mat-elevation-z8 mat-table padLeave even-odd-color\">\r\n                <thead>\r\n                  <tr class=\"mat-header-row ng-star-inserted\">\r\n                    <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> Leave Type</th>\r\n                    <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> From Date </th>\r\n                    <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> To Date</th>\r\n                    <th class=\"mat-header-cell cdk-column-position mat-column-position ng-star-inserted\"> No Of Days</th>\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr class=\"mat-row ng-star-inserted\" *ngFor=\"let item1 of item.leaveSanctionDetails\">\r\n                    <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                      {{item1.leaveCdDesc}}\r\n                    </td>\r\n                    <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">  {{item1.fromDT | date:'dd/MM/yyyy'}}</td>\r\n                    <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                      {{item1.toDT | date:'dd/MM/yyyy'}}\r\n                    </td>\r\n                    <td class=\"mat-cell cdk-column-position mat-column-position ng-star-inserted\">\r\n                      {{item1.noDays}}\r\n                    </td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </mat-expansion-panel>\r\n\r\n\r\n          </mat-accordion>\r\n\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <mat-paginator #b [pageSizeOptions]=\"[5, 10]\" (page)=\"getPaginationData1($event)\" showFirstLastButtons></mat-paginator>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <app-dialog [(visible)]=\"deletepopup\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n      <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\" DeleteLeavesSanction(leaveMainId,false)\">Delete</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </app-dialog>\r\n  <app-dialog [(visible)]=\"hooCheckerLogin\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 *ngIf=\"isVarified\" class=\"card-title text-center\">Are you sure you want to verify?</h3>\r\n      <h3 *ngIf=\"!isVarified\" class=\"card-title text-center\">Are you sure you want to reject?</h3>\r\n      <div *ngIf=\"!isVarified\">\r\n        <mat-card>\r\n          <input matInput type=\"text\" placeholder=\"Reason\" [(ngModel)]=\"reasonText\" />\r\n        </mat-card>\r\n        <label *ngIf=\"isReasonEmpty\" class=\"errormsg\">Please enter the reason</label>\r\n      </div>\r\n      <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"isVarified\" (click)=\"VerifiedSanc(true)\">YES</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" *ngIf=\"!isVarified\" (click)=\"RejectedSanc(true)\">YES</button>\r\n        <!--<button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">Close</button>-->\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"hooCheckerLogin = !hooCheckerLogin\">No</button>\r\n      </div>\r\n    </div>\r\n  </app-dialog>\r\n\r\n  <app-dialog [(visible)]=\"cclType\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <!--<h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>-->\r\n\r\n      <label class=\"card-title text-center\">Total CCL availed till date 31.12.2019:</label>\r\n      <mat-card>\r\n        <input matInput type=\"text\" placeholder=\"Total CCL availed\" [(ngModel)]=\"totalLeave\" />\r\n      </mat-card>\r\n      <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"saveTotalLeave()\">Save</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"cclType = !cclType\">Cancel</button>\r\n      </div>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.ts ***!
  \**************************************************************************/
/*! exports provided: LeavesSanctionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeavesSanctionComponent", function() { return LeavesSanctionComponent; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA;
var ELEMENT_DATA1;
var LeavesSanctionComponent = /** @class */ (function () {
    function LeavesSanctionComponent(datePipe, _Service, snackBar, formBuilder) {
        this.datePipe = datePipe;
        this._Service = _Service;
        this.snackBar = snackBar;
        this.formBuilder = formBuilder;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](ELEMENT_DATA);
        this.dataSourceforgrid = [];
        this.dataSourceHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](ELEMENT_DATA1);
        this.dataSourceforhistorygrid = [];
        this.ShowHistory = false;
        this.key = 'name'; //set default
        this.reverse = false;
        //p: number = 1;
        this.currentdate = new Date();
        this.showAddButton = false;
        this.showDeleteButton = false;
        this.cclFlag = false;
        this.showReturnReason = false;
        this.radiochecked = true;
        this.radioafterchecked = true;
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.UserRoleId = sessionStorage.getItem('userRoleID');
        this.showCancel = true;
        this.showForward = true;
        this.showSave = true;
        this.leavecomb = 0;
        this.group = [
            { id: true, name: 'HalfDay' },
            { id: false, name: 'FullDay' }
        ];
        this.createForm();
    }
    LeavesSanctionComponent.prototype.ngOnInit = function () {
        debugger;
        this.BindDropDownLeavesReason();
        this.BindDropDownLeavesType();
        this.leaveSanctionCreationForm.disable();
        this.isVarified = true;
        this.isReasonEmpty = false;
        this.ShowHistory = false;
    };
    //Search Form
    LeavesSanctionComponent.prototype.designationChange = function () {
        debugger;
        this.leaveSanctionCreationForm.disable();
        this.createForm();
        this.showCancel = true;
        this.showForward = true;
        this.showSave = true;
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.showReturnReason = false;
        this.leaveSanctionCreationForm.disable();
        this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionCreationForm.get('isRecovery').setValue("No");
        this.dataSource = null;
        this.ShowHistory = false;
        this.dataSourceforgrid = null;
        this.dataSourceforhistorygrid = null;
    };
    LeavesSanctionComponent.prototype.sortk = function (key) {
        this.key = key;
        this.reverse = !this.reverse;
    };
    LeavesSanctionComponent.prototype.GetLeavesSanction = function (value) {
        var _this = this;
        this.employeeCode = value;
        if (this.UserRoleId == "9") {
            this.leaveSanctionCreationForm.enable();
        }
        else {
            //this.leaveSanctionCreationForm.disable();
            //this.buttonText = "Verify";
        }
        this.createForm();
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.showReturnReason = false;
        this.ShowHistory = false;
        this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionCreationForm.get('isRecovery').setValue("No");
        //this.leaveSanctionCreationForm.get('orderNo').enable();
        this._Service.GetLeavesSanction(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceforgrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData(event);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        });
        this._Service.GetLeavesSanctionHistory(this.PermDdoId, value, this.UserRoleId).subscribe(function (result) {
            debugger;
            _this.alldataSourceforhistorygrid = result;
            var event = { pageIndex: 0, pageSize: 5 };
            _this.getPaginationData1(event);
            _this.dataSourceHistory = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](result);
            _this.dataSourceHistory.paginator = _this.paginator1;
            _this.dataSourceHistory.sort = _this.sort1;
        });
    };
    Object.defineProperty(LeavesSanctionComponent.prototype, "getleaveDetails", {
        get: function () {
            return this.leaveSanctionCreationForm.get('leaveSanctionDetails');
        },
        enumerable: true,
        configurable: true
    });
    //operational Form
    LeavesSanctionComponent.prototype.createForm = function () {
        this.leaveSanctionCreationForm = this.formBuilder.group({
            empCd: [this.employeeCode],
            empName: [null],
            permDDOId: [this.PermDdoId],
            orderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            orderDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            leaveCategory: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            isRecovery: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            leaveReasonCD: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            returnReason: [null],
            remarks: [null],
            verifyFlg: [null],
            leaveReasonDesc: [null],
            leaveMainId: [0],
            leaveSanctionDetails: this.formBuilder.array([this.createItem()])
        });
    };
    LeavesSanctionComponent.prototype.createItem = function () {
        return this.formBuilder.group({
            leaveCd: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            fromDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            toDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            payEntDT: [null],
            noDays: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            maxLeave: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            isMaxLeave: [false],
            isHalfDay: [false],
            isAfterNoon: [null]
        });
    };
    LeavesSanctionComponent.prototype.BindDropDownLeavesReason = function () {
        var _this = this;
        this._Service.GetLeavesReason().subscribe(function (data) {
            _this.ArrddlReason = data;
        });
    };
    LeavesSanctionComponent.prototype.BindDropDownLeavesType = function () {
        var _this = this;
        debugger;
        this._Service.GetLeavesType().subscribe(function (data) {
            _this.ArrddlLeavesType = data;
        });
    };
    LeavesSanctionComponent.prototype.LeaveTypeItemChange = function (i, value) {
        var _this = this;
        debugger;
        this.totalLeave = "";
        if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('leaveCd').value == 'CCL') {
            this._Service.GetLeavesTypeBalance(this.employeeCode).subscribe(function (result) {
                debugger;
                if (result.length == 0) {
                    _this.cclType = true;
                }
                else {
                    _this.cclType = false;
                }
                // if (result != 0) {  }
            });
        }
        //else { this.cclType = false; }
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value);
    };
    LeavesSanctionComponent.prototype.ChangeLeaveCategory = function (value) {
        debugger;
        this.showDeleteButton = value == 'Single' ? false : true;
        this.showAddButton = value == 'Single' ? false : true;
        if (value == 'Single') {
            while (this.leaveSanctionCreationForm.get('leaveSanctionDetails').controls.length - 1) {
                this.leaveSanctionCreationForm.get('leaveSanctionDetails').removeAt(0);
            }
        }
    };
    LeavesSanctionComponent.prototype.IsAnyRecovery = function (value) { };
    LeavesSanctionComponent.prototype.ChangeTime = function (value) {
    };
    LeavesSanctionComponent.prototype.ChangeDayChange = function () {
    };
    LeavesSanctionComponent.prototype.getPaginationData = function (event) {
        this.dataSourceforgrid = this.alldataSourceforgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesSanctionComponent.prototype.getPaginationData1 = function (event) {
        this.dataSourceforhistorygrid = this.alldataSourceforhistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
    };
    LeavesSanctionComponent.prototype.fillnoofDays = function (value, index) {
        debugger;
        if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('fromDT').value != null && this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('toDT').value != null) {
            debugger;
            this.firstdate = this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('fromDT').value;
            this.lastdate = this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('toDT').value;
            if (this.firstdate > this.lastdate) {
                alert('From date should be less than to date');
                this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('noDays').setValue(0);
                value == "s" ? this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('fromDT').setValue("") : this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('toDT').setValue("");
            }
            else {
                this.datedif = this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('toDT').value.diff(this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('fromDT').value, 'days');
                this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(index).get('noDays').setValue(this.datedif + 1);
            }
        }
    };
    LeavesSanctionComponent.prototype.AddRow = function () {
        debugger;
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').push(this.createItem());
    };
    LeavesSanctionComponent.prototype.DeleteRow = function (value) {
        debugger;
        if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').controls.length == 1) {
            alert('cant delete this row');
        }
        else {
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').removeAt(value);
        }
    };
    LeavesSanctionComponent.prototype.SaveLeavesSanction = function () {
        var _this = this;
        debugger;
        if (this.UserRoleId == "9") {
            this._Service.SaveLeavesSanction(this.leaveSanctionCreationForm.value, "I").subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Saved Successfully', null, { duration: 4000 });
                    _this.leaveSanctionCreationForm.reset();
                    _this.sanctionCreation.resetForm();
                    _this.GetLeavesSanction(_this.employeeCode);
                }
                else {
                    _this.snackBar.open('Save not Successfull', null, { duration: 4000 });
                }
            });
        }
        else if (this.UserRoleId == "8") {
        }
    };
    LeavesSanctionComponent.prototype.VerifiedSanc = function (flag) {
        var _this = this;
        if (flag) {
            this._Service.VerifyReturnSanction(this.leaveSanctionCreationForm.get('leaveMainId').value, "V", "Sanct", null).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Verified Successfully', null, { duration: 4000 });
                    _this.leaveSanctionCreationForm.reset();
                    _this.sanctionCreation.resetForm();
                    _this.GetLeavesSanction(_this.employeeCode);
                }
                else {
                    _this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
                }
            });
            //$('.dialog__close-btn').click();
            this.hooCheckerLogin = false;
        }
        else {
            this.isVarified = true;
        }
    };
    LeavesSanctionComponent.prototype.RejectedSanc = function (flag) {
        var _this = this;
        debugger;
        if (flag) {
            this.isReasonEmpty = false;
            if (this.reasonText.trim() === '') {
                this.isReasonEmpty = true;
                return false;
            }
            this._Service.VerifyReturnSanction(this.leaveSanctionCreationForm.get('leaveMainId').value, "R", "Sanct", this.reasonText).subscribe(function (result) {
                debugger;
                if (parseInt(result) >= 1) {
                    _this.snackBar.open('Returned Successfully', null, { duration: 4000 });
                    _this.leaveSanctionCreationForm.reset();
                    _this.sanctionCreation.resetForm();
                    _this.GetLeavesSanction(_this.employeeCode);
                    _this.reasonText = null;
                    _this.hooCheckerLogin = false;
                    //$('.dialog__close-btn').click();
                }
                else {
                    _this.snackBar.open('Return not Successfull', null, { duration: 4000 });
                }
            });
        }
        else {
            this.isVarified = false;
            return false;
        }
        this.reasonText = "";
    };
    LeavesSanctionComponent.prototype.saveTotalLeave = function () {
        var _this = this;
        debugger;
        //if (flag) {
        this._Service.SaveTotalLeaveAvailed(this.employeeCode, this.totalLeave).subscribe(function (result) {
            debugger;
            if (parseInt(result) >= 1) {
                _this.snackBar.open('Saved Successfully', null, { duration: 4000 });
                //this.leaveSanctionCreationForm.reset();
                //this.sanctionCreation.resetForm();
                //this.GetLeavesSanction(this.employeeCode);
            }
            else {
                _this.snackBar.open('Save not Successfull', null, { duration: 4000 });
            }
        });
        //$('.dialog__close-btn').click();
        this.cclType = false;
        //} else {
        //  this.isVarified = true;
        //}
    };
    //DeleteSuspensionDetails(value) {
    //    this._Service.DeleteLeavesSanction(value, 'Sanc').subscribe((result: any) => {
    //      this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
    //      this.GetLeavesSanction(this.employeeCode);
    //    });
    //  $('.dialog__close-btn').click();
    //}
    LeavesSanctionComponent.prototype.ForwardToChecker = function () {
        var _this = this;
        debugger;
        this.leavecomb = 0;
        this.clsclrh = "";
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').controls.forEach(function (control, index) {
            if (control.get('leaveCd').value == 'CL') {
                _this.leavecomb = _this.leavecomb + 1;
                _this.clsclrh = _this.clsclrh + "CL" + "*";
            }
            else if (control.get('leaveCd').value == 'EL') {
                _this.leavecomb = _this.leavecomb + 1;
            }
            else if (control.get('leaveCd').value == 'RH') {
                _this.clsclrh = _this.clsclrh + "RH" + "*";
            }
            else if (control.get('leaveCd').value == 'SCL') {
                _this.clsclrh = _this.clsclrh + "SCL" + "*";
            }
        });
        if (this.clsclrh.split("*").includes("CL")) {
            if (!((this.clsclrh.split("*").includes("RH")) || (this.clsclrh.split("*").includes("SCL")))) {
                alert('Please check the leave type.CL can only be clubbed with Special CL and RH');
                return false;
            }
        }
        if (this.leavecomb >= 2) {
            alert('Please check the leave type.CL cant be combined with EL');
            return false;
        }
        //if user selected combined type leave but fill only one leave type 
        if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Combined") {
            if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').length == 1) {
                alert('Please select single leave Type');
                return false;
            }
        }
        else if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Single") {
            if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').length > 1) {
                alert('Please select combined leave Type');
                return false;
            }
        }
        this._Service.SaveLeavesSanction(this.leaveSanctionCreationForm.value, "F").subscribe(function (result) {
            if (parseInt(result) >= 1) {
                _this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
                _this.leaveSanctionCreationForm.reset();
                _this.sanctionCreation.resetForm();
                _this.GetLeavesSanction(_this.employeeCode);
            }
            else {
                _this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
            }
        });
    };
    LeavesSanctionComponent.prototype.ReturnSanction = function () { };
    LeavesSanctionComponent.prototype.ResetLeavesSanction = function () {
        this.leaveSanctionCreationForm.reset();
        this.sanctionCreation.resetForm();
        //this.createForm();
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.showReturnReason = false;
        this.leaveSanctionCreationForm.get('leaveMainId').setValue(0);
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(0).get('isMaxLeave').setValue(false);
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(0).get('isHalfDay').setValue(false);
        this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
        this.leaveSanctionCreationForm.get('isRecovery').setValue("No");
    };
    LeavesSanctionComponent.prototype.EditLeavesSanction = function (value) {
        debugger;
        this.leaveSanctionCreationForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([]));
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.radioafterchecked = false;
        this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
        this.showAddButton = value.leaveCategory == 'Single' ? false : true;
        this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionCreationForm.get('leaveMainId').setValue(value.leaveMainId);
        this.leaveSanctionCreationForm.get('orderDT').setValue(value.orderDT);
        this.leaveSanctionCreationForm.get('orderNo').setValue(value.orderNo);
        this.leaveSanctionCreationForm.get('leaveCategory').setValue(value.leaveCategory);
        this.leaveSanctionCreationForm.get('isRecovery').setValue(value.isRecovery);
        this.leaveSanctionCreationForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
        if (value.returnReason != null) {
            this.showReturnReason = true;
            this.leaveSanctionCreationForm.get('returnReason').setValue(value.returnReason);
        }
        else {
            this.showReturnReason = false;
        }
        this.leaveSanctionCreationForm.get('remarks').setValue(value.remarks);
        for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('payEntDT').setValue(value.leaveSanctionDetails[i].payEntDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isMaxLeave').setValue(value.leaveSanctionDetails[i].isMaxLeave);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isHalfDay').setValue(value.leaveSanctionDetails[i].isHalfDay);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isAfterNoon').setValue(value.leaveSanctionDetails[i].isAfterNoon);
        }
        this.leaveSanctionCreationForm.enable();
    };
    LeavesSanctionComponent.prototype.ShowHistorygrid = function (isShow) {
        debugger;
        this.ShowHistory = isShow;
    };
    LeavesSanctionComponent.prototype.DissableLeaves = function (value) {
        debugger;
        this.leaveSanctionCreationForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([]));
        this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionCreationForm.get('leaveMainId').setValue(value.leaveMainId);
        this.leaveSanctionCreationForm.get('orderDT').setValue(value.orderDT);
        this.leaveSanctionCreationForm.get('orderNo').setValue(value.orderNo);
        this.leaveSanctionCreationForm.get('leaveCategory').setValue(value.leaveCategory);
        this.leaveSanctionCreationForm.get('isRecovery').setValue(value.isRecovery);
        this.leaveSanctionCreationForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
        if (value.returnReason != null) {
            this.showReturnReason = true;
            this.leaveSanctionCreationForm.get('returnReason').setValue(value.returnReason);
        }
        else {
            this.showReturnReason = false;
        }
        this.leaveSanctionCreationForm.get('remarks').setValue(value.remarks);
        for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('payEntDT').setValue(value.leaveSanctionDetails[i].payEntDT);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isMaxLeave').setValue(value.leaveSanctionDetails[i].isMaxLeave);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isHalfDay').setValue(value.leaveSanctionDetails[i].isHalfDay);
            this.leaveSanctionCreationForm.get('leaveSanctionDetails').at(i).get('isAfterNoon').setValue(value.leaveSanctionDetails[i].isAfterNoon);
        }
        this.leaveSanctionCreationForm.disable();
        if (this.UserRoleId == "9") {
            this.showCancel = true;
            this.showForward = true;
            this.showSave = true;
        }
        else {
            this.showSave = false;
        }
        this.radioafterchecked = false;
        //this.buttonText = "Verify";
        this.showDeleteButton = false;
        this.showAddButton = false;
    };
    LeavesSanctionComponent.prototype.DeleteLeavesSanction = function (leaveMainId, flag) {
        var _this = this;
        debugger;
        this.leaveMainId = flag ? leaveMainId : "0";
        if (!flag) {
            this._Service.DeleteLeavesSanction(leaveMainId, 'Sanc').subscribe(function (result) {
                _this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
                _this.GetLeavesSanction(_this.employeeCode);
            });
        }
        this.deletepopup = false;
    };
    LeavesSanctionComponent.prototype.checkValidation = function () {
        var _this = this;
        debugger;
        //CL and EL cant be combined.
        this.leavecomb = 0;
        this.clsclrh = "";
        this.leaveSanctionCreationForm.get('leaveSanctionDetails').controls.forEach(function (control, index) {
            if (control.get('leaveCd').value == 'CL') {
                _this.leavecomb = _this.leavecomb + 1;
                _this.clsclrh = _this.clsclrh + "CL" + "*";
            }
            else if (control.get('leaveCd').value == 'EL') {
                _this.leavecomb = _this.leavecomb + 1;
            }
            else if (control.get('leaveCd').value == 'RH') {
                _this.clsclrh = _this.clsclrh + "RH" + "*";
            }
            else if (control.get('leaveCd').value == 'SCL') {
                _this.clsclrh = _this.clsclrh + "SCL" + "*";
            }
        });
        if (this.clsclrh.split("*").includes("CL")) {
            if (!((this.clsclrh.split("*").includes("RH")) || (this.clsclrh.split("*").includes("SCL")))) {
                alert('Please check the leave type.CL can only be clubbed with Special CL and RH');
                return false;
            }
        }
        if (this.leavecomb >= 2) {
            alert('Please check the leave type.CL cant be combined with EL');
            return false;
        }
        //if user selected combined type leave but fill only one leave type 
        if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Combined") {
            if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').length == 1) {
                alert('Please select single leave Type');
                return false;
            }
        }
        else if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Single") {
            if (this.leaveSanctionCreationForm.get('leaveSanctionDetails').length > 1) {
                alert('Please select combined leave Type');
                return false;
            }
        }
    };
    LeavesSanctionComponent.prototype.validateOrdNo = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48)) {
            return true;
        }
        return false;
    };
    LeavesSanctionComponent.prototype.abc = function () {
        alert(this.findInvalidControls());
    };
    //Extra methods
    LeavesSanctionComponent.prototype.findInvalidControls = function () {
        debugger;
        var invalid = [];
        var controls = this.leaveSanctionCreationForm.controls;
        for (var name_1 in controls) {
            if (controls[name_1].invalid) {
                invalid.push(name_1);
            }
        }
        return invalid;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], LeavesSanctionComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], LeavesSanctionComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('b'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], LeavesSanctionComponent.prototype, "paginator1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('a'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], LeavesSanctionComponent.prototype, "sort1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sanctionCreationForm'),
        __metadata("design:type", Object)
    ], LeavesSanctionComponent.prototype, "sanctionCreation", void 0);
    LeavesSanctionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-leaves-sanction',
            template: __webpack_require__(/*! ./leaves-sanction.component.html */ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.html"),
            styles: [__webpack_require__(/*! ./leaves-sanction.component.css */ "./src/app/leaves-mgmt/leaves-sanction/leaves-sanction.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_0__["DatePipe"], _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_4__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], LeavesSanctionComponent);
    return LeavesSanctionComponent;
}());



/***/ }),

/***/ "./src/app/leaves-mgmt/test/test.component.css":
/*!*****************************************************!*\
  !*** ./src/app/leaves-mgmt/test/test.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".breadcrumb {\r\n  padding: 8px 15px;\r\n  margin-bottom: 20px;\r\n  list-style: none;\r\n  background-color: #000;\r\n  border-radius: 4px;\r\n}\r\n\r\n.basic-container {\r\n  /*padding: 5px;*/\r\n  padding: 0px;\r\n}\r\n\r\n.version-info {\r\n  font-size: 8pt;\r\n  float: right;\r\n}\r\n\r\n/*Header*/\r\n\r\n.head-bg {\r\n  /*background: #292c5f;*/\r\n  background: #3268ab;\r\n  padding: 7px 0;\r\n  box-shadow: 0px 6px 2px -3px rgba(0,0,0,0.75);\r\n}\r\n\r\n.logo {\r\n  width: 50%;\r\n  margin-top: 14px;\r\n  padding: 0 16px;\r\n  float: left;\r\n}\r\n\r\n.logo img {\r\n    float: left;\r\n    margin-right: 17px;\r\n  }\r\n\r\n.logo h1 {\r\n    color: #fff;\r\n    font-size: 170%;\r\n    font-family: initial;\r\n    font-weight: 700;\r\n    margin: auto;\r\n    text-transform: uppercase;\r\n    margin-top: 5px;\r\n  }\r\n\r\n.logo span {\r\n    color: #fff;\r\n    font-size: 132%;\r\n    margin-top: 6px;\r\n    float: left;\r\n  }\r\n\r\n.emblem img {\r\n  max-width: 50px;\r\n  float: left;\r\n}\r\n\r\n/*left nav*/\r\n\r\n/* .mat-sidenav {  background: #021c4d;} */\r\n\r\n.mat-sidenav {\r\n  width: 300px;\r\n  /*background-image: url('./assets/bgg.jpg');  \r\n  border: none;*/\r\n}\r\n\r\n.mat-sidenav:before {\r\n    content: \"\";\r\n    display: block;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    /*background-color: #3d3f6f;*/\r\n    background-color: #4c4c4c !important;\r\n    opacity: 0.9;\r\n  }\r\n\r\n.mat-sidenav-container {\r\n  height: 100vh;\r\n}\r\n\r\n.sidenav-toolbar {\r\n  height: 64px;\r\n  display: flex;\r\n  flex-direction: row;\r\n}\r\n\r\n.sidebar {\r\n  width: 260px;\r\n  position: fixed;\r\n}\r\n\r\n.main-panel {\r\n  position: relative;\r\n  float: right;\r\n  width: calc(100% - 260px);\r\n  transition: all .5s cubic-bezier(.685,.0473,.346,1);\r\n}\r\n\r\n.app-nav-menu > li {\r\n  float: left;\r\n}\r\n\r\n.mat-list-base .mat-list-item {\r\n  color: #fff;\r\n}\r\n\r\n.mat-list-item-content .mat-icon {\r\n  margin-right: 4px;\r\n  font-size: 18px;\r\n  margin-top: 7px;\r\n}\r\n\r\n.mat-drawer-container, .mat-drawer {\r\n  padding-top: 0px !important;\r\n}\r\n\r\n/*Main Page(Dashboard)\r\n.main-background {\r\n  background-image: url('./assets/main-bg.jpg');\r\n  height: 100vh;\r\n  background-size: cover;\r\n}*/\r\n\r\n/*Cards 28-11-18*/\r\n\r\n.card {\r\n  background: #fff;\r\n  border-radius: 6px;\r\n  box-shadow: 0 1px 4px 0 rgba(0,0,0,.14);\r\n  margin-top: 30px;\r\n  padding: 0 20px 20px 20px;\r\n}\r\n\r\n.card .card-icon {\r\n    border-radius: 3px;\r\n    background-color: #999;\r\n    padding: 25px;\r\n    margin-top: -20px;\r\n    margin-right: 15px;\r\n    float: left;\r\n    box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(255,152,0,.4);\r\n  }\r\n\r\n.card .card-header-warning .card-icon {\r\n    background: linear-gradient(60deg,#ffa726,#fb8c00);\r\n  }\r\n\r\n.card .card-header-success .card-icon {\r\n    background: linear-gradient(60deg,#66bb6a,#43a047);\r\n  }\r\n\r\n.card .card-header-danger .card-icon {\r\n    background: linear-gradient(60deg,#ef5350,#e53935);\r\n  }\r\n\r\n.card .card-header-info .card-icon {\r\n    background: linear-gradient(60deg,#26c6da,#00acc1);\r\n  }\r\n\r\n.card .card-icon i {\r\n    color: #fff\r\n  }\r\n\r\n.card-icon i {\r\n  font-size: 36px;\r\n}\r\n\r\n.card p.card-category {\r\n  text-align: right;\r\n  font-size: 14px;\r\n  color: #999;\r\n  padding-top: 14px;\r\n}\r\n\r\n.card h3 {\r\n  text-align: right;\r\n  color: #999;\r\n  margin: 0;\r\n}\r\n\r\n.card-footer {\r\n  border-top: 1px solid #eee;\r\n  margin-top: 20px;\r\n}\r\n\r\n.card-footer .stats {\r\n    color: #999;\r\n    font-size: 12px;\r\n  }\r\n\r\n.card .card-footer .stats .material-icons {\r\n  position: relative;\r\n  top: 4px;\r\n  font-size: 16px;\r\n  padding-top: 10px;\r\n}\r\n\r\n/*Chart*/\r\n\r\n.graph-body {\r\n  background: #fff;\r\n  border: 1px solid #ddd;\r\n  border-radius: 6px;\r\n  box-shadow: 0 1px 4px 0 rgba(0,0,0,.14);\r\n  padding: 10px;\r\n}\r\n\r\n.mt-40 {\r\n  margin-top: 40px;\r\n}\r\n\r\n.mat-drawer-content {\r\n  /*background-image: url('./assets/main-bg.jpg');\r\n  height: 100vh;\r\n  background-size: cover;*/\r\n  background: #f7f7f7;\r\n}\r\n\r\n.breadcrumb {\r\n  background-color: #dadada !important;\r\n  border-radius: 0 !important;\r\n  border-left: 10px solid #323232;\r\n}\r\n\r\n/*15/jan/18*/\r\n\r\n.mat-step-header {\r\n  width: 220px;\r\n  float: left;\r\n  border-right: 1px solid #e4e4e4;\r\n}\r\n\r\n.mat-step-header:hover {\r\n    background-color: #cfcdd8;\r\n  }\r\n\r\n.mat-step.ng-tns-c11-2.ng-star-inserted {\r\n  width: 100%;\r\n  float: left;\r\n}\r\n\r\n.mat-vertical-content-container {\r\n  width: calc(100% - 270px);\r\n  margin-left: 0 !important;\r\n  top: 93px;\r\n  position: absolute !important;\r\n  left: 270px;\r\n}\r\n\r\n.detail_one {\r\n  width: 55%;\r\n  float: left;\r\n  max-width: 100% !important;\r\n}\r\n\r\n.detail_two {\r\n  width: 42%;\r\n  float: left;\r\n  margin: 0 !important;\r\n}\r\n\r\n.w-100 {\r\n  width: 100%;\r\n}\r\n\r\n.mat-stepper-vertical-line::before {\r\n  border-left-style: none !important;\r\n}\r\n\r\n.mr-25 {\r\n  margin-right: 25px !important;\r\n}\r\n\r\n/*31/jan/19*/\r\n\r\n.btn-bg {\r\n  background: #e7e8ef;\r\n  border: 1px solid #e4dcdc;\r\n}\r\n\r\n.mat-vertical-stepper-header {\r\n  padding: 17px 25px !important;\r\n}\r\n\r\n/*04-2-19*/\r\n\r\n.wid-100 {\r\n  width: 100% !important;\r\n}\r\n\r\n.mb-15 {\r\n  margin-bottom: 15px;\r\n}\r\n\r\n.fom-title {\r\n  color: #fff;\r\n  margin-bottom: 15px;\r\n  font-weight: 700;\r\n  border-bottom: 1px solid #adadad;\r\n  padding-bottom: 10px;\r\n  background: #5e5e5e;\r\n  padding: 10px;\r\n  clear: both;\r\n  width: 100%;\r\n  /*margin-top: 15px;*/\r\n  float: left;\r\n}\r\n\r\n.combo-col {\r\n  padding-top: 20px;\r\n  padding-bottom: 6px;\r\n}\r\n\r\n.mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #cccccc !important;\r\n  width: 100%;\r\n  float: left;\r\n  background: #fff;\r\n  padding: 0px !important;\r\n}\r\n\r\n.select-lbl {\r\n  margin-top: 18px;\r\n}\r\n\r\n.selection-hed {\r\n  background-color: #e4e4e4;\r\n  border-left: 6px solid #929292;\r\n  margin-bottom: 10px;\r\n  padding: 8px 0 6px 0;\r\n  border-top: 1px solid #bdb4b4;\r\n  border-right: 1px solid #bdb4b4;\r\n  border-bottom: 1px solid #bdb4b4;\r\n}\r\n\r\n.go-btn {\r\n  box-shadow: none !important;\r\n  margin-top: 10px !important;\r\n}\r\n\r\n.pading-0 {\r\n  padding: 0 !important;\r\n}\r\n\r\nmat-radio-group > mat-radio-button:nth-child(1) {\r\n  margin-right: 15px;\r\n}\r\n\r\n.radio_btn_gender mat-radio-group > mat-radio-button:nth-child(2) {\r\n  margin-right: 15px;\r\n}\r\n\r\n.mat-radio-label-content {\r\n  font-weight: normal;\r\n}\r\n\r\n.dialog__close-btn {\r\n  top: 12px !important;\r\n  right: 12px !important;\r\n}\r\n\r\n.i-info {\r\n  margin-right: 6px;\r\n  color: #696969;\r\n  font-size: 21px;\r\n}\r\n\r\n.i-edit {\r\n  margin-right: 6px;\r\n  color: #636363;\r\n  font-size: 21px;\r\n  cursor: pointer;\r\n}\r\n\r\n.i-delet {\r\n  color: #ef4b4b;\r\n  font-size: 23px;\r\n  cursor: pointer;\r\n}\r\n\r\n.i-activate {\r\n  color: #12ab33;\r\n  font-size: 21px;\r\n  cursor: pointer;\r\n  margin-right: 6px;\r\n  width: 21px;\r\n}\r\n\r\n.i-dactivate {\r\n  color: #ea1b1b;\r\n  font-size: 21px;\r\n  cursor: pointer;\r\n  margin-right: 6px;\r\n  width: 21px;\r\n}\r\n\r\n.i-activate:focus, .i-activate:hover {\r\n  text-decoration: none;\r\n}\r\n\r\n.mat-header-cell {\r\n  font-size: 15px;\r\n  font-weight: 600;\r\n  color: #0a0a0a;\r\n}\r\n\r\n.btn {\r\n  margin: 0 10px;\r\n  min-width: 64px;\r\n}\r\n\r\n.btn-wraper {\r\n  margin-top: 35px;\r\n}\r\n\r\n/*22-2-19*/\r\n\r\n.gender-mr {\r\n  margin-right: 15px;\r\n}\r\n\r\n.popupbox .mat-form-field-infix {\r\n  width: 100% !important;\r\n}\r\n\r\n.even-odd-color tr:nth-child(even) {\r\n  background: #e9f4fb;\r\n}\r\n\r\n.border-non {\r\n  border: none !important;\r\n}\r\n\r\n/*Check Box*/\r\n\r\n.checkbox .mat-list-text {\r\n  color: #000 !important;\r\n}\r\n\r\n.checkbox .mat-list-option {\r\n  display: inline-block !important;\r\n  width: auto !important;\r\n}\r\n\r\n.checkbox .mat-list-item-content-reverse {\r\n  padding: 0 5px !important;\r\n}\r\n\r\n/*14-3-19*/\r\n\r\n.declaration-fom {\r\n  width: 100%;\r\n  float: left;\r\n}\r\n\r\n.declaration-fom > ul > li {\r\n    display: flex;\r\n    margin-bottom: 12px;\r\n  }\r\n\r\nol.lower-roman > li {\r\n  list-style-type: lower-roman;\r\n  margin-bottom: 15px;\r\n}\r\n\r\nol.lower-roman {\r\n  margin-top: 10px;\r\n}\r\n\r\n.dispp-block {\r\n  display: block !important;\r\n}\r\n\r\n.declaration-fom > ul > li > mat-checkbox {\r\n  margin-right: 10px;\r\n}\r\n\r\n.declaration-fom span {\r\n  font-size: 17px;\r\n}\r\n\r\n.declaration-fom {\r\n  background: #f1fbf0;\r\n  border-style: dotted;\r\n  border-color: #d8ded7;\r\n}\r\n\r\n.combo-col-2 {\r\n  padding-top: 20px;\r\n  padding-bottom: 13px;\r\n}\r\n\r\n/*upload file*/\r\n\r\n.upload-file-label {\r\n  width: auto;\r\n  float: left;\r\n  margin-right: 5px;\r\n}\r\n\r\n.check-list-opt {\r\n  height: 7px !important;\r\n  clear: both !important;\r\n}\r\n\r\n.float-left {\r\n  float: left;\r\n}\r\n\r\n/*28-03-2019*/\r\n\r\n.select-type {\r\n  background: #e8f7de;\r\n  border: 1px #b3c5a6;\r\n  border-style: dashed;\r\n  padding: 7px 10px 0px 11px;\r\n  margin-bottom: 15px;\r\n}\r\n\r\n.icon-right {\r\n  position: absolute;\r\n}\r\n\r\n/*02-04-2019*/\r\n\r\n.even-odd-color {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n.mat-table {\r\n  width: 100%;\r\n}\r\n\r\n.mat-step.ng-star-inserted {\r\n  width: 100%;\r\n  float: left;\r\n}\r\n\r\n.tabel-wraper { /*max-height: 550px;width: 550px;*/\r\n  overflow: auto;\r\n  max-height: 410px;\r\n}\r\n\r\n.tabel-wraper table {\r\n    min-width: 500px;\r\n  }\r\n\r\n.tabel-wraper tr > td {\r\n    /*min-width: 105px;*/\r\n  }\r\n\r\n/* width */\r\n\r\n.tabel-wraper::-webkit-scrollbar {\r\n    height: 8px;\r\n    width: 8px;\r\n  }\r\n\r\n/* Track */\r\n\r\n.tabel-wraper::-webkit-scrollbar-track {\r\n    background: #f1f1f1;\r\n  }\r\n\r\n/* Handle */\r\n\r\n.tabel-wraper::-webkit-scrollbar-thumb {\r\n    background: #a8a8a8;\r\n  }\r\n\r\n/* Handle on hover */\r\n\r\n.tabel-wraper::-webkit-scrollbar-thumb:hover {\r\n      background: rgba(0,0,0,.2);\r\n    }\r\n\r\n.emp2-head {\r\n  width: 100%;\r\n  float: left;\r\n}\r\n\r\n/**/\r\n\r\n.peragraph-input {\r\n  width: auto;\r\n  float: left;\r\n  padding: 16px 4px 10px 0px;\r\n}\r\n\r\n@media only screen and (max-width: 1627px) and (min-width: 1580px) {\r\n  .peragraph-input {\r\n    padding: 16px 9px 0 9px;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 1755px) and (min-width: 1628px) {\r\n  .peragraph-input {\r\n    padding: 16px 0 13px 0;\r\n  }\r\n}\r\n\r\n/*checkbox suspensiondetail*/\r\n\r\n.order-checkbox {\r\n  float: left;\r\n  padding: 0;\r\n  margin: 0px 0 6px 0 !important;\r\n}\r\n\r\n.attach-dattach-wraper {\r\n  margin: 25px 0;\r\n}\r\n\r\n.attach-btn {\r\n  background: #3c5b76;\r\n  color: #fff;\r\n  margin-right: 17px !important;\r\n}\r\n\r\n.dattach-btn {\r\n  background: #4cb39b;\r\n  color: #fff;\r\n}\r\n\r\n.pading-16 {\r\n  padding-top: 16px;\r\n}\r\n\r\n.display-non {\r\n  display: none;\r\n}\r\n\r\n/*form*/\r\n\r\n.form-wraper {\r\n  background: #edf0f1d9;\r\n}\r\n\r\n.breadcrumb > li > a {\r\n  color: #000;\r\n}\r\n\r\n/*Manoj pal 15-5-19*/\r\n\r\n.filter-option > .mat-pseudo-checkbox {\r\n  display: none !important;\r\n}\r\n\r\n/*Employee1*/\r\n\r\n.emp1-form {\r\n  max-height: 790px;\r\n  height: 790px;\r\n  /*background: #f0f7f6;*/\r\n}\r\n\r\n.emp1-table {\r\n  max-height: 790px;\r\n  height: 790px;\r\n}\r\n\r\n.delete-leave-btn {\r\n  padding: 2px 0 0 0 !important;\r\n  margin: auto;\r\n  min-width: 34px !important;\r\n  margin-bottom: 12px !important;\r\n}\r\n\r\n/*07-2-19*/\r\n\r\n.material-icons {\r\n  cursor: pointer;\r\n}\r\n\r\n/*13-3-19*/\r\n\r\n.leave-wraper {\r\n  width: 99%;\r\n  float: left;\r\n  background: #f6f7fd;\r\n  border: 1px solid #d9dcdb;\r\n  margin: 8px 5px;\r\n  padding: 5px 5px;\r\n}\r\n\r\n.add-leave-icon {\r\n  float: left;\r\n  margin-right: 11px;\r\n}\r\n\r\n.add-leave-btn {\r\n  padding: 2px 0 0 0 !important;\r\n  margin: auto;\r\n  min-width: 34px !important;\r\n  margin-bottom: 12px !important;\r\n  margin-right: 10px;\r\n  /*line-height: 23px !important;\r\n  margin-bottom: 15px !important;\r\n  margin-right: 0;\r\n  border: none !important;\r\n  background-color: #504b71 !important;\r\n  color: #fff !important;*/\r\n}\r\n\r\n.add-leave-btn:hover {\r\n    /*background: #746da0 !important;*/\r\n  }\r\n\r\n.text-right {\r\n  text-align: right;\r\n}\r\n\r\n.line-height-57 {\r\n  line-height: 57px;\r\n}\r\n\r\n/*Nav Active Hover*/\r\n\r\n.menu-list-item.mat-list-item.active {\r\n  background: #ffc954 !important;\r\n  z-index: 1111;\r\n}\r\n\r\n.select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n.select-lbl {\r\n  float: left;\r\n}\r\n\r\n.require-field-position {\r\n  top: 0;\r\n  position: absolute;\r\n  right: 71px;\r\n  font-size: 11px;\r\n}\r\n\r\n.commutation-table {\r\n  padding: 5px;\r\n  border: 1px solid #889492;\r\n}\r\n\r\n.commutation-table > tbody > tr {\r\n    line-height: 31px;\r\n  }\r\n\r\n.commutation-table > tbody > tr > td {\r\n      padding-left: 5px;\r\n    }\r\n\r\n.commutation-table > tbody > tr > td > th {\r\n        padding-left: 5px;\r\n      }\r\n\r\n.btn-search {\r\n  background: #28a745;\r\n  border: none;\r\n  color: white;\r\n  padding: 6px 12px 6px 12px;\r\n  font-size: 16px;\r\n  cursor: pointer;\r\n  border-radius: 5px;\r\n  margin-top: 6px;\r\n  margin-left: 18px;\r\n}\r\n\r\n.btn-search:hover {\r\n    background-color: #10902d;\r\n  }\r\n\r\n@media only screen and (max-width: 1727px) and (min-width: 1200px) {\r\n\r\n  .btn-search {\r\n    padding: 3px 8px 3px 8px;\r\n    font-size: 15px;\r\n    margin-top: 6px;\r\n    margin-left: 0;\r\n  }\r\n}\r\n\r\n/*17-06-19*/\r\n\r\n.add-del-btn {\r\n  padding: 0 !important;\r\n  margin-top: 10px;\r\n}\r\n\r\n.tabel-form > tbody > .table-head {\r\n  line-height: 34px;\r\n  background: #4671a9;\r\n  color: #fff;\r\n}\r\n\r\n.tabel-form > tbody > tr > th {\r\n  padding-left: 9px;\r\n}\r\n\r\n.tabel-form > tbody > tr > td {\r\n  line-height: 35px;\r\n  padding-left: 9px;\r\n  padding-right: 5px;\r\n}\r\n\r\n.tabel-form > tbody > tr:nth-child(even) {\r\n  background: #fff;\r\n}\r\n\r\n/**/\r\n\r\nlabel {\r\n  font-weight: normal !important;\r\n  font-size: inherit;\r\n  color: #7d7d7d;\r\n}\r\n\r\n/*20-06-2019*/\r\n\r\n.mat-list-base .mat-list-item {\r\n  font-size: 15px;\r\n}\r\n\r\n.action-single-btn {\r\n  text-align: center;\r\n  line-height: normal !important;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n\r\n/*25-06-2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.mb-20 {\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.margin-0 {\r\n  margin: 0px;\r\n}\r\n\r\nh4.card-title.text-center {\r\n  margin-bottom: 40px;\r\n}\r\n\r\n.margin-top-10px {\r\n  margin-top: 10px;\r\n}\r\n\r\n.tabel-total {\r\n  font-weight: 600;\r\n  font-size: 15px;\r\n}\r\n\r\n/*.recovery-tabel > tbody > tr > td:last-child {\r\n  text-align: center;\r\n}*/\r\n\r\n.upload-container {\r\n  margin-bottom: 19px;\r\n  margin-top: 10px;\r\n  padding-left: 0 !important;\r\n}\r\n\r\n/*SHOWING LOADING*/\r\n\r\n.LoadingBar {\r\n  position: fixed;\r\n  background-color: rgba(0,0,0,0.5);\r\n  z-index: 2147483647 !important;\r\n  opacity: 0.8;\r\n  overflow: hidden;\r\n  text-align: center;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%; /*padding-top: 20%;*/\r\n  /*background-image: url('../../Images/Loader.gif');*/\r\n  background-repeat: no-repeat;\r\n  background-position: center;\r\n  z-index: 10000000;\r\n  filter: alpha(opacity=40);\r\n}\r\n\r\n.LoadBusy {\r\n  position: fixed;\r\n  top: 0px;\r\n  right: 0px;\r\n  width: 100%;\r\n  height: 100%; /*background-color:#666;*/\r\n  /*background-image: url('../../Images/Loader.gif');*/\r\n  background-repeat: no-repeat;\r\n  background-position: center;\r\n  z-index: 10000000;\r\n  opacity: 0.4;\r\n  filter: alpha(opacity=40); /* For IE8 and earlier */\r\n}\r\n\r\n/*END OF SHOWING LOADING*/\r\n\r\n/*28-05-2019*/\r\n\r\n.view-btn {\r\n  background: #4299ed;\r\n  color: #fff;\r\n  padding: 6px 15px 6px 15px;\r\n  height: auto;\r\n  margin: 0;\r\n  line-height: normal;\r\n  border-radius: 4px;\r\n  border: none;\r\n}\r\n\r\n/*01-07-19*/\r\n\r\n.tbl-input > tbody > tr > td > input {\r\n  width: 100%;\r\n}\r\n\r\n.add-list {\r\n  background: #2ec48d !important;\r\n  border: none !important;\r\n}\r\n\r\n.remove-list {\r\n  background: #f35b21 !important;\r\n  border: none !important;\r\n}\r\n\r\n.add-list:hover {\r\n  background: #28aa7b !important;\r\n}\r\n\r\n.remove-list:hover {\r\n  background: #d44e1b !important;\r\n}\r\n\r\n.float-r {\r\n  float: right;\r\n}\r\n\r\n.dialog-wraper {\r\n}\r\n\r\n/*09-07-2019\r\n.login-left {height: 100vh;  background: #3268ab;}\r\n.login-emblem {\r\n  background-image: url('./assets/emblem-login.png');\r\n  background-repeat: no-repeat;\r\n  height: 294px;\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  margin: auto;\r\n  width:600px;\r\n}\r\n  */\r\n\r\n/*CSS edited on 26-07-2019 by Gulshan*/\r\n\r\n.breadcrumb > li + li:before {\r\n  color: #000 !important;\r\n}\r\n\r\nlabel {\r\n  color: #000 !important;\r\n}\r\n\r\n.selection-hed {\r\n  background-color: #f0f0f0;\r\n  border: 1px solid #dedede;\r\n  margin-bottom: 10px;\r\n  padding: 8px 0 6px 0;\r\n}\r\n\r\n.clearfix {\r\n  clear: both;\r\n  margin-bottom: 10px !important;\r\n}\r\n\r\n.pad10 {\r\n  margin: 10px !important;\r\n}\r\n\r\n.btnbg {\r\n  background-color: #e7e7e7;\r\n  padding: 12px;\r\n}\r\n\r\n.panel-header, .panel-header:active, .panel-header:hover, .panel-header:visited {\r\n  color: #fff !important;\r\n  background: #2468b3 !important;\r\n  padding: 10px !important;\r\n  padding-right: 20px !important;\r\n  height: auto !important;\r\n  font-size: 14px;\r\n}\r\n\r\n.panel-header:active {\r\n    background-color: #2468b3\r\n  }\r\n\r\n.panel-header:hover {\r\n    background-color: #2468b3 !important;\r\n  }\r\n\r\n.panel-header:visited {\r\n    background-color: #2468b3 !important;\r\n  }\r\n\r\n.mat-expansion-indicator::after {\r\n  color: #fff !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGVhdmVzLW1nbXQvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQix1QkFBdUI7RUFDdkIsbUJBQW1CO0NBQ3BCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGVBQWU7RUFDZixhQUFhO0NBQ2Q7O0FBRUQsVUFBVTs7QUFDVjtFQUNFLHdCQUF3QjtFQUN4QixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLDhDQUE4QztDQUMvQzs7QUFFRDtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLFlBQVk7Q0FDYjs7QUFFQztJQUNFLFlBQVk7SUFDWixtQkFBbUI7R0FDcEI7O0FBRUQ7SUFDRSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLHFCQUFxQjtJQUNyQixpQkFBaUI7SUFDakIsYUFBYTtJQUNiLDBCQUEwQjtJQUMxQixnQkFBZ0I7R0FDakI7O0FBRUQ7SUFDRSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixZQUFZO0dBQ2I7O0FBRUg7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtDQUNiOztBQUdELFlBQVk7O0FBQ1osMkNBQTJDOztBQUUzQztFQUNFLGFBQWE7RUFDYjtpQkFDZTtDQUNoQjs7QUFFQztJQUNFLFlBQVk7SUFDWixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLE9BQU87SUFDUCxRQUFRO0lBQ1IsU0FBUztJQUNULFVBQVU7SUFDViw4QkFBOEI7SUFDOUIscUNBQXFDO0lBQ3JDLGFBQWE7R0FDZDs7QUFFSDtFQUNFLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGFBQWE7RUFDYixjQUFjO0VBQ2Qsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsMEJBQTBCO0VBRTFCLG9EQUFvRDtDQUNyRDs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsNEJBQTRCO0NBQzdCOztBQU9EOzs7OztHQUtHOztBQUdILGtCQUFrQjs7QUFDbEI7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHdDQUF3QztFQUN4QyxpQkFBaUI7RUFDakIsMEJBQTBCO0NBQzNCOztBQUVDO0lBQ0UsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osNkVBQTZFO0dBQzlFOztBQUVEO0lBQ0UsbURBQW1EO0dBQ3BEOztBQUVEO0lBQ0UsbURBQW1EO0dBQ3BEOztBQUVEO0lBQ0UsbURBQW1EO0dBQ3BEOztBQUVEO0lBQ0UsbURBQW1EO0dBQ3BEOztBQUVEO0lBQ0UsV0FBVztHQUNaOztBQUVIO0VBQ0UsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osa0JBQWtCO0NBQ25COztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixVQUFVO0NBQ1g7O0FBRUQ7RUFDRSwyQkFBMkI7RUFDM0IsaUJBQWlCO0NBQ2xCOztBQUVDO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtHQUNqQjs7QUFFSDtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtDQUNuQjs7QUFFRCxTQUFTOztBQUNUO0VBQ0UsaUJBQWlCO0VBQ2pCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsd0NBQXdDO0VBQ3hDLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFOzsyQkFFeUI7RUFDekIsb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UscUNBQXFDO0VBQ3JDLDRCQUE0QjtFQUM1QixnQ0FBZ0M7Q0FDakM7O0FBRUQsYUFBYTs7QUFDYjtFQUNFLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0NBQWdDO0NBQ2pDOztBQUVDO0lBQ0UsMEJBQTBCO0dBQzNCOztBQUVIO0VBQ0UsWUFBWTtFQUNaLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsVUFBVTtFQUNWLDhCQUE4QjtFQUM5QixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1oscUJBQXFCO0NBQ3RCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsbUNBQW1DO0NBQ3BDOztBQUVEO0VBQ0UsOEJBQThCO0NBQy9COztBQUNELGFBQWE7O0FBQ2I7RUFDRSxvQkFBb0I7RUFDcEIsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsOEJBQThCO0NBQy9COztBQUVELFdBQVc7O0FBQ1g7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixpQ0FBaUM7RUFDakMscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2QsWUFBWTtFQUNaLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsWUFBWTtDQUNiOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGlCQUFpQjtFQUNqQixxQ0FBcUM7RUFDckMsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLCtCQUErQjtFQUMvQixvQkFBb0I7RUFDcEIscUJBQXFCO0VBQ3JCLDhCQUE4QjtFQUM5QixnQ0FBZ0M7RUFDaEMsaUNBQWlDO0NBQ2xDOztBQUVEO0VBQ0UsNEJBQTRCO0VBQzVCLDRCQUE0QjtDQUM3Qjs7QUFFRDtFQUNFLHNCQUFzQjtDQUN2Qjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLHFCQUFxQjtFQUNyQix1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxzQkFBc0I7Q0FDdkI7O0FBR0Q7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELFdBQVc7O0FBQ1g7RUFDRSxtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSx3QkFBd0I7Q0FDekI7O0FBRUQsYUFBYTs7QUFDYjtFQUNFLHVCQUF1QjtDQUN4Qjs7QUFFRDtFQUNFLGlDQUFpQztFQUNqQyx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSwwQkFBMEI7Q0FDM0I7O0FBSUQsV0FBVzs7QUFDWDtFQUNFLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBRUM7SUFDRSxjQUFjO0lBQ2Qsb0JBQW9CO0dBQ3JCOztBQUVIO0VBQ0UsNkJBQTZCO0VBQzdCLG9CQUFvQjtDQUNyQjs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLDBCQUEwQjtDQUMzQjs7QUFFRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFFRDtFQUNFLG9CQUFvQjtFQUNwQixxQkFBcUI7RUFDckIsc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQjtDQUN0Qjs7QUFDRCxlQUFlOztBQUNmO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSx1QkFBdUI7RUFDdkIsdUJBQXVCO0NBQ3hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxvQkFBb0I7RUFDcEIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtFQUNyQiwyQkFBMkI7RUFDM0Isb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztBQUVELGNBQWM7O0FBQ2Q7RUFDRSxvR0FBb0c7Q0FDckc7O0FBRUQ7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVELGdCQUFnQixtQ0FBbUM7RUFDakQsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjs7QUFFQztJQUNFLGlCQUFpQjtHQUNsQjs7QUFFRDtJQUNFLHFCQUFxQjtHQUN0Qjs7QUFFRCxXQUFXOztBQUNYO0lBQ0UsWUFBWTtJQUNaLFdBQVc7R0FDWjs7QUFFRCxXQUFXOztBQUNYO0lBQ0Usb0JBQW9CO0dBQ3JCOztBQUVELFlBQVk7O0FBQ1o7SUFDRSxvQkFBb0I7R0FDckI7O0FBRUMscUJBQXFCOztBQUNyQjtNQUNFLDJCQUEyQjtLQUM1Qjs7QUFFTDtFQUNFLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBQ0QsSUFBSTs7QUFDSjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0U7SUFDRSx3QkFBd0I7R0FDekI7Q0FDRjs7QUFFRDtFQUNFO0lBQ0UsdUJBQXVCO0dBQ3hCO0NBQ0Y7O0FBRUQsNkJBQTZCOztBQUM3QjtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsK0JBQStCO0NBQ2hDOztBQUVEO0VBQ0UsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLG9CQUFvQjtFQUNwQixZQUFZO0VBQ1osOEJBQThCO0NBQy9COztBQUVEO0VBQ0Usb0JBQW9CO0VBQ3BCLFlBQVk7Q0FDYjs7QUFFRDtFQUNFLGtCQUFrQjtDQUNuQjs7QUFFRDtFQUNFLGNBQWM7Q0FDZjs7QUFFRCxRQUFROztBQUNSO0VBQ0Usc0JBQXNCO0NBQ3ZCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUNELHFCQUFxQjs7QUFDckI7RUFDRSx5QkFBeUI7Q0FDMUI7O0FBRUQsYUFBYTs7QUFDYjtFQUNFLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2Qsd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsMkJBQTJCO0VBQzNCLCtCQUErQjtDQUNoQzs7QUFDRCxXQUFXOztBQUNYO0VBQ0UsZ0JBQWdCO0NBQ2pCOztBQUNELFdBQVc7O0FBQ1g7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQiwwQkFBMEI7RUFDMUIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtDQUNsQjs7QUFFRDtFQUNFLFlBQVk7RUFDWixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSw4QkFBOEI7RUFDOUIsYUFBYTtFQUNiLDJCQUEyQjtFQUMzQiwrQkFBK0I7RUFDL0IsbUJBQW1CO0VBQ25COzs7OzsyQkFLeUI7Q0FDMUI7O0FBRUM7SUFDRSxtQ0FBbUM7R0FDcEM7O0FBRUg7RUFDRSxrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxrQkFBa0I7Q0FDbkI7O0FBQ0Qsb0JBQW9COztBQUNwQjtFQUNFLCtCQUErQjtFQUMvQixjQUFjO0NBQ2Y7O0FBRUQ7RUFDRSxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0UsT0FBTztFQUNQLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsYUFBYTtFQUNiLDBCQUEwQjtDQUMzQjs7QUFFQztJQUNFLGtCQUFrQjtHQUNuQjs7QUFFQztNQUNFLGtCQUFrQjtLQUNuQjs7QUFFQztRQUNFLGtCQUFrQjtPQUNuQjs7QUFFUDtFQUNFLG9CQUFvQjtFQUNwQixhQUFhO0VBQ2IsYUFBYTtFQUNiLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0NBQ25COztBQUVDO0lBQ0UsMEJBQTBCO0dBQzNCOztBQUVIOztFQUVFO0lBQ0UseUJBQXlCO0lBQ3pCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZUFBZTtHQUNoQjtDQUNGOztBQUNELFlBQVk7O0FBQ1o7RUFDRSxzQkFBc0I7RUFDdEIsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0Usa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxrQkFBa0I7Q0FDbkI7O0FBRUQ7RUFDRSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtDQUNwQjs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFRCxJQUFJOztBQUNKO0VBQ0UsK0JBQStCO0VBQy9CLG1CQUFtQjtFQUNuQixlQUFlO0NBQ2hCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxnQkFBZ0I7Q0FDakI7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsK0JBQStCO0VBQy9CLFVBQVU7RUFDVixXQUFXO0NBQ1o7O0FBQ0QsY0FBYzs7QUFDZDtFQUNFLG1CQUFtQjtFQUNuQixZQUFZLENBQUMsNEJBQTRCO0VBQ3pDLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IsMkJBQTJCO0NBQzVCOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCOztBQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtDQUNqQjs7QUFFRDs7R0FFRzs7QUFDSDtFQUNFLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsMkJBQTJCO0NBQzVCOztBQUdELG1CQUFtQjs7QUFDbkI7RUFDRSxnQkFBZ0I7RUFDaEIsa0NBQWtDO0VBQ2xDLCtCQUErQjtFQUMvQixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixZQUFZLENBQUMscUJBQXFCO0VBQ2xDLHFEQUFxRDtFQUNyRCw2QkFBNkI7RUFDN0IsNEJBQTRCO0VBQzVCLGtCQUFrQjtFQUNsQiwwQkFBMEI7Q0FDM0I7O0FBRUQ7RUFDRSxnQkFBZ0I7RUFDaEIsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYSxDQUFDLDBCQUEwQjtFQUN4QyxxREFBcUQ7RUFDckQsNkJBQTZCO0VBQzdCLDRCQUE0QjtFQUM1QixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLDBCQUEwQixDQUFDLHlCQUF5QjtDQUNyRDs7QUFDRCwwQkFBMEI7O0FBRTFCLGNBQWM7O0FBQ2Q7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLDJCQUEyQjtFQUMzQixhQUFhO0VBQ2IsVUFBVTtFQUNWLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsYUFBYTtDQUNkOztBQUNELFlBQVk7O0FBRVo7RUFDRSxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSwrQkFBK0I7RUFDL0Isd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UsK0JBQStCO0VBQy9CLHdCQUF3QjtDQUN6Qjs7QUFFRDtFQUNFLCtCQUErQjtDQUNoQzs7QUFFRDtFQUNFLCtCQUErQjtDQUNoQzs7QUFFRDtFQUNFLGFBQWE7Q0FDZDs7QUFFRDtDQUNDOztBQUVEOzs7Ozs7Ozs7Ozs7OztJQWNJOztBQUVKLHVDQUF1Qzs7QUFFdkM7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSx1QkFBdUI7Q0FDeEI7O0FBRUQ7RUFDRSwwQkFBMEI7RUFDMUIsMEJBQTBCO0VBQzFCLG9CQUFvQjtFQUNwQixxQkFBcUI7Q0FDdEI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osK0JBQStCO0NBQ2hDOztBQUVEO0VBQ0Usd0JBQXdCO0NBQ3pCOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QiwrQkFBK0I7RUFDL0IseUJBQXlCO0VBQ3pCLCtCQUErQjtFQUMvQix3QkFBd0I7RUFDeEIsZ0JBQWdCO0NBQ2pCOztBQUVDO0lBQ0UseUJBQXlCO0dBQzFCOztBQUVEO0lBQ0UscUNBQXFDO0dBQ3RDOztBQUVEO0lBQ0UscUNBQXFDO0dBQ3RDOztBQUVIO0VBQ0UsdUJBQXVCO0NBQ3hCIiwiZmlsZSI6InNyYy9hcHAvbGVhdmVzLW1nbXQvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnJlYWRjcnVtYiB7XHJcbiAgcGFkZGluZzogOHB4IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4uYmFzaWMtY29udGFpbmVyIHtcclxuICAvKnBhZGRpbmc6IDVweDsqL1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuLnZlcnNpb24taW5mbyB7XHJcbiAgZm9udC1zaXplOiA4cHQ7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4vKkhlYWRlciovXHJcbi5oZWFkLWJnIHtcclxuICAvKmJhY2tncm91bmQ6ICMyOTJjNWY7Ki9cclxuICBiYWNrZ3JvdW5kOiAjMzI2OGFiO1xyXG4gIHBhZGRpbmc6IDdweCAwO1xyXG4gIGJveC1zaGFkb3c6IDBweCA2cHggMnB4IC0zcHggcmdiYSgwLDAsMCwwLjc1KTtcclxufVxyXG5cclxuLmxvZ28ge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgbWFyZ2luLXRvcDogMTRweDtcclxuICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbiAgLmxvZ28gaW1nIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxN3B4O1xyXG4gIH1cclxuXHJcbiAgLmxvZ28gaDEge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBmb250LXNpemU6IDE3MCU7XHJcbiAgICBmb250LWZhbWlseTogaW5pdGlhbDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gIH1cclxuXHJcbiAgLmxvZ28gc3BhbiB7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTMyJTtcclxuICAgIG1hcmdpbi10b3A6IDZweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gIH1cclxuXHJcbi5lbWJsZW0gaW1nIHtcclxuICBtYXgtd2lkdGg6IDUwcHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcblxyXG4vKmxlZnQgbmF2Ki9cclxuLyogLm1hdC1zaWRlbmF2IHsgIGJhY2tncm91bmQ6ICMwMjFjNGQ7fSAqL1xyXG5cclxuLm1hdC1zaWRlbmF2IHtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgLypiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4vYXNzZXRzL2JnZy5qcGcnKTsgIFxyXG4gIGJvcmRlcjogbm9uZTsqL1xyXG59XHJcblxyXG4gIC5tYXQtc2lkZW5hdjpiZWZvcmUge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgLypiYWNrZ3JvdW5kLWNvbG9yOiAjM2QzZjZmOyovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGM0YzRjICFpbXBvcnRhbnQ7XHJcbiAgICBvcGFjaXR5OiAwLjk7XHJcbiAgfVxyXG5cclxuLm1hdC1zaWRlbmF2LWNvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxufVxyXG5cclxuLnNpZGVuYXYtdG9vbGJhciB7XHJcbiAgaGVpZ2h0OiA2NHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLnNpZGViYXIge1xyXG4gIHdpZHRoOiAyNjBweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbi5tYWluLXBhbmVsIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyNjBweCk7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjVzIGN1YmljLWJlemllciguNjg1LC4wNDczLC4zNDYsMSk7XHJcbiAgdHJhbnNpdGlvbjogYWxsIC41cyBjdWJpYy1iZXppZXIoLjY4NSwuMDQ3MywuMzQ2LDEpO1xyXG59XHJcblxyXG4uYXBwLW5hdi1tZW51ID4gbGkge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4ubWF0LWxpc3QtYmFzZSAubWF0LWxpc3QtaXRlbSB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1pdGVtLWNvbnRlbnQgLm1hdC1pY29uIHtcclxuICBtYXJnaW4tcmlnaHQ6IDRweDtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgbWFyZ2luLXRvcDogN3B4O1xyXG59XHJcblxyXG4ubWF0LWRyYXdlci1jb250YWluZXIsIC5tYXQtZHJhd2VyIHtcclxuICBwYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuLypNYWluIFBhZ2UoRGFzaGJvYXJkKVxyXG4ubWFpbi1iYWNrZ3JvdW5kIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4vYXNzZXRzL21haW4tYmcuanBnJyk7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59Ki9cclxuXHJcblxyXG4vKkNhcmRzIDI4LTExLTE4Ki9cclxuLmNhcmQge1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDRweCAwIHJnYmEoMCwwLDAsLjE0KTtcclxuICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gIHBhZGRpbmc6IDAgMjBweCAyMHB4IDIwcHg7XHJcbn1cclxuXHJcbiAgLmNhcmQgLmNhcmQtaWNvbiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTk5O1xyXG4gICAgcGFkZGluZzogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCAyMHB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDdweCAxMHB4IC01cHggcmdiYSgyNTUsMTUyLDAsLjQpO1xyXG4gIH1cclxuXHJcbiAgLmNhcmQgLmNhcmQtaGVhZGVyLXdhcm5pbmcgLmNhcmQtaWNvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNjBkZWcsI2ZmYTcyNiwjZmI4YzAwKTtcclxuICB9XHJcblxyXG4gIC5jYXJkIC5jYXJkLWhlYWRlci1zdWNjZXNzIC5jYXJkLWljb24ge1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDYwZGVnLCM2NmJiNmEsIzQzYTA0Nyk7XHJcbiAgfVxyXG5cclxuICAuY2FyZCAuY2FyZC1oZWFkZXItZGFuZ2VyIC5jYXJkLWljb24ge1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDYwZGVnLCNlZjUzNTAsI2U1MzkzNSk7XHJcbiAgfVxyXG5cclxuICAuY2FyZCAuY2FyZC1oZWFkZXItaW5mbyAuY2FyZC1pY29uIHtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg2MGRlZywjMjZjNmRhLCMwMGFjYzEpO1xyXG4gIH1cclxuXHJcbiAgLmNhcmQgLmNhcmQtaWNvbiBpIHtcclxuICAgIGNvbG9yOiAjZmZmXHJcbiAgfVxyXG5cclxuLmNhcmQtaWNvbiBpIHtcclxuICBmb250LXNpemU6IDM2cHg7XHJcbn1cclxuXHJcbi5jYXJkIHAuY2FyZC1jYXRlZ29yeSB7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiAjOTk5O1xyXG4gIHBhZGRpbmctdG9wOiAxNHB4O1xyXG59XHJcblxyXG4uY2FyZCBoMyB7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgY29sb3I6ICM5OTk7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4uY2FyZC1mb290ZXIge1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZWVlO1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuXHJcbiAgLmNhcmQtZm9vdGVyIC5zdGF0cyB7XHJcbiAgICBjb2xvcjogIzk5OTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcblxyXG4uY2FyZCAuY2FyZC1mb290ZXIgLnN0YXRzIC5tYXRlcmlhbC1pY29ucyB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogNHB4O1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG5cclxuLypDaGFydCovXHJcbi5ncmFwaC1ib2R5IHtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDRweCAwIHJnYmEoMCwwLDAsLjE0KTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4ubXQtNDAge1xyXG4gIG1hcmdpbi10b3A6IDQwcHg7XHJcbn1cclxuXHJcbi5tYXQtZHJhd2VyLWNvbnRlbnQge1xyXG4gIC8qYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuL2Fzc2V0cy9tYWluLWJnLmpwZycpO1xyXG4gIGhlaWdodDogMTAwdmg7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsqL1xyXG4gIGJhY2tncm91bmQ6ICNmN2Y3Zjc7XHJcbn1cclxuXHJcbi5icmVhZGNydW1iIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGFkYWRhICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1sZWZ0OiAxMHB4IHNvbGlkICMzMjMyMzI7XHJcbn1cclxuXHJcbi8qMTUvamFuLzE4Ki9cclxuLm1hdC1zdGVwLWhlYWRlciB7XHJcbiAgd2lkdGg6IDIyMHB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNlNGU0ZTQ7XHJcbn1cclxuXHJcbiAgLm1hdC1zdGVwLWhlYWRlcjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZjZGQ4O1xyXG4gIH1cclxuXHJcbi5tYXQtc3RlcC5uZy10bnMtYzExLTIubmctc3Rhci1pbnNlcnRlZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5tYXQtdmVydGljYWwtY29udGVudC1jb250YWluZXIge1xyXG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyNzBweCk7XHJcbiAgbWFyZ2luLWxlZnQ6IDAgIWltcG9ydGFudDtcclxuICB0b3A6IDkzcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcbiAgbGVmdDogMjcwcHg7XHJcbn1cclxuXHJcbi5kZXRhaWxfb25lIHtcclxuICB3aWR0aDogNTUlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZGV0YWlsX3R3byB7XHJcbiAgd2lkdGg6IDQyJTtcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnctMTAwIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1zdGVwcGVyLXZlcnRpY2FsLWxpbmU6OmJlZm9yZSB7XHJcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1yLTI1IHtcclxuICBtYXJnaW4tcmlnaHQ6IDI1cHggIWltcG9ydGFudDtcclxufVxyXG4vKjMxL2phbi8xOSovXHJcbi5idG4tYmcge1xyXG4gIGJhY2tncm91bmQ6ICNlN2U4ZWY7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U0ZGNkYztcclxufVxyXG5cclxuLm1hdC12ZXJ0aWNhbC1zdGVwcGVyLWhlYWRlciB7XHJcbiAgcGFkZGluZzogMTdweCAyNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8qMDQtMi0xOSovXHJcbi53aWQtMTAwIHtcclxuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWItMTUge1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5mb20tdGl0bGUge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2FkYWRhZDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBiYWNrZ3JvdW5kOiAjNWU1ZTVlO1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgY2xlYXI6IGJvdGg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLyptYXJnaW4tdG9wOiAxNXB4OyovXHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5jb21iby1jb2wge1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA2cHg7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjY2NjICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnNlbGVjdC1sYmwge1xyXG4gIG1hcmdpbi10b3A6IDE4cHg7XHJcbn1cclxuXHJcbi5zZWxlY3Rpb24taGVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRlNGU0O1xyXG4gIGJvcmRlci1sZWZ0OiA2cHggc29saWQgIzkyOTI5MjtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDhweCAwIDZweCAwO1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjYmRiNGI0O1xyXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNiZGI0YjQ7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNiZGI0YjQ7XHJcbn1cclxuXHJcbi5nby1idG4ge1xyXG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICBtYXJnaW4tdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wYWRpbmctMCB7XHJcbiAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5tYXQtcmFkaW8tZ3JvdXAgPiBtYXQtcmFkaW8tYnV0dG9uOm50aC1jaGlsZCgxKSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xyXG59XHJcblxyXG4ucmFkaW9fYnRuX2dlbmRlciBtYXQtcmFkaW8tZ3JvdXAgPiBtYXQtcmFkaW8tYnV0dG9uOm50aC1jaGlsZCgyKSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xyXG59XHJcblxyXG4ubWF0LXJhZGlvLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbn1cclxuXHJcbi5kaWFsb2dfX2Nsb3NlLWJ0biB7XHJcbiAgdG9wOiAxMnB4ICFpbXBvcnRhbnQ7XHJcbiAgcmlnaHQ6IDEycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmktaW5mbyB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgY29sb3I6ICM2OTY5Njk7XHJcbiAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcblxyXG4uaS1lZGl0IHtcclxuICBtYXJnaW4tcmlnaHQ6IDZweDtcclxuICBjb2xvcjogIzYzNjM2MztcclxuICBmb250LXNpemU6IDIxcHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uaS1kZWxldCB7XHJcbiAgY29sb3I6ICNlZjRiNGI7XHJcbiAgZm9udC1zaXplOiAyM3B4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmktYWN0aXZhdGUge1xyXG4gIGNvbG9yOiAjMTJhYjMzO1xyXG4gIGZvbnQtc2l6ZTogMjFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgd2lkdGg6IDIxcHg7XHJcbn1cclxuXHJcbi5pLWRhY3RpdmF0ZSB7XHJcbiAgY29sb3I6ICNlYTFiMWI7XHJcbiAgZm9udC1zaXplOiAyMXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBtYXJnaW4tcmlnaHQ6IDZweDtcclxuICB3aWR0aDogMjFweDtcclxufVxyXG5cclxuLmktYWN0aXZhdGU6Zm9jdXMsIC5pLWFjdGl2YXRlOmhvdmVyIHtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcblxyXG4ubWF0LWhlYWRlci1jZWxsIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBjb2xvcjogIzBhMGEwYTtcclxufVxyXG5cclxuLmJ0biB7XHJcbiAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgbWluLXdpZHRoOiA2NHB4O1xyXG59XHJcblxyXG4uYnRuLXdyYXBlciB7XHJcbiAgbWFyZ2luLXRvcDogMzVweDtcclxufVxyXG4vKjIyLTItMTkqL1xyXG4uZ2VuZGVyLW1yIHtcclxuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbn1cclxuXHJcbi5wb3B1cGJveCAubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5ldmVuLW9kZC1jb2xvciB0cjpudGgtY2hpbGQoZXZlbikge1xyXG4gIGJhY2tncm91bmQ6ICNlOWY0ZmI7XHJcbn1cclxuXHJcbi5ib3JkZXItbm9uIHtcclxuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLypDaGVjayBCb3gqL1xyXG4uY2hlY2tib3ggLm1hdC1saXN0LXRleHQge1xyXG4gIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jaGVja2JveCAubWF0LWxpc3Qtb3B0aW9uIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcclxuICB3aWR0aDogYXV0byAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2hlY2tib3ggLm1hdC1saXN0LWl0ZW0tY29udGVudC1yZXZlcnNlIHtcclxuICBwYWRkaW5nOiAwIDVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuXHJcbi8qMTQtMy0xOSovXHJcbi5kZWNsYXJhdGlvbi1mb20ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4gIC5kZWNsYXJhdGlvbi1mb20gPiB1bCA+IGxpIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xyXG4gIH1cclxuXHJcbm9sLmxvd2VyLXJvbWFuID4gbGkge1xyXG4gIGxpc3Qtc3R5bGUtdHlwZTogbG93ZXItcm9tYW47XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufVxyXG5cclxub2wubG93ZXItcm9tYW4ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5kaXNwcC1ibG9jayB7XHJcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmRlY2xhcmF0aW9uLWZvbSA+IHVsID4gbGkgPiBtYXQtY2hlY2tib3gge1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmRlY2xhcmF0aW9uLWZvbSBzcGFuIHtcclxuICBmb250LXNpemU6IDE3cHg7XHJcbn1cclxuXHJcbi5kZWNsYXJhdGlvbi1mb20ge1xyXG4gIGJhY2tncm91bmQ6ICNmMWZiZjA7XHJcbiAgYm9yZGVyLXN0eWxlOiBkb3R0ZWQ7XHJcbiAgYm9yZGVyLWNvbG9yOiAjZDhkZWQ3O1xyXG59XHJcblxyXG4uY29tYm8tY29sLTIge1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxM3B4O1xyXG59XHJcbi8qdXBsb2FkIGZpbGUqL1xyXG4udXBsb2FkLWZpbGUtbGFiZWwge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG4uY2hlY2stbGlzdC1vcHQge1xyXG4gIGhlaWdodDogN3B4ICFpbXBvcnRhbnQ7XHJcbiAgY2xlYXI6IGJvdGggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZsb2F0LWxlZnQge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi8qMjgtMDMtMjAxOSovXHJcbi5zZWxlY3QtdHlwZSB7XHJcbiAgYmFja2dyb3VuZDogI2U4ZjdkZTtcclxuICBib3JkZXI6IDFweCAjYjNjNWE2O1xyXG4gIGJvcmRlci1zdHlsZTogZGFzaGVkO1xyXG4gIHBhZGRpbmc6IDdweCAxMHB4IDBweCAxMXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbi8qMDItMDQtMjAxOSovXHJcbi5ldmVuLW9kZC1jb2xvciB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMXB4IC0xcHggcmdiYSgwLDAsMCwuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwwLDAsLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLDAsMCwuMTIpO1xyXG59XHJcblxyXG4ubWF0LXRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1zdGVwLm5nLXN0YXItaW5zZXJ0ZWQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4udGFiZWwtd3JhcGVyIHsgLyptYXgtaGVpZ2h0OiA1NTBweDt3aWR0aDogNTUwcHg7Ki9cclxuICBvdmVyZmxvdzogYXV0bztcclxuICBtYXgtaGVpZ2h0OiA0MTBweDtcclxufVxyXG5cclxuICAudGFiZWwtd3JhcGVyIHRhYmxlIHtcclxuICAgIG1pbi13aWR0aDogNTAwcHg7XHJcbiAgfVxyXG5cclxuICAudGFiZWwtd3JhcGVyIHRyID4gdGQge1xyXG4gICAgLyptaW4td2lkdGg6IDEwNXB4OyovXHJcbiAgfVxyXG5cclxuICAvKiB3aWR0aCAqL1xyXG4gIC50YWJlbC13cmFwZXI6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIGhlaWdodDogOHB4O1xyXG4gICAgd2lkdGg6IDhweDtcclxuICB9XHJcblxyXG4gIC8qIFRyYWNrICovXHJcbiAgLnRhYmVsLXdyYXBlcjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgYmFja2dyb3VuZDogI2YxZjFmMTtcclxuICB9XHJcblxyXG4gIC8qIEhhbmRsZSAqL1xyXG4gIC50YWJlbC13cmFwZXI6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgIGJhY2tncm91bmQ6ICNhOGE4YTg7XHJcbiAgfVxyXG5cclxuICAgIC8qIEhhbmRsZSBvbiBob3ZlciAqL1xyXG4gICAgLnRhYmVsLXdyYXBlcjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWI6aG92ZXIge1xyXG4gICAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLC4yKTtcclxuICAgIH1cclxuXHJcbi5lbXAyLWhlYWQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi8qKi9cclxuLnBlcmFncmFwaC1pbnB1dCB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcGFkZGluZzogMTZweCA0cHggMTBweCAwcHg7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTYyN3B4KSBhbmQgKG1pbi13aWR0aDogMTU4MHB4KSB7XHJcbiAgLnBlcmFncmFwaC1pbnB1dCB7XHJcbiAgICBwYWRkaW5nOiAxNnB4IDlweCAwIDlweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTc1NXB4KSBhbmQgKG1pbi13aWR0aDogMTYyOHB4KSB7XHJcbiAgLnBlcmFncmFwaC1pbnB1dCB7XHJcbiAgICBwYWRkaW5nOiAxNnB4IDAgMTNweCAwO1xyXG4gIH1cclxufVxyXG5cclxuLypjaGVja2JveCBzdXNwZW5zaW9uZGV0YWlsKi9cclxuLm9yZGVyLWNoZWNrYm94IHtcclxuICBmbG9hdDogbGVmdDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMHB4IDAgNnB4IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmF0dGFjaC1kYXR0YWNoLXdyYXBlciB7XHJcbiAgbWFyZ2luOiAyNXB4IDA7XHJcbn1cclxuXHJcbi5hdHRhY2gtYnRuIHtcclxuICBiYWNrZ3JvdW5kOiAjM2M1Yjc2O1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbi1yaWdodDogMTdweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZGF0dGFjaC1idG4ge1xyXG4gIGJhY2tncm91bmQ6ICM0Y2IzOWI7XHJcbiAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5wYWRpbmctMTYge1xyXG4gIHBhZGRpbmctdG9wOiAxNnB4O1xyXG59XHJcblxyXG4uZGlzcGxheS1ub24ge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi8qZm9ybSovXHJcbi5mb3JtLXdyYXBlciB7XHJcbiAgYmFja2dyb3VuZDogI2VkZjBmMWQ5O1xyXG59XHJcblxyXG4uYnJlYWRjcnVtYiA+IGxpID4gYSB7XHJcbiAgY29sb3I6ICMwMDA7XHJcbn1cclxuLypNYW5vaiBwYWwgMTUtNS0xOSovXHJcbi5maWx0ZXItb3B0aW9uID4gLm1hdC1wc2V1ZG8tY2hlY2tib3gge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLypFbXBsb3llZTEqL1xyXG4uZW1wMS1mb3JtIHtcclxuICBtYXgtaGVpZ2h0OiA3OTBweDtcclxuICBoZWlnaHQ6IDc5MHB4O1xyXG4gIC8qYmFja2dyb3VuZDogI2YwZjdmNjsqL1xyXG59XHJcblxyXG4uZW1wMS10YWJsZSB7XHJcbiAgbWF4LWhlaWdodDogNzkwcHg7XHJcbiAgaGVpZ2h0OiA3OTBweDtcclxufVxyXG5cclxuLmRlbGV0ZS1sZWF2ZS1idG4ge1xyXG4gIHBhZGRpbmc6IDJweCAwIDAgMCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBtaW4td2lkdGg6IDM0cHggIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAxMnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLyowNy0yLTE5Ki9cclxuLm1hdGVyaWFsLWljb25zIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLyoxMy0zLTE5Ki9cclxuLmxlYXZlLXdyYXBlciB7XHJcbiAgd2lkdGg6IDk5JTtcclxuICBmbG9hdDogbGVmdDtcclxuICBiYWNrZ3JvdW5kOiAjZjZmN2ZkO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkOWRjZGI7XHJcbiAgbWFyZ2luOiA4cHggNXB4O1xyXG4gIHBhZGRpbmc6IDVweCA1cHg7XHJcbn1cclxuXHJcbi5hZGQtbGVhdmUtaWNvbiB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMXB4O1xyXG59XHJcblxyXG4uYWRkLWxlYXZlLWJ0biB7XHJcbiAgcGFkZGluZzogMnB4IDAgMCAwICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIG1pbi13aWR0aDogMzRweCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDEycHggIWltcG9ydGFudDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgLypsaW5lLWhlaWdodDogMjNweCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHggIWltcG9ydGFudDtcclxuICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzUwNGI3MSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7Ki9cclxufVxyXG5cclxuICAuYWRkLWxlYXZlLWJ0bjpob3ZlciB7XHJcbiAgICAvKmJhY2tncm91bmQ6ICM3NDZkYTAgIWltcG9ydGFudDsqL1xyXG4gIH1cclxuXHJcbi50ZXh0LXJpZ2h0IHtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuLmxpbmUtaGVpZ2h0LTU3IHtcclxuICBsaW5lLWhlaWdodDogNTdweDtcclxufVxyXG4vKk5hdiBBY3RpdmUgSG92ZXIqL1xyXG4ubWVudS1saXN0LWl0ZW0ubWF0LWxpc3QtaXRlbS5hY3RpdmUge1xyXG4gIGJhY2tncm91bmQ6ICNmZmM5NTQgIWltcG9ydGFudDtcclxuICB6LWluZGV4OiAxMTExO1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtbGJsIHtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLnJlcXVpcmUtZmllbGQtcG9zaXRpb24ge1xyXG4gIHRvcDogMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDcxcHg7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG59XHJcblxyXG4uY29tbXV0YXRpb24tdGFibGUge1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjODg5NDkyO1xyXG59XHJcblxyXG4gIC5jb21tdXRhdGlvbi10YWJsZSA+IHRib2R5ID4gdHIge1xyXG4gICAgbGluZS1oZWlnaHQ6IDMxcHg7XHJcbiAgfVxyXG5cclxuICAgIC5jb21tdXRhdGlvbi10YWJsZSA+IHRib2R5ID4gdHIgPiB0ZCB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgICAgLmNvbW11dGF0aW9uLXRhYmxlID4gdGJvZHkgPiB0ciA+IHRkID4gdGgge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICB9XHJcblxyXG4uYnRuLXNlYXJjaCB7XHJcbiAgYmFja2dyb3VuZDogIzI4YTc0NTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDZweCAxMnB4IDZweCAxMnB4O1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIG1hcmdpbi10b3A6IDZweDtcclxuICBtYXJnaW4tbGVmdDogMThweDtcclxufVxyXG5cclxuICAuYnRuLXNlYXJjaDpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTA5MDJkO1xyXG4gIH1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTcyN3B4KSBhbmQgKG1pbi13aWR0aDogMTIwMHB4KSB7XHJcblxyXG4gIC5idG4tc2VhcmNoIHtcclxuICAgIHBhZGRpbmc6IDNweCA4cHggM3B4IDhweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbi10b3A6IDZweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gIH1cclxufVxyXG4vKjE3LTA2LTE5Ki9cclxuLmFkZC1kZWwtYnRuIHtcclxuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuLnRhYmVsLWZvcm0gPiB0Ym9keSA+IC50YWJsZS1oZWFkIHtcclxuICBsaW5lLWhlaWdodDogMzRweDtcclxuICBiYWNrZ3JvdW5kOiAjNDY3MWE5O1xyXG4gIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4udGFiZWwtZm9ybSA+IHRib2R5ID4gdHIgPiB0aCB7XHJcbiAgcGFkZGluZy1sZWZ0OiA5cHg7XHJcbn1cclxuXHJcbi50YWJlbC1mb3JtID4gdGJvZHkgPiB0ciA+IHRkIHtcclxuICBsaW5lLWhlaWdodDogMzVweDtcclxuICBwYWRkaW5nLWxlZnQ6IDlweDtcclxuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi50YWJlbC1mb3JtID4gdGJvZHkgPiB0cjpudGgtY2hpbGQoZXZlbikge1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuXHJcbi8qKi9cclxubGFiZWwge1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWwgIWltcG9ydGFudDtcclxuICBmb250LXNpemU6IGluaGVyaXQ7XHJcbiAgY29sb3I6ICM3ZDdkN2Q7XHJcbn1cclxuLyoyMC0wNi0yMDE5Ki9cclxuLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LWl0ZW0ge1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLmFjdGlvbi1zaW5nbGUtYnRuIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcbi8qMjUtMDYtMjAxOSovXHJcbnNwYW4uYnRuLWxhYmVsIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogLTEycHg7IC8qIGRpc3BsYXk6IGlubGluZS1ibG9jazsgKi9cclxuICBwYWRkaW5nOiA4cHggMTJweDtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuMTUpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweCAwIDAgM3B4O1xyXG59XHJcblxyXG4ubWItMTAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5tYi0yMCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hcmdpbi0wIHtcclxuICBtYXJnaW46IDBweDtcclxufVxyXG5cclxuaDQuY2FyZC10aXRsZS50ZXh0LWNlbnRlciB7XHJcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxufVxyXG5cclxuLm1hcmdpbi10b3AtMTBweCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuLnRhYmVsLXRvdGFsIHtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLyoucmVjb3ZlcnktdGFiZWwgPiB0Ym9keSA+IHRyID4gdGQ6bGFzdC1jaGlsZCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59Ki9cclxuLnVwbG9hZC1jb250YWluZXIge1xyXG4gIG1hcmdpbi1ib3R0b206IDE5cHg7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi8qU0hPV0lORyBMT0FESU5HKi9cclxuLkxvYWRpbmdCYXIge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSk7XHJcbiAgei1pbmRleDogMjE0NzQ4MzY0NyAhaW1wb3J0YW50O1xyXG4gIG9wYWNpdHk6IDAuODtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7IC8qcGFkZGluZy10b3A6IDIwJTsqL1xyXG4gIC8qYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9JbWFnZXMvTG9hZGVyLmdpZicpOyovXHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgei1pbmRleDogMTAwMDAwMDA7XHJcbiAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTQwKTtcclxufVxyXG5cclxuLkxvYWRCdXN5IHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiAwcHg7XHJcbiAgcmlnaHQ6IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7IC8qYmFja2dyb3VuZC1jb2xvcjojNjY2OyovXHJcbiAgLypiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL0ltYWdlcy9Mb2FkZXIuZ2lmJyk7Ki9cclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICB6LWluZGV4OiAxMDAwMDAwMDtcclxuICBvcGFjaXR5OiAwLjQ7XHJcbiAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTQwKTsgLyogRm9yIElFOCBhbmQgZWFybGllciAqL1xyXG59XHJcbi8qRU5EIE9GIFNIT1dJTkcgTE9BRElORyovXHJcblxyXG4vKjI4LTA1LTIwMTkqL1xyXG4udmlldy1idG4ge1xyXG4gIGJhY2tncm91bmQ6ICM0Mjk5ZWQ7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogNnB4IDE1cHggNnB4IDE1cHg7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbjogMDtcclxuICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuLyowMS0wNy0xOSovXHJcblxyXG4udGJsLWlucHV0ID4gdGJvZHkgPiB0ciA+IHRkID4gaW5wdXQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uYWRkLWxpc3Qge1xyXG4gIGJhY2tncm91bmQ6ICMyZWM0OGQgIWltcG9ydGFudDtcclxuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnJlbW92ZS1saXN0IHtcclxuICBiYWNrZ3JvdW5kOiAjZjM1YjIxICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5hZGQtbGlzdDpob3ZlciB7XHJcbiAgYmFja2dyb3VuZDogIzI4YWE3YiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmVtb3ZlLWxpc3Q6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6ICNkNDRlMWIgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZsb2F0LXIge1xyXG4gIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuLmRpYWxvZy13cmFwZXIge1xyXG59XHJcblxyXG4vKjA5LTA3LTIwMTlcclxuLmxvZ2luLWxlZnQge2hlaWdodDogMTAwdmg7ICBiYWNrZ3JvdW5kOiAjMzI2OGFiO31cclxuLmxvZ2luLWVtYmxlbSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuL2Fzc2V0cy9lbWJsZW0tbG9naW4ucG5nJyk7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBoZWlnaHQ6IDI5NHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHdpZHRoOjYwMHB4O1xyXG59XHJcbiAgKi9cclxuXHJcbi8qQ1NTIGVkaXRlZCBvbiAyNi0wNy0yMDE5IGJ5IEd1bHNoYW4qL1xyXG5cclxuLmJyZWFkY3J1bWIgPiBsaSArIGxpOmJlZm9yZSB7XHJcbiAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcclxufVxyXG5cclxubGFiZWwge1xyXG4gIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zZWxlY3Rpb24taGVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmMGYwO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBwYWRkaW5nOiA4cHggMCA2cHggMDtcclxufVxyXG5cclxuLmNsZWFyZml4IHtcclxuICBjbGVhcjogYm90aDtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wYWQxMCB7XHJcbiAgbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5idG5iZyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3ZTdlNztcclxuICBwYWRkaW5nOiAxMnB4O1xyXG59XHJcblxyXG4ucGFuZWwtaGVhZGVyLCAucGFuZWwtaGVhZGVyOmFjdGl2ZSwgLnBhbmVsLWhlYWRlcjpob3ZlciwgLnBhbmVsLWhlYWRlcjp2aXNpdGVkIHtcclxuICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xyXG4gIGJhY2tncm91bmQ6ICMyNDY4YjMgIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZy1yaWdodDogMjBweCAhaW1wb3J0YW50O1xyXG4gIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuICAucGFuZWwtaGVhZGVyOmFjdGl2ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ2OGIzXHJcbiAgfVxyXG5cclxuICAucGFuZWwtaGVhZGVyOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDY4YjMgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC5wYW5lbC1oZWFkZXI6dmlzaXRlZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjQ2OGIzICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuLm1hdC1leHBhbnNpb24taW5kaWNhdG9yOjphZnRlciB7XHJcbiAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/leaves-mgmt/test/test.component.html":
/*!******************************************************!*\
  !*** ./src/app/leaves-mgmt/test/test.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"note-wraper\">\r\n  <button type=\"button\" class=\"slide-toggle note-slide\" (click)=\"Test();\">\r\n    <i class=\"material-icons\">\r\n      keyboard_arrow_left\r\n    </i>\r\n  </button>\r\n\r\n  <div class=\"box\">\r\n    <div class=\"box-inner\">\r\n      <b>Note :</b>  Please enter details when the employee is revoked after completion of Suspension period\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n<div style=\"padding:13px!important;\">\r\n\r\n  <h2>Alerts</h2>\r\n  <div class=\"alert alert-success\">\r\n    <strong>Success!</strong> This alert box could indicate a successful or positive action.\r\n  </div>\r\n\r\n  <div class=\"alert alert-info\">\r\n    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.\r\n  </div>\r\n  <div class=\"alert alert-warning\">\r\n    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.\r\n  </div>\r\n\r\n  <div class=\"alert alert-danger\">\r\n    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.\r\n  </div>\r\n\r\n</div>\r\n<app-desig-emp (desigchange)=\"designationChange()\" (onchange)=\"GetLeavesSanction($event)\"></app-desig-emp>\r\n<!---FOR TESTING CODE START--->\r\n<div class=\"col-md-12 col-lg-12\">\r\n  <form [formGroup]=\"leaveSanctionDetailsForm\" #LeaveSanctionForm=\"ngForm\">\r\n\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Leave Details</div>\r\n      <div class=\"col-md-12 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Sanction Order No\" formControlName=\"orderNo\" readonly disabled>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"OrderDate\" placeholder=\"Sanction Order Date\" formControlName=\"orderDT\" disabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"OrderDate\"></mat-datepicker-toggle>\r\n          <mat-datepicker #OrderDate></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n      <!--<div class=\"fom-title\">Leave Details</div>-->\r\n      <div class=\"col-md-12 col-lg-3 combo-col\">\r\n        <div class=\"col-md-12 col-lg-3 pading-0\">\r\n          <label>Leave Type</label>\r\n        </div>\r\n        <div class=\"col-md12 col-lg-9\">\r\n          <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"leaveCategory\" disabled>\r\n            <mat-radio-button [checked]='radiochecked' value=\"Single\" class=\"ml-10\">Single</mat-radio-button>\r\n            <mat-radio-button value=\"Combined\" class=\"ml-10\">Combined</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select matNativeControl placeholder=\"Select Leave Reason\" formControlName=\"leaveReasonCD\" disabled>\r\n            <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"leave-wraper\" formArrayName=\"leaveSanctionDetails\" *ngFor=\"let item1 of leaveSanctionDetailsForm.get('leaveSanctionDetails')['controls']; let i=index\">\r\n        <div [formGroupName]=\"i\">\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select matNativeControl placeholder=\"Select Leave Type\" formControlName=\"leaveCd\" disabled>\r\n                <mat-option *ngFor=\"let item of ArrddlLeavesType; let j=index\" (click)=\"LeaveTypeItemChange(i,item.leaveMaxAtATime)\" [value]=\"item.leaveTypeLeaveCd\">{{item.leaveTypeLeaveDesc}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"FDate\" readonly placeholder=\"From Date\" formControlName=\"fromDT\" disabled />\r\n              <mat-datepicker-toggle matSuffix [for]=\"FDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #FDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"TDate\" readonly placeholder=\"To Date\" formControlName=\"toDT\" disabled />\r\n              <!--(input)=\"alert('saga')\" onclick=\"alert('saga')\">-->\r\n              <mat-datepicker-toggle matSuffix [for]=\"TDate\"></mat-datepicker-toggle>\r\n              <mat-datepicker #TDate></mat-datepicker>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-3\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"No. of Days\" type=\"number\" formControlName=\"noDays\" disabled readonly>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n    </mat-card>\r\n\r\n  </form>\r\n  <div class=\"clearfix\"></div>\r\n</div>\r\n<!---FOR TESTING CODE END--->\r\n<!--<div class=\"col-md-12 col-lg-7\">\r\n</div>-->\r\n<!--right table-->\r\n\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel>\r\n    <!--<common-leaves-sanction [dataSource]=\"gridData\" (DissableLeavesSanction)=\"DissableLeavesSanction($event)\" (EditLeavesSanction)=\"EditLeavesSanction($event)\" (DeleteLeavesSanction)=\"DeleteLeavesSanction($event)\"></common-leaves-sanction>\r\n    <app-common-leaves-sanction [ShowinfoButton]=\"true\" [ShowdeleteButton]=\"false\" [ShoweditButton]=\"false\" [dataSource]=\"parentLeaveData\" (DissableLeavesSanction)=\"ShowLeavesSanction($event)\"></app-common-leaves-sanction>-->\r\n    <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header\">\r\n      <mat-panel-title style=\"color:#fff; font-weight:bold;\">\r\n        Leave Curtail/Extension Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Sanction Order No\" formControlName=\"curtOrderNo\">\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput [matDatepicker]=\"DOB2\" placeholder=\"Sanction Order Date\" formControlName=\"curtOrderDT\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"DOB2\"></mat-datepicker-toggle>\r\n        <mat-datepicker #DOB2></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md12 col-lg-6\">\r\n      <mat-radio-group (change)=\"ChangeLeaveCategory($event.value)\" formControlName=\"isCurtleave\">\r\n        <mat-radio-button [checked]='radiochecked' value=\"Curtail\" class=\"ml-10\">Leave Curtail</mat-radio-button>\r\n        <mat-radio-button value=\"Extension\" class=\"ml-10\">Leave Extension</mat-radio-button>\r\n      </mat-radio-group>\r\n\r\n    </div>\r\n\r\n    <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput placeholder=\"Reason for curtail/extension\" formControlName=\"curtReason\">\r\n        <!--<mat-select matNativeControl placeholder=\"Select Leave Reason\"  formControlName=\"leaveReasonCD\">\r\n          <mat-option *ngFor=\"let item of ArrddlReason\" [value]=\"item.cddirCodeValue\">{{item.cddirCodeText}}</mat-option>\r\n        </mat-select>-->\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput [matDatepicker]=\"FDate2\" readonly placeholder=\"From Date\" formControlName=\"curtFromDT\" />\r\n        <mat-datepicker-toggle matSuffix [for]=\"FDate2\"></mat-datepicker-toggle>\r\n        <mat-datepicker #FDate2></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-6\">\r\n      <mat-form-field class=\"wid-100\">\r\n        <input matInput [matDatepicker]=\"TDate2\" readonly placeholder=\"To Date\" formControlName=\"curtToDT\" />\r\n        ',\r\n        at\r\n        <mat-datepicker-toggle matSuffix [for]=\"TDate2\"></mat-datepicker-toggle>\r\n        <mat-datepicker #TDate2></mat-datepicker>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <textarea rows=\"5\" placeholder=\"enter remark if any!\" class=\"wid-100\" formControlName=\"curtRemarks\"></textarea>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper btnbg\">\r\n      <button type=\"submit\" class=\"btn btn-success\" (click)=\"checkValidation()\" [disabled]=\"leaveCurtailForm.invalid || showSave\">{{buttonText}}</button>\r\n      <button type=\"button\" class=\"btn btn-warning\" (click)=\"ResetLeavesSanction()\" [disabled]=\"showCancel\">Cancel</button>\r\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"ForwardToChecker()\" [disabled]=\"leaveCurtailForm.invalid || showForward\"> Forward to HOO Checker </button>\r\n    </div>\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n<div class=\"clearfix\"></div>\r\n<mat-accordion class=\"col-md-12\">\r\n  <mat-expansion-panel>\r\n    <mat-expansion-panel-header class=\"panel-h eader-bg panel-header\">\r\n      <mat-panel-title style=\"color:#fff; font-weight:bold;\">\r\n        Leave Curtail/Extension Details\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n\r\n    <mat-form-field class=\"pad10\">\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <div class=\"pad10\">\r\n      <table mat-table [dataSource]=\"dataSourceCurtail\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <!-- Leave Type -->\r\n        <ng-container matColumnDef=\"CurtExtenOrderNo\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Order No</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.curtOrderNo}}</td>\r\n        </ng-container>\r\n        <!-- From Date -->\r\n        <ng-container matColumnDef=\"CurtExtenOrderDate\">\r\n          <th mat-header-cell *matHeaderCellDef>Order Date</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.curtOrderDT}} </td>\r\n        </ng-container>\r\n        <!-- To Date Order No. -->\r\n        <ng-container matColumnDef=\"Leavetype\">\r\n          <th mat-header-cell *matHeaderCellDef>Leave Extension/ Curtail</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.isCurtleave}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"FromDate\">\r\n          <th mat-header-cell *matHeaderCellDef>From Date</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.curtFromDT}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"ToDate\">\r\n          <th mat-header-cell *matHeaderCellDef>To Date</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.curtToDT}} </td>\r\n        </ng-container>\r\n        <ng-container matColumnDef=\"Action\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <!--<div><a class=\"material-icons i-info\" matTooltip=\"Info\" (click)=\"DissableLeaves(element.leaveMainId,element.orderNo,element.orderDT,element.leaveCategory,element.leaveReasonCD,element.remarks,element.leaveSanctionDetails)\">error</a></div>-->\r\n            <div><a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"EditCurtailLeaves(element)\">edit</a></div>\r\n            <div><a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"DeleteCurtailLeaves(element.leaveMainId)\"> delete_forever </a></div>\r\n          </td>\r\n        </ng-container>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedCurtail\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedCurtail;\" (click)=\"rowHighlight(row)\"\r\n            [style.background]=\"highlightedRows.indexOf(row) != -1 ? 'lightgreen' : ''\"></tr>\r\n      </table>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n  </mat-expansion-panel>\r\n</mat-accordion>\r\n\r\n<style type=\"text/css\">\r\n  .box {\r\n    float: right;\r\n    overflow: hidden;\r\n    background: #fcf8e3;\r\n    display: none;\r\n  }\r\n  /* Add padding and border to inner content\r\n    for better animation effect */\r\n  .box-inner {\r\n    width: 700px;\r\n    padding: 10px;\r\n    border: 1px solid #a29415;\r\n  }\r\n\r\n  .note-slide[_ngcontent-c10] {\r\n    background: #ffb400;\r\n    border: none;\r\n    color: #fff;\r\n    padding: 5px 0 0 0;\r\n    margin: auto;\r\n  }\r\n\r\n  .note-wraper {\r\n    position: absolute;\r\n    right: 0;\r\n  }\r\n\r\n  .mat-expansion-indicator::after {\r\n    color: #fff !important;\r\n  }\r\n</style>\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/leaves-mgmt/test/test.component.ts":
/*!****************************************************!*\
  !*** ./src/app/leaves-mgmt/test/test.component.ts ***!
  \****************************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ELEMENT_DATA;
var TestComponent = /** @class */ (function () {
    function TestComponent(_Service, snackBar, formBuilder) {
        this._Service = _Service;
        this.snackBar = snackBar;
        this.formBuilder = formBuilder;
        this.displayedCurtail = ['CurtExtenOrderNo', 'CurtExtenOrderDate', 'Leavetype', 'FromDate', 'ToDate', 'Action'];
        this.highlightedRows = [];
        this.dataSourceCurtail = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](ELEMENT_DATA);
        this.showAddButton = false;
        this.showDeleteButton = false;
        this.showCheckBox0 = false;
        this.radiochecked = true;
        this.PermDdoId = "00003"; // sessionStorage.getItem('ddoid');
        this.showCancel = true;
        this.showForward = true;
        this.showSave = true;
        this.DetailsForm();
        this.CurtailForm();
    }
    TestComponent.prototype.ngOnInit = function () {
        //this.objlsmain = new LeavesSanctionMainModel();
        ////this.objlsmain.permDdoId = sessionStorage.getItem('ddoid');
        //this.objlsmain.permDdoId = "00003";
        //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
        this.BindDropDownLeavesReason();
        this.BindDropDownLeavesType();
        this.buttonText = "Save";
        this.leaveSanctionDetailsForm.get('orderNo').disable();
    };
    TestComponent.prototype.Test = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4___default()(".box").animate({
            width: "toggle"
        });
    };
    TestComponent.prototype.DetailsForm = function () {
        this.leaveSanctionDetailsForm = this.formBuilder.group({
            empCd: [this.employeeCode],
            empName: [null],
            permDDOId: [this.PermDdoId],
            orderNo: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveCategory: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            leaveReasonCD: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            remarks: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            verifyFlg: [null],
            leaveReasonDesc: [null],
            leaveMainId: [0],
            leaveSanctionDetails: this.formBuilder.array([this.createItem()])
        });
    };
    TestComponent.prototype.createItem = function () {
        return this.formBuilder.group({
            leaveCd: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            fromDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            toDT: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            noDays: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            maxLeave: [{ value: null, disabled: false }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            isMaxLeave: [{ value: false, disabled: false }]
        });
    };
    TestComponent.prototype.CurtailForm = function () {
        this.leaveCurtailForm = this.formBuilder.group({
            curtId: [0],
            curtEmpCd: [this.employeeCode],
            curtPermDdoId: [this.PermDdoId],
            preOrderNo: [this.SanctionOrderNo],
            preLeaveMainId: [this.leaveMainid],
            curtOrderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtOrderDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtFromDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtToDT: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            isCurtleave: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtReason: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            curtRemarks: [null],
            curtverifyFlg: [null],
        });
    };
    TestComponent.prototype.BindDropDownLeavesReason = function () {
        var _this = this;
        this._Service.GetLeavesReason().subscribe(function (data) {
            _this.ArrddlReason = data;
        });
    };
    TestComponent.prototype.BindDropDownLeavesType = function () {
        var _this = this;
        this._Service.GetLeavesType().subscribe(function (data) {
            _this.ArrddlLeavesType = data;
        });
    };
    //Search Form
    TestComponent.prototype.designationChange = function () {
        this.DetailsForm();
        this.showCancel = true;
        this.showForward = true;
        this.showSave = true;
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.buttonText = "Save";
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.leaveSanctionDetailsForm.get('orderNo').disable();
        //this.objlsmain = new LeavesSanctionMainModel();
        ////this.objlsmain.permDdoId = sessionStorage.getItem('ddoid');
        //this.objlsmain.permDdoId = "00003";
        ////this.objlsmain.empCd = value
        //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
        //this.parentLeaveData = null;
    };
    TestComponent.prototype.GetLeavesSanction = function (value) {
        this.employeeCode = value;
        this.DetailsForm();
        this.CurtailForm();
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.buttonText = "Save";
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.leaveSanctionDetailsForm.get('orderNo').enable();
        //this._Service.GetVerifiedLeavesSanction(this.PermDdoId, value).subscribe(result => {
        // this.parentLeaveData = result;
        //this.dataSource = new MatTableDataSource(result);      
        //this.dataSource.paginator = this.paginator;
        //this.dataSource.sort = this.sort;
        //}
        //this._Service.GetLeavesCurtailExten(this.PermDdoId, value).subscribe(result => {
        //  this.dataSourceCurtail = result;
        //  this.dataSourceCurtail = new MatTableDataSource(result);
        //  this.dataSourceCurtail.paginator = this.paginator;
        //  this.dataSourceCurtail.sort = this.sort;
        //})
    };
    Object.defineProperty(TestComponent.prototype, "getleaveDetails", {
        get: function () {
            return this.leaveSanctionDetailsForm.get('leaveSanctionDetails');
        },
        enumerable: true,
        configurable: true
    });
    //operational Form
    TestComponent.prototype.LeaveTypeItemChange = function (i, value) {
        //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
        //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);
    };
    TestComponent.prototype.ChangeLeavesType = function (value, index) {
        //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
        //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
    };
    TestComponent.prototype.ChangeLeaveCategory = function (value) {
        //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
        //this.showDeleteButton = value == 'Single' ? false : true;
        //this.showAddButton = value == 'Single' ? false : true;
        //alert(this.findInvalidControls());
    };
    TestComponent.prototype.fillnoofDays = function (value, index) {
        //if (this.leaveSanctionDetailsForm.get('fromDT_' + index).value != "undefined" || this.leaveSanctionDetailsForm.get('toDT_' + index).value) {
        //  this.firstdate = new Date(this.leaveSanctionDetailsForm.get('fromDT_' + index).value);
        //  this.lastdate = new Date(this.leaveSanctionDetailsForm.get('toDT_' + index).value);
        //  if (this.firstdate > this.lastdate) {
        //    alert('From date should be less than to date');
        //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(0);
        //    value == "s" ? this.leaveSanctionDetailsForm.get('fromDT_' + index).setValue("") : this.leaveSanctionDetailsForm.get('toDT_' + index).setValue("");
        //  }
        //  else {
        //    this.datedif = Math.abs(this.firstdate.getTime() - this.lastdate.getTime());
        //    this.datedif = Math.ceil(this.datedif / (1000 * 3600 * 24)) + 1;
        //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(this.datedif);
        //  }
        //}
    };
    TestComponent.prototype.SaveLeavesCurtExten = function () {
        var _this = this;
        this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
        this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
        this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
        this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
        if (this.leaveCurtailForm.get('curtId').value == null) {
            this.leaveCurtailForm.get('curtId').setValue(0);
        }
        this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "I").subscribe(function (result) {
            _this.snackBar.open('Save Successfully', null, { duration: 5000 });
            _this.GetLeavesSanction(_this.employeeCode);
        });
    };
    TestComponent.prototype.ForwardToChecker = function () {
        var _this = this;
        this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
        this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
        this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
        this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
        if (this.leaveCurtailForm.get('curtId').value == null) {
            this.leaveCurtailForm.get('curtId').setValue(0);
        }
        this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "F").subscribe(function (result) {
            _this.snackBar.open('Save Successfully', null, { duration: 5000 });
            _this.GetLeavesSanction(_this.employeeCode);
        });
    };
    TestComponent.prototype.ResetLeavesSanction = function () {
        this.DetailsForm();
        this.showDeleteButton = false;
        this.showAddButton = false;
        this.buttonText = "Save";
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    };
    //Grid operation
    TestComponent.prototype.ShowLeavesSanction = function (value) {
        this.leaveMainid = value.leaveMainId;
        this.SanctionOrderNo = value.orderNo;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        this.form2.resetForm();
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        //this.leaveCurtailForm.setControl('leaveSanctionDetails', new FormArray([]));
        //this.leaveCurtailForm.reset();
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.buttonText = "Save";
        this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
        this.showAddButton = value.leaveCategory == 'Single' ? false : true;
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
        this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
        for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
        }
    };
    TestComponent.prototype.EditCurtailLeaves = function (element) {
        //curtId: [0],
        //  curtEmpCd: [this.employeeCode],
        //    curtPermDdoId: [this.PermDdoId],
        //      preOrderNo: [this.SanctionOrderNo],
        //        preLeaveMainId: [this.leaveMainid],
        //          curtOrderNo: [null, Validators.required],
        //            curtOrderDT: [null, Validators.required],
        //              curtFromDT: [null, Validators.required],
        //                curtToDT: [null, Validators.required],
        //                  isCurtleave: [null, Validators.required],
        //                    curtReason: [null, Validators.required],
        //                      curtRemarks: [null],
        //                        curtverifyFlg: [null],
        this.leaveMainid = element.leaveMainId;
        this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([]));
        //this.leaveCurtailForm.reset();
        this.showCancel = false;
        this.showForward = false;
        this.showSave = false;
        this.buttonText = "Update";
        this.showDeleteButton = element.leaveCategory == 'Single' ? false : true;
        this.showAddButton = element.leaveCategory == 'Single' ? false : true;
        this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
        this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
        this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
        this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
        this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
        this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
        this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
        this.leaveCurtailForm.get('curtOrderNo').setValue(element.curtOrderNo);
        this.leaveCurtailForm.get('curtOrderDT').setValue(element.curtOrderDT);
        this.leaveCurtailForm.get('curtFromDT').setValue(element.curtFromDT);
        this.leaveCurtailForm.get('curtToDT').setValue(element.curtToDT);
        this.leaveCurtailForm.get('isCurtleave').setValue(element.isCurtleave);
        this.leaveCurtailForm.get('curtReason').setValue(element.curtReason);
        this.leaveCurtailForm.get('curtRemarks').setValue(element.curtRemarks);
        this.leaveCurtailForm.get('curtId').setValue(element.curtId);
        for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').push(this.createItem());
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
            this.leaveSanctionDetailsForm.get('leaveSanctionDetails').at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
        }
    };
    TestComponent.prototype.DeleteCurtailLeaves = function (value) {
        //this._Service.DeleteLeavesSanction(value).subscribe(result => {
        //  this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
        //  this.GetLeavesSanction(this.employeeCode);
        //})
    };
    TestComponent.prototype.checkValidation = function () {
        //if (this.leaveSanctionDetailsForm.get("leaveCategory").value == "Combined") {
        //  if (this.objlsmain.leaveSanctionDetails.length == 1) {
        //    alert('Please select single leave Type');
        //    return false;
        //  }
        //}
    };
    TestComponent.prototype.applyFilter = function (filterValue) {
        debugger;
        this.dataSourceCurtail.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceCurtail.paginator) {
            this.dataSourceCurtail.paginator.firstPage();
        }
    };
    TestComponent.prototype.abc = function () {
        alert(this.findInvalidControls());
    };
    //Extra methods
    TestComponent.prototype.findInvalidControls = function () {
        var invalid = [];
        var controls = this.leaveSanctionDetailsForm.controls;
        for (var name_1 in controls) {
            if (controls[name_1].invalid) {
                invalid.push(name_1);
            }
        }
        return invalid;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], TestComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], TestComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveSanctionForm'),
        __metadata("design:type", Object)
    ], TestComponent.prototype, "form1", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('LeaveCurtailForm'),
        __metadata("design:type", Object)
    ], TestComponent.prototype, "form2", void 0);
    TestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-test',
            template: __webpack_require__(/*! ./test.component.html */ "./src/app/leaves-mgmt/test/test.component.html"),
            styles: [__webpack_require__(/*! ./test.component.css */ "./src/app/leaves-mgmt/test/test.component.css")]
        }),
        __metadata("design:paramtypes", [_services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_3__["LeavesMgmtService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], TestComponent);
    return TestComponent;
}());



/***/ })

}]);
//# sourceMappingURL=leaves-mgmt-leaves-mgmt-module.js.map