(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["increment-increment-module"],{

/***/ "./src/app/increment/advance-increment/advance-increment.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/increment/advance-increment/advance-increment.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  .bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jcmVtZW50L2FkdmFuY2UtaW5jcmVtZW50L2FkdmFuY2UtaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0NBQ3hCOztFQUVDO0lBQ0UsWUFBWTtHQUNiOztFQUVIO0VBQ0Usa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7Q0FDWjs7RUFFRDtFQUNFLG9GQUFvRjtFQUNwRix1QkFBdUI7Q0FDeEI7O0VBRUQsZUFBZTs7RUFFZjtFQUNFLFlBQVk7Q0FDYjs7RUFFRDtFQUNFLHFCQUFxQjtDQUN0Qjs7RUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7RUFFRDtFQUNFLG9HQUFvRztDQUNyRzs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0VBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBR0QsZ0JBQWdCOztFQUNoQjtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0VBRUQ7RUFDRTtJQUNFLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFdBQVc7R0FDWjtDQUNGOztFQUNELGFBQWE7O0VBQ2I7RUFDRSxvQ0FBb0M7Q0FDckM7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBR0Q7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0NBQ2hCOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztFQUNEO0VBQ0UsNEJBQTRCO0NBQzdCIiwiZmlsZSI6InNyYy9hcHAvaW5jcmVtZW50L2FkdmFuY2UtaW5jcmVtZW50L2FkdmFuY2UtaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIG1hcmdpbjogNHB4XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vYXNzZXRzL2ltZy9leGFtcGxlcy9zaGliYTEuanBnJyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLyogMjktamFuLTE5ICovXHJcblxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXJnaW4tcmJsIHtcclxuICBtYXJnaW46IDAgNTBweCAxMHB4IDBcclxufVxyXG5cclxuLmZpbGQtb25lIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OCB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMXB4IC0xcHggcmdiYSgwLDAsMCwuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwwLDAsLjE0KSwgMCAxcHggM3B4IDAgcmdiYSgwLDAsMCwuMTIpO1xyXG59XHJcblxyXG4ubWF0LWNhcmQge1xyXG4gIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2U0ZTJlMjtcclxufVxyXG5cclxuLmV4YW1wbGUtY2FyZCB7XHJcbiAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDE4MXB4O1xyXG59XHJcblxyXG5cclxuLyogUmVzcG9uc2l2ZSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjhweCkgYW5kIChtYXgtd2lkdGggOiAxMDI0cHgpIHtcclxuICAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX29uZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMTAyNXB4KSBhbmQgKG1heC13aWR0aCA6IDEzOTdweCkge1xyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG5cclxuICAuZGV0YWlsX3R3byB7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gIH1cclxufVxyXG4vKjMxL2phbi8xOSovXHJcbi5tLTIwIHtcclxuICBtYXJnaW46IDAgMjBweCAyMHB4IDIwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIG1pbi13aWR0aDogMzAwcHg7XHJcbn1cclxuXHJcbi5tYXQtdGFibGUge1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG4gIG1heC1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1jZWxsLm1hdC1zb3J0LWhlYWRlci1zb3J0ZWQge1xyXG4gIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLm10LTEwIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0LWRyb3AtaGVhZCB7XHJcbiAgbWFyZ2luOiAwIDMwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5iZ2NvbG9yIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmYmRhOTk7XHJcbn1cclxuXHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/increment/advance-increment/advance-increment.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/increment/advance-increment/advance-increment.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Raju verma 20/06/2019--->\r\n<!--<app-desig-emp (desigchange)=\"designationChange()\" (onchange)=\"GetIncrementDetails($event)\" (numberGenerated)=\"onNumberGenerated()\" ></app-desig-emp>-->\r\n\r\n<div class=\"basic-container select-drop-head\">\r\n\r\n  <div class=\"alert\" [ngClass]=\"divbgcolor\" *ngIf=\"is_btnStatus\">\r\n    <strong> {{Message}}</strong>\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n      <span aria-hidden=\"true\" (click)=\"is_btnStatus=false\">&times;</span>\r\n    </button>&nbsp;\r\n  </div>\r\n\r\n  <div class=\"row  selection-hed\">\r\n\r\n    <div class=\"col-md-3\">\r\n      <div class=\"col-md-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Pay Commission\" id=\"msCddirID\" [(ngModel)]=\"_pScaleObj.msCddirID\" name=\"msCddirID\" required #msCddirID=\"ngModel\" (selectionChange)=\"refreshdata()\">\r\n            <mat-option label=\"Select Pay Commission\">Select Pay Commission</mat-option>\r\n            <mat-option *ngFor=\"let _payscalelist of CommissionCodelist\" [value]=\"_payscalelist.msCddirID\">\r\n              {{ _payscalelist.cddirCodeText }}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error>\r\n            <span [hidden]=\"!msCddirID.errors?.required\">Pay commission is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-md-3\">\r\n      <div class=\"col-md-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"designCtrl\" placeholder=\"Find Designation\" #singleSelect (selectionChange)=\"bindDropDownEmployee($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [noEntriesFoundLabel]=\"'Designation is not found'\" (keypress)=\"charaterOnly($event)\" [placeholderLabel]=\"'Find Designation...'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n              {{emp.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n      <div class=\"col-md-12\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"empCtrl\" placeholder=\"Find Employee\" #singleSelect (selectionChange)=\"getIncrementDetails($event.value)\">\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\" (keypress)=\"charaterOnly($event)\" [noEntriesFoundLabel]=\"'Employee is not found'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.empCode\">\r\n              {{emp.empName}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n  <div class=\"col-md-12 col-lg-12\">\r\n    <div class=\"example-card mat-card tabel-wraper\">\r\n      <div class=\"fom-title\">Increment Details</div>\r\n      <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <tr>\r\n          <ng-container matColumnDef=\"EmpName\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"OldBasic\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Old basic </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"OldWefDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Old wef date</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.oldWefDate | date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"NextIncDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Next Increment Date</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"PayLevel\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Pay Level</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"Flag\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Status</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.flag}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"editAdvIncrement(element.incID);\">edit</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"setDeleteId(element.incID);deletepopup = !deletepopup\"> delete_forever </a>\r\n            </td>\r\n          </ng-container>\r\n\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n    </div>\r\n  </div>\r\n\r\n  <form name=\"form\" #advIncrement=\"ngForm\">\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <mat-card class=\"example-card\" [ngClass]=\"bgcolor\">\r\n        <div class=\"fom-title\">Additional Increment</div>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Select Type\" [(ngModel)]=\"advanceInc.orderTypeID\" required #orderTypeID=\"ngModel\" name=\"orderTypeID\">\r\n                <mat-option label=\"Select Type\">Select Order Type</mat-option>\r\n                <mat-option [value]=\"1\">Sports</mat-option>\r\n                <mat-option [value]=\"2\">Other</mat-option>\r\n              </mat-select>\r\n              <mat-error>\r\n                <span *ngIf=\"advIncrement.submitted && orderTypeID.invalid\">Type required</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Select No. of Increments\" [(ngModel)]=\"advanceInc.noofIncrement\" required #noofIncrement=\"ngModel\" name=\"noofIncrement\">\r\n                <mat-option label=\"Select No. of Increments\">Select No. of Increments</mat-option>\r\n                <mat-option [value]=\"1\">1</mat-option>\r\n                <mat-option [value]=\"2\">2</mat-option>\r\n                <mat-option [value]=\"3\">3</mat-option>\r\n                <mat-option [value]=\"4\">4</mat-option>\r\n                <mat-option [value]=\"5\">5</mat-option>\r\n                <mat-option [value]=\"6\">6</mat-option>\r\n              </mat-select>\r\n              <mat-error><span *ngIf=\"advIncrement.submitted && noofIncrement.invalid\"> No. of Increments required</span></mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Order no\" maxlength=\"20\" required name=\"orderNo\" [(ngModel)]=\"advanceInc.orderNo\" #orderNo=\"ngModel\"\r\n                     onpaste=\"return false\" pattern=\"^[A-Za-z0-9]+$\" autocomplete=\"off\">\r\n              <mat-error><span *ngIf=\"advIncrement.submitted && orderNo.invalid\">Order no required</span></mat-error>\r\n              <mat-error><span *ngIf=\"orderNo.errors?.pattern\">Special character not allowed</span></mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"advanceInc.orderDate \" name=\"orderDate\" #orderDate=\"ngModel\"\r\n                     placeholder=\"Order Date\" readonly [disabled]=disbleflag required>\r\n              <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n              <mat-datepicker #DOJ></mat-datepicker>\r\n              <mat-error>\r\n                <span [hidden]=\"!orderDate.errors?.required\">Order Date is required</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput [matDatepicker]=\"DOW\" (click)=\"DOW.open()\" [(ngModel)]=\"advanceInc.wefDate \" name=\"wefDate\" #wefDate=\"ngModel\"\r\n                     placeholder=\"Wef Date\" readonly [disabled]=disbleflag required>\r\n              <mat-datepicker-toggle matSuffix [for]=\"DOW\"></mat-datepicker-toggle>\r\n              <mat-datepicker #DOW></mat-datepicker>\r\n              <mat-error>\r\n                <span [hidden]=\"!wefDate.errors?.required\">Wef Date is required</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-4\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Remarks\" maxlength=\"1000\" name=\"remark\" [(ngModel)]=\"advanceInc.remark\" #remark=\"ngModel\">\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12 text-center btn-wraper\" *ngIf=\"showhidediv\">\r\n            <button type=\"submit\" [ngClass]=\"btnCssClass\" *ngIf=\"savebuttonstatus\" (click)=\"advIncrement.onSubmit() ; advIncrement.valid && createAdvIncrement();\">{{btnUpdatetext}}</button>\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"resetForm();\">Cancel</button>\r\n            <button type=\"submit\" class=\"btn btn-primary\" (click)=\"advIncrement.onSubmit() ; advIncrement.valid && forwardpopup = !forwardpopup\" [disabled]=\"disableFDflag\">Forword To Checker</button>\r\n          </div>\r\n\r\n\r\n        </div>\r\n      </mat-card>\r\n\r\n    </div>\r\n\r\n  </form>\r\n\r\n\r\n<!--Pop Up Code-->\r\n\r\n<app-dialog [(visible)]=\"forwardpopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to Forward to Checker ?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"forwardtoChecker()\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"forwardpopup = !forwardpopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteAdvDetails(setDeletIDOnPopup)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/increment/advance-increment/advance-increment.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/increment/advance-increment/advance-increment.component.ts ***!
  \****************************************************************************/
/*! exports provided: AdvanceIncrementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvanceIncrementComponent", function() { return AdvanceIncrementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_increment_advincrement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/increment/advincrement.service */ "./src/app/services/increment/advincrement.service.ts");
/* harmony import */ var _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/masters/PayscaleModel */ "./src/app/model/masters/PayscaleModel.ts");
/* harmony import */ var _services_payscale_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/payscale.service */ "./src/app/services/payscale.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/leaves-mgmt/leaves-mgmt.service */ "./src/app/services/leaves-mgmt/leaves-mgmt.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import swal from 'sweetalert2';






var AdvanceIncrementComponent = /** @class */ (function () {
    function AdvanceIncrementComponent(advincservice, payscale, _Service) {
        this.advincservice = advincservice;
        this.payscale = payscale;
        this._Service = _Service;
        this.selectedIndex = 0;
        this.advanceInc = {};
        this.disbleflag = false;
        this.showhidediv = true;
        this.disableFDflag = true;
        this.btnCssClass = 'btn btn-success';
        this.desigchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.empCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.empFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.filteredEmp = new rxjs__WEBPACK_IMPORTED_MODULE_6__["ReplaySubject"](1);
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_6__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.displayedColumns = ['EmpName', 'OldBasic', 'OldWefDate', 'NextIncDate', 'PayLevel', 'Flag', 'action'];
    }
    AdvanceIncrementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._pScaleObj = new _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_3__["PayscaleModel"]();
        this.bindPayCommission();
        this.btnUpdatetext = "Save";
        this.username = sessionStorage.getItem('username');
        this.savebuttonstatus = true;
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.bindDropDownDesignation(this.PermDdoId);
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    AdvanceIncrementComponent.prototype.bindPayCommission = function () {
        var _this = this;
        this.payscale.BindCommissionCode().subscribe(function (result) {
            _this.CommissionCodelist = result;
        });
    };
    AdvanceIncrementComponent.prototype.bindDropDownDesignation = function (value) {
        var _this = this;
        this.advincservice.getAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    AdvanceIncrementComponent.prototype.bindDropDownEmployee = function (value) {
        var _this = this;
        this.advanceInc.msCddirID = this._pScaleObj.msCddirID;
        this.desigchange.emit();
        this.advincservice.GetEmployeeByDesigPayComm(value, this.advanceInc.msCddirID).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            _this.empCtrl.setValue(_this.EMP);
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    AdvanceIncrementComponent.prototype.backForwordSearchDesignation = function (value) {
        var _this = this;
        this._Service.GetAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            // set initial selection
            _this.designCtrl.setValue(_this.Design);
            // load the initial Design list
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    AdvanceIncrementComponent.prototype.backForwordSearchEmployee = function (value) {
        var _this = this;
        this.advanceInc.msCddirID = this._pScaleObj.msCddirID;
        this.advincservice.GetEmployeeByDesigPayComm(value, this.advanceInc.msCddirID).subscribe(function (data) {
            _this.ArrddlEmployee = data;
            _this.EMP = data;
            // set initial selection
            _this.empCtrl.setValue(_this.EMP);
            // load the initial EMP list
            _this.filteredEmp.next(_this.EMP);
        });
        this.empFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterEmp();
        });
    };
    AdvanceIncrementComponent.prototype.selectionChange = function ($event) {
        console.log('stepper.selectedIndex: ' + this.selectedIndex
            + '; $event.selectedIndex: ' + $event.selectedIndex);
        if ($event.selectedIndex === 0) {
            return;
        } // First step is still selected
        this.selectedIndex = $event.selectedIndex;
    };
    AdvanceIncrementComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    AdvanceIncrementComponent.prototype.filterEmp = function () {
        if (!this.EMP) {
            return;
        }
        var search = this.empFilterCtrl.value;
        if (!search) {
            this.filteredEmp.next(this.EMP.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredEmp.next(this.EMP.filter(function (EMP) { return EMP.empName.toLowerCase().indexOf(search) > -1; }));
    };
    AdvanceIncrementComponent.prototype.getIncrementDetails = function (value) {
        var _this = this;
        this.showhidediv = true;
        this.disableFDflag = true;
        this.empCode = value;
        this.advincservice.GetIncrementDetails(value).subscribe(function (result) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            for (var i = 0; i < _this.dataSource.filteredData.length; i++) {
                if (_this.dataSource.filteredData[i].status == "F") {
                    _this.showhidediv = false;
                }
                else
                    _this.showhidediv = true;
            }
        });
        this.btnUpdatetext = 'Save';
        this.advanceInc.IncID = 0;
        this.resetForm();
    };
    AdvanceIncrementComponent.prototype.createAdvIncrement = function () {
        var _this = this;
        debugger;
        this.advanceInc.loginUser = this.username;
        this.advanceInc.empCode = this.empCode;
        this.advanceInc.paylevel = this._pScaleObj.msCddirID;
        this.advincservice.CreateAdvIncrement(this.advanceInc).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                _this.is_btnStatus = true;
                _this.divbgcolor = "alert-success";
                _this.Message = result;
                // swal(result)
            }
            _this.resetForm();
            _this.btnUpdatetext = 'Save';
            _this.getIncrementDetails(_this.empCode);
            _this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        });
    };
    AdvanceIncrementComponent.prototype.editAdvIncrement = function (IncID) {
        var _this = this;
        this.advanceInc.IncID = this.IncID;
        this.advincservice.EditAdvIncrement(IncID).subscribe(function (result) {
            _this.advanceInc = result[0];
            _this.btnUpdatetext = 'Update';
            _this.disableFDflag = false;
            _this.bgcolor = "bgcolor";
            _this.btnCssClass = 'btn btn-info';
        });
    };
    AdvanceIncrementComponent.prototype.deleteAdvDetails = function (IncID) {
        var _this = this;
        this.advincservice.DeleteAdvDetails(IncID).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                _this.Message = result;
                _this.is_btnStatus = true;
                _this.divbgcolor = "alert-danger";
                //swal(result)
            }
            _this.getIncrementDetails(_this.advanceInc.Empcode);
            _this.form.resetForm();
            _this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
            _this.bindDropDownDesignation(_this.PermDdoId);
            _this.bindDropDownEmployee(0);
            _this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        });
    };
    AdvanceIncrementComponent.prototype.setDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    AdvanceIncrementComponent.prototype.resetForm = function () {
        this.btnUpdatetext = 'Save';
        this.disbleflag = false;
        this.form.resetForm();
        this.advanceInc.IncID = 0;
        this.bgcolor = "";
        this.btnCssClass = 'btn btn-success';
    };
    AdvanceIncrementComponent.prototype.forwardtoChecker = function () {
        var _this = this;
        this.advanceInc.loginUser = this.username;
        this.advanceInc.empCode = this.empCode;
        this.IncID = this.advanceInc.IncID;
        this.advincservice.Forwardtochecker(this.advanceInc).subscribe(function (result) {
            if (result != undefined) {
                _this.forwardpopup = false;
                _this.is_btnStatus = true;
                _this.divbgcolor = "alert-success";
                _this.Message = result;
                //swal(result)
            }
            _this.getIncrementDetails(_this.advanceInc.Empcode);
            _this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
            _this.bindDropDownDesignation(_this.PermDdoId);
            _this.bindDropDownEmployee(0);
            _this.resetForm();
            _this.btnUpdatetext = 'Save';
            _this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        });
    };
    AdvanceIncrementComponent.prototype.refreshdata = function () {
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.bindDropDownDesignation(this.PermDdoId);
        this.msDesigMastID = 0;
        this.bindDropDownEmployee(this.msDesigMastID);
        this.dataSource = '';
        this.resetForm();
    };
    AdvanceIncrementComponent.prototype.charaterOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8) {
            return true;
        }
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AdvanceIncrementComponent.prototype, "desigchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], AdvanceIncrementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], AdvanceIncrementComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('advIncrement'),
        __metadata("design:type", Object)
    ], AdvanceIncrementComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], AdvanceIncrementComponent.prototype, "singleSelect", void 0);
    AdvanceIncrementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-advance-increment',
            template: __webpack_require__(/*! ./advance-increment.component.html */ "./src/app/increment/advance-increment/advance-increment.component.html"),
            styles: [__webpack_require__(/*! ./advance-increment.component.css */ "./src/app/increment/advance-increment/advance-increment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_increment_advincrement_service__WEBPACK_IMPORTED_MODULE_2__["AdvincrementService"], _services_payscale_service__WEBPACK_IMPORTED_MODULE_4__["PayscaleService"], _services_leaves_mgmt_leaves_mgmt_service__WEBPACK_IMPORTED_MODULE_8__["LeavesMgmtService"]])
    ], AdvanceIncrementComponent);
    return AdvanceIncrementComponent;
}());



/***/ }),

/***/ "./src/app/increment/annual-increment-report/annual-increment-report.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/increment/annual-increment-report/annual-increment-report.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  /*lebel dropdown */\r\n\r\n  .select-lbl {\r\n  float: left;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jcmVtZW50L2FubnVhbC1pbmNyZW1lbnQtcmVwb3J0L2FubnVhbC1pbmNyZW1lbnQtcmVwb3J0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0NBQ3hCOztFQUVDO0lBQ0UsWUFBWTtHQUNiOztFQUVIO0VBQ0Usa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7Q0FDWjs7RUFFRDtFQUNFLG9GQUFvRjtFQUNwRix1QkFBdUI7Q0FDeEI7O0VBRUQsZUFBZTs7RUFFZjtFQUNFLFlBQVk7Q0FDYjs7RUFFRDtFQUNFLHFCQUFxQjtDQUN0Qjs7RUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7RUFFRDtFQUNFLG9HQUFvRztDQUNyRzs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0VBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBR0QsZ0JBQWdCOztFQUNoQjtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0VBRUQ7RUFDRTtJQUNFLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFdBQVc7R0FDWjtDQUNGOztFQUNELGFBQWE7O0VBQ2I7RUFDRSxvQ0FBb0M7Q0FDckM7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBR0Q7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0NBQ2hCOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztFQUNELG1CQUFtQjs7RUFDbkI7RUFDRSxZQUFZO0NBQ2IiLCJmaWxlIjoic3JjL2FwcC9pbmNyZW1lbnQvYW5udWFsLWluY3JlbWVudC1yZXBvcnQvYW5udWFsLWluY3JlbWVudC1yZXBvcnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxucCB7XHJcbiAgZm9udC1mYW1pbHk6IExhdG87XHJcbn1cclxuXHJcbi5leGFtcGxlLWNhcmQge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiA0cHhcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4vKiAyOS1qYW4tMTkgKi9cclxuXHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGwubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubXQtMTAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtZHJvcC1oZWFkIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLypsZWJlbCBkcm9wZG93biAqL1xyXG4uc2VsZWN0LWxibCB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/increment/annual-increment-report/annual-increment-report.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/increment/annual-increment-report/annual-increment-report.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Raju verma 24/06/19--->\r\n<form name=\"form\" #annualIncrment=\"ngForm\" novalidate>\r\n\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n      <div class=\"col-sm-12 col-md-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Pay Commission\" id=\"msCddirID\" [(ngModel)]=\"_pScaleObj.msCddirID\" name=\"msCddirID\">\r\n            <mat-option label=\"Select Pay Commission\" [value]=\"0\">Select Pay Commission</mat-option>\r\n            <mat-option *ngFor=\"let _payscalelist of CommissionCodelist\" [value]=\"_payscalelist.msCddirID\">\r\n              {{ _payscalelist.cddirCodeText }}\r\n            </mat-option>\r\n          </mat-select>\r\n          <mat-error><span *ngIf=\"annualIncrment.submitted && msCddirID.invalid\">Pay Commission required</span></mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-sm-12 col-md-3\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"getOrderWithEmployee()\">GO</button>\r\n      </div>\r\n      \r\n    </div>\r\n    </div>\r\n\r\n\r\n\r\n  <div class=\"col-md-12 col-lg-5\" *ngIf=\"showhideGried\">\r\n    <mat-card class=\"example-card\">\r\n      <div class=\"fom-title\">Annual Increment Report :</div>\r\n      <div class=\"row\">\r\n        <table mat-table [dataSource]=\"dataSourceEmp\" class=\"mat-elevation-z8 even-odd-color\" >\r\n          <tr class=\"table-head\">\r\n\r\n            <ng-container matColumnDef=\"OrderNo\">\r\n              <th mat-header-cell *matHeaderCellDef>Order No. </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"OrderDate\">\r\n              <th mat-header-cell *matHeaderCellDef>Order Date </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.orderDate|date}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"NoofEmployee\">\r\n              <th mat-header-cell *matHeaderCellDef>No. of Employee</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.noofEmployee}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"Status\">\r\n              <th mat-header-cell *matHeaderCellDef>Status</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"action\">\r\n              <th mat-header-cell *matHeaderCellDef> Action </th>\r\n              <td mat-cell *matCellDef=\"let element let k = index;\">\r\n                <a class=\"btn btn-info\" (click)=\"getAnnualIncReport(element.id);\">View</a>\r\n              </td>\r\n            </ng-container>\r\n          </tr>\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n        </table>\r\n        <div [hidden]=\"isTableHasData\">\r\n          No  Records Found\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</form>\r\n\r\n\r\n<div class=\"col-md-12 col-lg-7\" *ngIf=\"showhideGriedReport\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <div class=\"fom-title\">Annual Increment Report Details</div>\r\n    \r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <tr>\r\n        <ng-container matColumnDef=\"serialNo\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Serial No. </th>\r\n          <td mat-cell *matCellDef=\"let i=index\"> {{i+1}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"empName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Employee Name</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"empCode\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Employee Code</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n        </ng-container>\r\n\r\n        <!-- Weight Column -->\r\n        <ng-container matColumnDef=\"desigCode\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desigCode}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"oldBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Current Basic Pay</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"nextIncDate\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Increment Date</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate|date}} </td>\r\n        </ng-container>\r\n\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumn\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumn;\"></tr>\r\n    </table>\r\n   \r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/increment/annual-increment-report/annual-increment-report.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/increment/annual-increment-report/annual-increment-report.component.ts ***!
  \****************************************************************************************/
/*! exports provided: AnnualIncrementReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnnualIncrementReportComponent", function() { return AnnualIncrementReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_payscale_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/payscale.service */ "./src/app/services/payscale.service.ts");
/* harmony import */ var _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/masters/PayscaleModel */ "./src/app/model/masters/PayscaleModel.ts");
/* harmony import */ var _services_increment_annualincrement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/increment/annualincrement.service */ "./src/app/services/increment/annualincrement.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AnnualIncrementReportComponent = /** @class */ (function () {
    function AnnualIncrementReportComponent(payscale, incrementService) {
        this.payscale = payscale;
        this.incrementService = incrementService;
        this.isTableHasData = true;
        this.displayedColumns = ['OrderNo', 'OrderDate', 'NoofEmployee', 'Status', 'action'];
        this.displayedColumn = ['serialNo', 'empName', 'empCode', 'desigCode', 'oldBasic', 'nextIncDate'];
    }
    AnnualIncrementReportComponent.prototype.ngOnInit = function () {
        this._pScaleObj = new _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_2__["PayscaleModel"]();
        this.bindPayCommission();
        this.showhideGried = false;
        this.showhideGriedReport = false;
    };
    AnnualIncrementReportComponent.prototype.bindPayCommission = function () {
        var _this = this;
        this.payscale.BindCommissionCode().subscribe(function (result) {
            _this.CommissionCodelist = result;
        });
    };
    AnnualIncrementReportComponent.prototype.getOrderWithEmployee = function () {
        var _this = this;
        debugger;
        this.showhideGriedReport = false;
        this.PayLevel = this._pScaleObj.msCddirID;
        this.incrementService.GetOrderWithEmployee(this.PayLevel).subscribe(function (result) {
            _this.dataSourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result);
            _this.showhideGried = true;
            if (_this.dataSourceEmp.filteredData.length > 0)
                _this.isTableHasData = true;
            else
                _this.isTableHasData = false;
        });
    };
    AnnualIncrementReportComponent.prototype.getAnnualIncReport = function (ID) {
        var _this = this;
        debugger;
        this.showhideGriedReport = true;
        //this.regInc.orderID = ID;
        this.PayLevel = this._pScaleObj.msCddirID;
        this.incrementService.GetAnnualIncReportData(this.PayLevel, ID).subscribe(function (result) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            _this.showhideGriedReport = true;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], AnnualIncrementReportComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('annualIncrment'),
        __metadata("design:type", Object)
    ], AnnualIncrementReportComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], AnnualIncrementReportComponent.prototype, "sort", void 0);
    AnnualIncrementReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-annual-increment-report',
            template: __webpack_require__(/*! ./annual-increment-report.component.html */ "./src/app/increment/annual-increment-report/annual-increment-report.component.html"),
            styles: [__webpack_require__(/*! ./annual-increment-report.component.css */ "./src/app/increment/annual-increment-report/annual-increment-report.component.css")]
        }),
        __metadata("design:paramtypes", [_services_payscale_service__WEBPACK_IMPORTED_MODULE_1__["PayscaleService"], _services_increment_annualincrement_service__WEBPACK_IMPORTED_MODULE_3__["AnnualincrementService"]])
    ], AnnualIncrementReportComponent);
    return AnnualIncrementReportComponent;
}());



/***/ }),

/***/ "./src/app/increment/employee-due-increment/employee-due-increment.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/increment/employee-due-increment/employee-due-increment.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jcmVtZW50L2VtcGxveWVlLWR1ZS1pbmNyZW1lbnQvZW1wbG95ZWUtZHVlLWluY3JlbWVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLHVCQUF1QjtDQUN4Qjs7RUFFQztJQUNFLFlBQVk7R0FDYjs7RUFFSDtFQUNFLGtCQUFrQjtDQUNuQjs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQixXQUFXO0NBQ1o7O0VBRUQ7RUFDRSxvRkFBb0Y7RUFDcEYsdUJBQXVCO0NBQ3hCOztFQUVELGVBQWU7O0VBRWY7RUFDRSxZQUFZO0NBQ2I7O0VBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0VBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0VBRUQ7RUFDRSxvR0FBb0c7Q0FDckc7O0VBRUQ7RUFDRSxpQkFBaUI7RUFDakIsMEJBQTBCO0NBQzNCOztFQUVEO0VBQ0UsMkJBQTJCO0NBQzVCOztFQUVEO0VBQ0UsYUFBYTtDQUNkOztFQUdELGdCQUFnQjs7RUFDaEI7RUFDRTtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLFlBQVk7R0FDYjs7RUFFRDtJQUNFLFlBQVk7R0FDYjtDQUNGOztFQUVEO0VBQ0U7SUFDRSxXQUFXO0dBQ1o7O0VBRUQ7SUFDRSxXQUFXO0dBQ1o7Q0FDRjs7RUFDRCxhQUFhOztFQUNiO0VBQ0Usb0NBQW9DO0NBQ3JDOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUdEO0VBQ0UsY0FBYztFQUNkLHVCQUF1QjtFQUN2QixpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsYUFBYTtDQUNkOztFQUVEO0VBQ0UsaUJBQWlCO0NBQ2xCOztFQUVEO0VBQ0UsZUFBZTtDQUNoQjs7RUFFRDtFQUNFLFlBQVk7Q0FDYjs7RUFFRDtFQUNFLG1CQUFtQjtDQUNwQiIsImZpbGUiOiJzcmMvYXBwL2luY3JlbWVudC9lbXBsb3llZS1kdWUtaW5jcmVtZW50L2VtcGxveWVlLWR1ZS1pbmNyZW1lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxucCB7XHJcbiAgZm9udC1mYW1pbHk6IExhdG87XHJcbn1cclxuXHJcbi5leGFtcGxlLWNhcmQge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiA0cHhcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4vKiAyOS1qYW4tMTkgKi9cclxuXHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGwubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubXQtMTAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtZHJvcC1oZWFkIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/increment/employee-due-increment/employee-due-increment.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/increment/employee-due-increment/employee-due-increment.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Raju verma 24/08/2019--->\r\n\r\n<form name=\"form\" #f=\"ngForm\">\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n      \r\n      <div class=\"col-sm-12 col-md-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select [formControl]=\"designCtrl\" placeholder=\"Select Designation\" [(ngModel)]=\"ArrddlDesign.msDesigMastID\" >\r\n            <mat-option>\r\n              <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\"></ngx-mat-select-search>\r\n            </mat-option>\r\n            <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n              {{emp.desigDesc}}\r\n            </mat-option>\r\n          </mat-select>\r\n          \r\n          <mat-error>\r\n          <span [hidden]=\"!designFilterCtrl.errors?.required\">Designation is required</span>\r\n        </mat-error>\r\n\r\n        </mat-form-field>\r\n      </div>\r\n\r\n\r\n\r\n      <div class=\"col-sm-12 col-md-3\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"f.onSubmit() ; f.valid && getEmpDueIncReport()\">GO</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  </form>\r\n\r\n\r\n\r\n  <!--Report View-->\r\n\r\n<div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showPH\">\r\n  <div class=\"example-card mat-card tabel-wraper\">\r\n    <div class=\"fom-title\">Employees due for Increment Details :</div>\r\n  </div>\r\n  <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n    <tr>\r\n      <ng-container matColumnDef=\"serialNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Serial No. </th>\r\n        <td mat-cell *matCellDef=\"let i=index\"> {{i+1}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"empName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Employee Name</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n      </ng-container>\r\n\r\n\r\n      <ng-container matColumnDef=\"empCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Employee Code</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCode}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"desigCode\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Designation</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.desigCode}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"oldBasic\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Current Basic Pay</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"nextIncDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Increment Date</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate|date}} </td>\r\n      </ng-container>\r\n\r\n    </tr>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumn\"></tr>\r\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumn;\"></tr>\r\n  </table>\r\n  <div [hidden]=\"isTableHasData\">\r\n    No  Records Found\r\n  </div>\r\n  <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/increment/employee-due-increment/employee-due-increment.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/increment/employee-due-increment/employee-due-increment.component.ts ***!
  \**************************************************************************************/
/*! exports provided: EmployeeDueIncrementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeDueIncrementComponent", function() { return EmployeeDueIncrementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_increment_annualincrement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/increment/annualincrement.service */ "./src/app/services/increment/annualincrement.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EmployeeDueIncrementComponent = /** @class */ (function () {
    function EmployeeDueIncrementComponent(incrementService) {
        this.incrementService = incrementService;
        this.onchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.desigchange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.isTableHasData = false;
        this.displayedColumn = ['serialNo', 'empName', 'empCode', 'desigCode', 'oldBasic', 'nextIncDate'];
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
    }
    EmployeeDueIncrementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.showPH = false;
        this.bindDesignation(this.PermDdoId);
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    EmployeeDueIncrementComponent.prototype.bindDesignation = function (value) {
        var _this = this;
        this.incrementService.getAllDesignation(this.PermDdoId).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    EmployeeDueIncrementComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    EmployeeDueIncrementComponent.prototype.getEmpDueIncReport = function () {
        var _this = this;
        this.msDesigMastID = this.ArrddlDesign.msDesigMastID;
        this.incrementService.GetEmpDueForIncReport(this.msDesigMastID).subscribe(function (result) {
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
            if (_this.dataSource.filteredData.length > 0)
                _this.isTableHasData = true;
            else
                _this.isTableHasData = false;
        });
        this.showPH = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EmployeeDueIncrementComponent.prototype, "onchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EmployeeDueIncrementComponent.prototype, "desigchange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], EmployeeDueIncrementComponent.prototype, "singleSelect", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], EmployeeDueIncrementComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], EmployeeDueIncrementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], EmployeeDueIncrementComponent.prototype, "sort", void 0);
    EmployeeDueIncrementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-due-increment',
            template: __webpack_require__(/*! ./employee-due-increment.component.html */ "./src/app/increment/employee-due-increment/employee-due-increment.component.html"),
            styles: [__webpack_require__(/*! ./employee-due-increment.component.css */ "./src/app/increment/employee-due-increment/employee-due-increment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_increment_annualincrement_service__WEBPACK_IMPORTED_MODULE_2__["AnnualincrementService"]])
    ], EmployeeDueIncrementComponent);
    return EmployeeDueIncrementComponent;
}());



/***/ }),

/***/ "./src/app/increment/increment-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/increment/increment-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: IncrementRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncrementRoutingModule", function() { return IncrementRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _increment_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./increment.module */ "./src/app/increment/increment.module.ts");
/* harmony import */ var _regular_increment_regular_increment_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./regular-increment/regular-increment.component */ "./src/app/increment/regular-increment/regular-increment.component.ts");
/* harmony import */ var _advance_increment_advance_increment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./advance-increment/advance-increment.component */ "./src/app/increment/advance-increment/advance-increment.component.ts");
/* harmony import */ var _stop_increment_stop_increment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stop-increment/stop-increment.component */ "./src/app/increment/stop-increment/stop-increment.component.ts");
/* harmony import */ var _employee_due_increment_employee_due_increment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./employee-due-increment/employee-due-increment.component */ "./src/app/increment/employee-due-increment/employee-due-increment.component.ts");
/* harmony import */ var _annual_increment_report_annual_increment_report_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./annual-increment-report/annual-increment-report.component */ "./src/app/increment/annual-increment-report/annual-increment-report.component.ts");
/* harmony import */ var _increment_reg_increment_reg_increment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../increment/reg-increment/reg-increment.component */ "./src/app/increment/reg-increment/reg-increment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '', component: _increment_module__WEBPACK_IMPORTED_MODULE_2__["IncrementModule"], children: [
            { path: 'regincrement', component: _regular_increment_regular_increment_component__WEBPACK_IMPORTED_MODULE_3__["RegularIncrementComponent"] },
            { path: 'advincrement', component: _advance_increment_advance_increment_component__WEBPACK_IMPORTED_MODULE_4__["AdvanceIncrementComponent"] },
            { path: 'stopincrement', component: _stop_increment_stop_increment_component__WEBPACK_IMPORTED_MODULE_5__["StopIncrementComponent"] },
            { path: 'empdueincrmnt', component: _employee_due_increment_employee_due_increment_component__WEBPACK_IMPORTED_MODULE_6__["EmployeeDueIncrementComponent"] },
            { path: 'annualreport', component: _annual_increment_report_annual_increment_report_component__WEBPACK_IMPORTED_MODULE_7__["AnnualIncrementReportComponent"] },
            { path: 'regTestIncrement', component: _increment_reg_increment_reg_increment_component__WEBPACK_IMPORTED_MODULE_8__["RegIncrementComponent"] },
        ]
    }
];
var IncrementRoutingModule = /** @class */ (function () {
    function IncrementRoutingModule() {
    }
    IncrementRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], IncrementRoutingModule);
    return IncrementRoutingModule;
}());



/***/ }),

/***/ "./src/app/increment/increment.module.ts":
/*!***********************************************!*\
  !*** ./src/app/increment/increment.module.ts ***!
  \***********************************************/
/*! exports provided: IncrementModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncrementModule", function() { return IncrementModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _increment_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./increment-routing.module */ "./src/app/increment/increment-routing.module.ts");
/* harmony import */ var _increment_regular_increment_regular_increment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../increment/regular-increment/regular-increment.component */ "./src/app/increment/regular-increment/regular-increment.component.ts");
/* harmony import */ var _increment_advance_increment_advance_increment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../increment/advance-increment/advance-increment.component */ "./src/app/increment/advance-increment/advance-increment.component.ts");
/* harmony import */ var _increment_stop_increment_stop_increment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../increment/stop-increment/stop-increment.component */ "./src/app/increment/stop-increment/stop-increment.component.ts");
/* harmony import */ var _increment_employee_due_increment_employee_due_increment_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../increment/employee-due-increment/employee-due-increment.component */ "./src/app/increment/employee-due-increment/employee-due-increment.component.ts");
/* harmony import */ var _increment_annual_increment_report_annual_increment_report_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../increment/annual-increment-report/annual-increment-report.component */ "./src/app/increment/annual-increment-report/annual-increment-report.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _increment_reg_increment_reg_increment_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../increment/reg-increment/reg-increment.component */ "./src/app/increment/reg-increment/reg-increment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










//import { SharecomponentModule } from '../shared-module/shared-module.module;





var IncrementModule = /** @class */ (function () {
    function IncrementModule() {
    }
    IncrementModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_increment_regular_increment_regular_increment_component__WEBPACK_IMPORTED_MODULE_4__["RegularIncrementComponent"], _increment_advance_increment_advance_increment_component__WEBPACK_IMPORTED_MODULE_5__["AdvanceIncrementComponent"], _increment_stop_increment_stop_increment_component__WEBPACK_IMPORTED_MODULE_6__["StopIncrementComponent"],
                _increment_employee_due_increment_employee_due_increment_component__WEBPACK_IMPORTED_MODULE_7__["EmployeeDueIncrementComponent"], _increment_annual_increment_report_annual_increment_report_component__WEBPACK_IMPORTED_MODULE_8__["AnnualIncrementReportComponent"], _increment_reg_increment_reg_increment_component__WEBPACK_IMPORTED_MODULE_14__["RegIncrementComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _increment_routing_module__WEBPACK_IMPORTED_MODULE_3__["IncrementRoutingModule"],
                // SharecomponentModule,
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_10__["SharedModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_11__["MatTooltipModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_12__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_9__["NgxMatSelectSearchModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_13__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
            ]
        })
    ], IncrementModule);
    return IncrementModule;
}());



/***/ }),

/***/ "./src/app/increment/reg-increment/reg-increment.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/increment/reg-increment/reg-increment.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luY3JlbWVudC9yZWctaW5jcmVtZW50L3JlZy1pbmNyZW1lbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/increment/reg-increment/reg-increment.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/increment/reg-increment/reg-increment.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<form name=\"formain\" #mainf=\"ngForm\" novalidate>\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"col-md-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select [formControl]=\"designCtrl\" placeholder=\"Find Designation\" [(ngModel)]=\"ArrddlDesign.msDesigMastID\" (selectionChange)=\"refreshData()\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n                {{emp.desigDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"col-md-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select Pay Commission\" id=\"msCddirID\" [(ngModel)]=\"_pScaleObj.msCddirID\" name=\"msCddirID\" (selectionChange)=\"getEmpWithOrderNo($event)\">\r\n              <mat-option label=\"Select Pay Commission\">Select Pay Commission</mat-option>\r\n              <mat-option *ngFor=\"let _payscalelist of CommissionCodelist\" [value]=\"_payscalelist.msCddirID\">\r\n                {{ _payscalelist.cddirCodeText }}\r\n              </mat-option>\r\n            </mat-select>\r\n            <!--<mat-error><span *ngIf=\"mainf.submitted && msCddirID.invalid\">Pay Commission required</span></mat-error>-->\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv0\">\r\n\r\n    <div class=\"example-card mat-card\">\r\n      <div class=\"fom-title\">Release of Regular Increment</div>\r\n      <div class=\"col-md-12 mb-12 mb-15 pading-0 text-right\">\r\n        <button class=\"btn btn-labeled btn-success margin-0 text-center\" type=\"button\" style=\"cursor:pointer;\" (click)=\"showPH = !showPH\">\r\n          <span class=\"btn-label\"><i _ngcontent-c35=\"\" class=\"glyphicon glyphicon-plus\"></i></span>Add New Order Details\r\n        </button>\r\n      </div>\r\n\r\n      <table mat-table [dataSource]=\"dataSourceEmp\" class=\"even-odd-color tabel-form wid-100 recovery-tabel\">\r\n        <tr class=\"table-head\">\r\n          <ng-container matColumnDef=\"index\">\r\n            <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"OrderNo\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order No. </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"OrderDate\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order Date </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderDate|date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"NoofEmployee\">\r\n            <th mat-header-cell *matHeaderCellDef>No. of Emp. in Order</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.noofEmployee}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"Status\">\r\n            <th mat-header-cell *matHeaderCellDef>Status</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef> Action </th>\r\n            <td mat-cell *matCellDef=\"let element let k = index;\">\r\n              <a class=\"view-btn\" matTooltip=\"View\" (click)=\"getEmployeeForIncrement(element.id,k);\">View</a>\r\n            </td>\r\n          </ng-container>\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n\r\n\r\n\r\n\r\n    </div>\r\n  </div>\r\n\r\n</form>\r\n\r\n<!--Table For Add-->\r\n\r\n<div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv1\">\r\n  <p>\r\n    <b>Note :</b> 1) Please deselect any employee for whom Increment is not given. To deselect any employee Tick on checkbox and click on Remove button.\r\n    <br />\r\n    2) Please change w.e.f date for an employee to whom increment is not to be given from 1st of the month (due to leave etc.).\r\n    <br />\r\n    3) After saving Annual Increment details please click 'Forward to DDO' button\r\n  </p>\r\n  <!--<div class=\"example-card mat-card\">-->\r\n  <mat-card>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <form [formGroup]=\"formSave\" autocomplete=\"off\" (ngSubmit)=\"insertRegIncrementData();\">\r\n      <!--<ng-container formArrayName=\"IncrementSave\">-->\r\n\r\n\r\n        <!--<table mat-table [dataSource]=\"IncrementSave.controls\" *ngFor=\"let incSave of formSave.controls.IncrementSave.controls;let i = index\" matSort class=\"even-odd-color tabel-form wid-100 tabel-wraper tbl-input\">\r\n          <tr class=\"table-head\">\r\n            <ng-container matColumnDef=\"index\">\r\n              <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n              <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"select\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n                <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                              [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n              </th>\r\n\r\n              <td mat-cell *matCellDef=\"let row\">\r\n                <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null; addCheckedObj()\"\r\n                              [checked]=\"selectionAttach.isSelected(row)\">\r\n                </mat-checkbox>\r\n\r\n              </td>\r\n\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"EmpName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n              <td mat-cell *matCellDef=\"let element\">  {{incSave.empName}}</td>\r\n\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"Desigination\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{incSave.desigination}}  </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"OldBasic\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n              <td mat-cell *matCellDef=\"let element\">{{incSave.oldBasic}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"NewBasic\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{incSave.newBasic}} </td>\r\n            </ng-container>\r\n\r\n\r\n            <ng-container matColumnDef=\"WefDate\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Wef Date</th>\r\n              <td mat-cell *matCellDef=\"let element let l = index;\" [formGroup]=\"element\">\r\n                <mat-form-field floatLabel=\"never\">\r\n                  <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" formControlName=\"wefDate\" name=\"wefDate\"\r\n                         placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n                  <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #DOJ></mat-datepicker>\r\n                </mat-form-field>\r\n                {{element.wefDate | date}}\r\n                on\r\n              </td>\r\n              on\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"NextIncDate\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{incSave.nextIncDate}}</td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"PayLevel\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{incSave.payLevel}}</td>\r\n            </ng-container>\r\n\r\n\r\n            <ng-container matColumnDef=\"Remark\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n              <mat-cell *matCellDef=\"let element;\" [formGroup]=\"element\">\r\n                <mat-form-field>\r\n                  <input matInput placeholder=\"Remark\" formControlName=\"remark\" required />\r\n                </mat-form-field>\r\n              </mat-cell>\r\n\r\n            </ng-container>\r\n\r\n          </tr>\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns1\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns1; let i = index\"></tr>\r\n        </table>-->\r\n\r\n        <table matSort class=\"even-odd-color tabel-form wid-100 tabel-wraper tbl-input\" border=\"1\">\r\n          <tr class=\"table-head\">\r\n\r\n            <th>Index</th>\r\n            <th>Select</th>\r\n            <th>Employee Name</th>\r\n            <th>Desigination</th>\r\n            <th>Old Basic</th>\r\n            <th>New Basic</th>\r\n            <th>Wef Date</th>\r\n            <th>Next Inc Date</th>\r\n            <th>Pay Level</th>\r\n            <th>Remarks</th>\r\n          </tr>\r\n          <tbody formArrayName=\"IncrementSave\" *ngFor=\"let incSave of formSave.controls.IncrementSave.controls;let i = index\" >\r\n            <tr [formGroupName]=\"i\">\r\n\r\n              <td> {{i+1}}</td>\r\n              <td>\r\n                <mat-checkbox  (change)=\"getcheckbox($event)\" formControlName=\"empid\">\r\n                </mat-checkbox>\r\n              </td>\r\n              <td>  {{incSave.empName}}</td>\r\n              <td>  {{incSave.desigination}}</td>\r\n              <td>  {{incSave.oldBasic}}</td>\r\n              <td>  {{incSave.newBasic}}</td>\r\n\r\n              <td>\r\n                <mat-form-field floatLabel=\"never\">\r\n                  <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" formControlName=\"wefDate\" name=\"wefDate\"\r\n                         placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n                  <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #DOJ></mat-datepicker>\r\n                </mat-form-field>\r\n\r\n              </td>\r\n              <td>  {{incSave.nextIncDate}}</td>\r\n               \r\n\r\n              <td>  {{incSave.payLevel}}</td>\r\n              <td>\r\n                <!--<mat-form-field floatLabel=\"never\">\r\n    <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" formControlName=\"wefDate\" name=\"wefDate\"\r\n           placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n    <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n    <mat-datepicker #DOJ></mat-datepicker>\r\n  </mat-form-field>-->\r\n                <!--{{incSave.remark}}-->\r\n                <mat-form-field>\r\n                  <input matInput placeholder=\"Remark\" formControlName=\"remark\" required />\r\n                </mat-form-field>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n\r\n\r\n        </table>\r\n\r\n\r\n\r\n       \r\n      <!--</ng-container>-->\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"disablebtnflag\">Save</button>\r\n        <button type=\"reset\" class=\"btn btn-warning\" (click)=\"clearData();\" [disabled]=\"disablebtnflag\">Cancel</button>\r\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"forwardtocheckerFor();\" [disabled]=\"disablebtnflag\">Forward to DDO Checker</button>\r\n      </div>\r\n    </form>\r\n  </mat-card>\r\n  <!--</div>-->\r\n  <!--<mat-card>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color tabel-form wid-100 tabel-wraper tbl-input\">\r\n      <tbody formArrayName=\"IncrementSave\" *ngFor=\"let rateDetails  of getControls(); let i = index\">\r\n\r\n        <tr class=\"table-head\">\r\n          <ng-container matColumnDef=\"index\">\r\n            <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"select\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                            [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n            </th>\r\n\r\n            <td mat-cell *matCellDef=\"let row\">\r\n              <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null; addCheckedObj()\"\r\n                            [checked]=\"selectionAttach.isSelected(row)\">\r\n              </mat-checkbox>\r\n            </td>\r\n\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"empName\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n\r\n\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"desigination\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"oldBasic\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"newBasic\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.newBasic}} </td>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"wefDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Wef Date</th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <mat-form-field floatLabel=\"never\">\r\n                <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" formControlName=\"wefDate\" id=\"{{'wefDate'+i}}\" name=\"wefDate\"\r\n                       placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n                <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n                <mat-datepicker #DOJ></mat-datepicker>\r\n\r\n                <mat-error *ngIf=\"form.controls.IncrementSave.controls[i].controls.wefDate.error\">SlabNo is required</mat-error>\r\n              </mat-form-field>\r\n              {{element.wefDate | date}}\r\n            </td>\r\n\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"nextIncDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"payLevel\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"remark\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n            <td mat-cell *matCellDef=\"let element let j = index;\">\r\n              <textarea type=\"text\" required formControlName=\"remark\"  id=\"{{'remark'+i}}\" [disabled]=disableflag name=\"remark\" placeholder=\"Remark\"></textarea>\r\n               <mat-error *ngIf=\"form.controls.IncrementSave.controls[i].controls.remark.error\">SlabNo is required</mat-error>\r\n            </td>\r\n          </ng-container>\r\n\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns1\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns1; let i = index\"></tr>\r\n        </tbody>\r\n    </table>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </mat-card>-->\r\n\r\n\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-12 pading-0 text-center mb-20\" *ngIf=\"showhidediv3\">\r\n\r\n  <button class=\"btn btn-labeled btn-success add-list  text-center\" style=\"cursor:pointer;\" (click)=\"attachSelectedRows()\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-up\"></i></span>Add to list\r\n  </button>\r\n\r\n  <button class=\"btn btn-labeled btn-success remove-list  text-center\" style=\"cursor:pointer;\" (click)=\"deAttachSelectedRows()\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-down\"></i></span>Remove\r\n  </button>\r\n</div>\r\n\r\n<!--Table For Remove-->\r\n\r\n<div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv2\">\r\n\r\n  <mat-card>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilterDetach($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"even-odd-color tabel-form wid-100 tabel-wraper tbl-input\">\r\n      <tr class=\"table-head\">\r\n\r\n        <ng-container matColumnDef=\"select\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                          [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                          [checked]=\"selectionDeAttach.isSelected(row)\">\r\n            </mat-checkbox>\r\n          </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"EmpName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"Desigination\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"OldBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"NewBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.newBasic}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"NextIncDate\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"PayLevel\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n        </ng-container>\r\n\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2; let i = index\"></tr>\r\n    </table>\r\n    <mat-paginator #paginatorDetach [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </mat-card>\r\n  <!--</div>-->\r\n</div>\r\n\r\n\r\n\r\n<!---------------------Dialog Box For Add New Order Details  ------------------------>\r\n\r\n\r\n<app-dialog [(visible)]=\"showPH\">\r\n  <form name=\"form\" (ngSubmit)=\"f.valid && createOrder();\" #f=\"ngForm\" novalidate>\r\n    <mat-card>\r\n      <div class=\"fom-title\">Release of Regular Increment</div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Increment Certificate Order No.\" maxlength=\"20\" required name=\"orderNo\" [(ngModel)]=\"regInc.orderNo\" autocomplete=\"off\" #orderNo=\"ngModel\">\r\n          <mat-error><span *ngIf=\"f.submitted && orderNo.invalid\">Order no required</span></mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"regInc.orderDate \" name=\"orderDate\" #orderDate=\"ngModel\"\r\n                 placeholder=\"Order Date\" required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOJ></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!orderDate.errors?.required\">Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n        <button type=\"reset\" class=\"btn btn-warning\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n      </div>\r\n\r\n    </mat-card>\r\n  </form>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/increment/reg-increment/reg-increment.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/increment/reg-increment/reg-increment.component.ts ***!
  \********************************************************************/
/*! exports provided: RegIncrementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegIncrementComponent", function() { return RegIncrementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_payscale_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/payscale.service */ "./src/app/services/payscale.service.ts");
/* harmony import */ var _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/masters/PayscaleModel */ "./src/app/model/masters/PayscaleModel.ts");
/* harmony import */ var _services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/increment/regincrement.service */ "./src/app/services/increment/regincrement.service.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RegIncrementComponent = /** @class */ (function () {
    function RegIncrementComponent(payscale, regincrement, _formBuilder) {
        this.payscale = payscale;
        this.regincrement = regincrement;
        this._formBuilder = _formBuilder;
        this.IncrementData = [];
        this.regInc = {};
        this.regInc1 = [];
        this.dataDeAttach = [];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
        this.checkedObject = [];
        this.DcheckedObject = [];
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_7__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_7__["Subject"]();
        this.displayedColumns = ['OrderNo', 'OrderDate', 'NoofEmployee', 'Status', 'action'];
        this.displayedColumns1 = ['select', 'EmpName', 'Desigination', 'OldBasic', 'NewBasic', 'WefDate', 'NextIncDate', 'PayLevel', 'Remark']; //,
        this.displayedColumns2 = ['select', 'EmpName', 'Desigination', 'OldBasic', 'NewBasic', 'NextIncDate', 'PayLevel'];
        //End Attached and Detached
        // Get Employee for bind grid
        this.x = 0;
        // End
        // Save Increment Data
        this.id = 0;
    }
    RegIncrementComponent.prototype.ngOnInit = function () {
        this._pScaleObj = new _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_3__["PayscaleModel"]();
        this.bindPayCommission();
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.bindDesignation(this.PermDdoId);
        this.username = sessionStorage.getItem('username');
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = false;
        this.disableflag = false;
        this.showhidediv3 = false;
        this.disablebtnflag = false;
        this.formSave = this._formBuilder.group({
            IncrementSave: this._formBuilder.array([]) //this.addDuesRateDetailsFormGroup()
        });
    };
    RegIncrementComponent.prototype.addDuesRateDetailsFormGroup = function () {
        return this._formBuilder.group({
            id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            incID: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            empName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            oldBasic: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nextIncDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            oldWefDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            payLevel: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            noofIncrement: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderNo: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            wefDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            remark: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            desigCode: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            empCode: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            salaryMonth: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            loginUser: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            changeSubType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            orderTypeID: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            noofEmployee: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            status: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            desigination: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            clientIP: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            newBasic: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            empID: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            desigDesc: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            select: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    };
    Object.defineProperty(RegIncrementComponent.prototype, "IncrementSave", {
        get: function () {
            return this.formSave.get('IncrementSave');
        },
        enumerable: true,
        configurable: true
    });
    RegIncrementComponent.prototype.bindPayCommission = function () {
        var _this = this;
        this.payscale.BindCommissionCode().subscribe(function (result) {
            _this.CommissionCodelist = result;
        });
    };
    RegIncrementComponent.prototype.bindDesignation = function (value) {
        var _this = this;
        this.regincrement.getAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    RegIncrementComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    //Create Order No
    RegIncrementComponent.prototype.createOrder = function () {
        var _this = this;
        this.regInc.loginUser = this.username;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Regular Increment";
        this.regincrement.createOrderNo(this.regInc).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
            }
            _this.form.resetForm();
            _this.getEmpWithOrderNo(_this.regInc.DesigCode);
        });
    };
    RegIncrementComponent.prototype.getEmpWithOrderNo = function (value) {
        var _this = this;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Regular Increment";
        this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(function (result) {
            _this.dataSourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.dataeourceemp1 = result[0];
        });
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = true;
        this.showhidediv3 = false;
    };
    //End Order No
    // Attached and Detached Employee Data
    RegIncrementComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    RegIncrementComponent.prototype.isAllSelectedForDeAttach = function () {
        // debugger;
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    RegIncrementComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
        this.checkedObject = this.dataSource.data;
        this.checkedObject = this.selectionAttach.selected;
        //this.checkedObject = this.regInc1.wefDate;
        //this.checkedObject = this.regInc1.Remark; 
    };
    //working
    RegIncrementComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        //debugger;
        this.checkedObject = null;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
        this.checkedObject = this.dataSourceDeAttach.data;
        this.DcheckedObject = this.dataSourceDeAttach.data;
        // this.DcheckedObject = this.selectionDeAttach.selected;
    };
    RegIncrementComponent.prototype.addCheckedObj = function () {
        this.checkedObject = null;
        this.checkedObject = this.selectionAttach.selected;
    };
    RegIncrementComponent.prototype.deAttachSelectedRows = function () {
        var _this = this;
        // debugger;
        this.checkedObject = null;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                // this.dataDeAttach.push(item);
                _this.dataDeAttach.unshift(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSource.paginator = _this.paginator;
                _this.dataSourceDeAttach.paginator = _this.paginatorDetach;
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
        }
        else {
            alert("Please attach at least one Employee");
        }
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    };
    RegIncrementComponent.prototype.attachSelectedRows = function () {
        var _this = this;
        this.checkedObject = null;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                debugger;
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                //this.data.unshift(item);
                // this.dataSource = new MatTableDataSource(this.dataSource.data);
                // add a row from formarray .
                //  var abc = new FormArray(item);
                _this.IncrementSave.push(_this._formBuilder.group(item));
                // (<FormArray>this.formSave.get('IncrementSave')).push(this.addDuesRateDetailsFormGroup());
                //this.formSave.reset();
                _this.formSave.setControl('IncrementSave', _this.IncrementSave);
                //   this.formSave.addControl(item,);
                console.log(_this.IncrementSave);
                debugger;
                //this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true);
            this.dataSourceDeAttach.paginator = this.paginatorDetach;
        }
        else {
            alert("Please attach at least one Employee ");
        }
        //for (let i = 0; i < this.dataSourceEmp.filteredData.length; i++) {
        //  this.dataSourceEmp.filteredData[i].noofEmployee = 0;
        //}
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
        // this.dataeourceemp1[this.indexView].noofEmployee = this.totalAttach;    
    };
    RegIncrementComponent.prototype.getEmployeeForIncrement = function (ID, indexView) {
        var _this = this;
        this.indexView = indexView;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regincrement.getAllAsFormArray(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(function (result) {
            _this.data = result;
            debugger;
            _this.formSave.setControl('IncrementSave', result);
            //this.data.forEach(d => {
            //  this['wefDate' + this.x] = d.wefDate;
            //  this['remark' + this.x] = d.remark;
            //  this.x = this.x + 1;
            //});
            //this.checkedObject = result;
            //this.dataSource = new MatTableDataSource(result);
            //this.dataSource.paginator = this.paginator;
            //this.dataSource.sort = this.sort;
            //this.totalEmp = this.checkedObject.length;
            //this.disablebtnflag = false;
            //for (let i = 0; i < this.dataSource.filteredData.length; i++) {
            //  if (this.dataSource.filteredData[i].status == "F") {
            //    this.disableflag = true;
            //    this.showhidediv2 = false;
            //    this.showhidediv3 = false;
            //    this.disablebtnflag = true;
            //    this.dataSourceEmp.filteredData[this.indexView].status = "Forwarded";
            //  }
            //  else if (this.dataSource.filteredData[i].status == "E") {
            //    this.dataSourceEmp.filteredData[this.indexView].status = "Entered";
            //    this.disableflag = false;
            //    this.showhidediv2 = true;
            //    this.showhidediv3 = true;
            //    this.disablebtnflag = false;
            //  }
            //  else if (this.dataSource.filteredData[i].status == "V") {
            //    this.dataSourceEmp.filteredData[this.indexView].status = "Verified";
            //    this.showhidediv2 = false;
            //    this.showhidediv3 = false;
            //    this.disablebtnflag = false;
            //  }
            //  else {
            //    this.dataSourceEmp.filteredData[this.indexView].status = "";
            //    this.showhidediv2 = true;
            //    this.showhidediv3 = true;
            //    this.disablebtnflag = false;
            //  }
            //}
        });
        this.regincrement.getEmployeeForNonIncrement(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(function (result) {
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.dataDeAttach = result;
            _this.dataSourceDeAttach.paginator = _this.paginatorDetach; //sortDetach
            _this.dataSourceDeAttach.sort = _this.sortDetach;
        });
        this.showhidediv1 = true;
        this.showhidediv2 = true;
        this.showhidediv3 = true;
    };
    RegIncrementComponent.prototype.insertRegIncrementData = function () {
        var _this = this;
        debugger;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('Please attach at least one record!');
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    //let index: number = this.checkedObject.findIndex(c => c === d);
                    //alert(index)
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(d.empCode);
                    _this.IncrementData.push(d.payLevel);
                    _this.IncrementData.push(_this['wefDate' + _this.id]);
                    _this.IncrementData.push(_this['remark' + _this.id]);
                    _this.IncrementData.push(d.oldBasic);
                    _this.IncrementData.push(d.newBasic);
                    _this.IncrementData.push(_this.username);
                    _this.IncrementData.push(_this.ArrddlDesign.msDesigMastID);
                    _this.IncrementData.push(d.orderNo);
                    _this.IncrementData.push(d.orderDate);
                    _this.id = _this.id + 1;
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            this.regincrement.insertRegIncrementData(myArray).subscribe(function (result) {
                if (result != undefined) {
                    _this.deletepopup = false;
                    sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
                }
            });
        }
        this.IncrementData = [];
    };
    //End
    // Forward to checker
    RegIncrementComponent.prototype.forwardtocheckerFor = function () {
        var _this = this;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('Please attach at least one record!');
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.IncrementData.push(d.id);
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(_this.username);
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            this.regincrement.forwardtocheckerForInc(myArray).subscribe(function (result) {
                if (result != undefined) {
                    _this.deletepopup = false;
                    sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
                }
            });
        }
        this.IncrementData = [];
    };
    //End
    RegIncrementComponent.prototype.clearData = function () {
        this.showhidediv0 = false;
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv3 = false;
        this._pScaleObj.msCddirID = 0;
        this.form.resetForm();
    };
    RegIncrementComponent.prototype.refreshData = function () {
        this.clearData();
    };
    //Filter
    RegIncrementComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    RegIncrementComponent.prototype.applyFilterDetach = function (filterValue) {
        this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceDeAttach.paginatorDetach) {
            this.dataSourceDeAttach.paginatorDetach.firstPage();
        }
    };
    RegIncrementComponent.prototype.getcheckbox = function (item) {
        var a = this.formSave.setControl;
        //if (this.delarr.find(x => x == item)) {
        //  this.delarr.splice(this.delarr.indexOf(item), 1)
        //}
        //else {
        //  this.delarr.push(item);
        //}
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RegIncrementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RegIncrementComponent.prototype, "paginatorDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], RegIncrementComponent.prototype, "sortDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], RegIncrementComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], RegIncrementComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mainf'),
        __metadata("design:type", Object)
    ], RegIncrementComponent.prototype, "formain", void 0);
    RegIncrementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reg-increment',
            template: __webpack_require__(/*! ./reg-increment.component.html */ "./src/app/increment/reg-increment/reg-increment.component.html"),
            styles: [__webpack_require__(/*! ./reg-increment.component.css */ "./src/app/increment/reg-increment/reg-increment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_payscale_service__WEBPACK_IMPORTED_MODULE_2__["PayscaleService"], _services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_4__["RegincrementService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], RegIncrementComponent);
    return RegIncrementComponent;
}());



/***/ }),

/***/ "./src/app/increment/regular-increment/regular-increment.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/increment/regular-increment/regular-increment.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  textarea {\r\n  max-height: 38px;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  .bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jcmVtZW50L3JlZ3VsYXItaW5jcmVtZW50L3JlZ3VsYXItaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0NBQ3hCOztFQUVDO0lBQ0UsWUFBWTtHQUNiOztFQUVIO0VBQ0Usa0JBQWtCO0NBQ25COztFQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCOztFQUNEO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7Q0FDWjs7RUFFRDtFQUNFLG9GQUFvRjtFQUNwRix1QkFBdUI7Q0FDeEI7O0VBRUQsZUFBZTs7RUFFZjtFQUNFLFlBQVk7Q0FDYjs7RUFFRDtFQUNFLHFCQUFxQjtDQUN0Qjs7RUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7RUFFRDtFQUNFLG9HQUFvRztDQUNyRzs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0VBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBR0QsZ0JBQWdCOztFQUNoQjtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0VBRUQ7RUFDRTtJQUNFLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFdBQVc7R0FDWjtDQUNGOztFQUNELGFBQWE7O0VBQ2I7RUFDRSxvQ0FBb0M7Q0FDckM7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBR0Q7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0NBQ2hCOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztFQUNEO0VBQ0UsNEJBQTRCO0NBQzdCIiwiZmlsZSI6InNyYy9hcHAvaW5jcmVtZW50L3JlZ3VsYXItaW5jcmVtZW50L3JlZ3VsYXItaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcbnRleHRhcmVhIHtcclxuICBtYXgtaGVpZ2h0OiAzOHB4O1xyXG59XHJcbi5leGFtcGxlLWNhcmQge1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiA0cHhcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVyLWltYWdlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4vKiAyOS1qYW4tMTkgKi9cclxuXHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGwubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubXQtMTAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtZHJvcC1oZWFkIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLmJnY29sb3Ige1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZiZGE5OTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/increment/regular-increment/regular-increment.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/increment/regular-increment/regular-increment.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"col-md-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select [formControl]=\"designCtrl\" placeholder=\"Find Designation\" [(ngModel)]=\"ArrddlDesign.msDesigMastID\" (selectionChange)=\"refreshData()\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Find Designation...'\" [noEntriesFoundLabel]=\"'Designation is not found'\">\r\n                </ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredDesign | async\" [value]=\"emp.msDesigMastID\">\r\n                {{emp.desigDesc}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <div class=\"col-md-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select Pay Commission\" id=\"msCddirID\" [(ngModel)]=\"_pScaleObj.msCddirID\" name=\"msCddirID\" (selectionChange)=\"getEmpWithOrderNo()\">\r\n              <mat-option label=\"Select Pay Commission\">Select Pay Commission</mat-option>\r\n              <mat-option *ngFor=\"let _payscalelist of CommissionCodelist\" [value]=\"_payscalelist.msCddirID\">\r\n                {{ _payscalelist.cddirCodeText }}\r\n              </mat-option>\r\n            </mat-select>\r\n            <!--<mat-error><span *ngIf=\"mainf.submitted && msCddirID.invalid\">Pay Commission required</span></mat-error>-->\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv0\">\r\n\r\n    <div class=\"example-card mat-card\">\r\n      <div class=\"fom-title\">Release of Regular Increment</div>\r\n      <div class=\"col-md-12 mb-12 mb-15 pading-0 text-right\">\r\n        <button class=\"btn btn-labeled btn-success margin-0 text-center\" type=\"button\" style=\"cursor:pointer;\" (click)=\"showPH = !showPH\">\r\n          <span class=\"btn-label\"><i _ngcontent-c35=\"\" class=\"glyphicon glyphicon-plus\"></i></span>Add New Order Details\r\n        </button>\r\n      </div>\r\n\r\n      <table mat-table [dataSource]=\"dataSourceEmp\" class=\"mat-elevation-z8 even-odd-color\">\r\n        <tr class=\"table-head\">\r\n          <ng-container matColumnDef=\"index\">\r\n            <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"orderNo\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order No. </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"orderDate\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order Date </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderDate|date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"noofEmployee\">\r\n            <th mat-header-cell *matHeaderCellDef>No. of Emp. in Order</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.noofEmployee}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"status\">\r\n            <th mat-header-cell *matHeaderCellDef>Status</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef> Action </th>\r\n            <td mat-cell *matCellDef=\"let element let k = index;\">\r\n              <a class=\"btn btn-primary\" (click)=\"getEmployeeForIncrement(element.id,k);\">View</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"setDeleteId(element.id);deletepopup = !deletepopup\"> delete_forever </a>\r\n            </td>\r\n          </ng-container>\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n      <div [hidden]=\"isTableHasData\">\r\n        No  Records Found\r\n      </div>\r\n    </div>\r\n\r\n    \r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n \r\n\r\n\r\n  <!--Table For Add-->\r\n\r\n  <div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv1\">\r\n    <form name=\"formain\" #mainf=\"ngForm\" novalidate>\r\n      <p>\r\n        <b>Note :</b> 1) Please deselect any employee for whom Increment is not given. To deselect any employee Tick on checkbox and click on Remove button.\r\n        <br />\r\n        2) Please change w.e.f date for an employee to whom increment is not to be given from 1st of the month (due to leave etc.).\r\n        <br />\r\n        3) After saving Annual Increment details please click 'Forward to DDO' button\r\n      </p>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\" (click)=\"mainf.valid && insertRegIncrementData();\" [disabled]=\"disablebtnflag\">Save</button>\r\n        <button type=\"reset\" class=\"btn btn-warning\" (click)=\"clearData();\" [disabled]=\"disablebtnflag\">Cancel</button>\r\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"forwardtocheckerFor();\" [disabled]=\"disableFDflag\">Forward to DDO Checker</button>\r\n      </div>\r\n    </form>\r\n\r\n\r\n\r\n    <mat-card [ngClass]=\"bgcolor\">\r\n\r\n      <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n        <i class=\"material-icons icon-right\">search</i>\r\n      </mat-form-field>\r\n      <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n        <tr class=\"table-head\">\r\n          <ng-container matColumnDef=\"index\">\r\n            <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"select\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n              <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                            [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n            </th>\r\n\r\n            <td mat-cell *matCellDef=\"let row\">\r\n              <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null; addCheckedObj()\"\r\n                            [checked]=\"selectionAttach.isSelected(row)\">\r\n              </mat-checkbox>\r\n            </td>\r\n\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"empName\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"desigination\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"oldBasic\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"newBasic\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.newBasic}} </td>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"wefDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Wef Date</th>\r\n            <td mat-cell *matCellDef=\"let element let l = index;\">\r\n              <mat-form-field floatLabel=\"never\">\r\n                <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"element.wefDate\" name=\"wefDate\"\r\n                       placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n                <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n                <mat-datepicker #DOJ></mat-datepicker>\r\n                <!--<mat-error>\r\n              <span [hidden]=\"!wefDate.errors?.required\">Wef date is required</span>\r\n            </mat-error>-->\r\n              </mat-form-field>\r\n            </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"nextIncDate\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"payLevel\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"remark\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <!--<input matInput placeholder=\"Remarks\" required value={{element.remark}} [disabled]=disableflag name=\"remark\">-->\r\n              <textarea type=\"text\" required [(ngModel)]=\"element.remark\" [disabled]=disableflag name=\"remark\" placeholder=\"Remark\"></textarea>\r\n            </td>\r\n          </ng-container>\r\n\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns1\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns1; let i = index\"></tr>\r\n      </table>\r\n      <div [hidden]=\"isTableHasDataY\">\r\n        No  Records Found\r\n      </div>\r\n      <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n    </mat-card>\r\n    \r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n  <!--Table For Remove-->\r\n  <div class=\"col-md-12 col-lg-12 text-center btn-wraper\"></div>\r\n\r\n    <div class=\"col-md-12 col-lg-12 pading-0 text-center mb-20\" *ngIf=\"showhidediv3\">\r\n      <button class=\"btn btn-labeled btn-success add-list  text-center\" style=\"cursor:pointer;\" (click)=\"attachSelectedRows()\">\r\n        <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-up\"></i></span>Add to list\r\n      </button>\r\n      <button class=\"btn btn-labeled btn-success remove-list  text-center\" style=\"cursor:pointer;\" (click)=\"deAttachSelectedRows()\">\r\n        <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-down\"></i></span>Remove\r\n      </button>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv2\">\r\n      <mat-card>\r\n        <mat-form-field>\r\n          <input matInput (keyup)=\"applyFilterDetach($event.target.value)\" placeholder=\"Search\">\r\n          <i class=\"material-icons icon-right\">search</i>\r\n        </mat-form-field>\r\n        <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <tr class=\"table-head\">\r\n\r\n            <ng-container matColumnDef=\"select\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n                <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                              [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n              </th>\r\n              <td mat-cell *matCellDef=\"let row\">\r\n                <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                              [checked]=\"selectionDeAttach.isSelected(row)\">\r\n                </mat-checkbox>\r\n              </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"empName\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"desigination\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"oldBasic\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"newBasic\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.newBasic}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"nextIncDate\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n            </ng-container>\r\n\r\n            <ng-container matColumnDef=\"payLevel\">\r\n              <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n              <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n            </ng-container>\r\n\r\n          </tr>\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns2; let i = index\"></tr>\r\n        </table>\r\n        <div [hidden]=\"isTableHasDataX\">\r\n          No  Records Found\r\n        </div>\r\n        <mat-paginator #paginatorDetach [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n      </mat-card>\r\n      <!--</div>-->\r\n</div>\r\n\r\n\r\n\r\n  <!---------------------Dialog Box For Add New Order Details  ------------------------>\r\n\r\n\r\n  <app-dialog [(visible)]=\"showPH\">\r\n    <form name=\"form\" (ngSubmit)=\"f.valid && createOrder();\" #f=\"ngForm\" novalidate>\r\n      <mat-card>\r\n\r\n        <div>\r\n          <h4>New order- Regular Increment</h4><hr />\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput placeholder=\"Increment Certificate Order No.\" maxlength=\"20\" required name=\"orderNo\" [(ngModel)]=\"regInc.orderNo\"\r\n                   autocomplete=\"off\" #orderNo=\"ngModel\" pattern=\"^[A-Za-z0-9]+$\" onpaste=\"return false\">\r\n            <mat-error><span *ngIf=\"f.submitted && orderNo.invalid\">Order no required</span></mat-error>\r\n            <mat-error><span *ngIf=\"orderNo.errors?.pattern\">Special character not allowed</span></mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"regInc.orderDate \" name=\"orderDate\" #orderDate=\"ngModel\"\r\n                   placeholder=\"Order Date\" required>\r\n            <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n            <mat-datepicker #DOJ></mat-datepicker>\r\n            <mat-error>\r\n              <span [hidden]=\"!orderDate.errors?.required\">Order Date is required</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n          <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n          <button type=\"reset\" class=\"btn btn-warning\" (click)=\"reSetForm();\">Cancel</button>\r\n        </div>\r\n\r\n      </mat-card>\r\n    </form>\r\n  </app-dialog>\r\n\r\n\r\n<!------------------------------Delete Dialog Box---------------------------------->\r\n\r\n  <app-dialog [(visible)]=\"deletepopup\">\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n      <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteIncOrderDetails(setDeletIDOnPopup)\">Delete</button>\r\n        <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n      </div>\r\n\r\n    </div>\r\n  </app-dialog>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/increment/regular-increment/regular-increment.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/increment/regular-increment/regular-increment.component.ts ***!
  \****************************************************************************/
/*! exports provided: RegularIncrementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegularIncrementComponent", function() { return RegularIncrementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_payscale_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/payscale.service */ "./src/app/services/payscale.service.ts");
/* harmony import */ var _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/masters/PayscaleModel */ "./src/app/model/masters/PayscaleModel.ts");
/* harmony import */ var _services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/increment/regincrement.service */ "./src/app/services/increment/regincrement.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ELEMENT_DeAttachdata = [];
var ELEMENT_DATA = [];
var RegularIncrementComponent = /** @class */ (function () {
    function RegularIncrementComponent(payscale, regincrement) {
        this.payscale = payscale;
        this.regincrement = regincrement;
        this.IncrementData = [];
        this.regInc = {};
        this.regInc1 = [];
        this.dataDeAttach = [];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
        this.checkedObject = [];
        this.DcheckedObject = [];
        this.checktrue = false;
        this.isTableHasData = true;
        this.isTableHasDataY = true;
        this.isTableHasDataX = true;
        this.totalAttach = 0;
        this.designCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.filteredDesign = new rxjs__WEBPACK_IMPORTED_MODULE_6__["ReplaySubject"](1);
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.displayedColumns = ['orderNo', 'orderDate', 'noofEmployee', 'status', 'action'];
        this.displayedColumns1 = ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark']; //,
        this.displayedColumns2 = ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'nextIncDate', 'payLevel'];
    }
    RegularIncrementComponent.prototype.ngOnInit = function () {
        this._pScaleObj = new _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_2__["PayscaleModel"]();
        this.bindPayCommission();
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.bindDesignation(this.PermDdoId);
        this.username = sessionStorage.getItem('username');
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = false;
        this.disableflag = false;
        this.showhidediv3 = false;
        this.disablebtnflag = true;
        this.disableFDflag = true;
    };
    RegularIncrementComponent.prototype.bindPayCommission = function () {
        var _this = this;
        this.payscale.BindCommissionCode().subscribe(function (result) {
            _this.CommissionCodelist = result;
        });
    };
    RegularIncrementComponent.prototype.bindDesignation = function (value) {
        var _this = this;
        this.regincrement.getAllDesignation(value).subscribe(function (data) {
            _this.ArrddlDesign = data;
            _this.Design = data;
            _this.designCtrl.setValue(_this.Design);
            _this.filteredDesign.next(_this.Design);
        });
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesign();
        });
    };
    RegularIncrementComponent.prototype.filterDesign = function () {
        if (!this.Design) {
            return;
        }
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesign.next(this.Design.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        this.filteredDesign.next(this.Design.filter(function (Design) { return Design.desigDesc.toLowerCase().indexOf(search) > -1; }));
    };
    //Create Order No
    RegularIncrementComponent.prototype.createOrder = function () {
        var _this = this;
        this.regInc.loginUser = this.username;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Regular Increment";
        this.regincrement.createOrderNo(this.regInc).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                _this.showPH = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
            }
            _this.form.resetForm();
            _this.getEmpWithOrderNo();
        });
    };
    RegularIncrementComponent.prototype.getEmpWithOrderNo = function () {
        var _this = this;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Regular Increment";
        this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(function (result) {
            _this.dataSourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            if (_this.dataSourceEmp.filteredData.length > 0)
                _this.isTableHasData = true;
            else
                _this.isTableHasData = false;
        });
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = true;
        this.showhidediv3 = false;
    };
    RegularIncrementComponent.prototype.refreshData = function () {
        this.clearData();
        this._pScaleObj.msCddirID = 0;
        this.form.resetForm();
        this.dataSourceDeAttach = null;
    };
    RegularIncrementComponent.prototype.reSetForm = function () {
        this.form.resetForm();
    };
    //End Order No
    // Attached and Detached Employee Data
    RegularIncrementComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    RegularIncrementComponent.prototype.isAllSelectedForDeAttach = function () {
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    RegularIncrementComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
        this.checkedObject = this.dataSource.data;
        this.checkedObject = this.selectionAttach.selected;
    };
    //working
    RegularIncrementComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
        this.checkedObject = this.dataSourceDeAttach.data;
        this.DcheckedObject = this.dataSourceDeAttach.data;
        // this.DcheckedObject = this.selectionDeAttach.selected;
    };
    RegularIncrementComponent.prototype.addCheckedObj = function () {
        this.checkedObject = null;
        this.checkedObject = this.selectionAttach.selected;
    };
    RegularIncrementComponent.prototype.deAttachSelectedRows = function () {
        var _this = this;
        this.totalAttach = 0;
        this.checkedObject = null;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                // this.dataDeAttach.push(item);
                _this.dataDeAttach.unshift(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSource.paginator = _this.paginatorAttach;
                _this.dataSourceDeAttach.paginator = _this.paginatorDetach;
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
            if (this.dataSourceDeAttach.filteredData.length > 0)
                this.isTableHasDataX = true;
            else
                this.isTableHasDataX = false;
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()("Please attach at least one Employee");
        }
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    };
    RegularIncrementComponent.prototype.attachSelectedRows = function () {
        var _this = this;
        this.totalAttach = 0;
        this.disableFDflag = true;
        this.disableflag = false;
        this.checkedObject = null;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                _this.data.push(item);
                //this.data.unshift(item);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](_this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_7__["SelectionModel"](true, []);
            this.dataSourceDeAttach.paginator = this.paginatorDetach;
            this.dataSource.paginator = this.paginatorAttach;
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()("Please attach at least one Employee ");
        }
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
        if (this.dataSource.filteredData.length > 0) {
            this.disablebtnflag = false;
            this.isTableHasDataY = true;
        }
        else {
            this.disablebtnflag = true;
            this.isTableHasDataY = false;
        }
    };
    RegularIncrementComponent.prototype.getEmployeeForIncrement = function (ID, indexView) {
        var _this = this;
        this.totalAttach = 0;
        this.indexView = indexView;
        this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regincrement.getEmployeeForIncrements(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(function (result) {
            _this.data = result;
            // this.checkedObject = result;
            // this.dataSource = "";
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginatorAttach;
            _this.dataSource.sort = _this.sort;
            _this.totalEmp = _this.data.length;
            for (var i = 0; i < _this.dataSource.filteredData.length; i++) {
                if (_this.dataSource.filteredData[i].status == "F") {
                    _this.disableflag = true;
                    _this.showhidediv2 = false;
                    _this.showhidediv3 = false;
                    _this.disablebtnflag = true;
                    _this.disableFDflag = true;
                    _this.dataSourceEmp.filteredData[_this.indexView].status = "Forwarded";
                }
                else if (_this.dataSource.filteredData[i].status == "E") {
                    _this.dataSourceEmp.filteredData[_this.indexView].status = "Entered";
                    _this.disableflag = false;
                    _this.showhidediv2 = true;
                    _this.showhidediv3 = true;
                    _this.disablebtnflag = false;
                    _this.disableFDflag = false;
                }
                else if (_this.dataSource.filteredData[i].status == "V") {
                    _this.dataSourceEmp.filteredData[_this.indexView].status = "Verified";
                    _this.showhidediv2 = false;
                    _this.showhidediv3 = false;
                    _this.disablebtnflag = false;
                    _this.disableFDflag = false;
                }
                else {
                    _this.dataSourceEmp.filteredData[_this.indexView].status = "";
                    _this.showhidediv2 = true;
                    _this.showhidediv3 = true;
                    _this.disablebtnflag = true;
                    _this.disableFDflag = true;
                }
            }
            if (_this.dataSource.filteredData.length > 0)
                _this.isTableHasDataY = true;
            else
                _this.isTableHasDataY = false;
        });
        this.regincrement.getEmployeeForNonIncrement(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(function (result) {
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.dataDeAttach = result;
            _this.dataSourceDeAttach.paginator = _this.paginatorDetach; //sortDetach
            _this.dataSourceDeAttach.sort = _this.sortDetach;
            if (_this.dataSourceDeAttach.filteredData.length > 0)
                _this.isTableHasDataX = true;
            else
                _this.isTableHasDataX = false;
        });
        this.showhidediv1 = true;
        this.showhidediv2 = true;
        this.showhidediv3 = true;
    };
    // End
    // Save Increment Data
    RegularIncrementComponent.prototype.insertRegIncrementData = function () {
        var _this = this;
        this.checktrue = false;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            if (this.totalAttach == 0)
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('There are no record found!');
            else
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('Please attach record!');
        }
        else {
            this.IncrementData = [];
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(d.empCode);
                    _this.IncrementData.push(d.payLevel);
                    _this.IncrementData.push(d.wefDate);
                    if (d.wefDate == null) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('Wef date should not be blank');
                        _this.checktrue = true;
                        return false;
                    }
                    _this.IncrementData.push(d.remark);
                    _this.IncrementData.push(d.oldBasic);
                    _this.IncrementData.push(d.newBasic);
                    _this.IncrementData.push(_this.username);
                    _this.IncrementData.push(_this.ArrddlDesign.msDesigMastID);
                    _this.IncrementData.push(d.orderNo);
                    _this.IncrementData.push(d.orderDate);
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            if (this.checktrue == false) {
                this.regincrement.insertRegIncrementData(myArray).subscribe(function (result) {
                    if (result != undefined) {
                        _this.deletepopup = false;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
                    }
                });
            }
            this.disableFDflag = false;
        }
        this.IncrementData = [];
        this.selectionAttach.clear();
    };
    //End
    // Forward to checker
    RegularIncrementComponent.prototype.forwardtocheckerFor = function () {
        var _this = this;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()('Please attach records!');
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.IncrementData.push(d.id);
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(_this.username);
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            this.regincrement.forwardtocheckerForInc(myArray).subscribe(function (result) {
                if (result != undefined) {
                    _this.deletepopup = false;
                    sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
                }
            });
        }
        this.IncrementData = [];
    };
    //End
    RegularIncrementComponent.prototype.clearData = function () {
        this.showhidediv0 = false;
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv3 = false;
        this.deletepopup = false;
        this.form.resetForm();
        this._pScaleObj.msCddirID = 0;
    };
    //Filter
    RegularIncrementComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginatorAttach) {
            this.dataSource.paginatorAttach.firstPage();
        }
        if (this.dataSource.filteredData.length > 0)
            this.isTableHasDataY = true;
        else
            this.isTableHasDataY = false;
    };
    RegularIncrementComponent.prototype.applyFilterDetach = function (filterValue) {
        this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceDeAttach.paginatorDetach) {
            this.dataSourceDeAttach.paginatorDetach.firstPage();
        }
        if (this.dataSourceDeAttach.filteredData.length > 0)
            this.isTableHasDataX = true;
        else
            this.isTableHasDataX = false;
    };
    //End
    // Delete Order Details
    RegularIncrementComponent.prototype.setDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    RegularIncrementComponent.prototype.deleteIncOrderDetails = function (id) {
        var _this = this;
        this.regInc.OrderType = "Regular Increment";
        this.regincrement.deleteOrderDetails(id, this.regInc.OrderType).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default()(result);
            }
            _this.getEmpWithOrderNo();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RegularIncrementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RegularIncrementComponent.prototype, "paginatorDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorAttach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RegularIncrementComponent.prototype, "paginatorAttach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], RegularIncrementComponent.prototype, "sortDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], RegularIncrementComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], RegularIncrementComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mainf'),
        __metadata("design:type", Object)
    ], RegularIncrementComponent.prototype, "formain", void 0);
    RegularIncrementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-regular-increment',
            template: __webpack_require__(/*! ./regular-increment.component.html */ "./src/app/increment/regular-increment/regular-increment.component.html"),
            styles: [__webpack_require__(/*! ./regular-increment.component.css */ "./src/app/increment/regular-increment/regular-increment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_payscale_service__WEBPACK_IMPORTED_MODULE_1__["PayscaleService"], _services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_3__["RegincrementService"]])
    ], RegularIncrementComponent);
    return RegularIncrementComponent;
}());



/***/ }),

/***/ "./src/app/increment/regular-increment/regularmodel.ts":
/*!*************************************************************!*\
  !*** ./src/app/increment/regular-increment/regularmodel.ts ***!
  \*************************************************************/
/*! exports provided: regularModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regularModel", function() { return regularModel; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

var regularModel = /** @class */ (function () {
    function regularModel() {
    }
    regularModel.asFormGroup = function (regularModel) {
        var fg = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.id),
            incID: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.incID),
            empName: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.empName),
            oldBasic: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.oldBasic),
            nextIncDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.nextIncDate),
            oldWefDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.oldWefDate),
            payLevel: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.payLevel),
            noofIncrement: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.noofIncrement),
            orderNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.orderNo),
            orderDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.orderDate),
            orderType: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.orderType),
            wefDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.wefDate),
            remark: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.remark),
            desigCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.desigCode),
            empCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.empCode),
            salaryMonth: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.salaryMonth),
            loginUser: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.loginUser),
            changeSubType: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.changeSubType),
            orderTypeID: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.orderTypeID),
            noofEmployee: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.noofEmployee),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.status),
            desigination: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.desigination),
            clientIP: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.clientIP),
            newBasic: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.newBasic),
            empID: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.empID),
            msDesigMastID: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.msDesigMastID),
            desigDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](regularModel.desigDesc),
        });
        return fg;
    };
    return regularModel;
}());



/***/ }),

/***/ "./src/app/increment/stop-increment/stop-increment.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/increment/stop-increment/stop-increment.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n  .example-container > * {\r\n    width: 100%;\r\n  }\r\n\r\n  p {\r\n  font-family: Lato;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 300px;\r\n  margin: 4px\r\n}\r\n\r\n  .example-header-image {\r\n  background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\r\n  background-size: cover;\r\n}\r\n\r\n  /* 29-jan-19 */\r\n\r\n  table {\r\n  width: 100%;\r\n}\r\n\r\n  textarea {\r\n  max-height: 38px;\r\n}\r\n\r\n  .margin-rbl {\r\n  margin: 0 50px 10px 0\r\n}\r\n\r\n  .fild-one {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n  .mat-elevation-z8 {\r\n  box-shadow: 0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12);\r\n}\r\n\r\n  .mat-card {\r\n  box-shadow: none;\r\n  border: 1px solid #e4e2e2;\r\n}\r\n\r\n  .example-card {\r\n  max-width: 100% !important;\r\n}\r\n\r\n  .mat-form-field {\r\n  width: 181px;\r\n}\r\n\r\n  /* Responsive */\r\n\r\n  @media only screen and (min-width : 768px) and (max-width : 1024px) {\r\n  .mat-form-field {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_one {\r\n    width: 100%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 100%;\r\n  }\r\n}\r\n\r\n  @media only screen and (min-width : 1025px) and (max-width : 1397px) {\r\n  .detail_one {\r\n    width: 50%;\r\n  }\r\n\r\n  .detail_two {\r\n    width: 45%;\r\n  }\r\n}\r\n\r\n  /*31/jan/19*/\r\n\r\n  .m-20 {\r\n  margin: 0 20px 20px 20px !important;\r\n}\r\n\r\n  .mat-form-field-infix {\r\n  width: 100%;\r\n}\r\n\r\n  .example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 300px;\r\n}\r\n\r\n  .mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n  .mat-header-cell.mat-sort-header-sorted {\r\n  color: black;\r\n}\r\n\r\n  .mt-10 {\r\n  margin-top: 10px;\r\n}\r\n\r\n  .select-drop-head {\r\n  margin: 0 30px;\r\n}\r\n\r\n  .example-full-width {\r\n  width: 100%;\r\n}\r\n\r\n  .icon-right {\r\n  position: absolute;\r\n}\r\n\r\n  .bgcolor {\r\n  background-color: #fffbda99;\r\n}\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5jcmVtZW50L3N0b3AtaW5jcmVtZW50L3N0b3AtaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0NBQ3hCOztFQUVDO0lBQ0UsWUFBWTtHQUNiOztFQUVIO0VBQ0Usa0JBQWtCO0NBQ25COztFQUVEO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7Q0FDWjs7RUFFRDtFQUNFLG9GQUFvRjtFQUNwRix1QkFBdUI7Q0FDeEI7O0VBRUQsZUFBZTs7RUFFZjtFQUNFLFlBQVk7Q0FDYjs7RUFDRDtFQUNFLGlCQUFpQjtDQUNsQjs7RUFDRDtFQUNFLHFCQUFxQjtDQUN0Qjs7RUFFRDtFQUNFLG9CQUFvQjtDQUNyQjs7RUFFRDtFQUNFLG9HQUFvRztDQUNyRzs7RUFFRDtFQUNFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0VBRUQ7RUFDRSwyQkFBMkI7Q0FDNUI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBR0QsZ0JBQWdCOztFQUNoQjtFQUNFO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiOztFQUVEO0lBQ0UsWUFBWTtHQUNiO0NBQ0Y7O0VBRUQ7RUFDRTtJQUNFLFdBQVc7R0FDWjs7RUFFRDtJQUNFLFdBQVc7R0FDWjtDQUNGOztFQUNELGFBQWE7O0VBQ2I7RUFDRSxvQ0FBb0M7Q0FDckM7O0VBRUQ7RUFDRSxZQUFZO0NBQ2I7O0VBR0Q7RUFDRSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtDQUNsQjs7RUFFRDtFQUNFLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7O0VBRUQ7RUFDRSxhQUFhO0NBQ2Q7O0VBRUQ7RUFDRSxpQkFBaUI7Q0FDbEI7O0VBRUQ7RUFDRSxlQUFlO0NBQ2hCOztFQUVEO0VBQ0UsWUFBWTtDQUNiOztFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCOztFQUNEO0VBQ0UsNEJBQTRCO0NBQzdCIiwiZmlsZSI6InNyYy9hcHAvaW5jcmVtZW50L3N0b3AtaW5jcmVtZW50L3N0b3AtaW5jcmVtZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuICAuZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbnAge1xyXG4gIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIG1hcmdpbjogNHB4XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlci1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vYXNzZXRzL2ltZy9leGFtcGxlcy9zaGliYTEuanBnJyk7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLyogMjktamFuLTE5ICovXHJcblxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxudGV4dGFyZWEge1xyXG4gIG1heC1oZWlnaHQ6IDM4cHg7XHJcbn1cclxuLm1hcmdpbi1yYmwge1xyXG4gIG1hcmdpbjogMCA1MHB4IDEwcHggMFxyXG59XHJcblxyXG4uZmlsZC1vbmUge1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4IHtcclxuICBib3gtc2hhZG93OiAwIDJweCAxcHggLTFweCByZ2JhKDAsMCwwLC4yKSwgMCAxcHggMXB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCAzcHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbn1cclxuXHJcbi5tYXQtY2FyZCB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZTRlMmUyO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTgxcHg7XHJcbn1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDc2OHB4KSBhbmQgKG1heC13aWR0aCA6IDEwMjRweCkge1xyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfb25lIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLmRldGFpbF90d28ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiAxMDI1cHgpIGFuZCAobWF4LXdpZHRoIDogMTM5N3B4KSB7XHJcbiAgLmRldGFpbF9vbmUge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcblxyXG4gIC5kZXRhaWxfdHdvIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG59XHJcbi8qMzEvamFuLzE5Ki9cclxuLm0tMjAge1xyXG4gIG1hcmdpbjogMCAyMHB4IDIwcHggMjBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgbWluLXdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLm1hdC10YWJsZSB7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgbWF4LWhlaWdodDogNTAwcHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLWNlbGwubWF0LXNvcnQtaGVhZGVyLXNvcnRlZCB7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4ubXQtMTAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5zZWxlY3QtZHJvcC1oZWFkIHtcclxuICBtYXJnaW46IDAgMzBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pY29uLXJpZ2h0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLmJnY29sb3Ige1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZiZGE5OTtcclxufVxyXG5cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/increment/stop-increment/stop-increment.component.html":
/*!************************************************************************!*\
  !*** ./src/app/increment/stop-increment/stop-increment.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Raju verma 01/08/2019--->\r\n\r\n<form name=\"formain\" (ngSubmit)=\"mainf.valid && insertStopIncrementData();\" #mainf=\"ngForm\" novalidate>\r\n\r\n  <div class=\"alert\" [ngClass]=\"divbgcolor\" *ngIf=\"is_btnStatus\">\r\n    <strong> {{Message}}</strong>\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n      <span aria-hidden=\"true\" (click)=\"is_btnStatus=false\">&times;</span>\r\n    </button>&nbsp;\r\n  </div>\r\n\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n      <div class=\"col-md-3\">\r\n\r\n        <div class=\"col-md-12\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select Pay Commission\" id=\"msCddirID\" [(ngModel)]=\"_pScaleObj.msCddirID\" name=\"msCddirID\" (selectionChange)=\"refreshData()\">\r\n              <mat-option label=\"Select Pay Commission\">Select Pay Commission</mat-option>\r\n              <mat-option *ngFor=\"let _payscalelist of CommissionCodelist\" [value]=\"_payscalelist.msCddirID\">\r\n                {{ _payscalelist.cddirCodeText }}\r\n              </mat-option>\r\n            </mat-select>\r\n            <!--<mat-error><span *ngIf=\"mainf.submitted && msCddirID.invalid\">Pay Commission required</span></mat-error>-->\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"getEmpWithOrderNo()\">GO</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n  <div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv0\">\r\n\r\n    <div class=\"example-card mat-card\">\r\n      <div class=\"fom-title\">Stopping of Increment</div>\r\n      <div class=\"col-md-12 mb-12 mb-15 pading-0 text-right\">\r\n        <button class=\"btn btn-labeled btn-success margin-0 text-center\" type=\"button\" style=\"cursor:pointer;\" (click)=\"showPH = !showPH\">\r\n          <span class=\"btn-label\"><i _ngcontent-c35=\"\" class=\"glyphicon glyphicon-plus\"></i></span>Add New Order Details\r\n        </button>\r\n      </div>\r\n\r\n      <table mat-table [dataSource]=\"dataSourceEmp\" class=\"mat-elevation-z8 even-odd-color\">\r\n        <tr class=\"table-head\">\r\n          <ng-container matColumnDef=\"index\">\r\n            <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"orderNo\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order No. </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderNo}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"orderDate\">\r\n            <th mat-header-cell *matHeaderCellDef> Inc. Order Date </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.orderDate|date}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"noofEmployee\">\r\n            <th mat-header-cell *matHeaderCellDef>No. of Emp. in Order</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.noofEmployee}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"status\">\r\n            <th mat-header-cell *matHeaderCellDef>Status</th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef> Action </th>\r\n            <td mat-cell *matCellDef=\"let element let k = index;\">\r\n              <a class=\"btn btn-info\" matTooltip=\"View\" (click)=\"getEmployeeForIncrement(element.id,k);\">View</a>\r\n              <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"setDeleteId(element.id);deletepopup = !deletepopup\"> delete_forever </a>\r\n            </td>\r\n          </ng-container>\r\n        </tr>\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n      </table>\r\n      <div [hidden]=\"isTableHasData\">\r\n        No  Records Found\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"disablebtnflag\">Save</button>\r\n      <button type=\"reset\" class=\"btn btn-warning\" (click)=\"clearData();\" [disabled]=\"disablebtnflag\">Cancel</button>\r\n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"forwardtocheckerFor();\" [disabled]=\"disableFDflag\">Forward to DDO Checker</button>\r\n    </div>\r\n  </div>\r\n\r\n</form>\r\n\r\n<!--Table For Add-->\r\n\r\n<div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv1\">\r\n  <mat-card>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <!--<table mat-table [dataSource]=\"dataSource\" matSort class=\"even-odd-color\">-->\r\n    <table mat-table [dataSource]=\"dataSource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <tr class=\"table-head\">\r\n        <ng-container matColumnDef=\"index\">\r\n          <mat-header-cell *matHeaderCellDef> Index </mat-header-cell>\r\n          <mat-cell *matCellDef=\"let element; let i = index;\">{{i}}</mat-cell>\r\n        </ng-container>\r\n\r\n          <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n          <mat-checkbox (change)=\"$event ? masterToggleForAttach() : null\" [checked]=\"selectionAttach.hasValue() && isAllSelectedForAttach()\"\r\n                        [indeterminate]=\"selectionAttach.hasValue() && !isAllSelectedForAttach()\"></mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionAttach.toggle(row) : null; addCheckedObj()\"\r\n                        [checked]=\"selectionAttach.isSelected(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"empName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"desigination\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"oldBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"wefDate\">\r\n          el\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Wef Date</th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <mat-form-field floatLabel=\"never\">\r\n              <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"element.wefDate\" name=\"wefDate\"\r\n                     placeholder=\"Wef Date\" readonly [disabled]=disableflag required>\r\n              <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n              <mat-datepicker #DOJ></mat-datepicker>\r\n            </mat-form-field>\r\n          </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"nextIncDate\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            Next Inc Date\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"payLevel\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n        </ng-container>\r\n\r\n\r\n        <ng-container matColumnDef=\"remark\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>Remarks</th>\r\n          <td mat-cell *matCellDef=\"let element\">\r\n            <!--<input matInput placeholder=\"Remarks\" required [(ngModel)]=\"this['remark'+j]\" [disabled]=disableflag name=\"remark\">-->\r\n            <textarea type=\"text\" required [(ngModel)]=\"element.remark\" [disabled]=disableflag name=\"remark\" placeholder=\"remark\"></textarea>\r\n\r\n          </td>\r\n        </ng-container>\r\n\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns1\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns1; let i = index\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasDataY\">\r\n      No  Records Found\r\n    </div>\r\n    <mat-paginator #paginatorAttach [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </mat-card>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-lg-12 pading-0 text-center mb-20\" *ngIf=\"showhidediv3\">\r\n\r\n  <button class=\"btn btn-labeled btn-success add-list  text-center\" style=\"cursor:pointer;\" (click)=\"attachSelectedRows()\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-up\"></i></span>Add to list\r\n  </button>\r\n\r\n  <button class=\"btn btn-labeled btn-success remove-list  text-center\" style=\"cursor:pointer;\" (click)=\"deAttachSelectedRows()\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-arrow-down\"></i></span>Remove\r\n  </button>\r\n</div>\r\n\r\n<!--Table For Remove-->\r\n\r\n<div class=\"col-md-12 col-lg-12 mb-20\" *ngIf=\"showhidediv2\">\r\n  <mat-card>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilterDetach($event.target.value)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"dataSourceDeAttach\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n      <tr class=\"table-head\">\r\n\r\n        <ng-container matColumnDef=\"select\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            <mat-checkbox (change)=\"$event ? masterToggleForDeAttach() : null\" [checked]=\"selectionDeAttach.hasValue() && isAllSelectedForDeAttach()\"\r\n                          [indeterminate]=\"selectionDeAttach.hasValue() && !isAllSelectedForDeAttach()\"></mat-checkbox>\r\n          </th>\r\n          <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selectionDeAttach.toggle(row) : null\"\r\n                          [checked]=\"selectionDeAttach.isSelected(row)\">\r\n            </mat-checkbox>\r\n          </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"empName\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Employee Name</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.empName}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"desigination\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Desigination</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.desigination}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"oldBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Old Basic</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.oldBasic}} </td>\r\n        </ng-container>\r\n\r\n        <!--<ng-container matColumnDef=\"NewBasic\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> New Basic </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.newBasic}} </td>\r\n        </ng-container>-->\r\n\r\n        <ng-container matColumnDef=\"nextIncDate\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Next Inc Date  </th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.nextIncDate | date}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"payLevel\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header> Pay Level</th>\r\n          <td mat-cell *matCellDef=\"let element\"> {{element.payLevel}} </td>\r\n        </ng-container>\r\n\r\n      </tr>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2; let i = index\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasDataX\">\r\n      No  Records Found\r\n    </div>\r\n    <mat-paginator #paginatorDetach [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </mat-card>\r\n  <!--</div>-->\r\n</div>\r\n\r\n\r\n\r\n<!---------------------Dialog Box For Add New Order Details  ------------------------>\r\n\r\n\r\n<app-dialog [(visible)]=\"showPH\">\r\n  <form name=\"form\" (ngSubmit)=\"f.valid && createOrder();\" #f=\"ngForm\" novalidate>\r\n    <mat-card>\r\n      <div ><h4 class=\"card-title\">\r\n          New Order- Stopping of Increment</h4>\r\n      <hr/>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Increment Certificate Order No.\" maxlength=\"20\" required name=\"orderNo\" [(ngModel)]=\"regInc.orderNo\"\r\n                 autocomplete=\"off\" #orderNo=\"ngModel\" pattern=\"^[A-Za-z0-9]+$\" onpaste=\"return false\">\r\n          <mat-error><span *ngIf=\"f.submitted && orderNo.invalid\">Order no required</span></mat-error>\r\n          <mat-error><span *ngIf=\"orderNo.errors?.pattern\">Special character not allowed</span></mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-6\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"DOJ\" (click)=\"DOJ.open()\" [(ngModel)]=\"regInc.orderDate \" name=\"orderDate\" #orderDate=\"ngModel\"\r\n                 placeholder=\"Order Date\" required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"DOJ\"></mat-datepicker-toggle>\r\n          <mat-datepicker #DOJ></mat-datepicker>\r\n          <mat-error>\r\n            <span [hidden]=\"!orderDate.errors?.required\">Order Date is required</span>\r\n          </mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n        <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n        <button type=\"reset\" class=\"btn btn-warning\" (click)=\"reSetForm();\">Cancel</button>\r\n      </div>\r\n\r\n    </mat-card>\r\n  </form>\r\n</app-dialog>\r\n\r\n\r\n<!------------------------------Delete Dialog Box---------------------------------->\r\n\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteIncOrderDetails(setDeletIDOnPopup)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</app-dialog>\r\n"

/***/ }),

/***/ "./src/app/increment/stop-increment/stop-increment.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/increment/stop-increment/stop-increment.component.ts ***!
  \**********************************************************************/
/*! exports provided: StopIncrementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopIncrementComponent", function() { return StopIncrementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/increment/regincrement.service */ "./src/app/services/increment/regincrement.service.ts");
/* harmony import */ var _services_increment_stopincrement_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/increment/stopincrement.service */ "./src/app/services/increment/stopincrement.service.ts");
/* harmony import */ var _services_payscale_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/payscale.service */ "./src/app/services/payscale.service.ts");
/* harmony import */ var _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/masters/PayscaleModel */ "./src/app/model/masters/PayscaleModel.ts");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ELEMENT_DeAttachdata = [];
var ELEMENT_DATA = [];
var StopIncrementComponent = /** @class */ (function () {
    function StopIncrementComponent(regincrement, stopincrement, payscale) {
        this.regincrement = regincrement;
        this.stopincrement = stopincrement;
        this.payscale = payscale;
        this.IncrementData = [];
        this.regInc = {};
        this.regInc1 = [];
        this.dataDeAttach = [];
        this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.checkedObject = [];
        this.DcheckedObject = [];
        this.checktrue = false;
        this.isTableHasData = true;
        this.isTableHasDataY = true;
        this.isTableHasDataX = true;
        this.totalAttach = 0;
        this.btnCssClass = 'btn btn-success';
        this.displayedColumns = ['orderNo', 'orderDate', 'noofEmployee', 'status', 'action'];
        this.displayedColumns1 = ['select', 'empName', 'desigination', 'oldBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark']; //,
        this.displayedColumns2 = ['select', 'empName', 'desigination', 'oldBasic', 'nextIncDate', 'payLevel'];
    }
    StopIncrementComponent.prototype.ngOnInit = function () {
        this._pScaleObj = new _model_masters_PayscaleModel__WEBPACK_IMPORTED_MODULE_4__["PayscaleModel"]();
        this.username = sessionStorage.getItem('username');
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = false;
        this.disableflag = false;
        this.showhidediv3 = false;
        this.disablebtnflag = false;
        //this.GetEmpWithOrderNo();
        this.bindPayCommission();
    };
    StopIncrementComponent.prototype.bindPayCommission = function () {
        var _this = this;
        this.payscale.BindCommissionCode().subscribe(function (results) {
            _this.CommissionCodelist = results;
        });
    };
    //Create Order No
    StopIncrementComponent.prototype.createOrder = function () {
        var _this = this;
        this.regInc.loginUser = this.username;
        this.regInc.DesigCode = "0";
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Stop Increment";
        this.regincrement.createOrderNo(this.regInc).subscribe(function (result) {
            _this.Message = result;
            if (_this.Message != undefined) {
                _this.deletepopup = false;
                _this.showPH = false;
                _this.is_btnStatus = true;
                _this.divbgcolor = "alert-success";
                _this.Message = result;
            }
            _this.form.resetForm();
            //this.formain.resetForm();
            _this.getEmpWithOrderNo();
            _this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        });
    };
    StopIncrementComponent.prototype.getEmpWithOrderNo = function () {
        var _this = this;
        this.regInc.DesigCode = "0";
        this.regInc.PayLevel = this._pScaleObj.msCddirID;
        this.regInc.OrderType = "Stop Increment";
        this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(function (result) {
            _this.dataSourceEmp = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](result);
            if (_this.dataSourceEmp.filteredData.length > 0)
                _this.isTableHasData = true;
            else
                _this.isTableHasData = false;
        });
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv0 = true;
        this.showhidediv3 = false;
    };
    //End Order No
    // Attached and Detached Employee Data
    StopIncrementComponent.prototype.isAllSelectedForAttach = function () {
        var numSelected = this.selectionAttach.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    StopIncrementComponent.prototype.isAllSelectedForDeAttach = function () {
        var numSelected = this.selectionDeAttach.selected.length;
        var numRows = this.dataSourceDeAttach.data.length;
        return numSelected === numRows;
    };
    StopIncrementComponent.prototype.masterToggleForAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForAttach() ?
            this.selectionAttach.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selectionAttach.select(row); });
        this.checkedObject = this.dataSource.data;
        this.checkedObject = this.selectionAttach.selected;
    };
    //working
    StopIncrementComponent.prototype.masterToggleForDeAttach = function () {
        var _this = this;
        this.checkedObject = null;
        this.isAllSelectedForDeAttach() ?
            this.selectionDeAttach.clear() :
            this.dataSourceDeAttach.data.forEach(function (row) { return _this.selectionDeAttach.select(row); });
        this.checkedObject = this.dataSourceDeAttach.data;
        this.DcheckedObject = this.dataSourceDeAttach.data;
    };
    StopIncrementComponent.prototype.addCheckedObj = function () {
        this.checkedObject = null;
        this.checkedObject = this.selectionAttach.selected;
    };
    StopIncrementComponent.prototype.deAttachSelectedRows = function () {
        var _this = this;
        this.totalAttach = 0;
        this.checkedObject = null;
        if (this.selectionAttach.hasValue()) {
            this.selectionAttach.selected.forEach(function (item) {
                var index = _this.data.findIndex(function (d) { return d === item; });
                _this.dataSource.data.splice(index, 1);
                // this.dataDeAttach.push(item);
                _this.dataDeAttach.unshift(item);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](_this.dataDeAttach);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSource.paginator = _this.paginatorAttach;
                _this.dataSourceDeAttach.paginator = _this.paginatorDetach;
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            if (this.dataSourceDeAttach.filteredData.length > 0)
                this.isTableHasDataX = true;
            else
                this.isTableHasDataX = false;
        }
        else {
            alert("Please attach at least one employee");
        }
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    };
    StopIncrementComponent.prototype.attachSelectedRows = function () {
        var _this = this;
        this.totalAttach = 0;
        this.disableFDflag = true;
        this.disableflag = false;
        this.checkedObject = null;
        if (this.selectionDeAttach.hasValue()) {
            this.selectionDeAttach.selected.forEach(function (item) {
                var index = _this.dataDeAttach.findIndex(function (d) { return d === item; });
                _this.dataDeAttach.splice(index, 1);
                _this.data.push(item);
                //this.data.unshift(item);
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](_this.dataSource.data);
                _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](_this.dataDeAttach);
            });
            this.selectionAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.selectionDeAttach = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
            this.dataSource.paginator = this.paginatorAttach;
            this.dataSourceDeAttach.paginator = this.paginatorDetach;
        }
        else {
            alert("Please attach at least one employee ");
        }
        this.totalAttach = this.data.length;
        this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
        if (this.dataSource.filteredData.length > 0) {
            this.disablebtnflag = false;
            this.isTableHasDataY = true;
        }
        else {
            this.disablebtnflag = true;
            this.isTableHasDataY = false;
        }
    };
    StopIncrementComponent.prototype.getEmployeeForIncrement = function (ID, indexView) {
        var _this = this;
        this.indexView = indexView;
        this.stopincrement.getEmployeeStopIncrement(ID).subscribe(function (result) {
            _this.data = result;
            //this.checkedObject = result;
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](result);
            _this.dataSource.paginator = _this.paginatorAttach;
            _this.dataSource.sort = _this.sort;
            _this.totalEmp = _this.checkedObject.length;
            for (var i = 0; i < _this.dataSource.filteredData.length; i++) {
                if (_this.dataSource.filteredData[i].status == "F" || _this.dataSource.filteredData[i].status == "V") {
                    _this.disablebtnflag = true;
                    _this.disableFDflag = true;
                    _this.disableflag = true;
                    _this.showhidediv2 = false;
                    _this.showhidediv3 = false;
                }
                else if (_this.dataSource.filteredData[i].status == "E") {
                    _this.disablebtnflag = false;
                    _this.disableFDflag = false;
                    _this.disableflag = false;
                }
                else {
                    _this.disablebtnflag = false;
                    _this.disableFDflag = false;
                    _this.disableflag = false;
                }
            }
            if (_this.dataSource.filteredData.length > 0)
                _this.isTableHasDataY = true;
            else
                _this.isTableHasDataY = false;
        });
        this.stopincrement.getEmployeeForNotStop(ID).subscribe(function (result) {
            _this.dataSourceDeAttach = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"](result);
            _this.dataDeAttach = result;
            _this.dataSourceDeAttach.paginator = _this.paginatorDetach; //sortDetach
            _this.dataSourceDeAttach.sort = _this.sortDetach;
            if (_this.dataSourceDeAttach.filteredData.length > 0)
                _this.isTableHasDataX = true;
            else
                _this.isTableHasDataX = false;
        });
        this.showhidediv1 = true;
        this.showhidediv2 = true;
        this.showhidediv3 = true;
    };
    // End
    // Save Increment Data
    StopIncrementComponent.prototype.insertStopIncrementData = function () {
        var _this = this;
        debugger;
        this.checktrue = false;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            this.is_btnStatus = true;
            this.divbgcolor = "alert-danger";
            if (this.totalAttach == 0)
                this.Message = "There are no record found!";
            else
                this.Message = "Please attach record!";
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(d.empCode);
                    _this.IncrementData.push(d.payLevel);
                    _this.IncrementData.push(d.wefDate);
                    if (d.wefDate == null) {
                        _this.is_btnStatus = true;
                        _this.divbgcolor = "alert-danger";
                        _this.Message = "Wef date should not be blank";
                        _this.checktrue = true;
                        return false;
                    }
                    debugger;
                    _this.IncrementData.push(d.remark);
                    _this.IncrementData.push(d.oldBasic);
                    _this.IncrementData.push(_this.username);
                    // this.IncrementData.push(this.ArrddlDesign.msDesigMastID);
                    _this.IncrementData.push(d.orderNo);
                    _this.IncrementData.push(d.orderDate);
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            if (this.checktrue == false) {
                this.stopincrement.insertStopIncrementData(myArray).subscribe(function (result) {
                    _this.Message = result;
                    if (_this.Message != undefined) {
                        _this.deletepopup = false;
                        _this.is_btnStatus = true;
                        _this.divbgcolor = "alert-success";
                        _this.Message = result;
                    }
                });
            }
            this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        }
        this.IncrementData = [];
        this.selectionAttach.clear();
    };
    //End
    // Forward to checker
    StopIncrementComponent.prototype.forwardtocheckerFor = function () {
        var _this = this;
        var myArray = [];
        if (this.checkedObject == null || this.checkedObject.length == 0) {
            this.is_btnStatus = true;
            this.divbgcolor = "alert-danger";
            this.Message = "Please attach record!";
        }
        else {
            if (this.checkedObject != null || this.checkedObject.length != 0) {
                this.checkedObject.forEach(function (d) {
                    _this.IncrementData.push(d.id);
                    _this.IncrementData.push(d.empID);
                    _this.IncrementData.push(_this.username);
                    _this.regInc1 = _this.IncrementData;
                    myArray.push(_this.regInc1);
                    _this.IncrementData = [];
                });
            }
            this.stopincrement.forwardcheckerForStopInc(myArray).subscribe(function (result) {
                _this.Message = result;
                if (_this.Message != undefined) {
                    _this.deletepopup = false;
                    _this.is_btnStatus = true;
                    _this.divbgcolor = "alert-success";
                    _this.Message = result;
                }
                _this.bgcolor = "";
                setTimeout(function () {
                    _this.is_btnStatus = false;
                    _this.Message = '';
                }, 8000);
            });
        }
        this.IncrementData = [];
    };
    //End
    //Filter
    StopIncrementComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginatorAttach) {
            this.dataSource.paginatorAttach.firstPage();
        }
        if (this.dataSource.filteredData.length > 0)
            this.isTableHasDataY = true;
        else
            this.isTableHasDataY = false;
    };
    StopIncrementComponent.prototype.applyFilterDetach = function (filterValue) {
        this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceDeAttach.paginatorDetach) {
            this.dataSourceDeAttach.paginatorDetach.firstPage();
        }
        if (this.dataSourceDeAttach.filteredData.length > 0)
            this.isTableHasDataX = true;
        else
            this.isTableHasDataX = false;
    };
    //End
    StopIncrementComponent.prototype.clearData = function () {
        this.showhidediv0 = false;
        this.showhidediv1 = false;
        this.showhidediv2 = false;
        this.showhidediv3 = false;
        this.deletepopup = false;
        this.form.resetForm();
        this.bgcolor = "";
        this.btnCssClass = 'btn btn-success';
    };
    StopIncrementComponent.prototype.refreshData = function () {
        this.clearData();
        this.form.resetForm();
        this.dataSourceDeAttach = null;
    };
    StopIncrementComponent.prototype.reSetForm = function () {
        this.form.resetForm();
    };
    // Delete Order Details
    StopIncrementComponent.prototype.setDeleteId = function (msPayScaleID) {
        this.setDeletIDOnPopup = msPayScaleID;
    };
    StopIncrementComponent.prototype.deleteIncOrderDetails = function (id) {
        var _this = this;
        this.regInc.OrderType = "Stop Increment";
        this.regincrement.deleteOrderDetails(id, this.regInc.OrderType).subscribe(function (result) {
            if (result != undefined) {
                _this.deletepopup = false;
                _this.is_btnStatus = true;
                _this.divbgcolor = "alert-danger";
            }
            _this.getEmpWithOrderNo();
            _this.bgcolor = "";
            setTimeout(function () {
                _this.is_btnStatus = false;
                _this.Message = '';
            }, 8000);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"])
    ], StopIncrementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"])
    ], StopIncrementComponent.prototype, "paginatorDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('paginatorAttach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"])
    ], StopIncrementComponent.prototype, "paginatorAttach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sortDetach'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"])
    ], StopIncrementComponent.prototype, "sortDetach", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"])
    ], StopIncrementComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", Object)
    ], StopIncrementComponent.prototype, "form", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mainf'),
        __metadata("design:type", Object)
    ], StopIncrementComponent.prototype, "formain", void 0);
    StopIncrementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stop-increment',
            template: __webpack_require__(/*! ./stop-increment.component.html */ "./src/app/increment/stop-increment/stop-increment.component.html"),
            styles: [__webpack_require__(/*! ./stop-increment.component.css */ "./src/app/increment/stop-increment/stop-increment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_increment_regincrement_service__WEBPACK_IMPORTED_MODULE_1__["RegincrementService"], _services_increment_stopincrement_service__WEBPACK_IMPORTED_MODULE_2__["StopincrementService"], _services_payscale_service__WEBPACK_IMPORTED_MODULE_3__["PayscaleService"]])
    ], StopIncrementComponent);
    return StopIncrementComponent;
}());



/***/ }),

/***/ "./src/app/services/increment/advincrement.service.ts":
/*!************************************************************!*\
  !*** ./src/app/services/increment/advincrement.service.ts ***!
  \************************************************************/
/*! exports provided: AdvincrementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdvincrementService", function() { return AdvincrementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AdvincrementService = /** @class */ (function () {
    function AdvincrementService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    AdvincrementService.prototype.CreateAdvIncrement = function (advObj) {
        return this.httpclient.post(this.config.CreateAdvIncrement, advObj, { responseType: 'text' });
    };
    AdvincrementService.prototype.getAllDesignation = function (PermDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('PermDdoId', PermDdoId);
        return this.httpclient.get(this.config.GetEmployeeDesignation + "/GetEmployeeDesignation", { params: params });
    };
    AdvincrementService.prototype.GetIncrementDetails = function (empcode) {
        return this.httpclient.get(this.config.GetIncrementDetails + empcode, {});
    };
    AdvincrementService.prototype.EditAdvIncrement = function (incID) {
        return this.httpclient.get(this.config.EditAdvIncrement + incID, {});
    };
    AdvincrementService.prototype.DeleteAdvDetails = function (incID) {
        return this.httpclient.post(this.config.DeleteAdvDetails + incID, '', { responseType: 'text' });
    };
    AdvincrementService.prototype.Forwardtochecker = function (advObj) {
        debugger;
        return this.httpclient.post(this.config.Forwardtochecker, advObj, { responseType: 'text' });
    };
    AdvincrementService.prototype.GetEmployeeByDesigPayComm = function (msDesignID, paycomm) {
        return this.httpclient.get(this.config.GetEmployeeByDesigPayComm + '?msDesignID=' + msDesignID + ' &paycomm=' + paycomm, {});
    };
    AdvincrementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], AdvincrementService);
    return AdvincrementService;
}());



/***/ }),

/***/ "./src/app/services/increment/annualincrement.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/increment/annualincrement.service.ts ***!
  \***************************************************************/
/*! exports provided: AnnualincrementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnnualincrementService", function() { return AnnualincrementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AnnualincrementService = /** @class */ (function () {
    function AnnualincrementService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    AnnualincrementService.prototype.getAllDesignation = function (PermDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('PermDdoId', PermDdoId);
        return this.httpclient.get(this.config.GetEmployeeDesignation + "/GetEmployeeDesignation", { params: params });
    };
    AnnualincrementService.prototype.GetSalaryMonth = function () {
        return this.httpclient.get(this.config.GetSalaryMonth, {});
    };
    AnnualincrementService.prototype.GetOrderWithEmployee = function (paycode) {
        return this.httpclient.get(this.config.GetOrderWithEmployee + '?paycodeID=' + paycode, {});
    };
    AnnualincrementService.prototype.GetAnnualIncReportData = function (paycode, orderID) {
        return this.httpclient.get(this.config.GetAnnualIncReportData + '?paycodeID=' + paycode + '&orderID=' + orderID, {});
    };
    AnnualincrementService.prototype.GetEmpDueForIncReport = function (designCode) {
        return this.httpclient.get(this.config.GetEmpDueForIncReport + '?designCode=' + designCode, {});
    };
    AnnualincrementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], AnnualincrementService);
    return AnnualincrementService;
}());



/***/ }),

/***/ "./src/app/services/increment/regincrement.service.ts":
/*!************************************************************!*\
  !*** ./src/app/services/increment/regincrement.service.ts ***!
  \************************************************************/
/*! exports provided: RegincrementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegincrementService", function() { return RegincrementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _increment_regular_increment_regularmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../increment/regular-increment/regularmodel */ "./src/app/increment/regular-increment/regularmodel.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var RegincrementService = /** @class */ (function () {
    function RegincrementService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    RegincrementService.prototype.createOrderNo = function (advObj) {
        return this.httpclient.post(this.config.CreateOrderNo, advObj, { responseType: 'text' });
    };
    RegincrementService.prototype.getAllDesignation = function (PermDdoId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('PermDdoId', PermDdoId);
        return this.httpclient.get(this.config.GetEmployeeDesignation + "/GetEmployeeDesignation", { params: params });
    };
    RegincrementService.prototype.getOrderDetails = function (design, paycode, orderType) {
        return this.httpclient.get(this.config.GetOrderDetails + '?designID=' + design + ' &paycodeID=' + paycode + '&orderType= ' + orderType, {});
    };
    RegincrementService.prototype.GetEmployeeForIncrement = function (design, paycode, orderID) {
        return this.httpclient.get(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
    };
    RegincrementService.prototype.getAllAsFormArray = function (design, paycode, orderID) {
        return this.GetEmployeeForIncrement(design, paycode, orderID).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (albums) {
            // Maps all the albums into a formGroup defined in tge album.model.ts
            var fgs = albums.map(_increment_regular_increment_regularmodel__WEBPACK_IMPORTED_MODULE_3__["regularModel"].asFormGroup);
            debugger;
            return new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormArray"](fgs);
        }));
    };
    RegincrementService.prototype.getAllAsFormArray11 = function (design, paycode, orderID) {
        return this.httpclient.get(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
    };
    RegincrementService.prototype.getEmployeeForIncrements = function (design, paycode, orderID) {
        return this.httpclient.get(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
    };
    RegincrementService.prototype.getEmployeeForNonIncrement = function (design, paycode, orderID) {
        return this.httpclient.get(this.config.GetEmployeeForNonIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
    };
    RegincrementService.prototype.insertRegIncrementData = function (advObj) {
        return this.httpclient.post(this.config.AddEmpForIncrement, advObj, { responseType: 'text' });
    };
    RegincrementService.prototype.forwardtocheckerForInc = function (advObj) {
        return this.httpclient.post(this.config.ForwardtocheckerForInc, advObj, { responseType: 'text' });
    };
    RegincrementService.prototype.deleteOrderDetails = function (id, type) {
        return this.httpclient.post(this.config.deleteOrderDetails + '?id=' + id + '&type= ' + type, '', { responseType: 'text' });
    };
    RegincrementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], RegincrementService);
    return RegincrementService;
}());



/***/ }),

/***/ "./src/app/services/increment/stopincrement.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/increment/stopincrement.service.ts ***!
  \*************************************************************/
/*! exports provided: StopincrementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StopincrementService", function() { return StopincrementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var StopincrementService = /** @class */ (function () {
    function StopincrementService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    StopincrementService.prototype.getEmployeeStopIncrement = function (orderID) {
        debugger;
        return this.httpclient.get(this.config.GetEmployeeStopIncrement + '?orderID= ' + orderID, {});
    };
    StopincrementService.prototype.getEmployeeForNotStop = function (orderID) {
        return this.httpclient.get(this.config.GetEmployeeForNotStop + '?orderID= ' + orderID, {});
    };
    StopincrementService.prototype.insertStopIncrementData = function (advObj) {
        return this.httpclient.post(this.config.InsertStopIncrementData, advObj, { responseType: 'text' });
    };
    StopincrementService.prototype.forwardcheckerForStopInc = function (advObj) {
        return this.httpclient.post(this.config.ForwardcheckerForStopInc, advObj, { responseType: 'text' });
    };
    StopincrementService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], StopincrementService);
    return StopincrementService;
}());



/***/ })

}]);
//# sourceMappingURL=increment-increment-module.js.map