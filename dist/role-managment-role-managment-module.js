(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["role-managment-role-managment-module"],{

/***/ "./src/app/role-managment/menu/menu.component.css":
/*!********************************************************!*\
  !*** ./src/app/role-managment/menu/menu.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n\r\n\r\ninput[type=\"file\"] {\r\n  display: none;\r\n}\r\n\r\n.custom-file-upload {\r\n  border: 2px solid #ccc;\r\n  display: inline-block;\r\n  padding: 6px 12px;\r\n  cursor: pointer;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm9sZS1tYW5hZ21lbnQvbWVudS9tZW51LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUlBO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsdUJBQXVCO0VBQ3ZCLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIsZ0JBQWdCO0NBQ2pCIiwiZmlsZSI6InNyYy9hcHAvcm9sZS1tYW5hZ21lbnQvbWVudS9tZW51LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcblxyXG5cclxuaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5jdXN0b20tZmlsZS11cGxvYWQge1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICNjY2M7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/role-managment/menu/menu.component.html":
/*!*********************************************************!*\
  !*** ./src/app/role-managment/menu/menu.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12\">\r\n  <div class=\"example-card mat-card\">\r\n    <!--<div class=\"panel panel-body panel-default col-md-12\">\r\n      <div class=\"col-lg-1\"></div>\r\n      <div class=\"col-lg-10\">\r\n        <div class=\"form-group col-md-6 panel panel-body panel-default\">\r\n          <button type=\"button\" class=\"btn btn-info\" (click)=\"TextChange();showMenu = !showMenu\">Create Menu</button>\r\n        </div>\r\n\r\n      </div>\r\n\r\n      <div class=\"col-lg-1\"></div>\r\n    </div>-->\r\n    <!--Table Show Role And Revoke User Details-->\r\n\r\n\r\n    <div class=\"fom-title\">Menu List</div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\" [(ngModel)]=\"TxtSearch\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n    <button type=\"button\" class=\"btn btn-labeled btn-success margin-0 float-r\" (click)=\"TextChange();showMenu = !showMenu\">\r\n      <span class=\"btn-label\"><i class=\"glyphicon glyphicon-plus\"></i></span>Create Menu\r\n    </button>\r\n\r\n\r\n    <table mat-table [dataSource]=\"menudataSource\" matSort class=\"mat-elevation-z8 even-odd-color mat-table\">\r\n      <!-- Column -->\r\n      <ng-container matColumnDef=\"mainMenuName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header width=\"30%\"> Menu Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.mainMenuName}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"parentMenuName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header width=\"30%\">Parent Menu Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.parentMenuName}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"menuURL\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header width=\"30%\"> Menu URL </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.menuURL}} </td>\r\n      </ng-container>\r\n      <!--<ng-container matColumnDef=\"isActive\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Is Active </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.isActive}} </td>\r\n      </ng-container>-->\r\n      <!-- Action Column -->\r\n      <!--<ng-container matColumnDef=\"Edit\">\r\n      <th mat-header-cell *matHeaderCellDef> Edit </th>\r\n      <td mat-cell *matCellDef=\"let element\">-->\r\n      <!--<a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"UpdateMenu(element.msMenuID,element.mainMenuName,element.menuURL,element.parentMenu,element.isActive);showMenu = !showMenu\">edit</a>-->\r\n      <!--</td>\r\n      </ng-container>-->\r\n      <ng-container matColumnDef=\"Active\">\r\n        <th mat-header-cell *matHeaderCellDef width=\"10%\"> Actions </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!--<a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"FillFormDeletePop(element.msMenuID,element.mainMenuName,element.isActive);DeleteMenu = !DeleteMenu\"> delete_forever </a>-->\r\n          <div class=\"float-left\" *ngIf=\"element.createdStatus =='C'\">\r\n            <div class=\"float-left\"><a class=\"material-icons i-edit text-center\" (click)=\"UpdateMenu(element.msMenuID,element.menuURL);showMenu = !showMenu\" matTooltip=\"Edit\">edit</a></div>\r\n          </div>\r\n          <div *ngIf=\"element.createdStatus =='P'\">\r\n            <h5><b><A>Predefined</A></b></h5>\r\n          </div>\r\n          <div *ngIf=\"element.createdStatus =='C'\">\r\n            <div class=\"float-left\">\r\n              <div *ngIf=\"element.isActive =='Active'\">\r\n                <a class=\"material-icons i-activate\" matTooltip=\"Dactivate\" (click)=\"FillFormDeletePop(element.msMenuID,element.mainMenuName,element.isActive);DeleteMenu = !DeleteMenu\">check_circle</a>\r\n              </div>\r\n              <div *ngIf=\"element.isActive =='Not Active'\" class=\"float-left\">\r\n                <a class=\"material-icons i-dactivate\" matTooltip=\"Activate\" (click)=\"FillFormDeletePop(element.msMenuID,element.mainMenuName,element.isActive);DeleteMenu = !DeleteMenu\">cancel</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\">\r\n      <font color=\"red\">Data Not Found...</font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[10, 15, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n\r\n    <!--End Of Table Show Role And Revoke User Details-->\r\n\r\n  </div>\r\n</div>\r\n\r\n\r\n<app-dialog [(visible)]=\"showMenu\">\r\n  <form (ngSubmit)=\"roleassign.valid && SaveMenu();\" #roleassign=\"ngForm\" novalidate>\r\n\r\n    <div class=\"panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">{{MwnuHeaderText}}</h3>\r\n      <div class=\"col-sm-12 col-md-12 col-lg-12 select-type\">\r\n\r\n        <div class=\"col-sm-6 col-md-4 col-lg-4 pading-0\">\r\n          <div classclass=\"wid-100\">\r\n            <label class=\"label-font\">Select Type</label>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6 col-md-6 col-lg-6\">\r\n          <div classclass=\"wid-100\">\r\n            <mat-radio-group name=\"selType\" [(ngModel)]=\"selrdType\">\r\n              <mat-radio-button value=\"Root\" (click)=\"HideandShowradio('Root')\">Root Menu</mat-radio-button> <!--(click)=\"ChangeFormfromstate('Central')\"-->\r\n              <mat-radio-button value=\"SubMenu\" (click)=\"HideandShowradio('SubMenu')\">Sub Menu</mat-radio-button>\r\n\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <input type=\"text\" name=\"someData\" [(ngModel)]=\"hdnMenuId\" style=\"display: none;\" />\r\n      <div class=\"form-group col-md-12\">\r\n        <div class=\"form-group col-md-4\" *ngIf=\"IsShowHiddl\">\r\n\r\n          <!--<mat-form-field>\r\n      <select matNativeControl [(ngModel)]=\"mstMenu.parentMenu\" name=\"parentMenu\" #parentMenu=\"ngModel\" required>\r\n\r\n        <option *ngFor=\"let parentmenus of ArrMenus\" value=\"{{parentmenus.msMenuID}}\">{{parentmenus.mainMenuName}}</option>\r\n      </select>\r\n    </mat-form-field>-->\r\n          <!--<mat-form-field>\r\n      <mat-select placeholder=\"Select EmpCode\" [(ngModel)]=\"commonEmpCode\" name=\"Empcode\" #Empcode=\"ngModel\" (selectionChange)=\"GetEmpCode($event.value)\">\r\n        <mat-option>\r\n          <ngx-mat-select-search [formControl]=\"empFilterCtrl\" [placeholderLabel]=\"'Find Employee...'\"></ngx-mat-select-search>\r\n        </mat-option>\r\n\r\n        <mat-option *ngFor=\"let emp of filteredEmp | async\" [value]=\"emp.id\">\r\n          {{emp.name}}\r\n        </mat-option>\r\n      </mat-select>\r\n\r\n    </mat-form-field>-->\r\n\r\n\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select Menu\" [(ngModel)]=\"mstMenu.parentMenu\" name=\"parentMenu\" #parentMenu=\"ngModel\">\r\n\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"MenuFilterCtrl\" [placeholderLabel]=\"'Find Menu...'\"  [noEntriesFoundLabel]=\"'Menu is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let item of filteredMenu |async \" [value]=\"item.msMenuID\" [disabled]=\"Ishowhide\">\r\n                {{item.mainMenuName}}\r\n              </mat-option>\r\n              <!--<mat-option [value]=\"0\">\r\n                Select\r\n              </mat-option>-->\r\n            </mat-select>\r\n          </mat-form-field>\r\n\r\n\r\n          <!--<mat-form-field class=\"wid-100\">\r\n      <mat-select placeholder=\"Select Menu\" [(ngModel)]=\"mstMenu.parentMenu\" name=\"parentMenu\" #parentMenu=\"ngModel\" required>\r\n        <mat-option *ngFor=\"let parentmenus of ArrMenus\" [value]=\"parentmenus.msMenuID\" [disabled]=\"Ishowhide\">{{parentmenus.mainMenuName}}</mat-option>\r\n      </mat-select>\r\n    </mat-form-field>-->\r\n        </div>\r\n        <div class=\"form-group col-md-4\">\r\n          <mat-form-field class=\"example-full-width\">\r\n\r\n            <input matInput placeholder=\"Enter Menu Name\" required [(ngModel)]=\"mstMenu.mainMenuName\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" autocomplete=\"off\" name=\"mainMenuName\" #mainMenuName=\"ngModel\"><!--required-->\r\n            <mat-error>\r\n              <span *ngIf=\"mainMenuName.invalid\">Special charector are not allowed </span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"form-group col-md-4\">\r\n          <mat-form-field class=\"example-full-width\">\r\n            <input matInput placeholder=\"Enter Menu Url (abc/abc/abc)...\" [(ngModel)]=\"mstMenu.menuURL\" pattern=\"^[A-Za-z]+[/][A-Za-z]+[/][A-Za-z/]+$\" autocomplete=\"off\" name=\"menuURL\" required #menuURL=\"ngModel\"><!--required  [disabled]=\"Ishowhide\"-->\r\n            <mat-error>\r\n              <span *ngIf=\"menuURL.invalid\">Enter Currect Formate Url (abc/abc/abc)</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"col-md-12 col-lg-4\">\r\n          <mat-list-item class=\"primary-imenu-item\" role=\"listitem\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <mat-select placeholder=\"Role\"\r\n                          name=\"RoleSIDS\"\r\n                          class=\"filter-select\"\r\n                          [(ngModel)]=\"mstMenu.RoleSIDS\"\r\n                          [compareWith]=\"equalsRole\"\r\n                          required\r\n                          multiple\r\n                          #RoleSIDS=\"ngModel\">\r\n\r\n                <mat-option *ngFor=\"let role of ArrRoles\" [value]=\"role\" [disabled]=\"Ishowhide\">{{role.roleName}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </mat-list-item>\r\n        </div>\r\n        <!--<div class=\"form-group col-md-4\">\r\n\r\n    <label id=\"lblMenuUrl\" class=\"custom-file-upload\">\r\n      <input type=\"file\" #file name=\"MenuUrl\" (value)=\"MenuUrl\" [(ngModel)]=\"MenuUrl\"  (ngModelChange)=\"SetUrl(MenuUrl )\"/>\r\n      Browes Url\r\n    </label>\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <mat-form-field class=\"example-full-width\">\r\n      <input matInput placeholder=\"Menu Url \" required [(ngModel)]=\"MenuUrlName\" name=\"MenuUrlName\">\r\n    </mat-form-field>\r\n  </div>-->\r\n        <!--<label id=\"lblMenuUrl\" class=\"custom-file-upload\">\r\n          <input type=\"file\" #file name=\"MenuUrl\" (value)=\"MenuUrl\" [(ngModel)]=\"MenuUrl\" (ngModelChange)=\"SetUrl(MenuUrl )\" />\r\n          Browes Url\r\n        </label>-->\r\n      </div>\r\n\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\">{{btnUpdatetext}}</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n\r\n<!--Acitve And  DeActive Menu-->\r\n\r\n<app-dialog [(visible)]=\"DeleteMenu\">\r\n\r\n\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">{{ActiveDactiveHeaderText}}</h3>\r\n    <!--<div class=\"form-group text-center col-md-12\">\r\n      <div class=\"col-md-4\">\r\n        <label class=\"control-label\" for=\"textMenu\">Menu ID</label>\r\n        &nbsp; &nbsp;\r\n        <label class=\"control-label\">{{MenuID}}</label>\r\n      </div>\r\n      <div class=\"col-md-6\">\r\n        <label class=\"control-label\">Menu Name</label>\r\n        &nbsp; &nbsp;\r\n        {{showMwnuName}}\r\n      </div>\r\n\r\n\r\n    </div>-->\r\n    <div class=\"form-group text-center col-md-12\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ActiveDeactiveMenu();\">Yes</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/role-managment/menu/menu.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/role-managment/menu/menu.component.ts ***!
  \*******************************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_UserManagement_menu_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/UserManagement/menu.service */ "./src/app/services/UserManagement/menu.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dashboard_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../dashboard/dashboard/dashboard.component */ "./src/app/dashboard/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MenuComponent = /** @class */ (function () {
    function MenuComponent(http, router, _MenuService, snackBar, curMenu) {
        this.http = http;
        this.router = router;
        this._MenuService = _MenuService;
        this.snackBar = snackBar;
        this.curMenu = curMenu;
        //Variabel
        //'msMenuID', 'parentMenu', 
        this.displayedColumns = ['mainMenuName', 'parentMenuName', 'menuURL', 'Active']; //, 'leaveTypeDesc', 'action',];
        this.filteredMenu = new rxjs__WEBPACK_IMPORTED_MODULE_6__["ReplaySubject"](1);
        this.MenuFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.MenuCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
        this.mstMenu = {};
        this.hdnMenuId = 0;
        this.btnUpdatetext = 'Save';
        this.MwnuHeaderText = 'Create New Menu';
        this.notFound = true;
        this.fileToUpload = null;
    }
    //@ViewChild(DashboardComponent) private curMenu: DashboardComponent
    //End Variabel
    MenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selrdType = 'Root';
        this.BindDropDownMenu();
        this.BindMenuInGride();
        this.GetAllRoles();
        this.MenuFilterCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["takeUntil"])(this._onDestroy)).subscribe(function () { _this.filterMenu(); });
    };
    MenuComponent.prototype.TextChange = function () {
        this.btnUpdatetext = 'Save';
        this.MwnuHeaderText = 'Create New Menu';
        this.selrdType = 'Root';
        this.BindDropDownMenu();
        this.Clear();
        //this.Cancel();
    };
    MenuComponent.prototype.SaveMenu = function () {
        var _this = this;
        debugger;
        this.username = sessionStorage.getItem('username');
        this.uRoleID = sessionStorage.getItem('userRoleID');
        //this.curMenu.getAllMenusByUser();
        this.isLoading = true;
        this._MenuService.SaveMenu(this.hdnMenuId, this.uRoleID, this.mstMenu).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.curMenu.getAllMenusByUser(_this.username, _this.uRoleID);
            _this.Message = result;
            if (_this.Message != undefined) {
                _this.Clear();
                jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this.Message);
                //this.snackBar.open(this.Message, '', {
                //  duration: 2000,
                //});
                _this.roleassign.resetForm();
            }
            _this.BindMenuInGride();
            _this.BindDropDownMenu();
            _this.Ishowhide = false;
        });
        this.TxtSearch = '';
    };
    MenuComponent.prototype.BindDropDownMenu = function () {
        var _this = this;
        this._MenuService.BindDropDownMenu().subscribe(function (result) {
            debugger;
            _this.ArrMenus = result;
            _this.mstMenu.parentMenu = 0;
            // this.MenuFilterCtrl.setValue(this.ArrMenus.parentMenu);
            _this.filteredMenu.next(_this.ArrMenus);
        });
    };
    MenuComponent.prototype.equalsRole = function (objOne, objTwo) {
        if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
            return objOne.roleID === objTwo.roleID;
        }
    };
    //ArrmstMenu: any ;
    MenuComponent.prototype.UpdateMenu = function (MsMenuID, menuURL) {
        var _this = this;
        debugger;
        this._MenuService.FillUpdateMenu(MsMenuID).subscribe(function (result) {
            _this.mstMenu = result[0][0];
            _this.mstMenu.RoleSIDS = result[1];
            if (_this.mstMenu.parentMenu === 0) {
                _this.selrdType = 'Root';
            }
            else {
                _this.selrdType = 'SubMenu';
            }
            // this.mstMenu = result[0].msMenuID;
            // console.log(this.ArrmstMenu);
        });
        //this.mstMenu.parentMenu = this.mstMenu[0].msMenuID;
        //this.mstMenu.parentMenu = this.mstMenu[0].parentMenu;   
        //this.mstMenu.mainMenuName = this.mstMenu[0].mainMenuName;
        //this.mstMenu.menuURL = this.mstMenu[0].menuURL;
        this.hdnMenuId = MsMenuID;
        this.btnUpdatetext = 'Update';
        this.MwnuHeaderText = 'Edit Menu';
        this.Ishowhide = true;
        this.IsShowHiddl = true;
        //this.mstMenu.parentMenu = ParentMenu;
    };
    MenuComponent.prototype.BindMenuInGride = function () {
        var _this = this;
        this._MenuService.BindMenuInGride().subscribe(function (result) {
            _this.ArrMenusInGride = result;
            _this.menudataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatTableDataSource"](result);
            _this.menudataSource.paginator = _this.paginator;
            _this.menudataSource.sort = _this.sort;
            //Filetr inData source
            _this.menudataSource.filterPredicate =
                function (data, filtersJson) {
                    var matchFilter = [];
                    var filters = JSON.parse(filtersJson);
                    filters.forEach(function (filter) {
                        var val = data[filter.id] === null ? '' : data[filter.id];
                        matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
                    });
                    return matchFilter.every(Boolean);
                };
            //END Of Filetr inData source
        });
    };
    MenuComponent.prototype.FillFormDeletePop = function (msMenuID, mainMenuName, isActive) {
        debugger;
        //alert(mainMenuName);
        this.MenuID = msMenuID;
        this.mainMenuName = mainMenuName;
        this.showMwnuName = mainMenuName;
        if (isActive == 'Active') {
            this.checkMenu = false; //true;
            this.ActiveDactiveHeaderText = 'Do you want to deactivate menu'; //'Are Sure DeActivate Menu ?'
        }
        else {
            this.checkMenu = true; //false;
            this.ActiveDactiveHeaderText = 'Do you want to activate menu'; //'Are Sure DeActivate Menu ?'
        }
    };
    MenuComponent.prototype.ActiveDeactiveMenu = function () {
        var _this = this;
        //alert(this.checkMenu)
        //return;
        this.isLoading = true;
        this.MstMenu = {
            IsActive: this.checkMenu,
            msMenuID: this.MenuID
        };
        JSON.stringify(this.MstMenu);
        this.username = sessionStorage.getItem('username');
        this.uRoleID = sessionStorage.getItem('userRoleID');
        this._MenuService.ActiveDeactiveMenu(this.MstMenu).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.curMenu.getAllMenusByUser(_this.username, _this.uRoleID);
            _this.Message = result;
            _this.BindMenuInGride();
            if (_this.Message != undefined) {
                jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
                //this.snackBar.open(this.Message, '', {
                //  duration: 2000,
                //});
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(_this.Message);
            }
        });
        this.TxtSearch = '';
    };
    MenuComponent.prototype.Cancel = function () {
        jquery__WEBPACK_IMPORTED_MODULE_4__(".dialog__close-btn").click();
        this.Clear();
        this.IsShowHiddl = false;
        this.roleassign.resetForm();
    };
    MenuComponent.prototype.Clear = function () {
        this.hdnMenuId = 0;
        this.mstMenu.mainMenuName = '';
        this.mstMenu.menuURL = '';
        this.mstMenu.parentMenu = 0;
        this.mstMenu.childMenu = 0;
        this.Ishowhide = false;
    };
    MenuComponent.prototype.applyFilter = function (filterValue) {
        var tableFilters = [];
        tableFilters.push({
            id: 'mainMenuName',
            value: filterValue
        });
        this.menudataSource.filter = JSON.stringify(tableFilters);
        //this.menudataSource.filter = filterValue.trim().toLowerCase();
        if (this.menudataSource.paginator) {
            this.menudataSource.paginator.firstPage();
        }
        if (this.menudataSource.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    MenuComponent.prototype.GetAllRoles = function () {
        var _this = this;
        this._MenuService.GetPredefineRole().subscribe(function (result) {
            _this.ArrRoles = result;
            _this.mstMenu.parentMenu = 0;
        });
    };
    MenuComponent.prototype.selectAll = function (checkAll, select, values) {
        //this.toCheck = !this.toCheck;
        // alert('call mrthod');
        //debugger;
        if (checkAll) {
            select.update.emit(values);
        }
        else {
            select.update.emit([]);
        }
    };
    MenuComponent.prototype.SetUrl = function (fileName) {
        // alert('change--' + fileName)
        this.MenuUrlName = fileName;
    };
    MenuComponent.prototype.HideandShowradio = function (Rdfleg) {
        if (Rdfleg == 'Root') {
            this.IsShowHiddl = false;
        }
        if (Rdfleg == 'SubMenu') {
            this.IsShowHiddl = true;
        }
    };
    MenuComponent.prototype.charaterOnlyNoSpace = function (event) {
        // debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    //not use for filter search
    MenuComponent.prototype.filterMenu = function () {
        debugger;
        if (!this.filteredMenu) {
            return;
        }
        // get the search keyword
        var search = this.MenuFilterCtrl.value;
        if (!search) {
            this.filteredMenu.next(this.ArrMenus.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        //this.filteredMenu.next(
        //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
        //);
        var a = this.ArrMenus;
        this.filteredMenu.next(this.ArrMenus.filter(function (ArrMenus) { return ArrMenus.mainMenuName.toLowerCase().indexOf(search) > -1; }));
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginator"])
    ], MenuComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSort"])
    ], MenuComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('roleassign'),
        __metadata("design:type", Object)
    ], MenuComponent.prototype, "roleassign", void 0);
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/role-managment/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/role-managment/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"], _services_UserManagement_menu_service__WEBPACK_IMPORTED_MODULE_8__["MenuService"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSnackBar"],
            _dashboard_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__["DashboardComponent"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/role-managment/other-role/other-role.component.css":
/*!********************************************************************!*\
  !*** ./src/app/role-managment/other-role/other-role.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm9sZS1tYW5hZ21lbnQvb3RoZXItcm9sZS9vdGhlci1yb2xlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBRUQ7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckIiLCJmaWxlIjoic3JjL2FwcC9yb2xlLW1hbmFnbWVudC9vdGhlci1yb2xlL290aGVyLXJvbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5DaGVja0JveExheW91dCB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ucmRCb3hMYXlvdXQge1xyXG4gIHdpZHRoOiAyNHB4O1xyXG4gIGhlaWdodDogMTdweDtcclxufVxyXG5cclxuLlBvaW50ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuLm1hdC10cmVlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogNjUwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBoZWlnaHQ6IDI3MHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbn1cclxuXHJcbnNwYW4ucmRvLWJ0biB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG4uYnRuLXNwYWNlIHtcclxuICBtYXJnaW46IDAgMzVweCAwIDRweDtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUuZXZlbiB7XHJcbn1cclxuXHJcbi5yb2xlLWRldGFpbCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuICAucm9sZS1kZXRhaWwgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbWFyZ2luOiA5cHggMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnJvbGUtZGV0YWlsIHVsIHtcclxuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG5cclxuICAgIC5yb2xlLWRldGFpbCB1bCBsaSB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG50aC50cmVlLXRpdGxlIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5zcGFuLm51bS10cmVlLXRpdGxlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDg4cHg7XHJcbn1cclxuXHJcbiAgc3Bhbi5udW0tdHJlZS10aXRsZSA+IC5idG4tc3BhY2Uge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0MnB4O1xyXG4gIH1cclxuXHJcbiAgICBzcGFuLm51bS10cmVlLXRpdGxlID4gLmJ0bi1zcGFjZTpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4udGFibGUtaGVhZCB7XHJcbiAgYmFja2dyb3VuZDogI2MxYzFjMTtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUge1xyXG4gIG1pbi1oZWlnaHQ6IDA7XHJcbn1cclxuXHJcbi5hY2Nlc3MtbGJsLXdpdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmJ0bi1tYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLyowNy8wMS8yMDE5Ki9cclxuc3Bhbi5idG4tbGFiZWwge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAtMTJweDsgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xyXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC4xNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4IDAgMCAzcHg7XHJcbn1cclxuXHJcbi5pbm5lci1ib3gge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ubWItMTAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5DaGVja0JveExheW91dCB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ucmRCb3hMYXlvdXQge1xyXG4gIHdpZHRoOiAyNHB4O1xyXG4gIGhlaWdodDogMTdweDtcclxufVxyXG5cclxuLlBvaW50ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuLm1hdC10cmVlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogNjUwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBoZWlnaHQ6IDI3MHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbn1cclxuXHJcbnNwYW4ucmRvLWJ0biB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG4uYnRuLXNwYWNlIHtcclxuICBtYXJnaW46IDAgMzVweCAwIDRweDtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUuZXZlbiB7XHJcbn1cclxuXHJcbi5yb2xlLWRldGFpbCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuICAucm9sZS1kZXRhaWwgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbWFyZ2luOiA5cHggMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnJvbGUtZGV0YWlsIHVsIHtcclxuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG5cclxuICAgIC5yb2xlLWRldGFpbCB1bCBsaSB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG50aC50cmVlLXRpdGxlIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5zcGFuLm51bS10cmVlLXRpdGxlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDg4cHg7XHJcbn1cclxuXHJcbiAgc3Bhbi5udW0tdHJlZS10aXRsZSA+IC5idG4tc3BhY2Uge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0MnB4O1xyXG4gIH1cclxuXHJcbiAgICBzcGFuLm51bS10cmVlLXRpdGxlID4gLmJ0bi1zcGFjZTpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4udGFibGUtaGVhZCB7XHJcbiAgYmFja2dyb3VuZDogI2MxYzFjMTtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUge1xyXG4gIG1pbi1oZWlnaHQ6IDA7XHJcbn1cclxuXHJcbi5hY2Nlc3MtbGJsLXdpdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmJ0bi1tYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLyowNy8wMS8yMDE5Ki9cclxuc3Bhbi5idG4tbGFiZWwge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAtMTJweDsgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xyXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC4xNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4IDAgMCAzcHg7XHJcbn1cclxuXHJcbi5pbm5lci1ib3gge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ubWItMTAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5DaGVja0JveExheW91dCB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ucmRCb3hMYXlvdXQge1xyXG4gIHdpZHRoOiAyNHB4O1xyXG4gIGhlaWdodDogMTdweDtcclxufVxyXG5cclxuLlBvaW50ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuLm1hdC10cmVlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogNjUwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBoZWlnaHQ6IDI3MHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbn1cclxuXHJcbnNwYW4ucmRvLWJ0biB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG4uYnRuLXNwYWNlIHtcclxuICBtYXJnaW46IDAgMzVweCAwIDRweDtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUuZXZlbiB7XHJcbn1cclxuXHJcbi5yb2xlLWRldGFpbCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuICAucm9sZS1kZXRhaWwgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbWFyZ2luOiA5cHggMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnJvbGUtZGV0YWlsIHVsIHtcclxuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG5cclxuICAgIC5yb2xlLWRldGFpbCB1bCBsaSB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG50aC50cmVlLXRpdGxlIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5zcGFuLm51bS10cmVlLXRpdGxlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDg4cHg7XHJcbn1cclxuXHJcbiAgc3Bhbi5udW0tdHJlZS10aXRsZSA+IC5idG4tc3BhY2Uge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0MnB4O1xyXG4gIH1cclxuXHJcbiAgICBzcGFuLm51bS10cmVlLXRpdGxlID4gLmJ0bi1zcGFjZTpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4udGFibGUtaGVhZCB7XHJcbiAgYmFja2dyb3VuZDogI2MxYzFjMTtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUge1xyXG4gIG1pbi1oZWlnaHQ6IDA7XHJcbn1cclxuXHJcbi5hY2Nlc3MtbGJsLXdpdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmJ0bi1tYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLyowNy8wMS8yMDE5Ki9cclxuc3Bhbi5idG4tbGFiZWwge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAtMTJweDsgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xyXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC4xNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4IDAgMCAzcHg7XHJcbn1cclxuXHJcbi5pbm5lci1ib3gge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ubWItMTAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/role-managment/other-role/other-role.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/role-managment/other-role/other-role.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!---Raju verma 28/may/19--->\r\n<div class=\"col-md-12 col-lg-12\">\r\n  <div class=\"example-card mat-card\">\r\n\r\n    <!--Dialog Box-->\r\n    <app-dialog [(visible)]=\"Newrole\">\r\n      <form name=\"form\" (ngSubmit)=\"f.valid && SaveNewRole();\" #f=\"ngForm\" novalidate>\r\n\r\n\r\n        <mat-card class=\"example-card dialog-wraper\">\r\n          <div class=\"fom-title\">\r\n            {{lbleditrole}}\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <!--<input matInput placeholder=\"Enter Role Name\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" required name=\"textNewrole\" [(ngModel)]=\"textNewrole\">-->\r\n              <input id=\"textNewrole1\" name=\"textNewrole1\" #textNewrole1=\"ngModel\" matInput pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" placeholder=\"Enter Role Name\" autocomplete=\"off\" [(ngModel)]=\"textNewrole\" required>\r\n              <input type=\"text\" name=\"someData\" [(ngModel)]=\"hdnRoleId\" style=\"display: none;\" />\r\n              <mat-error>\r\n                <span *ngIf=\"f.submitted && !textNewrole1.valid\">Special character are not allowed</span>\r\n              </mat-error>\r\n            </mat-form-field>\r\n\r\n            <!--<input id=\"email\" name=\"email\" #ref_email=\"ngModel\" matInput type=\"email\" email placeholder=\"Email\" [(ngModel)]=\"email\" required>\r\n\r\n          <mat-error *ngIf=\"!ref_email.valid && ref_email.errors.required\">\r\n            Email is required\r\n          </mat-error>-->\r\n\r\n          </div>\r\n          <div class=\"col-sm-12 col-md-6 col-lg-6\">\r\n            <mat-form-field class=\"wid-100\">\r\n              <input matInput placeholder=\"Enter Role Description\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" autocomplete=\"off\" required name=\"textDescription\" [(ngModel)]=\"textDescription\">\r\n\r\n              <mat-error>\r\n                <span *ngIf=\"f.submitted && !textDescription.valid\">Special character are not allowed</span>\r\n            </mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n\r\n          <div class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n            <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"savebuttonstatus\">{{btnUpdatetext}}</button>\r\n            <!--<button type=\"button\" class=\"btn btn-warning\" (click)=\"ClearInput();\">Cancel</button>-->\r\n            <button type=\"button\" class=\"btn btn-warning\" (click)=\"Close();Newrole = !Newrole\">Cancel</button>\r\n          </div>\r\n        </mat-card>\r\n\r\n      </form>\r\n    </app-dialog>\r\n\r\n\r\n\r\n\r\n    <div class=\"fom-title\">Role List</div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\" [(ngModel)]=\"TxtSexrch\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n    <button type=\"button\" class=\"btn btn-labeled btn-success float-r margin-0\" (click)=\"TextChange();Newrole = !Newrole\">\r\n      <!--TextChange();-->\r\n      <span class=\"btn-label\"><i class=\"glyphicon glyphicon-plus\"></i></span>New Role\r\n    </button>\r\n\r\n    <table mat-table [dataSource]=\"roledataSource\" matSort class=\"mat-elevation-z8 even-odd-color mat-table tabel-wraper\">\r\n      <!-- Column -->\r\n      <ng-container matColumnDef=\"roleName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Role Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.roleName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"roleDescription\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Role Description </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.roleDescription}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"CreatedDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Created Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.createdDate}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"roleDeactivateDescription\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Deactivation Reason</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.deActDesc}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"DeactivationDate\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Deactivated On</th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.deactivationDate}} </td>\r\n      </ng-container>\r\n      <!-- Action Column -->\r\n      <!--<ng-container matColumnDef=\"Edit\">\r\n      <th mat-header-cell *matHeaderCellDef> Edit </th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <div *ngIf=\"element.roleStatus =='c'\">\r\n          <a class=\"material-icons i-edit text-center\" (click)=\"UpdateRole(element.roleName,element.roleID,element.roleDescription);Newrole = !Newrole\" matTooltip=\"Edit\">edit</a>\r\n        </div>\r\n      </td>\r\n    </ng-container>-->\r\n      <ng-container matColumnDef=\"Active\">\r\n        <th mat-header-cell *matHeaderCellDef> Actions </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <div class=\"float-left\" *ngIf=\"element.roleStatus =='c'\">\r\n            <a class=\"material-icons i-edit text-center\" (click)=\"UpdateRole(element.roleName,element.roleID,element.roleDescription);Newrole = !Newrole\" matTooltip=\"Edit\">edit</a>\r\n          </div>\r\n          <div *ngIf=\"element.roleStatus =='p'\">\r\n            <h5><b><A>Predefined</A></b></h5>\r\n          </div>\r\n          <div *ngIf=\"element.roleStatus =='c'\">\r\n            <div *ngIf=\"element.isActive =='Not Active'\">\r\n              <a class=\"material-icons i-dactivate\" matTooltip=\"Activate\" (click)=\"FillFormrevokerolePop(element.roleID,element.roleName,element.isActive,false);Activaterole = !Activaterole\">cancel</a>\r\n            </div>\r\n            <div *ngIf=\"element.isActive =='Active'\">\r\n              <a class=\"material-icons i-activate\" matTooltip=\"Dactivate\" (click)=\"FillFormrevokerolePop(element.roleID,element.roleName,element.isActive,true);Activaterole = !Activaterole\">check_circle</a>\r\n            </div>\r\n          </div>\r\n\r\n        </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\">\r\n      <font color=\"red\">Data Not Found...</font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\r\n\r\n\r\n\r\n  </div>\r\n  <!--End Of Table User Details-->\r\n</div>\r\n\r\n\r\n\r\n<app-dialog [(visible)]=\"Activaterole\">\r\n  <form name=\"form\" (ngSubmit)=\"frm.valid && Revokerole();\" #frm=\"ngForm\" novalidate>\r\n    <div class=\"card-content panel panel-body panel-default\">\r\n      <h3 class=\"card-title text-center\">{{ActiveDactiveHeaderText}}</h3>\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <!--<div class=\"col-md-4\">\r\n         <label class=\"control-label\">Role ID: {{roleID}}</label>\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n          <label class=\"control-label\">Role Name: &nbsp; {{showroleName}}</label>\r\n\r\n\r\n        </div>-->\r\n        <div class=\"col-md-3\"></div>\r\n        <div *ngIf=\"IsDeactivatetextDesc==true\">\r\n          <mat-form-field class=\"col-md-6\">\r\n            <input matInput placeholder=\"Enter reason for deactivate role\" required name=\"textDeactivateDescription\" pattern=\"^[a-zA-Z][a-zA-Z0-9 ]+$\" autocomplete=\"off\" [(ngModel)]=\"textDeactivateDescription\">\r\n            <mat-error>\r\n              <span *ngIf=\"frm.submitted &&!textDeactivateDescription.valid\" >Special charector are not allowed</span>\r\n            </mat-error>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group text-center col-md-12\">\r\n        <button type=\"submit\" class=\"btn btn-info\" >Yes</button>\r\n        &nbsp;\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"Cancel();\">Cancel</button>\r\n      </div>\r\n    </div>\r\n    </form>\r\n</app-dialog>\r\n\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/role-managment/other-role/other-role.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/role-managment/other-role/other-role.component.ts ***!
  \*******************************************************************/
/*! exports provided: OtherRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherRoleComponent", function() { return OtherRoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/UserManagement/role.service */ "./src/app/services/UserManagement/role.service.ts");
/* harmony import */ var rxjs_add_operator_finally__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/finally */ "./node_modules/rxjs-compat/_esm5/add/operator/finally.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OtherRoleComponent = /** @class */ (function () {
    function OtherRoleComponent(_RoleService, snackBar) {
        this._RoleService = _RoleService;
        this.snackBar = snackBar;
        this.displayedColumns = ['roleName', 'roleDescription', 'CreatedDate', 'roleDeactivateDescription', 'DeactivationDate', 'Active']; /*'CreatedDate', 'roleDescription', */
        this.checkIsActiveRole = true;
        this.ArrRoles = [];
        this.hdnRoleId = 0;
        this.notFound = true;
    }
    OtherRoleComponent.prototype.ngOnInit = function () {
        this.textNewrole = '';
        this.savebuttonstatus = true;
        this.btnUpdatetext = 'Save';
        this.lbleditrole = 'Create Role';
        this.GetAllDataRollList();
    };
    OtherRoleComponent.prototype.TextChange = function () {
        //this.lblPopupText = 'Create New Role';
        //this.btnPopupText = 'Save';
        this.textNewrole = "";
        this.textDescription = "";
        this.checkIsActiveRole = true;
        this.hdnRoleId = 0;
    };
    OtherRoleComponent.prototype.GetAllDataRollList = function () {
        var _this = this;
        this.isLoading = true;
        this._RoleService.GetAllDataRollList(sessionStorage.getItem('username')).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrRoles = result;
            _this.roledataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](result);
            _this.roledataSource.paginator = _this.paginator;
            _this.roledataSource.sort = _this.sort;
            //Filetr inData source
            _this.roledataSource.filterPredicate =
                function (data, filtersJson) {
                    var matchFilter = [];
                    var filters = JSON.parse(filtersJson);
                    filters.forEach(function (filter) {
                        var val = data[filter.id] === null ? '' : data[filter.id];
                        matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
                    });
                    return matchFilter.every(Boolean);
                };
            //END Of Filetr inData source
        });
    };
    OtherRoleComponent.prototype.SaveNewRole = function () {
        var _this = this;
        this.isLoading = true;
        this.MstRole = {
            RoleName: this.textNewrole,
            IsActive: this.checkIsActiveRole,
            RoleID: this.hdnRoleId,
            RoleDescription: this.textDescription,
            LogInUserName: sessionStorage.getItem('username'),
            RoleParentID: sessionStorage.getItem('userRoleID')
        };
        JSON.stringify(this.MstRole);
        this._RoleService.SaveNewRole(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            console.log(result);
            _this.Message = result;
            _this.GetAllDataRollList();
            if (_this.Message != undefined) {
                jquery__WEBPACK_IMPORTED_MODULE_2__(".dialog__close-btn").click();
                //this.snackBar.open(this.Message, '', {
                //  duration: 2000,
                //  verticalPosition: 'top',
                //  horizontalPosition: 'center',
                //});
                _this.btnUpdatetext = 'Save';
                _this.lbleditrole = 'Create Role';
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.Message);
            }
            _this.ClearInputRole();
        });
        this.TxtSexrch = '';
    };
    OtherRoleComponent.prototype.ClearInputRole = function () {
        this.textNewrole = '';
        this.checkIsActiveRole = true;
        this.textDescription = "";
    };
    OtherRoleComponent.prototype.InsertandUpdate = function () {
        alert(this.textNewrole);
        debugger;
    };
    OtherRoleComponent.prototype.UpdateRole = function (roleName, msRoleID, roleDescription) {
        this.textNewrole = roleName;
        this.textDescription = roleDescription;
        this.hdnRoleId = msRoleID;
        this.btnUpdatetext = 'Update';
        this.lbleditrole = 'Edit Role';
    };
    OtherRoleComponent.prototype.applyFilter = function (filterValue) {
        //alert(filterValue)
        debugger;
        var tableFilters = [];
        tableFilters.push({
            id: 'roleName',
            value: filterValue
        });
        this.roledataSource.filter = JSON.stringify(tableFilters);
        if (this.roledataSource.paginator) {
            this.roledataSource.paginator.firstPage();
        }
        //this.roledataSource.filteredData.roleName == filterValue.trim().toLowerCase();
        //if (this.roledataSource.paginator) {
        //  this.roledataSource.paginator.firstPage();
        //}
        if (this.roledataSource.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    OtherRoleComponent.prototype.FillFormrevokerolePop = function (roleID, roleName, isActive, Fleg) {
        if (Fleg == true) {
            //alert(Fleg)
            this.IsDeactivatetextDesc = true;
        }
        if (Fleg == false) {
            //alert(Fleg)
            this.IsDeactivatetextDesc = false;
        }
        this.roleID = roleID;
        this.showroleName = roleName;
        if (isActive == 'Active') {
            this.checkrole = false;
            this.ActiveDactiveHeaderText = 'Do you want to deactivate role?';
        }
        else {
            this.checkrole = true;
            this.ActiveDactiveHeaderText = 'Do you want to  activate role?';
        }
    };
    //Revoke for User
    OtherRoleComponent.prototype.Revokerole = function () {
        var _this = this;
        this.isLoading = true;
        debugger;
        this.MstRole = {
            IsActive: this.checkrole,
            roleID: this.roleID,
            RoleDescription: this.textDeactivateDescription
        };
        JSON.stringify(this.MstRole);
        this._RoleService.RevokeUser(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            if (_this.Message != undefined) {
                jquery__WEBPACK_IMPORTED_MODULE_2__(".dialog__close-btn").click();
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()(_this.Message);
                //this.snackBar.open(this.Message, '', {
                //  duration: 2000,
                //  verticalPosition: 'top',
                //  horizontalPosition: 'center',
                //});
                _this.textDeactivateDescription = '';
                _this.GetAllDataRollList();
            }
        });
    };
    OtherRoleComponent.prototype.Close = function () {
        this.btnUpdatetext = 'Save';
        this.IsDeactivatetextDesc = false;
        this.lbleditrole = 'Create Role';
    };
    OtherRoleComponent.prototype.charaterOnlyNoSpace = function (event) {
        // debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    OtherRoleComponent.prototype.Cancel = function () {
        this.textDeactivateDescription = '';
        jquery__WEBPACK_IMPORTED_MODULE_2__(".dialog__close-btn").click();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], OtherRoleComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], OtherRoleComponent.prototype, "sort", void 0);
    OtherRoleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-other-role',
            template: __webpack_require__(/*! ./other-role.component.html */ "./src/app/role-managment/other-role/other-role.component.html"),
            styles: [__webpack_require__(/*! ./other-role.component.css */ "./src/app/role-managment/other-role/other-role.component.css")]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_4__["RoleService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], OtherRoleComponent);
    return OtherRoleComponent;
}());



/***/ }),

/***/ "./src/app/role-managment/role-managment-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/role-managment/role-managment-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: RoleManagmentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleManagmentRoutingModule", function() { return RoleManagmentRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./role/role.component */ "./src/app/role-managment/role/role.component.ts");
/* harmony import */ var _role_managment_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./role-managment.module */ "./src/app/role-managment/role-managment.module.ts");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/role-managment/menu/menu.component.ts");
/* harmony import */ var _roleusermanagment_roleusermanagment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./roleusermanagment/roleusermanagment.component */ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.ts");
/* harmony import */ var _other_role_other_role_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./other-role/other-role.component */ "./src/app/role-managment/other-role/other-role.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '', component: _role_managment_module__WEBPACK_IMPORTED_MODULE_3__["RoleManagmentModule"], data: {
            breadcrumb: 'User Management'
        }, children: [
            {
                path: 'role', component: _role_role_component__WEBPACK_IMPORTED_MODULE_2__["RoleComponent"], data: {
                    breadcrumb: 'Role-Menu Management'
                }
            },
            {
                path: 'menu', component: _menu_menu_component__WEBPACK_IMPORTED_MODULE_4__["MenuComponent"], data: {
                    breadcrumb: 'New Menu Creation'
                }
            },
            {
                path: 'Roleusermanagment', component: _roleusermanagment_roleusermanagment_component__WEBPACK_IMPORTED_MODULE_5__["RoleusermanagmentComponent"], data: {
                    breadcrumb: 'Custom Role'
                }
            },
            {
                path: 'otherrole', component: _other_role_other_role_component__WEBPACK_IMPORTED_MODULE_6__["OtherRoleComponent"], data: {
                    breadcrumb: 'New Role Creation'
                }
            },
        ]
    }
];
var RoleManagmentRoutingModule = /** @class */ (function () {
    function RoleManagmentRoutingModule() {
    }
    RoleManagmentRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RoleManagmentRoutingModule);
    return RoleManagmentRoutingModule;
}());



/***/ }),

/***/ "./src/app/role-managment/role-managment.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/role-managment/role-managment.module.ts ***!
  \*********************************************************/
/*! exports provided: RoleManagmentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleManagmentModule", function() { return RoleManagmentModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _role_managment_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./role-managment-routing.module */ "./src/app/role-managment/role-managment-routing.module.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./role/role.component */ "./src/app/role-managment/role/role.component.ts");
/* harmony import */ var _roleusermanagment_roleusermanagment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./roleusermanagment/roleusermanagment.component */ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.ts");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/role-managment/menu/menu.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
/* harmony import */ var _other_role_other_role_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./other-role/other-role.component */ "./src/app/role-managment/other-role/other-role.component.ts");
/* harmony import */ var ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-mat-select-search */ "./node_modules/ngx-mat-select-search/fesm5/ngx-mat-select-search.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//import {DialogComponent } from '../dialog/dialog.component';








//import { UiSwitchModule } from 'ngx-toggle-switch';
var RoleManagmentModule = /** @class */ (function () {
    function RoleManagmentModule() {
    }
    RoleManagmentModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_role_role_component__WEBPACK_IMPORTED_MODULE_3__["RoleComponent"], _menu_menu_component__WEBPACK_IMPORTED_MODULE_5__["MenuComponent"], _roleusermanagment_roleusermanagment_component__WEBPACK_IMPORTED_MODULE_4__["RoleusermanagmentComponent"], _other_role_other_role_component__WEBPACK_IMPORTED_MODULE_10__["OtherRoleComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__["MatTooltipModule"],
                _role_managment_routing_module__WEBPACK_IMPORTED_MODULE_2__["RoleManagmentRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                ngx_mat_select_search__WEBPACK_IMPORTED_MODULE_11__["NgxMatSelectSearchModule"]
                //UiSwitchModule ,
            ]
        })
    ], RoleManagmentModule);
    return RoleManagmentModule;
}());



/***/ }),

/***/ "./src/app/role-managment/role/role.component.css":
/*!********************************************************!*\
  !*** ./src/app/role-managment/role/role.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.CheckBoxLayout {\r\n  width: 24px;\r\n  height: 25px;\r\n}\r\n\r\n.rdBoxLayout {\r\n  width: 24px;\r\n  height: 17px;\r\n}\r\n\r\n.Pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.mat-tree {\r\n  display: block;\r\n  width: 650px;\r\n  position: relative;\r\n  overflow-y: scroll;\r\n  height: 270px;\r\n  border: 1px solid #d4d4d4;\r\n}\r\n\r\nspan.rdo-btn {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n}\r\n\r\n.btn-space {\r\n  margin: 0 35px 0 4px;\r\n}\r\n\r\n.mat-tree-node.even {\r\n}\r\n\r\n.role-detail {\r\n  margin-top: 10px;\r\n}\r\n\r\n.role-detail span {\r\n    font-size: 15px;\r\n    font-weight: 600;\r\n    margin: 9px 0;\r\n    display: block;\r\n  }\r\n\r\n.role-detail ul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n  }\r\n\r\n.role-detail ul li {\r\n      padding-bottom: 5px;\r\n    }\r\n\r\nth.tree-title {\r\n  width: 50%;\r\n  float: left;\r\n}\r\n\r\nspan.num-tree-title {\r\n  position: absolute;\r\n  right: 88px;\r\n}\r\n\r\nspan.num-tree-title > .btn-space {\r\n    margin-right: 42px;\r\n  }\r\n\r\nspan.num-tree-title > .btn-space:last-child {\r\n      margin: 0 !important;\r\n    }\r\n\r\n.table-head {\r\n  background: #c1c1c1;\r\n  padding: 5px 10px;\r\n  float: left;\r\n  width: 100%;\r\n}\r\n\r\n.mat-tree-node {\r\n  min-height: 0;\r\n}\r\n\r\n.access-lbl-with {\r\n  width: 100%;\r\n}\r\n\r\n.btn-margin {\r\n  margin-top: 20px;\r\n}\r\n\r\n/*07/01/2019*/\r\n\r\nspan.btn-label {\r\n  position: relative;\r\n  left: -12px; /* display: inline-block; */\r\n  padding: 8px 12px;\r\n  background: rgba(0,0,0,0.15);\r\n  border-radius: 3px 0 0 3px;\r\n}\r\n\r\n.inner-box {\r\n  border: 1px solid #ddd;\r\n  padding: 15px;\r\n  margin-bottom: 20px;\r\n  background-color: #fff;\r\n  border-radius: 4px;\r\n}\r\n\r\n.mb-10 {\r\n  margin-bottom: 10px;\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm9sZS1tYW5hZ21lbnQvcm9sZS9yb2xlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBQ0Q7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7O0FBQ0Q7RUFDRSxZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUVEO0VBQ0UsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDtFQUNFLGdCQUFnQjtDQUNqQjs7QUFRRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixTQUFTO0NBQ1Y7O0FBRUQ7RUFDRSxxQkFBcUI7Q0FDdEI7O0FBRUQ7Q0FDQzs7QUFFRDtFQUNFLGlCQUFpQjtDQUNsQjs7QUFFQztJQUNFLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSxzQkFBc0I7SUFDdEIsVUFBVTtJQUNWLFdBQVc7R0FDWjs7QUFFQztNQUNFLG9CQUFvQjtLQUNyQjs7QUFFTDtFQUNFLFdBQVc7RUFDWCxZQUFZO0NBQ2I7O0FBRUQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiOztBQUVDO0lBQ0UsbUJBQW1CO0dBQ3BCOztBQUVDO01BQ0UscUJBQXFCO0tBQ3RCOztBQUVMO0VBQ0Usb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtDQUNiOztBQUVEO0VBQ0UsY0FBYztDQUNmOztBQUVEO0VBQ0UsWUFBWTtDQUNiOztBQUdEO0VBQ0UsaUJBQWlCO0NBQ2xCOztBQUNELGNBQWM7O0FBQ2Q7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWSxDQUFDLDRCQUE0QjtFQUN6QyxrQkFBa0I7RUFDbEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtDQUM1Qjs7QUFFRDtFQUNFLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7Q0FDcEI7O0FBRUQ7RUFDRSxvQkFBb0I7Q0FDckIiLCJmaWxlIjoic3JjL2FwcC9yb2xlLW1hbmFnbWVudC9yb2xlL3JvbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5DaGVja0JveExheW91dCB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ucmRCb3hMYXlvdXQge1xyXG4gIHdpZHRoOiAyNHB4O1xyXG4gIGhlaWdodDogMTdweDtcclxufVxyXG5cclxuLlBvaW50ZXIge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuLm1hdC10cmVlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogNjUwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBoZWlnaHQ6IDI3MHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbn1cclxuXHJcbnNwYW4ucmRvLWJ0biB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG4uYnRuLXNwYWNlIHtcclxuICBtYXJnaW46IDAgMzVweCAwIDRweDtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUuZXZlbiB7XHJcbn1cclxuXHJcbi5yb2xlLWRldGFpbCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuICAucm9sZS1kZXRhaWwgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbWFyZ2luOiA5cHggMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLnJvbGUtZGV0YWlsIHVsIHtcclxuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG5cclxuICAgIC5yb2xlLWRldGFpbCB1bCBsaSB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICB9XHJcblxyXG50aC50cmVlLXRpdGxlIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG5zcGFuLm51bS10cmVlLXRpdGxlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDg4cHg7XHJcbn1cclxuXHJcbiAgc3Bhbi5udW0tdHJlZS10aXRsZSA+IC5idG4tc3BhY2Uge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0MnB4O1xyXG4gIH1cclxuXHJcbiAgICBzcGFuLm51bS10cmVlLXRpdGxlID4gLmJ0bi1zcGFjZTpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4udGFibGUtaGVhZCB7XHJcbiAgYmFja2dyb3VuZDogI2MxYzFjMTtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC10cmVlLW5vZGUge1xyXG4gIG1pbi1oZWlnaHQ6IDA7XHJcbn1cclxuXHJcbi5hY2Nlc3MtbGJsLXdpdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLmJ0bi1tYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLyowNy8wMS8yMDE5Ki9cclxuc3Bhbi5idG4tbGFiZWwge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAtMTJweDsgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xyXG4gIHBhZGRpbmc6IDhweCAxMnB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC4xNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4IDAgMCAzcHg7XHJcbn1cclxuXHJcbi5pbm5lci1ib3gge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZGQ7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ubWItMTAge1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuLkNoZWNrQm94TGF5b3V0IHtcclxuICB3aWR0aDogMjRweDtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbn1cclxuXHJcbi5yZEJveExheW91dCB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgaGVpZ2h0OiAxN3B4O1xyXG59XHJcblxyXG4uUG9pbnRlciB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG4ubWF0LXRyZWUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiA2NTBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gIGhlaWdodDogMjcwcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcclxufVxyXG5cclxuc3Bhbi5yZG8tYnRuIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuXHJcbi5idG4tc3BhY2Uge1xyXG4gIG1hcmdpbjogMCAzNXB4IDAgNHB4O1xyXG59XHJcblxyXG4ubWF0LXRyZWUtbm9kZS5ldmVuIHtcclxufVxyXG5cclxuLnJvbGUtZGV0YWlsIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG4gIC5yb2xlLWRldGFpbCBzcGFuIHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBtYXJnaW46IDlweCAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG5cclxuICAucm9sZS1kZXRhaWwgdWwge1xyXG4gICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcblxyXG4gICAgLnJvbGUtZGV0YWlsIHVsIGxpIHtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuXHJcbnRoLnRyZWUtdGl0bGUge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbnNwYW4ubnVtLXRyZWUtdGl0bGUge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogODhweDtcclxufVxyXG5cclxuICBzcGFuLm51bS10cmVlLXRpdGxlID4gLmJ0bi1zcGFjZSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDQycHg7XHJcbiAgfVxyXG5cclxuICAgIHNwYW4ubnVtLXRyZWUtdGl0bGUgPiAuYnRuLXNwYWNlOmxhc3QtY2hpbGQge1xyXG4gICAgICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbi50YWJsZS1oZWFkIHtcclxuICBiYWNrZ3JvdW5kOiAjYzFjMWMxO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWF0LXRyZWUtbm9kZSB7XHJcbiAgbWluLWhlaWdodDogMDtcclxufVxyXG5cclxuLmFjY2Vzcy1sYmwtd2l0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcblxyXG4uYnRuLW1hcmdpbiB7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4vKjA3LzAxLzIwMTkqL1xyXG5zcGFuLmJ0bi1sYWJlbCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGxlZnQ6IC0xMnB4OyAvKiBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7ICovXHJcbiAgcGFkZGluZzogOHB4IDEycHg7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjE1KTtcclxuICBib3JkZXItcmFkaXVzOiAzcHggMCAwIDNweDtcclxufVxyXG5cclxuLmlubmVyLWJveCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcclxuICBwYWRkaW5nOiAxNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuXHJcbi5tYi0xMCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG4uQ2hlY2tCb3hMYXlvdXQge1xyXG4gIHdpZHRoOiAyNHB4O1xyXG4gIGhlaWdodDogMjVweDtcclxufVxyXG5cclxuLnJkQm94TGF5b3V0IHtcclxuICB3aWR0aDogMjRweDtcclxuICBoZWlnaHQ6IDE3cHg7XHJcbn1cclxuXHJcbi5Qb2ludGVyIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbi5tYXQtdHJlZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDY1MHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgaGVpZ2h0OiAyNzBweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xyXG59XHJcblxyXG5zcGFuLnJkby1idG4ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogMDtcclxufVxyXG5cclxuLmJ0bi1zcGFjZSB7XHJcbiAgbWFyZ2luOiAwIDM1cHggMCA0cHg7XHJcbn1cclxuXHJcbi5tYXQtdHJlZS1ub2RlLmV2ZW4ge1xyXG59XHJcblxyXG4ucm9sZS1kZXRhaWwge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbiAgLnJvbGUtZGV0YWlsIHNwYW4ge1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIG1hcmdpbjogOXB4IDA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcblxyXG4gIC5yb2xlLWRldGFpbCB1bCB7XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gIH1cclxuXHJcbiAgICAucm9sZS1kZXRhaWwgdWwgbGkge1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG5cclxudGgudHJlZS10aXRsZSB7XHJcbiAgd2lkdGg6IDUwJTtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuc3Bhbi5udW0tdHJlZS10aXRsZSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiA4OHB4O1xyXG59XHJcblxyXG4gIHNwYW4ubnVtLXRyZWUtdGl0bGUgPiAuYnRuLXNwYWNlIHtcclxuICAgIG1hcmdpbi1yaWdodDogNDJweDtcclxuICB9XHJcblxyXG4gICAgc3Bhbi5udW0tdHJlZS10aXRsZSA+IC5idG4tc3BhY2U6bGFzdC1jaGlsZCB7XHJcbiAgICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuLnRhYmxlLWhlYWQge1xyXG4gIGJhY2tncm91bmQ6ICNjMWMxYzE7XHJcbiAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtdHJlZS1ub2RlIHtcclxuICBtaW4taGVpZ2h0OiAwO1xyXG59XHJcblxyXG4uYWNjZXNzLWxibC13aXRoIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbi5idG4tbWFyZ2luIHtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbi8qMDcvMDEvMjAxOSovXHJcbnNwYW4uYnRuLWxhYmVsIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogLTEycHg7IC8qIGRpc3BsYXk6IGlubGluZS1ibG9jazsgKi9cclxuICBwYWRkaW5nOiA4cHggMTJweDtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuMTUpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweCAwIDAgM3B4O1xyXG59XHJcblxyXG4uaW5uZXItYm94IHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xyXG4gIHBhZGRpbmc6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG5cclxuLm1iLTEwIHtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/role-managment/role/role.component.html":
/*!*********************************************************!*\
  !*** ./src/app/role-managment/role/role.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"panel panel-body panel-default col-md-12\">\r\n  <div class=\"col-lg-1\"></div>\r\n  <div class=\"col-lg-10\">\r\n\r\n\r\n\r\n    <div class=\"form-group col-md-6 panel panel-body panel-default\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"TextChange();showDialog = !showDialog\">New Role</button>\r\n    </div>-->\r\n<!--Div Of Assign Role-->\r\n<!--<div class=\"form-group col-md-6 panel panel-body panel-default\">\r\n  <button type=\"button\" class=\"btn btn-info\" (click)=\"TextChange();showDialog = !showDialog\">Assign Role</button>\r\n</div>-->\r\n<!--End Of Div Of Assign Role-->\r\n<!--</div>\r\n\r\n  <div class=\"col-lg-1\"></div>\r\n</div>-->\r\n<!--Table Show Role And Revoke User Details-->\r\n<!--hide new role showAssignRole start   -->\r\n<!--<div class=\"col-md-6 mb-10\">\r\n  <button type=\"button\" class=\"btn btn-labeled btn-success\" (click)=\"TextChange();showDialog = !showDialog\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-plus\"></i></span>New Role\r\n  </button>\r\n</div>\r\n<div class=\"col-md-6 mb-10\">\r\n  <button type=\"button\" class=\"btn btn-labeled btn-success\" (click)=\"ClearInputRole();showAssignRole = !showAssignRole\">\r\n    <span class=\"btn-label\"><i class=\"glyphicon glyphicon-ok\"></i></span>Assign Role\r\n  </button>\r\n</div>-->\r\n<!--hide new role showAssignRole end   -->\r\n<!--Table Role Details-->\r\n<div class=\"col-md-12 col-lg-6\">\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Role List</div>\r\n    <!--<div class=\"table-responsive DivScrollbar\">\r\n    <table class=\"table table-striped\">\r\n      <thead>\r\n        <tr>\r\n          <th>Role</th>\r\n          <th>User</th>\r\n          <th>Assign Menu</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody id=\"myTableRoles\">\r\n        <tr *ngFor=\"let role of ArrRoles; let i = index\">\r\n          <td>{{role.roleName}}</td>\r\n          <td class=\"td-actions\">\r\n            <button type=\"button\" class=\"btn btn-info btn-xs\" (click)=\"GetUserDetails(i)\">\r\n              <i class=\"material-icons\">person</i>\r\n            </button>\r\n          </td>\r\n          <td class=\"td-actions\">\r\n            <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"TreestructureList(role.roleID,role.roleName);SaveAssignMenuPermission(role.roleID);showAssignMenu = !showAssignMenu\">\r\n              <i class=\"material-icons\">edit</i>\r\n            </button>\r\n          </td>\r\n        </tr>\r\n      </tbody>\r\n      <tfoot>\r\n        <tr *ngIf=\"ArrRoles?.length == 0\"><td><font color=\"red\">Records Not Found...</font></td></tr>\r\n      </tfoot>\r\n    </table>\r\n  </div>-->\r\n    <!--Material Table USe For Role-->\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\" [(ngModel)]=\"textSearch\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"roledataSource\" matSort class=\"mat-elevation-z8 even-odd-color mat-table\">\r\n      <!-- Column -->\r\n      <ng-container matColumnDef=\"roleName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Role Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.roleName}} </td>\r\n      </ng-container>\r\n      <!-- Action Column -->\r\n      <ng-container matColumnDef=\"User\">\r\n        <th mat-header-cell *matHeaderCellDef> Existing User </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <!--<a class=\"material-icons i-info\" matTooltip=\"info\" (click)=\"GetUserDetails(element.roleID)\">info</a>-->\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Details\" (click)=\"GetUserDetails(element.roleID,element.roleName)\">person</a>\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"AssignMenu\">\r\n        <th mat-header-cell *matHeaderCellDef> Assign/Un-assign Menu </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n\r\n          <i class=\"material-icons i-edit\" disabled matTooltip=\"Assign/Un-assign Menu\" (click)=\"TreestructureList(element.roleID,element.roleName,element.roleStatus);SaveAssignMenuPermission(element.roleID);showAssignMenu = !showAssignMenu\">edit</i>\r\n\r\n          <!--<a class=\"material-icons i-edit\" matTooltip=\"Edit\" > edit </a>-->\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"notFound\">\r\n      <font color=\"red\">Data Not Found...</font>\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[10, 15, 20]\" showFirstLastButtons></mat-paginator>\r\n    <!--End Of Material Table USe For Role-->\r\n  </div>\r\n</div>\r\n\r\n\r\n<!--End Of Table Role Details-->\r\n<!--Table User Details-->\r\n<div class=\"col-md-12 col-lg-6\">\r\n\r\n  <div class=\"example-card mat-card\">\r\n    <div class=\"fom-title\">Existing User List</div>\r\n    <mat-form-field>\r\n      <input matInput (keyup)=\"UserapplyFilter($event.target.value)\" (keypress)=\"charaterOnlyNoSpace($event)\" placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n    <table mat-table [dataSource]=\"ArrUserListdataSource\" matSort class=\"mat-elevation-z8 even-odd-color mat-table\">\r\n      <!-- Column -->\r\n      <ng-container matColumnDef=\"FirstName\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>User Name </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.firstName}} </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"Designation\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Role </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.designation}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"PAN\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>PAN </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.pan}} </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"EmpCd\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>\tEmp Code </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n      </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedUserColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedUserColumns;\"></tr>\r\n    </table>\r\n    <div [hidden]=\"isTableHasData\">\r\n      <font color=\"red\">Record not found</font>\r\n    </div>\r\n       <mat-paginator [pageSizeOptions]=\"[10, 15, 20]\" showFirstLastButtons></mat-paginator>\r\n    <!--End Of Table User Details-->\r\n\r\n\r\n\r\n  </div>\r\n</div>\r\n<!--End Of Table Show Role And Revoke User Details-->\r\n<!--Create New Role and Update On Popup-->\r\n<app-dialog [(visible)]=\"showDialog\">\r\n  <form ngNativeValidate (ngSubmit)=\"SaveNewRole()\" #f=\"ngForm\" class=\"panel panel-body panel-default\">\r\n    <div class=\"card-content\">\r\n      <h4 class=\"card-title text-center\"> {{lblPopupText}}</h4>\r\n      <div class=\"form-group label-floating\">\r\n        <label class=\"control-label\" for=\"textNewrole\">Enter Role Name</label>\r\n        <input id=\"textNewrole\" type=\"text\" [(ngModel)]=\"textNewrole\" name=\"textNewrole\" class=\"form-control\" required>\r\n        <input type=\"text\" name=\"someData\" [(ngModel)]=\"hdnRoleId\" style=\"display: none;\" />\r\n      </div>\r\n      <div class=\"checkbox\">\r\n        <label> <input type=\"checkbox\" [(ngModel)]=\"checkIsActiveRole\" name=\"optionsCheckboxes\" class=\"CheckBoxLayout\"> </label>\r\n        <label> Is Active     </label>\r\n      </div>\r\n      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"f.invalid\">{{btnPopupText}}</button>\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"ClearInputRole()\">Clear</button>\r\n    </div>\r\n  </form>\r\n</app-dialog>\r\n<!--Create New Role and Update On Popup-->\r\n<!--User Revoke On Popup-->\r\n<app-dialog [(visible)]=\"showrevoke\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Revoke Role</h3>\r\n    <div class=\"checkbox\">\r\n      <h4 class=\"card-title\">{{Username}}</h4>\r\n      <label> <input type=\"checkbox\" [(ngModel)]=\"checkRevokeUser\" name=\"checkRevokeUser\" class=\"CheckBoxLayout\"></label>\r\n      <label>  Is Active </label>\r\n    </div>\r\n    <button type=\"button\" class=\"btn btn-info\" (click)=\"RevokeUser()\">Save</button>\r\n    &nbsp;\r\n    <button type=\"button\" class=\"btn btn-info\" (click)=\"showrevoke = !showrevoke\">Cancel</button><!--RevokeCancel()\"-->\r\n  </div>\r\n</app-dialog>\r\n<!--End Of User Revoke On Popup-->\r\n<!--Assign Role-->\r\n<app-dialog [(visible)]=\"showAssignRole\">\r\n  <h5 class=\"card-title text-center\"><b>Assign Role</b></h5>\r\n  <div class=\"col-md-12\">\r\n    <div class=\"col-md-8\">\r\n      <input id=\"textSearch\" type=\"text\" [(ngModel)]=\"textSearch\" name=\"textSearch\" placeholder=\"Enter User  Pan/Employee Code\" class=\"form-control\">\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <button type=\"button\" class=\"btn btn-info text-center btn-sm\" (click)=\"Search(Upan, MsRoleID, roleName);\">GO</button>\r\n      <button type=\"button\" class=\"btn btn-info text-center btn-sm\" (click)=\"Cancel()\">Cancel</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-12 label-floating\">\r\n\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-striped\" style=\"height:350px\">\r\n        <thead>\r\n          <tr>\r\n            <th class=\"text-center\">User Name</th>\r\n            <th class=\"text-center\">Emp Code</th>\r\n            <th class=\"text-center\">PAN</th>\r\n            <!--<th class=\"text-center\">Designation</th>-->\r\n            <!--<th class=\"text-center\">IsActive</th>-->\r\n            <th class=\"text-center\">Roles</th>\r\n            <th class=\"text-center\"></th><!--Save-->\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n\r\n          <tr *ngFor=\"let User of ArrByPanUserDetail; let i = index\">\r\n            <!--ngfor-->\r\n            <td class=\"text-center\">{{User.firstName}}</td>\r\n            <td class=\"text-center\">{{User.empCd}}</td>\r\n            <td class=\"text-center\">{{User.pan}}</td>\r\n            <!--<td class=\"text-center\">{{User.designation}}</td>-->\r\n            <!--<td class=\"text-center\">{{User.isActive}}</td>-->\r\n            <td class=\"text-center\" width=\"500px;\">\r\n              <!--<mat-select multiple plaer=\"Select Role\" [(ngMo\"SlectedRole\" value=\"{{SlectedRole}}\">\r\n                      <mat-option *ngFor=\"let role of ArrRoles; let i = index\" id=\"Roles_{{role.roleID}}\" value=\"{{role.roleID}}\">\r\n                        {{role.roleName}}\r\n                      </mat-option>\r\n                    </mat-select>-->\r\n\r\n              <ng-multiselect-dropdown [placeholder]=\"'Select Role'\"\r\n                                       [data]=\"ArrAssignRoleForUser\"\r\n                                       [(ngModel)]=\"SlectedRole\"\r\n                                       [settings]=\"dropdownSettings\"\r\n                                       (onSelectAll)=\"onSelectAll($event)\">\r\n              </ng-multiselect-dropdown>\r\n\r\n\r\n            </td>\r\n            <td> <button type=\"button\" class=\"btn btn-info text-center btn-sm\" (click)=\"SaveAssignRoleForUser(i);\">Save</button></td>\r\n          </tr>\r\n\r\n        </tbody>\r\n        <tfoot>\r\n          <tr *ngIf=\"ArrByPanUserDetail?.length == 0\"><td><font color=\"red\">Records Not Found...</font></td></tr>\r\n        </tfoot>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</app-dialog>\r\n\r\n<!--End Of Assign Role-->\r\n\r\n<app-dialog [(visible)]=\"showAssignMenu\">\r\n  <div class=\"tree well col-lg-12\">\r\n    <h2>Assign/Un-assign Menu to {{nameRole}}</h2>\r\n    <div class=\"col-md-12\">\r\n      <table>\r\n        <tr class=\"table-head\">\r\n          <th class=\"tree-title\">\r\n            Menu Name\r\n          </th>\r\n          <th class=\"tree-title\">\r\n            <span class=\"num-tree-title\">\r\n              <label class=\"btn-space\">0</label>\r\n              <label class=\"btn-space\">1</label>\r\n              <label class=\"btn-space\">2</label>\r\n              <label class=\"btn-space\">3</label>\r\n            </span>\r\n          </th>\r\n          <!--<th>0</th>\r\n          <th>1</th>\r\n          <th>2</th>\r\n          <th>3</th>-->\r\n        </tr>\r\n        <tr>\r\n          <td>\r\n            <mat-tree [dataSource]=\"treeDataSource\" [treeControl]=\"treeControl\">\r\n              <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\r\n                <div class=\"mat-tree-node odd\">\r\n                  <button mat-icon-button disabled></button>\r\n                  {{node.mainMenuName}}\r\n                  <span class=\"rdo-btn\">\r\n                    <input type=\"radio\" hidden=\"hidden\" id=\"{{node.msMenuID}}\" value=\"{{node.status}}\">\r\n                    <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_0\" [disabled]=\"IsPermission\" [checked]=\"node.status == 0\" value=\"{{node.msMenuID}}_0\" (click)=\"RdValueSet(node.msMenuID,0);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                    <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_1\" [disabled]=\"IsPermission\" [checked]=\"node.status == 1\" value=\"{{node.msMenuID}}_1\" (click)=\"RdValueSet(node.msMenuID,1);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                    <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_2\"[disabled]=\"IsPermission\" [checked]=\"node.status == 2\" value=\"{{node.msMenuID}}_2\" (click)=\"RdValueSet(node.msMenuID,2);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                    <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_3\" [disabled]=\"IsPermission\" [checked]=\"node.status == 3\" value=\"{{node.msMenuID}}_3\" (click)=\"RdValueSet(node.msMenuID,3);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                  </span>\r\n                </div>\r\n              </mat-tree-node>\r\n              <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChildren\">\r\n                <div>\r\n                  <div class=\"mat-tree-node even\">\r\n                    <button mat-icon-button matTreeNodeToggle>\r\n                      <mat-icon class=\"mat-icon-rtl-mirror\">\r\n                        {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}\r\n                      </mat-icon>\r\n                    </button>\r\n                    {{node.mainMenuName}}<span class=\"rdo-btn\">\r\n                      <input type=\"radio\" hidden=\"hidden\" id=\"{{node.msMenuID}}\" value=\"{{node.status}}\">\r\n                      <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_0\" [disabled]=\"IsPermission\" value=\"{{node.msMenuID}}_0\" [checked]=\"node.status == 0\" (click)=\"RdValueSet(node.msMenuID,0);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                      <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_1\" [disabled]=\"IsPermission\" value=\"{{node.msMenuID}}_1\" [checked]=\"node.status == 1\" (click)=\"RdValueSet(node.msMenuID,1);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                      <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_2\" [disabled]=\"IsPermission\" value=\"{{node.msMenuID}}_2\" [checked]=\"node.status == 2\" (click)=\"RdValueSet(node.msMenuID,2);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                      <input type=\"radio\" class=\"btn-space\" id=\"{{node.msMenuID}}_3\" [disabled]=\"IsPermission\" value=\"{{node.msMenuID}}_3\" [checked]=\"node.status == 3\" (click)=\"RdValueSet(node.msMenuID,3);\" name=\"rdstatus_{{node.msMenuID}}\">\r\n                    </span>\r\n                  </div>\r\n                  <ul [class.hidden]=\"!treeControl.isExpanded(node)\">\r\n                    <ng-container matTreeNodeOutlet></ng-container>\r\n                  </ul>\r\n                </div>\r\n              </mat-nested-tree-node>\r\n            </mat-tree>\r\n          </td>\r\n        </tr>\r\n      </table>\r\n    </div>\r\n    <div class=\"col-md-12 role-detail\">\r\n      <span>Access Level:</span>\r\n      <table class=\"access-lbl-with\">\r\n        <tr>\r\n          <td>\r\n            0-- No Access\r\n          </td>\r\n          <td>\r\n            1-- View Only\r\n          </td>\r\n          <td>\r\n            2-- Add & Delete\r\n          </td>\r\n          <td>\r\n            3-- Edit & Delete\r\n          </td>\r\n        </tr>\r\n      </table>\r\n\r\n      <!--<ul>\r\n        <li>0-- No Access</li>\r\n        <li>1-- View Only</li>\r\n        <li>2-- Add & Delete</li>\r\n        <li>3-- Edit & Delete</li>\r\n\r\n      </ul>-->\r\n    </div>\r\n\r\n\r\n    <!--<div class=\"form-group col-md-6 panel panel-body panel-default\">\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"SaveMenuPerMission();\">Save</button>\r\n      <button type=\"button\" class=\"btn btn-info\">Cancel</button>\r\n    </div>-->\r\n\r\n    <div class=\"form-group text-center btn-margin col-md-12\">\r\n      \r\n     \r\n        <button *ngIf=\"Isbtnsave\" type=\"button\" class=\"btn btn-info\" (click)=\"SaveMenuPerMission();\">Save</button>\r\n       \r\n      <!--[disabled]=\"f.invalid\"-->\r\n      &nbsp;\r\n      <button type=\"button\" class=\"btn btn-info\" (click)=\"showAssignMenu=!showAssignMenu\">Cancel</button><!--AssignMenuPopUpCancel()-->\r\n    </div>\r\n  </div>\r\n\r\n</app-dialog>\r\n<!--Create New Role and Update On Popup-->\r\n<!--Loder-->\r\n<div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n  <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n</div>\r\n<!--End Of Loder-->\r\n"

/***/ }),

/***/ "./src/app/role-managment/role/role.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/role-managment/role/role.component.ts ***!
  \*******************************************************/
/*! exports provided: RoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleComponent", function() { return RoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/esm5/tree.es5.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm5/tree.es5.js");
/* harmony import */ var _services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/UserManagement/role.service */ "./src/app/services/UserManagement/role.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { Observable, of } from 'rxjs'



var RoleComponent = /** @class */ (function () {
    //For Flag
    //End Of Declare Variables
    function RoleComponent(http, _RoleService, snackBar) {
        this.http = http;
        this._RoleService = _RoleService;
        this.snackBar = snackBar;
        //Declare Variable
        this.displayedColumns = ['roleName', 'User', 'AssignMenu'];
        this.displayedUserColumns = ['FirstName', 'Designation', 'PAN', 'EmpCd'];
        this.isTableHasData = true;
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {};
        this.textNewrole = "";
        this.checkIsActiveRole = true;
        this.ArrRoles = [];
        this.hdnRoleId = 0;
        this.RoleIDs = '';
        this.iDsStatus = '';
        this.ArrBindSubmenu = [];
        this.ArrBindSubChildmenu = [];
        this.ArrTreestructureList = [];
        this.notFound = true;
        this.hasChildren = function (_, node) {
            return node.children && node.children.length > 0;
        };
        this.radioSelected = "";
        this.treeControl = new _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_5__["NestedTreeControl"](this.makeGetChildrenFunction());
        this.treeDataSource = new _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNestedDataSource"]();
    }
    RoleComponent.prototype.ngOnInit = function () {
        this.GetAllDataRollList();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'roleID',
            textField: 'roleName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: false
        };
        //this._RoleService.GetAssignRoleForUser(1).subscribe(result => {
        //  this.ArrAssignRoleForUser = result;
        //  //console.log(this.ArrByPanUserDetail);
        //  this.SlectedRole = this.ArrAssignRoleForUser;//   this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName }, { roleID: 2, roleName: "Controller" },];
        //})
        this.currentLesson = 'true';
    };
    RoleComponent.prototype.makeGetChildrenFunction = function () {
        return function (node) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["of"])(node.children); };
    };
    RoleComponent.prototype.TextChange = function () {
        this.lblPopupText = 'Create New Role';
        this.btnPopupText = 'Save';
        this.textNewrole = "";
        this.checkIsActiveRole = true;
        this.hdnRoleId = 0;
    };
    RoleComponent.prototype.resetRoleForm = function () {
        this.roleassign.resetForm();
    };
    RoleComponent.prototype.GetAllDataRollList = function () {
        var _this = this;
        //alert('hisa')
        //debugger;
        this.isLoading = true;
        debugger;
        this._RoleService.GetAllActiveRollList(sessionStorage.getItem('username')).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrRoles = result;
            _this.roledataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.roledataSource.paginator = _this.paginator;
            _this.roledataSource.sort = _this.sort;
            _this.selectedYears = [
                { id: 1 },
            ];
        });
    };
    RoleComponent.prototype.SaveNewRole = function () {
        var _this = this;
        this.isLoading = true;
        this.MstRole = {
            RoleName: this.textNewrole,
            IsActive: this.checkIsActiveRole,
            RoleID: this.hdnRoleId,
            LogInUserName: sessionStorage.getItem('username'),
            RoleParentID: '4'
        };
        JSON.stringify(this.MstRole);
        this._RoleService.SaveNewRole(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            console.log(result);
            _this.Message = result;
            _this.GetAllDataRollList();
            if (_this.Message != undefined) {
                jquery__WEBPACK_IMPORTED_MODULE_3__(".dialog__close-btn").click();
                _this.snackBar.open(_this.Message, '', {
                    duration: 2000,
                });
            }
            _this.ClearInputRole();
            _this.TxtSearch = '';
        });
    };
    RoleComponent.prototype.GetUserDetails = function (roleID, roleName) {
        var _this = this;
        this.isLoading = true;
        this.RoleNameUser = roleName;
        this.SlectedUserRowIndex = roleID;
        this.MstRole = {
            RoleID: roleID,
        };
        this._RoleService.GetUserDetails(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrUserList = result;
            _this.ArrUserListdataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"](result);
            _this.ArrUserListdataSource.paginator = _this.paginator;
            _this.ArrUserListdataSource.sort = _this.sort;
            if (_this.ArrUserListdataSource.data.length > 0) {
                _this.isTableHasData = true;
            }
            else {
                _this.isTableHasData = false;
            }
        });
        //alert(this.ArrUserList);
    };
    RoleComponent.prototype.ClearInputRole = function () {
        this.textNewrole = '';
        this.checkIsActiveRole = true;
        this.textSearch = '';
        this.ArrByPanUserDetail = null;
        this.textSearch = '';
        this.SlectedRole = '';
    };
    RoleComponent.prototype.FillRevokeDataOnPopUp = function (RowIndex) {
        this.Username = this.ArrUserList[RowIndex].firstName;
        this.hdnRoleId = this.ArrUserList[RowIndex].userID;
        if (this.ArrUserList[RowIndex].isActive == 'Active') {
            this.checkRevokeUser = true;
        }
        else {
            this.checkRevokeUser = false;
        }
    };
    //Update For role
    RoleComponent.prototype.UpdateRole = function (RowIndex) {
        this.lblPopupText = 'Update Role';
        this.btnPopupText = 'Update';
        this.textNewrole = this.ArrRoles[RowIndex].roleName;
        this.hdnRoleId = this.ArrRoles[RowIndex].roleID;
        if (this.ArrRoles[RowIndex].isActive == '1') {
            this.checkIsActiveRole = true;
        }
        else {
            this.checkIsActiveRole = false;
        }
    };
    //End Update For role
    //Revoke for User
    RoleComponent.prototype.RevokeUser = function () {
        var _this = this;
        this.isLoading = true;
        this.MstRole = {
            IsActive: this.checkRevokeUser,
            UserID: this.hdnRoleId
        };
        JSON.stringify(this.MstRole);
        this._RoleService.RevokeUser(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            if (_this.Message != undefined) {
                jquery__WEBPACK_IMPORTED_MODULE_3__(".dialog__close-btn").click();
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()(_this.Message);
                _this.GetUserDetails(_this.SlectedUserRowIndex, null);
            }
        });
    };
    //End Of Revoke for User
    RoleComponent.prototype.SaveAssignMenuPermission = function (roleID) {
        var _this = this;
        this.isLoading = true;
        this.RoleID = roleID;
        this._RoleService.SaveAssignMenuPermission().finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrAssignMenu = result;
        });
    };
    RoleComponent.prototype.Search = function (Upan, MsRoleID, roleName) {
        var _this = this;
        //debugger;
        //alert(Upan);
        this.isLoading = true;
        this.setFlag = 'SaveForRole';
        this.MsRoleID = MsRoleID;
        if (MsRoleID != undefined) {
            this.setFlag = 'EditForRole';
            this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName },];
        }
        if (MsRoleID == undefined || MsRoleID == '') {
            this._RoleService.GetAssignRoleForUser(this.textSearch).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
                _this.ArrAssignRoleForUser = result;
                _this.SlectedRole = _this.ArrAssignRoleForUser;
            });
        }
        if (Upan == undefined) {
            if (this.textSearch == undefined || this.textSearch == '') {
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()("Please Enter The Pan Number..");
                return false;
            }
        }
        else {
            this.textSearch = Upan;
        }
        this.MstRole = {
            Pan: this.textSearch
        };
        this._RoleService.Search(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrByPanUserDetail = result;
        });
    };
    RoleComponent.prototype.SaveAssignRoleForUser = function (RowIndex) {
        var _this = this;
        this.isLoading = true;
        for (var i = 0; i < this.SlectedRole.length; i++) {
            this.RoleIDs = this.RoleIDs + this.SlectedRole[i].roleID + '$'; //alert(i);
        }
        this.MstRole = {
            RoleIDs: this.RoleIDs,
            UserID: this.ArrByPanUserDetail[RowIndex].userID,
            setFlag: this.setFlag,
            MsRoleID: this.MsRoleID
        };
        this._RoleService.SaveAssignRoleForUser(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            _this.SlectedRole = null;
            _this.RoleIDs = null;
            if (_this.Message != undefined) {
                _this.ClearInputRole();
                jquery__WEBPACK_IMPORTED_MODULE_3__(".dialog__close-btn").click();
                _this.GetUserDetails(_this.SlectedUserRowIndex, null);
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()(_this.Message);
            }
        });
    };
    RoleComponent.prototype.searchUserListOnKeyUp = function (event) {
        jquery__WEBPACK_IMPORTED_MODULE_3__("#UserInput").on("keyup", function () {
            var value = jquery__WEBPACK_IMPORTED_MODULE_3__(this).val().toLowerCase();
            jquery__WEBPACK_IMPORTED_MODULE_3__("#DivUserList tr").filter(function () {
                jquery__WEBPACK_IMPORTED_MODULE_3__(this).toggle(jquery__WEBPACK_IMPORTED_MODULE_3__(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
    };
    //Tree structure For Test
    RoleComponent.prototype.TreestructureList = function (roleID, RoleName, roleStatus) {
        var _this = this;
        if (roleStatus == 'p') {
            this.Isbtnsave = false;
            this.IsPermission = true;
        }
        else {
            this.Isbtnsave = true;
            this.IsPermission = false;
        }
        this.isLoading = true;
        this.nameRole = RoleName;
        this._RoleService.TreestructureList(roleID).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrTreestructureList = result;
            _this.treeControl = new _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_5__["NestedTreeControl"](_this.makeGetChildrenFunction());
            _this.treeDataSource = new _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNestedDataSource"]();
            _this.treeDataSource.data = _this.ArrTreestructureList;
        });
    };
    //End Of Trss Structure
    RoleComponent.prototype.SaveMenuPerMission = function () {
        var _this = this;
        //debugger;
        //alert(' call'+this.textSearch)
        this.textSearch = '';
        this.isLoading = true;
        var iDsStatus = "";
        var iDsStatusarr = [];
        var RolID = this.RoleID;
        jquery__WEBPACK_IMPORTED_MODULE_3__(".rdo-btn  input[type=radio]:checked").each(function (i) {
            iDsStatusarr[i] = this.value + '_' + RolID;
        });
        this._RoleService.SaveMenuPerMission(iDsStatusarr).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            if (_this.Message != undefined) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()(_this.Message);
                jquery__WEBPACK_IMPORTED_MODULE_3__(".dialog__close-btn").click();
                _this.GetAllDataRollList();
            }
        });
    };
    RoleComponent.prototype.RdValueSet = function (Rdstatus, value) {
        var _this = this;
        debugger;
        this.isLoading = true;
        var id = Rdstatus + "/" + value;
        var currentid = Rdstatus + "_" + value;
        var iDsStatus = "";
        var iDsStatusarr = [];
        var RolID = this.RoleID;
        jquery__WEBPACK_IMPORTED_MODULE_3__(".rdo-btn  input[type=radio]:checked").each(function (i) {
            iDsStatusarr[i] = this.value + '_' + id;
        });
        var data = {
            id: id,
            iDsStatusarr: iDsStatusarr
        };
        var Status = iDsStatusarr;
        var currentstatus = '';
        this._RoleService.CheckMenuPerMission(iDsStatusarr).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            if (_this.Message != undefined && _this.Message != "") {
                var prevstatus = jquery__WEBPACK_IMPORTED_MODULE_3__("#" + Rdstatus).val();
                jquery__WEBPACK_IMPORTED_MODULE_3__("#" + Rdstatus + "_" + prevstatus).prop("checked", true);
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default()(_this.Message);
            }
            else {
                jquery__WEBPACK_IMPORTED_MODULE_3__("#" + Rdstatus).val(value);
            }
        });
    };
    RoleComponent.prototype.applyFilter = function (filterValue) {
        this.roledataSource.filter = filterValue.trim().toLowerCase();
        if (this.roledataSource.paginator) {
            this.roledataSource.paginator.firstPage();
        }
        if (this.roledataSource.filteredData.length > 0) {
            this.notFound = true;
        }
        else {
            this.notFound = false;
        }
    };
    RoleComponent.prototype.UserapplyFilter = function (filterValue) {
        //if (this.ArrUserListdataSource == null) {
        //  this.isTableHasData = false;
        //}
        this.ArrUserListdataSource.filter = filterValue.trim().toLowerCase();
        if (this.ArrUserListdataSource.paginator) {
            this.ArrUserListdataSource.paginator.firstPage();
        }
        if (this.ArrUserListdataSource.filteredData.length > 0) {
            this.isTableHasData = true;
        }
        else {
            this.isTableHasData = false;
        }
    };
    RoleComponent.prototype.Cancel = function () {
        this.textSearch = '';
        this.SlectedRole = null;
        this.ArrAssignRoleForUser = null;
        this.ArrByPanUserDetail = null;
        jquery__WEBPACK_IMPORTED_MODULE_3__(".dialog__close-btn").click();
    };
    RoleComponent.prototype.charaterOnlyNoSpace = function (event) {
        // debugger;
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], RoleComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], RoleComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('roleassign'),
        __metadata("design:type", Object)
    ], RoleComponent.prototype, "roleassign", void 0);
    RoleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-role',
            template: __webpack_require__(/*! ./role.component.html */ "./src/app/role-managment/role/role.component.html"),
            styles: [__webpack_require__(/*! ./role.component.css */ "./src/app/role-managment/role/role.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_6__["RoleService"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBar"]])
    ], RoleComponent);
    return RoleComponent;
}());



/***/ }),

/***/ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/role-managment/roleusermanagment/roleusermanagment.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvbGUtbWFuYWdtZW50L3JvbGV1c2VybWFuYWdtZW50L3JvbGV1c2VybWFuYWdtZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/role-managment/roleusermanagment/roleusermanagment.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n      <!--<div class=\"col-md-3\">\r\n          <mat-form-field class=\"wid-100\">\r\n        <mat-select matNativeControl placeholder=\"Select Role\" name=\"roleID\" (selectionChange)=\"Rolechange($event.value)\">\r\n          <mat-option label=\"Select Commission Code\">Select Role</mat-option>\r\n          <mat-option *ngFor=\"let _ArrRoles of ArrRoles\" [value]=\"_ArrRoles.roleID\">\r\n            {{ _ArrRoles.roleName }}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>-->\r\n      <!--<p *ngIf=\"show\">\r\n          <mat-error><span>Commission Code required</span></mat-error>\r\n        </p>\r\n      </div>-->\r\n\r\n      <div class=\"col-md-4\">\r\n        <div>\r\n          <!--*ngIf=\"ddlControlerDiv==true\"-->\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select controller\" [formControl]=\"controllerCtrl\" (selectionChange)=\"oncontrollerroleSelectchanged($event.value);\">\r\n              <!--HideShowBtn()-->\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"controllerFilterCtrl\" [placeholderLabel]=\"'Find controller...'\"  [noEntriesFoundLabel]=\"'Controller is not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let emp of filteredcontroller |async \" [value]=\"emp.values\">\r\n                {{emp.text}}\r\n              </mat-option>\r\n              <!--<mat-option [value]=\"0\">\r\n                Select\r\n              </mat-option>-->\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-2\">\r\n        <div>\r\n          <button type=\"button\" class=\"btn btn-info mt-10\" (click)=\"GO();Cancel();\">GO </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n  <div *ngIf=\"SearchAssignroleFlage\" class=\"col-lg-12 mb-15\">\r\n    <div class=\"example-card mat-card\">\r\n\r\n      <div class=\"col-md-12\">\r\n        <div class=\"col-md-4 select-radio select-lbl\">\r\n          <!--<h5><b> Search User By </b></h5>-->\r\n          Select:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n          <input type=\"radio\" name=\"Rdoption\" value=\"PAN\" (click)=\"BindUserListddl('PAN');\"> PAN &nbsp;&nbsp;\r\n          <input type=\"radio\" name=\"Rdoption\" value=\"NAME\" (click)=\"BindUserListddl('NAME');\"> Username&nbsp;&nbsp;\r\n          <input type=\"radio\" name=\"Rdoption\" value=\"EMPCODE\" (click)=\"BindUserListddl('EMPCODE');\"> EMPCODE&nbsp;&nbsp;\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select\" [formControl]=\"designFilter\" required (selectionChange)=\"SetText($event.value)\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Select'\" [noEntriesFoundLabel]=\"'Record not found'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let designa of filteredDesignation | async\" [value]=\"designa.text\">\r\n                {{designa.text}}\r\n              </mat-option>\r\n              <!--<mat-option [value]=\"0\">\r\n                Select\r\n              </mat-option>-->\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-2\">\r\n          <button type=\"button\" class=\"btn btn-primary mt-10\" (click)=\"Search(Upan, '', roleName);\">Assign role to user</button>\r\n        </div>\r\n      </div>\r\n      <!--<div class=\"col-md-12\">\r\n        <div class=\"col-md-2\">\r\n        </div>\r\n        <div class=\"col-md-4\">\r\n          <mat-form-field class=\"wid-100\">\r\n            <mat-select placeholder=\"Select\" [formControl]=\"designFilter\" required (selectionChange)=\"SetText($event.value)\">\r\n              <mat-option>\r\n                <ngx-mat-select-search [formControl]=\"designFilterCtrl\" [placeholderLabel]=\"'Select'\"></ngx-mat-select-search>\r\n              </mat-option>\r\n              <mat-option *ngFor=\"let designa of filteredDesignation | async\" [value]=\"designa.text\">\r\n                {{designa.text}}\r\n              </mat-option>\r\n\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <button type=\"button\" class=\"btn btn-info\" (click)=\"Search(Upan, '', roleName);\">Assign role to user</button>\r\n         </div>\r\n      </div>-->\r\n    </div>\r\n\r\n  </div>\r\n\r\n\r\n  <div *ngIf=\"GrideAssignroleFlage\" class=\"col-lg-12\">\r\n    <div class=\"example-card mat-card\">\r\n      <div class=\"fom-title\">Assign Role to User</div>\r\n      <div class=\"col-md-12\">\r\n        <table mat-table [dataSource]=\"ArrByPanUserDetaildatasource\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n          <ng-container matColumnDef=\"firstName\">\r\n            =\"\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> First Name </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.firstName}} </td>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"empCd\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Emp Code </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.empCd}} </td>\r\n          </ng-container>\r\n\r\n\r\n          <ng-container matColumnDef=\"pan\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Pan No </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.pan}} </td>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"role\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Role </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n              <ng-multiselect-dropdown [placeholder]=\"'Select Role'\" [data]=\"ArrRoles\" [(ngModel)]=\"SlectedRole\" [settings]=\"dropdownSettings\"\r\n                                       (onSelectAll)=\"onSelectAll($event)\">\r\n              </ng-multiselect-dropdown>\r\n            </td>\r\n\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n\r\n              <button type=\"button\" class=\"btn btn-success text-center btn-sm\" (click)=\"SaveAssignRoleForUser(element.userID);\">Save</button><!--(click)=\"SaveAssignRoleForUser(i);\"-->\r\n            </td>\r\n          </ng-container>\r\n\r\n          <tr mat-header-row *matHeaderRowDef=\"displayedUserColumns\"></tr>\r\n          <tr mat-row *matRowDef=\"let row; columns: displayedUserColumns;\"></tr>\r\n\r\n        </table>\r\n\r\n        <!--<mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!--Loder-->\r\n  <div *ngIf=\"isLoading\" style=\"padding-top: 20%;\" class=\"LoadingBar\">\r\n    <img height=\"70px\" width=\"70px\" src=\"~/../assets/Loder12.gif\">\r\n  </div>\r\n  <!--End Of Loder-->\r\n\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/role-managment/roleusermanagment/roleusermanagment.component.ts ***!
  \*********************************************************************************/
/*! exports provided: RoleusermanagmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleusermanagmentComponent", function() { return RoleusermanagmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/UserManagement/role.service */ "./src/app/services/UserManagement/role.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/UserManagement/controller.service */ "./src/app/services/UserManagement/controller.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RoleusermanagmentComponent = /** @class */ (function () {
    //END OF 
    function RoleusermanagmentComponent(_RoleService, http, snackBar, _controllerservice) {
        this._RoleService = _RoleService;
        this.http = http;
        this.snackBar = snackBar;
        this._controllerservice = _controllerservice;
        this.displayedColumns = ['firstName', 'empCd', 'pan', 'designation', 'action'];
        this.displayedUserColumns = ['firstName', 'empCd', 'pan', 'role', 'action']; //['userID', 'role', 'firstName', 'empCd', 'pan', 'action'];
        //For Serchable Dropdown
        this.designationCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.designFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.filteredDesignation = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
        /** Subject that emits when the component has been destroyed. */
        this._onDestroy = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.ArrUserList = [];
        this.isTableHasData = true;
        this.RoleIDs = '';
        this.unselectedRole = [];
        this.ArrByPanUserDetail = [];
        this.dropdownSettings = {};
        this.controllerCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.designFilter = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.controllerFilterCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.filteredcontroller = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
    }
    //End Of Bind ddl  Controler
    RoleusermanagmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.GetUserDetails('2');
        this.setFlag = "SaveForRole";
        this.btnAssignFlage = true;
        this.btnFlageView = false;
        this.GrideAssignroleFlage = false;
        this.SearchAssignroleFlage = false;
        this.GetAllDataRollList();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'roleID',
            textField: 'roleName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: false
        };
        this.currentLesson = 'true';
        // listen for search field value changes
        this.designFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filterDesignation();
        });
        //Controller ddl
        this.controllerFilterCtrl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(this._onDestroy))
            .subscribe(function () {
            _this.filtercontroller();
        });
        this.getallcontrollers(sessionStorage.getItem('controllerID'));
        if (sessionStorage.getItem('userRoleID') == '1') {
        }
        else {
            this.btnAssignRoledivfleg = false;
        }
    };
    //BindCommissionCode() {
    //  this.payscale.BindCommissionCode().subscribe(result => {
    //    this.ArrRoles = result;
    //  })
    //}
    RoleusermanagmentComponent.prototype.GetAllDataRollList = function () {
        var _this = this;
        this.isLoading = true;
        //alert(sessionStorage.getItem('username'));
        this._RoleService.GetAllCustomeRollList(sessionStorage.getItem('username')).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrRoles = result;
        });
    };
    RoleusermanagmentComponent.prototype.GetUserDetails = function (roleID) {
        var _this = this;
        this.isLoading = true;
        this.MstRole = {
            RoleID: roleID,
        };
        this._RoleService.GetUserDetails(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrUserList = result;
            _this.userdataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.ArrUserList);
            _this.userdataSource.paginator = _this.paginator;
            _this.userdataSource.sort = _this.sort;
            if (_this.userdataSource.data.length > 0) {
                _this.isTableHasData = true;
            }
            else {
                _this.isTableHasData = false;
            }
        });
    };
    RoleusermanagmentComponent.prototype.Rolechange = function (value) {
        var _this = this;
        if (value != undefined && value > 0) {
            this.temproleid = value;
            this.SelectRoleID = value;
            debugger;
            this.designFilterCtrl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(this._onDestroy))
                .subscribe(function () {
                _this.filterDesignation();
            });
            //Controller ddl
            this.controllerFilterCtrl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(this._onDestroy))
                .subscribe(function () {
                _this.filtercontroller();
            });
            this.getallcontrollers(sessionStorage.getItem('controllerID'));
        }
    };
    RoleusermanagmentComponent.prototype.GO = function () {
        if (sessionStorage.getItem('userRoleID') != null && sessionStorage.getItem('userRoleID') != undefined) {
            //if (this.SelectRoleID == undefined) {
            //  alert('Please Select Role');
            //  return;
            //}
            if (this.SelectControllerID == undefined) {
                alert('Please Select Controller');
                return;
            }
            this.btnAssignRoledivfleg = true;
            this.SearchAssignroleFlage = true;
            this.designFilter.setValue(0);
            this.GetUserDetails(this.SelectRoleID);
        }
    };
    RoleusermanagmentComponent.prototype.HideShowBtn = function () {
        var _this = this;
        this.MstRole = {
            RoleID: this.SelectRoleID,
            MsControllerID: this.SelectControllerID,
        };
        this._RoleService.GetUserDetails(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrUserFleg = result;
            if (_this.ArrUserFleg.length == 0) {
                // alert('Fi');
                _this.btnAssignFlage = true;
                _this.btnFlageView = false;
            }
            else if (_this.ArrUserFleg.length != 0) {
                // alert('hello')
                _this.btnAssignFlage = false;
                _this.btnFlageView = true;
            }
        });
        //alert(this.ArrUserList);
    };
    RoleusermanagmentComponent.prototype.Search = function (Upan, MsRoleID, roleName) {
        var _this = this;
        debugger;
        this.isLoading = true;
        // this.setFlag = 'SaveForRole';
        this.MsRoleID = MsRoleID;
        if (MsRoleID != undefined) {
            // this.setFlag = 'EditForRole';
            this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName },];
        }
        if (MsRoleID == undefined || MsRoleID == '') {
            this._RoleService.GetAssignRoleForUser(this.textSearch).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
                _this.ArrAssignRoleForUser = result;
                _this.SlectedRole = _this.ArrAssignRoleForUser;
                _this.slectedRoleAlready = _this.SlectedRole;
            });
        }
        if (Upan == undefined) {
            if (this.textSearch == undefined || this.textSearch == '') {
                alert("Please select PAN Or UserName Or EMPCODE");
                return false;
            }
        }
        else {
            this.textSearch = Upan;
        }
        this.MstRole = {
            Pan: this.textSearch
        };
        this._RoleService.Search(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrByPanUserDetail = result;
            _this.ArrByPanUserDetaildatasource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](_this.ArrByPanUserDetail);
            _this.ArrByPanUserDetaildatasource.paginator = _this.paginator;
            _this.ArrByPanUserDetaildatasource.sort = _this.sort;
            _this.GrideAssignroleFlage = true;
        });
    };
    //GetUserDetails(roleID, roleName)//RowIndex)
    //{
    //  //alert(roleID);
    //  this.RoleNameUser = roleName;
    //  this.SlectedUserRowIndex = roleID;
    //  this.MstRole = {
    //    RoleID: roleID,
    //  };
    //  this._RoleService.GetUserDetails(this.MstRole).subscribe(result => {
    //    this.ArrUserList = result;
    //  })
    //  //alert(this.ArrUserList);
    //}
    RoleusermanagmentComponent.prototype.SaveAssignRoleForUser = function (userID) {
        var _this = this;
        //debugger;
        this.isLoading = true;
        // alert(this.MsRoleID);
        for (var i = 0; i < this.SlectedRole.length; i++) {
            this.RoleIDs = this.RoleIDs + this.SlectedRole[i].roleID + '$'; //alert(i);
        }
        if (this.slectedRoleAlready.length != null) {
            this.setFlag = "EditForRole";
        }
        for (var j = 0; j < this.slectedRoleAlready.length; j++) {
            debugger;
            this.unselectedRole.push(this.slectedRoleAlready[j].roleID);
            for (var i = 0; i < this.SlectedRole.length; i++) {
                if (this.SlectedRole[i].roleID == this.slectedRoleAlready[j].roleID) {
                    this.slectedRoleAlready.splice(j, 1);
                    if (this.slectedRoleAlready.length === 0) {
                        break;
                    }
                    //   this.slectedRoleAlready[i]
                }
            }
        }
        debugger;
        this.MstRole = {
            RoleIDs: this.RoleIDs,
            UserID: userID,
            setFlag: this.setFlag,
            MsRoleID: this.MsRoleID,
            unselectedRole: this.slectedRoleAlready
        };
        debugger;
        this._RoleService.SaveAssignRoleForUser(this.MstRole).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.Message = result;
            _this.SlectedRole = null;
            _this.RoleIDs = null;
            if (_this.Message != undefined) {
                _this.ClearInputRole();
                jquery__WEBPACK_IMPORTED_MODULE_8__(".dialog__close-btn").click();
                if (_this.Message == 'Role UnAssigned Sucessfully') {
                    //alert('conditions');
                    _this.btnAssignFlage = true;
                    _this.btnFlageView = false;
                }
                if (_this.Message == 'Role Assigned Sucessfully') {
                    //alert('Assigned');
                    _this.btnAssignFlage = false;
                    _this.btnFlageView = true;
                }
                _this.GetUserDetails(_this.temproleid);
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default()(_this.Message);
                _this.RoleIDs = '';
                _this.GrideAssignroleFlage = false;
                _this.SearchAssignroleFlage = false;
            }
        });
    };
    RoleusermanagmentComponent.prototype.Cancel = function () {
        //this.filteredDesignation = null;
        this.designFilterCtrl.reset();
        this.textSearch = '';
        this.ArrUserListddl = null;
        //this.SlectedRole = null;
        this.ArrAssignRoleForUser = null;
        this.ArrByPanUserDetail = null;
        this.ArrByPanUserDetaildatasource = null;
        jquery__WEBPACK_IMPORTED_MODULE_8__(".dialog__close-btn").click();
    };
    RoleusermanagmentComponent.prototype.ClearInputRole = function () {
        // this.textNewrole = '';
        // this.checkIsActiveRole = true;
        this.textSearch = '';
        this.ArrByPanUserDetail = null;
        this.ArrByPanUserDetaildatasource = null;
        this.textSearch = '';
        //this.SlectedRole = null;
    };
    RoleusermanagmentComponent.prototype.applyFilter = function (filterValue) {
        this.userdataSource.filter = filterValue.trim().toLowerCase();
        if (this.userdataSource.paginator) {
            this.userdataSource.paginator.firstPage();
        }
    };
    RoleusermanagmentComponent.prototype.BindUserListddl = function (Rdtext) {
        var _this = this;
        this.isLoading = true;
        this.GrideAssignroleFlage = false;
        this.designFilter.setValue(0);
        this._RoleService.GetBindUserListddl(Rdtext, this.SelectControllerID).finally(function () { return _this.isLoading = false; }).subscribe(function (result) {
            _this.ArrUserListddl = result;
            _this.designationCtrl.setValue(_this.ArrUserListddl);
            _this.filteredDesignation.next(_this.ArrUserListddl);
        });
    };
    RoleusermanagmentComponent.prototype.SetText = function (text) {
        //alert(text)
        this.textSearch = text;
        this.GrideAssignroleFlage = false;
    };
    RoleusermanagmentComponent.prototype.filterDesignation = function () {
        if (!this.ArrUserListddl) {
            return;
        }
        // get the search keyword
        var search = this.designFilterCtrl.value;
        if (!search) {
            this.filteredDesignation.next(this.ArrUserListddl.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the
        this.filteredDesignation.next(this.ArrUserListddl.filter(function (ArrUserListddl) { return ArrUserListddl.text.toLowerCase().indexOf(search) > -1; }));
    };
    //Bind controller ddl
    RoleusermanagmentComponent.prototype.filtercontroller = function () {
        debugger;
        if (!this.controllerroleList) {
            return;
        }
        // get the search keyword
        var search = this.controllerFilterCtrl.value;
        if (!search) {
            this.filteredcontroller.next(this.controllerroleList.slice());
            return;
        }
        else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredcontroller.next(this.controllerroleList.filter(function (controllerroleList) { return controllerroleList.text.toLowerCase().indexOf(search) > -1; }));
    };
    RoleusermanagmentComponent.prototype.getallcontrollers = function (ControllerID) {
        var _this = this;
        debugger;
        //if (ControllerID == 0) {
        this.Query = 'getController';
        this.controllerCtrl.setValue(0);
        this._controllerservice.getAllControllerServiceclient(ControllerID, this.Query).subscribe(function (data) {
            _this.controllerroleList = data;
            _this.controllerCtrl.setValue(_this.controllerroleList);
            _this.filteredcontroller.next(_this.controllerroleList);
        });
        // }
    };
    //End Of Bind  Controler ddl
    RoleusermanagmentComponent.prototype.oncontrollerroleSelectchanged = function (SelectControllerID) {
        this.SelectControllerID = SelectControllerID;
        this.Cancel();
        this.GrideAssignroleFlage = false;
        this.SearchAssignroleFlage = false;
        // alert(SelectControllerID);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], RoleusermanagmentComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RoleusermanagmentComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('singleSelect'),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelect"])
    ], RoleusermanagmentComponent.prototype, "singleSelect", void 0);
    RoleusermanagmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-roleusermanagment',
            template: __webpack_require__(/*! ./roleusermanagment.component.html */ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.html"),
            styles: [__webpack_require__(/*! ./roleusermanagment.component.css */ "./src/app/role-managment/roleusermanagment/roleusermanagment.component.css")]
        }),
        __metadata("design:paramtypes", [_services_UserManagement_role_service__WEBPACK_IMPORTED_MODULE_2__["RoleService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"], _services_UserManagement_controller_service__WEBPACK_IMPORTED_MODULE_6__["controllerservice"]])
    ], RoleusermanagmentComponent);
    return RoleusermanagmentComponent;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/menu.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/UserManagement/menu.service.ts ***!
  \*********************************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var MenuService = /** @class */ (function () {
    function MenuService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
        //this.BaseUrl = 'http://localhost:55424/api/';
        //this.BaseUrl = 'http://10.199.72.57/epsapi/api/';
    }
    MenuService.prototype.SaveMenu = function (hdnMenuId, uRoleID, MstRole) {
        //alert(uRoleID);
        return this.httpclient.post(this.config.saveMenu + hdnMenuId + '&uRoleID=' + uRoleID, MstRole, { responseType: 'text' });
    };
    MenuService.prototype.BindDropDownMenu = function () {
        return this.httpclient.get(this.config.bindDropDownMenu, {});
    };
    MenuService.prototype.BindMenuInGride = function () {
        return this.httpclient.get(this.config.bindMenuInGride, {});
    };
    MenuService.prototype.FillUpdateMenu = function (MsMenuID) {
        return this.httpclient.get(this.config.FillUpdateMenu + MsMenuID, {});
    };
    MenuService.prototype.ActiveDeactiveMenu = function (MstMenu) {
        return this.httpclient.post(this.config.activeDeactiveMenu, MstMenu, { responseType: 'text' });
    };
    MenuService.prototype.GetPredefineRole = function () {
        //alert("all");
        return this.httpclient.get("" + this.config.GetPredefineRole);
    };
    MenuService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/services/UserManagement/role.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/UserManagement/role.service.ts ***!
  \*********************************************************/
/*! exports provided: RoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleService", function() { return RoleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


//import { AppConfig, APP_CONFIG } from '../../global/global.module';

var RoleService = /** @class */ (function () {
    function RoleService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
        this.BaseUrl = [];
    }
    RoleService.prototype.GetAllDataRollList = function (username) {
        //alert("all");
        return this.httpclient.get("" + (this.config.getAllRole + '?username=' + username));
    };
    RoleService.prototype.GetAllActiveRollList = function (username) {
        // alert("all");
        return this.httpclient.get("" + (this.config.getAllActiveRollList + '?username=' + username));
    };
    RoleService.prototype.GetAllCustomeRollList = function (username) {
        //alert("all");
        return this.httpclient.get("" + (this.config.GetAllCustomeRollList + '?username=' + username));
    };
    RoleService.prototype.GetBindUserListddl = function (RdText, SelectControllerID) {
        // alert(sessionStorage.getItem('controllerID'));
        debugger;
        //alert(sessionStorage.getItem('UserID'));
        return this.httpclient.get("" + (this.config.GetBindUserListddl + '?RdStatus=' + RdText + '&LoggRollID=' + sessionStorage.getItem('userRoleID') + '&LoogInUser=' + sessionStorage.getItem('UserID') + '&ControleerID=' + SelectControllerID + '&paoid=' + sessionStorage.getItem('paoid') + '&ddoid=' + sessionStorage.getItem('ddoid') + '&CurrUserControllerID=' + sessionStorage.getItem('controllerID')));
    };
    RoleService.prototype.SaveNewRole = function (MstRole) {
        return this.httpclient.post(this.config.saveNewRole, MstRole, { responseType: 'text' });
    };
    RoleService.prototype.GetUserDetails = function (UserRole) {
        //
        return this.httpclient.post(this.config.getUserDetails, UserRole);
    };
    RoleService.prototype.RevokeUser = function (MstRole) {
        return this.httpclient.post(this.config.revokeUser, MstRole, { responseType: 'text' });
    };
    RoleService.prototype.SaveAssignMenuPermission = function () {
        return this.httpclient.get(this.config.getAllRole, {});
    };
    RoleService.prototype.Search = function (MstRole) {
        // alert(this.config.getUserDetailsByPan);
        return this.httpclient.post(this.config.getUserDetailsByPan, MstRole);
    };
    RoleService.prototype.GetAssignRoleForUser = function (roleID) {
        //alert(this.config.getAssignRoleForUser + "-" + roleID);
        //debugger;
        return this.httpclient.post(this.config.getAssignRoleForUser + roleID, {}); //this.config.getAssignRoleForUser, MstRole);
    };
    RoleService.prototype.SaveAssignRoleForUser = function (MstRole) {
        return this.httpclient.post(this.config.saveAssignRoleForUser, MstRole, { responseType: 'text' });
    };
    RoleService.prototype.TreestructureList = function (roleID) {
        return this.httpclient.post(this.config.getAllMenuList + '?roleID=' + roleID + '&Uname=' + sessionStorage.getItem('username') + '&CurrURole=' + sessionStorage.getItem('userRoleID'), {});
    };
    RoleService.prototype.SaveMenuPerMission = function (iDsStatusarr) {
        return this.httpclient.post(this.config.saveMenuPerMission, iDsStatusarr, { responseType: 'text' });
    };
    RoleService.prototype.CheckMenuPerMission = function (iDsStatusarr) {
        return this.httpclient.post(this.config.checkMenuPerMission, iDsStatusarr, { responseType: 'text' });
    };
    RoleService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], RoleService);
    return RoleService;
}());



/***/ })

}]);
//# sourceMappingURL=role-managment-role-managment-module.js.map