(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["endofservice-endofservice-module"],{

/***/ "./src/app/endofservice/endofservice-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/endofservice/endofservice-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: EndofserviceRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndofserviceRoutingModule", function() { return EndofserviceRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _endofservice_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./endofservice.module */ "./src/app/endofservice/endofservice.module.ts");
/* harmony import */ var _endofservice_endofservice_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./endofservice/endofservice.component */ "./src/app/endofservice/endofservice/endofservice.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '', component: _endofservice_module__WEBPACK_IMPORTED_MODULE_2__["EndofserviceModule"], children: [
            { path: 'endofservice', component: _endofservice_endofservice_component__WEBPACK_IMPORTED_MODULE_3__["EndofserviceComponent"] },
        ]
    }
];
var EndofserviceRoutingModule = /** @class */ (function () {
    function EndofserviceRoutingModule() {
    }
    EndofserviceRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EndofserviceRoutingModule);
    return EndofserviceRoutingModule;
}());



/***/ }),

/***/ "./src/app/endofservice/endofservice.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/endofservice/endofservice.module.ts ***!
  \*****************************************************/
/*! exports provided: EndofserviceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndofserviceModule", function() { return EndofserviceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _endofservice_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./endofservice-routing.module */ "./src/app/endofservice/endofservice-routing.module.ts");
/* harmony import */ var _endofservice_endofservice_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./endofservice/endofservice.component */ "./src/app/endofservice/endofservice/endofservice.component.ts");
/* harmony import */ var _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared-module/shared-module.module */ "./src/app/shared-module/shared-module.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/*17-9-19 start*/




/*17-9-19 End*/



var EndofserviceModule = /** @class */ (function () {
    function EndofserviceModule() {
    }
    EndofserviceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_endofservice_endofservice_component__WEBPACK_IMPORTED_MODULE_7__["EndofserviceComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _endofservice_routing_module__WEBPACK_IMPORTED_MODULE_6__["EndofserviceRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"],
                _shared_module_shared_module_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]
            ]
        })
    ], EndofserviceModule);
    return EndofserviceModule;
}());



/***/ }),

/***/ "./src/app/endofservice/endofservice/endofservice.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/endofservice/endofservice/endofservice.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VuZG9mc2VydmljZS9lbmRvZnNlcnZpY2UvZW5kb2ZzZXJ2aWNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/endofservice/endofservice/endofservice.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/endofservice/endofservice/endofservice.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--------------- Form --------------->\r\n<!--Star Current Details-->\r\n<form name=\"form\" (ngSubmit)=\"endModel.valid && endofserviceMaster();\" #endModel=\"ngForm\" novalidate>\r\n  <div class=\"basic-container select-drop-head\">\r\n    <div class=\"row  selection-hed\">\r\n      <div class=\"col-md-5 col-sm-12 combo-col\">\r\n        <div class=\"col-sm-12 col-lg-3 pading-0\">\r\n          <label>Employee Status:</label>\r\n        </div>\r\n        <div class=\"col-sm-12 col-lg-5\">\r\n          <mat-radio-group (change)=\"radioChange($event.value)\">\r\n            <mat-radio-button [value]=\"0\" [checked]='true'>Superannuation</mat-radio-button>\r\n            <mat-radio-button [value]=\"1\">Other</mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Designation\" (selectionChange)=\"getDesignchange($event.value)\" [(ngModel)]=\"_endModel.desigID\" name=\"desigID\" required>\r\n            <mat-option *ngFor=\"let empData of getDesi\" [value]=\"empData.desigID\">{{empData.desigDesc}}</mat-option>\r\n          </mat-select>\r\n          <mat-error> Designation Required !</mat-error>\r\n        </mat-form-field>\r\n \r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Employee\" (selectionChange)=\"getEmpChange($event.value)\" [(ngModel)]=\"_endModel.msEmpID\" name=\"msEmpID\" required>\r\n            <mat-option *ngFor=\"let empData of getEmp\" [value]=\"empData.msEmpID\">{{empData.empName}}-{{empData.empCd}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Employee Code Required!</mat-error>\r\n        </mat-form-field>\r\n\r\n      </div>\r\n      <!--<div class=\"col-sm-12 col-md-1 margin-top-10px\"><button class=\"btn btn-info\" type=\"button\">GO</button></div>-->\r\n    </div>\r\n  </div>\r\n\r\n  <mat-accordion class=\"col-md-12\">\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n        <mat-panel-title class=\"acordion-heading\">\r\n          Current Details\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n      <!--<div class=\"display-non\">\r\n        <input type=\"hidden\" [(ngModel)]=\"_endModel.msEmpID\" name=\"msEmpID\" #msEmpID=\"ngModel\" />\r\n        <input type=\"hidden\" [(ngModel)]=\"_endModel.empCd\" name=\"empCd\" #empCd=\"ngModel\" />\r\n        <input type=\"hidden\" [(ngModel)]=\"_endModel.empName\" name=\"empName\" #empName=\"ngModel\" />\r\n      </div>-->\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Employee Type\" [(ngModel)]=\"_endModel.empApptType\" name=\"empApptType\" required #empApptType=\"ngModel\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker1\" placeholder=\"Superanuation Date\" [(ngModel)]=\"_endModel.superanuationDate\" name=\"superanuationDate\" required #superanuationDate=\"ngModel\" disabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker1></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Current Posting Mode\" [(ngModel)]=\"_endModel.currentPostingMode\" name=\"currentPostingMode\" required #currentPostingMode=\"ngModel\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker2\" placeholder=\"Date of Entry in Govt. Service \" [(ngModel)]=\"_endModel.dateofEntry\" name=\"dateofEntry\" required #dateofEntry=\"ngModel\" disabled>\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker2></mat-datepicker>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Last Bill Generated (Bill Month)\" [(ngModel)]=\"_endModel.billMonthName\" name=\"billMonthName\" required #billMonthName=\"ngModel\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Bill Year\" [(ngModel)]=\"_endModel.billYear\" name=\"billYear\" required #billYear=\"ngModel\" disabled>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <!--<div class=\"col-md-12 col-lg-12 text-center btn-wraper blue\">\r\n        <button type=\"submit\" class=\"btn btn-success\">Save</button>\r\n        <button type=\"submit\" class=\"btn btn-info\">Update</button>\r\n        <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n      </div>-->\r\n\r\n    </mat-expansion-panel>\r\n  </mat-accordion>\r\n  <!--End Current Details-->\r\n  <!--Star-- End of Service Details-->\r\n  <mat-accordion class=\"col-md-12\">\r\n    <mat-expansion-panel>\r\n      <mat-expansion-panel-header id=\"panel-header\" class=\"panel-header btm-m15\">\r\n        <mat-panel-title class=\"acordion-heading\">\r\n          End of Service Details\r\n        </mat-panel-title>\r\n      </mat-expansion-panel-header>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput placeholder=\"Order No.\" [(ngModel)]=\"_endModel.endOrderNo\" required #endOrderNo=\"ngModel\" [disabled]=\"disableflag\" name=\"endOrderNo\">\r\n          <mat-error>Order No Required !</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker3\" placeholder=\"Order Date\" [(ngModel)]=\"_endModel.endOrderDt\" required #endOrderDt=\"ngModel\" [disabled]=\"disableflag\" name=\"endOrderDt\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker3\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker3></mat-datepicker>\r\n          <mat-error>Order Date Required !</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <mat-select placeholder=\"Select Reason\" [(ngModel)]=\"_endModel.endReasonId\" name=\"endReasonId\" (selectionChange)=\"reasonChange($event.value)\" required>\r\n            <mat-option *ngFor=\"let getRes of getReason\" [value]=\"getRes.msCddirID\">{{getRes.cddirCodeText}}</mat-option>\r\n          </mat-select>\r\n          <mat-error>Reason Required !</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      \r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <input matInput [matDatepicker]=\"picker4\" placeholder=\"Date of End of Service\" [(ngModel)]=\"_endModel.endServDt\" required #endServDt=\"ngModel\"  [disabled]=\"disableflag\" name=\"endServDt\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"picker4\"></mat-datepicker-toggle>\r\n          <mat-datepicker #picker4></mat-datepicker>\r\n          <mat-error>Date of End of Service Required !</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-lg-4\">\r\n        <mat-form-field class=\"wid-100\">\r\n          <textarea matInput placeholder=\"Remark\" [(ngModel)]=\"_endModel.endRemark\" required #endRemark=\"ngModel\" [disabled]=\"disableflag\" name=\"endRemark\">         \r\n        </textarea>\r\n          <mat-error>Remarks Required !</mat-error>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-12 col-lg-4\">\r\n  <mat-form-field class=\"wid-100\">\r\n    <input matInput [matDatepicker]=\"picker5\" placeholder=\"Date of Death\" [(ngModel)]=\"_endModel.dtOfDeath\" [required]=\"requiredvalue\" #dtOfDeath=\"ngModel\" [disabled]=\"disabledeath\" name=\"dtOfDeath\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker5\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker5></mat-datepicker>\r\n    <mat-error>Date of Death Required !</mat-error>\r\n  </mat-form-field>\r\n</div>\r\n\r\n      <div class=\"col-md-12 col-lg-12 text-center btn-wraper blue\">\r\n        <button type=\"submit\" class=\"btn btn-success\" *ngIf=\"savebuttonstatus\">{{btnUpdatetext}}</button>\r\n        <!--<button type=\"submit\" class=\"btn btn-info\">Update</button>-->\r\n        <button type=\"button\" class=\"btn btn-warning\">Cancel</button>\r\n      </div>\r\n\r\n    </mat-expansion-panel>\r\n  </mat-accordion>\r\n</form>\r\n\r\n<!--End-- End of Service Details-->\r\n\r\n<div class=\"col-md-12 col-lg-12 margin-top-10px\">\r\n  <div class=\"example-card mat-card\">\r\n\r\n    <div class=\"fom-title\">Joining Details</div>\r\n    <!-- Search -->\r\n    <mat-form-field>\r\n      <input matInput placeholder=\"Search\">\r\n      <i class=\"material-icons icon-right\">search</i>\r\n    </mat-form-field>\r\n\r\n\r\n    <table mat-table [dataSource]=\"dataSource2\" matSort class=\"mat-elevation-z8 even-odd-color\">\r\n\r\n      <!-- Position Column -->\r\n      <ng-container matColumnDef=\"endOrderNo\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Number </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.endOrderNo}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Name Column -->\r\n      <ng-container matColumnDef=\"endOrderDt\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Order Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.endOrderDt|date}} </td>\r\n      </ng-container>\r\n\r\n      <!-- Weight Column -->\r\n      <ng-container matColumnDef=\"endServDt\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> From Date </th>\r\n        <td mat-cell *matCellDef=\"let element\"> {{element.endServDt|date}} </td>\r\n      </ng-container>\r\n\r\n\r\n\r\n      <!-- Symbol Column -->\r\n      <ng-container matColumnDef=\"action\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n        <td mat-cell *matCellDef=\"let element\">\r\n          <a class=\"material-icons i-info\" matTooltip=\"Info\">error</a>\r\n          <a class=\"material-icons i-edit\" matTooltip=\"Edit\" (click)=\"editDetails(element.msEmpID)\">edit</a>\r\n          <a class=\"material-icons i-delet\" matTooltip=\"Delete\" (click)=\"setDeleteId(element.msEmpID);deletepopup = !deletepopup\"> delete_forever </a>\r\n        </td>\r\n\r\n\r\n\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns2\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns2;\"></tr>\r\n    </table>\r\n    <div *ngIf=\"dataSource2.data.length == 0\">\r\n      No  Records Found\r\n    </div>\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10]\" [pageSize]=\"10\" showFirstLastButtons></mat-paginator>\r\n  </div>\r\n</div>\r\n<app-dialog [(visible)]=\"deletepopup\">\r\n  <div class=\"card-content panel panel-body panel-default\">\r\n    <h3 class=\"card-title text-center\">Are you sure you want to delete?</h3>\r\n    <div _ngcontent-c10=\"\" class=\"col-md-12 col-lg-12 text-center btn-wraper\">\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deleteTptaDetails(setDeletIDOnPopup)\">Delete</button>\r\n      <button type=\"button\" class=\"btn btn-info center\" (click)=\"deletepopup = !deletepopup\">Cancel</button>\r\n    </div>\r\n\r\n  </div>\r\n</app-dialog>\r\n\r\n"

/***/ }),

/***/ "./src/app/endofservice/endofservice/endofservice.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/endofservice/endofservice/endofservice.component.ts ***!
  \*********************************************************************/
/*! exports provided: EndofserviceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndofserviceComponent", function() { return EndofserviceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_endofservice_endofservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/endofservice/endofservice.service */ "./src/app/services/endofservice/endofservice.service.ts");
/* harmony import */ var _services_empdetails_posting_details_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/empdetails/posting-details.service */ "./src/app/services/empdetails/posting-details.service.ts");
/* harmony import */ var _model_EndofServiceModel_updatemodel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/EndofServiceModel/updatemodel */ "./src/app/model/EndofServiceModel/updatemodel.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { MatTableDataSource } from '@angular/material/table';




//export interface PeriodicElement2 {
//  orderDate: string;
//  orderNo: number;
//  fromDate: number;
//  action: string;
//}
//const ELEMENT_DATA2: PeriodicElement2[] = [
//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },
//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },
//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },
//];
var EndofserviceComponent = /** @class */ (function () {
    function EndofserviceComponent(endofserviceGetEmp, desing) {
        this.endofserviceGetEmp = endofserviceGetEmp;
        this.desing = desing;
        this.displayedColumns2 = ['endOrderNo', 'endOrderDt', 'endServDt', 'action'];
        this.dataSource2 = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"]();
    }
    //displayedColumns2: string[] = ['position', 'name', 'weight', 'symbol'];
    //dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
    //@ViewChild(MatSort) sort: MatSort;
    EndofserviceComponent.prototype.ngOnInit = function () {
        this._endModel = new _model_EndofServiceModel_updatemodel__WEBPACK_IMPORTED_MODULE_4__["Endofservicemodel"]();
        //this.dataSource2.sort = this.sort;
        this.btnUpdatetext = 'Save';
        this.savebuttonstatus = true;
        this.userName = sessionStorage.getItem('username');
        this.ddoId = Number(sessionStorage.getItem('ddoid'));
        this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
        this.roleId = Number(sessionStorage.getItem('userRoleID'));
        this.showEmp();
        this.showDesg();
        this.getReasonDetails();
        this.requiredvalue = false;
        this.disabledeath = true;
        this.result = 0;
        this.statusId = 50;
    };
    EndofserviceComponent.prototype.endofserviceMaster = function () {
        var _this = this;
        //this._endModel.dtOfDeath = null;
        // this._endModel.endReasonId = this.msCddirID;
        this._endModel.createdBy = this.userName;
        this._endModel.statusId = this.statusId; //entry 50, updated 65,79  RC 	Reject by DDO checker,80     V  	verify by DDO checker   
        this._endModel.createdDate = new Date();
        this.endofserviceGetEmp.InsertEndDetails(this._endModel).subscribe(function (res) {
            _this.successMsg = res;
        });
        if (this.result == 0) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()('Data successfully saved');
            // this.snackBar.open('Data Rejected Successfully', null, { duration: 4000 });
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()('Data successfully updated');
        }
        this.showEmpList(this._endModel.msEmpID);
        this.successMsg;
    };
    EndofserviceComponent.prototype.showEmp = function () {
        var _this = this;
        if (this.desigID == null) {
            this.endofserviceGetEmp.getEndofService('', this.PermDdoId, this.getRedio).subscribe(function (res) {
                _this.getEmp = res;
            });
            this.btnUpdatetext = 'Save';
        }
    };
    EndofserviceComponent.prototype.getReasonDetails = function () {
        var _this = this;
        this.endofserviceGetEmp.getEndofServiceReasonDt().subscribe(function (res) {
            _this.getReason = res;
        });
        debugger;
        this.getReason;
    };
    EndofserviceComponent.prototype.reasonChange = function (value) {
        debugger;
        if (value == "526") {
            this.requiredvalue = true;
            this.disabledeath = false;
        }
        else {
            this.requiredvalue = false;
            this.disabledeath = true;
            this._endModel.dtOfDeath = null;
        }
    };
    EndofserviceComponent.prototype.radioChange = function (value1) {
        //debugger;
        this.getRedio = value1;
    };
    EndofserviceComponent.prototype.getDesignchange = function (value) {
        var _this = this;
        debugger;
        this.getEmp = null;
        //this.desigID = null;
        this.desigID = value;
        this.endofserviceGetEmp.getEndofService(this.desigID, this.PermDdoId, this.getRedio).subscribe(function (res) {
            _this.getEmp = res;
            _this._endModel.msEmpID = null;
        });
    };
    EndofserviceComponent.prototype.getEmpChange = function (value) {
        var _this = this;
        debugger;
        this.endofserviceGetEmp.currentEndDetails(value).subscribe(function (res) {
            _this._endModel = res[0];
            _this._endModel.desigID = _this.desigID;
        });
        this.btnUpdatetext = 'Save';
        this.showEmpList(value);
    };
    EndofserviceComponent.prototype.showDesg = function () {
        var _this = this;
        //debugger;
        this.desing.GetAllDesignation().subscribe(function (res) {
            _this.getDesi = res;
        });
    };
    EndofserviceComponent.prototype.showEmpList = function (value) {
        var _this = this;
        this.endofserviceGetEmp.getEndofServiceList(value).subscribe(function (res) {
            _this.dataSource2 = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](res);
            _this.dataSource2.paginator = _this.paginator;
            _this.dataSource2.sort = _this.sort;
        });
    };
    EndofserviceComponent.prototype.editDetails = function (empId) {
        var _this = this;
        this.endofserviceGetEmp.getEndofServiceList(empId).subscribe(function (res) {
            _this._updatemodel = res[0];
            _this._endModel.endOrderNo = _this._updatemodel.endOrderNo;
            _this._endModel.endOrderDt = _this._updatemodel.endOrderDt;
            _this._endModel.endServDt = _this._updatemodel.endServDt;
            _this._endModel.endReasonId = _this._updatemodel.endReasonId;
            _this._endModel.endRemark = _this._updatemodel.endRemark;
            _this._endModel.statusId = _this._updatemodel.statusId;
            _this._endModel.dtOfDeath = _this._updatemodel.dtOfDeath;
            _this.btnUpdatetext = 'Update';
            _this.result = 1;
            _this.statusId = 60;
        });
    };
    EndofserviceComponent.prototype.setDeleteId = function (value) {
        this.setDeletIDOnPopup = value;
    };
    EndofserviceComponent.prototype.deleteTptaDetails = function (hraMasterID) {
        var _this = this;
        this.endofserviceGetEmp.DeleteEndDetails(hraMasterID).subscribe(function (res) {
            if (res != undefined) {
                _this.deletepopup = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()(res);
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], EndofserviceComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], EndofserviceComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('endModel'),
        __metadata("design:type", Object)
    ], EndofserviceComponent.prototype, "form", void 0);
    EndofserviceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-endofservice',
            template: __webpack_require__(/*! ./endofservice.component.html */ "./src/app/endofservice/endofservice/endofservice.component.html"),
            styles: [__webpack_require__(/*! ./endofservice.component.css */ "./src/app/endofservice/endofservice/endofservice.component.css")],
            providers: [_services_empdetails_posting_details_service__WEBPACK_IMPORTED_MODULE_3__["PostingDetailsService"]]
        }),
        __metadata("design:paramtypes", [_services_endofservice_endofservice_service__WEBPACK_IMPORTED_MODULE_2__["EndofserviceService"], _services_empdetails_posting_details_service__WEBPACK_IMPORTED_MODULE_3__["PostingDetailsService"]])
    ], EndofserviceComponent);
    return EndofserviceComponent;
}());



/***/ }),

/***/ "./src/app/model/EndofServiceModel/updatemodel.ts":
/*!********************************************************!*\
  !*** ./src/app/model/EndofServiceModel/updatemodel.ts ***!
  \********************************************************/
/*! exports provided: Updatemodel, Endofservicemodel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Updatemodel", function() { return Updatemodel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Endofservicemodel", function() { return Endofservicemodel; });
var Updatemodel = /** @class */ (function () {
    function Updatemodel() {
    }
    return Updatemodel;
}());

var Endofservicemodel = /** @class */ (function () {
    function Endofservicemodel() {
    }
    return Endofservicemodel;
}());



/***/ }),

/***/ "./src/app/services/endofservice/endofservice.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/endofservice/endofservice.service.ts ***!
  \***************************************************************/
/*! exports provided: EndofserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndofserviceService", function() { return EndofserviceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var EndofserviceService = /** @class */ (function () {
    function EndofserviceService(http, config) {
        this.http = http;
        this.config = config;
    }
    //
    EndofserviceService.prototype.getEndofService = function (designid, empPermDDOId, mode) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('designid', designid).set('empPermDDOId', empPermDDOId).set('mode', mode);
        return this.http.get("" + this.config.api_base_url + this.config.EndofServiceGetEmployee, { params: params });
    }; //
    EndofserviceService.prototype.currentEndDetails = function (msEmpID) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('msEmpID', msEmpID);
        return this.http.get("" + this.config.api_base_url + this.config.EmpCurrentEndDeta, { params: params });
    };
    //Insert Details
    //InsertEndDetails(_hraModel: any): Observable<any> {
    //  const params = new HttpParams().set('obj', _hraModel);
    //  return this.http.post<any>(`${this.config.api_base_url}${this.config.EndofServiceInsert}`, { params });
    //}
    EndofserviceService.prototype.InsertEndDetails = function (obj) {
        debugger;
        // const parameter = new HttpParams().set('Flag', Flag);
        return this.http.post("" + this.config.api_base_url + this.config.EndofServiceInsert, obj, { responseType: 'text' });
    };
    EndofserviceService.prototype.DeleteEndDetails = function (id) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('obj', id);
        return this.http.delete("" + this.config.api_base_url + this.config.EndofServiceDelete, { params: params });
    };
    //
    EndofserviceService.prototype.getEndofServiceReasonDt = function () {
        return this.http.get("" + this.config.api_base_url + this.config.EndofServiceReason);
    };
    EndofserviceService.prototype.getEndofServiceList = function (msEmpID) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('msEmpID', msEmpID);
        return this.http.get("" + this.config.api_base_url + this.config.EndofServiceGetDetailByEmp, { params: params });
    };
    EndofserviceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], EndofserviceService);
    return EndofserviceService;
}());



/***/ })

}]);
//# sourceMappingURL=endofservice-endofservice-module.js.map