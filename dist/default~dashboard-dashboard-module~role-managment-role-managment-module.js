(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~dashboard-dashboard-module~role-managment-role-managment-module"],{

/***/ "./src/app/dashboard/dashboard/dashboard.component.css":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/dashboard/dashboard.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard/dashboard/dashboard.component.html":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/dashboard/dashboard.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<mat-sidenav-container class=\"example-container\" autosize (click)=\"$event.stopPropagation()\" >\r\n  <mat-sidenav class=\"example-sidenav sidebar-mini\" #sideNav mode=\"side\" opened=\"true\">\r\n    <mat-nav-list>\r\n      <app-nav-menu *ngFor=\"let item of navItems\" [item]=\"item\" class=\"mg_b\" (numberGenerated)=\"onNumberGenerated()\" ></app-nav-menu>\r\n    </mat-nav-list>\r\n  </mat-sidenav>\r\n  <!--<p>Random number from child component: {{ randomNumber || 'Not yet generated' }}</p>-->\r\n  <button mat-icon-button (click)=\"sideNav.toggle()\">\r\n    <mat-icon class=\"material-icons md-24\">menu</mat-icon>\r\n  </button>\r\n\r\n  <ul class=\"breadcrumb\" *ngIf=\"navItems.length > 0\">\r\n    <li>\r\n      <a href=\"/dashboard/home\" (click)=\"resetbreadcrumbsDtls()\">Home </a> <span> {{breadcrumbsDtls.displayName}} </span>\r\n    </li>\r\n  </ul>\r\n  <!-- breadcrumb -->\r\n  <!--<div class=\"col\">\r\n    <ol class=\"breadcrumb\">\r\n      <li class=\"breadcrumb-item\"\r\n          *ngFor=\"let item of breadcrumbList; let i = index\"\r\n          [class.active]=\"i===breadcrumbList.length-1\">\r\n        <a [routerLink]=\"item.route\" *ngIf=\"i!==breadcrumbList.length-1\">\r\n          {{ item.displayName }}\r\n        </a>\r\n        <span *ngIf=\"i===breadcrumbList.length-1\">{{ item.displayName }}</span>\r\n      </li>\r\n    </ol>\r\n  </div>-->\r\n\r\n  <router-outlet></router-outlet>\r\n</mat-sidenav-container>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard/dashboard.component.ts":
/*!************************************************************!*\
  !*** ./src/app/dashboard/dashboard/dashboard.component.ts ***!
  \************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_nav_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/nav.service */ "./src/app/services/nav.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/dashboard/dashboard.service */ "./src/app/services/dashboard/dashboard.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(navService, _DashboardService, activatedRoute, router) {
        var _this = this;
        this.navService = navService;
        this._DashboardService = _DashboardService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.version = _angular_material__WEBPACK_IMPORTED_MODULE_2__["VERSION"];
        this.breadcrumbsDtls = {};
        this.menu = [];
        this.breadcrumbList = [];
        this.breadcrumbs = [];
        var breadcrumb = {
            label: 'Home',
            url: ''
        };
        this.username = sessionStorage.getItem('username');
        this.uRoleID = sessionStorage.getItem('userRoleID');
        this.getAllMenusByUser(this.username, this.uRoleID);
        //this.listenRouting();
        this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"]; })).subscribe(function (event) {
            debugger;
            _this.breadcrumbs1 = event;
            _this.breadcrumbs2 = _this.breadcrumbs1.url;
            //this.breadcrumbs2 = this.breadcrumbs2.substring(1, this.breadcrumbs2.length);
            var re = /\//gi;
            _this.breadcrumbs2 = _this.breadcrumbs2.substring(1, _this.breadcrumbs2.length).replace(re, " / ");
            // this.breadcrumbs1.substring(1);
            //let root: ActivatedRoute = this.activatedRoute.root;
            //this.breadcrumbs = this.getBreadcrumbs(root);
            //this.breadcrumbs = [breadcrumb, ...this.breadcrumbs];
            //   let lastBreadCrumbLabel = this.breadcrumbs[this.breadcrumbs.length - 1].label;
        });
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.onNumberGenerated();
        this.username = sessionStorage.getItem('username');
        this.uRoleID = sessionStorage.getItem('userRoleID');
        this.getAllMenusByUser(this.username, this.uRoleID);
        // this.listenRouting();
        //this.username = sessionStorage.getItem('username');
        //alert(sessionStorage.getItem('username'));
        // this.titleService.setTitle('EPS' + ':' + sessionStorage.getItem('menudisplayName').split('/')[1]);
    };
    DashboardComponent.prototype.onNumberGenerated = function () {
        //alert(this.randomNumber.displayName);
        //this.randomNumber = randomNumber;
        //console.log(this.randomNumber);
        this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
        this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
        //this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
        //this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
        // alert(this.breadcrumbsDtls.route);
        // console.log(sessionStorage.getItem('menudisplayName'));
    };
    DashboardComponent.prototype.resetbreadcrumbsDtls = function () {
        sessionStorage.setItem('menudisplayName', "");
        sessionStorage.setItem('menuroute', "");
    };
    DashboardComponent.prototype.getAllMenusByUser = function (username, uRoleID) {
        var _this = this;
        this._DashboardService.getAllMenusByUser(username, uRoleID).subscribe(function (result) {
            _this.ArrAllMenus = result;
            _this.navItems = _this.ArrAllMenus;
            _this.menu = _this.navItems;
        });
    };
    DashboardComponent.prototype.getBreadcrumbs = function (route, url, breadcrumbs) {
        if (url === void 0) { url = ''; }
        if (breadcrumbs === void 0) { breadcrumbs = []; }
        //debugger
        var ROUTE_DATA_BREADCRUMB = 'breadcrumb';
        var children = route.children;
        if (children.length === 0) {
            return breadcrumbs;
        }
        for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
            var child = children_1[_i];
            if (child.outlet !== _angular_router__WEBPACK_IMPORTED_MODULE_4__["PRIMARY_OUTLET"]) {
                continue;
            }
            if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
                return this.getBreadcrumbs(child, url, breadcrumbs);
            }
            var routeURL = child.snapshot.url.map(function (segment) { return segment.path; }).join("/");
            url += "/" + routeURL;
            var breadcrumb = {
                label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
                params: child.snapshot.params,
                url: url
            };
            breadcrumbs.push(breadcrumb);
            return this.getBreadcrumbs(child, url, breadcrumbs);
        }
        return breadcrumbs;
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_services_nav_service__WEBPACK_IMPORTED_MODULE_1__["NavService"], _services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_3__["DashboardService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/services/dashboard/dashboard.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/dashboard/dashboard.service.ts ***!
  \*********************************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DashboardService = /** @class */ (function () {
    function DashboardService(httpclient, config) {
        this.httpclient = httpclient;
        this.config = config;
    }
    DashboardService.prototype.getAllMenusByUser = function (username, uroleid) {
        return this.httpclient.get(this.config.getAllMenusByUser + "?username=" + username + "&uroleid=" + uroleid, {});
    };
    DashboardService.prototype.getDashboardDetail = function () {
        return this.httpclient.get(this.config.api_base_url + this.config.getDashboardDetal);
    };
    DashboardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_global_global_module__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"])),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _global_global_module__WEBPACK_IMPORTED_MODULE_2__["AppConfig"]])
    ], DashboardService);
    return DashboardService;
}());



/***/ }),

/***/ "./src/app/services/nav.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/nav.service.ts ***!
  \*****************************************/
/*! exports provided: NavService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavService", function() { return NavService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavService = /** @class */ (function () {
    function NavService(router) {
        var _this = this;
        this.router = router;
        this.currentUrl = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](undefined);
        // debugger;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                _this.currentUrl.next(event.urlAfterRedirects);
            }
        });
    }
    NavService.prototype.closeNav = function () {
        this.sideNav.close();
    };
    NavService.prototype.openNav = function () {
        this.sideNav.open();
    };
    NavService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NavService);
    return NavService;
}());



/***/ })

}]);
//# sourceMappingURL=default~dashboard-dashboard-module~role-managment-role-managment-module.js.map