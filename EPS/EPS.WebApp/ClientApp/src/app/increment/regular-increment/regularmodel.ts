import { FormControl, FormGroup, Validators } from '@angular/forms';

export class regularModel {
  id: number;
  incID: number;
  empName: string;
  oldBasic: Number;
  nextIncDate: Date;
  oldWefDate: Date;
  payLevel: string;
  noofIncrement: Number;
  orderNo: string;
  orderDate: Date;
  orderType: string
  wefDate: Date;
  remark: string;
  desigCode: string;
  empCode: string;
  salaryMonth: string;
  loginUser: string;
  changeSubType: string;
  orderTypeID: number
  noofEmployee: number
  status: string;
  desigination: string;
  clientIP: string;
  newBasic: number;
  empID: number;
  //public List < string > IncrementData { get; set; }

  msDesigMastID: number;
  desigDesc: string;

  static asFormGroup(regularModel: regularModel): FormGroup {
    const fg = new FormGroup({
      id: new FormControl(regularModel.id),
      incID: new FormControl(regularModel.incID),
      empName: new FormControl(regularModel.empName),
      oldBasic: new FormControl(regularModel.oldBasic),
      nextIncDate: new FormControl(regularModel.nextIncDate),
      oldWefDate: new FormControl(regularModel.oldWefDate),
      payLevel: new FormControl(regularModel.payLevel),
      noofIncrement: new FormControl(regularModel.noofIncrement),
      orderNo: new FormControl(regularModel.orderNo),
      orderDate: new FormControl(regularModel.orderDate),
      orderType: new FormControl(regularModel.orderType),
      wefDate: new FormControl(regularModel.wefDate),
      remark: new FormControl(regularModel.remark),
      desigCode: new FormControl(regularModel.desigCode),
      empCode: new FormControl(regularModel.empCode),
      salaryMonth: new FormControl(regularModel.salaryMonth),
      loginUser: new FormControl(regularModel.loginUser),
      changeSubType: new FormControl(regularModel.changeSubType),
      orderTypeID: new FormControl(regularModel.orderTypeID),
      noofEmployee: new FormControl(regularModel.noofEmployee),
      status: new FormControl(regularModel.status),
      desigination: new FormControl(regularModel.desigination),
      clientIP: new FormControl(regularModel.clientIP),
      newBasic: new FormControl(regularModel.newBasic),
      empID: new FormControl(regularModel.empID),
      msDesigMastID: new FormControl(regularModel.msDesigMastID),
      desigDesc: new FormControl(regularModel.desigDesc),
     //select: new FormControl(regularModel.select)
    });
    return fg;
  }
}
