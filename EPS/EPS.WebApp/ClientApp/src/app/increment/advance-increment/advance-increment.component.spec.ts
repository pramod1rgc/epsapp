import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceIncrementComponent } from './advance-increment.component';

describe('AdvanceIncrementComponent', () => {
  let component: AdvanceIncrementComponent;
  let fixture: ComponentFixture<AdvanceIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
