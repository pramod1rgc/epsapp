import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopIncrementComponent } from './stop-increment.component';

describe('StopIncrementComponent', () => {
  let component: StopIncrementComponent;
  let fixture: ComponentFixture<StopIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
