import { Component, OnInit, ViewChild } from '@angular/core';
import { RegincrementService } from '../../services/increment/regincrement.service';
import { StopincrementService } from '../../services/increment/stopincrement.service';
import { PayscaleService } from '../../services/payscale.service';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';


const ELEMENT_DeAttachdata: EmpDeAttach[] = [];
const ELEMENT_DATA: EmpAttach[] = [];
export interface EmpAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

export interface EmpDeAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

@Component({
  selector: 'app-stop-increment',
  templateUrl: './stop-increment.component.html',
  styleUrls: ['./stop-increment.component.css']
})
export class StopIncrementComponent implements OnInit {
  _pScaleObj: PayscaleModel;
  PermDdoId: string;
  Design: any[];
  msDesigMastID: any;
  wefDate: any;
  Remark: any;
  incID: any;
  noofEmployee: any;
  dataSource: any;
  dataSourceEmp: any;
  dataeourceemp1: any;
  Message: any;
  username: any;
  IncrementData: any[] = [];
  regInc: any = {};
  regInc1: any[] = [];
  dataDeAttach: any[] = [];
  dataSourceDeAttach: any;
  data: any;
  CommissionCodelist: any;
  disableflag: boolean;
  disablebtnflag: boolean;
  showhidediv0: boolean;
  showhidediv1: boolean;
  showhidediv2: boolean;
  showhidediv3: boolean;
  disableFDflag: boolean;
  msCddirID: any;
  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  checkedObject: any[] = [];
  DcheckedObject: any[] = [];
  showPH: boolean;
  deletepopup: boolean;
  checktrue = false;
  isTableHasData = true;
  isTableHasDataY = true;
  isTableHasDataX = true;
  totalAttach = 0;
  setDeletIDOnPopup: any;

  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  btnCssClass = 'btn btn-success';
  constructor(private regincrement: RegincrementService, private stopincrement: StopincrementService, private payscale: PayscaleService) {    
  }
  
  displayedColumns: string[] = ['orderNo', 'orderDate', 'noofEmployee', 'status', 'action'];
  displayedColumns1: string[] = ['select', 'empName', 'desigination', 'oldBasic',  'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  displayedColumns2: string[] = ['select', 'empName', 'desigination', 'oldBasic',  'nextIncDate', 'payLevel'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('paginatorDetach') paginatorDetach: MatPaginator;
  @ViewChild('paginatorAttach') paginatorAttach: MatPaginator;
  @ViewChild('sortDetach') sortDetach: MatSort;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('f') form: any;
  @ViewChild('mainf') formain: any;

  ngOnInit() {
    this._pScaleObj = new PayscaleModel();
    this.username = sessionStorage.getItem('username');
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = false;
    this.disableflag = false;
    this.showhidediv3 = false;
    this.disablebtnflag = false;
    //this.GetEmpWithOrderNo();
    this.bindPayCommission();
  }

  bindPayCommission() {
    this.payscale.BindCommissionCode().subscribe(results => {
      this.CommissionCodelist = results;
    })
  }


  //Create Order No
  createOrder() {
    this.regInc.loginUser = this.username;
    this.regInc.DesigCode = "0";
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Stop Increment";
    this.regincrement.createOrderNo(this.regInc).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        this.deletepopup = false;
        this.showPH = false;

        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";
        this.Message = result;
      }
      this.form.resetForm();
      //this.formain.resetForm();
      this.getEmpWithOrderNo();

      this.bgcolor = "";
      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
    })

  }
  getEmpWithOrderNo() {
    this.regInc.DesigCode = "0";
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Stop Increment";
    this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(result => {
      this.dataSourceEmp = new MatTableDataSource(result);
      if (this.dataSourceEmp.filteredData.length > 0)
        this.isTableHasData = true;
      else
        this.isTableHasData = false;
    })
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = true;
    this.showhidediv3 = false;
  }

  //End Order No

  // Attached and Detached Employee Data
  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllSelectedForDeAttach() {
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }

  masterToggleForAttach() {
    this.checkedObject = null;
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));

    this.checkedObject = this.dataSource.data;
    this.checkedObject = this.selectionAttach.selected;
  }

  //working
  masterToggleForDeAttach() {
    this.checkedObject = null;
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
      this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
    this.checkedObject = this.dataSourceDeAttach.data;
    this.DcheckedObject = this.dataSourceDeAttach.data;
  }


  addCheckedObj() {
    this.checkedObject = null;
    this.checkedObject = this.selectionAttach.selected;

  }


  deAttachSelectedRows() {
    this.totalAttach = 0;
    this.checkedObject = null;
    if (this.selectionAttach.hasValue()) {
      this.selectionAttach.selected.forEach(item => {
        let index: number = this.data.findIndex(d => d === item);
        this.dataSource.data.splice(index, 1);
        // this.dataDeAttach.push(item);
        this.dataDeAttach.unshift(item);
        this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);
        this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);
        this.dataSource.paginator = this.paginatorAttach;
        this.dataSourceDeAttach.paginator = this.paginatorDetach;
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
      if (this.dataSourceDeAttach.filteredData.length > 0)
        this.isTableHasDataX = true;
      else
        this.isTableHasDataX = false;
    }
    else {
      alert("Please attach at least one employee");
    }
    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
  }

 

  attachSelectedRows() {
    this.totalAttach = 0;
    this.disableFDflag = true;
    this.disableflag = false;
    this.checkedObject = null;
    if (this.selectionDeAttach.hasValue()) {
      this.selectionDeAttach.selected.forEach(item => {
        let index: number = this.dataDeAttach.findIndex(d => d === item);
        this.dataDeAttach.splice(index, 1);
        this.data.push(item);
        //this.data.unshift(item);
        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
      this.dataSource.paginator = this.paginatorAttach;
      this.dataSourceDeAttach.paginator = this.paginatorDetach;
    }
    else {
      alert("Please attach at least one employee ");
    }
    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    if (this.dataSource.filteredData.length > 0) {
      this.disablebtnflag = false;
      this.isTableHasDataY = true;
    }
    else {
      this.disablebtnflag = true;
      this.isTableHasDataY = false;
    }
  }
  //End Attached and Detached


  // Get Employee for bind grid
  totalEmp: number;
  indexView: number;
  getEmployeeForIncrement(ID: any, indexView: any) {
    this.indexView = indexView;
    this.stopincrement.getEmployeeStopIncrement(ID).subscribe(result => {
      this.data = result;
      //this.checkedObject = result;
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginatorAttach;
      this.dataSource.sort = this.sort;
      this.totalEmp = this.checkedObject.length;
      for (let i = 0; i < this.dataSource.filteredData.length; i++) {
        if (this.dataSource.filteredData[i].status == "F" || this.dataSource.filteredData[i].status == "V") {
          this.disablebtnflag = true;
          this.disableFDflag = true;
          this.disableflag = true;
          this.showhidediv2 = false;
          this.showhidediv3 = false;
        }
        else if (this.dataSource.filteredData[i].status == "E") {
          this.disablebtnflag = false;
          this.disableFDflag = false;
          this.disableflag = false;
        }
        else {
          this.disablebtnflag = false;
          this.disableFDflag = false;
          this.disableflag = false;
        }
      }
      if (this.dataSource.filteredData.length > 0)
        this.isTableHasDataY = true;
      else
        this.isTableHasDataY = false;
    })

    this.stopincrement.getEmployeeForNotStop(ID).subscribe(result => {
      this.dataSourceDeAttach = new MatTableDataSource(result);
      this.dataDeAttach = result;
      this.dataSourceDeAttach.paginator = this.paginatorDetach;  //sortDetach
      this.dataSourceDeAttach.sort = this.sortDetach;
      if (this.dataSourceDeAttach.filteredData.length > 0)
        this.isTableHasDataX = true;
      else
        this.isTableHasDataX = false;
    })

    this.showhidediv1 = true;
    this.showhidediv2 = true;
    this.showhidediv3 = true;
  }
  // End


  // Save Increment Data
 
  insertStopIncrementData() {
    debugger;
    this.checktrue = false;
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      this.is_btnStatus = true;
      this.divbgcolor = "alert-danger"; 
      if (this.totalAttach == 0)
        this.Message = "There are no record found!";
      else
        this.Message = "Please attach record!";
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.IncrementData.push(d.empID);
          this.IncrementData.push(d.empCode);
          this.IncrementData.push(d.payLevel);
          this.IncrementData.push(d.wefDate);
          if (d.wefDate == null) {
            this.is_btnStatus = true;
            this.divbgcolor = "alert-danger"; 
            this.Message="Wef date should not be blank";
            this.checktrue = true;
            return false;
          }
          debugger;
          this.IncrementData.push(d.remark);
          this.IncrementData.push(d.oldBasic);
          this.IncrementData.push(this.username);
         // this.IncrementData.push(this.ArrddlDesign.msDesigMastID);
          this.IncrementData.push(d.orderNo);
          this.IncrementData.push(d.orderDate);
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });
      }
      if (this.checktrue == false) {
        this.stopincrement.insertStopIncrementData(myArray).subscribe(result => {
          this.Message = result;
          if (this.Message != undefined) {
            this.deletepopup = false;
            this.is_btnStatus = true;
            this.divbgcolor = "alert-success";
            this.Message = result;
          }
        });
      }
      this.bgcolor = "";
      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
    }
    this.IncrementData = [];
    this.selectionAttach.clear();
  }

  //End


  // Forward to checker

  forwardtocheckerFor() {
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      this.is_btnStatus = true;
      this.divbgcolor = "alert-danger"; 
      this.Message = "Please attach record!";
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.IncrementData.push(d.id);
          this.IncrementData.push(d.empID);
          this.IncrementData.push(this.username);
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });
      }
      this.stopincrement.forwardcheckerForStopInc(myArray).subscribe(result => {
        this.Message = result;
        if (this.Message != undefined) {
          this.deletepopup = false;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";
          this.Message = result;
        }

        this.bgcolor = "";
        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);
      });
    }
    this.IncrementData = [];
  }

  //End

  
  //Filter

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginatorAttach) {
      this.dataSource.paginatorAttach.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasDataY = true;
    else
      this.isTableHasDataY = false;
  }

  applyFilterDetach(filterValue: string) {
    this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceDeAttach.paginatorDetach) {
      this.dataSourceDeAttach.paginatorDetach.firstPage();
    }
    if (this.dataSourceDeAttach.filteredData.length > 0)
      this.isTableHasDataX = true;
    else
      this.isTableHasDataX = false;
  }

  //End

  clearData() {
    this.showhidediv0 = false;
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv3 = false;
    this.deletepopup = false;
    this.form.resetForm();
    this.bgcolor = "";
    this.btnCssClass = 'btn btn-success';
  }

  refreshData() {
    this.clearData();
    this.form.resetForm();
    this.dataSourceDeAttach = null;
  }
  reSetForm() {
    this.form.resetForm();
  }
  // Delete Order Details

  setDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }

  deleteIncOrderDetails(id) {
    this.regInc.OrderType = "Stop Increment";
    this.regincrement.deleteOrderDetails(id, this.regInc.OrderType).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;

        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger"; 
      }
      this.getEmpWithOrderNo();

      this.bgcolor = "";
      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
    })
  }
  // End

}
