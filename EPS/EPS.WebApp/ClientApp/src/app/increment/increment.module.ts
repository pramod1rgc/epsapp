import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IncrementRoutingModule } from './increment-routing.module';
import { RegularIncrementComponent } from '../increment/regular-increment/regular-increment.component';
import { AdvanceIncrementComponent } from '../increment/advance-increment/advance-increment.component';
import { StopIncrementComponent } from '../increment/stop-increment/stop-increment.component';

import { EmployeeDueIncrementComponent } from '../increment/employee-due-increment/employee-due-increment.component';
import { AnnualIncrementReportComponent } from '../increment/annual-increment-report/annual-increment-report.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
//import { SharecomponentModule } from '../shared-module/shared-module.module;
import { SharedModule } from '../shared-module/shared-module.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {RegIncrementComponent} from '../increment/reg-increment/reg-increment.component';


@NgModule({
  declarations: [RegularIncrementComponent, AdvanceIncrementComponent, StopIncrementComponent,
                EmployeeDueIncrementComponent, AnnualIncrementReportComponent,RegIncrementComponent],
  imports: [
    CommonModule,
    IncrementRoutingModule,
   // SharecomponentModule,
    SharedModule, MatTooltipModule,
    MaterialModule, 
    FormsModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule, HttpClientModule, 
  ]
})
export class IncrementModule { }
