import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualIncrementReportComponent } from './annual-increment-report.component';

describe('AnnualIncrementReportComponent', () => {
  let component: AnnualIncrementReportComponent;
  let fixture: ComponentFixture<AnnualIncrementReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualIncrementReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualIncrementReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
