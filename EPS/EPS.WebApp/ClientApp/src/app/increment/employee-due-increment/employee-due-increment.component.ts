import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatSelect } from '@angular/material';
import { AnnualincrementService } from '../../services/increment/annualincrement.service';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { DesignationModel } from '../../model/Shared/DDLCommon';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-employee-due-increment',
  templateUrl: './employee-due-increment.component.html',
  styleUrls: ['./employee-due-increment.component.css']
})
export class EmployeeDueIncrementComponent implements OnInit {
  PermDdoId: string;
  dataSource: any;
  msDesigMastID: any;
  @Output() onchange = new EventEmitter();
  @Output() desigchange = new EventEmitter();
  ArrddlDesign: DesignationModel;
  showPH: boolean;
  isTableHasData=false;
  constructor(private incrementService: AnnualincrementService) { }

  displayedColumn: string[] = ['serialNo', 'empName', 'empCode', 'desigCode', 'oldBasic', 'nextIncDate'];
  public designCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  Design: any[];
  private _onDestroy = new Subject<void>();
  @ViewChild('singleSelect') singleSelect: MatSelect;
  @ViewChild('f') form: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.showPH = false;
    this.bindDesignation(this.PermDdoId);
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }



  bindDesignation(value) {
    this.incrementService.getAllDesignation(this.PermDdoId).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }


  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }


  getEmpDueIncReport() {
    this.msDesigMastID = this.ArrddlDesign.msDesigMastID;
    this.incrementService.GetEmpDueForIncReport(this.msDesigMastID).subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.filteredData.length > 0)
        this.isTableHasData = true;
      else
        this.isTableHasData = false;
    })
    this.showPH = true;
    
  }


}
