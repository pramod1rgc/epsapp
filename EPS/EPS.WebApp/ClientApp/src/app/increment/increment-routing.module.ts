import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncrementModule } from './increment.module';
import { RegularIncrementComponent } from './regular-increment/regular-increment.component';
import { AdvanceIncrementComponent } from './advance-increment/advance-increment.component';
import { StopIncrementComponent } from './stop-increment/stop-increment.component';
import { EmployeeDueIncrementComponent } from './employee-due-increment/employee-due-increment.component';
import { AnnualIncrementReportComponent } from './annual-increment-report/annual-increment-report.component';
import { RegIncrementComponent } from '../increment/reg-increment/reg-increment.component';

const routes: Routes = [
  {
    path: '', component: IncrementModule, children: [
      { path: 'regincrement', component: RegularIncrementComponent },
      { path: 'advincrement', component: AdvanceIncrementComponent },
      { path: 'stopincrement', component: StopIncrementComponent },

      { path: 'empdueincrmnt', component: EmployeeDueIncrementComponent },
      { path: 'annualreport', component: AnnualIncrementReportComponent },
      { path: 'regTestIncrement', component: RegIncrementComponent },

    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncrementRoutingModule { }
