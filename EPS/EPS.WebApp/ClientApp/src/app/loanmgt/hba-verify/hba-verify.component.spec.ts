import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HbaVerifyComponent } from './hba-verify.component';

describe('HbaVerifyComponent', () => {
  let component: HbaVerifyComponent;
  let fixture: ComponentFixture<HbaVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HbaVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HbaVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
