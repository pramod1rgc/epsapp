import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChngLoanTypeComponent } from './chng-loan-type.component';

describe('ChngLoanTypeComponent', () => {
  let component: ChngLoanTypeComponent;
  let fixture: ComponentFixture<ChngLoanTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChngLoanTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChngLoanTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
