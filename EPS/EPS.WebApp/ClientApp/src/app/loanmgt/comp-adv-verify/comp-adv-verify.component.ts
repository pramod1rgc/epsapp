import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { newloanModel, employee } from '../../model/newloanModel';
import swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { MasterService } from '../../services/master/master.service';
import { EmpdetailsService } from '../../services/empdetails/empdetails.service';
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-comp-adv-verify',
  templateUrl: './comp-adv-verify.component.html',
  styleUrls: ['./comp-adv-verify.component.css'],
  providers: [CommonMsg]
})
export class CompAdvVerifyComponent implements OnInit {
  fileToupload: File = null;
  imageUrl: string = "/assets/uploadfile/";
  purchageFlag: boolean = false;
  enlargingFlag: boolean = false;
  constructionFlag: boolean = false;
  readybuiltFlag: boolean = false;
  compAdvFlag: boolean = false;
  disableflag: boolean = false;
  disableSaveFlage: boolean = false;
  disableCancelFlage: boolean = false;
  disableFwdCheckerFlag: boolean = false;
  disableRejectFlag: boolean = false;
  disableCheckerFlag: boolean = false;
  dtls_Enlarg_1: boolean;
  public dtls_Enlarg: any;
  public mode: number;
  asyncResult: any;
  group: boolean;
  Message: number = null;
  disbleflag = false;
  disbleflagdownload = false;
  disbledwnloadflag = false;
  public callTypevar: number = 2;
  msDesignID: string;
  empCode: string;
  public purposecode: string;
  arrddlLoanType: any;
  public AarempCode: employee;
  arrddlLoanStatus: any;
  public _loandetails: newloanModel;
  arrLoanPurpose: any;
  loantypeandpurposes: any;
  temploanCode: any;
  public designid: string;
  public empid: string;
  buttonText: string = 'Save';
  dataSource: any;
  public loanCode: string;
  public PurposeCode: string;
  public ddoid: number;
  public tempId: number;
  public priVerifFlag: number;
  deletePopup: boolean;
  @ViewChild('loandetailsForm') loanDetailsForm;
  @ViewChild('NewLaon') NewLaonForm: any;
  submitted = false;
  public temp: number;
  objBankDetails: any = {};
    remarks: string;
    UserID: number;
    userRole: string;
  userRoleID: number;
  labelmsg: string = 'Select Purpose Type';
  constructor(private master: MasterService, private objBankService: BankDetailsService, private empDetails: EmpdetailsService, private _msg: CommonMsg,
    private _Service: NewLoanService, private objCommanService: CommanService) {
    this._loandetails = new newloanModel();
  }
  displayedColumns: string[] = ['EMPId', 'empCode', 'loanCode', 'purposeCode', 'Status', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedIndex = 0;
  Bill: any[];
  Design: any[];
  EMP: any[];
  setDeletIDOnPopup: any;
  empVfyCode1: string[];
  checkautofill: string;
  public username: string;
  public empstatus: Boolean = false;
  public ArrEmpCode: employee;
  public flag: Boolean = false;
  @ViewChild('pdetails') form: any;
  @ViewChild('EmPDdetailsForm') EmPDdetailsForm: any;
  formatsDateTest: string[] = [
    'dd/MM/yyyy',
  ];
  dateNow: Date = new Date();
  dateNowISO = this.dateNow.toISOString();
  dateNowMilliseconds = this.dateNow.getTime();
  public loantypeFlag: string;
  ngOnInit() {
    debugger;
    this.empstatus = false;
    this.flag = false;
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    if (this.userRoleID == 17) {
      this.FindEmpCode(this.username);
    }
    else if (this.userRoleID == 8) {
      this.FindEmpCode(this.username);
    }
    else if (this.userRoleID == 9) {
      this.FindEmpCode(this.username);
    }
    this.BindLoanStatus(this.UserID);
    this.group = false;
    this.BindLoanType();
  
  }

  FindEmpCode(username) {
    this.objCommanService.GetEmpCode(username).subscribe(data => {
      this.ArrEmpCode = data[0];
      //if (this.ArrEmpCode.serviceTime < '5') {
      // swal("At least 5Yrs. continuous in govt service for apply loan");    
      //}
      //if (this.ArrEmpCode.serviceType != 'R') {
      //  swal("only Permanent employee apply for the loan");
      //}   
      if (this.ArrEmpCode.empcode != null) {
        this.getLoanDetails(this.ArrEmpCode.empcode);
      }
    });
  }

  GetcommanMethod(value) {
    this.getLoanDetails(value);
  }  
  BindLoanType() {
    this.loantypeFlag = '2';
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this.arrddlLoanType = data;
      console.log(this.arrddlLoanType);
    })
  }
  valuechange(value) {
    this._loandetails.intTotInst = Math.floor(((this._loandetails.loanAmtSanc) / value));
    this.temp = Math.floor((this._loandetails.priInstAmt) * this._loandetails.intTotInst)
    this._loandetails.oddInstAmtInt = Math.floor(((this._loandetails.loanAmtSanc) - this.temp));
    if (this._loandetails.oddInstAmtInt == 0) {
      this._loandetails.oddInstNoInt = 0;
    }
    else {
      this._loandetails.oddInstNoInt = 1;
    }
  }
  checkvaluecLoanAmout(value) {
    if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
      swal(this._msg.maxLimitLoanAmountMsg);
      //swal("Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower");
      this._loandetails.loanAmtSanc = null;
    }
    if (Math.floor(this._loandetails.loanAmtSanc) > 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
      swal(this._msg.maximumLimitLoanAmountMsg)   
      this._loandetails.loanAmtSanc = null;
    }

    if (Number(this._Service.PurposeCode) == 3 && Math.floor(this._loandetails.loanAmtSanc) > 180000) {
      swal(this._msg.maxLimitLoanAmount1LK80Msg);   
      this._loandetails.loanAmtSanc = null;
    }
    if (Number(this._Service.PurposeCode) == 2 && Math.floor(this._loandetails.loanAmtSanc) > 250000) {
      swal(this._msg.maxLimitLoanAmount2LK50Msg);   
      this._loandetails.loanAmtSanc = null;
    }
  }
  getLoanDetails(value) {
    this.empid = value;
    this.designid = "0";
    this.getEmployeeDeatils(this.empid, this.designid);
  }
  getEmployeeDeatils(empCode: string, msDesignID: string) {

    this._Service.EmployeeInfo(empCode, msDesignID).subscribe(data => {
      this._loandetails = data[0];
      this.getLoanpurposeSelectedPayloanCode(this._loandetails.payLoanRefLoanCD);
    });
  }
  getloantypeandpurposestatus(LoanBillID: string) {
    this._Service.Loantypeandpurposestatus(LoanBillID).subscribe(data => {
      this.loantypeandpurposes = data;
      console.log(this.loantypeandpurposes);
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 
  async uploadfile() {
    let formData = new FormData();
    formData.append(this.fileToupload.name, this.fileToupload);
    this.asyncResult = await this.master.uploadFile(formData).subscribe(async res => {
    });
  }
  resetLoanDdetailsForm() {
    this.NewLaonForm.resetForm();
  }
  getLoanpurposeSelectedPayloanCode(value) {
    debugger;
   
    if (value == "21") {
      this.labelmsg = 'Select Purpose Type';
    }
    if (value == "100") {
      this.labelmsg = 'Select No. of Computer Loan Sanctioned';
    }
    this._Service.LoanCode = value;
    this.temploanCode = value;

    this._Service.EMPLoanPurposeByLoanCode(value, this.ArrEmpCode.empcode).subscribe(data => {
      this.arrLoanPurpose = data;
      console.log(data);
    });
  }









  getvalues(value) {
    debugger;
    this._Service.PurposeCode = value;
    this.loanCode = this.temploanCode;
    this.PurposeCode = value;
  }
  BindLoanStatus(value) {
    debugger;
    this.mode = 2;
    this._Service.DdoId = value;     
    this._Service.BindLoanStatus(this.mode,value).subscribe(data => {
      this.arrddlLoanStatus = data;
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  ViewEditLoanDetailsById(id: any, flag: any) {
    debugger;
    this.objBankDetails.branchName = '';
    this.objBankDetails.bankName = '';
    this._loandetails.bankIFSCCODE = '';
  
    this._Service.EditLoanDetailsByEMPID(id).subscribe(data => {
      this._loandetails = data[0];
      console.log(data[0]);
      this.tempId = id;   
      this.buttonText = "Update";
      if (this._loandetails.priVerifFlag == 22) {
        this.disableSaveFlage = false;
        this.disableCancelFlage = false;
        this.disableFwdCheckerFlag = true;    
      }
      if (this._loandetails.priVerifFlag == 8) {
        this.disableSaveFlage = true;
        this.disableCancelFlage = true;
        this.disableCheckerFlag = true;
        this.disableRejectFlag = true;
      }
      if (this._loandetails.priVerifFlag == 24)
      {     
        this.disableSaveFlage = true;
        this.disableCancelFlage = true;
        this.disableCheckerFlag = true;
        this.disableRejectFlag = true;

      }
      this.getLoanpurposeSelectedPayloanCode(this._loandetails.payLoanRefLoanCD);
      this.getvalues(this._loandetails.payLoanPurposeCode);
      if (this._Service.PurposeCode == '4') {
        this.group = true;
      }
      if (this._Service.PurposeCode == '1') { this.group = true; }
      if (this._Service.PurposeCode == '2') { this.group = true; }
      if (this._Service.PurposeCode == '3') { this.group = true; }
      if (this._Service.LoanCode == '100') {
        if (this._loandetails.comp_Certi_Info_1 == true) { this.group = true; }
      }
      if (flag === 1) {
        this.disbleflag = true;
        this.disbledwnloadflag = false;
        this.disableSaveFlage = false;
        this.disableCancelFlage = false;
        this.disableFwdCheckerFlag = false;
        this.disableRejectFlag = false;

      }
      else {
        this.disbledwnloadflag = true;
        this.disbleflag = false;
        this.disableSaveFlage = true;
        this.disableRejectFlag = true;
      }

      if (this._loandetails.bankIFSCCODE != "" && this._loandetails.bankIFSCCODE != undefined) {

        this.objBankService.getBankDetailsByIFSC(this._loandetails.bankIFSCCODE).subscribe(res => {
          this.objBankDetails = res;
        })
      }
      else {

        this.objBankDetails.branchName = '';
        this.objBankDetails.bankName = '';
        this._loandetails.bankIFSCCODE = '';
      }


    })

  }
  commanfunction() {
    this.resetLoanDdetailsForm();
    if (this._Service.PurposeCode == '4') {
      this.readybuiltFlag = false;
    }
    if (this._Service.PurposeCode == '1') {

      this.purchageFlag = false;
    }
    if (this._Service.PurposeCode == '2') {
      this.constructionFlag = false;
    }
    if (this._Service.PurposeCode == '3') {
      this.enlargingFlag = false;
    }
    if (this._Service.LoanCode == '100') {
      this.compAdvFlag = false;
      if (this._loandetails.comp_Certi_Info_1 == true) {
        this.group = true;
      }
    }
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  //Cancel() {
  //  $(".dialog__close-btn").click();
  //}
  popupclose() {
    this.disableSaveFlage = true;
    this.disableCancelFlage = true;
    this.disableFwdCheckerFlag = false;
    this.purchageFlag = false;
    this.enlargingFlag = false;
    this.constructionFlag = false;
    this.readybuiltFlag = false;
    this.compAdvFlag = false;
  }
  deleteEmployeeById(id: any) {
    this.mode = 2;
    this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(data => {
      this.Message = data;
      if (this.Message != null) {
        this.deletePopup = false;
        swal(this._msg.deleteMsg);
        this.BindLoanStatus(this._Service.DdoId);
        this.resetLoanDdetailsForm();
      }
    });
  }
  handleFileinput(file: FileList) {
    this.fileToupload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToupload);
  }
  urls: any = [];
  onSelectFile(event) {
    debugger
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event) => {
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  DownloadFile(url) {
    this.empDetails.getdownloadDetails(url).subscribe(res => {
      saveAs(new Blob([res], { type: this._msg.formatfileMsg }), "");
    }
    )
  }
  btnsubmit() {
    debugger;
    this._loandetails.createdby = this.username;
    this._loandetails.permDdoId = this.ddoid;
    this._loandetails.msEmpId = this.tempId;
    this.mode = 2;

    if (this.userRoleID == 8 && this._loandetails.priVerifFlag==22) {
      this._loandetails.priVerifFlag = 23;
    }
    else {

     // this._loandetails.priVerifFlag = 31;
    }
    this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(data => {
      this.flag = true;
      if (this._loandetails.priVerifFlag == 22) {
        swal(this._msg.VerifyHOOChecker)
        //this.snackBar.open('Verified by HOO checker', null, { duration: 5000 });
      }

      else {

        swal(this._msg.VerifyHOOChecker)

        //this.snackBar.open('Verified by HOO checker', null, { duration: 5000 });

      }
      this.BindLoanStatus(this._Service.DdoId);
      this.commanfunction();
    });
  }

  fwdhoochecker()
  {
    if (this.tempId != null) {

      this.mode = 1;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.VerifyHOOChecker)

        //this.snackBar.open('HOO Checker Verify', null, { duration: 5000 });
        this.BindLoanStatus(this._Service.DdoId);
        this.commanfunction();
      });

    }

  }
  forwardhoochecker() {
    debugger;
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {    
      this.mode = 3;
      this.remarks = this._loandetails.remarks;
      if (this.userRoleID == 8) {
        this._loandetails.priVerifFlag = 31;
      }
     
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.Rejected)

       // this.snackBar.open('Rejected', null, { duration: 5000 });
        // $(".dialog__close-btn").click();
        this.deletePopup = false;
        this.BindLoanStatus(this._Service.DdoId);
        this.commanfunction();
      });
    }
  }
  getBankDetailsByIFSC(bankIFSCCODE: string) {
    this.objBankService.getBankDetailsByIFSC(bankIFSCCODE).subscribe(res => {
      this.objBankDetails = res;

    });
  }
}
