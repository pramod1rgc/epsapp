import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvBillProcComponent } from './adv-bill-proc.component';

describe('AdvBillProcComponent', () => {
  let component: AdvBillProcComponent;
  let fixture: ComponentFixture<AdvBillProcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvBillProcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvBillProcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
