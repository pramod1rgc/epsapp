import { NgModule } from '@angular/core';
import { LoanmgtRoutingModule } from './loanmgt-routing.module';
import { ExistingloanComponent } from './existingloan/existingloan.component';
import { NewloanappComponent } from './newloanapp/newloanapp.component';
import { SanctionComponent } from './sanction/sanction.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { FloodadvComponent } from './floodadv/floodadv.component';
import { InstRecovryComponent } from './inst-recovry/inst-recovry.component';
import { RecovryChallanComponent } from './recovry-challan/recovry-challan.component';
import { RecovyScheduleComponent } from './recovy-schedule/recovy-schedule.component';
import { PayPastPerdComponent } from './pay-past-perd/pay-past-perd.component';
import { ChngLoanTypeComponent } from './chng-loan-type/chng-loan-type.component';
import { ChngLastInstPaidComponent } from './chng-last-inst-paid/chng-last-inst-paid.component';
import { AdvBillProcComponent } from './adv-bill-proc/adv-bill-proc.component';
import { CompAdvVerifyComponent } from './comp-adv-verify/comp-adv-verify.component';
import { HbaVerifyComponent } from './hba-verify/hba-verify.component';
import { RelsEmpLvlComponent } from './rels-emp-lvl/rels-emp-lvl.component';
import { RelsHooLvlComponent } from './rels-hoo-lvl/rels-hoo-lvl.component';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';
//import { ReleaseComponent } from './release/release.component';
import { CommanMstComponent } from '../shared-module/comman-mst/comman-mst.component';
import { CommonModule } from '@angular/common';
  @NgModule({
  declarations: [ExistingloanComponent, NewloanappComponent, SanctionComponent, 
    FloodadvComponent, InstRecovryComponent, RecovryChallanComponent, RecovyScheduleComponent,
    PayPastPerdComponent, ChngLoanTypeComponent, ChngLastInstPaidComponent, AdvBillProcComponent,
    CompAdvVerifyComponent, HbaVerifyComponent, RelsEmpLvlComponent, RelsHooLvlComponent],
  imports: [
    CommonModule,
    LoanmgtRoutingModule, MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTooltipModule,
    NgxMatSelectSearchModule,
    SharedModule
  ],
    exports: [CommanMstComponent]
   
  })
export class LoanmgtModule { }
