import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingloanComponent } from './existingloan.component';

describe('ExistingloanComponent', () => {
  let component: ExistingloanComponent;
  let fixture: ComponentFixture<ExistingloanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingloanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingloanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
