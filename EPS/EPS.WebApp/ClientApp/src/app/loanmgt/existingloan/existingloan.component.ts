import { ExistLoanService } from '../../services/loan-mgmt/exist-loan.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { ExistloanModel } from '../../model/LoanModel/existloanModel';
import { employee } from '../../model/newloanModel';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-existingloan',
  templateUrl: './existingloan.component.html',
  styleUrls: ['./existingloan.component.css'],
  providers: [CommonMsg]
})


export class ExistingloanComponent implements OnInit {
  public tempId: number;
  public callTypevar: number = 1;
  public ArrLoanSection: ExistloanModel
  EmpCode: string;
  public temp: number;

  disbleflag = false;
  FlagSave: boolean = false;
  FlagCancel: boolean = false;
  FlagForwordChecker: boolean = false;
  FlageAccept: boolean = false;
  FlagReject: boolean = false;

  buttonText: string = 'Save'; 
  LoanCd: number;
  ArrddlLoanType: any;
  Arrddlscheme: any;
  PermDdoId: string;
  MsDesignID: string;
  IsDisabled: boolean = false;
  IsFieldDisabled: boolean = true;

  tempdata: any;
  ArrddlLoanStatus: any;
  public empstatus: Boolean;
  dataSource: any;
  public ArrEmpCode: employee;  
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
  comparisonError: string = '';
  Compvalue: boolean = false;
  CtrCode: any;
  msPayAdvEmpdetID: any;
  deletepopup: boolean;


  constructor(private _Service: ExistLoanService, private commonMsg: CommonMsg) {
    this.ArrLoanSection = new ExistloanModel();
   
  }
  displayedColumns: string[] = ['empCD', 'description', 'payLoanRefLoanShortDesc', 'empPersVerifFlag', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public username: string;
  selectedIndex = 0;
  Bill: any[];
  Design: any[];
  EMP: any[];
  empVfyCode1: string[];
  checkautofill: string;
  /** End searching. */
  public callType: string;
 // private _onDestroy = new Subject<void>();
  @ViewChild('ExitLoan') ExitLoanForm: any;

  ngOnInit() {
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.BindExistingLoanStatus(this.ddoid,this.UserID);
    this.BindLoanType();
    this.buttonText = "Save"
  }

  GetcommanMethod(value) {
    debugger;
    this.GetAleadytakenLoandetails(value);
  }
  changecalltype(value) {
    debugger;
    this.callTypevar = value;
  }


  GetAleadytakenLoandetails(value) {
    debugger;
    this.FlagSave = false;
    this.FlagCancel = false;
    this.FlagForwordChecker = false;
    this.FlageAccept = false;
    this.FlagReject = false;
    this.buttonText = "Save"
  
    sessionStorage.removeItem("MSPayAdvEmpdetID");
    sessionStorage.removeItem("EmployeeCd");
   
     sessionStorage.setItem('EmployeeCd', value);
    this.MsDesignID = '0';
    this._Service.LoanSection(this.ddoid, value).subscribe(data => {
      this.ArrLoanSection = data;
      console.log(data);
      this.tempId = this.ArrLoanSection.msPayAdvEmpdetID;
      sessionStorage.setItem('MSPayAdvEmpdetID', String(this.tempId));
      
      if (sessionStorage.userRoleID != 5) {
        if ((data.empCd == null || data.priVerifFlag == null || data == null)) {
          this.FlagSave = true;
          this.FlagCancel = true;
          this.FlagForwordChecker = false;
          this.IsDisabled = false;
        }
        if (data.priVerifFlag == 61 || data.priVerifFlag == 62) {
          this.FlagSave = false;
          this.FlagCancel = false;
          this.FlagForwordChecker = false;
          this.IsDisabled = true
        }
        if (data.priVerifFlag == 64 || data.priVerifFlag == 63 ) {
          this.FlagSave = false;
          this.FlagCancel = false;
          this.FlagForwordChecker = true;
          this.IsDisabled = true
        }
      }
      else {
        if (data.priVerifFlag == 61) {
          this.FlageAccept = true;
          this.FlagReject = true;
          this.FlagSave = false;
          this.FlagCancel = false;
          this.FlagForwordChecker = false;
          this.IsDisabled = true
        }
      }
    });
  }



  BindExistingLoanStatus(ddoid, UserID) {
   
    this.IsDisabled = false;
    this._Service.BindExistingLoanStatus(ddoid, UserID, this.userRoleID.toString()).subscribe(data => {
        this.ArrddlLoanStatus = data;
        if (this.ArrddlLoanStatus != null) {
          if (data.empPersVerifFlag == 'EE') {
            this.IsDisabled = true;
          }
        }
        this.tempdata = this.ArrddlLoanStatus.PayLoanRefLoanShortDesc;
        this.ArrddlLoanStatus.payLoanRefLoanCD = this.tempdata;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
   
  }



  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  

  CompareValue() {  
    this.Compvalue = false;
    this.comparisonError = '';
    if (this.ArrLoanSection.loanAmtDisbursed != null && this.ArrLoanSection.loanAmtSanc != null) {
      this.Compvalue = false;
      if (this.ArrLoanSection.loanAmtDisbursed > this.ArrLoanSection.loanAmtSanc) {       
        this.comparisonError = 'loanAmtDisbursed should less than sanction';
        this.Compvalue = true;      
      }
      else {
        this.Compvalue = false;
     
      }
    }
     
  }



  BindLoanType() {
    this._Service.BindLoanType().subscribe(data => {
      this.ArrddlLoanType = data;
    });
  }

  BindHeadAccount(value) {
    this._Service.BindHeadAccount(value).subscribe(data => {
      this.ArrLoanSection.payLoanRefLoanHeadACP = data.payLoanRefLoanHeadACP;
    });
  }



  save() {
    debugger;
    var SchemeCode = this.ArrLoanSection.payLoanRefLoanHeadACP;

    var EmployeeCode = sessionStorage.getItem('EmployeeCd');
  //  var EmployeeCode = Number(sessionStorage.getItem('EmployeeCd'));
    var LoanCd = sessionStorage.getItem('LoanCode');

    var buttonName = document.activeElement.getAttribute("Name");
    if (buttonName == 'ForwardtoDDOChecker') {
      this.forwordDDOChecker();
    }
    else {
    if (this.buttonText == 'Save') {
      this.ArrLoanSection.EmpCd = EmployeeCode;
      this.ArrLoanSection.mode = 1;
      this.ArrLoanSection.schemeId = SchemeCode;
      this.ArrLoanSection.loanCD = LoanCd;
      this.ArrLoanSection.ddoId = this.ddoid;
      this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
      if (EmployeeCode != "") {
        this._Service.PostData(this.ArrLoanSection).subscribe(data => {
          if (data != "0") {
            this.ExitLoanForm.resetForm();
            swal(this.commonMsg.saveMsg);
            this.BindExistingLoanStatus(this.ddoid, this.UserID);
            this.clear();
          }
          if (data == "-1") {
            swal(this.commonMsg.alreadyExistMsg);
          }
        });
      }
      else {
        swal(this.commonMsg.EmpCdSelectMsg);
      }
      EmployeeCode == "";
    }
   
      if (this.buttonText == 'Update') {
        debugger;
        this.ArrLoanSection.mode = 2;
        this.ArrLoanSection.EmpCd = EmployeeCode;
        this.ArrLoanSection.schemeId = SchemeCode;
        this.ArrLoanSection.loanCD = LoanCd;
        this.ArrLoanSection.ddoId = this.ddoid;
        this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));

        if (EmployeeCode != "") {
          this._Service.PostData(this.ArrLoanSection).subscribe(data => {
            if (data != "0") {
              this.ExitLoanForm.resetForm();
              swal(this.commonMsg.updateMsg);
              this.BindExistingLoanStatus(this.ddoid, this.UserID);
              this.clear();
            }
            if (data == "-1") {
              swal(this.commonMsg.alreadyExistMsg);
            }
          });
        }
      }
  }  
 }

  forwordDDOChecker() {
    debugger;
    this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd'); 
    this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
    this.ArrLoanSection.loanCD = sessionStorage.getItem('LoanCode');
    this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
    this.ArrLoanSection.mode = 1;
    this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(data => {
      if (data != "0") {
        this.ExitLoanForm.resetForm();
        swal(this.commonMsg.forwardCheckerMsg);
        this.BindExistingLoanStatus(this.ddoid, this.UserID);
        this.clear();
      }

    });   
  }
  accept() {
    this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd'); 
    this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
    this.ArrLoanSection.loanCD = sessionStorage.getItem('LoanCode');
    this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
    this.ArrLoanSection.mode = 2;
    this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(data => {
      if (data != "0") {
        this.ExitLoanForm.resetForm();
        swal(this.commonMsg.VerifyedByChecker);
        this.BindExistingLoanStatus(this.ddoid, this.UserID);
        this.clear();
      }
    });
  }

  Reject() {
    this.ArrLoanSection.EmpCd = sessionStorage.getItem('EmployeeCd'); 
    this.ArrLoanSection.schemeId = this.ArrLoanSection.payLoanRefLoanHeadACP;
    this.ArrLoanSection.LoanCD = sessionStorage.getItem('LoanCode');
    this.ArrLoanSection.msPayAdvEmpdetID = Number(sessionStorage.getItem('MSPayAdvEmpdetID'));
    this.ArrLoanSection.mode = 3;
    this._Service.ExistingLoanForwordToDDO(this.ArrLoanSection).subscribe(data => {
      if (data != "0") {
        this.ExitLoanForm.resetForm();
        swal(this.commonMsg.RejectedByChecker);
        this.BindExistingLoanStatus(this.ddoid, this.UserID);
        this.clear();
      }
    });
  }

  

  GetLoanCode(value) {
    sessionStorage.setItem('LoanCode', value);
    this.BindHeadAccount(value)
  }

   

  ViewLoanDetailsById(EmpCd: any, msPayAdvEmpdetID: any) {
    debugger;
    this._Service.GetExistLoanDetailsByID(msPayAdvEmpdetID).subscribe(data => {
      this.ArrLoanSection = data[0];
      var recovData = data[0].priVerifFlag;   
      if (recovData == "61") {
        this.FlagForwordChecker = false;
      }
      if (recovData == "63") {
        this.FlagForwordChecker = false;
        this.FlagSave = false;
      }
      
      if (recovData == "64" && this.userRoleID == 6) {
        this.FlagForwordChecker = false;
      }
      if (data != null || data != undefined) {
        this.FlagSave = false;
        this.FlagCancel = false;
        this.IsDisabled = true;
      }
      else {
        this.FlagForwordChecker = true;
      }
    })
  }


  EditLoanDetailsById(empCD: any, msPayAdvEmpdetID: any,LoanCd:any) {
    debugger;
    
    sessionStorage.removeItem("MSPayAdvEmpdetID");
    sessionStorage.removeItem("EmployeeCd");
    sessionStorage.removeItem("LoanCode");

    sessionStorage.setItem('MSPayAdvEmpdetID', msPayAdvEmpdetID);
    sessionStorage.setItem('EmployeeCd', empCD);
    sessionStorage.setItem('LoanCode', LoanCd);

    this.buttonText = "Update";
    this._Service.GetExistLoanDetailsByID(msPayAdvEmpdetID).subscribe(data => {
      this.ArrLoanSection = data[0];        
      var recovData = data[0].priVerifFlag;
      if (data != null || data != undefined) {
        this.FlagSave = true;
        this.FlagCancel = false;
        this.IsDisabled = false;
      }   

      if (recovData == 62) {
        this.FlagForwordChecker = false;
      }
      if (this.ArrLoanSection.priVerifFlag == 61 && this.userRoleID==5) {
        this.FlagSave = false;
        this.FlageAccept = true;
        this.FlagReject = true;     
      }
      else {
        this.FlagForwordChecker = true;
      }
      this.BindLoanType();
    })
   
  }



  clear() {
    this.ExitLoanForm.resetForm();
    
  }

  cancel() {
    this.buttonText = "Save";
    this.ExitLoanForm.resetForm();
  }

 

  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }


  CalculateValue() {
    debugger;
    if (this.ArrLoanSection.loanAmtDisbursed != undefined && this.ArrLoanSection.priInstAmt != undefined) {

      var IsFloat = (this.ArrLoanSection.loanAmtDisbursed) % (this.ArrLoanSection.priInstAmt)
      if (IsFloat != 0) {
        this.ArrLoanSection.oddInstNoPri = 1;
      } else {
        this.ArrLoanSection.oddInstNoPri = 0;
      }

      this.ArrLoanSection.priTotInst = Math.floor(((this.ArrLoanSection.loanAmtDisbursed) / (this.ArrLoanSection.priInstAmt)));

      this.temp = Math.floor((this.ArrLoanSection.priInstAmt) * this.ArrLoanSection.priTotInst)

      this.ArrLoanSection.oddInstAmtPri = Math.floor(this.ArrLoanSection.loanAmtDisbursed - (this.temp));

      this.ArrLoanSection.totalInstallment = Math.floor((this.ArrLoanSection.oddInstNoPri) + (this.ArrLoanSection.priTotInst));
     
    }  
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  SetDeleteId(CtrCode, msPayAdvEmpdetID) {
    this.CtrCode = CtrCode;
    this.msPayAdvEmpdetID = msPayAdvEmpdetID;
  }

  DeleteDetails(CtrlCode:string , msPayAdvEmpdetID:string) {
    debugger;
    this._Service.DeleteEmpDetails(CtrlCode,msPayAdvEmpdetID,4).subscribe(data => {
      if (data != null) {
        this.BindExistingLoanStatus(this.ddoid, this.UserID);
        swal(this.commonMsg.deleteMsg);
        this.deletepopup = false;
      }
    });

  }
}
