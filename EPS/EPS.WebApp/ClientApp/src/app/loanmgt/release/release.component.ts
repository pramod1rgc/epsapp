import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { Router } from '@angular/router';
import { Sanction } from '../../model/LoanModel/sanction.model';
import swal from 'sweetalert2';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';
//import * as $ from 'jquery';
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { SanctionService } from '../../services/loan-mgmt/sanction.service';
@Component({
  selector: 'app-release',
  templateUrl: './release.component.html',
  styleUrls: ['./release.component.css']
})
export class ReleaseComponent implements OnInit {
  ArrSanction: any;
  releaseFlag: boolean;
  setDeletIDOnPopup: any;
  deletePopup: boolean;
  dataSource: any;
  buttonText: string = 'Save';
  public mode: number;
  ArrLoanType: any;
  EmpTypeFlag: boolean;
  @ViewChild('Sanction') SanctionForm: any;
  public _sanctiondetails: Sanction;
  objBankDetails: any = {};
  displayedColumns: string[] = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedIndex = 0;
  username: string;
  ArrEmpCode: any;
  empstatus: boolean;
  btndisable: boolean = true;
  disbleflag: boolean;
  show = false;
  type = "password";
  @ContentChild('showhideinput') input;
  public loantypeFlag: string;
  constructor(private _Service: NewLoanService, private router: Router, private objSanctionService: SanctionService, private objCommanService: CommanService, private snackBar: MatSnackBar, private objBankService: BankDetailsService) {
    this._sanctiondetails = new Sanction();
  }
  ngOnInit() {
    this.BindLoanType();
    
    this.username = sessionStorage.getItem('username');
    this.FindEmpCode(this.username);
  }
  FindEmpCode(username: string): any {
    debugger;
    this.objCommanService.GetEmpCode(username).subscribe(data => {
      this.ArrEmpCode = data[0];
      this._Service.DdoId = this.ArrEmpCode.ddoid;
      if (this._Service.DdoId != null || this._Service.DdoId != NaN) {
        this.BindGridLoanSanction(this._Service.DdoId);      
      }
      this._Service.RoleId = this.ArrEmpCode.roleid;
      if (this.ArrEmpCode.serviceType != 'R') {
        swal("only Permanent employee apply for the loan");
      }
      if (this._Service.RoleId == 8 || this._Service.RoleId == 9) {
        this.empstatus = true;
      }
      if (this.ArrEmpCode.empcode != null) {

        this.getLoanDetails(this.ArrEmpCode.empcode);
      }
    });
  }
  GetcommanMethod(value) {
    this.getLoanDetails(value);
  }
  BindLoanType() {
    this.loantypeFlag = '2';
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this.ArrLoanType = data;

    })
  }
  getLoanDetails(value) {
    debugger;
    this.getEmployeeDeatils(value, '0');
  }
  getEmployeeDeatils(EmpCode: string, MsDesignID: string) {
    this.mode = 1;
    this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(data => {
      this._sanctiondetails = data[0];
      if (this._sanctiondetails != undefined) {
        this.getBankDetailsByIFSC(this._sanctiondetails.ifscCD);
      }
      if (this._sanctiondetails == undefined) {
        swal("No Record Found..");
      }
      if (this._sanctiondetails.empApptType == 'R') {
        this.EmpTypeFlag = true;
      }
      else {
        this.EmpTypeFlag = false;
      }
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  btnsubmit() {
    if (this.buttonText == 'Save') {

      if (this._sanctiondetails.ConfirmbnkAcNo == this._sanctiondetails.bnkAcNo) {
        this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(data => {
          this._sanctiondetails = data;
          this._Service.HiddenId = data;
          if (data == "-1") {
            this.BindGridLoanSanction(this._Service.DdoId);
            swal('Already Exist Record.');
          }
          else {
            this.BindGridLoanSanction(this._Service.DdoId);
            swal('Save Successfully');
          }
        });
      }
      else {
        swal("Account Number not Matched Please try Again.");
        this._sanctiondetails.ConfirmbnkAcNo = '';
        this._sanctiondetails.bnkAcNo = '';
      }
    }
    if (this.buttonText == 'Update') {
      this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(data => {
        this._sanctiondetails = data;
        swal('Update Successfully');
        this.BindGridLoanSanction(this._Service.DdoId);
      });
    }
  }
  resetLoanDdetailsForm() {
    this.SanctionForm.resetForm();
  }
  BindGridLoanSanction(value) {
    this.mode = 3;
    this._Service.DdoId = value;
    this._Service.BindLoanStatus(this.mode, this._Service.DdoId).subscribe(data => {
      this.ArrSanction = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  ViewEditLoanDetailsById(id: any, flag: any) {
    debugger;
    this.mode = 2;
    this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(data => {
      this._sanctiondetails = data[0];
      this.buttonText = "Update";
      if (flag == 1) {
        this.disbleflag = true;
        this.btndisable = false;
      }
      if (flag == 0) {
        this.disbleflag = false;
        this.btndisable = true;
      }
    })
  }
  setempApptType(value) {
    debugger;
    if (value == "R") { this.EmpTypeFlag = false; }
    if (value == "T") { this.EmpTypeFlag = true; }
  }
  getBankDetailsByIFSC(ifscCD: string) {
    this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(res => {
      this.objBankDetails = res;
    });
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  deleteEmployeeById(id: any) {
    this.mode = 2;
    this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(data => {
      this._sanctiondetails = data;
      if (this._sanctiondetails != null) {
       // $(".dialog__close-btn").click();
        this.deletePopup = false;
        swal('Delete successfully.')
        this.BindGridLoanSanction(this._Service.DdoId);
        this.resetLoanDdetailsForm();
      }
    });
  }
  toggleShow() {
    debugger;
    this.show = !this.show;
    if (this.show) {
      this.type = "text";
    }
    else {
      this.type = "password";
    }
  }

}
