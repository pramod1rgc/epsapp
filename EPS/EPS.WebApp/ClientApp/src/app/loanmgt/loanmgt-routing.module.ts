import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExistingloanComponent } from './existingloan/existingloan.component';
import { NewloanappComponent } from './newloanapp/newloanapp.component';
import { SanctionComponent } from './sanction/sanction.component';
import { LoanmgtModule } from './loanmgt.module';
import { FloodadvComponent } from './floodadv/floodadv.component';
import { InstRecovryComponent } from './inst-recovry/inst-recovry.component';
import { RecovryChallanComponent } from './recovry-challan/recovry-challan.component';
import { RecovyScheduleComponent } from './recovy-schedule/recovy-schedule.component';
import { PayPastPerdComponent } from './pay-past-perd/pay-past-perd.component';
import { ChngLoanTypeComponent } from './chng-loan-type/chng-loan-type.component';
import { ChngLastInstPaidComponent } from './chng-last-inst-paid/chng-last-inst-paid.component';
import { AdvBillProcComponent } from './adv-bill-proc/adv-bill-proc.component';
import { CompAdvVerifyComponent } from './comp-adv-verify/comp-adv-verify.component';
import { HbaVerifyComponent } from './hba-verify/hba-verify.component';
import { RelsEmpLvlComponent } from './rels-emp-lvl/rels-emp-lvl.component';
import { RelsHooLvlComponent } from './rels-hoo-lvl/rels-hoo-lvl.component';

const routes: Routes = [
   {
    path: '', component: LoanmgtModule, data: {
      breadcrumb: 'Loan Management'
     }, children: [
      {
        path: ' ', component: LoanmgtModule, data: {
          breadcrumb: 'Loan Application'
        }
      },
      {
        path: 'loanapplication', component: NewloanappComponent, data: {
          breadcrumb: 'Loan Application Form'
        }
      },
      {
        path: 'comptadvncverfy', component: CompAdvVerifyComponent, data: {
          breadcrumb: 'Compt Adv Verify'
        }
      },
      {
        path: 'sanction', component: SanctionComponent, data: {
          breadcrumb: 'Sanction'
        }
      },
      {
        path: 'releasedetailshodlevel', component: RelsHooLvlComponent, data: {
          breadcrumb: 'Release Details'
        }
      },
    ]
  },
      { path: 'existingloan', component: ExistingloanComponent },
      { path: 'floodadv', component: FloodadvComponent },
      { path: 'instrecovy', component: InstRecovryComponent },
      { path: 'recovychallan', component: RecovryChallanComponent },
      { path: 'paypastperiod', component: PayPastPerdComponent },
      { path: 'recovyschedule', component: RecovyScheduleComponent },
      { path: 'changloantype', component: ChngLoanTypeComponent },
      { path: 'changelastinstpaid', component: ChngLastInstPaidComponent },
      { path: 'billprocess', component: AdvBillProcComponent },
      { path: 'hbaverify', component: HbaVerifyComponent },    
      { path: 'releasedetailsemplevel', component: RelsEmpLvlComponent },
      { path: 'releasedetailshodlevel', component: RelsHooLvlComponent }
     // { path: 'commanmaster', component: CommanMstComponent }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoanmgtRoutingModule { }
