import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecovryChallanComponent } from './recovry-challan.component';

describe('RecovryChallanComponent', () => {
  let component: RecovryChallanComponent;
  let fixture: ComponentFixture<RecovryChallanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecovryChallanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecovryChallanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
