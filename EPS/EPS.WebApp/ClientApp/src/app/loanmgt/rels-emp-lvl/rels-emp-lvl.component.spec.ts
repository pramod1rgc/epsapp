import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelsEmpLvlComponent } from './rels-emp-lvl.component';

describe('RelsEmpLvlComponent', () => {
  let component: RelsEmpLvlComponent;
  let fixture: ComponentFixture<RelsEmpLvlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelsEmpLvlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelsEmpLvlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
