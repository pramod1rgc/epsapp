import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommondesignModule } from './commondesign.module';
import { DesignComponent } from './design/design.component';
import { DemoComponent} from './demo/demo.component';

const routes: Routes = [
  {
    path: '', component: CommondesignModule, children: [
      { path: 'design', component: DesignComponent },
      { path: 'demo', component: DemoComponent }
     

    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommondesignRoutingModule { }
