import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommondesignRoutingModule } from './commondesign-routing.module';
import { DesignComponent } from './design/design.component';
import { DemoComponent } from './demo/demo.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [DesignComponent, DemoComponent],
  imports: [
    CommonModule,
    CommondesignRoutingModule,
    MaterialModule,
    
    FormsModule,
    ReactiveFormsModule, HttpClientModule,
    MatTooltipModule,
    NgxMatSelectSearchModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class CommondesignModule { }

