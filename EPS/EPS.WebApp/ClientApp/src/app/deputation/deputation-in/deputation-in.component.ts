import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { MatSnackBar} from '@angular/material';
import { Subject  } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { MasterService } from '../../services/master/master.service';
import { DeputationinService } from '../../services/deputation/deputationin.service';
import { Deputation_Deduction, designationCode, communicationCode} from '../../model/DeptationModel/depuationin-model';
import { FormArray } from '@angular/forms';
import { Subscription, ReplaySubject } from 'rxjs';
import { GetempcodeService } from '../../services/empdetails/getempcode.service';

import { CommonMsg } from '../../global/common-msg';
import { MyErrorStateMatcher } from '../../global/error-state-matcher';

@Component({
  selector: 'app-deputation-in',
  templateUrl: './deputation-in.component.html',
  styleUrls: ['./deputation-in.component.css'],
  providers: [CommonMsg]
})
export class DeputationInComponent implements OnInit {
  @Input()  empPageDeputation: any;
  subscription: Subscription;
  form: FormGroup;
  btnUpdateText: string
  //displayedColumns = ['id', 'userId', 'title','userIdss']
  displayedColumns = ['payItemsDeductionName', 'deduction_Schedule', 'payItemsDeductionAccCdScheme', 'communication_Address']
  displayedColumns2: string[] = ['srNo', 'deputeFrom', 'orderNo','Status', 'action'];
  submitted = false;
  deputationTypes: any = [];
  dataSource: any;
  serviceType: any = [];
  states: any = [];
  userName: string;
  deductioniinvalid: string
  roleId: any;
  dedutionItems: any = [];
  designation: any = [];
  deductionSchedule: any = ['Current Organitation PAO', 'Other PAO']
  savebuttonstatus: boolean;
  deputationCommAddress: any = [];
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  rejectionRemark: string = '';
  data: any;
  flag: boolean = false;
  eventsSubject: Subject<void> = new Subject<void>();
  disableflag: boolean = true;
 // dedutionExist: boolean = false;
  disableForwardflag: boolean = false;
  btndisabled: boolean = false;
  insertEditFlag: any;

  is_btnStatus: boolean;
  divbgcolor: string;
  Message: any;
  bgcolor: string;
  btnCssClass = 'btn btn-success';

  deputationDetail: Deputation_Deduction[];
 
  @ViewChild('formDirective') private formDirective;

  public designationCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesignation: Subject<designationCode[]> = new Subject<designationCode[]>();
  public communicationCtrl: FormControl = new FormControl();
  public communicationFilterCtrl: FormControl = new FormControl();
  public filteredCommunication: ReplaySubject<communicationCode[]> = new ReplaySubject<communicationCode[]>(1);
    RejectPopupForm: FormGroup;
  constructor(private master: MasterService, private deputationInService: DeputationinService, private snackBar: MatSnackBar,
    private objEmpGetCodeService: GetempcodeService, private _formBuilder: FormBuilder, private _message: CommonMsg) {
      this.empPageDeputation = true;
      this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
        // tslint:disable-next-line: max-line-length
        commonEmpCode => {
          debugger;
          if (commonEmpCode.selectedIndex === 11) {
         // debugger
          

            this.GetcommanMethod(commonEmpCode.empcode);
            this.empPageDeputation = false;

           } });
    }
  // @ViewChild('f') forms: Form;
  /* ==================for Deputation Details---------------------------*/

  private _onDestroy = new Subject<void>();
  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    
    //registerForm: FormGroup;
    this.roleId = sessionStorage.getItem('userRoleID');
    this.btnUpdateText = 'Save';
    this.savebuttonstatus = true;
    this.FormDeatils();
    this.RejectPopupcreateForm();
    //this.setChangeValidate();
    this.form.get('controllerId').setValue(sessionStorage.getItem('controllerID'))
    RejectPopupForm: FormGroup;
    this.getDeputationType();
    this.getServiceType();
    this.getState();
    this.getDedutionItems();
    //debugger;
    //this.getAllDesignation(33);
    this.getAllDesignation(sessionStorage.getItem('controllerID'));
    this.getDepuatationComm_Address();
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });
    this.communicationFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterCommunication();
      });
    this.btndisabled = true;
    this.disableForwardflag = true;
    this.form.disable();
    this.flag = true;
  }
 
  public empCd: string;
  public createdBy: string;
  GetcommanMethod(value) {
    //debugger;
    if (value == "") {
      this.btndisabled = true;
      this.empCd = value;
      this.btnUpdateText = 'Save';
      this.deputationDetail = [];
      const mapFormGroupValue = this.deputationDetail.map(Deputation_Deduction.asFormGroup);
      this.form.setControl('deputation_Deduction', new FormArray(mapFormGroupValue));
      this.formDirective.resetForm();
      this.dataSource = null
      this.form.disable();
    }
    else {
      this.empCd = value;
      this.btnUpdateText = 'Save';
      this.formDirective.resetForm();

      this.getDeputaiondetails(this.empCd, this.roleId);

      this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
      this.form.get('controllerId').setValue(sessionStorage.getItem('controllerID'));
      this.form.get('empCd').setValue(this.empCd)
    }
  }
 
  FormDeatils() {
    this.form = this._formBuilder.group({
      'deputation_Type': [null, Validators.required],
      'deputation_Type_Name': [null],
      'service_Type': [null, Validators.required],

      'deputation_Order': [null, [
        Validators.required
      ]],
      'deputation_Order_Date': [null, Validators.required],
      'deputation_From_Office': [null, Validators.required],
      'deputation_State': [null, Validators.required],
      'deputation_Effective_Date': [null, Validators.required],
      'deputation_Repatration_Date': [null],
      'desig_Before_Deputation': [null, Validators.required],
      'desig_Before_Remark': [null],
      'desig_After_Deputation': [null, Validators.required],
      'empCd': [null, Validators.required],
      'empDeputDetailsId': [null],
      'flagUpdate': [null],
      'rejectionRemark': [null],
      'createdBy': [null],
      'controllerId': [null],
    
      deputation_Deduction: this._formBuilder.array([]),
      'validate': ''

    });
  }
  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
    });

  }
  getDeputaiondetails(Empcd: any, roleId: any) {
    this.deputationInService.getDeputaionDetails(Empcd, roleId).subscribe(result => {

      let myArrayResult = [];
      myArrayResult.push(result);
      this.deputationDetail = result.deputation_Deduction;
      const mapFormGroupValue = this.deputationDetail.map(Deputation_Deduction.asFormGroup);
      this.form.setControl('deputation_Deduction', new FormArray(mapFormGroupValue));
     // debugger;
      if (result.empDeputDetailsId == null) {
        //this.form.reset();
        this.dataSource = null
        this.form.enable();
        this.form.clearValidators;
        this.disableflag = true;
        this.disableForwardflag = true;
        this.infoScreen = false;
        this.flag = true;
        if (this.roleId == '5') {
          this.form.disable();
        }
        if (this.empCd == null || this.empCd == undefined || this.empCd == "") {
          this.btndisabled = true;
        }
        else {
        this.btndisabled = false;
        }
        this.form.get('empCd').setValue(this.empCd)
      }
      else {
    
        //if (mapFormGroupValue.length == 0) {
        //  this.dedutionExist = true;
        //}
        //else {
        //  this.dedutionExist = false;
        //}
        this.form.patchValue(result);
        this.dataSource = myArrayResult;
        this.form.disable();
        this.flag = false;
        this.btndisabled = true;
        this.infoScreen = true;
        if (result.veriFlag == 'E' || result.veriFlag == 'U') {
          this.disableForwardflag = false;
        }
        else {
          this.disableForwardflag = true;
        }
       
       
      }
      if (this.roleId == '5' && (result.veriFlag == 'V' || result.veriFlag == 'R')) {
        this.infoScreen = false;
      }
     
     
      this.updateValidation();

    })
  }
  btnEditClick(empDeputId) {
   
    this.formDirective.resetForm();
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.form.patchValue(value);
    this.form.enable();
    this.flag = true;
    if (value.veriFlag == 'N') {
      this.btnUpdateText = 'Save';
    }
    else {
      this.btnUpdateText = 'Update';
    }
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
     // debugger;
      this.btndisabled = false;
      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      this.btndisabled = false;
     // debugger;
      this.disableflag = true;
      if (value.veriFlag == 'N') {
        this.disableForwardflag = true;
      }
      else {
        this.disableForwardflag = true;
      }
      //this.Btntxt = 'Save';
    }
  }
  btnInfoClick(empDeputId) {
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.form.patchValue(value);
    this.form.disable();
    //let empStatus = value.veriFlag;
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      //debugger;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      //debugger;
      this.disableForwardflag = true;
      //this.Btntxt = 'Save';
    }
  }
  updateValidation() {
    //debugger;
    if (this.form.get('desig_Before_Deputation').value !== 99) {
      this.form.get('desig_Before_Remark').clearValidators();
      this.form.get('desig_Before_Remark').updateValueAndValidity();
      this.form.get('desig_Before_Remark').setValue("");
    }
  }

  getDeputationType() {
    this.master.getDeputationType().subscribe(res => {
     // debugger;
      this.deputationTypes = res;
    })
  }

  getServiceType() {
    this.master.getServiceType().subscribe(res => {
   
      this.serviceType = res;
    })
  }
  getState() {
    this.master.getState().subscribe(res => {
   
      this.states = res;
    })
  }
  getDedutionItems() {
    this.deputationInService.GetPayItems().subscribe(res => {
  
      this.dedutionItems = res;
    })
  }
  getAllDesignation(controllerId: any) {
    this.deputationInService.GetAllDesignation(controllerId).subscribe(res => {

     
      this.designation = res;

      this.designationCtrl.setValue(this.designation);
      this.filteredDesignation.next(this.designation);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });
  }
  getDepuatationComm_Address() {
    this.deputationInService.GetCommunicationAddress().subscribe(res => {
   
      this.deputationCommAddress = res;
   
      this.communicationCtrl.setValue(this.deputationCommAddress);
      this.filteredCommunication.next(this.deputationCommAddress);
    });
    this.communicationFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterCommunication();
      });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  private filterDesignation() {

    if (!this.designation) {
      return;
    }
    // get the search keyword
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesignation.next(this.designation.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredDesignation.next(

      this.designation.filter(designation => designation.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterCommunication() {
    
    if (!this.deputationCommAddress) {
      return;
    }
    // get the search keyword
    let search = this.communicationFilterCtrl.value;
    if (!search) {
      this.filteredCommunication.next(this.deputationCommAddress.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredCommunication.next(

      this.deputationCommAddress.filter(deputationCommAddress => deputationCommAddress.pfAgency_Desc.toLowerCase().indexOf(search) > -1)
    );
  }
  resetForm() {
    //location.reload(true);
    this.form.reset();
    this.formDirective.resetForm();
    this.btnUpdateText = 'Save';
   
    this.disableForwardflag = true;
    this.btndisabled = true;
    this.dataSource = null;
    this.flag = true;
    this.form.disable();
    //if (this.empCd != null && this.empCd != undefined && this.empCd != "") {
    //  this.form.disable();
    //  this.getDeputaiondetails(this.empCd, this.roleId);
    //}
    this.eventsSubject.next(this.data);
  }
  get deputation_Deduction(): FormArray {
    return this.form.get('deputation_Deduction') as FormArray;
  }
 Checkdate()
 {
   debugger;
   
   if (this.form.get('deputation_Order_Date').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('deputation_Effective_Date').value || this.form.get('deputation_Order_Date').value > this.form.get('deputation_Effective_Date').value) {
    //this.form.get('deputation_Effective_Date').value
    this.form.get('deputation_Effective_Date').setValue("");
   }
   if (this.form.get('deputation_Effective_Date').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('deputation_Repatration_Date').value || this.form.get('deputation_Effective_Date').value > this.form.get('deputation_Repatration_Date').value) {
     //this.form.get('deputation_Effective_Date').value
     this.form.get('deputation_Repatration_Date').setValue("");
   }
//alert(dd)
}

  // On user change fill the deduction item scheme 
  async onUserChange(deputation_Deduction: FormGroup) {
    const deduc_Sche = deputation_Deduction.get('deduction_Schedule');
    
    const controlArray = <FormArray>this.form.get('deputation_Deduction');
    this.GetDeatisl(controlArray, deduc_Sche.value);
  }
  getvalue(deputation_Deduction: FormGroup) {
  
    const deduc_Sche = deputation_Deduction.get('deduction_Schedule');
    if (deduc_Sche.value == "") {

      deduc_Sche.setValue("");
      deduc_Sche.errors.required;
      this.deductioniinvalid = "in";
 
      
    }
  }
  async  GetDeatisl(controlArray, deductionSchedule) {
   
    for (let i = 0; i < controlArray.length; i++) {
      const msEmpDuesID = controlArray.controls[i].get('msEmpDuesId').value;
     
      console.log(i);
      if (deductionSchedule == "Other PAO") {
        await this.deputationInService.GetScheme_Code("0").subscribe(res => {

       
          controlArray.controls[i].get('deduction_Schedule').setValue(deductionSchedule);
          controlArray.controls[i].get('payItemsDeductionAccCdScheme').setValue(res.payItemsDeductionAccCdScheme);
          // this.deputationCommAddress = res;
        })
      }
      else {
        await this.deputationInService.GetScheme_Code(msEmpDuesID).subscribe(res => {
      

          controlArray.controls[i].get('deduction_Schedule').setValue(deductionSchedule);
          controlArray.controls[i].get('payItemsDeductionAccCdScheme').setValue(res.payItemsDeductionAccCdScheme);
          //DeduAccCdSche.markAsUntouched();

          // this.deputationCommAddress = res;
        })
      }
    }
  }
  //get f() { return this.form.controls; }
  getvalidateDeduction(deputation_Deduction: FormArray) {
   // debugger;
   
    for (let i = 0; i < deputation_Deduction.controls.length; i++) {
      //debugger;
      const deductionSchedule = deputation_Deduction.value[i].deduction_Schedule;
      const communicationAddress = deputation_Deduction.value[i].communication_Address;
      if (deductionSchedule == "" && communicationAddress == 0) {

        alert("Select deduction schedule And communication address");
        return false;
      }
      if (deductionSchedule == "" && communicationAddress != 0) {

        alert("Select deduction schedule");
        return false;
      }
      if (deductionSchedule != "" && communicationAddress == 0) {

        alert("Select communication address");
        return false;
      }
      //const msEmpDuesID = deputation_Deduction.controls[i].get('msEmpDuesId').value;
    }

  }
  onSubmit() {
    //debugger;
    this.submitted = true;

    this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
    // stop here if form is invalid
    var deductionvalidation = this.getvalidateDeduction(this.deputation_Deduction);
  
    if (this.form.invalid) {
      return false;
    }
    else {
      if (deductionvalidation == false) {
        return false;
      }
      else {
        this.deputationInService.InsertUpateDeputationDetails(this.form.value).subscribe(result1 => {
          if (parseInt(result1) >= 1) {
            if (this.btnUpdateText == 'Update') {
              this.is_btnStatus = true;
              this.divbgcolor = "alert-success";
              this.Message = this._message.updateMsg;

              this.form.reset();
              this.formDirective.resetForm();
              this.getDeputaiondetails(this.empCd, this.roleId);
              this.btnUpdateText = 'Save';
            }
            else {
              this.Message=this._message.saveMsg;
              this.form.reset();
              this.formDirective.resetForm();
              this.getDeputaiondetails(this.empCd, this.roleId);
              this.btnUpdateText = 'Save';
            }

          } else {
            if (this.btnUpdateText == 'Update') {
              this.Message=this._message.updateFailedMsg;
            }
            else {
              this.Message=this._message.saveFailedMsg;
            }
          }

          setTimeout(() => {
            this.is_btnStatus = false;
            this.Message = '';
          }, 8000);


        })
      }
    }
  }


  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = '';
    this.DeletePopup = false;
  }
  forwardStatusUpdate(statusFlag: any) {
    debugger;
   
    var RejectionComment = '';
    if (this.RejectPopupForm.invalid && statusFlag == 'R') {

      this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
      return false;
    }
    else
    {
      let empDeputDetailsId = this.form.get('empDeputDetailsId').value;
      if (statusFlag == 'R') {
        RejectionComment = this.rejectionRemark;
      }
      else {
        RejectionComment = ''
      }

      this.deputationInService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(result1 => {
        if (parseInt(result1) >= 1) {

          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";

          if (statusFlag == 0) {
            this.Message=this._message.forwardDDOCheckerMsg;
          }
          if (statusFlag == 'V') {
            this.Message=this._message.VerifyedByChecker;
          }
          if (statusFlag == 'R') {
            this.Message=this._message.RejectedByChecker;
          }
          this.form.reset();
          this.formDirective.resetForm();
          this.btnUpdateText = 'Save';
          this.getDeputaiondetails(this.empCd, this.roleId);
          this.infoScreen = false;
          this.ApprovePopup = false;
          this.RejectPopup = false;
          this.rejectionRemark = '';
        }

        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);

      });
    }
  }
}


