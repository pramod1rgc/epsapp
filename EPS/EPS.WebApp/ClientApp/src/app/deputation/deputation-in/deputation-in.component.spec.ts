import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeputationInComponent } from './deputation-in.component';

describe('DeputationInComponent', () => {
  let component: DeputationInComponent;
  let fixture: ComponentFixture<DeputationInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeputationInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeputationInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
