import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeputationInComponent } from './deputation-in/deputation-in.component';
import { DeputationOutComponent } from './deputation-out/deputation-out.component';
import { RepatriationDetailsComponent } from './repatriation-details/repatriation-details.component';
import { DeputationModule } from './deputation.module';
//import { CommanMstComponent } from '../loanmgt/comman-mst/comman-mst.component';
const routes: Routes = [
  {

    path: '', component: DeputationModule, children: [
      {
        path: 'deputationin', component: DeputationInComponent,


      }, {
        path: 'deputationout', component: DeputationOutComponent,
      },
      {
        path: 'repatriationdetails', component: RepatriationDetailsComponent,
      },
     
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeputationRoutingModule { }
