import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepatriationDetailsComponent } from './repatriation-details.component';

describe('RepatriationDetailsComponent', () => {
  let component: RepatriationDetailsComponent;
  let fixture: ComponentFixture<RepatriationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepatriationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepatriationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
