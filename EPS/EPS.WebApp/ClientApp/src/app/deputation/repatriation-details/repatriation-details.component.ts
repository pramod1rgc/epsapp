import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { MatSnackBar } from '@angular/material';
import { MasterService } from '../../services/master/master.service';
import { RepatriationdetailsService } from '../../services/deputation/repatriationdetails.service';
import { DeputationinService } from '../../services/deputation/deputationin.service';
import { CommonMsg } from '../../global/common-msg';
import { Subject } from 'rxjs/Subject';
import { MyErrorStateMatcher } from '../../global/error-state-matcher';
import swal from 'sweetalert2';
@Component({
  selector: 'app-repatriation-details',
  templateUrl: './repatriation-details.component.html',
  styleUrls: ['./repatriation-details.component.css'],
  providers: [CommonMsg]
})
export class RepatriationDetailsComponent implements OnInit {
  form: FormGroup;
  deputationTypes: any = [];
  serviceType: any = [];
  btnUpdatetext: string;
  savebuttonstatus: boolean;
  roleId: any;
  disableflag: boolean = true;
  disableForwardflag: boolean = true;
  data: any;
  depuInDate: any;
  eventsSubject: Subject<void> = new Subject<void>();
  dataSource: any;
  flag: boolean = false;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  btndisabled: boolean = false;
  rejectionRemark: string = '';
  displayedColumns2: string[] = ['srNo', 'deputeFrom', 'orderNo', 'Status', 'action'];
  @ViewChild('formDirective') private formDirective;
    RejectPopupForm: FormGroup;

  constructor(private _formBuilder: FormBuilder, private master: MasterService, private repatriationDetailsService: RepatriationdetailsService, private snackBar: MatSnackBar, private deputationinService: DeputationinService, private _message: CommonMsg) { }
  submitted = false;
  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.FormDeatils();
    this.RejectPopupcreateForm();
    this.btnUpdatetext = 'Save';
    this.roleId = sessionStorage.getItem('userRoleID');
    this.savebuttonstatus = true;
    this.getDeputationType();
    this.getServiceType();
    this.btndisabled = true;
    this.form.disable();
    this.flag = true;
  }
  public empCd: string;
  GetcommanMethod(value) {
    this.formDirective.resetForm();
    this.btnUpdatetext = 'Save';
    this.empCd = value;
    if (value == "") {
      this.btndisabled = true;
      this.dataSource = null
      this.form.disable();
    }
    else {
      this.getRepatriationDeputaionDetails(this.empCd, this.roleId);
      this.getCheckDuputationInEmployee(this.empCd, this.roleId);
    }

    //this.getLoanDetails(value);
   this.form.get('employeeCode').setValue(this.empCd)

  }
  getCheckDuputationInEmployee(Empcd: any, roleId: any)
  {

    debugger;
    this.repatriationDetailsService.getCheckDuputationInEmployee(Empcd, roleId).subscribe(result => {
      debugger;
      //if (result.length != 0) {
       
      if (result.deputationEffectiveDate == null) {
        this.formDirective.resetForm();
          this.btndisabled = true;
        
          this.form.get('employeeCode').setValue(this.empCd);
          this.dataSource = null
          this.form.disable();
          swal("Employee Is Not Eligible For Repatriation");
        }
        else {
          this.form.get('depuInDate').setValue(result.deputationEffectiveDate);
          this.depuInDate = result.deputationEffectiveDate;
        }
        
      //}

    })
   }

  getRepatriationDeputaionDetails(Empcd: any, roleId: any) {
    debugger;

    this.repatriationDetailsService.getRepatriationDeputaionDetails(Empcd, roleId).subscribe(result => {
      debugger;
      if (result.length == 0) {
        this.formDirective.resetForm();
        this.form.enable();
        this.dataSource = null;
        this.infoScreen = false;
        this.form.get('employeeCode').setValue(this.empCd);
        this.btndisabled = false;
        this.flag = true;
        if (this.roleId == '5') {
          this.form.disable();
        }
      }
      else {
        debugger;
        this.dataSource = result;
        this.form.patchValue(result[0]);
        this.form.disable()
        this.infoScreen = true;
        this.btndisabled = true;
        this.flag = false;
        if (result[0].veriFlag == 'E' || result[0].veriFlag == 'U') {
          this.disableForwardflag = false;
        }
        else {
          this.disableForwardflag = true;
        }
      }
      if (this.roleId == '5' && (result[0].veriFlag == 'V' || result[0].veriFlag == 'R')) {
        this.infoScreen = false;
      }
    })


  }
  Checkdate() {

    if (this.form.get('repatOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('repatEffecDate').value || this.form.get('repatOrderDate').value > this.form.get('repatEffecDate').value) {
      //this.form.get('deputation_Effective_Date').value
      this.form.get('repatEffecDate').setValue("");
    }
    if (this.form.get('repatOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('repatRelievingDate').value || this.form.get('repatOrderDate').value > this.form.get('repatRelievingDate').value) {
      //this.form.get('deputation_Effective_Date').value
      this.form.get('repatRelievingDate').setValue("");
    }
    //alert(dd)
  }
  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
    });

  }
  btnEditClick(empDeputId) {
    debugger;
    this.formDirective.resetForm();
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.form.patchValue(value);
    this.form.get('depuInDate').setValue(this.depuInDate);
    this.form.enable();
    this.flag = true;
    if (value.veriFlag == 'N') {
      this.btnUpdatetext = 'Save';
    }
    else {
      this.btnUpdatetext = 'Update';
    }
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;

      this.disableflag = true;
      this.btndisabled = false;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.btndisabled = false;
      this.disableflag = true;
      if (value.veriFlag == 'N') {
        this.disableForwardflag = true;
      }
      else {
        this.disableForwardflag = true;
      }
      //this.Btntxt = 'Save';
    }
  }
  btnInfoClick(empDeputId) {
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.form.patchValue(value);
    this.form.disable();
    this.form.get('depuInDate').setValue(this.depuInDate);
    //let empStatus = value.veriFlag;
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableForwardflag = true;
      //this.Btntxt = 'Save';
    }
  }
  getDeputationType() {
    this.master.getDeputationType().subscribe(res => {
      debugger;
      this.deputationTypes = res;
    })
  }
  getServiceType() {
    this.master.getServiceType().subscribe(res => {
      debugger;
      this.serviceType = res;
    })
  }
  ResetForm() {
    debugger;
    this.eventsSubject.next(this.data);
    this.form.reset();
    this.formDirective.resetForm();
    this.btnUpdatetext = 'Save';
    this.disableForwardflag = true;
    this.btndisabled = true;
    this.dataSource = null;

    this.form.disable();
    this.flag = true;
   // this.formDirective.resetForm();
   //// this.form.enable();
   // this.btnUpdatetext = 'Save';
   // this.disableForwardflag = true;
   // this.form.get('employeeCode').setValue(this.empCd.trim());
   // let value = this.dataSource[0];
   // if (this.dataSource[0].veriFlag == 'E' || this.dataSource[0].veriFlag == 'U') {
   //   this.disableForwardflag = false;
   // }
   // else {
   //   this.disableForwardflag = true;
   // }
   // this.form.patchValue(value);
   // this.form.disable();
   
  }
  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.rejectionRemark = '';
    
    this.DeletePopup = false;
  }
  FormDeatils() {
    this.form = this._formBuilder.group({
      'repatDeputedTypeId': [null, Validators.required],
      'repatDepuTypeName': [null],
      'repatServiceTypeId': [null, Validators.required],
      'repatOrderNo': [null, Validators.required],
      'repatOrderDate': [null, Validators.required],
      'repatEffecDate': [null, Validators.required],
      'repatRelievingDate': [null, Validators.required],
      'repatRemarks': [null],
      'employeeCode': [null, Validators.required],
      'empDeputDetailsId': [null],
      'flagUpdate': [null],
      'rejectionRemark': [null],
      'depuInDate': [null],


    });
  }
  forwardStatusUpdate(statusFlag: any) {

    var RejectionComment = '';
    if (this.RejectPopupForm.invalid && statusFlag == 'R') {

      this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
      return false;
    }
    else {
      let empDeputDetailsId = this.form.get('empDeputDetailsId').value;
      if (statusFlag == 'R') {
        RejectionComment = this.rejectionRemark;
      }
      else {
        RejectionComment = ''
      }
      debugger;
      this.deputationinService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(result1 => {
        if (parseInt(result1) >= 1) {
          if (statusFlag == 0) {
            this.snackBar.open(this._message.forwardDDOCheckerMsg, null, { duration: 4000 });
          }
          if (statusFlag == 'V') {
            this.snackBar.open(this._message.VerifyedByChecker, null, { duration: 4000 });
          }
          if (statusFlag == 'R') {
            this.snackBar.open(this._message.RejectedByChecker, null, { duration: 4000 });
          }
          this.form.reset();
          this.formDirective.resetForm();
          this.getRepatriationDeputaionDetails(this.empCd, this.roleId);
          this.btnUpdatetext = 'Save';
          this.infoScreen = false;
          this.ApprovePopup = false;
          this.RejectPopup = false;
          this.rejectionRemark = '';
        }
      });
    }
  }
  onSubmit() {
    debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    else {
      if (this.btnUpdatetext == 'Update') {
        this.form.get('flagUpdate').setValue('update');

      }
      else {
        this.form.get('flagUpdate').setValue('insert');
      }
      this.repatriationDetailsService.InsertUpateRepatriationDetails(this.form.value).subscribe(result1 => {

        let resultvalue = result1.split("-");

        if (parseInt(resultvalue[0]) >= 1) {
          if (this.btnUpdatetext == 'Update') {
            this.snackBar.open(this._message.updateMsg, null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getRepatriationDeputaionDetails(this.empCd, this.roleId);
            this.btnUpdatetext = 'Save';
            this.disableForwardflag = false;
          }
          else {
            this.snackBar.open(this._message.saveMsg, null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getRepatriationDeputaionDetails(this.empCd, this.roleId);
            this.btnUpdatetext == 'Save';
            this.disableForwardflag = true;
          }

        } else {

          if (this.btnUpdatetext == 'Update') {
            if (resultvalue[2] = 'Duplicate Record') {
              this.snackBar.open(this._message.depRepaAlreadyExits, null, { duration: 4000 });
            }
            else {
              this.snackBar.open(this._message.updateFailedMsg, null, { duration: 4000 });
            }

          }
          else if (resultvalue[2] = 'Duplicate Record') {
            this.snackBar.open(this._message.depRepaAlreadyExits, null, { duration: 4000 });
          }
          else {
            this.snackBar.open(this._message.saveFailedMsg, null, { duration: 4000 });
          }
        }
      })
    }
  }
}
