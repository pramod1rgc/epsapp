import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeputationRoutingModule } from './deputation-routing.module';
import { DeputationInComponent } from './deputation-in/deputation-in.component';
import { DeputationOutComponent } from './deputation-out/deputation-out.component';
import { RepatriationDetailsComponent } from './repatriation-details/repatriation-details.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoanmgtModule } from '../loanmgt/loanmgt.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';
@NgModule({
  declarations: [DeputationInComponent, DeputationOutComponent,  RepatriationDetailsComponent],
  imports: [
    CommonModule,
    DeputationRoutingModule,
    FormsModule, ReactiveFormsModule, MaterialModule, MatTooltipModule, NgxMatSelectSearchModule, LoanmgtModule, SharedModule
  ],
  exports:[DeputationInComponent]
})
export class DeputationModule { }
