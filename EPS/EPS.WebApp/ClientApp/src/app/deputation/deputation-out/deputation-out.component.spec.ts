import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeputationOutComponent } from './deputation-out.component';

describe('DeputationOutComponent', () => {
  let component: DeputationOutComponent;
  let fixture: ComponentFixture<DeputationOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeputationOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeputationOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
