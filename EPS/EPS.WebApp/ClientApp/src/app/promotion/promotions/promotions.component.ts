import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, Validators } from '@angular/forms';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { MatSnackBar, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.css']
})
export class PromotionsComponent implements OnInit {


  promotionsForm: FormGroup;
  //dataSource: any[] = [];
  allData: any[] = [];
  designations: any[] = [];
  objToBeDeleted: any;

  displayedColumns: string[] = ['S.No', 'PromotionType', 'OrderNo.', 'Designation', 'Status', 'Action'];


  tomorrow: Date = new Date();
  empCd: any;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  searchTerm: string = '';

  infoScreen: boolean = false;
  editScreen: boolean = false;
  transferDetailId: number;

  rejectionRemark: string = '';

  controllerId: string;
  roleId: string;
  ddoId: string;

  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  DeletePopup: boolean = false;
  ForwardToMakerPopup: boolean = false;

  constructor(private _service: PromotionsService, private snackBar: MatSnackBar, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.getAllDesignation();
  }

  createForm() {
    this.promotionsForm = new FormGroup({
      transferDetailId: new FormControl(0),
      promotionType: new FormControl('', Validators.required),
      orderNo: new FormControl('', [Validators.required, Validators.minLength(3)]),
      orderDt: new FormControl('', Validators.required),
      promotionWefDt: new FormControl('', Validators.required),
      joinOrderNo: new FormControl('', Validators.required),
      joinOrderDt: new FormControl('', Validators.required),
      joinedDt: new FormControl('', Validators.required),
      joinedAnBn: new FormControl('', Validators.required),
      toDesigCd: new FormControl('', Validators.required),
      remark: new FormControl('', Validators.required),
      rejectionRemark: new FormControl(''),
      trnGround: new FormControl(1),
      dueToCd: new FormControl("711"),
      permDdoId: new FormControl(""),
      toOfficeCd: new FormControl("00003000"),
      empCd: new FormControl(this.empCd),
      ddoId: new FormControl(this.ddoId),
      status: new FormControl('')
    });
    if (this.roleId == '5') {
      this.promotionsForm.disable();
    }
  }

  ngOnInit() {
    this.createForm();
  }

  getAllDesignation() {
    this._service.GetAllDesignation(this.controllerId).subscribe(result => {
      this.designations = result;
      this.designations.sort((a, b) => {
        if (a.desigDesc < b.desigDesc) return -1;
        else if (a.desigDesc > b.desigDesc) return 1;
        else return 0;
      });
    });
  }

  onSubmit() {
    if (this.promotionsForm.valid) {
      this._service.UpdatePromotionDetails(this.promotionsForm.value).subscribe(result => {
        let response = result as number;
        if (response) {
          if (response > 0) {
            this.promotionsForm.reset();
            //this.getEmpTransDetails(this.pageSize, this.pageNumber);
            this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
            this.formGroupDirective.resetForm();
            this.createForm();
            this.transferDetailId = null;
            this.editScreen = false;
            var btn = document.getElementById('headerButton');
            btn.click();
            this.empCd = null;
            this.getEmpTransDetails(10, 1);
          }
          if (response == -1) {
            this.snackBar.open("Order number already exist!", null, { duration: 4000 });
          }
        }

      });
      if (this.transferDetailId && this.promotionsForm.controls.status.value != 'E') {
        this.updateStatus("U");
      }
    }
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getEmpTransDetails(this.pageSize, this.pageNumber);
  }

  setJoiningDate() {
    this.promotionsForm.controls.joinedDt.setValue(this.promotionsForm.controls.promotionWefDt.value);
  }

  getEmpTransDetails(pageSize, pageNumber) {
    let employeeId = this.empCd;
    this.totalCount = 0;
    this.dataSource.data = []; // = new MatTableDataSource<any>();
    this._service.FetchEmployeeDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(result => {
      if (result && result != undefined && result.length > 0) {
        // this.dataSource = new MatTableDataSource(result);
        this.dataSource.data = result;
        //this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.allData = result;
        this.totalCount = result[0].totalCount;
      }
    });
  }

  fetchTableData(empCd: any) {
    this.empCd = empCd;
    this.createForm();
    this.promotionsForm.controls.empCd.setValue(this.empCd);
    this.getEmpTransDetails(this.pageSize, this.pageNumber);
    if (this.roleId != '5') {
      if (this.empCd) {
        this.promotionsForm.enable();
      }
      else {
        this.promotionsForm.disable();
      }
    }
  }

  applyFilter() {
    this.pageNumber = 1;
    this.getEmpTransDetails(this.pageSize, this.pageNumber);
    //this.dataSource.filter = value;
    //if (this.dataSource.paginator) {
    //  this.dataSource.paginator.firstPage();
    //}
  }


  btnEditClick(obj, isEdit) {
    if (isEdit) {
      this.transferDetailId = obj.transDetailId;
      this.promotionsForm.setValue({
        transferDetailId: obj.transDetailId,
        promotionType: obj.promotionType,
        orderNo: obj.orderNo,
        orderDt: obj.orderDt,
        promotionWefDt: obj.promotionWefDt,
        joinOrderNo: obj.joinOrderNo,
        joinOrderDt: obj.joinOrderDt,
        joinedDt: obj.joinedDt,
        joinedAnBn: obj.joinedAnBn,
        toDesigCd: obj.toDesigCd,
        remark: obj.remark,
        rejectionRemark: obj.rejectionRemark,
        trnGround: 1,
        dueToCd: "711",
        permDdoId: "",
        toOfficeCd: "00003000",
        empCd: this.empCd,
        ddoId: this.ddoId,
        status: obj.verifyFlag
      });
      this.promotionsForm.enable();
      this.infoScreen = false;
      this.editScreen = true;
    }
    else {
      this.transferDetailId = obj.transDetailId;
      this.promotionsForm.setValue({
        transferDetailId: obj.transDetailId,
        promotionType: obj.promotionType,
        orderNo: obj.orderNo,
        orderDt: obj.orderDt,
        promotionWefDt: obj.promotionWefDt,
        joinOrderNo: obj.joinOrderNo,
        joinOrderDt: obj.joinOrderDt,
        joinedDt: obj.joinedDt,
        joinedAnBn: obj.joinedAnBn,
        toDesigCd: obj.toDesigCd,
        remark: obj.remark,
        rejectionRemark: obj.rejectionRemark,
        trnGround: 1,
        dueToCd: "711",
        permDdoId: "",
        toOfficeCd: "00003000",
        empCd: this.empCd,
        ddoId: this.ddoId,
        status: obj.verifyFlag
      });
      this.promotionsForm.disable();
      this.infoScreen = true;
      this.editScreen = false;
    }
  }


  DeletePromotionDetails(obj) {
    this.objToBeDeleted = obj;
  }

  confirmDelete() {
    this._service.DeletePromotionDetails(this.objToBeDeleted.transDetailId).subscribe(result => {
      this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
      this.pageNumber = 1;
      this.paginator.firstPage();
      this.dataSource.paginator = this.paginator;
      this.getEmpTransDetails(this.pageSize, this.pageNumber);
      // $(".dialog__close-btn").click();
      this.DeletePopup = false;
      this.objToBeDeleted = null;
    });
  }

  cancelForm() {
    this.promotionsForm.reset();
    this.promotionsForm.enable();
    this.createForm();
    this.infoScreen = false;
    this.editScreen = false;
    this.formGroupDirective.resetForm();
  }

  forwardToChecker() {
    if (this.transferDetailId) {
      var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": "P", "PermDdoId": "", "DDOId": this.ddoId };
      this._service.ForwardTransferDetailsToChecker(forwardObj).subscribe(result => {
        this.snackBar.open(this._msg.forwardCheckerMsg, null, { duration: 4000 });
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        this.transferDetailId = null;
        this.promotionsForm.reset();
        this.createForm();
      });
    }
  }


  updateStatus(status: string) {
    if (this.transferDetailId) {
      var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": status, "PermDdoId": "", "RejectionRemark": this.rejectionRemark, "DDOId": this.ddoId };
      this._service.UpdateStatus(forwardObj).subscribe(result => {
        this.snackBar.open(this._msg.updateMsg, null, { duration: 4000 });
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        this.transferDetailId = null;
        this.promotionsForm.reset();
        this.createForm();
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
      });
    }
  }

}
