import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReversionsComponent } from './reversions.component';

describe('ReversionsComponent', () => {
  let component: ReversionsComponent;
  let fixture: ComponentFixture<ReversionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReversionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReversionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
