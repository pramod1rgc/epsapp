import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromotionModule } from './promotion.module';
import { PromotionsComponent } from '../promotion/promotions/promotions.component';
import { ReversionsComponent } from '../promotion/reversions/reversions.component'

const routes: Routes = [
  {
    path: '', component: PromotionModule, children: [
      {
        path: 'promotions', component: PromotionsComponent, data: {
          breadcrumb: 'Promotion Details'
        }
      },
      {
        path: 'reversionadhoc', component: ReversionsComponent, data: {
          breadcrumb: 'Reversion Details'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionRoutingModule { }
