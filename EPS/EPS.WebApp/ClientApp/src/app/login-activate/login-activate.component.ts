import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { LoginServices } from '../login/loginservice';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-login-activate',
  templateUrl: './login-activate.component.html',
  styleUrls: ['./login-activate.component.css'],
  providers: [LoginServices]
})
export class LoginActivateComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private _Service: LoginServices, private _snackBar: MatSnackBar) { }
  param1: string;
  param2: string;
  message: string;
  DeleteMenu: boolean;
  ngOnInit() {
    this.DeleteMenu = true;
    this.ActivateUser();
  }
  userroleActivate: any = [];
  genId: string;
  ActivateUser() {
    this.route.queryParams.subscribe(params => {
      this.genId = params['id'];
     
      if (this.genId != null) {
        this._Service.RoleActivation(this.genId).subscribe(data => {
          this.userroleActivate = data;
          this.message = this.userroleActivate
        });
      }
    });
  }
  redirectto() {
    this.router.navigate(['dashboard/login'])
  }
}
