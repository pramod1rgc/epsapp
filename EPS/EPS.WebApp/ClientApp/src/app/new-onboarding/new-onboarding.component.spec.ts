import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOnboardingComponent } from './new-onboarding.component';

describe('NewOnboardingComponent', () => {
  let component: NewOnboardingComponent;
  let fixture: ComponentFixture<NewOnboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOnboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
