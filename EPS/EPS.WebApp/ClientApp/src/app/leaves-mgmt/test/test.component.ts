import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar, MatCheckbox } from '@angular/material';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';
import { CommonLeavesSanctionComponent } from '../common-leaves-sanction/common-leaves-sanction.component';
//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
import swal from 'sweetalert2';
//import dayjs from 'dayjs';
import { DISABLED } from '@angular/forms/src/model';
import $ from 'jquery';
//import * as $ from 'jquery';
//import { forEach } from '@angular/router/src/utils/collection';
export interface PeriodicElement {
  CurtExtenOrderNo: string;
  CurtExtenOrderDate: string;
  Leavetype: string;
  FromDate: string;
  ToDate: string;
}
var ELEMENT_DATA: PeriodicElement[];

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  displayedCurtail: string[] = ['CurtExtenOrderNo', 'CurtExtenOrderDate', 'Leavetype', 'FromDate', 'ToDate', 'Action'];
  highlightedRows = [];
  dataSourceCurtail = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  //dataSourceCurtail = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  //displayedColumnCUrtExten: string[] = ['CurtExtenOrderNo', 'CurtExtenOrderDate', 'Leavetype', 'FromDate', 'ToDate', 'action'];
  leaveSanctionDetailsForm: FormGroup;
  leaveSanctionDetails: FormArray;
  leaveCurtailForm: FormGroup;
  ArrddlReason: ReasonForLeaveModel;
  ArrddlLeavesType: LeavesTypeModel[];
  showAddButton: boolean = false;
  showDeleteButton: boolean = false;
  showCheckBox0: boolean = false;
  radiochecked: boolean = true;
  leaveMainid: number;
  SanctionOrderNo: any;
  buttonText: string;
  employeeCode: string;
  PermDdoId: string = "00003";// sessionStorage.getItem('ddoid');
  firstdate: Date;
  lastdate: Date;
  datedif: number;
  showCancel: boolean = true;
  showForward: boolean = true;
  showSave: boolean = true;
  parentLeaveData: any;
  constructor(private _Service: LeavesMgmtService, private snackBar: MatSnackBar, private formBuilder: FormBuilder) { this.DetailsForm(); this.CurtailForm() }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('LeaveSanctionForm') form1: any;
  @ViewChild('LeaveCurtailForm') form2: any;
  ngOnInit() {
     //this.objlsmain = new LeavesSanctionMainModel();
    ////this.objlsmain.permDdoId = sessionStorage.getItem('ddoid');
    //this.objlsmain.permDdoId = "00003";
    //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
    this.BindDropDownLeavesReason();
    this.BindDropDownLeavesType();
    this.buttonText = "Save";
    this.leaveSanctionDetailsForm.get('orderNo').disable();
  }
  Test() {
     $(".box").animate({
      width: "toggle"
    });
  }
  DetailsForm() {

    this.leaveSanctionDetailsForm = this.formBuilder.group({
      empCd: [this.employeeCode],
      empName: [null],
      permDDOId: [this.PermDdoId],
      orderNo: [{ value: null, disabled: false }, Validators.required],
      orderDT: [{ value: null, disabled: false }, Validators.required],
      leaveCategory: [{ value: null, disabled: false }, Validators.required],
      leaveReasonCD: [null, Validators.required],
      remarks: [null, Validators.required],
      verifyFlg: [null],
      leaveReasonDesc: [null],
      leaveMainId: [0],
      leaveSanctionDetails: this.formBuilder.array([this.createItem()])
    });
  }
  createItem(): FormGroup {
    return this.formBuilder.group({
      leaveCd: [{ value: null, disabled: false }, Validators.required],
      fromDT: [{ value: null, disabled: false }, Validators.required],
      toDT: [{ value: null, disabled: false }, Validators.required],
      noDays: [{ value: null, disabled: false }, Validators.required],
      maxLeave: [{ value: null, disabled: false }, Validators.required],
      isMaxLeave: [{ value: false, disabled: false }]
    });
  }
  CurtailForm() {
    this.leaveCurtailForm = this.formBuilder.group({
      curtId: [0],
      curtEmpCd: [this.employeeCode],
      curtPermDdoId: [this.PermDdoId],
      preOrderNo: [this.SanctionOrderNo],
      preLeaveMainId: [this.leaveMainid],
      curtOrderNo: [null, Validators.required],
      curtOrderDT: [null, Validators.required],
      curtFromDT: [null, Validators.required],
      curtToDT: [null, Validators.required],
      isCurtleave: [null, Validators.required],
      curtReason: [null, Validators.required],
      curtRemarks: [null],
      curtverifyFlg: [null],

    });
  }
  BindDropDownLeavesReason() {
    this._Service.GetLeavesReason().subscribe(data => {
      this.ArrddlReason = data;
    });

  }
  BindDropDownLeavesType() {
    this._Service.GetLeavesType().subscribe(data => {
      this.ArrddlLeavesType = data;
    });
  }
  //Search Form
  designationChange() {   
    this.DetailsForm();
    this.showCancel = true;
    this.showForward = true;
    this.showSave = true;
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.buttonText = "Save";
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    this.leaveSanctionDetailsForm.get('orderNo').disable();
    //this.objlsmain = new LeavesSanctionMainModel();
    ////this.objlsmain.permDdoId = sessionStorage.getItem('ddoid');
    //this.objlsmain.permDdoId = "00003";
    ////this.objlsmain.empCd = value
    //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
    //this.parentLeaveData = null;

  }
  GetLeavesSanction(value) {
    this.employeeCode = value;
    this.DetailsForm();
    this.CurtailForm();
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.buttonText = "Save";
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    this.leaveSanctionDetailsForm.get('orderNo').enable();
    //this._Service.GetVerifiedLeavesSanction(this.PermDdoId, value).subscribe(result => {
     
     // this.parentLeaveData = result;
      //this.dataSource = new MatTableDataSource(result);      
      //this.dataSource.paginator = this.paginator;
      //this.dataSource.sort = this.sort;
    //}

    //this._Service.GetLeavesCurtailExten(this.PermDdoId, value).subscribe(result => {
      
    //  this.dataSourceCurtail = result;
    //  this.dataSourceCurtail = new MatTableDataSource(result);
    //  this.dataSourceCurtail.paginator = this.paginator;
    //  this.dataSourceCurtail.sort = this.sort;
    //})
  }
  get getleaveDetails() {
    return this.leaveSanctionDetailsForm.get('leaveSanctionDetails') as FormArray;
  }
  //operational Form

  LeaveTypeItemChange(i, value) {
    
    //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
    //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);

  }
  ChangeLeavesType(value, index) {
    
    //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
    //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
  }
  ChangeLeaveCategory(value) {
    //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
    //this.showDeleteButton = value == 'Single' ? false : true;
    //this.showAddButton = value == 'Single' ? false : true;
    //alert(this.findInvalidControls());
  }
  fillnoofDays(value, index) {
    

    //if (this.leaveSanctionDetailsForm.get('fromDT_' + index).value != "undefined" || this.leaveSanctionDetailsForm.get('toDT_' + index).value) {
    //  this.firstdate = new Date(this.leaveSanctionDetailsForm.get('fromDT_' + index).value);
    //  this.lastdate = new Date(this.leaveSanctionDetailsForm.get('toDT_' + index).value);
    //  if (this.firstdate > this.lastdate) {
    //    alert('From date should be less than to date');
    //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(0);
    //    value == "s" ? this.leaveSanctionDetailsForm.get('fromDT_' + index).setValue("") : this.leaveSanctionDetailsForm.get('toDT_' + index).setValue("");
    //  }
    //  else {
    //    this.datedif = Math.abs(this.firstdate.getTime() - this.lastdate.getTime());
    //    this.datedif = Math.ceil(this.datedif / (1000 * 3600 * 24)) + 1;
    //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(this.datedif);
    //  }

    //}
  }

  SaveLeavesCurtExten() {
    
    this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
    this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
    this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
    this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
    if (this.leaveCurtailForm.get('curtId').value == null) {
      this.leaveCurtailForm.get('curtId').setValue(0);
    }
    this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "I").subscribe(result => {
      this.snackBar.open('Save Successfully', null, { duration: 5000 });
      this.GetLeavesSanction(this.employeeCode);
    })
  }
  ForwardToChecker() {
    this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
    this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
    this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
    this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
    if (this.leaveCurtailForm.get('curtId').value == null) {
      this.leaveCurtailForm.get('curtId').setValue(0);
    }
    
    this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "F").subscribe(result => {
      this.snackBar.open('Save Successfully', null, { duration: 5000 });
      this.GetLeavesSanction(this.employeeCode);
    })
  }
  ResetLeavesSanction() {
    this.DetailsForm();
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.buttonText = "Save";
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");


  }

  //Grid operation
  ShowLeavesSanction(value) {
    
    this.leaveMainid = value.leaveMainId;
    this.SanctionOrderNo = value.orderNo;

    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.form2.resetForm();
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    //this.leaveCurtailForm.setControl('leaveSanctionDetails', new FormArray([]));
    //this.leaveCurtailForm.reset();
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.buttonText = "Save";
    this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
    this.showAddButton = value.leaveCategory == 'Single' ? false : true;
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
    this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
    for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
    }
  }
  EditCurtailLeaves(element) {
    

    //curtId: [0],
    //  curtEmpCd: [this.employeeCode],
    //    curtPermDdoId: [this.PermDdoId],
    //      preOrderNo: [this.SanctionOrderNo],
    //        preLeaveMainId: [this.leaveMainid],
    //          curtOrderNo: [null, Validators.required],
    //            curtOrderDT: [null, Validators.required],
    //              curtFromDT: [null, Validators.required],
    //                curtToDT: [null, Validators.required],
    //                  isCurtleave: [null, Validators.required],
    //                    curtReason: [null, Validators.required],
    //                      curtRemarks: [null],
    //                        curtverifyFlg: [null],

    this.leaveMainid = element.leaveMainId
    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));

    //this.leaveCurtailForm.reset();
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.buttonText = "Update";
    this.showDeleteButton = element.leaveCategory == 'Single' ? false : true;
    this.showAddButton = element.leaveCategory == 'Single' ? false : true;
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
    this.leaveCurtailForm.get('curtOrderNo').setValue(element.curtOrderNo);
    this.leaveCurtailForm.get('curtOrderDT').setValue(element.curtOrderDT);
    this.leaveCurtailForm.get('curtFromDT').setValue(element.curtFromDT);
    this.leaveCurtailForm.get('curtToDT').setValue(element.curtToDT);
    this.leaveCurtailForm.get('isCurtleave').setValue(element.isCurtleave);
    this.leaveCurtailForm.get('curtReason').setValue(element.curtReason);
    this.leaveCurtailForm.get('curtRemarks').setValue(element.curtRemarks);
    this.leaveCurtailForm.get('curtId').setValue(element.curtId);
    for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
    }


  }

  DeleteCurtailLeaves(value) {
    //this._Service.DeleteLeavesSanction(value).subscribe(result => {
    //  this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
    //  this.GetLeavesSanction(this.employeeCode);
    //})
  }
  checkValidation() {

    //if (this.leaveSanctionDetailsForm.get("leaveCategory").value == "Combined") {
    //  if (this.objlsmain.leaveSanctionDetails.length == 1) {
    //    alert('Please select single leave Type');
    //    return false;
    //  }
    //}

  }
  applyFilter(filterValue: string) {
    debugger;
    this.dataSourceCurtail.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceCurtail.paginator) {
      this.dataSourceCurtail.paginator.firstPage();
    }
  }
  abc() {
    alert(this.findInvalidControls());
  }
  //Extra methods
  public findInvalidControls() {
    
    const invalid = [];
    const controls = this.leaveSanctionDetailsForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

}
