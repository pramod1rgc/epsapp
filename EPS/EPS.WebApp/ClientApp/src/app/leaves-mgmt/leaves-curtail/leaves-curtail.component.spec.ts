import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesCurtailComponent } from './leaves-curtail.component';

describe('LeavesCurtailComponent', () => {
  let component: LeavesCurtailComponent;
  let fixture: ComponentFixture<LeavesCurtailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesCurtailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesCurtailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
