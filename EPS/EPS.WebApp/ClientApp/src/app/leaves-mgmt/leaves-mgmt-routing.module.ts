import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeavesMgmtModule } from './leaves-mgmt.module';
import {LeavesSanctionComponent} from '../leaves-mgmt/leaves-sanction/leaves-sanction.component';
import {LeavesCurtailComponent} from '../leaves-mgmt/leaves-curtail/leaves-curtail.component';
import { LeavesCancellComponent } from '../leaves-mgmt/leaves-cancell/leaves-cancell.component';
import { TestComponent } from '../leaves-mgmt/test/test.component';


const routes: Routes = [{
  path: '', component: LeavesMgmtModule, children: [
    {
      path: 'leavessanction', component: LeavesSanctionComponent, data: {
        breadcrumb: 'LeavesSanction'
      }  },
    {
      path: 'leavescurtail', component: LeavesCurtailComponent, data: {
        breadcrumb: 'LeavesCurtail'
      }  },
    {
      path: 'leavescancell', component: LeavesCancellComponent, data: {
        breadcrumb: 'LeavesCancell'
      }  },

         {
           path: 'leavestest', component: TestComponent, data: {
             breadcrumb: 'Leavestest'
      }
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeavesMgmtRoutingModule { }





