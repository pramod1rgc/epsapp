import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeavesMgmtRoutingModule } from './leaves-mgmt-routing.module';
import { LeavesSanctionComponent } from './leaves-sanction/leaves-sanction.component';
import { LeavesCurtailComponent } from './leaves-curtail/leaves-curtail.component';
import { LeavesCancellComponent } from './leaves-cancell/leaves-cancell.component';
import { TestComponent } from './test/test.component';

import { CommonLeavesSanctionComponent } from './common-leaves-sanction/common-leaves-sanction.component';
//import { MatTableModule} from "@angular/material";
import { DesigEmpComponent } from '../shared-module/desig-emp/desig-emp.component';
import { SharedModule } from '../shared-module/shared-module.module';
import {MaterialModule} from '../material.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';

//import { Ng2SearchPipeModule } from '@angular/core/npm_package.es6/';


@NgModule({
  declarations: [LeavesSanctionComponent, LeavesCurtailComponent, LeavesCancellComponent, TestComponent,
   CommonLeavesSanctionComponent
  ],
  imports: [
    CommonModule,
    LeavesMgmtRoutingModule,
    MaterialModule,
    SharedModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule
    //MatTableModule
  ]
})
export class LeavesMgmtModule {
  //app - desig - emp

}


