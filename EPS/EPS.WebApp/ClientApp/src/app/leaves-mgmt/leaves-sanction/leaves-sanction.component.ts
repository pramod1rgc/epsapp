import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { LeavesTypeModel, ReasonForLeaveModel } from '../../model/leavesmodel/leavessanctionmodel';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
export interface PeriodicElement {
  SanctionOrderNo: string;
  SanctionOrderDate: number;
  LeaveReason: number;
  LeaveCategory: string;
}
export interface PeriodicElement1 {
  SanctionOrderNo: string;
  SanctionOrderDate: number;
  LeaveReason: number;
  LeaveCategory: string;
}
var ELEMENT_DATA: PeriodicElement[];
var ELEMENT_DATA1: PeriodicElement1[];
@Component({
  selector: 'app-leaves-sanction',
  templateUrl: './leaves-sanction.component.html',
  styleUrls: ['./leaves-sanction.component.css'],
  providers: [DatePipe]
})
export class LeavesSanctionComponent implements OnInit {
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSourceforgrid: any[] = [];
  alldataSourceforgrid: any[];
  
  dataSourceHistory = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA1);
  dataSourceforhistorygrid: any[]=[];
  alldataSourceforhistorygrid: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('b') paginator1: MatPaginator;
  @ViewChild('a') sort1: MatSort;
  ShowHistory = false;
  key: string = 'name'; //set default
  reverse: boolean = false;
  //p: number = 1;
  currentdate=new Date();
  leaveSanctionCreationForm: FormGroup;
  leaveSanctionDetails: FormArray;
  ArrddlReason: ReasonForLeaveModel;
  ArrddlLeavesType: LeavesTypeModel[];
  showAddButton: boolean = false;
  showDeleteButton: boolean = false;
  leaveMainId: string;
  isVarified: boolean;
  isReasonEmpty: boolean;
  reasonText: string;
  totalLeave: string;
  deletepopup: boolean;
  hooCheckerLogin: boolean;
  cclType: boolean;
  cclFlag: boolean=false;
  showReturnReason: boolean = false;
  radiochecked: boolean = true;
  radioafterchecked: boolean = true;
  //buttonText: string;
  employeeCode: string;
  PermDdoId: string = sessionStorage.getItem('EmpPermDDOId');
  UserRoleId: string = sessionStorage.getItem('userRoleID');
  firstdate: string;
  lastdate: string;
  datedif: number;
  showCancel: boolean = true;
  showForward: boolean = true;
  showSave: boolean = true;
  leavecomb: number = 0;
  clsclrh: string;
  constructor(private datePipe: DatePipe,private _Service: LeavesMgmtService, private snackBar: MatSnackBar, private formBuilder: FormBuilder) { this.createForm() }

  @ViewChild('sanctionCreationForm') sanctionCreation: any;
  ngOnInit() {
    debugger;
    this.BindDropDownLeavesReason();
    this.BindDropDownLeavesType();
    this.leaveSanctionCreationForm.disable();
    this.isVarified = true;
    this.isReasonEmpty = false;
    this.ShowHistory = false;
  }

  //Search Form
  designationChange() {
    debugger;
    this.leaveSanctionCreationForm.disable();
    this.createForm();
    this.showCancel = true;
    this.showForward = true;
    this.showSave = true;
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.showReturnReason = false;
    this.leaveSanctionCreationForm.disable();
    this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionCreationForm.get('isRecovery').setValue("No");   
    this.dataSource = null;
    this.ShowHistory = false;

    this.dataSourceforgrid = null;
    this.dataSourceforhistorygrid = null;
  }
  sortk(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  GetLeavesSanction(value) {
    this.employeeCode = value;
    if (this.UserRoleId == "9") {
      this.leaveSanctionCreationForm.enable();

    }
    else {
      //this.leaveSanctionCreationForm.disable();
      //this.buttonText = "Verify";
    } 
    
    this.createForm();
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.showReturnReason = false;
    this.ShowHistory = false;
    
    
    this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionCreationForm.get('isRecovery').setValue("No");   
    //this.leaveSanctionCreationForm.get('orderNo').enable();
    this._Service.GetLeavesSanction(this.PermDdoId, value, this.UserRoleId).subscribe(result => {
      debugger;
      this.alldataSourceforgrid = result;     
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData(event);

      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
    this._Service.GetLeavesSanctionHistory(this.PermDdoId, value,this.UserRoleId).subscribe(result => {
      debugger;
      this.alldataSourceforhistorygrid = result;
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData1(event);

      this.dataSourceHistory = new MatTableDataSource(result);
      this.dataSourceHistory.paginator = this.paginator1;
      this.dataSourceHistory.sort = this.sort1;
    })
    
  }

  get getleaveDetails() {
    return this.leaveSanctionCreationForm.get('leaveSanctionDetails') as FormArray;
  }

  //operational Form
  createForm() {

    this.leaveSanctionCreationForm = this.formBuilder.group({
      empCd: [this.employeeCode],
      empName: [null],
      permDDOId: [this.PermDdoId],
      orderNo: [null, Validators.required],
      orderDT: [null, Validators.required],
      leaveCategory: [null, Validators.required],
      isRecovery: [null, Validators.required],
      leaveReasonCD: [null, Validators.required],
      returnReason:[null],
      remarks: [null],
      verifyFlg: [null],
      leaveReasonDesc: [null],
      leaveMainId: [0],
      leaveSanctionDetails: this.formBuilder.array([this.createItem()])
    });
  }
  createItem(): FormGroup {
    return this.formBuilder.group({
      leaveCd: [null, Validators.required],
      fromDT: [null, Validators.required],
      toDT: [null, Validators.required],
      payEntDT:[null],
      noDays: [null, Validators.required],
      maxLeave: [null, Validators.required],
      isMaxLeave: [false],
      isHalfDay: [false],
      isAfterNoon:[null]
    });
  }
  BindDropDownLeavesReason() {
    this._Service.GetLeavesReason().subscribe(data => {
      this.ArrddlReason = data;
    });

  }
  BindDropDownLeavesType() {
    debugger;
    this._Service.GetLeavesType().subscribe(data => {
      this.ArrddlLeavesType = data;
    });
  }
  LeaveTypeItemChange(i, value) {
    debugger;
    this.totalLeave = "";
    if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('leaveCd').value == 'CCL') {
      this._Service.GetLeavesTypeBalance(this.employeeCode).subscribe(result => {
        debugger;
        if (result.length == 0) {
          this.cclType = true;
        }
        else{
          this.cclType = false;
        }
        // if (result != 0) {  }

      })

    }
    //else { this.cclType = false; }

    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value);


  }
  ChangeLeaveCategory(value) {
debugger;
    this.showDeleteButton = value == 'Single' ? false : true;
    this.showAddButton = value == 'Single' ? false : true;

    if (value == 'Single') {
      while ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).controls.length - 1) {
        (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).removeAt(0);
      }
    }
  }
  IsAnyRecovery(value) { }
  ChangeTime(value) { 


  }
  ChangeDayChange()
  {


  }

  getPaginationData(event) {
    this.dataSourceforgrid = this.alldataSourceforgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  getPaginationData1(event) {
    this.dataSourceforhistorygrid = this.alldataSourceforhistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  group: any = [
    { id: true, name: 'HalfDay'},
    { id: false, name: 'FullDay'}
  ]
  fillnoofDays(value, index) {
    debugger;  
  if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('fromDT').value != null && (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('toDT').value != null) {
    debugger;
    this.firstdate = (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('fromDT').value;
      this.lastdate = (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('toDT').value
      if (this.firstdate > this.lastdate) {
        alert('From date should be less than to date');
        (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('noDays').setValue(0);
        value == "s" ? (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('fromDT').setValue("") : (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('toDT').setValue("");
      }
      else {
        this.datedif=(<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('toDT').value.diff((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('fromDT').value, 'days');
        (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(index).get('noDays').setValue(this.datedif+1);
      }

    }
  }
  AddRow() {
    debugger;
    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).push(this.createItem());
  }
  DeleteRow(value) {
    debugger;
    if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).controls.length==1) {
      alert('cant delete this row');
    }
    else {
(<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).removeAt(value);
}
    
  }
  SaveLeavesSanction() {
    debugger; 
      if (this.UserRoleId == "9") {
        this._Service.SaveLeavesSanction(this.leaveSanctionCreationForm.value, "I").subscribe(result => {
          debugger;
          if (parseInt(result) >= 1) {
            this.snackBar.open('Saved Successfully', null, { duration: 4000 });
            this.leaveSanctionCreationForm.reset();
            this.sanctionCreation.resetForm();
            this.GetLeavesSanction(this.employeeCode);
          } else {
            this.snackBar.open('Save not Successfull', null, { duration: 4000 });
          }
        })
      }
      else if (this.UserRoleId == "8")
      {
        
      }
  }
  VerifiedSanc(flag: boolean) {
    if (flag) {
      this._Service.VerifyReturnSanction(this.leaveSanctionCreationForm.get('leaveMainId').value, "V", "Sanct",null).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Verified Successfully', null, { duration: 4000 });
          this.leaveSanctionCreationForm.reset();
          this.sanctionCreation.resetForm();
          this.GetLeavesSanction(this.employeeCode);
        } else {
          this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
        }
      })
      //$('.dialog__close-btn').click();
      this.hooCheckerLogin = false;
    } else {
      this.isVarified = true;
    }

  }


  RejectedSanc(flag: boolean) {
    debugger;
    if (flag) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        return false;
      }
      this._Service.VerifyReturnSanction(this.leaveSanctionCreationForm.get('leaveMainId').value, "R", "Sanct", this.reasonText).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Returned Successfully', null, { duration: 4000 });
          this.leaveSanctionCreationForm.reset();
          this.sanctionCreation.resetForm();
          this.GetLeavesSanction(this.employeeCode);
          this.reasonText = null;
          this.hooCheckerLogin = false;
          //$('.dialog__close-btn').click();
        } else {
          this.snackBar.open('Return not Successfull', null, { duration: 4000 });
        }
      })
    }
    else {
      this.isVarified = false;
      return false;
    }
    this.reasonText = "";
  }

  saveTotalLeave() {
    debugger;
    //if (flag) {
      this._Service.SaveTotalLeaveAvailed(this.employeeCode,this.totalLeave).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Saved Successfully', null, { duration: 4000 });
          //this.leaveSanctionCreationForm.reset();
          //this.sanctionCreation.resetForm();
          //this.GetLeavesSanction(this.employeeCode);
        } else {
          this.snackBar.open('Save not Successfull', null, { duration: 4000 });
        }
      })
      //$('.dialog__close-btn').click();
      this.cclType = false;
    //} else {
    //  this.isVarified = true;
    //}

  }

  //DeleteSuspensionDetails(value) {
  //    this._Service.DeleteLeavesSanction(value, 'Sanc').subscribe((result: any) => {
  //      this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
  //      this.GetLeavesSanction(this.employeeCode);
  //    });
  //  $('.dialog__close-btn').click();

  //}
  ForwardToChecker() {
    debugger;
    this.leavecomb = 0;
    this.clsclrh = "";

    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).controls.forEach((control, index) => {
      if (control.get('leaveCd').value == 'CL') {
        this.leavecomb = this.leavecomb + 1;
        this.clsclrh = this.clsclrh + "CL"+"*" ;
      }
      else if (control.get('leaveCd').value == 'EL') {
        this.leavecomb = this.leavecomb + 1;
      }
      else if (control.get('leaveCd').value == 'RH') {
        this.clsclrh = this.clsclrh + "RH"+"*" ;
      }
      else if (control.get('leaveCd').value == 'SCL') {
        this.clsclrh = this.clsclrh + "SCL" + "*";
      }
    });
    if(this.clsclrh.split("*").includes("CL"))
    {
      if (!((this.clsclrh.split("*").includes("RH")) || (this.clsclrh.split("*").includes("SCL"))))
      {
        alert('Please check the leave type.CL can only be clubbed with Special CL and RH');
        return false;
      }

    }
    if (this.leavecomb >= 2) {
      alert('Please check the leave type.CL cant be combined with EL');
      return false;
    }

    //if user selected combined type leave but fill only one leave type 
    if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Combined") {
      if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).length == 1) {
        alert('Please select single leave Type');
        return false;
      }
    }
    else if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Single") {
      if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).length > 1) {
        alert('Please select combined leave Type');
        return false;
      }
    }


      this._Service.SaveLeavesSanction(this.leaveSanctionCreationForm.value, "F").subscribe(result => {
        if (parseInt(result) >= 1) {
          this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
          this.leaveSanctionCreationForm.reset();
          this.sanctionCreation.resetForm();
          this.GetLeavesSanction(this.employeeCode);
        } else {
          this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
        }
      })

  }
  ReturnSanction() { }

  ResetLeavesSanction() {
    this.leaveSanctionCreationForm.reset();
    this.sanctionCreation.resetForm();
    //this.createForm();
    this.showDeleteButton = false;
    this.showAddButton = false;
    this.showReturnReason = false;
    this.leaveSanctionCreationForm.get('leaveMainId').setValue(0);
    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(0).get('isMaxLeave').setValue(false);
    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(0).get('isHalfDay').setValue(false);
    this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionCreationForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionCreationForm.get('isRecovery').setValue("No");   
  }

  EditLeavesSanction(value) {
    debugger;
    this.leaveSanctionCreationForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.radioafterchecked = false;
    
    this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
    this.showAddButton = value.leaveCategory == 'Single' ? false : true;
    this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionCreationForm.get('leaveMainId').setValue(value.leaveMainId);
    this.leaveSanctionCreationForm.get('orderDT').setValue(value.orderDT);
    this.leaveSanctionCreationForm.get('orderNo').setValue(value.orderNo);
    this.leaveSanctionCreationForm.get('leaveCategory').setValue(value.leaveCategory);
    this.leaveSanctionCreationForm.get('isRecovery').setValue(value.isRecovery);
    this.leaveSanctionCreationForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
    if (value.returnReason!=null) {
      this.showReturnReason = true;
      this.leaveSanctionCreationForm.get('returnReason').setValue(value.returnReason);
    }
    else { this.showReturnReason = false; }
   
    this.leaveSanctionCreationForm.get('remarks').setValue(value.remarks);
    for (var i = 0; i < value.leaveSanctionDetails.length; i++) {     
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('payEntDT').setValue(value.leaveSanctionDetails[i].payEntDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isMaxLeave').setValue(value.leaveSanctionDetails[i].isMaxLeave);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isHalfDay').setValue(value.leaveSanctionDetails[i].isHalfDay);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isAfterNoon').setValue(value.leaveSanctionDetails[i].isAfterNoon);
    }
    this.leaveSanctionCreationForm.enable();
  }
  ShowHistorygrid(isShow: boolean) {
    debugger;
    this.ShowHistory = isShow;
  }
  DissableLeaves(value) {
    debugger;
    this.leaveSanctionCreationForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.leaveSanctionCreationForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionCreationForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionCreationForm.get('leaveMainId').setValue(value.leaveMainId);
    this.leaveSanctionCreationForm.get('orderDT').setValue(value.orderDT);
    this.leaveSanctionCreationForm.get('orderNo').setValue(value.orderNo);
    this.leaveSanctionCreationForm.get('leaveCategory').setValue(value.leaveCategory);
    this.leaveSanctionCreationForm.get('isRecovery').setValue(value.isRecovery);  
    this.leaveSanctionCreationForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
    if (value.returnReason != null) {
      this.showReturnReason = true;
      this.leaveSanctionCreationForm.get('returnReason').setValue(value.returnReason);
    }
    else { this.showReturnReason = false;}
    this.leaveSanctionCreationForm.get('remarks').setValue(value.remarks);
    for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('payEntDT').setValue(value.leaveSanctionDetails[i].payEntDT);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isMaxLeave').setValue(value.leaveSanctionDetails[i].isMaxLeave);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isHalfDay').setValue(value.leaveSanctionDetails[i].isHalfDay);
      (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).at(i).get('isAfterNoon').setValue(value.leaveSanctionDetails[i].isAfterNoon);
    }
    this.leaveSanctionCreationForm.disable();
    if (this.UserRoleId == "9") {
      this.showCancel = true;
      this.showForward = true;
      this.showSave = true;
    }
    else { this.showSave = false;}
    this.radioafterchecked = false;
    //this.buttonText = "Verify";
    this.showDeleteButton = false;
    this.showAddButton = false;
  }
  DeleteLeavesSanction(leaveMainId: string, flag: boolean) {
    debugger;

    this.leaveMainId = flag ? leaveMainId : "0";
    if (!flag) {
      this._Service.DeleteLeavesSanction(leaveMainId, 'Sanc').subscribe((result: any) => {
        this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
        this.GetLeavesSanction(this.employeeCode);
      });
    }
    this.deletepopup = false;
  }
  checkValidation(){
    debugger;

    //CL and EL cant be combined.

    this.leavecomb = 0;
    this.clsclrh = "";

    (<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).controls.forEach((control, index) => {
      if (control.get('leaveCd').value == 'CL') {
        this.leavecomb = this.leavecomb + 1;
        this.clsclrh = this.clsclrh + "CL" + "*";
      }
      else if (control.get('leaveCd').value == 'EL') {
        this.leavecomb = this.leavecomb + 1;
      }
      else if (control.get('leaveCd').value == 'RH') {
        this.clsclrh = this.clsclrh + "RH" + "*";
      }
      else if (control.get('leaveCd').value == 'SCL') {
        this.clsclrh = this.clsclrh + "SCL" + "*";
      }
    });
    if (this.clsclrh.split("*").includes("CL")) {
      if (!((this.clsclrh.split("*").includes("RH")) || (this.clsclrh.split("*").includes("SCL")))) {
        alert('Please check the leave type.CL can only be clubbed with Special CL and RH');
        return false;
      }

    }
    if (this.leavecomb >= 2) {
      alert('Please check the leave type.CL cant be combined with EL');
      return false;
    }

    //if user selected combined type leave but fill only one leave type 
    if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Combined") {
      if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).length == 1) {
        alert('Please select single leave Type');
        return false;
      }
    }
      else if (this.leaveSanctionCreationForm.get("leaveCategory").value == "Single") {
      if ((<FormArray>this.leaveSanctionCreationForm.get('leaveSanctionDetails')).length > 1) {
        alert('Please select combined leave Type');
        return false;
      }
      }
    }
  validateOrdNo(event):boolean
  {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48) ) {
        return true;
      }
      return false;
  }
  

  abc() {
    alert(this.findInvalidControls());
  }
  //Extra methods
  public findInvalidControls() {
    debugger;
    const invalid = [];
    const controls = this.leaveSanctionCreationForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
  


 
}
                           
