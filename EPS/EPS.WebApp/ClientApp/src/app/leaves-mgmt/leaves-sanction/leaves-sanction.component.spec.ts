import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesSanctionComponent } from './leaves-sanction.component';

describe('LeavesSanctionListComponent', () => {
  let component: LeavesSanctionComponent;
  let fixture: ComponentFixture<LeavesSanctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesSanctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesSanctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
