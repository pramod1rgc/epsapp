import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesCancellComponent } from './leaves-cancell.component';

describe('LeavesCancellComponent', () => {
  let component: LeavesCancellComponent;
  let fixture: ComponentFixture<LeavesCancellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesCancellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesCancellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
