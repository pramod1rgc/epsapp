import { Component, OnInit } from '@angular/core';
import { OnBoardingModel } from '../../model/UserManagementModel/OnBoardingModel';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OnBoardingService } from '../../services/UserManagement/OnBoarding.service';
import { DatePipe } from '@angular/common'

export interface commondll {
  values: string;
  text: string;
}

@Component({
  selector: 'app-onboarding-details',
  templateUrl: './onboarding-details.component.html',
  styleUrls: ['./onboarding-details.component.css'],
  providers: [DatePipe]
})
export class OnboardingDetailsComponent implements OnInit {

  ObjOnBoarding: OnBoardingModel;
  controllerroleList: any[];
  Query: any;
  ControllerID: any;
  OnboardingList: any[];
  ObjRemarks: any;
  Remarks: any;
  public controllerCtrl: FormControl = new FormControl();
  public controllerFilterCtrl: FormControl = new FormControl();
  public filteredcontroller: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  private _onDestroy = new Subject<void>();

  constructor(private Service: OnBoardingService, public datepipe: DatePipe) { }


  ngOnInit() {
    this.ObjOnBoarding = new OnBoardingModel();
    this.getallcontrollers();
    this.controllerFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filtercontroller(); });
  }

  //Search functionality
  private filtercontroller() {
    if (!this.controllerroleList) {
      return;
    }
    // get the search keyword
    let search = this.controllerFilterCtrl.value;
    if (!search) {
      this.filteredcontroller.next(this.controllerroleList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredcontroller.next(
      this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
    );
  }

  //For controller fetch
  getallcontrollers() {
      this.Service.GetAllReuestNoOfOnboarding().subscribe(data => {
        this.controllerroleList = data;
        this.controllerCtrl.setValue(this.controllerroleList);
        this.filteredcontroller.next(this.controllerroleList);
      });
    
  }
  
  RequestLetterNoChnage(value) {
    
    this.Service.FetchReuestLetterNoRecord(value).subscribe(data => {
      this.OnboardingList = data;
      this.ObjOnBoarding.RequestLetterDate = this.datepipe.transform(data[0].requestLetterDate, 'dd-MM-yyyy');
      this.ObjOnBoarding.ControllerID = data[0].controllerID;
      this.ObjOnBoarding.PAOID = data[0].paoid;
      this.ObjOnBoarding.DDOID = data[0].ddoid;
      this.ObjOnBoarding.FirstName = data[0].firstName;
      this.ObjOnBoarding.LastName = data[0].lastName;
      this.ObjOnBoarding.Designation = data[0].designation;
      this.ObjOnBoarding.EmailID = data[0].emailID;
      this.ObjOnBoarding.MobileNo = data[0].mobileNo;
      this.ObjOnBoarding.CFirstName = data[0].cFirstName;
      this.ObjOnBoarding.CLastName = data[0].cLastName;
      this.ObjOnBoarding.CDesignation = data[0].cDesignation;
      this.ObjOnBoarding.CEmailID = data[0].cEmailID;
      this.ObjOnBoarding.CMobileNo = data[0].cMobileNo;
      this.ObjOnBoarding.PFirstName = data[0].pFirstName;
      this.ObjOnBoarding.PLastName = data[0].pLastName;
      this.ObjOnBoarding.PDesignation = data[0].pDesignation;
      this.ObjOnBoarding.PEmailID = data[0].pEmailID;
      this.ObjOnBoarding.PMobileNo = data[0].pMobileNo;
      this.ObjOnBoarding.DFirstName = data[0].dFirstName;
      this.ObjOnBoarding.DLastName = data[0].dLastName;
      this.ObjOnBoarding.DDesignation = data[0].dDesignation;
      this.ObjOnBoarding.DEmailID = data[0].dEmailID;
      this.ObjOnBoarding.DMobileNo = data[0].dMobileNo;
      debugger;
    });
  }
  Cancel() {
    alert(this.Remarks);
  }
}
