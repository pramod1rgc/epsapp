import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingRegistrationComponent } from './onboarding-registration.component';

describe('OnboardingRegistrationComponent', () => {
  let component: OnboardingRegistrationComponent;
  let fixture: ComponentFixture<OnboardingRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
