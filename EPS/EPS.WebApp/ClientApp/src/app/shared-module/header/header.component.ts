import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }
  username: any;
  userRole: any;
  ngOnInit() {
    this.username = sessionStorage.getItem('username');
    this.userRole = sessionStorage.getItem('userRole');
  }
  LogOut() {
    sessionStorage.clear();
    //alert('LogOut');
    this.router.navigate(['/login'])
  }
  RedirectToLogin() {
    this.router.navigate(['/login'])
  }
}
