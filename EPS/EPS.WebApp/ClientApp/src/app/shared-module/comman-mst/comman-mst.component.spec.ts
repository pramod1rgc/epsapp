import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommanMstComponent } from './comman-mst.component';

describe('CommanMstComponent', () => {
  let component: CommanMstComponent;
  let fixture: ComponentFixture<CommanMstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommanMstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommanMstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
