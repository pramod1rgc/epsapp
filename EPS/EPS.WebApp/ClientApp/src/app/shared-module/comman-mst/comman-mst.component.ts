import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CommonMsg } from '../../global/common-msg';
import { BillCode, DesignationModel, DesignCode, EMPCode, EmpModel, PayBillGroupModel } from '../../model/Shared/DDLCommon';
import { CommanService } from '../../services/loan-mgmt/comman.service';
@Component({
  selector: 'app-comman-mst',
  templateUrl: './comman-mst.component.html',
  styleUrls: ['./comman-mst.component.css']
})

export class CommanMstComponent implements OnInit {
  @ViewChild('commanForm') commanForm: any;
  @Input() events: Observable<void>;
  private eventsSubscription: Subscription;
  @Output() onchange = new EventEmitter();
  ddlDesign: DesignationModel[];
  ddlBillGroup: PayBillGroupModel[];
  ArrddlEmployee: EmpModel[];
  ddlEmployee: EmpModel[];
  permDdoId: string;
  tempvar: any;
  public designId: string;
  public empid: string;
  username: string;
  ArrEmpCode: any;
  EmpCode: string;
  valueselected: number;
  @Input() data;
  question: number = 0;
  empVfyCode1: string[];
  checkautofill: string;
  selectedDesign: any;
  selectedEmp: any;
  constructor(private _Service: CommanService, private _msg: CommonMsg, private cd: ChangeDetectorRef) { }
  selectedIndex = 0;
  Bill: any[];
  Design: any[];
  EMP: any[];
  flag: any = null;
  billGroupMsg: string;
  desgMsg: string;
  empMsg: string;
  /** searching . */
  public billCtrl: FormControl = new FormControl();
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public billFilterCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredBill: ReplaySubject<BillCode[]> = new ReplaySubject<BillCode[]>(1);
  public filteredDesign: ReplaySubject<DesignCode[]> = new ReplaySubject<DesignCode[]>(1);
  public filteredEmp: ReplaySubject<EMPCode[]> = new ReplaySubject<EMPCode[]>(1);
  private _onDestroy = new Subject<void>();
  isLoading: boolean;

  ngOnInit() {
    this.billGroupMsg = this._msg.billGroupNotFoundMsg;
    this.desgMsg = this._msg.desgNotFoundMsg;
    this.empMsg = this._msg.empNotFoundMsg;
    this.username = sessionStorage.getItem('username');
    this.FindEmpCode(this.username);
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
    this.eventsSubscription = this.events.subscribe(data => {
      this.isLoading = true;
      this.resetCommonControl(data);
      //console.log(data);
    });
  }
  //ngOnDestroy() {
  //  this.eventsSubscription.unsubscribe();
  //}

  resetCommonControl(data) {
    this.commanForm.resetForm();
    this.cd.detectChanges();
    this.isLoading = false;
    //this.billCtrl.setValue(0);
    // this.designCtrl.setValue(0);
    //this.empCtrl.setValue(0);
    this.selectedDesign = '';
    this.selectedEmp = '';
    this.loadCommonMethod();
  }

  loadCommonMethod() {
    this.billGroupMsg = this._msg.billGroupNotFoundMsg;
    this.desgMsg = this._msg.desgNotFoundMsg;
    this.empMsg = this._msg.empNotFoundMsg;
    this.username = sessionStorage.getItem('username');
    this.FindEmpCode(this.username);
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
  }

  FindEmpCode(username) {
    //debugger;
    this._Service.GetEmpCode(username).subscribe(data => {
      this.ArrEmpCode = data[0];
      if (this.ArrEmpCode.empcode != null) {
        this.permDdoId = this.ArrEmpCode.ddoid;
        sessionStorage.setItem('PermDdoId', this.ArrEmpCode.ddoid);
        this.BindDropDownBillGroup(this.permDdoId);
        this.BackForwordSearchDesignation("0");
        this.BackForwordSearchEmployee("0");
      }
    });
  }

  BindDropDownBillGroup(value) {
    //debugger;
    this.isLoading = true;
    this._Service.GetAllPayBillGroup(value, this.flag).subscribe(result => {
      this.ddlBillGroup = result;
      this.isLoading = false
      this.Bill = result;
      this.billCtrl.setValue(this.Bill);

      this.empCtrl.setValue(this.EMP);
      this.filteredBill.next(this.Bill);
    });
  }

  allowAlphaNumericSpace(event): boolean {
    //console.log(event.target.value);
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if ((charCode !== 32) && // space        
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      else {
        if ((charCode !== 32) && // space        
          (charCode >= 48 && charCode <= 57)) { // number (0-9)
          alert(this._msg.numberNotAllowed);
          return true;
        }
        else {

          return false;
        }
      }
    }
    if ((charCode !== 32) && (charCode >= 48 && charCode <= 57)) { // number (0-9)
      alert(this._msg.numberNotAllowed);
      return true;
    }
  }

  tempdesignvar: any;
  BindDropDownDesignation(value) {

    this.permDdoId = sessionStorage.getItem('PermDdoId');
    this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, this.flag).subscribe(data => {
      this.ddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);

      this.empCtrl.setValue(this.EMP);
      this.filteredDesign.next(this.Design);
      this.tempdesignvar = this.designCtrl.value;
      if (this.designCtrl.value.length > 1) {
        this.tempdesignvar = '';
        this.BindDropDownEmployee(this.tempdesignvar);
      }
      this.onchange.emit('');
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }

  BindDropDownEmployee(value) {

    this.designId = value;
    this.permDdoId = sessionStorage.getItem('PermDdoId');
    this.tempvar = this.billCtrl.value;
    if (this.billCtrl.value.length > 2) {
      this.tempvar = '';
    }
    this._Service.GetAllEmpByBillgrIDDesigID(this.designId, this.tempvar, this.permDdoId, this.flag).subscribe(data => {
      this.ddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
      this.onchange.emit('');
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }

  BackForwordSearchDesignation(value) {
    this.isLoading = true;
    this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, this.flag).subscribe(data => {
      this.ddlDesign = data;
      this.isLoading = false;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  billFlag: any;
  BackForwordSearchEmployee(value) {
    //debugger;
    this.isLoading = true;
    this.permDdoId = sessionStorage.getItem('PermDdoId');
    if (this.billCtrl.value == null) {
      this.billFlag = this.billCtrl.value;
    }
    else if (this.billCtrl.value.length > 0) {
      this.billFlag = null;
    }
    else {
      this.billFlag = this.billCtrl.value;
    }
    this._Service.GetAllEmpByBillgrIDDesigID(value, this.billFlag, this.permDdoId, this.flag).subscribe(data => {
      this.ddlEmployee = data;
      this.isLoading = false;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });



    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }

  public selectionChange($event?: StepperSelectionEvent): void {
    console.log('stepper.selectedIndex: ' + this.selectedIndex
      + '; $event.selectedIndex: ' + $event.selectedIndex);
    if ($event.selectedIndex === 0) { return; }
    this.selectedIndex = $event.selectedIndex;
  }

  private filterBill() {
    if (!this.Bill) {
      return;
    }
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.Bill.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredBill.next(

      this.Bill.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredEmp.next(
      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }

  SelectedValue(value) {
    debugger;
    this.isLoading = true;
    if (value != "") {
      //alert('value' + value);
      var splitted = value.split("-", 3);
      this.empid = splitted[0].trim();
      this.selectedDesign = splitted[2].trim();
      //alert('bill' + this.selectedDesign);
      //this.designId = splitted[2].trim();
      this.selectedEmp = Number(splitted[1]);
      //alert('desig' + this.selectedEmp);
      this.cd.detectChanges();
      this.onchange.emit(this.empid);
      this.isLoading = false;
    }
    //else {
    //  //to reset common component in dropdownlist 
    //  this.billCtrl.setValue('');
    //  this.designCtrl.setValue('');
    //  this.empCtrl.setValue('');
    //  //this.loadCommonMethod();
    //  this.BackForwordSearchEmployee("0");
    //  this.BackForwordSearchDesignation("0");
    //  this.BindDropDownBillGroup(this.permDdoId);

    //}
  }

}
