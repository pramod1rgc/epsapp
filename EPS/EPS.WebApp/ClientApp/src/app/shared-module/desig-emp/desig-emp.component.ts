import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { MatSelect } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs'
import { MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';

@Component({
  selector: 'app-desig-emp',
  templateUrl: './desig-emp.component.html',
  styleUrls: ['./desig-emp.component.css']
})

export class DesigEmpComponent implements OnInit {
  @Output() onchange = new EventEmitter();
  @Output() desigchange = new EventEmitter();
  ArrddlDesign: DesignationModel;
  ArrddlEmployee: EmpModel;
  PermDdoId: string;
  constructor(private _Service: LeavesMgmtService, private snackBar: MatSnackBar) {
  }
  selectedIndex = 0;
  Design: any[];
  EMP: any[];
  placeholdervalue_desig: string = "Select Designation";
  placeholdervalue_emp: string = "Select Employee";
  @ViewChild('pdetails') form: any;
  @ViewChild('EmPDdetailsForm') EmPDdetailsForm: any;
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  public filteredEmp: ReplaySubject<EmpModel[]> = new ReplaySubject<EmpModel[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;
  private _onDestroy = new Subject<void>();
  question: number = 0;
  ngOnInit() {
    debugger;
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.BindDropDownDesignation(this.PermDdoId);
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
    this.BackForwordSearchEmployee("0");
  }
  ngOnChanges() {
  }

  BindDropDownDesignation(value) {

    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BindDropDownEmployee(value) {
    this.desigchange.emit();
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
    this.placeholdervalue_desig = "";
  }
  BackForwordSearchDesignation(value) {
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      // set initial selection
      this.designCtrl.setValue(this.Design);
      // load the initial Design list
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BackForwordSearchEmployee(value) {
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      // set initial selection
      this.empCtrl.setValue(this.EMP);
      // load the initial EMP list
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  public selectionChange($event?: StepperSelectionEvent): void {
    console.log('stepper.selectedIndex: ' + this.selectedIndex
      + '; $event.selectedIndex: ' + $event.selectedIndex);

    if ($event.selectedIndex === 0) { return; } // First step is still selected

    this.selectedIndex = $event.selectedIndex;
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEmp.next(

      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }
  GetEmployeedetails(value) {
    this.placeholdervalue_emp = "";
    this.onchange.emit(value);

  }
  charaterOnly(event): boolean {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8 ) {
      return true;
    }
    return false;
  }

}

