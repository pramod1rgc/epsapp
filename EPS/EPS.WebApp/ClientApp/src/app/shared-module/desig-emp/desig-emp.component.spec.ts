import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesigEmpComponent } from './desig-emp.component';

describe('DesigEmpComponent', () => {
  let component: DesigEmpComponent;
  let fixture: ComponentFixture<DesigEmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesigEmpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesigEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
