import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './dialog/dialog.component';
import { MaterialModule } from '../material.module';
import { DesigEmpComponent } from './desig-emp/desig-emp.component';
import { DesigEmpOrdnoComponent } from './desig-emp-ordno/desig-emp-ordno.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { HeaderComponent } from '../shared-module/header/header.component';
import { CommanMstComponent } from '../shared-module/comman-mst/comman-mst.component';
import { MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [DialogComponent, DesigEmpComponent, DesigEmpOrdnoComponent, HeaderComponent, CommanMstComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule,
    RouterModule,
    MatTooltipModule
  
  ],
  exports: [DialogComponent, DesigEmpComponent, DesigEmpOrdnoComponent, HeaderComponent, CommanMstComponent]
})
export class SharedModule { }


