import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesigEmpOrdnoComponent } from './desig-emp-ordno.component';

describe('DesigEmpOrdnoComponent', () => {
  let component: DesigEmpOrdnoComponent;
  let fixture: ComponentFixture<DesigEmpOrdnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesigEmpOrdnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesigEmpOrdnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

