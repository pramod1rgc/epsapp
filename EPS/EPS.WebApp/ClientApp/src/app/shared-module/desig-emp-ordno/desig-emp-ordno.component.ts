
import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { MatSelect } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs'
import { MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { EmpModel, DesignationModel, OrderModel } from '../../model/Shared/DDLCommon';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';


@Component({
  selector: 'app-desig-emp-ordno',
  templateUrl: './desig-emp-ordno.component.html',
  styleUrls: ['./desig-emp-ordno.component.css']
})
export class DesigEmpOrdnoComponent implements OnInit {

  placeholdervalue_desig: string = "Select Designation";
  placeholdervalue_emp: string = "Select Employee";
  placeholdervalue_ord: string = "Select Order Numer(Order Date)";

  @Output() desigchange = new EventEmitter();
  @Output() empchange = new EventEmitter();
  @Output() orderchange = new EventEmitter();
  ArrddlDesign: DesignationModel;
  ArrddlEmployee: EmpModel;
  @Input() ArrddlOrder: OrderModel;
  @Input() PageCode: string;
  PermDdoId: string;
  constructor(private _Service: LeavesMgmtService, private snackBar: MatSnackBar) {
  }
  selectedIndex = 0;
  Design: any[];
  EMP: any[];
  Order: any[];
  @ViewChild('pdetails') form: any;
  @ViewChild('EmPDdetailsForm') EmPDdetailsForm: any;
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public orderNoCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public orderNoFilterCtrl: FormControl = new FormControl();


  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  public filteredEmp: ReplaySubject<EmpModel[]> = new ReplaySubject<EmpModel[]>(1);
  public filteredOrder: ReplaySubject<OrderModel[]> = new ReplaySubject<OrderModel[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;
  private _onDestroy = new Subject<void>();
  question: number = 0;
  ngOnInit() {
    debugger;
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.BindDropDownDesignation(this.PermDdoId);
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
    this.BackForwordSearchEmployee("0");
    this.BackForwordSearchOrder("0");
  }
  ngOnChanges() {
  }



  BindDropDownDesignation(value) {
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BindDropDownEmployee(value) {
    this.desigchange.emit();
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });

    this.ArrddlOrder = null;
    this.Order = null;
    this.orderNoCtrl.setValue(this.Order);
    this.filteredOrder.next(this.Order);
    this.placeholdervalue_desig = "";
  }
  BindDropDownOrder(value) {
    debugger;
    this.empchange.emit(value);
    this._Service.GetOrderbyEmp(value, sessionStorage.getItem('userRoleID'), this.PageCode).subscribe(data => {
      this.ArrddlOrder = data;
      this.Order = data;
      this.orderNoCtrl.setValue(this.Order);
      this.filteredOrder.next(this.Order);
    });
    this.orderNoFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterOrder();
      });
    this.placeholdervalue_emp = "";
  }

  BackForwordSearchDesignation(value) {
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      // set initial selection
      this.designCtrl.setValue(this.Design);
      // load the initial Design list
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BackForwordSearchEmployee(value) {
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      // set initial selection
      this.empCtrl.setValue(this.EMP);
      // load the initial EMP list
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  BackForwordSearchOrder(value) {
    debugger;
    this._Service.GetOrderbyEmp(value, sessionStorage.getItem('userRoleID'), this.PageCode).subscribe(data => {
      this.ArrddlOrder = data;
      this.Order = data;
      // set initial selection
      this.orderNoCtrl.setValue(this.Order);
      // load the initial EMP list
      this.filteredOrder.next(this.Order);
    });
    this.orderNoFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterOrder();
      });
  }

  public selectionChange($event?: StepperSelectionEvent): void {
    console.log('stepper.selectedIndex: ' + this.selectedIndex
      + '; $event.selectedIndex: ' + $event.selectedIndex);

    if ($event.selectedIndex === 0) { return; } // First step is still selected

    this.selectedIndex = $event.selectedIndex;
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEmp.next(

      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterOrder() {
    if (!this.Order) {
      return;
    }
    let search = this.orderNoFilterCtrl.value;
    if (!search) {
      this.filteredOrder.next(this.Order.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredOrder.next(

      this.Order.filter(Order => Order.ordernName.toLowerCase().indexOf(search) > -1)
    );
  }


  GetOrderdetails(value) {
    debugger;
    this.placeholdervalue_ord = "";
    this.orderchange.emit(value);
  }

  charaterOnly(event): boolean {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8) {
      return true;
    }
    return false;
  }
}

