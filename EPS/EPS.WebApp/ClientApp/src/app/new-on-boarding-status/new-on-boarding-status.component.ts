import { Component, OnInit } from '@angular/core';
import { OnBoardingModel } from '../model/UserManagementModel/OnBoardingModel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-on-boarding-status',
  templateUrl: './new-on-boarding-status.component.html',
  styleUrls: ['./new-on-boarding-status.component.css']
})
export class NewOnBoardingStatusComponent implements OnInit {

  ObjOnBoarding: OnBoardingModel;
  constructor(private router: Router) { }

  ngOnInit() {
    this.ObjOnBoarding = new OnBoardingModel();
  }

  Cancel() {
    this.router.navigate(['login']);
  }
  SubmitForStatus() {

  }

}
