import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource  } from '@angular/material';
import swal from 'sweetalert2';
import { ddoservice } from '../../services/UserManagement/ddo.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-ddoroll',
  templateUrl: './ddoroll.component.html',
  styleUrls: ['./ddoroll.component.css'],
  providers: [CommonMsg]
})
export class DdorollComponent implements OnInit {

  DDOList: any = [];
  userroleList: any = [];
  EmpList: any = [];
  EmpListTemp: any = [];
  empCd: any;
  empName: any;
  Active: any;
  activeStatus: any;
  Messages: any;
  checkStatus = "NO";
  PAOID: any;
  AssignedEmp: any = [];
  deactivatePopup: any;
  ActivatePopup: any;
  Query: any;
  DDOID: any;
  MessageDataFound = true;
  isLoading: boolean;
  IsFlagUnAssigned: boolean;
  IsFlagAssign: boolean;
  btnFlage: boolean;
  btnFlageView: boolean;
  EmpListAssigned: any = [];

  displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
  dataSource = new MatTableDataSource(this.EmpList);
  datasourceEmp = new MatTableDataSource(this.AssignedEmp);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('DDODetail') form: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _Service: ddoservice, private commonMsg: CommonMsg) { }

  ngOnInit() {
    this.btnFlage = true;
    this.getallDDO(sessionStorage.getItem('paoid'));
  }
  AssignedDDO(DDOID) {
    this.IsFlagAssign = false;
    this.IsFlagUnAssigned = false;
    this.AssignedEmp = null;
    this.dataSource = null;
    this.MessageDataFound = false
    this.DDOID = DDOID;
    this.isLoading = true;
    this._Service.AssignedDDO(this.DDOID).subscribe(data => {
      this.EmpListAssigned = data;
      if (this.EmpListAssigned.length == 0)
      {
        this.btnFlage = true;
        this.btnFlageView = false;
        this.IsFlagAssign = false;
        this.IsFlagUnAssigned = true;
      }
      else if (this.EmpListAssigned.length != 0)
      {
        this.btnFlage = false;
        this.btnFlageView = true;
        this.IsFlagAssign = false;
        this.IsFlagUnAssigned = true;
      }
      this.isLoading= false;
    });
  }

  Go() {
    this.isLoading = true;
    this._Service.AssignedEmpDDO(this.DDOID).subscribe(data => {
      this.AssignedEmp = data;
      if (this.AssignedEmp.length == 0) {
        this.IsFlagAssign = true;
        this.GoEmpList();
      }
      else if(this.AssignedEmp.length != 0) {
        this.isLoading = false;
        this.IsFlagUnAssigned = true;
        this.IsFlagAssign = false;
      }
      else {
        this.IsFlagAssign = true;
        this.IsFlagUnAssigned = false;
      }
      var num = 0;
      for (num = 0; num < this.AssignedEmp.length; num++) {
        if (this.AssignedEmp[num]['activeStatus'] == this.commonMsg.dDOAdmin) {
          this.checkStatus = "Yes";
          break;
        }
      }
    });
  }
  GoEmpList() {
    this.checkStatus = "NO";
    this.isLoading = true;
    this._Service.EmployeeList(this.DDOID).subscribe(data => {
      this.EmpList = data;
      this.dataSource = new MatTableDataSource(this.EmpList.filter(emp => emp.activeStatus != this.commonMsg.dDOAdmin));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.EmpList.length > 0) {
        this.MessageDataFound = true;
      }
      else {
        this.MessageDataFound = false;
        }
      var num = 0;
      for (num = 0; num < this.EmpList.length; num++) {
        if (this.EmpList[num]['activeStatus'] == this.commonMsg.dDOAdmin) {
          this.checkStatus = "Yes";
          break;
        }
      }
      this.isLoading = false;
    });
  }

  EmployeeList(DDOID) {
    this.DDOID = DDOID;
    this.btnFlage = false;
    this.btnFlageView = false;
    this.isLoading = true;
    this._Service.EmployeeList(this.DDOID).finally(() => this.isLoading = false).subscribe(data => {
      this.EmpListTemp = data;
      if (this.EmpListTemp.length == 0) {
      }
      else if (this.EmpListTemp.length != 0)
      {
        this.btnFlage = false;
        this.btnFlageView = true;
      }
    });
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.dDOAdmin)
    {
      this.Active = true;
    }
    else
    {
      this.Active = false;
    }
  }

  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }

  Assigned() {
    this.isLoading = true;
    this._Service.Assigned(this.empCd, this.DDOID, this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result;
      if (this.Messages == this.commonMsg.USN) {
        this.IsFlagUnAssigned = false;
        this.btnFlageView = false;
        this.btnFlage = true;
        this.deactivatePopup = false;
      }
      if (this.Messages == this.commonMsg.ASN) {
        this.Messages = this.commonMsg.EmailMsg;
        this.btnFlageView = true;
        this.btnFlage = false;
        this.ActivatePopup = false;
      }
      if (this.Messages != null) {
        this.checkStatus = "NO"
        this.Go();
        swal(this.Messages)
      }
    })
  }

  GetAllUserRoles() {
    this.isLoading = true;
    this._Service.GetAllUserRoles(sessionStorage.getItem('username')).finally(() => this.isLoading = false).subscribe(data => {
      this.userroleList = data;
    });
  }

  getallDDO(PAOID) {
    this.isLoading = true;
    this.Query = this.commonMsg.getDDO;
    this._Service.GetAllDDO(PAOID, this.Query).finally(() => this.isLoading = false).subscribe(data => {
      this.DDOList = data;
    });
  }
}
