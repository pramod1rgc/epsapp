import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import swal from 'sweetalert2';
import { paoservice } from '../../services/UserManagement/pao.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-paoroll',
  templateUrl: './paoroll.component.html',
  styleUrls: ['./paoroll.component.css'],
  providers: [CommonMsg]
})

export class PaorollComponent implements OnInit {
  //userroleList: any = [];
  paosList: any = [];
  EmpList: any = [];
  MsPAOID: any;
  empCd: any;
  empName: any;
  Active: any;
  activeStatus: any;
  Messages: any;
  checkStatus = "NO";
  AssignedEmp: any = [];
  AssignedEmpTemp: any = [];
  deactivatePopup: any;
  ActivatePopup: any;
  Query: any;
  ddlDDOList: any = [];
  DDOID: any;
  PAOID: any;
  MessageDataFound = true;
  isLoading: boolean;
  IsFlagUnAssigned: boolean;
  IsFlagAssign: boolean;
  btnFlage: boolean;
  btnFlageView: boolean;
  DDLHideShow: boolean;
  AssignedEmpList: any = [];
  AssignedEmpListTemp: any = [];
  displayedColumns = ['EmpCode', 'DDOName', 'EmpName', 'CommonDesigDesc', 'Email',  'ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('PAODetail') form: any;
  @ViewChild('DDODetail') DDLDetailform: any;
  @ViewChild('empDetail') empDetailform: any;

  dataSource = new MatTableDataSource(this.EmpList);
  datasourceEmp = new MatTableDataSource(this.AssignedEmp);

  constructor(private _Service: paoservice, private commonMsg: CommonMsg) { }

  ngOnInit() {
    this.btnFlage = true;
    this.DDLHideShow = false;
    this.getallpaos(sessionStorage.getItem('controllerID'));
  }

  PAODDLChange() {
    this.DDLHideShow = true;
  }

  Go() {
    alert('go')
    this._Service.AssignedPAOEmp(this.MsPAOID).subscribe(data => {
      this.AssignedEmpList = data;
      this.AssignedEmpListTemp = new MatTableDataSource(this.AssignedEmpList);
      if (this.AssignedEmpListTemp.data.length > 0) {
        this.IsFlagAssign = false;
      }
      else {
        this.employeeList();
        this.IsFlagAssign = true;
      }
    });
  }
  employeeList() {
    this._Service.onpaoSelectchanged(this.MsPAOID).subscribe(data => {
      this.EmpList = data;
      this.AssignedEmp = this.EmpList.filter(emp => emp.activeStatus == this.commonMsg.PAO)
      this.datasourceEmp = new MatTableDataSource(this.AssignedEmp);
      this.dataSource = new MatTableDataSource(this.EmpList.filter(emp => emp.activeStatus != this.commonMsg.PAO));
      this.dataSource.paginator = this.paginator;
      if (this.dataSource.data.length > 0) {
        this.MessageDataFound = true;
      }
      else {
        this.MessageDataFound = false;
      }
      var num = 0;
      for (num = 0; num < this.EmpList.length; num++) {
        if (this.EmpList[num]['activeStatus'] == this.commonMsg.PAO) {
          this.checkStatus = "Yes";
          break;
        }
      }
    });
  }
  View() {
   // this.ddlDDOList = null;
    this._Service.AssignedEmpList(this.MsPAOID).subscribe(data => {
      this.AssignedEmp = data;
      if (this.AssignedEmp.length == 0) {
        this.IsFlagAssign = true;
        this.btnFlageView = false;
        this.btnFlage = true;
        this.IsFlagAssign = false;
        this.DDLHideShow = true;
      }
      else if (this.AssignedEmp.length != 0) {
        this.IsFlagUnAssigned = true;
        this.IsFlagAssign = false;
        this.btnFlageView = true;
        this.btnFlage = false;
        this.DDLHideShow = false;
      }
      else {
        this.IsFlagAssign = true;
        this.IsFlagUnAssigned = false;
      }
      var num = 0;
      for (num = 0; num < this.AssignedEmp.length; num++) {
        if (this.AssignedEmp[num]['activeStatus'] == this.commonMsg.PAO) {
          this.checkStatus = "Yes";
          break;
        }
      }
    });
  }

  Assigned() {
    this.isLoading = true;
    this._Service.Assigned(this.empCd, this.MsPAOID, this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result;
      if (this.Messages == this.commonMsg.USN) {
        this.IsFlagUnAssigned = false;
        this.IsFlagAssign = true;
        this.btnFlageView = false;
        this.btnFlage = true;
        this.dataSource = null;
        this.AssignedEmp = null;
        this.IsFlagAssign = false;
      }
      if (this.Messages == this.commonMsg.ASN) {
        this.IsFlagUnAssigned = true;
        this.IsFlagAssign = false;
        this.btnFlageView = true;
        this.btnFlage = false;
        this.Messages = this.commonMsg.EmailMsg;
        this.View();
      }

      if (this.Messages != null) {
        this.checkStatus = "NO"
        swal(this.Messages)
        this.ActivatePopup = false;
        this.deactivatePopup = false;
      }
    })
  }

  onpaoSelectchanged(MsPAOID) {
    //alert('Change')
   // this.ddlDDOList = null;
    this.MsPAOID = MsPAOID;
    this.getallDDO(this.MsPAOID);
    this.IsFlagUnAssigned = false;
    this.IsFlagAssign = false;
    this.dataSource = null;
    this.AssignedEmp = null;
    this.MessageDataFound = true;
    this.DDLHideShow = false;
    this.btnFlage = false;
    this.btnFlageView = false;
    this._Service.AssignedEmpList(this.MsPAOID).subscribe(data => {
      this.AssignedEmpTemp = data;
      if (this.AssignedEmpTemp.length == 0) {
        this.btnFlage = true;
        this.btnFlageView = false;
      }
      else if (this.AssignedEmpTemp.length != 0) {
        this.btnFlage = false;
        this.btnFlageView = true;
      }
    });

  }

  getallpaos(ControllerID) {
    // this.paosList = null;
    //this.ddlDDOList = null;
    this.Query = 'getPAO';
    this._Service.GetAllpaos(ControllerID, this.Query).subscribe(data => {
      this.paosList = data;
    });
  }

  getallDDO(PAOID) {
    //this.ddlDDOList = null;
    this.Query = 'getDDO';
    this._Service.GetAllDDO(PAOID, this.Query).subscribe(data => {
      this.ddlDDOList = data;
    });
  }

  DDOSelectChange(MsDDOID) {
    this.DDOID = MsDDOID
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.PAO) {
      this.Active = true;
    }
    else {
      this.Active = false;
    } 
  }

  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }
}
