import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared-module/shared-module.module';
import { PayrollRoutingModule } from './payroll-routing.module';
import { DdorollComponent } from './ddoroll/ddoroll.component';
import { PaorollComponent } from './paoroll/paoroll.component';
import { ControllerroleComponent } from './controllerrole/controllerrole.component';
import { MaterialModule } from '../material.module';
import { UsersbyddoComponent } from './usersbyddo/usersbyddo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MakerrollComponent } from './makerroll/makerroll.component';
import { HOOMakerComponent } from './hoo-maker/hoo-maker.component';
import { HOOCheckerComponent } from './hoo-checker/hoo-checker.component';
import { LoginActivateComponent } from './login-activate/login-activate.component';
@NgModule({
  declarations: [DdorollComponent, PaorollComponent, ControllerroleComponent,  UsersbyddoComponent, MakerrollComponent, HOOMakerComponent, HOOCheckerComponent, LoginActivateComponent],
  imports: [
    CommonModule,
    PayrollRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, SharedModule
  ]
})  
export class PayrollModule { }
