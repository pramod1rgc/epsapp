import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakerrollComponent } from './makerroll.component';

describe('MakerrollComponent', () => {
  let component: MakerrollComponent;
  let fixture: ComponentFixture<MakerrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakerrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakerrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
