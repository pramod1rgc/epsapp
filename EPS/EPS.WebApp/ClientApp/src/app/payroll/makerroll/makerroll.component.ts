import { Component, OnInit, ViewChild } from '@angular/core';
import { makerservice } from '../../services/UserManagement/maker.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-makerroll',
  templateUrl: './makerroll.component.html',
  styleUrls: ['./makerroll.component.css'],
  providers: [CommonMsg]
})
export class MakerrollComponent implements OnInit {

  constructor(private _Service: makerservice, private commonMsg: CommonMsg) { }
  empCd: any;
  empName: any;
  activeStatus: any;
  Active: any = null;
  length: any = null;
  makerList: any;
  AssignedEmp: any;
  datasourceEmp: any;
  dataSource: any;
  checkStatus: any;
  Messages: any;
  isLoading: boolean;
  MessageDataFound = true;
  SelfCheck: boolean;
  checked: any;
  deactivatePopup: boolean;
  ActivatePopup: boolean;
  AssignedBtnDisabled: boolean;
  CancelDisabled: boolean;
  AssignedMakerList: any;

  displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email','ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  ngOnInit() {
    this.makerEmpList();
    this.AssignedBtnDisabled = false;
    this.CancelDisabled = false;
    this.CheckedFlagSelfAssignDDOMaker();
    this.AssignedmakerEmpList();
  }

  AssignedmakerEmpList() {
    this._Service.AssignedMakerEmpList(sessionStorage.getItem('ddoid')).subscribe(data => {
      this.AssignedMakerList = data;
    });
  }

  Assigned() {
    this.isLoading = true;
    this.AssignedBtnDisabled = true;
    this.CancelDisabled = true;
    this._Service.AssignedMaker(this.empCd, sessionStorage.getItem('ddoid'), sessionStorage.getItem('UserID'), this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result.toLowerCase();
      if (this.Messages == this.commonMsg.USN.toLowerCase()) {
        this.Messages = this.commonMsg.USN;
        this.makerEmpList();
        this.AssignedmakerEmpList();
      }
      if (this.Messages == this.commonMsg.ASN.toLowerCase()) {
        this.Messages = this.commonMsg.EmailMsg;
        this.makerEmpList();
        this.AssignedmakerEmpList();
      }

      if (this.Messages != null) {
        this.checkStatus = "NO"
        swal(this.Messages);
        this.ActivatePopup = false;
        this.deactivatePopup = false;
      }
    })
  }

  makerEmpList() {
    this.AssignedBtnDisabled = false;
    this.CancelDisabled = false;
    this._Service.makerEmpList(sessionStorage.getItem('ddoid')).subscribe(data => {
      this.makerList = data;
      this.AssignedEmp = this.makerList.filter(emp => (emp.activeStatus == this.commonMsg.DDOChecker) || (emp.activeStatus == this.commonMsg.DDOMaker))
      this.datasourceEmp = new MatTableDataSource(this.AssignedEmp);
      this.dataSource = new MatTableDataSource(this.makerList.filter(emp => (emp.activeStatus != this.commonMsg.DDOChecker) && (emp.activeStatus != this.commonMsg.DDOMaker)));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.MessageDataFound = true;
        this.SelfCheck = false;
      }
      else {
        this.MessageDataFound = false;
        this.SelfCheck = true;
      }
    });
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.DDOChecker || activeStatus == this.commonMsg.DDOMaker) {
      this.Active = true;
    }
    else {
      this.Active = false;
    }
  }

  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }

  SelfAssign(IsAssign) {
    this.checked = IsAssign;
    this._Service.SelfAssignedDDOMaker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(result => {
      this.Messages = result;
      swal(this.Messages)
      this.ActivatePopup = false;
    })
  }
  CheckedFlagSelfAssignDDOMaker() {
    this.checked = true;
    this._Service.SelfAssignMakerRole(sessionStorage.getItem('UserID')).subscribe(result => {
      this.Messages = result;
      if (this.Messages == '[]') {
        this.checked = false;
      }
    })
  }
}
