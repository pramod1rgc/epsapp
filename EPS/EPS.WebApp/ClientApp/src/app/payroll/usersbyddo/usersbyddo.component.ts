import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { usersbyddoservice } from '../../services/UserManagement/usersbyddo.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-usersbyddo',
  templateUrl: './usersbyddo.component.html',
  styleUrls: ['./usersbyddo.component.css'],
  providers: [CommonMsg]
})

export class UsersbyddoComponent implements OnInit {
  EmpList: any = [];
  RoleID: any ;
  empCd: any;
  empName: any;
  Active: any;
  activeStatus: any;
  Messages: any;
  checkStatus = "NO";
  deactivatePopup: any;
  ActivatePopup: any;
  Query: any;
  MessageDataFound = true;
  MessageUnassign = true;
  dataSource: any;
  datasourceEmp: any;
  isLoading: boolean;
  checked : any;
  SelfCheck: boolean;
  displayedColumns = ['EmpCode', 'DDOName', 'PAOName','EmpName','CommonDesigDesc', 'Email', 'ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _Service: usersbyddoservice, private commonMsg: CommonMsg) {}

  ngOnInit() {
    this.CheckedFlagSelfAssignDDOChecker();
    this.AssignedDDOCheckerEmpList();
    this.EmployeeListOfDDO();
  }

  AssignedDDOCheckerEmpList() {
    this.isLoading = true;
    this._Service.AssignedDDOCheckerEmpList(sessionStorage.getItem('ddoid')).subscribe(data => {
      this.datasourceEmp = data;
      if (this.datasourceEmp.length > 0) {
      }
      else {
        this.SelfCheck = true;
      }
    });
  }
  
  Assigned() {
    this.isLoading = true;
    this._Service.AssignedCMD(this.empCd, this.RoleID, sessionStorage.getItem('ddoid'), sessionStorage.getItem('UserID'), this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result;
      if (this.Messages != null) {
        this.checkStatus = "NO"
        this.EmployeeListOfDDO();
        this.AssignedDDOCheckerEmpList();
        swal(this.Messages)
        this.ActivatePopup = false;
        this.deactivatePopup = false;
      }
    })
  }
  EmployeeListOfDDO() {
    this._Service.EmployeeListOfDDO(sessionStorage.getItem('ddoid')).subscribe(data => {
      this.EmpList = data;
      this.dataSource = new MatTableDataSource(this.EmpList.filter(emp => (emp.activeStatus != this.commonMsg.DDOChecker) && (emp.activeStatus != this.commonMsg.DDOMaker)));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0)
      {
        this.MessageUnassign = false;
        this.SelfCheck = false;
      }
      else
      {
        this.MessageUnassign = true;
        this.SelfCheck = true;
      }
      this.isLoading = false;
    });
  }
  
  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.DDOChecker || activeStatus == this.commonMsg.DDOMaker) {
      this.Active = true;
    }
    else {
      this.Active = false;
    }
  }
 
  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }
  SelfAssign(IsAssign) {
    this.checked = IsAssign;
    this._Service.SelfAssignDDOChecker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(result => {
      this.Messages = result;
      swal(this.Messages)
      this.ActivatePopup = false;
      this.deactivatePopup = false;
    })
  }
  CheckedFlagSelfAssignDDOChecker() {
    this.checked = true;
    this._Service.SelfAssignCheck(sessionStorage.getItem('UserID')).subscribe(result => {
      this.Messages = result;
      if (this.Messages == '[]') {
        this.checked = false;
      }
    })
  }
}
