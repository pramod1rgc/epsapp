import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnfreezeanddeletebillComponent } from './unfreezeanddeletebill.component';

describe('UnfreezeanddeletebillComponent', () => {
  let component: UnfreezeanddeletebillComponent;
  let fixture: ComponentFixture<UnfreezeanddeletebillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnfreezeanddeletebillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnfreezeanddeletebillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
