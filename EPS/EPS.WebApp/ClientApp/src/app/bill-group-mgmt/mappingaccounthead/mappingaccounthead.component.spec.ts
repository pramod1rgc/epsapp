import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingaccountheadComponent } from './mappingaccounthead.component';

describe('MappingaccountheadComponent', () => {
  let component: MappingaccountheadComponent;
  let fixture: ComponentFixture<MappingaccountheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingaccountheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingaccountheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
