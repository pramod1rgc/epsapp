import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Actdeactmapacoth1Component } from './actdeactmapacoth1.component';

describe('Actdeactmapacoth1Component', () => {
  let component: Actdeactmapacoth1Component;
  let fixture: ComponentFixture<Actdeactmapacoth1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Actdeactmapacoth1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Actdeactmapacoth1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
