import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { AccountHeadOT1 } from '../../model/accountheadOT1';
import { PayBillGroupService } from '../../services/PayBill/PayBillService';
import { SelectionModel } from '@angular/cdk/collections';
import { debug } from 'util';


@Component({
  selector: 'app-mappingaccounthead',
  templateUrl: './mappingaccounthead.component.html',
  styleUrls: ['./mappingaccounthead.component.css'],

})
export class MappingaccountheadComponent implements OnInit {

  mappingAccountHeadOT1: AccountHeadOT1 = {
    ddoCode: null,
    ddoCodeddoName: null

  }

  getAccountHeadOT1: any = [];
  getFinYr: any[];

  public modeselect = '2018';
  public FinFromYr; FinToYr: string;

  displayedColumns: string[] = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'select'];
  MappeddisplayedColumns: string[] = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'PaybillGroupStatus', 'Mappedselect'];
  //checked: boolean = true;
  buttonDisabled: boolean;
  data: any;
  dataSource: any;
  MappeddataSource: any;
  disabled: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('creationForm') form: any;
  constructor(private _Service: PayBillGroupService, private snackBar: MatSnackBar
  ) { }
  ngOnInit() {

    this.getAccountHeadsOT1();
    this.disabled = false;

  }
  public selection = new SelectionModel<AccountHeadAttach>(true, []);
  public Mappedselection = new SelectionModel<AccountHeadAttach>(true, []);


  getAccountHeadsOT1() {
    //debugger;
    if (this.modeselect === '2018') {
      this.FinFromYr = '2018';
      this.FinToYr = '2019';
    }
    this._Service.GetAccountHeadsOT1(this.FinFromYr, this.FinToYr, null).subscribe(data => {
      this.getAccountHeadOT1 = data;
      console.log(data[0].ddoCode);
    });
  }

  //on click of DDL_ddocode
  GetAccountHeadOT1data(value: any) {
    debugger;
    if (value === 'undefined' || null) {
      this.disabled = true;
    }
    this.getAccountHeadOT1AttachList(value);
    this.getAccountHeadOT1MappedAttachList(value);
  }
  // #region <Mapping of account head OT 1>
  getAccountHeadOT1AttachList(DDOCode: any) {
    this._Service.GetAccountHeadOT1AttachList(DDOCode, 'N').subscribe(res => {
      debugger;

      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.disabled = true;

    })
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected($event) {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle($event) {
    debugger;
    this.isAllSelected($event) ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }
  checkOptions() {
    debugger;

    const numSelected1 = this.selection.selected.length;

    var self = this;
    let isSelected: any = numSelected1;
    let ddoCodeSelected: any = this.mappingAccountHeadOT1.ddoCode;
    if (isSelected > 0) {
      //At least one is selected
      //alert(numSelected1);      
      this.selection.selected.forEach(function (value) {
        let accountHeadvalue: string = value.hiddenAccountHead;
        //billgroups.push(value.accountHead);

        self._Service.UpdateaccountHeadOT1(accountHeadvalue, ddoCodeSelected, 'Map').subscribe(result => {
          if (parseInt(result) >= 1) {

            self.snackBar.open('Update Successfully', null, { duration: 4000 });
            self.GetAccountHeadOT1data(ddoCodeSelected);
          } else {
            self.snackBar.open('Not Update Successfully', null, { duration: 4000 });
          }


        });

        //console.log(value.accountHead);
      });
      //mapped account head

      this.getAccountHeadOT1MappedAttachList(ddoCodeSelected);
      self.selection.clear();
      self.Mappedselection.clear();
      //alert(billgroups);
    } else {
      alert("select at least one");
    }

  }

  //onCompleteRow(dataSource) {
  //  dataSource.data.forEach(element => {
  //    console.log(element.accountHead);
  //  });
  //}

  // #endregion

  // #region <Mapped of account head OT 1>
  MappedapplyFilter(filterValue: string) {
    this.MappeddataSource.filter = filterValue.trim().toLowerCase();
    if (this.MappeddataSource.paginator) {
      this.MappeddataSource.paginator.firstPage();
    }
  }
  getAccountHeadOT1MappedAttachList(DDOCode: any) {
    this._Service.GetAccountHeadOT1AttachList(DDOCode, 'Y').subscribe(res => {
      debugger;
      this.MappeddataSource = new MatTableDataSource(res);
      this.MappeddataSource.sort = this.sort;
    })
  }
  MappedisAllSelected($event) {

    const numSelected = this.Mappedselection.selected.length;
    const numRows = this.MappeddataSource.data.length;
    return numSelected === numRows;
  }
  MappedmasterToggle($event) {
    debugger;
    this.MappedisAllSelected($event) ?
      this.Mappedselection.clear() :
      this.MappeddataSource.data.forEach(row => this.Mappedselection.select(row));

  }

  UnMappedcheckOptions() {
    debugger;

    const numSelected1 = this.Mappedselection.selected.length;

    var self = this;
    let isSelected: any = numSelected1;
    let ddoCodeSelected: any = this.mappingAccountHeadOT1.ddoCode;
   // let paybillGroupStatusSelected: any = this.Mappedselection.selected[0].paybillGroupStatus;
    //this.Mappedselection.selected.forEach(function (value) {
    //  let accountHeadvalue: string = value.hiddenAccountHead;
    //})
    if (isSelected > 0) {
      //At least one is selected
      //alert(numSelected1);


      this.Mappedselection.selected.forEach(function (value) {
        debugger;
        let accountHeadvalue: string = value.hiddenAccountHead;
        let paybillGroupStatusSelected: any = value.paybillGroupStatus;
        if (paybillGroupStatusSelected == 'No') {
          self._Service.UpdateaccountHeadOT1(accountHeadvalue, ddoCodeSelected, 'Unmap').subscribe(result => {
            if (parseInt(result) >= 1) {

              self.snackBar.open('Update Un-Map Successfully', null, { duration: 4000 });
              self.GetAccountHeadOT1data(ddoCodeSelected);
            } else {
              self.snackBar.open('Update Un-Map Not Successfully', null, { duration: 4000 });
            }


          });
        }
        else {
          alert("You Can only De-Activate the Account Head as Bill is Already Processed");
        }
        //console.log(value.accountHead);
      });
      //mapped account head

      this.getAccountHeadOT1MappedAttachList(ddoCodeSelected);

      self.selection.clear();
      self.Mappedselection.clear();
      //alert(billgroups);
    } else {
      alert("select at least one for Un-mapped");
    }

  }


  // #endregion

}
const ELEMENT_DATA: AccountHeadAttach[] = [];
export interface AccountHeadAttach {

  accountHead: string;
  objectHead: string;
  category: string;
  accountHeadName: string;
  paybillGroupStatus: string;
  hiddenAccountHead: string;
}
