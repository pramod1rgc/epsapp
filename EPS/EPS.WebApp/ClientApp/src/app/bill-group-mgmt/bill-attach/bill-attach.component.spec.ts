import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillAttachComponent } from './bill-attach.component';

describe('BillAttachComponent', () => {
  let component: BillAttachComponent;
  let fixture: ComponentFixture<BillAttachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillAttachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
