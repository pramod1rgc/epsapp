import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../services/PayBill/PayBillService';
import { BillGroupCreation } from '../../model/PayBillGroup/BGCreation';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { map, startWith } from 'rxjs/operators';
import { MasterService } from '../../services/master/master.service';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { debug } from 'util';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';
//import { FormControl } from '@angular/forms';
//import { Observable } from 'rxjs';


@Component({
  selector: 'app-bill-group-creation',
  templateUrl: './bill-group-creation.component.html',
  styleUrls: ['./bill-group-creation.component.css'],
  providers: [MasterService]
})
export class BillGroupCreationComponent implements OnInit {
  btnclose() {
    this.is_btnStatus = false;
  }
is_btnStatus = true;

  billGroupCreationForm: FormGroup;
  _btnText: string;
  flag: string;
  displayedColumns: string[] = ['S.No', 'billgrDesc', 'schemeCode', 'serviceType', 'groups', 'Action'];
  highlightedRows = [];
  billGroupCreation: BillGroupCreation = {
    payBillGroupId: null,
    schemeId: null,
    schemeCode: null,
    accountHead: null,
    serviceType: null,
    group: null,
    groupIds: null,
    billGroupName: null,    
    billForGAR: "false",
    isActive: null,
    serviceTypeId: null,
    permDDOId: null
  }
  //abc: string = "true";
 // abc = "false";
  checked = false
  group:any = [
    { id: 'A', name: 'A'},
    { id: 'B', name: 'B', checked: false },
    { id: 'BN-Gz', name: 'BN-Gz', checked: false },
    { id: 'C', name: 'C', checked: false },
    { id: 'D', name: 'D', checked: false }
  ]
  getAccountHead: any = [];
  editMode: boolean = false;
  serviceType: any = [];
  billGroup: any = [];
  dataSource = new MatTableDataSource();
  permDDOId: any;
  tempGroup: any = [];
  allData: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('creationForm') form: any;
  disableFlag: boolean;
  checkBoxValidation=false;
  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private masterServices: MasterService, private promotionServices: PromotionsService, private _msg: CommonMsg, private snackBar: MatSnackBar,
  ) { this.createForm() }

  ngOnInit() {
    debugger;
    this._btnText = 'Save';
    console.log(this.billGroupCreation.billForGAR);
    this.getAccountHeads('00003');
    this.getServiceType();
    this.BindAllBillGroup('00003');
    this.billGroupCreation.payBillGroupId = 0;
  }
  createForm() {
    this.billGroupCreationForm = this.fb.group({
      accountHead: ['', Validators.required],
      serviceType: ['', Validators.required],
      group: [''],
      billGroupName: ['', Validators.required],

      billForGAR: ['', Validators.required],
      payBillGroupId: ['', '']

    });
  }
  getAccountHeads(permDDOId: any) {
    

    this._Service.BindAccountHeads(permDDOId).subscribe(data => {
      this.getAccountHead = data;
      console.log(data[0].schemeCode);
    });
  }
  getServiceType() {
    this.masterServices.getServiceType().subscribe(data => {
      this.serviceType = data;
      console.log(data[0].serviceText);
    });
  }


  BindAllBillGroup(permDDOId: any) {
    this.flag = null;
    this._Service.BindPayBillGroup(permDDOId).subscribe(result => {
      this.billGroup = result;
      this.allData = result;
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  saveBillGroup(newBillGroup: BillGroupCreation): void {
    debugger;
    if (newBillGroup.group != null && newBillGroup.group.length != 0) {
      newBillGroup.groupIds = '';
      for (let entry of newBillGroup.group) {
        if (newBillGroup.groupIds == '') {
          newBillGroup.groupIds = entry;
        }
        else
          newBillGroup.groupIds = newBillGroup.groupIds + "," + entry;
      }
      this.tempGroup = newBillGroup.group;
      newBillGroup.group = "";
      console.log(newBillGroup);
      newBillGroup.permDDOId = '00003';
      //newBillGroup.payBillGroupId = 0;
      if (this.billGroupCreationForm.valid) {


        this._Service.InsertUpdatepayBillGroup(newBillGroup).subscribe(res => {
          if (res != "") {
            newBillGroup.group = this.tempGroup;
            swal(res)
          }
          else {
            if (this.editMode) {
              swal("Updated Successfully");
            }
            else {

              swal("Saved Successfully");
            }
            //swal("Saved Successfully");
            this.form.resetForm();
            this.ngOnInit();
            this.billGroupCreationForm.controls.billForGAR.setValue("false");
            //this.billGroupCreation.billForGAR = "false";
            //this.createForm();
          }
          //return false;
          //this.snackBar.open(res, null, { duration: 4000 });
          //this.BindAllBillGroup('00003');
        }
        );
        //this.snackBar.open('Save successfully', null, { duration: 4000 });

        //newBillGroup.billGroupName = '';
        //document.getElementById('btn_save').innerHTML = 'Save';
        this._btnText = 'Save';

        this.disableFlag = false;
      }
      else {
        newBillGroup.group = this.tempGroup;
      }
    }

    else {
      this.checkBoxValidation = true;
    
    }
  }
  groupSelection(newBillGroup: BillGroupCreation) {
    
    if (newBillGroup.group.length != 0) {
      this.checkBoxValidation = false;
    }
    //else {
    //  this.checkBoxValidation = true;
    //}
  }
  
  btnEditClick(schemeId, serviceType, group, billGroupName, billForGAR, payBillGroupId) {
    this.editMode = true;
    let billgroups: any = []
    this.disableFlag = true;
    
    for (let entry of group.split(','))
      {
      billgroups.push(entry.trim());
    }
    this.billGroupCreation.accountHead = schemeId;
    this.billGroupCreation.serviceType = String(serviceType);
    this.billGroupCreation.group = billgroups;
    //this.groups = 'A';
    this.billGroupCreation.billGroupName = billGroupName.trim();
    this.billGroupCreation.billForGAR = String(billForGAR);
    this.billGroupCreation.payBillGroupId=payBillGroupId;
    //billGroupCreation.group='A'
    //this.billGroupCreation.billForGAR = billForGAR;
    //document.getElementById('btn_save').innerHTML = 'Update';
    this._btnText = 'Update';

  }
  rowHighlight(row) {
    this.highlightedRows.pop();
    this.highlightedRows.push(row)
  }
  resetForm() {
    
    this.disableFlag = false;
    var a = this.billGroupCreation.billForGAR;
    this.billGroupCreation.accountHead = null;
    this.billGroupCreation.serviceType = null;
    this.billGroupCreation.group = null;
    this.billGroupCreation.billGroupName = null;
    this.billGroupCreation.billForGAR = "false";
    this.highlightedRows.pop();
    this._btnText = 'Save';

    //this.form.resetForm();
    //if (a) {
    //  this.billGroupCreation.billForGAR = "false";
    //}
    
  }
  //saveBillGroup(newBillGroup: BillGroupCreation) {
  //  newBillGroup.groupIds = "1";
  //  newBillGroup.group = "";
  //  this._Service.InsertUpdatepayBillGroup(newBillGroup).subscribe(res => {})
  //}

  lastAction: string;

  //data = [
  //  { label: 'one', checked: false },
  //  { label: 'two', checked: true },
  //  { label: 'three', checked: false },
  //  { label: 'four', checked: false },
  //  { label: 'five', checked: false }
  //];
  applyFilter(filterValue: string) {
    //this.dataSource.filter = filterValue.trim().toLowerCase();
    this.paginator.pageIndex = 0;
    this.dataSource = new MatTableDataSource(this.allData.filter(a => a.billgrDesc.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.schemeCode.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.serviceType.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.groups.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1  || a.billgrId.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1));
    this.dataSource.paginator = this.paginator;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  charaterOnlyNoSpace(event): boolean {
    let msg = this._msg.charaterOnlyNoSpace(event);
    return msg;
  }
  onChange(event, index, item) {

    item.checked = !item.checked;

    this.lastAction = 'index: ' + index + ', label: ' + item.label + ', checked: ' + item.checked;

    console.log(index, event, item);

  }
}
