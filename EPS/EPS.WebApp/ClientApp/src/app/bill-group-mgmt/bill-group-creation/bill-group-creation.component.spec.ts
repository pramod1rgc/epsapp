import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillGroupCreationComponent } from './bill-group-creation.component';

describe('BillGroupCreationComponent', () => {
  let component: BillGroupCreationComponent;
  let fixture: ComponentFixture<BillGroupCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillGroupCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillGroupCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
