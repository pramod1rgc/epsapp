import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewandforwardbillComponent } from './viewandforwardbill.component';

describe('ViewandforwardbillComponent', () => {
  let component: ViewandforwardbillComponent;
  let fixture: ComponentFixture<ViewandforwardbillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewandforwardbillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewandforwardbillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
