import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesDeductionsComponent } from './dues-deductions.component';

describe('DuesDeductionsComponent', () => {
  let component: DuesDeductionsComponent;
  let fixture: ComponentFixture<DuesDeductionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesDeductionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesDeductionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
