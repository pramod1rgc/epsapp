import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesDeductionsHeaderComponent } from './dues-deductions-header.component';

describe('DuesDeductionsHeaderComponent', () => {
  let component: DuesDeductionsHeaderComponent;
  let fixture: ComponentFixture<DuesDeductionsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesDeductionsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesDeductionsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
