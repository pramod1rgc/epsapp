import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemporarypbrprocessComponent } from './temporarypbrprocess.component';

describe('TemporarypbrprocessComponent', () => {
  let component: TemporarypbrprocessComponent;
  let fixture: ComponentFixture<TemporarypbrprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemporarypbrprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemporarypbrprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
