import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaybillgenerationComponent } from './paybillgeneration.component';

describe('PaybillgenerationComponent', () => {
  let component: PaybillgenerationComponent;
  let fixture: ComponentFixture<PaybillgenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaybillgenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaybillgenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
