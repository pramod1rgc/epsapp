import { Component, OnInit, ViewChild } from '@angular/core';

import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;

}


const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Rahul Khanna', weight: 121},
  { position: 1, name: 'Rahul Khanna', weight: 121 },
  { position: 1, name: 'Rahul Khanna', weight: 121 },
  { position: 1, name: 'Rahul Khanna', weight: 121 },
  { position: 1, name: 'Rahul Khanna', weight: 121 },
  { position: 1, name: 'Rahul Khanna', weight: 121 },
];


@Component({
  selector: 'app-paybillgeneration',
  templateUrl: './paybillgeneration.component.html',
  styleUrls: ['./paybillgeneration.component.css']
})
export class PaybillgenerationComponent implements OnInit {

  displayedColumns: string[] = ['select', 'position', 'weight', 'name'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
