import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { DialogComponent } from '../../dialog/dialog.component';
import { NgRecoveryCreationComponent } from './ng-recovery-creation/ng-recovery-creation.component';
import { NgRecoveryRoutingModule } from './ng-recovery-routing.module';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EntryforallemployeeComponent } from './entryforallemployee/entryforallemployee.component';
import { HeaderforallemployeeComponent } from './headerforallemployee/headerforallemployee.component';
import { FileuploadforallemployeeComponent } from './fileuploadforallemployee/fileuploadforallemployee.component';
import { EntryforsingleemployeeComponent } from './entryforsingleemployee/entryforsingleemployee.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { LoanmgtModule } from '../../loanmgt/loanmgt.module';
import { EditViewAllEmployeeComponent } from './edit-view-all-employee/edit-view-all-employee.component';
import { SharedModule } from '../../shared-module/shared-module.module';
import { NGRprocessingComponent } from './ngrprocessing/ngrprocessing.component';






 
@NgModule({
  declarations: [NgRecoveryCreationComponent, EntryforallemployeeComponent, HeaderforallemployeeComponent, FileuploadforallemployeeComponent, EntryforsingleemployeeComponent, EditViewAllEmployeeComponent, NGRprocessingComponent],
  imports: [
    CommonModule,
    NgRecoveryRoutingModule,
    MaterialModule, FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, LoanmgtModule, SharedModule
  ]
})
export class NgRecoveryModule {  }
