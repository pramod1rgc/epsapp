/*import { Component, OnInit } from '@angular/core';*/
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-ngrprocessing',
  templateUrl: './ngrprocessing.component.html',
  styleUrls: ['./ngrprocessing.component.css']
})


export class NGRprocessingComponent implements OnInit {
  displayedColumns: string[] = ['SrNo', 'PayBillName', 'BillId', 'BillDate', 'Noofemployee', 'BillType'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
    this.dataSource = new MatTableDataSource(NAMES);
  }

  ngOnInit() {

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
export interface UserData {
  SrNo: number;
  PayBillName: string;
  BillId: string;
  BillDate: number;
  Noofemployee: number;
  BillType: number;
}
/** Constants used to fill up our data base. */

const NAMES: UserData[] = [
  { SrNo: 1, PayBillName: 'This is a dummy text.', BillId: '3', BillDate: 4, Noofemployee: 343, BillType: 447 },

];

/**
 * @title Data table with sorting, pagination, and filtering.
 */



/** Builds and returns a new User. */

