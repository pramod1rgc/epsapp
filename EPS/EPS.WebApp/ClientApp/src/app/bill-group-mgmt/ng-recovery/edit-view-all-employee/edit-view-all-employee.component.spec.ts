import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditViewAllEmployeeComponent } from './edit-view-all-employee.component';

describe('EditViewAllEmployeeComponent', () => {
  let component: EditViewAllEmployeeComponent;
  let fixture: ComponentFixture<EditViewAllEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditViewAllEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditViewAllEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
