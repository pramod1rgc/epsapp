//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import * as $ from 'jquery';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { NgRecovery } from '../../../model/PayBillGroup/NgRecovery';
import { debug } from 'util';

@Component({
  selector: 'app-fileuploadforallemployee',
  templateUrl: './fileuploadforallemployee.component.html',
  styleUrls: ['./fileuploadforallemployee.component.css']
})
export class FileuploadforallemployeeComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('creationForm') form: any;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private snackBar: MatSnackBar,
  ) { /*this.createForm()*/ }

  //bindFinancialYear: any = [];
  ngOnInit() {
    //this.bindFinancialYear();
  }
  
  //bindFinancialYear() {
  //  debugger;

  //  this._Service.GetFinancialYear().subscribe(data => {
  //    //console.log(data[0].schemeCode);
  //  });
  //}

}
