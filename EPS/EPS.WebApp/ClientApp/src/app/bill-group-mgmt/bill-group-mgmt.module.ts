import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BillGroupMgmtRoutingModule } from './bill-group-mgmt-routing.module';
import {BillGroupCreationComponent} from './bill-group-creation/bill-group-creation.component';
import {MaterialModule} from '../material.module';
import { BillAttachComponent } from './bill-attach/bill-attach.component';
import { PaybillprocessComponent } from './paybillprocess/paybillprocess.component';
import { MappingaccountheadComponent } from './mappingaccounthead/mappingaccounthead.component';
import { Actdeactmapacoth1Component } from './mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component';
import { DuesDeductionsComponent } from './dues-deductions/dues-deductions.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { DuesDeductionsHeaderComponent } from './dues-deductions/dues-deductions-header/dues-deductions-header.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DuesanddeductionsemployeewiseComponent } from './duesanddeductionsemployeewise/duesanddeductionsemployeewise.component';
import { TemporarypbrprocessComponent } from './temporarypbrprocess/temporarypbrprocess.component';
import { PaybillgenerationComponent } from './paybillgeneration/paybillgeneration.component';
import { from } from 'rxjs';
import { UnfreezeanddeletebillComponent } from './unfreezeanddeletebill/unfreezeanddeletebill.component';
import { ViewandforwardbillComponent } from './viewandforwardbill/viewandforwardbill.component';
import { CommonMsg } from '../global/common-msg';

@NgModule({
  declarations: [BillGroupCreationComponent, BillAttachComponent, PaybillprocessComponent, MappingaccountheadComponent, Actdeactmapacoth1Component, DuesDeductionsComponent, DuesDeductionsHeaderComponent, DuesanddeductionsemployeewiseComponent, TemporarypbrprocessComponent, PaybillgenerationComponent, UnfreezeanddeletebillComponent, ViewandforwardbillComponent],
  imports: [
    CommonModule,
    BillGroupMgmtRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, SharedModule, NgxMatSelectSearchModule],
  providers: [CommonMsg]

})
export class BillGroupMgmtModule { }
