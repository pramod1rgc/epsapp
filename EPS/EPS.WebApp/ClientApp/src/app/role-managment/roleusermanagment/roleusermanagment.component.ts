import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar, MatSelect } from '@angular/material';
import { RoleService } from '../../services/UserManagement/role.service'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { controllerservice } from '../../services/UserManagement/controller.service';
import swal from 'sweetalert2';
import { MatTreeNestedDataSource } from '@angular/material/tree'
import { NestedTreeControl } from '@angular/cdk/tree'
import * as $ from 'jquery';
import { of } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface designationCode {
  text: string;

}
export interface commondll {
  values: string;
  text: string;
}


@Component({
  selector: 'app-roleusermanagment',
  templateUrl: './roleusermanagment.component.html',
  styleUrls: ['./roleusermanagment.component.css']
})
export class RoleusermanagmentComponent implements OnInit {
  displayedColumns: string[] = ['firstName', 'empCd', 'pan', 'designation', 'action'];
  displayedUserColumns: string[] = ['firstName', 'empCd', 'pan', 'role', 'action'];//['userID', 'role', 'firstName', 'empCd', 'pan', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //For Serchable Dropdown
  public designationCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesignation: ReplaySubject<designationCode[]> = new ReplaySubject<designationCode[]>(1);
  /** Subject that emits when the component has been destroyed. */
  private _onDestroy = new Subject<void>();
  @ViewChild('singleSelect') singleSelect: MatSelect;
  //END OF 

  constructor(private _RoleService: RoleService, private http: HttpClient, private snackBar: MatSnackBar, private _controllerservice: controllerservice) { }
  isLoading: boolean;
  MstRole: any;
  currentLesson: string;
  ArrUserList: any = [];
  userdataSource: any;
  ArrByPanUserDetaildatasource: any;
  isTableHasData = true;
  ArrRoles: any;
  result: any;
  RoleIDs = '';
  showAssignRole: boolean;
  showAssignMenu: boolean;
  SlectedRole: any;
  slectedRoleAlready: any;
  unselectedRole: any = [];
  setFlag: any;
  MsRoleID: number;
  textSearch: any;
  ArrAssignRoleForUser: any;
  ArrByPanUserDetail: any = [];
  Message: any;
  temproleid: any;
  dropdownSettings = {};
  ArrUserListddl: any;
  ddlControlerDiv: boolean;
  //Bind ddl Controler
  controllerroleList: any[];
  Query: any;
  public controllerCtrl: FormControl = new FormControl();
  public designFilter: FormControl = new FormControl();
  public controllerFilterCtrl: FormControl = new FormControl();
  public filteredcontroller: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  btnAssignRoledivfleg: boolean;
  SelectControllerID: any;
  SelectRoleID: any;
  ArrUserFleg: any;
  btnFlageView: boolean;
  btnAssignFlage: boolean;
  GrideAssignroleFlage: boolean;
  SearchAssignroleFlage: boolean;




  //End Of Bind ddl  Controler


  ngOnInit() {
    // this.GetUserDetails('2');
    this.setFlag ="SaveForRole"
    this.btnAssignFlage = true;

    this.btnFlageView = false;
    this.GrideAssignroleFlage = false;
    this.SearchAssignroleFlage = false;


    this.GetAllDataRollList();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'roleID',
      textField: 'roleName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false

    };

    this.currentLesson = 'true';

    // listen for search field value changes
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });




    //Controller ddl

    this.controllerFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filtercontroller();

      });

    this.getallcontrollers(sessionStorage.getItem('controllerID'));
    if (sessionStorage.getItem('userRoleID') == '1') {
     }
    else {
  
      this.btnAssignRoledivfleg = false;
    }
  }
  //BindCommissionCode() {
  //  this.payscale.BindCommissionCode().subscribe(result => {
  //    this.ArrRoles = result;
  //  })
  //}
  GetAllDataRollList() {
    this.isLoading = true;
    //alert(sessionStorage.getItem('username'));
    this._RoleService.GetAllCustomeRollList(sessionStorage.getItem('username')).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrRoles = result;
   

    })
  }

  GetUserDetails(roleID)//RowIndex)
  {
    this.isLoading = true;
    this.MstRole = {
      RoleID: roleID,

    };
    this._RoleService.GetUserDetails(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrUserList = result;
      this.userdataSource = new MatTableDataSource(this.ArrUserList);
      this.userdataSource.paginator = this.paginator;

      this.userdataSource.sort = this.sort;
      if (this.userdataSource.data.length > 0) {
        this.isTableHasData = true;
      }
      else {
        this.isTableHasData = false;
    

      }
    })
  }


  Rolechange(value) {

    if (value != undefined && value > 0) {
      this.temproleid = value;
      this.SelectRoleID = value;
      debugger;
      this.designFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterDesignation();
        });


      //Controller ddl

      this.controllerFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filtercontroller();

        });

      this.getallcontrollers(sessionStorage.getItem('controllerID'));
      
    }
  }


  GO() {
        
    if (sessionStorage.getItem('userRoleID') != null && sessionStorage.getItem('userRoleID') != undefined) {
      //if (this.SelectRoleID == undefined) {
      //  alert('Please Select Role');
      //  return;
      //}
      if (this.SelectControllerID == undefined) {
        alert('Please Select Controller');
        return;
      }
      this.btnAssignRoledivfleg = true;
      this.SearchAssignroleFlage = true;
      this.designFilter.setValue(0);
      this.GetUserDetails(this.SelectRoleID);     

    }

  }

  HideShowBtn() {//Hide Code
  
    this.MstRole = {
      RoleID: this.SelectRoleID,
      MsControllerID: this.SelectControllerID,
    };
    this._RoleService.GetUserDetails(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
     
      this.ArrUserFleg = result;
      if (this.ArrUserFleg.length == 0) {
        // alert('Fi');
        this.btnAssignFlage = true;
        this.btnFlageView = false;
   
      }
      else if (this.ArrUserFleg.length != 0) {
        // alert('hello')
        this.btnAssignFlage = false;
        this.btnFlageView = true;     
      }

    })
    //alert(this.ArrUserList);
  }

  Search(Upan, MsRoleID, roleName) {
    debugger
     
    this.isLoading = true;
   // this.setFlag = 'SaveForRole';
    this.MsRoleID = MsRoleID;
    if (MsRoleID != undefined) {
       // this.setFlag = 'EditForRole';
        this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName },];
    }

    if (MsRoleID == undefined || MsRoleID == '') {
      this._RoleService.GetAssignRoleForUser(this.textSearch).finally(() => this.isLoading = false).subscribe(result => {
        this.ArrAssignRoleForUser = result;

        this.SlectedRole = this.ArrAssignRoleForUser;
        this.slectedRoleAlready = this.SlectedRole;
      })

    }
    if (Upan == undefined) {
      if (this.textSearch == undefined || this.textSearch == '') {
        alert("Please select PAN Or UserName Or EMPCODE");
        return false;
      }


    }
    else {
      this.textSearch = Upan;
    }
    this.MstRole = {
      Pan: this.textSearch

    };
    this._RoleService.Search(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {

      this.ArrByPanUserDetail = result;
      this.ArrByPanUserDetaildatasource = new MatTableDataSource(this.ArrByPanUserDetail);
      this.ArrByPanUserDetaildatasource.paginator = this.paginator;
      this.ArrByPanUserDetaildatasource.sort = this.sort;

      this.GrideAssignroleFlage = true;

    })

  }


  //GetUserDetails(roleID, roleName)//RowIndex)
  //{
  //  //alert(roleID);
  //  this.RoleNameUser = roleName;
  //  this.SlectedUserRowIndex = roleID;
  //  this.MstRole = {
  //    RoleID: roleID,
  //  };
  //  this._RoleService.GetUserDetails(this.MstRole).subscribe(result => {
  //    this.ArrUserList = result;

  //  })
  //  //alert(this.ArrUserList);
  //}

  SaveAssignRoleForUser(userID) {
    //debugger;
    this.isLoading = true;
    
    // alert(this.MsRoleID);
    for (var i = 0; i < this.SlectedRole.length; i++) {
      this.RoleIDs = this.RoleIDs + this.SlectedRole[i].roleID + '$';//alert(i);
    }
  
    if (this.slectedRoleAlready.length != null) {
      this.setFlag = "EditForRole"
    }
    
    for (var j = 0; j < this.slectedRoleAlready.length; j++) {
      debugger;
      this.unselectedRole.push(this.slectedRoleAlready[j].roleID);
      for (var i = 0; i < this.SlectedRole.length; i++) {
        if (this.SlectedRole[i].roleID == this.slectedRoleAlready[j].roleID) {

          this.slectedRoleAlready.splice(j, 1);
          if (this.slectedRoleAlready.length === 0) {
            break;
          }
       //   this.slectedRoleAlready[i]

        }
      }

    }
    debugger;
     
    this.MstRole = {
      RoleIDs: this.RoleIDs,
        UserID: userID,//this.ArrByPanUserDetail[RowIndex].userID,
      setFlag: this.setFlag,
      MsRoleID: this.MsRoleID,
      unselectedRole: this.slectedRoleAlready
    };
    debugger;
    this._RoleService.SaveAssignRoleForUser(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;
      this.SlectedRole = null;
      this.RoleIDs = null;
      if (this.Message != undefined) {
       
        this.ClearInputRole();
        $(".dialog__close-btn").click();
        if (this.Message == 'Role UnAssigned Sucessfully') {
          //alert('conditions');
          this.btnAssignFlage = true;
          this.btnFlageView = false;
           
        }
        if (this.Message == 'Role Assigned Sucessfully') {
          //alert('Assigned');
          this.btnAssignFlage = false;
          this.btnFlageView = true;
           
        }


        this.GetUserDetails(this.temproleid);
        swal(this.Message)
        this.RoleIDs = '';
        this.GrideAssignroleFlage = false;
        this.SearchAssignroleFlage = false;

      }
    })
  }

  Cancel() {
    //this.filteredDesignation = null;
    this.designFilterCtrl.reset();
    this.textSearch = '';
    this.ArrUserListddl = null;
    //this.SlectedRole = null;
    this.ArrAssignRoleForUser = null;
    this.ArrByPanUserDetail = null;
    this.ArrByPanUserDetaildatasource = null;
    $(".dialog__close-btn").click();
  }

  ClearInputRole() {
    // this.textNewrole = '';
    // this.checkIsActiveRole = true;
    this.textSearch = '';
    this.ArrByPanUserDetail = null;
    this.ArrByPanUserDetaildatasource = null;
    this.textSearch = '';

    //this.SlectedRole = null;

  }



  applyFilter(filterValue: string) {
    this.userdataSource.filter = filterValue.trim().toLowerCase();
    if (this.userdataSource.paginator) {
      this.userdataSource.paginator.firstPage();
    }
  }



  BindUserListddl(Rdtext) {
    this.isLoading = true;
    this.GrideAssignroleFlage = false;
    this.designFilter.setValue(0);
    this._RoleService.GetBindUserListddl(Rdtext, this.SelectControllerID).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrUserListddl = result;

      this.designationCtrl.setValue(this.ArrUserListddl);
      this.filteredDesignation.next(this.ArrUserListddl);

    });

  }
  SetText(text) {
    //alert(text)
    this.textSearch = text;

    this.GrideAssignroleFlage = false;
  }



  private filterDesignation() {

    if (!this.ArrUserListddl) {
      return;
    }
    // get the search keyword
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesignation.next(this.ArrUserListddl.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the
    this.filteredDesignation.next(

      this.ArrUserListddl.filter(ArrUserListddl => ArrUserListddl.text.toLowerCase().indexOf(search) > -1)
    );
  }


  //Bind controller ddl


  private filtercontroller() {
     debugger;
    if (!this.controllerroleList) {
      return;
    }
    // get the search keyword
    let search = this.controllerFilterCtrl.value;
    if (!search) {

      this.filteredcontroller.next(this.controllerroleList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredcontroller.next(

      this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
    );
  }
  getallcontrollers(ControllerID) {
    debugger;
    //if (ControllerID == 0) {
      this.Query = 'getController';
      this.controllerCtrl.setValue(0);
      this._controllerservice.getAllControllerServiceclient(ControllerID, this.Query).subscribe(data => {

        this.controllerroleList = data;

        this.controllerCtrl.setValue(this.controllerroleList);
        this.filteredcontroller.next(this.controllerroleList);
      });


   // }
  }

  //End Of Bind  Controler ddl



  oncontrollerroleSelectchanged(SelectControllerID) {

    this.SelectControllerID = SelectControllerID;
    this.Cancel();

    this.GrideAssignroleFlage = false;
    this.SearchAssignroleFlage = false;
    // alert(SelectControllerID);
  }


}
