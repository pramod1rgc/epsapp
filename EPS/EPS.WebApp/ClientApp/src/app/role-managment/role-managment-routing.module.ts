import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleComponent } from './role/role.component';
import { RoleManagmentModule } from './role-managment.module';
import { MenuComponent } from './menu/menu.component';
import { RoleusermanagmentComponent } from './roleusermanagment/roleusermanagment.component'
import {OtherRoleComponent } from './other-role/other-role.component'
const routes: Routes = [

  {
    path: '', component: RoleManagmentModule, data: {
      breadcrumb: 'User Management'
    },children:[
    {
      path: 'role', component: RoleComponent, data: {
        breadcrumb: 'Role-Menu Management'
      }},
    {
      path: 'menu', component: MenuComponent, data: {
        breadcrumb: 'New Menu Creation'
        }
      },
      {
        path: 'Roleusermanagment', component: RoleusermanagmentComponent, data: {
          breadcrumb: 'Custom Role'
        }
      },
            {
              path: 'otherrole', component: OtherRoleComponent, data: {
          breadcrumb: 'New Role Creation'
        }
      },
]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleManagmentRoutingModule { }
