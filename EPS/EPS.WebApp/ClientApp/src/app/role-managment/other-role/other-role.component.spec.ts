import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherRoleComponent } from './other-role.component';

describe('OtherRoleComponent', () => {
  let component: OtherRoleComponent;
  let fixture: ComponentFixture<OtherRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
