import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartComponent } from './chart/chart.component';
import { HeaderComponent } from './header/header.component';
import {MaterialModule} from '../material.module';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NavService } from '../services/nav.service';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { UnderdevelopmentComponent } from './underdevelopment/underdevelopment.component';


@NgModule({
  declarations: [ChartComponent, HeaderComponent, HomeComponent, DashboardComponent, NavMenuComponent, PagenotfoundComponent, UnderdevelopmentComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
     ChartsModule
  ],
  providers: [NavService]
})
export class DashboardModule { }
