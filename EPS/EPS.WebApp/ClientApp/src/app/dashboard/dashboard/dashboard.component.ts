import { Component, ViewEncapsulation } from '@angular/core';
import { NavService } from '../../services/nav.service';
import { NavItem ,IBreadcrumb} from '../../model/nav-item';
import { VERSION } from '@angular/material';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET } from "@angular/router";
import "rxjs/add/operator/filter";
import { Location } from '@angular/common';
import { filter, distinctUntilChanged, map, subscribeOn } from 'rxjs/operators';
 


interface MyTreeNode {
  mainMenuName: string
  children?: MyTreeNode[]
}
@Component({
  selector: 'app-dashboard',                                                                                                                                                                        
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent  {
  navItems: NavItem[];
  version = VERSION;
  public breadcrumbs: IBreadcrumb[];
  public breadcrumbsDtls: any = {};
  name: string;
  menu: Array<any> = [];
  breadcrumbList: Array<any> = [];
  breadcrumbs1: any
  breadcrumbs2: any
  constructor(public navService: NavService, private _DashboardService: DashboardService,
    private activatedRoute: ActivatedRoute,
    private router: Router 
  ) {   
    this.breadcrumbs = [];
    let breadcrumb: IBreadcrumb = {
      label: 'Home',
     url: ''
    };
    this.username = sessionStorage.getItem('username');
    this.uRoleID = sessionStorage.getItem('userRoleID');
    this.getAllMenusByUser(this.username, this.uRoleID);
    //this.listenRouting();
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
      debugger
      this.breadcrumbs1 = event;
      this.breadcrumbs2 = this.breadcrumbs1.url
      //this.breadcrumbs2 = this.breadcrumbs2.substring(1, this.breadcrumbs2.length);
      let re = /\//gi;
      this.breadcrumbs2 = this.breadcrumbs2.substring(1, this.breadcrumbs2.length).replace(re, " / ");

      
     // this.breadcrumbs1.substring(1);
      //let root: ActivatedRoute = this.activatedRoute.root;
      //this.breadcrumbs = this.getBreadcrumbs(root);
      //this.breadcrumbs = [breadcrumb, ...this.breadcrumbs];
      //   let lastBreadCrumbLabel = this.breadcrumbs[this.breadcrumbs.length - 1].label;
    
    });
  }

  username: any;
  uRoleID: any;
  ngOnInit() {
    this.onNumberGenerated();
    this.username = sessionStorage.getItem('username');
    this.uRoleID = sessionStorage.getItem('userRoleID');
    this.getAllMenusByUser(this.username, this.uRoleID);
   // this.listenRouting();
    //this.username = sessionStorage.getItem('username');
    //alert(sessionStorage.getItem('username'));
   // this.titleService.setTitle('EPS' + ':' + sessionStorage.getItem('menudisplayName').split('/')[1]);
  }
  public onNumberGenerated() {
     
    //alert(this.randomNumber.displayName);
    //this.randomNumber = randomNumber;
    //console.log(this.randomNumber);
    this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
    this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
    //this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
    //this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
    // alert(this.breadcrumbsDtls.route);
   // console.log(sessionStorage.getItem('menudisplayName'));
  }


  resetbreadcrumbsDtls() {
    sessionStorage.setItem('menudisplayName', "");
    sessionStorage.setItem('menuroute', "");
  }

  ArrAllMenus: any;
  getAllMenusByUser(username, uRoleID) {
    this._DashboardService.getAllMenusByUser(username, uRoleID).subscribe(result => {
      this.ArrAllMenus = result;     
      this.navItems = this.ArrAllMenus;
      this.menu = this.navItems;
    })
  }

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadcrumb[] = []) {
    //debugger
    const ROUTE_DATA_BREADCRUMB: string = 'breadcrumb';
  let children: ActivatedRoute[] = route.children;

      if (children.length === 0) {
      return breadcrumbs;
    }
   for (let child of children) {
         if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }
   
      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

    
      let routeURL: string = child.snapshot.url.map(segment => segment.path).join("/");
    
      url += `/${routeURL}`;

      let breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url: url
      };
      breadcrumbs.push(breadcrumb);
  
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }

      return breadcrumbs;
   
  }


  //listenRouting() {
  //  debugger;
  //  let routerUrl: string,
  //    routerList: Array<any>,
  //    target: any;
  //  this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((router: any) => {
  //    debugger;
  //    routerUrl = router.urlAfterRedirects;
  //    if (routerUrl && typeof routerUrl === 'string') {
  //      // 初始化breadcrumb
  //      target = this.menu;
  //     // console.log(target);
  //      this.breadcrumbList.length = 0;
  //      // 取得目前routing url用/區格, [0]=第一層, [1]=第二層 ...etc
  //      routerList = routerUrl.slice(1).split('/');
  //      routerList.forEach((router, index) => {
  //        // 找到這一層在menu的路徑和目前routing相同的路徑
  //        debugger;
  //        target = target.find(page => page.route === router.url);
  //        // 存到breadcrumbList到時後直接loop這個list就是麵包屑了
  //        if (target !== undefined) {
             
  //        this.breadcrumbList.push({
  //          displayName: target.displayName,
  //         path: (index === 0) ? target.route : `${this.breadcrumbList[index - 1].route}/${target.route.slice(2)}`
  //        });
  //        } 
  //        if (index + 1 !== routerList.length) {
  //          if (this.menu[index].children.length > 0) {
  //            target = this.menu[index].children;
  //          }
          
  //        }
  //      });

  //      console.log(this.breadcrumbList);
  //    }
  //  });
  //}

}


