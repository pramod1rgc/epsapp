import { Component, Input, HostBinding, OnInit, Output, EventEmitter} from '@angular/core';
import { NavService } from '../../services/nav.service';
import { NavItem } from '../../model/nav-item';
import { Router, NavigationEnd } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Alert } from 'selenium-webdriver';
import { filter } from 'rxjs/operators';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { Title } from "@angular/platform-browser";
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ],
  providers: [NavService],
})


export class NavMenuComponent implements OnInit  {
  expanded: boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: NavItem;
  @Input() depth: number;
  chk: boolean;


  @Output() private numberGenerated = new EventEmitter<any[]>();
  //@Output() myEvent = new EventEmitter();
    breadcrumbList: any;
    uRoleID: string;
  username: string;
  constructor(public navService: NavService, public objDash: DashboardComponent, private titleService: Title,
    public router: Router) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
    localStorage.setItem('chk', 'false');
    //this.generateNumber(this.item);
  }


  ngOnInit() {
    this.titleService.setTitle('Home' + ':' + 'EPS');

    this.navService.currentUrl.subscribe((url: string) => {
       if (localStorage.getItem('chk') == "false") {
        if (this.item.route && url) {
          // console.log(`Checking '/${this.item.route}' against '${url}'`);
          this.expanded = url.indexOf(`/${this.item.route}`) === 0;
          this.ariaExpanded = this.expanded;
          // console.log(`${this.item.route} is expanded: ${this.expanded}`);
        }
      }
    });


  }
  names: any = {};
  onItemSelected(item: NavItem) {
   
    //this.generateNumber(item);
    if (!item.children || !item.children.length) {
      localStorage.setItem('chk', 'true');
      this.router.navigate([item.route]);


      // If same link is clicked again, then it reloads the component. ---begin

      let childrens = item.route.split('/');

      let currentUrl = this.router.url.split('/');

      if (childrens[childrens.length - 1] == currentUrl[currentUrl.length - 1]) {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
          this.router.navigate([item.route]));
      }

      // If same link is clicked again, then it reloads the component. ---end

      //this.navService.closeNav();
    }
    if (item.children && item.children.length) {

      this.expanded = !this.expanded;
    }
    //if (item !== undefined) {
    //  this.names.displayName = item.displayName;
    //  this.names.route = item.route
    //  // const randomNumber = Math.random();
    //  this.numberGenerated.emit(this.names);
    //}
  }

  public generateNumber(item: NavItem) {

   // sessionStorage.setItem('menudisplayName',"");

    if (item.children.length === 0) {
      if (item !== undefined) {
        
        this.numberGenerated.emit(this.names);
        sessionStorage.setItem('menudisplayName', ' / ' + item.displayName);
        this.titleService.setTitle(item.displayName + ':' + 'EPS');
         sessionStorage.setItem('menuroute', item.route);
        this.objDash.onNumberGenerated();
           
      }
    }
    
   
    //this.numberGenerated.emit=item.displayName;
  }

   

  //listenRouting() {
  //  debugger;
  //  let routerUrl: string,
  //    routerList: Array<any>,
  //    target: any;
  //  this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((router: any) => {
  //    debugger;
  //    routerUrl = router.urlAfterRedirects;
  //    if (routerUrl && typeof routerUrl === 'string') {
  //      // 初始化breadcrumb
  //      target = this.menu;
  //     // console.log(target);
  //      this.breadcrumbList.length = 0;
  //      // 取得目前routing url用/區格, [0]=第一層, [1]=第二層 ...etc
  //      routerList = routerUrl.slice(1).split('/');
  //      routerList.forEach((router, index) => {
  //        // 找到這一層在menu的路徑和目前routing相同的路徑
  //        debugger;
  //        target = target.find(page => page.route === router.url);
  //        // 存到breadcrumbList到時後直接loop這個list就是麵包屑了
  //        if (target !== undefined) {

  //        this.breadcrumbList.push({
  //          displayName: target.displayName,
  //         path: (index === 0) ? target.route : `${this.breadcrumbList[index - 1].route}/${target.route.slice(2)}`
  //        });
  //        } 
  //        if (index + 1 !== routerList.length) {
  //          if (this.menu[index].children.length > 0) {
  //            target = this.menu[index].children;
  //          }

  //        }
  //      });

  //      console.log(this.breadcrumbList);
  //    }
  //  });
  //}

}
