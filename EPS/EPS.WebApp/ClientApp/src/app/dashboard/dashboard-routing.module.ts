import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartComponent } from './chart/chart.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { UnderdevelopmentComponent } from './underdevelopment/underdevelopment.component';
const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: 'home', component: HomeComponent },
      { path: 'chart', component: ChartComponent },
      {
        path: 'role', loadChildren: '../role-managment/role-managment.module#RoleManagmentModule' 
         },
     // { path: 'test', loadChildren: '../master-mgmt/master-mgmt.module#MasterMgmtModule' },
      { path: 'onboarding', loadChildren: '../onboarding/onboarding.module#OnboardingModule' },
      { path: 'payroll', loadChildren: '../payroll/payroll.module#PayrollModule' },
      { path: 'loanmgt', loadChildren: '../loanmgt/loanmgt.module#LoanmgtModule' },
      { path: 'leavemgt', loadChildren: '../leaves-mgmt/leaves-mgmt.module#LeavesMgmtModule' },
      { path: 'employee', loadChildren: '../employee/employee.module#EmployeeModule' },
      { path: 'billGroup', loadChildren: '../bill-group-mgmt/bill-group-mgmt.module#BillGroupMgmtModule' },
      { path: 'master', loadChildren: '../masters/masters.module#MastersModule' },
      { path: 'suspension', loadChildren: '../suspension/suspension.module#SuspensionModule' },
      { path: 'commondesign', loadChildren: '../commondesign/commondesign.module#CommondesignModule' },
      { path: 'demodesign', loadChildren: '../commondesign/commondesign.module#CommondesignModule' },
      { path: 'promotion', loadChildren: '../promotion/promotion.module#PromotionModule' },
      { path: 'incometax', loadChildren: '../income-tax/income-tax.module#IncomeTaxModule' },
      //{ path: 'user', loadChildren: '../usermanagement/usermanagement.module#UsermanagementModule' },
      { path: 'recovery', loadChildren: '../recovery-excess-payment/recovery-excess-payment.module#RecoveryExcessPaymentModule' },
      { path: 'recoveryNonEPS', loadChildren: '../recovery-non-epspayment/recovery-non-epspayment.module#RecoveryNonEPSPaymentModule' },
      { path: 'deputation', loadChildren: '../deputation/deputation.module#DeputationModule' },
      { path: 'increment', loadChildren: '../increment/increment.module#IncrementModule' },
      { path: 'lienperiod', loadChildren: '../lienperiod/lienperiod.module#LienperiodModule' },
      { path: 'change', loadChildren: '../change/change.module#ChangeModule' },
      { path: 'profile', loadChildren: '../myprofile/myprofile.module#MyprofileModule' },
      { path: 'reports', loadChildren: '../reports/reports.module#ReportsModule' },
      { path: 'rejoiningofemployee', loadChildren: '../rejoiningofemployee/rejoiningofemployee.module#RejoiningofemployeeModule' },
      { path: 'endservice', loadChildren: '../endofservice/endofservice.module#EndofserviceModule' },
      { path: 'page', component: PagenotfoundComponent },
      { path: 'underdevelopment', component: UnderdevelopmentComponent },
      { path: '**', redirectTo: 'page', pathMatch: 'full' },

      
      

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
