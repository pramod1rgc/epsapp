import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferOfficeLienperiodComponent } from './transfer-office-lienperiod/transfer-office-lienperiod.component';
import { JoinAfterLienEpsEmployeeComponent } from './join-after-lien-eps-employee/join-after-lien-eps-employee.component';
import { NonEpsEmployeeJoinAfterLienComponent } from './non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component';

import { LienperiodModule } from './lienperiod.module';

const routes: Routes = [{
  
  path: '', component: LienperiodModule, children: [{
    
    path: 'transferonlien', component: TransferOfficeLienperiodComponent,
   },
    {
      path: 'epsempjoinafterlien', component: JoinAfterLienEpsEmployeeComponent,
    },
    {
      path: 'nonepsempjoinafterlien', component: NonEpsEmployeeJoinAfterLienComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LienperiodRoutingModule { }
