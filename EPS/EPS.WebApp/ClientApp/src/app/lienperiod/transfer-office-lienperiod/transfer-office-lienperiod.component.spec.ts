import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferOfficeLienperiodComponent } from './transfer-office-lienperiod.component';

describe('TransferOfficeLienperiodComponent', () => {
  let component: TransferOfficeLienperiodComponent;
  let fixture: ComponentFixture<TransferOfficeLienperiodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferOfficeLienperiodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferOfficeLienperiodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
