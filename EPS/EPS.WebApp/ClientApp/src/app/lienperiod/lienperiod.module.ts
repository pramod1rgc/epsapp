import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LienperiodRoutingModule } from './lienperiod-routing.module';
import { TransferOfficeLienperiodComponent } from './transfer-office-lienperiod/transfer-office-lienperiod.component';
import { JoinAfterLienEpsEmployeeComponent } from './join-after-lien-eps-employee/join-after-lien-eps-employee.component';
import { NonEpsEmployeeJoinAfterLienComponent } from './non-eps-employee-join-after-lien/non-eps-employee-join-after-lien.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { LoanmgtModule } from '../loanmgt/loanmgt.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [TransferOfficeLienperiodComponent, JoinAfterLienEpsEmployeeComponent, NonEpsEmployeeJoinAfterLienComponent],
  imports: [
    CommonModule,
    LienperiodRoutingModule, CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, MatTooltipModule, NgxMatSelectSearchModule, LoanmgtModule, SharedModule 
  ]
})
export class LienperiodModule { }
