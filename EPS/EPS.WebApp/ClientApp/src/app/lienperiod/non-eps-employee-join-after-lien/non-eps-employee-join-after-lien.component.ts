import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { MasterService } from '../../services/master/master.service';
import { NonepsemployeejoinafterlienperiodService } from '../../services/lienperiod/nonepsemployeejoinafterlienperiod.service';
import { MatSnackBar, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-non-eps-employee-join-after-lien',
  templateUrl: './non-eps-employee-join-after-lien.component.html',
  styleUrls: ['./non-eps-employee-join-after-lien.component.css'],
  providers: [CommonMsg]
})
export class NonEpsEmployeeJoinAfterLienComponent implements OnInit {
  form: FormGroup;
  btnUpdateText: string;
  joiningTimeDdl: any = ['ForeNoon', 'AfterNoon'];
  designation: any = [];
  joinService: any;
  roleId: any;
  getJoiningAccount: any;
  getofficelist: any;
  getPayCommission: any;
  officeCityClassList: any;
  nonEpsLienPeriodData: any;
  empCd: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  rejectionRemark: string = '';
  tblfiltervalue: string = '';
  lienId: string = '';
  notFound = true;
  displayedColumns: string[] = ['joiningOrderNo', 'joiningOrderDate', 'relievingOrderNo', 'relievingOrderDate', 'relievingOffice', 'Status', 'action'];
  disableflag: boolean = true;
  disableForwardflag: boolean = false;
  insertEditFlag: any;
  @ViewChild('freject') form2: any;
  @ViewChild('formDirective') private formDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private master: MasterService, private _formBuilder: FormBuilder, private snackBar: MatSnackBar, private nonepsemployeejoinafterlienperiodService: NonepsemployeejoinafterlienperiodService) { }

  ngOnInit() {
    this.roleId = sessionStorage.getItem('userRoleID');
    this.btnUpdateText = 'Save';
    this.FormDetails();
    this.getAllDesignation(sessionStorage.getItem('controllerID'));
    this.getJoinServices();
    this.getJoiningAccountOf();
    this.getOfficeList();
    this.getOfficeCityClassList();
    this.getPayCommissionLien();
    // this.getAllDesignation(sessionStorage.getItem('controllerID'));
  }
  FormDetails() {
    this.form = this._formBuilder.group({
      'employeeCode': [this.empCd, Validators.required],
      'relievingOrderNo': [null, Validators.required],
      'relievingOrderDate': [null, Validators.required],
      'relievingOffice': [null],
      'relievingBy': [null],
      'relievedOn': [null],
      'onRelievedDesignation': [null],
      'payCommissionId': [null],
      'payCommission': [null],
      'payScale': [null],
      'basicPay': [null],
      'joinServiceAs': [null],
      'joiningonAccountof': [null],
      'joiningOrderNo': [null],
      'joiningOrderDate': [null],
      'joiningTime': [null],
      'officeId': [null],
      'officeCityClass': [null],
      'joiningDesigcode': [null],
      'createdBy': [null],
      'existRecordAfterLien': [null],
      'flagUpdate': [null],
      'msAfterLeinId':[null],
     
      'rejectionRemark': [null],

    })
  }
  getJoinServices() {
    this.master.getJoiningMode().subscribe(res => {
      debugger;
      this.joinService = res;
    })
  }
  getJoiningAccountOf() {
    debugger;
    this.master.getJoiningAccountOf().subscribe(res => {
      debugger;
      this.getJoiningAccount = res;
    })
  }

  getPayCommissionLien() {
    this.master.getPayCommissionListLien().subscribe(res => {
      debugger;
      this.getPayCommission = res;
    })
  }

  getOfficeList() {
    this.master.getOfficeList(sessionStorage.getItem('controllerID'), sessionStorage.getItem('ddoid')).subscribe(res => {
      debugger;
      this.getofficelist = res;
    })
  }
  
   ltrim(tblfiltervalue) {
     return tblfiltervalue.replace(/^\s+/g, '');
}
  applyFilter(filterValue: string) {
    debugger;
    this.tblfiltervalue=this.ltrim(this.tblfiltervalue);
   
    if (this.nonEpsLienPeriodData === null || this.nonEpsLienPeriodData === undefined) {
      this.notFound = false;
    } 
    this.nonEpsLienPeriodData.filter = filterValue.trim().toLowerCase();
    this.nonEpsLienPeriodData.filterPredicate = function (data, filter: string): boolean {
      debugger;
      return data.joiningOrderNo.toLowerCase().includes(filter) || data.joiningOrderDate.includes(filter) || data.relievingOrderNo.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.relievingOffice.toLowerCase().includes(filter);
    };
    if (this.nonEpsLienPeriodData.paginator) {
      this.nonEpsLienPeriodData.paginator.firstPage();
    }
    if (this.nonEpsLienPeriodData.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  getAllDesignation(controllerId: any) {
    this.master.GetAllDesignation(controllerId).subscribe(res => {

      debugger;
      this.designation = res;
    })
  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  getOfficeCityClassList() {
    this.master.getOfficeCityClassList().subscribe(res => {
      this.officeCityClassList = res;
    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  valuw: any=[];
  btnEditClick(lienId) {
    debugger;

    let value = this.nonEpsLienPeriodData.filteredData.filter(x => x.msAfterLeinId == lienId)[0]
    this.form.patchValue(value);
    this.form.enable();
    this.btnUpdateText = 'Update';
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableflag = true;
      this.disableForwardflag = true;
      //this.Btntxt = 'Save';
    }
  }
  btnInfoClick(lienId) {
    debugger;
    let value = this.nonEpsLienPeriodData.filteredData.filter(x => x.msAfterLeinId == lienId)[0]
    this.form.patchValue(value);
    this.form.disable();
   
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;
   
      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableForwardflag = false;
  
    }
  }
  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.rejectionRemark = '';
    this.DeletePopup = false;
  }
  confirmDelete() {
    debugger;
    debugger;
    let value = this.nonEpsLienPeriodData.filteredData.filter(x => x.msAfterLeinId == this.lienId)[0]
   let orderNo = value.joiningOrderNo;
    this.nonepsemployeejoinafterlienperiodService.deleteNonEpsTransferOffLienPeriod(this.empCd, orderNo).subscribe(result => {

      if (parseInt(result) >= 1) {

        this.snackBar.open('Record Successfully Deleted ', null, { duration: 4000 });
        this.formDirective.resetForm();
        this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
        this.Cancel();
      }
      else {
        this.snackBar.open('Record Not Deleted ', null, { duration: 4000 });
      }
    })

  }
  deleteRecordClick(lienId) {
    this.lienId = lienId;
  }
  GetcommanMethod(value) {
   
    this.empCd = value;
    this.form.get('employeeCode').setValue(value);
    this.getEpsEmpJoinOffAfterLienPeriodDetails(value, this.roleId, true, "");
    //this.form.get('employeeCode').setValue(value);
    // this.btnUpdateText = 'Save';
  }
  getEpsEmpJoinOffAfterLienPeriodDetails(Empcd: any, roleId: any, Flag: boolean, insertEditFlag: any) {
    this.nonepsemployeejoinafterlienperiodService.getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(Empcd, roleId).subscribe(result => {
      debugger;
      if (result.length == 0) {
        this.formDirective.resetForm();
        this.form.enable();
        this.nonEpsLienPeriodData = null;
        this.form.get('employeeCode').setValue(this.empCd);
      }
      else {
        this.nonEpsLienPeriodData = new MatTableDataSource(result);
        this.nonEpsLienPeriodData.paginator = this.paginator;
        this.nonEpsLienPeriodData.sort = this.sort;
   
        this.form.get('employeeCode').setValue(this.empCd);
        //this.disableForwardflag = true;
      }
    })
     


  }

  ResetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    this.form.enable();
    this.btnUpdateText = 'Save';
    this.disableForwardflag = false;
    this.form.get('employeeCode').setValue(this.empCd);
    //this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
  }

  forwardStatusUpdate(statusFlag: any) {
    debugger;
    debugger;
    var RejectionComment ='';
    let employeeId = this.empCd.trim();
    let joiningOrderNo = this.form.get('joiningOrderNo').value;
    if (statusFlag == 'R') {
      RejectionComment = this.rejectionRemark;
    }
    else {
      RejectionComment = ''
    }
   
    this.nonepsemployeejoinafterlienperiodService.forwardNonEpsEmployeeStatusUpdate(employeeId, joiningOrderNo, statusFlag, RejectionComment).subscribe(result1 => {
      if (parseInt(result1) >= 1) {

        this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
        this.form.reset();
        this.formDirective.resetForm();
        this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
        this.infoScreen = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
        this.btnUpdateText = 'Save';

        this.disableForwardflag = false;
      }
    });
  }

 

  onSubmit() {
    debugger;
    //this.submitted = true;

    this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    else {
      if (this.btnUpdateText == 'Update') {
        this.form.get('flagUpdate').setValue('update');

      }
      else {
        this.form.get('flagUpdate').setValue('insert');
        let employeeid = this.form.get('employeeCode').value;
        if (employeeid == 0) {
          alert("Select Employee");
          return;
        }
      }
      this.nonepsemployeejoinafterlienperiodService.InsertUpateNonEpsEmployeeJoinAfterLienPeriod(this.form.value).subscribe(result1 => {
        debugger;
 
        let resultvalue = result1.split("-");

        if (parseInt(resultvalue[0]) >= 1) {
          if (this.btnUpdateText == 'Update') {
            this.snackBar.open('Update Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
            this.btnUpdateText = 'Save';
            this.disableForwardflag = false;
          }
          else {
            this.snackBar.open('Save Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId, true, "");
            this.btnUpdateText == 'Save';
            this.disableForwardflag = false;
          }

        } else {
         
          if (this.btnUpdateText == 'Update') {
            if (resultvalue[2] = 'Duplicate Record') {
              this.snackBar.open('Joining Order Number  Already Exits', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Update not Successfully', null, { duration: 4000 });
            }
           
          }
          else if (resultvalue[2] = 'Duplicate Record') {
            this.snackBar.open('Joining Order Number  Already Exits', null, { duration: 4000 });
          }
          else{
            this.snackBar.open('Save not Successfully', null, { duration: 4000 });
          }
        }
      })
    }
  }

}

