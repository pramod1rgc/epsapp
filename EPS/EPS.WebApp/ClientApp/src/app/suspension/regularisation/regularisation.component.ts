
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DesignationModel, EmpModel } from '../../model/Shared/DDLCommon';
import { RegulariseDetails } from '../../model/Suspension/RegulariseDetails';
import { MatPaginator, MatSnackBar, MatSelect, MatExpansionPanel } from '@angular/material';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SuspensionService } from '../../services/Suspension/Suspension_service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-regularisation',
  templateUrl: './regularisation.component.html',
  styleUrls: ['./regularisation.component.css']
})

export class RegularisationComponent implements OnInit {

  objempDesignation: DesignationModel;
  objempModel: EmpModel;
  isCheckerLogin = false;
  selectedvalue = 0;
  isRejected = false;
  disableButton = false;
  isViewDetails = true;
  regularId: number;
  selectedEmpSusId: string;
  empId: number;
  objempRegulariseDetails: RegulariseDetails;
  successMsg: string;
  reasonText: string;
  deletepopup: boolean;
  isReasonEmpty: boolean;
  hooCheckerPopup: boolean;
  isVarified: boolean;
  checkerbtnDisable: boolean;
  RegulariseDetailsForm: FormGroup;
  @ViewChild('formCreation') formJoining: any;
  dataRegularHistory: MatTableDataSource<RegulariseDetails>;
  dataRegular: MatTableDataSource<RegulariseDetails>;

  regularHistoryColumns: string[] = ['SuspenCat', 'SuspenTreated', 'OrderNo', 'OrderDate', 'Remarks', 'Status'];
  @ViewChild('sortingRH') sortingRH: MatSort;
  @ViewChild('pagingRH') pagingRH: MatPaginator;

  regularColumns: string[] = ['SuspenCat', 'SuspenTreated', 'OrderNo', 'OrderDate', 'Remarks', 'Status', 'Action'];
  @ViewChild('sortingReg') sortingReg: MatSort;
  @ViewChild('pagingReg') pagingReg: MatPaginator;
  @ViewChild('singleSelect') desigddl: MatSelect;
  @ViewChild('exPanel') exPanel: MatExpansionPanel;

  suspenCats: any = [{ Id: 1, Name: 'Minor Penalty' },
  { Id: 2, Name: 'Major Penalty' },
  { Id: 3, Name: 'Exonerate' }];

  suspenPeriod: any = [{ Id: 1, Name: 'Duty' },
  { Id: 2, Name: 'Employee on Leave' },
  { Id: 3, Name: 'Dies Non' }];

  ngOnInit() {

    this.objempRegulariseDetails = new RegulariseDetails();
    if (+sessionStorage.getItem('userRoleID') === 9) {
      this.isCheckerLogin = false;
    } else {
      this.isCheckerLogin = true;
    }
    this.checkerbtnDisable = true;
    console.log(this.isCheckerLogin);
    this.GetAllDesignation();
    this.GetAllEmployees('0');
    this.reasonText = '';
    this.isReasonEmpty = false;
    this.isRejected = false;
   // console.log(this.suspenCats.find(x => x.Id === 1).Name);
  }

  constructor(
    private objsuspensionservice: SuspensionService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.CreateJoiningDetailsForm();
  }


  CreateJoiningDetailsForm() {
    this.RegulariseDetailsForm = this.fb.group({
      SuspenCat: ['', Validators.required],
      SuspenTreated: ['', Validators.required],
      OrderNo: ['', Validators.required],
      OrderDate: ['', Validators.required],
      Remarks: ['', ''],
      RejectedReason: ['', '']
    });
  }

  GetAllDesignation() {
    this.objsuspensionservice
      .GetAllDesignation('')
      .subscribe(res => {
        this.objempDesignation = res;
      });
  }

  GetAllEmployees(desigId: string) {
    const comName = 'regular';
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice
      .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
      .subscribe(res => {
        this.objempModel = res;
      });
  }

  ddlDesignationChanged(desigId: string) {
    this.desigddl.placeholder = null;
    if (Number(desigId) === -1) {
       this.selectedvalue = null;
     //  this.objempModel = this.allempList;
       this.desigddl.placeholder = 'Select Designation';
       desigId = '0';
    }
    this.GetAllEmployees(desigId);
  }

  ddlEmployeeChanged(empId: string) {
    debugger;
    //this.myform.reset();
    this.RegulariseDetailsForm.enable();
    this.RegulariseDetailsForm.reset();
    this.isViewDetails = true;
    this.checkerbtnDisable = true;
    this.selectedEmpSusId = empId.split('@')[0].trim();
    this.selectedvalue = Number(empId.split('@')[1]);
    this.empId = Number(empId.split('@')[2]);
    console.log(this.selectedEmpSusId);
    this.BindJoiningDetails(Number(this.selectedEmpSusId));

  }

  BindJoiningDetails(selectedvalue: number) {
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice.GetRegulariseDetails(this.empId, checkerLogin).subscribe((result: any) => {
      console.log(result);

      result.forEach(item => {item.susCatname = this.suspenCats.find(b => b.Id === item.suspenCat).Name;
        item.sustreatedName = this.suspenPeriod.find(x => x.Id === item.suspenTreated).Name; });

      this.dataRegular = new MatTableDataSource<RegulariseDetails>(result.filter(obj => obj.flagStatus.trim() !== 'V'));
      this.dataRegular.paginator = this.pagingReg;
      this.dataRegular.sort = this.sortingReg;

      this.exPanel.disabled = false;
      if (this.dataRegular.data.length > 0) {
        this.exPanel.close();
        this.exPanel.disabled = true;
      }

      this.dataRegularHistory = new MatTableDataSource<RegulariseDetails>(result.filter(obj => obj.flagStatus.trim() === 'V'));
      this.dataRegularHistory.paginator = this.pagingRH;
      this.dataRegularHistory.sort = this.sortingRH;

    });



  }

  SavedRegulariseDetails(objRegular: RegulariseDetails): void {
    debugger;
    console.log(objRegular);
    this.disableButton = true;
    objRegular.JoiningId = Number(this.selectedEmpSusId);
    objRegular.RegulariseId = this.regularId;

    if (objRegular.FlagStatus === void 0) { objRegular.FlagStatus = 'RG'; }
    this.objsuspensionservice.SavedRegulariseDetails(objRegular).subscribe((arg: any) => {

      this.successMsg = arg;
      if (+arg > 0) {
        this.successMsg = 'Record saved successfully';
        this.regularId = 0;
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
        this.disableButton = false;
        this.RegulariseDetailsForm.reset();
      }


      // this.objempExtDetails.Id = 0;
      // this.objempExtDetails.Status = null;
      // this.BindSuspensionDetails(this.selectedEmpCode);
      this.snackBar.open(this.successMsg, null, { duration: 4000 });
    });

  }

  ddlSuspenCat(catId: number) {
    debugger;
    this.objempRegulariseDetails.SuspenTreated = null;
    //const susTreatedddl = this.RegulariseDetailsForm.get('SuspenTreated');
    switch (catId) {
      case 1: {
        this.suspenPeriod = [{ Id: 1, Name: 'Duty' },
        { Id: 2, Name: 'Employee on Leave' }];
        break;
      }
      case 2: {
        this.suspenPeriod = [{ Id: 2, Name: 'Employee on Leave' },
        { Id: 3, Name: 'Dies Non' }];
        break;
      }
      default: {
        this.suspenPeriod = [{ Id: 1, Name: 'Duty' },
        { Id: 2, Name: 'Employee on Leave' },
        { Id: 3, Name: 'Dies Non' }];
        break;
      }
    }
    // susTreatedddl.setValidators(Validators.required);
    // susTreatedddl.updateValueAndValidity();

  }

  applyFilter(filterValue: string) {
    if (this.isCheckerLogin) {
      this.dataRegular.filter = filterValue.trim().toLowerCase();
      if (this.dataRegular.paginator) {
        this.dataRegular.paginator.firstPage();
      }
    } else {
      this.dataRegularHistory.filter = filterValue.trim().toLowerCase();
      if (this.dataRegularHistory.paginator) {
        this.dataRegularHistory.paginator.firstPage();
      }
    }

  }


  btnEditClick(objReg: any, isViewOnly: boolean) {
    debugger;
    this.exPanel.disabled = false;
    this.exPanel.open();
    this.objempRegulariseDetails.RegulariseId = objReg.regulariseId;
    this.objempRegulariseDetails.SuspenCat = objReg.suspenCat;
    this.objempRegulariseDetails.SuspenTreated = objReg.suspenTreated;
    this.objempRegulariseDetails.OrderNo = objReg.orderNo;
    this.objempRegulariseDetails.OrderDate = objReg.orderDate;
    this.objempRegulariseDetails.FlagStatus = objReg.flagStatus;
    this.objempRegulariseDetails.Remarks = objReg.remarks;
    this.regularId = objReg.regulariseId;
     console.log(objReg.empJoiningId);
    this.isRejected = false;
    if (objReg.rejectedReason !== '') {
      this.objempRegulariseDetails.RejectedReason = objReg.rejectedReason;
      this.isRejected = true;
    }
    this.RegulariseDetailsForm.enable();
    this.isViewDetails = !isViewOnly;

    if (this.isCheckerLogin) {
      this.RegulariseDetailsForm.disable();
    }

    // if (this.isCheckerLogin) {
    // this.myform.form.disable();
    //  this.myform.controls.forEach(function(element) {
    //    element.disable();
    //  });
    // }


    if (isViewOnly) {
      this.RegulariseDetailsForm.disable();
      this.isViewDetails = false;
    }
  }

  ForwardToChecker(obj: RegulariseDetails): void {
    obj.FlagStatus = 'F';
    this.SavedRegulariseDetails(obj);
  }

  DeleteJoiningDetails(id: number, flag: boolean) {
    debugger;

    this.regularId = flag ? id : 0;
    if (!flag) {
      this.objsuspensionservice.DeleteSuspensionDetails(id, 'joining').subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }
    // $('.dialog__close-btn').click();
    this.deletepopup = flag;
  }


  VarifiedJoining(id: number, flag: boolean): void {

    console.log(id);

    if (!flag && id > 0) {
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_regular', 'V', '').subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Varified successfully';
          this.regularId = 0;
          this.BindJoiningDetails(Number(this.selectedEmpSusId));
          this.RegulariseDetailsForm.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }

    this.hooCheckerPopup = flag;
    this.isVarified = flag;
  }

  RejectedJoining(id: number, flag: boolean): boolean {
debugger;
    console.log(id);

    if (!flag && id > 0) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        this.successMsg = 'Please enter reason';
        return false;
      }

      if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
        this.isReasonEmpty = true;
        this.successMsg = 'Special characters are not allowed';
        return false;
      }
      this.successMsg = null;

      this.hooCheckerPopup = flag;
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_regular', 'R', this.reasonText).subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Rejected successfully';
          this.regularId = 0;
          this.BindJoiningDetails(Number(this.selectedEmpSusId));
          this.RegulariseDetailsForm.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      return false;
    }
    this.hooCheckerPopup = flag;
    this.isVarified = !flag;
    return false;
  }

}

