import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuspensionModule } from './suspension.module';
import { JoiningComponent } from './joining/joining.component';
import { SuspensionComponent } from './suspension/suspension.component';
import {ExtensionComponent} from './extension/extension.component';
import {RevocationComponent} from './revocation/revocation.component';
import {RegularisationComponent} from './regularisation/regularisation.component';


const routes: Routes = [
  {
    path: '', component: SuspensionModule, children: [

      { path: 'Joining', component: JoiningComponent },
      { path: 'Suspension', component: SuspensionComponent },
      { path: 'Extension', component: ExtensionComponent },
      { path: 'Revocation', component: RevocationComponent },
      { path: 'Regularisation', component: RegularisationComponent }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuspensionRoutingModule {

}
