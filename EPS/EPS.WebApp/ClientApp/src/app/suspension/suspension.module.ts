import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuspensionRoutingModule } from './suspension-routing.module';
import { MaterialModule } from '../material.module';
import { JoiningComponent } from './joining/joining.component';
import { SuspensionComponent } from './suspension/suspension.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { ExtensionComponent } from './extension/extension.component';
import { RevocationComponent } from './revocation/revocation.component';
import { RegularisationComponent } from './regularisation/regularisation.component';
//import { SharecomponentModule } from '../sharecomponent/sharecomponent.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [JoiningComponent, SuspensionComponent, ExtensionComponent, RevocationComponent, RegularisationComponent],
  imports: [CommonModule, SuspensionRoutingModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, MatTooltipModule,
    NgxMatSelectSearchModule]
})
export class SuspensionModule { }

