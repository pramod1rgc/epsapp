import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvincrementService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  CreateAdvIncrement(advObj) {
    return this.httpclient.post(this.config.CreateAdvIncrement, advObj, { responseType: 'text' });
  }

  getAllDesignation(PermDdoId: string): Observable<any> {
    const params = new HttpParams().set('PermDdoId', PermDdoId);
    return this.httpclient.get<any>(`${this.config.GetEmployeeDesignation}/GetEmployeeDesignation`, { params });
  }

  GetIncrementDetails(empcode): Observable<any> {
    return this.httpclient.get<any>(this.config.GetIncrementDetails + empcode, {});
  }

  EditAdvIncrement(incID): Observable<any> {
    return this.httpclient.get<any>(this.config.EditAdvIncrement + incID, {});
  }
  

  DeleteAdvDetails(incID: number) {
    return this.httpclient.post(this.config.DeleteAdvDetails + incID, '', { responseType: 'text' });

  }

  Forwardtochecker(advObj) {
    debugger;
    return this.httpclient.post(this.config.Forwardtochecker, advObj, { responseType: 'text' });
  }

  GetEmployeeByDesigPayComm(msDesignID?: string, paycomm?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetEmployeeByDesigPayComm + '?msDesignID=' + msDesignID + ' &paycomm=' + paycomm, {});
  }

}
