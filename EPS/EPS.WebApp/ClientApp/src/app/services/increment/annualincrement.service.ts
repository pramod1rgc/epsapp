import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnnualincrementService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  getAllDesignation(PermDdoId: string): Observable<any> {
    const params = new HttpParams().set('PermDdoId', PermDdoId);
    return this.httpclient.get<any>(`${this.config.GetEmployeeDesignation}/GetEmployeeDesignation`, { params });
  }

  GetSalaryMonth(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetSalaryMonth, {});
  }

  GetOrderWithEmployee(paycode?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetOrderWithEmployee + '?paycodeID=' + paycode, {});
  }

  GetAnnualIncReportData(paycode?: string, orderID?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetAnnualIncReportData + '?paycodeID=' + paycode + '&orderID=' + orderID, {});
  }

  GetEmpDueForIncReport(designCode?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetEmpDueForIncReport + '?designCode=' + designCode , {});
  }

}
