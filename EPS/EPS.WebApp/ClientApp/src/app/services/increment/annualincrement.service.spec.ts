import { TestBed } from '@angular/core/testing';

import { AnnualincrementService } from './annualincrement.service';

describe('AnnualincrementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnnualincrementService = TestBed.get(AnnualincrementService);
    expect(service).toBeTruthy();
  });
});
