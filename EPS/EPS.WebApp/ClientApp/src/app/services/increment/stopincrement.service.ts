import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StopincrementService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  getEmployeeStopIncrement(orderID?: number): Observable<any> {
    debugger;
    return this.httpclient.get<any>(this.config.GetEmployeeStopIncrement + '?orderID= ' + orderID, {});
  }

  getEmployeeForNotStop(orderID?: number): Observable<any> {
    return this.httpclient.get<any>(this.config.GetEmployeeForNotStop + '?orderID= ' + orderID, {});
  }


  insertStopIncrementData(advObj) {
    return this.httpclient.post(this.config.InsertStopIncrementData, advObj, { responseType: 'text' });
  }


  forwardcheckerForStopInc(advObj) {
    return this.httpclient.post(this.config.ForwardcheckerForStopInc, advObj, { responseType: 'text' });
  }
}
