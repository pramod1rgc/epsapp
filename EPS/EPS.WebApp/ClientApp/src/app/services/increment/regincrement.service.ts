import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { regularModel } from '../../increment/regular-increment/regularmodel';
import { FormArray } from '@angular/forms';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RegincrementService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  createOrderNo(advObj) {
    return this.httpclient.post(this.config.CreateOrderNo, advObj, { responseType: 'text' });
  }

  getAllDesignation(PermDdoId: string): Observable<any> {
    const params = new HttpParams().set('PermDdoId', PermDdoId);
    return this.httpclient.get<any>(`${this.config.GetEmployeeDesignation}/GetEmployeeDesignation`, { params });
  }
  

  getOrderDetails(design?: string, paycode?: string, orderType?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetOrderDetails + '?designID=' + design + ' &paycodeID=' + paycode + '&orderType= ' + orderType, {});
  }
  

  GetEmployeeForIncrement(design?: string, paycode?: string, orderID?: number): Observable<regularModel[]> {
    return this.httpclient.get<regularModel[]>(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
  }


  getAllAsFormArray(design?: string, paycode?: string, orderID?: number): Observable<FormArray> {
    return this.GetEmployeeForIncrement(design, paycode, orderID).pipe(map((albums: regularModel[]) => {
      // Maps all the albums into a formGroup defined in tge album.model.ts
      const fgs = albums.map(regularModel.asFormGroup);
      debugger
      return new FormArray(fgs);
    }));
  }
   

  getAllAsFormArray11(design?: string, paycode?: string, orderID?: number): Observable<regularModel[]> {
    return this.httpclient.get<regularModel[]>(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
  }

  getEmployeeForIncrements(design?: string, paycode?: string, orderID?: number): Observable<any> {
    return this.httpclient.get<any>(this.config.GetEmployeeForIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
  }

  getEmployeeForNonIncrement(design?: string, paycode?: string, orderID?: number): Observable<any> {
    return this.httpclient.get<any>(this.config.GetEmployeeForNonIncrement + '?designID=' + design + ' &paycodeID=' + paycode + '&orderID= ' + orderID, {});
  }

 
  insertRegIncrementData(advObj) {
    return this.httpclient.post(this.config.AddEmpForIncrement,advObj , { responseType: 'text' });
  }


  forwardtocheckerForInc(advObj) {
    return this.httpclient.post(this.config.ForwardtocheckerForInc, advObj, { responseType: 'text' });
  }

  deleteOrderDetails(id: number,type:string) {
    return this.httpclient.post(this.config.deleteOrderDetails + '?id=' + id + '&type= ' + type,'' , { responseType: 'text' });
  }

  //deleteHraMaster(hraMasterID: number) {
  //  return this.httpclient.post(this.config.DeleteHraMaster + hraMasterID, '', { responseType: 'text' });
  //}
}
