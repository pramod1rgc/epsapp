import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { OnBoardingModel } from '../../model/UserManagementModel/OnBoardingModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OnBoardingService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  OnBoardingSubmit(ObjOnBoarding) {
    return this.httpclient.post(this.config.OnBoardingSubmit, ObjOnBoarding, { responseType: 'text' });
  }

  GetAllReuestNoOfOnboarding(): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.GetAllReuestNoOfOnboarding, {});
  }

  FetchReuestLetterNoRecord(OnboardingId): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.FetchRequestLetterNoRecord + OnboardingId, {});
  }
}
