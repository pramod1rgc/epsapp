import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class controllerservice {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {    
  }

  getAllControllerServiceclient(controllerID, Query) :Observable<any[]> {
    return this.httpclient.get<any[]>( this.config.getAllControllers +controllerID + '&Query=' + Query,{});
  }
  
  GetAllpaos(controllerID, Query) {
    return this.httpclient.get(this.config.GetAllpaos + controllerID + '&Query=' + Query, {});
  }

  GetAllDDO(PAOID, Query) {
    return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
  }

  oncontrollerroleSelectchanged(MsControllerID, PAOID,DDOID) {
    return this.httpclient.get(this.config.controllerchanged + MsControllerID + '&PAOID=' + PAOID +'&DDOID='+ DDOID, {});
  }

  PaosAssigned(empCd, MsControllerID, Active) {
    return this.httpclient.get( this.config.PaosAssigned + empCd + '&MsControllerID=' + MsControllerID + '&Active=' + Active, { responseType: 'text'});

  }
  
  AssignedEmp(ControllerID) {
    return this.httpclient.get(this.config.AssignedEmp + ControllerID, {});
  }
}
