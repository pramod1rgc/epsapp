import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RoleService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
   
  } 

  
  GetAllDataRollList(username): Observable<any> {
    //alert("all");
    return this.httpclient.get<any>(`${this.config.getAllRole +'?username='+username}`);
  }


  GetAllActiveRollList(username): Observable<any> {
   // alert("all");
    return this.httpclient.get<any>(`${this.config.getAllActiveRollList + '?username=' + username}`);
  }
  GetAllCustomeRollList(username): Observable<any> {
    //alert("all");
    return this.httpclient.get<any>(`${this.config.GetAllCustomeRollList + '?username=' + username}`);
  }
  GetBindUserListddl(RdText, SelectControllerID): Observable<any> {
   // alert(sessionStorage.getItem('controllerID'));
    debugger;
   
    //alert(sessionStorage.getItem('UserID'));
    return this.httpclient.get<any>(`${this.config.GetBindUserListddl + '?RdStatus=' + RdText + '&LoggRollID=' + sessionStorage.getItem('userRoleID') + '&LoogInUser=' + sessionStorage.getItem('UserID') + '&ControleerID=' + SelectControllerID + '&paoid=' + sessionStorage.getItem('paoid') + '&ddoid=' + sessionStorage.getItem('ddoid') + '&CurrUserControllerID=' + sessionStorage.getItem('controllerID')}`);
  }
  SaveNewRole(MstRole) {
     return this.httpclient.post(this.config.saveNewRole, MstRole, { responseType: 'text' });
  }
  GetUserDetails(UserRole): Observable<any> {
    //
    return this.httpclient.post(this.config.getUserDetails, UserRole);
  }
  RevokeUser(MstRole):Observable<any>  {
      return this.httpclient.post(this.config.revokeUser, MstRole, { responseType: 'text' });
  }
  SaveAssignMenuPermission() {
    return this.httpclient.get(this.config.getAllRole, {});
  }
  Search(MstRole): Observable<any>  {
   // alert(this.config.getUserDetailsByPan);
      return this.httpclient.post(this.config.getUserDetailsByPan, MstRole);
  }
  GetAssignRoleForUser(roleID) {
    //alert(this.config.getAssignRoleForUser + "-" + roleID);
    //debugger;
    return this.httpclient.post(this.config.getAssignRoleForUser + roleID, {});//this.config.getAssignRoleForUser, MstRole);
  }
  SaveAssignRoleForUser(MstRole) {
      return this.httpclient.post(this.config.saveAssignRoleForUser, MstRole, { responseType: 'text' });
  }
  TreestructureList(roleID) {
    return this.httpclient.post(this.config.getAllMenuList + '?roleID=' + roleID + '&Uname=' + sessionStorage.getItem('username') + '&CurrURole=' + sessionStorage.getItem('userRoleID'), {});
  }
  SaveMenuPerMission(iDsStatusarr) {
    return this.httpclient.post(this.config.saveMenuPerMission, iDsStatusarr, { responseType: 'text' });

  }
  CheckMenuPerMission(iDsStatusarr) {
    return this.httpclient.post(this.config.checkMenuPerMission, iDsStatusarr, { responseType: 'text' });
  }
}
