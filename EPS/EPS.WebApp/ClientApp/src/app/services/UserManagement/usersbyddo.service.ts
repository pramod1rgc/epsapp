import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class usersbyddoservice {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }


  GetAllpaos(controllerID, Query) {
    return this.httpclient.get(this.config.GetAllpaos + controllerID + '&Query=' + Query, {});
  }
  onpaoSelectchanged(MsPAOID) {
    return this.httpclient.get(this.config.onpaoSelectchanged + MsPAOID, {});
  }

  AssignedCMD(empCd,RoleID, DDOID,UserID, Active) {
    return this.httpclient.get(this.config.AssignedCMD + empCd + '&RoleID=' + RoleID + '&DDOID=' + DDOID + '&UserID=' + UserID + '&Active=' + Active, { responseType: 'text' });
  }

  GetAllUserRolesByDDO(username) {
    return this.httpclient.get(this.config.GetAllUserRolesByDDO + username, {});
  }

  //For Assigned Employee
  AssignedDDOCheckerEmpList(DDOID) {
    return this.httpclient.get(this.config.AssignedDDOCheckerEmpList + DDOID, {});
  }

  //For  Employee list
  EmployeeListOfDDO(DDOID) {
    return this.httpclient.get(this.config.EmployeeListOfDDO + DDOID, {});
  }

  AssignChecker(empCd, RoleID, DDOID, Active) {
    return this.httpclient.get(this.config.AssignChecker+ empCd + '&RoleID=' + RoleID + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
  }

  //AssingedDDOCheckerMaker() {
  //  return this.httpclient.get(this.config.AssingedDDOCheckerMaker, {});
  //}

  SelfAssignDDOChecker(UserID, username, EmpPermDDOId, ddoid, IsAssign) {
    return this.httpclient.get(this.config.SelfAssignDDOChecker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
  }
  SelfAssignCheck(UserID) {
   
    return this.httpclient.get(this.config.SelfAssignCheck + UserID , { responseType: 'text' });
  }
}
