import { TestBed } from '@angular/core/testing';

import { PayscaleService } from './payscale.service';

describe('PayscaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PayscaleService = TestBed.get(PayscaleService);
    expect(service).toBeTruthy();
  });
});
