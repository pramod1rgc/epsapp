import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class StandardDeductionService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getEntertainMaster(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetStandardDeductionEntertainMaster`);
  }

  getStandardDeductionRule(pageNumber, pageSize, searchTerm): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetStandardDeductionRule?PageNumber=${pageNumber}&PageSize=${pageSize}&SearchTerm=${searchTerm}`);
  }

  upsertStandardDeductionRule(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/UpsertStandardDeductionRule`, obj, { responseType: 'text' });
  }

  deleteStandardDeductionRule(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/DeleteStandardDeductionRule`, obj, { responseType: 'text' });
  }

  deleteStandardDeductionRateDetail(id): Observable<any> {
    return this.http.delete(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/DeleteStandardDeductionRateDetail/${id}`);
  }
}
