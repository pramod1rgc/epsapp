import { TestBed } from '@angular/core/testing';

import { ExemptionDeductionService } from './exemption-deduction.service';

describe('ExemptionDeductionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExemptionDeductionService = TestBed.get(ExemptionDeductionService);
    expect(service).toBeTruthy();
  });
});
