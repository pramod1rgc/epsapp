import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module'
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  getAllMenusByUser(username, uroleid) {
    return this.httpclient.get(this.config.getAllMenusByUser + "?username=" + username + "&uroleid=" + uroleid, {});
  }
  getDashboardDetail() {
    return this.httpclient.get(this.config.api_base_url + this.config.getDashboardDetal);
  }
 
}
