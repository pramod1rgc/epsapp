import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { newloanModel } from '../../model/newloanModel';

@Injectable({
  providedIn: 'root'
})
export class NewLoanService {
  private _id: string; 
  private _type: string;
  private _roleid: number;
  private _ddoid: number;
  private _hiddenid: number;
  private _list:string[];
  get list() {
    return this._list;
  }
  set list(val) {
    this._list = val;
  }
  get LoanCode() {
    return this._id;
  }
  set LoanCode(val) {
    this._id = val;
  }
  get PurposeCode() {
    return this._type;
  }
  set PurposeCode(val) {
    this._type = val;
  }
  get RoleId() {
    return this._roleid;
  }
  set RoleId(val) {
    this._roleid = val;
  }

  get DdoId() {
    return this._ddoid;
  }
  set DdoId(val) {
    this._ddoid = val;
  }


  get HiddenId() {
    return this._hiddenid;
  }
  set HiddenId(val) {
    this._hiddenid = val;
  }
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  BindLoanStatus(mode: number, userid:number) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetLoanDetails + mode + '&userid=' + userid}`);
  }
  FwdtochkerDetails() {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetFwdtochkerDetails}`);
  }
  GetEmpLoanDetailsByID(EmpCd: newloanModel): Observable<any> {

    return this.http
      .get<newloanModel>(`${this.config.api_base_url}${this.config.GetEmpLoanDetailsByID + EmpCd}`);
  }
  EditLoanDetailsByEMPID(id: number): Observable<any> {
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.EditLoanDetailsByEMPID + id}`);
  }
  UpdateLoanDetailsbyID(id: number, mode: number, remarks: string, priVerifFlag: number): Observable<any> {
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.UpdateLoanDetailsbyID + id + '&mode=' + mode + '&remarks=' + remarks + '&priVerifFlag=' + priVerifFlag }`);
  }
  //DeleteLoanDetailsByEMPID(id: number, mode: number): Observable<any> {
  //  return this.http
  //    .get<any>(`${this.config.api_base_url}${this.config.DeleteLoanDetailsByEMPID + id + '&mode=' + mode}`);
  //}
  EMPLoanPurposeByLoanCode(payLoanCode: string, empCode: string) {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetLoanpurposebyLoanCode + payLoanCode + '&empCode=' + empCode}`);
  }
  EMPLoanPurposeCode(payLoanCode: string,purposecode:string) {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetDetailsbyLoanCodePurposecode + payLoanCode + '&purposecode=' + purposecode}`);
  }
  EmployeeName(MsDesignID: string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetAllEmpNameSelectedByDesignId + MsDesignID}`);
  }
  EmployeeInfo(EmpCode: string, MsDesignID: string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpDetailsbyEmpcode + EmpCode+'&MsDesignID='+MsDesignID}`);
  } 

  Loantypeandpurposestatus(LoanBillID: string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayLoanPurposeStatus + LoanBillID}`);
  }
  EmployeeSaveInfo(formData): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.PostLoanDetails, formData, { responseType: 'text' });
  }
  PropertySaveInfo(formData): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.PostPropertyOwnerDetails, formData, { responseType: 'text' });
  }

  EmployeeUpdateInfo(employeedetails): Observable<any> {
    return this.http.put(this.config.api_base_url + this.config.UpdateLoanDetails, employeedetails, { responseType: 'text' });
  }


  ValidateCoolingPeriod(LoanCode: number, PurposeCode: number, EmpCd: string): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.ValidateCoolingPeriod + LoanCode + '&PurposeCode=' + PurposeCode + '&EmpCd=' + EmpCd}`);
  }

  //Loantypeandpurposestatus(LoanBillID: string) {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayLoanPurposeStatus + LoanBillID}`);
  //}
    //BindLoanType() {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.GetLoanType}`);
  //}

  //GetEmpCode(username: string): Observable<any> {

  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpDetails + username}`);
  //}


   //BindDesignation(): Observable<any> {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
  //}
  //BindBillGroup(): Observable<any> {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownBillGroup}`);
  //}
  //BindDropDownEmp(): Observable<any> {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownEmp}`);
  //}

  //GetAllPayBillGroup(PermDdoId: string): Observable<any> {
  //  const params = new HttpParams().set('permddoId', PermDdoId);
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetPayBillGroup`, { params });
  //}
  //GetAllDesignation(PayBillGrpId: string): Observable<any> {
  //  const params = new HttpParams().set('billGroupId', PayBillGrpId);
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation`, { params });
  //}
  //GetAllEmp(DesigCode: string): Observable<any> {
  //  const params = new HttpParams().set('desigId', DesigCode).set('pageCode', 'Leaves');
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetEmp`, { params });
  //}

  //UpdateLoanSanctionDetails(loandetails): Observable<any> {
  //  return this.http.put(this.config.api_base_url + this.config.UpdateSanctionDetails, loandetails, { responseType: 'text' });

  //}

  //GetSanctionDetails(EmpCode: string, MsDesignID: string, mode: number) {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.GetSanctionDetailsbyEmpcode + EmpCode + '&MsDesignID=' + MsDesignID + '&mode=' + mode}`);
  //}
}
