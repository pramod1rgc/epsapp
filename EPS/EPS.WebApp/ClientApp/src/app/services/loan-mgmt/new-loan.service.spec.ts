import { TestBed } from '@angular/core/testing';

import { NewLoanService } from './new-loan.service';

describe('NewLoanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewLoanService = TestBed.get(NewLoanService);
    expect(service).toBeTruthy();
  });
});
