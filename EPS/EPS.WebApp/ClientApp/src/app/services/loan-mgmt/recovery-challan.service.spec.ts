import { TestBed } from '@angular/core/testing';

import { RecoveryChallanService } from './recovery-challan.service';

describe('RecoveryChallanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecoveryChallanService = TestBed.get(RecoveryChallanService);
    expect(service).toBeTruthy();
  });
});
