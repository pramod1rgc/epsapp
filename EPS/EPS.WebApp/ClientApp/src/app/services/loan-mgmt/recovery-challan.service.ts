import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { newloanModel } from '../../model/newloanModel';

@Injectable({
  providedIn: 'root'
})
export class RecoveryChallanService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  RecoveryChallan(EmpCode: string, MsDesignID: string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetRecoveryChallan + EmpCode + '&MsDesignID=' + MsDesignID}`);
  }

  //Insert Update recovery challan
  InsertUpateRecoveryChallan(recoveryChallanDetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateRecoveryChallan, recoveryChallanDetails, { responseType: 'text' })
  }
  //--end
}
