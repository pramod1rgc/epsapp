import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SanctionService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  UpdateLoanSanctionDetails(loandetails): Observable<any> {
    return this.http.put(this.config.api_base_url + this.config.UpdateSanctionDetails, loandetails, { responseType: 'text' });

  }

  GetSanctionDetails(EmpCode: string, MsDesignID: string, mode: number) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetSanctionDetailsbyEmpcode + EmpCode + '&MsDesignID=' + MsDesignID + '&mode=' + mode}`);
  }
}
