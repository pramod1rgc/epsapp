import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { FloodModel } from '../../model/LoanModel/floodModel';

@Injectable({
  providedIn: 'root'
})

export class FloodService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    //this.BaseUrl = 'http://localhost:55424/api/';
  }

  //constructor(private http: HttpClient,
  //  @Inject(APP_CONFIG) private config: AppConfig
  //) { }

  //GetEmpDetails(BillGrID: string, desigId: string, UserID:number ): Observable<any> {
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus + BillGrID + desigId + UserID}`);
  //}


  GetEmpDetails(BillGrID: string, desigId: string, UserID, mode: string): Observable<any> {
    debugger;
    const params = new HttpParams().set('BillGrID', BillGrID).set('desigId', desigId).set('UserID', UserID).set('mode', mode);
    return this.httpclient.get<any>(`${this.config.FloodController}/GetEmpDetails`, { params });
    //return null;
  }

  //InsertFloodData(ArrLoanData) {
  //  debugger;
  //  const params = new HttpParams().set('ArrLoanData', ArrLoanData);
  //  return this.httpclient.post<any>(`${this.config.FloodController}/InsertFloodData`, { params });
  //}


  InsertFloodData(ArrLoanData) {
    debugger;
    //const paramtr = new HttpParams().set('EmployeeCode', EmployeeCode);
   // const params = new HttpParams().set('EmployeeCode', EmployeeCode);
    return this.httpclient.post(`${this.config.FloodController}/InsertFloodData`, ArrLoanData, { responseType: 'text'});
  }


  ForwordToDdo(ArrLoanData) {
    debugger;
    //const paramtr = new HttpParams().set('EmployeeCode', EmployeeCode);
    // const params = new HttpParams().set('EmployeeCode', EmployeeCode);
    return this.httpclient.post(`${this.config.FloodController}/ForwordToDdo`, ArrLoanData, { responseType: 'text' });
  }






}
