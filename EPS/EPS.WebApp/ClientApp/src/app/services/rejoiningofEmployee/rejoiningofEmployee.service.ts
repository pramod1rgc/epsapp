import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RejoiningOfEmployeeService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  GetServiceEndEmployees(empCode: string, empName: string, empPanNo: string): Observable<any> {
    const params = new HttpParams().set('empCode', empCode).set('empName', empName).set('empPanNo', empPanNo); // controllerId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.ReJoiningServiceControllerName}/GetServiceEndEmployees`, { params });
  }

  GetReJoiningServiceEmployees(pageNumber: number, pageSize: number, SearchTerm: string, empCd: string, roleId: any): Observable<any> {
    const params = new HttpParams().set('pageNumber', pageNumber.toString()).set('pageSize', pageSize.toString()).set('SearchTerm', SearchTerm.toString()).set('empCd', empCd.toString()).set('roleId', roleId); // controllerId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.ReJoiningServiceControllerName}/GetReJoiningServiceEmployees`, { params });
  }

  UpsertReJoiningServiceDetails(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.ReJoiningServiceControllerName}/UpsertReJoiningServiceDetails`,
      obj, { responseType: 'text' });
  }

  DeleteReJoiningServiceDetails(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.ReJoiningServiceControllerName}/DeleteReJoiningServiceDetails`,
      obj, { responseType: 'text' });
  }

  UpdateReJoiningStatusDetails(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.ReJoiningServiceControllerName}/UpdateReJoiningStatusDetails`,
      obj, { responseType: 'text' });
  }
}
