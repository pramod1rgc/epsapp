import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { MasterModel } from '../../model/masterModel';
import { EmployeeTypeMaster } from '../../model/masterModel'



@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private http: HttpClient,@Inject(APP_CONFIG) private config: AppConfig
  ) { }


  getSalutation(): Observable<MasterModel> {
    return this.http
      .get<MasterModel>(`${this.config.api_base_url}${this.config.getSalutation}`);  
  }

  //getEmployeeType1(): Observable<EmployeeTypeMaster> {
   
  //  return this.http
  //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeType}`);
  //}
  getEmployeeType(serviceId?: string, deputId?: string): Observable<EmployeeTypeMaster> {
    const params = new HttpParams().set('serviceId', serviceId).set('deputationId', deputId);
    return this.http
      .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeType}`, { params });
  }
  //getEmployeeSubType1(parentId: any): Observable<EmployeeTypeMaster> {
  //  debugger
  //  const params = new HttpParams().set('parentTypeID', parentId)
  //  return this.http
  //    .get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`,{ params });
  //}
  getEmployeeSubType(parentId: number, isDeput?: string): Observable<any> {
   
  //  const params = new HttpParams().set('parentTypeID', parentId).set('IsDeput', isDeput);
 //   return this.http.get<EmployeeTypeMaster>(`${this.config.api_base_url}${this.config.getEmployeeSubType}`, { params });
    return this.http.get(this.config.api_base_url + this.config.getEmployeeSubType + '?parentTypeId=' + parentId + ' &IsDeput=' + isDeput, {});
  }

  //getJoiningMode1(): Observable<any> {
  //  return this.http
  //    .get<any>(`${this.config.api_base_url}${this.config.getJoiningMode}`);
  //}

  getJoiningMode(isDeput?: boolean, empSubTypeId?: string): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.getJoiningMode + '?isDeput=' + isDeput + '&MsEmpSubTypeID=' + empSubTypeId, {});
  }

  getJoiningCategory(parentId: number, isDeput?: string): Observable<any> {
   
    return this.http.get(this.config.api_base_url + 'master/getJoiningCategory' + '?parentTypeId=' + parentId + '&isDeput=' + isDeput, {});
  }
  
  //getDeputationType1(): Observable<MasterModel> {
   
  //  return this.http
  //    .get<MasterModel>(`${this.config.api_base_url}${this.config.getDeputationType}`);
  //}

  getDeputationType(serviceId?: string): Observable<any> {
    const params = new HttpParams().set('serviceId', serviceId);
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.getDeputationType}`, { params });
  }

  //getServiceType1(): Observable<any> {
   
  //  return this.http
  //    .get<any>(`${this.config.api_base_url}${this.config.getServiceType}`);
  //}

  getServiceType(isDeput?: string): Observable<any> {
    const params = new HttpParams().set('isDeput', isDeput);
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.getServiceType}`, { params });
  }

  getGender(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getGender}`);

  }
  getState(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetState}`);

  }

  uploadFile(uploadFiles) {

    return this.http.post(this.config.api_base_url + this.config.uploadFiles, uploadFiles, { responseType: 'text' });
  }

  getRelation(): Observable<MasterModel> {
    return this.http
      .get<MasterModel>(`${this.config.api_base_url}${this.config.getRelation}`);
  }

  GetAllDesignation(controllerId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getDesignationAll + controllerId}`);
  }

  getJoiningAccountOf(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getJoiningAccountOf}`);
  }

  getOfficeList(controllerId: any, ddoId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getOfficeList + '?controllerId= ' + controllerId + '&ddoId=' + ddoId}`);
  }
  getOfficeCityClassList(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getOfficeCityClass}`);
  }
  getPayCommissionListLien(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayCommissionListLien}`);
  }
}
