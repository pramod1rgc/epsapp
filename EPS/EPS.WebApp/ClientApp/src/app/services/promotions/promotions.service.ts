import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { PromotionModel } from '../../model/PromotionModel';
 

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  BindDropDownBillGroup(): Observable<any> {
    // return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
    // return this.http.get('http://localhost:55424/api/PayBill/GetAccountHeads');
    return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetPayBillGroup?PermDdoId=00003`);
  }
	
	BindDropDownDesigGroup(selectedBilgrp): Observable<any> {    
    //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
      return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
  }

  BindDropDownEmpGroup(selectedDesig): Observable<any> {
    //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
    return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetEmp?desigId=${selectedDesig}&pageCode=Leaves`);
  }

  GetAllDesignation(controllerId): Observable<any> {
    //return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation?billGroupId=${selectedBilgrp}`);
    return this.http.get(`${this.config.api_base_url}${this.config.SharedControllerName}/GetAllDesignationOfDepartment?id=${controllerId}`);
  }

  GetAllEmpWithDesignationDetails(suspendedEmployee: boolean, controllerId: string, ddoId: string): Observable<any> {
    const params = new HttpParams().set('suspendedEmployees', suspendedEmployee.toString()).set('controllerId', "013").set('DDOId', ddoId); // controllerId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetEmpWithDesignationDetails`, { params });
  }

  FetchEmployeeDetails(employeeId, pageNumber, pageSize, searchTerm, roleId): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.PromotionControllerName}/GetPromotionDetailsWithoutTransfer?empCode=${employeeId}&pageNumber=${pageNumber}&pageSize=${pageSize}&searchTerm=${searchTerm}&roleId=${roleId}`);
  }

  UpdatePromotionDetails(objPro): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.PromotionControllerName}/UpdatePromotionDetailsWithoutTransfer`,
      objPro, { responseType: 'text' });
  }

  DeletePromotionDetails(employeeId): Observable<any> {
    return this.http.delete(`${this.config.api_base_url}${this.config.PromotionControllerName}/${employeeId}`);
  }

  ForwardTransferDetailsToChecker(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.PromotionControllerName}/ForwardTransferDetailToChecker`,
      obj, { responseType: 'text' });
  }

  UpdateStatus(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.PromotionControllerName}/UpdateStatus`,
      obj, { responseType: 'text' });
  }

  // Reversion API's
  FetchReversionEmployeeDetails(employeeId, pageNumber, pageSize, searchTerm, roleId): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.PromotionControllerName}/GetReversionDetailsWithoutTransfer?empCode=${employeeId}&pageNumber=${pageNumber}&pageSize=${pageSize}&searchTerm=${searchTerm}&roleId=${roleId}`);
  }

  UpdateReversionDetails(objPro): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.PromotionControllerName}/UpdateReversionDetailsWithoutTransfer`,
      objPro, { responseType: 'text' });
  }

  DeleteReversionDetails(employeeId): Observable<any> {
    return this.http.delete(`${this.config.api_base_url}${this.config.PromotionControllerName}/reversion/${employeeId}`);
  }
}
