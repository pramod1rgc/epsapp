import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class EndofserviceService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  //
  getEndofService(designid: any, empPermDDOId: string, mode: any): Observable<any> {
    const params = new HttpParams().set('designid', designid).set('empPermDDOId', empPermDDOId).set('mode', mode);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.EndofServiceGetEmployee}`, { params});
  }//

  currentEndDetails(msEmpID: any): Observable<any> {
    const params = new HttpParams().set('msEmpID', msEmpID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.EmpCurrentEndDeta}`, { params });
  }
  //Insert Details
  //InsertEndDetails(_hraModel: any): Observable<any> {
  //  const params = new HttpParams().set('obj', _hraModel);
  //  return this.http.post<any>(`${this.config.api_base_url}${this.config.EndofServiceInsert}`, { params });
  //}
  InsertEndDetails(obj: any): Observable<any> {
    debugger;
   // const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.EndofServiceInsert}`,
      obj, { responseType: 'text'});
  }
  DeleteEndDetails(id: any): Observable<any> {
    const params = new HttpParams().set('obj', id);
    return this.http.delete<any>(`${this.config.api_base_url}${this.config.EndofServiceDelete}`, { params });
  }
  //
  getEndofServiceReasonDt(): Observable<any> {    
    return this.http.get<any>(`${this.config.api_base_url}${this.config.EndofServiceReason}`);
  }
  getEndofServiceList(msEmpID:any): Observable<any> {
    const params = new HttpParams().set('msEmpID', msEmpID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.EndofServiceGetDetailByEmp}`, { params });
  }
}
