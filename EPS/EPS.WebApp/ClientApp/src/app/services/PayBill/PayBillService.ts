import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class PayBillGroupService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig)
  { }


  BindAccountHeads(permDDoId: any): Observable<any> {
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.getAccountHeads + permDDoId}`);
  }

  InsertUpdatepayBillGroup(objBillGroup) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.InsertUpdatePayBillGroup, objBillGroup, { responseType:'text' })
  }

  //BindAccountHeads() {
  //  //return this.httpclient.get(`${this.config.api_base_url}/${this.config.getAccountHeads}`);
  //}
  BindPayBillGroup(permDDoId: any): Observable<any>
  {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.BindPayBillGroup + permDDoId}`);
    //asdfsf
  }

 

  GetPayBillGroupDetailsById(parentId: any): Observable<any> {
    debugger
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GetPayBillGroupDetailsById + parentId}`);
  }


  BindAttachEmployee(): Observable<any>
  {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayBillGroupDetailsById}`);
  }

  getEmpAttachList():Observable<any>
  {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayBillGroupEmpAttach}`);

  }

  getEmpdEAttachList(BillGrpID:any):Observable<any>
  {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayBillGroupEmpDeAttach+ BillGrpID}`);

  }


  UpDatePayBillGroupId(empCode: string, BillGroupId: string) :Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.UpdatePayBillGroupIdByEmpCode + empCode + '&BillGroupId='+BillGroupId , { responseType:'text' })
  }

  UpdatePayBillGroupNullbyEmpcode(empCode: any): Observable<any> {
    
    return this.http.post(this.config.api_base_url + this.config.updatePayBillGroupIdnullByEmpCode + empCode  , { responseType:'text' })
      
  }
  //amit
  GetAccountHeadsOT1(FinFromYr: string, FinToYr: string, Flag: any): Observable<any> {   
    return this.http.get(this.config.api_base_url + this.config.getAccountHeadsOT1 + FinFromYr + '&FinToYr=' + FinToYr + '&Flag=' + Flag)
  }

  GetAccountHeadOT1AttachList(DDOCode: any,Flag:any): Observable<any> {
   
    return this.http.get(this.config.api_base_url + this.config.GetAccountHeadOT1AttachList + DDOCode + '&Flag=' + Flag)
  }
  
 
  UpdateaccountHeadOT1(accountHeadvalue: string, DDOCode: string, Flag: any): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.UpdateaccountHeadOT1 + accountHeadvalue + '&DDOCode=' + DDOCode + '&Flag=' + Flag, { responseType: 'text' })
  }

  GetAccountHeadOT1PaybillGroupStatus(DDOCode: any, Flag: any): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetAccountHeadOT1PaybillGroupStatus + DDOCode + '&Flag=' + Flag)
  }
  UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue: string, DDOCode: string, Flag: any): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.UpdateAccountHeadOT1PaybillGroupStatus + accountHeadvalue + '&DDOCode=' + DDOCode + '&Flag=' + Flag, { responseType: 'text' })
  }
  GetNgRecovery(permDDoId: any): Observable<any> {
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.getNgRecovery + permDDoId}`);
  }

  GetBankdetailsByIfsc(ifscCode: any): Observable<any> {
    debugger;
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GetBankDetailsByIfscCode + ifscCode}`);
  }


  InsertUpdateNgRecovery(objNgRecovery) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.insertUpdateNgRecovery, objNgRecovery, { responseType: 'text' })
  }

  ActiveDeactiveNgRecovery(objNgRecovery) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.activeDeactiveNgRecovery, objNgRecovery,  { responseType: 'text' })
  }


  GetFinancialYear(): Observable<any> {
    debugger;
    return this.http.get(this.config.api_base_url + this.config.getfinancialYear)
  }

  GetPayBillGroupByFinancialYear(finYearFrom: string, finYearTo:string, permDdoId: string): Observable<any> {
    return this.http.get(this.config.api_base_url + "PayBill/GetPayBillGroupByFinancialYear?FinYearFrom=" + finYearFrom + "&FinYearTo=" + finYearTo + "&PermDdoId=" + permDdoId);
  }

  GetEmployeesByPayItemCode(PayItemCd: number): Observable<any> {
    return this.http.get(this.config.api_base_url + "PayBill/GetEmployeesByPayItemCode?PayItemCd=" + PayItemCd);
  }

  GetPayItemsFromDesignationCode(DesigCode: number): Observable<any> {
    return this.http.get(this.config.api_base_url + "PayBill/GetPayItemsFromDesignationCode?DesigCode=" + DesigCode);
  }

  UpdateEmpNonComputationalDuesDeductAmount(obj: any): Observable<any> {
    return this.http.post(this.config.api_base_url + "PayBill/UpdateEmpNonComputationalDuesDeductAmount", obj, { responseType: 'text' });
  }

  GetDesignationByBillgrID(billGroupId: string, PermDdoId: string): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.SharedControllerName + "/GetDesignationByBillgrID?billGroupId=" + billGroupId + "&PermDdoId=" + PermDdoId);
  }

  GetAllMonth(): Observable<any> {
    debugger;
    return this.http.get(this.config.api_base_url + this.config.getAllMonth)
  }

  GetEmployeeByPaybillAndDesig(PermDDOId: string, PayBillGroupId: number, DesigCd: number): Observable<any> {
    debugger;
    return this.http.get(this.config.api_base_url + this.config.GetEmployeeByPaybillAndDesig +'?PermDDOId='+ PermDDOId + '&PayBillGroupId=' + PayBillGroupId + '&DesigCd=' + DesigCd)
  }

  GetNgRecoveryList(PermDDOId: string): Observable<any> {
    debugger;
    return this.http.get(this.config.api_base_url + this.config.GetNgRecoveryList + '?PermDDOId=' + PermDDOId)
  }

  InsertNgrecoveryEntry(ngEntryList) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.InsertNgrecoveryEntry, ngEntryList, { responseType: 'text' });
  }

  GetAllNgRecoveryandPaybillGrpDetails(permDdoId: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId);
    return this.http.get<any>(this.config.api_base_url + this.config.GetAllNgRecoveryandPaybillGrpDetails, { params });
  }

}

