import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DateofbirthService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  InsertDOBDetails(objPro: any, Flag: any): Observable<any> {
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertDOBDetails}/InsertDOBDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  
  GetDOBDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDOBDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }

  ForwardToCheckerDOBUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOBUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //common delete using flag
  ChangeDeleteDetails(empCd: any, orderNo: any, flag: any): Observable<any> {
    return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag)
  }
}
