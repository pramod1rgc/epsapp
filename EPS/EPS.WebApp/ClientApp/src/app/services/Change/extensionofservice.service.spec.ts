import { TestBed } from '@angular/core/testing';

import { ExtensionofService } from './extensionofservice.service';

describe('ExtensionofserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExtensionofService = TestBed.get(ExtensionofService);
    expect(service).toBeTruthy();
  });
});
