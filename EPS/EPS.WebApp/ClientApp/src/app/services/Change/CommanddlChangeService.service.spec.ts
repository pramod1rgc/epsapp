import { TestBed } from '@angular/core/testing';

import { CommanddlChangeService } from './CommanddlChangeService.service';

describe('CommanddlChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommanddlChangeService = TestBed.get(CommanddlChangeService);
    expect(service).toBeTruthy();
  });
});
