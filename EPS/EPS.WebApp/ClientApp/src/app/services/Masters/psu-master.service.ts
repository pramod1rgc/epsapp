import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { PsuMaster } from '../../model/masters/psu-master';

@Injectable({
  providedIn: 'root'
})
export class PsuMasterService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  GetDistList(stateId: any): Observable<any> {
    const params = new HttpParams().set('stateId', stateId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.psuMasterControllerName}/GetDisttList`, {params});
  }
  GetStateList(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.psuMasterControllerName}/GetStateList`);
  }

  SavedPsuMasterDetails(objPsu: PsuMaster) {
    return this.http.post(`${this.config.api_base_url}${this.config.psuMasterControllerName}/SavedPSUDetails`,
    objPsu, { responseType: 'json' });
  }

  GetPsuMasterDetails(): Observable<any> {
   // const params = new HttpParams().set('empCode', empId).set('isCheckerLogin', isCheckerLogin).set('comName', comName);
return this.http.get<any>(`${this.config.api_base_url}${this.config.psuMasterControllerName}/GetPsuMasterDetails`);
  }

  DeletePsuMasterDetails(id: any) {
    const param = new HttpParams().set('id', id);
       return this.http.delete<any>(`${this.config.api_base_url}${this.config.psuMasterControllerName}/DeletePsuMasterDetails`,
        {params: param,  responseType: 'json'});
  }

}
