import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TptaMasterService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  
  createTptaMaster(hraObj): Observable<any> {
    return this.httpclient.post(this.config.CreateTptaMaster, hraObj, { responseType: 'text' });
  }
  

  getTptaMasterDetails(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetTptaMasterDetails , {});
  }
 

  editTptaMasterDetails(MasterID): Observable<any> {
    return this.httpclient.get<any>(this.config.EditTptaMasterDetails + MasterID, {});
  }
  

  deleteTptaMaster(MasterID) {
    return this.httpclient.post(this.config.DeleteTptaMaster + MasterID, '', { responseType: 'text' });
  }
}
