import { TestBed } from '@angular/core/testing';

import { GpfRecoveryrulesService } from './gpf-recoveryrules.service';

describe('GpfRecoveryrulesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpfRecoveryrulesService = TestBed.get(GpfRecoveryrulesService);
    expect(service).toBeTruthy();
  });
});
