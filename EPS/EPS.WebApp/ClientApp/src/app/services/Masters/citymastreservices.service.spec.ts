import { TestBed } from '@angular/core/testing';

import { CitymastreservicesService } from './citymastreservices.service';

describe('CitymastreservicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CitymastreservicesService = TestBed.get(CitymastreservicesService);
    expect(service).toBeTruthy();
  });
});
