
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { Gpfadvancereasonsmodel } from '../../model/masters/gpfadvancereasonsmodel';
import { Text } from '@angular/compiler/src/render3/r3_ast';
@Injectable({
  providedIn: 'root'
})
export class GpfadvancereasonsserviceService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  //BindMainReasons() {
    
     
  //  debugger;
  //  return this.httpclient.get<any>("http://localhost:55424/api/GpfAdvanceReasons/GetGPFAdvanceReasonsDetails");
  //}
  BindMainReasons(): Observable<any> {
    return this.httpclient.get(`${this.config.BindMainReason}`);
  }

  BindPfType(): Observable<any> {
    return this.httpclient.get(`${this.config.GetPfType}`);
  }
  GetGPFAdvanceReasonsDetailsInGride(AdvWithdraw): Observable<any> {
   
    return this.httpclient.post<any>(this.config.GetGpfAdvancereasonsDetails + '?advWithdraw=' + AdvWithdraw , { responseType: 'text' } );
  }

  InsertandUpdate(_objgpfmaster: any) {

    return this.httpclient.post(this.config.GpfAdvancereasonsInsertandUpdate, _objgpfmaster, { responseType: 'text' });
  }

  Delete(pfRefNo) {
    //return this.httpclient.post(this.config.Delete + pfRefNo, '', { responseType: 'text' });
    return this.httpclient.post(this.config.api_base_url + 'GpfAdvanceReasons/Delete?pfRefNo=' + pfRefNo,'', { responseType: 'text'});
  }


  BindMainReasons1() {
    //return this.httpclient
    //  .get<Gpfadvancereasonsmodel>(`${this.config.BindMainReasons}`);
  }

  GetGpfAdvanceReasonseDetails() {
    return this.httpclient.get<any>("http://localhost:55424/api/payscale/BindCommissionCode");
  //  return this.httpclient.get<any>(this.config.GetGpfAdvanceReasonseDetails);
  }

}
