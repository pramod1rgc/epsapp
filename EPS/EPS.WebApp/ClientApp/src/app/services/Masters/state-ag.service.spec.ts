import { TestBed } from '@angular/core/testing';

import { StateAgService } from './state-ag.service';

describe('StateAgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateAgService = TestBed.get(StateAgService);
    expect(service).toBeTruthy();
  });
});
