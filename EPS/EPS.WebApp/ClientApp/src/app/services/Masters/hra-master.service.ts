import { Injectable, Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { hraModel } from '../../model/masters/hraModel';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HraMasterService {
    constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  BindPayCode() {
    return this.httpclient
      .get<hraModel>(`${this.config.BindPayScaleCode}`);
  }

  bindPayLevel() {
    return this.httpclient
      .get<hraModel>(`${this.config.BindPayLevel}`);
  }

 
  bindCityList(stateID): Observable<any> {
    return this.httpclient.get<any>(this.config.GetCityDetails + stateID, {});
  }

  getCityClass(payCommId): Observable<any> {
    return this.httpclient.get<any>(this.config.GetCityClass + payCommId, {});
  }

  getPayCommissionByEmployeeType(employeeType): Observable<any> {
    return this.httpclient.get<any>(this.config.GetPayCommissionByEmployeeType + employeeType, {});
  }

  createHraMaster(hraObj): Observable<any> {
    return this.httpclient.post(this.config.CreateHraMaster, hraObj, { responseType: 'text' });
  }

  getHraMasterDetails(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetHraMasterDetails, {});
  }

  editHraMasterDetails(hraMasterID): Observable<any> {
    return this.httpclient.get<any>(this.config.EditHraMasterDetails + hraMasterID, {});
  }


  deleteHraMaster(hraMasterID: number) {
    return this.httpclient.post(this.config.DeleteHraMaster + hraMasterID, '', { responseType: 'text' });

  }


}
