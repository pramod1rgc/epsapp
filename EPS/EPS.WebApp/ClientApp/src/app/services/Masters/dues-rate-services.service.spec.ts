import { TestBed } from '@angular/core/testing';

import { DuesRateServicesService } from './dues-rate-services.service';

describe('DuesRateServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DuesRateServicesService = TestBed.get(DuesRateServicesService);
    expect(service).toBeTruthy();
  });
});
