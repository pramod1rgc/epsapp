import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { DuesDefinitionmasterModel } from '../../model/masters/DuesDefinitionmasterModel';

@Injectable({
  providedIn: 'root'
})
export class CitymastreservicesService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  geCityClassMasterList(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GeCityClassMasterList}`);
  }

  SaveCityClass(obj): Observable<any> {
    debugger;
    return this.http.post(`${this.config.api_base_url}${this.config.CityMasterController}/SaveCityClassmasterDetails`, obj, { responseType: 'text' });
  }


  //CheckRecordExitsOrNot(obj): Observable<any> {
  //  //return this.http.get(`${this.config.api_base_url}${this.config.CityMasterController}/CheckRecordexitsOrNotinCitymaster`, obj);
  // debugger
  //  //return this.http.get(`${this.config.api_base_url + this.config.CityMasterController}/CheckRecordexitsOrNotinCitymaster`, + cclPaycommId + '&cclCityclass=' + cclCityclass);
  //  return this.http.post(`${this.config.api_base_url}${this.config.CityMasterController}/CheckRecordexitsOrNotinCitymaster`, obj, { responseType: 'text' });

  //}


  DeleteCityClass(DuesCd: string): Observable<any> {

    return this.http.post<DuesDefinitionmasterModel>(this.config.api_base_url + 'CityMaster/DeleteCityClass?DuesCd=' + DuesCd, { responseType: 'text' });

  }

}








