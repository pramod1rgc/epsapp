import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DlismasterService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig) { }
  getPfType(): Observable<any> {
    return this.http.get<any>(this.config.api_base_url + 'GpfDlisExceptions/GetPfType');
  }
  getDlisExceptions(): Observable<any> {
    return this.http.get<any>(this.config.api_base_url + 'GpfDlisExceptions/GetGpfDlisExceptions');
  }
  getPayScale(payCommId: any): Observable<any> {
    return this.http.get<any>(this.config.api_base_url + 'GpfDlisExceptions/GetPayScale?PayCommId='+payCommId);
  }
  insertUpdateGpfDlisException(dlisExceptions): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + 'GpfDlisExceptions/InsertUpdateGpfDlisExceptions', dlisExceptions);
  }
  deleteGpfDlisException(id: any): Observable<any> {
    return this.http
      .post<any>(this.config.api_base_url + 'GpfDlisExceptions/DeleteGpfDlisExceptions?MsGpfDlisExceptionsRefID='+id, { responseType: 'text' });
  }
}
