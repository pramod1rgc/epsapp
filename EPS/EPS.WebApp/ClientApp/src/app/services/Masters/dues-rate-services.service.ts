import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { DuesRateMasterModel } from '../../model/masters/DuesRateMaster';
import { IDuesRate } from '../../masters/dues-rate/IDuesRate';

@Injectable({
  providedIn: 'root'
})
export class DuesRateServicesService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  private handleError(err) {
    let body
    if (err instanceof Response) {
      body = err.json() || '';
    }
    return Observable.throw(body.error["message"]);
  }

  GetorginzationTypeForDuesRate(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetOrgnaztionTypeForDuesRate}`);

  }

  GetStateForDuesRate(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetStateForDuesRate}`);
  }


  GetPayCommForDuesRate(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayCommForDuesRate}`);

  }


  GetDuesCodeDuesDefination(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetDuesCodeDuesDefination}`);

  }



  GetCityClassForDuesRate(payCommId: number) {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetCityClassForDuesRate + payCommId}`);

  }



  addDuesRate(employee: IDuesRate): Observable<IDuesRate> {

    return this.http.post<IDuesRate>(this.config.api_base_url + this.config.AddDuesDetails, employee)
  }


  GetSlabTypeByPayCommId(payCommId: number) {

    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GetSlabTypeByPayCommId + payCommId}`);
  }

  InsertUpateDuesRateDetails(objDuesRateandDuesMasterModel: any): Observable<any> {

    return this.http.post<any>(this.config.api_base_url + this.config.saveUpdateDuesRateDetails, objDuesRateandDuesMasterModel)
  }

  getDuesRatemasterList(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetDuesRateList}`);
  }


  DeleteDuesRateMaster(msduesratedetailsid: number) {
    //return this.http.post(this.config.DeleteDuesRatemaster + msduesratedetailsid, '', { responseType: 'text' });
    return this.http.post(this.config.api_base_url + 'DuesDefinationandDuesMaster/DeleteDuesRateDetails?msduesratedetailsid=' + msduesratedetailsid, { responseType: 'text' });
  }
}
