import { TestBed } from '@angular/core/testing';

import { DuesDefinitionServicesService } from './dues-definition-services.service';

describe('DuesDefinitionServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DuesDefinitionServicesService = TestBed.get(DuesDefinitionServicesService);
    expect(service).toBeTruthy();
  });
});
