import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GpfadvancerulesService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig) { }
  //rules = 'GpfWithdrawRules/GetGpfWithdrawRules';
  //rules1 = 'GpfWithdrawRules/GetGpfWithdrawRulesById';
  //InsertUpdateEmployeeDetails1 = 'GpfWithdrawRules/InsertUpdateGpfWithdrawRules';
  //Deleted = 'GpfWithdrawRules/DeleteGpfWithdrawRules?msGpfWithdrawRulesRefID=';
  getGpfRules(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GpfAdvanceRules}`);
  }

  getGpfRulesByID(id: any): Observable<any> {
    const params = new HttpParams().set('msGpfAdvanceRulesRefID', id);
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GpfAdvanceRulesByID}`, { params });
  }

  insertUpdateGpfRules(GpfAdvanceRules): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.InsertUpdateGpfAdvanceRules, GpfAdvanceRules);
  }

  deleteGpfRules(id: any, Isflag): Observable<any> {
    return this.http
      .post<any>(this.config.api_base_url + this.config.DeleteGpfAdvanceRules + id + '&Isflag=' + Isflag, { responseType: 'text' });
  }
}
