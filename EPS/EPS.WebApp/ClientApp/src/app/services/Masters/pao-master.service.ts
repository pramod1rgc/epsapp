
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PaoMasterService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  GetMSPAODetails(): Observable<any> {

    return this.http.get<any>(`${this.config.getMSPAODetails}`);
  }
  upadateMsPAO(objmsPao): Observable<any> {
    if (objmsPao.PinCode.length == 0)
      objmsPao.PinCode = 0
    let res = this.http.post(`${this.config.updateMSPAO}`, objmsPao, { responseType: 'text' });
    return res;
  }

  private handleError(errorResponce: HttpErrorResponse) {
    if (errorResponce.error instanceof ErrorEvent) {
      console.log('client side error');
    } else {
      console.log(JSON.stringify(errorResponce.error));
    }
    return throwError('Something bad happened; please try again later.');
  }

  InsertMsPAO(objmsPao): Observable<any> {
    return this.http.post(`${this.config.insertMSPAO}`, objmsPao, { responseType: "text" });
  }
  DeleteMsPAO(mspaoId): Observable<any> {
    return this.http.post(this.config.deleteMSPAO +'?mspaoId='+mspaoId,'', { responseType: "text" });
  }
  GetAllController(): Observable<any> {
    return this.http.get<any>(`${this.config.DDOMasterControllerName}/GetAllController`);
  }
  GetPAOByControllerId(ControllerId: string): Observable<any> {
    const params = new HttpParams().set('ControllerId', ControllerId);
    return this.http.get<any>(`${this.config.DDOMasterControllerName}/GetPAOByControllerId`, { params });
  }
  GetState(): Observable<any> {
    return this.http.get<any>(`${this.config.DDOMasterControllerName}/GetState`);
  }
  GetCity(StateId: string): Observable<any> {
    const params = new HttpParams().set('StateId', StateId);
    return this.http.get<any>(`${this.config.DDOMasterControllerName}/GetCity`, { params });
  }
}
