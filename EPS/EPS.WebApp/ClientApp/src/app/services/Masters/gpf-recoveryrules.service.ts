import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GpfRecoveryrulesService {
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig) { }
  //rules = 'GpfWithdrawRules/GetGpfWithdrawRules';
  //rules1 = 'GpfWithdrawRules/GetGpfWithdrawRulesById';
  //InsertUpdateEmployeeDetails1 = 'GpfWithdrawRules/InsertUpdateGpfWithdrawRules';
  //Deleted = 'GpfWithdrawRules/DeleteGpfWithdrawRules?msGpfWithdrawRulesRefID=';
  getGpfRules(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GpfRecoveryRules}`);
  }

  getGpfRulesByID(id: any): Observable<any> {
    const params = new HttpParams().set('msGpfRecoveryRulesRefID', id);
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GpfRecoveryRulesByID}`, { params });
  }

  insertUpdateGpfRules(objgpfRecoveryRules: any) {

    return this.http.post(this.config.api_base_url + this.config.InsertUpdateGpfRecoveryRules, objgpfRecoveryRules, { responseType: 'text' });
  }

  //insertUpdateGpfRules(objgpfRecoveryRules): Observable<any> {
  //  debugger;
  //  return this.http.post<any>(this.config.api_base_url + this.config.InsertUpdateGpfRecoveryRules, objgpfRecoveryRules);
  //}

  deleteGpfRules(id: any): Observable<any> {
    return this.http
      .post<any>(this.config.api_base_url + this.config.DeleteGpfRecoveryRules + id, { responseType: 'text' });
  }
}
