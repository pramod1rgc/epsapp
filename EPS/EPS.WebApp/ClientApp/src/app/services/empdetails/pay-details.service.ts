import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayDetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getOrganisationType(EmpCode: string): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getOrganisationType + '?EmpCode=' + EmpCode}`);
  }


  getNonComputationalDues(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getNonComputationalDues}`);
  }


  getNonComputationalDeductions(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getNonComputationalDeductions}`);
  }

  getPayLevel(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayLevel}`);
  }

  getEntitledOffVeh(): Observable<any> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getEntitledOffVeh + '?Module=' + 'Employee_Entitledofficevehicle'}`);
  }



  getPayIndex(PayLevel: string): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayIndex + '?PayLevel=' + PayLevel}`);
  }



  getBasicDetails(PayLevel: string, PayIndex: number) {

    // tslint:disable-next-line: max-line-length
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBasicDetails + '?PayLevel=' + PayLevel + '&PayIndex=' + PayIndex}`);
  }

  getGradePay() {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.getGradePay}`);
  }

  getPayScale(GradePay: number): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayScale + '?GradePay=' + GradePay}`);
  }

  SaveUpdatePayDetails(objPayDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.SaveUpdatePayDetails, objPayDetails, { responseType: 'text' });
  }
  getPayDetails(empCode: string , roleId: any) {
  const params = new HttpParams().set('EmpCode', empCode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayDetails}`, { params });
  }

}
