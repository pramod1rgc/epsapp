import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForwardToCheckerService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getAllEmpDetails ( roleId: any): Observable<any> {
    const params = new HttpParams().set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllEmpDetails}`, { params });
  }
  getAllEmployeeCompleteDetails(roleId: any, empCode: any): Observable<any> {
    const params = new HttpParams().set('RoleId', roleId).set('empCode', empCode);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllEmployeeCompleteDetails}`, { params });
  }
  ForwardToCheckerEmpDetails(objForwardToChecker: any) {
    debugger;
    // const params = new HttpParams().set('empcodes', empcodes).set('UserID', UserID);
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerEmpDetails , objForwardToChecker ,
      { responseType: 'text' });
  }

  verifyEmpData(empCode: string, remark: string) {
    debugger;
    const params = new HttpParams().set('empcodes', empCode). set('remark', remark);
    return this.http.post(this.config.api_base_url + this.config.verifyEmpData, params , { responseType: 'text' });
  }
  
}
