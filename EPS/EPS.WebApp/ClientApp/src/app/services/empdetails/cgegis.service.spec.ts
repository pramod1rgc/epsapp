import { TestBed } from '@angular/core/testing';

import { CGEGISService } from './cgegis.service';

describe('CGEGISService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CGEGISService = TestBed.get(CGEGISService);
    expect(service).toBeTruthy();
  });
});
