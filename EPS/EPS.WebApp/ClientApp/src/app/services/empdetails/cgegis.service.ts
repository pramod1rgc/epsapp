import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CGEGISService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }
  getInsuranceApplicable(empcode: string): Observable<any> {
    const params = new HttpParams().set('EmpCode', empcode);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getInsuranceApplicable}`, { params });
  }
  getCGEGISCategory(insuranceApplicableId: any, empcode: any): Observable<any> {
    const params = new HttpParams().set('insuranceApplicableId', insuranceApplicableId).set('EmpCode', empcode);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getCGEGISCategory}`, { params });
  }


  SaveUpdateCGEGISDetails(objCGEGISDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.SaveUpdateCGEGISDetails, objCGEGISDetails, { responseType: 'text' });
  }

  getCGEGISDetails(empcode: any, roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCode', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getCGEGISDetails}`, { params });
  }
}
