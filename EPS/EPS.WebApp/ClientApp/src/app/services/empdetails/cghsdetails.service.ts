import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CghsdetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  SaveUpdateCGHSDetails(objCGEGISDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.SaveUpdateCGHSDetails , objCGEGISDetails , { responseType: 'text' });
  }

  getCGHSDetails(empcode: any, roleID: any): Observable<any> {
      const params = new HttpParams().set('EmpCd', empcode).set('RoleId', roleID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getCGHSDetails}`, { params });
  }
}
