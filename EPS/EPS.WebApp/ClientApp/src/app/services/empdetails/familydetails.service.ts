import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FamilydetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }



  GetMaritalStatus(): Observable<string[]> {

    return this.http.get<string[]>(`${this.config.api_base_url}${this.config.GetMaritalStatus}`);
  }

  getAllFamilyDetails(empcode: any , roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllFamilyDetails}`, { params });
  }

  getFamilyDetails(empcode: any, MsDependentDetailID: any): Observable<any> {
   const params = new HttpParams().set('EmpCd', empcode).set('MsDependentDetailID', MsDependentDetailID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getFamilyDetails}`, { params });
  }


  UpdateEmpFamilyDetails(objFamilyDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.UpdateEmpFamilyDetails , objFamilyDetails , { responseType: 'text' });
  }
  DeleteFamilyDetails(EmpCd: string , MsDependentDetailID: Number) {
    // tslint:disable-next-line:max-line-length
    return this.http.post(this.config.api_base_url + this.config.DeleteFamilyDetails + '?EmpCd=' + EmpCd + ' &MsDependentDetailID=' + MsDependentDetailID, { responseType: 'text' });
  }

}
