import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { EmployeeDetailsModel } from '../../model/empdetails';



@Injectable({
  providedIn: 'root'
})
export class EmpdetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  //private handleError(err) {
  //  let body;
  //  if (err instanceof Response) {
  //    body = err.json() || '';
  //  }
  //  return Observable.throw(body.error['message']);
  //}
  GetVerifyEmpCode(): Observable<string[]> {

    return this.http.get<string[]>(`${this.config.api_base_url}${this.config.getverifiedEmpCode}`);
  }
  GetEmpPersonalDetailsByID(EmpCd: any, roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', EmpCd).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpPersonalDetailsByID}`, { params });
  }

  UpdateEmpDetails(empdetails) {
  return this.http.post(this.config.api_base_url + this.config.UpdateEmpDetails, empdetails, { responseType: 'text' });
  }


  getMakerEmpList(roleId: any): Observable<any> {
    const params = new HttpParams().set('roleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getMakerEmpList}`, {params});
  }

  InsertUpdateEmpDetails(empdetails): Observable<any>{
    return this.http.post<any>(this.config.api_base_url + this.config.InsertUpdateEmployeeDetails, empdetails);
   
  }


  GetEmpPHDetails(EmpCd: EmployeeDetailsModel): Observable<any> {

       return this.http
      .get<EmployeeDetailsModel>(`${this.config.api_base_url}${this.config.GetEmpPHDetails + EmpCd}`);
  }

  GetPhysicalDisabilityTypes(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPhysicalDisabilityTypes}`);
  }

  SavePHDetails(empPHDetails): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.SavePHDetails , empPHDetails , { responseType: 'text' });
  }
  DeleteEmpDetails(EmpCd: string): Observable<any> {

    return this.http.post<EmployeeDetailsModel>(this.config.api_base_url + 'EmployeeDetails/DeleteEmployeeDetails?EmpCd=' + EmpCd,
    { responseType: 'text' });
      // .get<EmployeeDetailsModel>(`${this.config.api_base_url}${this.config.GetEmpPHDetails + EmpCd}`);
  }
  getdownloadDetails(url: string) {
    debugger
    return this.http.get(this.config.api_base_url + 'EmployeeDetails/DownloadFile?url=' + url, { responseType: 'arraybuffer' });
  }

  GetIsDeputEmp(EmpCd: string): Observable<any> {
    const params = new HttpParams().set('EmpCd', EmpCd);
    return this.http
      .get<EmployeeDetailsModel>(`${this.config.api_base_url}${this.config.GetIsDeputEmp }`, {params});
  }
  VerifyRejectionEmpDetails(empdetails): Observable<EmployeeDetailsModel> {
    return this.http.post<EmployeeDetailsModel>(this.config.api_base_url + 'EmployeeDetails/VerifyRejectionEmployeeDtl', empdetails);

  }
   
}
