import { TestBed } from '@angular/core/testing';

import { ForwardToCheckerService } from './forward-to-checker.service';

describe('ForwardToCheckerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForwardToCheckerService = TestBed.get(ForwardToCheckerService);
    expect(service).toBeTruthy();
  });
});
