import { TestBed } from '@angular/core/testing';

import { RepatriationdetailsService } from './repatriationdetails.service';

describe('RepatriationdetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RepatriationdetailsService = TestBed.get(RepatriationdetailsService);
    expect(service).toBeTruthy();
  });
});
