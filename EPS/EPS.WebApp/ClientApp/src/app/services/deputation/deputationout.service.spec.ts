import { TestBed } from '@angular/core/testing';

import { DeputationoutService } from './deputationout.service';

describe('DeputationoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeputationoutService = TestBed.get(DeputationoutService);
    expect(service).toBeTruthy();
  });
});
