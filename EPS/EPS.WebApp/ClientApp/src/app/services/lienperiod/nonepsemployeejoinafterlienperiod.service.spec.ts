import { TestBed } from '@angular/core/testing';

import { NonepsemployeejoinafterlienperiodService } from './nonepsemployeejoinafterlienperiod.service';

describe('NonepsemployeejoinafterlienperiodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NonepsemployeejoinafterlienperiodService = TestBed.get(NonepsemployeejoinafterlienperiodService);
    expect(service).toBeTruthy();
  });
});
