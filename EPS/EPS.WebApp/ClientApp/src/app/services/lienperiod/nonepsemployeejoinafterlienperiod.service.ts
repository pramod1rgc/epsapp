import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { text } from '@angular/core/src/render3';
@Injectable({
  providedIn: 'root'
})
export class NonepsemployeejoinafterlienperiodService {
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd: any, roleId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp + '?empCd= ' + empCd + '&roleId=' + roleId}`);
  }

  //Insert Update Non Eps Employee Join After Lien Period
  InsertUpateNonEpsEmployeeJoinAfterLienPeriod(NonEpsEmpJoinLienPeriodDetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateNonEpsEmployeeJoinAfterLienPeriod, NonEpsEmpJoinLienPeriodDetails, { responseType: 'text' } )
  }
  //--end

  //Delete Non Eps Employee Join After Lien Period 

  deleteNonEpsTransferOffLienPeriod(empCd,orderNo): Observable<any> {
    debugger;
    return this.http.get<any>(this.config.api_base_url + this.config.deleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo)
  }
  
  //--end
  //
  forwardNonEpsEmployeeStatusUpdate(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.nonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= '+ empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //ForwardToCheckerUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
  //  return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  //}
  //--end
}
