import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IncomeTaxRoutingModule } from './income-tax-routing.module';
import { ExemptionDeductionComponent } from './exemption-deduction/exemption-deduction.component';
import { StandardDeductionComponent } from './standard-deduction/standard-deduction.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';
import { ExemptionDeductionService } from '../services/income-tax/exemption-deduction.service';
import { StandardDeductionService } from '../services/income-tax/standard-deduction.services';
import { ItRatesComponent } from './it-rates/it-rates.component';
import { OtherDuesComponent} from './other-dues/other-dues.component'
import { ITRatesService } from '../services/income-tax/it-rates.service';
import { CommonMsg } from '../global/common-msg';
import { MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [ExemptionDeductionComponent, StandardDeductionComponent, ItRatesComponent, OtherDuesComponent],
  imports: [
    MatTooltipModule,
    CommonModule,
    IncomeTaxRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, SharedModule
  ],
  providers: [ExemptionDeductionService, StandardDeductionService, ITRatesService, CommonMsg]
})
export class IncomeTaxModule { }
