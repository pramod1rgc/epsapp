import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';
import { ITRatesService } from '../../services/income-tax/it-rates.service';

@Component({
  selector: 'app-it-rates',
  templateUrl: './it-rates.component.html',
  styleUrls: ['./it-rates.component.css']
})
export class ItRatesComponent implements OnInit {
  btnclose() {
    this.is_btnStatus = false;
  }
  is_btnStatus = true;

  itRatesForm: FormGroup;
  filterRatesForm: FormGroup;
  formDataSource = new MatTableDataSource();
  dataSource = new MatTableDataSource();
  DMLStatusList = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  displayedColumnsForForm: string[] = ['lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'action'];
  displayedColumns: string[] = ['finYear', 'rateType', 'rateFor', 'lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'action'];
  dspColumnStatusHeader: string[] = ['finYear', 'rateType', 'rateFor', 'lowerLimit', 'upperLimit', 'addedAmount', 'percentageValue', 'message'];

  ddoId: any;
  financialYears: any[] = [];
  elementToBeDeleted: any;

  pageSize: number = 5;
  pageNumber: number = 0;
  totalCount: number = 0;
  DeletePopup: boolean = false;
  lblSubmitStatus: string = 'Save';
  isLoading: boolean = false;
  rateType: any[] = [];
  rateFor: any[] = [];
  isDisableAddRow: boolean = false;
  IsDMLStatus: boolean = false;
  isSuccessStatus: boolean = false;
  isItRatePanel: boolean = false;
  isItRateDetailPanel: boolean = false;
  responseMessage: string = '';
  bgColor: string = '';
  btnCssClass: string = 'btn btn-success';
  constructor(private formBuilder: FormBuilder, private _service: ITRatesService, private _msg: CommonMsg) {
    this.ddoId = sessionStorage.getItem('ddoid');
    this.createForm();
    this.createFilterForm();
  }

  createForm() {
    this.itRatesForm = new FormGroup({
      isrFinYr: new FormControl('', [Validators.required]),
      isrRateType: new FormControl('', [Validators.required]),
      isrRateFor: new FormControl('', [Validators.required]),
      ddoId: new FormControl(this.ddoId),
      rateDetails: new FormArray([], [Validators.minLength(1)])
    });
    this.addRateDetail();
  }

  createFilterForm() {
    this.filterRatesForm = new FormGroup({
      finYear: new FormControl(''),
      rateType: new FormControl(''),
      rateFor: new FormControl(''),
    });
  }


  createRateDetail(): FormGroup {
    return new FormGroup({
      id: new FormControl(this.rateDetails.value.length + 1),
      itSurcharge_RateID: new FormControl(0),
      isrLlimit: new FormControl('', [Validators.required]),
      isrUlimit: new FormControl('', [Validators.required]),
      isrAddedAmt: new FormControl('', [Validators.required]),
      isrPercVal: new FormControl('', [Validators.required, Validators.pattern("^0*(?:[1-9][0-9]?|100)$")]),
    });
  }

  addRateDetail() {
    this.rateDetails.push(this.createRateDetail());
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
  }

  get rateDetails() {
    return this.itRatesForm.get("rateDetails") as FormArray;
  }

  deleteRateDetail(index: any, obj: any) {
    if (this.rateDetails.length > 1) {
      this.rateDetails.removeAt(index);
      this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    }
  }

  onSubmit() {
      this.isLoading = true;
      if (this.itRatesForm.valid) {
        this.filterRatesForm.reset();
        let status = this.itRatesForm.value.rateDetails[0].itSurcharge_RateID > 0 ? true : false;
        this._service.upsertItRates(this.itRatesForm.value).subscribe(response => {
          this.IsDMLStatus = true;
          if (response && response.length > 0) {
            this.formGroupDirective.resetForm();
            this.createForm();
            this.createFilterForm();
            this.search(-1);
            this.lblSubmitStatus = 'Save';
            this.DMLStatusList = new MatTableDataSource(response);
            this.isItRatePanel = false;
            this.bgColor = '';
          }
        }, (error) => {
          this.isSuccessStatus = true;
          this.responseMessage = this._msg.apiErrorMsg;
          this.isLoading = false;
          });
      } else { this.isLoading = false; }
  }



  DeleteDetails(element) {
    this.elementToBeDeleted = element;
  }

  confirmDelete() {
    this.isSuccessStatus = true;
    this._service.deleteItRates(this.elementToBeDeleted.itSurcharge_RateID, this.ddoId).subscribe(response => {
      this.elementToBeDeleted = null;
      this.responseMessage = this._msg.deleteMsg;
      this.createForm();
      this.DeletePopup = false;
      this.createFilterForm();
      this.search(-1);
      setTimeout(() => this.isSuccessStatus = false, this._msg.messageTimer);
    });
  }

  btnEditClick(element) {
    this.isDisableAddRow = true;
    this.isItRatePanel = true;
    this.rateDetails.removeAt(0);
    this.lblSubmitStatus = 'Update';
    this.btnCssClass = 'btn btn-info';
    this.bgColor = 'bgcolor';
    this.itRatesForm = new FormGroup({
      isrFinYr: new FormControl(element.isrFinYr.trim(), [Validators.required]),
      isrRateType: new FormControl(element.isrRateType.trim(), [Validators.required]),
      isrRateFor: new FormControl(element.isrRateFor.trim(), [Validators.required]),
      ddoId: new FormControl(this.ddoId),
      rateDetails: new FormArray([], [Validators.minLength(1)])
    });
    this.rateDetails.push(new FormGroup({
      id: new FormControl(this.rateDetails.value.length + 1),
      itSurcharge_RateID: new FormControl(element.itSurcharge_RateID),
      isrLlimit: new FormControl(element.isrLlimit, [Validators.required,]),
      isrUlimit: new FormControl(element.isrUlimit, [Validators.required]),
      isrAddedAmt: new FormControl(element.isrAddedAmt, [Validators.required]),
      isrPercVal: new FormControl(element.isrPercVal, [Validators.required]),
    }));
  }

  search(e) {
    this.isLoading = true;
    this.totalCount = 0;
    let ffm = this.filterRatesForm.value;
    this._service.getItRates(ffm.finYear == null ? '' : ffm.finYear, ffm.rateType == null ? '' : ffm.rateType, ffm.rateFor == null ? '' : ffm.rateFor).subscribe(response => {
      this.isLoading = false;
      if (response.length > 0) {
        this.totalCount = response[0].totalCount;
        this.isItRateDetailPanel = true;
      } else {
        this.isItRatePanel = e == 0 ? true : false;
      }
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.cancelForm(0);
    });
  }
  searchForReset() {
    this.isLoading = true;
    this.totalCount = 0;
    let ffm = this.filterRatesForm.value;
    this._service.getItRates(ffm.finYear == null ? '' : ffm.finYear, ffm.rateType == null ? '' : ffm.rateType, ffm.rateFor == null ? '' : ffm.rateFor).subscribe(response => {
      this.isLoading = false;
      if (response.length > 0) {
        this.totalCount = response[0].totalCount;
      }
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
    });
  }
  cancelForm(i) {
    this.lblSubmitStatus = 'Save';
    this.isDisableAddRow = false;
    this.bgColor = '';
    this.btnCssClass='btn btn-success';
    if (i == 1) {
      this.filterRatesForm.reset();
      this.createFilterForm();
      this.searchForReset();
    } else if (i == -1) {
      this.filterRatesForm.reset();
      this.createFilterForm();
      this.searchForReset();
      this.itRatesForm.reset();
      this.formGroupDirective.resetForm();
      this.createForm();
    }
    else {
      this.itRatesForm.reset();
      this.formGroupDirective.resetForm();
      this.createForm();
    }
  }

  validateUpperLimit(i) {
    let lLmt = this.itRatesForm.controls.rateDetails.value[i].isrLlimit;
    let uLmt = this.itRatesForm.controls.rateDetails.value[i].isrUlimit;
    if (this.checkUpperLimitExists(lLmt, uLmt)) {
      this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': true });
      return { 'collapse': true };
    } else {
      if (lLmt == '' || uLmt == '') {
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
          return { 'invalidError': true };
        } else {
          this.rateDetails.controls[i].get("isrUlimit").setErrors(null);
          this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
          return null;
        }
      }
    }
  }
  validateLowerLimit(i) {
    let lLmt = this.itRatesForm.controls.rateDetails.value[i].isrLlimit;
    let uLmt = this.itRatesForm.controls.rateDetails.value[i].isrUlimit;
    if (this.checkUpperLimitExists(lLmt, uLmt)) {
      this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': true });
      return { 'collapse': true };
    } else {
      this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'collapse': false });
      if (lLmt == '' || uLmt == '') {
        lLmt == '' ? this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'required': true }) : null;
        uLmt == '' ? this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'required': true }) : null;
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[i].get("isrLlimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[i].get("isrUlimit").setErrors({ 'invalidError': false });
          return { 'invalidError': true };
        } else {
          this.rateDetails.controls[i].get("isrLlimit").setErrors(null);
          this.rateDetails.controls[i].get("isrUlimit").setErrors(null);
          return null;
        }
      }
    }
  }

  checkUpperLimitExists(lowerLimt, ulimit) {
    if (this.itRatesForm.controls.rateDetails.value != undefined && this.itRatesForm.controls.rateDetails.value.length > 0) {
      if (this.itRatesForm.controls.rateDetails.value.length == 1) {
        return false;
      } else {
        let status = this.itRatesForm.controls.rateDetails.value.filter(p => (parseInt(p.isrLlimit) <= parseInt(lowerLimt) && parseInt(p.isrUlimit) >= lowerLimt) || (parseInt(p.isrLlimit) <= parseInt(ulimit) && parseInt(p.isrUlimit) >= ulimit))
        if (status == undefined || status == null || status == '') {
          return false;
        } else {
          if (status.length == 1 && parseInt(status[0].isrLlimit) == lowerLimt && parseInt(status[0].isrUlimit) == ulimit) {
            return false;
          } else {
            return true;
          }
        }
      }
    }
  }

  sortData(sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      return;
    }
    this.dataSource = new MatTableDataSource(data.sort((a: any, b: any) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'finYear': return this.compare(a.isrFinYr, b.isrFinYr, isAsc);
        case 'rateType': return this.compare(a.isrRateType, b.isrRateType, isAsc);
        case 'rateFor': return this.compare(a.isrRateFor, b.isrRateFor, isAsc);
        case 'lowerLimit': return this.compare(a.isrLlimit, b.isrLlimit, isAsc);
        case 'upperLimit': return this.compare(a.isrUlimit, b.isrUlimit, isAsc);
        case 'addedAmount': return this.compare(a.isrAddedAmt, b.isrAddedAmt, isAsc);
        case 'percentageValue': return this.compare(a.isrPercVal, b.isrPercVal, isAsc);
        default: return 0;
      }
    }));
    this.dataSource.paginator = this.paginator;
  }


  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  GetFinancialYear() {
    this._service.getFinancialYear().subscribe(result => {
      this.financialYears = result;
    })
  }
  GetAllRateTypeAndRateFor() {
    this._service.getAllRateTypeAndRateFor('').subscribe(result => {
      if (result && result.length > 0) {
        this.rateType = result.filter(p => p.codeText == 'RT');
        this.rateFor = result.filter(p => p.codeText == 'RF');
      }
    })
  }

  ngOnInit() {
    this.GetFinancialYear();
    this.GetAllRateTypeAndRateFor();
    this.search(0);
    this.lblSubmitStatus = 'Save';
  }
}

