import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItRatesComponent } from './it-rates.component';

describe('ItRatesComponent', () => {
  let component: ItRatesComponent;
  let fixture: ComponentFixture<ItRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
