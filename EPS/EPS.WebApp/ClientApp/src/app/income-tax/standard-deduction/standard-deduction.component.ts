import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { StandardDeductionService } from '../../services/income-tax/standard-deduction.services';

@Component({
  selector: 'app-standard-deduction',
  templateUrl: './standard-deduction.component.html',
  styleUrls: ['./standard-deduction.component.css']
})

export class StandardDeductionComponent implements OnInit {
  btnclose() {
    this.is_btnStatus = false;
  }
  is_btnStatus = false;

  incomeTaxRatesForm: FormGroup;

  displayedColumnsForForm: string[] = ['lowerLimit', 'upperLimit', 'percentageValue', 'maxAdmissible', 'action'];
  //displayedColumns: string[] = ['sno', 'serSecCode', 'serFinFyr', 'serFinTyr', 'serRateFor', 'action'];
  displayedColumns: string[] = ['serSecCode', 'serFinFyr', 'serFinTyr', 'serRateFor', 'action'];
  formDataSource = new MatTableDataSource();

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource = new MatTableDataSource();
  searchTerm: string = '';
  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  DeletePopup: boolean = false;
  elementToBeDeleted: any;
  entertainMaster: any[] = [];
  standardDeductionRules: any[] = [];
  ddoId: any;
  financialYears: any[] = [];
  submitButtonText: string = 'Save';
  isRateDetailPanel: boolean = false;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  bgColor: string;
  btnCssClass: string = 'btn btn-success';

  constructor(private formBuilder: FormBuilder, private _service: StandardDeductionService, private _msg: CommonMsg) {
    this.ddoId = sessionStorage.getItem('ddoid');
    for (var i = 1999; i < 2021; i++) {
      var j = i + 1;
      var str = i + "-" + j;
      this.financialYears.push(str);
    }
  }

  createForm() {
    this.incomeTaxRatesForm = new FormGroup({
      msSdrID: new FormControl('', [Validators.required]),
      serSecCode: new FormControl('', [Validators.required]),
      serSecDesc: new FormControl(''),
      serSlab: new FormControl(''),
      serFinFyr: new FormControl('', [Validators.required]),
      serFinTyr: new FormControl('', [Validators.required]),
      serRateFor: new FormControl('', [Validators.required]),
      ddoId: new FormControl(this.ddoId),
      rateDetails: new FormArray([], [Validators.minLength(1)])
    });
    this.addRateDetail();
  }

  createRateDetail(): FormGroup {
    return this.formBuilder.group({
      id: new FormControl(this.rateDetails.value.length + 1),
      msSdentRuleID: new FormControl(0),
      serLLimit: new FormControl('', [Validators.required]),
      serULimit: new FormControl('', [Validators.required]),
      serPercValue: new FormControl('', [Validators.required, Validators.pattern("^0*(?:[1-9][0-9]?|100)$")]),
      serValue: new FormControl('', [Validators.required])
    });
  }

  //customValidators(frm: FormGroup): any {
  //  let lLimit = frm.controls.serLLimit.value;
  //  let uLimit = frm.controls.serULimit.value;
  //  if (+lLimit > +uLimit) {
  //    frm.controls.serLLimit.setErrors({ 'invalidError': true });
  //    return { 'invalidError': true };
  //  }
  //  else {
  //    frm.controls.serLLimit.setErrors(null);
  //    if (!lLimit) {
  //      frm.controls.serLLimit.setErrors({ 'required': true })
  //    }
  //    return null;
  //  }
  //}

  onFinYearChange() {
    let finFyr = this.incomeTaxRatesForm.controls.serFinFyr.value;
    let finTyr = this.incomeTaxRatesForm.controls.serFinTyr.value;

    let splitFyr = finFyr.split('-');
    let splitTyr = finTyr.split('-');
    if (splitTyr.length > 1 && splitFyr.length > 1) {
      if (splitFyr[1] > splitTyr[0]) {
        this.incomeTaxRatesForm.controls.serFinFyr.setErrors({ 'invalidError': true });
      }
      else {
        this.incomeTaxRatesForm.controls.serFinFyr.setErrors(null);
      }
    }
  }

  addRateDetail() {
    this.rateDetails.push(this.createRateDetail());
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
  }

  get rateDetails() {
    return this.incomeTaxRatesForm.get("rateDetails") as FormArray;
  }

  deleteRateDetail(index: any) {
    if (this.rateDetails.length > 1) {
      //if (obj.msSdentRuleID && obj.msSdentRuleID > 0) {
      //  this._service.deleteStandardDeductionRateDetail(obj.msSdentRuleID).subscribe(response => {
      //    this.snackBar.open("Entry Deleted", null, { duration: 4000 });
      //  });
      //}
      this.rateDetails.removeAt(index);
      this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    }
  }

  onSubmit() {
    if (this.incomeTaxRatesForm.valid) {
      this._service.upsertStandardDeductionRule(this.incomeTaxRatesForm.value).subscribe(response => {
        if (response > 0) {
          //this.getStandardDeductionRules();
          this.incomeTaxRatesForm.reset();
          if (this.submitButtonText == 'Save') {
            //swal(this._msg.saveMsg);
            this.responseMessage = this._msg.saveMsg;
          } else {
            //swal(this._msg.updateMsg);
            this.responseMessage = this._msg.updateMsg;
          }
          this.isSuccessStatus = true;
          this.isWarningStatus = false;

          this.formGroupDirective.resetForm();
          this.createForm();
          this.getStandardDeductionRules();
        }
        else {
          //swal(this._msg.alreadyExistMsg);
          this.isSuccessStatus = false;
          this.isWarningStatus = true;
          this.responseMessage = this._msg.alreadyExistMsg;       
        }
        this.submitButtonText = 'Save';

        setTimeout(() => {
          this.isSuccessStatus = false;
          this.isWarningStatus = false;
          this.responseMessage = '';
        }, this._msg.messageTimer);
      });
      this.bgColor = '';
      this.btnCssClass = 'btn btn-success';
    }
  }

  cancelForm() {
    this.incomeTaxRatesForm.reset();
    this.formGroupDirective.resetForm();
    this.createForm();
    this.searchTerm = '';
    this.getStandardDeductionRules();
    this.submitButtonText = 'Save';
    this.bgColor = '';
    this.btnCssClass = 'btn btn-success';
  }

  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.getStandardDeductionRules();
  }

  getPaginationData(e) {
    this.pageNumber = e.pageIndex + 1;
    this.pageSize = e.pageSize;
    this.getStandardDeductionRules();
  }

  btnEditClick(element) {
    while (this.rateDetails.length != 0) {
      this.rateDetails.removeAt(0);
    }
   // this.isRateDetailPanel = false;
    this.incomeTaxRatesForm.patchValue(element);
    this.incomeTaxRatesForm.controls.ddoId.setValue(this.ddoId);
    this.bindEntertainMasters(element.msSdrID);
    this.rateDetails.removeAt(0);
    element.rateDetails.forEach(rate => this.rateDetails.push(this.formBuilder.group(rate)));
    //for (var i = 0; i < this.rateDetails.length; i++) {
    //  this.rateDetails.controls[i].setValidators(this.customValidators);
    //}
    this.isRateDetailPanel = true;
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    this.submitButtonText = 'Update';
    this.bgColor = 'bgcolor';
    this.btnCssClass = 'btn btn-info';
  }

  resetRateDetailPanel() {
    this.isRateDetailPanel = false;
    this.bgColor = '';
  }

  DeleteDetails(element) {
    this.elementToBeDeleted = element;
  }

  confirmDelete() {
    this._service.deleteStandardDeductionRule(this.elementToBeDeleted).subscribe(response => {
      this.elementToBeDeleted = null;
      this.pageNumber = 1;
      this.paginator.firstPage();
      this.dataSource.paginator = this.paginator;
      this.getStandardDeductionRules();
      this.createForm();
      this.incomeTaxRatesForm.reset();
      this.formGroupDirective.resetForm();

      //swal(this._msg.deleteMsg);
      this.isWarningStatus = true;
      this.isSuccessStatus = false;
      this.responseMessage=this._msg.deleteMsg;

      this.DeletePopup = false;

      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);
    });
  }

  getEntertainMaster() {
    this._service.getEntertainMaster().subscribe(response => {
      this.entertainMaster = response;
    });
  }

  getStandardDeductionRules() {
    this._service.getStandardDeductionRule(this.pageNumber, this.pageSize, this.searchTerm).subscribe(response => {
      this.standardDeductionRules = response;
      this.dataSource = new MatTableDataSource(response);
      this.totalCount = response[0].totalCount;
    });
  }

  bindEntertainMasters(id) {
    if (id) {
      let selectedData = this.entertainMaster.filter(a => a.msSdrId == id);
      this.incomeTaxRatesForm.controls.msSdrID.setValue(selectedData[0].msSdrId);
      this.incomeTaxRatesForm.controls.serSecCode.setValue(selectedData[0].sdeSecCode);
      this.incomeTaxRatesForm.controls.serSecDesc.setValue(selectedData[0].sdeDesc);
      this.incomeTaxRatesForm.controls.serSlab.setValue(selectedData[0].sdeSlab);
    }
    else {
      this.incomeTaxRatesForm.controls.msSdrID.setValue('');
      this.incomeTaxRatesForm.controls.serSecCode.setValue('');
      this.incomeTaxRatesForm.controls.serSecDesc.setValue('');
      this.incomeTaxRatesForm.controls.serSlab.setValue('');
    }
  }

  ngOnInit() {
    this.createForm();
    this.getEntertainMaster();
    this.getStandardDeductionRules();
    this.submitButtonText = 'Save';
    this.isRateDetailPanel = false;
  }

  //modifyFinancialYearFromInput(event: any, finYr: string) {
  //  if (finYr == 'from') {
  //    if (event.key != "Backspace") {
  //      let value = this.incomeTaxRatesForm.controls.serFinFyr.value;
  //      if (value.length == 4) {
  //        this.incomeTaxRatesForm.controls.serFinFyr.setValue(value + '-');
  //      }
  //    }
  //  }
  //  else {
  //    if (event.key != "Backspace") {
  //      let value = this.incomeTaxRatesForm.controls.serFinTyr.value;
  //      if (value.length == 4) {
  //        this.incomeTaxRatesForm.controls.serFinTyr.setValue(value + '-');
  //      }
  //    }
  //  }
  //}

  validateUpperLimit(index) {
    debugger;
    let lLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serLLimit;
    let uLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serULimit;
    if (this.checkUpperLimitExists(lLmt, uLmt)) {
      this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': true });
      return { 'collapse': true };
    } else {
      if (lLmt == '' || uLmt == '') {
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[index].get("serULimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[index].get("serLLimit").setErrors(null);
          return { 'invalidError': true };
        } else {
          //this.rateDetails.controls[index].get("serULimit").setErrors(null);
          //this.rateDetails.controls[index].get("serLLimit").setErrors(null);
          this.rateDetails.controls.forEach(key => {
            key.get("serULimit").setErrors(null);
            key.get("serLLimit").setErrors(null);
          });
          return null;
        }
      }
    }
  }

  validateLowerLimit(index) {
    debugger;
    let lLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serLLimit;
    let uLmt = this.incomeTaxRatesForm.controls.rateDetails.value[index].serULimit;
    if (this.checkUpperLimitExists(lLmt, uLmt)) {
      this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': true });
      return { 'collapse': true };
    } else {
      //this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': false });
      this.rateDetails.controls.forEach(key => {
        console.log(key);
        key.get("serLLimit").setErrors({ 'collapse': false });
        key.get("serLLimit").setErrors(null);
      });
      //this.rateDetails.controls[index].get("serLLimit").setErrors({ 'collapse': false });
      //this.rateDetails.controls[0].get("serLLimit").setErrors({ 'collapse': false });
      if (lLmt == '' || uLmt == '') {
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[index].get("serLLimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[index].get("serULimit").setErrors({ 'invalidError': false });
          return { 'invalidError': true };
        } else {
          this.rateDetails.controls[index].get("serLLimit").setErrors(null);
          this.rateDetails.controls[index].get("serULimit").setErrors(null);
          return null;
        }
      }
    }
  }

  checkUpperLimitExists(lowerLimt, upperLimit) {
    if (this.incomeTaxRatesForm.controls.rateDetails.value != undefined && this.incomeTaxRatesForm.controls.rateDetails.value.length > 0) {
      if (this.incomeTaxRatesForm.controls.rateDetails.value.length == 1) {
        return false;
      } else {
        let status = this.incomeTaxRatesForm.controls.rateDetails.value.filter(p => (parseInt(p.serLLimit) <= parseInt(lowerLimt) && parseInt(p.serULimit) >= lowerLimt) || (parseInt(p.serLLimit) <= parseInt(upperLimit) && parseInt(p.serULimit) >= upperLimit))
        if (status == undefined || status == null || status == '') {
          return false;
        } else {
          if (status.length == 1 && parseInt(status[0].serLLimit) == lowerLimt && parseInt(status[0].serULimit) == upperLimit) {
            return false;
          } else {
            return true;
          }
        }
      }
    }
  }

}
