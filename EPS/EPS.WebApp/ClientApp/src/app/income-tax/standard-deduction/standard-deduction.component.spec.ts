import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardDeductionComponent } from './standard-deduction.component';

describe('StandardDeductionComponent', () => {
  let component: StandardDeductionComponent;
  let fixture: ComponentFixture<StandardDeductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardDeductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardDeductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
