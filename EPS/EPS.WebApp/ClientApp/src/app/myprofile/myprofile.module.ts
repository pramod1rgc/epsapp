import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { MyprofileRoutingModule } from './myprofile-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MatTableModule } from '@angular/material' 
@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
    MyprofileRoutingModule,
    MatTableModule,
    MaterialModule
  ]
})
export class MyprofileModule { }

