import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyprofileModule } from './myprofile.module';
import { UserProfileComponent } from './user-profile/user-profile.component'
         



const routes: Routes = [

  {


    path: 'UserProfile', component: UserProfileComponent, data: {
      breadcrumb: 'My Profile'
    }
    //, children: [
    //  {
    //    path: 'UserProfile', component: UserProfileComponent, data:
    //    {
    //      breadcrumb: 'Personal Details'
    //    }
    //  }

    //]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyprofileRoutingModule { }
