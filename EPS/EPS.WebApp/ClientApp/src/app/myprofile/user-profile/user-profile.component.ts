import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
import { UserprofileService } from '../myprofile.module/../user-profile/userprofile.service'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private _UserprofileService: UserprofileService) { }
  displayedColumns: string[] = ['name', 'relations', 'dob', 'panNo'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  FamilyDetailsSource: any;
  ArrResult: any;
  //For Personal Details
  uname: string;
  updob: string;
  upan: string;
  ugender: string;
  Uempcode: string;
  UMobileNo: string;
  UJoinDt: string;
  UAdhaarNo: string;
  Email: string;
  EmpType: string;
  EmpSubType: string;
  ServiceTypeName: string;
  joining_CatogaryName: string;
  joining_Catogary: string;
  EmpJoinDt: string;

  //End Of Personal Details
  //Bank Details
  AcNo: string;
  IFSCCode: string;
  BankName: string;
  BranchState: string;
  BranchCity: string;
  BranchName: string;
  BranchAddress: string;
  PANNO: string;
  //END OF Bank Details
  //Array 
  ArrPersonal: any;
  ArrBank: any;
  ArrFamily: any;
  ArrNomnee: any;
  //END Of Array



  ngOnInit() {
    //alert('call')
    this.GetUserProfileDetails();
  }



  GetUserProfileDetails() {
    debugger
    this._UserprofileService.GetUserProfileDetails().subscribe(result => {
      debugger;
      this.ArrResult = result;
      this.ArrPersonal = result[0]
      this.ArrFamily = result[1]
      this.FamilyDetailsSource = new MatTableDataSource(this.ArrFamily);
      this.FamilyDetailsSource.paginator = this.paginator;
      this.FamilyDetailsSource.sort = this.sort;
      this.ArrBank = result[2]
      this.ArrNomnee = result[3]
      this.ArrBank = result[2];
      this.AcNo = this.ArrBank[0].bnkAcNo;
      this.IFSCCode = this.ArrBank[0].ifscCD;
      this.BankName = this.ArrBank[0].bankName;
      this.BranchState = this.ArrBank[0].branchState;
      this.BranchCity = this.ArrBank[0].branchCity;
      this.BranchName = this.ArrBank[0].branchName;
      this.BranchAddress = this.ArrBank[0].branchAddress1;
      this.uname = this.ArrPersonal[0].empname;
      this.updob = this.ArrPersonal[0].empDOB;
      this.ugender = this.ArrPersonal[0].empGender;
      this.upan = this.ArrPersonal[0].empPanNo;
      this.Uempcode = this.ArrPersonal[0].empCd;
      this.UJoinDt = this.ArrPersonal[0].empJoinDt;
      this.UMobileNo = this.ArrPersonal[0].empMobileNo;
      this.UAdhaarNo = this.ArrPersonal[0].empAdhaarNo;
      this.Email = this.ArrPersonal[0].emp_Email;
      this.EmpType = this.ArrPersonal[0].empType;
      this.EmpSubType = this.ArrPersonal[0].empSubType;
      this.ServiceTypeName = this.ArrPersonal[0].serviceTypeName;
      this.joining_CatogaryName = this.ArrPersonal[0].joining_CatogaryName;
      this.joining_Catogary = this.ArrPersonal[0].joining_Catogary;
      this.EmpJoinDt = this.ArrPersonal[0].empJoinDt;
      debugger;
      //alert(this.ArrResult);

    })

  }
}
