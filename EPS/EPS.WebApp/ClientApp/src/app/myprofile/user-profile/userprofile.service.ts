import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserprofileService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  GetUserProfileDetails(): Observable<any> {
    //alert("Call Method");
    return this.httpclient.get<any>(`${this.config.GetUserProfileDetails + '?username=' + sessionStorage.getItem('username')}`);
  }
}
