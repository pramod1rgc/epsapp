import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
//import { MatTableDataSource } from '@angular/material/table';
import { EndofserviceService } from '../../services/endofservice/endofservice.service';
import { PostingDetailsService } from '../../services/empdetails/posting-details.service';
import { FormControl, FormGroup, Validator } from '@angular/forms';
import { Endofservicemodel, Updatemodel } from '../../model/EndofServiceModel/updatemodel';
import { getLocaleDateFormat } from '@angular/common';
import  swal  from 'sweetalert2';




//export interface PeriodicElement2 {

//  orderDate: string;
//  orderNo: number;
//  fromDate: number;
//  action: string;




//}

//const ELEMENT_DATA2: PeriodicElement2[] = [

//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },
//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },
//  { orderNo: 1, orderDate: 'Hydrogen', fromDate: 1.0079, action: 'Verified' },

//];


@Component({
  selector: 'app-endofservice',
  templateUrl: './endofservice.component.html',
  styleUrls: ['./endofservice.component.css'],
  providers: [PostingDetailsService]
})
export class EndofserviceComponent implements OnInit {
  getRedio: any;
  desigID: any;
  successMsg: any;
  showDialog: boolean;
  getDesi: any[];
  getEmp: any[];
  getReason: any[];
  userName: string;
  ddoId: any;
  userId: number;
  roleId: number;
  PermDdoId: any;
  empMode: number;
 btnUpdatetext:any;
  savebuttonstatus: any;
  msCddirID: number;
  validatingForm: FormGroup;
  _endModel: Endofservicemodel;
  _updatemodel: Updatemodel;
  displayedColumns2: string[] = ['endOrderNo', 'endOrderDt', 'endServDt', 'action'];
  dataSource2 = new MatTableDataSource();
  requiredvalue: boolean;
  disabledeath: boolean;
  result: any;
  setDeletIDOnPopup: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('endModel') form: any;
  snackBar: any;
  deletepopup: boolean;
  statusId: any;

  constructor(private endofserviceGetEmp: EndofserviceService, private desing: PostingDetailsService) {
    
  }
  //displayedColumns2: string[] = ['position', 'name', 'weight', 'symbol'];
  //dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
  //@ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this._endModel = new Endofservicemodel();
    //this.dataSource2.sort = this.sort;
    this.btnUpdatetext = 'Save';
    this.savebuttonstatus = true;
    this.userName = sessionStorage.getItem('username');
    this.ddoId = Number(sessionStorage.getItem('ddoid'));    
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.showEmp();
    this.showDesg();
    this.getReasonDetails();
    this.requiredvalue = false;
    this.disabledeath = true;
    this.result = 0;
    this.statusId=50;
    
  }
  endofserviceMaster() {
    //this._endModel.dtOfDeath = null;
    // this._endModel.endReasonId = this.msCddirID;
    this._endModel.createdBy = this.userName;
    this._endModel.statusId = this.statusId;//entry 50, updated 65,79  RC 	Reject by DDO checker,80     V  	verify by DDO checker   

    this._endModel.createdDate = new Date();
    this.endofserviceGetEmp.InsertEndDetails(this._endModel).subscribe(res => {
      this.successMsg = res;
    });
    if (this.result == 0) {
      swal('Data successfully saved');
      // this.snackBar.open('Data Rejected Successfully', null, { duration: 4000 });
    } else { swal('Data successfully updated'); }
    this.showEmpList(this._endModel.msEmpID);
   
    this.successMsg;
  }
  showEmp() {
    
    if (this.desigID == null) {
      this.endofserviceGetEmp.getEndofService('', this.PermDdoId, this.getRedio).subscribe(res => {
        this.getEmp = res;

      });
      this.btnUpdatetext = 'Save';
    }
  }
  getReasonDetails() {

    this.endofserviceGetEmp.getEndofServiceReasonDt().subscribe(res => {
      this.getReason = res;
    });
    debugger;
    this.getReason;
  }
  reasonChange(value: any)
  {
    debugger;
    if (value == "526") {
      this.requiredvalue = true;
      this.disabledeath = false;
    } else {
      this.requiredvalue = false;
      this.disabledeath = true;
      this._endModel.dtOfDeath = null;
    }
  }
  radioChange(value1: any) {
    //debugger;
    this.getRedio = value1;   
  }
  getDesignchange(value: any) {
    debugger;
    this.getEmp = null;
    //this.desigID = null;
    this.desigID = value;
    this.endofserviceGetEmp.getEndofService(this.desigID, this.PermDdoId, this.getRedio).subscribe(res => {
      this.getEmp = res;
      this._endModel.msEmpID = null;
      
    });
  }
  getEmpChange(value: any) {
    debugger;

    this.endofserviceGetEmp.currentEndDetails(value).subscribe(res => {
    
      this._endModel = res[0];
      this._endModel.desigID = this.desigID;
    });
    this.btnUpdatetext = 'Save';
    this.showEmpList(value)
      }
  showDesg() {
    //debugger;
    this.desing.GetAllDesignation().subscribe(res => {
      this.getDesi = res;
    });
  }
  showEmpList(value) {
    
    this.endofserviceGetEmp.getEndofServiceList(value).subscribe(res => {
      this.dataSource2 = new MatTableDataSource(res);
      this.dataSource2.paginator = this.paginator;
      this.dataSource2.sort = this.sort;
    });
  }

  editDetails(empId: any)
  {
    this.endofserviceGetEmp.getEndofServiceList(empId).subscribe(res => {
      this._updatemodel = res[0];
      this._endModel.endOrderNo = this._updatemodel.endOrderNo;
      this._endModel.endOrderDt = this._updatemodel.endOrderDt;
      this._endModel.endServDt = this._updatemodel.endServDt;
      this._endModel.endReasonId = this._updatemodel.endReasonId;
      this._endModel.endRemark = this._updatemodel.endRemark;
      this._endModel.statusId = this._updatemodel.statusId;
      this._endModel.dtOfDeath = this._updatemodel.dtOfDeath;
      this.btnUpdatetext = 'Update';
      this.result = 1;
      this.statusId = 60;
    });
  }
  setDeleteId(value) {
    this.setDeletIDOnPopup = value;
  }
  deleteTptaDetails(hraMasterID) {
    this.endofserviceGetEmp.DeleteEndDetails(hraMasterID).subscribe(res => {      
      if (res != undefined) {
        this.deletepopup = false;
        swal(res)
      }
     
    })
  }

}
