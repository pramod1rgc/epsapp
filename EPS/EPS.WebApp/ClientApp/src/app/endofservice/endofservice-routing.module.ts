import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EndofserviceModule } from './endofservice.module';
import { EndofserviceComponent } from './endofservice/endofservice.component';

const routes: Routes = [

  {
    path: '', component: EndofserviceModule, children: [
      { path: 'endofservice', component: EndofserviceComponent},
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EndofserviceRoutingModule { }
