import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*17-9-19 start*/
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';
/*17-9-19 End*/


import { EndofserviceRoutingModule } from './endofservice-routing.module';
import { EndofserviceComponent } from './endofservice/endofservice.component';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [EndofserviceComponent],
  imports: [
    CommonModule,
    EndofserviceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    MatTooltipModule,
    SharedModule
  ]
})
export class EndofserviceModule { }

