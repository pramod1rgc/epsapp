import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';


@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;
  PermDdoId: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;


  contForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumnsMobileNo1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentMobileNo', 'Status', 'Action']
  displayedColumnsMobileNo2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedMobileNo', 'Status']

  displayedColumnsEmail1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentEmail', 'Status', 'Action']
  displayedColumnsEmail2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedEmail', 'Status']

  displayedColumnsEmpCode1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentEmpEcode', 'Status', 'Action']
  displayedColumnsEmpCode2 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedEmpEcode', 'Status']
  list: any = [];
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;


  @ViewChild('MobileNo1currentPagination', { read: MatPaginator }) MobileNo1currentPagination: MatPaginator;
  @ViewChild('MobileNo2historyPagination', { read: MatPaginator }) MobileNo2historyPagination: MatPaginator;

  @ViewChild('Email1currentPagination', { read: MatPaginator }) Email1currentPagination: MatPaginator;
  @ViewChild('Email2historyPagination', { read: MatPaginator }) Email2historyPagination: MatPaginator;

  @ViewChild('EmpCode1currentPagination', { read: MatPaginator }) EmpCode1currentPagination: MatPaginator;
  @ViewChild('EmpCode2historyPagination', { read: MatPaginator }) EmpCode2historyPagination: MatPaginator;


  dstMobileNo1: MatTableDataSource<any> = new MatTableDataSource<any>();
  dstMobileNo2: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  dstEmail1: MatTableDataSource<any> = new MatTableDataSource<any>();
  dstEmail2: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  dstEmpEcode1: MatTableDataSource<any> = new MatTableDataSource<any>();
  dstEmpEcode2: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  MobileNodiv: boolean = false;
  Emaildiv: boolean = false;
  EmpEcodediv: boolean = false;

  EDITpara: string = '';

  constructor(private _service: ChangeSr, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');

    console.log(this.username);
  }

  ngOnInit() {
    debugger;
    this.Btntxt = 'Save';
    //this.Bindddltype();
    this.RejectPopupcreateForm();
    this.createForm();
    this.infoScreen = true;


  }
  //Bindddltype() { use when the value takes from proc Mobile No./ E-mail /Emp.Code(Org.Code)

  //  debugger;
  //  this._service.GetDdlvalue('contactDetails').subscribe(res => {
  //    debugger;
  //    this.list = res;

  //  })
  //}

  //
  GetcommonMethod(value) {
    debugger;
    this.formGroupDirective.resetForm();
    this.empCd = value;


    if (this.empCd) {
      this.contForm.enable();
      this.contForm.patchValue({
        contactDetails: "MobileNo"
      });
      this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'MobileNo');
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'MobileNo');
    }
    else {
      this.contForm.disable();
      //this.contForm.controls.contactDetails.enable();
    }

   
    this.getcontactDetailsChange("MobileNo");
    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.contForm.disable();
      this.contForm.controls.contactDetails.enable();
    }

    this.MobileNodiv = true;
    this.Emaildiv = false;
    this.EmpEcodediv = false;
  }
  

  getcontactDetailsChange(value) {
    this.Btntxt = 'Save';
    this.fwdtochker = false;
    this.editable = false;
    if (value == 'MobileNo') { //MobileNo

      this.MobileNodiv = true;
      this.Emaildiv = false;
      this.EmpEcodediv = false;
      //

      this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'MobileNo');
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'MobileNo');

      //
      this.contForm.controls["MobileNo"].setValidators([
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]*$')]);
      //
      this.contForm.controls['OrderNo'].setValue('');
      this.contForm.controls['OrderDate'].setValue('');
      this.contForm.controls['Remarks'].setValue('');
      this.contForm.controls['rejectionRemark'].setValue('');
      this.contForm.controls['oldOrderNo'].setValue('');

      this.contForm.controls['MobileNo'].setValue('');
      //
      this.contForm.controls['EmailPart1'].value == "";
      this.contForm.controls['EmailPart1'].disable()
      this.contForm.controls['EmailPart1'].clearValidators();  

      this.contForm.controls['EmailPart2'].value == "";
      this.contForm.controls['EmailPart2'].disable()
      this.contForm.controls['EmailPart2'].clearValidators();


      this.contForm.controls['EmpEcode'].value == "";
      this.contForm.controls['EmpEcode'].disable()
      this.contForm.controls['EmpEcode'].clearValidators();

    }

    else if (value == 'Email') { //Email
      this.MobileNodiv = false;
      this.Emaildiv = true;
      this.EmpEcodediv = false;
      //

      this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'Email');
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'Email');
      //
      this.contForm.controls["EmailPart1"].setValidators([
        Validators.minLength(5),
        Validators.maxLength(80),
        Validators.pattern("^[a-zA-Z0-9_.+-]+$")
      ]);
      this.contForm.controls["EmailPart2"].setValidators([
        Validators.minLength(5),
        Validators.maxLength(80),
        Validators.pattern("^@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
      ]);

   

      //
      this.contForm.controls['OrderNo'].setValue('');
      this.contForm.controls['OrderDate'].setValue('');
      this.contForm.controls['Remarks'].setValue('');
      this.contForm.controls['rejectionRemark'].setValue('');
      this.contForm.controls['oldOrderNo'].setValue('');

      this.contForm.controls['EmailPart1'].setValue('');
      this.contForm.controls['EmailPart2'].setValue('');

      //

      this.contForm.controls['MobileNo'].value == "";
      this.contForm.controls['MobileNo'].disable()
      this.contForm.controls['MobileNo'].clearValidators(); 

      this.contForm.controls['EmpEcode'].value == "";
      this.contForm.controls['EmpEcode'].disable()
      this.contForm.controls['EmpEcode'].clearValidators();
    }
    else if (value == 'EmpEcode') {//EmpEcode
      this.MobileNodiv = false;
      this.Emaildiv = false;
      this.EmpEcodediv = true;
      //

      this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'EmpEcode');
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'EmpEcode');

      //
      this.contForm.controls["EmpEcode"].setValidators([
        Validators.minLength(4),
        Validators.maxLength(10),
        Validators.pattern('^[0-9]*$')]);

      //
      this.contForm.controls['OrderNo'].setValue('');
      this.contForm.controls['OrderDate'].setValue('');
      this.contForm.controls['Remarks'].setValue('');
      this.contForm.controls['rejectionRemark'].setValue('');
      this.contForm.controls['oldOrderNo'].setValue('');

      this.contForm.controls['EmpEcode'].setValue('');


      //
      this.contForm.controls['EmailPart1'].value == "";
      this.contForm.controls['EmailPart1'].disable()
      this.contForm.controls['EmailPart1'].clearValidators();

      this.contForm.controls['EmailPart2'].value == "";
      this.contForm.controls['EmailPart2'].disable()
      this.contForm.controls['EmailPart2'].clearValidators();


      this.contForm.controls['MobileNo'].value == "";
      this.contForm.controls['MobileNo'].disable()
      this.contForm.controls['MobileNo'].clearValidators();

    }
  }


  createForm() {
    this.contForm = new FormGroup({

      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      OrderDate: new FormControl('', Validators.required),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),

      contactDetails: new FormControl(''),

      MobileNo: new FormControl(''),
      EmailPart1: new FormControl(''),
      EmailPart2: new FormControl(''),
      EmpEcode: new FormControl(''),
      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')
    });
    this.contForm.disable();

  }


  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9/-_ ]*$')])
    });

  }


  onSubmit(flag?: any) {
    debugger;
    //console.log(contactDetails);
    this.contForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });
    if (!this.contForm.valid) {
      return false;
    }
    let contactDetails = this.contForm.controls.contactDetails.value;
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }
    //if (this.contForm.valid) {
      debugger;
      this._service.InsertMobileNoEmailEmpCodeDetails(this.contForm.value, flag, contactDetails).subscribe(result => {
        debugger;
        if (contactDetails == 'MobileNo') { //mobile_no
          this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'MobileNo');
          this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'MobileNo');
          this.Btntxt = 'Save';
          if (flag == 'EDIT') {
            this.Btntxt = 'Update';
            if (result == '1') {
              swal(this._msg.updateMsg);
              this.Btntxt = 'Save';
              this.editable = true;
              this.formGroupDirective.resetForm();

            }
            else {
              swal(this._msg.updateFailedMsg);
            }
          }
          else {
            if (result == '1') {
              swal(this._msg.saveMsg);

              this.formGroupDirective.resetForm();
            }
            else {
              swal(this._msg.saveFailedMsg);
              this.formGroupDirective.resetForm();
            }
          }

          this.fwdtochker = false;
          this.contForm.disable();
        }

        else if (contactDetails == 'Email') { //e-mail
          this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'Email');
          this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'Email');

          this.Btntxt = 'Save';
          if (flag == 'EDIT') {
            this.Btntxt = 'Update';
            if (result == '1') {

              swal(this._msg.updateMsg);
              this.Btntxt = 'Save';
              this.editable = true;
              this.formGroupDirective.resetForm();

            }
            else {
              swal(this._msg.updateFailedMsg);
            }
          }
          else {
            if (result == '1') {
              swal(this._msg.saveMsg);

              this.formGroupDirective.resetForm();
            }
            else {
              swal(this._msg.saveFailedMsg);
              this.formGroupDirective.resetForm();
            }
          }
          this.fwdtochker = false;
          this.contForm.disable();
        }
        else if (contactDetails == 'EmpEcode') { //org_code
          this.getcontFormDetails1(this.empCd, this.roleId, 'history', 'EmpEcode');
          this.getcontFormDetails1(this.empCd, this.roleId, 'current', 'EmpEcode');
          this.Btntxt = 'Save';
          if (flag == 'EDIT') {
            this.Btntxt = 'Update';
            if (result == '1') {

              swal(this._msg.updateMsg);
              this.Btntxt = 'Save';
              this.editable = true;
              this.formGroupDirective.resetForm();

            }
            else {
              swal(this._msg.updateFailedMsg);
            }
          }
          else {
            if (result == '1') {
              swal(this._msg.saveMsg);

              this.formGroupDirective.resetForm();
            }
            else {
              swal(this._msg.saveFailedMsg);
              this.formGroupDirective.resetForm();
            }
          }

          this.fwdtochker = false;
          this.contForm.disable();
        }
      });
    //}
  }

  getcontFormDetails1(empCd: any, roleId: any, flag: any, SelectionOptFlag: any) {
    debugger;
    // let contactDetails = this.contForm.get('contactDetails').value;
    if (SelectionOptFlag == 'MobileNo') {
      this.pageNumber = 1;
      this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'MobileNo').subscribe(data => {
        debugger;
        var self = this;
        if (flag == 'current') {
          debugger;
          this.dstMobileNo1 = new MatTableDataSource(data);

          //filter in Data source        

          this.dstMobileNo1.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.currentMobileNo.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstMobileNo1.data.length;
          this.dstMobileNo1.paginator = this.MobileNo1currentPagination;
          //enable or disbale form using current status of orderNo user
          let list: string[] = [];

          data.forEach(element => {
            list.push(element.status);

          });

          if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
            debugger;
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.contForm.patchValue({
              contactDetails: "MobileNo"
            });

            this.infoScreen = true;
          }
          else {
            self.contForm.enable();
            this.infoScreen = false;


          }
          if (this.roleId == '5') {
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.infoScreen = false;
          }
        }
        else if (flag == 'history') {
          //debugger;
          this.dstMobileNo2 = new MatTableDataSource(data);

          //filter in Data source        

          this.dstMobileNo2.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.revisedMobileNo.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstMobileNo2.data.length;
          this.dstMobileNo2.paginator = this.MobileNo2historyPagination;
        }

      });
    }
    else if (SelectionOptFlag == 'Email') {
      this.pageNumber = 1;
      this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'Email').subscribe(data => {
        debugger;
        var self = this;
        if (flag == 'current') {
          debugger;
          this.dstEmail1 = new MatTableDataSource(data);

          //filter in Data source        

          this.dstEmail1.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.currentEmail.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstEmail1.data.length;
          this.dstEmail1.paginator = this.Email1currentPagination;
          //enable or disbale form using current status of orderNo user
          let list: string[] = [];

          data.forEach(element => {
            list.push(element.status);

          });

          if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
            debugger;
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.contForm.patchValue({
              contactDetails: "Email"
            });
            this.infoScreen = true;
          }
          else {
            self.contForm.enable();
            this.infoScreen = false;
          }
          if (this.roleId == '5') {
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.infoScreen = false;
          }
        }
        else if (flag == 'history') {
          //debugger;
          this.dstEmail2 = new MatTableDataSource(data);
          //filter in Data source        

          this.dstEmail2.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.revisedEmail.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstEmail2.data.length;
          this.dstEmail2.paginator = this.Email2historyPagination;
        }

      });
    }
    else if (SelectionOptFlag == 'EmpEcode') {
      this.pageNumber = 1;
      this._service.GetMobileNoEmailEmpCodeDetails(empCd, roleId, flag, 'EmpEcode').subscribe(data => {
        debugger;
        var self = this;
        if (flag == 'current') {
          debugger;
          this.dstEmpEcode1 = new MatTableDataSource(data);

          //filter in Data source        

          this.dstEmpEcode1.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.currentEmpEcode.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstEmpEcode1.data.length;
          this.dstEmpEcode1.paginator = this.EmpCode1currentPagination;
          //enable or disbale form using current status of orderNo user
          let list: string[] = [];

          data.forEach(element => {
            list.push(element.status);

          });

          if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
            debugger;
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.contForm.patchValue({
              contactDetails: "EmpEcode"
            });
            this.infoScreen = true;
          }
          else {
            self.contForm.enable();
            this.infoScreen = false;
          }
          if (this.roleId == '5') {
            self.contForm.disable();
            this.contForm.controls.contactDetails.enable();
            this.infoScreen = false;
          }
        }
        else if (flag == 'history') {
          //debugger;
          this.dstEmpEcode2 = new MatTableDataSource(data);
          //filter in Data source        

          this.dstEmpEcode2.filterPredicate = (data, filter) => {
            return data.empCd.toLowerCase().includes(filter) ||
              data.empName.toLowerCase().includes(filter) ||
              data.revisedEmpEcode.toLowerCase().includes(filter);
          }

        //END Of filter in Data source

          this.pageSize = this.dstEmpEcode2.data.length;
          this.dstEmpEcode2.paginator = this.EmpCode2historyPagination;
        }

      });
    }

  }




  btnEditClick(obj) {
    debugger;
    this.contForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,

      contactDetails: obj.contactDetails,

      MobileNo: obj.mobileNo,
      EmailPart1: obj.emailPart1,
      EmailPart2: obj.emailPart2,
      EmpEcode: obj.empEcode,

      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;
    console.log(this.oldOrderNo);
    debugger;
    this.contForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';


    let contactDetails = obj.contactDetails;

    if (contactDetails == 'MobileNo') {
      let empStatus = obj.status;

      if (empStatus == 'R') {
        this.EDITpara = 'EDIT';
        this.editable = true;
      }
      else if (empStatus == 'E' || empStatus == 'U') {
        this.EDITpara = 'EDIT';
        this.editable = false;
      }
      else {
        this.EDITpara = '';
        this.editable = false;
      }

      if (this.roleId == '6' && empStatus == 'U') {
        this.fwdtochker = true;
      }
      else {
        this.fwdtochker = false;
      }
    }
    else if (contactDetails == 'Email') {
      let empStatus = obj.status;

      if (empStatus == 'R') {
        this.EDITpara = 'EDIT';
        this.editable = true;
      }
      else if (empStatus == 'E' || empStatus == 'U') {
        this.EDITpara = 'EDIT';
        this.editable = false;
      }
      else {
        this.EDITpara = '';
        this.editable = false;
      }

      if (this.roleId == '6' && empStatus == 'U') {
        this.fwdtochker = true;
      }
      else {
        this.fwdtochker = false;
      }
    }
    else if (contactDetails == 'EmpEcode') {
      let empStatus = obj.status;

      if (empStatus == 'R') {
        this.EDITpara = 'EDIT';
        this.editable = true;
      }
      else if (empStatus == 'E' || empStatus == 'U') {
        this.EDITpara = 'EDIT';
        this.editable = false;
      }
      else {
        this.EDITpara = '';
        this.editable = false;
      }

      if (this.roleId == '6' && empStatus == 'U') {
        this.fwdtochker = true;
      }
      else {
        this.fwdtochker = false;
      }
    }


    //let empStatus = obj.status;

    //if (empStatus == 'R') {
    //  this.EDITpara = 'EDIT';
    //  this.editable = true;
    //}
    //else if (empStatus == 'E' || empStatus == 'U') {
    //  this.EDITpara = 'EDIT';
    //  this.editable = false;
    //}
    //else {
    //  this.EDITpara = '';
    //  this.editable = false;
    //}

    //if (this.roleId == '6' && empStatus == 'U') {
    //  this.fwdtochker = true;
    //}
    //else {
    //  this.fwdtochker = false;
    //}



  }
  //email
  applyFiltercurrentEmail1(value) {
    //debugger;

    this.Email1currentPagination.pageIndex = 0;
    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstEmail1.filter = value;
    if (this.dstEmail1.filteredData.length > 0) {
      this.dstEmail1.filter = value.trim().toLowerCase();
    } else {
      this.dstEmail1.filteredData.length = 0;
    }

  }
  applyFilterhistoryEmail2(value) {

    //debugger;
    this.Email2historyPagination.pageIndex = 0;

    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstEmail2.filter = value;

    if (this.dstEmail2.filteredData.length > 0) {
      this.dstEmail2.filter = value.trim().toLowerCase();
    } else {
      this.dstEmail2.filteredData.length = 0;
    }


  }



  applyFiltercurrentMobileNo1(value) {
    //debugger;

    this.MobileNo1currentPagination.pageIndex = 0;
    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstMobileNo1.filter = value;
    if (this.dstMobileNo1.filteredData.length > 0) {
      this.dstMobileNo1.filter = value.trim().toLowerCase();
    } else {
      this.dstMobileNo1.filteredData.length = 0;
    }
  }
  applyFilterhistoryMobileNo2(value) {
    //debugger;
    this.MobileNo2historyPagination.pageIndex = 0;

    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstMobileNo2.filter = value;

    if (this.dstMobileNo2.filteredData.length > 0) {
      this.dstMobileNo2.filter = value.trim().toLowerCase();
    } else {
      this.dstMobileNo2.filteredData.length = 0;
    }
  }

  applyFiltercurrentEmpCode1(value) {

    this.EmpCode1currentPagination.pageIndex = 0;
    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstEmpEcode1.filter = value;
    if (this.dstEmpEcode1.filteredData.length > 0) {
      this.dstEmpEcode1.filter = value.trim().toLowerCase();
    } else {
      this.dstEmpEcode1.filteredData.length = 0;
    }
  }
  applyFilterhistoryEmpCode2(value) {

    //debugger;
    this.EmpCode2historyPagination.pageIndex = 0;

    value = value.trim(); // Remove whitespace
    value = value.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dstEmpEcode2.filter = value;

    if (this.dstEmpEcode2.filteredData.length > 0) {
      this.dstEmpEcode2.filter = value.trim().toLowerCase();
    } else {
      this.dstEmpEcode2.filteredData.length = 0;
    }
  }



  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {
      debugger;
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }
  objToBeDeleted: any;
  txtcontactDetails: any;
  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;
    let contactDetails = this.objToBeDeleted.contactDetails;
    this.contForm.patchValue({
      contactDetails: contactDetails
    });
  }



  confirmDelete() {
    //debugger;

    let contactDetails = this.objToBeDeleted.contactDetails;
   
    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, contactDetails).subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {
        swal(this._msg.deleteMsg);

        this.formGroupDirective.resetForm();

        this.contForm.enable();
        this.infoScreen = false;
      }
      else {
        swal(this._msg.deleteFailedMsg);
      }
      this.contForm.patchValue({
        contactDetails: contactDetails
      });

      this.getcontFormDetails1(this.empCd, this.roleId, 'history', contactDetails);
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', contactDetails);
      this.DeletePopup = false;
      this.editable = false;
    });
  }
  Cancel() {
    //debugger;
    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = null;
  }
  cancelForm() {
    //debugger;
    this.formGroupDirective.resetForm();
    this.contForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {
    //debugger;
    this.oldOrderNo = '';
    this.contForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,

      contactDetails: obj.contactDetails,

      MobileNo: obj.mobileNo,
      EmailPart1: obj.emailPart1,
      EmailPart2: obj.emailPart2,
      EmpEcode: obj.empEcode,

      Remarks: obj.remarks,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo
    });
    //debugger;
    let contactDetails = obj.contactDetails;

    if (contactDetails == 'MobileNo') {
      let empStatus = obj.status;
      this.contForm.disable();
      this.contForm.controls.contactDetails.enable();
      if (this.roleId == '5' && empStatus == 'N') {
        this.contForm.enable();
      }
      else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
        this.infoScreen = false;
      }
      else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
        this.infoScreen = true;
        this.btnCancel = false;
        this.Btntxt = 'Save';
      }
      else {
        this.infoScreen = true;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
      }
    }
    else if (contactDetails == 'Email') {
      let empStatus = obj.status;
      this.contForm.disable();
      this.contForm.controls.contactDetails.enable();
      if (this.roleId == '5' && empStatus == 'N') {
        this.contForm.enable();
      }
      else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
        this.infoScreen = false;
      }
      else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
        this.infoScreen = true;
        this.btnCancel = false;
        this.Btntxt = 'Save';
      }
      else {
        this.infoScreen = true;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
      }
    }
    else if (contactDetails == 'EmpEcode') {
      let empStatus = obj.status;
      this.contForm.disable();
      this.contForm.controls.contactDetails.enable();
      if (this.roleId == '5' && empStatus == 'N') {
        this.contForm.enable();
      }
      else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
        this.infoScreen = false;
      }
      else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
        this.infoScreen = true;
        this.btnCancel = false;
        this.Btntxt = 'Save';
      }
      else {
        this.infoScreen = true;
        this.fwdtochker = false;
        this.Btntxt = 'Save';
      }
    }
    

  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.contForm.valid) {
      return false;
    }
    //debugger;
    let employeeId = this.empCd.trim();
    let orderNo = this.contForm.get('OrderNo').value;

    let contactDetails = this.contForm.controls.contactDetails.value;
   
    this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, null, null, contactDetails).subscribe(result => {
      //debugger;
      if (result == 1) {
        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.fwdtochker = false;
      }
      else {
        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }
      this.contForm.patchValue({
        contactDetails: contactDetails
      });
      this.getcontFormDetails1(this.empCd, this.roleId, 'history', contactDetails);
      this.getcontFormDetails1(this.empCd, this.roleId, 'current', contactDetails);

    });

    this.Btntxt = 'Save';
  }


  updateStatus(status: string) {
    //debugger;    

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.contForm.get('OrderNo').value;

    let contactDetails = this.contForm.controls.contactDetails.value;
  
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, status, rejectionRemark1, contactDetails).subscribe(result => {
          if (result == 1) {

            swal(this._msg.DDORejectedByChecker);

            this.getcontFormDetails1(this.empCd, this.roleId, 'history', contactDetails);
            this.getcontFormDetails1(this.empCd, this.roleId, 'current', contactDetails);

            this.formGroupDirective.reset();
            this.contForm.disable();
            this.infoScreen = false;

          }
          else {

            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(employeeId, orderNo, status, rejectionRemark1, contactDetails).subscribe(result => {
        if (result == 1) {
          swal(this._msg.DDOVerifiedByChecker);

          this.getcontFormDetails1(this.empCd, this.roleId, 'history', contactDetails);
          this.getcontFormDetails1(this.empCd, this.roleId, 'current', contactDetails);

          this.formGroupDirective.reset();

          this.contForm.disable();
          this.infoScreen = false;

        }
        else {
          swal(this._msg.updateFailedMsg);
        }

        this.contForm.patchValue({
          contactDetails: contactDetails
        });

        this.ApprovePopup = false;
        this.RejectPopup = false;


      });
    }
  }




}

