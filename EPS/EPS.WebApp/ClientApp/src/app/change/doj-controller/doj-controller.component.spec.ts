import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DojControllerComponent } from './doj-controller.component';

describe('DojControllerComponent', () => {
  let component: DojControllerComponent;
  let fixture: ComponentFixture<DojControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DojControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DojControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
