import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-state-gis',
  templateUrl: './state-gis.component.html',
  styleUrls: ['./state-gis.component.css']
})
export class StateGisComponent implements OnInit {

  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;
  PermDdoId: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  StateGISForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentStateOtherGIS', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedStateOtherGIS', 'Status']

  list1: any = [];
  list2: any = [];
  EDITpara: string = '';
  StateGISAdjusted: boolean = false;


  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  isSubmitted = false;


  constructor(private _service: ChangeSr, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
  }

  ngOnInit() {
    this.Btntxt = 'Save';
    this.createForm();
    this.Bindddltype();
    this.RejectPopupcreateForm();
    this.infoScreen = true;
    //this.onChanges();//reset field by Y or N
  }

  GetcommonMethod(value) {
    debugger;
    this.formGroupDirective.resetForm();
    this.empCd = value;
    this.isSubmitted = false;

    if (this.empCd) {
      this.StateGISForm.enable();
      //bind form
      this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
      this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');
    }
    else {
      this.StateGISForm.disable();
    }

    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.StateGISForm.disable();
    }

    this.StateGISAdjusted = false;
  }

  //
  createForm() {
    this.StateGISForm = new FormGroup({
      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),

      OrderDate: new FormControl('', Validators.required),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),

      StateOtherGIS: new FormControl('', [Validators.required]),

      StateGISAdjusted: new FormControl('', [Validators.required]),

      StateGISAGOffice: new FormControl(''),
      StateGISPAOCode: new FormControl(''),
      StateGISOtherOffice: new FormControl(''),

      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')
    });
    this.StateGISForm.disable();

  }


  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }
  //onChanges(): void {
  //  this.StateGISForm.get('StateOtherGIS')
  //    .valueChanges
  //    .subscribe(value => {
  //      let StateGISAdjusted = this.StateGISForm.get('StateGISAdjusted');
  //      let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
  //      let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
  //      let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');


  //      if (value === 'N') {
  //        debugger;
  //        this.StateGISAdjusted = false;
  //        StateGISAdjusted.setValue(null);
  //        StateGISAdjusted.disable()
  //        StateGISAdjusted.clearValidators();
  //        StateGISAGOffice.setValue(null);
  //        StateGISAGOffice.disable()
  //        StateGISAGOffice.clearValidators();
  //        StateGISPAOCode.setValue(null);
  //        StateGISPAOCode.disable()
  //        StateGISPAOCode.clearValidators();
  //        StateGISOtherOffice.setValue(null);
  //        StateGISOtherOffice.disable()
  //        StateGISOtherOffice.clearValidators();
  //      }
  //      else if (value === 'Y') {
  //        this.StateGISAdjusted = true;
  //        StateGISAdjusted.enable();
  //        StateGISAdjusted.setValidators([Validators.required]);
  //        StateGISAdjusted.setValue(null);
  //      }
  //    });

  //  this.StateGISForm.get('StateGISAdjusted')
  //    .valueChanges
  //    .subscribe(value => {
  //      let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
  //      let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
  //      let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');


  //      if (value === 'A') {
  //        debugger;
  //        StateGISAGOffice.enable();
  //        StateGISAGOffice.setValidators([Validators.required]);
  //        StateGISAGOffice.setValue(null);
  //      }
  //      else {
  //        StateGISAGOffice.disable()
  //        StateGISAGOffice.clearValidators();
  //      }
  //      if (value === 'P') {
  //        debugger;
  //        StateGISPAOCode.enable();
  //        StateGISPAOCode.setValidators([Validators.required, Validators.maxLength(6), Validators.pattern('^[0-9]*$')]);
  //        StateGISPAOCode.setValue(null);
  //      }
  //      else {
  //        StateGISPAOCode.disable()
  //        StateGISPAOCode.clearValidators();
  //      }
  //      if (value === 'O') {
  //        debugger;
  //        StateGISOtherOffice.enable();
  //        StateGISOtherOffice.setValidators([Validators.required]);
  //        StateGISOtherOffice.setValue(null);
  //      }
  //      else {

  //        StateGISOtherOffice.disable()
  //        StateGISOtherOffice.clearValidators();
  //      }

  //    });
  //}

  getStGIS1DetailsChange(value) {
    let StateGISAdjusted = this.StateGISForm.get('StateGISAdjusted');
    let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
    let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
    let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');


    if (value === 'N') {
      debugger;
      this.StateGISAdjusted = false;
      StateGISAdjusted.setValue(null);
      StateGISAdjusted.disable()
      StateGISAdjusted.clearValidators();
      StateGISAGOffice.setValue(null);
      StateGISAGOffice.disable()
      StateGISAGOffice.clearValidators();
      StateGISPAOCode.setValue(null);
      StateGISPAOCode.disable()
      StateGISPAOCode.clearValidators();
      StateGISOtherOffice.setValue(null);
      StateGISOtherOffice.disable()
      StateGISOtherOffice.clearValidators();
    }
    else if (value === 'Y') {
      this.StateGISAdjusted = true;
      StateGISAdjusted.enable();
      StateGISAdjusted.setValidators([Validators.required]);
      StateGISAdjusted.setValue(null);
    }
  }

  getGIS2DetailsChange(value) {
    let StateGISAGOffice = this.StateGISForm.get('StateGISAGOffice');
    let StateGISPAOCode = this.StateGISForm.get('StateGISPAOCode');
    let StateGISOtherOffice = this.StateGISForm.get('StateGISOtherOffice');

    if (value === 'A') {
      debugger;
      StateGISAGOffice.enable();
      StateGISAGOffice.setValidators([Validators.required]);
      StateGISAGOffice.setValue(null);
    }
    else {
      StateGISAGOffice.disable()
      StateGISAGOffice.clearValidators();
    }
    if (value === 'P') {
      debugger;
      StateGISPAOCode.enable();
      StateGISPAOCode.setValidators([Validators.required, Validators.maxLength(6), Validators.pattern('^[0-9]*$')]);
      StateGISPAOCode.setValue(null);
    }
    else {
      StateGISPAOCode.disable()
      StateGISPAOCode.clearValidators();
    }
    if (value === 'O') {
      debugger;
      StateGISOtherOffice.enable();
      StateGISOtherOffice.setValidators([Validators.required]);
      StateGISOtherOffice.setValue(null);
    }
    else {

      StateGISOtherOffice.disable()
      StateGISOtherOffice.clearValidators();
    }
  }


  //
  Bindddltype() {
    this._service.GetDdlvalue('StateGISAGOffice').subscribe(res => {
      this.list1 = res;

    })
    this._service.GetDdlvalue('StateGISOtherOffice').subscribe(res => {

      this.list2 = res;
    })
  }

  onSubmit(flag?: any) {
    this.isSubmitted = true;
    //debugger;
    this.StateGISForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });

    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }
    if (!this.StateGISForm.valid) {
      return false;
    }
    //console.log(this.StateGISForm.value);
    if (this.StateGISForm.valid) { //change in proc of save & remove snackbar 
      debugger;
      this._service.InsertStateGISFormDetails(this.StateGISForm.value, flag).subscribe(result => {
        debugger;

        this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
        this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');

        this.Btntxt = 'Save';
        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {

            swal(this._msg.updateMsg);
            //bind form

            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
            this.StateGISAdjusted = false;

            // this.isSubmitted = false;
          }
          else {

            swal(this._msg.updateFailedMsg);

          }
        }
        else {
          if (result == '1') {

            swal(this._msg.saveMsg);

            this.formGroupDirective.resetForm();
            this.isSubmitted = false;

          }

          else { //implement this in all forms
            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;

          }
        }
        this.isSubmitted = false;

        this.fwdtochker = false;
        this.StateGISForm.disable();
      });

    }
  }


  getStateGISFormDetails1(empCd: any, roleId: any, flag: any) { //to give role_id parameter & paste pageinate code to 2nd mat-table
    //debugger;
    //this.Bindddltype(); //set ddl
    this.pageNumber = 1;

    this._service.GetStateGISFormDetails(empCd, roleId, flag).subscribe(data => {
      //debugger;
      var self = this;
      if (flag == 'current') {
      
        this.dataSource = new MatTableDataSource(data);
        //filter in Data source
        this.dataSource.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.currentStateOtherGIS.toLowerCase().includes(filter);
        }
        //END Of filter in Data source

        this.pageSize = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        //enable or disbale form using current status of orderNo user
        let list: string[] = [];

        data.forEach(element => {
          list.push(element.status);

        });

        if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
          debugger;
          self.StateGISForm.disable();

          this.infoScreen = true;
        }
        else {
          self.StateGISForm.enable();
        }
        if (this.roleId == '5') {
          self.StateGISForm.disable();
          this.infoScreen = false;
        }


      }
      else if (flag == 'history') {
        //debugger;
        this.dataSource1 = new MatTableDataSource(data);
        //filter in Data source
        this.dataSource1.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.revisedStateOtherGIS.toLowerCase().includes(filter);
        }
        //END Of filter in Data source
        this.pageSize = this.dataSource1.data.length;
        this.dataSource1.paginator = this.historyPagination;
      }

    });
  }

  //EDIT
  compare(o1: any, o2: any) {

    if (o1 == o2)
      return true;
    else
      return false
  }



  btnEditClick(obj) {
    debugger;
    this.StateGISForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,

      StateOtherGIS: obj.stateOtherGIS,
      StateGISAdjusted: obj.stateGISAdjusted,
      StateGISAGOffice: obj.stateGISAGOffice,
      StateGISPAOCode: obj.stateGISPAOCode,
      StateGISOtherOffice: obj.stateGISOtherOffice,

      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;
    console.log(this.oldOrderNo);

    this.StateGISForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';
    debugger;
    let StateOtherGIS = obj.stateOtherGIS;//check Y/N value or not

    if (StateOtherGIS == 'Y') {
      this.StateGISAdjusted = true;
    }
    else if (StateOtherGIS == 'N') {
      this.StateGISAdjusted = false;
    }

    let empStatus = obj.status;

    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }

    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }

  }

  applyFiltercurrent(filterValue) {
    //debugger;

    this.paginator.pageIndex = 0;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  applyFilterhistory(filterValue) {
    //debugger;
    this.historyPagination.pageIndex = 0;

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource1.filter = filterValue;

    if (this.dataSource1.filteredData.length > 0) {
      this.dataSource1.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource1.filteredData.length = 0;
    }
  }


  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {
      debugger;
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }

  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.isSubmitted = false;
    this.Btntxt = 'Save';
    this.fwdtochker = false;
    this.StateGISAdjusted = false;
  }


  confirmDelete() {
    //debugger;
    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'StateGIS').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {
        swal(this._msg.deleteMsg);

        //this.editable = true;
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.StateGISAdjusted = false;
        this.StateGISForm.enable();
        this.infoScreen = false;
      }
      else {
        swal(this._msg.deleteFailedMsg);
      }

      //this.pageNumber = 1;
      this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
      this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');
      this.DeletePopup = false;

    });
  }
  Cancel() {
    //debugger;
    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = null;
  }
  cancelForm() {
    //debugger;
    this.formGroupDirective.resetForm();
    this.StateGISForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {
    //debugger;
    this.oldOrderNo = '';
    this.StateGISForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,
      StateOtherGIS: obj.stateOtherGIS,
      StateGISAdjusted: obj.stateGISAdjusted,
      StateGISAGOffice: obj.stateGISAGOffice,
      StateGISPAOCode: obj.stateGISPAOCode,
      StateGISOtherOffice: obj.stateGISOtherOffice,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo
    });
    //debugger;
    let StateOtherGIS = obj.stateOtherGIS;//check Y/N value or not

    if (StateOtherGIS == 'Y') {
      this.StateGISAdjusted = true;
    }
    else if (StateOtherGIS == 'N') {
      this.StateGISAdjusted = false;
    }

    let empStatus = obj.status;
    this.StateGISForm.disable();

    if (this.roleId == '5' && empStatus == 'N') {
      this.StateGISForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.btnCancel = false;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }

  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.StateGISForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.StateGISForm.get('OrderNo').value;

    this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, null, null).subscribe(result => {
      //debugger;
      if (result == 1) {
        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.isSubmitted = false;
        this.StateGISAdjusted = false;
        this.fwdtochker = false;
      }
      else {
        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }

      this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
      this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');

    });

    this.Btntxt = 'Save';
  }


  updateStatus(status: string) {
    //debugger;   

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.StateGISForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {

            swal(this._msg.DDORejectedByChecker);

            this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
            this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');

            this.formGroupDirective.reset();
            this.isSubmitted = false;
            this.StateGISAdjusted = false;
            this.StateGISForm.disable();
            this.infoScreen = false;
            //this.createForm();

          }
          else {

            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerStateGISUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {
          swal(this._msg.DDOVerifiedByChecker);

          this.getStateGISFormDetails1(this.empCd, this.roleId, 'history');
          this.getStateGISFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.isSubmitted = false;
          this.StateGISAdjusted = false;
          this.StateGISForm.disable();
          this.infoScreen = false;
          //this.createForm();
        }
        else {
          swal(this._msg.updateFailedMsg);
        }
        this.ApprovePopup = false;
        this.RejectPopup = false;

      });
    }
  }

}
