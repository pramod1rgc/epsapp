import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaPDComponent } from './add-ta-pd.component';

describe('AddTaPDComponent', () => {
  let component: AddTaPDComponent;
  let fixture: ComponentFixture<AddTaPDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaPDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaPDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
