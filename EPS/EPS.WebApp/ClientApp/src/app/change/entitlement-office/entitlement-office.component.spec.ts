import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntitlementOfficeComponent } from './entitlement-office.component';

describe('EntitlementOfficeComponent', () => {
  let component: EntitlementOfficeComponent;
  let fixture: ComponentFixture<EntitlementOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntitlementOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntitlementOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
