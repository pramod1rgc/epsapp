import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtensionofserviceComponent } from './extensionofservice.component';

describe('ExtensionofserviceComponent', () => {
  let component: ExtensionofserviceComponent;
  let fixture: ComponentFixture<ExtensionofserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtensionofserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtensionofserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
