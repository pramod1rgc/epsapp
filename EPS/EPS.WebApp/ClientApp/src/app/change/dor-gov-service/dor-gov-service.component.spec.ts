import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DorGovServiceComponent } from './dor-gov-service.component';

describe('DorGovServiceComponent', () => {
  let component: DorGovServiceComponent;
  let fixture: ComponentFixture<DorGovServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DorGovServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DorGovServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
