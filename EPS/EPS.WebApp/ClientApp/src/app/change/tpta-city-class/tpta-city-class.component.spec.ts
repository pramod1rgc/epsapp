import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TptaCityClassComponent } from './tpta-city-class.component';

describe('TptaCityClassComponent', () => {
  let component: TptaCityClassComponent;
  let fixture: ComponentFixture<TptaCityClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TptaCityClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TptaCityClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
