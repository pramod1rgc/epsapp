import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { MatSelect } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs'
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { DesignationModel, PayBillGroupModel, EmpModel, BillCode, DesignCode, EMPCode } from '../../model/Shared/DDLCommon';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

@Component({
  selector: 'app-commonddl',
  templateUrl: './commonddl.component.html',
  styleUrls: ['./commonddl.component.css']
})
export class CommonddlComponent implements OnInit {
  @Output() onchange = new EventEmitter();
  ddlDesign: DesignationModel[];
  ddlBillGroup: PayBillGroupModel[];
  ArrddlEmployee: EmpModel[];
  ddlEmployee: EmpModel[];
  permDdoId: string;
  tempvar: any;
  public designId: string;
  public empid: string;
  username: string;
  ArrEmpCode: any;
  EmpCode: string;
  valueselected: number;
  @Input() data;
  question: number = 0;
  empVfyCode1: string[];
  checkautofill: string;
  selectedDesign: any;
  selectedEmp: any;
  constructor(private _Service: CommanService) { }
  selectedIndex = 0;
  Bill: any[];
  Design: any[];
  EMP: any[];
  /** searching . */
  public billCtrl: FormControl = new FormControl();
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public billFilterCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredBill: ReplaySubject<BillCode[]> = new ReplaySubject<BillCode[]>(1);
  public filteredDesign: ReplaySubject<DesignCode[]> = new ReplaySubject<DesignCode[]>(1);
  public filteredEmp: ReplaySubject<EMPCode[]> = new ReplaySubject<EMPCode[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;
  private _onDestroy = new Subject<void>();
  ngOnInit() {
    debugger;
    this.username = sessionStorage.getItem('username');
    this.FindEmpCode(this.username);
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
  }
  FindEmpCode(username) {
    debugger;
    this._Service.GetEmpCode(username).subscribe(data => {
      debugger;
      this.ArrEmpCode = data[0];
      if (this.ArrEmpCode.empcode != null) {
        this.permDdoId = this.ArrEmpCode.ddoid;
        sessionStorage.setItem('PermDdoId', this.ArrEmpCode.ddoid);
        this.BindDropDownBillGroup(this.permDdoId); //paybill
        this.BackForwordSearchDesignation("0");
        this.BackForwordSearchEmployee("0");
      }
    });
  }
  BindDropDownBillGroup(value) {
    debugger;
    this._Service.GetAllPayBillGroup(value, 'Change').subscribe(result => {
      debugger;
      this.ddlBillGroup = result;
      this.Bill = result;
      this.billCtrl.setValue(this.Bill);
      this.empCtrl.setValue(this.EMP);
      this.filteredBill.next(this.Bill);

    });
  }
  tempdesignvar: any;
  BindDropDownDesignation(value) {
    debugger;
    this.permDdoId = sessionStorage.getItem('PermDdoId');
    this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, 'Change').subscribe(data => {
      this.ddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.empCtrl.setValue(this.EMP);
      this.filteredDesign.next(this.Design);
      this.tempdesignvar = this.designCtrl.value;
      if (this.designCtrl.value.length > 1) {
        this.tempdesignvar = '';
        this.BindDropDownEmployee(this.tempdesignvar);
      }
      this.onchange.emit('');
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BindDropDownEmployee(value) {
    debugger;
    this.designId = value;
    this.permDdoId = sessionStorage.getItem('PermDdoId');
    this.tempvar = this.billCtrl.value;
    if (this.billCtrl.value.length > 2) {
      this.tempvar = '';
    }
    this._Service.GetAllEmpByBillgrIDDesigID(this.designId, this.tempvar, this.permDdoId, 'Change').subscribe(data => {
      debugger;
      this.ddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
      this.onchange.emit('');

    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  BackForwordSearchDesignation(value) {
    this._Service.GetAllDesignationByBillgrID(value, this.permDdoId, 'Change').subscribe(data => {
      debugger;
      this.ddlDesign = data;

      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  BackForwordSearchEmployee(value) {
    this.permDdoId = sessionStorage.getItem('PermDdoId');
    this._Service.GetAllEmpByBillgrIDDesigID(value, this.billCtrl.value, this.permDdoId, 'Change').subscribe(data => {
      debugger;
      this.ddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  public selectionChange($event?: StepperSelectionEvent): void {
    console.log('stepper.selectedIndex: ' + this.selectedIndex
      + '; $event.selectedIndex: ' + $event.selectedIndex);
    if ($event.selectedIndex === 0) { return; }
    this.selectedIndex = $event.selectedIndex;
  }
  private filterBill() {
    debugger;
    if (!this.Bill) {
      return;
    }
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.Bill.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredBill.next(

      this.Bill.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterDesign() {
    debugger;
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    debugger;
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredEmp.next(
      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }
  SelectedValue(value) {
    debugger;
    if (value != "") {
      var splitted = value.split("-", 3);
      this.empid = splitted[0];
      this.selectedEmp = Number(splitted[1]);
      this.selectedDesign = splitted[2].trim();
      this.designId = splitted[2].trim();
      this.onchange.emit(this.empid);
    }
  }
}
