import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PfNpsComponent } from './pf-nps.component';

describe('PfNpsComponent', () => {
  let component: PfNpsComponent;
  let fixture: ComponentFixture<PfNpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PfNpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PfNpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
