import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanNoComponent } from './pan-no.component';

describe('PanNoComponent', () => {
  let component: PanNoComponent;
  let fixture: ComponentFixture<PanNoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanNoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
