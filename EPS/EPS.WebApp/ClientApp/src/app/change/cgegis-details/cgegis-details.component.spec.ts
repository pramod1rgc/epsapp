import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CgegisDetailsComponent } from './cgegis-details.component';

describe('CgegisDetailsComponent', () => {
  let component: CgegisDetailsComponent;
  let fixture: ComponentFixture<CgegisDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CgegisDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CgegisDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
