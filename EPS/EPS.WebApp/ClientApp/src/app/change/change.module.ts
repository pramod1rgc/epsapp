import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangeRoutingModule } from './change-routing.module';
import { ExtensionofserviceComponent } from '../change/extensionofservice/extensionofservice.component';
//import { DialogComponent } from '../dialog/dialog.component';
import { LoanmgtModule } from '../loanmgt/loanmgt.module';
//import { SharecomponentModule } from '../sharecomponent/sharecomponent.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommonddlComponent } from './commonddl/commonddl.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
  
import { DateofbirthComponent } from './dateofbirth/dateofbirth.component';
import { NamegenderComponent } from './namegender/namegender.component';
import { PfNpsComponent } from './pf-nps/pf-nps.component';
import { PanNoComponent } from './pan-no/pan-no.component';
import { HraCityClassComponent } from './hra-city-class/hra-city-class.component';
import { ExservicemanComponent } from './exserviceman/exserviceman.component';
import { DesignationComponent } from './designation/designation.component';
import { CgegisDetailsComponent } from './cgegis-details/cgegis-details.component';
import { JoininModeComponent } from './joinin-mode/joinin-mode.component';
import { CghsDetailsComponent } from './cghs-details/cghs-details.component';
import { CastecategoryDetailsComponent } from './castecategory-details/castecategory-details.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { EntitlementOfficeComponent } from './entitlement-office/entitlement-office.component';
import { DojControllerComponent } from './doj-controller/doj-controller.component';
import { TptaCityClassComponent } from './tpta-city-class/tpta-city-class.component';
import { DoeGovServiceComponent } from './doe-gov-service/doe-gov-service.component';
import { DorGovServiceComponent } from './dor-gov-service/dor-gov-service.component';
import { AddTaPDComponent } from './add-ta-pd/add-ta-pd.component';
import { StateGisComponent } from './state-gis/state-gis.component';
import { CommonMsg } from '../global/common-msg';




@NgModule({
  declarations: [ExtensionofserviceComponent, DateofbirthComponent, NamegenderComponent, CommonddlComponent, PfNpsComponent, PanNoComponent, HraCityClassComponent, ExservicemanComponent, DesignationComponent, CgegisDetailsComponent, JoininModeComponent, CghsDetailsComponent, CastecategoryDetailsComponent, BankDetailsComponent, ContactDetailsComponent, EntitlementOfficeComponent, DojControllerComponent, TptaCityClassComponent, DoeGovServiceComponent, DorGovServiceComponent, AddTaPDComponent, StateGisComponent ],
  imports: [
    CommonModule,
    ChangeRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, LoanmgtModule, SharedModule, NgxMatSelectSearchModule
  ],
  providers: [CommonMsg]
})

export class ChangeModule { }
