import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  BKreadonly: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  BankForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentbankName', 'currentsavingAcNo', 'currentbranchName', 'currentifscCode', 'currentaccNoof', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedbankName', 'revisedsavingAcNo', 'revisedbranchName', 'revisedifscCode', 'revisedaccNoof', 'Status']

  list: any = [];
  EDITpara: string = '';
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  isSubmitted = false;

  constructor(private _service: ChangeSr, private _msg: CommonMsg, private objBankDetailsService: BankDetailsService, private fb: FormBuilder) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');
  }

  ngOnInit() {
    this.Btntxt = 'Save';
    this.createForm();
    this.RejectPopupcreateForm();
    this.infoScreen = true;
  }

  GetcommonMethod(value) {
    debugger;
    this.formGroupDirective.resetForm();
    this.empCd = value;
    this.isSubmitted = false;
    if (this.empCd) {
      this.BankForm.enable();
      this.getBankFormDetails1(this.empCd, this.roleId, 'history');
      this.getBankFormDetails1(this.empCd, this.roleId, 'current');
    }
    else {
      this.BankForm.disable();
    }

    //bind form
    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.BankForm.disable();
    }


  }

  //to Get Bank account by ifsc
  getBankDetailsByIFSC(IFSCcodevalue: string) {
    debugger;
    this.objBankDetailsService.getBankDetailsByIFSC(IFSCcodevalue).subscribe(res => {
      // debugger;

      this.BankForm.patchValue({
        BankName: res.bankName,
        BranchName: res.branchName
      });
    });
  }

  // custom validator to check that two fields match
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  createForm() {
    this.BankForm = this.fb.group({
      OrderNo: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])],
      OrderDate: ['', Validators.required],
      Remarks: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]],

      IFSCCode: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern('^[a-zA-Z][a-zA-Z0-9 ]+$')]],
      BankName: [''],
      BranchName: [''],
      savingAcNo: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9]*$')]],
      conSavingAcNo: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9]*$')]],

      AccNoof: ['', [Validators.required]],
      empCd: [this.empCd],
      username: [this.username],
      rejectionRemark: [this.rejectionRemark],
      oldOrderNo: ['']
    }, {
        validator: this.MustMatch('savingAcNo', 'conSavingAcNo')
      });
    this.BankForm.disable();    

  }



compare(o1: any, o2: any) {
  //
  if (o1 == o2)
    return true;
  else
    return false
}




RejectPopupcreateForm() {

  this.RejectPopupForm = new FormGroup({
    rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
  });

}

onSubmit(flag ?: any) {
  this.isSubmitted = true;
  this.BankForm.patchValue({
    empCd: this.empCd,
    username: this.username
  });
  debugger;
  //check duplicate record then submit
  if (this.oldOrderNo != null && this.Btntxt == "Update") {
    flag = "EDIT";
  }
  if (!this.BankForm.valid) {
    return false;
  }
  if (this.BankForm.valid) { //change in proc of save & remove snackbar 
    debugger;
    this._service.InsertBankFormDetails(this.BankForm.value, flag).subscribe(result => {
      this.getBankFormDetails1(this.empCd, this.roleId, 'history');
      this.getBankFormDetails1(this.empCd, this.roleId, 'current');

      this.Btntxt = 'Save';
      if (flag == 'EDIT') {
        this.Btntxt = 'Update';
        if (result == '1') {
          swal(this._msg.updateMsg);
          //bind form
          this.Btntxt = 'Save';
          this.editable = true;
          this.formGroupDirective.resetForm();
          this.isSubmitted = false;
        }
        else {

          swal(this._msg.updateFailedMsg);

        }
      }
      else {
        if (result == '1') {
          swal(this._msg.saveMsg);
          this.formGroupDirective.resetForm();
          this.isSubmitted = false;
        }

        else {
          swal(this._msg.saveFailedMsg);
          this.formGroupDirective.resetForm();
        }
      }
      this.isSubmitted = false;
      this.fwdtochker = false;
      this.BankForm.disable();
    });

  }

}


getBankFormDetails1(empCd: any, roleId: any, flag: any) { //to give role_id parameter & paste pageinate code to 2nd mat-table


  this.pageNumber = 1;
  this._service.GetBankFormDetails(empCd, roleId, flag).subscribe(data => {

    var self = this;
    if (flag == 'current') {
      debugger;
      this.dataSource = new MatTableDataSource(data);
      //filter in Data source    

      this.dataSource.filterPredicate = (data, filter) => {
        return data.empCd.toLowerCase().includes(filter) ||
          data.empName.toLowerCase().includes(filter) ||
          data.currentbankName.toLowerCase().includes(filter) ||
          data.currentsavingAcNo.toLowerCase().includes(filter) ||
          data.currentbranchName.toLowerCase().includes(filter) ||
          data.currentifscCode.toLowerCase().includes(filter) ||
          data.currentaccNoof.toLowerCase().includes(filter);
      }
        //END Of filter in Data source


      this.pageSize = this.dataSource.data.length;
      this.dataSource.paginator = this.paginator;
      //enable or disbale form using current status of orderNo user
      let list: string[] = [];

      data.forEach(element => {
        list.push(element.status);

      });

      if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
        debugger;
        self.BankForm.disable();

        this.infoScreen = true;
      }
      else {
        self.BankForm.enable();
      }
      if (this.roleId == '5') {
        self.BankForm.disable();
        this.infoScreen = false;
      }

    }
    else if (flag == 'history') {

      this.dataSource1 = new MatTableDataSource(data);

      //filter in Data source    

      this.dataSource1.filterPredicate = (data, filter) => {
        return data.empCd.toLowerCase().includes(filter) ||
          data.empName.toLowerCase().includes(filter) ||
          data.revisedbankName.toLowerCase().includes(filter) ||
          data.revisedsavingAcNo.toLowerCase().includes(filter) ||
          data.revisedbranchName.toLowerCase().includes(filter) ||
          data.revisedifscCode.toLowerCase().includes(filter) ||
          data.revisedaccNoof.toLowerCase().includes(filter);
      }
        //END Of filter in Data source


      this.pageSize = this.dataSource1.data.length;
      this.dataSource1.paginator = this.historyPagination;
    }

  });
}

//EDIT


btnEditClick(obj) {
  debugger;


  this.BankForm.setValue({
    empCd: this.empCd,
    OrderNo: obj.orderNo,
    OrderDate: obj.orderDate,

    IFSCCode: obj.ifscCode,
    BankName: obj.bankName,
    BranchName: obj.branchName,
    savingAcNo: obj.savingAcNo,
    conSavingAcNo: obj.conSavingAcNo,
    AccNoof: obj.accNoof,
    Remarks: obj.remarks,
    rejectionRemark: obj.rejectionRemark,
    username: this.username,
    oldOrderNo: obj.orderNo

  });
  this.oldOrderNo = obj.orderNo;

  this.BankForm.enable();
  this.infoScreen = false;

  //this.fwdtochker = true;
  this.Btntxt = 'Update';

  let empStatus = obj.status;

  if (empStatus == 'R') {
    this.EDITpara = 'EDIT';
    this.editable = true;
  }
  else if (empStatus == 'E' || empStatus == 'U') {
    this.EDITpara = 'EDIT';
    this.editable = false;
  }
  else {
    this.EDITpara = '';
    this.editable = false;
  }


  if (this.roleId == '6' && empStatus == 'U') {
    this.fwdtochker = true;
  }
  else {
    this.fwdtochker = false;
  }

}

applyFiltercurrent(filterValue) {


  this.paginator.pageIndex = 0;
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

  this.dataSource.filter = filterValue;
  if (this.dataSource.filteredData.length > 0) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  } else {
    this.dataSource.filteredData.length = 0;
  }

}
applyFilterhistory(filterValue) {

  this.historyPagination.pageIndex = 0;

  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches


  this.dataSource1.filter = filterValue;

  if (this.dataSource1.filteredData.length > 0) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  } else {
    this.dataSource1.filteredData.length = 0;
  }
}


allowAlphaNumericSpace(event): boolean {
  if (event.target.value === "") {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode !== 32) && // space
      (charCode >= 48 && charCode <= 57) || // numeric (0-9)
      (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
      (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
      return true;
    }
    return false;
  }
}
objToBeDeleted: any;

DeleteChangeDetails(obj) {
  this.objToBeDeleted = obj;
  this.formGroupDirective.resetForm();
  this.isSubmitted = false;
  this.Btntxt = 'Save';
  this.fwdtochker = false;
}


confirmDelete() {

  this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'Bank').subscribe(result => {

    this.objToBeDeleted = null;
    if (result == 1) {
      swal(this._msg.deleteMsg);
      this.formGroupDirective.resetForm();
      this.isSubmitted = false;
      this.BankForm.enable();
      this.infoScreen = false;
    }
    else {

      swal(this._msg.deleteFailedMsg);
    }

    this.getBankFormDetails1(this.empCd, this.roleId, 'history');
    this.getBankFormDetails1(this.empCd, this.roleId, 'current');

    this.DeletePopup = false;

  });
}
Cancel() {

  this.DeletePopup = false;
  this.ApprovePopup = false;
  this.RejectPopup = false;
  this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

  this.rejectionRemark = null;
}
cancelForm() {
  this.formGroupDirective.resetForm();
  this.isSubmitted = false;
  this.BankForm.enable();
  this.editable = false;
  this.fwdtochker = false;
  this.Btntxt = 'Save';
}

btnInfoClick(obj) {

  this.oldOrderNo = '';
  this.BankForm.setValue({
    empCd: this.empCd,
    OrderNo: obj.orderNo,
    OrderDate: obj.orderDate,
    IFSCCode: obj.ifscCode,
    BankName: obj.bankName,
    BranchName: obj.branchName,
    savingAcNo: obj.savingAcNo,
    conSavingAcNo: obj.conSavingAcNo,
    AccNoof: obj.accNoof,
    Remarks: obj.remarks,
    rejectionRemark: obj.rejectionRemark,
    username: this.username,
    oldOrderNo: this.oldOrderNo
  });


  let empStatus = obj.status;
  this.BankForm.disable();

  if (this.roleId == '5' && empStatus == 'N') {
    this.BankForm.enable();
  }
  else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
    this.infoScreen = false;
  }
  else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
    this.infoScreen = true;
    this.btnCancel = false;
    this.Btntxt = 'Save';
  }
  else {
    this.infoScreen = true;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

}

//fwdtochecker

forwardToChecker() {
  //debugger;
  if (!this.BankForm.valid) {
    return false;
  }
  let employeeId = this.empCd.trim();
  let orderNo = this.BankForm.get('OrderNo').value;

  this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, null, null).subscribe(result => {

    if (result == 1) {

      swal(this._msg.forwardDDOCheckerMsg);
      this.formGroupDirective.reset();
      this.isSubmitted = false;
      this.fwdtochker = false;

    }
    else {

      swal(this._msg.forwardDDOCheckerFailedMsg);
      this.fwdtochker = true;
    }

    this.getBankFormDetails1(this.empCd, this.roleId, 'history');
    this.getBankFormDetails1(this.empCd, this.roleId, 'current');

  });

  this.Btntxt = 'Save';
}


updateStatus(status: string) {

  let rejectionRemark1 = '';
  let employeeId = this.empCd;
  let orderNo = this.BankForm.get('OrderNo').value;
  if (status == 'R') {
    rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

    if (this.RejectPopupForm.valid) {
      this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {

          swal(this._msg.DDORejectedByChecker);

          this.getBankFormDetails1(this.empCd, this.roleId, 'history');
          this.getBankFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.isSubmitted = false;
          this.BankForm.disable();
          this.infoScreen = false;

        }
        else {

          swal(this._msg.updateFailedMsg);
        }
        this.ApprovePopup = false;
        this.RejectPopup = false;

        this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

      });
    }
  }
  else {
    this._service.ForwardToCheckerBankFormUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
      if (result == 1) {
        swal(this._msg.DDOVerifiedByChecker);
        this.getBankFormDetails1(this.empCd, this.roleId, 'history');
        this.getBankFormDetails1(this.empCd, this.roleId, 'current');

        this.formGroupDirective.reset();
        this.isSubmitted = false;
        this.BankForm.disable();
        this.infoScreen = false;
        //this.createForm();

      }
      else {

        swal(this._msg.updateFailedMsg);

      }
      this.ApprovePopup = false;
      this.RejectPopup = false;


    });
  }
}

}
