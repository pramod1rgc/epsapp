import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-cghs-details',
  templateUrl: './cghs-details.component.html',
  styleUrls: ['./cghs-details.component.css']
})
export class CghsDetailsComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  CGHSForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['Sr.No', 'EmpCode', 'EmpName', 'currentCGHSDeductionApplicable', 'currentCGHSCardNo', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmpName', 'revisedCGHSDeductionApplicable', 'revisedCGHSCardNo', 'Status']

  list: any = [];
  EDITpara: string = '';
  CGHSCardNo: boolean = false;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  isSubmitted = false;
  constructor(private _service: ChangeSr, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');


  }

  ngOnInit() {
    this.Btntxt = 'Save';
    this.createForm();
    this.RejectPopupcreateForm();

    this.infoScreen = true;
  }

  GetcommonMethod(value) {

    this.formGroupDirective.resetForm();
    this.empCd = value;
    this.isSubmitted = false;

    if (this.empCd) {
      this.CGHSForm.enable();
      //bind form
      this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
      this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

    }
    else {
      this.CGHSForm.disable();
    }


    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.CGHSForm.disable();
    }
   
    this.CGHSCardNo = false;
  }

  getCGHSDetailsChange(value) {

    if (value == 'Y') {
      this.CGHSCardNo = true;
    }
    else if (value == 'N') {
      this.CGHSCardNo = false;

    }
  }

  createForm() {
    this.CGHSForm = new FormGroup({
      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),
      OrderDate: new FormControl('', Validators.required),
      CGHSDeduction: new FormControl('', Validators.required),
      CGHSCardNo: new FormControl('', [Validators.minLength(6), Validators.maxLength(10), Validators.pattern("^[0-9]*$")]),
      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')

    });
    this.CGHSForm.disable();

  }


  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }


  onSubmit(flag?: any) {
    debugger;
    this.isSubmitted = true;


    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }

    if (this.CGHSForm.valid) {

      this._service.InsertCGHSFormDetails(this.CGHSForm.value, flag).subscribe(result => {
        debugger;
        //this.pageNumber = 1;
        this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
        this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

        this.Btntxt = 'Save';

        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {

            swal(this._msg.updateMsg);
            //bind form
            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
            this.CGHSCardNo = false;
          }
          else {

            swal(this._msg.updateFailedMsg);

          }
        }
        else {
          if (result == '1') {
            swal(this._msg.saveMsg);
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
          }
          else {
            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();

          }
        }
        this.fwdtochker = false;
        this.CGHSForm.disable();
      });

    }
  }

  getCGHSFormDetails1(empCd: any, roleId: any, flag: any) {

    this.pageNumber = 1;
    this._service.GetCGHSFormDetails(empCd, roleId, flag).subscribe(data => {
      var self = this;

      if (flag == 'current') {

        this.dataSource = new MatTableDataSource(data);

        //filter in Data source    

        this.dataSource.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.currentCGHSDeductionApplicable.toLowerCase().includes(filter) || data.currentCGHSCardNo.toLowerCase().includes(filter) ;
        }
        //END Of filter in Data source

        this.pageSize = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        let list: string[] = [];
        //enable or disbale form using current status of orderNo user
        data.forEach(element => {
          list.push(element.status);

        });
        if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {

          self.CGHSForm.disable();

          this.infoScreen = true;
        }
        else {
          self.CGHSForm.enable();
        }
        if (this.roleId == '5') {
          self.CGHSForm.disable();
          this.infoScreen = false;
        }
      }
      else if (flag == 'history') {

        this.dataSource1 = new MatTableDataSource(data);

        //filter in Data source        

        this.dataSource1.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.revisedCGHSDeductionApplicable.toLowerCase().includes(filter) || data.revisedCGHSCardNo.toLowerCase().includes(filter);
        }
     
        //END Of filter in Data source
        this.pageSize = this.dataSource1.data.length;
        this.dataSource1.paginator = this.historyPagination;
      }

    });
  }

  btnEditClick(obj) {
    this.CGHSForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,
      CGHSDeduction: obj.cghsDeduction,
      CGHSCardNo: obj.cghsCardNo,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;


    this.CGHSForm.enable();
    this.infoScreen = false;
    debugger;
    //this.fwdtochker = true;
    this.Btntxt = 'Update';

    let cghsDeduction = obj.cghsDeduction;//check cghsDeduction value or not

    if (cghsDeduction == 'Y') {
      this.CGHSCardNo = true;
    }
    else if (cghsDeduction == 'N') {
      this.CGHSCardNo = false;
    }
    let empStatus = obj.status;

    //if (empStatus == 'P' || empStatus == 'V' || empStatus == 'R') {
    //  this.editable = false;
    //}
    //else
    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }


    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }

  }


  applyFiltercurrent(filterValue) {
    //

    this.paginator.pageIndex = 0;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  applyFilterhistory(filterValue) {
    //
    this.historyPagination.pageIndex = 0;

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches


    this.dataSource1.filter = filterValue;

    if (this.dataSource1.filteredData.length > 0) {
      this.dataSource1.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource1.filteredData.length = 0;
    }
  }


  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {

      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }

  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;    
    this.CGHSCardNo = false;
  }

  confirmDelete() {

    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'cghs').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {

        swal(this._msg.deleteMsg);

        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.CGHSCardNo = false;
        this.CGHSForm.enable();
        this.infoScreen = false;
      }
      else {

        swal(this._msg.deleteFailedMsg);
      }
      this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
      this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

      this.DeletePopup = false;

    });
  }

  Cancel() {
    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
    this.rejectionRemark = null;
  }

  cancelForm() {
    this.formGroupDirective.resetForm();
    this.isSubmitted = false;
    this.CGHSForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {

    this.oldOrderNo = '';
    this.CGHSForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,
      CGHSDeduction: obj.cghsDeduction,
      CGHSCardNo: obj.cghsCardNo,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo

    });
    let cghsDeduction = obj.cghsDeduction;//check cghsDeduction value or not
    debugger;
    if (cghsDeduction == 'Y') {
      this.CGHSCardNo = true;
    }
    else if (cghsDeduction == 'N') {
      this.CGHSCardNo = false;
    }
    let empStatus = obj.status;
    this.CGHSForm.disable();

    if (this.roleId == '5' && empStatus == 'N') {
      this.CGHSForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.btnCancel = true;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }
  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.CGHSForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.CGHSForm.get('OrderNo').value;

    this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, null, null).subscribe(result => {

      if (result == 1) {

        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.isSubmitted = false;
        this.fwdtochker = false;

      }
      else {

        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }

      this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
      this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

    });

    this.Btntxt = 'Save';

  }


  updateStatus(status: string) {      

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.CGHSForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {

            swal(this._msg.DDORejectedByChecker);

            this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
            this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

            this.formGroupDirective.reset();
            this.isSubmitted = false;
            this.CGHSForm.disable();
            this.infoScreen = false;
            //this.createForm();

          }
          else {

            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerCGHSUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {


          swal(this._msg.DDOVerifiedByChecker);

          this.getCGHSFormDetails1(this.empCd, this.roleId, 'history');
          this.getCGHSFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.isSubmitted = false;
          this.CGHSForm.disable();
          this.infoScreen = false;
          //this.createForm();

        }
        else {

          swal(this._msg.updateFailedMsg);

        }
        this.ApprovePopup = false;
        this.RejectPopup = false;


      });
    }
  }

}


