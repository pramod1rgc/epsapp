import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CastecategoryDetailsComponent } from './castecategory-details.component';

describe('CastecategoryDetailsComponent', () => {
  let component: CastecategoryDetailsComponent;
  let fixture: ComponentFixture<CastecategoryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CastecategoryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CastecategoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
