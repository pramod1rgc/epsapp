import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HraCityClassComponent } from './hra-city-class.component';

describe('HraCityClassComponent', () => {
  let component: HraCityClassComponent;
  let fixture: ComponentFixture<HraCityClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HraCityClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HraCityClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
