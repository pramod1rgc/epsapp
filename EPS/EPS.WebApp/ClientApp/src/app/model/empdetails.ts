
export interface EmployeeDetailsModel {
  msEmpTypeID: number;
  parentTypeID: number;
  EmpCd: string;
  EmpFirstName: string;
  EmpMiddleName: string;
  EmpLastName: string;
  EmpDOB: Date;
  EmpApptType: string;
  EmpApptTypeName: string;
  EmpPfType: string;
  EmpOffId: string;
  EmpTitle: string;
  EmpEntryDt: Date;
  EmpFieldDeptCd: string;
  EmpDesigCd: number;
  EmpCuroffDt: Date;
  EmpJoinDt: Date;
  Emp_type: string;
  Emp_typeName: string;
  Emp_adhaar_no: string;
  Emp_pan_no: string;
  EmpSubType: string;
  Joining_Catogary: string;
  ServiceType: string;
  IpAddress: string;
  VerifyFlag: string;
  UserId: number;

}
