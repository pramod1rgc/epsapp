export class NgRecovery {
  payNgRecoId: Number;
  ngDedCd: Number;
  ngDesc: string;
  address: string;
  isBankDetails: string;
  ifscCd: string;
  bankId: Number;
  bankName: string;
  branchId: Number;
  branchName: string;
  bnfName: string;
  bankAcNo: string;
  isActive: boolean;
  permDDOId: string;
  pfmsUniqueCode: string;
  venderStatus: string;
  

}
