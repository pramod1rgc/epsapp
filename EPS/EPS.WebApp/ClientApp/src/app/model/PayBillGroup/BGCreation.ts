export class BillGroupCreation {
  payBillGroupId: Number;
  schemeId: Number;
  schemeCode: string;
  accountHead: string;
  serviceType: string;
  group: string;
  groupIds: string;
  billGroupName: string;
  billForGAR: string;
  isActive: boolean;
  serviceTypeId: Number;
  permDDOId: string;

}
