export class Updatemodel {


  msEmpID: any; 
  endOrderNo: any;
  endOrderDt: any;
  remark: string;
  endServDt: any;
  endReasonId: number;
  endReason: string;
  statusId: any;
  dtOfDeath: any;
  endRemark: string;
  createdBy: string;
  createdDate: any;
}


export class Endofservicemodel {
  msEmpID: any;
  desigID: any;
  empCd: any;
  empName: any;
  msCddirID: any;
  cddirCodeText: any;
  msEmpServiceEndID: any;
  empApptType: any;
  superanuationDate: any;
  currentPostingMode: any;
  dateofEntry: any;
  billMonthName: string;
  billYear: number;
  endOrderNo: any;
  endOrderDt: any;
  remark: string;
  endServDt: any;
  endReasonId: number;
  endReason: string;
  statusId: any;
  dtOfDeath: any;
  endRemark: string;
  createdBy: string;
  createdDate: any;

}




