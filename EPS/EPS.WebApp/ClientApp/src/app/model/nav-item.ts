import { Params } from "@angular/router";

export interface NavItem {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: NavItem[];
  }
  export interface IBreadcrumb {
    label: string;
    params?: Params;
    url: string;
  }