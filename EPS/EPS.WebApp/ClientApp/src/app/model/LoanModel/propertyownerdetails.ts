export class PropOwnerDetlsModel {
  msPropOwnerDetlsID: number;
  payLoanPurposeID: number;
  ownerName: string;
  ownerPanNo: string;
  bankId: number;
  branchID: number;
  bankIFSCCODE: string;
  bankAccountNo: number;
  createdIP: string;
  modifiedIP: string;
  createdBy: string;
  createdDate: string;
  modifiedBy: string;
  modifiedDate: string;
  empCd: string;
  msEmpID: number;

}
