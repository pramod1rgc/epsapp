export class Sanction {
  msEmpId: number;
  payLoanRefLoanCD: number=0;
  orderno: number;
  sancOrdDT: Date;
  sancOrdNo: number;
  orderdate: Date;
  amountreqloan: number;
  loanAmtSanc: number;
  empApptType: string;
  empName: string;
  description: string;
  ifscCD: string;
  bankName: string;
  branchName: string;
  bnkAcNo: string;
  ConfirmbnkAcNo: string;
  confirmbnkAcNo: string;
  msPropOwnerDetlsID:number;
  priVerifFlag: number;
  sanction_Remarks: string='';
  release_Remarks: string='';
  loanAmtDisbursed: number;
 

}
