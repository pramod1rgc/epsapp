export class EmpSanction {
  sancOrdDT: Date;
  sancOrdNo: string;
  loanAmtDisbursed: number;
  priTotInst: number;
  priInstAmt: number;
  oddInstNoPri: number;
  oddInstAmtPri: number;
  priLstInstRec: number;
  billMonth: number;
  outstandingAmt: number;
  deductedAmt: number;
  statusId: number;
}

export class EmpSancModel {
  orderNo: string;
  //orderDate: Date;
  recoveredInstNo: number;
  loantype: number;
  mode: number;
  EmpCD: string;
  OrderDt: Date;
  TotAmtRecov: number;
  StatusId: number;

}

export class InstEmpModel {
  OrderNo: string;
  OrderDt: Date;
  TotAmtRecov: number;
  StatusId: number;
  BillMonth: number;
  LoanCD: number;
  payloanrefloandesc: number;
  EmpCD: number;
  MsEmpLoanMultiInstId: number;
  
}
