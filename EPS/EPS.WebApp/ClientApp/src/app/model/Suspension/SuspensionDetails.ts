export class SuspensionDetails {
  Id: number;
  FromDate: Date;
  ToDate: Date;
  OrderNo: string;
  OrderDate: Date;
  SalaryPercent: number;
  Remarks: string;
  PermddoId: string;
  EmpCode: string;
  Status: string;
  IsEditable = false;
  SusPeriod: number;
  RejectedReason: string;
}








