export class RegulariseDetails {
  RegulariseId: number;
  JoiningId: number;
  SuspenCat: number;
  SuspenTreated: number;
  OrderNo: string;
  OrderDate: Date;
  Remarks: string;
  FlagStatus: string;
  RejectedReason: string;
  IPAddress: string;
  IsEditable: boolean;
}

