export class Gpfadvancereasonsmodel {
  msPfAgencyID: number;
  pfType: number;
  pfRefNo: number;
  pfTypeID: number;
  mainReasonText: string;
  mainReasonID: number;
  reasonDescription: string;
  outstandingLimitForPreviousAdvance: string;
  monthsBetweenAdvances: string;
  sealingForAdvancePay: string;
  sealingForAdvanceBalance: string;
  convertWithdrawals: string;
  noOfAdvances: string;
  pFRuleReferenceNumber: string;
  advWithdraw:string
}    

