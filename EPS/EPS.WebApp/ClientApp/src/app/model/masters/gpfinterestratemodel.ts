export class Gpfinterestratemodel {
  gPFInterestID: number;
  wefMonthYear: any;
  toMonthYear: any;
  gPFRuleReferenceNumber: string;
  newGPFInterestRate: string;
}
