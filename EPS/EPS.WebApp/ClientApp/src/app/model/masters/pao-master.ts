export class PaoMastermodel {
  PAOCode: string;
  PAOName :string;
  PAOLang :string;
  PAOAddress :string;
  PAODescription: string;
  PinCode: number;
  ContactNo :string;
  FaxNo :string;
  EmailAddress: string;
  StateId: number;
  CityId: number;
  StateName :string;
  CityName: string;
  ControllerId: number;
  ControllerCode: string;
  constructor() { }
}
export interface MsPaomodel {
  Paoid: number;
  PAOCode: string;
  PAOName: string;
  PAOLang: string;
  PAOAddress: string;
  PAODescription: string;
  PinCode: number;
  ContactNo: string;
  FaxNo: string;
  EmailAddress: string;
  StateId: number;
  CityId: number;
  StateName: string;
  CityName: string;
  ControllerId: number;
  ControllerCode: string;
}
