export interface StateAGOfficers {
  MsStateAgId: number;
  PfType: string;
  PfTypeDesc: string;
  StateAgCode: string;
  StateAgDesc: string;
  StateCode: string;
  StateName: string;
  CreatedIP: string;
}

export interface PfType {
  PfType: string;
  PfTypeDesc: string;
}
export interface State {
  StateCode: string;
  StateName: string;
}
