export class DeductionDefinitionMasterModel {

  payItemsNameShort: string;
  payItemsCD: number;
  subtractedFromSalary: boolean;
  grantNo: number;
  functionalHead: string;
  objectHead: string;
  categoryID: number;
  exemptedFromITax: boolean;
  autoCalculated: boolean;
  constructor() { }
}
