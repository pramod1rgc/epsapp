export  interface DesignationMasterModel {
  CommonDesigSrNo: number;
  GroupCDDesc: string;
  CommonDesigDesc: string;
  CommonDesigGroupCD: number;
  CommonDesigSuperannAge: number
}
