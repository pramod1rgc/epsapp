"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
//export class Album {
//  userId: number;
//  id: number;
//  title: string;
//  communi_address: string;
//  static asFormGroup(album: Album): FormGroup {
//    const fg = new FormGroup({
//      userId: new FormControl(album.userId, Validators.required),
//      id: new FormControl(album.id, Validators.required),
//      title: new FormControl(album.title, Validators.required),
//      communi_address: new FormControl(album.communi_address, Validators.required)
//    });
//    return fg;
//  }
//}
var Deputation_Deduction = /** @class */ (function () {
    function Deputation_Deduction() {
    }
    Deputation_Deduction.asFormGroup = function (deputation_Deduction) {
        var fg = new forms_1.FormGroup({
            payItemsDeductionName: new forms_1.FormControl(deputation_Deduction.payItemsDeductionName, forms_1.Validators.required),
            payItemsDeductionAccCdScheme: new forms_1.FormControl(deputation_Deduction.payItemsDeductionAccCdScheme, forms_1.Validators.required),
            deduction_Schedule: new forms_1.FormControl(deputation_Deduction.deduction_Schedule, forms_1.Validators.required),
            communication_Address: new forms_1.FormControl(deputation_Deduction.communication_Address, forms_1.Validators.required),
            payItemsCd: new forms_1.FormControl(deputation_Deduction.payItemsCd),
            msEmpDuesId: new forms_1.FormControl(deputation_Deduction.msEmpDuesId)
        });
        return fg;
    };
    return Deputation_Deduction;
}());
exports.Deputation_Deduction = Deputation_Deduction;
var SecemeCode = /** @class */ (function () {
    function SecemeCode() {
    }
    return SecemeCode;
}());
exports.SecemeCode = SecemeCode;
//# sourceMappingURL=depuationin-model.js.map