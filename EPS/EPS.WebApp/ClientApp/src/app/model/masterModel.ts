export interface MasterModel {
    MsCddirID: number;
    CddirCodeType: string;
    CddirCodeValue: string;
    CddirCodeText: string;
}

export interface EmployeeTypeMaster{
    msEmpTypeID: number;
    parentTypeID: number;
    codeType: string;
    codeText: string;
}