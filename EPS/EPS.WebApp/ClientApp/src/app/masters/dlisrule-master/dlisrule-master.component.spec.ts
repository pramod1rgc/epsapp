import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DLISRuleMasterComponent } from './dlisrule-master.component';

describe('DLISRuleMasterComponent', () => {
  let component: DLISRuleMasterComponent;
  let fixture: ComponentFixture<DLISRuleMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DLISRuleMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DLISRuleMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
