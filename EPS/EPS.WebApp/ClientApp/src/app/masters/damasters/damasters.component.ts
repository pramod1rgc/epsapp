import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DaMasterModel } from '../../model/masters/da-master-model';
import { DamastersService } from '../../services/masters/damasters.service'
import { MasterService } from '../../services/master/master.service'
import { FormControl} from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort,  MatSnackBar } from '@angular/material';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { CommonMsg } from '../../global/common-msg';
import { PayscaleService } from '../../services/payscale.service';
@Component({
  selector: 'app-damasters',
  templateUrl: './damasters.component.html',
  styleUrls: ['./damasters.component.css'],
  providers: [CommonMsg]
})

export class DAMastersComponent implements OnInit {
  constructor(private http: HttpClient, private daservice: DamastersService, private payscaleservice: PayscaleService, private masterService: MasterService, private _msg: CommonMsg) {
  }
  objda: DaMasterModel;
  objda2: DaMasterModel;
  public statestatus = false;
  public CDAStatus = false;
  Message: any;
  StateList: any;
  dataSource: any;
  btnUpdatetext: any;
  is_datype: boolean;
  is_orderno: boolean;
  is_orderdate: boolean;
  is_newdrate: boolean;
  is_newwef: boolean;
  is_state: boolean;
  is_dasubtype: boolean;
  setDeletIDOnPopup: any;
  savebuttonstatus: boolean;
  TempCurrentDaTypeStatus: any;
  TempcurrentDrate: any;
  TempcurrentWEF: any;
  TempStateid: any;
  searchfield: any;
  IsFlegCDA: boolean;
  IsFlegState: boolean;
  isTableHasData = true;
  is_dis_CDA: boolean;
  is_dis_IDA: boolean;
  infoflag: boolean = false;
  deletepopup: boolean;
  msCddirID: any;
  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  btnCssClass = 'btn btn-success';
  public Stateid: FormControl = new FormControl();
  pipe: DatePipe;
  CommissionCodelist: any;
  displayedColumns: string[] = ['daType', 'daSubType', 'currentDrate', 'currentWEF', 'newDrate', 'newWEF', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('daform') damaster: any;

  ngAfterViewInit() {
  }
  ngOnInit() {
    this.IsFlegCDA = false;
    this.IsFlegState = false;
    this.objda = new DaMasterModel();
    this.objda2 = new DaMasterModel();
    this.objda.DAType = 'Central';
    this.BindState();
    this.BindCommissionCode();
    this.statestatus = false;
    this.CDAStatus = true;
    this.GetDADetails();
    this.objda.DASubType = 'CDA';
    this.btnUpdatetext = 'Save';
    this.savebuttonstatus = true;
    this.GetDACurrent_Rate_Wef('Central', this.objda.DASubType);
  }

  BindState() {
    this.masterService.getState().subscribe(result => {
      this.StateList = result;
    })
  }

  BindCommissionCode() {
    this.payscaleservice.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }
  GetDADetails() {
    this.daservice.GetDADetails().subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.pipe = new DatePipe('en');
      const defaultPredicate = this.dataSource.filterPredicate;
      this.dataSource.filterPredicate = (data, filter) => {
        var newWEF = this.pipe.transform(data.newWEF, 'dd/MM/yyyy');
        var currentWEF = this.pipe.transform(data.currentWEF, 'dd/MM/yyyy');
        return currentWEF.indexOf(filter) >= 0 || newWEF.indexOf(filter) >= 0 || defaultPredicate(data, filter);
      }  
    })
  }

  Edit(msDaMasterID, dAType, dASubType, stateid, currentDrate, currentWEF, orderNo, orderDate, NewDrate, newWEF, payCommissionID) {
    this.objda.MsDaMasterID = msDaMasterID;
    this.ChangeForm(dAType);
    this.objda.DAType = dAType;
    this.objda.DASubType = dASubType;
    this.is_dis_CDA = true;
    this.is_dis_IDA = true;
    if (stateid > 0 && dAType != 'Central') {
      this.objda.Stateid = stateid;
      this.TempStateid = stateid;
    }
    this.is_btnStatus = false;
    this.Message = '';
    this.TempCurrentDaTypeStatus = dAType;
    if (dAType == 'Central') {
      this.IsFlegCDA = false;
      this.IsFlegState = true;
    }
    if (dAType == 'State') {
      this.IsFlegCDA = true;
      this.IsFlegState = false;
    }
    this.objda.payCommissionID = payCommissionID;
    this.TempcurrentDrate=currentDrate;
    this.TempcurrentWEF= currentWEF;
    this.objda.currentDrate = currentDrate;
    this.objda.currentWEF = currentWEF;
    this.objda.OrderNo = orderNo;
    this.objda.OrderDate = orderDate;
    this.objda.NewWEF = newWEF;
    this.objda.NewDrate = NewDrate;
    this.savebuttonstatus = true;
    this.btnUpdatetext = 'Update';
    this.is_orderno = false;
    this.is_orderdate = false;
    this.is_newdrate = false;
    this.is_newwef = false;
    this.is_state = false;
    this.is_dasubtype = false;
    this.is_datype = false;
    this.bgcolor = "bgcolor";
    this.btnCssClass = 'btn btn-info';
  }
  Info(msDaMasterID, dAType, dASubType, stateid, currentDrate, currentWEF, orderNo, orderDate, NewDrate, newWEF, payCommissionID) {
    this.infoflag = true;
    this.objda.MsDaMasterID = msDaMasterID;
    this.ChangeForm(dAType);
    this.objda.DAType = dAType;
    this.objda.DASubType = dASubType;
    this.objda.Stateid = stateid;
    this.objda.payCommissionID = payCommissionID;
    this.objda.currentDrate = currentDrate;
    this.objda.currentWEF = currentWEF;
    this.objda.OrderNo = orderNo;
    this.objda.OrderDate = orderDate;
    this.objda.NewWEF = newWEF;
    this.objda.NewDrate = NewDrate;
    this.savebuttonstatus = false;
    this.is_orderno = true;
    this.is_orderdate = true;
    this.is_newdrate = true;
    this.is_newwef = true;
    this.is_state = true;
    this.is_dasubtype = true;
    this.is_datype = true;
  }

  InsertandUpdate() {
      this.daservice.InsertandUpdate(this.objda).subscribe(result => {
        this.Message = result;
        if (this.Message != undefined) {
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";          
          this.IsFlegCDA = false;
          this.IsFlegState = false;
        }
        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);
        this.GetDADetails();
        this.searchfield = "";
        this.bgcolor = "";
        this.resetEmPDdetailsForm();
        this.ChangeForm('Central');
        this.TempStateid = 0;
        this.TempcurrentDrate= '';
        this.TempcurrentWEF='';
        this.Stateid.setValue(0);   
      })
  }

  Cancel() {
    this.IsFlegCDA = false;
    this.IsFlegState = false;
    $(".dialog__close-btn").click();
    this.resetEmPDdetailsForm();
    this.is_dis_CDA = false;
    this.is_dis_IDA = false;
    this.infoflag = false;
    this.searchfield = "";
    this.applyFilter(this.searchfield);
    this.btnCssClass = 'btn btn-success';
  }

  GetDACurrent_Rate_Wef(DaType, Status) {
    if (this.objda.MsDaMasterID == undefined || this.objda.MsDaMasterID == 0) {
      this.daservice.GetDACurrent_Rate_Wef(DaType, Status).subscribe(result => {
      this.objda2 = result;
      this.objda.currentWEF = this.objda2.currentWEF;
      this.objda.currentDrate = this.objda2.currentDrate;
      })
    }
  }

  GetDACurrent_Rate_WefFromState(DaType) {
    if (this.TempCurrentDaTypeStatus == DaType) {
      this.objda.currentWEF = this.TempcurrentWEF;
      this.objda.currentDrate = this.TempcurrentDrate;
    }   
  }
  Delete(MsDaMasterID) {
    this.daservice.Delete(MsDaMasterID).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
      }
      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
      this.GetDADetails();
    })
  }
  ChangeFormfromstate(status) {

    if (status == 'Central' && this.btnUpdatetext != 'Update' && this.infoflag==false) {
      this.resetEmPDdetailsForm();
      //this.GetDACurrent_Rate_WefFromState('Central');
      this.CDAStatus = true;
      this.statestatus = false;
      this.objda.DASubType = 'CDA';

    }

    else if (status == 'State' && this.btnUpdatetext != 'Update' && this.infoflag == false) {
      this.damaster.resetForm();
     this.objda.Stateid = null;
      this.CDAStatus = false;
      this.statestatus = true;
      this.objda.DASubType = 'IDA';

    }
  }

  StateSelectionChange(Stateid) {
    if ((this.TempStateid == Stateid && this.TempStateid != undefined) && (this.objda.MsDaMasterID == undefined || this.objda.MsDaMasterID > 0)) {
      this.objda.currentWEF = this.TempcurrentWEF;
      this.objda.currentDrate = this.TempcurrentDrate;
    }
    else {
      this.daservice.GetDACurrent_Rate_WefForState('State', Stateid).subscribe(result => {
        this.objda2 = result;
        this.objda.currentWEF = this.objda2.currentWEF;
        this.objda.currentDrate = this.objda2.currentDrate;
      })
    }
  }

  Change_CDA_IDA(Status) {
    if (this.objda.DAType == 'Central') {
      this.GetDACurrent_Rate_Wef('Central', Status);
      this.CDAStatus = true;
      this.statestatus = false;
    }
  }

  ChangeForm(status) {
    if (status == 'Central') {
      this.objda.DASubType = 'CDA'
      this.GetDACurrent_Rate_Wef('Central', this.objda.DASubType);
      this.CDAStatus = true;
      this.statestatus = false;
    }
    else if (status == 'State') {
      this.CDAStatus = false;
      this.statestatus = true;
      this.objda.DASubType = 'IDA';
    }
  }
  SetDeleteId(MsDaMasterID) {
    this.setDeletIDOnPopup = 0;
  }
  public restrictNumeric(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which == 46) {
      return true;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  resetEmPDdetailsForm() {
    this.objda.MsDaMasterID = 0;
    this.is_dis_CDA = false;
    this.is_dis_IDA = false;
    this.ChangeForm('Central');
    this.objda.DAType = 'Central';
    this.objda.DASubType = 'CDA';
    this.savebuttonstatus = true;
    this.btnUpdatetext = 'Save';
    this.is_orderno = false;
    this.is_orderdate = false;
    this.is_newdrate = false;
    this.is_newwef = false;
    this.is_state = false;
    this.is_dasubtype = false;
    this.is_datype = false;
    this.damaster.resetForm();
    this.TempcurrentWEF = '';
    this.TempcurrentDrate = '';
    this.TempStateid = 0;
    this.infoflag = false;
    this.bgcolor = "";
    this.btnCssClass = 'btn btn-success';

  }
  applyFilter(filterValue: string) {
    debugger;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }

  charaterOnlyNoSpace(event): boolean {
    let msg= this._msg.charaterOnlyNoSpace(event);
    return msg;
  }
  
}
