import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DAMastersComponent } from './damasters.component';

describe('DAMastersComponent', () => {
  let component: DAMastersComponent;
  let fixture: ComponentFixture<DAMastersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DAMastersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DAMastersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
