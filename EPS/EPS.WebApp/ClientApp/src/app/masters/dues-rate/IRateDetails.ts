export interface IRateDetails {
       SlabNo :string;
       LowerLimit :string;
       UpperLimit :string;
       Value: string;
       MinAmount : string;
}