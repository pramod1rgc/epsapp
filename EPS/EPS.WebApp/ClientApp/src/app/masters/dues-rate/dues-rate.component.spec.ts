import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesRateComponent } from './dues-rate.component';

describe('DuesRateComponent', () => {
  let component: DuesRateComponent;
  let fixture: ComponentFixture<DuesRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
