import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { DuesRateServicesService } from '../../services/masters/dues-rate-services.service';
import { FormArray } from '@angular/forms';
import { IDuesRate } from './IDuesRate';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';


@Component({
  selector: 'app-dues-rate',
  templateUrl: './dues-rate.component.html',
  styleUrls: ['./dues-rate.component.css'],
  providers: [CommonMsg]
})
export class DuesRateComponent implements OnInit {
  is_btnStatus = true;
  isLoading: boolean;
  isTableHasData = true;
  form: FormGroup;
  savebuttonstatus: boolean;
  submitted = false;
  DuesRateDetails: IDuesRate;
  @ViewChild('DuesRate') DuesDuesMaster: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  btnUpdateText: string
  displayedColumns = ['srNo', 'payRatesPayItemCD', 'payItemsName', 'withEffectFrom', 'withEffectTo', 'value', 'msSlabTypeDes', 'activated', 'action']
  formDataSource = new MatTableDataSource();
  OrgnizationType: any = [];
  StateMode: any = [];
  CityClassModel: any = [];
  DuesCodeDuesDefination: any = [];
  PayCommissions: any = [];
  SlabType: any = [];
  dataSource: any = [];
  deletepopup: boolean;
  setDeletIDOnPopup: any;
  disabledValue: boolean;
  Message: any;
  maxLength: number = 6;
  percentValuePattern: string = '^[0-9]{1,6}$';
  CheckOrgnationType: any;
  msPayRatesID: any;
  duesRate: any[] = [];
  isDisable = false;
  filter: string;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  isOnInit: boolean = false;
  isDuesRateMasterPanel: boolean = false;
  bgColor: string;
  btnCssClass: string = 'btn btn-success';
  constructor(private fb: FormBuilder, private _service: DuesRateServicesService, private _msg: CommonMsg) {
  }
  ngOnInit() {
    this.savebuttonstatus = true;
    this.btnUpdateText = "Save";
    this.GetOrgnizationType();
    this.GetPayCommTypeForDuesRate();
    this.GetDuesCodeDuesDifnation();
    this.GetDuesRateDetails();
    this.FormDeatils();
  }
  FormDeatils() {
    this.form = this.fb.group({
      OrganizationType: [null, Validators.required],
      PayCommission: [null, Validators.required],
      DuesCodeDuesDefination: [null, Validators.required],
      CityClass: [null, Validators.required],
      WithEffectFrom: [null],
      WithEffectTo: [null],
      state: [null],
      SlabType: [null, Validators.required],
      Value: [null, Validators.required],
      VideLetterNo: [null, Validators.required],
      VideLetterDate: [null, Validators.required],
      Activated: ['Yes', Validators.required],
      MsPayRatesID: [null],
      filter:[null],
      RateDetails: this.fb.array([
        this.addDuesRateDetailsFormGroup()
      ])
    }, { validator: this.DateValidation, });
  }
  getControls() {
    return (this.form.get('RateDetails') as FormArray).controls;
  }
  getGroupControl(index, fieldName) {
    return (<FormArray>this.form.get('RateDetails')).at(index).get(fieldName);
  }
  addProduct() {
    (<FormArray>this.form.get('RateDetails')).push(this.addDuesRateDetailsFormGroup())
  }
  onSubmit() {
    debugger;
   this.filter = '';
    this.isLoading = true;
    const controlArray = <FormArray>this.form.get('RateDetails');
    for (let i = 0; i < controlArray.controls.length; i++) {
      const lowerLimit = controlArray.value[i].lowerLimit;
      const upperLimit = controlArray.value[i].upperLimit;
      if (lowerLimit > upperLimit) {
        this.isLoading = false;
        swal("Upper limit is grater than or equal lower limit");
        return false;
      }
    }
    (<FormArray>this.form.get('RateDetails')).controls.forEach((group: FormGroup) => {
      (<any>Object).values(group.controls).forEach((control: FormControl) => {

        control.markAsTouched();
      })
    });
    if (this.form.valid) {
      this.submitted = true;
      this.isDisable = true;
      this._service.InsertUpateDuesRateDetails(this.form.value).subscribe(result1 => {
        if (parseInt(result1) >= 1) {
          if (this.btnUpdateText == 'Update') {
            this.Message = this._msg.updateMsg;
            if (this.Message != undefined) {

              this.deletepopup = false;
              this.isSuccessStatus = true;
              this.responseMessage = this.Message;
              this.resetForm();
            }
            this.GetDuesRateDetails();
            this.bgColor = '';
            this.btnCssClass = 'btn btn-success';
            setTimeout(() => {
              this.isSuccessStatus = false;
              this.isWarningStatus = false;
              this.responseMessage = '';
            }, this._msg.messageTimer);
            this.resetForm();
            this.FormDeatils();
            this.btnUpdateText = 'Save';
            this.isDisable = false;
          }
          else {

            this.resetForm();
            this.FormDeatils();
            this.btnUpdateText == 'Save';
            this.GetDuesRateDetails();
            this.Message = this._msg.saveMsg;
            if (this.Message != undefined) {
              this.deletepopup = false;
              this.isSuccessStatus = true;
              this.responseMessage = this.Message;
            }
            this.bgColor = '';
            this.btnCssClass = 'btn btn-success';
            setTimeout(() => {
              this.isSuccessStatus = false;
              this.isWarningStatus = false;
              this.responseMessage = '';
            }, this._msg.messageTimer);
            this.isDisable = false;
          }
        }
        else {
          this.isDisable = false;
          this.Message = this._msg.alreadyExistMsg;
          if (this.Message != undefined) {
            this.deletepopup = false;
            swal(this.Message)
            this.GetDuesRateDetails();
            this.resetForm();
            this.FormDeatils();
          }
        }

      })
    }

    this.isLoading = false;
  }
  resetForm() {
 //   debugger
    this.form.reset();
    this.formGroupDirective.resetForm();
    this.FormDeatils();
    this.btnUpdateText = "Save";
    this.isLoading = false;
  }
  get rateDetails() {
    return this.form.get("RateDetails") as FormArray;
  }
  omit_special_char(event) {
    var k;
    k = event.charCode;  //
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }
  editButtonClick(obj) {
    this.isDisable = false;
    while (this.rateDetails.length != 0) {
      this.rateDetails.removeAt(0);
    }
    this.btnUpdateText = "Update";
    this.GetStateForDuesRate();
    this.getSlabType(obj.payCommission);
    this.GetDuesCodeDuesDifnation();
    this.GetCityClass(obj.payCommission);
    this.msPayRatesID = obj.msPayRatesID;
    this.form.patchValue(obj);
    this.form.controls.OrganizationType.setValue(obj.organizationType);


    this.form.controls.DuesCodeDuesDefination.setValue(obj.duesCodeDuesDefination)
    this.form.controls.PayCommission.setValue(obj.payCommission)
    this.form.controls.CityClass.setValue(obj.cityClass)
    this.form.controls.WithEffectFrom.setValue(obj.withEffectFrom)
    this.form.controls.WithEffectTo.setValue(obj.withEffectTo)
    this.form.controls.state.setValue(obj.state)
    this.form.controls.SlabType.setValue(obj.slabType)
    this.form.controls.Value.setValue(obj.value)
    this.form.controls.VideLetterNo.setValue(obj.videLetterNo)
    this.form.controls.VideLetterDate.setValue(obj.videLetterDate)
    this.form.controls.Activated.setValue(obj.activated)
    this.form.controls.MsPayRatesID.setValue(obj.msPayRatesID)
    this.rateDetails.removeAt(0);
    obj.rateDetails.forEach(rate => this.rateDetails.push(this.fb.group(rate)));
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    this.CheckOrgnationType = this.form.controls.OrganizationType.setValue(obj.organizationType);

    this.onSelectOrgnization(obj.organizationType)
    this.isLoading = false;
    this.isDuesRateMasterPanel = true;
    this.bgColor = 'bgcolor';
    this.btnCssClass = 'btn btn-info';
  }
  onSelectOrgnization(value: string) {
    if (value === 'A') {
      this.form.get('state').disable();
      this.form.get('state').reset();
    } else {
      this.GetStateForDuesRate();
      this.form.get('state').enable();
    }
  }
  GetDuesRateDetails() {
    this._service.getDuesRatemasterList().subscribe(res => {
      this.duesRate = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
    })
  }
  GetStateForDuesRate() {
    this._service.GetStateForDuesRate().subscribe(res => {
      this.StateMode = res;
    })
  }
  GetOrgnizationType() {
    this._service.GetorginzationTypeForDuesRate().subscribe(res => {
      this.OrgnizationType = res;
    });
  }
  applyFilter(event: Event) {
    const value = (event.target as HTMLInputElement).value;
    //let data = this.dataSource.data;
    if (value.length>0) {
      this.dataSource.filter = value.trim().toLowerCase();
      this.dataSource.filterPredicate = function (data: any, filter) {
        return data.payItemsName.toLowerCase().includes(filter) || data.payRatesPayItemCD.toString().toLowerCase().includes(filter) || data.msSlabTypeDes.toLowerCase().includes(filter) || data.withEffectFrom.includes(filter);
      }
    }
    else {
      this.dataSource = new MatTableDataSource(this.duesRate);
      this.dataSource.paginator = this.paginator;
    }
  }
  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  GetPayCommTypeForDuesRate() {
    this._service.GetPayCommForDuesRate().subscribe(res => {
      this.PayCommissions = res;
    })
  }
  GetDuesCodeDuesDifnation() {
    this._service.GetDuesCodeDuesDefination().subscribe(res => {
      this.DuesCodeDuesDefination = res;
    })
  }
  AddRowOnClick(): void {
    (<FormArray>this.form.get('RateDetails')).push(this.addDuesRateDetailsFormGroup());
  }
  removeRateDetailsButtonClick(skillGroupIndex: number): void {
    const skillsFormArray = <FormArray>this.form.get('RateDetails');
    skillsFormArray.removeAt(skillGroupIndex);
    skillsFormArray.markAsDirty();
    skillsFormArray.markAsTouched();
  }
  addDuesRateDetailsFormGroup(): FormGroup {
    return this.fb.group({
      slabNo: [null, Validators.required],
      lowerLimit: [null, Validators.required],
      upperLimit: [null, Validators.required],
      valueDuesRate: [null, Validators.required],
      minAmount: [null, Validators.required]


    }, { validator: this.Checkvalue, });
  }

  getSlabType(payCommId: number) {

    this._service.GetSlabTypeByPayCommId(payCommId).subscribe(res => {
      this.SlabType = res;
    });

    this._service.GetCityClassForDuesRate(payCommId).subscribe(result => {

      this.CityClassModel = result;
    })
  }
  PercentValueChange(valueParcentageorfixed: string) {
    if (valueParcentageorfixed == 'N') {
      this.maxLength = 6;

      this.percentValuePattern = '^[0-9]{1,6}$';

    }
    else if (valueParcentageorfixed == 'Y') {
      this.maxLength = 3;

      this.percentValuePattern = '^.{1,3}$';
    }
  }
  GetCityClass(payCommId: number) {
    this._service.GetCityClassForDuesRate(payCommId).subscribe(result => {
      this.CityClassModel = result;
    })
  }
  DateValidation(group: FormGroup): { [key: string]: any } | null {
    const WithEffectFrom = group.controls['WithEffectFrom'];
    const WithEffectTo = group.controls['WithEffectTo'];
    if (WithEffectTo.value != null) {

      if (new Date(WithEffectTo.value) < new Date(WithEffectFrom.value)) {
        WithEffectTo.setErrors({ validateDateUL: true })
      }
      else {
        WithEffectTo.setErrors(null);

      }
      return null;
    }
  }
  Checkvalue(group: FormGroup): { [key: string]: any } | null {
    const LowerLimit = group.controls['lowerLimit'];
    const UpperLimit = group.controls['upperLimit'];
    if (UpperLimit.value != null && UpperLimit.value != '') {
      if (+LowerLimit.value > +UpperLimit.value) { // this is the trick
        UpperLimit.setErrors({ validateUL: true });
        return { validateUL: true };
      }
      else {
        UpperLimit.setErrors(null);
        if (!UpperLimit) {
          UpperLimit.setErrors({ 'required': true })

        }
      }
      return null;
    }
  }
  onSelectionChanged({ value }) {

    if (value === 'A') {
      this.form.get('state').disable();
      this.form.get('state').reset();
    } else {
      this.GetStateForDuesRate();
      this.form.get('state').enable();
    }
  }
  SetDeleteId(msDuesrateID) {

    this.setDeletIDOnPopup = msDuesrateID;
    this.disabledValue = true;
    this.resetForm();
  }
  deleteDuesRateSDetails(msDuesrateid) {

    if (this.msPayRatesID == msDuesrateid) {
      this.resetForm();
    }
    this._service.DeleteDuesRateMaster(msDuesrateid).subscribe(result => {
      this.Message = "Record Deleted!";
      if (this.Message != undefined) {
        this.deletepopup = false;
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteMsg;
      }
      this.GetDuesRateDetails();
      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);
   
    })
    this.disabledValue = false;
  }
  btnclose() {
    this.is_btnStatus = false;
  }
}









