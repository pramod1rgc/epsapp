import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DdomasterComponent } from './ddomaster.component';

describe('DdomasterComponent', () => {
  let component: DdomasterComponent;
  let fixture: ComponentFixture<DdomasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DdomasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DdomasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
