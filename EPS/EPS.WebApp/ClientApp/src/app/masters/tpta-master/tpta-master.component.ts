import { Component, forwardRef, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/Master/master.service';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { HraMasterService } from '../../services/Masters/hra-master.service';
import { TptaMasterService } from '../../services/Masters/tpta-master.service';
import { hraModel } from '../../model/masters/hraModel';
import { MatPaginator, MatTableDataSource, MatSort, MatExpansionPanel } from '@angular/material';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators, FormGroupDirective, NG_VALUE_ACCESSOR} from '@angular/forms';
import { DuesRateServicesService } from '../../services/masters/dues-rate-services.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-tpta-master',
  templateUrl: './tpta-master.component.html',
  styleUrls: ['./tpta-master.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TptaMasterComponent),
    multi: true,
  }]
})
export class TptaMasterComponent implements OnInit {
  tptaMasterForm: FormGroup;
  states: any[] ;
  username: any;
  CommissionCodelist: any[] = [];
  SlabTypelist: any[]=[] ;
  CityClass: any[]=[] ;
  dataSource: any;
  setDeletIDOnPopup: any;
  UnionTerritory: any;
  disbleflag = false;
  btnUpdatetext: any;
  savebuttonstatus: boolean;
  disableFlag: boolean;
  is_btnStatus: boolean;
  _pScaleObj: PayscaleModel;
  _hraModel: hraModel;
  isTableHasData = true;
  searchfield: string='';
  deletepopup: any;
  Message: any;
  divbgcolor: string;
  showhidestate = true;
  showhidebtn = true;
  bgcolor: string;
  btnCssClass = 'btn btn-success';

  constructor(private master: MasterService, private hraMaster: HraMasterService, private tptaMaster: TptaMasterService, private formBuilder: FormBuilder,
    private _service: DuesRateServicesService, private comnmsg: CommonMsg) {
  }
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  displayedColumns: string[] = ['employeeType', 'payCommisionName', 'stateName', 'slabNo', 'cityClass', 'slabtypDesc', 'lowerLimit', 'upperLimit', 'minvalue', 'tptaAmount', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('hraform') form: any;
  @ViewChild('panelTpta') tptaPanel: MatExpansionPanel;
  formDataSource = new MatTableDataSource();


  ngOnInit() {
    this.createForm();
    this.btnUpdatetext = 'Save';
    this._pScaleObj = new PayscaleModel();
    this._hraModel = new hraModel();
    this.bindState();
    this.getTptaMasterDetails();
    this.savebuttonstatus = true;
    this.username = sessionStorage.getItem('username');
    this.disableFlag = false;
  }


  createForm() {
    this.tptaMasterForm = new FormGroup({
      empTypeID: new FormControl('', [Validators.required]),
      payCommissionCode: new FormControl('', [Validators.required]),
      stateId: new FormControl('0', [Validators.required]),
      stateName: new FormControl(''),
      loginUser: new FormControl(''),
      tptaMasterID: new FormControl(0),
      rateDetails: new FormArray([], [Validators.minLength(1)])      
    });
    this.addRateDetail();
  } 

  createRateDetail(): FormGroup {
    return this.formBuilder.group({
      tptaMasterID: new FormControl(0),
      slabNo: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      slabType: new FormControl('', [Validators.required]),
      lowerLimit: new FormControl('', [Validators.required]),
      upperLimit: new FormControl('', [Validators.required]),
      minvalue: new FormControl('', [Validators.required]),
      tptaAmount: new FormControl('', [Validators.required])
    }); //, { validators: this.LimitValidators}

  }

  addRateDetail() {
    this.rateDetails.push(this.createRateDetail());
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
  }

  get rateDetails() {
    return this.tptaMasterForm.get("rateDetails") as FormArray;
  }

  deleteRateDetail(index: any) {
    if (this.rateDetails.length > 1) {
      this.rateDetails.removeAt(index);
      this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    }
  }

  bindState() {
    this.master.getState().subscribe(res => {
      this.states = res;
    });
  }

  getPayCommision(empTypeID: string) {
    this._hraModel.payCommissionCode = '';
    this.hraMaster.getPayCommissionByEmployeeType(empTypeID).subscribe(results => {
      this.CommissionCodelist = results;
      
    })
    if (this.tptaMasterForm.controls.empTypeID.value == 1)
      this.showhidestate = false;
    else
      this.showhidestate = true;
  }


  getSlabType(payCommId: number) {
    debugger;
    //this.tptaMasterForm.get('city').patchValue("");
      this._service.GetSlabTypeByPayCommId(payCommId).subscribe(res => {
        this.SlabTypelist = res;
      })
    this.hraMaster.getCityClass(payCommId).subscribe(res => {
      this.CityClass = res;
    })

  }

  getPayCommisionByID(empTypeID: string) {
    this.hraMaster.getPayCommissionByEmployeeType(empTypeID).subscribe(results => {
      this.CommissionCodelist = results;
    })
  }

  createTptaMaster() {
    //debugger;
    if (this.tptaMasterForm.controls["stateId"].value == 0 && this.tptaMasterForm.controls.empTypeID.value != 1) {
      this.tptaMasterForm.controls["stateId"].setErrors({ 'invalidError': true });
      return;
    }
    if (this.tptaMasterForm.valid) {
      this.tptaMasterForm.controls.loginUser.setValue(this.username);
      this.tptaMaster.createTptaMaster(this.tptaMasterForm.value).subscribe(result => {
        if (result != undefined) {
          this.deletepopup = false;
          if (this.btnUpdatetext == "Update") {
            this.Message = this.comnmsg.updateMsg;
            this.is_btnStatus = true;
            this.divbgcolor = "alert-success";
          }
          else if (this.btnUpdatetext == "Save") {
            this.Message = result;
            this.is_btnStatus = true;
            this.divbgcolor = "alert-success";
          }
        }
        this.getTptaMasterDetails();
        this.clearInput();
        this.formGroupDirective.resetForm();
        this.bgcolor = "";

        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);

      }) }
    
  }


  editTptaMaster(obj) {
    while (this.rateDetails.length != 0) {
      this.rateDetails.removeAt(0);
    }
    this.tptaPanel.open();
    this.tptaMasterForm.patchValue(obj);
    this.rateDetails.removeAt(0);
    obj.rateDetails.forEach(rate => this.rateDetails.push(this.formBuilder.group(rate)));
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    this.getPayCommisionByID(obj.empTypeID);
    this.getSlabType(obj.payCommissionCode);
    this.bindState();
    this.btnUpdatetext = 'Update';
    this.btnCssClass = 'btn btn-info';
    this.disableFlag = true;
    this.showhidebtn = false;
    this.bgcolor = "bgcolor";
    if (obj.empTypeID == 1)
      this.showhidestate = false;
    else
      this.showhidestate = true;
  }

  getTptaMasterDetails() {
    this.tptaMaster.getTptaMasterDetails().subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  setDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }


  deleteTptaDetails(hraMasterID) {
    this.tptaMaster.deleteTptaMaster(hraMasterID).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        this.Message = this.comnmsg.deleteMsg;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
      }
      this.createForm();
      this.getTptaMasterDetails();
      this.searchfield = "";
      this.bgcolor = "";

      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
    })
  }

  ltrim(searchfield) {
    return searchfield.replace(/^\s+/g, '');
  }
  

  applyFilter(filterValue: string) {
    this.searchfield = this.ltrim(this.searchfield);
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function (data, filter: string): boolean {
      return data.payCommisionName.toLowerCase().includes(filter) || data.stateName.toLowerCase().includes(filter) || data.employeeType.toLowerCase().includes(filter)
        || data.slabNo.includes(filter) || data.cityClass.toLowerCase().includes(filter) || data.slabtypDesc.toLowerCase().includes(filter)
        || data.lowerLimit.includes(filter) || data.upperLimit.includes(filter) || data.minvalue.includes(filter) || data.tptaAmount.includes(filter); 
    }
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
    
  }

  clearInput() {
    this.tptaMasterForm.reset();
    this.formGroupDirective.resetForm();
    this.createForm();
    this.btnUpdatetext = 'Save';
    this.disableFlag = false;
    this.showhidebtn = true;
    this.bgcolor = "";
    this.btnCssClass = 'btn btn-success';
  }

  
  // Validation start
  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  
  validateUpperLimit(i) {
    let lLmt = this.tptaMasterForm.controls.rateDetails.value[i].lowerLimit;
    let uLmt = this.tptaMasterForm.controls.rateDetails.value[i].upperLimit;
      if (lLmt == '' || uLmt == '') {
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[i].get("upperLimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[i].get("lowerLimit").setErrors(null);
          return { 'invalidError': true };
        } else {
          this.rateDetails.controls[i].get("upperLimit").setErrors(null);
          this.rateDetails.controls[i].get("lowerLimit").setErrors(null);
          return null;
        }
      }
    }

  validateLowerLimit(i) {
    let lLmt = this.tptaMasterForm.controls.rateDetails.value[i].lowerLimit;
    let uLmt = this.tptaMasterForm.controls.rateDetails.value[i].upperLimit;
    if (lLmt == '' || uLmt == '') {
        return null;
      } else {
        if (+lLmt > +uLmt) {
          this.rateDetails.controls[i].get("lowerLimit").setErrors({ 'invalidError': true });
          this.rateDetails.controls[i].get("upperLimit").setErrors({ 'invalidError': false });
          return { 'invalidError': true };
        } else {
          this.rateDetails.controls[i].get("lowerLimit").setErrors(null);
          this.rateDetails.controls[i].get("upperLimit").setErrors(null);
          return null;
        }
      }
    }


  validateMinAmt(i) { 
    let lLmt = this.tptaMasterForm.controls.rateDetails.value[i].minvalue;
    let uLmt = this.tptaMasterForm.controls.rateDetails.value[i].tptaAmount;
    if (lLmt == '' || uLmt == '') {
      return null;
    } else {
      if (+lLmt > +uLmt) {
        this.rateDetails.controls[i].get("minvalue").setErrors({ 'invalidError': true });
        this.rateDetails.controls[i].get("tptaAmount").setErrors({ 'invalidError': false });
        return { 'invalidError': true };
      } else {
        this.rateDetails.controls[i].get("minvalue").setErrors(null);
        this.rateDetails.controls[i].get("tptaAmount").setErrors(null);
        return null;
      }
    }
  }

  validateUpperAmt(i) {
    let lLmt = this.tptaMasterForm.controls.rateDetails.value[i].minvalue;
    let uLmt = this.tptaMasterForm.controls.rateDetails.value[i].tptaAmount;
    if (lLmt == '' || uLmt == '') {
      return null;
    } else {
      if (+lLmt > +uLmt) {
        this.rateDetails.controls[i].get("tptaAmount").setErrors({ 'invalidError': true });
        this.rateDetails.controls[i].get("minvalue").setErrors(null);
        return { 'invalidError': true };
      } else {
        this.rateDetails.controls[i].get("tptaAmount").setErrors(null);
        this.rateDetails.controls[i].get("minvalue").setErrors(null);
        return null;
      }
    }
  }

// Validation End

}
