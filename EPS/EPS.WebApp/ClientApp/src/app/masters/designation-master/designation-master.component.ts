
import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { DesignationMasterModel } from '../../model/masters/designation-master';
import { DesignationMasterService } from '../../services/masters/designation-master.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
export interface Group {
  GroupCd: string;
  GroupId: number;
}
const Group_DATA: Group[] = [
  { GroupId: 1, GroupCd: 'A',},
  { GroupId: 2, GroupCd: 'B',},
  { GroupId: 3, GroupCd: 'C',},
  { GroupId: 4, GroupCd: 'D' },
];

@Component({
  selector: 'app-designation-master',
  templateUrl: './designation-master.component.html',
  styleUrls: ['./designation-master.component.css']
})
export class DesignationMasterComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  objdesigModel: DesignationMasterModel = {} as any;
  selectedOption: number;
  response: any;
  designationForm: FormGroup;
  displayedColumns: string[] = ['commonDesigDesc', 'groupCDDesc','commonDesigSuperannAge', 'action'];
  group_data = Group_DATA;
  dataSource: any;
  DesignationList: any = [];
  filterList: any = [];
  errorMsg: string;
  MsDeleteId: any;
  disbleflag = false;
  searchvalue: any;

  isSuccessStatus: boolean = false;
  responseMessage: string = '';
  isPanel: boolean = false;
  isLoading: boolean = false;
  bgColor: string = '';
  btnCssClass: string = 'btn btn-success';
  btnUpdatetext: any;

  @ViewChild('designmaster') designmasterForm: any;
  deletepopup: boolean;
  constructor(private _service: DesignationMasterService, private _msg: CommonMsg) { }

  showMessage(msg) {
    this.responseMessage = msg;
    this.isSuccessStatus = true;
    this.isLoading = false;
    this.bgColor = '';
    setTimeout(() => this.isSuccessStatus = false, this._msg.messageTimer);
  }
  btnSaveclick() {
    this.isLoading = true;
    if (document.getElementById('btn_save').innerHTML == 'Update') {
     
      this._service.updateMsdesign(this.objdesigModel).subscribe(res => {
        //this.response = res
        this.showMessage(res);
        this.bindDesigList();
       // swal(this.response);
        
         this.resetForm();
      })
    }
    if (document.getElementById('btn_save').innerHTML == 'Save') {
      
      this._service.insertMsdesign(this.objdesigModel).subscribe(res => {
        //this.response = res
        this.showMessage(res);
       // swal(this.response);
        this.bindDesigList();
        this.resetForm();

      })
    }

  }
  resetForm() {
    this.designmasterForm.resetForm();
    this.errorMsg = null;
    this.disbleflag = false;
    this.searchvalue= null;
    this.bindLeaveTypeMaxid();
    this.btnUpdatetext = 'Save';
   // document.getElementById('btn_save').innerHTML = 'Save';
    
    }
  btnEditClick(CommonDesigDesc, CommonDesigGroupCD, CommonDesigSrNo, CommonDesigSuperannAge) {
    this.bgColor = 'bgcolor';
    this.btnUpdatetext = 'Update';
    this.isPanel = true;
    this.btnCssClass = 'btn btn-info';

    this.disbleflag = false;
    this.errorMsg = null;
    this.objdesigModel.CommonDesigDesc = CommonDesigDesc;
    this.objdesigModel.CommonDesigGroupCD = CommonDesigGroupCD;
    this.objdesigModel.CommonDesigSrNo = CommonDesigSrNo;
    this.objdesigModel.CommonDesigSuperannAge = CommonDesigSuperannAge;
   // document.getElementById('btn_save').innerHTML = 'Update';
  }
  ngOnInit() {
    this.isLoading = true;
    this.btnUpdatetext = 'Save';
    this.bindDesigList();
    this.bindLeaveTypeMaxid();
  }
 
  bindDesigList() {
    debugger;
    this._service.getMsdesignDetails().subscribe(res => {
      this.DesignationList = res;    
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    })
  }
  bindLeaveTypeMaxid() {
    this._service.getMsdesignMaxid().subscribe(res => {
      this.objdesigModel.CommonDesigSrNo = res; 
    })
  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  checkAllreadyexist() {
    this.disbleflag = true;
    debugger;
    this.filterList = this.DesignationList.filter(item => item.commonDesigDesc.toLowerCase() == this.objdesigModel.CommonDesigDesc.toLowerCase() && item.commonDesigSuperannAge == this.objdesigModel.CommonDesigSuperannAge && item.commonDesigGroupCD== this.objdesigModel.CommonDesigGroupCD)
  if (this.filterList.length>0) {
      this.disbleflag = false;
      this.errorMsg = 'Record already exist !!!!!!!';
    }
    else {
      this.errorMsg =null;
    }
  }
  SetDeleteId(msid) {
    this.MsDeleteId = msid;
  }
  DeleteDetails(Msdesignid) {
    this.isLoading = true;
    this._service.DeleteMsdesign(Msdesignid).subscribe(res => {
      this.showMessage(res);
      this.bindDesigList();
      this.resetForm();
      this.deletepopup = false;
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



}
