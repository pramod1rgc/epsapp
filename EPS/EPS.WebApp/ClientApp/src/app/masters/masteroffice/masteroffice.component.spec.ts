import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterofficeComponent } from './masteroffice.component';

describe('MasterofficeComponent', () => {
  let component: MasterofficeComponent;
  let fixture: ComponentFixture<MasterofficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterofficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterofficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
