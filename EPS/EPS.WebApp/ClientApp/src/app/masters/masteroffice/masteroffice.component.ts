import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

export interface PeriodicElement {
  Offices: string;
  state: string;



  position: number;
  symbol: string;
  address: string;
 
 
}
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, state: 'Office of the Principal Accountant General (A&E) Karnataka', Offices: 'Bangalore', address: 'Park House Road, Near KPSC Office, Bangalore - 560001 ', symbol: 'H' },
  { position: 1, state: 'Office of the Principal Accountant General (A&E) Karnataka', Offices: 'Bangalore', address: 'Park House Road, Near KPSC Office, Bangalore - 560001 ', symbol: 'H' },
  { position: 1, state: 'Office of the Principal Accountant General (A&E) Karnataka', Offices: 'Bangalore', address: 'Park House Road, Near KPSC Office, Bangalore - 560001 ', symbol: 'H' },
  { position: 1, state: 'Office of the Principal Accountant General (A&E) Karnataka', Offices: 'Bangalore', address: 'Park House Road, Near KPSC Office, Bangalore - 560001 ', symbol: 'H' },
  { position: 1, state: 'Office of the Principal Accountant General (A&E) Karnataka', Offices: 'Bangalore', address: 'Park House Road, Near KPSC Office, Bangalore - 560001 ', symbol: 'H' },
 
];

@Component({
  selector: 'app-masteroffice',
  templateUrl: './masteroffice.component.html',
  styleUrls: ['./masteroffice.component.css']
})
export class MasterofficeComponent implements OnInit {
  is_btnStatus = true;

  displayedColumns: string[] = ['position', 'state', 'Offices', 'address', 'symbol'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  btnclose() {
    this.is_btnStatus = false;
  }
}
