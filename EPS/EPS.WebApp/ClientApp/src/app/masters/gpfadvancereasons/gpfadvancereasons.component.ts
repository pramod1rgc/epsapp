import { Component, OnInit, ViewChild } from '@angular/core';
import { Gpfadvancereasonsmodel } from '../../model/masters/gpfadvancereasonsmodel';
import { HttpClient } from '@angular/common/http';
import { GpfadvancereasonsserviceService } from '../../services/Masters/gpfadvancereasonsservice.service'
import { MatPaginator, MatTableDataSource, MatSort, MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-gpfadvancereasons',
  templateUrl: './gpfadvancereasons.component.html',
  styleUrls: ['./gpfadvancereasons.component.css']
})
export class GpfadvancereasonsComponent implements OnInit {

  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  divbox: string;
  isClicked: boolean = false;
  isTableHasData = true;
  constructor(private http: HttpClient, private gpfservice: GpfadvancereasonsserviceService) {

  }
  objgpf: Gpfadvancereasonsmodel;

  @ViewChild('gpfviewchild') gpfmaster: any;
  mainReasons: any;
  pfTypeList: any;
  result: any;
  Message: any;
  btnUpdatetext: any;
  savebuttonstatus: boolean;
  ArrGPFAdvanceReasonsDetails: any;
  dataSource: any;
  setDeletIDOnPopup: any;
  deletepopup: boolean;
  displayedColumns: string[] = ['pfType', 'reasonDescription', 'mainReasonID', 'outstandingLimitForPreviousAdvance', 'sealingForAdvancePay', 'sealingForAdvanceBalance', 'pFRuleReferenceNumber', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('panel1') firstPanel: MatExpansionPanel;
  ngOnInit() {
    this.objgpf = new Gpfadvancereasonsmodel();
    this.BindMainReasons();
    // this.BindPfType();
    this.btnUpdatetext = 'Save';
    this.savebuttonstatus = true;
    this.GetGPFInterestRateDetails();
    this.divbox = "box_dn";

  }
  BindMainReasons() {
    this.gpfservice.BindMainReasons().subscribe(result => {
      this.mainReasons = result;
    })
  }

  test(dstatus) {
    if (dstatus == true) {
      this.divbox = "box_db";
    }
    else {
      this.divbox = "box_dn";
    }
  }

  btnclose() {
    this.is_btnStatus = false;
  }
  //BindPfType() {
  //  this.gpfservice.BindPfType().subscribe(result => {
  //    this.pfTypeList = result;
  //  })
  //}
  Edit(pfRefNo, pfType, mainReasonID, reasonDescription, outstandingLimitForPreviousAdvance, monthsBetweenAdvances, sealingForAdvanceBalance, sealingForAdvancePay, convertWithdrawals, noOfAdvances, pFRuleReferenceNumber) {
    debugger;

    this.firstPanel.open();
    this.objgpf.pfRefNo = pfRefNo;
    this.objgpf.pfType = pfType;
    this.objgpf.mainReasonID = mainReasonID;
    this.objgpf.reasonDescription = reasonDescription;
    this.objgpf.outstandingLimitForPreviousAdvance = outstandingLimitForPreviousAdvance;
    this.objgpf.monthsBetweenAdvances = monthsBetweenAdvances;
    this.objgpf.sealingForAdvanceBalance = sealingForAdvanceBalance;
    this.objgpf.sealingForAdvancePay = sealingForAdvancePay;
    this.objgpf.convertWithdrawals = convertWithdrawals;
    this.objgpf.noOfAdvances = noOfAdvances;
    this.objgpf.pFRuleReferenceNumber = pFRuleReferenceNumber;
    this.btnUpdatetext = 'Update';
    this.isClicked = true;
    this.bgcolor = "bgcolor";
    this.is_btnStatus = false;
  }

  resetForm() {
    this.objgpf.pfRefNo = null;
    this.gpfmaster.resetForm();
    this.btnUpdatetext = 'Save';
    this.isClicked = false;
   
    this.bgcolor = "";

  }

  InsertandUpdate() {
    this.objgpf.advWithdraw = 'AR';
    this.gpfservice.InsertandUpdate(this.objgpf).subscribe(result => {
      this.Message = result;

      if (result == "Record updated successfully.") {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";

        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }
      else if (result == "Failed to update record.") {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }

      else if (result == "Records saved successfully.") {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }
      else if (result == "Record Already Exist.") {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }
      else if (result == "Failed to save record.") {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }

      this.deletepopup = false;
      this.GetGPFInterestRateDetails();
      this.resetForm();
      this.isClicked = false;
    })
  }


  GetGPFInterestRateDetails() {
    this.gpfservice.GetGPFAdvanceReasonsDetailsInGride('AR').subscribe(data => {
      this.ArrGPFAdvanceReasonsDetails = data;
      this.dataSource = new MatTableDataSource(data);

      //filter in Data source    

      this.dataSource.filterPredicate = (data, filter) => {
        return data.pfTypeName.toLowerCase().includes(filter) ||
          data.reasonDescription.toLowerCase().includes(filter) ||
          data.mainReasonText.toLowerCase().includes(filter) ||
          data.outstandingLimitForPreviousAdvance.toLowerCase().includes(filter) ||
          data.sealingForAdvancePay.toLowerCase().includes(filter) ||
          data.sealingForAdvanceBalance.toLowerCase().includes(filter) ||
          data.pFRuleReferenceNumber.toLowerCase().includes(filter);
      }
      //END Of filter in Data source
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
    })
  }


  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  Delete(pfRefNo) {

    this.gpfservice.Delete(pfRefNo).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
        this.deletepopup = false;
      }
      this.resetForm();
      this.GetGPFInterestRateDetails();
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }

    if (this.dataSource.filteredData.length > 0) {
      this.isTableHasData = true;
    }
    else {
      this.isTableHasData = false;
    }


  }

  charaterOnlyNoSpace(event): boolean {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

}
