import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaomasterComponent } from './paomaster.component';

describe('PaomasterComponent', () => {
  let component: PaomasterComponent;
  let fixture: ComponentFixture<PaomasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaomasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaomasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
