
import { Component, OnInit, ViewChild } from '@angular/core';
import { PaoMasterService } from '../../services/masters/pao-master.service';
import { MsPaomodel } from '../../model/masters/pao-master';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';
import { commons } from '../../global/commons';
@Component({
  selector: 'app-paomaster',
  templateUrl: './paomaster.component.html',
  styleUrls: ['./paomaster.component.css']
})
export class PaomasterComponent implements OnInit {

  displayedColumns: string[] = ['controllerCode', 'pAOCode', 'pAOName', 'pAOAddress', 'pAODescription',
    'contactNo', 'faxNo', 'emailAddress', 'stateName', 'cityName', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  response: string;
  objMsPao: MsPaomodel = {} as any;
  submitted = false;
  MsPaoList: any = [];
  filterList: any = [];
  errorMsg: string;
  isValidDate: any;
  MsDeleteId: any;
  searchvalue: any;
  ArrddController: any = [];
  ArrddState: any = [];
  ArrddCity: any = [];
  ArrddPAO: any = [];
  @ViewChild('mspaofm') form: any;
  disbleflag = false;
  deletepopup: boolean;
  isSuccessStatus: boolean = false;
  responseMessage: string = '';
  isPAOPanel: boolean = false;
  isLoading: boolean = false;
  dmlMessage: string = 'Save';
  bgColor: string = '';
  btnCssClass: string = 'btn btn-success';

  constructor(private _service: PaoMasterService, private _msg: CommonMsg, private _common: commons) { }

  ngOnInit() {
    this.isLoading = true;
    this.bindMsPaoList();
    this.BindDropDownController();
    this.BindDropState();
  }

  bindMsPaoList() {
    this._service.GetMSPAODetails().subscribe(res => {
      this.MsPaoList = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    })
  }
  SetDeleteId(msid) {
    this.MsDeleteId = msid;
  }
  DeleteDetails(Mspaoid) {
    this.isLoading = true;
    this._service.DeleteMsPAO(Mspaoid).subscribe(res => {
      this.showMessage(res);
      this.deletepopup = false;
      this.bindMsPaoList();
    })
  }

  BindDropDownController() {
    this._service.GetAllController().subscribe(data => {
      this.ArrddController = data;
    });
  }
  btnSaveclick() {
    this.isLoading = true;
    this.objMsPao.CityId = (this.objMsPao.CityId == undefined || this.objMsPao.CityId == null) ? 0 : this.objMsPao.CityId;
    this._service.upadateMsPAO(this.objMsPao).subscribe(res => {
      this.showMessage(res);
      this.bindMsPaoList();
    }, err => {
      this.showMessage(err);
    })
  }

  showMessage(msg) {
    this.responseMessage = msg;
    this.isSuccessStatus = true;
    this.isLoading = false;
    this.bgColor = '';
    setTimeout(() => this.isSuccessStatus = false, this._msg.messageTimer);
  }
  BindDropPAO(value: string) {
    this.objMsPao.PAOCode = null;
    this.objMsPao.Paoid = null;
    this._service.GetPAOByControllerId(value).subscribe(data => {
      this.ArrddPAO = data;
    });
    this.bindMsPaoList();
  }
  BindDropState() {
    if (this.ArrddState.length == 0) {
      this._service.GetState().subscribe(data => {
        this.ArrddState = this._common.SortArrayAlphabetically(data, 'stateText', 'asc');
      });
    }
  }
  BindDropCity(value: string) {
    this.objMsPao.CityId = null;
    this.objMsPao.CityName = null;
      this._service.GetCity(value).subscribe(data => {
        this.ArrddCity = this._common.SortArrayAlphabetically(data, 'cityText', 'asc');
      });
  }

  PAOChange(PAOCode: string) {
    this.disbleflag = true;
    this.filterList = this.MsPaoList.filter(item => item.paoCode == PAOCode && item.controllerId == this.objMsPao.ControllerId)
    if (this.filterList.length > 0) {
      this.objMsPao.Paoid = this.filterList[0]['msPAOID'];
      this.objMsPao.PAOCode = this.filterList[0]['paoCode'];
      this.objMsPao.PAOName = this.filterList[0]['paoName'];
      this.objMsPao.PAOLang = this.filterList[0]['paoLang'];
      this.objMsPao.PAOAddress = this.filterList[0]['paoAddress'];
      this.objMsPao.PAODescription = this.filterList[0]['paoDescription'];
      this.objMsPao.PinCode = this.filterList[0]['pinCode'];
      this.objMsPao.ContactNo = this.filterList[0]['contactNo'];
      this.objMsPao.FaxNo = this.filterList[0]['faxNo'];
      this.objMsPao.EmailAddress = this.filterList[0]['emailAddress'];
      this.BindDropCity(this.filterList[0]['stateId']);
      this.objMsPao.StateId = this.filterList[0]['stateId'];
      this.objMsPao.CityId = this.filterList[0]['cityId'];
      this.objMsPao.StateName = this.filterList[0]['stateName'];
      this.objMsPao.CityName = this.filterList[0]['cityName'];
      this.objMsPao.ControllerId = this.filterList[0]['controllerId'];
      this.objMsPao.ControllerCode = this.filterList[0]['controllerCode'];

      this.disbleflag = false;
    }
    else {
      this.errorMsg = null;
    }
  }
  btnEditClick(PAOCode: string, controllerId: string, ) {
    this.btnCssClass = 'btn btn-info';
    this.disbleflag = true;
    this.dmlMessage = 'Update';
    this.bgColor = 'bgcolor';
    this.filterList = this.MsPaoList.filter(item => item.paoCode == PAOCode && item.controllerId == controllerId)
    this.isPAOPanel = true;
    if (this.filterList.length > 0) {
      this.BindDropPAO(controllerId);
      this.objMsPao.Paoid = this.filterList[0]['msPAOID'];
      this.objMsPao.PAOCode = this.filterList[0]['paoCode'];
      this.objMsPao.PAOName = this.filterList[0]['paoName'];
      this.objMsPao.PAOLang = this.filterList[0]['paoLang'];
      this.objMsPao.PAOAddress = this.filterList[0]['paoAddress'];
      this.objMsPao.PAODescription = this.filterList[0]['paoDescription'];
      this.objMsPao.PinCode = this.filterList[0]['pinCode'];
      this.objMsPao.ContactNo = this.filterList[0]['contactNo'];
      this.objMsPao.FaxNo = this.filterList[0]['faxNo'];
      this.objMsPao.EmailAddress = this.filterList[0]['emailAddress'];
      this.BindDropCity(this.filterList[0]['stateId']);
      this.objMsPao.StateId = this.filterList[0]['stateId'];
      this.objMsPao.CityId = this.filterList[0]['cityId'];
      this.objMsPao.StateName = this.filterList[0]['stateName'];
      this.objMsPao.CityName = this.filterList[0]['cityName'];
      this.objMsPao.ControllerId = this.filterList[0]['controllerId'];
      this.objMsPao.ControllerCode = this.filterList[0]['controllerCode'];
      this.disbleflag = false;
    }
    else {
      this.errorMsg = null;
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  resetForm() {
    this.form.resetForm();
    this.errorMsg = null;
    this.disbleflag = false;
    this.searchvalue = null;
    this.bindMsPaoList();
    this.BindDropDownController();
    this.BindDropState();
    this.isSuccessStatus = false;
    this.responseMessage = '';
    this.dmlMessage = 'Save';
    this.bgColor = '';
    this.btnCssClass = 'btn btn-success';
  }
}
