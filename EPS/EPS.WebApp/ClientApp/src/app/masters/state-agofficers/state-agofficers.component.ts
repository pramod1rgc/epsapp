import { Component, OnInit, ViewChild} from '@angular/core';
import { PfType, State, StateAGOfficers } from '../../model/masters/state-agofficers';
import { StateAgService } from '../../services/Masters/state-ag.service';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-state-agofficers',
  templateUrl: './state-agofficers.component.html',
  styleUrls: ['./state-agofficers.component.css']
})
export class StateAGOfficersComponent implements OnInit {
  displayedColumns: string[] = ['Sr.No.','PF Type', 'State', 'State AG Description', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  response: string;
  objPfType: PfType
  objState: State
  objStateAg: StateAGOfficers = {} as any;
  submitted = false;
  PfTypeList: any = [];
  StateList: any = [];
  StateAgList: any = [];
  filterList: any = [];
  errorMsg: string;


  btnUpdatetext: any;
  isSuccessStatus: boolean = false;
  responseMessage: string = '';
  isPanel: boolean = false;
  isLoading: boolean = false;
  bgColor: string = '';
  btnCssClass: string = 'btn btn-success';

  //error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  @ViewChild('StateAg') StateAgForm: any;
  disbleflag = false;
  constructor(private _service: StateAgService, private _msg: CommonMsg) { }
  ngOnInit() {
    this.btnUpdatetext = 'Save';
    this.isLoading = true;
    this.BindPftype();
    this.BindState();
    this.bindStateAgList();
  }

  showMessage(msg) {
    this.responseMessage = msg;
    this.isSuccessStatus = true;
    this.isLoading = false;
    this.bgColor = '';
    setTimeout(() => this.isSuccessStatus = false, this._msg.messageTimer);
  }

  bindStateAgList() {
    this._service.getMsStateAglist().subscribe(res => {
      this.StateAgList = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  BindPftype() {
    this._service.getMsPfTypeList().subscribe(res => {
      this.objPfType = res;
      this.PfTypeList = this.objPfType;
      
  })
  }
  BindState() {
    this._service.getMsStateList().subscribe(res => {
      this.objState = res;
      this.StateList = this.objState;

    })
  }
  btnSaveClick() {
    //alert(this.objStateAg.PfType
    //)
    //alert(this.objStateAg.StateAgDesc)
    //alert(this.objStateAg.StateCode)
    //alert(this.objStateAg.PfTypeDesc)
  }
  EditStateAgMaster(msStateAgId: number) {
  }
  SetDeleteId(msStateAgId: number) {
  }
}
