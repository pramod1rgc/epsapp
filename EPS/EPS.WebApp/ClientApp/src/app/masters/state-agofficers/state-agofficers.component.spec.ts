import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateAGOfficersComponent } from './state-agofficers.component';

describe('StateAGOfficersComponent', () => {
  let component: StateAGOfficersComponent;
  let fixture: ComponentFixture<StateAGOfficersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateAGOfficersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateAGOfficersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
