
export interface IDeductionDetails {
  SlabNo: string;
  LowerLimit: string;
  UpperLimit: string;
  Value: string;
  MinAmount: string;
}
