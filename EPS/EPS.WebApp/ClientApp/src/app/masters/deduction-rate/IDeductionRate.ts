import { IDeductionDetails } from './IDeductionRateDetails';

export interface IDeductionRate {
  id: number;
  fullName: string;
  email: string;
  phone?: number;
  contactPreference: string;
  RateDetails: IDeductionDetails[];
}
