import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PsuMaster } from '../../model/masters/psu-master';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PsuMasterService } from '../../services/Masters/psu-master.service';

@Component({
  selector: 'app-psu',
  templateUrl: './psu.component.html',
  styleUrls: ['./psu.component.css']
})

export class PsuComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  is_btnStatus = false;
  objPsuModel: PsuMaster;
  stateList: any;
  distList: any;
  allCities: any;
  PsuForm: FormGroup;
  isBankDetailsAvail: boolean;
  successMsg: string;
  deletepopup: boolean;
  psuId: number;
  dvClassColor: string;
  strongMsg: string;
  msg: string;
  @ViewChild('psuForm') psuForm: any;
  dataSourcePsuDetails: MatTableDataSource<PsuMaster>;
  psuColumns: string[] = ['PsuCode', 'Desc', 'DescBilingual', 'Address', 'PhoneNo', 'State', 'City', 'Action'];

  ngOnInit() {
    this.objPsuModel = new PsuMaster();
    this.isBankDetailsAvail = false;
    console.log(sessionStorage.getItem('token'));

  }

  constructor(
    private objPsuService: PsuMasterService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.BindStateDDL();
    this.BindCitiesDDL(0);
    this.BindPsuMaster();
    this.CreatePsuMasterForm();

  }

  CreatePsuMasterForm() {
    this.PsuForm = this.fb.group({
      PsuCode: ['', Validators.required],
      PDesc: ['', Validators.required],
      PDescBilingual: ['', Validators.required],
      PAddress: ['', Validators.required],
      PPinCode: ['', Validators.required],
      PPhoneNo: ['', Validators.required],
      PFaxNo: ['', Validators.required],
      PEmail: ['', Validators.required],
      PStateName: ['', Validators.required],
      PCity: ['', Validators.required],
      PIsBankDetails: ['', Validators.required],
      PIfscCode: ['', ''],
      PBankName: ['', ''],
      PBranchName: ['', ''],
      PBankAccNum: ['', ''],
      PConfirmAccNum: ['', ''],
      PChequeInFavour: ['', Validators.required],
    },
    {
      validator: this.MustMatch('PBankAccNum', 'PConfirmAccNum')
    });
  }

  btnclose() {
    this.is_btnStatus = false;
  }

  BindStateDDL() {
    this.objPsuService.GetStateList().subscribe(res => {
      this.stateList = res;
    });
  }

  ddlState(stateId: number) {
    this.distList = this.allCities.filter(x => x.stateId === stateId);
  }

  BindCitiesDDL(stateId: number) {
    this.objPsuService
      .GetDistList(stateId)
      .subscribe(res => {
        this.allCities = res;
      });
  }
  radioChange(selected: string) {
    debugger;
    this.isBankDetailsAvail = selected === '1' ? true : false;
    const PIfscCode = this.PsuForm.get('PIfscCode');
    const PBankName = this.PsuForm.get('PBankName');
    const PBranchName = this.PsuForm.get('PBranchName');
    const PBankAccNum = this.PsuForm.get('PBankAccNum');
    const PConfirmAccNum = this.PsuForm.get('PConfirmAccNum');
    const PChequeInFavour = this.PsuForm.get('PChequeInFavour');

    if (this.isBankDetailsAvail) {
      PIfscCode.setValidators(Validators.required);
      PBankName.setValidators([Validators.required, Validators.pattern('[a-zA-Z ]*')]);
      PBranchName.setValidators(Validators.required);
      PBankAccNum.setValidators([Validators.required, Validators.minLength(9), Validators.maxLength(17)]);
      PConfirmAccNum.setValidators([Validators.required, Validators.minLength(9), Validators.maxLength(17)]);
      PChequeInFavour.setValidators(null);
    } else {
      PIfscCode.setValidators(null);
      PBankName.setValidators(null);
      PBranchName.setValidators(null);
      PBankAccNum.setValidators(null);
      PConfirmAccNum.setValidators(null);
      PChequeInFavour.setValidators(Validators.required);
    }
    PIfscCode.setValue(null);
    PBankName.setValue(null);
    PBranchName.setValue(null);
    PBankAccNum.setValue(null);
    PConfirmAccNum.setValue(null);
    PChequeInFavour.setValue(null);

    PIfscCode.updateValueAndValidity();
    PBankName.updateValueAndValidity();
    PBranchName.updateValueAndValidity();
    PBankAccNum.updateValueAndValidity();
    PConfirmAccNum.updateValueAndValidity();
    PChequeInFavour.updateValueAndValidity();

  }
  SavePSUMasterDetails(objPsu: PsuMaster): void {
    debugger
    // if (objPsu.PsuId == null) { objPsu.PsuId = 0; }
    //objPsu.IPAddress = null;
    // switch (objPsu.Status) {
    //   case 'F':
    //     this.successMsg = 'Suspension Details Forwarded Successfully';
    //     break;
    //   case 'R':
    //     this.successMsg = 'Suspension Details Returned Successfully';
    //     break;
    //   default:
    //     this.successMsg = 'Suspension Details saved successfully';
    //     break;
    // }
    this.is_btnStatus = true;
    this.objPsuService.SavedPsuMasterDetails(objPsu).subscribe((arg: any) => {
      this.dvClassColor = 'alert alert-info alert-dismissible';
      this.successMsg = 'PSU Details saved successfully';
      if (+arg > 0) {
        this.dvClassColor = 'alert alert-success alert-dismissible';
        this.strongMsg = 'Success!';
        this.msg = 'PSU Details saved successfully';
        debugger;
        this.psuForm.resetForm();
        this.BindPsuMaster();
      } else {
        this.dvClassColor = 'alert alert-warning alert-dismissible';
        this.strongMsg = 'Warning!';
        this.msg = arg;
      }
     // this.snackBar.open(this.successMsg, null, { duration: 4000 });

    });
    console.log(objPsu);

  }

  BindPsuMaster(): void {
    this.objPsuService.GetPsuMasterDetails().subscribe((result: any) => {
      console.log(result);
      this.dataSourcePsuDetails = new MatTableDataSource<PsuMaster>(result);
      this.dataSourcePsuDetails.paginator = this.paginator;
      // this.dataSourcePsuDetails.sort = this.t1Sort;

      // this.exPanel.disabled = false;
      // if (this.dataSource.data.length > 0) {
      //   this.exPanel.close();
      //   this.exPanel.disabled = true;
      // }

      // this.dataSourceHistory = new MatTableDataSource<SuspensionDetails>(result.filter(obj => obj.status === 'Verified'));
      // this.dataSourceHistory.paginator = this.paginatorHistory;
      // this.dataSourceHistory.sort = this.t2Sort;

    });

  }

  btnEditClick(objpsu: any, isViewOnly: boolean) {
    debugger;
    console.log(objpsu);
    this.ddlState(Number(objpsu.pStateName));
    this.isBankDetailsAvail = objpsu.pIsBankDetails === 1 ? true : false;
    this.objPsuModel.PsuId = objpsu.psuId;
    this.objPsuModel.PsuCode = objpsu.psuCode;
    this.objPsuModel.PDesc = objpsu.pDesc;
    this.objPsuModel.PDescBilingual = objpsu.pDescBilingual;
    this.objPsuModel.PAddress = objpsu.pAddress;
    this.objPsuModel.PPinCode = objpsu.pPinCode;
    this.objPsuModel.PPhoneNo = objpsu.pPhoneNo;
    this.objPsuModel.PFaxNo = objpsu.pFaxNo;
    this.objPsuModel.PEmail = objpsu.pEmail;
    this.objPsuModel.PStateName = objpsu.pStateName;
    this.objPsuModel.PCity = objpsu.pCity;
    this.objPsuModel.PIsBankDetails = objpsu.pIsBankDetails.toString();
    this.objPsuModel.PChequeInFavour = objpsu.pChequeInFavour;
    this.objPsuModel.PIfscCode = objpsu.pIfscCode;
    this.objPsuModel.PBankName = objpsu.pBankName;
    this.objPsuModel.PBranchName = objpsu.pBranchName;
    this.objPsuModel.PBankAccNum = objpsu.pBankAccNum;
    this.objPsuModel.PConfirmAccNum = objpsu.pConfirmAccNum;

    // this.exPanel.disabled = false;
    // this.exPanel.open();

    // this.objempSusDetails.Id = objsus.id;
    // this.objempSusDetails.FromDate = objsus.fromDate;
    // this.objempSusDetails.OrderDate = objsus.orderDate;
    // this.objempSusDetails.OrderNo = objsus.orderNo;
    // this.objempSusDetails.ToDate = objsus.toDate;
    // this.objempSusDetails.SalaryPercent = objsus.salaryPercent;
    // this.objempSusDetails.Remarks = objsus.remarks;
    // this.objempSusDetails.SusPeriod = objsus.susPeriod;
    // this._btnText = 'Save';
    // this._Id = objsus.id;
    // this.isRejected = false;
    // if (objsus.rejectedReason !== '') {
    //   this.objempSusDetails.RejectedReason = objsus.rejectedReason;
    //   this.isRejected = true;
    // }
    // this.SuspensionDetailsForm.enable();
    // // const toDate = this.SuspensionDetailsForm.get('ToDate');
    // // if (objsus.isTillOrder) {
    // //   this.objempSusDetails.ToDate = null;
    // //   toDate.disable();
    // //   toDate.setValidators(null);
    // // } else {
    // //   toDate.enable();
    // //   toDate.setValidators(Validators.required);
    // // }
    // // toDate.updateValueAndValidity();
    // const fdate = new Date(objsus.fromDate);
    // fdate.setDate(fdate.getDate() + +objsus.susPeriod);
    // this.validUptoDate = fdate.toDateString();
    // this.isViewDetails = true;
    // if (this.isCheckerLogin) {
    //   this._btnText = 'Accept';
    //   this.SuspensionDetailsForm.disable();
    // }

    // if (isViewOnly) {
    //   this.SuspensionDetailsForm.disable();
    //   this.isViewDetails = false;
    // }
  }

  DeleteJoiningDetails(id: number, flag: boolean) {
    debugger;

    this.psuId = flag ? id : 0;
    if (!flag) {
      this.psuForm.resetForm();
      this.objPsuService.DeletePsuMasterDetails(id).subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindPsuMaster();
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }
    // $('.dialog__close-btn').click();
    this.deletepopup = flag;
  }
 // custom validator to check that two fields match
 MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}

}

