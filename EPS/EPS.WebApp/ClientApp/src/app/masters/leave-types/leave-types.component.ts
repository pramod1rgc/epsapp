import { Component, OnInit, ViewChild } from '@angular/core';
import { LeaveTypeService } from '../../services/masters/leave-type.service';
import { LeaveTypeModel } from '../../model/masters/leave-type';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import {FormGroup } from '@angular/forms';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';


@Component({
  selector: 'app-leave-types',
  templateUrl: './leave-types.component.html',
  styleUrls: ['./leave-types.component.css']

})

export class LeaveTypesComponent implements OnInit {

  displayedColumns: string[] = ['leaveTypeCd', 'leaveTypeDesc', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  leaveTypeForm : FormGroup;
  dataSource: any;
  msleavetypeid: number;
  msleavetypecd: any;
  msleavetypedesc: string;
  response: string;
  objLeaveType: LeaveTypeModel
  submitted = false;
  LeaveTypeList: any = [];
  filterList: any = [];
  errorMsg: string;
  isValidDate: any;
  MsDeleteId: any;
  searchvalue: any;
  is_btnStatus = true;
  @ViewChild('Leavetype') LeavetypeForm: any;
  disbleflag = false;
  deletepopup: boolean;
  isSuccessStatus: boolean = false;
  responseMessage: string = '';
  isLeavePanel: boolean = false;
  bgColor: string = '';
  isLoading: boolean = false;

  constructor(private _service: LeaveTypeService, private _msg: CommonMsg)
  {
  }
 
  ngOnInit() {
   
    this.objLeaveType = new LeaveTypeModel();
    this.bindLeaveTypeList();
    this.bindLeaveTypeMaxid();
  }
  
  btnSaveClick() {
    this.isLoading = true;
      if (document.getElementById('btn_save').innerHTML == 'Save') {
        this._service.InsertMstLeavetype(this.objLeaveType).subscribe(res => {
          this.showMessage(res);
          this.bindLeaveTypeList();
          this.resetForm();
        })
      }
      else {
        this._service.upadateMstLeavetype(this.objLeaveType).subscribe(res => {
          this.showMessage(res);
          this.resetForm();
          this.bindLeaveTypeList();
        })
    }
 }

  showMessage(msg) {
    this.isSuccessStatus = true;
    this.responseMessage = msg;
    this.isLoading = false;
    setTimeout(() => this.isSuccessStatus = false, this._msg.messageTimer);
  }
  
  resetForm() {
    this.LeavetypeForm.resetForm();
     this.bgColor = '';
    this.disbleflag = false;
    this.searchvalue = null;
    this.objLeaveType = new LeaveTypeModel();
    document.getElementById('btn_save').innerHTML = 'Save';
    this.errorMsg = null;
       }
  btnEditClick(leavetypeid, leavetype, leavetypedesc, wef, wet) {
    this.bgColor = 'bgcolor';
    this.errorMsg = null;
    this.isLeavePanel = true;
    this.objLeaveType = new LeaveTypeModel();
    this.objLeaveType.LeaveTypeID = leavetypeid;
    this.objLeaveType.LeaveTypeCd = leavetype;
    this.objLeaveType.LeaveTypeDesc = leavetypedesc;
    this.objLeaveType.WEF = wef;
    this.objLeaveType.WET = wet;
    document.getElementById('btn_save').innerHTML = 'Update';
  }
  bindLeaveTypeList() {
    this._service.getLeaveTypeDetails().subscribe(res => {
      this.LeaveTypeList = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  Enablebtnsave() {
    this.disbleflag = true;
  }
  checkAllreadyexist() {
    this.disbleflag = true;
    debugger
    this.filterList = this.LeaveTypeList.filter(item => item.leaveTypeCd.trim().toLowerCase()== this.objLeaveType.LeaveTypeCd.trim().toLowerCase() && item.leaveTypeDesc.trim().toLowerCase() == this.objLeaveType.LeaveTypeDesc.trim().toLowerCase())
    if (this.filterList.length > 0) {
      this.errorMsg = 'Record Already Exist !!!!!!!';
      this.disbleflag = false;
    }
    else {
      this.errorMsg = null;
    }
  }
  SetDeleteId(msid) {
    this.MsDeleteId = msid;
  }
  DeleteDetails(Msleavetypeid) {
    this._service.DeleteMstLeavetype(Msleavetypeid).subscribe(res => {
      this.showMessage(res);
      this.bindLeaveTypeList();
      this.resetForm();
      this.deletepopup = false;
    })
  }
  bindLeaveTypeMaxid() {
    this._service.getLeaveTypeMaxid().subscribe(res => {
      this.objLeaveType.LeaveTypeID = res;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function (data, filter: string): boolean {
      return data.leaveTypeCd.toLowerCase().includes(filter) || data.leaveTypeDesc.toLowerCase().includes(filter);
    }
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}



/** @title Input with a custom ErrorStateMatcher */


