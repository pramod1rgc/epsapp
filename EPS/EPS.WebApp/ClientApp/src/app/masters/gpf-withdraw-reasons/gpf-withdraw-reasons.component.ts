import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
//import { GpfWithdrawalRulesService } from '../../services/Masters/gpf-withdrawal-rules.service';
import { GpfadvancereasonsserviceService } from '../../services/Masters/gpfadvancereasonsservice.service';



@Component({
  selector: 'app-gpf-withdraw-reasons',
  templateUrl: './gpf-withdraw-reasons.component.html',
  styleUrls: ['./gpf-withdraw-reasons.component.css'],
  providers: [GpfadvancereasonsserviceService]
})
export class GpfWithdrawReasonsComponent implements OnInit {
  sortActive: string
  dataSource: any;
  btnText = 'Save';
  mainReasons: any = [];
  objgpf: any = {};
 // message: any;
  setDeletIDOnPopup: any;
  notFound = true;
  displayedColumns: string[] = ['pfType', 'mainReasonText', 'reasonDescription', 'sealingForAdvanceBalance', 'sealingForAdvancePay','minService','pFRuleReferenceNumber.', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('msGpfWithdrawReasons') form: any;
  deletePopUp: boolean;
  isLoading = false;
  message: any;
  divBgColor: any
  btnStatus: boolean;
  panelOpenState: boolean
  bgColor: string;
  isClicked = false;
  constructor(private withdrawalReasons: GpfadvancereasonsserviceService,
  
  ) { }

  ngOnInit() {
    this.getMainReasons();
    this.getGpfWithdrawalReasons();
  }

   
  getMainReasons() {

  //  debugger;
    this.withdrawalReasons.BindMainReasons().subscribe(res => {
       this.mainReasons = res;

    })

  }

  insertUpdateGpfWithdrawalReasons() {
  
    this.isLoading = true;
   // this.btnText = 'Processing';
    this.objgpf.advWithdraw = 'WR';
    this.withdrawalReasons.InsertandUpdate(this.objgpf).subscribe(res => {
      this.isLoading = false;
      this.message = res;
      this.btnStatus = true;
      this.divBgColor = "alert-success";    
      if (this.message === 'Record Already Exist.') {
        this.divBgColor = "alert-warning";
      }
      else {
        this.resetForm();
        this.getGpfWithdrawalReasons();
        this.bgColor = '';
        this.isClicked = false;
      }
      setTimeout(() => {
        this.btnStatus = false;
        this.message = '';
      }, 8000);
    })
  }
 
  getGpfWithdrawalReasons() {

    this.withdrawalReasons.GetGPFAdvanceReasonsDetailsInGride('WR').subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (res.length === 0) {
        this.panelOpenState = true;
      }
        
    })
  }

   
   
  applyFilter(filterValue: string) {
    
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  editGpfWithdrawalReasons(element) {
    debugger
    this.objgpf.pfRefNo = element.pfRefNo;
    this.objgpf.pfType = element.pfType;
    this.objgpf.mainReasonID = element.mainReasonID;
    this.objgpf.reasonDescription = element.reasonDescription;
    this.objgpf.sealingForAdvanceBalance = element.sealingForAdvanceBalance;
    this.objgpf.sealingForAdvancePay = element.sealingForAdvancePay;
    this.objgpf.convertWithdrawals = element.convertWithdrawals;
    this.objgpf.noOfAdvances = element.noOfAdvances;
    this.objgpf.pFRuleReferenceNumber = element.pFRuleReferenceNumber;
    this.objgpf.minService = element.minService;
    this.btnText = 'Update';
    if (element != undefined || element != null) {
      this.panelOpenState = true;
      this.bgColor = "bgcolor";
      this.isClicked = true;
    }

  }
   setDeleteId(pfRefNo) {

    this.setDeletIDOnPopup = pfRefNo;
  }

// closeDialog() {
//   this.deletepopup = false;
//}
  deleteGpfWithdrawalReasons(pfRefNo) {

    this.withdrawalReasons.Delete(pfRefNo).subscribe(result => {
      if (this.objgpf.pfRefNo == pfRefNo) {
        this.resetForm();
      }
    //  this.message = result;
      if (result != undefined) {
        this.deletePopUp = false;
        this.message = result;
        this.btnStatus = true;
        this.divBgColor = "alert-danger";
      }
      this.getGpfWithdrawalReasons();
      setTimeout(() => {
        this.btnStatus = false;
        this.message = '';
      }, 8000);
    })
  }
  resetForm() {
    this.form.resetForm();
    this.btnText = 'Save';
    this.objgpf = {};
    this.bgColor = '';
    this.isClicked = false;
  }

  charaterOnlyNoSpace(event): boolean {
  //  debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
     if (charCode !== 32) {
       if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
         (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
         return true
       }
       else {
         return false;
       }
      }
      else {
      return  false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

}
