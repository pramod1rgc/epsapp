import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfWithdrawReasonsComponent } from './gpf-withdraw-reasons.component';

describe('GpfWithdrawReasonsComponent', () => {
  let component: GpfWithdrawReasonsComponent;
  let fixture: ComponentFixture<GpfWithdrawReasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfWithdrawReasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfWithdrawReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
