import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, FormGroupDirective, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatTableModule, MatTable } from '@angular/material';

import { PraoMasterService } from '../../services/Masters/prao-master.service';
import { CommonMsg } from '../../global/common-msg';
import { PraoControllerModel } from '../../model/masters/prao-master-model';
import swal from 'sweetalert2';



export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string; 
  phone: string;
  email: string;
  state: string;
  city: string;
}

@Component({
  selector: 'app-prao',
  templateUrl: './prao.component.html',
  styleUrls: ['./prao.component.css'],
  providers: [CommonMsg]
})
export class PraoComponent implements OnInit {

  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  divbox: string;
  Message: string;
  isClicked: boolean = false;
  displayedColumns: string[] = ['position', 'controllerCode', 'controllerName', 'phoneNo', 'email', 'stateID', 'cityID', 'PerformAction'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('praoForm') praoForm: any;
  @ViewChild('msgGpfRules') msgGpfRules: any;

  public PraoModel: PraoControllerModel;

  username: string;
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
  ArrControllerCode: any;
  ArrControllerName: any;
  ArrState: any;
  ArrDist: any;
  dataSource: any;
  ArrProMasterDetails: any;
  deletepopup: boolean;
  CtrCode: any;
  btnText: any;
  Ctrlcodeid: any;
  CtrID: any;
  IsDisabled: boolean = false;




  constructor(private fb: FormBuilder, private _Service: PraoMasterService, private commonMsg: CommonMsg) {
    this.PraoModel = new PraoControllerModel();
  }
  ngOnInit() {
    this.btnText = "Save";
   
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));

    this.BindControllerCode();
    this.BindState();

    this.BindPraoMasterDetails();
  }

  SetDeleteId(CtrCode, controllerID) {
    this.CtrCode = CtrCode;
    this.CtrID = controllerID;
  }

  BindControllerCode() {   
    this._Service.BindControllerCode().subscribe(data => {
      if (data != null) {
        this.ArrControllerCode = data;
      }
    })
  }

  BindState() {   
    this._Service.BindState().subscribe(data => {
      if (data != null) {
        this.ArrState = data;
      }
    })
  }

  
  SaveOrUpdate(PraoModel, CtrlCode) {
    debugger;
    this.IsDisabled = false;
    this.bgcolor = "";
    this.PraoModel.Mode = 2;
    //this.PraoModel.ControllerCode = CtrlCode.controllerCode;
    //this.PraoModel.ControllerID = CtrlCode.controllerID;
    //this.PraoModel.ControllerName = CtrlCode.controllerName;
    this._Service.SaveOrUpdate(PraoModel).subscribe(data => {     
      if (data != "0") {
        if (this.btnText == "Update") {
          this.Message = this.commonMsg.updateMsg;
        }
        if (this.btnText == "Save") {
          this.Message = this.commonMsg.saveMsg;
        }      
        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";
        this.isClicked = false;
        this.praoForm.resetForm();
        this.BindPraoMasterDetails();
        this.resetForm();
      }
    });
    
  }

  test(dstatus) {
    if (dstatus == true) {
      this.divbox = "box_db";
    }
    else {
      this.divbox = "box_dn";
    }
  }

  BindPraoMasterDetails() {
    this._Service.BindPraoMasterDetails().subscribe(data => {
      this.ArrProMasterDetails = data;
      this.dataSource = new MatTableDataSource(this.ArrProMasterDetails);;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  BindCtrlName(event: any) {
    this.is_btnStatus = false;
    var data = event.source.triggerValue;
    this.Ctrlcodeid = data.split("-");
    this.BindCtrlDetails(this.Ctrlcodeid[1], this.Ctrlcodeid[0]);
    this.BindState();
    this._Service.BindCtrlName(event.value).subscribe(data => {
      if (data != null) {
        this.ArrControllerName = data;
      }
    })
  }

  BindCtrlDetails(CtrlCode, CtrlId) {   
    this._Service.ViewEditOrDeletePrao(CtrlCode, CtrlId, "1").subscribe(data => {
      this.PraoModel = data[0];
      this.BindDistrict(data[0]["stateID"]);    
    });   
  }

  BindDistrict(stateId) {   
    if (stateId != 0) {      
      this._Service.BindDistrict(stateId).subscribe(data => {
        if (data != null) {
          this.ArrDist = data;
        }
      })
    }
  }

  EditDetails(CtrlCode, controllerID, stateID, controllerName) {
    debugger;
    this.is_btnStatus = false;
    this.BindContrlName(CtrlCode);   
    this.IsDisabled = true;
    this.bgcolor = "bgcolor";
    this.btnText = "Update";
    this.ArrControllerName = { ControllerID: controllerID, ControllerCode: CtrlCode, ControllerName: controllerName }; 
    this.BindDistrict(stateID);
    this._Service.ViewEditOrDeletePrao(CtrlCode, controllerID, "1").subscribe(data => {
    this.PraoModel = data[0];
    });   
  }

  BindContrlName(CtrlCode) {
    debugger;
    this._Service.BindCtrlName(CtrlCode).subscribe(data => {
      if (data != null) {
        this.ArrControllerName = data;
      }
    })
  }

  DeleteDetails(CtrlCode, controllerID) {
    this._Service.ViewEditOrDeletePrao(CtrlCode, controllerID, "2").subscribe(data => {
      if (data != null) {
        this.BindPraoMasterDetails();
        this.deletepopup = false;
        var alreadyExists = null;
         this.Message = this.commonMsg.deleteMsg;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        this.isClicked = false;
        this.resetForm();
        alreadyExists = "true";
      }
    });
   
  }

  resetForm() {
    this.IsDisabled = false;
    this.bgcolor = "";
    this.btnText = "Save";
    this.praoForm.resetForm();
    this.ArrDist = null;
  }

  btnclose() {
    this.is_btnStatus = false;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


}
