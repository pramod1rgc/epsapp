import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesDuesMasterComponent } from './dues-dues-master.component';

describe('DuesDuesMasterComponent', () => {
  let component: DuesDuesMasterComponent;
  let fixture: ComponentFixture<DuesDuesMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesDuesMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesDuesMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
