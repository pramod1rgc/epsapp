import { Component, ViewChild, OnInit } from '@angular/core';
import { fdMasterModel } from '../../model/masters/fdMasterModel';
import { FdgpfMasterService } from '../../services/Masters/fdgpf-master.service';
import {  FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatExpansionPanel} from '@angular/material';


@Component({
  selector: 'app-fd-gpf-master',
  templateUrl: './fd-gpf-master.component.html',
  styleUrls: ['./fd-gpf-master.component.css']
})
export class FdGpfMasterComponent implements OnInit {

  username: any;
  _fdMasterModel: fdMasterModel;
  validatingForm: FormGroup;
  dataSource: any;
  btnUpdatetext: any;
  disbleflag = false;
  setDeletIdOnPopup: any;
  savebuttonstatus: boolean;
  isTableHasData = true;
  searchfield: any;
  deletepopup: any;
  is_btnStatus: boolean;
  divbgcolor: string;
  Message: any;
  bgcolor: string;
  btnCssClass = 'btn btn-success';
  constructor(private fdMasterService: FdgpfMasterService) {

  }

  displayedColumns: string[] = ['serialNo', 'relationShip', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fdForm') form: any;
  @ViewChild('panelfd') fdPanel: MatExpansionPanel;

  ngOnInit() {
    this._fdMasterModel = new fdMasterModel();
    this.btnUpdatetext = 'Save';
    this.username = sessionStorage.getItem('username');
    this.savebuttonstatus = true;
    this.getGpfMasterDetails();
  }

  createfdGpfMaster() {
      this._fdMasterModel.loginUser = this.username;
      this.fdMasterService.CreatefdGpfMaster(this._fdMasterModel).subscribe(result => {
        if (result != undefined) {
          this.deletepopup = false;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";
          this.Message = result;
        }
        this.getGpfMasterDetails()
        this.resetForm();
        this.btnUpdatetext = 'Save';
        this.searchfield = "";
        this.bgcolor = "";

        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);
      })

  }


  editGpfMaster(fdMasterID: any) {
    this.fdMasterService.EditFdGpfMasterDetails(fdMasterID).subscribe(result => {
      this._fdMasterModel = result[0];
      this.btnUpdatetext = 'Update';
      this.bgcolor = "bgcolor";
      this.btnCssClass = 'btn btn-info';
      this.fdPanel.open();
    })
  }


  getGpfMasterDetails() {
    this.fdMasterService.GetFdGpfMasterDetails().subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  ltrim(searchfield) {
    return searchfield.replace(/^\s+/g, '');
  }


  applyFilter(filterValue: string) {
    this.searchfield = this.ltrim(this.searchfield);
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) 
       this.isTableHasData = true;
     else
       this.isTableHasData = false;
    
  }

  setDeleteId(msPayScaleID) {
    this.setDeletIdOnPopup = msPayScaleID;
  }


  deleteGpfDetails(fdMasterID) {
    this.fdMasterService.DeleteFdGpfMaster(fdMasterID).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        this.Message = result;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";      }
      this.resetForm();
      this.getGpfMasterDetails();
      this.searchfield = "";
      this.bgcolor = "";

      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
    })
  }
  
  
  resetForm() {
    this.btnUpdatetext = 'Save';
    this.disbleflag = false;
    this.form.resetForm();
    this._fdMasterModel.fdMasterID = 0;
    this.bgcolor = "";
    this.btnCssClass = 'btn btn-success';
  }
 
  charaterOnly(event): boolean {
      const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8 || charCode == 45) {
        return true;
      }
      return false;
  }


  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32 && ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))) {
        return true;
      }
      return false;
    }
  }

}
