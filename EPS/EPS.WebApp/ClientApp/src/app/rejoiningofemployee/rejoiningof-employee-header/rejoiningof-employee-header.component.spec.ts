import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejoiningofEmployeeHeaderComponent } from './rejoiningof-employee-header.component';

describe('RejoiningofEmployeeHeaderComponent', () => {
  let component: RejoiningofEmployeeHeaderComponent;
  let fixture: ComponentFixture<RejoiningofEmployeeHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejoiningofEmployeeHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejoiningofEmployeeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
