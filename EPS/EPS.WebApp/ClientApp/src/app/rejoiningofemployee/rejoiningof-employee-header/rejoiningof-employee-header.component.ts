import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RejoiningOfEmployeeService } from '../../services/rejoiningofEmployee/rejoiningofEmployee.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, ReplaySubject } from 'rxjs';
import { RejoiningofemployeeComponent } from '../rejoiningofemployee/rejoiningofemployee.component';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-rejoiningof-employee-header',
  templateUrl: './rejoiningof-employee-header.component.html',
  styleUrls: ['./rejoiningof-employee-header.component.css']
})
export class RejoiningofEmployeeHeaderComponent implements OnInit {

  headersForm: FormGroup;
  employeeList: any[] = [];

  public searchEmployee: FormControl = new FormControl('');
  public searchEmployeePAN: FormControl = new FormControl('');
  private _onDestroy = new Subject<void>();
  public filteredEmp: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredPAN: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private _service: RejoiningOfEmployeeService, private _rejoiningComponent: RejoiningofemployeeComponent) { }


  createForm() {
    this.headersForm = new FormGroup({
      searchEmployeeType: new FormControl('name'),
      searchEmployee: new FormControl('', Validators.required)
    });
  }

  resetForm() {
    this.createForm();
    this._rejoiningComponent.dataSource = new MatTableDataSource();
    this._rejoiningComponent.totalCount = 0;
  }

  getServiceEndEmployees() {
    this._service.GetServiceEndEmployees('', '', '').subscribe(response => {
      this.employeeList = response;
      this.filteredEmp.next(this.employeeList);
      this.filteredPAN.next(this.employeeList);
    });
  }

  onSubmit() {
    var employee = this.employeeList.filter(a => a.msEmpId == this.headersForm.controls.searchEmployee.value);
    if (employee.length > 0) {
      this._rejoiningComponent.setEmployeeDetails(employee[0]);
    }
  }

  ngOnInit() {
    this.createForm();
    this.getServiceEndEmployees();

    this.searchEmployee.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });

    this.searchEmployeePAN.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmpPAN();
      });
  }

  private filterEmp() {
    if (!this.employeeList) {
      return;
    }
    // get the search keyword
    let search = this.searchEmployee.value;
    if (!search) {
      this.filteredEmp.next(this.employeeList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEmp.next(
      this.employeeList.filter(emp => emp.empName.toLowerCase().indexOf(search) > -1 || emp.empCd.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterEmpPAN() {
    if (!this.employeeList) {
      return;
    }
    // get the search keyword
    let search = this.searchEmployeePAN.value;
    if (!search) {
      this.filteredPAN.next(this.employeeList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredPAN.next(
      this.employeeList.filter(emp => emp.empPanNo ? emp.empPanNo.toLowerCase().indexOf(search) > -1 : false)
    );
  }

}
