import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RejoiningofemployeeModule } from '../rejoiningofemployee/rejoiningofemployee.module'
import { RejoiningofemployeeComponent } from './rejoiningofemployee/rejoiningofemployee.component';
const routes: Routes = [
  {
    path: '', component: RejoiningofemployeeModule, children:[
      {
        path: 'rejoining', component: RejoiningofemployeeComponent
      }

    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RejoiningofemployeeRoutingModule { }
