import { NgModule, InjectionToken } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, } from '@angular/router';
import { MaterialModule } from '../material.module';
import { RoleManagmentModule } from '../role-managment/role-managment.module';

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export class AppConfig {
  api_base_url: string;
  getAllRole: string;
  getAllActiveRollList: string;
  GetAllCustomeRollList: string;
  GetBindUserListddl: string;
  getSalutation: string;
  uploadFiles: string;
  getRelation: string;
  getEmployeeType: string;
  getJoiningMode: string;
  getJoiningCategory: string;
  getverifiedEmpCode: string; 
  GetPhysicalDisabilityTypes: string;
  GetMaritalStatus: string;
  getFamilyDetails: string;
  getAllFamilyDetails: string;
  GetEmpPersonalDetailsByID: string;
  SavePHDetails: string;
  UpdateEmpFamilyDetails: string;
  DeleteFamilyDetails: string;
  GetIsDeputEmp: string;

  getAllNomineeDetails: string;
  getNomineeDetails: string;
  UpdateNomineeDetails: string;
  DeleteNomineeDetails: string;
  getBankDetails: string;
  getBankDetailsByIFSC: string;
  getAllIFSC: string;
  UpdateBankDetails: string;
  GetHRACity: string;
  GetTACity: string;
  GetAllDesignation: string;
  SaveUpdatePostingDetails: string;
  GetAllPostingDetails: string;
  getMaintainByOfc: string;
  getServiceDetails: string;
  SaveUpdateServiceDetails: string;
  getOrganisationType: string;
  getNonComputationalDues: string;
  getNonComputationalDeductions: string;
  getPayLevel: string;
  getPayIndex: string;
  getEntitledOffVeh: string;
  getBasicDetails: string;
  getGradePay: string;
  getPayScale: string;
  SaveUpdatePayDetails: string;
  getPayDetails: string;

  getInsuranceApplicable: string;
  getCGEGISCategory: string
  SaveUpdateCGEGISDetails: string;
  getCGEGISDetails: string;
  SaveUpdateCGHSDetails: string;
  getCGHSDetails: string;
  getQuarterOwnedby: string;
  getAllottedTo: string;
  getRentStatus: string;
  getQuarterType: string;
  GetCustodian: string;
  GetGPRACityLocation: string;
  SaveUpdateQuarterDetails: string;
  QuarterAllDetails: string;
  QuarterDetails: string;
  DeleteQuarterDetails: string;
  getAllEmpDetails: string;
  getAllEmployeeCompleteDetails: string;
  ForwardToCheckerEmpDetails: string;
  verifyEmpData: string;

  saveNewRole: string;
  getUserDetails: string;
  revokeUser: string; 
  getUserDetailsByPan: string;
  saveAssignRoleForUser: string;
  getAllMenuList: string;
  saveMenuPerMission: string;
  checkMenuPerMission: string;
  getAssignRoleForUser: string;
  saveMenu: string;
  FillUpdateMenu: string;
  bindDropDownMenu: string;
  bindMenuInGride: string;
  activeDeactiveMenu: string;
  getEmployeeSubType: string;
  GetPredefineRole: string;
  /*Dashboard*/
  getAllMenusByUser: string;
  getDashboardDetal: string;
/*End Of DashBoard*/


/*Start DDO Master*/
  DDOMasterControllerName: string;

/*End Of DDO Master*/


/*Start PRAO Master*/
 
  GetControllerCode: string;
  BindCtrlName: string;
  BindState: string;
  BindDistrict: string;
  SaveOrUpdate: string;
  BindPraoMasterDetails: string;
  ViewEditOrDeletePrao: string;



/*End Of PRAO Master*/








  /*Start PayBillGroup*/

  getAccountHeads: string;
  InsertUpdatePayBillGroup: string;
  BindPayBillGroup: string;
  GetPayBillGroupDetailsById: string;
  GetPayBillGroupEmpAttach: string
  GetPayBillGroupEmpDeAttach: string
  UpdatePayBillGroupIdByEmpCode: string;
  updatePayBillGroupIdnullByEmpCode: string;
  getNgRecovery: string;
  insertUpdateNgRecovery: string;
  activeDeactiveNgRecovery: string;

  getfinancialYear: string;
  getAllMonth: string;
  GetEmployeeByPaybillAndDesig: string;
  //GetEmployeeAttach: string;



  /*Start PayBillGroup*/
  getMakerEmpList: string;
  suspensionControllerName: string;
  UpdateEmpDetails: string;
  getGender: string;
  InsertUpdateEmployeeDetails: string;
  GetEmpPHDetails: string;
  GetState: string;
  GetBankDetailsByIfscCode: string;
  InsertNgrecoveryEntry: string;
  GetNgRecoveryList: string;


  GetAllNgRecoveryandPaybillGrpDetails: string;
  //amit AccountHeadsOT1-paybill
  getAccountHeadsOT1: string;
  GetAccountHeadOT1AttachList: string;
  UpdateaccountHeadOT1: string;
  GetAccountHeadOT1PaybillGroupStatus: string;
  UpdateAccountHeadOT1PaybillGroupStatus: string;
  //Login

  LoginDetails: string;
  UserDetails: string;
  LoginNewUser: string;
  RoleActivation: string;
  currentrUrl: string;
  IsMenuPermissionAssigned: string;
  //-----------------User Management/Payroll-------------------

  getAllControllers: string;
  controllerchanged: string;
  PaosAssigned: string;
  GetAllUserRoles: string;
  GetAllDDO: string;
  EmployeeList: string;
  Assigned: string;
  //GetAllUserRoles: string;
  GetAllpaos: string;
  onpaoSelectchanged: string;
  AssignedPAO: string;
  login: string;
  GetAllUserRolesByDDO: string;
  EmployeeListOfDDO: string;
  AssignedCMD: string;
  AssignChecker: string;
  //AssingedDDOChecker: string;
  //AssingedDDOCheckerMaker: string;
  makerEmpList: string;
  AssignedMaker: string;
  HooCheckerEmpList: string;
  AssignedHOOChecker: string;
  hooMakerEmpList: string;
  AssignedHOOMaker: string;
  SelfAssignDDOChecker: string;
  SelfAssignCheck: string;
  SelfAssignedDDOMaker: string;
  SelfAssignMakerRole: string;
  SelfAssignHOOChecker: string;
  SelfAssignHOOCheckerRole: string;
  AssignedEmp: string;
  AssignedEmpList: string;
  AssignedEmpDDO: string;
  AssignedDDO: string;
  AssignedPAOEmp: string;
  AssignedDDOCheckerEmpList: string;
  AssignedMakerEmpList: string;
  OnBoardingSubmit: string;
  GetAllReuestNoOfOnboarding: string;
  FetchRequestLetterNoRecord: string;
  //==========Masters===========================
  psuMasterControllerName: string;
  //=========leavetype master======
  getMstLeaveTypeList: string;
  getMstLeavetypeMaxid: string;
  upadateMstLeavetype: string;
  insertLeavetype: string;
  deleteLeavetype: string;
  //=========Design master======
  getMsdesignlist: string;
  getMsdesignMaxid: string;
  updateMsdesign: string;
  insertMsdesign: string;
  deleteMsdesign: string;

  //=========State Ag Master============
  getMsStateAglist: string;
  getMsStateList: string;
  getMsPfTypeList: string;
  insertMsStateAg: string;
  upadateMsStateAg: string;


  //=========Pao master======
  getMSPAODetails: string;
  updateMSPAO: string;
  insertMSPAO: string;
  deleteMSPAO: string;

  //=========payscale master======
  BindCommissionCode: string;
  BindPayScaleFormat: string;
  BindPayScaleGroup: string;
  CreatePayScale: string;
  GetPayScaleDetails: string;
  EditPayScaleDetails: string;
  GetPayScaleCode: string;
  Delete: string;
  GetPayScaleDetailsBYComCode: string;
  //===========CityClassMaster================

  GeCityClassMasterList: string;
  CityMasterController: string;

  //============End City Class master ============
  //========== Dues Defination=======

  InsertUpdateDuesDefinationMaster: string;
  GetDuesDefinationmaster: string;
  GetDuesDefinationMasterDetailsByID: string;
  GetAutoGenratedDuesCode: string;

  GetOrgnaztionTypeForDuesRate: string;
  GetStateForDuesRate: string;
  GetPayCommForDuesRate: String;
  GetCityClassForDuesRate: string;
  GetDuesCodeDuesDefination: string;
  AddDuesDetails: string;
  GetSlabTypeByPayCommId: string;
  saveUpdateDuesRateDetails: string;
  GetDuesRateList: string;
  DeleteDuesRatemaster: string;

  //======= end ==========
  //=====================GPF Interest Rate=======================
  GpfInterestRateInsertandUpdate: string;
  GetGPFInterestRateDetails: string;
  DeleteGpfInterestRate: string;
  //==============Gpf Advance Reasons==========================================
  BindMainReason: string;
  GpfAdvancereasonsInsertandUpdate: string;
  GetPfType: string;
  GetGpfAdvancereasonsDetails: string;
  GpfAdvancereasonsDelete: string;

  //==============Gpf Advance Rules==========================================
  GpfAdvanceRules: string;
  GpfAdvanceRulesByID: string;
  InsertUpdateGpfAdvanceRules: string;
  DeleteGpfAdvanceRules: string;

  //==============Gpf Recovery Rules==========================================
  GpfRecoveryRules: string;
  GpfRecoveryRulesByID: string;
  InsertUpdateGpfRecoveryRules: string;
  DeleteGpfRecoveryRules: string;


  //==============Gpf Withdrawal Rules==========================================
  GpfWithdrawalRules: string;
  GpfWithdrawalRulesByID: string;
  InsertUpdateGpfWithdrawalRules: string;
  DeleteGpfWithdrawalRules: string;
  //=========Da master======
  //Delete : string;
  InsertandUpdate: string;
  GetDADetails: string;
  GetDACurrent_Rate_Wef: string;
  DeleteDaMaster: string;
  GetDACurrent_Rate_Wef_state: string;

  //end of Masters
  getDeputationType: string;
  getServiceType: string;

  //=========Hra Master======
  BindPayScaleCode: string;
  BindPayLevel: string;
  GetCityDetails: string;
  GetCityClass: string;
  CreateHraMaster: string;
  UpdateHraMaster: string;
  GetHraMasterDetails: string;
  EditHraMasterDetails: string;
  DeleteHraMaster: string;
  GetPayCommissionByEmployeeType: string;

  //=========Tpta Master======
  BindUnionTerritory: string;
  CreateTptaMaster: string;
  GetTptaMasterDetails: string;
  EditTptaMasterDetails: string;
  GetTptaMasterDetailsByID: string;
  DeleteTptaMaster: string;

  //====Family Definition GPF Master==========

  CreatefdGpfMaster: string;
  GetFdGpfMasterDetails: string;
  EditFdGpfMasterDetails: string;
  DeleteFdGpfMaster: string;

  //====Family Definition Pension Master==========

  //CreatefdPensionMaster: string;
  //GetFdPensionMasterDetails: string;
  //EditFdPensionMasterDetails: string;
  //DeleteFdPensionMaster: string;

  //====Commutation table Pension Master==========

  CreateCommPensionMaster: string;
  GetCommPensionMasterDetails: string;


  //-------------leave management-----------
  LeavesControllerName: string;
  //End leave management

  //------------existing loan-----
  FloodController: string;
  MultipleInstRecoveryController: string;

  getBindDropDownDesign: string;
  getBindDropDownBillGroup: string;
  getBindDropDownEmp: string;
  getLoanAdvanceDetails: string;

  GetLoanType: string;
  getAllEmpNameSelectedDesignId: string;
  ExistingLoanStatus: string;
  GetExistLoanDetailsByID: string;
  //-----------------------------------
  GetLoanpurposebyLoanCode: string;
  GetDetailsbyLoanCodePurposecode: string;
  GetAllEmpNameSelectedByDesignId: string;
  DeleteEmpDetails: string;

  //------------------New loan mgt----
  GetEmpDetailsbyEmpcode: string;
  GetSanctionDetailsbyEmpcode: string;
  GetRecoveryChallan: string;
  GetPayLoanPurposeStatus: string;
  PostLoanDetails: string;
  PostPropertyOwnerDetails: string;
  UpdateLoanDetails: string;
  getExistingLoanType: string;
  getBindHeadAccount: string;
  ExistingLoanInsertData: string;
  GetExistingEmpLoanDetails: string;
  GetLoanDetails: string;
  GetFwdtochkerDetails: string;
  GetEmpLoanDetailsByID: string;
  EditLoanDetailsByEMPID: string;
  UpdateLoanDetailsbyID: string;
  DeleteLoanDetailsByEMPID: string;
  GetEmpDetails: string;
  GetEmpfilterbydesignBillgroup: string;
  GetBillgroupfilterbydesignID: string;
  UpdateSanctionDetails: string;
  ExistingLoanForwordToDDO: string;
  saveUpdateRecoveryChallan: string;
  ValidateCoolingPeriod: string;
  //end

  // ----Shared variables name--
  SharedControllerName: string;
  // ---End


  // Recovery Excess Payment

  GetFinancialYears: string;
  GetRecoveryLoanType: string;
  GetRecoveryElement: string;
  GetAccountHead: string;
  GetRecoveryComponent: string;
  InsertUpdateExRecovery:string
  GetExcessRecoveryDetail: string;
  EditExcessRecoveryDetail: string;
  DeleteExcessRecoveryData: string;
  ForwardExRecoveryData: string;
  insertUpdateComponentAmt: string;
  getComponentAmtDetails: string;
  deleteComponentAmt: string;


  getTempPaymentRecovery: string;
  SaveTempPayment: string;
  SaveRecoveryExcess: string;


  //End Of Recovery Excess Payment


  // Deputation
  getPayItems: string;
  getDesignationAll: string;
  getDepuatationCommunticationAddress: string;
  getDeductionScheduleDetailsbyEmployee: string;
  saveUpdateDeputationInDetails: string;
  updateforwardStatusUpdateByDepID: string
  updateforwardStatusUpdateByEmpCode: string
  getScheme_CodeByMsEmpDuesID: string;
  getAllDepuatationOutDetailsByEmployee: string;
  saveUpdateDeputationOutDetails: string;
  getAllRepatriationDetailsByEmployee: string;
  saveUpdateRepatriationDetails: string;
  getCheckDuputationInEmployee: string;
  //End


  //Promotions
  PromotionControllerName: string;
  //End

  //ReJoiningService
  ReJoiningServiceControllerName: string;
  //End
  //LienPeriod
  getControllerList: string;
  getDdolistbyControllerid: string;
  getOfficeAddressDetails: string;
  saveUpdateTransferOffLienPeriod: string
  getTransferofficelienPeriodDetails: string
  lienPeriodUpdateForwardStatusUpdateByEmpCode: string;
  deleteTransferOffLienPeriod: string;
  getEpsEmpJoinOffAfterLienPeriodDetailByEmp: string;

  getJoiningAccountOf: string;
  getOfficeList: string;
  getOfficeCityClass: string;
  saveUpdateEpsEmployeeJoinAfterLienPeriod: string;

  deleteEpsEmployeeJoinAfterLienPeriodById: string;
  epsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: string;

  getPayCommissionListLien: string;
  getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp: string;
  saveUpdateNonEpsEmployeeJoinAfterLienPeriod: string;
  deleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode: string;
  nonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: string;
  //End


  // Increment

  // Advance Increment
  CreateAdvIncrement: string;
  GetIncrementDetails: string;
  EditAdvIncrement: string;
  DeleteAdvDetails: string;
  Forwardtochecker: string;
  GetEmployeeByDesigPayComm: string;

  //Regular Increment
  CreateOrderNo: string;
  GetOrderDetails: string;
  deleteOrderDetails: string;
  GetEmployeeForIncrement: string;
  GetEmployeeForNonIncrement: string;
  AddEmpForIncrement: string;
  ForwardtocheckerForInc: string;
  GetEmployeeDesignation: string;
  // Stop Increment
  GetEmployeeStopIncrement: string;
  GetEmployeeForNotStop: string;
  InsertStopIncrementData: string;
  ForwardcheckerForStopInc: string;

  // Annual Increment Report
  GetSalaryMonth: string;
  GetOrderWithEmployee: string;
  GetAnnualIncReportData: string;
  GetEmpDueForIncReport: string;
  //End

  //Change
  
  GetDdlvalue: string; //common
  ChangeDeleteDetails: string;
  

  EOSControllerName: string;//ExtensionOfService
  GetExtensionOfServiceDetails: string;
  //FetchEOSDetails: string;  
  ForwardToCheckerUserDtls: string;
     
  InsertDOBDetails: string;//dob
  GetDOBDetails: string;
  ForwardToCheckerDOBUserDtls: string;


  InsertNameGenderDetails: string; //name
  GetNameGenderDetails: string;
  ForwardToCheckerNameGenderUserDtls: string;

  InsertPfNpsFormDetails: string;//pfnps
  GetPfNpsFormDetails: string;
  ForwardToCheckerPfNpsUserDtls: string;

  InsertPanNoFormDetails: string; //pan
  GetPanNoFormDetails: string;
  //FetchPanNosrchDetails: string;
  ForwardToCheckerPanNoUserDtls: string;


  InsertHraccFormDetails: string; //hra
  GetHraccFormDetails: string;
  //FetchHraccsrchDetails: string;  
  ForwardToCheckerHraccUserDtls: string;

  InsertdesignationFormDetails: string; //designation
  GetdesignationFormDetails: string;
  ForwardToCheckerdesignationUserDtls: string;

  InsertexservicemenFormDetails: string; //ex-servicemen
  GetexservicemenFormDetails: string;
  ForwardToCheckerexservicemenUserDtls: string;

  InsertCGHSFormDetails: string; //CGHSForm
  GetCGHSFormDetails: string;
  ForwardToCheckerCGHSUserDtls: string;


  InsertCGEGISFormDetails: string; //CGEGIS
  GetCGEGISFormDetails: string;
  ForwardToCheckerCGEGISFormUserDtls: string;


  InsertJoiningModeFormDetails: string; //JoiningMode
  GetJoiningModeFormDetails: string;
  ForwardToCheckerJoiningModeFormUserDtls: string;

  InsertDOJDetails: string; //DOJ
  GetDOJDetails: string;
  ForwardToCheckerDOJUserDtls: string;

  InsertDOEDetails: string; //DOE
  GetDOEDetails: string;
  ForwardToCheckerDOEUserDtls: string;

  InsertDORDetails: string; //DOR
  GetDORDetails: string;
  ForwardToCheckerDORUserDtls: string;

  InsertCasteCategoryFormDetails: string; //CasteCategory
  GetCasteCategoryFormDetails: string;
  ForwardToCheckerCasteCategoryFormUserDtls: string;
  
  InsertEntofficevehicleFormDetails: string; //Entofficevehicle
  GetEntofficevehicleFormDetails: string;
  ForwardToCheckerEntofficevehicleFormUserDtls: string;

  InsertBankFormDetails: string; //BankForm
  GetBankFormDetails: string;
  ForwardToCheckerBankFormUserDtls: string;

  InsertaddtapdFormDetails: string; //addtapd
  GetaddtapdFormDetails: string;
  ForwardToCheckeraddtapdUserDtls: string;

  InsertTPTAFormDetails: string; //TPTA
  GetTPTAFormDetails: string;
  ForwardToCheckerTPTAUserDtls: string;

  InsertStateGISFormDetails: string; //StateGIS
  GetStateGISFormDetails: string;
  ForwardToCheckerStateGISUserDtls: string;

  InsertMobileNoEmailEmpCodeDetails: string; //MobileNo_Email_EmpCode
  GetMobileNoEmailEmpCodeDetails: string;
  ForwardToCheckerMobileNoEmailEmpCodeUserDtls: string;

  //IncomeTax
  IncomeTaxControllerName: string;

  //User Profile
  GetUserProfileDetails: string;
  //End Of User Profile
  // Start End of Service
  EndofServiceGetEmployee: string;
  EmpCurrentEndDeta: string;
  EndofServiceReason: string;
  EndofServiceGetDetailByEmp: string;
  EndofServiceInsert: string;
  EndofServiceUpdate: string;
  EndofServiceDelete: string;
   //End End of Service

  //Deduction Master
  categoryList: string;
  deductionDescription: string;
  deductionDescriptionChange: string;
  getAutoGenratedDeductionCode: string;
  getDeductionDefinitionList: string;
  deductionDefinitionSubmit: string;
      //===============Deduction Rate Master=======================
  GetOrgnaztionTypeForDeductionRate: string;
  GetStateForDeductionRate: string;
  GetPayCommForDeductionRate: String;
  GetCityClassForDeductionRate: string;
  GetDeductionCodeDeductionDefination: string;
  AddDeductionDetails: string;
  GetDeductionSlabTypeByPayCommId: string;
  saveUpdateDeductionRateDetails: string;
  GetDeductionRateList: string;
  DeleteDeductionRatemaster: string;

  //Deduction Master end
}

const getHostName = window.location.hostname;
let apiHost;
if (getHostName === 'localhost') {
  //apiHost = 'http://localhost:55424/api';
  apiHost = 'http://localhost:55424/api/';
} else {
  apiHost = 'http://10.199.72.53:82/api/';
}
export const APP_DI_CONFIG: AppConfig = {
  api_base_url: apiHost,
  //==============DashBoard=======
  getAllMenusByUser: apiHost + 'Dashboard/getAllMenusByUser',
  getDashboardDetal: 'Dashboard/GetDashboardDetails',
  //============End Of DashBoard======



  //=========Masters======================


  //=======  City Class ============

  GeCityClassMasterList: 'CityMaster/GetCitymasterList',
  // SaveCityClass:'CityMaster/SaveCityClass',

  //========= End City Class ============
  //============ Dues Master ===============

  InsertUpdateDuesDefinationMaster: 'DuesDefinationandDuesMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
  GetDuesDefinationmaster: 'DuesDefinationandDuesMaster/GetDuesdefinationMasterList',
  GetDuesDefinationMasterDetailsByID: 'DuesDefinationandDuesMaster/GetEmpPersonalDetailsByID?DuesCD=',

  GetAutoGenratedDuesCode: 'DuesDefinationandDuesMaster/GetAutoGenratedDuesCode',

  GetOrgnaztionTypeForDuesRate: 'DuesDefinationandDuesMaster/GetOrgnazationTypeForDues',
  GetStateForDuesRate: 'DuesDefinationandDuesMaster/GetStateForDuesRate',
  GetPayCommForDuesRate: 'DuesDefinationandDuesMaster/GetPayCommForDuesRate',
  GetCityClassForDuesRate: 'DuesDefinationandDuesMaster/GetCityClassForDuesRate?payCommId=',
  GetDuesCodeDuesDefination: 'DuesDefinationandDuesMaster/GetDuesCodeDuesDefination',

  AddDuesDetails: 'DuesDefinationandDuesMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
  GetSlabTypeByPayCommId: 'DuesDefinationandDuesMaster/GetSlabtypeByPayCommId?payCommId=',
  saveUpdateDuesRateDetails: 'DuesDefinationandDuesMaster/SaveUpdateDuesRateDetails',
  GetDuesRateList: 'DuesDefinationandDuesMaster/GetDuesCodeDuesRate',
  DeleteDuesRatemaster: apiHost + 'DuesDefinationandDuesMaster/DeleteDuesRateDetails?msduesratedetailsid=',
  //=============== end
  //=========leavetype master======
  getMstLeaveTypeList: apiHost + 'Leavetype/GetLeavetypelist',
  getMstLeavetypeMaxid: apiHost + 'Leavetype/GetLeavetypeMaxid',
  upadateMstLeavetype: apiHost + 'Leavetype/upadateLeavetype',
  insertLeavetype: apiHost + 'Leavetype/insertLeavetype',
  deleteLeavetype: apiHost + 'Leavetype/DeleteLeavetype',
  //=========Design master======
  getMsdesignlist: apiHost + 'DesignMaster/GetMsdesignlist',
  getMsdesignMaxid: apiHost + 'DesignMaster/GetMsdesignMaxid',
  updateMsdesign: apiHost + 'DesignMaster/UpdateMsdesign',
  insertMsdesign: apiHost + 'DesignMaster/InsertMsdesign',
  deleteMsdesign: apiHost + 'DesignMaster/DeleteMsdesign',
  //=========Ms State Ag master======
  getMsPfTypeList: apiHost + 'MsStateAg/GetMsPfTypeList',
  getMsStateList: apiHost + 'MsStateAg/GetMsStateList',
  getMsStateAglist: apiHost + 'MsStateAg/GetMsStateAglist',
  insertMsStateAg: apiHost + 'MsStateAg/InsertMsStateAg',
  upadateMsStateAg: apiHost + 'MsStateAg/UpadateMsStateAg',

  //=========Ms PAO Master======
  getMSPAODetails: apiHost + 'PAOMaster/GetMSPAODetails',
  updateMSPAO: apiHost + 'PAOMaster/UpdateMSPAO',
  insertMSPAO: apiHost + 'PAOMaster/InsertMSPAO',
  deleteMSPAO: apiHost + 'PAOMaster/DeleteMSPAO',
 

  //============payscale==========
  BindCommissionCode: apiHost + 'payscale/BindCommissionCode',
  BindPayScaleFormat: apiHost + 'payscale/BindPayScaleFormat',
  BindPayScaleGroup: apiHost + 'payscale/BindPayScaleGroup',
  CreatePayScale: apiHost + 'payscale/CreatePayScale',

  GetPayScaleDetails: apiHost + 'payscale/GetPayScaleDetails',
  EditPayScaleDetails: apiHost + 'payscale/EditPayScaleDetails?msPayScaleID=',
  GetPayScaleDetailsBYComCode: apiHost + 'payscale/GetPayScaleDetailsBYComCode?CommCDID=',
  GetPayScaleCode: apiHost + 'payscale/GetPayScaleCode?CddirCodeValue=',
  Delete: apiHost + 'payscale/Delete?msPayScaleID=',


  //==================================================
  InsertandUpdate: apiHost + 'DaStateCenteral/InsertandUpdate',
  GetDADetails: apiHost + 'DaStateCenteral/GetDADetails',
  GetDACurrent_Rate_Wef: apiHost + 'DaStateCenteral/GetDACurrent_Rate_Wef?DaType=',
  DeleteDaMaster: apiHost +'DaStateCenteral/Delete?msDaMasterID=',
  GetDACurrent_Rate_Wef_state: apiHost +'DaStateCenteral/GetDACurrent_Rate_Wef_state?DaType=',
 


  //============Hra Master==========
  BindPayScaleCode: apiHost + 'HraMaster/BindPayScaleCode',
  BindPayLevel: apiHost + 'HraMaster/BindPayLevel',
  GetCityDetails: apiHost + 'HraMaster/GetCityDetails?StateID=',
  GetCityClass: apiHost + 'HraMaster/GetCityClass?payCommId=',
  CreateHraMaster: apiHost + 'HraMaster/CreateHraMaster',
  UpdateHraMaster: apiHost + 'HraMaster/UpdateHraMaster',
  GetHraMasterDetails: apiHost + 'HraMaster/GetHraMasterDetails',
  EditHraMasterDetails: apiHost + 'HraMaster/EditHraMasterDetails?hraMasterID=',
  DeleteHraMaster: apiHost + 'HraMaster/DeleteHraMaster?hraMasterID=',
  GetPayCommissionByEmployeeType: apiHost + 'HraMaster/GetPayCommissionByEmployeeType?employeeType=',

  //================PraoMaster===============
  
  GetControllerCode: 'PraoMaster/GetControllerCode',
  BindCtrlName: 'PraoMaster/BindCtrlName?CtrlrCode=',
  BindState: 'PraoMaster/BindState',
  BindDistrict: 'PraoMaster/BindDistrict?stateId=',
  SaveOrUpdate: 'PraoMaster/SaveOrUpdate?PraoModel=',
  BindPraoMasterDetails: 'PraoMaster/BindPraoMasterDetails',
  ViewEditOrDeletePrao: apiHost +'PraoMaster/ViewEditOrDeletePrao',



  //============Tpta Master==========
  BindUnionTerritory: apiHost + 'TptaMaster/BindUnionTerritory',
  CreateTptaMaster: apiHost + 'TptaMaster/CreateTptaMaster',
  GetTptaMasterDetails: apiHost + 'TptaMaster/GetTptaMasterDetails',
  EditTptaMasterDetails: apiHost + 'TptaMaster/EditTptaMasterDetails?masterID=',
  GetTptaMasterDetailsByID: apiHost + 'TptaMaster/GetTptaMasterDetailsByID?masterID=',
  DeleteTptaMaster: apiHost + 'TptaMaster/DeleteTptaMaster?masterID=',

  //====Family Definition GPF Master==========
  CreatefdGpfMaster: apiHost + 'FdGpfMaster/CreatefdGpfMaster',
  GetFdGpfMasterDetails: apiHost + 'FdGpfMaster/GetFdGpfMasterDetails',
  EditFdGpfMasterDetails: apiHost + 'FdGpfMaster/EditFdGpfMasterDetails?fdMasterID=',
  DeleteFdGpfMaster: apiHost + 'FdGpfMaster/DeleteFdGpfMaster?fdMasterID=',

  //====Family Definition Pension Master==========
  //CreatefdPensionMaster: apiHost + 'FdGpfMaster/CreatefdPensionMaster',
  //GetFdPensionMasterDetails: apiHost + 'FdGpfMaster/GetFdPensionMasterDetails',
  //EditFdPensionMasterDetails: apiHost + 'FdGpfMaster/EditFdPensionMasterDetails?fdMasterID=',
  //DeleteFdPensionMaster: apiHost + 'FdGpfMaster/DeleteFdPensionMaster?fdMasterID=',

  //====Commutation table Pension Master==========
  CreateCommPensionMaster: apiHost + 'CommutationTable/CreateCommPensionMaster',
  GetCommPensionMasterDetails: apiHost + 'CommutationTable/GetCommPensionMasterDetails',
  //=====================Gpf Interest Rate==========================================
  DeleteGpfInterestRate: apiHost + 'GPFInterestRate/Delete?gpfInterestID=',
  GetGPFInterestRateDetails: apiHost + 'GPFInterestRate/GetGPFInterestRateDetails',
  GpfInterestRateInsertandUpdate: apiHost + 'GPFInterestRate/InsertandUpdate',
  //======================Gpf Advance Reasons=================================
  BindMainReason: apiHost + 'GpfAdvanceReasons/BindMainReason',
  GpfAdvancereasonsInsertandUpdate: apiHost + 'GpfAdvanceReasons/GpfAdvancereasonsInsertandUpdate',
  GetPfType: apiHost + 'Master/GetPfType',
  GetGpfAdvancereasonsDetails: apiHost + 'GpfAdvanceReasons/GetGpfAdvancereasonsDetails',
  GpfAdvancereasonsDelete: apiHost + 'GpfAdvanceReasons/GpfAdvancereasonsDelete?GpfAdvanceReasonID=',
  //======================Gpf Withdrawal Rules=================================
  GpfWithdrawalRules: 'GpfWithdrawRules/GetGpfWithdrawRules',
  InsertUpdateGpfWithdrawalRules: 'GpfWithdrawRules/InsertUpdateGpfWithdrawRules',
  GpfWithdrawalRulesByID: 'GpfWithdrawRules/GetGpfWithdrawRulesById',
  DeleteGpfWithdrawalRules: 'GpfWithdrawRules/DeleteGpfWithdrawRules?msGpfWithdrawRulesRefID=',
  //======================Gpf Advance Rules=================================
  GpfAdvanceRules: 'GpfAdvanceRules/GetGpfAdvanceRules',
  InsertUpdateGpfAdvanceRules: 'GpfAdvanceRules/InsertUpdateGpfAdvanceRules',
  GpfAdvanceRulesByID: 'GpfAdvanceRules/GetGpfAdvanceRulesById',
  DeleteGpfAdvanceRules: 'GpfAdvanceRules/DeleteGpfAdvanceRules?msGpfAdvanceRulesRefID=',

  //======================Gpf Recovery Rules=================================
  GpfRecoveryRules: 'GpfRecoveryRules/GetGpfRecoveryRules',
  InsertUpdateGpfRecoveryRules: 'GpfRecoveryRules/InsertUpdateGpfRecoveryRules',
  GpfRecoveryRulesByID: 'GpfRecoveryRules/GetGpfRecoveryRulesById',
  DeleteGpfRecoveryRules: 'GpfRecoveryRules/DeleteGpfRecoveryRules?msGpfRecoveryRulesRefID=',



  //End Of Masters

  //Role Manegement
  getAllRole: apiHost + 'Role/GetAlRoles',
  getAllActiveRollList: apiHost + 'Role/GetAllActiveRollList',
  GetAllCustomeRollList: apiHost + 'Role/GetAllCustomeRollList',
  getAssignRoleForUser: apiHost + 'Role/GetAssignRoleForUser?roleID=',
  GetBindUserListddl: apiHost + 'Role/GetBindUserListddl',

  saveNewRole: apiHost + 'Role/CreateNewRole',
  getUserDetails: apiHost + 'Role/GetUserDetails',
  revokeUser: apiHost + 'Role/RevokeUser',
  getUserDetailsByPan: apiHost + 'Role/GetUserDetailsByPan',
  saveAssignRoleForUser: apiHost + 'Role/SaveAssignRoleForUser',
  getAllMenuList: apiHost + 'Role/GetAllMenuList',
  saveMenuPerMission: apiHost + 'Role/SaveMenuPerMission',
  checkMenuPerMission: apiHost + 'Role/CheckMenuPerMission',
  saveMenu: apiHost + 'Menu/SaveMenu?hdnMenuId=',
  FillUpdateMenu: apiHost + 'Menu/FillUpdateMenu?MenuId=',
  bindDropDownMenu: apiHost + 'Menu/BindDropDownMenu',
  bindMenuInGride: apiHost + 'Menu/BindMenuInGride',
  activeDeactiveMenu: apiHost + 'Menu/ActiveDeactiveMenu',
  GetPredefineRole: apiHost + 'Menu/GetPredefineRole',
  //End Of RoleManagmentModule

  // Employee Data Capture
  getSalutation: 'master/getsalutation',
  getRelation: 'master/getRelation',
  getEmployeeType: 'master/getEmpType',
  getEmployeeSubType: 'master/getEmpSubType',
  getJoiningMode: 'master/getJoiningMode',
  getJoiningCategory: 'master/getJoiningCategory?parentTypeId=',
  getverifiedEmpCode: 'EmployeeDetails/GetVerifyEmpCode',
  GetEmpPersonalDetailsByID: 'EmployeeDetails/GetEmpPersonalDetailsByID',
  GetIsDeputEmp: 'EmployeeDetails/GetIsDeputEmp',
  getMakerEmpList: 'EmployeeDetails/MakerEmpList',
  UpdateEmpDetails: 'EmployeeDetails/UpdateEmpDetails',
  SavePHDetails: 'EmployeeDetails/SavePHDetails',
  getDeputationType: 'master/getdeputationtype',
  getServiceType: 'master/getservicetype',
  getGender: 'master/getgender',
  InsertUpdateEmployeeDetails: 'EmployeeDetails/InsertUpdateEmpDetails',
  GetEmpPHDetails: 'EmployeeDetails/GetEmpPHDetails?EmpCd=',
  GetPhysicalDisabilityTypes: 'EmployeeDetails/GetPhysicalDisabilityTypes',
  GetState: 'master/getstate',
  uploadFiles: 'master/uploadFiles',
  GetMaritalStatus: 'FamilyDetails/GetMaritalStatus',
  getFamilyDetails: 'FamilyDetails/getFamilyDetails',
  getAllFamilyDetails: 'FamilyDetails/getAllFamilyDetails',
  UpdateEmpFamilyDetails: 'FamilyDetails/UpdateEmpFamilyDetails',
  DeleteFamilyDetails: 'FamilyDetails/DeleteFamilyDetails',
  getAllNomineeDetails: 'NomineeDetails/getAllNomineeDetails',
  getNomineeDetails: 'NomineeDetails/getNomineeDetails',

  UpdateNomineeDetails: 'NomineeDetails/UpdateNomineeDetails',
  DeleteNomineeDetails: 'NomineeDetails/DeleteNomineeDetails',
  getBankDetails: 'BankDetails/GetBankDetails',
  getBankDetailsByIFSC: 'BankDetails/GetBankDetailsByIFSC',
  getAllIFSC: 'BankDetails/GetAllIFSC',

  UpdateBankDetails: 'BankDetails/UpdateBankDetails',
  GetHRACity: 'PostingDetails/GetHRACity',
  GetTACity: 'PostingDetails/GetTACity',
  GetAllDesignation: 'PostingDetails/GetAllDesignation',
  SaveUpdatePostingDetails: 'PostingDetails/SaveUpdatePostingDetails',
  GetAllPostingDetails: 'PostingDetails/GetAllPostingDetails',

  getMaintainByOfc: 'ServiceDetails/getMaintainByOfc',
  getServiceDetails: 'ServiceDetails/getAllServiceDetails',
  SaveUpdateServiceDetails: 'ServiceDetails/SaveUpdateServiceDetails',
  getOrganisationType: 'PayDetails/getOrganisationType',
  getNonComputationalDues: 'PayDetails/GetNonComputationalDues',
  getNonComputationalDeductions: 'PayDetails/GetNonComputationalDeductions',
  getPayLevel: 'PayDetails/getPayLevel',
  getPayIndex: 'PayDetails/getPayIndex',
  getBasicDetails: 'PayDetails/getBasicDetails',
  getGradePay: 'PayDetails/getGradePay',
  getPayScale: 'PayDetails/getPayScale',
  SaveUpdatePayDetails: 'PayDetails/SaveUpdatePayDetails',
  getPayDetails: 'PayDetails/getPayDetails',
  getEntitledOffVeh: 'PayDetails/getEntitledOffVeh',
  getInsuranceApplicable: 'CGEGIS/GetInsuranceApplicable',
  getCGEGISCategory: 'CGEGIS/GetCGEGISCategory',
  SaveUpdateCGEGISDetails: 'CGEGIS/SaveUpdateCGEGISDetails',
  getCGEGISDetails: 'CGEGIS/GetCGEGISDetails',
  SaveUpdateCGHSDetails: 'CGHSDetails/SaveUpdateCGHSDetails',
  getCGHSDetails: 'CGHSDetails/getCGHSDetails',
  getQuarterOwnedby: 'QuarterDetails/getQuarterOwnedby',
  getAllottedTo: 'QuarterDetails/getAllottedTo',
  getRentStatus: 'QuarterDetails/getRentStatus',
  getQuarterType: 'QuarterDetails/getQuarterType',
  GetCustodian: 'QuarterDetails/GetCustodian',
  GetGPRACityLocation: 'QuarterDetails/GetGPRACityLocation',
  SaveUpdateQuarterDetails: 'QuarterDetails/SaveUpdateQuarterDetails',
  QuarterAllDetails: 'QuarterDetails/QuarterAllDetails',
  QuarterDetails: 'QuarterDetails/QuarterDetails',
  DeleteQuarterDetails: 'QuarterDetails/DeleteQuarterDetails',
  getAllEmpDetails: 'ForwardToChecker/getAllEmpDetails',
  getAllEmployeeCompleteDetails: 'ForwardToChecker/getAllEmployeeCompleteDetails',
  ForwardToCheckerEmpDetails: 'ForwardToChecker/ForwardToCheckerEmpDetails',
  verifyEmpData: 'ForwardToChecker/verifyEmpData',
  // End Of Employee Data Capture

  // Login
  LoginDetails: 'LoginRole/LoginCheck',
  UserDetails: apiHost + 'LoginRole/UserDetails?username=',
  LoginNewUser: 'LoginRole/LoginNewUser',
  RoleActivation: apiHost + 'LoginRole/RoleActivation?ID=',
  currentrUrl: apiHost + 'LoginRole/currentrUrl?url=',
  IsMenuPermissionAssigned: apiHost + 'LoginRole/IsMenuPermissionAssigned',
  //-----------------User creation---------------------
  getAllControllers: apiHost + 'Controllerrole/GetAllControllers?controllerID=',
  controllerchanged: apiHost + 'Controllerrole/oncontrollerroleSelectchanged?MsControllerID=',
  PaosAssigned: apiHost + 'Controllerrole/PaosAssigned?empCd=',
  GetAllUserRoles: apiHost + 'Controllerrole/GetAllUserRoles?username=',

  GetAllDDO: apiHost + 'ddorole/GetAllDDO?PAOID=',
  EmployeeList: apiHost + 'ddorole/EmployeeList?DDOID=',
  Assigned: apiHost + 'ddorole/Assigned?empCd=',
  //GetAllUserRoles: apiHost + 'Controllerrole/GetAllUserRoles?username='
  GetAllpaos: apiHost + 'paoroll/GetAllpaos?controllerID=',
  onpaoSelectchanged: apiHost + 'paoroll/GetAllDDOsUnderSelectedPAOs?MsPAOID=',
  AssignedPAO: apiHost + 'paoroll/Assigned?empCd=',
  login: apiHost + 'Login/Login?userId=',
  GetAllUserRolesByDDO: apiHost + 'UsersByDDO/GetAllUserRolesByDDO?username=',
  EmployeeListOfDDO: apiHost + 'UsersByDDO/EmployeeListOfDDO?DDOID=',
  AssignedCMD: apiHost + 'UsersByDDO/AssignedCMD?empCd=',
  AssignChecker: apiHost + 'UsersByDDO/AssignChecker?empCd=',
  //AssingedDDOChecker: apiHost + 'UsersByDDO/AssingedDDOChecker',
  //AssingedDDOCheckerMaker: apiHost + 'UsersByDDO/AssingedDDOCheckerMaker',
  makerEmpList: apiHost + 'maker/makerEmpList?DDOID=',
  AssignedMaker: apiHost + 'maker/AssignedMaker?empCd=',
  HooCheckerEmpList: apiHost + 'HOO/HooCheckerEmpList?DDOID=',
  AssignedHOOChecker: apiHost + 'HOO/AssignedHOOChecker?empCd=',
  hooMakerEmpList: apiHost + 'HOO/hooMakerEmpList?DDOID=',
  AssignedHOOMaker: apiHost + 'HOO/AssignedHOOMaker?empCd=',
  SelfAssignDDOChecker: apiHost + 'UsersByDDO/SelfAssignDDOChecker?UserID=',
  SelfAssignCheck: apiHost + 'UsersByDDO/SelfAssignCheck?UserID=',
  SelfAssignedDDOMaker: apiHost + 'maker/SelfAssignedDDOMaker?UserID=',
  SelfAssignMakerRole: apiHost + 'maker/SelfAssignMakerRole?UserID=',
  SelfAssignHOOChecker: apiHost + 'HOO/SelfAssignedHOOChecker?UserID=',
  SelfAssignHOOCheckerRole: apiHost + 'HOO/SelfAssignHOOCheckerRole?UserID=',
  AssignedEmp: apiHost + 'Controllerrole/AssignedEmp?controllerID=',
  AssignedEmpList: apiHost + 'paoroll/AssignedEmpList?PAOID=',
  AssignedEmpDDO: apiHost + 'ddorole/AssignedEmpDDO?DDOID=',
  AssignedDDO: apiHost + 'ddorole/AssignedDDO?DDOID=',
  AssignedPAOEmp: apiHost + 'paoroll/AssignedPAOEmp?PAOID=',
  AssignedDDOCheckerEmpList: apiHost + 'UsersByDDO/AssignedDDOCheckerEmpList?DDOID=',
  AssignedMakerEmpList: apiHost + 'maker/AssignedMakerEmpList?DDOID=',
  OnBoardingSubmit: apiHost + 'OnBoarding/OnBoardingSubmit',
  GetAllReuestNoOfOnboarding: apiHost + 'OnBoarding/GetAllReuestNoOfOnboarding',
  FetchRequestLetterNoRecord: apiHost + 'OnBoarding/FetchRequestLetterNoRecord?OnboardingId=',
  //------------------


  /*Start PayBillGroup*/


  getAccountHeads: 'PayBill/GetAccountHeads?PermDDOId=',

  InsertUpdatePayBillGroup: 'PayBill/InsertUpdatePayBillGroup',
  BindPayBillGroup: 'Shared/GetPayBillGroup?PermDdoId=',
  GetPayBillGroupDetailsById: 'PayBill/GetPayBillGroupDetailsById?BillGroupId=',
  GetPayBillGroupEmpAttach: 'PayBill/GetpayBillGroupEmpAttach?EmpPermDDOId=00003',
  GetPayBillGroupEmpDeAttach: 'Paybill/GetpayBillGroupEmpDeAttach?EmpPermDDOId=00003&BillGrpID=',
  UpdatePayBillGroupIdByEmpCode: 'Paybill/updatePayBillGroupbyEmpCodeandBillGroupId?empCode=',
  updatePayBillGroupIdnullByEmpCode: 'Paybill/updatePayBillGroupNullbyEmpCode?empCode=',
  getAccountHeadsOT1: 'PayBill/GetAccountHeadsOT1?FinFromYr=',
  GetAccountHeadOT1AttachList: 'PayBill/GetAccountHeadOT1AttachList?DDOCode=',
  UpdateaccountHeadOT1: 'PayBill/UpdateaccountHeadOT1?accountHeadvalue=',
  GetAccountHeadOT1PaybillGroupStatus: 'PayBill/GetAccountHeadOT1PaybillGroupStatus?DDOCode=',
  UpdateAccountHeadOT1PaybillGroupStatus: 'PayBill/UpdateAccountHeadOT1PaybillGroupStatus?accountHeadvalue=',

  getNgRecovery: 'PayBill/GetNgRecovery?PermDDOId=',
  GetBankDetailsByIfscCode: 'PayBill/GetBankdetailsByIfsc?IfscCode=',
  insertUpdateNgRecovery: 'PayBill/InsertUpdateNgRecovery',
  activeDeactiveNgRecovery: 'PayBill/ActiveDeactiveNgRecovery',

  getfinancialYear: 'PayBill/GetFinancialYear',
  getAllMonth: 'PayBill/GetAllMonth',
  GetEmployeeByPaybillAndDesig: 'PayBill/GetEmployeeByPaybillAndDesig',
  InsertNgrecoveryEntry: 'PayBill/InsertAllNgEntry',
  GetNgRecoveryList: 'PayBill/GetNgRecoveryList',
  GetAllNgRecoveryandPaybillGrpDetails: 'PayBill/GetAllNgRecoveryandPaybillGrpDetails',
  /*End PayBillGroup*/

  // -------Suspension Module--------------
  suspensionControllerName: 'Suspension',
  // -------Suspension Module End--------

  psuMasterControllerName: 'PsuMaster',

  // -------------leave management-----------
  LeavesControllerName: apiHost + 'Leaves',
  //End leave management


  // -------------leave management-----------
  DDOMasterControllerName: apiHost + 'DDOMaster',
  //End leave management

  

  //-----------existing-loan-------------------
  FloodController: apiHost + 'Flood',
  MultipleInstRecoveryController: apiHost + 'MultipleInstRecovery',

  getBindDropDownDesign: 'ExistingLoan/BindDropDownDesign',
  getBindDropDownBillGroup: 'ExistingLoan/BindDropDownBillGroup',
  getBindDropDownEmp: 'existingloan/BindDropDownEmp',
  getLoanAdvanceDetails: 'ExistingLoan/LoanAdvanceDetails?ddoid=',
  GetLoanType: 'LoanApplication/GetLoanType?loantypeFlag=',
  getAllEmpNameSelectedDesignId: 'existingloan/GetAllEmpNameSelectedDesignId?MsDesignID=',
  GetExistingEmpLoanDetails: 'existingloan/GetEmpLoanListDetails?PayBillGpID=',
  ExistingLoanStatus: 'ExistingLoan/BindExistingLoanStatus',
  GetExistLoanDetailsByID: 'ExistingLoan/GetExistLoanDetailsByID?MSPayAdvEmpdetID=',
  ExistingLoanForwordToDDO: 'ExistingLoan/ExistingLoanForwordToDDO',
  DeleteEmpDetails: apiHost + 'ExistingLoan/DeleteEmpDetails',


  //----------------RecoveryChallan------------------------------
  GetRecoveryChallan: 'RecoveryChallan/GetRecoveryChallan?EmpCode=',
  saveUpdateRecoveryChallan: 'RecoveryChallan/SaveUpdateRecoveryChallan',
  //-------------------------------------------------------------------------




  //----------------new loan application------------------------------
  GetLoanpurposebyLoanCode: 'LoanApplication/GetLoanpurposebyLoanCode?payLoanCode=',
  GetDetailsbyLoanCodePurposecode: 'LoanApplication/GetDetailsbyLoanCodePurposecode?payLoanCode=',
  GetAllEmpNameSelectedByDesignId: 'existingloan/GetAllEmpNameSelectedDesignId?MsDesignID=',
  GetEmpDetailsbyEmpcode: 'LoanApplication/GetEmpDetailsbyEmpcode?EmpCode=',
  GetSanctionDetailsbyEmpcode: 'LoanApplication/GetSanctionDetailsbyEmpcode?EmpCode=',
  GetPayLoanPurposeStatus: 'LoanApplication/GetPayLoanPurposeStatus?LoanBillID=',
  PostLoanDetails: 'LoanApplication/SaveLoanDetails',
  PostPropertyOwnerDetails: 'LoanApplication/SavePropertyOwnerDetails',
  UpdateLoanDetails: 'LoanApplication/UpdateLoanDetails',
  getExistingLoanType: 'ExistingLoan/BindDropDownLoanType',
  getBindHeadAccount: 'existingloan/BindHeadAccount?LoanCD=',
  ExistingLoanInsertData: 'existingloan/SaveExistingLoanData',
  GetLoanDetails: 'LoanApplication/GetLoanDetails?mode=',
  GetFwdtochkerDetails: 'LoanApplication/GetFwdtochkerDetails',
  GetEmpLoanDetailsByID: 'LoanApplication/GetEmpLoanDetailsByID?EmpCd=',
  EditLoanDetailsByEMPID: 'LoanApplication/EditLoanDetailsByEmpID?id=',
  UpdateLoanDetailsbyID: 'LoanApplication/UpdateLoanDetailsbyID?id=',
  DeleteLoanDetailsByEMPID: 'LoanApplication/DeleteLoanDetailsByEMPID?id=',
  GetEmpDetails: 'LoanApplication/GetEmpDetails?EmpCd=',
  GetEmpfilterbydesignBillgroup: 'LoanApplication',
  GetBillgroupfilterbydesignID: 'LoanApplication',
  UpdateSanctionDetails: 'LoanApplication/UpdateSanctionDetails',
  ValidateCoolingPeriod: 'LoanApplication/ValidateCoolingPeriod?LoanCode=',
  //end

  // --Shared----

  SharedControllerName: 'Shared',

  //--end

  // --Recovery Excess Payment

  GetFinancialYears: apiHost + 'ExcessRecoveryPayment/GetFinancialYears',
  GetRecoveryLoanType: apiHost + 'ExcessRecoveryPayment/GetLoanPaymentType',
  GetRecoveryElement: apiHost + 'ExcessRecoveryPayment/GetRecoveryElement',
  GetAccountHead: apiHost + 'ExcessRecoveryPayment/GetAccountHead',
  GetRecoveryComponent: apiHost + 'ExcessRecoveryPayment/GetRecoveryComponent',
  InsertUpdateExRecovery: apiHost + 'ExcessRecoveryPayment/InsertUpdateExRecovery',
  GetExcessRecoveryDetail: apiHost + 'ExcessRecoveryPayment/GetExcessRecoveryDetail',
  EditExcessRecoveryDetail: apiHost + 'ExcessRecoveryPayment/EditExcessRecoveryDetail',
  DeleteExcessRecoveryData: apiHost + 'ExcessRecoveryPayment/DeleteExcessRecoveryData?RecExcessId=',
  ForwardExRecoveryData: apiHost + 'ExcessRecoveryPayment/ForwardExRecoveryData',

  insertUpdateComponentAmt: apiHost + 'ExcessRecoveryPayment/insertUpdateComponentAmt',
  getComponentAmtDetails: apiHost + 'ExcessRecoveryPayment/getComponentAmtDetails',
  deleteComponentAmt: apiHost + 'ExcessRecoveryPayment/deleteComponentAmt?CmpId=',


  SaveTempPayment: apiHost + 'RecoveryPayment/SaveTempPayment',
  SaveRecoveryExcess: apiHost + 'RecoveryPayment/SaveRecoveryExcess',
  getTempPaymentRecovery: apiHost + 'RecoveryPayment/getTempPaymentRecovery',
  //--end

  //Depuatation
  getPayItems: 'DeputationIn/GetPayItems',
  getDesignationAll: 'DeputationIn/GetAllDesignation?controllerId=',
  getDepuatationCommunticationAddress: 'DeputationIn/GetCommunicationAddress',
  getDeductionScheduleDetailsbyEmployee: 'DeputationIn/GetDeductionScheduleDetails',
  saveUpdateDeputationInDetails: 'DeputationIn/SaveUpdateDeputationInDetails',
  getScheme_CodeByMsEmpDuesID: 'DeputationIn/GetScheme_CodeByMsEmpDuesID?MsEmpDuesID=',
  getAllDepuatationOutDetailsByEmployee: 'DeputationOut/GetAllDepuatationOutDetailsByEmployee',
  saveUpdateDeputationOutDetails: 'DeputationOut/SaveUpdateDeputationOutDetails',
  getAllRepatriationDetailsByEmployee: 'RepatriationDetails/GetAllRepatriationDetailsByEmployee',
  saveUpdateRepatriationDetails: 'RepatriationDetails/SaveUpdateRepatriationDetails',
  updateforwardStatusUpdateByDepID: 'DeputationIn/UpdateforwardStatusUpdateByDepId',
  updateforwardStatusUpdateByEmpCode: 'DeputationIn/UpdateforwardStatusUpdateByEmpCode',
  getCheckDuputationInEmployee: 'RepatriationDetails/CheckDepuatationinEmployee',
  //--end

  //Promotions
  PromotionControllerName: 'Promotion',
  //--end

  //ReJoiningService
  ReJoiningServiceControllerName: 'ReJoiningService',
  //--end

  //Lien Period
  getControllerList: 'Transferofflienperiod/GetControllList',
  getDdolistbyControllerid: 'Transferofflienperiod/GetDdolistbyControllerid?controllerId=',
  getOfficeAddressDetails: 'Transferofflienperiod/GetOfficeAddressDetails',
  saveUpdateTransferOffLienPeriod: 'Transferofflienperiod/SaveUpdateTransferOffLienPeriod',
  deleteTransferOffLienPeriod: 'Transferofflienperiod/DeleteTransferOffLienPeriod',
  getTransferofficelienPeriodDetails: 'Transferofflienperiod/getTransferofficelienPeriodDetails',
  lienPeriodUpdateForwardStatusUpdateByEmpCode: 'Transferofflienperiod/UpdateforwardStatusUpdateByEmpCode',

  getEpsEmpJoinOffAfterLienPeriodDetailByEmp: 'EpsEmpJoinOffAfterLienPeriod/GetEpsEmpJoinOffAfterLienPeriodDetailByEmp',
  getJoiningAccountOf: 'master/GetJoiningAccount',
  getOfficeList: 'master/GetOfficeList',
  getOfficeCityClass: 'master/GetOfficeCityClassList',
  saveUpdateEpsEmployeeJoinAfterLienPeriod: 'EpsEmpJoinOffAfterLienPeriod/SaveUpdateEpsEmployeeJoinAfterLienPeriod',
  deleteEpsEmployeeJoinAfterLienPeriodById: 'EpsEmpJoinOffAfterLienPeriod/DeleteEpsEmployeeJoinById',
  epsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: 'EpsEmpJoinOffAfterLienPeriod/UpdateforwardStatusUpdateByEmpCode',
  getPayCommissionListLien: 'master/GetPayCommissionList',


  getNonEpsEmpJoinOffAfterLienPeriodDetailByEmp: 'NonEpsEmpAfterLienPeriod/GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp',
  saveUpdateNonEpsEmployeeJoinAfterLienPeriod: 'NonEpsEmpAfterLienPeriod/SaveUpdateNonEpsEmployeeJoinAfterLienPeriod',
  deleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode: 'NonEpsEmpAfterLienPeriod/DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode',
  nonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode: 'NonEpsEmpAfterLienPeriod/NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode',
  //--end

  //Increment

  //Advance Increment
  CreateAdvIncrement: apiHost + 'AdvIncrement/CreateAdvIncrement',
  GetIncrementDetails: apiHost + 'AdvIncrement/GetIncrementDetails?empcode=',
  EditAdvIncrement: apiHost + 'AdvIncrement/EditAdvIncrement?incID=',
  DeleteAdvDetails: apiHost + 'AdvIncrement/DeleteAdvDetails?incID=',
  Forwardtochecker: apiHost + 'AdvIncrement/Forwardtochecker',
  GetEmployeeByDesigPayComm: apiHost + 'AdvIncrement/GetEmployeeByDesigPayComm',

  // Regular Incremnt
  CreateOrderNo: apiHost + 'Regincrement/CreateOrderNo',
  GetOrderDetails: apiHost + 'Regincrement/GetOrderDetails',
  deleteOrderDetails: apiHost +'Regincrement/deleteOrderDetails',
  GetEmployeeForIncrement: apiHost + 'Regincrement/GetEmployeeForIncrement',
  GetEmployeeForNonIncrement: apiHost + 'Regincrement/GetEmployeeForNonIncrement',
  AddEmpForIncrement: apiHost + 'Regincrement/InsertRegIncrementData',
  ForwardtocheckerForInc: apiHost + 'Regincrement/ForwardtocheckerForInc',
  GetEmployeeDesignation: apiHost + 'Regincrement',

  // Stop Increment
  GetEmployeeStopIncrement: apiHost + 'StopIncrement/GetEmployeeStopIncrement',
  GetEmployeeForNotStop: apiHost + 'StopIncrement/GetEmployeeForNotStop',
  InsertStopIncrementData: apiHost + 'StopIncrement/InsertStopIncrementData',
  ForwardcheckerForStopInc: apiHost + 'StopIncrement/ForwardcheckerForStopInc',

  // Annual Increment Report
  GetSalaryMonth: apiHost + 'AnnualIncrement/GetSalaryMonth',
  GetOrderWithEmployee: apiHost + 'AnnualIncrement/GetOrderWithEmployee',
  GetAnnualIncReportData: apiHost + 'AnnualIncrement/GetAnnualIncReportData',
  GetEmpDueForIncReport: apiHost + 'AnnualIncrement/GetEmpDueForIncReport',

  //ENd

  //change
  ChangeDeleteDetails: 'Change/ChangeDeleteDetails?empCd=',
  GetDdlvalue: 'Change/GetDdlvalue?Flag=',

  EOSControllerName: 'EOS',
  GetExtensionOfServiceDetails: 'EOS/GetExtensionOfServiceDetails?empcode=',
  //FetchEOSDetails: 'EOS/FetchEOSDetails?employeeId=',
  
  ForwardToCheckerUserDtls: 'EOS/ForwardToCheckerUserDtls?empCd=',
  

  InsertDOBDetails: 'Change',
  GetDOBDetails: 'Change/GetDOBDetails?empcode=',
  ForwardToCheckerDOBUserDtls: 'Change/ForwardToCheckerDOBUserDtls?empCd=',

  InsertNameGenderDetails: 'Change',
  GetNameGenderDetails: 'Change/GetNameGenderDetails?empcode=',
  ForwardToCheckerNameGenderUserDtls: 'Change/ForwardToCheckerNameGenderUserDtls?empCd=',


  InsertPfNpsFormDetails: 'Change',
  GetPfNpsFormDetails: 'Change/GetPfNpsFormDetails?empcode=',
  ForwardToCheckerPfNpsUserDtls: 'Change/ForwardToCheckerPfNpsUserDtls?empCd=',

  InsertPanNoFormDetails: 'Change',
  GetPanNoFormDetails: 'Change/GetPanNoFormDetails?empcode=',
  //FetchPanNosrchDetails: 'Change/FetchPanNosrchDetails?employeeId=',
  ForwardToCheckerPanNoUserDtls: 'Change/ForwardToCheckerPanNoUserDtls?empCd=',

  InsertHraccFormDetails: 'Change',
  GetHraccFormDetails: 'Change/GetHraccFormDetails?empcode=',
  //FetchHraccsrchDetails: 'Change/FetchHraccsrchDetails?employeeId=',  
  ForwardToCheckerHraccUserDtls: 'Change/ForwardToCheckerHraccUserDtls?empCd=',

  InsertdesignationFormDetails: 'Change',
  GetdesignationFormDetails: 'Change/GetdesignationFormDetails?empcode=',
  ForwardToCheckerdesignationUserDtls: 'Change/ForwardToCheckerdesignationUserDtls?empCd=',

  InsertexservicemenFormDetails: 'Change',
  GetexservicemenFormDetails: 'Change/GetexservicemenFormDetails?empcode=',
  ForwardToCheckerexservicemenUserDtls: 'Change/ForwardToCheckerexservicemenUserDtls?empCd=',
   
  InsertCGHSFormDetails: 'Change',
  GetCGHSFormDetails: 'Change/GetCGHSFormDetails?empcode=',
  ForwardToCheckerCGHSUserDtls: 'Change/ForwardToCheckerCGHSUserDtls?empCd=',

  InsertCGEGISFormDetails: 'Change',
  GetCGEGISFormDetails: 'Change/GetCGEGISFormDetails?empcode=',
  ForwardToCheckerCGEGISFormUserDtls: 'Change/ForwardToCheckerCGEGISFormUserDtls?empCd=',


  InsertJoiningModeFormDetails: 'Change',
  GetJoiningModeFormDetails: 'Change/GetJoiningModeFormDetails?empcode=',
  ForwardToCheckerJoiningModeFormUserDtls: 'Change/ForwardToCheckerJoiningModeFormUserDtls?empCd=',

  InsertDOJDetails: 'Change',
  GetDOJDetails: 'Change/GetDOJDetails?empcode=',
  ForwardToCheckerDOJUserDtls: 'Change/ForwardToCheckerDOJUserDtls?empCd=',

  InsertDOEDetails: 'Change',
  GetDOEDetails: 'Change/GetDOEDetails?empcode=',
  ForwardToCheckerDOEUserDtls: 'Change/ForwardToCheckerDOEUserDtls?empCd=',

  InsertDORDetails: 'Change',
  GetDORDetails: 'Change/GetDORDetails?empcode=',
  ForwardToCheckerDORUserDtls: 'Change/ForwardToCheckerDORUserDtls?empCd=',

  InsertCasteCategoryFormDetails: 'Change',
  GetCasteCategoryFormDetails: 'Change/GetCasteCategoryFormDetails?empcode=',
  ForwardToCheckerCasteCategoryFormUserDtls: 'Change/ForwardToCheckerCasteCategoryFormUserDtls?empCd=',

  InsertEntofficevehicleFormDetails: 'Change',
  GetEntofficevehicleFormDetails: 'Change/GetEntofficevehicleFormDetails?empcode=',
  ForwardToCheckerEntofficevehicleFormUserDtls: 'Change/ForwardToCheckerEntofficevehicleFormUserDtls?empCd=',

  InsertBankFormDetails: 'Change',
  GetBankFormDetails: 'Change/GetBankFormDetails?empcode=',
  ForwardToCheckerBankFormUserDtls: 'Change/ForwardToCheckerBankFormUserDtls?empCd=',

  InsertaddtapdFormDetails: 'Change',
  GetaddtapdFormDetails: 'Change/GetaddtapdFormDetails?empcode=',
  ForwardToCheckeraddtapdUserDtls: 'Change/ForwardToCheckeraddtapdUserDtls?empCd=',

  InsertTPTAFormDetails: 'Change',
  GetTPTAFormDetails: 'Change/GetTPTAFormDetails?empcode=',
  ForwardToCheckerTPTAUserDtls: 'Change/ForwardToCheckerTPTAUserDtls?empCd=',

  InsertStateGISFormDetails: 'Change',
  GetStateGISFormDetails: 'Change/GetStateGISFormDetails?empcode=',
  ForwardToCheckerStateGISUserDtls: 'Change/ForwardToCheckerStateGISUserDtls?empCd=',


  InsertMobileNoEmailEmpCodeDetails: 'Change',
  GetMobileNoEmailEmpCodeDetails: 'Change/GetMobileNoEmailEmpCodeDetails?empcode=',
  ForwardToCheckerMobileNoEmailEmpCodeUserDtls: 'Change/ForwardToCheckerMobileNoEmailEmpCodeUserDtls?empCd=',
  //--end


  // -------End of Service Module--------------
  EndofServiceGetEmployee: 'EndofService/GetEndofServiceEmployees',
  EndofServiceReason: 'EndofService/EndofServiceReasonDetails',
  EmpCurrentEndDeta: 'EndofService/EmpCurrentEndDetails',
  EndofServiceGetDetailByEmp: 'EndofService/EndofServiceListDetails',
  EndofServiceInsert: 'EndofService/EndofServiceInsert',
  EndofServiceUpdate: 'EndofService/EndofServiceUpdate',
  EndofServiceDelete: 'EndofService/DeleteEndofService',
  // -------End of Service Module--------

  //User Profile
  GetUserProfileDetails: apiHost + 'UserProfile/GetUserdetailsProfile',
  //End Of User Profile

  IncomeTaxControllerName: 'IncomeTax',
  CityMasterController: 'CityMaster',

  //============================== Deduction Master ========================================
  categoryList: apiHost + 'DeductionMaster/CategoryList',
  deductionDescription: apiHost + 'DeductionMaster/DeductionDescription',
  deductionDescriptionChange: apiHost + 'DeductionMaster/DeductionDescriptionChange?payItemsCD=',
  getAutoGenratedDeductionCode: apiHost + 'DeductionMaster/GetAutoGenratedDeductionCode',
  getDeductionDefinitionList: apiHost + 'DeductionMaster/GetDeductionDefinitionList',
  deductionDefinitionSubmit: apiHost + 'DeductionMaster/DeductionDefinitionSubmit',

         //=============Deduction Rate Master==========
  GetOrgnaztionTypeForDeductionRate: 'DeductionMaster/GetOrgnazationTypeForDues',
  GetStateForDeductionRate: 'DeductionMaster/GetStateForDuesRate',
  GetPayCommForDeductionRate: 'DeductionMaster/GetPayCommForDuesRate',
  GetCityClassForDeductionRate: 'DeductionMaster/GetCityClassForDuesRate?payCommId=',
  GetDeductionCodeDeductionDefination: 'DeductionMaster/GetDuesCodeDuesDefination',
  AddDeductionDetails: 'DeductionMaster/InsertUpdateDuesDefinationandDuesMasterDetails',
  GetDeductionSlabTypeByPayCommId: 'DeductionMaster/GetSlabtypeByPayCommId?payCommId=',
  saveUpdateDeductionRateDetails: 'DeductionMaster/SaveUpdateDuesRateDetails',
  GetDeductionRateList: 'DeductionMaster/GetDuesCodeDuesRate',
  DeleteDeductionRatemaster: apiHost + 'DeductionMaster/DeleteDuesRateDetails?msduesratedetailsid=',

  //============================ Deduction Master end==============================================
};



@NgModule({
  imports: [

    FormsModule,
    HttpModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  declarations: [],
  exports: [

    FormsModule,
    HttpModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: APP_CONFIG,
    useValue: APP_DI_CONFIG
  }]
})

export class GlobalModule {
}

