
export class commons {

  SortArrayAlphabetically(data, column, order):any {
    if (order == 'desc') {
      return data.sort(function (a, b) {
        if (a[column] > b[column]) { return -1; }
        if (a[column] < b[column]) { return 1; }
        return 0;
      });
    } else {
      return data.sort(function (a, b) {
        if (a[column] < b[column]) { return -1; }
        if (a[column] > b[column]) { return 1; }
        return 0;
      });
    }
  }

  SortArrayIntegerwise(data, column, order): any {
    if (order == 'desc') {
      return data.sort((a, b) => b[column] - a[column]);
    } else {
      return data.sort((a, b) => a[column] - b[column]);
    }
  }
}
