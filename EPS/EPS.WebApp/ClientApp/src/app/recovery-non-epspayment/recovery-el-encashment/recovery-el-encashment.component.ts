import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { MatPaginator, MatTableDataSource, MatSort, MatSelect, MatSnackBar } from '@angular/material';
import { PayBillGroupModel, DesignationModel, EmpModel } from '../../model/Shared/DDLCommon';
import { takeUntil } from 'rxjs/operators';
import { recoveryService } from '../../services/Recovery/recovery.service';
import swal from 'sweetalert2';

interface BillCode {
  id: string;
  name: string;
}
interface DesignCode {
  id: string;
  name: string;
}
interface EMPCode {
  id: string;
  name: string;
}

@Component({
  selector: 'app-recovery-el-encashment',
  templateUrl: './recovery-el-encashment.component.html',
  styleUrls: ['./recovery-el-encashment.component.css']
})
export class RecoveryELEncashmentComponent implements OnInit {

  constructor(private _Service: recoveryService) { }


  ArrddlBillGroup: PayBillGroupModel;
  ArrddlDesign: DesignationModel[];
  ArrddlEmployee: EmpModel;
  Bill: any[];
  Design: any[];
  EMP: any[];
  PermDdoId: string;
  private _onDestroy = new Subject<void>();
  recoveryELEncashmentList: any;


  /** searching . */
  public billCtrl: FormControl = new FormControl();
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public billFilterCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredBill: ReplaySubject<BillCode[]> = new ReplaySubject<BillCode[]>(1);
  public filteredDesign: ReplaySubject<DesignCode[]> = new ReplaySubject<DesignCode[]>(1);
  public filteredEmp: ReplaySubject<EMPCode[]> = new ReplaySubject<EMPCode[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;

  displayedColumns = ['InstAmount', 'FYear', 'AccHead', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource(this.recoveryELEncashmentList);


  ngOnInit() {
    this.PermDdoId = "00003";
    this.BindDropDownBillGroup(this.PermDdoId);
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
  }


  private filterBill() {
    if (!this.Bill) {
      return;
    }
    // get the search keyword
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.Bill.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBill.next(

      this.Bill.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  BindDropDownBillGroup(value) {
    this._Service.GetAllPayBillGroup(value).subscribe(result => {
      this.ArrddlBillGroup = result;
      this.Bill = result;
      this.billCtrl.setValue(this.Bill);
      this.filteredBill.next(this.Bill);
    });
  }
  BindDropDownDesignation(value) {
    debugger;
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }

  BindDropDownEmployee(value) {
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }

    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredEmp.next(
      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }
  submit() { }
  GetAleadytakenLoandetails(value) { }
}
