import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecoveryNonEPSPaymentModule } from './recovery-non-epspayment.module';
import { RecoveryELEncashmentComponent } from './recovery-el-encashment/recovery-el-encashment.component';
import { OtherRecoveryComponent } from './other-recovery/other-recovery.component';

const routes: Routes = [{
  path: '', component: RecoveryNonEPSPaymentModule, children: [

    { path: 'recoveryELEncashment', component: RecoveryELEncashmentComponent },
    { path: 'otherRecovery', component: OtherRecoveryComponent },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecoveryNonEPSPaymentRoutingModule { }
