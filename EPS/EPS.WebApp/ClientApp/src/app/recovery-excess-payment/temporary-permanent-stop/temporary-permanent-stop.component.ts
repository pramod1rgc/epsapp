import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import { recoveryService } from '../../services/Recovery/recovery.service';
import { PayBillGroupModel, DesignationModel, EmpModel } from '../../model/Shared/DDLCommon';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
interface BillCode {
  id: string;
  name: string;
}
interface DesignCode {
  id: string;
  name: string;
}
interface EMPCode {
  id: string;
  name: string;
}
@Component({
  selector: 'app-temporary-permanent-stop',
  templateUrl: './temporary-permanent-stop.component.html',
  styleUrls: ['./temporary-permanent-stop.component.css']
})
export class TemporaryPermanentStopComponent implements OnInit {

  //Declare Variable
  public show_dialog: boolean = false;


  Message: any;
  btnSave: boolean;
  displayedColumns: string[] = ['payMentStop', 'orderNo', 'orderdate', 'orderfromdate', 'ordertodate', 'action'];//, 'leaveTypeDesc', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  dataSourceRecoveryPayment: any;
  ObjTempData: any = {};
  ArrTempRecoveryPayment: any;
  ArrRecoveryDetails: any;
  ArrddlBillGroup: PayBillGroupModel;
  ArrddlDesign: DesignationModel[];
  ArrddlEmployee: EmpModel;
  Bill: any[];
  Design: any[];
  EMP: any[];
  PermDdoId: string;
  private _onDestroy = new Subject<void>();
  /** searching . */
  public billCtrl: FormControl = new FormControl();
  public designCtrl: FormControl = new FormControl();
  public empCtrl: FormControl = new FormControl();
  public billFilterCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredBill: ReplaySubject<BillCode[]> = new ReplaySubject<BillCode[]>(1);
  public filteredDesign: ReplaySubject<DesignCode[]> = new ReplaySubject<DesignCode[]>(1);
  public filteredEmp: ReplaySubject<EMPCode[]> = new ReplaySubject<EMPCode[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;


  //End Of Variable Declare
  constructor(private _Service: recoveryService) { }

  ngOnInit() {
    this.PermDdoId = "00003";
    //this.BindDropDownBillGroup(this.PermDdoId);
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
    //this.getTempPaymentRecovery();
    this.btnSave = true;
  }


  private filterBill() {
    if (!this.Bill) {
      return;
    }
    // get the search keyword
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.Bill.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBill.next(

      this.Bill.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  BindDropDownBillGroup(value) {
    this._Service.GetAllPayBillGroup(value).subscribe(result => {
      this.ArrddlBillGroup = result;
      this.Bill = result;
      this.billCtrl.setValue(this.Bill);
      this.filteredBill.next(this.Bill);
    });
  }
  BindDropDownDesignation(value) {
    debugger;
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }

  BindDropDownEmployee(value) {
    this._Service.GetAllEmp(value).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }

    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredEmp.next(
      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }

  getTempPaymentRecovery() {
    this._Service.getTempPaymentRecovery().subscribe(result => {
      this.ArrTempRecoveryPayment = result;
      this.dataSourceRecoveryPayment = new MatTableDataSource(result);
      this.dataSourceRecoveryPayment.paginator = this.paginator;
      this.dataSourceRecoveryPayment.sort = this.sort;
      console.log(this.dataSourceRecoveryPayment)
    })
  }


  applyFilter(filterValue: string) {
    this.dataSourceRecoveryPayment.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceRecoveryPayment.paginator) {
      this.dataSourceRecoveryPayment.paginator.firstPage();
    }
  }
  SavePayment(ObjTempData) {

    //debugger;
    //alert("SavePayment");
    this._Service.SaveTempPayment(ObjTempData).subscribe(data => {
      this.Message = data;
      swal(this.Message);
    });

  }

  FillForm(payMentStop, orderNo, orderdate, orderfromdate, ordertodate) {
    //alert(payMentStop + "-" + orderNo + "-" + orderdate + "-" + orderfromdate + "-" + ordertodate);
    this.ObjTempData.payMentStop = payMentStop;
    this.ObjTempData.orderNo = orderNo;
    this.ObjTempData.orderdate = '2019-03-03';//orderdate;
    this.ObjTempData.orderfromdate = orderfromdate;
    this.ObjTempData.ordertodate = ordertodate;
    this.btnSave = false;
  }
  Clear() {
    this.btnSave = true;
  }

  toggle() {
    this.show_dialog = !this.show_dialog;
  }

  UpdateRecord(Value) {
    alert(Value);
  }
}
