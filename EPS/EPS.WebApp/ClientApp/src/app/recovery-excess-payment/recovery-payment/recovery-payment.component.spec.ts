import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryPaymentComponent } from './recovery-payment.component';

describe('RecoveryPaymentComponent', () => {
  let component: RecoveryPaymentComponent;
  let fixture: ComponentFixture<RecoveryPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveryPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
