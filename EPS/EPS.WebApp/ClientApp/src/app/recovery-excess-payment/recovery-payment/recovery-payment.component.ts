import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSelect} from '@angular/material';
import { ExcessRecPaymentService } from '../../services/Recovery/excess-rec-payment.service';
import swal from 'sweetalert2';
import { FormGroup } from '@angular/forms';


export class DynamicGrid {
  title1: string;
  title2: string;
  title3: string;
} 

@Component({
  selector: 'app-recovery-payment',
  templateUrl: './recovery-payment.component.html',
  styleUrls: ['./recovery-payment.component.css']
})
export class RecoveryPaymentComponent implements OnInit {

  Message: any;
  validatingForm: FormGroup;
  ObjTempData: any = {};
  recpayment: any = {};
  deductionlist: any;
  recoveryTypeID: number;
  deductionTypeId: number;
  finYearlist: any;
  loanTypelist: any;
  finYear: any;
  accHeadlist: any;
  componentlist: any;
  accHeadId: number;
  excessAmt: number;
  installmentAmt: number;
  installmentNo: number;
  oddInstallmentNo: number;
  oddInstallmentAmt: number;
  paidInstallmentNo: number = 0;
  componentId: number;
  public temp: number;
  showhideDiv: boolean;
  showhideDropdown: boolean;
  showHideTable: boolean;
  username: string;
  ddoId: number;
  UserId: number;
  savebuttonstatus: boolean;
  btnUpdatetext: any;
  public callTypevar: number = 1;
  CreatedBy: any;
  dynamicArray: Array<DynamicGrid> = [];
  newDynamic: any = {};
  EmpCode: any;
  deletepopup: any;
  setDeletIDOnPopup: any;
  deletepopup1: any;
  setDeletIDOnPopup1: any;
  disableFlag = false;
  showhideButton = true;
  constructor(private _Service: ExcessRecPaymentService) { }

  displayedColumns: string[] = ['recoveryType', 'excessAmt', 'accountNo', 'sanctionOrderNo', 'sanctionOrdDate','status'];
  dataSource: any;

  displayedColumns2: string[] = ['recoveryType', 'excessAmt', 'accountNo', 'sanctionOrderNo', 'sanctionOrdDate','description','action'];
  dataSource2: any;

  dataSourcebif: any;
  displayedColumns3: string[] = ['componentName', 'bifAmt', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('recoveryForm') form: any;


  ngOnInit() {
    this.btnUpdatetext = "Save";
    this.username = sessionStorage.getItem('username');
    this.ddoId = Number(sessionStorage.getItem('ddoid'));
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.showhideDiv = false;
    this.savebuttonstatus = true;
    this.getFinancialYear();
    this.getRecoveryElement();
    this.getRecoveryLoanType();
    this.newDynamic = { componentId: "", bifAmt: ""};
    this.dynamicArray.push(this.newDynamic);
  }


  getFinancialYear(){
    this._Service.getFinancialYear().subscribe(data => {
      this.finYearlist = data;
    })
  }

  getRecoveryLoanType() {
    this._Service.getRecoveryLoanType().subscribe(data => {
      this.loanTypelist = data;
    })
  }

  getcommanMethod(value) {
    this.clearInput();
    this.EmpCode = value;
    this.getRecoveryComponent(value);
    this.getComponentAmtDetails(value);
    this.getExcessRecoveryDetails();
  }

  getRecoveryComponent(value) {
    this._Service.getRecoveryComponent(this.username, value).subscribe(data => {
      this.componentlist = data;
    })
  }

  getRecoveryElement() {
    this._Service.getRecoveryElement().subscribe(data => {
      this.deductionlist = data;
    })
  }


  getAccountHead(value) {
    debugger;
    this._Service.getAccountHead("","", value,"").subscribe(data => {
      this.accHeadlist = data;
    })
  }



  saveExcessRecovery() {
    this.recpayment.PermDdoId = this.ddoId;
    this.recpayment.CreatedBy = this.username;
    this.recpayment.EmpCode = this.EmpCode;
    this._Service.insertUpdateExRecovery(this.recpayment).subscribe(data => {
      this.Message = data;
      swal(this.Message);
      this.getExcessRecoveryDetails();
      this.clearInput();
    });
    
  }

  getExcessRecoveryDetails() {
    debugger;
    this.recpayment.EmpCode = this.EmpCode;
    this._Service.getExcessRecoveryDetail(this.recpayment.EmpCode).subscribe(result => {
      this.dataSource2 = new MatTableDataSource(result);
    })
  }

  editExcessPayment(recExcessId: any) {
    this.showhideButton = true;
    this._Service.editExcessRecoveryDetail(recExcessId).subscribe(result => {
      this.recpayment = result[0];
      this.recpayment.finYear = result[0].finYear
      this.getAccountHead(this.recpayment.finYear);
      if (result[0].recoveryTypeID == "99") {
        this.showhideDiv = true;
        this.showhideDropdown = false;
      }
      if (result[0].recoveryTypeID == "98") {
        this.showhideDiv = false;
        this.showhideDropdown = true;
      }
      this.btnUpdatetext = 'Update';
      this.disableFlag = true;
    })
  }


  viewExcessPayment(recExcessId: any) {
    this.showhideButton = false;
    this._Service.editExcessRecoveryDetail(recExcessId).subscribe(result => {
      this.recpayment = result[0];
      this.recpayment.finYear = result[0].finYear
      this.getAccountHead(this.recpayment.finYear);
      if (result[0].recoveryTypeID == "99") {
        this.showhideDiv = true;
        this.showhideDropdown = false;
      }
      if (result[0].recoveryTypeID == "98") {
        this.showhideDiv = false;
        this.showhideDropdown = true;
      }
      this.btnUpdatetext = 'Update';
      this.disableFlag = true;
    })
  }


  deleteExcessRecovery(recExcessId) {
    //debugger;
    this._Service.deleteExcessRecoveryData(recExcessId).subscribe(result => {
      if (result != undefined) {
        this.deletepopup1 = false;
        swal(result);
      }
      this.getExcessRecoveryDetails();
    })
    
  }

  setDeleteRecId(recExcessID) {
    this.setDeletIDOnPopup1 = recExcessID;
  }


  clearInput() {
    this.btnUpdatetext = 'Save';
    this.form.resetForm();
    this.disableFlag = false;
  }


  forwordDDOChecker() {

  }



  //Calculation Part
  calculateValue(value) {

    this.recpayment.installmentNo = Math.floor(((this.recpayment.excessAmt) / value));
    this.temp = Math.floor((this.recpayment.installmentAmt) * this.recpayment.oddInstallmentNo)
    this.recpayment.oddInstallmentAmt = (this.recpayment.excessAmt - (this.recpayment.installmentAmt * this.recpayment.installmentNo));
    this.recpayment.paidInstallmentNo = 0;
    if (this.recpayment.oddInstallmentAmt == 0) {
      this.recpayment.oddInstallmentNo = 0;
    }
    else {
      this.recpayment.oddInstallmentNo = 1;
    }
  }
  //End

  // Show hide element
  showHideElement(value: any) {
    if (value == "99") {
      this.showhideDiv = true;
      this.showhideDropdown = false;
    }
    if (value == "98") {
      this.showhideDiv = false;
      this.showhideDropdown = true;
    }
  }
  //End


  // Insert Update Component Amount

  saveRecCompAmount() {
    this.recpayment.CreatedBy = this.username
    this.recpayment.RecCmpId = 0;
    this.recpayment.EmpCode = this.EmpCode;
    this._Service.insertUpdateComponentAmt(this.recpayment).subscribe(data => {
      this.Message = data;
      //swal(this.Message);
      this.getComponentAmtDetails(this.recpayment.EmpCode);
      this.recpayment.bifAmt = "0";
    })
    
  }

  getComponentAmtDetails(value) {
    this.recpayment.EmpCode = this.EmpCode;
    this._Service.getComponentAmtDetails(this.recpayment.EmpCode,'1').subscribe(result => {
      this.dataSourcebif = new MatTableDataSource(result);
    })
  }

  deleteComponentAmt(recCmpId) {
    //debugger;
    this._Service.deleteComponentAmt(recCmpId).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        swal(result);
      }
      this.getComponentAmtDetails(this.recpayment.EmpCode);
    })
  }

  setDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }



  //End





}
