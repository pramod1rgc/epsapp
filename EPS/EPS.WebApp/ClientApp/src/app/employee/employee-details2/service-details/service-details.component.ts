import { Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../../services/master/master.service';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription } from 'rxjs';
import { ServiceDetailsService } from '../../../services/empdetails/service-details.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';
@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.css']
})
export class ServiceDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  serviceTypes: any = [];
  MaintainByOfc: any = [];
  objServiceDetails: any = {};
  objAllServiceDetails: any = {};
  @ViewChild('Service') Service: any;
  constructor(private commonMsg: CommonMsg , private objEmpGetCodeService: GetempcodeService,
    private master: MasterService, private objServiceDetailsService: ServiceDetailsService,
  ) {
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex === 5) { this.getAllServiceDetails(commonEmpCode.empcode); } });
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.getServiceType();
    this.getMaintainByOfc();
  }
  getServiceType() {
    this.master.getServiceType().subscribe(res => {
      this.serviceTypes = res;
    });
  }
  getMaintainByOfc() {
    this.objServiceDetailsService.getMaintainByOfc().subscribe(res => {
      this.MaintainByOfc = res;
    });
  }
  getAllServiceDetails(empCd: string) {
    this.getEmpCode = empCd;
    this.objServiceDetailsService.getAllServiceDetails(empCd, this.roleId).subscribe(res => {
      this.objServiceDetails = res;
      if (this.objServiceDetails.serviceType === '' || this.objServiceDetails.serviceType == null) {
        this.btnText = 'Save';
        this.VerifyFlag = 'E';
      } else {
        this.btnText = 'Update';
        this.VerifyFlag = 'U';
      }

    });
  }

  SaveUpdateServiceDetails() {
    if (this.getEmpCode === undefined) {
      alert('Please select the any employee ');
    } else {
      this.objServiceDetails.EmpCode = this.getEmpCode;
      this.objServiceDetails.VerifyFlag = this.VerifyFlag;
      this.objServiceDetails.UserId = this.UserId;
      this.objServiceDetailsService.SaveUpdateServiceDetails(this.objServiceDetails).subscribe(result => {
        // tslint:disable-next-line:radix
        if (parseInt(result) >= 1) {
          this.getAllServiceDetails(this.getEmpCode);
          this.resetServicedetailsForm();
          swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }
  }

  resetServicedetailsForm() {
    this.Service.resetForm();
  }
}
