import { Component, OnInit, ViewChild } from '@angular/core';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription, ReplaySubject, Observable, Subject } from 'rxjs';
import { MasterService } from '../../../services/master/master.service';
import { FormControl, NgModel } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { PayscaleService } from '../../../services/payscale.service';
import { PayDetailsService } from '../../../services/empdetails/pay-details.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';

// tslint:disable-next-line: class-name
interface gradePay {
  id: string;
  gradePay: string;
}
@Component({
  selector: 'app-pay-details',
  templateUrl: './pay-details.component.html',
  styleUrls: ['./pay-details.component.css']
})
export class PayDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  payCommission: any = [];
  OrganisationType: any = [];
  payLevelDetails: any = [];
  objPayDetails: any = {};
  objAllPayDetails: any = {};
  State: any = [];
  reqPayState: boolean;
  nonComputationalDues: any[];
  nonComputationalDeductionList: any[];
  showFieldBy7thPay: boolean;
  showDetailsBy7thPay: boolean;
  showDetailsBy6thPay: boolean;
  showFieldBy6thPay: boolean;
  showofficevehicle: boolean;
  payIndexDetails: any = [];
  gradepay: any[];
  entitledOffVeh: any[];
  duesSelect: NgModel;
  deduction: NgModel;
  SacleIDs: Observable<any>;
  duesAllCheck: boolean;
  @ViewChild('Pay') PayDetails: any;
  private _onDestroy = new Subject<void>();
  public gradeCtrl: FormControl = new FormControl();
  states: any = [];
  /** control for the MatSelect filter keyword */
  public gradepayFilterCtrl: FormControl = new FormControl();

  /** list of employee   filtered by search keyword */
  public filteredgradepay: ReplaySubject<gradePay[]> = new ReplaySubject<gradePay[]>(1);
  maxDate = "1920-01-01";  
  // tslint:disable-next-line: max-line-length
  constructor(private commonMsg: CommonMsg, private objEmpGetCodeService: GetempcodeService, private objPayScaleServive: PayscaleService,
    private objPayDetailsService: PayDetailsService, private master: MasterService, ) {
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex === 4) { this.getPayDetails(commonEmpCode.empcode); } });
    this.reqPayState = false;
    this.showFieldBy7thPay = true;
    this.showFieldBy6thPay = false;
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.reqPayState = false;
    this.showFieldBy7thPay = true;
    this.showofficevehicle = false;
    this.showFieldBy6thPay = false;
    this.duesAllCheck = true;
    this.getGradePay();
    this.gradepayFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGradePay();
      });
    this.getPayCommission();
    this.getOrganisationType();
    this.getNonComputationalDues();
    this.getNonComputationalDeductions();
    this.getPayLevel();
    this.getEntitledOffVeh();
  }

  private filterGradePay() {

    if (!this.gradepay) {
      return;
    }
    // get the search keyword
    let search = this.gradepayFilterCtrl.value;
    if (!search) {
      this.filteredgradepay.next(this.gradepay.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the employee
    this.filteredgradepay.next(
      this.gradepay.filter(gradepay => gradepay.gradePay.toString().toLowerCase().indexOf(search) > -1)
    );
  }
  equalsDues(objOne, objTwo) {

    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.duesId === objTwo.duesId;
    }
  }
  equalsDeduction(objOne, objTwo) {

    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.deductionId === objTwo.deductionId;
    }
  }
  getPayCommission() {
    this.objPayScaleServive.BindCommissionCode().subscribe(res => {
      this.payCommission = res;
    });
  }
  getOrganisationType() {

    const EmpCode = this.getEmpCode;

    this.objPayDetailsService.getOrganisationType(EmpCode).subscribe(res => {
      this.OrganisationType = res;
    });
    this.objPayDetails.organisation_Type = '';
    this.objPayDetails.stateID = '';
    this.reqPayState = false;
  }

  GetAllSatate(organisationnValue: any) {

    if (organisationnValue === '1') {
      this.getState();
      this.reqPayState = true;
    } else {
      this.reqPayState = false;
    }
  }
  getState() {
    this.master.getState().subscribe(res => {
      this.states = res;
    });
  }
  getNonComputationalDues() {
    this.objPayDetailsService.getNonComputationalDues().subscribe(res => {

      this.nonComputationalDues = res;
      // debugger;
    });

  }
  getNonComputationalDeductions() {
    this.objPayDetailsService.getNonComputationalDeductions().subscribe(res => {
      this.nonComputationalDeductionList = res;
    });
  }

  async getPayDetails(empCd: string) {
    this.getEmpCode = empCd;
    this.resetPayDetailsForm();
    if (empCd !== undefined && empCd !== null) {
    }
    if (!Array.isArray(empCd)) {

      await this.objPayDetailsService.getPayDetails(empCd, this.roleId).subscribe(res => {
        // tslint:disable-next-line: no-debugger
        this.objAllPayDetails.gradepay = res.gradePay;
        this.objAllPayDetails.PayScalePscDscr = res.payScalePscDscr;
        this.objAllPayDetails.payInPb = res.payInPb;
        this.objAllPayDetails.basicPay = res.basicPay;
        this.objAllPayDetails.basicPayDT = res.basicPayDT;
        this.objAllPayDetails.incrpayDT = res.incrpayDT;
        if (res.payComm === '' || res.payComm == null) {
          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }
        if (res.payComm === 16) {
          this.getPayIndex(res.payLevel);
          this.showFieldBy6thPay = false;
          this.showFieldBy7thPay = true;
          this.showDetailsBy7thPay = true;
          this.showDetailsBy6thPay = false;


        } else if (res.payComm === 12) {
          const pscScaleCd = res.pscScaleCd;
          this.getPayScale(res.gradePay);
          this.objPayDetails.pscScaleCd = pscScaleCd;

          this.showofficevehicle = false;
          this.showFieldBy7thPay = false;
          this.showFieldBy6thPay = true;
          this.showDetailsBy7thPay = false;
          this.showDetailsBy6thPay = true;
        }
        setTimeout(() => {
          this.objPayDetails = res;
        }, 1000);
      });
    } else {
      this.resetPayDetailsForm();

    }
    this.getOrganisationType();

  }

  getInfoByPayCommission(paycommission: number) {

    // 16 for 7th pay commission
    // 12 for 6th pay commission
      this.objPayDetails.basicPay = '';
    if (paycommission === 16) {
      this.objPayDetails.payLevel = '';
      this.objPayDetails.payIndex = '';
      this.objPayDetails.gradePay = '';
      this.objPayDetails.pscScaleCd = '';
      this.objPayDetails.payInPb = '';
      this.objPayDetails.basicPay = '';
      this.showFieldBy6thPay = false;
      this.showFieldBy7thPay = true;

    } else if (paycommission === 12) {
      this.objPayDetails.payLevel = '';
      this.objPayDetails.payIndex = '';
      this.objPayDetails.gradePay = '';
      this.objPayDetails.pscScaleCd = '';
      this.objPayDetails.payInPb = '';
      this.objPayDetails.basicPay = '';
      this.showFieldBy7thPay = false;
      this.showFieldBy6thPay = true;
      this.showofficevehicle = false;
      this.objPayDetails.entitledOffVehID = '';
    }
  }

  getPayLevel() {
    this.objPayDetailsService.getPayLevel().subscribe(res => {

      this.payLevelDetails = res;
    });

  }
  getEntitledOffVeh() {
    this.objPayDetailsService.getEntitledOffVeh().subscribe(res => {

      this.entitledOffVeh = res;
    });

  }
  getPayIndex(PayLevel: any) {
    this.objPayDetails.payIndex = '';
    this.objPayDetails.basicPay = '';
    if (PayLevel > 13) {
      this.showofficevehicle = true;
    } else {
      this.showofficevehicle = false;
      this.objPayDetails.entitledOffVehID = '';
    }
    this.objPayDetailsService.getPayIndex(PayLevel).subscribe(res => {
      this.payIndexDetails = res;
    });

  }

  // tslint:disable-next-line: no-shadowed-variable
  async getPayScale(gradePay: number) {

    this.objPayDetails.pscScaleCd = '';

    this.SacleIDs = await this.objPayDetailsService.getPayScale(gradePay);

  }
  resetPayInPbAndBasicPay() {
    this.objPayDetails.payInPb = '';
    this.objPayDetails.basicPay = '';
  }

  getGradePay() {

    this.objPayDetailsService.getGradePay().subscribe(res => {

      this.gradepay = res;
      // set initial selection
      this.gradeCtrl.setValue(this.gradepay);

      // load the initial employee list
      this.filteredgradepay.next(this.gradepay);

    });
  }

  getBasicDetails(PayLevel: string, PayIndex: number) {

    this.objPayDetailsService.getBasicDetails(PayLevel, PayIndex).subscribe(res => {

      this.objPayDetails.basicPay = res[0]['basicPay'];
    });

  }

  SaveUpdatePayDetails() {

    const a = this.objPayDetails;
    // debugger;
    if (this.getEmpCode === undefined) {
      // this.reqEmpcode = true;
      alert('Please Select Employee');

    } else {

      if (this.objPayDetails.payComm === '16') {
        this.objPayDetails.gradePay = 0;
        this.objPayDetails.pscScaleCd = 0;
        this.objPayDetails.payInPb = 0;

      } else if (this.objPayDetails.payComm === '12') {
        this.objPayDetails.payLevel = '';
        this.objPayDetails.payIndex = '';
      }
      this.objPayDetails.EmpCode = this.getEmpCode;
      this.objPayDetails.VerifyFlag = this.VerifyFlag;
      this.objPayDetails.UserId = this.UserId;
      this.objPayDetailsService.SaveUpdatePayDetails(this.objPayDetails).subscribe(result => {

        // tslint:disable-next-line:radix
        if (parseInt(result) >= 1) {
          this.getPayDetails(this.getEmpCode);
          this.getPayIndex(this.objPayDetails.payLevel);
          swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }

    // this.resetPayDetailsForm();
  }


  selectAll(checkAll, select: NgModel, values) {
    // this.toCheck = !this.toCheck;


    if (checkAll) {
      select.update.emit(values);
    } else {
      select.update.emit([]);
    }
  }
  CalculateBasicPay() {
    // tslint:disable-next-line: radix
    this.objPayDetails.basicPay = parseInt(this.objPayDetails.payInPb) + parseInt(this.objPayDetails.gradePay);
  }

  resetPayDetailsForm() {
    this.PayDetails.resetForm();
    this.objPayDetails = {};
    this.btnText = 'Save';

  }
}
