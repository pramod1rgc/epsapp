import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CghsdetailsService } from '../../../services/empdetails/cghsdetails.service';
import { Subscription } from 'rxjs';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';
@Component({
  selector: 'app-cghs-details',
  templateUrl: './cghs-details.component.html',
  styleUrls: ['./cghs-details.component.css']
})
export class CghsDetailsComponent implements OnInit {
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any ;
  UserId: any;
  roleId: any;
  getEmpCode: string;
  objCGHSDetails: any = {};
  objAllCGHSDetails: any = {};
  @ViewChild('CGHS') CGHSForm: NgForm; // its a template reference variable
  showDeduction: boolean;
  showempCghsNoDetails: boolean;
  showcghsDescriptionDetails: boolean;
  showcghsDescription: boolean;
  constructor(private commonMsg: CommonMsg,  private objCghsdetailsService: CghsdetailsService,
     private objEmpGetCodeService: GetempcodeService ){
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex  === 3) {this.getCGHSDetails(commonEmpCode.empcode); } });
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.showDeduction = true;
    this.showcghsDescriptionDetails = false;
    this.showempCghsNoDetails = true;
    this.showcghsDescription = false;
  }

  SaveUpdateCGHSDetails() {
    // debugger
    if (this.getEmpCode === undefined) {
     alert('Please Select Employee');
    } else {
      this.objCGHSDetails.EmpCd = this.getEmpCode;
      this.objCGHSDetails.VerifyFlag = this.VerifyFlag;
      this.objCGHSDetails.UserId = this.UserId;

      this.objCghsdetailsService.SaveUpdateCGHSDetails(this.objCGHSDetails).subscribe(result => {
        // tslint:disable-next-line: radix
        if ( parseInt(result) >= 1) {
          this.getCGHSDetails(this.getEmpCode);
          swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }
  }
  getCGHSDetails(empCd: string) {
    this.getEmpCode = empCd;
    this.objAllCGHSDetails = {};
     if (!Array.isArray(empCd)) {
      this.objCghsdetailsService.getCGHSDetails(empCd , this.roleId).subscribe(res => {
        this.objCGHSDetails = res;

        if (this.objCGHSDetails.empCghsFlag != null) {
          if (this.objCGHSDetails.empCghsNo != null) {
            this.showDeduction = true;
            this.showcghsDescription = false;
            this.showempCghsNoDetails = true;
            this.showcghsDescriptionDetails = false;
          } else {
            if (this.objCGHSDetails.empCghsFlag === 'N') {
              this.showDeduction = false;
              this.showcghsDescription = false;
              this.showcghsDescriptionDetails = false;
              this.showempCghsNoDetails = false;
            } else {
              this.showDeduction = false;
              this.showcghsDescription = true;
              this.showcghsDescriptionDetails = true;
              this.showempCghsNoDetails = false;
            }

          }
        }
        // debugger;

        if (this.objCGHSDetails.cgisDeduct === 'Y') {
          this.objAllCGHSDetails.cgisDeduct = 'CGHS';
        } else if (this.objCGHSDetails.cgisDeduct !== null) {

          this.objAllCGHSDetails.cgisDeduct = 'MIS State / Other';
        }

        if (this.objCGHSDetails.empCghsFlag === 'Y') {
          this.objAllCGHSDetails.empCghsFlag = 'Yes';
        } else if (this.objCGHSDetails.cgisDeduct !== null) {
          this.objAllCGHSDetails.empCghsFlag = 'No';
        }

        this.objAllCGHSDetails.empCghsNo = this.objCGHSDetails.empCghsNo;
        this.objAllCGHSDetails.cghsDescription = this.objCGHSDetails.cghsDescription;

        if (this.objCGHSDetails.cgisDeduct === '' || this.objCGHSDetails.cgisDeduct == null) {

          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }
      });
    } else {
      this.resetCGGDetailsForm();
    }
  }
  resetCGGDetailsForm() {

    this.CGHSForm.resetForm();
  }
  showAndHideMIS(deduction: any) {
    // debugger;
    if (deduction === 'Y') {
      this.objCGHSDetails.cghsDescription = null;
      if (this.objCGHSDetails.empCghsFlag === 'Y') {
        this.showDeduction = true;
      } else {
        if (this.objCGHSDetails.empCghsFlag != null) {
          this.showDeduction = false;
        }

      }
      this.showcghsDescription = false;
    } else {
      this.objCGHSDetails.empCghsFlag = 'Y';
      this.objCGHSDetails.empCghsNo = null;
      this.showDeduction = false;
      this.showcghsDescription = true;
    }
  }
  showAndHideMISbyFlag(flag: any) {
    // debugger;

    if (flag === 'N') {
      if (this.objCGHSDetails.cgisDeduct === 'Y') {

        this.showDeduction = false;
        this.objCGHSDetails.empCghsNo = null;
      }

    } else {
      if (this.objCGHSDetails.cgisDeduct === 'Y') {

        this.showDeduction = true;
      }
    }
    if (flag === 'N') {
      if (this.objCGHSDetails.cgisDeduct === 'O') {

        this.showcghsDescription = false;
        this.objCGHSDetails.cghsDescription = null;
      }

    } else {
      if (this.objCGHSDetails.cgisDeduct === 'O') {

        this.showcghsDescription = true;
      }
    }
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


}
