import { Component, OnInit, ViewChild } from '@angular/core';
import {PostingDetailsService} from '../../../services/empdetails/posting-details.service';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription } from 'rxjs';
import { MasterService } from '../../../services/master/master.service';
import { CommonMsg } from '../../../global/common-msg'; 
import swal from 'sweetalert2';

@Component({
  selector: 'app-posting-details',
  templateUrl: './posting-details.component.html',
  styleUrls: ['./posting-details.component.css']
})
export class PostingDetailsComponent implements OnInit {
  maxDate = new Date();
  btnText = 'Save';
  VerifyFlag: any ;
  HRACity: any = [];
  TACity: any = [];
  designation: any = [];
  joiningModes: any = [];
  objPostingDetails: any = {};
  objAllPostingDetails: any = {};
  @ViewChild('Posting') Posting: any;
  subscription: Subscription;
  UserId: any;
  roleId: any;
  commonEmpCode: any = {};
  constructor(private objPostingDetailsService: PostingDetailsService,  private commonMsg: CommonMsg,
    private objEmpGetCodeService: GetempcodeService  , private master: MasterService
   ) {
     // this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(commonEmpCode => { this.commonEmpCode = commonEmpCode; });
    // tslint:disable-next-line: max-line-length
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      commonEmpCode => { if (commonEmpCode.selectedIndex  === 1) {this.GetAllPostingDetails(commonEmpCode.empcode); } });
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
     this.GetHRACity();
    this.GetTACity();
    this.GetAllDesignation();
    this.getJoiningMode();
  }
  GetHRACity() {
    this.objPostingDetailsService.GetHRACity().subscribe(res => {
      this.HRACity = res;
    });
  }

  GetTACity() {
    this.objPostingDetailsService.GetTACity().subscribe(res => {
      this.TACity = res;
    });
  }

  GetAllDesignation() {
    this.objPostingDetailsService.GetAllDesignation().subscribe(res => {

      this.designation = res;
    });
  }

  getJoiningMode() {
    this.master.getJoiningMode().subscribe(res => {
      this.joiningModes = res;
    });
  }

  GetAllPostingDetails(empCd: string) {
    this.commonEmpCode = empCd;
 if (!Array.isArray(empCd)) {
      this.objPostingDetailsService.GetAllPostingDetails(empCd, this.roleId).subscribe(res => {
        this.objPostingDetails = res;
        this.objAllPostingDetails.name = res.name;
        this.objAllPostingDetails.designation = res.designation;
        this.objAllPostingDetails.empCurdesigDt1 = res.empCurdesigDt;
        this.objAllPostingDetails.empCuroffDt1 = res.empCuroffDt;
        this.objAllPostingDetails.joiningMode = res.joiningMode;
        this.objAllPostingDetails.tACityName = res.taCityName;
        this.objAllPostingDetails.hraCityName = res.hraCityName;
        this.objAllPostingDetails.joiningMode1 = res.joiningMode;
        if (this.objPostingDetails.designation === '' || this.objPostingDetails.designation == null) {
          // debugger;
          this.btnText = 'Save';
          this.VerifyFlag = 'E' ;
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U' ;
        }

      });
    } else {
      this.resetPostingdetailsForm();
    }
  }
  //  SaveUpdatePostingDetails


  SaveUpdatePostingDetails() {
    debugger;
    if (this.commonEmpCode === undefined || this.commonEmpCode === null) {
     // this.reqEmpcode = true;
     alert('Please Select Employee');

    } else {
      this.objPostingDetails.empCd = this.commonEmpCode;
      this.objPostingDetails.userID = this.UserId ;
      this.objPostingDetails.VerifyFlag = this.VerifyFlag ;
      this.objPostingDetailsService.SaveUpdatePostingDetails(this.objPostingDetails).subscribe(result => {
        console.log(result);
        // tslint:disable-next-line:radix
        if (parseInt(result) >= 1) {
          this.GetAllPostingDetails(this.objPostingDetails.empCd );
          this.resetPostingdetailsForm();
           swal(this.commonMsg.updateMsg);       
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }
  }

  resetPostingdetailsForm() {
    this.Posting.resetForm();
    this.objPostingDetails = {};
  }
}
