import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { NgForm } from '@angular/forms';
import { PayscaleService } from '../../../services/payscale.service';
import { MasterService } from '../../../services/master/master.service';
import { CGEGISService } from '../../../services/empdetails/cgegis.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';
@Component({
  selector: 'app-cgegis-details',
  templateUrl: './cgegis-details.component.html',
  styleUrls: ['./cgegis-details.component.css']
})
export class CgegisDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  showDdlCategory: boolean;
  showDdlCategoryState = false;
  showDdlCategoryText = false;

  group: Observable<any>;
  insuranceApplicable: Observable<any>;
  CGEGISCategory: Observable<any>;
  @ViewChild('CGEGIS') cGEGISDetailsForm: NgForm; // its a template reference variable

  objCGEGISDetails: any = {};
  objAllCGEGISDetails: any = {};
  states: any = [];
  btnText = 'Save';
  VerifyFlag: any ;
  UserId: any;
  roleId: any;
  maxDate = "1920-01-01";
  // tslint:disable-next-line: max-line-length
  constructor(private commonMsg: CommonMsg, private objEmpGetCodeService: GetempcodeService ,  private objPayScaleServive: PayscaleService, private master: MasterService
    , private objCGEGISService: CGEGISService, ) {
     // this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(commonEmpCode => { this.commonEmpCode = commonEmpCode; });
    // tslint:disable-next-line: max-line-length
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex  === 2) {this.getCGEGISDetails(commonEmpCode.empcode); this.getInsuranceApplicable(commonEmpCode.empcode); } });
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.getgroup();
    this.showDdlCategory = true;
  }

  getgroup() {
    this.group = this.objPayScaleServive.BindPayScaleGroup();
  }
  getInsuranceApplicable(empCd: string) {
   this.insuranceApplicable = this.objCGEGISService.getInsuranceApplicable(empCd);
  }

  getCGEGISCategory(insuranceApplicableId: any) {
    debugger;
   // this.objCGEGISDetails.payGisApplicable = '';
    if (insuranceApplicableId === 229 || insuranceApplicableId === 1007) {
      this.showDdlCategory = true;
      this.showDdlCategoryState = false;
      this.showDdlCategoryText = false;
      this.CGEGISCategory = this.objCGEGISService.getCGEGISCategory(insuranceApplicableId, this.getEmpCode);
    } else if (insuranceApplicableId === 228) {
      this.getState();
      this.showDdlCategoryState = true;
      this.showDdlCategory = false;
      this.showDdlCategoryText = false;
    } else {
      this.showDdlCategoryText = true;
      this.showDdlCategory = false;
      this.showDdlCategoryState = false;
    }

  }
  getState() {
    this.master.getState().subscribe(res => {
      this.states = res;
    });
  }

  resetCGEGISDetailsForm() {
    // debugger;
    this.cGEGISDetailsForm.resetForm();

  }
  SaveUpdateCGEGISDetails() {

    if (this.getEmpCode === undefined) {
     alert('Please Select Employee');
    } else {
      this.objCGEGISDetails.EmpCode = this.getEmpCode;
      this.objCGEGISDetails.VerifyFlag = this.VerifyFlag;
      this.objCGEGISDetails.UserId = this.UserId;

      this.objCGEGISService.SaveUpdateCGEGISDetails(this.objCGEGISDetails).subscribe(result => {
         // tslint:disable-next-line: radix
         if (parseInt(result) >= 1) {
          this.getCGEGISDetails(this.getEmpCode);
           swal(this.commonMsg.updateMsg);             
         } else {
           swal(this.commonMsg.updateFailedMsg);    
        }
      });
    }
  }
  getCGEGISDetails(empCd: string) {
    this.getEmpCode = empCd;
    if (!Array.isArray(empCd)) {
      this.objCGEGISService.getCGEGISDetails(empCd, this.roleId).subscribe(res => {
        this.objCGEGISDetails = res;
        this.objAllCGEGISDetails.gisMembershipDT = this.objCGEGISDetails.gisMembershipDT;
        this.getCGEGISCategory(this.objCGEGISDetails.gis_deduction);
          if (this.objAllCGEGISDetails.gisMembershipDT === '' || this.objAllCGEGISDetails.gisMembershipDT == null) {

          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }

      });
      this.getInsuranceApplicable(empCd);

    } else {
      this.resetCGEGISDetailsForm();
    }
  }

}
