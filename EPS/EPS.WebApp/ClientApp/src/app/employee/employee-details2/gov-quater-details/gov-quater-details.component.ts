import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription, Observable } from 'rxjs';
import { QuaterDetailsService } from '../../../services/empdetails/quater-details.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../../global/common-msg';


@Component({
  selector: 'app-gov-quater-details',
  templateUrl: './gov-quater-details.component.html',
  styleUrls: ['./gov-quater-details.component.css'],
  providers: [CommonMsg]
})
export class GovQuaterDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  deletepopup: any;
  setDeletIDOnPopup: any;
  empCd: string;
  AllotcateQuater: boolean;
  _listQuarterOwnedby: Observable<any>;
  _listAllottedTo: Observable<any>;
  _listRentStatus: Observable<any>;
  _listQuarterType: Observable<any>;
  _listCustodian: Observable<any>;
  _listGPRACityLocation: Observable<any>;
  objQuaterDetails: any = {};
  objVacateQtrDtls: any = {};
  _saveDisabledflag = false;
  disbleQuaterflag = false;
  isTableHasData = true;
  quaterdataSource: any;
  quaterdisplayedColumns: string[] = ['Allotedto', 'Date', 'Action'];
  @ViewChild('quaterpaginator') quaterpaginator: MatPaginator;
  @ViewChild('quatersort') quatersort: MatSort;
  @ViewChild('Quarter') quaterForm: any;
  maxDate = "1950-01-01";
  constructor(private commonMsg: CommonMsg, private objEmpGetCodeService: GetempcodeService ,
     private objQuaterDetailsService: QuaterDetailsService,
  ) {
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex === 8) { this.QuarterAllDetails(commonEmpCode.empcode); } });
  }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.AllotcateQuater = true;
    this.getQuarterOwnedby();
    this.getAllottedTo();
    this.getRentStatus();
    this.getQuarterType();
    this.objQuaterDetails.susBookingYn = 'N';
    this.GetGPRACityLocation();
    this.objQuaterDetails.allocAndVac = 'allocate';
    this.disbleQuaterflag = false;
  }

  showAllocateAndVacate(AllotcateQuater: boolean): void {
    // debugger;
    if (this.objQuaterDetails.msEmpAccmID === undefined || null) {
      if (AllotcateQuater === false) {
        this._saveDisabledflag = false;
      } else {
        this._saveDisabledflag = false;
      }
    }
    this.AllotcateQuater = AllotcateQuater;
    this.objQuaterDetails.susBookingYn = 'N';
  }
  getQuarterOwnedby() {

    this._listQuarterOwnedby = this.objQuaterDetailsService.getQuarterOwnedby();
  }
  getAllottedTo() {

    this._listAllottedTo = this.objQuaterDetailsService.getAllottedTo();
  }
  getRentStatus() {

    this._listRentStatus = this.objQuaterDetailsService.getRentStatus();
  }
  getQuarterType() {

    this._listQuarterType = this.objQuaterDetailsService.getQuarterType();
  }
  GetCustodian(QrtrOwnedBy: number) {
    this._listCustodian = this.objQuaterDetailsService.GetCustodian(QrtrOwnedBy);

  }
  GetGPRACityLocation() {

    this._listGPRACityLocation = this.objQuaterDetailsService.GetGPRACityLocation();
  }

  SaveUpdateQuarterDetails() {
    debugger
    if (this.getEmpCode === undefined) {
      swal('Please Select employee');

    } else {
      this.objQuaterDetails.EmpCd = this.getEmpCode;
     if (this.objQuaterDetails.msEmpAccmID === 0 || this.objQuaterDetails.msEmpAccmID === undefined) {
        this.objQuaterDetails.VerifyFlag = 'E';
      } else {
        this.objQuaterDetails.VerifyFlag = 'U';
      }
      this.objQuaterDetails.UserId = this.UserId;
      if (this.objQuaterDetails.allocAndVac === 'allocate') {
        // this.objQuaterDetails.vacatingLetterNo =null;
        // this.objQuaterDetails.vacatingLetterDt =null;
        // this.objQuaterDetails.accmVacateDt =null;
        this.objQuaterDetailsService.SaveUpdateQuarterDetails(this.objQuaterDetails).subscribe(result => {
          // tslint:disable-next-line: radix
          if (parseInt(result) >= 1) {
            this.QuarterAllDetails(this.getEmpCode);
            if (this.objQuaterDetails.msEmpAccmID==0)
              swal(this.commonMsg.saveMsg);
            else
              swal(this.commonMsg.updateMsg);            
            
          } else {
            swal(this.commonMsg.saveFailedMsg);
          }
          this.resetQuaterDdetailsForm();
        });
        
      } else {
        this.objVacateQtrDtls.allocAndVac = 'vacate';
        this.objVacateQtrDtls.MSEmpAccmID = this.objQuaterDetails.msEmpAccmID;
        this.objVacateQtrDtls.EmpCd = this.getEmpCode;
        this.objVacateQtrDtls.UserId = this.UserId;
        this.objQuaterDetailsService.SaveUpdateQuarterDetails(this.objVacateQtrDtls).subscribe(result => {
          // tslint:disable-next-line: radix
          if (parseInt(result) >= 1) {
            this.QuarterAllDetails(this.getEmpCode);
            if (this.objQuaterDetails.msEmpAccmID == 0)
              swal(this.commonMsg.saveMsg);
            else
              swal(this.commonMsg.updateMsg);                      
          } else {
            swal(this.commonMsg.saveFailedMsg);
          }
          this.resetQuaterDdetailsForm();  
        });
        
      }

    }
  }

  QuarterAllDetails(empCd: string) {
  this.getEmpCode = empCd;
    this.resetQuaterDdetailsForm();
     if (this.getEmpCode !== '' && this.getEmpCode !== undefined) {
      this.objQuaterDetailsService.QuarterAllDetails(empCd, this.roleId).subscribe(res => {
     this.quaterdataSource = new MatTableDataSource(res);
      this.quaterdataSource.paginator = this.quaterpaginator;
        this.quaterdataSource.sort = this.quatersort;
      });
    } else {
      this.resetQuaterDdetailsForm();
    }
  }


  QuaterDetailsFilter(filterValue: string) {
    // debugger;
    this.quaterdataSource.filter = filterValue.trim().toLowerCase();
    if (this.quaterdataSource.quaterpaginator) {
      this.quaterdataSource.quaterpaginator.firstPage();
    }
    if (this.quaterdataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }

  QuarterDetails(empCd: string, flag: any, mSEmpAccmID: number) {
    debugger;
    if (this.getEmpCode !== '' && this.getEmpCode !== undefined) {
      this.showAllocateAndVacate(true);
      this.objQuaterDetailsService.QuarterDetails(empCd, mSEmpAccmID).subscribe(res => {
        this.objQuaterDetails = res;
        // this.objVacateQtrDtls  =this.objQuaterDetails
        this.objVacateQtrDtls.accmQrtrAllot = res.accmQrtrAllot;
        this.objVacateQtrDtls.allotLetterDt = res.allotLetterDt;
        this.objVacateQtrDtls.accmOccupDt = res.accmOccupDt;
        this.objVacateQtrDtls.qrtrAddress1 = res.qrtrAddress1;
        this.objVacateQtrDtls.custId = res.custId;
        this.objVacateQtrDtls.accmQrtrType = res.accmQrtrType;
        this.objVacateQtrDtls.accmRentStat = res.accmRentStat;
        this.objVacateQtrDtls.accmRent = res.accmRent;
        this.objVacateQtrDtls.accmWaterCharge = res.accmWaterCharge;
        this.objVacateQtrDtls.accmAddRent1 = res.accmAddRent1;
        this.objVacateQtrDtls.accmGarageRent = res.accmGarageRent;
        this.objVacateQtrDtls.accmServCharge = res.accmServCharge;
        this.objVacateQtrDtls.vacatingLetterNo = res.vacatingLetterNo;
        this.objVacateQtrDtls.vacatingLetterDt = res.vacatingLetterDt;
        this.objVacateQtrDtls.accmVacateDt = res.accmVacateDt;
      if (this.objQuaterDetails.accmQrtrAllot === '' || this.objQuaterDetails.accmQrtrAllot == null) {
          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }
        if (this.objQuaterDetails.accmQrtrAllot == 0) {
          debugger;
          this.objQuaterDetails.allocAndVac = 'vacate';
          this.showAllocateAndVacate(false)
        }
        else {
          this.objQuaterDetails.allocAndVac = 'allocate';
          this.showAllocateAndVacate(true)
        }
        this.GetCustodian(this.objQuaterDetails.qrtrOwnedBy);
        if (flag === 1) {
          this.disbleQuaterflag = false;
          this._saveDisabledflag = false;
        } else {
          this.disbleQuaterflag = true;
          this._saveDisabledflag = true;
        }

      });
    } else {
      this.disbleQuaterflag = false;
      this.resetQuaterDdetailsForm();
    }
  }


  resetQuaterDdetailsForm() {
    this.btnText = 'Save';
    this.quaterForm.resetForm();

    this.objVacateQtrDtls.msEmpAccmID = undefined;
    this.objQuaterDetails.msEmpAccmID = undefined;
    // this.objQuaterDetails=''
    // this.objVacateQtrDtls='';
    this.objQuaterDetails = {};
   // this.objQuaterDetails.allocAndVac ="allocate";
  }


  DeleteQuarterDetails() {
     this.objQuaterDetailsService.DeleteQuarterDetails(this.empCd, this.setDeletIDOnPopup).subscribe(result1 => {
      // tslint:disable-next-line:radix
      if (result1 >= 1) {
        this.QuarterAllDetails(this.empCd);
        this.resetQuaterDdetailsForm();
        this.deletepopup = !this.deletepopup;
        swal(this.commonMsg.deleteMsg);
      } else {
        swal(this.commonMsg.deleteFailedMsg);
      }
    });
  }

  SetDeleteId(Empcode: any , DeletIDOnPopup: any ) {
    this.setDeletIDOnPopup = DeletIDOnPopup;
    this.empCd = Empcode;
  }


  // Validation 

  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  
  charaterWithNumeric(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46  || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }
    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
