import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { MasterService } from '../../services/master/master.service';
import { EmployeeTypeMaster, MasterModel} from '../../model/masterModel';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { EmpdetailsService } from '../../services/empdetails/empdetails.service';
import { saveAs } from 'file-saver';
import 'rxjs/Rx';
import { PostingDetailsService } from '../../services/empdetails/posting-details.service';
import { DatePipe } from '@angular/common';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Rx';
import { designationCode } from '../../model/DeptationModel/depuationin-model';
import { takeUntil, take, debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-employee-details1',
  templateUrl: './employee-details1.component.html',
  styleUrls: ['./employee-details1.component.css'],
  providers: [CommonMsg]
})

export class EmployeeDetails1Component implements OnInit {
  salutations: MasterModel;
  employeeTypes: EmployeeTypeMaster;
  joiningModes: any;
  joiningCategory: any;
  employeeSubTypes: any=[];
  dataSource: any=[];
  getEmpById: any = {};
  deputationTypes: any[];
  serviceTypes: any;
  disbleflag = false;
  disbleflagdownload = false;
  asyncResult : any;
  buttonText: string = 'Save';
  showAndHideDepuType: boolean;
  displayedColumns: string[] = ['name', 'empDOB', 'empJoinDt', 'EmpVerifFlag', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('empDetail') form: any;
  disableForwardflag= true;
  maxDate = new Date();
  ageLimit = new Date();
  msg: string;
  userName: string;
  ddoId: number;
  userId: number;
  roleId: number;
  btnchecker = false;
  showRemark: boolean;
  deletepopup: boolean;
  popUpMsg: string
  deleteShow = true;
  deletepopup1: boolean;
  designations: any = [];
  pipe: DatePipe;
  notFound = true;
  isLoading = false;
  @ViewChild('file') file: ElementRef;
  public designationCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesignation: Subject<designationCode[]> = new Subject<designationCode[]>();
  private _onDestroy = new Subject<void>();
  
  constructor(private master: MasterService, 
    private empDetails: EmpdetailsService,
    private designation: PostingDetailsService,
    private commonMsg: CommonMsg
  ) { }

 
  ngOnInit() {
    this.ageLimit.setFullYear(this.maxDate.getFullYear() - 18);
    this.userName = sessionStorage.getItem('username');
    this.ddoId = Number(sessionStorage.getItem('ddoid'));
    this.getEmpById.userId = Number(sessionStorage.getItem('UserID'));
    this.getEmpById.empOffId=sessionStorage.getItem('controllerID');
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    if (this.roleId === 8) {
      this.disableForwardflag = false;
      this.disbleflag = true;
    }
    this.getEmpById.isDeput = false;
    this.showAndHideDepuType = false;
    this.getSalutation();
    this.getMakerEmpList();
    this.getServiceType(this.getEmpById.isDeput);
    this.getDesignation();
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });
  }
  
  getSalutation() {
    this.master.getSalutation().subscribe(res => {
      this.salutations = res;
    })
  }

  deputChange(val: boolean) {
    this.getEmpById.serviceType = '';
    if (val == true) {
      this.showAndHideDepuType = true;
      this.getEmpById.isDeput = true;
      this.getServiceType(this.getEmpById.isDeput);
      }
    else {
      this.showAndHideDepuType = false;
      this.getEmpById.isDeput = false;
      this.getServiceType(this.getEmpById.isDeput);
    }    
  }

  getDeputationType() {
    this.master.getDeputationType(this.getEmpById.serviceType).subscribe(res => {
        this.deputationTypes = res;      
    })
  }
  getServiceType(isDeput) {
    this.getEmpById.deputTypeID = '';
    this.master.getServiceType(isDeput).subscribe(res => {
      this.serviceTypes = res;  
    });
  }

  getEmpType() {
    this.getEmpById.empSubType = '';
    this.master.getEmployeeType(this.getEmpById.serviceType, this.getEmpById.deputTypeID).subscribe(res => {
      this.employeeTypes = res;
    })
  }

  getEmpServicetype() {
    this.getEmpById.emp_type = '';
    if(this.getEmpById.isDeput == false) {
      this.getEmpType();
    }
    else {
      this.master.getDeputationType(this.getEmpById.serviceType).subscribe(res => {
        this.deputationTypes = res;       
      })
    }
  }


  getEmployeeSubType() {
    this.getEmpById.empApptType = '';
    this.master.getEmployeeSubType(this.getEmpById.emp_type, this.getEmpById.isDeput).subscribe(res => {
      this.employeeSubTypes = res;     
    })
  }


  getJoiningMode() {
    this.getEmpById.joining_Catogary = '';
    this.master.getJoiningMode(this.getEmpById.isDeput, this.getEmpById.empSubType).subscribe(res => {
      this.joiningModes = res;
      
    })
  }

  getJoiningCategory() {
    this.master.getJoiningCategory(this.getEmpById.empApptType, this.getEmpById.isDeput).subscribe(res => {
      this.joiningCategory = res;     
    });    
  }
 
  setReguDate(value: any) {
    this.getEmpById.regularisationDate = value;
    this.getEmpById.lastDateIncrement = value;
  }
  getMakerEmpList() {  
    this.empDetails.getMakerEmpList(this.roleId).subscribe(res => {   
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.pipe = new DatePipe('en');
      const defaultPredicate = this.dataSource.filterPredicate;
      this.dataSource.filterPredicate = (data, filter) => {
        const dob = this.pipe.transform(data.empDOB, 'dd/MM/yyyy');
        const joiningDate = this.pipe.transform(data.empJoinDt, 'dd/MM/yyyy');
        return dob.indexOf(filter) >= 0 || joiningDate.indexOf(filter) >= 0 || defaultPredicate(data, filter);
      }  
     
    })
  }

     
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  trackById(item) {
    return item.empCd;
  }

  //async getEmployeeById(id: any, flag) {
  //    this.btnchecker = true;
  //    this.popUpMsg = 'Are you sure you want to Forward to HOO Checker ?';
  //    await this.empDetails.GetEmpPersonalDetailsByID(id, this.roleId).subscribe(res => {
  //    this.getEmpById.isDeput = res.isDeput;
  //    this.getEmpType(res.serviceType, null);
  //  //  this.getEmployeeSubType(res.emp_type);
  //  //  this.getJoiningMode(res.empSubType);
  //  //  this.getJoiningCategory(res.empApptType);
  //  //  this.getDeputationType(res.serviceType);
  //    this.getServiceType(this.getEmpById.isDeput);
  //    setTimeout(() => {
  //        this.getEmpById = res;
  //    }, 1000);
  //    if (res.isDeput === true) {
  //      this.showAndHideDepuType = true;
  //    }
  //    else {
  //      this.showAndHideDepuType = false;
  //    }
  //    this.buttonText = 'Update';
  //    if (flag === 1) {
  //      this.disbleflag = true;
  //      this.disbleflagdownload = false;
  //      this.buttonText = 'Save';
  //    }
  //      else if (res.documentUploadName != undefined) {
  //      this.disbleflagdownload = true;
  //      this.disbleflag = false;
  //      }
  //      else {
  //        this.disbleflag = false;
  //        this.disbleflagdownload = false;
  //      }
  //  })
  
  //}
  getEmployeeById(element,flag) {
    this.btnchecker = true;
    this.popUpMsg = 'Are you sure you want to Forward to HOO Checker ?';
    this.buttonText = 'Update';
    this.getEmpById.empCd = element.empCd;
    this.getEmpById.serviceType = element.serviceType;
    this.getEmpById.deputTypeID = element.deputTypeID;
    this.getDeputationType();
    this.getEmpById.isDeput = element.isDeput;
    this.getEmpById.emp_type = element.emp_type;
    this.getEmpType();
    this.getEmpById.empSubType = element.empSubType;
    this.getEmployeeSubType();
    this.getEmpById.empApptType = element.empApptType;  
    this.getJoiningMode();
    this.getEmpById.joining_Catogary = element.joining_Catogary;
    this.getJoiningCategory();
    this.getEmpById.empFirstName = element.empFirstName;
    this.getEmpById.empMiddleName = element.empMiddleName;
    this.getEmpById.empLastName = element.empLastName;
    this.getEmpById.empGender = element.empGender;
    this.getEmpById.emp_adhaar_no = element.emp_adhaar_no;
    this.getEmpById.emp_pan_no = element.emp_pan_no;
    this.getEmpById.empTitle = element.empTitle;
    this.getEmpById.empDOB = element.empDOB;
    this.getEmpById.empJoinDt = element.empJoinDt;
    this.getEmpById.departmentCD = element.departmentCD;
    this.getEmpById.regularisationDate = element.regularisationDate;
    this.getEmpById.lastDateIncrement = element.lastDateIncrement;
    this.getEmpById.msDesigMastID = element.msDesigMastID;
    this.getEmpById.regularisationTime = element.regularisationTime;
    this.getEmpById.empEntryDt = element.empEntryDt;
    this.getEmpById.documentUpload = element.documentUpload;
    this.getEmpById.documentUploadName = element.documentUploadName;
    this.getEmpById.empVerifFlag = 'U';
   
    if (element.isDeput === true) {
      this.showAndHideDepuType = true;
    }
    else {
      this.showAndHideDepuType = false;
    }
    if (flag === 1) {
      this.disbleflag = true;
      this.disbleflagdownload = false;
      this.buttonText = 'Save';
    }
    else if (element.documentUploadName != undefined) {
      this.disbleflagdownload = true;
      this.disbleflag = false;
      this.file.nativeElement.value = '';
    }
    else {
      this.disbleflag = false;
      this.disbleflagdownload = false;
    }
   
  }

  async InsertUpdateEmployeeDetails(files) {
    this.isLoading = true;
    this.buttonText = 'Processing';
    if (this.getEmpById.empCd === null || this.getEmpById.empCd === undefined ) {
      this.getEmpById.empVerifFlag = 'E';
      }
    else {
      if (this.getEmpById.empVerifFlag === null || this.getEmpById.empVerifFlag === 'E') {
        this.getEmpById.empVerifFlag = 'U';

      }      
    }
     
    if (this.getEmpById.isDeput == false) {
      this.getEmpById.deputTypeID = null;
    }
   
    if (files.length == 1) {
      if (files[0].type != 'application/pdf') {
        this.msg = this.commonMsg.pdffileMsg;
        return false;
      }
    }
    
    if (files.length == 0) {
     
      this.empDetails.InsertUpdateEmpDetails(this.getEmpById).subscribe(res => {
        this.isLoading = false;
       
         this.getMakerEmpList();
         if (res == undefined && this.getEmpById.empCd === null)
           swal(this.commonMsg.saveFailedMsg);
         else if (res > 1 && this.getEmpById.empCd === null)
        swal(this.commonMsg.saveMsg);
         else if (res > 1 && this.getEmpById.empCd != null)
             swal(this.commonMsg.updateMsg);
             this.ResetForm();        
        }
       )   
     }
     else { 
       const formData = new FormData();
       var fname = null;
       for (let file of files) {
   
         formData.append(file.name, file);
         fname = file.name;
         this.getEmpById.DocumentUploadName = fname;
       }
       this.asyncResult = await this.master.uploadFile(formData).subscribe(async res => {
         this.getEmpById.DocumentUpload = res;
       
         if (this.getEmpById.DocumentUpload !== undefined && fname !== null) {
          
           this.empDetails.InsertUpdateEmpDetails(this.getEmpById).subscribe(res => {
             this.isLoading = false;
             if (res == undefined && this.getEmpById.empCd === null)
               swal(this.commonMsg.saveFailedMsg);
             else if (res > 1 && this.getEmpById.empCd === null)
               swal(this.commonMsg.saveMsg);
             else if (res > 1 && this.getEmpById.empCd != null)
               swal(this.commonMsg.updateMsg);
             this.getMakerEmpList();
             this.ResetForm();
           });
         }
        
       });
     }
  
  }
  change() {
    this.getEmpById.documentUploadName = '';
  }

  ResetForm() {
    this.disbleflag = false;
    this.file.nativeElement.value = '';
    this.getEmpById.documentUploadName = '';
    this.form.resetForm();
    this.buttonText = 'Save';
    this.disbleflagdownload = false;
    this.getEmpById.isDeput = false;
    this.showAndHideDepuType = false;
    this.msg = '';
  }


  DownloadFile()
  {
    this.empDetails.getdownloadDetails(this.getEmpById.documentUpload).subscribe(res => {
      saveAs(new Blob([res], { type: 'application/pdf;charset=utf-8' }), this.getEmpById.documentUploadName);
    })
   }
 
  openDialog(empCd) {
    this.deleteShow = true;
    this.getEmpById.empCd = empCd;
 
  }
  deleteEmployeeById() {
   
    this.empDetails.DeleteEmpDetails(this.getEmpById.empCd).subscribe(res => {
      this.getMakerEmpList();
      this.deletepopup1 = false;
     
      swal(this.commonMsg.deleteMsg);
    });
    this.ResetForm();
  }
  forwordToChecker(files) {
 
   this.getEmpById.empVerifFlag = 'F';
    this.InsertUpdateEmployeeDetails(files);
    this.deletepopup = false;
   swal(this.commonMsg.forwardHooCheckerMsg);
  }
 
  approvedRejected(Flag: any) {
     if (Flag === 'V') {
       this.showRemark = true;
       this.popUpMsg = 'Are you sure you want to Verify Employee ?';
  
     } else {
       this.showRemark = false;
       this.popUpMsg = 'Are you sure you want to Reject Employee ?';
     }
    this.getEmpById.EmpVerifFlag = Flag;
   }
  verifyRejectEmpDtls() {
    this.deletepopup = false;
      this.empDetails.VerifyRejectionEmpDetails(this.getEmpById).subscribe(res => {
        this.ResetForm();
        this.getMakerEmpList();
    });
     
  }
  getDesignation() {
    this.designation.GetAllDesignation().subscribe(res => {
      this.designations = res;
      this.designationCtrl.setValue(this.designations);
      this.filteredDesignation.next(this.designations);
    });
    this.designFilterCtrl.valueChanges
      .pipe(take(1),takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });
  }

  private filterDesignation() {

    if (!this.designations) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesignation.next(this.designations.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesignation.next(

      this.designations.filter(designation => designation.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
   
}



