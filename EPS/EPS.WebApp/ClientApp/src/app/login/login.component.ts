import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { loginModel } from '../model/loginModel';
import { LoginServices } from './loginservice';
import { common } from '../payroll/common';
import { MatSnackBar } from '@angular/material';
//import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getLoginUserDetails } from '../login/getLoginUserDetails';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginServices]
})
export class LoginComponent implements OnInit {
  loginModel: loginModel;
  
  //logins: Observable<loginModel[]>;
  constructor(private router: Router, private _Service: LoginServices, private _common: common, private _snackBar: MatSnackBar, private route: ActivatedRoute, private _LoginUserDetails: getLoginUserDetails) {
    
  }

  loginDetails: any = [];
  userDetails: any = [];
  userroleActivate: any = [];
  CurrRoleIDStatus: any;
  username: any;
  CheckLoginRole: boolean;
  UserName: string;
  

  ngOnInit() {
    this.loginModel = new loginModel();
    this.CheckLoginRole = false;
  }

 // genId: string;
 // param2: string;
  //ActivateUser() {
  //  this.route.queryParams.subscribe(params => {
  //    this.genId = params['id'];
  //    alert(this.genId)
  //    if (this.genId != null) {
  //      this._Service.RoleActivation(this.genId).subscribe(data => {
  //        this.userroleActivate = data;
  //        if (this.userroleActivate != null) {
  //          this._snackBar.open('Role Activated Successfully, Please Login!', null, { duration: 4000 });
  //        }
  //      });
  //    }
  //  });
  //}
  
  UserDetails() {
   

    this._Service.UserDetails(this.loginModel.Username).subscribe(data => {
      this.userDetails = data;
      //const logins = new loginModel();
      //logins.Username = this.loginModel.Username;
      this._LoginUserDetails.SendLoginUserDetails(this.loginModel.Username, this.userDetails[0].paoid, this.userDetails[0].ddoid, this.userDetails[0].controllerID, this.userDetails[0].msUserID,
        this.userDetails[0].empPermDDOId
      )
      sessionStorage.setItem('username', this.loginModel.Username);
      sessionStorage.setItem('paoid', this.userDetails[0].paoid);
      sessionStorage.setItem('ddoid', this.userDetails[0].ddoid);
      sessionStorage.setItem('controllerID', this.userDetails[0].controllerID);
      sessionStorage.setItem('UserID', this.userDetails[0].msUserID);
      sessionStorage.setItem('EmpPermDDOId', this.userDetails[0].empPermDDOId);
      if (this.userDetails.length == "1") {
        sessionStorage.setItem('UserID', this.userDetails[0].msUserID);        
        sessionStorage.setItem('userRole', this.userDetails[0].userRole);
        sessionStorage.setItem('userRoleID', this.userDetails[0].msRoleID);
        sessionStorage.setItem('EmpPermDDOId', this.userDetails[0].empPermDDOId);
        //this.router.navigate(['dashboard/home']);
      }
      if (this.userDetails.length > 1) {     
        this.UserName = sessionStorage.getItem('username');
        this.CheckLoginRole = true;
      }
    });
  }
  redirectto() {

      if (this.loginModel.Username == null) {

        this.username = this.loginModel.Username;
        this._common.username = this.username;
        alert("Please enter username/password")
        return;
      }
      if (this.loginModel.Username != null || this.loginModel.Username != '') {

        this.username = this.loginModel.Username;
        this._common.username = this.username;
      }
      if (this.loginModel.Password == null) {

        alert("Please enter username/password")
        return;
      }
      this._Service.Login(this.loginModel).subscribe(data => {
        this.loginDetails = data;
      
        var Arrrolestatus = this.loginDetails.split('_');
        
        if (Arrrolestatus[0] == 'yes') {

          if (Arrrolestatus[1] == 'O')
          {
            this.UserDetails();
          }

          if (Arrrolestatus[1] == 'N') {
            sessionStorage.setItem('NewUser', this.loginModel.Username);
            this.router.navigate(['/changepassword'])
          }
          if (Arrrolestatus[1] == 'NF') {
            alert('Please activate your account.')
          }
        }
        else {
          alert('Please enter valid email/password.')
        }

      });
   
  }
  RoleSelectchanged(RowIndex) {
    sessionStorage.setItem('userRole', this.userDetails[RowIndex].userRole);
    sessionStorage.setItem('userRoleID', this.userDetails[RowIndex].msRoleID);
  }
  msRoleIDs: string;
  RedirecToHome() {
    if (this.msRoleIDs == undefined) {
      alert("Please Select Role")
      return;
    }      
    //this.router.navigate(['dashboard/home']);
  }
}



