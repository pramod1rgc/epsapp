import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../global/global.module';
import { loginModel } from '../model/loginModel';

@Injectable()

export class LoginServices {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {

    this.BaseUrl = 'http://localhost:55424/api/';
  }

  
  Login(loginModel: loginModel) {
    const params = new HttpParams().set('Username',loginModel.Username).set('Password',loginModel.Password);
    return this.httpclient
      .get<any>(`${this.config.api_base_url}${this.config.LoginDetails}`, { params });
  }
  UserDetails(username) {
    return this.httpclient.get(this.config.UserDetails + username, {});
  }

  ChangePassword(ObjPwdDetails) {
    return this.httpclient.post(this.config.api_base_url + this.config.LoginNewUser, ObjPwdDetails, { responseType: 'text' });
  }
  RoleActivation(ID) {
    return this.httpclient.get(this.config.RoleActivation+ ID, { responseType: 'text' });
  }
  currentrUrl(url) {
    return this.httpclient.get(this.config.currentrUrl+url, { responseType: 'text' });
  }
  IsMenuPermissionAssigned(UserName, msRoleIDs) {
    
    

    return this.httpclient.get(this.config.IsMenuPermissionAssigned + "?username=" + UserName + "&uroleid=" + msRoleIDs, { responseType: 'text' });
    //return this.http.post(`${this.config.api_base_url}${this.config.InsertPanNoFormDetails}/InsertPanNoFormDetails`,
    //  objPro, { responseType: 'text', params: parameter });
  }
}
