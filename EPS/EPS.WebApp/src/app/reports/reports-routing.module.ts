import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestreportComponent } from '../reports/testreport/testreport.component';

const routes: Routes = [

  {


    path: 'report', component: TestreportComponent, data: {
      breadcrumb: 'My Profile'
    }
    
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
