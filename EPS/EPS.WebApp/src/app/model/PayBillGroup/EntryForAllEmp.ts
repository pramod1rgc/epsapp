export class EntryForAllEmp {
  FinYear: string;
  Month: string;
  NgRecovery: Number;
  PreviousMonth: string;
  BillgrId: Number;
  DesigId: Number;
  EmpCd: string;
  EmpName: string;
  Amount: Number;
  InstallmentCount: Number;
  RecoveredInstallment: Number;
}
