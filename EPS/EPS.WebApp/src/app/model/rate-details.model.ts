import { FormControl, FormGroup, Validators } from '@angular/forms';

export class RateDetails {
  public SlabNo:string;
  public LowerLimit:string;
  public UpperLimit:string;
  public Value:string;
  public MinAmount:string
static asFormGroup(rateDetails: RateDetails): FormGroup {
  const fg = new FormGroup({
    SlabNo: new FormControl(rateDetails.SlabNo, Validators.required),
    LowerLimit: new FormControl(rateDetails.LowerLimit, Validators.required),
    UpperLimit: new FormControl(rateDetails.UpperLimit, Validators.required),
    Value: new FormControl(rateDetails.Value, Validators.required),
    MinAmount: new FormControl(rateDetails.MinAmount),
    
  });
  return fg;
}
}
     
  		