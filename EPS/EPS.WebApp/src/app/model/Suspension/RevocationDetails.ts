export class RevocationDetails {
  EmpRevocId: number;
  EmpTransSusId: number;
  RevocationDate: Date;
  RevocOrderNo: string;
  RevocOrderDate: Date;
  Remarks: string;
  FlagStatus: string;
  IPAddress: string;
  IsEditable: boolean;
  RejectedReason: string;
}


