export class JoiningDetails {
  EmpJoiningId: number;
  EmpRevokId: number;
  JoiningDate: Date;
  FnAn: string;
  OrderNo: string;
  OrderDate: Date;
  FlagStatus: string;
  RejectedReason: string;
  IPAddress: string;
  IsEditable = false;
}
