export class ExtensionDetails {
  Id: number;
  SuspenId: number;
  FromDate: Date;
  ToDate: Date;
  OrderNo: string;
  OrderDate: Date;
  SalaryPercent: number;
  Remarks: string;
  PermddoId: string;
  Status: string;
  IsEditable = false;
  SusPeriod: number;
  RejectedReason: string;
}









