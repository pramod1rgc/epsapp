
export class OnBoardingModel {
  RequestLetterNo: string;
  RequestLetterDate: string;
  ControllerID: number;
  PAOID: number;
  DDOID: number;
  FirstName: string;
  LastName: string;
  Designation: string;
  EmailID: string;
  MobileNo: string;
  CFirstName: string;
  CLastName: string;
  CDesignation: string;
  CEmailID: string;
  CMobileNo: string;
  PFirstName: string;
  PLastName: string;
  PDesignation: string;
  PEmailID: string;
  PMobileNo: string;
  DFirstName: string;
  DLastName: string;
  DDesignation: string;
  DEmailID: string;
  DMobileNo: string;
}
