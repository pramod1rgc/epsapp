export class PsuMaster {
  PsuId = 0;
  PsuCode: string;
  PDesc: string;
  PDescBilingual: string;
  PAddress: string;
  PPinCode: number;
  PPhoneNo: number;
  PFaxNo: number;
  PEmail: string;
  PStateName: number;
  PCity: number;
  PIsBankDetails = '0';
  PIfscCode: string;
  PBankName: string;
  PBranchName: string;
  PBankAccNum: number;
  PConfirmAccNum: number;
  PChequeInFavour: string;
  IPAddress: string;
  StateName: string;
  CityName: string;
}



