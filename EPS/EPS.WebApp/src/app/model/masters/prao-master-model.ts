export class PraoControllerModel {
 
  MsControllerID: number;
  ControllerID: string;
  ControllerName: string;
  ControllerCode: string;
  Address: string;
  PhoneNo: string;
  FaxNo: string;
  Email: string;
  Pincode: number;
  StateID: string;
  statecode: string;
  StateName: string;
  cityID: string;
  DistrictId: string;
  DistrictName: string;
  DistrictStateID: string;
  Mode: number;
  constructor() { }
}


