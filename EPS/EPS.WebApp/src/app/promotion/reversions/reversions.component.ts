import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';


@Component({
  selector: 'app-reversions',
  templateUrl: './reversions.component.html',
  styleUrls: ['./reversions.component.css']
})
export class ReversionsComponent implements OnInit {

  reversionsForm: FormGroup;
  empCd: any = null;
  designations: any[] = [];

  displayedColumns: string[] = ['S.No', 'ReversionType', 'OrderNo.', 'Designation', 'Status', 'Action'];


  tomorrow: Date = new Date();

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  objToBeDeleted: any = null;

  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  searchTerm: string = '';
  infoScreen: boolean = false;
  editScreen: boolean = false;
  transferDetailId: number;

  rejectionRemark: string = '';

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  ForwardToMakerPopup: boolean = false;

  controllerId: string;
  roleId: string;
  ddoId: string;

  constructor(private _service: PromotionsService, private snackBar: MatSnackBar, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
  }

  ngOnInit() {
    this.createForm();
    this.getAllDesignation();
  }

  createForm() {
    this.reversionsForm = new FormGroup({
      transferDetailId: new FormControl(0),
      reversionType: new FormControl('', [Validators.required]),
      orderNo: new FormControl('', [Validators.required, Validators.minLength(3)]),
      orderDt: new FormControl('', Validators.required),
      reversionWefDt: new FormControl('', Validators.required),
      joinOrderNo: new FormControl('', Validators.required),
      joinOrderDt: new FormControl('', Validators.required),
      joinedDt: new FormControl('', Validators.required),
      joinedAnBn: new FormControl('', Validators.required),
      toDesigCd: new FormControl('', Validators.required),
      remark: new FormControl('', Validators.required),
      rejectionRemark: new FormControl(''),
      trnGround: new FormControl(2),
      dueToCd: new FormControl("712"),
      permDdoId: new FormControl(""),
      toOfficeCd: new FormControl("00003000"),
      empCd: new FormControl(this.empCd, [Validators.required]),
      ddoId: new FormControl(this.ddoId),
      status: new FormControl('')
    });
    if (this.roleId == '5') {
      this.reversionsForm.disable();
    }
  }

  fetchTableData(empCd: any) {
    if (empCd) {
      this.empCd = empCd;
      //this.createForm();
      this.reversionsForm.controls.empCd.setValue(this.empCd);
      this.getEmpTransDetails(this.pageSize, this.pageNumber);
      if (this.roleId != '5') {
        if (this.empCd) {
          this.reversionsForm.enable();
        }
        else {
          this.reversionsForm.disable();
        }
      }
    }
    else {
      this.empCd = empCd;
      //this.createForm();
      this.reversionsForm.controls.empCd.setValue(this.empCd);
      this.dataSource.data = [];
      this.totalCount = 0;
    }
  }

  getEmpTransDetails(pageSize, pageNumber) {
    let employeeId = this.empCd;
    this.totalCount = 0;
    this.dataSource.data = []; // = new MatTableDataSource<any>();
    this._service.FetchReversionEmployeeDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(result => {
      if (result && result != undefined && result.length > 0) {
        this.dataSource.data = result; // = new MatTableDataSource(result);
        // this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.totalCount = result[0].totalCount;
      }
    });
  }

  cancelForm() {
    this.reversionsForm.reset();
    this.reversionsForm.enable();
    this.createForm();
    this.infoScreen = false;
    this.editScreen = false;
    this.formGroupDirective.resetForm();
  }

  getAllDesignation() {
    this._service.GetAllDesignation(this.controllerId).subscribe(result => {
      this.designations = result;
      this.designations.sort((a, b) => {
        if (a.desigDesc < b.desigDesc) return -1;
        else if (a.desigDesc > b.desigDesc) return 1;
        else return 0;
      });
    });
  }

  DeleteReversionDetails(obj) {
    this.objToBeDeleted = obj;
  }

  confirmDelete() {
    this._service.DeleteReversionDetails(this.objToBeDeleted.transDetailId).subscribe(result => {
      this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
      this.pageNumber = 1;
      this.paginator.firstPage();
      this.dataSource.paginator = this.paginator;
      this.getEmpTransDetails(this.pageSize, this.pageNumber);
      this.DeletePopup = false;
      this.objToBeDeleted = null;
    });
  }

  btnEditClick(obj, isEdit) {
    if (isEdit) {
      this.transferDetailId = obj.transDetailId;
      this.reversionsForm.setValue({
        transferDetailId: obj.transDetailId,
        reversionType: obj.reversionType,
        orderNo: obj.orderNo,
        orderDt: obj.orderDt,
        reversionWefDt: obj.reversionWefDt,
        joinOrderNo: obj.joinOrderNo,
        joinOrderDt: obj.joinOrderDt,
        joinedDt: obj.joinedDt,
        joinedAnBn: obj.joinedAnBn,
        toDesigCd: obj.toDesigCd,
        remark: obj.remark,
        rejectionRemark: obj.rejectionRemark,
        trnGround: 2,
        dueToCd: "712",
        permDdoId: "",
        toOfficeCd: "00003000",
        empCd: this.empCd,
        ddoId: this.ddoId,
        status: obj.verifyFlag
      });
      this.reversionsForm.enable();
      this.infoScreen = false;
      this.editScreen = true;
    }
    else {
      this.transferDetailId = obj.transDetailId;
      this.reversionsForm.setValue({
        transferDetailId: obj.transDetailId,
        reversionType: obj.reversionType,
        orderNo: obj.orderNo,
        orderDt: obj.orderDt,
        reversionWefDt: obj.reversionWefDt,
        joinOrderNo: obj.joinOrderNo,
        joinOrderDt: obj.joinOrderDt,
        joinedDt: obj.joinedDt,
        joinedAnBn: obj.joinedAnBn,
        toDesigCd: obj.toDesigCd,
        remark: obj.remark,
        rejectionRemark: obj.rejectionRemark,
        trnGround: 2,
        dueToCd: "712",
        permDdoId: "",
        toOfficeCd: "00003000",
        empCd: this.empCd,
        ddoId: this.ddoId,
        status: obj.verifyFlag
      });
      this.reversionsForm.disable();
      this.infoScreen = true;
      this.editScreen = false;
    }
  }

  applyFilter(value) {
    this.pageNumber = 1;
    this.getEmpTransDetails(this.pageSize, this.pageNumber);
    //this.dataSource.filter = value;
    //if (this.dataSource.paginator) {
    //  this.dataSource.paginator.firstPage();
    //}
  }

  onSubmit() {
    if (this.reversionsForm.valid) {
      this._service.UpdateReversionDetails(this.reversionsForm.value).subscribe(result => {
        let response = result as number;
        if (response) {
          if (response > 0) {
            this.reversionsForm.reset();
            //this.getEmpTransDetails(this.pageSize, this.pageNumber);
            this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
            this.formGroupDirective.resetForm();
            this.createForm();
            this.transferDetailId = null;
            this.editScreen = false;
            var btn = document.getElementById('headerButton');
            btn.click();
            this.empCd = null;
            this.getEmpTransDetails(10, 1);
          }
          if (response == -1) {
            this.snackBar.open("Order number already exist!", null, { duration: 4000 });
          }
        }
      });
      if (this.transferDetailId && this.reversionsForm.controls.status.value != 'E') {
        this.updateStatus("U");
      }
    }
  }

  setJoiningDate() {
    this.reversionsForm.controls.joinedDt.setValue(this.reversionsForm.controls.reversionWefDt.value);
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getEmpTransDetails(this.pageSize, this.pageNumber);
  }

  forwardToChecker() {
    if (this.transferDetailId) {
      var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": "P", "PermDdoId": "", "DDOId": this.ddoId };
      this._service.ForwardTransferDetailsToChecker(forwardObj).subscribe(result => {
        this.snackBar.open(this._msg.forwardCheckerMsg, null, { duration: 4000 });
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        this.transferDetailId = null;
        this.reversionsForm.reset();
        this.createForm();
      });
    }
  }

  updateStatus(status: string) {
    if (this.transferDetailId) {
      var forwardObj = { "MsEmpTransDet": this.transferDetailId, "VerifyFlag": status, "PermDdoId": "", "RejectionRemark": this.rejectionRemark, "DDOId": this.ddoId };
      this._service.UpdateStatus(forwardObj).subscribe(result => {
        this.snackBar.open(this._msg.updateMsg, null, { duration: 4000 });
        this.getEmpTransDetails(this.pageSize, this.pageNumber);
        this.transferDetailId = null;
        this.reversionsForm.reset();
        this.createForm();
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
      });
    }
  }


  refresh() {
    //this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
    //  this.router.navigate([item.route]));
  }

}
