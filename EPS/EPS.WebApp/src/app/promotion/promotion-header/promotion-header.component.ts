import { Component, OnInit, ViewChild } from '@angular/core';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { PromotionsComponent } from '../promotions/promotions.component';
import { ReversionsComponent } from '../reversions/reversions.component';
import { takeUntil } from 'rxjs/operators';
import { Subject, ReplaySubject } from 'rxjs';


@Component({
  selector: 'app-promotion-header',
  templateUrl: './promotion-header.component.html',
  styleUrls: ['./promotion-header.component.css']
})


export class PromotionHeaderComponent implements OnInit {

  BillGroup: any;
  EmpGroup: any;
  DesigGroup: any;
  EmpDetails: any;
  headersForm: FormGroup;
  allDesignation: any[] = [];
  allEmployees: any[] = [];
  public billFilterCtrl: FormControl = new FormControl();
  public desigFilterCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredBill: Subject<any[]> = new Subject<any[]>();
  public filteredDesig: Subject<any[]> = new Subject<any[]>();
  public filteredEmployee: Subject<any[]> = new Subject<any[]>();
  suspendedEmployees: boolean = false;
  controllerId: any;
  ddoId: any;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;


  constructor(private _Service: PromotionsService, private _PromoComp: PromotionsComponent, private _ReverComp: ReversionsComponent, private route: ActivatedRoute) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.ddoId = sessionStorage.getItem('ddoid');
  }

  ngOnInit() {
    this.BindDropDownBillGroup();
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
    this.desigFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesig();
      });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
    this.getAllDesignation();
    this.getAllEmployees();
    //if (this.router.url === '/dashboard/promotion/reversions')
    //  this.selectedRadio = '2';
    //else
    //  this.selectedRadio = '1';


    this.headerDataForms();
  }

  private filterBill() {
    if (!this.BillGroup) {
      return;
    }
    // get the search keyword
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.BillGroup.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBill.next(

      this.BillGroup.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterDesig() {
    if (!this.allDesignation) {
      return;
    }
    // get the search keyword
    let search = this.desigFilterCtrl.value;
    if (!search) {
      this.filteredDesig.next(this.allDesignation.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredDesig.next(

      this.allDesignation.filter(desig => desig.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterEmp() {
    if (!this.allEmployees) {
      return;
    }
    // get the search keyword
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmployee.next(this.allEmployees.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEmployee.next(

      this.allEmployees.filter(emp => emp.empName.toLowerCase().indexOf(search) > -1 || emp.empCd.toLowerCase().indexOf(search) > -1)
    );
  }

  getAllDesignation() {
    this._Service.GetAllDesignation(this.controllerId).subscribe(result => {
      this.allDesignation = result;
      this.allDesignation.sort((a, b) => {
        if (a.desigDesc < b.desigDesc) return -1;
        else if (a.desigDesc > b.desigDesc) return 1;
        else return 0;
      });
      this.filteredDesig.next(this.allDesignation);
    });
  }

  getAllEmployees() {
    this._Service.GetAllEmpWithDesignationDetails(this.suspendedEmployees, this.controllerId, this.ddoId).subscribe(result => {
      this.allEmployees = result;
      this.allEmployees.sort((a, b) => {
        if (a.empName < b.empName) return -1;
        else if (a.empName > b.empName) return 1;
        else return 0;
      });
      this.filteredEmployee.next(this.allEmployees);
    });
  }

  setSuspendedEmployees(isSuspended) {
    if (isSuspended) {
      this.headerDataForms();
      this.formGroupDirective.resetForm();
      this.getEmpTransDetails();
      this._ReverComp.createForm();
      this._ReverComp.reversionsForm.controls.reversionType.setValue('penalty');
    }
    this.suspendedEmployees = isSuspended;
    this.getAllEmployees();
  }

  headerDataForms() {
    this.headersForm = new FormGroup({
      pay_bill_group: new FormControl('', Validators.required),
      designation: new FormControl('', [Validators.required]),
      employee: new FormControl('', Validators.required)
    });
  }

  resetForm() {
    this.headerDataForms();
    //this.headersForm.reset();
    this.formGroupDirective.resetForm();
  }


  BindDropDownBillGroup() {
    this._Service.BindDropDownBillGroup().subscribe(result => {
      this.BillGroup = result;
      this.BillGroup.sort((a, b) => {
        if (a.billgrDesc < b.billgrDesc) return -1;
        else if (a.billgrDesc > b.billgrDesc) return 1;
        else return 0;
      });
      this.filteredBill.next(this.BillGroup);
    });
  }

  BindDropDownDesigGroup(selectedBilgrp) {
    if (selectedBilgrp) {
      this._Service.BindDropDownDesigGroup(selectedBilgrp).subscribe(result => {
        this.DesigGroup = result;
        this.DesigGroup.sort((a, b) => {
          if (a.desigDesc < b.desigDesc) return -1;
          else if (a.desigDesc > b.desigDesc) return 1;
          else return 0;
        });
        this.filteredDesig.next(this.DesigGroup);
      });
    }
  }


  onBillDesgSel(selectedDesig) {
    if (selectedDesig) {
      this._Service.BindDropDownEmpGroup(selectedDesig).subscribe(result => {
        this.EmpGroup = result;
        this.EmpGroup.sort((a, b) => {
          if (a.empName < b.empName) return -1;
          else if (a.empName > b.empName) return 1;
          else return 0;
        });
        this.filteredEmployee.next(this.EmpGroup);
      })
    }
  }

  getEmpTransDetails() {
    for (var i = 0; i < this.allEmployees.length; i++) {
      if (this.allEmployees[i].empCd == this.headersForm.value.employee) {
        this.headersForm.controls.designation.setValue(this.allEmployees[i].designationCd);
        break;
      }
    }
    if (this.route.snapshot.routeConfig.path == 'promotions') {
      this._PromoComp.fetchTableData(this.headersForm.value.employee);
    }
    if (this.route.snapshot.routeConfig.path == 'reversionadhoc') {
      this._ReverComp.fetchTableData(this.headersForm.value.employee);
    }
  }


}
