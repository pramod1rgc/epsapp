import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromotionRoutingModule } from './promotion-routing.module';
import { PromotionsComponent } from './promotions/promotions.component';
import { MaterialModule } from '../material.module';
import { PromotionHeaderComponent } from './promotion-header/promotion-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ReversionsComponent } from './reversions/reversions.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommonMsg } from '../global/common-msg';

@NgModule({
  declarations: [PromotionsComponent, PromotionHeaderComponent, ReversionsComponent],
  imports: [
    CommonModule,
    PromotionRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, SharedModule
  ],
  providers: [ReversionsComponent, PromotionsComponent, CommonMsg]

})
export class PromotionModule { }
