import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinAfterLienEpsEmployeeComponent } from './join-after-lien-eps-employee.component';

describe('JoinAfterLienEpsEmployeeComponent', () => {
  let component: JoinAfterLienEpsEmployeeComponent;
  let fixture: ComponentFixture<JoinAfterLienEpsEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinAfterLienEpsEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinAfterLienEpsEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
