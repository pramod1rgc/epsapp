import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { MasterService } from '../../services/master/master.service';
import { EpsemployeejoinafterlienperiodService } from '../../services/lienperiod/epsemployeejoinafterlienperiod.service';
import { MatSnackBar, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { DatePipe } from '@angular/common';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-join-after-lien-eps-employee',
  templateUrl: './join-after-lien-eps-employee.component.html',
  styleUrls: ['./join-after-lien-eps-employee.component.css'],
  providers: [CommonMsg]
})
export class JoinAfterLienEpsEmployeeComponent implements OnInit {
  form: FormGroup;
  btnUpdateText: string;
  joiningTimeDdl: any = ['ForeNoon', 'AfterNoon'];
  designation: any = [];
  joinService: any;
  roleId: any;
  getJoiningAccount: any;
  getofficelist: any;
  officeCityClassList: any;
  epsJoinData: any;
  empCd: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  rejectionRemark: string = '';
  tblfiltervalue: string = '';
  lienId: string = '';
  notFound = true;
  displayedColumns: string[] = ['joiningOrderNo', 'joiningOrderDate', 'relievingOrderNo', 'relievingOrderDate', 'relievingOffice', 'Status', 'action'];
  disableflag: boolean = true;
  disableForwardflag: boolean = false;
  insertEditFlag: any;
  @ViewChild('freject') form2: any;
  @ViewChild('formDirective') private formDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private master: MasterService, private _formBuilder: FormBuilder, private snackBar : MatSnackBar, private epsemployeejoinafterlienperiodService:EpsemployeejoinafterlienperiodService) { }

  ngOnInit() {
    this.roleId = sessionStorage.getItem('userRoleID');
    this.btnUpdateText = 'Save';
    this.FormDetails();
    this.getAllDesignation(sessionStorage.getItem('controllerID'));
    this.getJoinServices();
    this.getJoiningAccountOf();
    this.getOfficeList();
    this.getOfficeCityClassList();
   // this.getAllDesignation(sessionStorage.getItem('controllerID'));
  }
  FormDetails() {
    this.form = this._formBuilder.group({
      'employeeCode': [this.empCd, Validators.required],
      'relievingOrderNo': [null, Validators.required],
      'relievingOrderDate': [null, Validators.required],
      'relievingOffice': [null],
      'relievingBy': [null],
      'relievingDdoId': [null],
      'relievedOn': [null],
      'onRelievedDesignationId':[null],
      'onRelievedDesignation': [null],
      'payCommissionId':[null],
      'payCommission': [null],
      'payScaleId':[null],
      'payScale': [null],
      'basicPay': [null],
      'joinServiceAs': [null],
      'joiningonAccountof': [null],
      'joiningOrderNo': [null],
      'joiningOrderDate': [null],
      'joiningTime': [null],
      'officeId': [null],
      'officeCityClass': [null],
      'joiningDesigcode': [0, Validators.required],
      'createdBy': [null],
      'existRecordAfterLien': [null],
      'flagUpdate': [null],
      'msAfterLeinId': [null],
      'rejectionRemark': [null],
    })
  }
  getJoinServices() {
    this.master.getJoiningMode().subscribe(res => {
      debugger;
      this.joinService = res;
    })
  }
  getJoiningAccountOf() {
    debugger;
    this.master.getJoiningAccountOf().subscribe(res => {
      debugger;
      this.getJoiningAccount = res;
    })
  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  getOfficeList() {
    this.master.getOfficeList(sessionStorage.getItem('controllerID'), sessionStorage.getItem('ddoid')).subscribe(res => {
      debugger;
      this.getofficelist = res;
    })
  }
  getAllDesignation(controllerId: any) {
    this.master.GetAllDesignation(controllerId).subscribe(res => {

      debugger;
      this.designation = res;
    })
  }
  getOfficeCityClassList() {
    this.master.getOfficeCityClassList().subscribe(res => {
      this.officeCityClassList = res;
    })
  }
  GetcommanMethod(value) {
    // alert(value);
    debugger;
    this.empCd = value;
    this.form.get('employeeCode').setValue(value);
    this.getEpsEmpJoinOffAfterLienPeriodDetails(value, this.roleId);
    this.form.get('employeeCode').setValue(this.empCd);
    // this.btnUpdateText = 'Save';
  }
  ltrim(tblfiltervalue) {
    return tblfiltervalue.replace(/^\s+/g, '');
  }
 
  applyFilter(filterValue: string) {
    debugger;
    this.tblfiltervalue = this.ltrim(this.tblfiltervalue);
    if (this.epsJoinData === null || this.epsJoinData === undefined) {
      this.notFound = false;
    }
    

   this.epsJoinData.filter = filterValue.trim().toLowerCase();;
    this.epsJoinData.filterPredicate = function (data, filter: string): boolean {
      debugger;
      var joiningOrderNo = "";
      if (data.joiningOrderNo != '' && data.joiningOrderNo != null) {
        joiningOrderNo = data.joiningOrderNo.toLowerCase();
        return data.relievingOrderNo.toLowerCase().includes(filter) ||   data.relievingOffice.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.joiningOrderDate.includes(filter);
      }
      else {
        return data.relievingOrderNo.toLowerCase().includes(filter) || joiningOrderNo.includes(filter) || data.relievingOffice.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter); 
      }
      
    };
    if (this.epsJoinData.paginator) {
      this.epsJoinData.paginator.firstPage();
    }
    if (this.epsJoinData.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  btnEditClick(lienId) {
    debugger;
    this.formDirective.resetForm();
    let value = this.epsJoinData.filteredData.filter(x => x.msAfterLeinId == lienId)[0]
    if (value.joiningDesigcode === 0) {
      value.joiningDesigcode = null;
      value.joiningonAccountof = null;
    }
    this.form.patchValue(value);
    this.form.enable();
    if (value.veriFlag == 'N') {
      this.btnUpdateText = 'Save';
    }
    else {
      this.btnUpdateText = 'Update';
    }
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableflag = true;
      if (value.veriFlag == 'N') {
        this.disableForwardflag = false;
      }
      else {
        this.disableForwardflag = true;
      }
      //this.Btntxt = 'Save';
    }
  }
  btnInfoClick(lienId) {
    let value = this.epsJoinData.filteredData.filter(x => x.msAfterLeinId == lienId)[0]
    this.form.patchValue(value);
    this.form.disable();
    //let empStatus = value.veriFlag;
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableForwardflag = false;
      //this.Btntxt = 'Save';
    }
  }

  getEpsEmpJoinOffAfterLienPeriodDetails(Empcd: any, roleId: any) {
    this.epsemployeejoinafterlienperiodService.getEpsEmpJoinOffAfterLienPeriodDetailByEmp(Empcd, roleId).subscribe(result => {

      if (result.length == 0) {
        this.formDirective.resetForm();
        this.form.enable();
        this.epsJoinData = null;
        this.form.get('employeeCode').setValue(this.empCd);
      }
      else {
       
        this.epsJoinData = new MatTableDataSource(result);
        this.epsJoinData.paginator = this.paginator;
        this.epsJoinData.sort = this.sort;
            this.form.disable();
          }
      })
}

  ResetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    this.btnUpdateText = 'Save';
   
    this.disableForwardflag = false;
   this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
  }
  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.rejectionRemark = '';
    this.DeletePopup = false;
  }
 
  forwardStatusUpdate(statusFlag: any) {
    debugger;
    var RejectionComment = '0';
    //let employeeId = this.empCd.trim();
    let joiningOrderNo = this.form.get('joiningOrderNo').value;
    if (statusFlag == 'R') {
      RejectionComment = this.rejectionRemark;
    }
    else {
      RejectionComment = '0'
    }
  
    this.epsemployeejoinafterlienperiodService.forwardStatusUpdate(this.empCd, joiningOrderNo, statusFlag, RejectionComment).subscribe(result1 => {
      if (parseInt(result1) >= 1) {

        this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
        this.form.reset();
        this.formDirective.resetForm();
        this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
        this.btnUpdateText = 'Save';
        this.Cancel();
        this.infoScreen = false;
        this.disableForwardflag = false;
      }
    });
  }

  deleteRecordClick(lienId) {
    this.lienId = lienId;
  }

  confirmDelete() {

  
    this.epsemployeejoinafterlienperiodService.deleteEpsEmployeeJoinAfterLienPeriodById(this.lienId).subscribe(result1 => {
      debugger;
      if (parseInt(result1) >= 1) {

        this.snackBar.open('Record Successfully deleted', null, { duration: 4000 });
        this.form.reset();
        this.formDirective.resetForm();
        this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
        this.btnUpdateText = 'Save';
        this.Cancel();

      }
      else {
        this.snackBar.open('Record not deleted', null, { duration: 4000 });
      }
    });

  }
  onSubmit() {
    debugger;
    //this.submitted = true;

    this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    else {
      if (this.btnUpdateText == 'Update') {
        this.form.get('flagUpdate').setValue('update');

      }
      else {
        this.form.get('flagUpdate').setValue('insert');
      }
      this.epsemployeejoinafterlienperiodService.InsertUpateEpsEmployeeJoinAfterLienPeriod(this.form.value).subscribe(result1 => {
        let resultvalue = result1.split("-");
        if (parseInt(resultvalue[0]) >= 1) {
          if (this.btnUpdateText == 'Update') {
            this.snackBar.open('Update Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
            this.btnUpdateText = 'Save';
          }
          else {
            this.snackBar.open('Save Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
            this.btnUpdateText == 'Save';
          }

        }
        else {

          if (this.btnUpdateText == 'Update') {
            if (resultvalue[2] = 'Duplicate Record') {
              this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Update not Successfully', null, { duration: 4000 });
            }
            
          }
          else if (resultvalue[2] = 'Duplicate Record') {
            this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
          }
          else {
            this.snackBar.open('Save not Successfully', null, { duration: 4000 });
          }
        }

      })
    }
  }

}
