import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, Form } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { MasterService } from '../../services/master/master.service';
import { CommonMsg } from '../../global/common-msg';
import { MatSnackBar, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TransferOfficeLienperiodService } from '../../services/lienperiod/transfer-office-lienperiod.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-transfer-office-lienperiod',
  templateUrl: './transfer-office-lienperiod.component.html',
  styleUrls: ['./transfer-office-lienperiod.component.css'],
  providers: [CommonMsg]
})
export class TransferOfficeLienperiodComponent implements OnInit {
  form: FormGroup;
  formPopUp: FormGroup;
  formRemarks: FormGroup;
  btnUpdateText: string;
  showCivilValue: boolean;
  savebuttonstatus: boolean;
  relHideShow: boolean;
  OffHideShow: boolean = false;
  paoDdoHideShow: boolean = true;
  designation: any = [];
  tranferLienPeriodData: any;
  disableForwardflag: boolean = false;
  insertEditFlag: any;
  eventsSubject: Subject<void> = new Subject<void>();
 
  roleId: any;
  disableflag: boolean = true;
  displayedColumns: string[] = ['orderNo', 'orderDate', 'relievingOrderNo', 'relievingOrderDate', 'officeName','Status', 'action'];
  @ViewChild('formDirective') private formDirective;
  @ViewChild('formDirectivepop') private formDirectivepop;
  @ViewChild('freject') form2: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private master: MasterService,private _formBuilder: FormBuilder, private transferOfficeLienperiodService: TransferOfficeLienperiodService, private snackBar: MatSnackBar) { }
  reasonTransferDdl: any = ['Technical Resignation With Lien Period'];
  relievingTimeDdl: any = ['ForeNoon', 'AfterNoon'];
  controllerList: any;
  ddoList: any;
  empCd: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  rejectionRemark: string = '';
  tblfiltervalue: string = '';
  isOfFEditable: boolean = true;
  lienId: string = '';
  notFound = true;
  displayedColumns2: string[] = ['checked','OfficeName', 'OfficeAddress', 'TelephoneNo', 'E-MailId'];
  //displayedColumns2: string[] = ['Office Name', 'Office Address', 'Telephone No', 'E-Mail Id'];
  officeAddressDetails: any=[];
  tranferOfficeData: any;
  ngOnInit() {
    this.roleId = sessionStorage.getItem('userRoleID');
    this.btnUpdateText = 'Save';
    this.savebuttonstatus = true;
    this.FormDetails();
    this.formPopUpDetails();
    this.getControllerList();
    this.getAllDesignation(sessionStorage.getItem('controllerID'));
  }
  GetcommanMethod(value) {
    debugger;
    this.empCd = value;
    this.form.get('employeeCode').setValue(value);
    this.getTransfer_office_lien_Period_Details(value, this.roleId);
    this.form.get('employeeCode').setValue(this.empCd);
   // this.btnUpdateText = 'Save';
  }
getTransferReasonType()
{
  
  if (this.form.get('reasonTransfer').value !== "") {
    this.relHideShow = true;
  }
  else {
    this.relHideShow = false;
  }
}
  bindddolist() {
  
    this.getDdoListByControllerId(this.formPopUp.get('controllerId').value);
  }
  FormDetails() {
    this.form = this._formBuilder.group({
      'employeeCode': [this.empCd, Validators.required],
      'orderNo': [null, Validators.required],
      'orderDate': [null, Validators.required],
      'reasonTransfer': [null],
      'remarks': [null],
      'relevingOrderNo': [null],
      'relievingOrderDate': [null, Validators.required],
      'relievingTime': [null],
      'relievingDesigcode': [null],
      'department': [null],
      'officeId': [null],
    
      'officeName': [null],
      'officeNameother':[null],
      'paoId': [null],
      'paoName': [null],
      'ddoId':[null],
      'ddoName': [null],
      'officeAddress': [null],
      'createdBy': [null],
      'rowNumber': [null],
      'flagUpdate': [null],
      'rejectionRemark': [null],
      'mslienId': [null]
    });
  }

 
  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      debugger;
      if (f.value > t.value) {
        swal("Date from should be less than Date to");
       
      }
      return {};
    }
  }
  CheckDate() {
    debugger;
    if (this.form.get('relievingOrderDate').value >= this.form.get('orderDate').value) {
      swal("Alert Hello");
    }
  }

  
  btnEditClick(lienId) {
   
 
    let value = this.tranferLienPeriodData.filteredData.filter(x => x.mslienId == lienId)[0]
    this.getDepartment(value.department);
    this.form.patchValue(value);
    this.form.enable();
    this.btnUpdateText = 'Update';
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;


      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      
      this.disableflag = true;
      this.disableForwardflag = true;
      //this.Btntxt = 'Save';
    }
  }
  btnInfoClick(lienId) {
    
    let value = this.tranferLienPeriodData.filteredData.filter(x => x.mslienId == lienId)[0]
    if (value.department == 'Other') {
      this.OffHideShow = true;
      this.paoDdoHideShow = false;
    }
    else {
    
      this.OffHideShow = false;
      this.paoDdoHideShow = true;
    }
    this.form.patchValue(value);
    
    this.form.disable();
    //let empStatus = value.veriFlag;
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      
      this.disableForwardflag = false;
      //this.Btntxt = 'Save';
    }
  }
  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.rejectionRemark = '';
   this.DeletePopup = false;
  }
  getDepartment(valueId: any) {
    this.ClearPop();
    if (valueId == 'Other') {
      
      this.OffHideShow = true;
      this.paoDdoHideShow = false;
      this.form.get('paoId').clearValidators();
      this.form.get('paoId').updateValueAndValidity();
      this.form.get('paoId').setValue("");
      this.form.get('paoName').clearValidators();
      this.form.get('paoName').updateValueAndValidity();
      this.form.get('paoName').setValue("");
      this.form.get('ddoId').clearValidators();
      this.form.get('ddoId').updateValueAndValidity();
      this.form.get('ddoId').setValue("");
      this.form.get('ddoName').clearValidators();
      this.form.get('ddoName').updateValueAndValidity();
      this.form.get('ddoName').setValue("");
      this.form.get('officeId').clearValidators();
      this.form.get('officeId').updateValueAndValidity();
      this.form.get('officeId').setValue("");
      this.form.get('officeName').setValue("");
      this.isOfFEditable = false;
      this.showCivilValue = false;
    }
    else {
      this.form.get('officeAddress').clearValidators();
      this.form.get('officeAddress').updateValueAndValidity();
      this.form.get('officeAddress').setValue("");
      this.form.get('officeName').setValue("");
      this.OffHideShow = false;
      this.paoDdoHideShow = true;
      this.isOfFEditable = true;
       
    }
    //alert("test");
  }
  formPopUpDetails() {
    this.formPopUp = this._formBuilder.group({
      'controllerId': [null],
      'ddoId': [null],
      'officeName':[null]
    })
  }

  
  //get Controller List
  getControllerList() {
   
    this.transferOfficeLienperiodService.getControllerList().subscribe(res => {
        
       this.controllerList = res;
      })
    
  }
  //Get DDO LIST Using ControllerId
  getDdoListByControllerId(controllerID: any) {
    this.transferOfficeLienperiodService.getDdoListbyControllerId(controllerID).subscribe(res => {
      
      this.ddoList = res;
    })
  }
  GetAllDetails() {
    let Conrtollid = this.formPopUp.get('controllerId').value;
    let ddoid = this.formPopUp.get('ddoId').value;
    let officename = this.formPopUp.get('officeName').value;
    if (officename == null) {
      
      officename = 0;
    }
    this.getOfficeAddressDetails(Conrtollid, ddoid, officename);
  }
  ClearPop() {
    this.tranferOfficeData = null;
    this.formDirectivepop.resetForm();
  }

  getOfficeAddressDetails(controllerID: any, ddoId: any, officeName: any) {
    if (officeName == "") {
      officeName = 0;
    }
    debugger;
    if (this.formPopUp.invalid) {
      return;
    }
    else {
      this.transferOfficeLienperiodService.getOfficeAddressDetails(controllerID, ddoId, officeName).subscribe(res => {
        debugger;
        if (res.length == 0) {
          this.officeAddressDetails = null;
          this.tranferOfficeData = null;
          swal("Data Not Found ?")
        }
        else {
        this.officeAddressDetails = res;
          this.tranferOfficeData = res;
        }
      })
    }
  }
  getTransfer_office_lien_Period_Details(Empcd: any, roleId: any) {
    this.transferOfficeLienperiodService.getTransfer_office_lien_Period_Details(Empcd, roleId).subscribe(result => {
      
      
      debugger;
      //if (Flag == true) {
      if (result.length== 0) {
        this.formDirective.resetForm();
        this.form.enable();
        this.tranferLienPeriodData = null;
        this.form.get('employeeCode').setValue(this.empCd);
      }
      else {
        this.tranferLienPeriodData = new MatTableDataSource(result);
        this.tranferLienPeriodData.paginator = this.paginator;
        this.tranferLienPeriodData.sort = this.sort;
        this.form.get('employeeCode').setValue(Empcd);
        if (this.form.get('reasonTransfer').value !== "select") {
          this.relHideShow = true;
        }
        else {
          this.relHideShow = false;
        }
      }
     })

  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
 }
  getAllDesignation(controllerId: any) {
    this.master.GetAllDesignation(controllerId).subscribe(res => {

      
      this.designation = res;
    })
      }
  GetSelectCode(code: any) {
    this.officeAddressDetails
    debugger;
    let groups = this.tranferOfficeData.filter(({ officeId }) => code.includes(officeId));
    this.form.get('officeId').setValue(groups[0].officeId);
    this.form.get('officeName').setValue(groups[0].officeName.trim());
    this.form.get('paoName').setValue(groups[0].paoName);
    this.form.get('ddoName').setValue(groups[0].ddoName);
    this.form.get('paoId').setValue(groups[0].paoId);
    this.form.get('ddoId').setValue(groups[0].ddoId);
   
    this.showCivilValue = false;
    this.ClearPop(); 
  }
  ltrim(tblfiltervalue) {
    return tblfiltervalue.replace(/^\s+/g, '');
  }
  applyFilter(filterValue: string) {
    debugger;
    this.tblfiltervalue = this.ltrim(this.tblfiltervalue);
    if (this.tranferLienPeriodData === null || this.tranferLienPeriodData === undefined) {
      this.notFound = false;
    }
    
    
    this.tranferLienPeriodData.filter = filterValue.trim().toLowerCase();
    this.tranferLienPeriodData.filterPredicate = function (data, filter: string): boolean {
      debugger;
      return data.orderNo.toLowerCase().includes(filter) || data.orderDate.toLowerCase().includes(filter) || data.relevingOrderNo.toLowerCase().includes(filter) || data.relievingOrderDate.includes(filter) || data.officeName.toLowerCase().includes(filter);
     };
    if (this.tranferLienPeriodData.paginator) {
      this.tranferLienPeriodData.paginator.firstPage();
    }
    if (this.tranferLienPeriodData.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  confirmDelete() {
    
    let value = this.tranferLienPeriodData.filteredData.filter(x => x.mslienId == this.lienId)[0]
    
    let orderNo = value.orderNo;
    this.transferOfficeLienperiodService.deleteTransferOffLienPeriod(this.empCd, orderNo).subscribe(result => {

      if (parseInt(result) >= 1) {

        this.snackBar.open('Record Successfully Deleted ', null, { duration: 4000 });
        this.formDirective.reset();
        this.getTransfer_office_lien_Period_Details(this.empCd, this.roleId);
        this.Cancel();

      }
      else {
        this.snackBar.open('Record Not Deleted ', null, { duration: 4000 });
      }
    })
  }
  deleteRecordClick(lienId) {
    this.lienId = lienId;
   
  }
  ResetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    this.form.enable();
    this.btnUpdateText = 'Save';
    this.disableForwardflag = false;
    this.form.get('employeeCode').setValue(this.empCd.trim());
   
  }
  forwardStatusUpdate(statusFlag: any) {
    
    var RejectionComment = '0';
   
    let joiningOrderNo = this.form.get('orderNo').value;
    if (statusFlag == 'R') {
      RejectionComment = this.rejectionRemark;
    }
    else {
      RejectionComment ='0'
    }
    this.transferOfficeLienperiodService.forwardStatusUpdate(this.empCd, joiningOrderNo, statusFlag, RejectionComment).subscribe(result1 => {
      if (parseInt(result1) >= 1) {

        this.snackBar.open('Status Update Successfully', null, { duration: 4000 });
        this.form.reset();
        this.formDirective.resetForm();
        this.getTransfer_office_lien_Period_Details(this.empCd, this.roleId);
        this.btnUpdateText = 'Save';
        this.infoScreen = false;
        this.ApprovePopup = false;
        this.RejectPopup = false;
        this.rejectionRemark = '';
      
        this.disableForwardflag = false;

      }
      else {
        this.snackBar.open('Status not Update Successfully', null, { duration: 4000 });
      }
    });
  }
  onSubmit() {
    
    //this.submitted = true;

    this.form.get('createdBy').setValue(sessionStorage.getItem('username'));
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    else {
      if (this.btnUpdateText == 'Update') {
        this.form.get('flagUpdate').setValue('update');

      }
      else {
        this.form.get('flagUpdate').setValue('insert');
        let employeeid = this.form.get('employeeCode').value;
        if (employeeid == 0) {
          alert("Select Employee");
          return;
        }
      }
      this.transferOfficeLienperiodService.InsertUpateTransferOffLienPeriodService(this.form.value).subscribe(result1 => {
        let resultvalue = result1.split("-");
        if (parseInt(resultvalue[0]) >= 1) {
          if (this.btnUpdateText == 'Update') {
            this.snackBar.open('Update Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getTransfer_office_lien_Period_Details(this.empCd, this.roleId);
            this.btnUpdateText = 'Save';
          }
          else {
            this.snackBar.open('Save Successfully', null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getTransfer_office_lien_Period_Details(this.empCd, this.roleId);
            this.btnUpdateText == 'Save';
          }

        } 
          else {

          if (this.btnUpdateText == 'Update') {
            if (resultvalue[2] = 'Duplicate Record') {
              this.snackBar.open('Order Number  Already Exits', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Update not Successfully', null, { duration: 4000 });
            }

             
            }
            else if (resultvalue[2] = 'Duplicate Record') {
            this.snackBar.open('Order Number Already Exits', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Save not Successfully', null, { duration: 4000 });
            }
          }
        
      })
    }
  }
  
}
