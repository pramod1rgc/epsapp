import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonEpsEmployeeJoinAfterLienComponent } from './non-eps-employee-join-after-lien.component';

describe('NonEpsEmployeeJoinAfterLienComponent', () => {
  let component: NonEpsEmployeeJoinAfterLienComponent;
  let fixture: ComponentFixture<NonEpsEmployeeJoinAfterLienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonEpsEmployeeJoinAfterLienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonEpsEmployeeJoinAfterLienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
