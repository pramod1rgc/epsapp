import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    debugger
        // add authorization header with jwt token if available
      let currentUser = JSON.parse(sessionStorage.getItem('Token'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                Authorization: `Bearer ${currentUser.token}`
              }
            });
          debugger;
        }

        return next.handle(request);
    }
}
