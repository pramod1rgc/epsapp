import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogintokenComponent } from './logintoken.component';

describe('LogintokenComponent', () => {
  let component: LogintokenComponent;
  let fixture: ComponentFixture<LogintokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogintokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogintokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
