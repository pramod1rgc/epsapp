import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { loginModel } from '../model/loginModel';
import { LoginServices } from '../login/loginservice';
import { common } from '../payroll/common';
import { MatSnackBar } from '@angular/material';
import { getLoginUserDetails } from '../login/getLoginUserDetails';
import { LocationStrategy } from '@angular/common';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-logintoken',
  templateUrl: './logintoken.component.html',
  styleUrls: ['./logintoken.component.css']
})
export class LogintokenComponent implements OnInit {

  loginModel: loginModel;
  constructor(private location: LocationStrategy, private router: Router, private _Service: LoginServices, private _common: common, private _snackBar: MatSnackBar, private route: ActivatedRoute,
    private _LoginUserDetails: getLoginUserDetails) {

    history.pushState(null, null, window.location.href);
    this.location.onPopState(() => {
      history.pushState(null, null, window.location.href);
    });
    this.currentURL = window.location.href;
  }

  currentURL = '';
  loginDetails: any;
  userDetails: any = [];
  userroleActivate: any = [];
  CurrRoleIDStatus: any;
  username: any;
  CheckLoginRole: boolean;
  UserName: string;
  token: any = [];
  tokenSub: any = [];
  hash: any;
  urlResult: any;
  isMenuPermissionAssigned: boolean;

  ngOnInit() {

    //alert(this.currentURL);
    //var ArrUrl = this.currentURL.split('/');//Abs.split('/');'http://10.199.72.53:8181/login'
    // alert(ArrUrl+''+ArrUrl[1] + '' + ArrUrl[2]);
    //this.url = ArrUrl[2] + '/';
    this.baseURL(this.currentURL);
    this.loginModel = new loginModel();
    this.CheckLoginRole = false;
  }

  UserDetails() {

    this._Service.UserDetails(this.loginModel.Username).subscribe(data => {
      this.userDetails = data;
      this._LoginUserDetails.SendLoginUserDetails(this.loginModel.Username, this.userDetails[0].paoid, this.userDetails[0].ddoid, this.userDetails[0].controllerID, this.userDetails[0].msUserID, this.userDetails[0].empPermDDOId)

      sessionStorage.setItem('username', this.loginModel.Username);
      sessionStorage.setItem('paoid', this.userDetails[0].paoid);
      sessionStorage.setItem('ddoid', this.userDetails[0].ddoid);
      sessionStorage.setItem('controllerID', this.userDetails[0].controllerID);
      sessionStorage.setItem('UserID', this.userDetails[0].msUserID);
      sessionStorage.setItem('EmpPermDDOId', this.userDetails[0].empPermDDOId);
      if (this.userDetails.length == "1") {
        sessionStorage.setItem('UserID', this.userDetails[0].msUserID);
        sessionStorage.setItem('userRole', this.userDetails[0].userRole);
        sessionStorage.setItem('userRoleID', this.userDetails[0].msRoleID);
        sessionStorage.setItem('EmpPermDDOId', this.userDetails[0].empPermDDOId);
        //   this.router.navigate(['dashboard/home']);
      }
      if (this.userDetails.length > 1) {
        this.UserName = sessionStorage.getItem('username');
        this.CheckLoginRole = true;
      }
    });
  }
  redirectto() {

    if (this.loginModel.Username == null) {

      this.username = this.loginModel.Username;
      this._common.username = this.username;
      alert("Please enter username/password")
      return;
    }
    if (this.loginModel.Username != null || this.loginModel.Username != '') {

      this.username = this.loginModel.Username;
      this._common.username = this.username;
    }
    if (this.loginModel.Password == null) {

      alert("Please enter username/password")
      return;
    }
    this._Service.Login(this.loginModel).pipe(first()).subscribe(data => {
      this.loginDetails = data;
      sessionStorage.setItem('token', JSON.stringify(this.loginDetails.jwT_Secret));

      if (this.loginDetails.status == 'yes') {

        if (this.loginDetails.userType == 'O') {
          this.UserDetails();
        }
        if (this.loginDetails.userType == 'N') {
          sessionStorage.setItem('NewUser', this.loginModel.Username);
          this.router.navigate(['/changepassword'])
        }
        if (this.loginDetails.userType == 'NF') {
          alert('Please activate your account.')
        }
      }
      else {
        alert('Please enter valid email/password.')
      }

    });

  }

  RoleSelectchanged(RowIndex) {
    sessionStorage.setItem('userRole', this.userDetails[RowIndex].userRole);
    sessionStorage.setItem('userRoleID', this.userDetails[RowIndex].msRoleID);
  }
  msRoleIDs: string;
  RedirecToHome() {

    if (this.msRoleIDs == undefined) {
      alert("Please Select Role")

      return;
    }
    this.IsMenuPermissionAssigned(this.loginModel.Username, this.msRoleIDs);

  }

  OnBoarding() {
    this.router.navigate(['NewOnboarding']);
  }
  NewOnBoardingStatus() {
    this.router.navigate(['NewOnBoardingStatus']);
  }

  baseURL(url) {
    this._Service.currentrUrl(url).subscribe(data => {
      this.urlResult = data;
    });
  }
  IsMenuPermissionAssigned(UserName, msRoleIDs) {

    this._Service.IsMenuPermissionAssigned(UserName, msRoleIDs).pipe(first()).subscribe(data => {
      this.isMenuPermissionAssigned = Boolean(JSON.parse(data));
      if (this.isMenuPermissionAssigned) { this.router.navigate(['dashboard/home']); }
      else { this.router.navigate(['dashboard/underdevelopment']); }
    });
  }
}
