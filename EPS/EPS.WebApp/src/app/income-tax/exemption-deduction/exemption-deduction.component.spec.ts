import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExemptionDeductionComponent } from './exemption-deduction.component';

describe('ExemptionDeductionComponent', () => {
  let component: ExemptionDeductionComponent;
  let fixture: ComponentFixture<ExemptionDeductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExemptionDeductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExemptionDeductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
