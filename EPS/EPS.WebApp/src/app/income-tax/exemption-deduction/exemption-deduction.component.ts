import { ChangeDetectorRef, Component, forwardRef, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { MatSort, MatTableDataSource } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';
import { ExemptionDeductionService } from '../../services/income-tax/exemption-deduction.service';


@Component({
  selector: 'app-exemption-deduction',
  templateUrl: './exemption-deduction.component.html',
  styleUrls: ['./exemption-deduction.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ExemptionDeductionComponent),
    multi: true,
  }]
})
export class ExemptionDeductionComponent implements OnInit, OnChanges {

  incomeTaxRatesForm: FormGroup;

  displayedColumnsForForm: string[] = ['lowerLimit', 'upperLimit', 'percentageValue', 'normalRebate', 'spclRebate', 'maxSaving', 'action'];
  //displayedColumns: string[] = ['lowerLimit', 'upperLimit', 'percentageValue', 'normalRebate', 'spclRebate', 'maxSaving', 'action'];
  //displayedColumns: string[] = ['serialNo', 'sectionCode', 'financialYearFrom', 'financialYearTo', 'slDesc', 'action']; removed serial no
  displayedColumns: string[] = ['sectionCode', 'financialYearFrom', 'financialYearTo', 'slDesc', 'action'];
  formDataSource = new MatTableDataSource();

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort: MatSort;

  data: any[] = [];
  DeletePopup: boolean = false;
  elementToBeDeleted: any;
  allData: any[] = [];
  ArrddlMajSecCode: any[] = [];
  ArrddlSecCode: any[] = [];
  ArrddlSubSecCode: any[] = [];
  ArrddlSecCodeDesc: any[] = [];
  ArrddlSlabLimit: any[] = [];
  ArrddlPayCommission: any[] = [];
  ArrddlYearFrom: any[] = [];
  ArrddlYearTo: any[] = [];

  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  searchTerm: string = '';
  userName: string = '';
  incomeTaxDetails: any[] = [];
  incomeTaxDetailsSearch: any[] = [];
  noRecordMsg: string;
  maxLength: number = 6;
  percentValuePattern: string = '^[0-9]{1,6}$';
  submitButtonText: string = 'Save';
  Iscondition: boolean = false;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  isOnInit: boolean = false;
  isRateDetailPanel: boolean = false;
  bgColor: string;
  btnCssClass: string = 'btn btn-success';

  constructor(private formBuilder: FormBuilder, private _Service: ExemptionDeductionService, private _msg: CommonMsg, private cdRef: ChangeDetectorRef) {
  }
  ngOnChanges() {
  }

  createForm() {
    this.incomeTaxRatesForm = new FormGroup({
      majorSectionCode: new FormControl('', [Validators.required]),
      sectionCode: new FormControl('', [Validators.required]),
      subSectionCode: new FormControl('', [Validators.required]),
      msEdrId: new FormControl(0),
      sectionDesc: new FormControl(''),
      userName: new FormControl(''),
      financialYearFrom: new FormControl('', [Validators.required]),
      financialYearTo: new FormControl(''),
      payCommission: new FormControl(''),
      slabLimitsFor: new FormControl('', [Validators.required]),
      rateDetails: new FormArray([], [Validators.minLength(1)])
    });
    this.addRateDetail();
  }

  createRateDetail(): FormGroup {
    return this.formBuilder.group({
      id2: new FormControl(0),
      exmDedRebateId: new FormControl(0),
      lowerLimit: new FormControl('', [Validators.required]),
      upperLimit: new FormControl('', [Validators.required]),
      percentageValue: new FormControl('', [Validators.required]),
      normalRebate: new FormControl('', [Validators.required, Validators.pattern(this.percentValuePattern)]),
      spclRebate: new FormControl('', [Validators.required, Validators.pattern(this.percentValuePattern)]),
      maxSaving: new FormControl('', [Validators.required]),
      limitOverlapError: new FormControl(false),
    }, { validators: this.LimitValidators.bind(this) });
  }

  addRateDetail() {
    this.rateDetails.push(this.createRateDetail());
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
  }

  get rateDetails() {
    return this.incomeTaxRatesForm.get("rateDetails") as FormArray;
  }

  deleteRateDetail(index: any) {
    if (this.rateDetails.length > 1) {
      this.rateDetails.removeAt(index);
      this.formDataSource = new MatTableDataSource(this.rateDetails.value);
    }
  }

  onSubmit() {
    if (this.incomeTaxRatesForm.valid) {   
      this.incomeTaxRatesForm.controls.userName.setValue(this.userName);
      this._Service.UpsertExemptionDeductionDetails(this.incomeTaxRatesForm.value).subscribe(response => {
        if (response != undefined || response != null) {
          this.getExempDedDetails(this.pageSize, this.pageNumber);
          this.incomeTaxRatesForm.reset();
          //this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
          //swal(response);
          this.formGroupDirective.resetForm();
          this.createForm();
          this.submitButtonText = 'Save';
          this.isSuccessStatus = true;
          this.responseMessage = response;
        }
        else {
          this.isSuccessStatus = false;
          this.isWarningStatus = true;
          this.responseMessage = this._msg.saveFailedMsg;
        }
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';

        setTimeout(() => {
          this.isSuccessStatus = false;
          this.isWarningStatus = false;
          this.responseMessage = '';
        }, this._msg.messageTimer);
      });
    }
  }

  cancelForm() {
    this.incomeTaxRatesForm.reset();
    this.formGroupDirective.resetForm();
    this.createForm();
    this.submitButtonText = 'Save';
    this.bgColor = '';
    this.btnCssClass = 'btn btn-success';
  }

  applyFilterPrevious(value) {
    let data = this.dataSource.data as any[];
    //if (value && data.length > 0) {
    if (value) {
      value = value.toLowerCase();
      //let data = this.dataSource.data as any[];
      let filteredData = data.filter(a => a.financialYearFrom.toLowerCase().includes(value) || a.financialYearTo.toLowerCase().includes(value) || a.majorSectionCode.toLowerCase().includes(value) || a.sectionCode.toLowerCase().includes(value) || a.slDesc.toLowerCase().includes(value));
      this.dataSource = new MatTableDataSource(filteredData);
    }
    else {
      this.dataSource = new MatTableDataSource(this.incomeTaxDetails);
    }
    this.totalCount = this.dataSource.data.length;
  }

  applyFilter(value) {
    if (value) {
      this.totalCount = 0;
      this._Service.GetExemptionDeductionDetails(this.pageNumber, this.pageSize, this.searchTerm).subscribe(filteredData => {
        if (filteredData && filteredData != undefined && filteredData.length > 0) {
          this.dataSource = new MatTableDataSource(filteredData);
          this.dataSource.sort = this.sort;
          this.allData = filteredData;
          this.totalCount = filteredData[0].totalCount;
        }
        else {
          this.getExempDedDetails(this.pageSize, 1);
        }
      });
    }
    else {
      //this.dataSource = new MatTableDataSource(this.incomeTaxDetailsSearch);
      this.getExempDedDetails(this.pageSize, 1);
    }
    this.totalCount = this.dataSource.data.length;
  }
    
  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getExempDedDetails(this.pageSize, this.pageNumber);
  }

  getExempDedDetails(pageSize, pageNumber) {
    this.totalCount = 0;
    this.dataSource = new MatTableDataSource<any>();
    this._Service.GetExemptionDeductionDetails(pageNumber, pageSize, this.searchTerm).subscribe(result => {
      if (result && result != undefined && result.length > 0) {
        this.incomeTaxDetails = result;
        this.dataSource = new MatTableDataSource(result);
        this.dataSource.sort = this.sort;
        this.allData = result;
        this.totalCount = result[0].totalCount;
      }
    });
  }

  btnEditClick(obj) {
    while (this.rateDetails.length != 0) {
      this.rateDetails.removeAt(0);
    }
    this.incomeTaxRatesForm.patchValue(obj);
    this.rateDetails.removeAt(0);
    obj.rateDetails.forEach(rate => this.rateDetails.push(this.formBuilder.group(rate)));
    this.formDataSource = new MatTableDataSource(this.rateDetails.value);
        
    this.BindSectionCode(obj.majorSectionCode);
    this.BindSubSectionCodeOnEdit(obj.majorSectionCode, obj.sectionCode);

    this.submitButtonText = 'Update';
    this.isRateDetailPanel = true;
    this.bgColor = 'bgcolor';
    this.btnCssClass = 'btn btn-info';
  }

  deleteDetails(element) {
    this.elementToBeDeleted = element;
  }

  confirmDelete() {
    this._Service.DeleteExemptionDeductionDetails(this.elementToBeDeleted.drrID, this.elementToBeDeleted.drrID2).subscribe(response => {
      if ((response != undefined || response != null) && response > 0) {
        let data = this.dataSource.data as any[];
        let index = data.indexOf(this.elementToBeDeleted);
        data.splice(index, 1);
        this.dataSource = new MatTableDataSource(data);
        this.elementToBeDeleted = null;
        this.totalCount = this.totalCount - 1;
        //this.getExempDedDetails(10, 1);
        this.createForm();
        //this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
        //swal(this._msg.deleteMsg);
        //$(".dialog__close-btn").click();
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteMsg;
      }
      else {
        //swal(this._msg.deleteFailedMsg);
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteFailedMsg;
      }
      this.DeletePopup = false;

      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);

    });
  }

  ngOnInit() {
    this.isOnInit = true;
  
    this.createForm();
    this.dataSource = new MatTableDataSource(this.data);
    this.totalCount = this.data.length;
    this.BindMajorSectionCode();
    this.BindSlabLimit(7);
    //this.BindFromFinancialYear();
    //this.BindToFinancialYear();
    this.GetFinancialYear();
     
    this.userName = sessionStorage.getItem('username');
    this.getExempDedDetails(this.pageSize, this.pageNumber);
    this.noRecordMsg = this._msg.noRecordMsg;
    this.submitButtonText = 'Save';      
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  BindMajorSectionCode() {
    this._Service.GetMajorSectionCode().subscribe(data => {
      this.ArrddlMajSecCode = data;
    })
  }
   
  BindSectionCode(sectionCode) {
    this._Service.GetSectionCode(sectionCode).subscribe(data => {
      this.ArrddlSecCode = data;
    })
  }

  BindSubSectionCode() {
    var selectedMajSecCode = this.incomeTaxRatesForm.controls.majorSectionCode.value;
    var selectedSecCode = this.incomeTaxRatesForm.controls.sectionCode.value;
    this.ArrddlSubSecCode = [];
    this._Service.GetSubSectionCode(selectedMajSecCode, selectedSecCode).subscribe(data => {
      this.ArrddlSubSecCode = data;
      if (this.ArrddlSubSecCode.length == 1) {
        this.incomeTaxRatesForm.controls.subSectionCode.setValue(this.ArrddlSubSecCode[0].subSecCode);
        this.BindSectionCodeDescription();
      } else {
        this.incomeTaxRatesForm.controls.subSectionCode.setValue('');
        this.incomeTaxRatesForm.controls.sectionDesc.setValue('');
      }
    })    
  }

  BindSubSectionCodeOnEdit(majSecCode, secCode) {
    this._Service.GetSubSectionCode(majSecCode, secCode).subscribe(data => {
      this.ArrddlSubSecCode = data;
    })  
  }

  BindSectionCodeDescription() {
    var selectedMajSecCode = this.incomeTaxRatesForm.controls.majorSectionCode.value;
    var selectedSecCode = this.incomeTaxRatesForm.controls.sectionCode.value;
    var selectedSubSecCode = this.incomeTaxRatesForm.controls.subSectionCode.value;

    this._Service.GetSectionCodeDescription(selectedMajSecCode, selectedSecCode, selectedSubSecCode).subscribe(data => {
      this.ArrddlSecCodeDesc = data;
      this.incomeTaxRatesForm.controls.sectionDesc.setValue(this.ArrddlSecCodeDesc[0].secDesc);
      this.incomeTaxRatesForm.controls.msEdrId.setValue(this.ArrddlSecCodeDesc[0].secId)
    })
  }

  BindSlabLimit(payCommission) {
    this._Service.GetSlabLimit(payCommission).subscribe(data => {
      this.ArrddlSlabLimit = data;
    })
  }

  BindPayCommission() {
    this._Service.GetPayCommission().subscribe(data => {
      this.ArrddlPayCommission = data;
    })
  }
   
  GetFinancialYear() {
    this._Service.GetFinancialYear().subscribe(data => {
      this.ArrddlYearFrom = data;
      this.ArrddlYearTo = data;
    })    
  } 
  
  LimitValidators(frm: FormGroup): any {    
    let lLimit = frm.controls.lowerLimit.value;
    let uLimit = frm.controls.upperLimit.value;
    if (+lLimit > +uLimit) {
      frm.controls.lowerLimit.setErrors({ 'invalidError': true });
      return { 'invalidError': true };
    }
    else {
      frm.controls.lowerLimit.setErrors(null);
      if (!lLimit) {
        frm.controls.lowerLimit.setErrors({ 'required': true })
      }
      if (!this.isOnInit) {
        if (this.Iscondition) {
          frm.controls.lowerLimit.setErrors({ 'limitOverlapError': true });
          return { 'limitOverlapError': true };
        }
        else {
          frm.controls.lowerLimit.setErrors(null);
        }
      }
      return null;
    }
  }

  PercentValueChange(index) {
    
    var percentValue = this.rateDetails.value[index].percentageValue;
    if (percentValue == 'P') {
      this.maxLength = 3;
      this.percentValuePattern = '^(?!0)(100|[1-9]?[0-9])(\.\d{1,2})?$';
    }
    else {
      this.maxLength = 6;
      this.percentValuePattern = '^[0-9]{1,6}$';
    }
    this.rateDetails.controls[index].get("spclRebate").setValidators([Validators.required, Validators.pattern(this.percentValuePattern)]);
    this.rateDetails.controls[index].get("normalRebate").setValidators([Validators.required, Validators.pattern(this.percentValuePattern)]);

    this.validateLowerLimit(index);
    //alert(this.rateDetails.value[index].limitOverlapError);
  }

  validateLowerLimit(index) {
    
    if (+index > 0) {
      var lLimit = this.rateDetails.value[index].lowerLimit;
      var preLimit = this.rateDetails.value[index - 1].upperLimit;
      
      if (+lLimit <= +preLimit) {
        this.rateDetails.controls[index].get("lowerLimit").setErrors({ 'limitOverlapError': true });      
        this.rateDetails.controls[index].value.limitOverlapError = true;
        this.Iscondition = true;
      }
      else {
        this.Iscondition = false;
      }
     
    }
  }

  resetRateDetailPanel() {
    this.isRateDetailPanel = false;
    this.bgColor = '';
  }

}
