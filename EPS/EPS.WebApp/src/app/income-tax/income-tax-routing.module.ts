import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncomeTaxModule } from './income-tax.module';
import { ExemptionDeductionComponent } from './exemption-deduction/exemption-deduction.component';
import { StandardDeductionComponent } from './standard-deduction/standard-deduction.component';
import { ItRatesComponent } from './it-rates/it-rates.component';
import { OtherDuesComponent} from './other-dues/other-dues.component'

const routes: Routes = [
  {
    path: '', component: IncomeTaxModule, children: [
      { path: 'exemptiondeduction', component: ExemptionDeductionComponent },
      { path: 'standarddeduction', component: StandardDeductionComponent, data: { breadcrumb: 'Standard Deduction' }},
      { path: 'itrates', component: ItRatesComponent, data: { breadcrumb: 'IT Rates' } },
      { path: 'otherdues', component: OtherDuesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncomeTaxRoutingModule { }
