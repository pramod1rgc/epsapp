import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherDuesComponent } from './other-dues.component';

describe('OtherDuesComponent', () => {
  let component: OtherDuesComponent;
  let fixture: ComponentFixture<OtherDuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherDuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherDuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
