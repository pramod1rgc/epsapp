import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { OtherDuesService } from '../../services/income-tax/other-dues.service';


@Component({
  selector: 'app-other-dues',
  templateUrl: './other-dues.component.html',
  styleUrls: ['./other-dues.component.css']
})

export class OtherDuesComponent implements OnInit {

  otherDuesForm: FormGroup;

  //displayedColumns: string[] = ['serialNo', 'duesDesc', 'majorSectionCode', 'sectionCode', 'subSectionCode', 'sectionCombined', 'action'];
  //displayedColumns: string[] = ['serialNo', 'duesDesc', 'sectionCode', 'action'];
  displayedColumns: string[] = ['duesDesc', 'sectionCode', 'action'];
  formDataSource = new MatTableDataSource();

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  data: any[] = [];
  DeletePopup: boolean = false;
  elementToBeDeleted: any;
  allData: any[] = [];
  ArrddlMajSecCode: any[] = [];
  ArrddlSecCode: any[] = [];
  ArrddlSubSecCode: any[] = [];
  ArrddlSecCodeDesc: any[] = [];
  ArrddlDues: any[] = [];

  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  searchTerm: string = '';
  userName: string = '';
  otherDuesDetails: any[] = [];
  noRecordMsg: string;
  onEdit: boolean = false;
  submitButtonText: string = 'Save';
  isRateDetailPanel: boolean = false;
  bgColor: string;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  isLoading: boolean = false;
  btnCssClass: string = 'btn btn-success';
  isUpdated: boolean = false;

  constructor(private _service: OtherDuesService, private _msg: CommonMsg) { }

  createForm() {
    this.otherDuesForm = new FormGroup({
      msEdrId: new FormControl(0),
      majorSectionCode: new FormControl('', [Validators.required]),
      sectionCode: new FormControl('', [Validators.required]),
      subSectionCode: new FormControl('', [Validators.required]),
      sectionDesc: new FormControl(''),
      duesId: new FormControl(null, [Validators.required]),
      duesDesc: new FormControl(''),
      userName: new FormControl('')
    });
  }

  ngOnInit() {
    this.isLoading = true;
    this.onEdit = false;
    this.createForm();
    this.dataSource = new MatTableDataSource(this.data);
    this.totalCount = this.data.length;
    this.BindOtherDues(false);
    this.BindMajorSectionCode();

    this.userName = sessionStorage.getItem('username');
    this.getOtherDuesDetails(this.pageSize, this.pageNumber);
    this.noRecordMsg = this._msg.noRecordMsg;

    this.submitButtonText = 'Save';
    this.isLoading = false;
    this.isUpdated = false;
  }

  onSubmit() {
    if (this.otherDuesForm.valid) {
      this.isLoading = true;
      this.otherDuesForm.controls.userName.setValue(this.userName);
      this._service.SaveOtherDuesDetails(this.otherDuesForm.value).subscribe(response => {
        if (response && response != undefined) {
         
          this.isSuccessStatus = true;
          if (this.isUpdated) {
            this.responseMessage = this._msg.updateMsg;
          } else {
            this.responseMessage = this._msg.saveMsg;
          }

          this.getOtherDuesDetails(this.pageSize, this.pageNumber);
          this.BindOtherDues(false);
          this.otherDuesForm.reset();
          //this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
          //swal(this._msg.saveMsg);
      
          this.formGroupDirective.resetForm();
          this.createForm();
          this.submitButtonText = 'Save';
          this.onEdit = false;
        }
        else {
          if (this.isUpdated) {
            this.responseMessage = this._msg.updateFailedMsg;
          }
          else {
            this.responseMessage = this._msg.saveFailedMsg;
          }
          this.isWarningStatus = true;
        }

        this.bgColor = '';
        this.isRateDetailPanel = false;
        this.btnCssClass = 'btn btn-success';
        this.isUpdated = false;
      });

      this.isLoading = false;
      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);

    }
  }

  cancelForm() {
    this.onEdit = false;
    this.otherDuesForm.reset();
    this.formGroupDirective.resetForm();
    this.createForm();
    this.BindOtherDues(false);
    this.submitButtonText = 'Save';
    this.bgColor = '';
    this.btnCssClass = 'btn btn-success';
    this.isUpdated = false;
  }

  applyFilterPrevious(value) {
    let data = this.dataSource.data as any[];
    //if (value && data.length > 0) {
    if (value) {
      value = value.toLowerCase();
      //let data = this.dataSource.data as any[];
      let filteredData = data.filter(a => a.duesDesc.toLowerCase().includes(value) || a.majorSectionCode.toLowerCase().includes(value) || a.sectionCode.toLowerCase().includes(value));
      this.dataSource = new MatTableDataSource(filteredData);
    }
    else {
      this.dataSource = new MatTableDataSource(this.otherDuesDetails);
    }
    this.totalCount = this.dataSource.data.length;
  }

  applyFilter(value) {
    if (value) {
      this.totalCount = 0;
      this._service.getOtherDuesDetails(this.pageNumber, this.pageSize, this.searchTerm).subscribe(filteredData => {
        if (filteredData && filteredData != undefined && filteredData.length > 0) {
          this.dataSource = new MatTableDataSource(filteredData);
          this.dataSource.sort = this.sort;
          this.allData = filteredData;
          this.totalCount = filteredData[0].totalCount;
        }
        else {
          this.getOtherDuesDetails(this.pageSize, 1);
        }
      });
    }
    else {
      this.getOtherDuesDetails(this.pageSize, 1);
    }
    this.totalCount = this.dataSource.data.length;
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getOtherDuesDetails(this.pageSize, this.pageNumber);
  }

  getOtherDuesDetails(pageSize, pageNumber) {
    this.totalCount = 0;
    this.dataSource = new MatTableDataSource<any>();
    this._service.getOtherDuesDetails(pageNumber, pageSize, this.searchTerm).subscribe(result => {
      if (result && result != undefined && result.length > 0) {
        this.otherDuesDetails = result;
        this.dataSource = new MatTableDataSource(result);
        this.dataSource.sort = this.sort;
        this.allData = result;
        this.totalCount = result[0].totalCount;
      }
    });
  }

  btnEditClick(obj) {
    this.isLoading = true;
    this.onEdit = true;
    this.BindOtherDues(true);

    this.otherDuesForm.patchValue(obj);

    this.otherDuesForm.controls.duesId.setValue('' + obj.duesId + '');

    this.BindSectionCode(obj.majorSectionCode);
    this.BindSubSectionCodeOnEdit(obj.majorSectionCode, obj.sectionCode);

    this.submitButtonText = 'Update';
    this.bgColor = 'bgcolor';
    this.isRateDetailPanel = true;
    this.btnCssClass = 'btn btn-info';
    this.isUpdated = true;
  }

  deleteDetails(element) {
    this.elementToBeDeleted = element;
  }

  confirmDelete() {
    this.isUpdated = false;
    this._service.DeleteOtherDuesDetails(this.elementToBeDeleted.duesId, this.userName).subscribe(response => {
      if ((response != undefined || response != null) && response > 0) {
        let data = this.dataSource.data as any[];
        let index = data.indexOf(this.elementToBeDeleted);
        data.splice(index, 1);
        this.dataSource = new MatTableDataSource(data);
        this.elementToBeDeleted = null;
        this.totalCount = this.totalCount - 1;
        this.createForm();
        this.BindOtherDues(false);
        //this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
        //swal(this._msg.deleteMsg);
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteMsg;
      }
      else {
        //swal(this._msg.deleteFailedMsg);
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteFailedMsg;
      }
      this.DeletePopup = false;

      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);
    });
  }

  //cancel() {
  //  $(".dialog__close-btn").click();
  //}

  BindOtherDues(allDues) {
    this._service.GetOtherDues(allDues).subscribe(data => {
      this.ArrddlDues = data;
    })
  }

  BindMajorSectionCode() {
    this._service.GetMajorSectionCode().subscribe(data => {
      this.ArrddlMajSecCode = data;
    })

    this.otherDuesForm.controls.sectionCode.setValue('');
    this.otherDuesForm.controls.subSectionCode.setValue('');
    this.otherDuesForm.controls.sectionDesc.setValue('');
  }

  BindSectionCode(sectionCode) {
    this._service.GetSectionCode(sectionCode).subscribe(data => {
      this.ArrddlSecCode = data;
    })

    this.BindSubSectionCode();
    this.otherDuesForm.controls.sectionDesc.setValue('');
    this.otherDuesForm.controls.msEdrId.setValue(0);
  }

  BindSubSectionCode() {
    debugger;
    var selectedMajSecCode = this.otherDuesForm.controls.majorSectionCode.value;
    var selectedSecCode = this.otherDuesForm.controls.sectionCode.value;

    this._service.GetSubSectionCode(selectedMajSecCode, selectedSecCode).subscribe(data => {
      this.ArrddlSubSecCode = data;
      if (data.length == 1) {
        this.otherDuesForm.controls.subSectionCode.setValue(data[0].subSecCode)
      } else {
        if (!this.onEdit) {
          //this.otherDuesForm.controls.subSectionCode.setValue('');
        }
      }

      this.otherDuesForm.controls.sectionDesc.setValue('');
      if (this.onEdit) {
        this.BindSectionCodeDescription();
        this.onEdit = false;
      }
      else {
        if (this.ArrddlSubSecCode.length == 1) {
          this.otherDuesForm.controls.subSectionCode.setValue(this.ArrddlSubSecCode[0].subSecCode);
          this.BindSectionCodeDescription();
        } else {
          this.otherDuesForm.controls.subSectionCode.setValue('');
          this.otherDuesForm.controls.sectionDesc.setValue('');
        }
      }

    })
  }

  BindSubSectionCodeOnEdit(majSecCode, secCode) {
    debugger;
    this._service.GetSubSectionCode(majSecCode, secCode).subscribe(data => {
      this.ArrddlSubSecCode = data;
    })

    this.BindSectionCodeDescription();
  }

  BindSectionCodeDescription() {
    debugger;
    var selectedMajSecCode = this.otherDuesForm.controls.majorSectionCode.value;
    var selectedSecCode = this.otherDuesForm.controls.sectionCode.value;
    var selectedSubSecCode = this.otherDuesForm.controls.subSectionCode.value;

    this._service.GetSectionCodeDescription(selectedMajSecCode, selectedSecCode, selectedSubSecCode).subscribe(data => {
      if ((data != null || data != undefined) && data.length > 0) {
        this.ArrddlSecCodeDesc = data;
        this.otherDuesForm.controls.sectionDesc.setValue(data[0].secDesc);
        this.otherDuesForm.controls.msEdrId.setValue(data[0].secId);
      }
      this.isLoading = false;
    })
  }

  resetRateDetailPanel() {
    this.isRateDetailPanel = false;
    this.bgColor = '';
  }

}
