import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BankDetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getBankDetails(empcode: any, roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBankDetails}`, { params });
  }
  getBankDetailsByIFSC(ifscCD: any) {
    // tslint:disable-next-line:max-line-length
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBankDetailsByIFSC + '?ifscCD=' + ifscCD}`);
  }
  getAllIFSC() {
    // tslint:disable-next-line:max-line-length
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllIFSC}`);
  }

  UpdateBankDetails(objBankDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.UpdateBankDetails, objBankDetails, { responseType: 'text' });
  }
}
