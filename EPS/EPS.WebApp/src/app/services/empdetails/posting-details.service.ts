import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostingDetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  GetHRACity(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetHRACity }`);
  }
  GetTACity(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetTACity }`);
  }
  GetAllDesignation(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetAllDesignation }`);
  }
  SaveUpdatePostingDetails(objPostingDetails: any): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.SaveUpdatePostingDetails , objPostingDetails , { responseType: 'text' });
  }

  GetAllPostingDetails(empcode: any, roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', empcode).set('roleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetAllPostingDetails}`, {params});
  }
}
