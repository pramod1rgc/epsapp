import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetempcodeService {
  private subject = new Subject<any>();
  constructor() { }
  SendEmpCode(commonEmpCode: string , matselectedIndex: any) {
    this.subject.next({ empcode: commonEmpCode ,  selectedIndex: matselectedIndex });
}

getEmpCode(): Observable<any> {
  return this.subject.asObservable();
}
}
