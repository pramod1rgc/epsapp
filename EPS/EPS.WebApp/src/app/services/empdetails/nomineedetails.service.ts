import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NomineedetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getAllNomineeDetails(empcode: any , roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllNomineeDetails}`, { params });
  }
  getNomineeDetails(empcode: any, NomineeID: any): Observable<any> {
     const params = new HttpParams().set('EmpCd', empcode).set('NomineeID', NomineeID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getNomineeDetails}`, { params });
  }
  UpdateNomineeDetails(objNomineeDetails: any) {
    return this.http.post(this.config.api_base_url + this.config.UpdateNomineeDetails , objNomineeDetails , { responseType: 'text' });
  }
  DeleteNomineeDetails(EmpCd: string , NomineeID: any) {
    // tslint:disable-next-line:max-line-length
    return this.http.post(this.config.api_base_url + this.config.DeleteNomineeDetails + '?EmpCd=' + EmpCd + ' &NomineeID=' + NomineeID, { responseType: 'text' });
    // const params = new HttpParams().set('EmpCd', EmpCd).set('NomineeID', NomineeID);
    // return this.http.post<any>(`${this.config.api_base_url}${this.config.DeleteNomineeDetails}`, { params });
  }
}

