import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuaterDetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  getQuarterOwnedby(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getQuarterOwnedby}`);
  }
  getAllottedTo(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllottedTo}`);
  }
  getRentStatus(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getRentStatus}`);
  }
  getQuarterType(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getQuarterType}`);
  }
  GetCustodian(QrtrOwnedBy: number): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetCustodian + '?QrtrOwnedBy=' + QrtrOwnedBy}`);
  }
  GetGPRACityLocation(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetGPRACityLocation}`);
  }

  SaveUpdateQuarterDetails(objQuaterDetails: any) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.SaveUpdateQuarterDetails, objQuaterDetails, { responseType: 'text' });
  }


  QuarterAllDetails(empcode: any, roleId: any): Observable<any> {
      const params = new HttpParams().set('EmpCd', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.QuarterAllDetails}`, { params });
  }
  QuarterDetails(empcode: any, mSEmpAccmID: any): Observable<any> {
    const params = new HttpParams().set('EmpCd', empcode).set('MSEmpAccmID', mSEmpAccmID);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.QuarterDetails}`, { params });
  }

  DeleteQuarterDetails(EmpCd: string, msEmpAccmID: any) {
    return this.http.post(this.config.api_base_url + this.config.DeleteQuarterDetails + '?EmpCd=' + EmpCd + 
    ' &MSEmpAccmID=' + msEmpAccmID, { responseType: 'text' });
  }
}

