import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceDetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getMaintainByOfc(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getMaintainByOfc}`);
  }

  getAllServiceDetails(empcode: string, roleId: any): Observable<any> {
    const params = new HttpParams().set('EmpCode', empcode).set('RoleId', roleId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getServiceDetails}`, { params });
  }

  SaveUpdateServiceDetails(objServiceDetails: any): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.SaveUpdateServiceDetails, objServiceDetails, { responseType: 'text' });
  }

}
