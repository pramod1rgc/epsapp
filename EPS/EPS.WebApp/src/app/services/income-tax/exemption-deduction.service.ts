import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ExemptionDeductionService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  GetMajorSectionCode(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetMajorSectionCode`);
  }

  GetSectionCode(majorSecCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSectionCode?majorSecCode=${majorSecCode}`);
  }

  GetSubSectionCode(majorSecCode, sectionCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSubSectionCode?majorSecCode=${majorSecCode}&sectionCode=${sectionCode}`);
  }

  GetSectionCodeDescription(majorSectionCode, sectionCode, subSectionCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSectionCodeDescription?majorSectionCode=${majorSectionCode}&sectionCode=${sectionCode}&subSectionCode=${subSectionCode}`);
  }

  GetSlabLimit(payCommission): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSlabLimit?payCommission=${payCommission}`);
  }

  GetPayCommission(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetPayCommission`);
  }

  GetExemptionDeductionDetails(pageNumber, pageSize, searchTerm): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetExemptionDeductionDetails?pageNumber=${pageNumber}&pageSize=${pageSize}&searchTerm=${searchTerm}`);
  }
  
  UpsertExemptionDeductionDetails(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/UpsertExemptionDeductionDetails`, obj, { responseType: 'text' });
  }

  DeleteExemptionDeductionDetails(drrId, drrId2): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/DeleteExemptionDeductionDetails?drrId=${drrId}&drrId2=${drrId2}`, { responseType: 'text' });
  }

  GetFinancialYear(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.getfinancialYear}`);
  }

}
