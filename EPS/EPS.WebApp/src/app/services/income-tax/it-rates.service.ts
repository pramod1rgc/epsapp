import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable, throwError  } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ITRatesService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getItRates(finYear: string, rateType: string, rateFor: string): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetITRates?FinYear=${finYear}&RateType=${rateType}&RateFor=${rateFor}`);
  }

  upsertItRates(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/UpsertITRates`, obj).pipe(catchError(this.handleError));
  }

  deleteItRates(id, ddoId): Observable<any> {
    return this.http.delete(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/DeleteITRates/${id}/${ddoId}`);
  }

  getFinancialYear(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.getfinancialYear}`);
  }

  getAllRateTypeAndRateFor(type): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetRateTypeAndRateFor/${type}`);
  }

  private handleError(errorResponce: HttpErrorResponse) {
    if (errorResponce.error instanceof ErrorEvent) {
      console.log('client side error');
    } else {
      console.log('server side error');
    }
    return throwError('Something bad happened; please try again later.');
  }
}
