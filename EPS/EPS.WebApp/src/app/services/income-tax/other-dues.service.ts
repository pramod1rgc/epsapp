import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class OtherDuesService {

  //constructor() { }
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  GetOtherDues(allDues): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetOtherDues?allDues=${allDues}`);
  }

  GetMajorSectionCode(): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetMajorSectionCodeForDues`);
  }

  GetSectionCode(majorSecCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSectionCode?majorSecCode=${majorSecCode}`);
  }

  GetSubSectionCode(majorSecCode, sectionCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSubSectionCode?majorSecCode=${majorSecCode}&sectionCode=${sectionCode}`);
  }

  GetSectionCodeDescription(majorSectionCode, sectionCode, subSectionCode): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetSectionCodeDescription?majorSectionCode=${majorSectionCode}&sectionCode=${sectionCode}&subSectionCode=${subSectionCode}`);
  }
   
  getOtherDuesDetails(pageNumber, pageSize, searchTerm): Observable<any> {
    return this.http.get(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/GetOtherDuesDetails?pageNumber=${pageNumber}&pageSize=${pageSize}&searchTerm=${searchTerm}`);
  }

  SaveOtherDuesDetails(obj): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/SaveOtherDuesDetails`, obj, { responseType: 'text' });
  }

  DeleteOtherDuesDetails(duesId, userName): Observable<any> {
    return this.http.post(`${this.config.api_base_url}${this.config.IncomeTaxControllerName}/DeleteOtherDuesDetails?duesId=${duesId}&userName=${userName}`, { responseType: 'text' });
  }

}
