import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GpfWithdrawalRulesService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig) { }
  //rules = 'GpfWithdrawRules/GetGpfWithdrawRules';
  //rules1 = 'GpfWithdrawRules/GetGpfWithdrawRulesById';
  //InsertUpdateEmployeeDetails1 = 'GpfWithdrawRules/InsertUpdateGpfWithdrawRules';
  //Deleted = 'GpfWithdrawRules/DeleteGpfWithdrawRules?msGpfWithdrawRulesRefID=';
  getGpfWithdrawalRules(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GpfWithdrawalRules}`);
  }

  getGpfWithdrawalRulesByID(id:any): Observable<any> {
  //  const params = new HttpParams().set('msGpfWithdrawRulesRefID', id);
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GpfWithdrawalRulesByID}` + '?msGpfWithdrawRulesRefID=' + id);
  }

  insertUpdateGpfWithdrawalRules(GpfWithdrawalRules): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.InsertUpdateGpfWithdrawalRules, GpfWithdrawalRules);
  }

  deleteGpfWithdrawalRules(id: any): Observable<any> {
    return this.http
      .post<any>(this.config.api_base_url + this.config.DeleteGpfWithdrawalRules + id, { responseType: 'text' });
  }
}
