
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Injectable, Inject } from '@angular/core';
import { DaMasterModel } from '../../model/masters/da-master-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DamastersService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }
  Delete(msDaMasterID) {
    return this.httpclient.post(this.config.DeleteDaMaster +msDaMasterID, '', { responseType: 'text' });
    //return this.httpclient.post(this.config.api_base_url + 'DaStateCenteral/Delete?msDaMasterID=' + msDaMasterID, '', { responseType: 'text' });
  }
  InsertandUpdate(_objdamaster: any) {
    return this.httpclient.post(this.config.InsertandUpdate, _objdamaster, { responseType: 'text' });
  }
  GetDADetails(): Observable<any> {

    return this.httpclient.get<any>(this.config.GetDADetails, { });
  }
  GetDACurrent_Rate_Wef(DaType, SubType) {

    return this.httpclient.get<any>(this.config.GetDACurrent_Rate_Wef + DaType + '&SubType=' + SubType, { });
  }

  GetDACurrent_Rate_WefForState(DaType,StateId) {
    return this.httpclient.get<any>(this.config.GetDACurrent_Rate_Wef_state +DaType + '&StateId=' + StateId, {});

    //return this.httpclient.get<any>(this.config.api_base_url + 'DaStateCenteral/GetDACurrent_Rate_Wef_state?DaType=' + DaType + '&StateId=' + StateId, {});

  }
}
