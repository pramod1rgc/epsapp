import { TestBed } from '@angular/core/testing';

import { GpfWithdrawalRulesService } from './gpf-withdrawal-rules.service';

describe('GpfWithdrawalRulesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpfWithdrawalRulesService = TestBed.get(GpfWithdrawalRulesService);
    expect(service).toBeTruthy();
  });
});
