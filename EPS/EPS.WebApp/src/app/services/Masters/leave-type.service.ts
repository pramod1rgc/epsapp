import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { text } from '@angular/core/src/render3';


@Injectable({
  providedIn: 'root'
})
export class LeaveTypeService {

  
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'

    })
  };
  getLeaveTypeDetails(): Observable<any> {

    return this.http.get<any>(`${this.config.getMstLeaveTypeList}`);
  }
  getLeaveTypeMaxid(): Observable<any> {

    return this.http.get<any>(`${this.config.getMstLeavetypeMaxid}`);
  }
  upadateMstLeavetype(objleavetype): Observable<any> {
    debugger;
    return this.http.post(`${this.config.upadateMstLeavetype}`, objleavetype, { responseType: "text" });
  }
 InsertMstLeavetype(objleavetype): Observable<any> {
   debugger;
   return this.http.post(`${this.config.insertLeavetype}`, objleavetype, { responseType: "text" });
  }
  DeleteMstLeavetype(msleavetypeId): Observable<any> {
    debugger;
    return this.http.post(this.config.deleteLeavetype +'?msLeavetypeId='+ msleavetypeId, '', { responseType: 'text' });
  }
}
