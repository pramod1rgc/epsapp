import { TestBed } from '@angular/core/testing';

import { FdgpfMasterService } from './fdgpf-master.service';

describe('FdgpfMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FdgpfMasterService = TestBed.get(FdgpfMasterService);
    expect(service).toBeTruthy();
  });
});
