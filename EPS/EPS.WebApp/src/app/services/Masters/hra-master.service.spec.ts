import { TestBed } from '@angular/core/testing';

import { HraMasterService } from './hra-master.service';

describe('HraMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HraMasterService = TestBed.get(HraMasterService);
    expect(service).toBeTruthy();
  });
});
