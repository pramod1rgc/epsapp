import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { text } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class StateAgService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  getMsStateAglist(): Observable<any> {

    return this.http.get<any>(`${this.config.getMsStateAglist}`);
  }
  getMsStateList(): Observable<any> {

    return this.http.get<any>(`${this.config.getMsStateList}`);
  }
  getMsPfTypeList(): Observable<any> {

    return this.http.get<any>(`${this.config.getMsPfTypeList}`);
  }
  upadateMsStateAg(objStateAg): Observable<any> {
    debugger;
    return this.http.post(`${this.config.upadateMstLeavetype}`, objStateAg, { responseType: "text" });
  }
  InsertMsStateAg(objStateAg): Observable<any> {
    debugger;
    return this.http.post(`${this.config.insertLeavetype}`, objStateAg, { responseType: "text" });
  }
}
