import { TestBed } from '@angular/core/testing';

import { GpfadvancerulesService } from './gpfadvancerules.service';

describe('GpfadvancerulesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpfadvancerulesService = TestBed.get(GpfadvancerulesService);
    expect(service).toBeTruthy();
  });
});
