import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GpfinterestrateService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }
  InsertandUpdate(_objgpfmaster: any) {

    return this.httpclient.post(this.config.GpfInterestRateInsertandUpdate, _objgpfmaster, { responseType: 'text' });
  }

  GetGPFInterestRateDetails() {
    return this.httpclient.get<any>(this.config.GetGPFInterestRateDetails);
  }


  Delete(gpfInterestID) {
    return this.httpclient.post(this.config.DeleteGpfInterestRate + gpfInterestID, '', { responseType: 'text' });
    //return this.httpclient.post(this.config.api_base_url + 'GPFInterestRate/Delete?gpfInterestID=' + gpfInterestID,'', { responseType: 'text'});
  } 
}
