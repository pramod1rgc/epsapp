import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs'; //from net
import { AppConfig, APP_CONFIG } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class DDOMasterService {
  //BaseUrl: any = [];

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    //this.BaseUrl = 'http://localhost:55424/api/';
  }
  GetAllController(): Observable<any> {
    debugger;
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetAllController`);
  }
  GetPAOByControllerId(ControllerId: string): Observable<any> {
    debugger;
    const params = new HttpParams().set('ControllerId', ControllerId);
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetPAOByControllerId`, { params });
  }
  GetODdoByPao(PaoCode: string, ): Observable<any> {
    debugger;
    const params = new HttpParams().set('PaoID', PaoCode);
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetODdoByPao`, { params });
  }
  GetDDOcategory(): Observable<any> {
    debugger;
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetDDOcategory`);
  }
  GetState(): Observable<any> {
    debugger;
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetState`);
  }
  GetCity(StateId: string): Observable<any> {
    debugger;
    const params = new HttpParams().set('StateId', StateId);
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetCity`, { params });
  }
  GetDDOMaster(): Observable<any> {
    debugger;
    return this.httpclient.get<any>(`${this.config.DDOMasterControllerName}/GetDDOMaster`);
  }
  SaveDDOMaster(_objDdoMasterModel: any) {
    debugger;
    return this.httpclient.post(`${this.config.DDOMasterControllerName}/SaveDDOMaster`, _objDdoMasterModel);
  }
  //DeleteDDOMaster(_objDdoMasterModel: any) {
  //  debugger;
  //  return this.httpclient.post(`${this.config.DDOMasterControllerName}/DeleteDDOMaster`, _objDdoMasterModel);
  //}

  DeleteDDOMaster(ddoId: string): Observable<any> {
    debugger;
    const params = new HttpParams().set('ddoId', ddoId);
    return this.httpclient.delete<any>(`${this.config.DDOMasterControllerName}/DeleteDDOMaster`, { params });
  }
}












