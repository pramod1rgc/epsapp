import { TestBed } from '@angular/core/testing';

import { GpfinterestrateService } from './gpfinterestrate.service';

describe('GpfinterestrateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GpfinterestrateService = TestBed.get(GpfinterestrateService);
    expect(service).toBeTruthy();
  });
});
