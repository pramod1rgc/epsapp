import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { DuesDefinitionmasterModel } from '../../model/masters/DuesDefinitionmasterModel';


@Injectable({
  providedIn: 'root'
})
export class DuesDefinitionServicesService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  InsertUpdateDuesDefinationMaster(objDuesDefinationmaster): Observable<any> {


    return this.http.post(this.config.api_base_url + this.config.InsertUpdateDuesDefinationMaster, objDuesDefinationmaster, { responseType: 'text' })

  }

  getDuesDefinationmasterList(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetDuesDefinationmaster}`);
  }

  DeleteDuesmasterbyDuesCode(DuesCd: string): Observable<any> {

    return this.http.post<DuesDefinitionmasterModel>(this.config.api_base_url + 'DuesDefinationandDuesMaster/DeleteDuesDefinationmasterDetails?DuesCd=' + DuesCd, { responseType: 'text' });
    //.get<EmployeeDetailsModel>(`${this.config.api_base_url}${this.config.GetEmpPHDetails + EmpCd}`);
  }

  GetDuesDefinationMasterDetailsByID(DuesCd: DuesDefinitionmasterModel): Observable<any> {

    return this.http
      .get<DuesDefinitionmasterModel>(`${this.config.api_base_url}${this.config.GetDuesDefinationMasterDetailsByID + DuesCd}`);
  }

  UpdateDuesDefinationMasterDetails(empdetails) {
    return this.http.post(this.config.api_base_url + this.config.UpdateEmpDetails, empdetails, { responseType: 'text' });
  }

  GetAutoGenratedDuseCode(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetAutoGenratedDuesCode}`);
  }

}
