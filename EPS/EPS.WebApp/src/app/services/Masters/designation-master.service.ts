import { Injectable ,Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { DesignationMasterModel } from '../../model/masters/designation-master';
import { text } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class DesignationMasterService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  
 getMsdesignDetails(): Observable<any> {

    return this.http.get<any>(`${this.config.getMsdesignlist}`);
  }
  getMsdesignMaxid(): Observable<any> {

    return this.http.get<any>(`${this.config.getMsdesignMaxid}`);
  }
  updateMsdesign(objMsdesign): Observable<any> {
    debugger;
    return this.http.post(`${this.config.updateMsdesign}`, objMsdesign, { responseType: 'text' });
  }
  insertMsdesign(objMsdesign): Observable<any> {
    debugger;
    return this.http.post(`${this.config.insertMsdesign}`, objMsdesign, { responseType: 'text' });
  }
  DeleteMsdesign(CommonDesigSrNo): Observable<any> {
    debugger;
    //return this.http.post(`${this.config.deleteMsdesign + CommonDesigSrNo}`, '', { responseType: 'text' });
    return this.http.post(this.config.deleteMsdesign +'?CommonDesigSrNo='+CommonDesigSrNo, '', { responseType: 'text' });
  }
}
