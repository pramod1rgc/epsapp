import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { PraoControllerModel } from '../../model/masters/prao-master-model';

@Injectable({
  providedIn: 'root'
})
export class PraoMasterService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  BindControllerCode(): Observable<any> {

    return this.http.get(`${this.config.api_base_url}${this.config.GetControllerCode}`);
  }
  BindCtrlName(CtrlrCode:string): Observable<any> {

    return this.http.get(`${this.config.api_base_url}${this.config.BindCtrlName + CtrlrCode}`);
  }

  BindState(): Observable<any> {

    return this.http.get(`${this.config.api_base_url}${this.config.BindState}`);
  }
  BindDistrict(stateId: string) {
    return this.http.get(`${this.config.api_base_url}${this.config.BindDistrict + stateId}`);
  }

  SaveOrUpdate(PraoModel) {
   
    return this.http.post(this.config.api_base_url + this.config.SaveOrUpdate, PraoModel);
  }

  BindPraoMasterDetails(){
 
  return this.http.get(this.config.api_base_url + this.config.BindPraoMasterDetails);
  }

  ViewEditOrDeletePrao(CtrlCode:string, controllerID:string, Mode:string) {
   
    //return this.http.post(this.config.api_base_url + this.config.ViewEditOrDeletePrao, PraoModel);
    return this.http.get(this.config.ViewEditOrDeletePrao + "?CtrlCode=" + CtrlCode + "&controllerID=" + controllerID + "&Mode=" + Mode, {});
  }


  //getAllUser(username, uroleid) {
  //  return this.httpclient.get(this.config.getAllMenusByUser + "?username=" + username + "&uroleid=" + uroleid, {});
  //}
  

}
