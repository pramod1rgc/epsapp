import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FdgpfMasterService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  //Family Definition GPF Master Services

  CreatefdGpfMaster(hraObj) {
    return this.httpclient.post(this.config.CreatefdGpfMaster, hraObj, { responseType: 'text' });
  }


  GetFdGpfMasterDetails(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetFdGpfMasterDetails, {});
  }

  EditFdGpfMasterDetails(fdMasterID): Observable<any> {
    return this.httpclient.get<any>(this.config.EditFdGpfMasterDetails + fdMasterID, {});
  }


  DeleteFdGpfMaster(fdMasterID: number) {
    return this.httpclient.post(this.config.DeleteFdGpfMaster + fdMasterID, '', { responseType: 'text' });
  }


}
