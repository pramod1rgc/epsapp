import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { IDeductionRate } from '../../masters/deduction-rate/IDeductionRate';

@Injectable({
  providedIn: 'root'
})
export class deductionMasterService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

  categoryList(): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.categoryList);
  }

  deductionDescription(): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.deductionDescription);
  }

  deductionDescriptionChange(payItemsCD): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.deductionDescriptionChange + payItemsCD);
  }

  // get Deduction Definition List
  getDeductionDefinitionList(): Observable<any[]> {
    return this.httpclient.get<any[]>(this.config.getDeductionDefinitionList);
  }

  deductionDefinitionSubmit(objDeductionDefinition): Observable<any>  {
    return this.httpclient.post(this.config.deductionDefinitionSubmit, objDeductionDefinition, { responseType: 'text' });
  }
    //======================Deduction Rate Master====================
    
  getOrginzationTypeForDeductionRate(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetOrgnaztionTypeForDeductionRate}`);
  }

  getStateForDeductionRate(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetStateForDeductionRate}`);
  }

  getPayCommForDeductionRate(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetPayCommForDeductionRate}`);
  }


  getDeductionCodeDeductionDefination(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetDeductionCodeDeductionDefination}`);
  }

  getCityClassForDeductionRate(payCommId: number) {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetCityClassForDeductionRate + payCommId}`);
  }

  addDeductionRate(employee: IDeductionRate): Observable<IDeductionRate> {
    return this.httpclient.post<IDeductionRate>(this.config.api_base_url + this.config.AddDeductionDetails, employee)
  }


  getSlabTypeByPayCommId(payCommId: number) {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetDeductionSlabTypeByPayCommId + payCommId}`);
  }

  insertUpateDeductionRateDetails(objDuesRateandDuesMasterModel: any): Observable<any> {
    return this.httpclient.post<any>(this.config.api_base_url + this.config.saveUpdateDeductionRateDetails, objDuesRateandDuesMasterModel)
  }

  getDeductionRateMasterList(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.api_base_url}${this.config.GetDeductionRateList}`);
  }

  deleteDeductionRateMaster(msduesratedetailsid: number) {
    return this.httpclient.post(this.config.api_base_url + 'DeductionMaster/DeleteDeductionRatemaster?msduesratedetailsid=' + msduesratedetailsid, { responseType: 'text' });
  }

}
