import { TestBed } from '@angular/core/testing';

import { TptaMasterService } from './tpta-master.service';

describe('TptaMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TptaMasterService = TestBed.get(TptaMasterService);
    expect(service).toBeTruthy();
  });
});
