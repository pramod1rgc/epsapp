import { TestBed } from '@angular/core/testing';

import { DamastersService } from './damasters.service';

describe('DamastersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DamastersService = TestBed.get(DamastersService);
    expect(service).toBeTruthy();
  });
});
