import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import {SuspensionDetails } from '../../model/Suspension/SuspensionDetails';
import {ExtensionDetails } from '../../model/Suspension/ExtensionDetails';
import {RevocationDetails} from '../../model/Suspension/RevocationDetails';
import { JoiningDetails } from '../../model/Suspension/JoiningDetails';
import { RegulariseDetails } from '../../model/Suspension/RegulariseDetails';

@Injectable({
  providedIn: 'root'
})
export class SuspensionService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  GetAllDesignation(Id: string): Observable<any> {
    const params = new HttpParams().set('id', Id);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetAllDesignation`, {params});
  }

  GetAllEmpOnSelectedDesignation(desigId: string, isCheckerLogin: string, comName: string): Observable<any> {
    const params = new HttpParams().set('desigId', desigId).set('isCheckerLogin', isCheckerLogin).set('comName', comName);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetAllEmployees`, {params});
  }

  SavedNewSuspensionDetails(objsus: SuspensionDetails) {
        return this.http.post(`${this.config.api_base_url}${this.config.suspensionControllerName}/SavedNewSuspensionDetails`,
         objsus, { responseType: 'text' });
      }

      SavedExtensionDetails(objsus: ExtensionDetails) {
        return this.http.post(`${this.config.api_base_url}${this.config.suspensionControllerName}/SavedExtensionDetails`,
         objsus, { responseType: 'json' });
      }

      SavedRevocationDetails(objRevok: RevocationDetails) {
        return this.http.post(`${this.config.api_base_url}${this.config.suspensionControllerName}/SavedRevocationDetails`,
        objRevok, { responseType: 'json' });
      }

      GetSuspensionDetails(empId: string, isCheckerLogin: string, comName: string): Observable<any> {

        const params = new HttpParams().set('empCode', empId).set('isCheckerLogin', isCheckerLogin).set('comName', comName);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetSuspensionDetails`, {params});
      }

      DeleteSuspensionDetails(id: any, compName: string) {
        const param = new HttpParams().set('id', id).set('compName', compName);
       return this.http.delete<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/DeleteSuspensionDetails`,
        {params: param,  responseType: 'json'});
      }

      VerifiedAndRejectedSusDetails(id: string, status: string, reason: string) {
        const param = new HttpParams().set('idAndcompName', id).set('status', status).set('reason', reason);
        return this.http.post<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/VerifiedAndRejectedSuspension`, null,
        {params: param, responseType: 'json'});
      }

      GetRevocationDetails(selectedvalue: any, empId: any, isCheckerLogin: string): Observable<any> {

        const params = new HttpParams().set('susId', selectedvalue).set('empId', empId).set('isCheckerLogin', isCheckerLogin);
        return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetRevocationDetails`, {params});
      }

      SavedJoiningDetails(objJoining: JoiningDetails) {
        return this.http.post(`${this.config.api_base_url}${this.config.suspensionControllerName}/SavedJoiningDetails`,
        objJoining, { responseType: 'json' });
      }

      GetJoiningDetails(selectedvalue: any, isCheckerLogin: string) {
        const params = new HttpParams().set('joinId', selectedvalue).set('isCheckerLogin', isCheckerLogin);
        return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetJoiningDetails`, {params});
      }

      SavedRegulariseDetails(objRegular: RegulariseDetails) {
        return this.http.post(`${this.config.api_base_url}${this.config.suspensionControllerName}/SavedRegulariseDetails`,
        objRegular, { responseType: 'json' });
      }

      GetRegulariseDetails(selectedvalue: any, isCheckerLogin: string) {
        const params = new HttpParams().set('joiningId', selectedvalue).set('isCheckerLogin', isCheckerLogin);
        return this.http.get<any>(`${this.config.api_base_url}${this.config.suspensionControllerName}/GetRegulariseDetails`, {params});
      }


}

