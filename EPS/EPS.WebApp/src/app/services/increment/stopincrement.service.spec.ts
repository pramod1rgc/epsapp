import { TestBed } from '@angular/core/testing';

import { StopincrementService } from './stopincrement.service';

describe('StopincrementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StopincrementService = TestBed.get(StopincrementService);
    expect(service).toBeTruthy();
  });
});
