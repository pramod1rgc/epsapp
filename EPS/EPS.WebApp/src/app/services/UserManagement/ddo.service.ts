import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class ddoservice {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  //getAllControllerServiceclient(controllerID, Query) {
  //  // debugger;
  //  //return this.httpclient.get(this.BaseUrl + 'Controllerrole/GetAllControllers?controllerID=' + controllerID + '&Query=' + Query, {});
  //  return this.httpclient.get(this.config.getAllControllers + controllerID + '&Query=' + Query, {});
  //}

  GetAllDDO(PAOID, Query) {
    //debugger;
   // return this.httpclient.get(this.BaseUrl + 'ddorole/GetAllDDO?PAOID=' + PAOID + '&Query=' + Query, {});
    return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
  }
  EmployeeList(DDOID) {
   // debugger;
    //return this.httpclient.get(this.BaseUrl + 'ddorole/EmployeeList?DDOID=' + DDOID, {});
    return this.httpclient.get(this.config.EmployeeList + DDOID, {});
  }

  Assigned(empCd, DDOID, Active) {
    //debugger;
    //alert(Active)
    //return this.httpclient.get(this.BaseUrl + 'ddorole/Assigned?empCd=' + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
    return this.httpclient.get(this.config.Assigned + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
  }

  GetAllUserRoles(username) {
    //debugger;
    //return this.httpclient.get(this.BaseUrl + 'Controllerrole/GetAllUserRoles?username=' + username, {});
    return this.httpclient.get(this.config.GetAllUserRoles + username, {});
  }
  AssignedEmpDDO(DDOID) {
    return this.httpclient.get(this.config.AssignedEmpDDO + DDOID, {});
  }
  AssignedDDO(DDOID) {
    return this.httpclient.get(this.config.AssignedDDO + DDOID, {});
  }
}
