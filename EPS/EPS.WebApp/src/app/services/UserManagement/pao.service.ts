import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class paoservice {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

 
  GetAllpaos(controllerID, Query) {
    return this.httpclient.get(this.config.GetAllpaos+ controllerID + '&Query=' + Query, {});
  }

  onpaoSelectchanged(MsPAOID) {
    return this.httpclient.get(this.config.onpaoSelectchanged + MsPAOID, {});
  }

  GetAllDDO(PAOID, Query) {
    return this.httpclient.get(this.config.GetAllDDO + PAOID + '&Query=' + Query, {});
  }

  Assigned(empCd, MsPAOID, Active) {
    return this.httpclient.get(this.config.AssignedPAO + empCd + '&MsPAOID=' + MsPAOID + '&Active=' + Active, { responseType: 'text'});
  }

  AssignedEmpList(MsPAOID) {
    
    return this.httpclient.get(this.config.AssignedEmpList + MsPAOID, {});
  }
  AssignedPAOEmp(MsPAOID) {

    return this.httpclient.get(this.config.AssignedPAOEmp + MsPAOID, {});
  }
}
