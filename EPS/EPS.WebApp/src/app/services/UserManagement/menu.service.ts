import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {

    //this.BaseUrl = 'http://localhost:55424/api/';
    //this.BaseUrl = 'http://10.199.72.57/epsapi/api/';
  }

  SaveMenu(hdnMenuId, uRoleID, MstRole) {
    //alert(uRoleID);
    return this.httpclient.post(this.config.saveMenu + hdnMenuId + '&uRoleID=' + uRoleID, MstRole, { responseType: 'text' });
  }
  BindDropDownMenu() {
      return this.httpclient.get(this.config.bindDropDownMenu, {});

  }
   BindMenuInGride(): Observable<any> {
    return this.httpclient.get<any>(this.config.bindMenuInGride, {});
  }
  FillUpdateMenu(MsMenuID): Observable<any> {
    return this.httpclient.get<any>(this.config.FillUpdateMenu + MsMenuID, {});
  }
  ActiveDeactiveMenu(MstMenu) {    
    return this.httpclient.post(this.config.activeDeactiveMenu, MstMenu, { responseType: 'text' });
  }
  GetPredefineRole(): Observable<any> {
    //alert("all");
    return this.httpclient.get<any>(`${this.config.GetPredefineRole}`);
  }

}
