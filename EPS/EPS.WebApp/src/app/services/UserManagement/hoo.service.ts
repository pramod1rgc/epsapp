import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class hooservice {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  hooCheckerEmpList(DDOID) {
    return this.httpclient.get(this.config.HooCheckerEmpList + DDOID, {});
  }

  AssignedHOOChecker(empCd, DDOID, Active) {
    return this.httpclient.get(this.config.AssignedHOOChecker + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
  }

  hooMakerEmpList(DDOID) {
    return this.httpclient.get(this.config.hooMakerEmpList + DDOID, {});
  }
  AssignedHOOMaker(empCd, DDOID, Active) {
    return this.httpclient.get(this.config.AssignedHOOMaker + empCd + '&DDOID=' + DDOID + '&Active=' + Active, { responseType: 'text' });
  }
 
  SelfAssignHOOChecker(UserID, username, EmpPermDDOId, ddoid, IsAssign) {
    return this.httpclient.get(this.config.SelfAssignHOOChecker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
  }
  SelfAssignHOOCheckerRole(UserID) {
    return this.httpclient.get(this.config.SelfAssignHOOCheckerRole + UserID, { responseType: 'text' });
  }
}

