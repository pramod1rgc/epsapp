import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';

@Injectable({
  providedIn: 'root'
})
export class makerservice {
  
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

  makerEmpList(DDOID) {
    return this.httpclient.get(this.config.makerEmpList + DDOID, {});
  }

  AssignedMakerEmpList(DDOID) {
    return this.httpclient.get(this.config.AssignedMakerEmpList + DDOID, {});
  }
  
  AssignedMaker(empCd, DDOID, UserID, Active) {
    return this.httpclient.get(this.config.AssignedMaker + empCd + '&DDOID=' + DDOID + '&UserID=' + UserID+ '&Active=' + Active, { responseType: 'text' });
  }

  SelfAssignedDDOMaker(UserID, username, EmpPermDDOId, ddoid, IsAssign) {
    return this.httpclient.get(this.config.SelfAssignedDDOMaker + UserID + '&username=' + username + '&EmpPermDDOId=' + EmpPermDDOId + '&ddoid=' + ddoid + '&IsAssign=' + IsAssign, { responseType: 'text' });
  }
  SelfAssignMakerRole(UserID) {
    return this.httpclient.get(this.config.SelfAssignMakerRole + UserID, { responseType: 'text' });
  }
}
