import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ExistLoanService {
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  public _id: string;
  public _roleid: number;
  get Empcode() {
    return this._id;
  }

  set Empcode(val) {
    this._id = val;
  }
  get RoleId() {
    return this._roleid;
  }
  set RoleId(val) {
    this._roleid = val;
  }

  BindDropDownDesign(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
  }
  BindDropDownBillGroup(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownBillGroup}`);
  }
  BindDropDownEmp(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownEmp}`);
  }
  
  BindLoanType() {
     
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getExistingLoanType}`);
  }
  BindHeadAccount(LoanCD:number) {
     
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindHeadAccount + LoanCD}`);
  }

 
  GetExistLoanDetailsByID(MSPayAdvEmpdetID: string): Observable<any> {

    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.GetExistLoanDetailsByID + MSPayAdvEmpdetID}`);
  }
  PostData(ArrLoanSection) {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.ExistingLoanInsertData, ArrLoanSection);
  }

  EmployeeInfo(MsDesignID: string) {   
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllEmpNameSelectedDesignId + MsDesignID}`);
  }
  
  LoanSection(ddoid:number , EmpCode:string): Observable<any> {
    debugger;
    const params = new HttpParams().set('EmpCode', EmpCode).set('PermDdoId', ddoid.toString());
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getLoanAdvanceDetails}/LoanAdvanceDetails` , { params });
    //return this.http.get<any>(`${this.config.api_base_url}${this.config.getLoanAdvanceDetails + ddoid +'&EmpCode=' + EmpCode}`);
  }


  // For GetList Data
  GetEmpLoanDetails(PayBillGpID: string): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetExistingEmpLoanDetails}`);
  }

  BindExistingLoanStatus(ddoId: number, UserID: string, userRoleID:string ): Observable<any> {
    debugger;
    const params = new HttpParams().set('UserID', UserID).set('userRoleID', userRoleID).set('ddoId', ddoId.toString());
   // return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus + ddoId + '&UserID=' + UserID }`);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus}`, { params });
  }

  //BindExistingLoanStatus(userid: string): Observable<any> {
  //  const params = new HttpParams().set('Userid', userid);
  //  return this.http.get<any>(`${this.config.api_base_url}${this.config.ExistingLoanStatus}/BindExistingLoanStatus`, { params });
  //}




  ExistingLoanForwordToDDO(ArrLoanSection) {
    return this.http.post(this.config.api_base_url + this.config.ExistingLoanForwordToDDO, ArrLoanSection);
  }

  PopulateEmpData(desigId:number): Observable<any> { 
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindHeadAccount + desigId}`);
  }

  

  DeleteEmpDetails(CtrlCode: string, msPayAdvEmpdetID: string, Mode:number ) {
    debugger;
    //return this.http.get(this.config.ViewEditOrDeletePrao + "?CtrlCode=" + CtrlCode + "&controllerID=" + controllerID + "&Mode=" + Mode, {});
    //return this.http.post(this.config.api_base_url + this.config.ViewEditOrDeletePrao, PraoModel);
    return this.http.get(this.config.DeleteEmpDetails + "?CtrlCode=" + CtrlCode + "&msPayAdvEmpdetID=" + msPayAdvEmpdetID+ "&Mode=" + Mode, {});
  }

}
