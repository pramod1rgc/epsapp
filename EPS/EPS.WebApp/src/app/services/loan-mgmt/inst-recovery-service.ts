import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InstRecoveryService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    //this.BaseUrl = 'http://localhost:55424/api/';
  }

  GetRecoveryEmpDetails(value) {
    debugger;
    const params = new HttpParams().set('EmpCd', value);
    return this.httpclient.get<any>(`${this.config.MultipleInstRecoveryController}/GetRecoveryEmpDetails`, { params }); 
  }
  BindSanctionDetails(value,EmpCD) {
    const params = new HttpParams().set('LoanCd', value).set('EmpCD', EmpCD);
    return this.httpclient.get<any>(`${this.config.MultipleInstRecoveryController}/BindSanctionDetails`, { params }); 
  }
  SaveData(EmpSanc: any) {
    debugger;
    return this.httpclient.post<any>(`${this.config.MultipleInstRecoveryController}/SaveSanctionData`, EmpSanc); 
  }

  ForwardtoDDOChecker(EmpSanc: any) { 
    return this.httpclient.post<any>(`${this.config.MultipleInstRecoveryController}/ForwardtoDDOChecker`, EmpSanc); 
  }
  GetInstEmpDetails(EmpCd :any) {
    const params = new HttpParams().set('EmpCD', EmpCd);
    return this.httpclient.get<any>(`${this.config.MultipleInstRecoveryController}/GetInstEmpDetails`, { params }); 
  }

  Accept(EmpCD: any) {
    debugger;
      const params = new HttpParams().set('EmpCD', EmpCD);
      //return this.httpclient.post(`${this.config.MultipleInstRecoveryController}/Accept`, EmpCD, { responseType:'text' });
   return this.httpclient.get<any>(`${this.config.MultipleInstRecoveryController}/Accept`, { params});
  }

  Reject(EmpCD: any) {
    return this.httpclient.post<any>(`${this.config.MultipleInstRecoveryController}/Reject`, EmpCD);
  }

  EditLoanDetailsById(InstEmpDetais: any) {
    //const params = new HttpParams().set('msPayAdvEmpdetID', msPayAdvEmpdetID).set('EmpCd', EmpCd);
    return this.httpclient.post<any>(`${this.config.MultipleInstRecoveryController}/EditLoanDetailsById`, InstEmpDetais);
  }

  
  DeleteLoanDetailsById(msEmpLoanMultiInstId: any, empCD: any) {
    debugger;
    const params = new HttpParams().set('msEmpLoanMultiInstId', msEmpLoanMultiInstId).set('empCD', empCD);
    return this.httpclient.delete<any>(`${this.config.MultipleInstRecoveryController}/DeleteLoanDetailsById`, { params});
  }

}

