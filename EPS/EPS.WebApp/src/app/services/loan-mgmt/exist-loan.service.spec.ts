import { TestBed } from '@angular/core/testing';

import { ExistLoanService } from './exist-loan.service';

describe('ExistLoanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExistLoanService = TestBed.get(ExistLoanService);
    expect(service).toBeTruthy();
  });
});
