import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CommanService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  GetAllPayBillGroup(PermDdoId: string,Flag:string): Observable<any> {
    const params = new HttpParams().set('permddoId', PermDdoId).set('Flag', Flag);
   return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetPayBillGroupByPermDDOId`, { params });

  }
  GetAllDesignation(PayBillGrpId: string, Flag: string): Observable<any> {
    const params = new HttpParams().set('billGroupId', PayBillGrpId).set('Flag', Flag);;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation`, { params });
  }

  GetAllDesignationByBillgrID(PayBillGrpId: string, PermDdoId: string, Flag: string): Observable<any> {
    const params = new HttpParams().set('billGroupId', PayBillGrpId).set('PermDdoId', PermDdoId).set('Flag', Flag);;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignationByBillgrID`, { params });
  }

  GetAllEmp(DesigCode: string, BillgrID: string, Flag: string): Observable<any> {
    const params = new HttpParams().set('desigId', DesigCode).set('BillgrID', BillgrID).set('Flag', Flag);;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpfilterbydesignBillgroup}/GetEmpfilterbydesignBillgroup`, { params });

  }

  GetAllEmpByBillgrIDDesigID(DesigCode: string, BillgrID: string, PermDdoId: string, Flag: string): Observable<any> {
    const params = new HttpParams().set('desigId', DesigCode).set('BillgrID', BillgrID).set('PermDdoId', PermDdoId).set('Flag', Flag);;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpfilterbydesignBillgroup}/GetEmpfilterbydesignBillID`, { params });

  }

  GetBillgroupfilterbydesignID(DesigCode: string, Flag: string): Observable<any> {
    const params = new HttpParams().set('desigId', DesigCode).set('DesigCode', DesigCode).set('Flag', Flag);;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetBillgroupfilterbydesignID}/GetBillgroupfilterbydesignID`, { params });

  }


  BindDesignation(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownDesign}`);
  }
  BindBillGroup(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownBillGroup}`);
  }
  BindDropDownEmp(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getBindDropDownEmp}`);
  }

  BindLoanType(loantypeFlag:string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetLoanType + loantypeFlag}`);
  }

  GetEmpCode(username: string): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetEmpDetails + username}`);
  }
  Loantypeandpurposestatus(LoanBillID: string) {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.GetPayLoanPurposeStatus + LoanBillID}`);
  }

  DeleteLoanDetailsByEMPID(id: number, mode: number): Observable<any> {
    return this.http
      .get<any>(`${this.config.api_base_url}${this.config.DeleteLoanDetailsByEMPID + id + '&mode=' + mode}`);
  }
}
