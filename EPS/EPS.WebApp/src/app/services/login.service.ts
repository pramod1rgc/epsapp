import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient) {
    //this.BaseUrl = 'http://localhost:55424/api/';
    this.BaseUrl = 'http://10.199.72.53/epsapi/api/';
  }

  login(userId, password, logintype) {
 
   // return this.httpclient.post(this.BaseUrl + 'Login/Login', { userId, password, logintype}, { responseType: 'text' });
    return this.httpclient.post(this.BaseUrl + 'Login/Login?userId=' + userId + '&password='+password + '&logintype=' + logintype, {}, { responseType: 'text' });
  }
}
