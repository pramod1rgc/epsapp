import { TestBed } from '@angular/core/testing';

import { EndofserviceService } from './endofservice.service';

describe('EndofserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EndofserviceService = TestBed.get(EndofserviceService);
    expect(service).toBeTruthy();
  });
});
