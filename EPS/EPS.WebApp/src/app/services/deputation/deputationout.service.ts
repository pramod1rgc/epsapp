import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeputationoutService {
  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }


  getAllDepuatationOutDetails(EmpCd: any, roleId: number): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllDepuatationOutDetailsByEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId}`);
  }
  InsertUpateDeputationOutDetails(deputationoutdetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateDeputationOutDetails, deputationoutdetails, { responseType: 'text' })
  }
}
