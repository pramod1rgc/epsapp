import { TestBed } from '@angular/core/testing';

import { DeputationinService } from './deputationin.service';

describe('DeputationinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeputationinService = TestBed.get(DeputationinService);
    expect(service).toBeTruthy();
  });
});
