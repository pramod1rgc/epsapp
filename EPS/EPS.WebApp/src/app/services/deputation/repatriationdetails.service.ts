import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RepatriationdetailsService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }
  getRepatriationDeputaionDetails(EmpCd: any, roleId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getAllRepatriationDetailsByEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId}`);

  }
  getCheckDuputationInEmployee(EmpCd: any, roleId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getCheckDuputationInEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId}`);

  }
  InsertUpateRepatriationDetails(repatriationDetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateRepatriationDetails, repatriationDetails, { responseType: 'text' })
  }

}
