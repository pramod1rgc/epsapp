import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';
import { Deputation_Deduction, SecemeCode } from '../../model/DeptationModel/depuationin-model';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DeputationinService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }
  GetPayItems(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.getPayItems}`);
  }

  GetAllDesignation(controllerId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getDesignationAll + controllerId}`);
  }
  GetCommunicationAddress(): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getDepuatationCommunticationAddress}`);
  }
  GetDeductionDetails(EmpCd: any, roleId: number): Observable<any> {
    debugger;
    return this.http.get<Deputation_Deduction[]>(`${this.config.api_base_url}${this.config.getDeductionScheduleDetailsbyEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId}`)
      .pipe(map((deputation_Deduction: Deputation_Deduction[]) => {
        debugger;
        return deputation_Deduction;
      }));
    
  }
  getAllAsFormArray2(EmpCd: any, roleId: number): Observable<any> {
    debugger;
    return this.http.get<Deputation_Deduction[]>(`${this.config.api_base_url}${this.config.getDeductionScheduleDetailsbyEmployee + '?EmpCd= ' + EmpCd + '&roleId=' + roleId}`)
      .pipe(map((deputation_Deduction: Deputation_Deduction[]) => {
        debugger;
        return deputation_Deduction;
      }));

  }
   GetScheme_Code(MsEmpDuesID: string): Observable<any> {
    var id = JSON.stringify(MsEmpDuesID)
   debugger;
  
     return this.http.get<SecemeCode>(`${this.config.api_base_url}${this.config.getScheme_CodeByMsEmpDuesID + id}`)
      .pipe(map((schemecode: SecemeCode) => {
        debugger;
        return  schemecode;
    }));
  }
  InsertUpateDeputationDetails(deputationdetails: any): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.saveUpdateDeputationInDetails, deputationdetails)
  }
  getDeputaionDetails(EmpCd: any, roleId: number): Observable<any> {
    return this.GetDeductionDetails(EmpCd, roleId).pipe(map((deputation_Deduction: Deputation_Deduction[]) => {
     
      debugger;
      return deputation_Deduction;
    }));

  }
  forwardStatusUpdate(empDeputDetailsId, status, rejectionRemark): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.updateforwardStatusUpdateByDepID + '?empDeputDetailsId= ' + empDeputDetailsId + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
 
}
