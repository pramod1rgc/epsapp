import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //from net
import { AppConfig, APP_CONFIG } from '../../global/global.module';


//import { EmpDesigModel } from '../../model/EmpDesigModel';
//import { EmpDesigModel } from '../../model/EmpDesigModel';
//import { EmpDesigModel } from '../../model/EmpDesigModel';
@Injectable({
  providedIn: 'root'
})
export class LeavesMgmtService {
  //BaseUrl: any = [];

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { 
    //this.BaseUrl = 'http://localhost:55424/api/';
  }
  GetAllDesignation(PermDdoId: string): Observable<any> {
    const params = new HttpParams().set('PermDdoId', PermDdoId); 
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetAllDesignation`, { params});
  }
  GetAllEmp(msDesigMastID: string): Observable<any> {
    const params = new HttpParams().set('MsDesignID', msDesigMastID);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetEmployeeByDesig`, { params }); 
  }
  GetOrderbyEmp(empCd: string, userRoleId: string, pageCode:string): Observable<any> {
    const params = new HttpParams().set('EmpCd', empCd).set('userRoleId', userRoleId).set('pageCode', pageCode);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetOrderByEmployee`, { params });
  } 
  GetLeavesType(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesType`);
  }
  GetLeavesReason(): Observable<any> {
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesReason`);
  }



  GetLeavesSanction(permDdoId: string, empCode: string,userRoleId:string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesSanction`, { params });
  }
  GetLeavesSanctionHistory(permDdoId: string, empCode: string, userRoleId: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesSanctionHistory`, { params });
  }
  SaveLeavesSanction(_objdamaster: any,_status:string) {
    const paramtr = new HttpParams().set('Status', _status);
    return this.httpclient.post(`${this.config.LeavesControllerName}/SaveLeavesSanction`, _objdamaster, { responseType: 'text', params: paramtr });
  }



  GetLeavesCurtailDetail(permDdoId: string, empCode: string, userRoleId:string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesCurtailDetail`, { params });
  }
  GetLeavesCurtailHistory(permDdoId: string, empCode: string, userRoleId: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesCurtailHistory`, { params });
  }
  GetVerifiedLeavesSanction(permDdoId: string, ordNo: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('ordId', ordNo);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetVerifiedLeavesSanction`, { params });
  }
  SaveLeavescurtExten(_objdamaster: any, _status: string) {
    debugger;
    const paramtr = new HttpParams().set('Status', _status);
    return this.httpclient.post(`${this.config.LeavesControllerName}/SaveLeavesCurtExten`, _objdamaster, { responseType: 'text', params: paramtr });
  }




  
  GetLeavesCancel(permDdoId: string, empCode: string, userRoleId: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesCancel`, { params });
  }
  GetLeavesCancelHistory(permDdoId: string, empCode: string, userRoleId: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('empCode', empCode).set('userRoleId', userRoleId);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesCancelHistory`, { params });
  }
  GetVerifiedLeaveForCancel(permDdoId: string, ordNo: string): Observable<any> {
    const params = new HttpParams().set('permDdoId', permDdoId).set('ordId', ordNo);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetVerifiedLeaveForCancel`, { params });
  }
  SaveLeavesCancel(_objdamaster: any, _status: string) {
    debugger;
    const paramtr = new HttpParams().set('Status', _status);
    return this.httpclient.post(`${this.config.LeavesControllerName}/SaveLeavesCancel`, _objdamaster, { responseType: 'text', params: paramtr });
  }




  DeleteLeavesSanction(id: string, pageCode: string): Observable<any> {
    debugger;
    const params = new HttpParams().set('id', id).set('pageCode', pageCode);
    return this.httpclient.delete<any>(`${this.config.LeavesControllerName}/DeleteLeavesSanction`, { params });
  }
  VerifyReturnSanction(Id: string,status:string,pagecode:string,reason:string) {
    debugger;
    const params = new HttpParams().set('Id', Id).set('Status', status).set('PageCd', pagecode).set('Reason', reason);
    return this.httpclient.post(`${this.config.LeavesControllerName}/VerifyReturnSanction`, null, { responseType: 'text', params: params });
  }


  SaveTotalLeaveAvailed(empCode: string,TotalLeaveAvailed: string) {
    debugger;
    const params = new HttpParams().set('EmpCd', empCode).set('TotalLeaveAvailed', TotalLeaveAvailed);
    return this.httpclient.post(`${this.config.LeavesControllerName}/SaveTotalLeaveAvailed`, null, { responseType: 'text', params: params });
  }
  GetLeavesTypeBalance(empCode: string): Observable<any> {
    const params = new HttpParams().set('EmpCd', empCode);
    return this.httpclient.get<any>(`${this.config.LeavesControllerName}/GetLeavesTypeBalance`, { params });
  }

}












