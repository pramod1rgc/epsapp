import { TestBed } from '@angular/core/testing';

import { LeavesMgmtService } from './leaves-mgmt.service';

describe('LeavesMgmtService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeavesMgmtService = TestBed.get(LeavesMgmtService);
    expect(service).toBeTruthy();
  });
});
