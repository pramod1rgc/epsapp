import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangeSr {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  //Getddlvalue
  GetDdlvalue(Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDdlvalue + Flag);
  }

  //common delete using flag
  ChangeDeleteDetails(empCd: any, orderNo: any, flag: any): Observable<any> {
    return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag)
  }

  //NameGender
  InsertNameGenderDetails(objPro: any, Flag: any): Observable<any> {
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertNameGenderDetails}/InsertNameGenderDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetNameGenderDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetNameGenderDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }

  ForwardToCheckerNameGenderUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerNameGenderUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //pfNps

  InsertPfNpsFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertPfNpsFormDetails}/InsertPfNpsFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetPfNpsFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetPfNpsFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }


  ForwardToCheckerPfNpsUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerPfNpsUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //panno
  InsertPanNoFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertPanNoFormDetails}/InsertPanNoFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetPanNoFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetPanNoFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }

  //FetchPanNosrchDetails(employeeId, pageNumber, pageSize, searchTerm, roleId, flag1): Observable<any> {
  //  return this.http.get(this.config.api_base_url + this.config.FetchPanNosrchDetails + employeeId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&searchTerm=' + searchTerm + '&roleId=' + roleId + '&flag=' + flag1);

  //}

  ForwardToCheckerPanNoUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerPanNoUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //HRA City Class
  InsertHraccFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertHraccFormDetails}/InsertHraccFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetHraccFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetHraccFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }

  //FetchHraccsrchDetails(employeeId, pageNumber, pageSize, searchTerm, roleId, flag1): Observable<any> {
  //  return this.http.get(this.config.api_base_url + this.config.FetchHraccsrchDetails + employeeId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&searchTerm=' + searchTerm + '&roleId=' + roleId + '&flag=' + flag1);

  //}

  ForwardToCheckerHraccUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerHraccUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //designation
  InsertdesignationFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertdesignationFormDetails}/InsertdesignationFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetdesignationFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetdesignationFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }

  ForwardToCheckerdesignationUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerdesignationUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  ////Exservicemen
  InsertexservicemenFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertexservicemenFormDetails}/InsertexservicemenFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }

  GetexservicemenFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetexservicemenFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerexservicemenUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerexservicemenUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //cghs

  InsertCGHSFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertCGHSFormDetails}/InsertCGHSFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetCGHSFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetCGHSFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerCGHSUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCGHSUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //CGEGIS

  InsertCGEGISFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertCGEGISFormDetails}/InsertCGEGISFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetCGEGISFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetCGEGISFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerCGEGISFormUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCGEGISFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //joiningmode

  InsertJoiningModeFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertJoiningModeFormDetails}/InsertJoiningModeFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetJoiningModeFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetJoiningModeFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerJoiningModeFormUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerJoiningModeFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }


  //DOJ

  InsertDOJDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertDOJDetails}/InsertDOJDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetDOJDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDOJDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerDOJUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOJUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //DOE

  InsertDOEDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertDOEDetails}/InsertDOEDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetDOEDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDOEDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerDOEUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDOEUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //DOR

  InsertDORDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertDORDetails}/InsertDORDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetDORDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDORDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerDORUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerDORUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //CasteCategory

  InsertCasteCategoryFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertCasteCategoryFormDetails}/InsertCasteCategoryFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetCasteCategoryFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetCasteCategoryFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerCasteCategoryFormUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerCasteCategoryFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //Entofficevehicle

  InsertEntofficevehicleFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertEntofficevehicleFormDetails}/InsertEntofficevehicleFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetEntofficevehicleFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetEntofficevehicleFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerEntofficevehicleFormUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerEntofficevehicleFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //bank

  InsertBankFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertBankFormDetails}/InsertBankFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetBankFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetBankFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerBankFormUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerBankFormUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //addtapd

  InsertaddtapdFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertaddtapdFormDetails}/InsertaddtapdFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetaddtapdFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetaddtapdFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckeraddtapdUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckeraddtapdUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //TPTAForm

  InsertTPTAFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertTPTAFormDetails}/InsertTPTAFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetTPTAFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetTPTAFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerTPTAUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerTPTAUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //StateGIS

  InsertStateGISFormDetails(objPro: any, Flag: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertStateGISFormDetails}/InsertStateGISFormDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetStateGISFormDetails(EmpCode, roleId, Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetStateGISFormDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag);
  }
  ForwardToCheckerStateGISUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerStateGISUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }




  //MobileNo_Email_EmpCode

  InsertMobileNoEmailEmpCodeDetails(objPro: any, flag: any, Contactdetails: any): Observable<any> {
    debugger;
    const parameter = new HttpParams().set('flag', flag).set('Contactdetails', Contactdetails);
    return this.http.post(`${this.config.api_base_url}${this.config.InsertMobileNoEmailEmpCodeDetails}/InsertMobileNoEmailEmpCodeDetails`,
      objPro, { responseType: 'text', params: parameter });
  }
  GetMobileNoEmailEmpCodeDetails(EmpCode, roleId, Flag, ContactdetailsSelectedOption): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetMobileNoEmailEmpCodeDetails + EmpCode + '&roleId=' + roleId + '&Flag=' + Flag + '&ContactdetailsSelectedOption=' + ContactdetailsSelectedOption);
  }
  ForwardToCheckerMobileNoEmailEmpCodeUserDtls(empCd, orderNo, status, rejectionRemark, ContactdetailsSelectedOption): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerMobileNoEmailEmpCodeUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark + '&ContactdetailsSelectedOption=' + ContactdetailsSelectedOption, { responseType: 'text' })
  }




}
