import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExtensionofService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  //common delete using flag
  ChangeDeleteDetails(empCd: any, orderNo: any, flag: any): Observable<any> {
    return this.http.delete(this.config.api_base_url + this.config.ChangeDeleteDetails + empCd + '&orderNo=' + orderNo + '&flag=' + flag)
  }

  //Getddlvalue
  GetDdlvalue(Flag): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetDdlvalue + Flag);
  }


  InsertEOSDetails(objPro: any, Flag: any): Observable<any> {
    
    const parameter = new HttpParams().set('Flag', Flag);
    return this.http.post(`${this.config.api_base_url}${this.config.EOSControllerName}/InsertEOSDetails`,
      objPro, { responseType: 'text', params: parameter  });
  }
  
  GetExtensionOfServiceDetails(EmpCode, roleId): Observable<any> {
    return this.http.get(this.config.api_base_url + this.config.GetExtensionOfServiceDetails + EmpCode + '&roleId=' + roleId);
  }

  
  //fwdtochecker
  
  ForwardToCheckerUserDtls(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post(this.config.api_base_url + this.config.ForwardToCheckerUserDtls + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }

  //FetchEOSDetails(employeeId, pageNumber, pageSize, searchTerm, roleId): Observable<any> {
  //  return this.http.get(this.config.api_base_url + this.config.FetchEOSDetails + employeeId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&searchTerm=' + searchTerm + '&roleId=' + roleId);

  //}

}
