import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class EpsemployeejoinafterlienperiodService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  getEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd: any, roleId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getEpsEmpJoinOffAfterLienPeriodDetailByEmp + '?empCd= ' + empCd + '&roleId=' + roleId}`);
  }

  //Insert Update Eps Employee Join After Lien Period
  InsertUpateEpsEmployeeJoinAfterLienPeriod(epsEmployeeJoinLienPeriodDetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateEpsEmployeeJoinAfterLienPeriod, epsEmployeeJoinLienPeriodDetails, { responseType: 'text' } )
  }
  //--end

  //Delete Eps Employee Join After Lien Period By EmployeeCode
  deleteEpsEmployeeJoinAfterLienPeriodById(lienId: any): Observable<any> {
    debugger;
    return this.http.get<any>(`${this.config.api_base_url}${this.config.deleteEpsEmployeeJoinAfterLienPeriodById + '?lienId= ' + lienId}`);
  }
  //--end
  //
  forwardStatusUpdate(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.epsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //--end
}
