import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';


import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TransferOfficeLienperiodService {

  constructor(private http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  //get Controller List
  getControllerList(): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.getControllerList}`);
  }
  //--end
  //Get DDOLIST Using Controller ID
  getDdoListbyControllerId(controllerId: any): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.getDdolistbyControllerid + controllerId}`);
  }//--end
  //Get Office Address Details Using Controllerid& ddoId
  getOfficeAddressDetails(controllerID: any, ddoId: any, officeName: any): Observable<any> {

    return this.http.get<any>(`${this.config.api_base_url}${this.config.getOfficeAddressDetails + '?controllerID= ' + controllerID + '&ddoId=' + ddoId + '&officeName=' + officeName}`);
  }
  getTransfer_office_lien_Period_Details(empCd: any, roleId: any): Observable<any> {
    return this.http.get<any>(`${this.config.api_base_url}${this.config.getTransferofficelienPeriodDetails + '?empCd= ' + empCd + '&roleId=' + roleId}`);
  }
  //--end
  //Insert Update Transfer Office Lien Period
  InsertUpateTransferOffLienPeriodService(transferOffLienPeriodDetails: any): Observable<any> {
    debugger;
    return this.http.post(this.config.api_base_url + this.config.saveUpdateTransferOffLienPeriod, transferOffLienPeriodDetails, { responseType: 'text' })
  }
  //--end
  //
  forwardStatusUpdate(empCd, orderNo, status, rejectionRemark): Observable<any> {
    return this.http.post<any>(this.config.api_base_url + this.config.lienPeriodUpdateForwardStatusUpdateByEmpCode + '?empCd= ' + empCd + '&orderNo=' + orderNo + '&status=' + status + '&rejectionRemark=' + rejectionRemark, { responseType: 'text' })
  }
  //--end

  //Delete Record Transfer lien period Office
  deleteTransferOffLienPeriod(empCd, orderNo): Observable<any> {
    return this.http.get<any>(this.config.api_base_url + this.config.deleteTransferOffLienPeriod + '?empCd= ' + empCd + '&orderNo=' + orderNo)
  }
  //--end

}
