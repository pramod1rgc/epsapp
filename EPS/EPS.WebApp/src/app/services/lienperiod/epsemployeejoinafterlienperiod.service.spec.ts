import { TestBed } from '@angular/core/testing';

import { EpsemployeejoinafterlienperiodService } from './epsemployeejoinafterlienperiod.service';

describe('EpsemployeejoinafterlienperiodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EpsemployeejoinafterlienperiodService = TestBed.get(EpsemployeejoinafterlienperiodService);
    expect(service).toBeTruthy();
  });
});
