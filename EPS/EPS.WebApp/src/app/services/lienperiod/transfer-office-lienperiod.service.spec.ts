import { TestBed } from '@angular/core/testing';

import { TransferOfficeLienperiodService } from './transfer-office-lienperiod.service';

describe('TransferOfficeLienperiodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransferOfficeLienperiodService = TestBed.get(TransferOfficeLienperiodService);
    expect(service).toBeTruthy();
  });
});
