import { TestBed } from '@angular/core/testing';

import { ExcessRecPaymentService } from './excess-rec-payment.service';

describe('ExcessRecPaymentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExcessRecPaymentService = TestBed.get(ExcessRecPaymentService);
    expect(service).toBeTruthy();
  });
});
