import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExcessRecPaymentService {
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  
  getFinancialYear(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetFinancialYears, {});
  }

  getRecoveryLoanType(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetRecoveryLoanType, {});
  }

  getRecoveryElement(): Observable<any> {
    return this.httpclient.get<any>(this.config.GetRecoveryElement, {});
  }

  getAccountHead(paybill?: string, ddoId?: string, finYear?: number, recoveryType?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetAccountHead + '?paybill=' + paybill + ' &ddoId=' + ddoId + '&finYear= ' + finYear + '&recoveryType= ' + recoveryType, {});
  }

  getRecoveryComponent(paybill?: string, EmpId?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.GetRecoveryComponent + '?paybill=' + paybill + ' &EmpId=' + EmpId , {});
  }

  insertUpdateExRecovery(recExPaymentObj) {
    return this.httpclient.post(this.config.InsertUpdateExRecovery, recExPaymentObj, { responseType: 'text' });
  }


  getExcessRecoveryDetail(empCode:string): Observable<any> {
    const params = new HttpParams().set('empCode', empCode);
    return this.httpclient.get<any>(this.config.GetExcessRecoveryDetail, { params});
  }

 

  editExcessRecoveryDetail(recExcessId): Observable<any> {
    const params = new HttpParams().set('recExcessId', recExcessId);
    return this.httpclient.get<any>(this.config.EditExcessRecoveryDetail, { params});
  }


  deleteExcessRecoveryData(RecExcessId) {
    return this.httpclient.post(this.config.DeleteExcessRecoveryData + RecExcessId, '', { responseType: 'text' });
  }


  forwardExRecoveryData(recExPaymentObj) {
    return this.httpclient.post(this.config.ForwardExRecoveryData , recExPaymentObj,  { responseType: 'text' });
  }

// Component Details region 
  insertUpdateComponentAmt(recExPaymentObj) {
    return this.httpclient.post(this.config.insertUpdateComponentAmt, recExPaymentObj, { responseType: 'text' });
  }

  getComponentAmtDetails(EmpId?: string, Session?: string): Observable<any> {
    return this.httpclient.get<any>(this.config.getComponentAmtDetails + '?EmpId=' + EmpId + ' &Session=' + Session, {});
  }

  deleteComponentAmt(CmpId) {
    debugger;
    return this.httpclient.post(this.config.deleteComponentAmt + CmpId, '', { responseType: 'text' });
  }
  //End

}
