import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class recoveryService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  GetAllPayBillGroup(PermDdoId: string): Observable<any> {
    const params = new HttpParams().set('permddoId', PermDdoId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetPayBillGroup`, { params });
  }
  GetAllDesignation(PayBillGrpId: string): Observable<any> {
    const params = new HttpParams().set('billGroupId', PayBillGrpId);
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetDesignation`, { params });
  }
  GetAllEmp(DesigCode: string): Observable<any> {
    const params = new HttpParams().set('desigId', DesigCode).set('pageCode', 'Leaves');
    return this.http.get<any>(`${this.config.api_base_url}${this.config.SharedControllerName}/GetEmp`, { params });
  }
}
