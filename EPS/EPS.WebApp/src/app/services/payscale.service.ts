import { Injectable, Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { PayscaleModel } from '../model/masters/PayscaleModel'
import { Observable } from 'rxjs';
import { AppConfig, APP_CONFIG } from '../global/global.module';
@Injectable({
  providedIn: 'root'
})
export class PayscaleService {
  //BaseUrl: any = [];
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    //this.BaseUrl = 'http://localhost:55424/api/';
  }

  BindCommissionCode() {
    return this.httpclient
      .get<PayscaleModel>(`${this.config.BindCommissionCode}`);
  }
  BindPayScaleFormat() {
    return this.httpclient
      .get<PayscaleModel>(`${this.config.BindPayScaleFormat}`);
  }
  BindPayScaleGroup() {
    return this.httpclient.get<PayscaleModel>(`${this.config.BindPayScaleGroup}`);
  }

  GetPayScaleDetails(): Observable<any> {
    //debugger;
    return this.httpclient.get<any>(`${this.config.GetPayScaleDetails}`);
   // return this.httpclient.get<any>('${this.config.GetPayScaleDetails, {}}');

    //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/GetPayScaleDetails', {});

  }

  EditPayScaleDetails(msPayScaleID): Observable<any> {

    return this.httpclient.get<any>(this.config.EditPayScaleDetails+ msPayScaleID, {});
    //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/EditPayScaleDetails?msPayScaleID=' + msPayScaleID, {});
  }

  GetPayScaleDetailsBYComCode(CommCDID): Observable<any> {

    return this.httpclient.get<any>(this.config.GetPayScaleDetailsBYComCode+ CommCDID, {});
    //return this.httpclient.get<any>(this.config.api_base_url + 'payscale/GetPayScaleDetailsBYComCode?CommCDID=' + CommCDID, {});
  }

  GetPayScaleCode(CddirCodeValue): Observable<any> {
        return this.httpclient.get<any>(this.config.GetPayScaleCode+ CddirCodeValue, {});
   // return this.httpclient.get<any>(this.config.api_base_url + 'Payscale/GetPayScaleCode?CddirCodeValue=' + CddirCodeValue, {});
  }

  Delete(msPayScaleID) {
    return this.httpclient.post(this.config.Delete + msPayScaleID,'', { responseType: 'text'});
   // return this.httpclient.post(this.config.api_base_url + 'Payscale/Delete?msPayScaleID='+ msPayScaleID,'', { responseType: 'text'});
  }


  CreatePayScale(_pScaleObj) {
    return this.httpclient.post(this.config.CreatePayScale, _pScaleObj, { responseType: 'text' });
  }


}

