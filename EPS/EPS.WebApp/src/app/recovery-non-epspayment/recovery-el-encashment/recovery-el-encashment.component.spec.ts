import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryELEncashmentComponent } from './recovery-el-encashment.component';

describe('RecoveryELEncashmentComponent', () => {
  let component: RecoveryELEncashmentComponent;
  let fixture: ComponentFixture<RecoveryELEncashmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveryELEncashmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryELEncashmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
