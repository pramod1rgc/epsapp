import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecoveryNonEPSPaymentRoutingModule } from './recovery-non-epspayment-routing.module';
import { RecoveryELEncashmentComponent } from './recovery-el-encashment/recovery-el-encashment.component';
import { OtherRecoveryComponent } from './other-recovery/other-recovery.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [RecoveryELEncashmentComponent, OtherRecoveryComponent],
  imports: [
    CommonModule,
    RecoveryNonEPSPaymentRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTooltipModule,
    NgxMatSelectSearchModule,
    SharedModule
  ]
})
export class RecoveryNonEPSPaymentModule { }
