import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherRecoveryComponent } from './other-recovery.component';

describe('OtherRecoveryComponent', () => {
  let component: OtherRecoveryComponent;
  let fixture: ComponentFixture<OtherRecoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherRecoveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherRecoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
