import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DdorollComponent } from './ddoroll.component';

describe('DdorollComponent', () => {
  let component: DdorollComponent;
  let fixture: ComponentFixture<DdorollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DdorollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DdorollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
