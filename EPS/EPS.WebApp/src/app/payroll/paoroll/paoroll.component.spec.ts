import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaorollComponent } from './paoroll.component';

describe('PaorollComponent', () => {
  let component: PaorollComponent;
  let fixture: ComponentFixture<PaorollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaorollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaorollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
