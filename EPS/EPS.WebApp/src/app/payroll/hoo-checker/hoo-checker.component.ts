import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { hooservice } from '../../services/UserManagement/hoo.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-hoo-checker',
  templateUrl: './hoo-checker.component.html',
  styleUrls: ['./hoo-checker.component.css'],
  providers: [CommonMsg]
})
export class HOOCheckerComponent implements OnInit {

  constructor(private _Service: hooservice, private commonMsg: CommonMsg) { }
  //Variables
  dataSource: any;
  empCd: any;
  empName: any;
  activeStatus: any;
  Active: any = null;
  HooCheckerList: any;
  HooCheckerListAssigned: any;
  AssignedEmp: any;
  datasourceEmp: any;
  checkStatus: any;
  Messages: any;
  isLoading: boolean;
  checked: any;
  SelfCheck: boolean;
  MessageDataFound = true;
  deactivatePopup: boolean;
  ActivatePopup: boolean;
  displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.hooCheckerEmpList();
    this.SelfAssignHOOCheckerRole();
  }
  // employee list asssign and unassign
  hooCheckerEmpList() {
    this._Service.hooCheckerEmpList(sessionStorage.getItem('ddoid')).subscribe(data => {
     this.HooCheckerList = data['listEmployeeModel'];
      this.HooCheckerListAssigned = data['listEmployeeModelAsigned'];
      this.AssignedEmp = this.HooCheckerList.filter(emp => (emp.activeStatus == this.commonMsg.HOOChecker))
      this.datasourceEmp = new MatTableDataSource(this.AssignedEmp);
      this.dataSource = new MatTableDataSource(this.HooCheckerList.filter(emp => (emp.activeStatus != this.commonMsg.HOOChecker)));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.MessageDataFound = true;
        this.SelfCheck = false;
      }
      else {
        this.MessageDataFound = false;
        this.SelfCheck = true;
      }
      var num = 0;
      for (num = 0; num < this.HooCheckerList.length; num++) {
        if (this.HooCheckerList[num]['activeStatus'] == this.commonMsg.HOOChecker) {
          this.checkStatus = "Yes";
          break;
        }
      }
    });
  }

  Assigned() {
    this.isLoading = true;
    this._Service.AssignedHOOChecker(this.empCd, sessionStorage.getItem('ddoid'), this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result;
      if (this.Messages != null) {
        this.checkStatus = "NO"
        this.hooCheckerEmpList();
        swal(this.Messages)
        this.ActivatePopup = false;
        this.deactivatePopup = false;
      }
    })
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.HOOChecker) {
      this.Active = true;
    }
    else {
      this.Active = false;
    }
  }
  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }

  SelfAssign(IsAssign) {
    debugger;
    this.checked = IsAssign;
    this._Service.SelfAssignHOOChecker(sessionStorage.getItem('UserID'), sessionStorage.getItem('username'), sessionStorage.getItem('EmpPermDDOId'), sessionStorage.getItem('ddoid'), IsAssign).subscribe(result => {
      this.Messages = result;
      swal(this.Messages)
      this.ActivatePopup = false;
    })
  }
  SelfAssignHOOCheckerRole() {
    this.checked = true;
    this._Service.SelfAssignHOOCheckerRole(sessionStorage.getItem('UserID')).subscribe(result => {
      this.Messages = result;
      if (this.Messages == '[]') {
        this.checked = false;
      }
    })
  }
}
