import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HOOCheckerComponent } from './hoo-checker.component';

describe('HOOCheckerComponent', () => {
  let component: HOOCheckerComponent;
  let fixture: ComponentFixture<HOOCheckerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HOOCheckerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HOOCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
