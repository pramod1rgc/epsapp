import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayrollModule } from '../payroll/payroll.module';
import { DdorollComponent } from './ddoroll/ddoroll.component';
import { PaorollComponent } from './paoroll/paoroll.component';
import { ControllerroleComponent } from './controllerrole/controllerrole.component';
import { UsersbyddoComponent } from './usersbyddo/usersbyddo.component';
import { MakerrollComponent } from './makerroll/makerroll.component';
import { HOOCheckerComponent } from './hoo-checker/hoo-checker.component';
import { HOOMakerComponent } from './hoo-maker/hoo-maker.component';
import { LoginActivateComponent } from './login-activate/login-activate.component';

const routes: Routes = [
  {
    path: '', component: PayrollModule, data: {
      breadcrumb: 'User Management/ Role-User Management'
    }, children: [

      {
        path: 'controller', component: ControllerroleComponent, data: {
          breadcrumb: 'Pre-defined Role (Controller)'
        } },
      
      {
        path: 'ddo', component: DdorollComponent, data: {
          breadcrumb: 'Pre-defined Role (DDO)'
        } },
      {
        path: 'pao', component: PaorollComponent, data: {
          breadcrumb: 'Pre-defined Role (PAO)'
        } },
      {
        path: 'usersbyddo', component: UsersbyddoComponent, data: {
          breadcrumb: 'Pre-defined Role (DDO Checker)'
        } },
      {
        path: 'maker', component: MakerrollComponent, data: {
          breadcrumb: 'Pre-defined Role (DDO Maker)'
        } },
      {
        path: 'hoochecker', component: HOOCheckerComponent, data: {
          breadcrumb: 'Pre-defined Role (HOO Checker)'
        } },
      {
        path: 'hoomaker', component: HOOMakerComponent, data: {
          breadcrumb: 'Pre-defined Role (HOO Maker)'
        } },
      { path: 'activate', component: LoginActivateComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayrollRoutingModule { }
