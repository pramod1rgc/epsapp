import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersbyddoComponent } from './usersbyddo.component';

describe('UsersbyddoComponent', () => {
  let component: UsersbyddoComponent;
  let fixture: ComponentFixture<UsersbyddoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersbyddoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersbyddoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
