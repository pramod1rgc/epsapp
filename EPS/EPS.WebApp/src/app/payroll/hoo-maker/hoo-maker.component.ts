import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { hooservice } from '../../services/UserManagement/hoo.service';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-hoo-maker',
  templateUrl: './hoo-maker.component.html',
  styleUrls: ['./hoo-maker.component.css'],
  providers: [CommonMsg]
})
export class HOOMakerComponent implements OnInit {

  constructor(private _Service: hooservice, private commonMsg: CommonMsg) { }
  //Variables
  dataSource: any;
  empCd: any;
  empName: any;
  activeStatus: any;
  Active: any = null;
  HooMakerList: any;
  HooMakerListAssigned: any;
  AssignedEmp: any;
  datasourceEmp: any;
  checkStatus: any;
  Messages: any;
  isLoading: boolean;
  deactivatePopup: boolean;
  ActivatePopup: boolean;

  displayedColumns = ['EmpCode', 'EmpName', 'DDOName', 'PAOName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.hooMakerEmpList();
  }

  hooMakerEmpList() {
    
    this._Service.hooMakerEmpList(sessionStorage.getItem('ddoid')).subscribe(data => {
      this.HooMakerList = data['listEmployeeModel'];
      this.HooMakerListAssigned = data['listEmployeeModelAsigned'];
      this.AssignedEmp = this.HooMakerList.filter(emp => (emp.activeStatus == this.commonMsg.HOOMaker))
      this.datasourceEmp = new MatTableDataSource(this.AssignedEmp);
      this.dataSource = new MatTableDataSource(this.HooMakerList.filter(emp => (emp.activeStatus != this.commonMsg.HOOMaker)));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      var num = 0;
      for (num = 0; num < this.HooMakerList.length; num++) {
        if (this.HooMakerList[num]['activeStatus'] == this.commonMsg.HOOMaker) {
          this.checkStatus = "Yes";
          break;
        }
      }
    });
  }

  Assigned() {
    this.isLoading = true;
    this._Service.AssignedHOOMaker(this.empCd, sessionStorage.getItem('ddoid'), this.Active).finally(() => this.isLoading = false).subscribe(result => {
      this.Messages = result;
      if (this.Messages != null) {
        this.checkStatus = "NO"
        this.hooMakerEmpList();
        swal(this.Messages)
        this.ActivatePopup = false;
        this.deactivatePopup = false;
      }
    })
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus = activeStatus;
    if (activeStatus == this.commonMsg.HOOMaker) {
      this.Active = true;
    }
    else {
      this.Active = false;
    }
  }

  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }
  
}
