import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HOOMakerComponent } from './hoo-maker.component';

describe('HOOMakerComponent', () => {
  let component: HOOMakerComponent;
  let fixture: ComponentFixture<HOOMakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HOOMakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HOOMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
