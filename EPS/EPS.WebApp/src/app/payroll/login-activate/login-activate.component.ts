import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-activate',
  templateUrl: './login-activate.component.html',
  styleUrls: ['./login-activate.component.css']
})
export class LoginActivateComponent implements OnInit {
  param1: string;
  param2: string;
  constructor(private route: ActivatedRoute) { }
  
  ngOnInit() {
    debugger;
    this.route.queryParams.subscribe(params => {
    this.param1 = params['param1'];
      this.param2 = params['param2'];
      alert(this.param1)
      debugger;
    });
  }

}
