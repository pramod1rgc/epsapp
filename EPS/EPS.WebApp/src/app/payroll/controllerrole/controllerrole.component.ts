import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { controllerservice } from '../../services/UserManagement/controller.service';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import 'rxjs/add/operator/finally';
import { CommonMsg } from '../../global/common-msg';

export interface commondll {
  values: string;
  text: string;
}

@Component({
  selector: 'app-controllerrole',
  templateUrl: './controllerrole.component.html',
  styleUrls: ['./controllerrole.component.css'],
  providers: [CommonMsg]
})

export class ControllerroleComponent implements OnInit {
  
  controllerroleList: any[];
  paosList: any = [];
  ddlPAOList: any = [];
  ddlDDOList: any = [];
  empCd: any;
  empName: any;
  Active: any;
  activeStatus: any;
  Messages: any;
  checkStatus ="NO";
  AssignedEmp: any = [];
  deactivatePopup: boolean;
  activatePopup : boolean;
  Query: any;
  MessageDataFound = true;
  MsControllerID: any;
  PAOID: any;
  DDOID: any;
  isLoading: boolean;
  AssignedEmpList: any = [];
  AssignedEmpListChanged: any = [];
  IsFlagUnAssigned: boolean;
  IsFlagAssign: boolean;
  btnFlage: boolean;
  btnFlageView: boolean;
  DDLHideShowFlage: boolean;
  AssignedEmpFlag: any = [];

  displayedColumns = ['EmpCode', 'DDOName', 'PAOName', 'EmpName', 'CommonDesigDesc', 'Email', 'ActiveDeactive'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('controllerDetail') form: any;
  @ViewChild('DDLDetail') DDLDetailform: any;
  @ViewChild('empDetail') empDetailform: any; 
  dataSource = new MatTableDataSource(this.paosList);
  datasourceEmp = new MatTableDataSource(this.AssignedEmp);

  public controllerCtrl: FormControl = new FormControl();
  public controllerFilterCtrl: FormControl = new FormControl();
  public filteredcontroller: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  private _onDestroy = new Subject<void>();


  constructor(private _Service: controllerservice, private commonMsg: CommonMsg) { }
 
  ngOnInit() {
    this.btnFlage = true;
    this.IsFlagAssign = false;
    this.DDLHideShowFlage = false;
    this.getallcontrollers(sessionStorage.getItem('controllerID'));
    this.controllerFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filtercontroller(); });
  }
  
  //Search functionality
  private filtercontroller()
  {
    if (!this.controllerroleList)
    {
      return;
    }
    // get the search keyword
    let search = this.controllerFilterCtrl.value;
    if (!search)
    {
      this.filteredcontroller.next(this.controllerroleList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredcontroller.next(
      this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
    );
  }

  ControllerChnage(MsControllerID) {
    this.AssignedEmpList = null;
    this.dataSource = null;
    this.MessageDataFound = true;
    this.MsControllerID = MsControllerID;
    this.getallpaos(MsControllerID);
    this.IsFlagUnAssigned = false;
    this.IsFlagAssign = false;
    this.DDLHideShowFlage = false;

    this._Service.AssignedEmp(this.MsControllerID).subscribe(data => {
      this.AssignedEmpListChanged = data;
      if (this.AssignedEmpListChanged.length == 0) {
        this.btnFlage = true;
        this.btnFlageView = false;
      }
      else if (this.AssignedEmpListChanged.length != 0) {
        this.btnFlage = false;
        this.btnFlageView = true;
      }
    });
  }

  getallcontrollers(ControllerID) {
    if (ControllerID == 0)
    {
      this.Query = this.commonMsg.getController;
      this._Service.getAllControllerServiceclient(ControllerID, this.Query).subscribe(data => {
        this.controllerroleList = data;
        this.controllerCtrl.setValue(this.controllerroleList);
        this.filteredcontroller.next(this.controllerroleList);
      });
    }
  }

  getallpaos(ControllerID) {
    this.Query = this.commonMsg.getPAO;
    this._Service.GetAllpaos(ControllerID, this.Query).subscribe(data => {
      this.ddlPAOList = data;
    });
  }

  getallDDO(PAOID) {
    this.Query = this.commonMsg.getDDO;
    this._Service.GetAllDDO(PAOID, this.Query).subscribe(data => {
      this.ddlDDOList = data;
    });
  }

  onpaoSelectchanged(MsPAOID) {
    this.ddlDDOList = null;
    this.PAOID = MsPAOID;
    this.getallDDO(MsPAOID);
  }

  DDOSelectChange(MsDDOID) {
    this.DDOID = MsDDOID
  }

  View() {
    this._Service.AssignedEmp(this.MsControllerID).subscribe(data => {
      this.AssignedEmpList = data;
      if (this.AssignedEmpList.length == 0) {
        this.btnFlage = true;
        this.btnFlageView = false;
        this.DDLHideShowFlage = true;
      }
      else if (this.AssignedEmpList.length != 0) {
        this.IsFlagUnAssigned = true;
        this.btnFlage = false;
        this.btnFlageView = true;
        this.DDLHideShowFlage = false;
      }

      var num = 0;
      for (num = 0; num < this.AssignedEmpList.length; num++) {
        if (this.AssignedEmpList[num]['activeStatus'] == this.commonMsg.Controller) {
          this.checkStatus = "Yes";
          this.btnFlageView = true;
          break;
        }
      }
    });
  }

  go() {
    
    this.IsFlagAssign = true;
    //this.MsControllerID = '13';
    //this.PAOID = '144';
    //this.DDOID = '00003';
      this._Service.oncontrollerroleSelectchanged(this.MsControllerID, this.PAOID, this.DDOID).subscribe(data => {
      this.paosList = data;
        this.AssignedEmp = this.paosList.filter(emp => emp.activeStatus == this.commonMsg.Controller)
      this.datasourceEmp = new MatTableDataSource(this.AssignedEmp);
        this.dataSource = new MatTableDataSource(this.paosList.filter(emp => emp.activeStatus != this.commonMsg.Controller));
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

      if (this.dataSource.data.length > 0) {
        this.MessageDataFound = true;
      } else {
        this.MessageDataFound = false;
      }
      var num = 0;
      for (num = 0; num < this.paosList.length; num++) {
        if (this.paosList[num]['activeStatus'] == this.commonMsg.Controller) {
          this.checkStatus = "Yes";
          break;
        }
      }
      });
    this.View();
  }
  
  async PaosAssigned() {
    this.isLoading = true;
    this._Service.PaosAssigned(this.empCd, this.MsControllerID, this.Active).finally(() => this.isLoading = false).subscribe(result => {    
      this.Messages = result.toLowerCase();
      if (this.Messages == this.commonMsg.USN.toLowerCase()) {
        this.IsFlagUnAssigned = false;
        this.dataSource = null;
        this.AssignedEmp = null;
        this.Messages = this.commonMsg.USN;
        this.btnFlageView = false;
        this.btnFlage = true;
        this.IsFlagAssign = false;
        this.DDLHideShowFlage = false;
        this.getallcontrollers(sessionStorage.getItem('controllerID'));
      }
      if (this.Messages == this.commonMsg.ASN.toLowerCase()) {
        this.Messages = this.commonMsg.EmailMsg;
        this.btnFlageView = false;
        this.btnFlage = true;
        this.IsFlagAssign = false;
        this.DDLHideShowFlage = false;
        this.getallcontrollers(sessionStorage.getItem('controllerID'));
        this.IsFlagUnAssigned = true;
        this.View();
      }
      if (this.Messages === this.commonMsg.USN) {
        this.checkStatus = "NO"
        swal(this.Messages)
        this.deactivatePopup = false;
      }
      else {
        swal(this.Messages)
        this.activatePopup = false;
      }
    })
  }  

  oncontrollerroleSelectchanged(MsControllerID) {
    this.MsControllerID = MsControllerID;
    this.getallpaos(MsControllerID);
  }

  fillPopup(empCd, empName, activeStatus) {
    this.empCd = empCd;
    this.empName = empName;
    this.activeStatus =activeStatus;
    if (activeStatus == this.commonMsg.Controller) {
      this.Active = true;
    }
    else {
      this.Active = false;
    }
  }
   
  filter(filter: string) {
    filter = filter.trim();
    filter = filter.toLocaleLowerCase();
    this.dataSource.filter = filter;
  }
}
