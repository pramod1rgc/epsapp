import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllerroleComponent } from './controllerrole.component';

describe('ControllerroleComponent', () => {
  let component: ControllerroleComponent;
  let fixture: ComponentFixture<ControllerroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllerroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllerroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
