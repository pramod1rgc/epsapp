import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingRegistrationComponent } from './onboarding-registration/onboarding-registration.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';
import { OnboardingComponent } from './onboarding.component';
import { OnboardingDetailsComponent } from './onboarding-details/onboarding-details.component';

@NgModule({
  declarations: [OnboardingRegistrationComponent, OnboardingComponent, OnboardingDetailsComponent],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, SharedModule
  ]
})
export class OnboardingModule { }
