import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnboardingModule } from '../onboarding/onboarding.module';
import { OnboardingRegistrationComponent } from './onboarding-registration/onboarding-registration.component';
import { OnboardingDetailsComponent } from './onboarding-details/onboarding-details.component';


const routes: Routes = [

  {
    path: '', component: OnboardingModule, data: {
      breadcrumb: 'On Boarding/ Onboarding Registration'
    }, children: [

      {
        path: 'OnboardingRegistration', component: OnboardingRegistrationComponent, data: {
          breadcrumb: 'Onboarding Registration'
        }
      },
      {
        path: 'OnboardingDetails', component: OnboardingDetailsComponent, data: {
          breadcrumb: 'Onboarding Details'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }
