import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfadvancerulesComponent } from './gpfadvancerules.component';

describe('GpfadvancerulesComponent', () => {
  let component: GpfadvancerulesComponent;
  let fixture: ComponentFixture<GpfadvancerulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfadvancerulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfadvancerulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
