import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatExpansionPanel } from '@angular/material';
import { GpfadvancerulesService } from '../../services/Masters/gpfadvancerules.service';
import { CommonMsg } from '../../global/common-msg';
import * as moment from 'moment';


@Component({
  selector: 'app-gpfadvancerules',
  templateUrl: './gpfadvancerules.component.html',
  styleUrls: ['./gpfadvancerules.component.css']
})
export class GpfadvancerulesComponent implements OnInit {
  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  divbox: string;
  isClicked: boolean = false; 
  setDeletIDOnPopup: any;
  dataSource: any;
  gpfrules: any = {};
  Message: any;
  
  is_btnDeleteStatus: boolean;
  btnText = 'Save';
  deletepopup: boolean;
  AdvanceRule: boolean;
  panelOpenState: boolean = true;
  
  
  interval;
  dstatus: boolean;
  isTableHasData = true;
  dataSourceArray: any[];
  displayedColumns: string[] = ['pfType', 'ruleApplicableFromDate', 'rulesValidTillDate', 'monthsBeforeRetirement', 'gpfRuleRefNo', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('msgGpfRules') form: any;
  max = new Date(new Date().getFullYear(), 0, -363);

  @ViewChild('panel1') firstPanel: MatExpansionPanel;
  constructor(private gpfrule: GpfadvancerulesService, private commonMsg: CommonMsg)
  { }
 
  ngOnInit() {
    this.getGpfRules();
    
    this.divbox = "box_dn";
    
  }
  getGpfRules() {
    this.gpfrule.getGpfRules().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSourceArray = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (res == undefined || res == null) {
        this.panelOpenState = true;
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }
  getgpfrulesalRulesByID(id: any) {
    this.firstPanel.open();
    if (id != undefined) {
      this.btnText = 'Update';
      this.isClicked = true;
    } else {
      this.btnText = 'Save';
      this.isClicked = false;
    }
    this.is_btnStatus = false;
    this.gpfrule.getGpfRulesByID(id).subscribe(res => {
      this.gpfrules = res[0];
      if (res != undefined || res != null) {
        this.panelOpenState = true;
        this.bgcolor = "bgcolor";
      }
    })

  }
  

  test(dstatus) {
    if (dstatus == true) {
      this.divbox = "box_db";
    }
    else {
      this.divbox = "box_dn";
    }   
  }

  
  insertUpdateGpfRules() {
    var alreadyExists = null;
    for (var i = 0; i < this.dataSourceArray.length; i++) {
      var getPfType = null;
      if (this.dataSourceArray[i].pfType === 'GPF') {
        getPfType = 'G';
      } else getPfType = "C";
      
      if (getPfType === this.gpfrules.pfType) {
        var fromDate = moment(this.gpfrules.ruleApplicableFromDate).format('YYYY-MM-DD HH:mm:ss');
        var fromtableDate = this.dataSourceArray[i].ruleApplicableFromDate;
        //var tabletDate = moment(fromtableDate).format('DD-MMM-YYYY');
        var toDate = moment(this.gpfrules.rulesValidTillDate).format('YYYY-MM-DD HH:mm:ss');
        var totableDate = this.dataSourceArray[i].rulesValidTillDate;
        // var tabletDate1 = moment(totableDate).format('DD-MMM-YYYY');
        if ((fromDate > fromtableDate) && (fromDate < totableDate) || (toDate > fromtableDate) && (toDate < totableDate) && this.gpfrules.msGpfAdvanceRulesRefID != this.dataSourceArray[i].msGpfAdvanceRulesRefID) {
          this.Message = this.commonMsg.alreadyExistMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
          alreadyExists = "true";
          break;
        }
        else if ((fromDate < fromtableDate) && (toDate > fromtableDate) && this.gpfrules.msGpfAdvanceRulesRefID != this.dataSourceArray[i].msGpfAdvanceRulesRefID) {
          this.Message = this.commonMsg.alreadyExistMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
          alreadyExists = "true";
          break;
        }
      }
    }
    if (alreadyExists !== "true") {
      debugger;
      this.gpfrule.insertUpdateGpfRules(this.gpfrules).subscribe(res => {
        //if (this.gpfrules.MsGpfAdvanceRulesRefID === undefined) {
          if (res != undefined || res != null) {
            //this.panelOpenState = false;
            this.bgcolor = "";
          }

          if (this.btnText == 'Update' && Number(res) > 0) {
            this.Message = this.commonMsg.updateMsg
            this.is_btnStatus = true;
            this.divbgcolor = "alert-success";
            setTimeout(() => {
              this.is_btnStatus = false;
            }, 10000);
            //this.startTimer();
          }
          else if (this.btnText == 'Save' && Number(res) > 0) {
            this.Message = this.commonMsg.saveMsg;
            this.is_btnStatus = true;
            this.divbgcolor = "alert-success";
            setTimeout(() => {
              this.is_btnStatus = false;
            }, 10000);
            //  this.startTimer();
          }
          else {
            this.Message = this.commonMsg.saveFailedMsg;
            this.is_btnStatus = true;
            this.divbgcolor = "alert-danger";
            setTimeout(() => {
              this.is_btnStatus = false;
            }, 10000);
            //  this.startTimer();
          }
        //}
        this.getGpfRules();
        this.form.resetForm();
        this.btnText = 'Save';
        this.isClicked = false;

      });
    }
  }

 
  setDeleteId(MsID) {

    this.setDeletIDOnPopup = MsID;
  }
  resetForm() {
    this.btnText = 'Save';
    this.isClicked = false;
    this.form.resetForm();
    this.gpfrules = {};
   
    this.gpfrules.MsGpfAdvanceRulesRefID = 0;
    this.bgcolor = "";
   // this.startTimer();
  }

  btnclose() {
    this.is_btnStatus = false;

  }
  
  deactivateActivate(id: any) {
    this.gpfrule.deleteGpfRules(id, 1).subscribe(res => {
      //this.Message = res;   
      if (res != undefined && Number(res)>0) {
        this.deletepopup = false;
        this.resetForm();
        this.getGpfRules();
        this.Message = this.commonMsg.deleteMsg;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";        
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
      }
    })
  }

    // Validation
  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  
  charaterWithNumeric(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

}

