import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar, MatCheckbox, MatExpansionPanel } from '@angular/material';
import { DDOMasterService } from '../../services/Masters/ddo-master.service';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';
import * as $ from 'jquery';
import { CommonMsg } from '../../global/common-msg';
import { forEach } from '@angular/router/src/utils/collection';
import { DISABLED } from '@angular/forms/src/model';


export interface PeriodicElement {
  ddoId: string;
  controllerCode: number;
  paoCode: number;
  ddoCode: string;
  ddoName: string;
  ddoTypeText: string;
  ddoAddress: string;
  contactNo: string;
  emailAddress: string;
  stateName: string;
  cityName: string;
}
var ELEMENT_DATA: PeriodicElement[];
@Component({
  selector: 'app-ddomaster',
  templateUrl: './ddomaster.component.html',
  styleUrls: ['./ddomaster.component.css']
})
export class DdomasterComponent implements OnInit {
  isClicked: boolean = false;
  is_btnStatus: boolean;
  isTableHasData = true;
  DDOMasterUpdateForm: FormGroup;
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('panel1') firstPanel: MatExpansionPanel;
  displayedColumns: string[] = ['ddoId', 'controllerCode', 'paoCode', 'ddoCode', 'ddoName', 'ddoTypeText', 'ddoAddress', 'contactNo', 'emailAddress', 'stateName', 'cityName', 'Action'];
  ArrddController: any;
  ArrddPAO: any;
  ArrddDDO: any;
  ArrddDDOCategory: any;
  ArrddState: any;
  ArrddCity: any;
  TxtSexrch: any;
  Message: any;
  ddoId: string;
  btnUpdateText: string;
  panelOpenStateForm: boolean = true;
  panelOpenStateGrid: boolean = true;
  bgcolor: string = "bgcolor";
  deletepopup: boolean;
  divbgcolor: string;
  @ViewChild('DDOUpdateForm') DDOUpdate: any;
  constructor(private _Service: DDOMasterService, private snackBar: MatSnackBar, private formBuilder: FormBuilder, private commonMsg: CommonMsg) {
    this.createForm()
  }

  ngOnInit() {
    this.BindDropDownController();
    this.BindDropCategory();
    this.BindDropState();
    this.GetDDODetails();
    this.btnUpdateText = "Save";
    this.isClicked = false;
    this.bgcolor = "";
  }
  createForm() {
    this.DDOMasterUpdateForm = this.formBuilder.group({
      controllerId: ["", Validators.required],
      controllerCode: [null],
      paoId: ["", Validators.required],
      paoCode: [null],
      ddoTypeText: [null],
      stateName: [null],
      cityName: [null],
      ddoId: [null, Validators.required],
      ddoCode: [null],
      ddoName: [null],
      ddoLang: [null],
      ddoTypeId: [null, Validators.required],
      ddoAddress: [null],
      pinCode: [null],
      contactNo: [null],
      faxNo: [null],
      emailAddress: [null],
      stateId: [null, Validators.required],
      cityId: [null, Validators.required]
    });
  }
  GetDDODetails() {
    this._Service.GetDDOMaster().subscribe(result => {
      debugger;
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })

  }
  BindDropDownController() {
    debugger
    this._Service.GetAllController().subscribe(data => {
      this.ArrddController = data;
    });

  }
  BindDropPAO(value) {
    debugger;
    this.DDOMasterUpdateForm.get('paoId').setValue(null);
    this._Service.GetPAOByControllerId(value).subscribe(data => {
      this.ArrddPAO = data;
    });
  }
  BindDropDDO(value) {
    debugger;
    this._Service.GetODdoByPao(value).subscribe(data => {
      this.ArrddDDO = data;
    });
  }
  BindDropState() {
    debugger;
    this._Service.GetState().subscribe(data => {
      this.ArrddState = data;
    });
  }
  BindDropCity(value) {
    debugger;
    this._Service.GetCity(value).subscribe(data => {
      this.ArrddCity = data;
    });
  }
  BindDropCategory() {
    debugger;
    this._Service.GetDDOcategory().subscribe(data => {
      this.ArrddDDOCategory = data;
    });
  }
  ControllerChange(value) {
    debugger;
    this.BindDropPAO(value.controllerId);
  }
  PAOChange(value) { debugger; this.BindDropDDO(value.paoId); }
  DDOChange(value) {
    debugger;
    this.DDOMasterUpdateForm.get('ddoName').setValue(value.ddoName);
    this.DDOMasterUpdateForm.get('ddoLang').setValue(value.ddoLang);
    this.DDOMasterUpdateForm.get('ddoTypeId').setValue(value.ddoTypeId);
    this.DDOMasterUpdateForm.get('ddoAddress').setValue(value.ddoAddress);
    this.DDOMasterUpdateForm.get('pinCode').setValue(value.pinCode);
    this.DDOMasterUpdateForm.get('contactNo').setValue(value.contactNo);
    this.DDOMasterUpdateForm.get('faxNo').setValue(value.faxNo);
    this.DDOMasterUpdateForm.get('emailAddress').setValue(value.emailAddress);
    this.DDOMasterUpdateForm.get('stateId').setValue(value.stateId);
    this.DDOMasterUpdateForm.get('cityId').setValue(value.cityId);
  }
  DdoCatgryChange() { }
  StateChange(value) {
    debugger;
    this.BindDropCity(value.stateId);
  }
  SaveDDOMaster() {
    this._Service.SaveDDOMaster(this.DDOMasterUpdateForm.getRawValue()).subscribe(result => {
      debugger;
      if (result >= 1) {
        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";
        if (this.btnUpdateText == "Save") {
          this.Message = this.commonMsg.saveMsg
          //this.snackBar.open('Saved Successfully', null, { duration: 4000 });
        }
        else {
          debugger;
          this.Message = this.commonMsg.updateMsg
          //this.snackBar.open('Updated Successfully', null, { duration: 4000 });
        }
        this.DDOMasterUpdateForm.reset();
        this.DDOUpdate.reset();
        this.TxtSexrch = null;
        this.btnUpdateText = 'Save';
        this.isClicked = false;
        this.bgcolor = "";        
        this.GetDDODetails();

        this.DDOMasterUpdateForm.get('controllerId').enable();
        this.DDOMasterUpdateForm.get('paoId').enable();
        this.DDOMasterUpdateForm.get('ddoId').enable();
        this.DDOMasterUpdateForm.get('ddoTypeId').enable();
      } else {
        this.divbgcolor = "alert-danger";
        this.Message = this.commonMsg.saveFailedMsg;
        // this.snackBar.open('Save not Successfull', null, { duration: 4000 });
      }
    })
    setTimeout(() => {
      this.is_btnStatus = false;
    }, 10000);
  
  }
  EditDDOmaster(value) {
    debugger;
    //this.DDOMasterUpdateForm.reset();
    //this.DDOUpdate.reset();
    this.firstPanel.open();
    this.DDOMasterUpdateForm.get('controllerId').disable();
    this.DDOMasterUpdateForm.get('controllerId').setValue(value.controllerId);
    this.BindDropPAO(value.controllerId);
    this.DDOMasterUpdateForm.get('paoId').disable();
    this.DDOMasterUpdateForm.get('paoId').setValue(value.paoId);
    this.BindDropDDO(value.paoId);
    this.DDOMasterUpdateForm.get('ddoId').disable();
    this.DDOMasterUpdateForm.get('ddoId').setValue(value.ddoId);
    this.DDOMasterUpdateForm.get('ddoTypeId').disable();
    this.DDOMasterUpdateForm.get('ddoTypeId').setValue(value.ddoTypeId);
    this.DDOMasterUpdateForm.get('ddoAddress').setValue(value.ddoAddress);
    this.DDOMasterUpdateForm.get('pinCode').setValue(value.pinCode);
    this.DDOMasterUpdateForm.get('contactNo').setValue(value.contactNo);
    this.DDOMasterUpdateForm.get('faxNo').setValue(value.faxNo);
    this.DDOMasterUpdateForm.get('emailAddress').setValue(value.emailAddress);
    this.DDOMasterUpdateForm.get('stateId').setValue(value.stateId);
    this.BindDropCity(value.stateId);
    this.DDOMasterUpdateForm.get('cityId').setValue(value.cityId);
    this.DDOMasterUpdateForm.get('ddoName').setValue(value.ddoName);
    this.DDOMasterUpdateForm.get('ddoLang').setValue(value.ddoLang);

    this.btnUpdateText = "Update";
    this.isClicked = true;
    this.bgcolor = "bgcolor";
    this.is_btnStatus = false;
  }
  CancelDDOmaster() {
    debugger;
    this.DDOMasterUpdateForm.reset();
    this.DDOUpdate.reset();
    this.TxtSexrch = null;
    this.btnUpdateText = "Save";
    this.isClicked = false;
    this.bgcolor = "";
  }


  DeleteDDOmaster(ddoId: string,flag: boolean) {
    debugger;
    this.ddoId = flag ? ddoId : "0";
    if (!flag) {
      this._Service.DeleteDDOMaster(ddoId).subscribe((result: any) => {
        if (result >= 1) {
          this.Message = this.commonMsg.deleteMsg
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";
          this.DDOMasterUpdateForm.reset();
          this.DDOUpdate.reset();
          this.TxtSexrch = null;
          this.GetDDODetails();
        } else {
          this.Message = this.commonMsg.deleteFailedMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
        }
      });
    }
    setTimeout(() => {
      this.is_btnStatus = false;
    }, 10000);

    this.deletepopup = false;
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.isTableHasData = true;
    } else {
      this.isTableHasData = false;
    }
  }
  btnclose() {
    this.is_btnStatus = false;
  }
  omit_special_char(event) {
    var k;
    k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

  charaterOnlyNoSpace(event): boolean {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }
}
