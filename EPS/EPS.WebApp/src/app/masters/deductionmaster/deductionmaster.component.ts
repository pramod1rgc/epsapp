import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { deductionMasterService } from '../../services/Masters/deductionMaster.service';
import { CommonMsg } from '../../global/common-msg';
import { DeductionDefinitionMasterModel } from '../../model/masters/DeductionDefinitionMasterModel';
//import swal from 'sweetalert2';

@Component({
  selector: 'app-deductionmaster',
  templateUrl: './deductionmaster.component.html',
  styleUrls: ['./deductionmaster.component.css']
})

export class DeductionmasterComponent implements OnInit {

  constructor(private _Service: deductionMasterService, private commonMsg: CommonMsg) { }

  deductionDefinationList: any;
  displayedColumns: string[] = ['payItemsCD', 'payItemsName', 'payItemsNameShort', 'grantNo', 'functionalHead', 'objectHead','symbol'];
  dataSource = new MatTableDataSource(this.deductionDefinationList);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('DeductionDefinition') DeductionDefinition: any;

  objDeductionDefinition: DeductionDefinitionMasterModel;
 
  // Variables
  categoryListOfDeduction: any[];
  deductionDescriptionList: any[];
  deletepopup: boolean;
  is_btnStatus: boolean;
  Message: any;
  divbgcolor: string;
  bgcolor: string;
  btnUpdatetext: any;
  savebuttonstatus: boolean;
  isTableHasData = true;
  showElement: boolean;

   ngOnInit() {
     this.objDeductionDefinition = new DeductionDefinitionMasterModel();
     this.dataSource.sort = this.sort;
     this.dataSource.paginator = this.paginator;
     this.btnUpdatetext = 'Save';
     this.savebuttonstatus = true;
     this.categoryList();
     this.deductionDescription();
     this.getDeductionDefinitionList();
     this.objDeductionDefinition.subtractedFromSalary = true;
     this.objDeductionDefinition.exemptedFromITax = false;
     this.objDeductionDefinition.autoCalculated = false;
  }

  //For Category drop down List
  categoryList() {
    this._Service.categoryList().subscribe(data => {
      this.categoryListOfDeduction = data;
    });
  }

  // For deduction Description List
  deductionDescription() {
    this._Service.deductionDescription().subscribe(data => {
      this.deductionDescriptionList = data;
    });
  }

  deductionDescriptionChange(payItemsCD) {
      this._Service.deductionDescriptionChange(payItemsCD).subscribe(data => {
        this.objDeductionDefinition.payItemsCD = payItemsCD;
        this.objDeductionDefinition.payItemsNameShort = data[0]["payItemsNameShort"];
        this.objDeductionDefinition.grantNo = data[0]["grantNo"];
        this.objDeductionDefinition.functionalHead = data[0]["functionalHead"];
        this.objDeductionDefinition.objectHead = data[0]["objectHead"];
        this.objDeductionDefinition.categoryID = data[0]["categoryID"];
        this.objDeductionDefinition.exemptedFromITax = data[0]["exemptedFromITax"];
        this.objDeductionDefinition.autoCalculated = data[0]["autoCalculated"];
        this.objDeductionDefinition.subtractedFromSalary = data[0]["subtractedFromSalary"];
    });
  }

  // for category drop down list select change
  categoryChange(value) {
    //alert(value);
  }
  
  //Bind Deduction code
  //getAutoGenratedDeductionCode() {
  //  this._Service.getAutoGenratedDeductionCode().subscribe(res => {
  //    //this.objDeductionDefinition.deductionCD = res.deductionCD;
  //   //console.log(res);
  //  });
  //}

  getDeductionDefinitionList() {
    this._Service.getDeductionDefinitionList().subscribe(data => {
      this.deductionDefinationList = data;
      this.dataSource = new MatTableDataSource(this.deductionDefinationList);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  getDeductionDefinitionById(element) {

    this.objDeductionDefinition.payItemsCD = element.payItemsCD;
    this.objDeductionDefinition.payItemsNameShort =element.payItemsNameShort;
    this.objDeductionDefinition.grantNo = element.grantNo;
    this.objDeductionDefinition.functionalHead = element.functionalHead;
    this.objDeductionDefinition.objectHead = element.objectHead;
    this.objDeductionDefinition.categoryID = element.categoryID;
    this.objDeductionDefinition.subtractedFromSalary = element.subtractedFromSalary;
    this.objDeductionDefinition.exemptedFromITax = element.exemptedFromITax;
    this.objDeductionDefinition.autoCalculated = element.autoCalculated;

    this.btnUpdatetext = 'Update';
    this.bgcolor = "bgcolor";
  }

  deductionDefinitionSubmit(objDeductionDefinition) {
    this._Service.deductionDefinitionSubmit(objDeductionDefinition).subscribe(data => {
      if (data == "1") {
        this.Message = this.commonMsg.saveMsg;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-success";
        this.Reset();
        this.hideShowDiv();
      }
    });
  }
 
  Reset() {
    this.DeductionDefinition.resetForm();
    this.objDeductionDefinition.subtractedFromSalary = true;
    this.objDeductionDefinition.exemptedFromITax = false;
    this.objDeductionDefinition.autoCalculated = false;
    this.btnUpdatetext = 'Save';
    this.bgcolor = "";
  }

  btnclose() {
    this.is_btnStatus = false;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }

  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  hideShowDiv() {
    this.showElement = true;
    setTimeout(() => {
      //console.log('hide');
      this.showElement = false;
    }, 5000);
  }

}
