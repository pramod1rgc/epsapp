import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionmasterComponent } from './deductionmaster.component';

describe('DeductionmasterComponent', () => {
  let component: DeductionmasterComponent;
  let fixture: ComponentFixture<DeductionmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
