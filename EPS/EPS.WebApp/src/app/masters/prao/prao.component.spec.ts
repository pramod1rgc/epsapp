import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PraoComponent } from './prao.component';

describe('PraoComponent', () => {
  let component: PraoComponent;
  let fixture: ComponentFixture<PraoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PraoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PraoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
