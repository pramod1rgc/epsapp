import { Component, OnInit, ViewChild,  ChangeDetectorRef } from '@angular/core';

import {  FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { from } from 'rxjs';
import { DISABLED } from '@angular/forms/src/model';
import { DuesDefinitionServicesService } from '../../services/masters/dues-definition-services.service';
import { DuesDefinitionmasterModel } from '../../model/masters/DuesDefinitionmasterModel';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dues-dues-master',
  templateUrl: './dues-dues-master.component.html',
  styleUrls: ['./dues-dues-master.component.css']
})
export class DuesDuesMasterComponent implements OnInit {
  isTableHasData = true;
  DuesDefinitionFrom: FormGroup;
  objDuesDefinitionFrom: DuesDefinitionmasterModel = {} as any;
  btnUpdatetext: any;
  is_Computational: boolean;
  is_DuesCode: boolean;
  is_Description: boolean;
  is_ShortName: boolean;
  dataSource: any;
  Message: any;
  deletepopup: boolean;
  setDeletIDOnPopup: any;
  distableCancelOnUpdate: boolean;
  public ComputationalStatus = false;
  public WhetherExemptedFromITaxStatus = false;
  savebuttonstatus: boolean;
  disabledValue: boolean;
  pipe: DatePipe;
  temp: any;
  searchfield: any;
  filterList: any;
  is_btnStatus = true;
  DuesDefinationList: any = [];
  @ViewChild('f') DuesDuesMaster: any;
  @ViewChild(MatSort) sort: MatSort;
  btnFlag: any;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  isOnInit: boolean = false;
  isDuesDifinationMasterPanel: boolean = false;
  bgColor: string;
  btnCssClass: string = 'btn btn-success';
  //displayedColumns: string[] = ['payItemsCD', 'payItemsName', 'payItemsNameShort', 'payItemsWef', 'payItemsComputable', 'payItemsIsTaxable', 'action'];

  displayedColumns: string[] = ['payItemsCD', 'payItemsName', 'payItemsNameShort', 'WithEffectFrom','WithEffectTo', 'payItemsComputable', 'payItemsIsTaxable', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
    isValidDate: any;
  error: any = { isError: false, errorMessage: '' };

  constructor(private _service: DuesDefinitionServicesService, private fb: FormBuilder, protected changerefetec: ChangeDetectorRef, private commonMsg: CommonMsg) {
    //this.createForm();

  }
  objDuesDefinition: DuesDefinitionmasterModel;
  InsertandUpdate(objDuesDefinition) {

    this.isValidDate = this.validateDates(objDuesDefinition.withEffectFrom, objDuesDefinition.withEffectTo);

    this._service.InsertUpdateDuesDefinationMaster(this.objDuesDefinition).subscribe(res => {

      if (res > 0) {
        if (this.btnUpdatetext === 'Save') {
          if (this.isValidDate) {
            if (res === "1") {
              this.Message = this.commonMsg.saveMsg;
              if (this.Message != undefined) {
                this.deletepopup = false;
                this.isSuccessStatus = true;
                this.responseMessage = this.Message;
              }

            }

            this.bgColor = '';
            this.btnCssClass = 'btn btn-success';
            setTimeout(() => {
              this.isSuccessStatus = false;
              this.isWarningStatus = false;
              this.responseMessage = '';
            }, this.commonMsg.messageTimer);

            this.GetAutoGenratedDuesCode();
            this.getDuesDefinationList();
            this.resetForm();
            this.changerefetec.detectChanges();

            this.SetTexandComptableValue();
            this.searchfield = "";
          }
        }
        else {
          this.Message = this.commonMsg.updateMsg;
          if (this.Message != undefined) {
            this.deletepopup = false;
            this.isSuccessStatus = true;
            this.responseMessage = this.Message;
          }
          this.bgColor = '';
          this.btnCssClass = 'btn btn-success';
          setTimeout(() => {
            this.isSuccessStatus = false;
            this.isWarningStatus = false;
            this.responseMessage = '';
          }, this.commonMsg.messageTimer);

          this.getDuesDefinationList();
          this.resetForm();
          this.changerefetec.detectChanges();

          this.SetTexandComptableValue();
          this.distableCancelOnUpdate = true;
          this.btnUpdatetext = 'Save';
          this.searchfield = "";

        }
      }
      else {
        if (res.StatusCodes.Status304NotModified) {
          this.Message = "Record Already Exist !!!!!!!";
        }
      }
      this.distableCancelOnUpdate = false;
    });


  }

  checkAllreadyexist() {
    this.btnFlag = false;
    this.filterList = this.DuesDefinationList.filter(item => item.payItemsNameShort.trim().toLowerCase() == this.objDuesDefinition.payItemsNameShort.trim().toLowerCase() && item.payItemsName.trim().toLowerCase() == this.objDuesDefinition.payItemsName.trim().toLowerCase())
    if (this.filterList.length > 0) {
      this.Message = this.commonMsg.alreadyExistMsg;
       //changes
      if (this.Message != undefined) {
        this.disabledValue = true;
        this.deletepopup = false;
        this.btnFlag = true;
        swal(this.Message)
      }
      this.disabledValue = false;
    }
    else {
      this.Message = null;

    }
  }

  getDuesdefinationByIdEdit(msPayItemsID, payItemsCD, payItemsName, payItemsNameShort, payItemsWef, payItemsComputable, payItemsIsTaxable) {
    debugger;
    this.objDuesDefinition.msPayItemsID = msPayItemsID;
    this.objDuesDefinition.payItemsCD = payItemsCD;
    this.objDuesDefinition.payItemsName = payItemsName;
    this.objDuesDefinition.payItemsNameShort = payItemsNameShort;
    this.objDuesDefinition.payItemsWef = payItemsWef;
    this.objDuesDefinition.payItemsComputable = payItemsComputable;
    this.objDuesDefinition.payItemsIsTaxable = payItemsIsTaxable;

    this.btnUpdatetext = 'Update';
    this.distableCancelOnUpdate = true;
    this.isDuesDifinationMasterPanel = true;
    this.bgColor = 'bgcolor';
    this.btnCssClass = 'btn btn-info';
  }


  omit_special_char(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

  SetTexandComptableValue() {

    this.objDuesDefinition.payItemsComputable = 0;
    this.objDuesDefinition.payItemsIsTaxable = 0;
  }


  resetForm() {

    this.DuesDuesMaster.resetForm();
    this.changerefetec.detectChanges();
    this.SetTexandComptableValue();
    this.GetAutoGenratedDuesCode();
    this.btnUpdatetext = 'Save';
    this.bgColor = '';

  }

  getDuesDefinationList() {

    this._service.getDuesDefinationmasterList().subscribe(res => {
      this.DuesDefinationList = res;
      this.dataSource = new MatTableDataSource(res);
      this.pipe = new DatePipe('en');

      const defaultPredicate = this.dataSource.filterPredicate;
      this.dataSource.filterPredicate = (data, filter) => {
        var formatted = this.pipe.transform([data.withEffectFrom], 'dd/MM/yyyy');
        return formatted.indexOf(filter) >= 0 || defaultPredicate(data, filter);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.temp = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
    })
  }

  deleteDuesDefinationByDuesCode(DuesCd) {

    this._service.DeleteDuesmasterbyDuesCode(DuesCd).subscribe(res => {
      this.Message = res;
      if (this.Message != undefined) {
        this.deletepopup = false;
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this.commonMsg.deleteMsg;
      }
      this.resetForm();
      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this.commonMsg.messageTimer);
   
      this.getDuesDefinationList();
      this.GetAutoGenratedDuesCode();
    });
    this.disabledValue = false;

  }




  applyFilter(filterValue: string) {

    let data = this.dataSource.data as any[];
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.isTableHasData = true;
    } else {
      this.isTableHasData = false;
    }
  }


  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
    this.disabledValue = true;
  }

  GetAutoGenratedDuesCode() {
    this._service.GetAutoGenratedDuseCode().subscribe(res => {

      this.objDuesDefinition.payItemsCD = res.payItemsCD;


    });
  }

  ngOnInit() {
    this.objDuesDefinition = new DuesDefinitionmasterModel();
    this.btnUpdatetext = 'Save';
    this.savebuttonstatus = true;
    this.SetTexandComptableValue();
    this.is_DuesCode = true;
    this.is_Description = true;
    this.is_ShortName = true;
    this.getDuesDefinationList();
    this.GetAutoGenratedDuesCode();
    this.disabledValue = false;
  }
  charaterOnlyNoSpace(event): boolean {

    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  validateDates(withEffectFrom: string, withEffectTo: string) {
    this.isValidDate = true;
    this.error = "";
    if ((withEffectFrom != null && withEffectTo != null) && (withEffectTo) < (withEffectFrom)) {
      this.error = { isError: true, errorMessage: 'End date should be greater than start date.' };
      this.isValidDate = false;
    }
    return this.isValidDate;
  }
  btnclose() {
    this.is_btnStatus = false;
  }
}



