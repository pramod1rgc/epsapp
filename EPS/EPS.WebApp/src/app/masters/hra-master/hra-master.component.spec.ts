import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HraMasterComponent } from './hra-master.component';

describe('HraMasterComponent', () => {
  let component: HraMasterComponent;
  let fixture: ComponentFixture<HraMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HraMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HraMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
