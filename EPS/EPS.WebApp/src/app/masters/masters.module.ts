import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MastersRoutingModule } from './masters-routing.module';
import { LeaveTypesComponent } from '../masters/leave-types/leave-types.component';
import { DAMastersComponent } from '../masters/damasters/damasters.component';
import { DesignationMasterComponent } from '../masters/designation-master/designation-master.component';
import { HraMasterComponent } from '../masters/hra-master/hra-master.component';
import { TptaMasterComponent } from '../masters/tpta-master/tpta-master.component';
import {GpfinterestrateComponent } from '../masters/gpfinterestrate/gpfinterestrate.component';
import { PayScaleComponent } from '../masters/pay-scale/pay-scale.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../shared-module/shared-module.module';
import { StateAGOfficersComponent } from './state-agofficers/state-agofficers.component';
import { FdGpfMasterComponent } from '../masters/fd-gpf-master/fd-gpf-master.component';
import { DLISRuleMasterComponent } from './dlisrule-master/dlisrule-master.component';
import { DuesDuesMasterComponent } from './dues-dues-master/dues-dues-master.component';
import { DuesRateComponent } from './dues-rate/dues-rate.component';
import { CitymasterComponent  }  from './citymaster/citymaster.component'
import { GpfadvancereasonsComponent } from './gpfadvancereasons/gpfadvancereasons.component';
import { GpfWithdrawRulesComponent } from './gpf-withdraw-rules/gpf-withdraw-rules.component';
import { GpfWithdrawReasonsComponent } from './gpf-withdraw-reasons/gpf-withdraw-reasons.component';
import { GpfadvancerulesComponent } from './gpfadvancerules/gpfadvancerules.component'
import { GPFRecoveryRulesComponent } from './gpfrecovery-rules/gpfrecovery-rules.component';
import { DdomasterComponent } from './ddomaster/ddomaster.component';
import { PaomasterComponent } from './paomaster/paomaster.component';
import { PraoComponent } from './prao/prao.component';
import { PsuComponent } from './psu/psu.component';
import { CommonMsg } from '../global/common-msg';
import { commons } from '../global/commons';
import { DeductionmasterComponent } from './deductionmaster/deductionmaster.component';
import { DeductionRateComponent } from './deduction-rate/deduction-rate.component';
import { MasterofficeComponent} from './masteroffice/masteroffice.component';


@NgModule({
  declarations: [PayScaleComponent, LeaveTypesComponent, DAMastersComponent, DesignationMasterComponent, HraMasterComponent, GpfinterestrateComponent, TptaMasterComponent, StateAGOfficersComponent, PraoComponent
    , FdGpfMasterComponent, DLISRuleMasterComponent, DuesDuesMasterComponent, DuesRateComponent, CitymasterComponent, GpfadvancereasonsComponent, GpfWithdrawRulesComponent, GpfWithdrawReasonsComponent, GpfadvancerulesComponent, GPFRecoveryRulesComponent, DdomasterComponent, PaomasterComponent, PsuComponent, DeductionmasterComponent, DeductionRateComponent, MasterofficeComponent],
  imports: [
    CommonModule,
    MastersRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule, HttpClientModule,
    MatTooltipModule, SharedModule
    
  ],
  providers: [CommonMsg, commons]
})
export class MastersModule {


}

