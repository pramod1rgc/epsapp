import { IRateDetails } from './IRateDetails';

export interface IDuesRate {
    id: number;
    fullName: string;
    email: string;
    phone?: number;
    contactPreference: string;
    RateDetails: IRateDetails[];
}