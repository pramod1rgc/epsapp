import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FdGpfMasterComponent } from './fd-gpf-master.component';

describe('FdGpfMasterComponent', () => {
  let component: FdGpfMasterComponent;
  let fixture: ComponentFixture<FdGpfMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FdGpfMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FdGpfMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
