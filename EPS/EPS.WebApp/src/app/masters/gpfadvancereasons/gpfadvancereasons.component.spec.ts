import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfadvancereasonsComponent } from './gpfadvancereasons.component';

describe('GpfadvancereasonsComponent', () => {
  let component: GpfadvancereasonsComponent;
  let fixture: ComponentFixture<GpfadvancereasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfadvancereasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfadvancereasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
