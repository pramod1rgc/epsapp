
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatExpansionPanel } from '@angular/material';
import { GpfRecoveryrulesService } from '../../services/Masters/gpf-recoveryrules.service';
import { CommonMsg } from '../../global/common-msg'; 
import * as moment from 'moment';


@Component({
  selector: 'app-gpfrecovery-rules',
  templateUrl: './gpfrecovery-rules.component.html',
  styleUrls: ['./gpfrecovery-rules.component.css']
})
export class GPFRecoveryRulesComponent implements OnInit {

  is_btnStatus: boolean;
  divbgcolor: string;
  bgcolor: string;
  divbox: string;
  isClicked: boolean = false;

  @ViewChild('msgGpfRules') msgGpfRules: any;
  gpfrules: any = {};
  setDeletIDOnPopup: any;
  dataSource: any;
  Message: any;
  btnText = 'Save';
  oddInstallment: boolean;
  msGpfRecoveryID: number;
  deletepopup: any;
  isTableHasData = true;
  dataSourceArray: any[];
  disableflag=false;
  max = new Date(new Date().getFullYear(), 0, -1824);;
  constructor(private gpfrule: GpfRecoveryrulesService, private commonMsg: CommonMsg) { }
  displayedColumns: string[] = ['pfType', 'ruleApplicableFromDate', 'rulesValidTillDate', 'gpfRuleRefNo', 'minInstalmentst', 'maxInstalmentst', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('panel1') firstPanel: MatExpansionPanel;
  ngOnInit() {
    this.getGpfRules();
    this.divbox = "box_dn";
  }
  getGpfRules() {
    this.gpfrule.getGpfRules().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSourceArray = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  test(dstatus) {
    if (dstatus == true) {
      this.divbox = "box_db";
    }
    else {
      this.divbox = "box_dn";
    }
  }

  btnclose() {
    this.is_btnStatus = false;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }


  getGpfRecoveryRulesByID(id: any) {

    this.firstPanel.open();
    this.disableflag = true;
    this.btnText = 'Update';
    if (id != undefined) {
      this.btnText = 'Update';
      this.isClicked = true;
      this.bgcolor = "bgcolor";
    } else {
      this.btnText = 'Save';
    }
    this.is_btnStatus = false;
    this.gpfrule.getGpfRulesByID(id).subscribe(res => {
      this.gpfrules = res[0];
      if (this.gpfrules.oddInstallment==true) {
        this.gpfrules.oddInstallment = "true";
      }
      else if (this.gpfrules.oddInstallment == false){
        this.gpfrules.oddInstallment = "false";
      }
    })
  }



  setDeleteId(MsID) {
    this.setDeletIDOnPopup = MsID;
  }
  resetForm() {
    this.disableflag = false;
    this.gpfrules.msGpfRecoveryID = 0;
    this.btnText = 'Save';
    this.msgGpfRules.resetForm();
    this.isClicked = false;
    
    this.bgcolor = "";
  }

  delete(ID) {
    this.gpfrule.deleteGpfRules(ID).subscribe(res => {
      this.Message = res;
      if (this.Message != undefined) {
        if (this.Message == 1) {
          this.Message = this.commonMsg.deleteMsg;
        }
        if (this.Message == -1) {
          this.Message = this.commonMsg.deleteFailedMsg;
        }
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
        this.deletepopup = false;
        
      }
      this.resetForm();
      this.getGpfRules();
    })
  }

  deleteGpfRules(id: any) {
    this.gpfrule.deleteGpfRules(id).subscribe(res => {
      this.Message = res;
      if (this.Message != undefined) {
        
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
        setTimeout(() => {
          this.is_btnStatus = false;
        }, 10000);
        this.deletepopup = false;
      }
      this.resetForm();
      this.getGpfRules();
    })
  }


  insertUpdateGpfRules() {
    this.disableflag = false;
    this.btnText = 'Save';
    var alreadyExists = null;
    for (var i = 0; i < this.dataSourceArray.length; i++) {
      var getPfType = null;
      if (this.dataSourceArray[i].pfType === 'GPF') {
        getPfType = 'G';
      } else getPfType = "C";

      if (getPfType === this.gpfrules.pfType) {
        var fromDate = moment(this.gpfrules.ruleApplicableFromDate).format('YYYY-MM-DD HH:mm:ss');
        var fromtableDate = this.dataSourceArray[i].ruleApplicableFromDate;
        //var tabletDate = moment(fromtableDate).format('DD-MMM-YYYY');
        var toDate = moment(this.gpfrules.rulesValidTillDate).format('YYYY-MM-DD HH:mm:ss');
        var totableDate = this.dataSourceArray[i].rulesValidTillDate;
        // var tabletDate1 = moment(totableDate).format('DD-MMM-YYYY');
        if ((fromDate > fromtableDate) && (fromDate < totableDate) || (toDate > fromtableDate) && (toDate < totableDate) && this.gpfrules.msGpfRecoveryID != this.dataSourceArray[i].msGpfRecoveryID) {
          this.Message = this.commonMsg.alreadyExistMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
          this.msgGpfRules.resetForm();
         
          alreadyExists = "true";
          break;
        }
        else if ((fromDate < fromtableDate) && (toDate > fromtableDate) && this.gpfrules.msGpfRecoveryID != this.dataSourceArray[i].msGpfRecoveryID) {
          this.Message = this.commonMsg.alreadyExistMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
          this.msgGpfRules.resetForm();
          alreadyExists = "true";
          break;
        }
      }
    }
    if (alreadyExists !== "true") {
      this.gpfrule.insertUpdateGpfRules(this.gpfrules).subscribe(res => {
        if (this.gpfrules.msGpfRecoveryID > 0 && Number(res) > 0) {
          this.Message = this.commonMsg.updateMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
        }
        else if (Number(res) > 0) {
          this.Message = this.commonMsg.saveMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
        }
        else {
          this.Message = this.commonMsg.errorMsg;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-danger";
          setTimeout(() => {
            this.is_btnStatus = false;
          }, 10000);
        }
        this.getGpfRules();
        this.msgGpfRules.resetForm();
        this.btnText = 'Save';
        this.gpfrules.msGpfRecoveryID = 0;
        this.is_btnStatus = true;
        this.bgcolor = "";
        this.isClicked = false;
      });}
    
  }


  charaterWithNumeric(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

}
