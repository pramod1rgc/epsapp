import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GPFRecoveryRulesComponent } from './gpfrecovery-rules.component';

describe('GPFRecoveryRulesComponent', () => {
  let component: GPFRecoveryRulesComponent;
  let fixture: ComponentFixture<GPFRecoveryRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GPFRecoveryRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GPFRecoveryRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
