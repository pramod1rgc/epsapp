import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TptaMasterComponent } from './tpta-master.component';

describe('TptaMasterComponent', () => {
  let component: TptaMasterComponent;
  let fixture: ComponentFixture<TptaMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TptaMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TptaMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
