import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfWithdrawRulesComponent } from './gpf-withdraw-rules.component';

describe('GpfWithdrawRulesComponent', () => {
  let component: GpfWithdrawRulesComponent;
  let fixture: ComponentFixture<GpfWithdrawRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfWithdrawRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfWithdrawRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
 
  });
});
