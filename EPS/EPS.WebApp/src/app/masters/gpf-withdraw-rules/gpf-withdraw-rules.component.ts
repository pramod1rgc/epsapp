import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { GpfWithdrawalRulesService } from '../../services/Masters/gpf-withdrawal-rules.service';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-gpf-withdraw-rules',
  templateUrl: './gpf-withdraw-rules.component.html',
  styleUrls: ['./gpf-withdraw-rules.component.css']
})
export class GpfWithdrawRulesComponent implements OnInit {
  dataSource: any;
  dataSourceArray: any[];
  gpfWithdraw: any = {};
  btnText = 'Save';
  setDeletIDOnPopup: any;
  deletePopUp = false;
  notFound = true;
  //displayedColumns: string[] = ['pfType', 'ruleApplicableFromDate', 'pfTypeValidTillDate', 'noOfWithdraw', 'sealingWithPay', 'maxPerOfBalance',
  //  'minService', 'maxServiceWithPurpose', 'maxServiceWithoutPurpose', 'gpfRuleRefNo','Action'];
  displayedColumns: string[] = ['pfType', 'ruleApplicableFromDate', 'pfTypeValidTillDate', 'gpfRuleRefNo', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('msGpfWithdrawRules') form: any;
  isLoading = false;
  message: any;
  divBgColor: any
  btnStatus: boolean;
  panelOpenState: boolean
  bgColor: string;
  isClicked = false;
  constructor(private gpfWithdrawal: GpfWithdrawalRulesService, private commonMsg: CommonMsg
  ) { }

  ngOnInit() {
    this.getGpfWithdrawalRules();
  }
  getGpfWithdrawalRules() {
    debugger;
    this.gpfWithdrawal.getGpfWithdrawalRules().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSourceArray = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (res.length===0) {
        this.panelOpenState = true;
      }

    })
  }
  applyFilter(filterValue: string) {
    //  debugger;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }


  getGpfWithdrawalRulesByID(id: any) {
     debugger
    if (id != undefined) {
      this.btnText = 'Update';
      this.isClicked = true;
    } else {
      this.btnText = 'Save';
    }
    this.gpfWithdrawal.getGpfWithdrawalRulesByID(id).subscribe(res => {
      this.gpfWithdraw = res[0];
      if (res != undefined || res != null) {
        this.panelOpenState = true;
        this.bgColor = "bgcolor";
      }
    })

  }

  insertUpdateGpfWithdrawalRules() {
    this.isLoading = true;
    //this.btnText = 'Processing';
    var alreadyExists = null;
    for (var i = 0; i < this.dataSourceArray.length; i++) {       
      var getPfType = null;
      if (this.dataSourceArray[i].pfType === 'GPF') {
        getPfType = 'G';
      } else getPfType = "C";
      if (getPfType === this.gpfWithdraw.pfType) {
        var fromDate = moment(this.gpfWithdraw.ruleApplicableFromDate).format('YYYY-MM-DD HH:mm:ss');
        var fromtableDate = this.dataSourceArray[i].ruleApplicableFromDate;
        //var tabletDate = moment(fromtableDate).format('DD-MMM-YYYY');
        var toDate = moment(this.gpfWithdraw.pfTypeValidTillDate).format('YYYY-MM-DD HH:mm:ss');
        var totableDate = this.dataSourceArray[i].pfTypeValidTillDate;
       // var tabletDate1 = moment(totableDate).format('DD-MMM-YYYY');
        if ((fromDate > fromtableDate) && (fromDate < totableDate) || (toDate > fromtableDate) && (toDate < totableDate) && this.gpfWithdraw.msGpfWithdrawRulesRefID != this.dataSourceArray[i].msGpfWithdrawRulesRefID) {
          swal("Date is less than selected Date");
          alreadyExists = "true";
          this.isLoading = false;
          break;
        }
        else if ((fromDate < fromtableDate) && (toDate > fromtableDate) && this.gpfWithdraw.msGpfWithdrawRulesRefID != this.dataSourceArray[i].msGpfWithdrawRulesRefID) {
          swal("Date is less than selected Date");
          alreadyExists = "true";
          this.isLoading = false;
          break;
        }
      }       
    }
    if (alreadyExists !== "true") {
      this.gpfWithdrawal.insertUpdateGpfWithdrawalRules(this.gpfWithdraw).subscribe(res => {
        this.isLoading = false;
       
        if (this.gpfWithdraw.msGpfWithdrawRulesRefID === undefined) {
          this.message = this.commonMsg.saveMsg;
          this.btnStatus = true;
          this.divBgColor = "alert-success";
        } else {
          this.message = this.commonMsg.updateMsg;
          this.btnStatus = true;
          this.divBgColor = "alert-success";
        }
        this.getGpfWithdrawalRules();
        this.resetForm();
        setTimeout(() => {
          this.btnStatus = false;
          this.message = '';
        }, 8000);
      });
    }

  }

  resetForm() {
    this.form.resetForm();
    this.btnText = 'Save';
    this.gpfWithdraw = {};
    this.bgColor = '';
    this.isClicked = false;
  }
 
  setDeleteId(msGpfWithdrawRulesRefID) {
    // debugger;
    this.setDeletIDOnPopup = msGpfWithdrawRulesRefID;
  }
  deleteGpfWithdrawalRules(id: any) {
    // debugger
    this.gpfWithdrawal.deleteGpfWithdrawalRules(id).subscribe(res => {
      if (res == id) {
        this.resetForm();
      }
      if (res != id) {
        this.deletePopUp = false;
        this.resetForm();
        this.message = this.commonMsg.deleteMsg;
        this.divBgColor = "alert-danger";
        this.btnStatus = true;
      }
      this.getGpfWithdrawalRules();
      setTimeout(() => {
        this.btnStatus = false;
        this.message = '';
      }, 8000);
    })
  }
  charaterOnlyNoSpace(event): boolean {
    //  debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      
    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }

}

