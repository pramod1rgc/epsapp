import { Component, OnInit, ViewChild} from '@angular/core';
import { PfType, State, StateAGOfficers } from '../../model/masters/state-agofficers';
import { StateAgService } from '../../services/Masters/state-ag.service';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { get } from 'selenium-webdriver/http';
@Component({
  selector: 'app-state-agofficers',
  templateUrl: './state-agofficers.component.html',
  styleUrls: ['./state-agofficers.component.css']
})
export class StateAGOfficersComponent implements OnInit {
  displayedColumns: string[] = ['Sr.No.','PF Type', 'State', 'State AG Description', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  response: string;
  objPfType: PfType
  objState: State
  objStateAg: StateAGOfficers = {} as any;
  submitted = false;
  PfTypeList: any = [];
  StateList: any = [];
  StateAgList: any = [];
  filterList: any = [];
  errorMsg: string;
  is_btnStatus = true;
  //error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  @ViewChild('StateAg') StateAgForm: any;
  disbleflag = false;
  constructor(private _service: StateAgService) { }
  ngOnInit() {
    this.BindPftype();
    this.BindState();
    this.bindStateAgList();
  }
  bindStateAgList() {
    this._service.getMsStateAglist().subscribe(res => {
      this.StateAgList = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  BindPftype() {
    this._service.getMsPfTypeList().subscribe(res => {
      this.objPfType = res;
      this.PfTypeList = this.objPfType;
      
  })
  }
  BindState() {
    this._service.getMsStateList().subscribe(res => {
      this.objState = res;
      this.StateList = this.objState;

    })
  }
  btnSaveClick() {
    alert(this.objStateAg.PfType
    )
    alert(this.objStateAg.StateAgDesc)
    alert(this.objStateAg.StateCode)
    alert(this.objStateAg.PfTypeDesc)
  }
  EditStateAgMaster(msStateAgId: number) {
  }
  SetDeleteId(msStateAgId: number) {
  }
  btnclose() {
    this.is_btnStatus = false;

  }

}
