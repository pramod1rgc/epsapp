import { Component, OnInit,ViewChild } from '@angular/core';
import { DlismasterService } from '../../services/Masters/dlismaster.service';
import { MasterService } from '../../services/master/master.service';
import { CommonMsg } from '../../global/common-msg';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-dlisrule-master',
  templateUrl: './dlisrule-master.component.html',
  styleUrls: ['./dlisrule-master.component.css']
})
export class DLISRuleMasterComponent implements OnInit {
  pfTypes: any;
  dlisExceptions: any;
  dataSource: any;
  displayedColumns: string[] = ['pfDesc', 'ruleApplicableFromDate', 'pfTypeValidTillDate', 'payCommDesc', 'dlisAvgAmt', 'toPayScale', 'fromPayScale','gpfRuleRefNo','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  notFound = true;
  payCommission: any;
  payScale: any;
  dlisDetails: any = {};
  divBgColor: any
  btnStatus: boolean;
  panelOpenState: any;
  bgColor: string;
  isLoading = false;
  message: any;
  @ViewChild('msGpfDlisExceptions') form: any;
  btnText = 'Save';
  deletePopUp = false;
  setDeletIDOnPopup: any;
  isClicked = false;

  constructor(private dlisMaster: DlismasterService, private master: MasterService, private commonMsg: CommonMsg) { }

  ngOnInit() {
    this.getPfType();
    this.getDlisExceptions();
    this.getPayCommission();
  }
  getPfType() {
    this.dlisMaster.getPfType().subscribe(data => {
      this.pfTypes = data;
    })
  }
  getPayCommission() {
    this.master.getPayCommissionListLien().subscribe(data => {
      this.payCommission = data;
    })
  }
  getPayScale() {
    this.dlisDetails.fromMsPayScaleID = '';
    this.dlisDetails.toMsPayScaleID = '';
    this.dlisMaster.getPayScale(this.dlisDetails.msPayCommId).subscribe(data => {
      this.payScale = data;
    })
  }
  getDlisExceptions() {
    this.dlisMaster.getDlisExceptions().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (data.length === 0) {
        this.panelOpenState = true;
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  insertUpdateGpfDlisException() {
   // debugger
    this.isLoading = true;
    this.dlisMaster.insertUpdateGpfDlisException(this.dlisDetails).subscribe(res => {
      this.isLoading = false;
      if (this.dlisDetails.msGpfDlisExceptionsRefID === undefined) {
        this.message = this.commonMsg.saveMsg;
        this.btnStatus = true;
        this.divBgColor = "alert-success";
      } else {
        this.message = this.commonMsg.updateMsg;
        this.btnStatus = true;
        this.divBgColor = "alert-success";
       
      }
      this.getDlisExceptions();
      this.resetForm();
      setTimeout(() => {
        this.btnStatus = false;
        this.message = '';
      }, 8000);
    });
  }
  editGpfDlisException(element) {
  //  debugger
    if (element != undefined || element != null) {
      this.panelOpenState = true;
      this.bgColor = "bgcolor";
      this.isClicked = true;
    }
    this.btnText = 'Update';
    this.dlisDetails.msPfTypeId = element.msPfTypeId; 
    this.dlisDetails.msGpfDlisExceptionsRefID = element.msGpfDlisExceptionsRefID; 
    this.dlisDetails.ruleApplicableFromDate = element.ruleApplicableFromDate;
    this.dlisDetails.pfTypeValidTillDate = element.pfTypeValidTillDate;
    this.dlisDetails.msPayCommId = element.msPayCommId;
    this.getPayScale();
    this.dlisDetails.fromMsPayScaleID = element.fromMsPayScaleID;  
    this.dlisDetails.toMsPayScaleID = element.toMsPayScaleID;
    this.dlisDetails.dlisAvgAmt = element.dlisAvgAmt;
    this.dlisDetails.gpfRuleRefNo = element.gpfRuleRefNo;
  }
  resetForm() {
    this.form.resetForm();
    this.btnText = 'Save';
    this.bgColor = '';
    this.isClicked = false;
  }
  setDeleteId(msGpfDlisExceptionsRefID) {
    // debugger;
    this.setDeletIDOnPopup = msGpfDlisExceptionsRefID;
  }
  deleteGpfDlisException(id: any) {
    // debugger
    this.dlisMaster.deleteGpfDlisException(id).subscribe(data => {
      if (data == id) {
        this.resetForm();
      }
      if (data != id) {
        this.deletePopUp = false;
        this.resetForm();
        this.message = this.commonMsg.deleteMsg;
        this.divBgColor = "alert-danger";
        this.btnStatus = true;
      } else if (data === undefined) {
        this.message = this.commonMsg.deleteFailedMsg;
        this.divBgColor = "alert-danger";
        this.btnStatus = true;
      }
      this.getDlisExceptions();
      setTimeout(() => {
        this.btnStatus = false;
        this.message = '';
      }, 8000);
    })
  }
  charaterOnlyNoSpace(event): boolean {
    //  debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }

    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }
}
