import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MastersModule } from './masters.module';
import { LeaveTypesComponent } from './leave-types/leave-types.component';
import { PayScaleComponent } from './pay-scale/pay-scale.component';
import { DAMastersComponent } from './damasters/damasters.component';
import { DesignationMasterComponent } from './designation-master/designation-master.component';
import { HraMasterComponent } from './hra-master/hra-master.component';
import { TptaMasterComponent } from './tpta-master/tpta-master.component';
import { GpfinterestrateComponent } from './gpfinterestrate/gpfinterestrate.component';
import { StateAGOfficersComponent } from './state-agofficers/state-agofficers.component';
import { FdGpfMasterComponent } from './fd-gpf-master/fd-gpf-master.component';
import { DLISRuleMasterComponent } from './dlisrule-master/dlisrule-master.component';
import { DuesDuesMasterComponent } from './dues-dues-master/dues-dues-master.component';
import { DuesRateComponent } from './dues-rate/dues-rate.component';
import { CitymasterComponent }  from './citymaster/citymaster.component'
import { GpfadvancereasonsComponent } from './gpfadvancereasons/gpfadvancereasons.component';
import { GpfWithdrawRulesComponent } from './gpf-withdraw-rules/gpf-withdraw-rules.component';
import { GpfWithdrawReasonsComponent } from './gpf-withdraw-reasons/gpf-withdraw-reasons.component';
import { GPFRecoveryRulesComponent } from './gpfrecovery-rules/gpfrecovery-rules.component';
import { GpfadvancerulesComponent } from './gpfadvancerules/gpfadvancerules.component';
import { PaomasterComponent } from './paomaster/paomaster.component';
import { DdomasterComponent } from './ddomaster/ddomaster.component';
import { PraoComponent } from './prao/prao.component';
import { PsuComponent } from './psu/psu.component';
import { DeductionmasterComponent } from './deductionmaster/deductionmaster.component';
import { DeductionRateComponent } from './deduction-rate/deduction-rate.component';
import { MasterofficeComponent } from './masteroffice/masteroffice.component';



const routes: Routes = [

  {
    path: '', component: MastersModule, data: { breadcrumb: 'Master'}, children: [
      { path: 'leavetype', component: LeaveTypesComponent },
      {
        path: 'payscale', component: PayScaleComponent},
      { path: 'damst', component: DAMastersComponent },
      { path: 'designmst', component: DesignationMasterComponent },
      { path: 'hramst', component: HraMasterComponent},
      {
        path: 'gpsinterestrate', component: GpfinterestrateComponent},
      { path: 'tptamst', component: TptaMasterComponent},
      { path: 'stateagofficers', component: StateAGOfficersComponent },
      {
        path: 'fdgpfmst', component: FdGpfMasterComponent},
      { path: 'dlisrule', component: DLISRuleMasterComponent },
      { path: 'Duesmaster', component: DuesDuesMasterComponent },
      { path: 'DuesRatemst', component: DuesRateComponent },
      { path: 'citymst', component: CitymasterComponent },
      {
        path: 'gpfadvancereasons', component: GpfadvancereasonsComponent},
      { path: 'gpfwithdrawrules', component: GpfWithdrawRulesComponent },
      { path: 'gpfwithdrawreasons', component: GpfWithdrawReasonsComponent },
      {
        path: 'gpfFRecoveryRules', component: GPFRecoveryRulesComponent},
      {
        path: 'gpfadvancerules', component: GpfadvancerulesComponent
      },
      {
        path: 'ddomaster', component: DdomasterComponent
      },
      {
        path: 'paomaster', component: PaomasterComponent
      }
      ,
      {
        path: 'Prao', component: PraoComponent
      }
      ,
      {
        path: 'Psu', component: PsuComponent
      },
      
      {
        path: 'deductionmst', component: DeductionmasterComponent
      },

      {
        path: 'deductionRate', component: DeductionRateComponent
      }
      ,

      {
        path: 'masteroffice', component: MasterofficeComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MastersRoutingModule { }
