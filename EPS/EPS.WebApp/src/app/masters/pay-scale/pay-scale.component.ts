import { PayscaleService } from '../../services/payscale.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule,MatSnackBar  } from '@angular/material';
import { debug } from 'util';
import * as $ from 'jquery';
import swal from 'sweetalert2';
@Component({
  selector: 'app-pay-scale',
  templateUrl: './pay-scale.component.html',
  styleUrls: ['./pay-scale.component.css']
})
export class PayScaleComponent implements OnInit {
  msPayScaleID: number;
  Message: any;
  CommissionCodelist: any;
  msCddirID:any;
  Payscalefmtlist: any;
  PayScaleGrouplist: any;
  PayScalefield: any;
  options: FormGroup;
  public paylevelstatus = false;
  public gradepaystatus = false;
  payscalelist: any = {};
  mstpayscale: any;
  arrvalue: any[];
  payscaleArrobj: payscaleArr[] = [];
  array = "";
  btnUpdatetext: any;
  tempmsCddirID: number;
  temppayScaleCode: string;
  _pScaleObj: PayscaleModel;
  _editScaleObj: PayscaleModel;
  dataSource: any;
  show: boolean = true;
  setDeletIDOnPopup: any;
  control: FormControl;
  is_commtype: boolean;
  deletepopup: boolean;
  is_btnStatus = true;
  constructor(private http: HttpClient, private payscale: PayscaleService, private snackBar: MatSnackBar) {
    this.control = new FormControl('', [Validators.required]);
  }
 displayedColumns: string[] = [/*'position',*//*'name',*/ 'PayScaleCode', 'WithEffectFrom', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('f') payScaleForm   : any;

  ngAfterViewInit() { 
  }
  ngOnInit() {
    this._pScaleObj = new PayscaleModel();
    this._editScaleObj = new PayscaleModel();
    this.BindCommissionCode();
    this.BindPayScaleFormat();
    this.BindPayScaleGroup();
    this.GetPayScaleDetails();
    this.btnUpdatetext = 'Save';
    this.show = false;
    this.is_commtype = false;
  }
  //A 15-feb-
  SetDeleteId(msPayScaleID) {

    this.setDeletIDOnPopup = msPayScaleID;
  }

  GetPayScaleDetails() {
    this.payscale.GetPayScaleDetails().subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  GetPayScaleDetailsBYComCode(CommCDID:any) {
    this.payscale.GetPayScaleDetailsBYComCode(CommCDID).subscribe(result => {
    //this.payscale.GetPayScaleDetails().subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  GetPayScaleCode(CddirCodeValue:any) {
  this.payscale.GetPayScaleCode(CddirCodeValue).subscribe(result => {
    this._pScaleObj.payScaleCode= result;
  })
}


  BindCommissionCode() {
    this.payscale.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }
  Cancel() {
    $(".dialog__close-btn").click();
    this.resetForm();
  }


  BindPayScaleFormat() {
    this.payscale.BindPayScaleFormat().subscribe(result => {
      this.Payscalefmtlist = result;
    })
  }

  BindPayScaleGroup() {

    this.payscale.BindPayScaleGroup().subscribe(result => {
      this.PayScaleGrouplist = result;
    })
  }

  ClearInput() {
    this._pScaleObj.msPayScaleID = null;
    this._pScaleObj.msCddirID = null;
    this._pScaleObj.paysfm_id = null;
    this._pScaleObj.paysGroup_id = null;
    this._pScaleObj.payScaleCode = null;
    this._pScaleObj.paysfm_id = null;
    this._pScaleObj.payScaleGradePay = null;
    this._pScaleObj.payGISGroupID = null;
    this._pScaleObj.payScaleWithEffectFrom = null;
    this._pScaleObj.payScaleLevel = null;
    this._pScaleObj.payScaledescription = null;
    this['L'] = null;
    this['I1'] = null;
    this['S1'] = null;
    this['I2'] = null;
    this['S2'] = null;
    this['I3'] = null;
    this['S3'] = null;
    this['I4'] = null;
    this['U'] = null;
    this.payscaleArrobj = [];
    this.btnUpdatetext = 'Save';
    this.is_commtype = false;
  }

  resetForm() {
    this._pScaleObj.msPayScaleID = null;
   // this.payScaleForm.resetForm();
    this.btnUpdatetext = 'Save';
    this.ClearInput();
  }

  valMsg: string='';
  CreatePayScale() {
    debugger;
    if (this._pScaleObj.msCddirID == null) {
      alert('Commission Code required')
      //this.valMsg + = "Commission Code required ?";
    }
    else if (this._pScaleObj.payScaleLevel == null && this.paylevelstatus == true) {
      alert('Pay Level required')
      //this.valMsg + = "Level required?";
    }
    else if (this._pScaleObj.paysGroup_id == null || this._pScaleObj.paysGroup_id == undefined) {
      alert('Group required')
      //this.valMsg + = "Group required?";
    }
    else if (this._pScaleObj.paysfm_id == null || this._pScaleObj.paysfm_id == undefined) {
      alert('Format required')
      //this.valMsg + = "Format required?";
    }
    
    else if (this._pScaleObj.payGISGroupID == null || this._pScaleObj.payGISGroupID==undefined) {
      alert('GIS Group required')
      //this.valMsg + = "GIS Group required?";
    }
    
    else if ((this._pScaleObj.payScaleGradePay == null || this._pScaleObj.payScaleGradePay == '' || this._pScaleObj.payScaleGradePay == undefined) && this.gradepaystatus == true)
    {
      alert('Grade Pay required')
      //this.valMsg + = "Grade Pay required?";

    }
    else if (Number(this._pScaleObj.payScaleGradePay)==0 && this.gradepaystatus == true) {
      alert('Grade Pay should not be zero')
      //this.valMsg + = "Grade Pay should not be zero?";
    }
    else if (this._pScaleObj.payScaleWithEffectFrom == '' || this._pScaleObj.payScaleWithEffectFrom == undefined) {
      alert('With Effect From required')
      //this.valMsg + = "With Effect From required?";
    }
    else {
      this._pScaleObj.payScalePscLoLimit = this['L'];
      this._pScaleObj.payScalePscInc1 = this['I1'];
      this._pScaleObj.payScalePscStage1 = this['S1'];
      this._pScaleObj.payScalePscInc2 = this['I2'];
      this._pScaleObj.payScalePscStage2 = this['S2'];
      this._pScaleObj.payScalePscInc3 = this['I3'];
      this._pScaleObj.payScalePscStage3 = this['S3'];
      this._pScaleObj.payScalePscInc4 = this['I4'];
      this._pScaleObj.payScalePscUpLimit = this['U'];
      this._pScaleObj.payScalePscLoLimit = this['L'];
      this.payscale.CreatePayScale(this._pScaleObj).subscribe(result => {
        this.Message = result;
        if (this.Message != undefined) {
          $(".dialog__close-btn").click();
          swal(this.Message)
          //this.snackBar.open(this.Message, '', {
          //  duration: 2000,
    
          //});

        }
        this.GetPayScaleDetailsBYComCode(this._pScaleObj.msCddirID)
        // this.ClearInput();
        this.resetForm();
      })
    }
    //else {
    //  //  this.model = false;
    //  //this.show = true;
    //}
  }
  //CreatePayScale() {
  //  debugger;
   
  //  this._pScaleObj.payScalePscLoLimit = this['L'];
  //  this._pScaleObj.payScalePscInc1 = this['I1'];
  //  this._pScaleObj.payScalePscStage1 = this['S1'];
  //  this._pScaleObj.payScalePscInc2 = this['I2'];
  //  this._pScaleObj.payScalePscStage2 = this['S2'];
  //  this._pScaleObj.payScalePscInc3 = this['I3'];
  //  this._pScaleObj.payScalePscStage3 = this['S3'];
  //  this._pScaleObj.payScalePscInc4 = this['I4'];
  //  this._pScaleObj.payScalePscUpLimit = this['U'];
  //  this._pScaleObj.payScalePscLoLimit = this['L'];
  //  if (this._pScaleObj.msCddirID != null) {
  //    this.payscale.CreatePayScale(this._pScaleObj).subscribe(result => {
  //      this.Message = result;
  //      if (this.Message != undefined) {
  //        $(".dialog__close-btn").click();
  //        //swal(this.Message)
  //        this.snackBar.open(this.Message, '', {
  //          duration: 2000,
  //          //verticalPosition: 'top',
  //          //horizontalPosition: 'center'
  //        });

  //      }
  //      this.GetPayScaleDetailsBYComCode(this._pScaleObj.msCddirID)
  //     // this.ClearInput();
  //      this.resetForm();
  //    })
  //  }
  //  else {
  //  //  this.model = false;
  //    this.show = true;
  //  }
  //}

  Delete(msPayScaleID) {
    this.payscale.Delete(msPayScaleID).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        swal(this.Message)
        //this.snackBar.open(this.Message, '', {
        //  duration: 2000,
        //  //verticalPosition: 'top',
        //  //horizontalPosition: 'center'
        //});
      }
      this.GetPayScaleDetails();
    })
  }




  EditPayScale(msPayScaleID, payScaleCode)  {
    this.payscale.EditPayScaleDetails(msPayScaleID).subscribe(result => {
      this._pScaleObj = result;
      this.is_commtype = true;
      this['U'] = this._pScaleObj.payScalePscUpLimit;
      this['L'] = this._pScaleObj.payScalePscLoLimit;
      this['I1'] = this._pScaleObj.payScalePscInc1;
      this['S1'] = this._pScaleObj.payScalePscStage1;
      this['I2'] = this._pScaleObj.payScalePscInc2;
      this['S2'] = this._pScaleObj.payScalePscStage2;
      this['I3'] = this._pScaleObj.payScalePscInc3;
      this['S3'] = this._pScaleObj.payScalePscStage3;
      this['I4'] = this._pScaleObj.payScalePscInc4;
      this._pScaleObj.payScaleCode = this._pScaleObj.payScaleCode;
      this._pScaleObj.msPayScaleID = this._pScaleObj.msPayScaleID;
      this._pScaleObj.msCddirID = this._pScaleObj.msCddirID;
      this.tempmsCddirID = this._pScaleObj.msCddirID;
      this._pScaleObj.paysfm_id = this._pScaleObj.paysfm_id;
      this._pScaleObj.paysGroup_id = this._pScaleObj.paysGroup_id;
      this._pScaleObj.payScaleCode = this._pScaleObj.payScaleCode;
      this.temppayScaleCode = this._pScaleObj.payScaleCode;
      this._pScaleObj.payScale = this._pScaleObj.payScale;
      this._pScaleObj.payScaleGradePay = this._pScaleObj.payScaleGradePay;
      this._pScaleObj.payGISGroupID = this._pScaleObj.payGISGroupID;
 
      this._pScaleObj.payScaleWithEffectFrom = this._pScaleObj.payScaleWithEffectFrom;
      this._pScaleObj.payScaledescription = this._pScaleObj.payScaledescription;    
      this._pScaleObj.payScaleLevel = this._pScaleObj.payScaleLevel;
      //this['U'] = this._pScaleObj.payScalePscUpLimit;
      //this['L'] = this._pScaleObj.payScalePscLoLimit;
      //this['I1'] = this._pScaleObj.payScalePscInc1;
      //this['S1'] = this._pScaleObj.payScalePscStage1;
      //this['I2'] = this._pScaleObj.payScalePscInc2;
      //this['S2'] = this._pScaleObj.payScalePscStage2;
      //this['I3'] = this._pScaleObj.payScalePscInc3;
      //this['S3'] = this._pScaleObj.payScalePscStage3;
      //this['I4'] = this._pScaleObj.payScalePscInc4;

      this.ChangePayS_Fmt(this._pScaleObj.paysfm_id);

      this.CommissionCode(this._pScaleObj.msCddirID);
      this.btnUpdatetext = 'Update'; 
    })
  }
  //=======================Change form dynamic Commission Code change============================
  CommissionCode(value) {

    for (let code of this.CommissionCodelist) {
      if (value != undefined) {
        this.GetPayScaleDetailsBYComCode(value);
      }

      if (value == code.msCddirID) {
        this.show = false;

        if (code.cddirCodeText.trim() == "6th Pay Commission ( Central Govt. )") {
          this.paylevelstatus = false;
          this.gradepaystatus = true;
        }
        else if (code.cddirCodeText.trim() == "Consolidated Pay") {
          this.paylevelstatus = false;
          this.gradepaystatus = false;
        }
        else if (code.cddirCodeText.trim() == "7th Pay Commission(Central)") {
          this.paylevelstatus = true;
          this.gradepaystatus = true;
        }
        else if (code.cddirCodeText.trim() == "Other Pay Commission") {
          this.paylevelstatus = true;
          this.gradepaystatus = false;
        }
      }
    }
    if (this.tempmsCddirID != value && value != undefined) {

      this.GetPayScaleCode(value);

    }
    else {

      this._pScaleObj.payScaleCode = this.temppayScaleCode;

    }


  }

  public clearArray() {
    this.payscaleArrobj = [];
  }

  ChangePayS_Fmt(value) {
    debugger;
    this.clearArray();
    for (let num of this.Payscalefmtlist) {
      if (value == num.paysfm_id) {
        if (num.paysfm_Text.trim() != "N.A.") {
          var array = num.paysfm_Text.split('-');
          var fieldlength = num.paysfm_Text.split('-').length;
          for (let i = 0; i < fieldlength; i++) {
            this.payscaleArrobj.push(
              new payscaleArr(array[i].trim(), i));
          }

        }
        else {
          this.payscaleArrobj = [];
        }
      }
    }
  }
  charaterOnlyNoSpace(event): boolean {
    debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

    //return true;

  
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  numberCharOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 47 && charCode < 58) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
      return true;
    }
    return false;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  btnclose() {
    this.is_btnStatus = false;
  }
}
export class payscaleArr {
  name: string;
  id: number;
  constructor(_name: string, _id: number) {
    this.name = _name;
    this.id = _id;
  }

}


