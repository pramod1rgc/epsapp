import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, FormGroupDirective } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatTableModule, MatTable } from '@angular/material';
import { MatSelect } from '@angular/material';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { DuesRateServicesService } from '../../services/masters/dues-rate-services.service';
import { from } from 'rxjs';
import { CitymastreservicesService } from '../../services/Masters/citymastreservices.service';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { error } from 'util';
import { CommonMsg } from '../../global/common-msg';
import { invalid } from '@angular/compiler/src/render3/view/util';
@Component({
  selector: 'app-citymaster',
  templateUrl: './citymaster.component.html',
  styleUrls: ['./citymaster.component.css'],
  providers: [CommonMsg]
})
export class CitymasterComponent implements OnInit {
  is_btnStatus = true;
  isTableHasData = true;
  form: FormGroup;
  savebuttonstatus: boolean;
  submitted = false;
  @ViewChild('f') CitymasterviewChild: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  btnUpdateText: string
  displayedColumns = ['srNo', 'payCommDesc', 'cclCityclass', 'action']
  PayCommissions: any = [];
  dataSource: any = [];
  Message: any;
  setDeletIDOnPopup: any;
  userName: string = '';
  deletepopup: boolean;
  filterList: any = [];
  CityMasterList: any;
  errorMessage: any = [];
  DisableStatus: boolean;
  totalCount: number = 0;
  CheckRecordExits: any;
  isSuccessStatus: boolean = false;
  isWarningStatus: boolean = false;
  responseMessage: string = '';
  isOnInit: boolean = false;
  isCityMasterPanel: boolean = false;
  bgColor: string;
  btnCssClass: string = 'btn btn-success';
  constructor(private fb: FormBuilder, private _service: DuesRateServicesService, private _servivecityMaster: CitymastreservicesService, private _msg: CommonMsg) {
  }
  ngOnInit() {
    this.savebuttonstatus = true;
    this.btnUpdateText = "Save";
    this.GetPayCommType();
    this.FormDeatils();
    this.GetCitymasterDetails();
  }
  GetPayCommType() {
    this._service.GetPayCommForDuesRate().subscribe(res => {
      this.PayCommissions = res;
    })
  }
  GetCitymasterDetails() {
    debugger;
    this._servivecityMaster.geCityClassMasterList().subscribe(res => {
      this.CityMasterList = res;
      this.dataSource = res;
      this.dataSource = new MatTableDataSource(res);
      //filter in Data source
        //return data.PayCommDesc.toLowerCase().includes(filter) || data.CclCityclass.toLowerCase().includes(filter);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
    })
  }
  editButtonClick(obj) {
    this.GetPayCommType();
    this.btnUpdateText = "Update";
    this.form.patchValue(obj);
    this.form.controls.cclPaycommId.setValue(obj.cclPaycommId);
    this.form.controls.msCityclassID.setValue(obj.msCityclassID);
    this.btnUpdateText = 'Update';
    this.isCityMasterPanel = true;
    this.bgColor = 'bgcolor';
    this.btnCssClass = 'btn btn-info';
  }
  deleteDuesDefinationByDuesCode(DuesCd) {
    debugger;
    this._servivecityMaster.DeleteCityClass(DuesCd).subscribe(res => {
      this.Message = this._msg.deleteMsg;
      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        this.isSuccessStatus = false;
        this.isWarningStatus = true;
        this.responseMessage = this._msg.deleteMsg;
      }

      this.GetCitymasterDetails();
      setTimeout(() => {
        this.isSuccessStatus = false;
        this.isWarningStatus = false;
        this.responseMessage = '';
      }, this._msg.messageTimer);
    });
    this.resetForm()
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  resetForm() {
    this.form.reset();
    this.formGroupDirective.resetForm();
    this.btnUpdateText = "Save";
  }
  omit_special_char(event) {
    var k;
    k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

  applyFilter(filterValue: string) {

    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function (data, filter: string): boolean {
      return data.payCommDesc.toLowerCase().includes(filter) || data.cclCityclass.toLowerCase().includes(filter);
    };

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.isTableHasData = true;
    } else {
      this.isTableHasData = false;
    }
  }

  charaterOnlyNoSpace(event): boolean {


    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  onSubmit() {
    debugger;
    this.form.get('userName').setValue(sessionStorage.getItem('username'));

    if (this.form.valid) {
      this._servivecityMaster.SaveCityClass(this.form.value).subscribe(response => {
        this.Message = response;
        if (this.Message != undefined) {
          this.isSuccessStatus = true;
          this.responseMessage = response;

        }

        this.GetCitymasterDetails();
        this.bgColor = '';
        this.btnCssClass = 'btn btn-success';
        setTimeout(() => {
          this.isSuccessStatus = false;
          this.isWarningStatus = false;
          this.responseMessage = '';
        }, this._msg.messageTimer);
      });
    }
      this.resetForm();
  }
  FormDeatils() {
    this.form = this.fb.group({
      msCityclassID:[0],
      cclPaycommId: [0, Validators.required],
      cclCityclass: [null, [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      userName: new FormControl('')
    });
  }
  Cancel() {
    $(".dialog__close-btn").click();
    this.resetForm();
  }
  onSelectionChanged($event) {
  //this.checkAllreadyexist(this.form.controls['cclCityclass'].value);
  }
  //}

  checkAllreadyexist(filterValue: string) {
    this.filterList = this.CityMasterList.filter(item => item.cclCityclass == filterValue && item.cclPaycommId == this.form.controls['cclPaycommId'].value )
    if (this.filterList.length > 0) {
      this.Message = 'Record Already Exist!';
      if (this.Message != undefined) {
        $(".dialog__close-btn").click();

        swal(this.Message);
        this.form.setErrors({ 'invalid': true });
      }
    }
    else {
      this.form.clearValidators();
      this.Message = null;

    }
  }
  btnclose() {
    this.is_btnStatus = false;
  }
}
