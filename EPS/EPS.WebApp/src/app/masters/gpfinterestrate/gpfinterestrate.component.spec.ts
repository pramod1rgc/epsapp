import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfinterestrateComponent } from './gpfinterestrate.component';

describe('GpfinterestrateComponent', () => {
  let component: GpfinterestrateComponent;
  let fixture: ComponentFixture<GpfinterestrateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfinterestrateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfinterestrateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
