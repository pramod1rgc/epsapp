import { Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {GpfinterestrateService } from '../../services/Masters/gpfinterestrate.service'
import { HttpClient } from '@angular/common/http';
import { Gpfinterestratemodel } from '../../model/masters/gpfinterestratemodel';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { DatePipe} from '@angular/common';
@Component({
  selector: 'app-gpfinterestrate',
  templateUrl: './gpfinterestrate.component.html',
  styleUrls: ['./gpfinterestrate.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class GpfinterestrateComponent implements OnInit {
  constructor(private http: HttpClient, private gpfservice: GpfinterestrateService) { }
  objgpf: Gpfinterestratemodel;
  btnUpdatetext: any;
  savebuttonstatus: boolean;
  setDeletIDOnPopup: any;
  displayedColumns: string[] = ['wefMonthYear', 'toMonthYear', 'newGPFInterestRate', 'gPFRuleReferenceNumber','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('gpfviewchild') gpfmaster: any;
  @ViewChild('WefDate') wefDate: any;
  gpfservicedataSource: any;
  gpfservicedata: any;
  Message: any;
  isTableHasData = true;
  is_toMonthYear: boolean;
  is_wefMonthYear: boolean;
  deletepopup: boolean;
  pipe: DatePipe;
  searchfield: any;
  is_btnStatus = true;
  divbgcolor: string;
  bgcolor: string;
  btnCssClass = 'btn btn-success';
  ngOnInit() { 
    this.objgpf = new Gpfinterestratemodel();
    this.btnUpdatetext = 'Save';
    this.savebuttonstatus = true;
    this.GetGPFInterestRateDetails();
    this.is_wefMonthYear = false;
    this.is_toMonthYear = false;
  }

  dateFilter = (ob: Date) => new Date(ob).getDate() == 1;
  dateFilter2 = (ob1: Date) => new Date(ob1).getDate() == this.getDaysInMonth(new Date(ob1).getMonth(), new Date(ob1).getFullYear())

  Edit(gPFInterestID, newGPFInterestRate, wefMonthYear, toMonthYear, gPFRuleReferenceNumber) {
    this.objgpf.gPFInterestID = gPFInterestID;
    this.objgpf.newGPFInterestRate = newGPFInterestRate;
    this.objgpf.wefMonthYear = wefMonthYear;
    this.objgpf.toMonthYear = toMonthYear;
    this.objgpf.gPFRuleReferenceNumber = gPFRuleReferenceNumber;
    this.btnUpdatetext = 'Update';
    this.is_wefMonthYear = true;
    this.is_toMonthYear = true;
    this.bgcolor = "bgcolor";
    this.btnCssClass = 'btn btn-info';
    this.is_btnStatus = false;
    this.Message = '';
  }

  getDaysInMonth(month, year): number{
  // Here January is 1 based
  //Day 0 is the last day in the previous month  
  return new Date(year, month+1, 0).getDate();
  // Here January is 0 based
  // return new Date(year, month+1, 0).getDate();
};
  Info(gPFInterestID, newGPFInterestRate, wefMonthYear, toMonthYear, gPFRuleReferenceNumber) {
    this.objgpf.gPFInterestID = gPFInterestID;
    this.objgpf.newGPFInterestRate = newGPFInterestRate;
    this.objgpf.wefMonthYear = wefMonthYear
    this.objgpf.toMonthYear = toMonthYear;
    this.objgpf.gPFRuleReferenceNumber = gPFRuleReferenceNumber;
  }

  GetGPFInterestRateDetails() {
    this.gpfservice.GetGPFInterestRateDetails().subscribe(result => {
      this.gpfservicedata = new MatTableDataSource(result);
      this.gpfservicedata.paginator = this.paginator;
      this.gpfservicedata.sort = this.sort;
      this.pipe = new DatePipe('en');
      const defaultPredicate = this.gpfservicedata.filterPredicate;
      this.gpfservicedata.filterPredicate = (data, filter) => {
        var toMonthYear = this.pipe.transform(data.toMonthYear, 'dd/MM/yyyy');
        var wefMonthYear = this.pipe.transform(data.wefMonthYear, 'dd/MM/yyyy');
        return wefMonthYear.indexOf(filter) >= 0 || toMonthYear.indexOf(filter) >= 0 || defaultPredicate(data, filter);
      }
    })
  }

  SetDeleteId(gpfInterestID) {
    this.setDeletIDOnPopup = gpfInterestID;
  }

  InsertandUpdate() {
    //if (new Date(this.objgpf.wefMonthYear).valueOf() >= new Date(this.objgpf.toMonthYear).valueOf()) {
    //  alert("Wef month & year should be greater then To Month & Year!");
    //  return false;
    //}
     if(Number(this.objgpf.newGPFInterestRate) < 1 || Number(this.objgpf.newGPFInterestRate) >= 100) {
      alert('Interest Rate Range Between 1 to 100!');
      return false;
    }
    else {
      this.gpfservice.InsertandUpdate(this.objgpf).subscribe(result => {
        if (result != undefined && result != 'Wef month & year already exist') {
          this.deletepopup= false;
          this.is_btnStatus = true;
          this.divbgcolor = "alert-success"; 
          this.GetGPFInterestRateDetails();
          this.resetForm();
          this.searchfield = "";
          this.bgcolor = "";
          this.Message = result;
        }
        else if (result == 'Wef month & year already exist') {
          this.deletepopup = false;
          this.Message = result;
        }
        setTimeout(() => {
          this.is_btnStatus = false;
          this.Message = '';
        }, 8000);
      })
    }
  }

  resetForm() {
   this.objgpf.gPFInterestID = 0;
    this.gpfmaster.resetForm();
    this.btnUpdatetext = 'Save';
    this.is_wefMonthYear = false;
    this.is_toMonthYear = false;
    this.bgcolor = "";
    this.btnCssClass = 'btn btn-success';
  }

  Delete(gpfInterestID) {
    this.gpfservice.Delete(gpfInterestID).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        this.deletepopup = false;
        this.is_btnStatus = true;
        this.divbgcolor = "alert-danger";
      }

      setTimeout(() => {
        this.is_btnStatus = false;
        this.Message = '';
      }, 8000);
      this.GetGPFInterestRateDetails();
    })
  }
  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  applyFilter(filterValue: string) {
    this.gpfservicedata.filter = filterValue.trim().toLowerCase();
    if (this.gpfservicedata.paginator) {
      this.gpfservicedata.paginator.firstPage();
    }
    if (this.gpfservicedata.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }
  //btnclose() {
  //  this.is_btnStatus = false;

  //}

}
