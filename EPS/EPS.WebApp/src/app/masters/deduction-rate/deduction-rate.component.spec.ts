import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionRateComponent } from './deduction-rate.component';

describe('DeductionRateComponent', () => {
  let component: DeductionRateComponent;
  let fixture: ComponentFixture<DeductionRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
