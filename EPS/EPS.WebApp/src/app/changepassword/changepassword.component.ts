import { Component, OnInit } from '@angular/core';
import { LoginServices } from '../login/loginservice';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css'],
  providers: [LoginServices]
})
export class ChangepasswordComponent implements OnInit {
  DeleteMenu: boolean;
  username: string;
  ObjPwdDetails: any = {};
  PasswordChangeStatus: string;

  constructor(private router: Router, private _LoginServices: LoginServices, private fb: FormBuilder) { this.frmSignup = this.createSignupForm(); }

  ngOnInit() {
    this.DeleteMenu = true;
    this.ObjPwdDetails.username = sessionStorage.getItem('NewUser');
  }

  Save() {
    
    if (this.ObjPwdDetails.OldPassword == null) {
      alert("Please enter OldPassword")
      return;
    }

    if (this.ObjPwdDetails.NewPassword == null) {
      alert("Please enter New Password")
      return;
    }
    if (this.ObjPwdDetails.ConfirmPassword == null) {
      alert("Please enter Confirm Password")
      return;
    }
    if (this.ObjPwdDetails.ConfirmPassword != this.ObjPwdDetails.NewPassword) {
      alert("Please enter match New Password and Confirm Password")
      return;
    }
   
    this._LoginServices.ChangePassword(this.ObjPwdDetails).subscribe(data => {
      this.PasswordChangeStatus = data;
      debugger;
      if (this.PasswordChangeStatus == 'Pass') {
        alert("Password changed successfully, Please login!");
        this.router.navigate(['/login'])
      }
      if (this.PasswordChangeStatus == 'change') {
        alert("Please change Password");
      }
    });
    this.clear()
  }

  clear() {
    this.ObjPwdDetails.OldPassword = "";
    this.ObjPwdDetails.NewPassword = "";
    this.ObjPwdDetails.ConfirmPassword = "";

  }
  Cancel() {
    this.DeleteMenu = false;
    this.router.navigate(['/login']);
  }
  public restrictNumeric(password) {
    debugger
    //alert('Blur')
    var minMaxLength = /^[\s\S]{8,15}$/,
      upper = /[A-Z]/,
      lower = /[a-z]/,
      number = /[0-9]/,
      special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;

    if (minMaxLength.test(password) &&
      upper.test(password) &&
      lower.test(password) &&
      number.test(password) &&
      special.test(password)
    ) {
      return true;
    }
    else {
      alert('Passwords must contain at least eight characters, including uppercase, lowercase letters,operator and numbers.');
      return false;
    }
    
  }

  //----For test------
  public frmSignup: FormGroup;
  createSignupForm(): FormGroup {
    return this.fb.group(
      {
        email: [
          null,
          Validators.compose([Validators.email, Validators.required])
        ],
        password: [
          null,
          Validators.compose([
            Validators.required,
            // check whether the entered password has a number
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            // check whether the entered password has upper case letter
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalCase: true
            }),
            // check whether the entered password has a lower case letter
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallCase: true
            }),
            // check whether the entered password has a special character
            CustomValidators.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
              {
                hasSpecialCharacters: true
              }
            ),
            Validators.minLength(8)
          ])
        ],
        confirmPassword: [null, Validators.compose([Validators.required])]
      },
      {
        // check whether our password and confirm password match
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }

  submit() {
    //console.log(this.frmSignup.value);
    this.ObjPwdDetails.username = this.frmSignup.value.email;
    this.ObjPwdDetails.NewPassword = this.frmSignup.value.password;
    this._LoginServices.ChangePassword(this.ObjPwdDetails).subscribe(data => {
      this.PasswordChangeStatus = data;
      debugger;
      if (this.PasswordChangeStatus == 'Pass') {
        alert("Password changed successfully, Please login!");
        this.router.navigate(['/login'])
      }
      if (this.PasswordChangeStatus == 'change') {
        alert("Please change Password");
      }
    });
    this.clear();
  }

}
