import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { RecoveryChallanService } from '../../services/loan-mgmt/recovery-challan.service';
import { MatSnackBar, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { CommanService } from '../../services/loan-mgmt/comman.service';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  status: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
  { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
  { position: 1, name: 'Hydrogen', weight: 1.0079, status: 'pending', symbol: 'H' },
];
export interface PeriodicElement2 {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA2: PeriodicElement2[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'Verified' },
];
@Component({
  selector: 'app-recovry-challan',
  templateUrl: './recovry-challan.component.html',
  styleUrls: ['./recovry-challan.component.css']
})
export class RecovryChallanComponent implements OnInit {
  btnUpdateText: string;
  form: FormGroup;
  @ViewChild('formDirective') formDirective: any;
  _loanType: any = [];
  userRoleID: number;
  showAndHideMode: number;
  RevocationFlag: boolean = false;
  getPaymentById: any = {};
  panelOpenState: boolean = false;
  displayedColumns: string[] = ['LoanType', 'AmountDisbursed', 'CurrentRecoveryof', 'TotalNoInstalments', 'createdBy', 'status', 'symbol'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  //@ViewChild('formDirective') private formDirective;
  displayedColumns2: string[] = ['LoanType', 'AmountDisbursed', 'CurrentRecoveryof', 'TotalNoInstalments', 'status', 'symbol'];
  dataSource2 = new MatTableDataSource(ELEMENT_DATA2);
  recoveryArr: any = [];
  currentDate: {};
  @ViewChild(MatSort) sort: MatSort;
  empid: any;
  designid: string;
  username: string;
  ddoid: number;
  UserID: number;
  userRole: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  RecoveryChallanData: MatTableDataSource<{}>;
  isSubmitted = false;
  loantypeFlag: string;
  ngOnInit() {

    this.btnUpdateText = 'Save';
    this.showAndHideMode = 1;
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.dataSource.sort = this.sort;
    this.BindLoanType();
  }
  constructor(private snackBar: MatSnackBar, private _Service: RecoveryChallanService, private objCommanService: CommanService) {
    this.createForm();
  }

  modeChange(val: number) {
    debugger;
    if (val == 1) {
      this.showAndHideMode = 1;
      //this.form.controls['challanNo'].setValidators([Validators.required]);
      //this.form.controls['challanAmount'].setValidators([Validators.required]);
      //this.form.controls['challanDate'].setValidators([Validators.required]);
      //this.form.controls['paymentMode'].setValidators([Validators.required]);
      //this.form.controls['chequeNoDDNo'].setValidators([Validators.required]);
      //this.form.controls['outstandingAmount'].setValidators([Validators.required]);
      //this.form.controls['lastInstalmentNoRecovered'].setValidators([Validators.required]); 


    }
    if (val == 2) {
      this.showAndHideMode = 2;
      //this.form.controls['NTRPTransactionId'].setValidators([Validators.required]);
      //this.form.controls['NTRPchallanAmt'].setValidators([Validators.required]); 
      //this.form.controls['NTRPchallanDate'].setValidators([Validators.required]);

      //this.form.controls['NTRPpaymentMode'].setValidators([Validators.required]);
      //this.form.controls['NTRPReceiptDetails'].setValidators([Validators.required]);
      //this.form.controls['NTRPupdatedOutstandingAmount'].setValidators([Validators.required]);
      //this.form.controls['NTRPupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
    }
    if (val == 3) {
      this.showAndHideMode = 3;

      //this.form.controls['ProchallanNo'].setValidators([Validators.required]);
      //this.form.controls['ProchallanAmount'].setValidators([Validators.required]);
      //this.form.controls['ProchallanDate'].setValidators([Validators.required]);
      //this.form.controls['PropaymentMode'].setValidators([Validators.required]);
      //this.form.controls['ProupdatedOutstandingAmount'].setValidators([Validators.required]);
      //this.form.controls['ProupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]); 
      //this.form.controls['Proremarks'].setValidators([Validators.required]); 



    }
    if (val == 4) {
      this.showAndHideMode = 4;

      //this.form.controls['TENo'].setValidators([Validators.required]);
      //this.form.controls['TEAmounts'].setValidators([Validators.required]);
      //this.form.controls['TEDate'].setValidators([Validators.required]);
      //this.form.controls['TEpaymentMode'].setValidators([Validators.required]);
      //this.form.controls['TEupdatedOutstandingAmount'].setValidators([Validators.required]);
      //this.form.controls['TEupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
      //this.form.controls['TEremarks'].setValidators([Validators.required]); 

    
      //this.form.controls['ProchallanNo'].setValidators(null);
      //this.form.controls.ProchallanNo.setErrors(null);
      //this.form.controls.ProchallanAmount.setValidators(null);
      //this.form.controls.ProchallanAmount.setErrors(null);
      //this.form.controls.ProchallanDate.setValidators(null);
      //this.form.controls.ProchallanDate.setErrors(null);
      //this.form.controls.PropaymentMode.setValidators(null);
      //this.form.controls.PropaymentMode.setErrors(null);
      //this.form.controls.ProupdatedOutstandingAmount.setValidators(null);
      //this.form.controls.ProupdatedOutstandingAmount.setErrors(null);
      //this.form.controls.ProupdatedLastInstalmentNoRecovered.setValidators(null);
      //this.form.controls.ProupdatedLastInstalmentNoRecovered.setErrors(null);
      //this.form.controls.Proremarks.setValidators(null);
      //this.form.controls.Proremarks.setErrors(null);



      //this.form.controls.NTRPTransactionId.setValidators(null);
      //this.form.controls.NTRPTransactionId.setErrors(null);
      //this.form.controls.NTRPchallanAmt.setValidators(null);
      //this.form.controls.NTRPchallanAmt.setErrors(null);
      //this.form.controls.NTRPchallanDate.setValidators(null);
      //this.form.controls.NTRPchallanDate.setErrors(null);
      //this.form.controls.NTRPpaymentMode.setValidators(null);
      //this.form.controls.NTRPpaymentMode.setErrors(null);
      //this.form.controls.NTRPReceiptDetails.setValidators(null);
      //this.form.controls.NTRPReceiptDetails.setErrors(null);
      //this.form.controls.NTRPupdatedOutstandingAmount.setValidators(null);
      //this.form.controls.NTRPupdatedOutstandingAmount.setErrors(null);
      //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setValidators(null);
      //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setErrors(null);



     //// this.form.controls.challanNo.setValidators(null);
     //// this.form.controls.challanNo.setErrors(null);
     // this.form.controls.challanAmount.setValidators(null);
     // this.form.controls.challanAmount.setErrors(null);
     // this.form.controls.challanDate.setValidators(null);
     // this.form.controls.challanDate.setErrors(null);
     // this.form.controls.chequeNoDDNo.setValidators(null);
     // this.form.controls.chequeNoDDNo.setErrors(null);
     // this.form.controls.outstandingAmount.setValidators(null);
     // this.form.controls.outstandingAmount.setErrors(null);
     // this.form.controls.lastInstalmentNoRecovered.setValidators(null);
     // this.form.controls.lastInstalmentNoRecovered.setErrors(null);
    



     
    }

  }


  validateMethod(val: number) {
    debugger;
    if (val == 1) {
      this.showAndHideMode = 1;
      this.form.controls['challanNo'].setValidators([Validators.required]);
      this.form.controls['challanAmount'].setValidators([Validators.required]);
      this.form.controls['challanDate'].setValidators([Validators.required]);
      this.form.controls['paymentMode'].setValidators([Validators.required]);
      this.form.controls['chequeNoDDNo'].setValidators([Validators.required]);
      this.form.controls['outstandingAmount'].setValidators([Validators.required]);
      this.form.controls['lastInstalmentNoRecovered'].setValidators([Validators.required]);


    }
    if (val == 2) {
      this.showAndHideMode = 2;
      this.form.controls['NTRPTransactionId'].setValidators([Validators.required]);
      this.form.controls['NTRPchallanAmt'].setValidators([Validators.required]);
      this.form.controls['NTRPchallanDate'].setValidators([Validators.required]);

      this.form.controls['NTRPpaymentMode'].setValidators([Validators.required]);
      this.form.controls['NTRPReceiptDetails'].setValidators([Validators.required]);
      this.form.controls['NTRPupdatedOutstandingAmount'].setValidators([Validators.required]);
      this.form.controls['NTRPupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
    }
    if (val == 3) {
      this.showAndHideMode = 3;

      this.form.controls['ProchallanNo'].setValidators([Validators.required]);
      this.form.controls['ProchallanAmount'].setValidators([Validators.required]);
      this.form.controls['ProchallanDate'].setValidators([Validators.required]);
      this.form.controls['PropaymentMode'].setValidators([Validators.required]);
      this.form.controls['ProupdatedOutstandingAmount'].setValidators([Validators.required]);
      this.form.controls['ProupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
      this.form.controls['Proremarks'].setValidators([Validators.required]);



    }
    if (val == 4) {
      this.showAndHideMode = 4;

      this.form.controls['TENo'].setValidators([Validators.required]);
      this.form.controls['TEAmounts'].setValidators([Validators.required]);
      this.form.controls['TEDate'].setValidators([Validators.required]);
      this.form.controls['TEpaymentMode'].setValidators([Validators.required]);
      this.form.controls['TEupdatedOutstandingAmount'].setValidators([Validators.required]);
      this.form.controls['TEupdatedLastInstalmentNoRecovered'].setValidators([Validators.required]);
      this.form.controls['TEremarks'].setValidators([Validators.required]);


      //this.form.controls['ProchallanNo'].setValidators(null);
      //this.form.controls.ProchallanNo.setErrors(null);
      //this.form.controls.ProchallanAmount.setValidators(null);
      //this.form.controls.ProchallanAmount.setErrors(null);
      //this.form.controls.ProchallanDate.setValidators(null);
      //this.form.controls.ProchallanDate.setErrors(null);
      //this.form.controls.PropaymentMode.setValidators(null);
      //this.form.controls.PropaymentMode.setErrors(null);
      //this.form.controls.ProupdatedOutstandingAmount.setValidators(null);
      //this.form.controls.ProupdatedOutstandingAmount.setErrors(null);
      //this.form.controls.ProupdatedLastInstalmentNoRecovered.setValidators(null);
      //this.form.controls.ProupdatedLastInstalmentNoRecovered.setErrors(null);
      //this.form.controls.Proremarks.setValidators(null);
      //this.form.controls.Proremarks.setErrors(null);



      //this.form.controls.NTRPTransactionId.setValidators(null);
      //this.form.controls.NTRPTransactionId.setErrors(null);
      //this.form.controls.NTRPchallanAmt.setValidators(null);
      //this.form.controls.NTRPchallanAmt.setErrors(null);
      //this.form.controls.NTRPchallanDate.setValidators(null);
      //this.form.controls.NTRPchallanDate.setErrors(null);
      //this.form.controls.NTRPpaymentMode.setValidators(null);
      //this.form.controls.NTRPpaymentMode.setErrors(null);
      //this.form.controls.NTRPReceiptDetails.setValidators(null);
      //this.form.controls.NTRPReceiptDetails.setErrors(null);
      //this.form.controls.NTRPupdatedOutstandingAmount.setValidators(null);
      //this.form.controls.NTRPupdatedOutstandingAmount.setErrors(null);
      //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setValidators(null);
      //this.form.controls.NTRPupdatedLastInstalmentNoRecovered.setErrors(null);



      //// this.form.controls.challanNo.setValidators(null);
      //// this.form.controls.challanNo.setErrors(null);
      // this.form.controls.challanAmount.setValidators(null);
      // this.form.controls.challanAmount.setErrors(null);
      // this.form.controls.challanDate.setValidators(null);
      // this.form.controls.challanDate.setErrors(null);
      // this.form.controls.chequeNoDDNo.setValidators(null);
      // this.form.controls.chequeNoDDNo.setErrors(null);
      // this.form.controls.outstandingAmount.setValidators(null);
      // this.form.controls.outstandingAmount.setErrors(null);
      // this.form.controls.lastInstalmentNoRecovered.setValidators(null);
      // this.form.controls.lastInstalmentNoRecovered.setErrors(null);





    }

  }


  createForm() {
    this.form = new FormGroup({
      loanType: new FormControl('', [Validators.required]),
      createdBy: new FormControl(''),
      payLoanRefLoanCD: new FormControl('', [Validators.required]),
      amountDisbursed: new FormControl('', [Validators.required]),
      installmentAmount: new FormControl('', [Validators.required]),
      totalNoInstalments: new FormControl('', [Validators.required]),
      oddInstlNo: new FormControl('', [Validators.required]),
      oddInstlAmt: new FormControl('', [Validators.required]),
      outstandingAmount: new FormControl('', [Validators.required]),
      lastInstalmentNoRecovered: new FormControl('', [Validators.required]),


      paymentmadethrough: new FormControl(''),
      PFMSchallanNo: new FormControl('', [Validators.required]),
      PFMSchallanAmt: new FormControl('', [Validators.required]),        
      PFMSchallanDate: new FormControl({ value: this.currentDate }, [Validators.required]),
      PFMSpaymentMode: new FormControl('', [Validators.required]),
      chequeNoDDNo: new FormControl('', [Validators.required]),
      PFMSupdatedOutstandingAmount: new FormControl('', [Validators.required]),
      PFMSupdatedLastInstalmentNoRecovered: new FormControl('', [Validators.required]),


      NTRPTransactionId: new FormControl(''),
      NTRPchallanAmount: new FormControl(''),
      NTRPReceiptDetails: new FormControl(''),
      NTRPchallanAmt: new FormControl(''),
      NTRPchallanDate: new FormControl(''),
      NTRPpaymentMode: new FormControl(''),
      NTRPupdatedOutstandingAmount: new FormControl(''),
      NTRPupdatedLastInstalmentNoRecovered: new FormControl(''),

      ProchallanNo: new FormControl(''),
      ProchallanAmount: new FormControl(''),
      ProchallanDate: new FormControl(''),
      PropaymentMode: new FormControl(''),
      ProupdatedOutstandingAmount: new FormControl(''),
      ProupdatedLastInstalmentNoRecovered: new FormControl(''),     
      Proremarks: new FormControl(''),

      TENo: new FormControl(''),
      TEAmounts: new FormControl(''),
      TEDate: new FormControl(''),
      TEpaymentMode: new FormControl(''),
      TEupdatedOutstandingAmount: new FormControl(''),
      TEupdatedLastInstalmentNoRecovered: new FormControl(''),
      TEremarks: new FormControl(''),      
      flag: new FormControl(0)

    });
  }

  tempValue: number;
  onRadiobtnClick() {

  }
  onSubmit() {
    debugger;
    if (!this.form.valid) {
      return false;
    }
    else {
      debugger;
     this.tempValue= this.form.get('paymentmadethrough').value;
      this.validateMethod(this.tempValue);
      this.form.controls.flag.setValue(1);
      //this.form.controls.get('')
      this.form.get('flag').value;
      //this._Service.InsertUpateRecoveryChallan(this.form.value).subscribe(result1 => {
      //  let resultvalue = result1.split("-");
      //  if (parseInt(resultvalue[0]) >= 1) {
      //    if (this.btnUpdateText == 'Update') {
      //      this.snackBar.open('Update Successfully', null, { duration: 4000 });
      //      this.form.reset();
      //      this.formDirective.resetForm();
      //      this.btnUpdateText = 'Save';
      //    }
      //    else {
      //      this.snackBar.open('Save Successfully', null, { duration: 4000 });
      //      this.form.reset();
      //      this.formDirective.resetForm();
      //      // this.getEpsEmpJoinOffAfterLienPeriodDetails(this.empCd, this.roleId);
      //      this.btnUpdateText == 'Save';
      //    }

      //  }
      //  //else {

      //  //  if (this.btnUpdateText == 'Update') {
      //  //    if (resultvalue[2] = 'Duplicate Record') {
      //  //      this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
      //  //    }
      //  //    else {
      //  //      this.snackBar.open('Update not Successfully', null, { duration: 4000 });
      //  //    }

      //  //  }
      //  //  else if (resultvalue[2] = 'Duplicate Record') {
      //  //    this.snackBar.open('Joining Order Number Already Exits', null, { duration: 4000 });
      //  //  }
      //  //  else {
      //  //    this.snackBar.open('Save not Successfully', null, { duration: 4000 });
      //  //  }
      //  //}

      //})
    }

    //this.form.controls.ProchallanAmount.setValidators(null);
    this.form.get('ProchallanAmount').clearValidators();
    //this.form.get('ProchallanAmount').setErrors(null);
    //this.form.get('ProchallanAmount').updateValueAndValidity();
  }
  BindLoanType() {
    debugger;
    this.loantypeFlag = '4';
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this._loanType = data;
    })
  }
  GetcommanMethod(value) {
    this.getEmployeeDeatils(value, "0");
  }
  applyFilter(filterValue: string) {

  }
  getEmployeeDeatils(EmpCode: string, MsDesignID: string) {
    debugger;
    this._Service.RecoveryChallan(EmpCode, MsDesignID).subscribe(result => {
      if (result.length == 0) {
        this.RecoveryChallanData = null;
      }
      else {
        debugger;
        this.recoveryArr = result;
        console.log(this.recoveryArr);
        this.RecoveryChallanData = new MatTableDataSource(result);
        this.RecoveryChallanData.paginator = this.paginator;
        this.RecoveryChallanData.sort = this.sort;

      }
    })
  }
  charaterOnlyNoSpace(event): boolean {

    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
 
  ViewRevocationDetails() {
    debugger;
    this.panelOpenState = true;
    console.log(this.recoveryArr);
    this.form.patchValue(this.recoveryArr[0]);
  }
  ResetForm() {
    this.panelOpenState = false;
   // this.form.reset();
    this.formDirective.resetForm();
  }

}
