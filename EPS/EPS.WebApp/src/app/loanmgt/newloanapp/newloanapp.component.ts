import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs'
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { NewLoanModel, employee, IfscCode } from '../../model/LoanModel/new-loan-model';
import swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { MasterService } from '../../services/master/master.service';
import { EmpdetailsService } from '../../services/empdetails/empdetails.service';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { PropOwnerDetlsModel } from '../../model/LoanModel/propertyownerdetails';
import { CommonMsg } from '../../global/common-msg';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-newloanapp',
  templateUrl: './newloanapp.component.html',
  styleUrls: ['./newloanapp.component.css'],
  providers: [CommonMsg]
})
export class NewloanappComponent implements OnInit {
  data: any;
  eventsSubject: Subject<void> = new Subject<void>();
  public ifscCtrl: FormControl = new FormControl();
  public ifscFilterCtrl: FormControl = new FormControl();

  Ifsc: any[];
  BankesInfo: any[];
  deletePopup: boolean;
  fileToupload: File = null;
  imageUrl: string = "/assets/uploadfile/";
  PurchageFlag: boolean = false;
  EnlargingFlag: boolean = false;
  ConstructionFlag: boolean = false;
  ReadybuiltFlag: boolean = false;
  CompAdvFlag: boolean = false;
  disableflag: boolean = false;
  disableSaveFlage: boolean = true;
  disableCancelFlage: boolean = true;
  disableFwdCheckerFlag: boolean = false;
  disableFwdMakerFlag: boolean = false;
  isSubmitted = false;
  dtls_Enlarg_1: boolean;
  public dtls_Enlarg: any;
  public mode: number;
  asyncResult: any;
  group: boolean;
  Message: number = null;
  disbleflag = false;
  disbleRemarksflag = false;
  disbleflagdownload = false;
  disbledwnloadflag = false;
  public callTypevar: number = 2;
  MsDesignID: string;
  EmpCode: string;
  public purposecode: string;
  ArrddlLoanType: any;
  public ArrEmpCode: employee;
  ArrddlLoanStatus: any;
  public _loandetails: NewLoanModel;
  public _propdetlsmodels: PropOwnerDetlsModel;
  ArrLoanPurpose: any;
  Loantypeandpurposes: any;
  TempLoanCode: any;
  public designid: string;
  public empid: string;
  buttonText: string;
  dataSource: any;
  public LoanCode: string;
  public PurposeCode: string;
  public ddoid: number;
  public tempId: number;
  public param: any;
  @ViewChild('NewLaon') form: any;
  submitted = false;
  public temp: number;
  remarks: string = '';
  UserID: number;
  userRole: string;
  userRoleID: number;
  tempLoanAmtSanc: number;
  temppriInstAmt: number;
  temploanAmtSanc: number;
  tempcost: number;
  tempamt_Act_Pay: number;
  msg: string;
  Ifscfilter: any[];
  tempamtActPay: number;
  tempNum_Inst: number;
  isTableHasData: boolean;
  public coolingPeriod: boolean = false;
  myDate = new Date();
  labelmsg: string = 'Select Purpose Type';
  constructor(private master: MasterService, private empDetails: EmpdetailsService, private _Service: NewLoanService,
    private _msg: CommonMsg, private snackBar: MatSnackBar, private objBankService: BankDetailsService, private objCommanService: CommanService) {
    this._loandetails = new NewLoanModel();
    this._propdetlsmodels = new PropOwnerDetlsModel();
  }
  displayedColumns: string[] = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedIndex = 0;
  Bill: any[];
  Design: any[];
  EMP: any[];
  setDeletIDOnPopup: any;
  empVfyCode1: string[];
  checkautofill: string;
  public username: string;
  public empstatus: Boolean = false;
  public flag: Boolean = false;
  objBankDetails: any = {};
  objAllIFSCCode: any = {};
  private _onDestroy = new Subject<void>();
  public filteredIfsc: Subject<IfscCode[]> = new Subject<IfscCode[]>();
  public loantypeFlag: string;
  ngOnInit() {
    this.buttonText = 'Save';
    this.empstatus = false;
    this.flag = false;
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.BindLoanStatus(this.UserID);
    this.group = false;
    this.BindLoanType();
    if (this.userRoleID == 17) {
      this.FindEmpCode(this.username);
    }
    else if (this.userRoleID == 9) {
      this.FindEmpCode(this.username);
    }
    this.getAllIFSC();
    this.ifscFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterIfsc();
      });
  }
  private filterIfsc() {
    if (!this.Ifsc) {
      return;
    }
    let search = this.ifscFilterCtrl.value;
    if (!search) {
      this.filteredIfsc.next(this.Ifsc.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredIfsc.next(

      this.Ifsc.filter(Ifsc => Ifsc.ifscCD.toLowerCase().indexOf(search) > -1)
    );
  }
  onValidate() {
    this.tempcost = Number(this._loandetails.adv_Amount);
    this.tempamt_Act_Pay = Number(this._loandetails.loanAmtSanc);
    if (Math.floor(this.tempamt_Act_Pay) < Math.floor(this.tempcost)) {

      swal(this._msg.amountLessEqualloanAmountMsg);
      this._loandetails.adv_Amount = null;
    }
  }
  onAmtActuallyPaidValidate() {
    this.tempcost = Number(this._loandetails.cost);
    this.tempamt_Act_Pay = Number(this._loandetails.amt_Act_Pay);
    if (Math.floor(this.tempcost) < Math.floor(this.tempamt_Act_Pay)) {
      swal(this._msg.amountActuallyPaidMsg);
      this._loandetails.amt_Act_Pay = null;
    }
  }

  GetcommanMethod(value) {

    this.getLoanDetails(value);


  }
  BindLoanType() {

    this.loantypeFlag = '2';//new loan type dropdownlist
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this.ArrddlLoanType = data;

    })
  }
  FindEmpCode(username) {
    debugger;
    this.objCommanService.GetEmpCode(username).subscribe(data => {
      this.ArrEmpCode = data[0];
      // uncomment to validate for the loan requet before 5 year 
      //if (this.ArrEmpCode.serviceTime < '5') {
      // swal("At least 5Yrs. continuous in govt service for apply loan");    
      //}
      //if (this.ArrEmpCode.serviceType != 'R') {
      //  swal("only Permanent employee apply for the loan");
      //}   
      if (this.ArrEmpCode.empcode != null) {
        this.getLoanDetails(this.ArrEmpCode.empcode);
      }
    });
  }
  validatechange() {
    this.tempamtActPay = Number(this._loandetails.amt_Act_Pay);
    this.tempcost = Number(this._loandetails.cost);
    if (Math.floor(this.tempcost) < this.tempamtActPay) {
      swal(this._msg.costGreateThenAmountActuallyPaid);
    }
  }
  validatechangeInstallmentsRepayment() {
    this.tempNum_Inst = Number(this._loandetails.num_Inst);
    if (Math.floor(this.tempNum_Inst) >= 151) {
      swal(this._msg.noInstallmentsRepaymentAmountshouldbelessthanorequal150);
      this._loandetails.num_Inst = null;
    }
  }


  valuechange(value) {
    this.temppriInstAmt = Number(this._loandetails.priInstAmt);
    this.temploanAmtSanc = Number(this._loandetails.loanAmtSanc);
    this._loandetails.intTotInst = Math.floor(((this._loandetails.loanAmtSanc) / value));
    this.temp = Math.floor((this._loandetails.priInstAmt) * this._loandetails.intTotInst)
    this._loandetails.oddInstAmtInt = Math.floor(((this._loandetails.loanAmtSanc) - this.temp));
    if (Math.floor(this.temppriInstAmt) == 0) {
      swal(this._msg.enterCorrectValueMsg);
      this._loandetails.priInstAmt = null;
    }
    if (this._loandetails.oddInstAmtInt == 0) {
      this._loandetails.oddInstNoInt = 0;
    }
    else {
      this._loandetails.oddInstNoInt = 1;
    }

    if (Math.floor(this.temploanAmtSanc) < Math.floor(this.temppriInstAmt)) {

      swal(this._msg.loanAmountGreaterthenInstallMsg);
      this._loandetails.priInstAmt = null;

    }
  }
  loanamt: number;
  checkvaluecLoanAmout(value) {

    this.tempLoanAmtSanc = Number(value);
    this.loanamt = Math.floor(Number(this._loandetails.pay_basic))
    if (Math.floor(this.loanamt * 34) >= 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
      swal(this._msg.maximumLimitLoanAmountMsg);
    }
    if (this.tempLoanAmtSanc >= 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
      swal(this._msg.maximumLimitLoanAmountMsg);
    }

    if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
      swal(this._msg.maxLimitLoanAmountMsg);
      this._loandetails.loanAmtSanc = null;
    }


    if (this._loandetails.payLoanRefLoanCD == 100 && value > 50000) {
      swal(this._msg.maxLimitLoanAmountMsg);
      this._loandetails.loanAmtSanc = null;
    }
    if (Math.floor(this._loandetails.loanAmtSanc) > 2500000 && this._loandetails.payLoanRefLoanCD == 21) {
      swal(this._msg.maximumLimitLoanAmountMsg);
      this._loandetails.loanAmtSanc = null;
    }

    if (Number(this._Service.PurposeCode) == 3 && Math.floor(this._loandetails.loanAmtSanc) > 180000) {
      swal(this._msg.maxLimitLoanAmount1LK80Msg);
      this._loandetails.loanAmtSanc = null;
    }
    if (Number(this._Service.PurposeCode) == 2 && Math.floor(this._loandetails.loanAmtSanc) > 250000) {
      swal(this._msg.maxLimitLoanAmount2LK50Msg);
      this._loandetails.loanAmtSanc = null;
    }
    if (Math.floor(this.tempLoanAmtSanc) == 0) {
      swal(this._msg.enterCorrectValueMsg);
      this._loandetails.loanAmtSanc = null;
    }
  }
  getLoanDetails(value) {
    
    this.empid = value;
    this.designid = "0";
    this.getEmployeeDeatils(this.empid, this.designid);

  }
  getEmployeeDeatils(EmpCode: string, MsDesignID: string) {
    
    this._Service.EmployeeInfo(EmpCode, MsDesignID).subscribe(data => {
      this._loandetails = data[0];
      this._loandetails.payLoanRefLoanCD = '';
      this._loandetails.payLoanPurposeCode = '';
      if (this._loandetails == undefined) {
        swal(this._msg.noRecordMsg);
        this._loandetails = new NewLoanModel();
      }
      else {

        this.PurposeCode = String(this._loandetails.payLoanPurposeID);
        this.LoanCode = String(this._loandetails.payLoanRefLoanCD);
        this.getLoanpurposeSelectedPayLoanCode(this._loandetails.payLoanRefLoanCD);
      }
    });
  }
  getLoantypeandpurposestatus(LoanBillID: string) {
    this._Service.Loantypeandpurposestatus(LoanBillID).subscribe(data => {
      this.Loantypeandpurposes = data;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
      if (this.dataSource.filteredData.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
    }
  }
  btnsubmit() {
    
    var pop_Decl_1 = this._loandetails.pop_Decl_1
    var pop_Decl_2 = this._loandetails.pop_Decl_2
    var pop_Decl_3 = this._loandetails.pop_Decl_3
    var const_Dec_1 = this._loandetails.const_Dec_1
    var const_Dec_2 = this._loandetails.const_Dec_2
    var const_Dec_3 = this._loandetails.const_Dec_3
    var dtls_Enlarg_1 = this._loandetails.dtls_Enlarg_1
    var dtls_Enlarg_2 = this._loandetails.dtls_Enlarg_2
    var dtls_Enlarg_3 = this._loandetails.dtls_Enlarg_3
    var dtls_Built_Flat_1 = this._loandetails.dtls_Built_Flat_1
    var dtls_Built_Flat_2 = this._loandetails.dtls_Built_flat_2
    var dtls_Built_Flat_3 = this._loandetails.dtls_Built_flat_3
    var comp_Certi_Info_1 = this._loandetails.comp_Certi_Info_1
    var comp_Certi_Info_2 = this._loandetails.comp_Certi_Info_2
    this._loandetails.createdBy = this.username;
    this._loandetails.bankName = this.objBankDetails.bankName;
    this._loandetails.branchName = this.objBankDetails.branchName;
    this._loandetails.adv_Draw_Date = new Date();
    if (this.userRoleID == 9) {
      this._loandetails.priVerifFlag = 33;
    }
    else {
      this._loandetails.priVerifFlag = 8;
    }
    if (this.LoanCode == '100' && this._loandetails.upload_Sign != "0") {
      this._loandetails.upload_Sign = this.fileToupload.name;
    }
    if (pop_Decl_1 == true && pop_Decl_2 == true && pop_Decl_3 == true || const_Dec_1 == true && const_Dec_2 == true && const_Dec_3 == true ||
      dtls_Enlarg_1 == true && dtls_Enlarg_2 == true && dtls_Enlarg_3 == true || dtls_Built_Flat_1 == true && dtls_Built_Flat_2 == true && dtls_Built_Flat_3 == true
      || comp_Certi_Info_1 == true && comp_Certi_Info_2) {
      if (this.buttonText == 'Save') {        
        this.ValidateCoolingPeriod(this._loandetails.payLoanRefLoanCD, this._loandetails.payLoanPurposeCode, this._loandetails.empCd);
        if (this.coolingPeriod == true) {
          this._Service.EmployeeSaveInfo(this._loandetails).subscribe(data => {
            this._loandetails = data[0];
            this._Service.HiddenId = data[0];
            this.uploadfile();
            if (data == "-1") {
              this.BindLoanStatus(this._Service.DdoId);
              // this.snackBar.open(this._msg.alreadyExistMsg, null, { duration: 5000 });
              swal(this._msg.alreadyExistMsg);
              this.commanfunction();
              this.form.resetForm();
            }
            else {
              this.BindLoanStatus(this._Service.DdoId);
              this.snackBar.open(this._msg.saveMsg, null, { duration: 5000 });
              this.commanfunction();
              this.form.resetForm();
              this.disableSaveFlage = false;
              this.disableFwdCheckerFlag = false;
              this.form.resetForm();
            }

          });
        }
        else {
          alert(this._msg.coolingPeriodMsg);
        }
      }
    }
    else {

      swal(this._msg.checkDeclarationMsg);
    }

    if (pop_Decl_1 == true && pop_Decl_2 == true && pop_Decl_3 == true || const_Dec_1 == true && const_Dec_2 == true && const_Dec_3 == true ||
      dtls_Enlarg_1 == true && dtls_Enlarg_2 == true && dtls_Enlarg_3 == true || dtls_Built_Flat_1 == true && dtls_Built_Flat_2 == true && dtls_Built_Flat_3 == true
      || comp_Certi_Info_1 == true && comp_Certi_Info_2) {
      if (this.buttonText == 'Update') {
        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;
          this._loandetails.priVerifFlag = 33;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
          this._loandetails.priVerifFlag = 8;
        }
        this._loandetails.msEmpId = this.tempId;
        this._Service.EmployeeUpdateInfo(this._loandetails).subscribe(data => {
          this._loandetails = data[0];
          this.uploadfile();
          this.snackBar.open(this._msg.updateMsg, null, { duration: 5000 });
          this.form.resetForm();
          this.disableSaveFlage = true;
          this.disableFwdCheckerFlag = false;
          this.BindLoanStatus(this._Service.DdoId);
          this.commanfunction();
        });

      }
    }

    else {
      this.snackBar.open(this._msg.checkDeclarationMsg, null, { duration: 5000 });
    }
  }
  async uploadfile() {
    let formData = new FormData();
    formData.append(this.fileToupload.name, this.fileToupload);
    this.asyncResult = await this.master.uploadFile(formData).subscribe(async res => {
    });
  }
  LoanDdetailsForm() {
    this.objBankDetails.bankName = '';
    this.objBankDetails.branchName = '';
    this._loandetails.loanAmtSanc = null;
    this._loandetails.priInstAmt = null;
    this._loandetails.intTotInst = null;
    this._loandetails.oddInstNoInt = null;
    this._loandetails.oddInstAmtInt = null;
    this._loandetails.ownerName = '';
    this._loandetails.descriptions = '';
    this._loandetails.ownerPanNo = '';
    this._loandetails.bankIFSCCODE = '';
    this.objBankDetails.bankName = '';
    this.objBankDetails.branchName = '';
    this._loandetails.bankAccountNo = '';
    this.PurposeCode = undefined;
    this._loandetails.address = '';
    this._loandetails.area_Sq_Ft = null;
    this._loandetails.cost = null;
    this._loandetails.amt_Act_Pay = null;
    this._loandetails.purp_Acquire = null;
    this._loandetails.unexp_Port_Lease = '';
    this._loandetails.flr_Area = null;
    this._loandetails.amt_Adv_Req = null;
    this._loandetails.num_Inst = null;
    this._loandetails.address = null;
    this._loandetails.area_Sq_Ft = null;
    this._loandetails.cost = null;
    this._loandetails.amt_Act_Pay = null;
    this._loandetails.purp_Acquire = null;
    this._loandetails.unexp_Port_Lease = null;
    this._loandetails.address = '';
    this._loandetails.plint_Area = null;
    this._loandetails.plth_Prop_Enlarge = null;
    this._loandetails.coc = null;
    this._loandetails.cop_Enlarge = null;
    this._loandetails.tot_Plint_Area = null;
    this._loandetails.tot_Cost = null;
    this._loandetails.address = null;
    this._loandetails.plint_Area = null;
    this._loandetails.prc_Settled = null;
    this._loandetails.amt_Paid = null;
    this._loandetails.anti_Price_Pc = null;
    this._loandetails.desNoInsRePaidForAdv = null;
    this._loandetails.adv_Amount = null;
    this._loandetails.adv_Draw_Date = null;
    this._loandetails.const_Date = null;
    this._loandetails.payLoanRefLoanCD = 0;
    this._loandetails.payLoanPurposeCode = 0;
  }
  getLoanpurposeSelectedPayLoanCode(value) {    
    debugger;
    //using this table name [ODS].[tblMsPayLoanRef] 
    if (value == "21") {
      this.labelmsg = 'Select Purpose Type';
    }
    if (value == "100") {
      this.labelmsg = 'Select No. of Computer Loan Sanctioned';
    }
    
    this._Service.LoanCode = value;
    this.TempLoanCode = value;
    this.LoanCode = this.TempLoanCode;   
    this._Service.EMPLoanPurposeByLoanCode(value, this.ArrEmpCode.empcode).subscribe(data => {
      this.ArrLoanPurpose = data;


    });
  }
  getvalues(value) {
    this._Service.PurposeCode = value;
    this.LoanCode = this.TempLoanCode;
    this.PurposeCode = value;
  }
  BindLoanStatus(value) {
    debugger;
    this.mode = 1;//new loan select data in the table
    this._Service.DdoId = value;
    this._Service.BindLoanStatus(this.mode, value).subscribe(data => {
      this.ArrddlLoanStatus = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      }
      else {
        this.isTableHasData = false;
      }
    });
  }
  ViewEditLoanDetailsById(id: any, flag: any) {
    
    this._Service.EditLoanDetailsByEMPID(id).subscribe(data => {
      this._loandetails = data[0];
      this.tempId = id;
      if (flag == 0) {
        this.buttonText = "Update";

      }
      if (this._loandetails.priVerifFlag == 31) {
        this.disableSaveFlage = true;
        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;

        }
        else {
          this.disableFwdMakerFlag = false;
        }
      }
      if (this._loandetails.priVerifFlag == 33) {
        this.disableSaveFlage = true;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._loandetails.priVerifFlag == 22) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._loandetails.priVerifFlag == 8) {
        this.disableSaveFlage = true;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._loandetails.priVerifFlag == 24) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._loandetails.priVerifFlag == 25) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      this.getLoanpurposeSelectedPayLoanCode(this._loandetails.payLoanRefLoanCD);
      this.getvalues(this._loandetails.payLoanPurposeCode);
      if (this._Service.PurposeCode == '4') {
        this.group = true;
      }
      if (this._Service.PurposeCode == '1') { this.group = true; }
      if (this._Service.PurposeCode == '2') { this.group = true; }
      if (this._Service.PurposeCode == '3') { this.group = true; }
      if (this._Service.LoanCode == '100') {
        if (this._loandetails.comp_Certi_Info_1 == true) { this.group = true; }
      }
      if (flag === 1) {
        this.disbleflag = true;
        this.disbledwnloadflag = false;
        this.disableSaveFlage = false;
        this.disableCancelFlage = false;

        if (this.userRoleID == 9) {
          this.disableFwdCheckerFlag = false;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = false;
        }
      }
      else {
        this.disbledwnloadflag = true;
        this.disbleflag = false;
        this.disableCancelFlage = false;
      }

      if (this._loandetails.bankIFSCCODE != "" && this._loandetails.bankIFSCCODE != undefined) {

        this.objBankService.getBankDetailsByIFSC(this._loandetails.bankIFSCCODE).subscribe(res => {
          this.objBankDetails = res;
        })
      }
      else {

        this.objBankDetails.branchName = '';
        this.objBankDetails.bankName = '';
        this._loandetails.bankIFSCCODE = '';
      }
    })
  }
  commanfunction() {
    this.LoanDdetailsForm();
    if (this._Service.PurposeCode == '4') {
      this.ReadybuiltFlag = false;
    }
    if (this._Service.PurposeCode == '1') {

      this.PurchageFlag = false;
    }
    if (this._Service.PurposeCode == '2') {
      this.ConstructionFlag = false;
    }
    if (this._Service.PurposeCode == '3') {
      this.EnlargingFlag = false;
    }
    if (this._Service.LoanCode == '100') {
      this.CompAdvFlag = false;
      if (this._loandetails.comp_Certi_Info_1 == true) {
        this.group = true;
      }
    }
  }


  forwarHooMaker() {
    
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._loandetails.priVerifFlag = 24;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        //this.snackBar.open(this._msg.forwardHooMakerMsg, null, { duration: 5000 });
        swal(this._msg.forwardHooMakerMsg);
        this.disableSaveFlage = false;
        this.disableFwdMakerFlag = false;
        this.BindLoanStatus(this._Service.DdoId);
        this.commanfunction();
      });
    }
  }
  forwarHooChecker() {

    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._loandetails.priVerifFlag = 22;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._loandetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        // this.snackBar.open(this._msg.forwardHooCheckerMsg, null, { duration: 5000 });
        swal(this._msg.forwardHooCheckerMsg);
        this.disableSaveFlage = false;
        this.disableFwdCheckerFlag = false;
        this.BindLoanStatus(this._Service.DdoId);
        this.commanfunction();
      });
    }
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  deleteEmployeeById(id: any) {
    this.mode = 1;
    this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(data => {
      this.Message = data;
      if (this.Message != null) {
        this.deletePopup = false;
        swal(this._msg.deleteMsg)
        this.BindLoanStatus(this._Service.DdoId);
        this.LoanDdetailsForm();

      }
    });
  }
  handleFileinput(file: FileList) {

    this.fileToupload = file.item(0);
    if (file.length != 0 && this.LoanCode == '100') {
      if (file[0].type != this._msg.apppdffileMsg) {
        this.msg = this._msg.pdffileMsg;
        return false;
      }
    }

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToupload);
  }
  urls: any = [];
  onSelectFile(event) {
    debugger
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event) => {
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  DownloadFile(url) {
    this.empDetails.getdownloadDetails(url).subscribe(res => {
      saveAs(new Blob([res], { type: this._msg.formatfileMsg }), "");
    }
    )
  }

  getAllIFSC() {
    this.objBankService.getAllIFSC().subscribe(res => {
      this.objAllIFSCCode = res;
      this.Ifsc = res;
      this.ifscCtrl.setValue(this.Ifsc);
      this.filteredIfsc.next(this.Ifsc);
    });
  }

  getBankDetailsByIFSC(bankIFSCCODE: string) {
    this.objBankService.getBankDetailsByIFSC(bankIFSCCODE).subscribe(res => {
      this.objBankDetails.bankId = '';
      this.objBankDetails.branchId = '';
      this.objBankDetails = res;
    });
  }
  resetLoanDdetailsForm() {
    this.LoanDdetailsForm();
    this.eventsSubject.next(this.data);
  }


  ValidateCoolingPeriod(LoanCode: number, PurposeCode: number, EmpCd: string) {
    this._Service.ValidateCoolingPeriod(LoanCode, PurposeCode, EmpCd).subscribe(res => {
      this.coolingPeriod = res;

    });

  }

}

