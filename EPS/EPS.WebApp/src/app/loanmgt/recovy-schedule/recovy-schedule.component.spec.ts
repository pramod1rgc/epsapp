import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecovyScheduleComponent } from './recovy-schedule.component';

describe('RecovyScheduleComponent', () => {
  let component: RecovyScheduleComponent;
  let fixture: ComponentFixture<RecovyScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecovyScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecovyScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
