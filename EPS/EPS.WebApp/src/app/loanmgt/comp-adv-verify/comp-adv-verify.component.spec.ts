import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompAdvVerifyComponent } from './comp-adv-verify.component';

describe('CompAdvVerifyComponent', () => {
  let component: CompAdvVerifyComponent;
  let fixture: ComponentFixture<CompAdvVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompAdvVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompAdvVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
