import { Component, OnInit, ViewChild } from '@angular/core';
//import { MatSnackBar } from '@angular/material';
import { ExistloanModel } from '../../model/LoanModel/existloanModel';
import { InstRecoveryService } from '../../services/loan-mgmt/inst-recovery-service';
import { MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { EmpSanction, EmpSancModel, InstEmpModel } from '../../model/LoanModel/EmpSanctionModel';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';



@Component({
  selector: 'app-inst-recovry',
  templateUrl: './inst-recovry.component.html',
  styleUrls: ['./inst-recovry.component.css'],
  providers: [CommonMsg]
})



export class InstRecovryComponent implements OnInit {
  btnname: string;
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
  public username: string;
  dataSource: any;
  ArrLoanSection: any;


  public callTypevar: number = 1;
  RecoveryDetails: ExistloanModel[] = [];
  SanctionDetails: EmpSanction = new EmpSanction();
  EmpSanc: EmpSancModel;
  EmpCD: string;
  InstEmpDetais: InstEmpModel = new InstEmpModel();

  IsDisabled = false;
  FlagSave: boolean = false;
  FlagCancel: boolean = false;
  FlagForwordChecker: boolean = false;
  FlageAccept: boolean = false;
  FlagReject: boolean = false;
  FlagUpdate: boolean = false;
  deletepopup: any;
  setDeletIDOnPopup: any;
  setDeletIDOnPopup2: any;
  constructor(private _Service: InstRecoveryService, private commonMsg: CommonMsg) {
    //this.RecoveryDetails = new ExistloanModel();
    //this.SanctionDetails = new EmpSanction();
  }
  displayedColumns: string[] = ['loanType', 'sancOrdNo', 'billMonth', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('InstRecover') InstRecoverForm: any;

  ngOnInit() {
    this.btnname = 'Save';
    this.EmpSanc = new EmpSancModel();
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.deletepopup = false;
  }

  GetcommanMethod(value) {
    this.GetRecoveryEmpDetails(value);
  }

  GetRecoveryEmpDetails(value: any) {
 
    this.FlagSave = false;
    this.FlageAccept = false;
    this.FlagReject = false;
    this.FlagForwordChecker = false;
    this.FlagCancel = false;
    this.dataSource = null;
    //this.cancel();
    this.InstRecoverForm.resetForm();
    this.RecoveryDetails = [];
    
    sessionStorage.setItem("EmpCd", value);
    this._Service.GetRecoveryEmpDetails(value).subscribe(data => {
      this.RecoveryDetails = data;
     // console.log(this.RecoveryDetails);
      if (data != null || data != undefined) {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      else {
        this.dataSource = null;
      }
      })
  }


  BindSanctionDetails(value: any) {   
   // this.cancel();
    this.InstRecoverForm.resetForm();
    this.EmpCD = sessionStorage.getItem("EmpCd");
    this._Service.BindSanctionDetails(value, this.EmpCD).subscribe(data => {
      if (data[0] == null) {
        this.btnname = 'Save'
        this.FlagSave = true;
        this.FlagCancel = true;
      }
      else {
        this.SanctionDetails = data[0];
        var empStatus = data[0].statusId;
      }
     // console.log(this.SanctionDetails);
      if (this.userRoleID == 5) {
        this.GetInstEmpDetails(this.EmpCD)
        if (empStatus == 61) {
          this.FlagSave = false;
          this.IsDisabled = true;
          this.FlageAccept = true;
          this.FlagReject = true;
          this.FlagForwordChecker = false;
        }
        if (empStatus == 62) {
          this.FlagSave = false;
          this.FlagCancel = false;
          this.IsDisabled = false;
          this.FlagForwordChecker = false;
        }
        if (empStatus == 64) {
          this.FlagSave = false;
          this.IsDisabled = true;
          this.FlageAccept = true;
          this.FlagReject = true;
          this.FlagForwordChecker = false;
        }
      }
      else {
        this.GetInstEmpDetails(this.EmpCD)
        if (empStatus == 61) {
          //this.FlagSave = true;
          //this.FlagCancel = true;
          this.IsDisabled = true;
          this.FlagForwordChecker = false;
        }

        if (empStatus == 64) {
          this.FlagSave = true;
          this.FlagCancel = false;
          // this.FlagUpdate = true;
          this.FlagForwordChecker = true;
          this.btnname = 'Update';
        }
      }
     
    })
  }

  GetInstEmpDetails(EmpCD: any) {
    this._Service.GetInstEmpDetails(EmpCD).subscribe(data => {
      this.InstEmpDetais = data[0];
     // console.log(this.InstEmpDetais);
    })
  }

  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  Save(InstEmpDetais) {
  
    this.EmpCD = sessionStorage.getItem("EmpCd");
    InstEmpDetais.EmpCD = this.EmpCD;
    InstEmpDetais.mode = 1;
    var buttonName = document.activeElement.getAttribute("Name");
    if (this.btnname == 'Save') {
      this._Service.SaveData(this.InstEmpDetais).subscribe(data => {
        if (data == "1") {
          swal(this.commonMsg.saveMsg);
          this.GetRecoveryEmpDetails(this.EmpCD);
          this.InstRecoverForm.resetForm();
        }
      });
    }
    else if (this.btnname == 'Update')
    {
   
      this._Service.EditLoanDetailsById(this.InstEmpDetais).subscribe(data => {
        if (data == "1") {
          swal(this.commonMsg.updateMsg);
        }
      });
    }
    else {
      if (buttonName == 'ForwardtoDDOChecker') {
        this.ForwardtoDDOChecker(InstEmpDetais);
      }
    }
  }

  ForwardtoDDOChecker(InstEmpDetais) {

    this.EmpCD = sessionStorage.getItem("EmpCd");
    InstEmpDetais.EmpCD = this.EmpCD;
    InstEmpDetais.mode = 2;
    this._Service.ForwardtoDDOChecker(this.InstEmpDetais).subscribe(data => {
      if (data == "1") {
        swal(this.commonMsg.forwardCheckerMsg);
      }
     // console.log(this.SanctionDetails);
    })
  }


  Accept() {
 
    this.EmpCD = sessionStorage.getItem("EmpCd");
  //  EmpSanc.EmpCD = this.EmpCD;
    //EmpSanc.mode = 1;
    this._Service.Accept(this.EmpCD).subscribe(data => {
      if (data == "1") {
        swal(this.commonMsg.VerifyedByChecker);
      }     
    })
  }

  Reject() {
    this.EmpCD = sessionStorage.getItem("EmpCd");
   // EmpSanc.EmpCD = this.EmpCD;
    //EmpSanc.mode = 2;
    this._Service.Reject(this.EmpCD).subscribe(data => {
      if (data == "1") {
        swal(this.commonMsg.RejectedByChecker);
      }
    })
  }

  EditLoanDetailsById(msEmpLoanMultiInstId,EmpCd ) {

   // this.FlagUpdate = true;
    this.btnname = 'Update';
    this.FlagSave = true;
    sessionStorage.setItem('msEmpLoanMultiInstId', msEmpLoanMultiInstId);
    sessionStorage.setItem('EmpCd', EmpCd);
    this.GetInstEmpDetails(EmpCd);
  }
  Update(InstEmpDetais:any) {
    
    this.FlagForwordChecker = true;
    InstEmpDetais.MsEmpLoanMultiInstId = sessionStorage.getItem('msEmpLoanMultiInstId');
    InstEmpDetais.EmpCD  = sessionStorage.getItem('EmpCd');

    this._Service.EditLoanDetailsById(this.InstEmpDetais).subscribe(data => {
      if (data == "1") {
        swal(this.commonMsg.updateMsg);
      }
    })
  }


  DeleteLoanDetailsById(msEmpLoanMultiInstId, empCD) {
    this._Service.DeleteLoanDetailsById(msEmpLoanMultiInstId, empCD).subscribe(data => {
      if (data == "1") {
        swal(this.commonMsg.deleteMsg);
      }
    })
  }

  //cancel() {
  //  this.InstRecoverForm.resetForm();
  //}


  setDeleteId(loanId,empCd) {
    this.setDeletIDOnPopup = loanId;
    this.setDeletIDOnPopup2 = empCd;
  }
}


