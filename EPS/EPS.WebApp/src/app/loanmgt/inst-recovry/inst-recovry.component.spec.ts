import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstRecovryComponent } from './inst-recovry.component';

describe('InstRecovryComponent', () => {
  let component: InstRecovryComponent;
  let fixture: ComponentFixture<InstRecovryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstRecovryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstRecovryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
