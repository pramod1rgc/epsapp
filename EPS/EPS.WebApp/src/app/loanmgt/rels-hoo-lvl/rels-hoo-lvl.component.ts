import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { Router } from '@angular/router';
import { Sanction } from '../../model/LoanModel/sanction.model';
import swal from 'sweetalert2';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { SanctionService } from '../../services/loan-mgmt/sanction.service';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-rels-hoo-lvl',
  templateUrl: './rels-hoo-lvl.component.html',
  styleUrls: ['./rels-hoo-lvl.component.css'],
  providers: [CommonMsg]
})
export class RelsHooLvlComponent implements OnInit {
  maxDate = new Date();
  public DescripStatus: any;
  ArrSanction: any;
  deletePopup: boolean;
  setDeletIDOnPopup: any;
  dataSource: any;
  buttonText: string = 'Save';
  public mode: number;
  ArrLoanType: any;
  EmpTypeFlag: boolean;
  @ViewChild('Release') ReleaseForm: any;
  public _releasedetails: Sanction;
  objBankDetails: any = {};
  displayedColumns: string[] = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedIndex = 0;
  username: string;
  ArrEmpCode: any;
  empstatus: boolean;
  btndisable: boolean = false;
  disbleflag: boolean;
  show = false;
  type = "password";
  @ContentChild('showhideinput') input;
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
  deletepopup: any;
  showExtraClass: true;
  disableFwdCheckerFlag: boolean;
  flag: boolean;
  remarks: string;
  tempId: number;
  disableSaveFlage: boolean;
  disableFwdMakerFlag: boolean;
  disableVerifyCheckerFlag: boolean = false;
  disableRejectCheckerFlag: boolean = false;
  loantypeFlag: string;

  constructor(private _msg: CommonMsg, private _Service: NewLoanService, private objSanctionService: SanctionService,
    private objCommanService: CommanService,  private objBankService: BankDetailsService) {
    this._releasedetails = new Sanction();   
  }
  ngOnInit() {
   
    this.resetReleaseForm();
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.BindLoanType();
    this.BindGridLoanSanction(this.UserID);
  }
  GetcommanMethod(value) {
    this.getLoanDetails(value);
  }
  BindLoanType() {
    this.loantypeFlag = '2';
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this.ArrLoanType = data;

    })
  }
  getLoanDetails(value) {
     
    this.getEmployeeDeatils(value, '0');
  }
  getEmployeeDeatils(EmpCode: string, MsDesignID: string) {
    this.mode = 1;
    this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(data => {
       
      this._releasedetails = data[0];
      if (this._releasedetails != undefined) {
        this.getBankDetailsByIFSC(this._releasedetails.ifscCD);
      }
      if (this._releasedetails == undefined) {
        this._releasedetails = new Sanction();
      }
      if (this._releasedetails.empApptType == 'R') {
        this.EmpTypeFlag = true;
      }
      else {
        this.EmpTypeFlag = false;
      }
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  resetReleaseForm() {
    this.ReleaseForm.resetForm();
    this.objBankDetails.branchName = '';
    this._releasedetails.bnkAcNo = '';
    this.objBankDetails.branchName = '';
    this._releasedetails.ConfirmbnkAcNo = '';
  }
  btnsubmit() {
     
    if (this.buttonText == 'Save') {
      if (this.userRoleID == 6) {
        this._releasedetails.priVerifFlag = 55;
      }
      else {
        this._releasedetails.priVerifFlag = 8;
      }
      this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(data => {
        this._releasedetails = data;
        this._Service.HiddenId = data;
        if (data == "-1") {
          this._Service.DdoId = this.UserID;
          this.BindGridLoanSanction(this._Service.DdoId);
          swal(this._msg.alreadyExistMsg);
        }
        else {
          this._Service.DdoId = this.UserID;
          this.BindGridLoanSanction(this._Service.DdoId);
          swal(this._msg.saveMsg);
          this.ReleaseForm.resetForm();
        }
      });
    }
    if (this.buttonText == 'Update') {

      if (this.userRoleID == 6) {
        this.disableFwdCheckerFlag = true;
        this._releasedetails.priVerifFlag = 55;
      }
      else {

        this.disableFwdCheckerFlag = false;
        this.disableFwdMakerFlag = true;
        this._releasedetails.priVerifFlag = 8;
      }
      this._releasedetails.msEmpId = this.tempId;
      this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(data => {
        swal(this._msg.updateMsg);
        this._Service.DdoId = this.UserID;
        this.BindGridLoanSanction(this._Service.DdoId);
        this.ReleaseForm.resetForm();
      });
    }
  }
  resetLoanDdetailsForm() {
    this.ReleaseForm.resetForm();
  }
  BindGridLoanSanction(value) {     
    this.mode = 3;//loan sanction and release details(HOO Label) ,select data in the table
    this.UserID = value;
    this._Service.BindLoanStatus(this.mode, this.UserID).subscribe(data => {
      this.ArrSanction = data;
      this._releasedetails = data;
      this.DescripStatus = this.ArrSanction.moduleStatus;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  ViewEditLoanDetailsById(id: any, flag: any) {
    debugger;
    this.mode = 2;
    this.tempId = id;
    this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(data => {
      debugger;
      this._releasedetails = data[0];
      this._releasedetails.bnkAcNo = data[0].bnkAcNo;
      this._releasedetails.confirmbnkAcNo = data[0].bnkAcNo;
      this.buttonText = "Update";
      if (this._releasedetails.priVerifFlag == 23) {
        this.btndisable = true;
        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._releasedetails.priVerifFlag == 35) {
        this.btndisable = false;
        if (this.userRoleID == 6) {
          this.disableFwdMakerFlag = false;
          this.disableFwdCheckerFlag = false;
        }
        else {

          this.disableFwdMakerFlag = true;

        }
      }


      if (this._releasedetails.priVerifFlag == 31) {
        this.btndisable = true;
        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._releasedetails.priVerifFlag == 33) {
        this.btndisable = true;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {
          this.disableFwdMakerFlag = false;

        }
      }
      if (this._releasedetails.priVerifFlag == 22) {
        this.btndisable = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._releasedetails.priVerifFlag == 8) {
        this.disableSaveFlage = true;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._releasedetails.priVerifFlag == 24) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._releasedetails.priVerifFlag == 25) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {
          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._releasedetails.priVerifFlag == 34) {
        this.btndisable = true;
        if (this.userRoleID == 5) {
          this.disableVerifyCheckerFlag = true;
          this.disableRejectCheckerFlag = true;
        }
        else {
          this.disableFwdMakerFlag = false;
        }
      }
      if (this._releasedetails.priVerifFlag == 56) {
        this.btndisable = true;
        if (this.userRoleID == 5) {
          this.disableVerifyCheckerFlag = true;
          this.disableRejectCheckerFlag = true;
        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }


      if (flag == 1) {
        this.disbleflag = false;
        this.btndisable = true;

        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = true;
      }
      if (flag == 0) {
        this.disbleflag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = true;

      }

      if (this._releasedetails.ifscCD != "" && this._releasedetails.ifscCD != undefined) {

        this.objBankService.getBankDetailsByIFSC(this._releasedetails.ifscCD).subscribe(res => {
          this.objBankDetails = res;
        })
      }
      else {

        this.objBankDetails.branchName = '';
        this.objBankDetails.bankName = '';
        this._releasedetails.ifscCD = '';
      }
      if (flag == 1) {
        this.disbleflag = false;
        this.btndisable = true;

      }
      if (flag == 0) {
        this.disbleflag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = false;
      }
      if (flag == 2 && this.userRoleID == 6 && this._releasedetails.priVerifFlag == 34) {
        this.disableVerifyCheckerFlag = true;
        this.disableRejectCheckerFlag = true;
        this.disbleflag = true;
        this.btndisable = false;
      }


      if (flag == 1 && this.userRoleID == 5 && this._releasedetails.priVerifFlag == 56) {
        this.disableVerifyCheckerFlag = true;
        this.disableRejectCheckerFlag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = false;
        this.disbleflag = false;
        this.btndisable = false;
      }
    })
  }
  getBankDetailsByIFSC(ifscCD: string) {
    this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(res => {
      this.objBankDetails = res;
    });
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  deleteEmployeeById(id: any) {
    this.mode = 2;
    this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(data => {
      this._releasedetails = data;
      if (this._releasedetails != null) {
        this.deletePopup = false;
        swal(this._msg.deleteMsg);
        this._Service.DdoId = this.UserID;
        this.BindGridLoanSanction(this._Service.DdoId);       
        this.ReleaseForm.resetForm();
      }
    });
  }
  toggleShow() {     
    this.show = !this.show;
    if (this.show) {
      this.type = "text";
    }
    else {
      this.type = "password";
    }
  }
  forwarDDOMaker() {
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._releasedetails.priVerifFlag = 34;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.ForwardToDDOMaker);
        this.disableSaveFlage = false;
        this.disableFwdMakerFlag = false;
        this._Service.DdoId = this.UserID;
        this.BindGridLoanSanction(this.UserID);
        this.ReleaseForm.resetForm();
      });
    }
  }
  forwarDDOChecker() {
     
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._releasedetails.priVerifFlag = 56;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.ForwardToDDOChecker);
        this.disableSaveFlage = false;
        this.disableFwdCheckerFlag = false;
        this.BindGridLoanSanction(this.UserID);
        this.ReleaseForm.resetForm();
      });
    }
  }
  VerifyChecker() {
     
    if (this.userRoleID == 5) {
      this._releasedetails.priVerifFlag = 57;
    }
    this._releasedetails.msEmpId = this.tempId;
    this.objSanctionService.UpdateLoanSanctionDetails(this._releasedetails).subscribe(data => {
      this._releasedetails = data;
      this._Service.HiddenId = data;
      if (data == "-1") {
        this.BindGridLoanSanction(this.UserID);
        swal(this._msg.alreadyExistMsg);
      }
      else {
        this.BindGridLoanSanction(this.UserID);
        swal(this._msg.VerifyDDOChecker);
        this.ReleaseForm.resetForm();
      }
    });
  }
  Reject() {
     
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 5;
      this.remarks = this._releasedetails.sanction_Remarks;

      if (this.userRoleID == 5) {
        this._releasedetails.priVerifFlag = 58;
      }
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._releasedetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.Rejected);
        this.deletePopup = false;
        this.BindGridLoanSanction(this._Service.DdoId);
        this.ReleaseForm.resetForm();
      });
    }
  }
}
