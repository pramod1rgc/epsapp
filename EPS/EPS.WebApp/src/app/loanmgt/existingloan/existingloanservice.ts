import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Variable } from '@angular/compiler/src/render3/r3_ast';
@Injectable()

export class existingloanservice {
  BaseUrl: any = [];
  constructor(private httpclient: HttpClient) {

    this.BaseUrl = 'http://localhost:55424/api/';
  }

  BindDropDownDesign() {
    return this.httpclient.get(this.BaseUrl + 'existingloan/BindDropDownDesign', {});
  }
  BindDropDownBillGroup() {
    return this.httpclient.get(this.BaseUrl + 'existingloan/BindDropDownBillGroup', {});

  }
  BindLoanType() {
    return this.httpclient.get(this.BaseUrl + 'LoanApplication/BindDropDownLoanType', {});

  }
   EmployeeInfo(MsDesignID:string) {
     debugger;
    return this.httpclient.get(this.BaseUrl + 'existingloan/GetAllEmpNameSelectedDesignId?MsDesignID=' +MsDesignID, {});
   
  }
  LoanSection(EmpCode: string, MsDesignID: string)
  {
    debugger;
    return this.httpclient.get(this.BaseUrl + 'ExistingLoan/LoanAdvanceDetails?EmpCode=' + EmpCode + '&MsDesignID=' + MsDesignID, {});
  }

 

}
