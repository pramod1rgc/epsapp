import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayPastPerdComponent } from './pay-past-perd.component';

describe('PayPastPerdComponent', () => {
  let component: PayPastPerdComponent;
  let fixture: ComponentFixture<PayPastPerdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayPastPerdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayPastPerdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
