import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { NewLoanService } from '../../services/loan-mgmt/new-loan.service';
import { Sanction } from '../../model/LoanModel/sanction.model';
import swal from 'sweetalert2';
import { BankDetailsService } from '../../services/empdetails/bank-details.service';
import { CommanService } from '../../services/loan-mgmt/comman.service';
import { SanctionService } from '../../services/loan-mgmt/sanction.service';
import { CommonMsg } from '../../global/common-msg';
import { Subject } from "rxjs";
@Component({
  selector: 'app-sanction',
  templateUrl: './sanction.component.html',
  styleUrls: ['./sanction.component.css'],
  providers: [CommonMsg]
})
export class SanctionComponent implements OnInit {

  data: any;
  eventsSubject: Subject<void> = new Subject<void>();
  ArrSanction: any;
  setDeletIDOnPopup: any;
  dataSource: any;
  buttonText: string = 'Save';
  public mode: number;
  deletePopup: boolean;
  ArrLoanType: any;
  EmpTypeFlag: boolean;
  @ViewChild('Sanction') SanctionForm: any;
  public _sanctiondetails: Sanction;
  objBankDetails: any = {};
  displayedColumns: string[] = ['EMPId', 'EMPCode', 'LoanCode', 'PurposeCode', 'Status', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedIndex = 0;
  username: string;
  ArrEmpCode: any;
  empstatus: boolean;
  btndisable: boolean = false;
  disbleflag: boolean = false;
  show = false;
  jwt: any = {};
  type = "password";
  @ContentChild('showhideinput') input;
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
  disableSaveFlage: boolean = false;
  disableCancelFlage: boolean = false;
  disableFwdCheckerFlag: boolean = false;
  disableFwdMakerFlag: boolean = false;
  disableVerifyCheckerFlag: boolean = false;
  disableRejectCheckerFlag: boolean = false;
  disableUserBankDetailsFlag: boolean = false;
  tempId: any;
  flag: boolean;
  remarks: any;
  temp: number;
  public loantypeFlag: string;
  constructor(private _msg: CommonMsg, private _Service: NewLoanService, private objCommanService: CommanService,
    private objSanctionService: SanctionService, private objBankService: BankDetailsService) {
    this._sanctiondetails = new Sanction();
  }
  ngOnInit() {

    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.BindLoanType();
    this.BindGridLoanSanction(this.UserID);
  }
  GetcommanMethod(value) {
    this.getEmployeeDeatils(value, '0');
  }
  BindLoanType() {
    this.loantypeFlag = '2';
    this.objCommanService.BindLoanType(this.loantypeFlag).subscribe(data => {
      this.ArrLoanType = data;
    })
  }

  getEmployeeDeatils(EmpCode: string, MsDesignID: string) {
    this.mode = 1;
    this._sanctiondetails.payLoanRefLoanCD = 0;
    this.objSanctionService.GetSanctionDetails(EmpCode, MsDesignID, this.mode).subscribe(data => {
      this._sanctiondetails = data[0];
      if (this._sanctiondetails != undefined) {
        this.getBankDetailsByIFSC(this._sanctiondetails.ifscCD);
      }
      if (this._sanctiondetails == undefined) {
        swal(this._msg.noRecordMsg);
        this._sanctiondetails = new Sanction();
      }
      if (this._sanctiondetails.empApptType == 'R') {
        this.EmpTypeFlag = true;
        this.disableUserBankDetailsFlag = false;
      }
      else {
        this.EmpTypeFlag = false;
      }
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  btnsubmit() {

    if (this.buttonText == 'Save') {
      if (this.userRoleID == 6) {
        this.disableFwdCheckerFlag = true;
        this._sanctiondetails.priVerifFlag = 34;
      }
      if (this.userRoleID == 5) {
        this.disableFwdCheckerFlag = true;
        this._sanctiondetails.priVerifFlag = 35;
      }
      this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(data => {
        this._sanctiondetails = data;
        this._Service.HiddenId = data;
        if (data == "-1") {
          this.BindGridLoanSanction(this.UserID);
          swal(this._msg.alreadyExistMsg);
        }
        else {
          this.BindGridLoanSanction(this.UserID);
          swal(this._msg.saveMsg);

        }

      });

      this.SanctionForm.resetForm();
    }
    if (this.buttonText == 'Update') {

      if (this.userRoleID == 6) {
        this.disableFwdCheckerFlag = true;
        this._sanctiondetails.priVerifFlag = 34;
      }
      if (this.userRoleID == 5) {
        this.disableFwdCheckerFlag = true;
        this._sanctiondetails.priVerifFlag = 35;
      }
      this._sanctiondetails.msEmpId = this.tempId;
      this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(data => {
        this._sanctiondetails = data;
        swal(this._msg.updateMsg);
        this.BindGridLoanSanction(this.UserID);
        this.SanctionForm.resetForm();
      });

    }
  }
  resetLoanDdetailsForm() {
    debugger;
    this.SanctionForm.resetForm();
    this.eventsSubject.next(this.data);

  }  

  BindGridLoanSanction(value) {
    this.mode = 3; //loan sanction and release details(HOO Label) ,select data in the table
    this.UserID = value;
    this._Service.BindLoanStatus(this.mode, this.UserID).subscribe(data => {
      this.ArrSanction = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  valuechange(value) {

    if (this._sanctiondetails.loanAmtSanc >= value) {
      this._sanctiondetails.loanAmtDisbursed;
    }

    else {
      this._sanctiondetails.loanAmtDisbursed;
      swal(this._msg.AmountShouldbelessthanRequestedAmount);
    }


  }
  ViewEditLoanDetailsById(id: any, flag: any) {

    this.mode = 3;
    this.tempId = id;
    this.objSanctionService.GetSanctionDetails(id, '0', this.mode).subscribe(data => {
      this._sanctiondetails = data[0];
      this._sanctiondetails.ifscCD;
      this.getBankDetailsByIFSC(this._sanctiondetails.ifscCD);

      this.buttonText = "Update";
      if (this._sanctiondetails.priVerifFlag == 23) {
        this.btndisable = true;
        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._sanctiondetails.priVerifFlag == 35) {
        this.btndisable = false;
        if (this.userRoleID == 6) {
          this.disableFwdMakerFlag = false;
          this.disableFwdCheckerFlag = false;
        }
        else {

          this.disableFwdMakerFlag = true;

        }
      }


      if (this._sanctiondetails.priVerifFlag == 31) {
        this.btndisable = true;
        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._sanctiondetails.priVerifFlag == 33) {
        this.btndisable = true;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;

        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._sanctiondetails.priVerifFlag == 22) {
        this.btndisable = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._sanctiondetails.priVerifFlag == 8) {
        this.disableSaveFlage = true;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._sanctiondetails.priVerifFlag == 24) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._sanctiondetails.priVerifFlag == 25) {
        this.disableSaveFlage = false;

        if (this.userRoleID == 6) {
          this.disableFwdCheckerFlag = true;
        }
        else {

          this.disableFwdCheckerFlag = false;
          this.disableFwdMakerFlag = true;
        }
      }
      if (this._sanctiondetails.priVerifFlag == 34) {
        this.btndisable = true;
        if (this.userRoleID == 5) {
          this.disableVerifyCheckerFlag = true;
          this.disableRejectCheckerFlag = true;
        }
        else {

          this.disableFwdMakerFlag = false;

        }
      }
      if (this._sanctiondetails.empApptType == 'R' || this._sanctiondetails.empApptType == 'T') {
        this.EmpTypeFlag = true;
        this.disableUserBankDetailsFlag = true;
      }
      else {
        this.EmpTypeFlag = true;
        this.disableUserBankDetailsFlag = false;
      }
      if (flag == 1) {
        this.disbleflag = false;
        this.btndisable = true;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = true;

      }
      if (flag == 0) {
        this.disbleflag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = true;
      }
      if (this._sanctiondetails.ifscCD != "" && this._sanctiondetails.ifscCD != undefined) {
        this.objBankService.getBankDetailsByIFSC(this._sanctiondetails.ifscCD).subscribe(res => {
          this.objBankDetails = res;
        })
      }
      else {
        this.objBankDetails.branchName = '';
        this.objBankDetails.bankName = '';
        this._sanctiondetails.ifscCD = '';
      }
      if (flag == 2 && this.userRoleID == 6 && this._sanctiondetails.priVerifFlag == 34) {
        this.disableVerifyCheckerFlag = true;
        this.disableRejectCheckerFlag = true;
        this.disbleflag = true;
        this.btndisable = false;
      }
      if (flag == 2 && this.userRoleID == 5 && this._sanctiondetails.priVerifFlag == 35) {
        this.disableVerifyCheckerFlag = true;
        this.disableRejectCheckerFlag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = false;
      }
      if (flag == 1 && this.userRoleID == 6) {
        this.disbleflag = false;
        this.btndisable = true;
      }
      else if (flag == 1 && this.userRoleID == 5) {
        this.disbleflag = false;
        this.btndisable = false;
        this.disableFwdCheckerFlag = false;
      }
      if (flag == 0) {
        this.disbleflag = true;
        this.btndisable = false;
        this.disableFwdMakerFlag = false;
        this.disableFwdCheckerFlag = false;
        this.disableUserBankDetailsFlag = true;

      }
    })
  }

  getBankDetailsByIFSC(ifscCD: string) {
    this.objBankService.getBankDetailsByIFSC(ifscCD).subscribe(res => {
      this.objBankDetails = res;

    });
  }
  SetDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  deleteEmployeeById(id: any) {
    this.mode = 2;
    this.objCommanService.DeleteLoanDetailsByEMPID(id, this.mode).subscribe(data => {
      this._sanctiondetails = data;
      if (this._sanctiondetails != null) {
        this.deletePopup = false;
        swal(this._msg.deleteMsg);
        this.BindGridLoanSanction(this.UserID);
        this.resetLoanDdetailsForm();
      }
    });
  }
  toggleShow() {

    this.show = !this.show;
    if (this.show) {
      this.type = "text";
    }
    else {
      this.type = "password";
    }
  }

  forwarDDOMaker() {
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._sanctiondetails.priVerifFlag = 34;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.ForwardToDDOMaker);
        this.disableSaveFlage = false;
        this.disableFwdMakerFlag = false;
        this.BindGridLoanSanction(this.UserID);
        this.SanctionForm.resetForm();
      });
    }
  }
  forwarDDOChecker() {

    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 2;
      this._sanctiondetails.priVerifFlag = 35;
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.ForwardToDDOChecker);
        this.disableSaveFlage = false;
        this.disableFwdCheckerFlag = false;
        this.BindGridLoanSanction(this.UserID);
        this.SanctionForm.resetForm();
      });
    }
  }
  VerifyChecker() {
    this._sanctiondetails.priVerifFlag = 54;
    this._sanctiondetails.msEmpId = this.tempId;
    this.objSanctionService.UpdateLoanSanctionDetails(this._sanctiondetails).subscribe(data => {
      this._sanctiondetails = data;
      this._Service.HiddenId = data;
      if (data == "-1") {
        this.BindGridLoanSanction(this.UserID);
        swal(this._msg.alreadyExistMsg);
      }
      else {
        this.BindGridLoanSanction(this.UserID);
        swal(this._msg.VerifyDDOChecker);
      }
    });
  }

  Cancel() {
    this.deletePopup = false;
  }

  Reject() {
    if (this._Service.HiddenId != null) {
    }
    if (this.tempId != null) {
      this.mode = 4;
      this.remarks = this._sanctiondetails.sanction_Remarks;

      if (this.userRoleID == 5) {
        this._sanctiondetails.priVerifFlag = 32;
      }
      else {

        this._sanctiondetails.priVerifFlag = 35;
      }
      this._Service.UpdateLoanDetailsbyID(this.tempId, this.mode, this.remarks, this._sanctiondetails.priVerifFlag).subscribe(data => {
        this.flag = true;
        swal(this._msg.Rejected);
        this.deletePopup = false;
      });
    }
  }
  setempApptType(value) {

    if (value == "R") { this.disableUserBankDetailsFlag = false; }
    if (value == "T") { this.disableUserBankDetailsFlag = true; }
  }
}
