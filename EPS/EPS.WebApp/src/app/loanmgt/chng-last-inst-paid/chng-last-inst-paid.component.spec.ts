import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChngLastInstPaidComponent } from './chng-last-inst-paid.component';

describe('ChngLastInstPaidComponent', () => {
  let component: ChngLastInstPaidComponent;
  let fixture: ComponentFixture<ChngLastInstPaidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChngLastInstPaidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChngLastInstPaidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
