import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloodadvComponent } from './floodadv.component';

describe('FloodadvComponent', () => {
  let component: FloodadvComponent;
  let fixture: ComponentFixture<FloodadvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloodadvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloodadvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
