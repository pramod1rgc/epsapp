import { Component, OnInit,  ViewChild } from '@angular/core';
//import { ExistLoanService } from '../../services/loan-mgmt/exist-loan.service';
//import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
//import { Router } from '@angular/router';
//import { ReplaySubject } from 'rxjs';
//import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
//import { LeavesSanctionMainModel } from '../../model/LeavesModel/leavesSanctionModel';
import { PromotionsService } from '../../services/promotions/promotions.service';
import { FloodService } from '../../services/loan-mgmt/flood-service';
//import { locale } from 'moment';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { FloodModel, EmpDeAttach,EmpAttach } from '../../model/LoanModel/floodModel';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';
//import { and } from '@angular/router/src/utils/collection';

//interface BillCode {
//  id: string;
//  name: string;
//}

@Component({
  selector: 'app-floodadv',
  templateUrl: './floodadv.component.html',
  styleUrls: ['./floodadv.component.css'],
  providers: [FloodService, CommonMsg]
})
export class FloodadvComponent implements OnInit {

  //leaveSanctionCreationForm: FormGroup;
  public ArrLoanData: FloodModel;
  BillGroup: any;
  EmpGroup: any;
  DesigGroup: any;
  EmpDetails: any;
  dataSource: any;
  dataDeAttach: any[] = [];
 
  dataSourceDeAttach: any;
  data: any;
  isTableHasData = true;
  checkedObject: any[] = [];
  DcheckedObject: any[] = [];


  totalEmpcode: any[] = [];
  DisbursedAmnt = 7500;

  btnHideShow: boolean = false;
  
  FlagSave: boolean = false;
  FlagCancel: boolean = false;
  FlagForwordChecker: boolean = false;
  FlageAccept: boolean = false;
  FlagReject: boolean = false;


  stringArr: string[];
  displayedColumns: string[] = ['select', 'EmpFirstName', 'EmpCd', 'DesigDesc','priVerifFlag',];
  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);


  public username: string;
  ddoid: number;
  UserID: number;
  userRole: string;
  userRoleID: number;
 public mode: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('FloodLaon') FloodLaon: any;

  //showErrors: boolean = false;
 

  constructor(private _PromotionsService: PromotionsService, private _FloodService: FloodService, private commonMsg: CommonMsg) {
    this.ArrLoanData = new FloodModel();
  }

  ngOnInit() {
    
    this.username = sessionStorage.getItem('username');
    this.ddoid = Number(sessionStorage.getItem('ddoid'));
    this.UserID = Number(sessionStorage.getItem('UserID'));
    //this.userRole = sessionStorage.getItem('userRole');
    this.userRoleID = Number(sessionStorage.getItem('userRoleID'));
    this.username = sessionStorage.getItem('username');

    this.BindDropDownBillGroup();
  }
 

  SaveRecords() {
    debugger;
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      swal(this.commonMsg.AttachAtleastOne)
    }

    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.totalEmpcode.push(d.empCd);
        });
      }
      this.ArrLoanData.totalEmpcode = this.totalEmpcode;
      this._FloodService.InsertFloodData(this.ArrLoanData).subscribe(result => {
        this.EmpDetails = result;
        //alert(this.EmpDetails);
        if (this.EmpDetails != 0 && this.EmpDetails != -1) {
          swal(this.commonMsg.saveMsg);
        }
        else {
          swal(this.commonMsg.alreadyExistMsg)
        }
      });
    }
    //this.checkedObject = null;
    this.totalEmpcode = [];
  }

  ForwardtoDDOChecker() {
    if (this.checkedObject.length == 0 || this.checkedObject == null) {
      swal(this.commonMsg.AttachAtleastOne)
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.totalEmpcode.push(d.empCd);
        });
      }
      this.ArrLoanData.totalEmpcode = this.totalEmpcode;
      this.ArrLoanData.Mode = 1;
      this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(result => {
        this.EmpDetails = result;
        //alert(this.EmpDetails);
        if (this.EmpDetails != 0 && this.EmpDetails != -1) {
          swal(this.commonMsg.forwardCheckerMsg);
        }
        else {
          swal(this.commonMsg.noRecordMsg)
        }
      });
    }
    this.totalEmpcode = [];
  }

  accept() {
    if (this.checkedObject.length == 0 || this.checkedObject == null) {
      swal(this.commonMsg.AttachAtleastOne)
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.totalEmpcode.push(d.empCd);
        });
      }
      this.ArrLoanData.totalEmpcode = this.totalEmpcode;
      this.ArrLoanData.Mode = 2;
      this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(result => {
        this.EmpDetails = result;
        //alert(this.EmpDetails);
        if (this.EmpDetails != 0 && this.EmpDetails != -1) {
          swal(this.commonMsg.VerifyedByChecker);
        }
        else {
          swal(this.commonMsg.noRecordMsg)
        }
      });
    }
    this.totalEmpcode = [];
  }



  Reject() {
    if (this.checkedObject.length == 0 || this.checkedObject == null) {
      swal(this.commonMsg.AttachAtleastOne)
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.totalEmpcode.push(d.empCd);
        });
      }
      this.ArrLoanData.totalEmpcode = this.totalEmpcode;
      this.ArrLoanData.Mode = 3;
      this._FloodService.ForwordToDdo(this.ArrLoanData).subscribe(result => {
        this.EmpDetails = result;
        //alert(this.EmpDetails);
        if (this.EmpDetails != 0 && this.EmpDetails != -1) {
          swal(this.commonMsg.RejectedByChecker)
        }
        else {
          swal(this.commonMsg.noRecordMsg)
        }
      });
    }
    this.totalEmpcode = [];
  }


  //toggleErrors() {
  //  this.showErrors = !this.showErrors;
  //}


  BindDropDownBillGroup() {
  
    this._PromotionsService.BindDropDownBillGroup().subscribe(result => {
      this.BillGroup = result;
    })
  }

  BindDropDownDesigGroup(selectedBilgrp) {
    localStorage.setItem('BillGrpId', selectedBilgrp);
    this._PromotionsService.BindDropDownDesigGroup(selectedBilgrp).subscribe(result => {
      this.DesigGroup = result;
      //console.log(this.DesigGroup);
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onBillGrpSel(selectedBilgrp) {
    this.BindDropDownDesigGroup(selectedBilgrp);
  }

  GetDetailsForFlood(desigId) {
    this.btnHideShow = true;
    this.FlagSave = true;
    this.FlagCancel = true;
    this.FlagForwordChecker = true;

    if (this.userRoleID == 5) {
      this.FlageAccept = true;
      this.FlagReject = true;
      this.FlagSave = true;
      this.FlagCancel = true;
      this.FlagForwordChecker = true;
      this.mode = "2";
    }
    else {
      this.mode = "1";
    }

    var billGrID = localStorage.getItem('BillGrpId')
    this._FloodService.GetEmpDetails(billGrID, desigId, this.UserID, this.mode).subscribe(result => {
      this.EmpDetails = result;
      this.dataSource = new MatTableDataSource(result);
      this.data = Object.assign(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllSelectedForDeAttach() {
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }

  masterToggleForAttach() {
    this.checkedObject = null;
   
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));
    this.checkedObject = this.dataSource.data;
    this.checkedObject = this.selectionAttach.selected;
   // alert(this.selectionAttach.selected);
  }

  //working
  masterToggleForDeAttach() {
    this.checkedObject = null;
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
     this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
   // this.checkedObject = this.dataSourceDeAttach.data;
    this.DcheckedObject = this.dataSourceDeAttach.data;
   // this.DcheckedObject = this.selectionDeAttach.selected;
  }


  AddCheckedObj() {
    this.checkedObject = null;
    this.checkedObject = this.selectionAttach.selected;
  }


  AttachSelectedRows() {
    this.checkedObject = null;
    if (this.selectionAttach.hasValue()) {
     
      this.selectionAttach.selected.forEach(item => {
        let index: number = this.data.findIndex(d => d === item);
        this.dataSource.data.splice(index, 1);
        // this.dataDeAttach.push(item);
        this.dataDeAttach.unshift(item);
        this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);
        this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);
        this.dataSource.paginator = this.paginator;
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
    }
    else {
      swal(this.commonMsg.AttachAtleastOne);
    }
  }


  DeAttachSelectedRows() {
    this.checkedObject = null;
    if (this.selectionDeAttach.hasValue()) {
      this.selectionDeAttach.selected.forEach(item => {
        let index: number = this.dataDeAttach.findIndex(d => d === item);
        this.dataDeAttach.splice(index, 1);
        //this.data.push(item);
        this.data.unshift(item);
        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
      this.dataSource.paginator = this.paginator;
    }
    else {
      alert(this.commonMsg.DeAttachAtleastOne);
    }
  }


  cancel() {
    this.FloodLaon.resetForm();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  CalculateValue(value) {
    this.ArrLoanData.PriInstAmt = Math.round(7500 / value);
    if (this.ArrLoanData.PriInstAmt==Infinity) {
      this.ArrLoanData.PriInstAmt = 0;
    }
  }

  CalculatePriBal(value) {
    //var a = parseInt(value);
    //console.log(a);
    this.ArrLoanData.PriBalance = 0;
    var paidAmt = this.ArrLoanData.PriInstAmt * parseInt(value); 
    var PriBalance = 7500 - paidAmt
    this.ArrLoanData.PriBalance = PriBalance;
   // alert(PriBalance);
  }
}




//const ELEMENT_DeAttachdata: EmpDeAttach[] = [];
//const ELEMENT_DATA: EmpAttach[] = [];

//export interface EmpAttach {
//  EmpFirstName: string;
//  EmpCd: string;
//  DesigDesc: string;
//}

//export interface EmpDeAttach {
//  EmpFirstName: string;
//  EmpCd: string;
//  DesigDesc: string;
//}
