import { Component, OnInit, ViewChild } from '@angular/core';
import { SuspensionService } from '../../services/Suspension/Suspension_service';
import {
  PayBillGroupModel,
  EmpModel,
  DesignationModel
} from '../../model/Shared/DDLCommon';
import { ExtensionDetails } from '../../model/Suspension/ExtensionDetails';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { MatSnackBar, MatSort, MatPaginator, MatTableDataSource, MatSelect, MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-extension',
  templateUrl: './extension.component.html',
  styleUrls: ['./extension.component.css']
})
export class ExtensionComponent implements OnInit {
  objempdesig: PayBillGroupModel;
  objempDesignation: DesignationModel;
  objempModel: EmpModel;
  objempExtDetails: ExtensionDetails;
  selectedBillGroup: string;
  selectedDesignation: string;
  selctedEmp: string;
  SuspensionDetailsForm: FormGroup;
  permddoId: string;
  selectedEmpCode: string;
  selectedvalue = 0;
  successMsg: string;
  orderValidFlag: boolean;
  extDataDetails: MatTableDataSource<ExtensionDetails>;
  dataSourceHistory: MatTableDataSource<ExtensionDetails>;
  _btnText: string;
  _btnUpdateText: string;
  _Id: number;
  deactivatePopup: any;
  validUptoDate: string;
  susPeriodInNumber = 0;
  ShowSusDetails = false;
  isCheckerLogin = false;
  disableButton = false;
  isViewDetails = true;
  isRejected = false;
  reasonText: string;
  isVarified: boolean;
  deletepopup: boolean;
  hooCheckerLogin: boolean;
  isReasonEmpty: boolean;
  highlightedRows = [];
  displayedColumns: string[] = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status', 'Action'];
  historyTableColumns: string[] = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status'];
  todayDate: Date;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatPaginator) hisPaginator: MatPaginator;
  // @ViewChild(MatSort) hisSort: MatSort;

  @ViewChild('paginatorTable1') paginator: MatPaginator;
  @ViewChild('paginatorTable2') paginatorHistory: MatPaginator;

  @ViewChild('t1Sort') t1Sort: MatSort;
  @ViewChild('t2Sort') t2Sort: MatSort;

  @ViewChild('formCreation') myformm: any;
  @ViewChild('singleSelect') desigddl: MatSelect;
  @ViewChild('exPanel') exPanel: MatExpansionPanel;


  constructor(
    private objsuspensionservice: SuspensionService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.CreateSuspensionDetailsForm();
  }

  ngOnInit() {

    this._btnUpdateText = 'Forward to  HOO Checker';
    this._btnText = 'Save';
    if (+sessionStorage.getItem('userRoleID') === 9) {
      this.isCheckerLogin = false;
    } else {
      this.isCheckerLogin = true;
      this.ShowSusDetails = true;
      this._btnUpdateText = 'Reject';
      this._btnText = 'Accept';
    }
    this.GetAllDesignation();
    this.GetAllEmployees('0');
    this.objempExtDetails = new ExtensionDetails();
    this.orderValidFlag = true;

    // this.SuspensionDetailsForm.disable();
    this.SuspensionDetailsForm.get('FromDate').disable();
    // this.SuspensionDetailsForm.get('btnSuspensionDetails').disable();
    this.validUptoDate = '';
    this.reasonText = '';
    this.isVarified = true;
    this.isReasonEmpty = false;
    this.todayDate = new Date();
    // if (this.isCheckerLogin === 'true') {
    //   this.SuspensionDetailsForm.disable();

    // }

  }
  GetAllEmployees(desigId: string) {
    const comName = 'exten';
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice
      .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
      .subscribe(res => {
        this.objempModel = res;
      });
  }

  GetAllDesignation() {
    this.objsuspensionservice
      .GetAllDesignation('')
      .subscribe(res => {
        this.objempDesignation = res;
      });
  }

  // GetPayBillGroup() {
  //   this.objsuspensionservice.GetPayBillGroup().subscribe(res => {
  //     this.objempdesig = res;
  //   });
  // }

  CreateSuspensionDetailsForm() {
    this.SuspensionDetailsForm = this.fb.group({
      FromDate: ['', Validators.required],
      OrderNo: ['', Validators.required],
      SusPeriod: ['', [Validators.required, Validators.min(1), Validators.max(120)]],
      OrderDate: ['', Validators.required],
      SalaryPercent: ['', [Validators.required, Validators.max(75), Validators.min(50), Validators.maxLength(2)]],
      Remarks: ['', Validators.required],
      RejectedReason: ['', '']
    });
  }

  ddlDesignationChanged(desigId: string) {
    //this.selectedEmpValue = null;
    this.desigddl.placeholder = null;
    if (Number(desigId) === -1) {
       this.selectedvalue = null;
     //  this.objempModel = this.allempList;
       this.desigddl.placeholder = 'Select Designation';
       desigId = '0';
    }

    this.GetAllEmployees(desigId);
  }

  ShowSuspensionDetails(isShow: boolean): void {
    this.ShowSusDetails = isShow;
  }

  AutoFillDate(daysAndDate: any, isdate: boolean) {
    let fdate: Date;
    if (isdate) {
      fdate = new Date(daysAndDate);
      fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
    } else {
      this.susPeriodInNumber = daysAndDate;
      fdate = new Date(this.objempExtDetails.FromDate);
      fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
      this.objempExtDetails.ToDate = new Date(fdate + 'UTC');
    }
    this.validUptoDate = fdate.toDateString();
  }
  // ddlPayBillGroupChanged(payBillGroupId: string) {
  //   this.objsuspensionservice
  //     .GetAllDesignationOnSelectedPayBillGroup(payBillGroupId)
  //     .subscribe(res => {
  //       this.objempDesignation = res;
  //     });
  // }
  ddlEmployeeChanged(empId: string) {
    this.selectedEmpCode = empId.split('@')[0].trim();
    this.selectedvalue = Number(empId.split('@')[1]);
    // this.SuspensionDetailsForm.get('FromDate').enable();
    this.orderValidFlag = false;
    this.isViewDetails = true;
    this.BindSuspensionDetails(this.selectedEmpCode);
    this.Reset();
    if (this.isCheckerLogin) {
      this._btnText = 'Accept';
    }
  }

  BindSuspensionDetails(empId: string) {
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    const comName = 'exten';
    this.objsuspensionservice.GetSuspensionDetails(empId, checkerLogin, comName).subscribe((result: any) => {

      this.extDataDetails = new MatTableDataSource<ExtensionDetails>(result.filter(obj => obj.status !== 'Verified'));
      this.extDataDetails.paginator = this.paginator;
      this.extDataDetails.sort = this.t1Sort;

      this.exPanel.disabled = false;
      if (this.extDataDetails.data.length > 0) {
        this.exPanel.close();
        this.exPanel.disabled = true;
      }

      this.dataSourceHistory = new MatTableDataSource<ExtensionDetails>(result.filter(obj => obj.status === 'Verified'));
      this.dataSourceHistory.paginator = this.paginatorHistory;
      this.dataSourceHistory.sort = this.t2Sort;

    });



  }

  GetSuspensionDetails() {
    alert('Go');
  }

  chkTillFurtherOrder(chk: any) {
    // this.orderValidFlag = chk.checked ? true : false;
    const toDate = this.SuspensionDetailsForm.get('ToDate');

    if (chk.checked) {
      toDate.disable();
      this.objempExtDetails.ToDate = null;
      toDate.setValidators(null);
    } else {
      toDate.enable();
      toDate.setValidators(Validators.required);
    }
    toDate.updateValueAndValidity();
  }

  SavedSuspensionDetails(objsus: ExtensionDetails): void {
    // tslint:disable-next-line:no-debugger
    debugger;
    objsus.PermddoId = '00003';
    this.disableButton = true;
    // objsus.EmpCode = this.selectedEmpCode;
    // if (objsus.IsTillOrder == null) { objsus.IsTillOrder = false; }
    if (objsus.Status == null) { objsus.Status = 'I'; }
    // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
    // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
    this.objsuspensionservice.SavedExtensionDetails(objsus).subscribe((arg: any) => {
      if (+arg > 0) {
        this.successMsg = 'Record saved successfully';
      }
      this.myformm.resetForm();
      this.objempExtDetails.Id = 0;
      this.objempExtDetails.Status = null;
      this.BindSuspensionDetails(this.selectedEmpCode);
      this.disableButton = false;
      this.snackBar.open(this.successMsg, null, { duration: 4000 });
    });
  }

  ForwardToChecker(obj: ExtensionDetails): void {
    obj.Status = 'F';
    this.SavedSuspensionDetails(obj);
  }

  VarifiedSus(id: number, flag: boolean): void {
    if (flag) {
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_exten', 'V', '').subscribe((arg: any) => {

        if (+arg > 0) {
          this.successMsg = 'Varified successfully';
        }
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      this.myformm.resetForm();
      this.objempExtDetails.Id = 0;
      this.objempExtDetails.Status = null;
     // $('.dialog__close-btn').click();
      this.hooCheckerLogin = false;
    } else {
      this.isVarified = true;
    }
  }

  RejectedSus(id: number, flag: boolean): boolean {

    if (flag) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        this.successMsg = 'Please enter reason';
        return false;
      }

      if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
        this.isReasonEmpty = true;
        this.successMsg = 'Special characters are not allowed';
        return false;
      }
      this.successMsg = null;

      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_exten', 'R', this.reasonText).subscribe((arg: any) => {
        if (+arg > 0) {
          this.successMsg = 'Rejected successfully';
        }
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
        return false;
      });
      this.myformm.resetForm();
      this.objempExtDetails.Id = 0;
      this.objempExtDetails.Status = null;
      this.hooCheckerLogin = false;
      //$('.dialog__close-btn').click();
    } else {
      this.isVarified = false;
      return false;
    }
  }

  rowHighlight(objsus: any) {
    this.objempExtDetails = new ExtensionDetails();
    this.highlightedRows.pop();
    this.highlightedRows.push(objsus);
    this.objempExtDetails.SuspenId = objsus.id;
    console.log(objsus.id);
    // //this.objempSusDetails.FromDate = objsus.fromDate;
    // this.objempSusDetails.OrderDate = objsus.orderDate;
    // this.objempSusDetails.OrderNo = objsus.orderNo;
    // this.objempSusDetails.ToDate = objsus.toDate;
    // this.objempSusDetails.SalaryPercent = objsus.salaryPercent;
    // this.objempSusDetails.Remarks = objsus.remarks;
    // this.objempSusDetails.SusPeriod = objsus.susPeriod;
    const toDate = new Date(objsus.toDate);
    toDate.setDate(toDate.getDate() + 1);
    this.objempExtDetails.FromDate = new Date(toDate + 'UTC');
    this._btnText = 'Save';
    // const fdate = new Date(objsus.fromDate);
    // fdate.setDate(fdate.getDate() + +objsus.susPeriod);
    // this.validUptoDate = fdate.toDateString();
  }
  // btnEditClick(id: number, fromDate: Date, toDate: number, orderNo: string, orderDate: Date, salaryPercent: number, remarks: string) {
  //   this.objempSusDetails.Id = id;
  //   this.objempSusDetails.FromDate = fromDate;
  //   this.objempSusDetails.ToDate = toDate;
  //   this.objempSusDetails.OrderNo = orderNo;
  //   this.objempSusDetails.OrderDate = orderDate;
  //   this.objempSusDetails.Remarks = remarks;
  //   this.objempSusDetails.SalaryPercent = salaryPercent;

  //   }



  btnEditClick(objsus: any, isViewOnly: boolean) {
    debugger;
    this.exPanel.disabled = false;
    this.exPanel.open();
    this.objempExtDetails.Id = objsus.id;
    this.objempExtDetails.FromDate = objsus.fromDate;
    this.objempExtDetails.OrderDate = objsus.orderDate;
    this.objempExtDetails.OrderNo = objsus.orderNo;
    // this.objempExtDetails.ToDate = objsus.toDate;
    this.objempExtDetails.SalaryPercent = objsus.salaryPercent;
    this.objempExtDetails.Remarks = objsus.remarks;
    this.objempExtDetails.SusPeriod = objsus.susPeriod;
    this._btnText = 'Save';
    this._Id = objsus.id;

    this.isRejected = false;
    if (objsus.rejectedReason !== '') {
      this.objempExtDetails.RejectedReason = objsus.rejectedReason;
      this.isRejected = true;
    }
    // const toDate = this.SuspensionDetailsForm.get('ToDate');
    // if (objsus.isTillOrder) {
    //   this.objempSusDetails.ToDate = null;
    //   toDate.disable();
    //   toDate.setValidators(null);
    // } else {
    //   toDate.enable();
    //   toDate.setValidators(Validators.required);
    // }
    // toDate.updateValueAndValidity();
    this.SuspensionDetailsForm.enable();
    const fdate = new Date(objsus.fromDate);
    fdate.setDate(fdate.getDate() + +objsus.susPeriod);
    this.validUptoDate = fdate.toDateString();

    this.objempExtDetails.ToDate = new Date(fdate + 'UTC');

    if (this.isCheckerLogin) {
      this._btnText = 'Accept';
      this.SuspensionDetailsForm.disable();
    }
    this.isViewDetails = true;
    if (isViewOnly) {
      this.SuspensionDetailsForm.disable();
      this.isViewDetails = false;
    }

  }

  DeleteSuspensionDetails(id: number, flag: boolean) {
    this._Id = flag ? id : 0;
    this.isViewDetails = true;
    if (!flag) {
      this.objsuspensionservice.DeleteSuspensionDetails(id, 'exten').subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }
    this.deletepopup = false;
   // $('.dialog__close-btn').click();

  }

  applyFilter(filterValue: string) {
    if (this.isCheckerLogin) {
      this.extDataDetails.filter = filterValue.trim().toLowerCase();
      if (this.extDataDetails.paginator) {
        this.extDataDetails.paginator.firstPage();
      }
    } else {
      this.dataSourceHistory.filter = filterValue.trim().toLowerCase();
      if (this.dataSourceHistory.paginator) {
        this.dataSourceHistory.paginator.firstPage();
      }
    }

  }

  Reset() {
    this._btnText = 'Save';
    this.objempExtDetails = new ExtensionDetails();
    // this.SuspensionDetailsForm.reset();
    //    this.SuspensionDetailsForm.reset({
    // });

  }
}

