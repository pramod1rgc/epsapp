import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SuspensionService } from '../../services/Suspension/Suspension_service';
import {
  PayBillGroupModel,
  EmpModel,
  DesignationModel
} from '../../model/Shared/DDLCommon';
import { SuspensionDetails } from '../../model/Suspension/SuspensionDetails';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar, MatSort, MatPaginator, MatTableDataSource, MatExpansionPanel, MatSelect, MatDatepicker } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-suspension',
  templateUrl: './suspension.component.html',
  styleUrls: ['./suspension.component.css']
})
export class SuspensionComponent implements OnInit {
  objempdesig: PayBillGroupModel;
  objempDesignation: DesignationModel;
  objempModel: EmpModel;
  objempSusDetails: SuspensionDetails;
  selectedBillGroup: string;
  selectedDesignation: string;
  selctedEmp: string;
  SuspensionDetailsForm: FormGroup;
  permddoId: string;
  selectedEmpCode: string;
  successMsg: string;
  orderValidFlag: boolean;
  dataSource: MatTableDataSource<SuspensionDetails>;
  dataSourceHistory: MatTableDataSource<SuspensionDetails>;
  _btnText: string;
  _btnUpdateText: string;
  _Id: number;
  deactivatePopup: any;
  validUptoDate: string;
  susPeriodInNumber = 0;
  ShowSusDetails = false;
  isCheckerLogin = false;
  isViewDetails = true;
  selectedvalue = 0;
  selectedEmpValue = 0;
  isRejected = false;
  disableButton = false;
  reasonText: string;
  isVarified: boolean;
  isReasonEmpty: boolean;
  deletepopup: boolean;
  hooCheckerLogin: boolean;
  displayedColumns: string[] = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status', 'Action'];
  historyTableColumns: string[] = ['OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Remarks', 'Status'];
  desigList: any[];
  allempList: any;
  todayDate: Date;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatPaginator) hisPaginator: MatPaginator;
  // @ViewChild(MatSort) hisSort: MatSort;

  @ViewChild('paginatorTable1') paginator: MatPaginator;
  @ViewChild('paginatorTable2') paginatorHistory: MatPaginator;

  @ViewChild('t1Sort') t1Sort: MatSort;
  @ViewChild('t2Sort') t2Sort: MatSort;
  @ViewChild('susPanel') exPanel: MatExpansionPanel;
  @ViewChild('formCreation') myformm: any;
  @ViewChild('singleSelect') desigddl: MatSelect;

  /** control for the selected bank for option groups */
  public desigfilterddl: FormControl = new FormControl();
  /** list of bank groups filtered by search keyword for option groups */
  public filteredDesig: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor(
    private objsuspensionservice: SuspensionService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.CreateSuspensionDetailsForm();
  }

  ngOnInit() {
    // this.GetPayBillGroup();
    // console.log(sessionStorage.getItem('userRole'));
    // listen for search field value changes
    this.desigfilterddl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesig();
      });

    console.log(sessionStorage.getItem('userRoleID'));
    this._btnUpdateText = 'Forward to  HOO Checker';
    this._btnText = 'Save';
    if (+sessionStorage.getItem('userRoleID') === 9) {
      this.isCheckerLogin = false;
    } else {
      this.isCheckerLogin = true;
      this.ShowSusDetails = true;
      this._btnUpdateText = 'Reject';
      this._btnText = 'Accept';
    }
    console.log(this.isCheckerLogin);
    this.GetAllDesignation();
    this.GetAllEmployees('0');
    this.objempSusDetails = new SuspensionDetails();
    this.orderValidFlag = true;

    // this.SuspensionDetailsForm.disable();
    this.SuspensionDetailsForm.get('FromDate').disable();
    // this.SuspensionDetailsForm.get('btnSuspensionDetails').disable();
    this.validUptoDate = '';
    this.reasonText = '';
    this.isVarified = true;
    this.isReasonEmpty = false;
    this.todayDate = new Date();

    // if (this.isCheckerLogin === 'true') {
    //   this.SuspensionDetailsForm.disable();

    // }

  }

  protected filterDesig() {

    if (!this.desigList) {
      return;
    }
    // get the search keyword
    let search = this.desigfilterddl.value;
    if (!search) {
      this.filteredDesig.next(this.desigList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the designs
    // this.filteredDesig.next(
    //   this.desig.filter(desg => {
    //     const showdesigDesc = desg.desigDesc.toLowerCase().indexOf(search) > -1;
    //     if (!showdesigDesc) {
    //       desg.desigDesc = desg.desigDesc.filter(desgn => desgn.name.toLowerCase().indexOf(search) > -1);
    //     }
    //     return desg.desigDesc.length > 0;
    //   })
    // );

    this.filteredDesig.next(
      this.desigList.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  // Search functionality
  // private filtercontroller()
  // {
  //   if (!this.controllerroleList)
  //   {
  //     return;
  //   }
  //   // get the search keyword
  //   let search = this.controllerFilterCtrl.value;
  //   if (!search)
  //   {
  //     this.filteredcontroller.next(this.controllerroleList.slice());
  //     return;
  //   }
  //   else {
  //     search = search.toLowerCase();
  //   }
  //   // filter the banks
  //   this.filteredcontroller.next(
  //     this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
  //   );
  // }

  GetAllEmployees(desigId: string) {
    console.log(desigId);
    if (this.allempList != null && this.allempList.length > 0) {
      this.objempModel = this.allempList.filter(x => x.empCd.split('@')[1] == desigId);
    }
    console.log(this.objempModel);
    const comName = 'suspen';
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice
      .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
      .subscribe(res => {
        this.objempModel = res;
        if (Number(desigId) === 0) { this.allempList = res; }
      });
  }

  GetAllDesignation() {

    this.objsuspensionservice.GetAllDesignation('').subscribe(res => {
      this.desigList = res;
      this.filteredDesig.next(this.desigList);
    });
  }

  // GetPayBillGroup() {
  //   this.objsuspensionservice.GetPayBillGroup().subscribe(res => {
  //     this.objempdesig = res;
  //   });
  // }

  CreateSuspensionDetailsForm() {
    this.SuspensionDetailsForm = this.fb.group({
      FromDate: ['', Validators.required],
      OrderNo: ['', Validators.required],
      SusPeriod: ['', [Validators.required, Validators.min(1), Validators.max(120)]],
      OrderDate: ['', Validators.required],
      SalaryPercent: ['', [Validators.required, Validators.max(75), Validators.min(50), Validators.maxLength(2)]],
      Remarks: ['', Validators.required],
      RejectedReason: ['', '']
    });
  }

  ddlDesignationChanged(desigId: string) {
    this.selectedEmpValue = null;
    this.desigddl.placeholder = null;
    if (Number(desigId) === -1) {
      this.selectedvalue = null;
      this.objempModel = this.allempList;
      this.desigddl.placeholder = 'Select Designation';
      return true;
    }

    this.GetAllEmployees(desigId);
  }

  ShowSuspensionDetails(isShow: boolean): void {
    this.ShowSusDetails = isShow;
  }

  AutoFillDate(daysAndDate: any, isdate: boolean) {
    let fdate: Date;
    if (isdate) {
      fdate = new Date(daysAndDate);
      fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
    } else {
      this.susPeriodInNumber = daysAndDate;
      fdate = new Date(this.objempSusDetails.FromDate);
      fdate.setDate(fdate.getDate() + +this.susPeriodInNumber);
    }
    this.validUptoDate = fdate.toDateString();
  }
  // ddlPayBillGroupChanged(payBillGroupId: string) {
  //   this.objsuspensionservice
  //     .GetAllDesignationOnSelectedPayBillGroup(payBillGroupId)
  //     .subscribe(res => {
  //       this.objempDesignation = res;
  //     });
  // }
  ddlEmployeeChanged(empId: string) {

    this.selectedEmpCode = empId.split('@')[0].trim();
    this.selectedvalue = Number(empId.split('@')[1]);
    this.isViewDetails = true;
    this.SuspensionDetailsForm.get('FromDate').enable();
    this.orderValidFlag = false;
    this.BindSuspensionDetails(this.selectedEmpCode);
    this.Reset();
    if (this.isCheckerLogin) {
      this._btnText = 'Accept';
    }
  }

  BindSuspensionDetails(empId: string) {
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    const comName = 'suspen';
    this.objsuspensionservice.GetSuspensionDetails(empId, checkerLogin, comName).subscribe((result: any) => {
      console.log(result);
      this.dataSource = new MatTableDataSource<SuspensionDetails>(result.filter(obj => obj.status !== 'Verified'));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.t1Sort;

      this.exPanel.disabled = false;
      if (this.dataSource.data.length > 0) {
        this.exPanel.close();
        this.exPanel.disabled = true;
      }

      this.dataSourceHistory = new MatTableDataSource<SuspensionDetails>(result.filter(obj => obj.status === 'Verified'));
      this.dataSourceHistory.paginator = this.paginatorHistory;
      this.dataSourceHistory.sort = this.t2Sort;

    });
  }

  GetSuspensionDetails() {
    alert('Go');
  }

  chkTillFurtherOrder(chk: any) {
    // this.orderValidFlag = chk.checked ? true : false;
    const toDate = this.SuspensionDetailsForm.get('ToDate');

    if (chk.checked) {
      toDate.disable();
      this.objempSusDetails.ToDate = null;
      toDate.setValidators(null);
    } else {
      toDate.enable();
      toDate.setValidators(Validators.required);
    }
    toDate.updateValueAndValidity();
  }

  SavedSuspensionDetails(objsus: SuspensionDetails): void {
    // tslint:disable-next-line:no-debugger
    debugger;
    this.disableButton = true;
    objsus.PermddoId = '00003';
    objsus.EmpCode = this.selectedEmpCode;
    console.log(this.selectedEmpCode);
    // if (objsus.IsTillOrder == null) { objsus.IsTillOrder = false; }
    if (objsus.Status == null) { objsus.Status = 'I'; }
    // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
    // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
    switch (objsus.Status) {
      case 'F':
        this.successMsg = 'Suspension Details Forwarded Successfully';
        break;
      case 'R':
        this.successMsg = 'Suspension Details Returned Successfully';
        break;
      default:
        this.successMsg = 'Suspension Details saved successfully';
        break;
    }
    this.objsuspensionservice.SavedNewSuspensionDetails(objsus).subscribe((arg: any) => {

      if (+arg > 0) {
        this.myformm.resetForm();
        this.objempSusDetails.Id = 0;
        this.objempSusDetails.Status = null;
        this._btnText = 'Save';
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.disableButton = false;
      } else {
        this.successMsg = arg;
      }
      this.snackBar.open(this.successMsg, null, { duration: 4000 });

    });
  }

  ForwardToChecker(obj: SuspensionDetails): void {
    obj.Status = 'F';
    this.SavedSuspensionDetails(obj);
  }

  VarifiedSus(id: number, flag: boolean): void {
    if (flag && this.objempSusDetails.Id > 0) {
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_suspen', 'V', '').subscribe((arg: any) => {

        if (+arg > 0) {
          this.successMsg = 'Varified successfully';
        }
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      this.myformm.resetForm();
      this.objempSusDetails.Id = 0;
      this.objempSusDetails.Status = null;
      this.validUptoDate = '';
      this.SuspensionDetailsForm.get('FromDate').enable();
      // $('.dialog__close-btn').click();
      this.hooCheckerLogin = false;
    } else {
      this.isVarified = true;
    }
  }

  RejectedSus(id: number, flag: boolean): boolean {

    if (flag && this.objempSusDetails.Id > 0) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        this.successMsg = 'Please enter reason';
        return false;
      }

      if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
        this.isReasonEmpty = true;
        this.successMsg = 'Special characters are not allowed';
        return false;
      }
      this.successMsg = null;

      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_suspen', 'R', this.reasonText).subscribe((arg: any) => {
        if (+arg > 0) {
          this.successMsg = 'Rejected successfully';
        }
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
        return false;
      });
      this.myformm.resetForm();
      this.objempSusDetails.Id = 0;
      this.objempSusDetails.Status = null;
      this.validUptoDate = '';
      this.SuspensionDetailsForm.get('FromDate').enable();
      // $('.dialog__close-btn').click();
      this.hooCheckerLogin = false;
    } else {
      this.isVarified = false;
      return false;
    }
  }

  // btnEditClick(id: number, fromDate: Date, toDate: number, orderNo: string, orderDate: Date, salaryPercent: number, remarks: string) {
  //   this.objempSusDetails.Id = id;
  //   this.objempSusDetails.FromDate = fromDate;
  //   this.objempSusDetails.ToDate = toDate;
  //   this.objempSusDetails.OrderNo = orderNo;
  //   this.objempSusDetails.OrderDate = orderDate;
  //   this.objempSusDetails.Remarks = remarks;
  //   this.objempSusDetails.SalaryPercent = salaryPercent;

  //   }



  btnEditClick(objsus: any, isViewOnly: boolean) {
    debugger;

    this.exPanel.disabled = false;
    this.exPanel.open();

    this.objempSusDetails.Id = objsus.id;
    this.objempSusDetails.FromDate = objsus.fromDate;
    this.objempSusDetails.OrderDate = objsus.orderDate;
    this.objempSusDetails.OrderNo = objsus.orderNo;
    this.objempSusDetails.ToDate = objsus.toDate;
    this.objempSusDetails.SalaryPercent = objsus.salaryPercent;
    this.objempSusDetails.Remarks = objsus.remarks;
    this.objempSusDetails.SusPeriod = objsus.susPeriod;
    this._btnText = 'Save';
    this._Id = objsus.id;
    this.isRejected = false;
    if (objsus.rejectedReason !== '') {
      this.objempSusDetails.RejectedReason = objsus.rejectedReason;
      this.isRejected = true;
    }
    this.SuspensionDetailsForm.enable();
    // const toDate = this.SuspensionDetailsForm.get('ToDate');
    // if (objsus.isTillOrder) {
    //   this.objempSusDetails.ToDate = null;
    //   toDate.disable();
    //   toDate.setValidators(null);
    // } else {
    //   toDate.enable();
    //   toDate.setValidators(Validators.required);
    // }
    // toDate.updateValueAndValidity();
    const fdate = new Date(objsus.fromDate);
    fdate.setDate(fdate.getDate() + +objsus.susPeriod);
    this.validUptoDate = fdate.toDateString();
    this.isViewDetails = true;
    if (this.isCheckerLogin) {
      this._btnText = 'Accept';
      this.SuspensionDetailsForm.disable();
    }

    if (isViewOnly) {
      this.SuspensionDetailsForm.disable();
      this.isViewDetails = false;
    }
  }

  DeleteSuspensionDetails(id: number, flag: boolean) {
    this.isViewDetails = true;
    this._Id = flag ? id : 0;
    if (!flag) {
      this.objsuspensionservice.DeleteSuspensionDetails(id, 'suspen').subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindSuspensionDetails(this.selectedEmpCode);
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      this.objempSusDetails = new SuspensionDetails();
      this.validUptoDate = '';
    }
    // $('.dialog__close-btn').click();
    this.deletepopup = false;
  }

  applyFilter(filterValue: string) {
    if (this.isCheckerLogin) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    } else {
      this.dataSourceHistory.filter = filterValue.trim().toLowerCase();
      if (this.dataSourceHistory.paginator) {
        this.dataSourceHistory.paginator.firstPage();
      }
    }

  }

  Reset() {
    this._btnText = 'Save';
    this.objempSusDetails = new SuspensionDetails();
    // this.SuspensionDetailsForm.reset();
    //    this.SuspensionDetailsForm.reset({
    // });


  }

}

