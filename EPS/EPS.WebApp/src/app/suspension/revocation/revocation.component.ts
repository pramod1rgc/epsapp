import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SuspensionService } from '../../services/Suspension/Suspension_service';
import { MatSnackBar, MatPaginator, MatSelect, MatExpansionPanel } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DesignationModel, EmpModel } from '../../model/Shared/DDLCommon';
import { RevocationDetails } from '../../model/Suspension/RevocationDetails';


@Component({
  selector: 'app-revocation',
  templateUrl: './revocation.component.html',
  styleUrls: ['./revocation.component.css']
})

export class RevocationComponent implements OnInit {

  objempDesignation: DesignationModel;
  objempModel: EmpModel;
  isCheckerLogin = false;
  selectedvalue = 0;
  isRejected = false;
  disableButton = false;
  isViewDetails = true;
  revId: number;
  selectedEmpSusId: string;
  empId: number;
  objempRevokDetails: RevocationDetails;
  successMsg: string;
  reasonText: string;
  deletepopup: boolean;
  isReasonEmpty: boolean;
  hooCheckerPopup: boolean;
  isVarified: boolean;
  RevocationDetailsForm: FormGroup;
  checkerbtnDisable: boolean;
  dataRevocationHistory: MatTableDataSource<RevocationDetails>;

  dataRevocation: MatTableDataSource<RevocationDetails>;
  revokHistoryColumns: string[] = ['OrderNo', 'OrderDate', 'RevocationDate', 'Remarks', 'Status'];
  @ViewChild('revHSort') revHSort: MatSort;
  @ViewChild('pagingRevokH') pagingRevokH: MatPaginator;

  revokColumns: string[] = ['OrderNo', 'OrderDate', 'RevocationDate', 'Remarks', 'Status', 'Action'];
  @ViewChild('revSort') revSort: MatSort;
  @ViewChild('pagingRevok') pagingRevok: MatPaginator;

  @ViewChild('formCreation') myform: any;
  @ViewChild('singleSelect') desigddl: MatSelect;
  @ViewChild('revPanel') exPanel: MatExpansionPanel;
  todayDate: Date;

  ngOnInit() {
    this.objempRevokDetails = new RevocationDetails();
    if (+sessionStorage.getItem('userRoleID') === 9) {
      this.isCheckerLogin = false;
    } else {
      this.isCheckerLogin = true;
    }
    this.checkerbtnDisable = true;
    console.log(this.isCheckerLogin);
    this.GetAllDesignation();
    this.GetAllEmployees('0');
    this.reasonText = '';
    this.isReasonEmpty = false;
    this.isRejected = false;
    this.todayDate = new Date();
  }

  constructor(
    private objsuspensionservice: SuspensionService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    // this.CreateSuspensionDetailsForm();
  }

  ddlDesignationChanged(desigId: string) {
    this.desigddl.placeholder = null;
    if (Number(desigId) === -1) {
      this.selectedvalue = null;
      //  this.objempModel = this.allempList;
      this.desigddl.placeholder = 'Select Designation';
      desigId = '0';
    }
    this.GetAllEmployees(desigId);
  }

  GetAllDesignation() {
    this.objsuspensionservice
      .GetAllDesignation('')
      .subscribe(res => {
        this.objempDesignation = res;
      });
  }

  GetAllEmployees(desigId: string) {
    const comName = 'revok';
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice
      .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
      .subscribe(res => {
        this.objempModel = res;
      });
  }

  ddlEmployeeChanged(empId: string) {
    debugger;
    this.myform.reset();
    this.checkerbtnDisable = true;
    this.selectedEmpSusId = empId.split('@')[0].trim();
    this.selectedvalue = Number(empId.split('@')[1]);
    this.empId = Number(empId.split('@')[2]);
    this.BindRevocationDetails(Number(this.selectedEmpSusId));

  }

  BindRevocationDetails(selectedvalue: number) {
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice.GetRevocationDetails(selectedvalue, this.empId, checkerLogin).subscribe((result: any) => {
      console.log(result);
      this.dataRevocation = new MatTableDataSource<RevocationDetails>(result.filter(obj => obj.flagStatus.trim() !== 'V'));
      this.dataRevocation.paginator = this.pagingRevok;
      this.dataRevocation.sort = this.revSort;

      this.exPanel.disabled = false;
      if (this.dataRevocation.data.length > 0) {
        this.exPanel.close();
        this.exPanel.disabled = true;
      }

      this.dataRevocationHistory = new MatTableDataSource<RevocationDetails>(result.filter(obj => obj.flagStatus.trim() === 'V'));
      this.dataRevocationHistory.paginator = this.pagingRevokH;
      this.dataRevocationHistory.sort = this.revHSort;

    });


  }


  SavedRevocationDetails(objRevok: RevocationDetails): void {
    debugger;
    this.disableButton = true;
    objRevok.EmpTransSusId = Number(this.selectedEmpSusId);
    objRevok.EmpRevocId = this.revId;
    // console.log(objRevok.EmpTransSusId);
    // console.log(objRevok.RevocOrderDate);
    // console.log(objRevok.RevocationDate);
    // console.log(objRevok.RevocOrderNo);
    // console.log(objRevok.Remarks);
    // tslint:disable-next-line:no-debugger

    if (objRevok.FlagStatus === void 0) { objRevok.FlagStatus = 'RE'; }
    // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
    // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
    switch (objRevok.FlagStatus) {
      case 'F':
        this.successMsg = 'Revocation Details Forwarded Successfully';
        break;
      case 'R':
        this.successMsg = 'Revocation Details Returned Successfully';
        break;
      default:
        this.successMsg = 'Revocation Details saved successfully';
        break;
    }

    this.objsuspensionservice.SavedRevocationDetails(objRevok).subscribe((arg: any) => {


      if (+arg > 0) {
        this.revId = 0;
        this.BindRevocationDetails(Number(this.selectedEmpSusId));
        this.disableButton = false;
        this.myform.reset();
      } else {
        this.successMsg = arg;
      }


      // this.objempExtDetails.Id = 0;
      // this.objempExtDetails.Status = null;
      // this.BindSuspensionDetails(this.selectedEmpCode);
      this.snackBar.open(this.successMsg, null, { duration: 4000 });
    });
  }

  ForwardToChecker(obj: RevocationDetails): void {
    obj.FlagStatus = 'F';
    this.SavedRevocationDetails(obj);
  }

  DeleteRevokDetails(id: number, flag: boolean) {
    debugger;

    this.revId = flag ? id : 0;
    if (!flag) {
      this.objsuspensionservice.DeleteSuspensionDetails(id, 'revok').subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindRevocationDetails(Number(this.selectedEmpSusId));
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }
    // $('.dialog__close-btn').click();
    this.deletepopup = flag;
  }

  applyFilter(filterValue: string) {
    if (this.isCheckerLogin) {
      this.dataRevocation.filter = filterValue.trim().toLowerCase();
      if (this.dataRevocation.paginator) {
        this.dataRevocation.paginator.firstPage();
      }
    } else {
      this.dataRevocationHistory.filter = filterValue.trim().toLowerCase();
      if (this.dataRevocationHistory.paginator) {
        this.dataRevocationHistory.paginator.firstPage();
      }
    }

  }


  btnEditClick(objRevok: any, isViewOnly: boolean) {
    debugger;
    this.exPanel.disabled = false;
    this.exPanel.open();
    this.objempRevokDetails.EmpRevocId = objRevok.empRevocId;
    this.objempRevokDetails.RevocOrderNo = objRevok.revocOrderNo;
    this.objempRevokDetails.RevocOrderDate = objRevok.revocOrderDate;
    this.objempRevokDetails.RevocationDate = objRevok.revocationDate;
    this.objempRevokDetails.FlagStatus = objRevok.flagStatus;
    this.objempRevokDetails.Remarks = objRevok.remarks;
    this.revId = objRevok.empRevocId;

    this.isRejected = false;
    if (objRevok.rejectedReason !== '') {
      this.objempRevokDetails.RejectedReason = objRevok.rejectedReason;
      this.isRejected = true;
    }

    this.isViewDetails = !isViewOnly;

    if (this.isCheckerLogin) {
      this.checkerbtnDisable = false;
    }
    // if (this.isCheckerLogin) {
    //  // this.myform.form.disable();
    //  this.myform.controls.forEach(function(element) {
    //    element.disable();
    //  });
    // }


    console.log(this.myform.controls);
    console.log(this.myform.form.controls);

    if (isViewOnly) {
      this.myform.form.disabled = true;
    }
  }

  VarifiedRevok(id: number, flag: boolean): void {

    console.log(id);

    if (!flag && id > 0) {
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_revok', 'V', '').subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Varified successfully';
          this.revId = 0;
          this.BindRevocationDetails(Number(this.selectedEmpSusId));
          this.myform.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }

    this.hooCheckerPopup = flag;
    this.isVarified = flag;
  }

  RejectedRevok(id: number, flag: boolean): boolean {
    debugger;
    console.log(id);
    this.hooCheckerPopup = flag;
    if (!flag && id > 0) {
      this.isReasonEmpty = false;

      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        this.successMsg = 'Please enter reason';
        return false;
      }

      if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
        this.isReasonEmpty = true;
        this.successMsg = 'Special characters are not allowed';
        return false;
      }
      this.successMsg = null;

      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_revok', 'R', this.reasonText).subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Rejected successfully';
          this.revId = 0;
          this.BindRevocationDetails(Number(this.selectedEmpSusId));
          this.myform.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      return false;
    }
    this.isVarified = !flag;
    return false;
  }

  Reset() {
    this.myform.reset();
  }

}

