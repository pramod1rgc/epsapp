import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSelect, MatExpansionPanel } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DesignationModel, EmpModel } from '../../model/Shared/DDLCommon';
import { JoiningDetails } from '../../model/Suspension/JoiningDetails';
import { SuspensionService } from '../../services/Suspension/Suspension_service';


@Component({
  selector: 'app-joining',
  templateUrl: './joining.component.html',
  styleUrls: ['./joining.component.css']
})

export class JoiningComponent implements OnInit {

  objempDesignation: DesignationModel;
  objempModel: EmpModel;
  isCheckerLogin = false;
  selectedvalue = 0;
  isRejected = false;
  isViewDetails = true;
  disableButton = false;
  joiningId: number;
  selectedEmpSusId: string;
  empId: number;
  objempJoiningDetails: JoiningDetails;
  successMsg: string;
  reasonText: string;
  deletepopup: boolean;
  isReasonEmpty: boolean;
  hooCheckerPopup: boolean;
  isVarified: boolean;
  checkerbtnDisable: boolean;
  JoiningDetailsForm: FormGroup;
  @ViewChild('formCreation') formJoining: any;
  dataJoiningHistory: MatTableDataSource<JoiningDetails>;
  dataJoining: MatTableDataSource<JoiningDetails>;

  joiningHistoryColumns: string[] = ['OrderNo', 'OrderDate', 'JoiningDate', 'Fn/An', 'Status'];
  @ViewChild('sortingJH') sortingJH: MatSort;
  @ViewChild('pagingJH') pagingJH: MatPaginator;

  joiningColumns: string[] = ['OrderNo', 'OrderDate', 'JoiningDate', 'Fn/An', 'Status', 'Action'];
  @ViewChild('sortingJoining') sortingJoining: MatSort;
  @ViewChild('pagingJoining') pagingJoining: MatPaginator;
  @ViewChild('singleSelect') desigddl: MatSelect;
  @ViewChild('exPanel') exPanel: MatExpansionPanel;


  ngOnInit() {
    this.objempJoiningDetails = new JoiningDetails();
    if (+sessionStorage.getItem('userRoleID') === 9) {
      this.isCheckerLogin = false;
    } else {
      this.isCheckerLogin = true;
    }
    this.checkerbtnDisable = true;
    console.log(this.isCheckerLogin);
    this.GetAllDesignation();
    this.GetAllEmployees('0');
    this.reasonText = '';
    this.isReasonEmpty = false;
    this.isRejected = false;

  }

  constructor(
    private objsuspensionservice: SuspensionService, private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.CreateJoiningDetailsForm();
  }


  CreateJoiningDetailsForm() {
    this.JoiningDetailsForm = this.fb.group({
      JoiningDate: ['', Validators.required],
      FnAn: ['', Validators.required],
      OrderNo: ['', Validators.required],
      OrderDate: ['', Validators.required],
      RejectedReason: ['', '']
    });
  }

  GetAllDesignation() {
    this.objsuspensionservice
      .GetAllDesignation('')
      .subscribe(res => {
        this.objempDesignation = res;
      });
  }

  GetAllEmployees(desigId: string) {
    const comName = 'joining';
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice
      .GetAllEmpOnSelectedDesignation(desigId, checkerLogin, comName)
      .subscribe(res => {
        this.objempModel = res;
      });
  }

  ddlDesignationChanged(desigId: string) {
    debugger;
    this.desigddl.placeholder = null;
    if (Number(desigId) === -1) {
      this.selectedvalue = null;
      //  this.objempModel = this.allempList;
      this.desigddl.placeholder = 'Select Designation';
      desigId = '0';
    }
    this.GetAllEmployees(desigId);
  }

  ddlEmployeeChanged(empId: string) {
    debugger;
    //this.myform.reset();
    this.JoiningDetailsForm.enable();
    this.JoiningDetailsForm.reset();
    this.isViewDetails = true;
    this.checkerbtnDisable = true;
    this.selectedEmpSusId = empId.split('@')[0].trim();
    this.selectedvalue = Number(empId.split('@')[1]);
    this.empId = Number(empId.split('@')[2]);
    console.log(this.selectedEmpSusId);
    this.BindJoiningDetails(Number(this.selectedEmpSusId));

  }

  BindJoiningDetails(selectedvalue: number) {
    const checkerLogin = this.isCheckerLogin ? 'true' : 'false';
    this.objsuspensionservice.GetJoiningDetails(this.empId, checkerLogin).subscribe((result: any) => {
      console.log(result);
      this.dataJoining = new MatTableDataSource<JoiningDetails>(result.filter(obj => obj.flagStatus.trim() !== 'V'));
      this.dataJoining.paginator = this.pagingJoining;
      this.dataJoining.sort = this.sortingJoining;

      this.exPanel.disabled = false;
      if (this.dataJoining.data.length > 0) {
        this.exPanel.close();
        this.exPanel.disabled = true;
      }

      this.dataJoiningHistory = new MatTableDataSource<JoiningDetails>(result.filter(obj => obj.flagStatus.trim() === 'V'));
      this.dataJoiningHistory.paginator = this.pagingJH;
      this.dataJoiningHistory.sort = this.sortingJH;
    });
  }

  SavedJoiningDetails(objJoining: JoiningDetails): void {
    debugger;
    console.log(objJoining);
    this.disableButton = true;
    objJoining.EmpRevokId = Number(this.selectedEmpSusId);
    objJoining.EmpJoiningId = this.joiningId;
    //this.formJoining.reset();
    //return true;
    if (objJoining.FlagStatus === void 0) { objJoining.FlagStatus = 'JN'; }
    // objsus.FromDate = new Date(objsus.FromDate + 'UTC');
    // objsus.OrderDate = new Date(objsus.OrderDate + 'UTC');
    this.objsuspensionservice.SavedJoiningDetails(objJoining).subscribe((arg: any) => {

      this.successMsg = arg;
      if (+arg > 0) {
        this.successMsg = 'Record saved successfully';
        this.joiningId = 0;
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
        this.disableButton = false;
        this.JoiningDetailsForm.reset();
      }


      // this.objempExtDetails.Id = 0;
      // this.objempExtDetails.Status = null;
      // this.BindSuspensionDetails(this.selectedEmpCode);
      this.snackBar.open(this.successMsg, null, { duration: 4000 });
    });

  }

  applyFilter(filterValue: string) {
    if (this.isCheckerLogin) {
      this.dataJoining.filter = filterValue.trim().toLowerCase();
      if (this.dataJoining.paginator) {
        this.dataJoining.paginator.firstPage();
      }
    } else {
      this.dataJoiningHistory.filter = filterValue.trim().toLowerCase();
      if (this.dataJoiningHistory.paginator) {
        this.dataJoiningHistory.paginator.firstPage();
      }
    }

  }

  btnEditClick(objRevok: any, isViewOnly: boolean) {
    debugger;
    this.exPanel.disabled = false;
    this.exPanel.open();
    this.objempJoiningDetails.EmpJoiningId = objRevok.empJoiningId;
    this.objempJoiningDetails.OrderDate = objRevok.orderDate;
    this.objempJoiningDetails.JoiningDate = objRevok.joiningDate;
    this.objempJoiningDetails.OrderNo = objRevok.orderNo;
    this.objempJoiningDetails.FlagStatus = objRevok.flagStatus;
    this.objempJoiningDetails.FnAn = objRevok.fnAn;
    this.joiningId = objRevok.empJoiningId;
    console.log(objRevok.empJoiningId);
    this.isRejected = false;
    if (objRevok.rejectedReason !== '') {
      this.objempJoiningDetails.RejectedReason = objRevok.rejectedReason;
      this.isRejected = true;
    }
    this.JoiningDetailsForm.enable();
    this.isViewDetails = !isViewOnly;

    if (this.isCheckerLogin) {
      this.JoiningDetailsForm.disable();
    }

    // if (this.isCheckerLogin) {
    // this.myform.form.disable();
    //  this.myform.controls.forEach(function(element) {
    //    element.disable();
    //  });
    // }


    if (isViewOnly) {
      this.JoiningDetailsForm.disable();
      this.isViewDetails = false;
    }
  }

  DeleteJoiningDetails(id: number, flag: boolean) {
    debugger;

    this.joiningId = flag ? id : 0;
    if (!flag) {
      this.objsuspensionservice.DeleteSuspensionDetails(id, 'joining').subscribe((arg: any) => {
        this.successMsg = arg;
        this.BindJoiningDetails(Number(this.selectedEmpSusId));
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }
    // $('.dialog__close-btn').click();
    this.deletepopup = flag;
  }

  ForwardToChecker(obj: JoiningDetails): void {
    obj.FlagStatus = 'F';
    this.SavedJoiningDetails(obj);
  }

  VarifiedJoining(id: number, flag: boolean): void {

    console.log(id);

    if (!flag && id > 0) {
      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_joining', 'V', '').subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Varified successfully';
          this.joiningId = 0;
          this.BindJoiningDetails(Number(this.selectedEmpSusId));
          this.JoiningDetailsForm.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
    }

    this.hooCheckerPopup = flag;
    this.isVarified = flag;
  }

  RejectedJoining(id: number, flag: boolean): boolean {
    debugger;
    console.log(id);
    this.hooCheckerPopup = flag;
    if (!flag && id > 0) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        this.successMsg = 'Please enter reason';
        return false;
      }

      if (/^[a-zA-Z0-9- /,_]*$/.test(this.reasonText.trim()) === false) {
        this.isReasonEmpty = true;
        this.successMsg = 'Special characters are not allowed';
        return false;
      }
      this.successMsg = null;

      this.objsuspensionservice.VerifiedAndRejectedSusDetails(id.toString() + '_joining', 'R', this.reasonText).subscribe((arg: any) => {
        this.successMsg = arg;
        if (+arg > 0) {
          this.successMsg = 'Rejected successfully';
          this.joiningId = 0;
          this.BindJoiningDetails(Number(this.selectedEmpSusId));
          this.JoiningDetailsForm.reset();
        }
        this.snackBar.open(this.successMsg, null, { duration: 4000 });
      });
      return false;
    }
    this.isVarified = !flag;
    return false;
  }

}

