import { BrowserModule } from '@angular/platform-browser';
import { AuthguardGuard } from './authguard/authguard.guard';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { CdkTreeModule } from '@angular/cdk/tree';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { LoginComponent } from './login/login.component';
import { LogintokenComponent } from './logintoken/logintoken.component';
import { GlobalModule } from './global/global.module';
import { ErrorInterceptor } from './global/error.interceptor';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { SharedModule } from './shared-module/shared-module.module';
import { LoginActivateComponent } from './login-activate/login-activate.component';
import { LoginServices } from './login/loginservice';
import { NewOnboardingComponent } from './new-onboarding/new-onboarding.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { httpInterceptor } from './logintoken/httpInterceptor';
import { NewOnBoardingStatusComponent } from './new-on-boarding-status/new-on-boarding-status.component';

 
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogintokenComponent,
    ChangepasswordComponent,
    LoginActivateComponent,
    NewOnboardingComponent,
    NewOnBoardingStatusComponent,
       
    
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    NgxMatSelectSearchModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    CdkTreeModule,
    ChartsModule,
    GlobalModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    RouterModule.forRoot([
      {path:'',redirectTo:'login',pathMatch:'full'},
      //{ path: 'login', component: LoginComponent },
      { path: 'login', component: LogintokenComponent },
      { path: 'NewOnboarding', component: NewOnboardingComponent },
      { path: 'NewOnBoardingStatus', component: NewOnBoardingStatusComponent },
      { path: 'changepassword', component: ChangepasswordComponent },
      { path: 'activate', component: LoginActivateComponent },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthguardGuard] },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
            
    ],)
  ],

  providers: [
    { provide: HashLocationStrategy, useClass: LocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: httpInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    [AuthguardGuard, LoginServices],
   
  ],
 
  bootstrap: [AppComponent]
})
export class AppModule { }
