import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesanddeductionsemployeewiseComponent } from './duesanddeductionsemployeewise.component';

describe('DuesanddeductionsemployeewiseComponent', () => {
  let component: DuesanddeductionsemployeewiseComponent;
  let fixture: ComponentFixture<DuesanddeductionsemployeewiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesanddeductionsemployeewiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesanddeductionsemployeewiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
