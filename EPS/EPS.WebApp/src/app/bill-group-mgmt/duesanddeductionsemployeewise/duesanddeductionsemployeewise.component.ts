
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-duesanddeductionsemployeewise',
  templateUrl: './duesanddeductionsemployeewise.component.html',
  styleUrls: ['./duesanddeductionsemployeewise.component.css']
})
export class DuesanddeductionsemployeewiseComponent implements OnInit {

  displayedColumns: string[] = [ 'name', 'weight', 'symbol', 'remark', 'periodicity', 'month', ];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  remark: string;

  weight: number;
  symbol: string;
  periodicity: string;
  month: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
  { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
  { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },
  { name: 'Dress Allowance', remark: 'Dress Allowance', weight: 1.0079, symbol: 'H', periodicity: 'H', month: 'H' },

];
