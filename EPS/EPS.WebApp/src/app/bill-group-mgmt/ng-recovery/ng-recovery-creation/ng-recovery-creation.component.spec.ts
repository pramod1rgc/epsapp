import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgRecoveryCreationComponent } from './ng-recovery-creation.component';

describe('NgRecoveryCreationComponent', () => {
  let component: NgRecoveryCreationComponent;
  let fixture: ComponentFixture<NgRecoveryCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgRecoveryCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgRecoveryCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
