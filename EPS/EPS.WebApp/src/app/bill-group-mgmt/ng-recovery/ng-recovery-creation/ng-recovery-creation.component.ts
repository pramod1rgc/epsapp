import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
//import * as $ from 'jquery';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { NgRecovery } from '../../../model/PayBillGroup/NgRecovery';
import { debug } from 'util';
//import { CommonMsg } from 'src/app/global/common-msg';
import { CommonMsg } from '../../../global/common-msg';


@Component({
  selector: 'app-ng-recovery-creation',
  templateUrl: './ng-recovery-creation.component.html',
  styleUrls: ['./ng-recovery-creation.component.css']
})
export class NgRecoveryCreationComponent implements OnInit {
  btnclose() {
    this.is_btnStatus = false;
  }
  is_btnStatus = true;


  showBankDetails: boolean = true;

  deactivatePopup: boolean;
  activatePopup: boolean;
  //applyFilter: any;
  editMode: boolean = false;
  //getNgrecoveryData: any;
  ngRecoveryCreationForm: FormGroup;
  _btnText: string;
  displayedColumns: string[] = ['S.No', 'NGR Code', 'PFMS  Unique Id', 'NG Recovery Description', 'IFSC Code', 'Bank A/C No.', 'Vendor Status', 'Action'];
  highlightedRows = [];
  getBankDetails: any = [];
  ngRecoveryCreation: NgRecovery = {
    payNgRecoId: 0,
    ngDesc: null,
    ngDedCd: null,
    address: null,
    isBankDetails: "Yes",
    ifscCd: null,
    bankId: null,
    bankName: null,
    branchId: null,
    branchName: null,
    bnfName: null,
    bankAcNo: null,
    isActive: null,
    permDDOId: null,
    pfmsUniqueCode: null,
    venderStatus: null,

  }
  requiredvalidation: boolean;
  dataSource: any;

  allData: any[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('creationForm') form: any;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _Service: PayBillGroupService, private fb: FormBuilder, private _msg: CommonMsg
    , private snackBar: MatSnackBar,
  ) { this.createForm() }

  ngOnInit() {
    this.requiredvalidation = false;
    this.getNgrecovery('00003');
    this._btnText = 'Save';
  }

  createForm() {
    this.ngRecoveryCreationForm = this.fb.group({
      //ngDesc: ['', [Validators.required, Validators.pattern('[-(),./a-zA-Z ]*')]],
      //address: ['', [Validators.required, Validators.pattern('[-(),./a-zA-Z ]*')]],
      ngDesc: ['', [Validators.required]],
      address: ['', [Validators.required]],
      isBankDetails: ['Yes', Validators.required],
      ifscCd: ['', Validators.required],
      bankId: ['', ''],
      bankName: ['', Validators.required],
      branchId: ['', ''],
      branchName: ['', Validators.required],
      bnfName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
      bankAcNo: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(17), Validators.pattern('^[0-9]*$')]],
      payNgRecoId: [0],
      permDDOId: [''],
      isActive: [false],
      ngDedCd: [0],


    });
    console.log(this.ngRecoveryCreationForm);
  }
  getNgrecovery(permDDOId: any) {

    this._Service.GetNgRecovery(permDDOId).subscribe(data => {
      //this.getNgrecoveryData = data;
      this.allData = data;
      this.dataSource = new MatTableDataSource(data);
      console.log(this.dataSource);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  checkBankDetails(value: any) {
    this.showBankDetails = value == "No" ? false : true;
    if (value == "No") {
      debugger;
      //alert('No');
      //this.ngRecoveryCreationForm.get('ifscCd').setValidators([Validators.required]);
      // this.ngRecoveryCreationForm.controls['ifscCd'].setValidators(null);
      //================= by manoj pal================================
      this.ngRecoveryCreationForm.controls['ifscCd'].setErrors(null);
      this.ngRecoveryCreationForm.controls['bankName'].setErrors(null);
      this.ngRecoveryCreationForm.controls['branchName'].setErrors(null);
      this.ngRecoveryCreationForm.controls['bnfName'].setErrors(null);
      this.ngRecoveryCreationForm.controls['bankAcNo'].setErrors(null);

      //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();
      //this.ngRecoveryCreationForm.get("ifscCd").updateValueAndValidity();
      //this.ngRecoveryCreationForm.get("ifscCd").setValidators([Validators.required]);
    }
    if (value == "Yes") {
      this.ngRecoveryCreationForm.get('ifscCd').setValidators([Validators.required]);

      this.ngRecoveryCreationForm.get('bankName').setValidators([Validators.required]);
      this.ngRecoveryCreationForm.get('branchName').setValidators([Validators.required]);
      this.ngRecoveryCreationForm.get('bnfName').setValidators([Validators.required, Validators.pattern('[a-zA-Z ]*')]);
      this.ngRecoveryCreationForm.get('bankAcNo').setValidators([Validators.required, Validators.minLength(9), Validators.maxLength(17)]);
      //alert('Yes');
      //this.ngRecoveryCreationForm.get('ifscCd').clearValidators();
      //this.ngRecoveryCreationForm.get("ifscCd").updateValueAndValidity();

      //this.ngRecoveryCreationForm.get("ifscCd").setValidators([Validators.required]);
      //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();

      //this.ngRecoveryCreationForm.get("ifscCd").clearValidators();

    }
  }

  getBankdetailsByIfsc(IfscCode: any) {
    debugger;
    this.ngRecoveryCreation.bankName = null;
    this.ngRecoveryCreation.branchName = null;
    this._Service.GetBankdetailsByIfsc(IfscCode).subscribe(data => {
      this.getBankDetails = data;
      if (data.length == 0) {
        //this.ngRecoveryCreationForm.get('ifscCd')
        this.ngRecoveryCreationForm.get('ifscCd').setErrors({ 'incorrect': true });
      }
      else {
        this.ngRecoveryCreation.bankName = data[0].bankName;
        this.ngRecoveryCreation.bankId = data[0].bankId;
      }
      //console.log(data[0].schemeCode);
    });
  }
  onBranchSelection(branchId) {
    let selectedBranch = this.getBankDetails.filter(a => a.branchId == branchId);
    if (selectedBranch.length > 0) {
      this.ngRecoveryCreationForm.controls.branchName.setValue(selectedBranch[0].branchName);
    }
  }

  saveNgRecovery(): void {
    debugger;
    //ngRecoveryCreation.groupIds = '';
    //for (let entry of newBillGroup.group) {
    //  if (newBillGroup.groupIds == '') {
    //    newBillGroup.groupIds = entry;
    //  }
    //  else
    //    newBillGroup.groupIds = newBillGroup.groupIds + "," + entry;
    //}
    //newBillGroup.group = "";

    //console.log(newBillGroup);
    //newBillGroup.permDDOId = '00003';
    ////newBillGroup.payBillGroupId = 0;

    //this.ngRecoveryCreationForm.controls.payNgRecoId.setValue(ngRecoveryCreation.payNgRecoId)

    //ngRecoveryCreation.payNgRecoId = ngRecoveryCreation.payNgRecoId;
    //ngRecoveryCreation.ngDedCd = 0;
    //ngRecoveryCreation.permDDOId = '00003';
    //ngRecoveryCreation.isActive = false;
    //var abc = this.ngRecoveryCreationForm.controls.branchId.value;
    this.ngRecoveryCreationForm.controls.ngDedCd.setValue(0);
    this.ngRecoveryCreationForm.controls.permDDOId.setValue('00003');
    this.ngRecoveryCreationForm.controls.isActive.setValue(false);
    this.onBranchSelection(this.ngRecoveryCreationForm.controls.branchId.value);

    //console.log(this.ngRecoveryCreationForm);
    if (this.ngRecoveryCreationForm.valid) {


      this._Service.InsertUpdateNgRecovery(this.ngRecoveryCreationForm.value).subscribe(res => {
        //this.BindAllBillGroup('00003');
        this.getNgrecovery('00003')
        if (this.editMode) {
          this.snackBar.open('Updated successfully', null, { duration: 4000 });
        }
        else {

          this.snackBar.open('Saved successfully', null, { duration: 4000 });
        }
        this.editMode = false;
      }
      );
      this.createForm();
      this.form.resetForm();
      this._btnText = 'Save';
      this.highlightedRows.pop();
      this.form.resetForm();
      //this.ngOnInit();
    }



    //this.form.resetForm();
    //newBillGroup.billGroupName = '';
    //document.getElementById('btn_save').innerHTML = 'Save';
    //this.billGroupCreation.billForGAR = "false";
    //this.disableFlag = false;
  }



  btnEditClick(ngRecoveryCreation: NgRecovery) {
    debugger;
    this._btnText = 'Update';
    this.editMode = true;
    if (ngRecoveryCreation.ifscCd == "") {
      this.ngRecoveryCreationForm.controls.isBankDetails.setValue("No");
      this.checkBankDetails("No");
    }
    else {
      this.ngRecoveryCreationForm.controls.isBankDetails.setValue("Yes");
      this.getBankdetailsByIfsc(ngRecoveryCreation.ifscCd);
      this.ngRecoveryCreationForm.controls.branchId.setValue(ngRecoveryCreation.branchId);
      this.ngRecoveryCreationForm.controls.ifscCd.setValue(ngRecoveryCreation.ifscCd);
      this.ngRecoveryCreationForm.controls.bnfName.setValue(ngRecoveryCreation.bnfName);
      this.ngRecoveryCreationForm.controls.bankAcNo.setValue(ngRecoveryCreation.bankAcNo);
      //this.ngRecoveryCreationForm.controls.isBankDetails.setValue(ngRecoveryCreation.isBankDetails);




      //this.ngRecoveryCreation.branchId = ngRecoveryCreation.branchId;
      //this.ngRecoveryCreation.ifscCd = ngRecoveryCreation.ifscCd;
      //this.ngRecoveryCreation.bnfName = ngRecoveryCreation.bnfName;
      //this.ngRecoveryCreation.bankAcNo = ngRecoveryCreation.bankAcNo;
      //this.ngRecoveryCreation.isBankDetails = "Yes";
      this.checkBankDetails("Yes");
    }
    this.ngRecoveryCreationForm.controls.payNgRecoId.setValue(ngRecoveryCreation.payNgRecoId);
    this.ngRecoveryCreationForm.controls.ngDesc.setValue(ngRecoveryCreation.ngDesc);
    this.ngRecoveryCreationForm.controls.address.setValue(ngRecoveryCreation.address);

    //this.ngRecoveryCreation.payNgRecoId = ngRecoveryCreation.payNgRecoId;
    //this.ngRecoveryCreation.ngDesc = ngRecoveryCreation.ngDesc;
    //this.ngRecoveryCreation.address = ngRecoveryCreation.address;
    //this._btnText = 'Update';

    //let billgroups: any = []
    //this.disableFlag = true;
    //debugger;
    //for (let entry of group.split(',')) {
    //  billgroups.push(entry.trim());
    //}
    //this.billGroupCreation.accountHead = schemeId;
    //this.billGroupCreation.serviceType = serviceType;
    //this.billGroupCreation.group = billgroups;
    ////this.groups = 'A';
    //this.billGroupCreation.billGroupName = billGroupName.trim();
    //this.billGroupCreation.billForGAR = String(billForGAR);
    //this.billGroupCreation.payBillGroupId = payBillGroupId;
    ////billGroupCreation.group='A'
    ////this.billGroupCreation.billForGAR = billForGAR;
    //document.getElementById('btn_save').innerHTML = 'Update';

  }
  //btnActiveClick(ngRecoveryCreation: NgRecovery) {

  //}
  fillPopup(payNgRecoId, status) {
    debugger;
    this.ngRecoveryCreation.payNgRecoId = payNgRecoId;
    this.ngRecoveryCreation.isActive = status;
    this.ngRecoveryCreation.ngDedCd = 0;
  }

  CancelClick() {
    this._btnText = 'Save';
    this.form.resetForm();
    this.highlightedRows.pop();
    this.ngRecoveryCreation.isBankDetails = "Yes";
    this.checkBankDetails("Yes");
  }

  //Cancel() {
  //  $(".dialog__close-btn").click();
  //}
  activateDeactivate() {
    debugger;
    this._Service.ActiveDeactiveNgRecovery(this.ngRecoveryCreation).subscribe(res => {

      this.getNgrecovery("00003");
      if (this.ngRecoveryCreation.isActive) {
        this.snackBar.open('Activated successfully', null, { duration: 4000 });
      }
      else {
        this.snackBar.open('Deactivated successfully', null, { duration: 4000 });
      }
    }
    );
    // this.getNgrecovery("00003");
    // $(".dialog__close-btn").click();
    //this.getNgrecovery('00003');
    this.deactivatePopup = false;
    this.activatePopup = false;
    this.highlightedRows.pop();
  }
  rowHighlight(row) {
    this.highlightedRows.pop();
    this.highlightedRows.push(row)
  }

  applyFilter(filterValue: string) {
    this.dataSource = new MatTableDataSource(this.allData.filter(a => a.status.indexOf(filterValue) > -1));
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  charaterOnlyNoSpace(event): boolean {
    let msg = this._msg.charaterOnlyNoSpace(event);
    return msg;
  }


  applyFiltercurrent(filterValue: string) {
    debugger;
    this.paginator.pageIndex = 0;
    this.dataSource = new MatTableDataSource(this.allData.filter(a => a.ngDesc.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.ngDedCd.toString().indexOf(filterValue) > -1 || a.pfmsUniqueCode.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.ifscCd.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.bankAcNo.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 || a.vendorStatus.trim().toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1));
    this.dataSource.paginator = this.paginator;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    //this.dataSource.filter = filterValue.trim().toLowerCase();
    //if (this.dataSource.filteredData.length > 0) {
    //  this.dataSource.filter = filterValue.trim().toLowerCase();
    //  // this.dataSource.filter = filterValue.trim().toLowerCase();
    //} else {
    //  this.dataSource.filteredData.length = 0;
    //}

  }
}
