import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderforallemployeeComponent } from './headerforallemployee.component';

describe('HeaderforallemployeeComponent', () => {
  let component: HeaderforallemployeeComponent;
  let fixture: ComponentFixture<HeaderforallemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderforallemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderforallemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
