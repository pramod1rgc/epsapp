import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import * as $ from 'jquery';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { NgRecovery } from '../../../model/PayBillGroup/NgRecovery';
import { debug } from 'util';
import { Router } from '@angular/router';


@Component({
  selector: 'app-headerforallemployee',
  templateUrl: './headerforallemployee.component.html',
  styleUrls: ['./headerforallemployee.component.css']
})



export class HeaderforallemployeeComponent implements OnInit {




  selectedRadio: any;
  monthList: any[] = [];
  financialYearList: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('creationForm') form: any;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private snackBar: MatSnackBar, private router: Router
  ) { /*this.createForm()*/ }

  ngOnInit() {
    this.bindFinancialYear();
    this.bindAllMonth();
    if (this.router.url === '/dashboard/billGroup/ngrecovery/fileuploadforallemployee')
      this.selectedRadio = '2';
    else
      this.selectedRadio = '1';



    }

  toggleRecComp(value) {
    if (value == "1") {
      this.router.navigate(['dashboard/billGroup/ngrecovery/entryforallemployee']);
    }
    else {
      this.router.navigate(['dashboard/billGroup/ngrecovery/fileuploadforallemployee']);
    }   
  }

  bindFinancialYear() {
    debugger;

    this._Service.GetFinancialYear().subscribe(data => {
      this.financialYearList = data;
      console.log(this.financialYearList);
      //console.log(data[0].schemeCode);
    });
  }

  bindAllMonth() {
    debugger;

    this._Service.GetAllMonth().subscribe(data =>   {
      this.monthList = data;
      console.log(this.monthList);
      //console.log(data[0].schemeCode);
    });
  }

}
