import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileuploadforallemployeeComponent } from './fileuploadforallemployee.component';

describe('FileuploadforallemployeeComponent', () => {
  let component: FileuploadforallemployeeComponent;
  let fixture: ComponentFixture<FileuploadforallemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileuploadforallemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileuploadforallemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
