import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgRecoveryCreationComponent } from './ng-recovery-creation/ng-recovery-creation.component';
import { NgRecoveryModule } from './ng-recovery.module';
import { EntryforallemployeeComponent } from './entryforallemployee/entryforallemployee.component';
import { HeaderforallemployeeComponent } from './headerforallemployee/headerforallemployee.component';
import { FileuploadforallemployeeComponent } from './fileuploadforallemployee/fileuploadforallemployee.component';
import { EntryforsingleemployeeComponent } from './entryforsingleemployee/entryforsingleemployee.component';
import { NGRprocessingComponent } from './ngrprocessing/ngrprocessing.component';
const routes: Routes = [
  {
    path: '', component: NgRecoveryModule, children: [
   
      { path: 'ngrecoverycreation', component: NgRecoveryCreationComponent },
      { path: 'entryforallemployee', component: EntryforallemployeeComponent },
      { path: 'headerforallemployee', component: HeaderforallemployeeComponent },
      { path: 'fileuploadforallemployee', component: FileuploadforallemployeeComponent },
      { path: 'entryforsingleemployee', component: EntryforsingleemployeeComponent },
      { path: 'ngrprocessing', component: NGRprocessingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgRecoveryRoutingModule { }
