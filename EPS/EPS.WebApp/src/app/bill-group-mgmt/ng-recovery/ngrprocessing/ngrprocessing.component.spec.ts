import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NGRprocessingComponent } from './ngrprocessing.component';

describe('NGRprocessingComponent', () => {
  let component: NGRprocessingComponent;
  let fixture: ComponentFixture<NGRprocessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NGRprocessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NGRprocessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
