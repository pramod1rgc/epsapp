import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryforsingleemployeeComponent } from './entryforsingleemployee.component';

describe('EntryforsingleemployeeComponent', () => {
  let component: EntryforsingleemployeeComponent;
  let fixture: ComponentFixture<EntryforsingleemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryforsingleemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryforsingleemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
