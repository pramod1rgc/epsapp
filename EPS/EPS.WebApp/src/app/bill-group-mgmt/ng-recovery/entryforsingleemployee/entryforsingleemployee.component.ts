
import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import * as $ from 'jquery';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { NgRecovery } from '../../../model/PayBillGroup/NgRecovery';

import { debug } from 'util';

@Component({
  selector: 'app-entryforsingleemployee',
  templateUrl: './entryforsingleemployee.component.html',
  styleUrls: ['./entryforsingleemployee.component.css']
})
export class EntryforsingleemployeeComponent implements OnInit {


  callTypevar: any;
  applyFilter: any;
  financialYearList: any[] = [];
  monthList: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('creationForm') form: any;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private snackBar: MatSnackBar
  ) { /*this.createForm()*/ }



  GetcommanMethod(value) {
    debugger;
    //alert(1);
    //this.GetAleadytakenLoandetails(value);
    localStorage.setItem('EmployeeCd', value);
  }



  ngOnInit() {
    this.bindFinancialYear();
    this.bindAllMonth();
  }
  bindFinancialYear() {
    debugger;

    this._Service.GetFinancialYear().subscribe(data => {
      this.financialYearList = data;
    });
  }

  bindAllMonth() {
    debugger;

    this._Service.GetAllMonth().subscribe(data => {
      this.monthList = data;
      console.log(this.monthList);
      //console.log(data[0].schemeCode);
    });
  }
}
