import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import { CommanService } from '../../../services/loan-mgmt/comman.service';
import { DesignationModel, PayBillGroupModel, EmpModel } from '../../../model/Shared/DDLCommon';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs'
import { SelectionModel } from '@angular/cdk/collections';
//import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';


interface BillCode {
  id: string;
  name: string;
}
interface DesignCode {
  id: string;
  name: string;
}
//02-08-19 End
@Component({
  selector: 'app-entryforallemployee',
  templateUrl: './entryforallemployee.component.html',
  styleUrls: ['./entryforallemployee.component.css']
})
export class EntryforallemployeeComponent implements OnInit {

  @ViewChild('creationForm') form: any;
  dataSourceforgrid: any[];
  displayedColumns: string[] = ['CheckBox', 'S.No', 'Employee Code', 'Employee Name', 'SubsAmount','RecovAmount','TotAmount', 'No. of Installment', 'Recovered No. of Installment'];
  NgList: any[] = [];
  NgRecoveryList: any[] = [];
  dataSource = new MatTableDataSource<EmpAttach>();
  EntryForAllEmployee: FormGroup;
  EmpList: any[] = [];
  Design: any[];
  checkBoxValue: any;
  ArrddlBillGroup: PayBillGroupModel;
  ArrddlDesign: DesignationModel[];
  toggleRecoveryBool: string = 'false';
  Bill: any[];
  PayBillGroupId: any;
  selected2: any;
  selected: any;
  dataList: any;
  i: any;
  item: any;


  checkedObject: any[] = [];
  //finYear: string;
  //month: string;
  ngRecovery: Number;
  previousMonth: string;
  billgrId: Number;
  desigId: Number;
  empCd: string;
  empName: string;
  amount: Number;
  universalAmount: Number;
  installmentCount: Number;
  recoveredInstallment: Number;
  employeeList: FormArray;
  totalSumAmount: number = 0;
  RowWiseSumAmount: number=0;

  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  flag: any = null;

  constructor(private _Service: PayBillGroupService, private _CommanService: CommanService, private formBuilder: FormBuilder) { }


  public billCtrl: FormControl = new FormControl();
  public designCtrl: FormControl = new FormControl();
  public billFilterCtrl: FormControl = new FormControl();
  public filteredBill: ReplaySubject<BillCode[]> = new ReplaySubject<BillCode[]>(1);
  public filteredDesign: ReplaySubject<DesignCode[]> = new ReplaySubject<DesignCode[]>(1);
  public designFilterCtrl: FormControl = new FormControl();


  private _onDestroy = new Subject<void>();
  toggleRecovery(value) {
    this.toggleRecoveryBool = value;
    //console.log(this.toggleRecoveryBool);
  }

  ngOnInit() {
    this.getNgrecovery('00003');
    this.getNgRecoveryList('00003');
    this.BindDropDownBillGroup('00003');
    //this.BindDropDownDesignation('00003');
    this.CreateForm();
    //this.getEmployee('00003', 12, 150);
  }

  expansionHeader() {
    //alert("called");
  }

  getNgrecovery(permDDOId: any) {
    //debugger;

    this._Service.GetNgRecovery(permDDOId).subscribe(data => {
      this.NgList = data;
      //this.getNgrecoveryData = data;
      //this.dataSource = new MatTableDataSource(data);
      //this.dataSource.paginator = this.paginator;
      //this.dataSource.sort = this.sort;
    });
  }

  getNgRecoveryList(permDDOId: any) {
    debugger;

    this._Service.GetAllNgRecoveryandPaybillGrpDetails(permDDOId).subscribe(data => {
      this.NgRecoveryList = data;
      this.dataSourceforgrid = data;
      //this.dataSourceforgrid = new MatTableDataSource(data);
      //this.dataSourceforgrid.paginator = this.paginator;
      //this.dataSourceforgrid.sort = this.sort;
      console.log(data);
      //this.getNgrecoveryData = data;
      //this.dataSource = new MatTableDataSource(data);
      //this.dataSource.paginator = this.paginator;
      //this.dataSource.sort = this.sort;
    });
  }


  CreateForm() {
    this.EntryForAllEmployee = this.formBuilder.group({
      //FinYear
      //finYear: [null, ''],
      //month: [null, ''],
      ngRecovery: [null, Validators.required],
      previousMonth: ["false", Validators.required],
      billGrId: [0, Validators.required],
      desigid: [null, ''],
      universalAmount: [''],
      totalAmount: [''],
      //empCd: [null, Validators.required],
      //empName: [null, Validators.required],
      //amount: [0, Validators.required],
      //installmentCount: [null, Validators.required],
      //recoveredInstallment: [null, Validators.required],
      employeeList: this.formBuilder.array([])
    });
    //this.addEmpDetail();
  }

  addEmpDetail() {
    this.GetEmployeeList.push(this.createItem());
    this.dataSource = new MatTableDataSource(this.GetEmployeeList.value);
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      empCode: [null, Validators.required],
      empName: [null, Validators.required],
      subscriptionAmt: [null, Validators.required],
      recoveryAmt: [null, Validators.required],
      totalAmt: [0, Validators.required],
      installmentCount: [null, Validators.required],
      recoveredInstallment: [0],
      ngDedCd: [0],
      permDdoId:[null]
    });
  }


  get GetEmployeeList() {
    return this.EntryForAllEmployee.get('employeeList') as FormArray;
  }
  RowSubsSum(Value1: number, Value2) {
    debugger;

    this.RowWiseSumAmount = 0;

    //this.RowWiseSumAmount = Number(Value1) + this.GetEmployeeList.at(Value2).get('RecovAmount').value;
    //this.GetEmployeeList.at(Value2).get('SubsAmount').setValue(Number(Value1));
    this.GetEmployeeList.at(Value2).get('totalAmt').setValue(Number(Value1) + Number(this.GetEmployeeList.at(Value2).get('recoveryAmt').value))
    
  }
  RowRecovSum(Value1: number, Value2) {
    debugger;
    //this.RowWiseSumAmount = 0;

    
    //this.GetEmployeeList.at(Value2).get('RecovAmount').setValue(Number(Value1));
      this.GetEmployeeList.at(Value2).get('totalAmt').setValue(Number(Value1) + Number(this.GetEmployeeList.at(Value2).get('subscriptionAmt').value))
  }
  SumAmount() {
    this.totalSumAmount = 0;
    debugger;
    for (var i = 0; i < this.checkedObject.length; i++) {
      this.totalSumAmount += +this.checkedObject[i].totalAmt; //.at(i).get('totalAmt').value;

    }
    //this.totalSumAmount = this.EntryForAllEmployee.get('universalAmount').value * this.EmpList.length;
  }

  updateDatasource(element, value) {
    //this.GetEmployeeList.reset();

    element.totalAmt = value;
   // this.SumAmount();
    //element.amount = value;
   // this.dataSource = new MatTableDataSource(this.GetEmployeeList.value);
    //this.GetEmployeeList.at(index).get('amount').setValue
  }

  updateInstallment(element, value) {
    element.installmentCount = value;
  }

  getEmployee(permDDOId: any, payBillGroupId: any, designationCd: any) {
    debugger;
    this._Service.GetEmployeeByPaybillAndDesig(permDDOId, payBillGroupId, designationCd).subscribe(data => {
      this.EmpList = data;
      this.dataSource = new MatTableDataSource(data);
      for (var i = 0; i < this.EmpList.length; i++) {
        this.GetEmployeeList.push(this.formBuilder.group({
          empCode: [this.EmpList[i].empCode, Validators.required],
          empName: [this.EmpList[i].empName, Validators.required],
          subscriptionAmt: [null, Validators.required],
          recoveryAmt: [null, Validators.required],
          totalAmt: [null, Validators.required],
          installmentCount: [null, Validators.required],
          recoveredInstallment: [0]
        }));
      }
      console.log(this.dataSource);
      //this.EmpList.forEach(emp => this.GetEmployeeList.push(this.formBuilder.group(emp)));
    });
  }

  BindDropDownBillGroup(value) {
    debugger;
    this._CommanService.GetAllPayBillGroup(value, this.flag).subscribe(result => {
      this.ArrddlBillGroup = result;
      this.Bill = result;
      //console.log(result);
      this.billCtrl.setValue(this.Bill);
      this.filteredBill.next(this.Bill);
    });
  }

  BindDropDownDesignation(value) {
    this.PayBillGroupId = value;
    this.getEmployee('00003', value, 0);


    this._CommanService.GetAllDesignation(value, this.flag).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      //console.log(data);
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });


    this.selectionAttach.clear();
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    // get the search keyword
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterBill() {
    if (!this.Bill) {
      return;
    }
    // get the search keyword
    let search = this.billFilterCtrl.value;

    if (!search) {
      this.filteredBill.next(this.Bill.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBill.next(

      this.Bill.filter(Bill => Bill.billgrDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  FilterEmployeeByDesignation(value) {
    // this.getEmployee('00003', this.PayBillGroupId, value);
    this._Service.GetEmployeeByPaybillAndDesig('00003', this.PayBillGroupId, value).subscribe(data => {
      this.EmpList = data;
      this.dataSource = new MatTableDataSource(data);
      while (this.GetEmployeeList.length != 0) {
        this.GetEmployeeList.removeAt(0);
      }
      for (var i = 0; i < this.EmpList.length; i++) {
        this.GetEmployeeList.push(this.createItem());
        this.GetEmployeeList.at(i).get('empCode').setValue(this.EmpList[i].empCode);
        this.GetEmployeeList.at(i).get('empName').setValue(this.EmpList[i].empName);
        this.GetEmployeeList.at(i).get('subscriptionAmt').setValue(this.checkBoxValue ? +this.EntryForAllEmployee.get('subscriptionAmt').value : null);
        this.GetEmployeeList.at(i).get('recoveryAmt').setValue(this.checkBoxValue ? +this.EntryForAllEmployee.get('recoveryAmt').value : null);
        this.GetEmployeeList.at(i).get('totalAmt').setValue(this.checkBoxValue ? +this.EntryForAllEmployee.get('totalAmt').value : null);
        this.GetEmployeeList.at(i).get('installmentCount').setValue(null);
        this.GetEmployeeList.at(i).get('recoveredInstallment').setValue(null);
      }
      this.selectionAttach.clear();
    });
  }
   
  onChange(event, index, item) {
    debugger;
    this.checkBoxValue = event.checked;
    if (event.checked) {
      //this.GetEmployeeList;
     // var abc = this.EntryForAllEmployee.get('universalAmount').value * this.EmpList.length;

      for (var i = 0; i < this.EmpList.length; i++) {
        //this.GetEmployeeList.push(this.createItem());
        this.GetEmployeeList.at(i).get('subscriptionAmt').setValue(this.EntryForAllEmployee.get('universalAmount').value);
        this.RowSubsSum(this.EntryForAllEmployee.get('universalAmount').value, i);
        console.log(i);
      }
      this.dataSource = new MatTableDataSource(this.GetEmployeeList.value);
      //this.updateDatasource();
      //this.EntryForAllEmployee.get('totalAmount').setValue(this.EmpList.length * abc);
    }
    else {
      for (var i = 0; i < this.EmpList.length; i++) {
        //this.GetEmployeeList.push(this.createItem());
        this.GetEmployeeList.at(i).get('amount').setValue(0);
      }
    }
    //this.SumAmount();
  }


  allChkChange(event) {

    if (event.checked) {

    }

  }

  SaveNgDetails() {
    debugger;
    this.dataList = this.EntryForAllEmployee.value;
    this.checkedObject;
    var abcd = this.EntryForAllEmployee.get('ngRecovery').value;
    for (var i = 0; i < this.checkedObject.length; i++)
    {
      this.checkedObject[i].ngDedCd = this.EntryForAllEmployee.get('ngRecovery').value;
      this.checkedObject[i].permDdoId = '00003';

    }

    this._Service.InsertNgrecoveryEntry(this.checkedObject).subscribe(res => {
      //this.BindAllBillGroup('00003');
    }
    );
    //var abcf = this.checkedObject;
    this.form.resetForm();
  }

  masterToggleForAttach() {
    debugger;
    this.checkedObject = null;

    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.GetEmployeeList.value.forEach(row => this.selectionAttach.select(row));
    this.checkedObject = this.GetEmployeeList.value;
    this.checkedObject = this.selectionAttach.selected;
    this.SumAmount();
    // alert(this.selectionAttach.selected);
  }

  isAllSelectedForAttach() {
    //debugger;
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  AddCheckedObj() {
    debugger;
    this.checkedObject = null;
    this.checkedObject = this.selectionAttach.selected;
    this.SumAmount();
    //console.log(this.selectionAttach);
  }
}
const ELEMENT_DATA: EmpAttach[] = [];
export interface EmpAttach {
  empName:          string;
  empCode:          string;
  subscriptionAmt:  Number;
  recoveryAmt:      Number;
  totalAmt:         Number;
  installmentCount: Number;
  ngDedCd:          Number;
}
export interface EmpDeAttach {
  empName:          string;
  empCode:          string;
  subscriptionAmt:  Number;
  recoveryAmt:      Number;
  totalAmt:         Number;
  installmentCount: Number;
  ngDedCd:          Number;
}
