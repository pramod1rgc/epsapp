import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryforallemployeeComponent } from './entryforallemployee.component';



describe('EntryforallemployeeComponent', () => {
  let component: EntryforallemployeeComponent;
  let fixture: ComponentFixture<EntryforallemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryforallemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryforallemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
