import { Component, OnInit,ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../services/PayBill/PayBillService';

import { BillGroupAttach } from '../../model/BGAttach';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { map, startWith } from 'rxjs/operators';
import { MasterService } from '../../services/master/master.service';
import { FormControl } from '@angular/forms';
import { debug } from 'util';
import { EmployeeTypeMaster } from '../../model/masterModel';
import { SelectionModel } from '@angular/cdk/collections';
import { stringify } from '@angular/core/src/render3/util';

@Component({
  selector: 'app-bill-attach',
  templateUrl: './bill-attach.component.html',
  styleUrls: ['./bill-attach.component.css']
})
export class BillAttachComponent implements OnInit {
  billGroupAttachForm: FormGroup;
  employeeTypes: EmployeeTypeMaster;
  disbleflag = false;
  isTableHasData = true;
  getEmpById: any = {};
  employeeSubTypes: EmployeeTypeMaster;
  // sfgdfgd
  displayedColumns: string[] = [ 'empCode', 'empName', 'desc', 'desigFieldDeptCD', 'select',];
  
 


  dataDeAttach : any;
  dataSourceDeAttach: any;
  
  //dataSourceDeattach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);

  data: any;
  dataSource:any;
  
  

  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  @ViewChild('f') form: any;
  billAttach: BillGroupAttach = {
    payBillGroupId: null,
    schemeId: null,
    serviceType: null,
    group: null,
    groupId: null,
    billGroupName: null,
    billForGAR: null,
    isActive: null,
    EmployeeType: null,
    EmployeeSubType: null,
    Designation: null,
    billgroupId: null,
    billgrDesc: null,
    billgrFor: null,
    empDOB:null
  }


   getPayBillGroup: any = [];

  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private masterServices: MasterService,private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.disbleflag = true;

    this.getPayBillGroups('00003');
    this.getEmployeetype();
    this.getEmpAttachList();
    
    


  }


  getEmpAttachList() {
    this._Service.getEmpAttachList().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.data = Object.assign(res);
     this.dataSource = new MatTableDataSource<EmpAttach>(this.data);
    })
  }

  getEmpDeAttachList(BillGrpId:any) {
    this._Service.getEmpdEAttachList(BillGrpId).subscribe(res => {
      this.dataSourceDeAttach = new MatTableDataSource(res);
      this.dataDeAttach = Object.assign(res);
     this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
    })
  }

  getEmployeetype() {
    this.masterServices.getEmployeeType().subscribe(res => {
      this.employeeTypes = res;
    })
  }

  getEmployeeSubType(parentId: any) {
    this.masterServices.getEmployeeSubType(parentId).subscribe(res => {
      this.employeeSubTypes = res;
      
    })
   
  }

  getEmployeeTypeId(value: any) {
    if (value === 'undefined' || null) {

    }
    this.getEmployeeSubType(value);
    this.getEmpById.employeeSubTypes = 0;
  }


  // toppings = new FormControl();
  // toppingList: string[] = ['Regular', 'Co-terminus', 'Deputation In', 'Consultant', 'On-Contract', 'Member of Parliament'];


  EmployeeSybType = new FormControl();
  EmployeeSubTypeList: string[] = ['']


  GetPayBillGroupId(value: any) {
    if (value === 'undefined' || null) {

    }
    this.GetPayBillGroupDetailsById(value);
    
    this.getEmpDeAttachList(value);
  }

  GetPayBillGroupDetailsById(parentId: any) {
   
    this._Service.GetPayBillGroupDetailsById(parentId).subscribe(res => {
      debugger;
      this.billAttach = res;
     
      if (this.billAttach.serviceType == "" && this.billAttach.billForGAR == "") {
        this.billAttach.serviceType = "No servie Type";
        this.billAttach.billForGAR = "  this value is empty";
        this.billAttach.billgroupId=parentId;
      }

      this.billAttach.billgroupId=parentId;

      console.log(this.billAttach);
    })
  }


  getPayBillGroups(permDDOId: any) {
    this._Service.BindPayBillGroup(permDDOId).subscribe(data => {
      this.getPayBillGroup = data;
      console.log(data[0].schemeCode);
    });
  }



  isAllSelectedForDeAttach() {
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }


  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggleForDeAttach() {
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
      this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
  }

  masterToggleForAttach() {
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));
  }


  DeAttachSelectedRows() {
    if(this.selectionDeAttach.hasValue())
    {
    this.selectionDeAttach.selected.forEach(item => {
      let index: number = this.dataDeAttach.findIndex(d => d === item);  
      

      
        this.dataDeAttach.splice(index,1);
        this.data.push(item);
        
        
        

        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);

        
      
    });
    this.selectionAttach = new SelectionModel<EmpAttach>(true, []);

    this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
    

  }
  else{
  alert("Please DeAttach at least one Employee ");
}

  }


  AttachSelectedRows() {
    

   if(this.selectionAttach.hasValue())
   {
    
    this.selectionAttach.selected.forEach(item => {

     
      let index: number = this.data.findIndex(d => d === item);
      
      this.dataSource.data.splice(index, 1);
     
      this.dataDeAttach.push(item)

      
      this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);

      this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);

      
    });
    this.selectionAttach = new SelectionModel<EmpAttach>(true, []);

    this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  }
  else{
    alert("Please  Attach at least one Employee");
  }
    
  }

  saveMethod(newBillGroup: BillGroupAttach) {
debugger;
    var DeAttach= this.dataSourceDeAttach;
    var Attach = this.dataSource;

    
    if(confirm("Are you sure to Save  ")) {
      
      

      
      let BillGrpId  = String(this.billAttach.billgroupId).trim();
alert(BillGrpId);
     
      this.dataSource.data.forEach(element => {
         let EmployeeCode: string  =   element.empCode
         this._Service.UpDatePayBillGroupId(EmployeeCode,BillGrpId).subscribe(result=>{
          if (parseInt(result) >= 1) {
                
                 this.snackBar.open('BillGroup Update Successfully', null, { duration: 4000 });
               } else {
                 this.snackBar.open('BillGroup not Successfully', null, { duration: 4000 });
               }


         });


        //  this.objPostingDetailsService.SaveUpdatePostingDetails(this.objPostingDetails).subscribe(result => {
        //   console.log(result);
        //   // tslint:disable-next-line:radix
        //   if (parseInt(result) >= 1) {
        //    this.GetAllPostingDetails(this.comoonEmoCode.Empcode);
        //   this.resetPostingdetailsForm();
        //     this.snackBar.open('Save Successfully', null, { duration: 4000 });
        //   } else {
        //     this.snackBar.open('Save not Successfully', null, { duration: 4000 });
        //   }
        // });
      });



    }
  }

  ResetForm() {
    
    this.form.resetForm();
   

  }
}

const ELEMENT_DeAttachdata:EmpAttach[] =[];
const ELEMENT_DATA: EmpAttach[] = [];
export interface EmpAttach {
  sno: string
  EmpCode: string;
  Empname: string;
  Designation: string;
  DepttEmpCode: string;
}

export interface EmpDeAttach {
 sno: string
 EmpCode: string;
 Empname: string;
 Designation: string;
 DepttEmpCode: string;
}


  



