import { SelectionModel } from '@angular/cdk/collections';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { PayBillGroupService } from '../../services/PayBill/PayBillService';
import { MatSnackBar } from '@angular/material';
import { CommonMsg } from '../../global/common-msg';


export interface PeriodicElement {
  msEmpDuesId: number;
  empCd: string;
  payItemsCd: number;
  empId: number;
  empName: string;
  amount: number;
  isSelected: boolean;
}

//const ELEMENT_DATA: PeriodicElement[] = [
//  { position: 1, name: 'Hydrogen', code: 1.0079, amount: 'H', isSelected: false },
//  { position: 2, name: 'Helium', code: 4.0026, amount: 'He', isSelected: false },
//  { position: 3, name: 'Lithium', code: 6.941, amount: 'Li', isSelected: false },
//  { position: 4, name: 'Beryllium', code: 9.0122, amount: 'Be', isSelected: false },
//  { position: 5, name: 'Boron', code: 10.811, amount: 'B', isSelected: false },
//  { position: 6, name: 'Carbon', code: 12.0107, amount: 'C', isSelected: false },
//  { position: 7, name: 'Nitrogen', code: 14.0067, amount: 'N', isSelected: false },
//  { position: 8, name: 'Oxygen', code: 15.9994, amount: 'O', isSelected: false },
//  { position: 9, name: 'Fluorine', code: 18.9984, amount: 'F', isSelected: false },
//  { position: 10, name: 'Neon', code: 20.1797, amount: 'Ne', isSelected: false },
//];

/**
 * @title Table with selection
 */



@Component({
  selector: 'app-dues-deductions',
  templateUrl: './dues-deductions.component.html',
  styleUrls: ['./dues-deductions.component.css']
})
export class DuesDeductionsComponent implements OnInit {
  @ViewChild(MatPaginator, {}) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'position', 'name', 'code', 'symbol'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  //selection = new SelectionModel<PeriodicElement>(true, []);


  totalCount: number = 0;

  duesDeductForm: FormGroup;
  allPayItems: any[] = [];
  payItems: any[] = [];

  ddoId: string;
  showTable: boolean = false;

  constructor(private formBuilder: FormBuilder, private _service: PayBillGroupService, private snackBar: MatSnackBar, private _msg: CommonMsg) {
    this.ddoId = sessionStorage.getItem('ddoid');
  }


  createForm() {
    this.duesDeductForm = new FormGroup({
      category: new FormControl(''),
      payItem: new FormControl(''),
      amount: new FormControl(''),
      ddoId: new FormControl(this.ddoId),
      empDetails: new FormArray([])
    });
    this.addEmployeeDetail();
  }

  createEmployeeForm(): FormGroup {
    return this.formBuilder.group({
      sNo: new FormControl(this.employeeDetails.value.length + 1),
      msEmpDuesId: new FormControl(0),
      empCd: new FormControl(''),
      payItemCd: new FormControl(0),
      empId: new FormControl(0),
      empName: new FormControl(''),
      amount: new FormControl(0),
      isSelected: new FormControl(false)
    });
  }

  addEmployeeDetail() {
    this.employeeDetails.push(this.createEmployeeForm());
    this.dataSource = new MatTableDataSource(this.employeeDetails.value);
  }

  get employeeDetails() {
    return this.duesDeductForm.get("empDetails") as FormArray;
  }

  setPayItems() {
    if (this.duesDeductForm.controls.category.value == 'dues') {
      this.payItems = this.allPayItems.filter(a => a.isDeus_Deduct == 77);
    }
    else {
      this.payItems = this.allPayItems.filter(a => a.isDeus_Deduct == 78);
    }
  }

  getEmployees() {
    while (this.employeeDetails.length > 0) {
      this.employeeDetails.removeAt(0);
    }
    this._service.GetEmployeesByPayItemCode(this.duesDeductForm.controls.payItem.value).subscribe(response => {
      for (var i = 0; i < response.length; i++) {
        this.employeeDetails.push(this.formBuilder.group({
          sNo: this.employeeDetails.value.length + 1,
          msEmpDuesId: response[i].msEmpDuesId,
          empCd: response[i].empCd,
          payItemCd: response[i].payItemsCd,
          empId: response[i].empId,
          empName: response[i].empName,
          amount: response[i].amount,
          isSelected: false
        }));
      }
      //response.forEach(emp => this.employeeDetails.push(this.formBuilder.group(emp)));
      this.dataSource = new MatTableDataSource(this.employeeDetails.value);
      this.dataSource.paginator = this.paginator;
      this.totalCount = this.dataSource.data.length;
    });
  }

  selectAll(event) {
    this.employeeDetails.controls.forEach(el => el.get("isSelected").setValue(event.checked));
    //this.dataSource = new MatTableDataSource(this.employeeDetails.value);
    //this.dataSource.paginator = this.paginator;
  }

  ///** Whether the number of selected elements matches the total number of rows. */
  //isAllSelected() {
  //  const numSelected = this.selection.selected.length;
  //  const numRows = this.dataSource.data.length;
  //  return numSelected === numRows;
  //}

  ///** Selects all rows if they are not all selected; otherwise clear selection. */
  //masterToggle() {
  //  this.isAllSelected() ?
  //    this.selection.clear() :
  //    this.dataSource.data.forEach(row => this.selection.select(row));
  //}

  onSubmit() {
    if (this.employeeDetails.value.filter(a => a.isSelected == true).length > 0) {
      var obj = { DDOId: this.ddoId, EmpDetails: this.employeeDetails.value.filter(a => a.isSelected == true) };
      this._service.UpdateEmpNonComputationalDuesDeductAmount(obj).subscribe(response => {
        while (this.employeeDetails.length > 0) {
          this.employeeDetails.removeAt(0);
        }
        this.createForm();
        this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
        this.showTable = false;
      });
    }
    else {
      this.snackBar.open("Please Select a Employee.", null, { duration: 4000 });
    }
  }

  cancelForm() {
    while (this.employeeDetails.length > 0) {
      this.employeeDetails.removeAt(0);
    }
    this.createForm();
    this.showTable = false;
  }

  goButton() {
    this.getEmployees();
    this.showTable = true;
  }

  setAmount(event) {
    if (event.checked) {
      this.employeeDetails.controls.forEach(el => el.get("amount").setValue(this.duesDeductForm.controls.amount.value));
    }
    else {
      this.employeeDetails.controls.forEach(el => el.get("amount").setValue(''));
    }
    this.dataSource = new MatTableDataSource(this.employeeDetails.value);
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(searchTerm) {
    this.dataSource.filter = searchTerm.trim();
    this.paginator.firstPage();
    this.totalCount = this.dataSource.filteredData.length;
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.createForm();
  }
}
