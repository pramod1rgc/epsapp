import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, Validators } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import { DuesDeductionsComponent } from '../dues-deductions.component';

@Component({
  selector: 'app-dues-deductions-header',
  templateUrl: './dues-deductions-header.component.html',
  styleUrls: ['./dues-deductions-header.component.css']
})
export class DuesDeductionsHeaderComponent implements OnInit {

  BillGroup: any;
  FinYearGroup: any;
  DesigGroup: any;
  selectedRadio: any;
  FinYearDetails: any;
  headersForm: FormGroup;
  allDesignation: any[] = [];
  allFinYear: any[] = [];
  public billFilterCtrl: FormControl = new FormControl();
  public desigFilterCtrl: FormControl = new FormControl();
  public finYearFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredBill: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredDesig: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredFinYear: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  controllerId: any;
  ddoId: any;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;


  constructor(private _service: PayBillGroupService, private duesComponent: DuesDeductionsComponent) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.ddoId = sessionStorage.getItem('ddoid');

  }

  ngOnInit() {
    this.billFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBill();
      });
    this.desigFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesig();
      });
    this.finYearFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterFinYear();
      });
    this.getAllFinYears();
    //if (this.router.url === '/dashboard/promotion/reversions')
    //  this.selectedRadio = '2';
    //else
    //  this.selectedRadio = '1';


    this.headerDataForms();
  }

  private filterBill() {
    if (!this.BillGroup) {
      return;
    }
    // get the search keyword
    let search = this.billFilterCtrl.value;
    if (!search) {
      this.filteredBill.next(this.BillGroup.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBill.next(

      this.BillGroup.filter(Bill => Bill.billGrDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterDesig() {
    if (!this.allDesignation) {
      return;
    }
    // get the search keyword
    let search = this.desigFilterCtrl.value;
    if (!search) {
      this.filteredDesig.next(this.allDesignation.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredDesig.next(

      this.allDesignation.filter(desig => desig.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  private filterFinYear() {
    if (!this.allFinYear) {
      return;
    }
    // get the search keyword
    let search = this.finYearFilterCtrl.value;
    if (!search) {
      this.filteredFinYear.next(this.allFinYear.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredFinYear.next(

      this.allFinYear.filter(fin => fin.fullDescription.toLowerCase().indexOf(search) > -1)
    );
  }


  getAllFinYears() {
    this._service.GetFinancialYear().subscribe(response => {
      this.allFinYear = response;
      this.filteredFinYear.next(this.allFinYear);
    });
  }

  headerDataForms() {
    this.headersForm = new FormGroup({
      financial_year: new FormControl('', Validators.required),
      pay_bill_group: new FormControl('', Validators.required),
      designation: new FormControl('', [Validators.required])
    });
  }

  getPayBillGroups() {
    var finYearTo = +this.headersForm.controls.financial_year.value;
    var finYearFrom = +finYearTo - 1;

    this._service.GetPayBillGroupByFinancialYear(finYearFrom.toString(), finYearTo.toString(), "00003").subscribe(response => {
      this.BillGroup = response;
      this.filteredBill.next(this.BillGroup);
    });
  }

  getDesignations() {
    this._service.GetDesignationByBillgrID(this.headersForm.controls.pay_bill_group.value, "00003").subscribe(response => {
      this.allDesignation = response;
      this.filteredDesig.next(this.allDesignation);
    });
  }

  onSubmit() {
    this._service.GetPayItemsFromDesignationCode(this.headersForm.controls.designation.value).subscribe(response => {
      this.duesComponent.allPayItems = response;
    });
  }

}
