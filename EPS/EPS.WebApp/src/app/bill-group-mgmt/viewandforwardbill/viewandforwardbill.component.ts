import { Component, OnInit, ViewChild } from '@angular/core';

import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export interface PeriodicElement {
 
  position: number;
  weight: number;
  name: string;
  schemecode: string;
  billbd: string;
  billtype: string;


}


const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, weight: 121, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
  { position: 2, weight: 122, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
  { position: 3, weight: 123, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
  { position: 4, weight: 124, name: '201904008', schemecode: 'Regular', billbd: 'April 2019', billtype: 'Bill Processed' },
];

//Table 2
export interface PeriodicElement2 {
  position: number;  
  name: string;
}
const ELEMENT_DATA2: PeriodicElement2[] = [
  { position: 1, name: 'GAR 13 (Outer)', },
  { position: 1, name: 'MTR 13-A(Inner)', },
];
//Table 2 end

@Component({
  selector: 'app-viewandforwardbill',
  templateUrl: './viewandforwardbill.component.html',
  styleUrls: ['./viewandforwardbill.component.css']
})
export class ViewandforwardbillComponent implements OnInit {

  displayedColumns: string[] = ['position', 'weight', 'name', 'schemecode', 'billbd', 'billtype',];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);


  //Table 2
  displayedColumns2: string[] = ['position', 'name'];
  dataSource2 = new MatTableDataSource<PeriodicElement2>(ELEMENT_DATA2);
  //Table 2 end


  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

 

  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource2.paginator = this.paginator;
    this.dataSource2.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
