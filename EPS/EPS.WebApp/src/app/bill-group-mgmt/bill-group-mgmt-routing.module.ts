import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillGroupMgmtModule } from './bill-group-mgmt.module';
import { BillGroupCreationComponent } from './bill-group-creation/bill-group-creation.component';
import { BillAttachComponent } from './bill-attach/bill-attach.component';
import { PaybillprocessComponent } from './paybillprocess/paybillprocess.component';
import { MappingaccountheadComponent } from './mappingaccounthead/mappingaccounthead.component';
import { Actdeactmapacoth1Component } from './mappingaccounthead/actdeactmapacoth1/actdeactmapacoth1.component';
import { DuesDeductionsComponent } from './dues-deductions/dues-deductions.component'
import { DuesanddeductionsemployeewiseComponent } from './duesanddeductionsemployeewise/duesanddeductionsemployeewise.component';
import { TemporarypbrprocessComponent } from './temporarypbrprocess/temporarypbrprocess.component';
import { PaybillgenerationComponent } from './paybillgeneration/paybillgeneration.component';
import { UnfreezeanddeletebillComponent } from './unfreezeanddeletebill/unfreezeanddeletebill.component';
import { ViewandforwardbillComponent } from './viewandforwardbill/viewandforwardbill.component';
const routes: Routes = [
  {
    path: '', component: BillGroupMgmtModule, children: [
      { path: 'billgroupcreation', component: BillGroupCreationComponent },
      { path: 'billattach', component: BillAttachComponent },
      { path: 'payBillProcess', component: PaybillprocessComponent },

      { path: 'ngrecovery', loadChildren: '../bill-group-mgmt/ng-recovery/ng-recovery.module#NgRecoveryModule' },
      { path: 'mappingaccountheadotherthan1', component: MappingaccountheadComponent },
      { path: 'actdeactmapacoth1', component: Actdeactmapacoth1Component },
      { path: 'DuesDductions', component: DuesDeductionsComponent },
      { path: 'DuesanddeductionsemployeewiseComponent', component: DuesanddeductionsemployeewiseComponent },
      { path: 'temporarypbrprocessComponent', component: TemporarypbrprocessComponent },
      { path: 'paybillgenerationComponent', component: PaybillgenerationComponent },
      { path: 'unfreezeanddeletebillComponent', component: UnfreezeanddeletebillComponent },
      { path: 'viewandforwardbillComponent', component: ViewandforwardbillComponent }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillGroupMgmtRoutingModule { }
