import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaybillprocessComponent } from './paybillprocess.component';

describe('PaybillprocessComponent', () => {
  let component: PaybillprocessComponent;
  let fixture: ComponentFixture<PaybillprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaybillprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaybillprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
