import { Component, OnInit, ViewChild } from '@angular/core';
import { PayBillGroupService } from '../../services/PayBill/PayBillService';

import { BillGroupAttach } from '../../model/BGAttach';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatSnackBar, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { map, startWith } from 'rxjs/operators';
import { MasterService } from '../../services/master/master.service';
import { FormControl } from '@angular/forms';
import { debug } from 'util';
import { EmployeeTypeMaster } from '../../model/masterModel';
import { SelectionModel } from '@angular/cdk/collections';
import { AppDateAdapter, APP_DATE_FORMATS} from './date.adapter';

@Component({
  selector: 'app-paybillprocess',
  templateUrl: './paybillprocess.component.html',
  styleUrls: ['./paybillprocess.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class PaybillprocessComponent implements OnInit {
  btnclose() {
    this.is_btnStatus = false;
  }
  is_btnStatus = true;

  billGroupAttachForm: FormGroup;
  employeeTypes: EmployeeTypeMaster;
  disbleflag = false;
  isTableHasData = true;
  getEmpById: any = {};
  employeeSubTypes: EmployeeTypeMaster;
  // sfgdfgd
  displayedColumns: string[] = ['empCode', 'empName', 'desc', 'desigFieldDeptCD', 'select',];



  selected: any;
  selected1: any;

  dataDeAttach: any;
  dataSourceDeAttach: any;

  //dataSourceDeattach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);

  data: any;
  dataSource: any;



  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  @ViewChild('f') form: any;
   billAttach: BillGroupAttach = {
    payBillGroupId: null,
    schemeId: null,
    serviceType: null,
    group: null,
    groupId: null,
    billGroupName: null,
    billForGAR: null,
    isActive: null,
    EmployeeType: null,
    EmployeeSubType: null,
    Designation: null,
     billgroupId: null,
     billgrDesc: null,
     billgrFor: null,
     empDOB:null
  }


  getPayBillGroup: any = [];

  constructor(private _Service: PayBillGroupService, private fb: FormBuilder
    , private masterServices: MasterService, private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {
    this.disbleflag = true;

    this.getPayBillGroups('00003');
    this.getEmployeetype();
    this.getEmpAttachList();




  }


  getEmpAttachList() {
    this._Service.getEmpAttachList().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.data = Object.assign(res);
      this.dataSource = new MatTableDataSource<EmpAttach>(this.data);
    })
  }

  getEmpDeAttachList(BillGrpId: any) {
    this._Service.getEmpdEAttachList(BillGrpId).subscribe(res => {
      this.dataSourceDeAttach = new MatTableDataSource(res);
      this.dataDeAttach = Object.assign(res);
      this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
    })
  }

  getEmployeetype() {
    this.masterServices.getEmployeeType().subscribe(res => {
      this.employeeTypes = res;
    })
  }

  getEmployeeSubType(parentId: any) {
    this.masterServices.getEmployeeSubType(parentId).subscribe(res => {
      this.employeeSubTypes = res;

    })

  }

  getEmployeeTypeId(value: any) {
    if (value === 'undefined' || null) {

    }
    this.getEmployeeSubType(value);
    this.getEmpById.employeeSubTypes = 0;
  }


  // toppings = new FormControl();
  // toppingList: string[] = ['Regular', 'Co-terminus', 'Deputation In', 'Consultant', 'On-Contract', 'Member of Parliament'];


  EmployeeSybType = new FormControl();
  EmployeeSubTypeList: string[] = ['']


  GetPayBillGroupId(value: any) {
    if (value === 'undefined' || null) {

    }
    this.GetPayBillGroupDetailsById(value);

    this.getEmpDeAttachList(value);
  }

  GetPayBillGroupDetailsById(parentId: any) {

    this._Service.GetPayBillGroupDetailsById(parentId).subscribe(res => {
      this.billAttach = res;

      if (this.billAttach.serviceType == "" && this.billAttach.billForGAR == "") {
        this.billAttach.serviceType = "No servie Type";
        this.billAttach.billForGAR = "  this value is empty";
      }

this.billAttach.billgroupId=parentId;

      console.log(this.billAttach);
    })
  }


  getPayBillGroups(permDDOId: any) {
    this._Service.BindPayBillGroup(permDDOId).subscribe(data => {
      this.getPayBillGroup = data;
      console.log(data[0].schemeCode);
    });
  }



  isAllSelectedForDeAttach() {
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }


  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggleForDeAttach() {
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
      this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
  }

  masterToggleForAttach() {
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));
  }


  DeAttachSelectedRows() {
    if (this.selectionDeAttach.hasValue()) {
      this.selectionDeAttach.selected.forEach(item => {
        let index: number = this.dataDeAttach.findIndex(d => d === item);



        this.dataDeAttach.splice(index, 1);
        this.data.push(item);




        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);



      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);

      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);


    }
    else {
      alert("Please Unselect at least one Employee ");
    }

  }


  AttachSelectedRows() {


    if (this.selectionAttach.hasValue()) {

      this.selectionAttach.selected.forEach(item => {


        let index: number = this.data.findIndex(d => d === item);

        this.dataSource.data.splice(index, 1);

        this.dataDeAttach.push(item)


        this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);

        this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);


      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);

      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
    }
    else {
      alert("Please  select at least one Employee");
    }

  }

  saveMethod(name: string) {
    if (confirm("Are you sure to process pay Bill   ")) {
      console.log("Implement process functionality here");
    }
  }

  ResetForm() {

    this.form.resetForm();


  }
}

const ELEMENT_DeAttachdata: EmpAttach[] = [];
const ELEMENT_DATA: EmpAttach[] = [];
export interface EmpAttach {
  sno: string
  EmpCode: string;
  Empname: string;
  Designation: string;
  DepttEmpCode: string;
}

export interface EmpDeAttach {
  sno: string
  EmpCode: string;
  Empname: string;
  Designation: string;
  DepttEmpCode: string;
}

  









 





