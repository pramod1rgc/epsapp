import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { AccountHeadOT1 } from '../../../model/accountheadOT1';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-actdeactmapacoth1',
  templateUrl: './actdeactmapacoth1.component.html',
  styleUrls: ['./actdeactmapacoth1.component.css']
})
export class Actdeactmapacoth1Component implements OnInit {

  mappingAccountHeadOT1: AccountHeadOT1 = {
    ddoCode: null,
    ddoCodeddoName: null

  }

  getAccountHeadOT1: any = [];
  getFinYr: any[];

  public modeselect = '2018';
  public FinFromYr; FinToYr: string;

  displayedColumns: string[] = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName','AccountHeadStatus', 'select'];
  MappeddisplayedColumns: string[] = ['S.No', 'AccountHead', 'ObjectHead', 'Category', 'AccountHeadName', 'AccountHeadStatus', 'Mappedselect'];
  //checked: boolean = true;
  buttonDisabled: boolean;
  data: any;
  dataSource: any;
  MappeddataSource: any;                                                                                                                                                         
  disabled: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('creationForm') form: any;
  constructor(private _Service: PayBillGroupService, private snackBar: MatSnackBar
  ) { }
  ngOnInit() {

    this.getAccountHeadsOT1();
    this.disabled = false;

  }

  public selection = new SelectionModel<AccountHeadAttach>(true, []);
  public Mappedselection = new SelectionModel<AccountHeadAttach>(true, []);
   //Get DDO Info 
 

  getAccountHeadsOT1() {
    //debugger;
    if (this.modeselect === '2018') {
      this.FinFromYr = '2018';
      this.FinToYr = '2019';
    }
    this._Service.GetAccountHeadsOT1(this.FinFromYr, this.FinToYr,"ACT_DEACTIVE").subscribe(data => {
      this.getAccountHeadOT1 = data;
      console.log(data[0].ddoCode);
    });
  }

   //on click of DDL_ddocode   
  GetAccountHeadOT1ActDedata(value: any) {
    debugger;
    if (value === 'undefined' || null) {
      this.disabled = true;
    }
    this.getAccountActivatedHeadOT1(value);
    this.getAccountDeActivatedHeadOT1(value);
  }
  // #region <Activated   Mapping of account head OT 1>
  getAccountActivatedHeadOT1(DDOCode: any) {
    this._Service.GetAccountHeadOT1PaybillGroupStatus(DDOCode, 'Y').subscribe(res => {
      debugger;

      this.dataSource = new MatTableDataSource(res);
      this.dataSource.sort = this.sort;
      this.disabled = true;

    })
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected($event) {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle($event) {
    debugger;
    this.isAllSelected($event) ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }
  De_ActivecheckOptions() {
    debugger;

    const numSelected1 = this.selection.selected.length;

    var self = this;
    let isSelected: any = numSelected1;
    let ddoCodeSelected: any = this.mappingAccountHeadOT1.ddoCode;
    if (isSelected > 0) {
      //At least one is selected
      //alert(numSelected1);      
      this.selection.selected.forEach(function (value) {
        let accountHeadvalue: string = value.hiddenAccountHead;
        //billgroups.push(value.accountHead);

        self._Service.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, ddoCodeSelected, 'ACTIVE').subscribe(result => {
          if (parseInt(result) >= 1) {

            self.snackBar.open('Deactive Update Successfully', null, { duration: 4000 });
            self.GetAccountHeadOT1ActDedata(ddoCodeSelected);
          } else {
            self.snackBar.open('Deactive Not Update Successfully', null, { duration: 4000 });
          }

        });

        //console.log(value.accountHead);
      });
      //mapped account head

      this.getAccountDeActivatedHeadOT1(ddoCodeSelected);
      self.selection.clear();
      self.Mappedselection.clear();
      //alert(billgroups);
    } else {
      alert("select at least one for Deactive");
    }

  }

  //onCompleteRow(dataSource) {
  //  dataSource.data.forEach(element => {
  //    console.log(element.accountHead);
  //  });
  //}

  // #endregion

  // #region <De-Activated Mapped of account head OT 1>
  MappedapplyFilter(filterValue: string) {
    this.MappeddataSource.filter = filterValue.trim().toLowerCase();
    if (this.MappeddataSource.paginator) {
      this.MappeddataSource.paginator.firstPage();
    }
  }
  getAccountDeActivatedHeadOT1(DDOCode: any) {
    this._Service.GetAccountHeadOT1PaybillGroupStatus(DDOCode, 'N').subscribe(res => {
      debugger;
      this.MappeddataSource = new MatTableDataSource(res);
      this.MappeddataSource.sort = this.sort;
    })
  }
  MappedisAllSelected($event) {

    const numSelected = this.Mappedselection.selected.length;
    const numRows = this.MappeddataSource.data.length;
    return numSelected === numRows;
  }
  MappedmasterToggle($event) {
    debugger;
    this.MappedisAllSelected($event) ?
      this.Mappedselection.clear() :
      this.MappeddataSource.data.forEach(row => this.Mappedselection.select(row));

  }

  ActivecheckOptions() {
    debugger;

    const numSelected1 = this.Mappedselection.selected.length;

    var self = this;
    let isSelected: any = numSelected1;
    let ddoCodeSelected: any = this.mappingAccountHeadOT1.ddoCode;
    //let paybillGroupStatusSelected: any = this.Mappedselection.selected[0].paybillGroupStatus;

    if (isSelected > 0) {
      //At least one is selected
      //alert(numSelected1);

     // if (paybillGroupStatusSelected == 'Yes') {
        this.Mappedselection.selected.forEach(function (value) {
          let accountHeadvalue: string = value.hiddenAccountHead;
          //billgroups.push(value.accountHead);

          self._Service.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, ddoCodeSelected, null).subscribe(result => {
            if (parseInt(result) >= 1) {

              self.snackBar.open('Active Update Successfully', null, { duration: 4000 });
              self.GetAccountHeadOT1ActDedata(ddoCodeSelected);
            } else {
              self.snackBar.open('Active Not Update Successfully', null, { duration: 4000 });
            }


          });

          //console.log(value.accountHead);
        });
        //mapped account head

        this.getAccountDeActivatedHeadOT1(ddoCodeSelected);
      //}
      //else {
      //  alert("You Can only De-Activate the Account Head as Bill is Already Processed");
      //}
      self.selection.clear();
      self.Mappedselection.clear();
      //alert(billgroups);
    } else {
      alert("select at least one for Active");
    }

  }


  // #endregion

}
const ELEMENT_DATA: AccountHeadAttach[] = [];
export interface AccountHeadAttach {

  accountHead: string;
  objectHead: string;
  category: string;
  accountHeadName: string;
  paybillGroupStatus: string;
  descriptions: string;
  hiddenAccountHead: string;
}
