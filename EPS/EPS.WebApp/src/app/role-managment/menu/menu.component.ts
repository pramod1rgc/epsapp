import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators, FormControl, NgModel } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
 
import { MenuService } from '../../services/UserManagement/menu.service'
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
import { DashboardComponent } from '../../dashboard/dashboard/dashboard.component';

export interface MenuData {
  mainMenuName: string;
  parentMenuName: string;
  menuURL: string;
}
export interface commondll {
  values: string;
  text: string;
}
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private _MenuService: MenuService, private snackBar: MatSnackBar,
    private curMenu: DashboardComponent
  ) { }
  //Variabel
  //'msMenuID', 'parentMenu', 
  displayedColumns: string[] = ['mainMenuName','parentMenuName', 'menuURL', 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('roleassign') roleassign: any;
  public filteredMenu: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public MenuFilterCtrl: FormControl = new FormControl();
  public MenuCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  isLoading: boolean;
  menudataSource: any;
  txtMenuName: any;
  txtMenuUrl: any;
  mstMenu: any = {};
  ArrMenus: any;
  ArrSubMenus: any;
  ArrMenusInGride: any;
  Message: any;
  hdnMenuId = 0;
  MenuID: any;
  mainMenuName: any;
  showMwnuName: any;
  checkMenu: any;
  MstMenu: any;
  btnUpdatetext = 'Save';
  MwnuHeaderText = 'Create New Menu';
  ActiveDactiveHeaderText: any;
  showMenu: boolean;
  DeleteMenu: boolean;
  ArrRoles: any;
  Ishowhide: boolean;
  TxtSearch: string;
  username: string;
  uRoleID: string;
  selrdType: string;
  IsShowHiddl: boolean;
  notFound = true;
  ArrUpdateRoleSelected :any;
  //@ViewChild(DashboardComponent) private curMenu: DashboardComponent
  //End Variabel

  ngOnInit() {
    this.selrdType = 'Root';
    this.BindDropDownMenu();
    this.BindMenuInGride();
    this.GetAllRoles();
    this.MenuFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterMenu(); });
 
  }

  TextChange() {
    this.btnUpdatetext = 'Save';
    this.MwnuHeaderText = 'Create New Menu';
    this.selrdType = 'Root';
    this.BindDropDownMenu();
    this.Clear();
    //this.Cancel();
  }
  SaveMenu() {
    debugger;

    this.username = sessionStorage.getItem('username');
    this.uRoleID = sessionStorage.getItem('userRoleID');
   
    //this.curMenu.getAllMenusByUser();
    this.isLoading = true;
    this._MenuService.SaveMenu(this.hdnMenuId, this.uRoleID, this.mstMenu).finally(() => this.isLoading = false).subscribe(result => {
      this.curMenu.getAllMenusByUser(this.username, this.uRoleID);
      this.Message = result;
      
      if (this.Message != undefined) {
        this.Clear();
        $(".dialog__close-btn").click();
        swal(this.Message)
        //this.snackBar.open(this.Message, '', {
        //  duration: 2000,
        //});
        this.roleassign.resetForm();
      }
      this.BindMenuInGride();
      this.BindDropDownMenu();
      this.Ishowhide = false;

    })
    this.TxtSearch = '';
  }

  BindDropDownMenu() {
    this._MenuService.BindDropDownMenu().subscribe(result => {
      debugger;
      this.ArrMenus = result;
      this.mstMenu.parentMenu = 0;

     // this.MenuFilterCtrl.setValue(this.ArrMenus.parentMenu);
      this.filteredMenu.next(this.ArrMenus);
    })
  }
  equalsRole(objOne, objTwo) {

    if (typeof objOne !== 'undefined' && typeof objTwo !== 'undefined') {
      return objOne.roleID === objTwo.roleID;
    }
  }
  //ArrmstMenu: any ;
  UpdateMenu(MsMenuID, menuURL)//(RowIndex)
  {
    debugger;
    this._MenuService.FillUpdateMenu(MsMenuID).subscribe(result => {
      
      this.mstMenu = result[0][0];
      this.mstMenu.RoleSIDS = result[1];
      if (this.mstMenu.parentMenu === 0) {
        
        this.selrdType = 'Root';
      } else {
        this.selrdType = 'SubMenu';
      }
      // this.mstMenu = result[0].msMenuID;
      // console.log(this.ArrmstMenu);
    });
    //this.mstMenu.parentMenu = this.mstMenu[0].msMenuID;
    //this.mstMenu.parentMenu = this.mstMenu[0].parentMenu;   
    //this.mstMenu.mainMenuName = this.mstMenu[0].mainMenuName;
    //this.mstMenu.menuURL = this.mstMenu[0].menuURL;
    this.hdnMenuId = MsMenuID;
    this.btnUpdatetext = 'Update';
    this.MwnuHeaderText = 'Edit Menu';
    this.Ishowhide = true;
    this.IsShowHiddl = true;
    
    //this.mstMenu.parentMenu = ParentMenu;
 
  }
  BindMenuInGride() {
    this._MenuService.BindMenuInGride().subscribe(result => {
      this.ArrMenusInGride = result;
      this.menudataSource = new MatTableDataSource(result);
      this.menudataSource.paginator = this.paginator;
      this.menudataSource.sort = this.sort;

      //Filetr inData source
      this.menudataSource.filterPredicate =
        (data: MenuData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
       //END Of Filetr inData source
    })
  }
  FillFormDeletePop(msMenuID, mainMenuName, isActive) {
    debugger;
    //alert(mainMenuName);
    this.MenuID = msMenuID;
    this.mainMenuName = mainMenuName;
    this.showMwnuName = mainMenuName;
    if (isActive == 'Active') {
      this.checkMenu = false//true;
      this.ActiveDactiveHeaderText = 'Do you want to deactivate menu';//'Are Sure DeActivate Menu ?'
    }
    else {
      this.checkMenu = true//false;
      this.ActiveDactiveHeaderText = 'Do you want to activate menu';//'Are Sure DeActivate Menu ?'
    }
  }

  ActiveDeactiveMenu() {
    //alert(this.checkMenu)
    //return;
    this.isLoading = true;
    this.MstMenu = {
      IsActive: this.checkMenu,
      msMenuID: this.MenuID
    };
    JSON.stringify(this.MstMenu);
    this.username = sessionStorage.getItem('username');
    this.uRoleID = sessionStorage.getItem('userRoleID');
    this._MenuService.ActiveDeactiveMenu(this.MstMenu).finally(() => this.isLoading = false).subscribe(result => {
      
      this.curMenu.getAllMenusByUser(this.username, this.uRoleID);
      this.Message = result;
      this.BindMenuInGride();
      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        //this.snackBar.open(this.Message, '', {
        //  duration: 2000,
        //});
       swal(this.Message)
      }
    })
    this.TxtSearch = '';
  }
  Cancel() {
    $(".dialog__close-btn").click();
    this.Clear();
    this.IsShowHiddl = false;
    this.roleassign.resetForm();
  }

  Clear() {

    this.hdnMenuId = 0;
    this.mstMenu.mainMenuName = '';
    this.mstMenu.menuURL = '';
    this.mstMenu.parentMenu = 0;
    this.mstMenu.childMenu = 0;
    this.Ishowhide = false;
  }

  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'mainMenuName',
      value: filterValue
    });


    this.menudataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.menudataSource.paginator) {
      this.menudataSource.paginator.firstPage();
    }
    if (this.menudataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  GetAllRoles() {
    this._MenuService.GetPredefineRole().subscribe(result => {
      this.ArrRoles = result;
      this.mstMenu.parentMenu = 0;
    })
  } 

  selectAll(checkAll, select: NgModel, values) {
    //this.toCheck = !this.toCheck;
   // alert('call mrthod');
    //debugger;

    if (checkAll) {
      select.update.emit(values);
    }
    else {
      select.update.emit([]);
    }
  }
  MenuUrlName: any;
  fileToUpload: File = null;
  SetUrl(fileName)
  {
   // alert('change--' + fileName)
    this.MenuUrlName = fileName;
  }
  HideandShowradio(Rdfleg) {
    if (Rdfleg == 'Root') {
      this.IsShowHiddl = false;

    }
    if (Rdfleg == 'SubMenu') {
      this.IsShowHiddl = true;
    }
  }
  charaterOnlyNoSpace(event): boolean {
    // debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
 
  //not use for filter search
  private filterMenu() {
     debugger;
    if (!this.filteredMenu) {
      return;
    }
    // get the search keyword
    let search = this.MenuFilterCtrl.value;
    if (!search) {

      this.filteredMenu.next(this.ArrMenus.slice());
      return;
    }
    else {
     search = search.toLowerCase();
    }
    // filter the banks
    //this.filteredMenu.next(

    //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
    //);
    var a = this.ArrMenus;
    this.filteredMenu.next(
      this.ArrMenus.filter(ArrMenus => ArrMenus.mainMenuName.toLowerCase().indexOf(search) > -1)
    );
  }
 
  
}
