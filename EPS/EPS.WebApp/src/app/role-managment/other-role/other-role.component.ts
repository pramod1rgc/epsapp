
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Gpfinterestratemodel } from '../../model/masters/gpfinterestratemodel';
import { MasterService } from '../../services/master/master.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';
import { debug, log } from 'util';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import {RoleService } from '../../services/UserManagement/role.service';
import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/finally';
export interface RoleData {
  roleName: string;
  roleDescription: string;
  createdDate: string;
  deActDesc: string;
  deactivationDate: string;
}

@Component({
  selector: 'app-other-role',
  templateUrl: './other-role.component.html',
  styleUrls: ['./other-role.component.css']
})

export class OtherRoleComponent implements OnInit {
  displayedColumns: string[] = ['roleName', 'roleDescription', 'CreatedDate','roleDeactivateDescription','DeactivationDate','Active']; /*'CreatedDate', 'roleDescription', */
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  textNewrole: string;
  textDescription: string;
  savebuttonstatus: boolean;
  btnUpdatetext: string;
  checkIsActiveRole: any = true;
  MstRole: any;
  ArrRoles: any = [];
  hdnRoleId = 0;
  roledataSource: any;
  Message: any;
  roleID: any;
 // roleName: string;
  showroleName: string;
  checkrole: boolean;
  ActiveDactiveHeaderText: string;
  IsDeactivatetextDesc: boolean;
  isLoading: boolean;
  textDeactivateDescription: string;
  TxtSexrch: string;
  lbleditrole: string;
  Newrole: boolean;
  Activaterole: boolean;
  notFound = true;
  constructor(private _RoleService: RoleService, private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.textNewrole = '';
    this.savebuttonstatus = true;
    this.btnUpdatetext = 'Save';
    this.lbleditrole = 'Create Role';
    this.GetAllDataRollList();
  }

  TextChange() {
    //this.lblPopupText = 'Create New Role';
    //this.btnPopupText = 'Save';
    this.textNewrole = "";
    this.textDescription = "";
    this.checkIsActiveRole = true;
    this.hdnRoleId = 0;
  }


  GetAllDataRollList() {
    this.isLoading = true;
    this._RoleService.GetAllDataRollList(sessionStorage.getItem('username')).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrRoles = result;
      this.roledataSource = new MatTableDataSource(result);
      this.roledataSource.paginator = this.paginator;
      this.roledataSource.sort = this.sort;

    //Filetr inData source
      this.roledataSource.filterPredicate =
        (data: RoleData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
       //END Of Filetr inData source
    })
  }


  SaveNewRole() {
    this.isLoading = true;
     this.MstRole = {
      RoleName: this.textNewrole,
      IsActive: this.checkIsActiveRole,
       RoleID: this.hdnRoleId,
       RoleDescription: this.textDescription,
      LogInUserName: sessionStorage.getItem('username'),
      RoleParentID: sessionStorage.getItem('userRoleID')
    };
    JSON.stringify(this.MstRole);
    this._RoleService.SaveNewRole(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      console.log(result);
      this.Message = result;
      this.GetAllDataRollList();

      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        //this.snackBar.open(this.Message, '', {
        //  duration: 2000,
        //  verticalPosition: 'top',
        //  horizontalPosition: 'center',
        //});
        this.btnUpdatetext = 'Save';
        this.lbleditrole = 'Create Role';
        swal(this.Message)
      }
      this.ClearInputRole();
    })
    this.TxtSexrch = '';
  }

  ClearInputRole() {
    this.textNewrole = '';
    this.checkIsActiveRole = true;
    this.textDescription = "";

  }

  InsertandUpdate() {
    alert(this.textNewrole);
    debugger;

  }
  UpdateRole(roleName, msRoleID, roleDescription) {
   
    this.textNewrole = roleName;
    this.textDescription = roleDescription;
    this.hdnRoleId = msRoleID;
    this.btnUpdatetext = 'Update';
    this.lbleditrole = 'Edit Role';
  }

  applyFilter(filterValue: string) {
    //alert(filterValue)
     debugger;
    const tableFilters = [];
    tableFilters.push({
      id: 'roleName',
      value: filterValue
    });
     
    this.roledataSource.filter = JSON.stringify(tableFilters);
    if (this.roledataSource.paginator) {
      this.roledataSource.paginator.firstPage();
    }
   
    //this.roledataSource.filteredData.roleName == filterValue.trim().toLowerCase();
    //if (this.roledataSource.paginator) {
    //  this.roledataSource.paginator.firstPage();
    //}
    if (this.roledataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }



  FillFormrevokerolePop(roleID, roleName, isActive, Fleg) {
    
    if (Fleg == true) {
      //alert(Fleg)
      this.IsDeactivatetextDesc = true;
    }
    if (Fleg == false) {
      //alert(Fleg)
      this.IsDeactivatetextDesc = false;
    }
    this.roleID = roleID;
    this.showroleName = roleName;
    if (isActive == 'Active') {
      this.checkrole = false
      this.ActiveDactiveHeaderText = 'Do you want to deactivate role?'
    }
    else {
      this.checkrole = true
      this.ActiveDactiveHeaderText = 'Do you want to  activate role?'
    }
  }

  //Revoke for User
  Revokerole() {
    this.isLoading = true;
    debugger;
    this.MstRole = {
      IsActive: this.checkrole,
      roleID: this.roleID,
      RoleDescription: this.textDeactivateDescription
    };
    JSON.stringify(this.MstRole);
    this._RoleService.RevokeUser(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;

      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        swal(this.Message);
        //this.snackBar.open(this.Message, '', {
        //  duration: 2000,
        //  verticalPosition: 'top',
        //  horizontalPosition: 'center',
        //});
        this.textDeactivateDescription = '';
        this.GetAllDataRollList();
      }
    })
  }
  Close() {
    this.btnUpdatetext = 'Save';
    this.IsDeactivatetextDesc = false;
    this.lbleditrole = 'Create Role';
  }
  charaterOnlyNoSpace(event): boolean {
    // debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  Cancel() {
    this.textDeactivateDescription = '';
    $(".dialog__close-btn").click();
  }
}
