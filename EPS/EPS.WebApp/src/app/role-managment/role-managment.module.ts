import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleManagmentRoutingModule } from './role-managment-routing.module';
import { RoleComponent } from './role/role.component';
import { RoleusermanagmentComponent } from './roleusermanagment/roleusermanagment.component'
//import {DialogComponent } from '../dialog/dialog.component';
import { MenuComponent } from './menu/menu.component'
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MaterialModule } from '../material.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from '../shared-module/shared-module.module';
import { OtherRoleComponent } from './other-role/other-role.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatTooltipModule } from '@angular/material/tooltip';


//import { UiSwitchModule } from 'ngx-toggle-switch';
@NgModule({
  declarations: [RoleComponent, MenuComponent, RoleusermanagmentComponent, OtherRoleComponent],
  imports: [
    CommonModule,
    MatTooltipModule,
    RoleManagmentRoutingModule,
    FormsModule,
    MaterialModule,
    NgMultiSelectDropDownModule,
    SharedModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule
    //UiSwitchModule ,
    
  ]
})
export class RoleManagmentModule { }
