import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as $ from 'jquery';
import { MatTreeNestedDataSource } from '@angular/material/tree'
import { NestedTreeControl } from '@angular/cdk/tree'
//import { Observable, of } from 'rxjs'
import { RoleService } from '../../services/UserManagement/role.service'
import { of } from 'rxjs';
import { MatPaginator, MatTableDataSource, MatSort, ErrorStateMatcher, MatInputModule, MatSnackBar } from '@angular/material';

interface MyTreeNode {
  mainMenuName: string
  children?: MyTreeNode[]
}
@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  //Declare Variable
  displayedColumns: string[] = ['roleName', 'User', 'AssignMenu'];
  displayedUserColumns: string[] = ['FirstName', 'Designation', 'PAN','EmpCd'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  roledataSource: any;
  @ViewChild('roleassign') roleassign: any;
  Isbtnsave: boolean;
  isLoading: boolean;
  currentLesson: string;
  years: any[];
  Username: any;
  isTableHasData = true;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  RoleNameUser: string;
  TxtSearch: string;
  ngOnInit() {
    this.GetAllDataRollList();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'roleID',
      textField: 'roleName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false
    };
  
    //this._RoleService.GetAssignRoleForUser(1).subscribe(result => {
    //  this.ArrAssignRoleForUser = result;
    //  //console.log(this.ArrByPanUserDetail);
    //  this.SlectedRole = this.ArrAssignRoleForUser;//   this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName }, { roleID: 2, roleName: "Controller" },];
    //})
    this.currentLesson = 'true';
  }

  // Declare Variables
  ArrAssignRoleForUser: any;
  ArrUserListdataSource: any;
  setFlag: any;
  MsRoleID: number;
  //ForFlag
  RoleID: any;
  RoleName: any;
  Role: any;
  MstMenu: any;
  textNewrole: any = "";
  checkIsActiveRole: any = true;
  checkRevokeUser: any;
  MstRole: any;
  ArrUserList: any;
  ArrRoles: any = [];
  bodyText: any;
  hdnRoleId = 0;
  Message: any;
  lblPopupText: any;
  btnPopupText: any;
  textSearch: any;
  ArrUserDetails: any;
  ArrByPanUserDetail: any;
  SlectedRole: any;
  selectedYears: any[];
  RoleIDs = '';
  iDsStatus = '';
  ArrAllMainMenu: any;
  ArrBindSubmenu: any = [];
  ArrAssignMenu: any;
  ArrBindSubChildmenu: any = [];
  ArrTreestructureList: any = [];
  SlectedUserRowIndex: any;
  SelectedRole: any;
  showDialog: boolean;
  showrevoke: boolean;
  showAssignRole: boolean;
  showAssignMenu: boolean;
  IsPermission: boolean;
  treeControl: NestedTreeControl<MyTreeNode>;
  treeDataSource: MatTreeNestedDataSource<MyTreeNode>;
  nameRole: any;
  Upan: any;
  roleName: any;
  notFound = true;
  //For Flag

  //End Of Declare Variables
  constructor(private http: HttpClient, private _RoleService: RoleService, private snackBar: MatSnackBar) {

    this.treeControl = new NestedTreeControl<MyTreeNode>(this.makeGetChildrenFunction())
    this.treeDataSource = new MatTreeNestedDataSource();
  }

  hasChildren = (_: number, node: MyTreeNode) => {
    return node.children && node.children.length > 0
  }

  private makeGetChildrenFunction() {
    return node => of(node.children)
  }

  TextChange() {
    this.lblPopupText = 'Create New Role';
    this.btnPopupText = 'Save';
    this.textNewrole = "";
    this.checkIsActiveRole = true;
    this.hdnRoleId = 0;
  }



  resetRoleForm() {
    this.roleassign.resetForm();
  }

  GetAllDataRollList() {
    //alert('hisa')
    //debugger;
    this.isLoading = true;
    debugger;
      this._RoleService.GetAllActiveRollList(sessionStorage.getItem('username')).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrRoles = result;
      this.roledataSource = new MatTableDataSource(result);
      this.roledataSource.paginator = this.paginator;
      this.roledataSource.sort = this.sort;
      this.selectedYears = [
        { id: 1 },
      ]
    })
  }


  SaveNewRole() {
    this.isLoading = true;
    this.MstRole = {
      RoleName: this.textNewrole,
      IsActive: this.checkIsActiveRole,
      RoleID: this.hdnRoleId,
      LogInUserName: sessionStorage.getItem('username'),
      RoleParentID:'4'
    };
    JSON.stringify(this.MstRole);
    this._RoleService.SaveNewRole(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      console.log(result);
      this.Message = result;
      this.GetAllDataRollList();

      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        this.snackBar.open(this.Message, '', {
          duration: 2000,
        });
        
      }
      this.ClearInputRole();
      this.TxtSearch = '';
    })
  }

  GetUserDetails(roleID, roleName)//RowIndex)
  {
    this.isLoading = true;
    this.RoleNameUser = roleName;
    this.SlectedUserRowIndex = roleID;
    this.MstRole = {
      RoleID: roleID,
    };
    this._RoleService.GetUserDetails(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrUserList = result;
      this.ArrUserListdataSource = new MatTableDataSource(result);
      this.ArrUserListdataSource.paginator = this.paginator;
      this.ArrUserListdataSource.sort = this.sort;
      if (this.ArrUserListdataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }

    })
    //alert(this.ArrUserList);
  }

  ClearInputRole() {
    this.textNewrole = '';
    this.checkIsActiveRole = true;
    this.textSearch = '';
    this.ArrByPanUserDetail = null;
    this.textSearch = '';
    this.SlectedRole = '';

  }

  FillRevokeDataOnPopUp(RowIndex) {
    this.Username = this.ArrUserList[RowIndex].firstName;
    this.hdnRoleId = this.ArrUserList[RowIndex].userID;
    if (this.ArrUserList[RowIndex].isActive == 'Active') {
      this.checkRevokeUser = true;
    }
    else {
      this.checkRevokeUser = false;
    }
  }
  //Update For role
  UpdateRole(RowIndex) {
    this.lblPopupText = 'Update Role';
    this.btnPopupText = 'Update';
    this.textNewrole = this.ArrRoles[RowIndex].roleName;
    this.hdnRoleId = this.ArrRoles[RowIndex].roleID;
    if (this.ArrRoles[RowIndex].isActive == '1') {
      this.checkIsActiveRole = true;
    }
    else {
      this.checkIsActiveRole = false;
    }
  }
  //End Update For role

  //Revoke for User
  RevokeUser() {
    this.isLoading = true;
    this.MstRole = {
      IsActive: this.checkRevokeUser,
      UserID: this.hdnRoleId
    };
    JSON.stringify(this.MstRole);
    this._RoleService.RevokeUser(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;

      if (this.Message != undefined) {
        $(".dialog__close-btn").click();
        swal(this.Message)
        this.GetUserDetails(this.SlectedUserRowIndex, null);
      }
    })
  }

  //End Of Revoke for User

  SaveAssignMenuPermission(roleID) {
    this.isLoading = true;
    this.RoleID = roleID;
    this._RoleService.SaveAssignMenuPermission().finally(() => this.isLoading = false).subscribe(result => {
      this.ArrAssignMenu = result;
     })
  }


  Search(Upan, MsRoleID, roleName) {
    //debugger;
    //alert(Upan);
    this.isLoading = true;
    this.setFlag = 'SaveForRole';
    this.MsRoleID = MsRoleID;
    if (MsRoleID != undefined) {
      this.setFlag = 'EditForRole';
      this.SlectedRole = [{ roleID: parseInt(MsRoleID), roleName: roleName },];
    }

    if (MsRoleID == undefined || MsRoleID == '') {
      this._RoleService.GetAssignRoleForUser(this.textSearch).finally(() => this.isLoading = false).subscribe(result => {
        this.ArrAssignRoleForUser = result;

        this.SlectedRole = this.ArrAssignRoleForUser; 
      })

    }
    if (Upan == undefined) {
        if (this.textSearch == undefined || this.textSearch == '') {
        swal("Please Enter The Pan Number..");
        return false;
      }


    }
    else {
      this.textSearch = Upan;
    }
    this.MstRole = {
      Pan: this.textSearch

    };
    this._RoleService.Search(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {

      this.ArrByPanUserDetail = result;
      
    })

  }

  SaveAssignRoleForUser(RowIndex) {
    this.isLoading = true;
    for (var i = 0; i < this.SlectedRole.length; i++) {
      this.RoleIDs = this.RoleIDs + this.SlectedRole[i].roleID + '$';//alert(i);
    }

    this.MstRole = {
      RoleIDs: this.RoleIDs,
      UserID: this.ArrByPanUserDetail[RowIndex].userID,
      setFlag: this.setFlag,
      MsRoleID: this.MsRoleID
    };

    this._RoleService.SaveAssignRoleForUser(this.MstRole).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;
      this.SlectedRole = null;
      this.RoleIDs = null;
      if (this.Message != undefined) {
        this.ClearInputRole();
        $(".dialog__close-btn").click();
        this.GetUserDetails(this.SlectedUserRowIndex, null);
        swal(this.Message)


      }
    })
  }



  searchUserListOnKeyUp(event: Event) {
    $("#UserInput").on("keyup", function () {
      var value = $(this).val().toLowerCase();
      $("#DivUserList tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  }


  radioSelected = "";
  //Tree structure For Test
  TreestructureList(roleID, RoleName, roleStatus) {
    if (roleStatus == 'p') {
      this.Isbtnsave = false;
      this.IsPermission = true;
    }
    else {
      this.Isbtnsave = true;
      this.IsPermission = false;
    }
    this.isLoading = true;
    this.nameRole = RoleName;
    this._RoleService.TreestructureList(roleID).finally(() => this.isLoading = false).subscribe(result => {
      this.ArrTreestructureList = result;
      this.treeControl = new NestedTreeControl<MyTreeNode>(this.makeGetChildrenFunction())
      this.treeDataSource = new MatTreeNestedDataSource()
      this.treeDataSource.data = this.ArrTreestructureList;

    })
  }
  //End Of Trss Structure


  SaveMenuPerMission() {
    //debugger;
    //alert(' call'+this.textSearch)
    this.textSearch = '';
    this.isLoading = true;
    var iDsStatus = "";
    var iDsStatusarr = [];
    var RolID = this.RoleID
    $(".rdo-btn  input[type=radio]:checked").each(function (i) {
      iDsStatusarr[i] = this.value + '_' + RolID;
    });
    this._RoleService.SaveMenuPerMission(iDsStatusarr).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        swal(this.Message)       
        $(".dialog__close-btn").click();       
        this.GetAllDataRollList();       
      }
    })

  }

  RdValueSet(Rdstatus, value) {
    debugger;
    this.isLoading = true;
    var id = Rdstatus + "/" + value;
    var currentid = Rdstatus + "_" + value;
    var iDsStatus = "";
    var iDsStatusarr = [];
    var RolID = this.RoleID
    $(".rdo-btn  input[type=radio]:checked").each(function (i) {
      iDsStatusarr[i] = this.value + '_' + id;
    });

    var data = {
      id: id,
      iDsStatusarr: iDsStatusarr
    };
    var Status = iDsStatusarr;
    var currentstatus = '';
    this._RoleService.CheckMenuPerMission(iDsStatusarr).finally(() => this.isLoading = false).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined && this.Message != "") {
        var prevstatus = $("#" + Rdstatus).val();
        $("#" + Rdstatus + "_" + prevstatus).prop("checked", true);
        swal(this.Message)
      }
      else {
        $("#" + Rdstatus).val(value);
      }
    })
  }


  applyFilter(filterValue: string) {
    this.roledataSource.filter = filterValue.trim().toLowerCase();
    if (this.roledataSource.paginator) {
      this.roledataSource.paginator.firstPage();
    }
    if (this.roledataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  UserapplyFilter(filterValue: string) {
    //if (this.ArrUserListdataSource == null) {
    //  this.isTableHasData = false;
    //}
    this.ArrUserListdataSource.filter = filterValue.trim().toLowerCase();
    if (this.ArrUserListdataSource.paginator) {
      this.ArrUserListdataSource.paginator.firstPage();
    }
    if (this.ArrUserListdataSource.filteredData.length > 0) {
      this.isTableHasData = true;
    } else {
      this.isTableHasData = false;
    }
  }


  Cancel() {

    this.textSearch = '';
    this.SlectedRole = null;
    this.ArrAssignRoleForUser = null;
    this.ArrByPanUserDetail = null;
    $(".dialog__close-btn").click();
  }


  charaterOnlyNoSpace(event): boolean {
    // debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

}

