import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleusermanagmentComponent } from './roleusermanagment.component';

describe('RoleusermanagmentComponent', () => {
  let component: RoleusermanagmentComponent;
  let fixture: ComponentFixture<RoleusermanagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleusermanagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleusermanagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
