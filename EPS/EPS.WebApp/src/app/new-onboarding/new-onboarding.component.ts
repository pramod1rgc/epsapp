import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { controllerservice } from '../services/UserManagement/controller.service';
import { OnBoardingService } from '../services/UserManagement/OnBoarding.service';
import { OnBoardingModel } from '../model/UserManagementModel/OnBoardingModel';
import { MatSnackBar } from '@angular/material';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface commondll {
  values: string;
  text: string;
}


@Component({
  selector: 'app-new-onboarding',
  templateUrl: './new-onboarding.component.html',
  styleUrls: ['./new-onboarding.component.css']
})
 
export class NewOnboardingComponent implements OnInit {


  constructor(private _Service: controllerservice, private Service: OnBoardingService, private snackBar: MatSnackBar, private router: Router) { }

  public controllerCtrl: FormControl = new FormControl();
  public controllerFilterCtrl: FormControl = new FormControl();
  public filteredcontroller: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  private _onDestroy = new Subject<void>();
  @ViewChild('OnBoarding') OnBoardingValues: any;

  // properties
  controllerroleList: any[];
  Query: any;
  ControllerID: any;
  PAOID: any;
  DDOID: any;
  PAOList: any = [];
  DDOList: any = [];
  ObjOnBoarding: OnBoardingModel;
  result: any;

  ngOnInit() {
    this.ObjOnBoarding = new OnBoardingModel();
    this.getallcontrollers(sessionStorage.getItem('controllerID'));
    this.controllerFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filtercontroller(); });
  }

  //Search functionality
  private filtercontroller() {
    if (!this.controllerroleList) {
      return;
    }
    // get the search keyword
    let search = this.controllerFilterCtrl.value;
    if (!search) {
      this.filteredcontroller.next(this.controllerroleList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredcontroller.next(
      this.controllerroleList.filter(controllerroleList => controllerroleList.text.toLowerCase().indexOf(search) > -1)
    );
  }

  //For controller fetch
  getallcontrollers(ControllerID) {
    
      this.Query = 'getController';
      this._Service.getAllControllerServiceclient(ControllerID, this.Query).subscribe(data => {
        this.controllerroleList = data;
        this.controllerCtrl.setValue(this.controllerroleList);
        this.filteredcontroller.next(this.controllerroleList);
      });
  }

  ControllerChnage(MsControllerID) {
    this.ControllerID = MsControllerID;
    if (this.ControllerID == null) {
      alert("Please select Controller")
      return;
    }
    this.getallpaos(MsControllerID);
  }

  getallpaos(ControllerID) {
    this.DDOList = null;
    this.Query = 'getPAO';
    this._Service.GetAllpaos(ControllerID, this.Query).subscribe(data => {
      this.PAOList = data;
    });
  }

  PAOSelectchanged(PAOID) {
    this.DDOList = null;
    this.PAOID = PAOID;
    this.GetAllDDO(PAOID);
  }
  GetAllDDO(PAOID) {
    this.Query = 'getDDO';
    this._Service.GetAllDDO(PAOID, this.Query).subscribe(data => {
      this.DDOList = data;
    });
  }

  OnBoardingSubmit(ObjOnBoarding) {
    ObjOnBoarding.ControllerID = this.ControllerID;
    ObjOnBoarding.PAOID = this.PAOID;
    ObjOnBoarding.DDOID = this.DDOID;
    debugger;
    this.Service.OnBoardingSubmit(ObjOnBoarding).subscribe(data => {
      debugger;
      this.result = data;
      if (this.result == '1') {
        swal('Submitted to EPS Admin for approval');
        this.router.navigate(['login']);
      }
      if (this.result == '0') {
        swal('On-Boarding request already exist');
      }
      this.resetOnBoardingForm();
    });
  }

  DDOSelectChange(DDOID) {
    this.DDOID = DDOID;
  }

  resetOnBoardingForm() {
    this.OnBoardingValues.resetForm();
  }

  Reset() {
    this.resetOnBoardingForm();
    this.router.navigate(['login']);
  }

  RedirectToLogin() {
    this.router.navigate(['login']);
  }

}
