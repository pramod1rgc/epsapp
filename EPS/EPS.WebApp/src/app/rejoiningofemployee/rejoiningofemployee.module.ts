import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RejoiningofemployeeRoutingModule } from './rejoiningofemployee-routing.module';
import { RejoiningofemployeeComponent } from './rejoiningofemployee/rejoiningofemployee.component';
import { from } from 'rxjs';
@NgModule({
  declarations: [RejoiningofemployeeComponent, RejoiningofEmployeeHeaderComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RejoiningofemployeeRoutingModule,
    FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule, SharedModule
  ],
  providers: [CommonMsg]
})
export class RejoiningofemployeeModule { }
import { Component, ViewEncapsulation } from '@angular/core';
import { RejoiningofEmployeeHeaderComponent } from './rejoiningof-employee-header/rejoiningof-employee-header.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommonMsg } from '../global/common-msg';



