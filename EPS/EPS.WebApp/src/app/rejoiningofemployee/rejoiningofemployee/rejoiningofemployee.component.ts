import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective } from '@angular/forms';
import { MatSnackBar, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { RejoiningOfEmployeeService } from '../../services/rejoiningofEmployee/rejoiningofEmployee.service';
import * as $ from 'jquery';
import { CommonMsg } from '../../global/common-msg';


@Component({
  //selector: 'app-rejoiningofemployee',
  templateUrl: './rejoiningofemployee.component.html',
  styleUrls: ['./rejoiningofemployee.component.css']
})
export class RejoiningofemployeeComponent implements OnInit {
  displayedColumns: string[] = ['OrderNo', 'OrderDate', 'EmployeeType', 'EndofServiceDate', 'PANNo', 'Status', 'Action'];
  dataSource: MatTableDataSource<any>;
  showDialog: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;


  employeeDetails: any;

  rejoiningForm: FormGroup;

  pageSize: number = 5;
  pageNumber: number = 1;
  totalCount: number = 0;
  searchTerm: string = '';
  ddoId: any;
  roleId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;

  elementToBeDeleted: any;

  reasonForRej: string;
  editMode: boolean = false;
  infoMode: boolean = false;

  constructor(private _service: RejoiningOfEmployeeService, private snackBar: MatSnackBar, private _msg: CommonMsg) {
    this.ddoId = sessionStorage.getItem('ddoid');
    this.roleId = sessionStorage.getItem('userRoleID');
  }


  createForm() {
    this.rejoiningForm = new FormGroup({
      msEmpReinstateDetID: new FormControl(0),
      isPreviousGovService: new FormControl('n'),
      empCd: new FormControl(''),
      empId: new FormControl(0),
      empRejoiningDate: new FormControl(''),
      empJoiningTime: new FormControl(''),
      pensionAmount: new FormControl(0),
      reJoiningEmployeeType: new FormControl(''),
      reJoiningEmployeeTypeId: new FormControl(0),
      toDDOId: new FormControl(this.ddoId),
      daTakenFrom: new FormControl(''),
      nocOrderNo: new FormControl(''),
      nocOrderDt: new FormControl(''),
      orderNo: new FormControl(''),
      orderDt: new FormControl(''),
      remark: new FormControl(''),
      verifFlag: new FormControl(''),
      empPanNo: new FormControl(''),
      endServDt: new FormControl(''),
      empName: new FormControl(''),
      empGender: new FormControl(''),
      empDOB: new FormControl(''),
      empEndReason: new FormControl(''),
      empTypeID: new FormControl(0),
      currentType: new FormControl(''),
      reasonForRej: new FormControl('')
    });
    if (this.roleId == '5') {
      this.rejoiningForm.disable();
    }
  }

  setEmployeeDetails(employee: any) {
    this.employeeDetails = employee;
    if (this.roleId == '6') {
      this.rejoiningForm.enable();
    }
    this.createForm();
    this.editMode = false;
    this.infoMode = false;
    this.rejoiningForm.controls.empCd.setValue(this.employeeDetails.empCd);
    this.rejoiningForm.controls.empId.setValue(this.employeeDetails.msEmpId);
    this.rejoiningForm.controls.empPanNo.setValue(this.employeeDetails.empPanNo);
    this.rejoiningForm.controls.endServDt.setValue(this.employeeDetails.endServDt);
    this.rejoiningForm.controls.empName.setValue(this.employeeDetails.empName);
    this.rejoiningForm.controls.empGender.setValue(this.employeeDetails.empGender);
    this.rejoiningForm.controls.empDOB.setValue(this.employeeDetails.empDOB);
    this.rejoiningForm.controls.empEndReason.setValue(this.employeeDetails.empEndReason);
    this.rejoiningForm.controls.empTypeID.setValue(this.employeeDetails.empApptTypeId);
    this.rejoiningForm.controls.currentType.setValue(this.employeeDetails.empType);
    this.getRejoiningDetails();
  }

  setPreviousJoiningDetails(evnt) {
    if (evnt.value == 'y') {
      if (this.employeeDetails) {
        this.rejoiningForm.controls.empRejoiningDate.setValue(this.employeeDetails.joiningDate);
        this.rejoiningForm.controls.empJoiningTime.setValue(this.employeeDetails.joiningTiming);
        this.rejoiningForm.controls.reJoiningEmployeeTypeId.setValue(this.employeeDetails.empApptTypeId.toString());
      }
    }
    else {
      this.rejoiningForm.controls.empRejoiningDate.setValue('');
      this.rejoiningForm.controls.empJoiningTime.setValue('');
      this.rejoiningForm.controls.reJoiningEmployeeTypeId.setValue(0);
    }
  }

  onSubmit() {
    this._service.UpsertReJoiningServiceDetails(this.rejoiningForm.value).subscribe(result => {
      let response = result as number;
      if (response) {
        if (response > 0) {
          this.rejoiningForm.reset();
          this.snackBar.open(this._msg.saveMsg, null, { duration: 4000 });
          this.formGroupDirective.resetForm();
          this.createForm();
          this.getRejoiningDetails();
        }
        if (response == -1) {
          this.snackBar.open(this._msg.saveFailedMsg, null, { duration: 4000 });
        }
      }
    });
  }

  getRejoiningDetails() {
    this.totalCount = 0;
    this._service.GetReJoiningServiceEmployees(this.pageNumber, this.pageSize, this.searchTerm, this.employeeDetails.empCd, this.roleId).subscribe(response => {
      if (response.length > 0) {
        this.dataSource = new MatTableDataSource(response);
        this.totalCount = response[0].totalCount;
        this.dataSource.sort = this.sort;
      }
    });
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    console.log(this.pageNumber);
    this.getRejoiningDetails();
  }

  cancelForm() {
    this.rejoiningForm.enable();
    this.rejoiningForm.reset();
    this.formGroupDirective.resetForm();
    this.createForm();
   
    this.employeeDetails = null;
    this.elementToBeDeleted = null;
    this.editMode = false;
    this.infoMode = false;
  }

  editForm(element) {
    this.rejoiningForm.setValue({
      msEmpReinstateDetID: element.msEmpReinstateDetID,
      isPreviousGovService: element.isPreviousGovService,
      empCd: element.empCd,
      empId: element.msEmpID,
      empRejoiningDate: element.empRejoiningDt,
      empJoiningTime: element.empJoiningTime,
      pensionAmount: element.pensionAmount,
      reJoiningEmployeeType: element.relApptType,
      reJoiningEmployeeTypeId: element.reEmpTypeID.toString(),
      toDDOId: this.ddoId,
      daTakenFrom: element.daTakenFrom,
      nocOrderNo: element.nocOrderNo,
      nocOrderDt: element.nocOrderDt,
      orderNo: element.orderNo,
      orderDt: element.orderDt,
      remark: element.remark,
      verifFlag: element.verifFlag,
      empPanNo: element.empPanNo,
      endServDt: element.endServDt,
      empName: element.empName,
      empGender: element.empGender,
      empDOB: element.empDOB,
      empEndReason: element.empEndReason,
      empTypeID: element.empTypeID,
      currentType: element.currentType,
      reasonForRej: element.reasonForRej
    });
    this.rejoiningForm.enable();
    this.editMode = true;
    this.infoMode = false;
  }

  formInfo(element) {
    this.rejoiningForm.setValue({
      msEmpReinstateDetID: element.msEmpReinstateDetID,
      isPreviousGovService: element.isPreviousGovService,
      empCd: element.empCd,
      empId: element.msEmpID,
      empRejoiningDate: element.empRejoiningDt,
      empJoiningTime: element.empJoiningTime,
      pensionAmount: element.pensionAmount,
      reJoiningEmployeeType: element.relApptType,
      reJoiningEmployeeTypeId: element.reEmpTypeID.toString(),
      toDDOId: this.ddoId,
      daTakenFrom: element.daTakenFrom,
      nocOrderNo: element.nocOrderNo,
      nocOrderDt: element.nocOrderDt,
      orderNo: element.orderNo,
      orderDt: element.orderDt,
      remark: element.remark,
      verifFlag: element.verifFlag,
      empPanNo: element.empPanNo,
      endServDt: element.endServDt,
      empName: element.empName,
      empGender: element.empGender,
      empDOB: element.empDOB,
      empEndReason: element.empEndReason,
      empTypeID: element.empTypeID,
      currentType: element.currentType,
      reasonForRej: element.reasonForRej
    });
    this.rejoiningForm.disable();
    this.infoMode = true;
    this.editMode = false;
  }

  DeleteRejoiningDetails(element) {
    this.elementToBeDeleted = element;
  }

  Cancel() {
    $(".dialog__close-btn").click();
    this.reasonForRej = '';
  }

  confirmDelete() {
    var obj = {
      msEmpReinstateDetID: this.elementToBeDeleted.msEmpReinstateDetID,
      empCd: this.elementToBeDeleted.empCd,
      toDDOId: this.ddoId,
      ipAddress: ""
    }
    this._service.DeleteReJoiningServiceDetails(obj).subscribe(result => {
      this.snackBar.open(this._msg.deleteMsg, null, { duration: 4000 });
      this.pageNumber = 1;
      this.paginator.firstPage();
      this.dataSource.paginator = this.paginator;
      this.getRejoiningDetails();
      $(".dialog__close-btn").click();
      this.reasonForRej = '';
      this.elementToBeDeleted = null;
      this.cancelForm();
    });
  }

  changeStatus(status) {
    var obj = {
      msEmpReinstateDetID: this.rejoiningForm.controls.msEmpReinstateDetID.value,
      empCd: this.rejoiningForm.controls.empCd.value,
      toDDOId: this.ddoId,
      ipAddress: "",
      status: status,
      reason: this.reasonForRej
    }

    this._service.UpdateReJoiningStatusDetails(obj).subscribe(response => {
      this.rejoiningForm.reset();
      this.snackBar.open(this._msg.updateMsg, null, { duration: 4000 });
      this.formGroupDirective.resetForm();
      this.createForm();
      this.getRejoiningDetails();
      $(".dialog__close-btn").click();
      this.reasonForRej = '';
    });
  }

  applyFilter() {
    this.pageNumber = 1;
    this.getRejoiningDetails();
    this.paginator.firstPage();
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.createForm();
  }
}
//export interface UserData {
//  OrderNo: number;
//  OrderDate: number;
//  EmployeeType: string;
//  EndofServiceDate: number;
//  PANNo: number;

//}
///** Constants used to fill up our data base. */

//const NAMES: UserData[] = [
//  { OrderNo: 1, OrderDate: 2, EmployeeType: '3', EndofServiceDate: 4, PANNo: 343 },
//  { OrderNo: 34, OrderDate: 56, EmployeeType: 'kasjdlaf', EndofServiceDate: 56, PANNo: 556 },
//  { OrderNo: 1, OrderDate: 77, EmployeeType: 'asdfladk', EndofServiceDate: 77, PANNo: 34 },
//  { OrderNo: 777, OrderDate: 2, EmployeeType: 'jjoolkkk', EndofServiceDate: 11, PANNo: 34343 },
//  { OrderNo: 55, OrderDate: 45, EmployeeType: '3', EndofServiceDate: 33, PANNo: 333 },
//  { OrderNo: 33, OrderDate: 12, EmployeeType: '3', EndofServiceDate: 12, PANNo: 3434222 },
//  { OrderNo: 888, OrderDate: 21, EmployeeType: '3', EndofServiceDate: 88, PANNo: 34322 }

//];

/**
 * @title Data table with sorting, pagination, and filtering.
 */



/** Builds and returns a new User. */

