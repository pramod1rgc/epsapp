import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejoiningofemployeeComponent } from './rejoiningofemployee.component';

describe('RejoiningofemployeeComponent', () => {
  let component: RejoiningofemployeeComponent;
  let fixture: ComponentFixture<RejoiningofemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejoiningofemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejoiningofemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
