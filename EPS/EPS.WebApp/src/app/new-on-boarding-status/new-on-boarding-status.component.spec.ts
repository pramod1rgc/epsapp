import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOnBoardingStatusComponent } from './new-on-boarding-status.component';

describe('NewOnBoardingStatusComponent', () => {
  let component: NewOnBoardingStatusComponent;
  let fixture: ComponentFixture<NewOnBoardingStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOnBoardingStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOnBoardingStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
