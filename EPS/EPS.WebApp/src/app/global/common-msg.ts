export class CommonMsg {
  /* common messages use in application */
  saveMsg: string = 'Record saved successfully.';
  updateMsg: string = 'Record updated successfully.';
  deleteMsg: string = 'Record deleted successfully.';
  alreadyExistMsg: string = 'Record already exist.';
  noRecordMsg: string = 'No Record Found.';
  enterCorrectValueMsg: string = 'Please enter correct value.';
  checkDeclarationMsg: string = 'Please check Declaration.';
  forwardHooMakerMsg: string = 'Forward to HOO Maker';
  forwardHooCheckerMsg: string = 'Forward to HOO Checker';
  updateFailedMsg: string = 'Failed to update Record.';
  alreadyForwardedMsg: string = 'Record Already Forwarded';
  deleteFailedMsg: string = 'Failed to Delete Record.';
  forwardCheckerMsg: string = 'Record Forwarded to Checker Successfully';
  DDOVerifiedByChecker: string = 'Record Verified Successfully';
  DDORejectedByChecker: string = 'Record Rejected Successfully';
  AttachAtleastOne: string = 'Please attach at least one record';
  DeAttachAtleastOne: string = 'Please DeAttach at least one Employee';
  saveFailedMsg: string = 'Failed to Save Record.';
  formatfileMsg: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  pdffileMsg: string = 'only pdf file upload';
  apppdffileMsg: string = 'application/pdf';
  errorMsg: string = 'An error has occurred. Please contact your system administrator.';
  apiErrorMsg: string = 'server api error occure.';
  messageTimer: number = 15000;
  /* common messages use in application */

  /* New Loan message */
  amountLessEqualloanAmountMsg: string = 'Entered Amount should be less than or equal to Loan Amount';
  amountActuallyPaidMsg: string = 'Entered Actually Paid Amount should be less than or equal to Cost Amount'
  loanAmountGreaterthenInstallMsg: string = 'Loan Amount should be greater than installment Amount';
  maximumLimitLoanAmountMsg: string = 'Maximum Limit for Loan Amount is Rs. 25 Lakhs';
  maxLimitLoanAmountMsg: string = 'Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower';
  maxLimitLoanAmount1LK80Msg: string = 'Maximum Limit for Loan Amount is Rs. 1,80,000.00';
  maxLimitLoanAmount2LK50Msg: string = 'Maximum Limit for Loan Amount is Rs. 2,50,000.00';
  costGreateThenAmountActuallyPaid: string = 'Cost should be greater then Amount actually paid.';
  noInstallmentsRepaymentAmountshouldbelessthanorequal150: string = 'No. of Installments for Repayment Amount should be less than or equal 150.';
  VerifyedByChecker: string = 'Verifyed by DDO Checker';
  RejectedByChecker: string = 'Rejected by DDO Checker';
  ForwardToDDOMaker: string = 'Forward to DDO Maker';
  ForwardToDDOChecker: string = 'Forward to DDO Checker';
  VerifyDDOChecker: string = 'Verify DDO Checker';
  VerifyHOOChecker: string = 'Verified HOO checker';
  Rejected: string = 'Rejected';
  PermanentEmpeApplyLoan: string = 'only Permanent employee apply for the loan';
  AmountShouldbelessthanRequestedAmount: string = 'Amount should be less than Amount requested';
  AccountNumberNotMatched: string = 'Account Number not Matched Please try Again.';
  forwardDDOCheckerMsg: string = 'Record Forwarded to DDO Checker Successfully';
  forwardDDOCheckerFailedMsg: string = 'Failed to Forwarded Record to DDO Checker';
  coolingPeriodMsg: string = 'Minimum cooling period is 3 years for computer advance loan';
  EmpCdSelectMsg: string = 'Please Select Employee';
  sessionExpiredMsg: string = 'Your session has been expired!';
  /* New Loan message */

  /*Deputation  */
  depRepaAlreadyExits: string = 'Repatriation Order Number  Already Exits';
  /*Deputation  */

  /*   User Management/Payroll  */
  getController: string = 'getController';
  getPAO: string = 'getPAO';
  getDDO: string = 'getDDO';
  Controller: string = 'Controller';
  USN: string = 'Unassigned Successfully';
  ASN: string = 'Assigned Successfully';
  EmailMsg: string = 'Assigned successfully,Role activation link has been sent to your email.';
  dDOAdmin: string = 'DDO Admin';
  HOOChecker: string = 'HOO Checker';
  HOOMaker: string = 'HOO Maker';
  DDOChecker: string = 'DDO Checker';
  DDOMaker: string = 'DDO Maker'
  PAO: string = 'PAO';

  /*   User Management/Payroll  */

  /* Common Component Message */
  billGroupNotFoundMsg: string = 'Pay Bill Group is not found';
  desgNotFoundMsg: string = 'Designation is not found';
  empNotFoundMsg: string = 'Employee is not found';
  numberNotAllowed: string = 'Number Not Allowed';
  /* Common Component Message */


  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }


}
