"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var commons = /** @class */ (function () {
    function commons() {
    }
    commons.prototype.SortArrayAlphabetically = function (data, column, order) {
        if (order == 'desc') {
            return data.sort(function (a, b) {
                if (a[column] > b[column]) {
                    return -1;
                }
                if (a[column] < b[column]) {
                    return 1;
                }
                return 0;
            });
        }
        else {
            return data.sort(function (a, b) {
                if (a[column] < b[column]) {
                    return -1;
                }
                if (a[column] > b[column]) {
                    return 1;
                }
                return 0;
            });
        }
    };
    commons.prototype.SortArrayIntegerwise = function (data, column, order) {
        if (order == 'desc') {
            return data.sort(function (a, b) { return b[column] - a[column]; });
        }
        else {
            return data.sort(function (a, b) { return a[column] - b[column]; });
        }
    };
    return commons;
}());
exports.commons = commons;
//# sourceMappingURL=commons.js.map