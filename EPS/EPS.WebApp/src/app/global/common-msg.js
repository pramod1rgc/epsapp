"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonMsg = /** @class */ (function () {
    function CommonMsg() {
        /* common messages use in application */
        this.saveMsg = 'Records has been saved successfully.';
        this.updateMsg = 'Record has been updated successfully.';
        this.deleteMsg = 'Record has been deleted successfully.';
        this.alreadyExistMsg = 'Record Already Exist.';
        this.noRecordMsg = 'No Record Found.';
        this.enterCorrectValueMsg = 'Please Enter correct value.';
        this.checkDeclarationMsg = 'Please check Declaration.';
        this.forwardHooMakerMsg = 'Forward to HOO Maker';
        this.forwardHooCheckerMsg = 'Forward to HOO Checker';
        this.updateFailedMsg = 'Failed to update Record.';
        this.alreadyForwardedMsg = 'Record Already Forwarded';
        this.deleteFailedMsg = 'Failed to Delete Record.';
        this.forwardCheckerMsg = 'Record Forwarded to Checker Successfully';
        this.DDOVerifiedByChecker = 'Record Verified Successfully';
        this.DDORejectedByChecker = 'Record Rejected Successfully';
        this.AttachAtleastOne = 'Please attach at least one record';
        this.DeAttachAtleastOne = 'Please DeAttach at least one Employee';
        this.saveFailedMsg = 'Failed to Save Record.';
        this.formatfileMsg = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.pdffileMsg = 'only pdf file upload';
        this.apppdffileMsg = 'application/pdf';
        this.errorMsg = 'An error has occurred. Please contact your system administrator.';
        /* common messages use in application */
        /* New Loan message */
        this.amountLessEqualloanAmountMsg = 'Entered Amount should be less than or equal to Loan Amount';
        this.amountActuallyPaidMsg = 'Entered Actually Paid Amount should be less than or equal to Cost Amount';
        this.loanAmountGreaterthenInstallMsg = 'Loan Amount should be greater than installment Amount';
        this.maximumLimitLoanAmountMsg = 'Maximum Limit for Loan Amount is Rs. 25 Lakhs';
        this.maxLimitLoanAmountMsg = 'Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower';
        this.maxLimitLoanAmount1LK80Msg = 'Maximum Limit for Loan Amount is Rs. 1,80,000.00';
        this.maxLimitLoanAmount2LK50Msg = 'Maximum Limit for Loan Amount is Rs. 2,50,000.00';
        this.costGreateThenAmountActuallyPaid = 'Cost should be greater then Amount actually paid.';
        this.noInstallmentsRepaymentAmountshouldbelessthanorequal150 = 'No. of Installments for Repayment Amount should be less than or equal 150.';
        this.VerifyedByChecker = 'Verifyed by DDO Checker';
        this.RejectedByChecker = 'Rejected by DDO Checker';
        this.ForwardToDDOMaker = 'Forward to DDO Maker';
        this.ForwardToDDOChecker = 'Forward to DDO Checker';
        this.VerifyDDOChecker = 'Verify DDO Checker';
        this.VerifyHOOChecker = 'Verified HOO checker';
        this.Rejected = 'Rejected';
        this.PermanentEmpeApplyLoan = 'only Permanent employee apply for the loan';
        this.AmountShouldbelessthanRequestedAmount = 'Amount should be less than Amount requested';
        this.AccountNumberNotMatched = 'Account Number not Matched Please try Again.';
        this.forwardDDOCheckerMsg = 'Record Forwarded to DDO Checker Successfully';
        this.forwardDDOCheckerFailedMsg = 'Failed to Forwarded Record to DDO Checker';
        /* New Loan message */
        /*Deputation  */
        this.depRepaAlreadyExits = 'Repatriation Order Number  Already Exits';
        /*Deputation  */
        /*   User Management/Payroll  */
        this.getController = 'getController';
        this.getPAO = 'getPAO';
        this.getDDO = 'getDDO';
        this.Controller = 'Controller';
        this.USN = 'Unassigned Successfully';
        this.ASN = 'Assigned Successfully';
        this.EmailMsg = 'Assigned successfully,Role activation link has been sent to your email.';
        this.dDOAdmin = 'DDO Admin';
        this.HOOChecker = 'HOO Checker';
        this.HOOMaker = 'HOO Maker';
        this.DDOChecker = 'DDO Checker';
        this.DDOMaker = 'DDO Maker';
        this.PAO = 'PAO';
        /*   User Management/Payroll  */
        /* Common Component Message */
        this.billGroupNotFoundMsg = 'Pay Bill Group is not found';
        this.desgNotFoundMsg = 'Designation is not found';
        this.empNotFoundMsg = 'Employee is not found';
        this.numberNotAllowed = 'Number Not Allowed';
    }
    /* Common Component Message */
    CommonMsg.prototype.charaterOnlyNoSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    return CommonMsg;
}());
exports.CommonMsg = CommonMsg;
//# sourceMappingURL=common-msg.js.map