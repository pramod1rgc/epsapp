import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private titleService: Title) { }
  username: any;
  userRole: any;
  ngOnInit() {
    this.username = sessionStorage.getItem('username');
    this.userRole =  sessionStorage.getItem('userRole');
  }
  LogOut() {
    sessionStorage.clear();
    this.titleService.setTitle('EPS');
    //alert('LogOut');
    this.router.navigate(['/login'])
  }
}
