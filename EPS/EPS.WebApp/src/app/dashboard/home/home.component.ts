import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  dashboard: any = {};
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getDashboardDetail();
  }
  getDashboardDetail() {
  //  alert(sessionStorage.getItem('UserID'));
    this.dashboardService.getDashboardDetail().subscribe(res => {
      this.dashboard = res[0];
    })
  }
}
