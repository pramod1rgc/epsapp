import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeModule } from './employee.module';
import { EmployeeDetails1Component } from './employee-details1/employee-details1.component';
import { EmployeeDetails2Component } from './employee-details2/employee-details2.component';
import {ForwardToCheckerComponent} from './forward-to-checker/forward-to-checker.component';

const routes: Routes = [
  {
    path: '', component: EmployeeModule, children: [
      { path: 'empdetails1', component: EmployeeDetails1Component  },
      { path: 'empdetails2', component: EmployeeDetails2Component  },
      { path: 'forwardtochecker', component: ForwardToCheckerComponent  },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
