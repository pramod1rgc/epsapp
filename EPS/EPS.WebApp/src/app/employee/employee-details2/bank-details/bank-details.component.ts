import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MasterService } from '../../../services/master/master.service';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription, ReplaySubject, Subject } from 'rxjs';
import { BankDetailsService } from '../../../services/empdetails/bank-details.service';
import { PayBillGroupService } from '../../../services/PayBill/PayBillService';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';
interface IfscCode {
  id: string;
  name: string;
}
@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  objBankDetails: any = {};
  objAllBankDetails: any = {};
  BankesInfo: any[];
 @ViewChild('Bank') BankForm: any;
  objAllIFSCCode: any = {};
  Ifsc: any[];
  public ifscCtrl: FormControl = new FormControl();
  public ifscFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredIfsc: ReplaySubject<IfscCode[]> = new ReplaySubject<IfscCode[]>(1);

  constructor(private snackBar: MatSnackBar, private objEmpGetCodeService: GetempcodeService,
    private objPayBillGroupService: PayBillGroupService, private commonMsg: CommonMsg,
    private master: MasterService, private objBankService: BankDetailsService,
  ) {
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex === 7) {  this.getBankDetails(commonEmpCode.empcode)} });
  }

  ngOnInit() {
     this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.getAllIFSC();
    this.ifscFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterIfsc();
      });
     
  }
  private filterIfsc() {
    if (!this.Ifsc) {
      return;
    }
    let search = this.ifscFilterCtrl.value;
    if (!search) {
      this.filteredIfsc.next(this.Ifsc.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredIfsc.next(

      this.Ifsc.filter(Ifsc => Ifsc.ifscCD.toLowerCase().indexOf(search) > -1)
    );
  }
  async getBankDetails(empCd: string) {
    this.getEmpCode = empCd;
    this.resetBankDetailsForm();
    await  this.objBankService.getBankDetails(empCd ,this.roleId).subscribe(res => {
      this.getBankDetailsByIFSC(res.ifscCD);
      setTimeout(() => {
        this.objBankDetails = res;
        this.objAllBankDetails.ifscCD1 = this.objBankDetails.ifscCD;
        this.objAllBankDetails.bankName1 = this.objBankDetails.bankName;
        this.objAllBankDetails.branchName1 = this.objBankDetails.branchName;
        this.objAllBankDetails.bnkAcNo1 = this.objBankDetails.bnkAcNo;
        if (this.objBankDetails.ifscCD === '' || this.objBankDetails.ifscCD == null) {
          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }
    }, 1000);
      });
  }
  getBankDetailsByIFSC(ifscCD: string) {
    debugger;
   this.objPayBillGroupService.GetBankdetailsByIfsc(ifscCD).subscribe(res => {
      // debugger;
      this.objBankDetails.bankId = '';
        this.objBankDetails.branchId = '';
       this.BankesInfo = res;
       });
  }

  resetBankDetailsForm() {
    this.BankForm.resetForm();
  }

  UpdateBankDetails() {
    if (this.getEmpCode === undefined) {
      alert('Please select any employee');

    } else {
      this.objBankDetails.EmpCD = this.getEmpCode;
      this.objBankDetails.VerifyFlag = this.VerifyFlag;
      this.objBankDetails.UserId = this.UserId;
      this.objBankService.UpdateBankDetails(this.objBankDetails).subscribe(result => {
        console.log(result);
        // tslint:disable-next-line:radix
        if (parseInt(result) >= 1) {
          this.getBankDetails(this.getEmpCode);
          swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }
  }

  getAllIFSC() {
    debugger;
    this.objBankService.getAllIFSC().subscribe(res => {
      this.objAllIFSCCode = res;
      this.Ifsc = res;
      this.ifscCtrl.setValue(this.Ifsc);
      this.filteredIfsc.next(this.Ifsc);
    });
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


}
