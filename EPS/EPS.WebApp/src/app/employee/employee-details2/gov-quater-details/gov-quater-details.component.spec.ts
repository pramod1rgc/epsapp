import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovQuaterDetailsComponent } from './gov-quater-details.component';

describe('GovQuaterDetailsComponent', () => {
  let component: GovQuaterDetailsComponent;
  let fixture: ComponentFixture<GovQuaterDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovQuaterDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovQuaterDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
