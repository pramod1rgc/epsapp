import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material';
import { EmpdetailsService } from '../../services/empdetails/empdetails.service';
import { EmployeeDetailsModel } from '../../model/empdetails';

import { MatSelect } from '@angular/material';
import {Subject } from 'rxjs';
//import { Subject } from 'rxjs/Subject';
import { DeputationinService } from '../../services/deputation/deputationin.service';
import {GetempcodeService} from '../../services/empdetails/getempcode.service';
import { takeUntil } from 'rxjs/operators';
interface EMCODE {
  id: string;
  name: string;
}
@Component({
  selector: 'app-employee-details2',
  templateUrl: './employee-details2.component.html',
  styleUrls: ['./employee-details2.component.css'],
  providers: [DeputationinService]
})

export class EmployeeDetails2Component implements OnInit {
  showdeput = true;
  Preshowdeput = true;
  @ViewChild('stepper') stepper;
  isLoading: boolean;
  empCd: string;
  myControl = new FormControl();
  verifyEmpCode: any[];
  selectedIndex = 0;
  isLinear = false;
  empvarifyCode: EmployeeDetailsModel;
  empVfyCode: string[] = [];
  empcodes: any[];
  empVfyCode1: string[];
  checkautofill: string;
  public comoonEmoCode: any = {};
  commonEmpCode: string;
  reqEmpcode: boolean;
  btnText = 'Save';
  chkMatstepper: MatStepper;
  GetMatSelectedIndex: Number;
  disbleflag = false;
  /** control for the selected emp */
  public empCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public empFilterCtrl: FormControl = new FormControl();

  /** list of employee   filtered by search keyword */
  public filteredEmp: Subject<EMCODE[]> = new Subject<EMCODE[]>();

  @ViewChild('singleSelect') singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  private _onDestroy = new Subject<void>();

  registerForm: FormGroup;
  submitted = false;
  empPageDeputation: boolean;
  constructor( private objEmpDetails: EmpdetailsService,
   private objgetEmpService: GetempcodeService

  ) {
    this.GetVerifyEmpCode();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
  //  this.empPageDeputation = true;
    this.GetMatSelectedIndex = 0;
    this.GetVerifyEmpCode();
   // listen for search field value changes
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmployee();
      });
  }
 // =============stepper selection change==============
  public selectionChange($event, stepper): void {
    debugger;
    this.GetMatSelectedIndex = $event.selectedIndex;
     stepper.selected.interacted = false;
    this.chkMatstepper = $event;
    // stepper.next();
    if (this.commonEmpCode !== undefined && this.commonEmpCode !== null)  {
      this.GetIsDeputEmp(this.commonEmpCode);
     }
       this.GetEmpCode(this.commonEmpCode);
    this.selectedIndex = $event.selectedIndex;
  }
  GetEmpCode(empcode: string) {
    // alert(this.GetMatSelectedIndex);
   this.commonEmpCode = empcode;
   this.GetIsDeputEmp(empcode);
   if ( this.commonEmpCode !== undefined &&  this.commonEmpCode !== null)  {
    this.objgetEmpService.SendEmpCode(empcode, this.GetMatSelectedIndex);
   }
  }
  private filterEmployee() {
    //  debugger;;
    if (!this.empcodes) {
      return;
    }
    // get the search keyword
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.empcodes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the employee
    this.filteredEmp.next(
      this.empcodes.filter(empcodes => empcodes.name.toLowerCase().indexOf(search) > -1)
    );
  }

  GetVerifyEmpCode() {
    this.objEmpDetails.GetVerifyEmpCode().subscribe(res => {
      this.empVfyCode = res;
      this.empcodes = res;
      // set initial selection
      this.empCtrl.setValue(this.empcodes);

      // load the initial employee list
      this.filteredEmp.next(this.empcodes);
     
    });
  }

 
  goBack(stepper: MatStepper) {
    stepper.selected.interacted = false;
    this.chkMatstepper = stepper;
    stepper.previous();
  }
  goForward(stepper: MatStepper) {
   // debugger;
   this.chkMatstepper = stepper;
    stepper.selected.interacted = false;
     stepper.next();
  }

  // ==================Personal Details ===============================
  GetIsDeputEmp(empcode: any) {
     if (empcode !== undefined && empcode !== null) {
      this.objEmpDetails.GetIsDeputEmp(empcode).subscribe(data => {
        debugger;
       this.showdeput = data;
      });
    }
  }
}

