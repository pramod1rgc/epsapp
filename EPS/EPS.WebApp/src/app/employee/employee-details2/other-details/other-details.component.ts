import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MasterService } from '../../../services/master/master.service';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-other-details',
  templateUrl: './other-details.component.html',
  styleUrls: ['./other-details.component.css']
})
export class OtherDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  constructor(private snackBar: MatSnackBar, private objEmpGetCodeService: GetempcodeService,
    private master: MasterService,
  ) {
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      // tslint:disable-next-line: max-line-length
      commonEmpCode => { if (commonEmpCode.selectedIndex === 5) { } });
  }
  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
  }

}
