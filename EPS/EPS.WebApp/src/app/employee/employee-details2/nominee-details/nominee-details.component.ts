import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { FormGroup } from '@angular/forms';
import { MasterService } from '../../../services/master/master.service';
import { NomineedetailsService } from '../../../services/empdetails/nomineedetails.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../../global/common-msg';


@Component({
  selector: 'app-nominee-details',
  templateUrl: './nominee-details.component.html',
  styleUrls: ['./nominee-details.component.css']
})
export class NomineeDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  deletepopup: any;
  setDeletIDOnPopup: any;
  empCd: string;
  isTableHasData = true;
  salutations: any;
  relations: any;
  disbleNomineeflag = false;
  objNomineeDetails: any = {};
  states: any = [];
  nomineedataSource: any;
  maxDate = new Date();
  nomineedisplayedColumns: string[] = ['Name', 'RelationShip', 'Percentage', 'Action'];

  @ViewChild('nomineepaginator') nomineepaginator: MatPaginator;
  @ViewChild('nomineesort') nomineesort: MatSort;

  @ViewChild('pdetails') personalDetailsForm: any;
  @ViewChild('Nominee') NomineeForm: any;
  registerForm: FormGroup;
  submitted = false;
  constructor(private commonMsg: CommonMsg, private objEmpGetCodeService: GetempcodeService , private master: MasterService,
    private objNomineeService: NomineedetailsService, ) {
   this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
     // tslint:disable-next-line: max-line-length
     commonEmpCode => { if (commonEmpCode.selectedIndex === 10) { this.getAllNomineeDetails(commonEmpCode.empcode); } });
 }

  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.getSalutation();
    this.getState();
    this.getRelation();
    this.resetNomineeDdetailsForm();
  }
  getSalutation() {
    // debugger;
    this.master.getSalutation().subscribe(res => {
      this.salutations = res;
    });
  }

  getRelation() {
    this.master.getRelation().subscribe(res => {
      this.relations = res;
    });
  }
  getState() {
    this.master.getState().subscribe(res => {
      this.states = res;
    });
  }

  getAllNomineeDetails(empCd: string) {
    this.getEmpCode = empCd;
    this.resetNomineeDdetailsForm();
    if (!Array.isArray(empCd)) {
      this.objNomineeService.getAllNomineeDetails(empCd,this.roleId).subscribe(res => {
        this.nomineedataSource = new MatTableDataSource(res);
        this.nomineedataSource.paginator = this.nomineepaginator;
        this.nomineedataSource.sort = this.nomineesort;

      });
    }
  }

  NomineeFilter(filterValue: string) {
    this.nomineedataSource.filter = filterValue.trim().toLowerCase();
    if (this.nomineedataSource.paginator) {
      this.nomineedataSource.paginator.firstPage();
    }
    if (this.nomineedataSource.filteredData.length > 0)
      this.isTableHasData = true;
    else
      this.isTableHasData = false;
  }

  getNomineeDetails(empCd: string, flag: any, NomineeID: any) {
    if (!Array.isArray(empCd)) {
      this.objNomineeService.getNomineeDetails(empCd, NomineeID).subscribe(res => {
        this.objNomineeDetails = res;
        if (this.objNomineeDetails.nomineeFirstName === '' || this.objNomineeDetails.nomineeFirstName == null) {
          this.btnText = 'Save';
        } else {
          this.btnText = 'Update';
        }
        if (flag === 1) {
          this.disbleNomineeflag = false;
        } else {
          this.disbleNomineeflag = true;
        }
      });
    } else {
      this.objNomineeDetails = {};
      this.resetNomineeDdetailsForm();

    }
  }

  UpdateNomineeDetails() {
    // tslint:disable-next-line:no-debugger
    // debugger;
    if (this.getEmpCode === undefined) {
    alert('Please Select any Employee');
    } else {
      this.objNomineeDetails.Empcd = this.getEmpCode;
      // tslint:disable-next-line: triple-equals
      if (this.objNomineeDetails.nomineeID === 0 || this.objNomineeDetails.nomineeID === undefined) {
        this.objNomineeDetails.VerifyFlag = 'E';
      } else {
        this.objNomineeDetails.VerifyFlag = 'U';
      }
      this.objNomineeDetails.userID = this.UserId ;
      this.objNomineeService.UpdateNomineeDetails(this.objNomineeDetails).subscribe(result1 => {
       // tslint:disable-next-line: radix
       if (parseInt(result1) >= 1) {
          this.getAllNomineeDetails(this.getEmpCode);          
         if (this.btnText == "Save")
           swal(this.commonMsg.saveMsg);
         else
           swal(this.commonMsg.updateMsg);
       } else {
         swal(this.commonMsg.saveFailedMsg);
       }
        this.resetNomineeDdetailsForm();
      });
    }
  }

  DeleteNomineeDetails() {
    this.objNomineeService.DeleteNomineeDetails(this.empCd, this.setDeletIDOnPopup).subscribe(result1 => {
      // tslint:disable-next-line:radix
      if (result1 >= 1) {
        this.getAllNomineeDetails(this.empCd);
        this.resetNomineeDdetailsForm();
        this.deletepopup = !this.deletepopup;
        swal(this.commonMsg.deleteMsg);

      } else {
        swal(this.commonMsg.deleteFailedMsg);
      }
    });
  }
  resetNomineeDdetailsForm() {
    this.disbleNomineeflag = false;
    this.objNomineeDetails = {};
    this.NomineeForm.resetForm();
    this.btnText = 'Save';
  }
  SetDeleteId(Empcode: any , DeletIDOnPopup: any ) {
    this.setDeletIDOnPopup = DeletIDOnPopup;
    this.empCd = Empcode;
  }

  //validation

  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }
  charaterWithNumeric(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }
    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }



}
