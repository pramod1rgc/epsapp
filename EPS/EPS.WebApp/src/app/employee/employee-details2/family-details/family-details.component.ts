import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { FamilydetailsService } from '../../../services/empdetails/familydetails.service';
import { MasterService } from '../../../services/master/master.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../../global/common-msg';


@Component({
  selector: 'app-family-details',
  templateUrl: './family-details.component.html',
  styleUrls: ['./family-details.component.css']
})
export class FamilyDetailsComponent implements OnInit {
  getEmpCode: string;
  subscription: Subscription;
  btnText = 'Save';
  VerifyFlag: any;
  UserId: any;
  roleId: any;
  deletepopup: any;
  setDeletIDOnPopup: any;
  empCd: string;
  isTableHasData = true;
  dataSource: any;
  objFamilyDetails: any = {};
  objMaritalStatus: any;
  salutations: any;
  relations: any;
  displayedColumns: string[] = ['Name', 'MaritalStatus', 'RelationShip', 'BirthDate', 'Action'];
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('familypaginator') familypaginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('pdetails') form: any;
  @ViewChild('EmPDdetailsForm') EmPDdetailsForm: any;
  @ViewChild('Family') FamilyForm: any;
  maxDate = new Date();
  minDate = "1850-01-01";
  disbleflag = false;
  constructor(private objEmpGetCodeService: GetempcodeService ,  private master: MasterService,
    private objFamilyService: FamilydetailsService, private commonMsg: CommonMsg
 ) {
   this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
     // tslint:disable-next-line: max-line-length
     commonEmpCode => { if (commonEmpCode.selectedIndex === 9) { this.getAllFamilyDetails(commonEmpCode.empcode); } });
 }
  ngOnInit() {
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.GetMaritalStatus();
    this.getSalutation();
    this.getRelation();
    this.resetFamilyDdetailsForm();
  }

  getFamilyDetails(empCd: string, flag: any, MsDependentDetailID: any) {

    if (!Array.isArray(empCd)) {

      this.objFamilyService.getFamilyDetails(empCd, MsDependentDetailID).subscribe(res => {
        this.objFamilyDetails = res;
        if (this.objFamilyDetails.relationshipId === '' || this.objFamilyDetails.relationshipId == null) {
          this.btnText = 'Save';
          this.VerifyFlag = 'E';
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U';
        }
        if (flag === 1) {
          this.disbleflag = false;
        } else {
          this.disbleflag = true;
        }
      });
    } else {
      this.resetFamilyDdetailsForm();
    }
  }
  getAllFamilyDetails(empCd: string) {
    this.getEmpCode = empCd;
     this.resetFamilyDdetailsForm();
     this.objFamilyService.getAllFamilyDetails(empCd,this.roleId).subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      // debugger;
      // alert(this.dataSource.data.length)
      if (this.dataSource.data.length > 0) {
        this.isTableHasData = true;
      } else {
        this.isTableHasData = false;
      }
      this.dataSource.paginator = this.familypaginator;
      this.dataSource.sort = this.sort;
      // this.resetFamilyDdetailsForm();
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
          this.isTableHasData = true;
      else
         this.isTableHasData = false;
  }
  resetFamilyDdetailsForm() {
    this.FamilyForm.resetForm();
    this.btnText = 'Save';
    this.objFamilyDetails = {};
  }
  GetMaritalStatus() {
    this.objFamilyService.GetMaritalStatus().subscribe(res => {
      this.objMaritalStatus = res;
    });
  }

  getSalutation() {
    // debugger;
    this.master.getSalutation().subscribe(res => {
      this.salutations = res;
    });
  }

  getRelation() {
    this.master.getRelation().subscribe(res => {
      this.relations = res;
    });
  }

  UpdateEmpFamilyDetails() {
    if (this.getEmpCode === undefined || this.getEmpCode === '') {
        alert('Please Select employee');

    } else {
      this.objFamilyDetails.empCd = this.getEmpCode;
      this.objFamilyDetails.UserId = this.UserId;
      if (this.objFamilyDetails.msDependentDetailID === 0 || this.objFamilyDetails.msDependentDetailID === undefined) {
        this.objFamilyDetails.VerifyFlag = 'E';
      } else {
        this.objFamilyDetails.VerifyFlag = 'U';
      }
      this.objFamilyService.UpdateEmpFamilyDetails(this.objFamilyDetails).subscribe(result1 => {
        console.log(result1);
        if (parseInt(result1) >= 1) {          
          if (this.btnText == "Save")
            swal(this.commonMsg.saveMsg);
          else
            swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.saveFailedMsg);
        }
        this.getAllFamilyDetails(this.getEmpCode);
        this.resetFamilyDdetailsForm();
      });
    }
  }
  DeleteFamilyDetails() {
    this.objFamilyService.DeleteFamilyDetails(this.empCd, this.setDeletIDOnPopup).subscribe(result1 => {
      // tslint:disable-next-line:radix
      if (result1 >= 1) {
        this.getAllFamilyDetails(this.empCd);
        this.resetFamilyDdetailsForm();
        this.deletepopup = !this.deletepopup;
        swal(this.commonMsg.deleteMsg);
      } else {
        swal(this.commonMsg.deleteFailedMsg);
      }
    });
  }
  SetDeleteId(Empcode: any , DeletIDOnPopup: any ) {
    this.setDeletIDOnPopup = DeletIDOnPopup;
    this.empCd = Empcode;
  }

  trackById(item) {
    console.log(item.msDependentDetailID);
    return item.msDependentDetailID;
  }

  //validation

  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

}
