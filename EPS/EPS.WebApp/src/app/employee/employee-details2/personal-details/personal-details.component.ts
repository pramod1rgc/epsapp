import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { EmpdetailsService } from '../../../services/empdetails/empdetails.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ReplaySubject, Subscription } from 'rxjs';
import { GetempcodeService } from '../../../services/empdetails/getempcode.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';
interface EMCODE {
  id: string;
  name: string;
}

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})

export class PersonalDetailsComponent implements OnInit {
  isLoading: boolean;
  reqEmpcode: boolean;
 @ViewChild('pdetails') form: any;
  empdetails: any = {};
  showPH: boolean;
  phEmpDetails: any = {};
  btnText = 'Save';
  VerifyFlag: any;
  showdeput = true;
  Preshowdeput = true;
  UserId: any;
  roleId: any;
  empphTypes: any;
  commonEmpCode: any = {};
  @ViewChild('EmPDdetailsForm') EmPDdetailsForm: any;
  empVfyCode: string[] = [];
  maxDate = new Date();
  empcodes: any[];
  @Input() public simpleText: string;
  public empCtrl: FormControl = new FormControl();
  subscription: Subscription;
  /** list of employee   filtered by search keyword */
  public filteredEmp: ReplaySubject<EMCODE[]> = new ReplaySubject<EMCODE[]>(1);

  constructor(private _formBuilder: FormBuilder, private objEmpDetails: EmpdetailsService,  private commonMsg: CommonMsg,
    private objEmpGetCodeService: GetempcodeService
   ) {
     // this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(commonEmpCode => { this.commonEmpCode = commonEmpCode; });
    // tslint:disable-next-line: max-line-length
    this.subscription = this.objEmpGetCodeService.getEmpCode().subscribe(        // tslint:disable-next-line: no-non-null-assertion
      commonEmpCode => { if (commonEmpCode.selectedIndex  === 0) {this.GetEmpPersonalDetailsByID(commonEmpCode.empcode); } });
  }
  ngOnInit() {
     this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
  }
  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
}
  GetEmpPersonalDetailsByID(empcode: any) {
   this.isLoading = true;
    // debugger;
    if (empcode !== undefined && empcode !== null) {
      this.GetIsDeputEmp(empcode);
    }
    this.reqEmpcode = false;
    this.empdetails = '';
    if (empcode !== 0) {
      this.objEmpDetails.GetEmpPersonalDetailsByID(empcode,this.roleId).subscribe(data => {
        this.empdetails = data;

        if (data.emp_mobile_no === '' || data.emp_mobile_no == null) {
          this.btnText = 'Save';
          this.VerifyFlag = 'E' ;
        } else {
          this.btnText = 'Update';
          this.VerifyFlag = 'U' ;
        }
      });
    }
  }
  GetIsDeputEmp(empcode: any) {
    if (empcode !== 0) {
      this.objEmpDetails.GetIsDeputEmp(empcode).subscribe(data => {
        this.showdeput = data;
      });
    }
  }
  UpdateEmpDetails(): number {
    if (this.commonEmpCode === undefined) {
      this.reqEmpcode = true;

    } else {
      this.empdetails.UserID = this.UserId ;
      this.empdetails.VerifyFlag = this.VerifyFlag;
      this.objEmpDetails.UpdateEmpDetails(this.empdetails).subscribe(result => {
        // tslint:disable-next-line:radix
        if (parseInt(result) > 1) {
          
          swal(this.commonMsg.updateMsg);
        } else {
          swal(this.commonMsg.updateFailedMsg);
        }
      });
    }
    return 1;

  }

  resetPersonalDdetailsForm() {
    this.empdetails.emp_Email = '';
    this.empdetails.emp_mobile_no = '';
    this.empdetails.empPhFlag = '';

  }

  GetEmpPHDetails(empcode: any) {
    this.objEmpDetails.GetEmpPHDetails(empcode).subscribe(data => {
      this.phEmpDetails = data[0];
      this.empdetails.msEmpPhId = data[0].msEmpPhId;
      this.empdetails.phCertAuth = data[0].phCertAuth;
      this.empdetails.phType = data[0].phType;
      this.empdetails.phPcnt = data[0].phPcnt;
      this.empdetails.phCertNo = data[0].phCertNo;
      this.empdetails.phCertDt = data[0].phCertDt;
      // this.empdetails.phCertAuth =data[0].phCertAuth;
      this.empdetails.phAdmOrdno = data[0].phAdmOrdno;
      this.empdetails.phAdmOrddt = data[0].phAdmOrddt;

      this.empdetails.isSevere = data[0].isSevere;
      this.empdetails.empDoubleTa = data[0].empDoubleTa;
      this.empdetails.phRemarks = data[0].phRemarks;
    });
  }
  GetPhysicalDisabilityTypes() {
    this.objEmpDetails.GetPhysicalDisabilityTypes().subscribe(res => {
      this.empphTypes = res;
    });
  }
  SavePHDetails() {

    if (this.commonEmpCode === undefined) {
      this.reqEmpcode = true;

    } else {
      // debugger;
      this.empdetails.empCd = this.empdetails.empCd;
      this.empdetails.userID = this.UserId ;
      if (this.empdetails.msEmpPhId !== undefined) {
        this.empdetails.phVfFlg = 'U';
      } else {
        this.empdetails.phVfFlg = 'E';
      }
      this.objEmpDetails.SavePHDetails(this.empdetails).subscribe(result1 => {
        //console.log(result1);
        // tslint:disable-next-line:radix
        if (parseInt(result1) >= 1) {
        //  this.snackBar.open('Save Successfully', null, { duration: 4000 });
          swal(this.commonMsg.updateMsg);
        } else {
          // this.snackBar.open('Save not Successfully', null, { duration: 4000 });
          swal(this.commonMsg.updateFailedMsg);
        }
        this.showPH = false;
      });
    }
  }

  resetEmPDdetailsForm() {
    this.EmPDdetailsForm.resetForm();
  }

  charaterOnlyNoSpace(event): boolean {
    //  debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (event.target.value === "") {
      if (charCode !== 32) {
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
          (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
          return true
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }


    }
    else if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 32 ||
      (charCode >= 47 && charCode <= 57) || charCode == 46 || charCode == 44 || charCode == 40 || charCode == 41 || charCode == 45) {
      return true
    }
    else {
      return false;
    }
  }
}
