import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeDetails1Component } from './employee-details1/employee-details1.component';
import { EmployeeDetails2Component } from './employee-details2/employee-details2.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { MasterService } from '../services/master/master.service';
import { SharedModule } from '../shared-module/shared-module.module';
import { MatTooltipModule } from '@angular/material/tooltip';

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ForwardToCheckerComponent } from '../employee/forward-to-checker/forward-to-checker.component';
import {PersonalDetailsComponent} from '../employee/employee-details2/personal-details/personal-details.component';
import {PostingDetailsComponent} from '../employee/employee-details2/posting-details/posting-details.component';
import { CgegisDetailsComponent } from './employee-details2/cgegis-details/cgegis-details.component';
import { CghsDetailsComponent } from './employee-details2/cghs-details/cghs-details.component';
import { PayDetailsComponent } from './employee-details2/pay-details/pay-details.component';
import { ServiceDetailsComponent } from './employee-details2/service-details/service-details.component';
import { OtherDetailsComponent } from './employee-details2/other-details/other-details.component';
import { BankDetailsComponent } from './employee-details2/bank-details/bank-details.component';
import { GovQuaterDetailsComponent } from './employee-details2/gov-quater-details/gov-quater-details.component';
import { FamilyDetailsComponent } from './employee-details2/family-details/family-details.component';
import { NomineeDetailsComponent } from './employee-details2/nominee-details/nominee-details.component';
// import { DialogComponent } from '../dialog/dialog.component';
import { DeputationModule } from '../deputation/deputation.module';

import { CommonMsg } from '../global/common-msg';

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [EmployeeDetails1Component, EmployeeDetails2Component,  ForwardToCheckerComponent, PersonalDetailsComponent, PostingDetailsComponent, CgegisDetailsComponent, CghsDetailsComponent, PayDetailsComponent, ServiceDetailsComponent, OtherDetailsComponent, BankDetailsComponent, GovQuaterDetailsComponent, FamilyDetailsComponent, NomineeDetailsComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTooltipModule,
    NgxMatSelectSearchModule,
    NgMultiSelectDropDownModule,
    DeputationModule
    
  ],
  providers: [CommonMsg]
})
export class EmployeeModule { }
