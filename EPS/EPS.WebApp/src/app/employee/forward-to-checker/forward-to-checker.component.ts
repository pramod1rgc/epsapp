import { Component, OnInit, ViewChild } from '@angular/core';
import { ForwardToCheckerService } from '../../services/empdetails/forward-to-checker.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-forward-to-checker',
  templateUrl: './forward-to-checker.component.html',
  styleUrls: ['./forward-to-checker.component.css']
})
export class ForwardToCheckerComponent implements OnInit {
  UserId: number;
  roleId: number;
  dataSource: any;
  verifiedForm: FormGroup;
  empdetails: boolean;
  empCompletedetails: any;
  approvePopup: boolean;
  rejectPopup: boolean;

  displayedColumns: string[] = ['select', 'empCode', 'empName', 'empDesignation', 'serviceType', 'empType', 'empSubType', 'empJoiningMode'
    , 'empJoiningCategory', 'empDateOfBirth', 'empDateOfJoining', 'empPanNo', 'status', 'remark','action'];
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selection = new SelectionModel<any>(true, []);
  data: any[] = [];
  objForwardToChecker: any = {};
  constructor(private objForwardToCheckerService: ForwardToCheckerService, private snackBar: MatSnackBar, private _formBuilder: FormBuilder) {
    this.createForm();
  }
  ngOnInit() {
    debugger;
    this.UserId = Number(sessionStorage.getItem('UserID'));
    this.roleId = Number(sessionStorage.getItem('userRoleID'));
    this.getAllEmpDetails();

  }

  createForm() {
    this.verifiedForm  = new FormGroup({
      status: new FormControl(''),//[Validators.required, Validators.minLength(3)]),
      remark: new FormControl(''),
      empCode: new FormControl(''),

    
    })
  };
  getAllEmpDetails() {
    this.objForwardToCheckerService.getAllEmpDetails(this.roleId).subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  getAllEmployeeCompleteDetails(empCode: any) {
    this.objForwardToCheckerService.getAllEmployeeCompleteDetails(this.roleId, empCode).subscribe(res => {
      debugger;
      this.empCompletedetails = res;
      console.log(res[0][0].empAdhaarNo);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  ForwardToChecker() {
    debugger;
    //this.data = undefined;
    if (this.selection.hasValue()) {
      this.selection.selected.forEach(item => {
        this.data.push(item.empCode);
      });
    this.objForwardToChecker.empCodes = this.data;
    this.objForwardToChecker.UserId = this.UserId;
      this.objForwardToCheckerService.ForwardToCheckerEmpDetails(this.objForwardToChecker).subscribe(result => {
        // tslint:disable-next-line: radix
        if (parseInt(result) >= 1) {
           this.getAllEmpDetails();
             this.snackBar.open('Save Successfully', null, { duration: 4000 });
         } else {
           this.snackBar.open('Save not Successfully', null, { duration: 4000 });
         }
       });
    } else {
      alert('Please Select at least one Employee ');
    }

  }
  verifyEmpData(empCode: string, remark: string) {
    debugger;
    
      this.objForwardToCheckerService.verifyEmpData(empCode, remark).subscribe(result => {
        // tslint:disable-next-line: radix
        if (parseInt(result) >= 1) {
          this.getAllEmpDetails();
          this.snackBar.open('Save Successfully', null, { duration: 4000 });
        } else {
          this.snackBar.open('Save not Successfully', null, { duration: 4000 });
        }
      });
    } 

  }

