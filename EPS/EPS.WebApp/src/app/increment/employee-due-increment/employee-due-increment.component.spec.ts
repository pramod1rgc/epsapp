import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDueIncrementComponent } from './employee-due-increment.component';

describe('EmployeeDueIncrementComponent', () => {
  let component: EmployeeDueIncrementComponent;
  let fixture: ComponentFixture<EmployeeDueIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDueIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDueIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
