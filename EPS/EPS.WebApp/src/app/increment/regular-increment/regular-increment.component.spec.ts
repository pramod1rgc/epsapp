import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegularIncrementComponent } from './regular-increment.component';

describe('RegularIncrementComponent', () => {
  let component: RegularIncrementComponent;
  let fixture: ComponentFixture<RegularIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegularIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegularIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
