import { Component, OnInit, ViewChild } from '@angular/core';
import { PayscaleService } from '../../services/payscale.service';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { DesignationModel } from '../../model/Shared/DDLCommon';
import { RegincrementService } from '../../services/increment/regincrement.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import swal from 'sweetalert2';


const ELEMENT_DeAttachdata: EmpDeAttach[] = [];
const ELEMENT_DATA: EmpAttach[] = [];

export interface EmpAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

export interface EmpDeAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

@Component({
  selector: 'app-regular-increment',
  templateUrl: './regular-increment.component.html',
  styleUrls: ['./regular-increment.component.css']
})



export class RegularIncrementComponent implements OnInit {
  CommissionCodelist: any;
  salaryMonthlist: any;
  _pScaleObj: PayscaleModel;
  ArrddlDesign: DesignationModel;
  PermDdoId: string;
  Design: any[];
  msDesigMastID: any;
  wefDate: any;
  Remark: any;
  incID: any;
  noofEmployee: any;
  dataSource: any;
  dataSourceEmp: any;
  Message: any;
  username: any;
  IncrementData: any[] = [];
  regInc: any = {};
  regInc1: any[] = [];
  dataDeAttach: any[] = [];
  dataSourceDeAttach: any;
  data: any;
  showPH: boolean;
  disableflag: any;
  disablebtnflag: any;
  showhidediv0: any;
  showhidediv1: any;
  showhidediv2: any;
  showhidediv3: any;
  selectionAttach = new SelectionModel<EmpAttach>(true, []);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
  checkedObject: any[] = [];
  DcheckedObject: any[] = [];
  deletepopup: boolean;
  disableFDflag: boolean;
  checktrue= false;
  isTableHasData = true;
  isTableHasDataY = true;
  isTableHasDataX = true;
  setDeletIDOnPopup: any;
  totalAttach = 0;
  constructor(private payscale: PayscaleService, private regincrement: RegincrementService) { }

  public designCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  private _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['orderNo', 'orderDate', 'noofEmployee', 'status', 'action'];
  displayedColumns1: string[] = ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  displayedColumns2: string[] = ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'nextIncDate', 'payLevel'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('paginatorDetach') paginatorDetach: MatPaginator;
  @ViewChild('paginatorAttach') paginatorAttach: MatPaginator;
  @ViewChild('sortDetach') sortDetach: MatSort;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('f') form: any;
  @ViewChild('mainf') formain: any;

  ngOnInit() {
    this._pScaleObj = new PayscaleModel();
    this.bindPayCommission();
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.bindDesignation(this.PermDdoId);
    this.username = sessionStorage.getItem('username');
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = false;
    this.disableflag = false;
    this.showhidediv3 = false;
    this.disablebtnflag = true;
    this.disableFDflag = true;
  }


  bindPayCommission() {
    this.payscale.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }


  bindDesignation(value) {
    this.regincrement.getAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
     
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  //Create Order No
  createOrder() {
    this.regInc.loginUser = this.username;
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Regular Increment";
    this.regincrement.createOrderNo(this.regInc).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        this.showPH = false;
        swal(result)
      }
      this.form.resetForm();
      this.getEmpWithOrderNo();
    })

  }

  getEmpWithOrderNo() {
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Regular Increment";
    this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(result => {
      this.dataSourceEmp = new MatTableDataSource(result);
      if (this.dataSourceEmp.filteredData.length > 0)
        this.isTableHasData = true;
      else
        this.isTableHasData = false;
    })
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = true;
    this.showhidediv3 = false;
  }

  refreshData()
  {
    this.clearData();
    this._pScaleObj.msCddirID = 0;
    this.form.resetForm();
    this.dataSourceDeAttach = null;
  }


  reSetForm() {
    this.form.resetForm();
  }

  //End Order No


  // Attached and Detached Employee Data
  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllSelectedForDeAttach() {
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }
  

  masterToggleForAttach() {
    this.checkedObject = null;
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));
    this.checkedObject = this.dataSource.data;
    this.checkedObject = this.selectionAttach.selected;
  }

  //working
  masterToggleForDeAttach() {
    this.checkedObject = null;
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
      this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
    this.checkedObject = this.dataSourceDeAttach.data;
    this.DcheckedObject = this.dataSourceDeAttach.data;
    // this.DcheckedObject = this.selectionDeAttach.selected;
  }


  addCheckedObj() {
    this.checkedObject = null;
    this.checkedObject = this.selectionAttach.selected;

  }


  deAttachSelectedRows() {
    this.totalAttach = 0;
    this.checkedObject = null;
    if (this.selectionAttach.hasValue()) {
      this.selectionAttach.selected.forEach(item => {
        let index: number = this.data.findIndex(d => d === item);
        this.dataSource.data.splice(index, 1);
        // this.dataDeAttach.push(item);
        this.dataDeAttach.unshift(item);
        this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);
        this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);
        this.dataSource.paginator = this.paginatorAttach;
        this.dataSourceDeAttach.paginator = this.paginatorDetach;
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
      if (this.dataSourceDeAttach.filteredData.length > 0)
        this.isTableHasDataX = true;
      else
        this.isTableHasDataX = false;
    }
    else {
      swal("Please attach at least one Employee");
    }

    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
  }
  

  attachSelectedRows() {
    this.totalAttach = 0;
    this.disableFDflag = true;
    this.disableflag = false;
    this.checkedObject = null;
    if (this.selectionDeAttach.hasValue()) {
      this.selectionDeAttach.selected.forEach(item => {
        let index: number = this.dataDeAttach.findIndex(d => d === item);
        this.dataDeAttach.splice(index, 1);
        this.data.push(item);
        //this.data.unshift(item);
        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true, []);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true, []);
      this.dataSourceDeAttach.paginator = this.paginatorDetach;
      this.dataSource.paginator = this.paginatorAttach;
      
    }
    else {
      swal("Please attach at least one Employee ");
    }
    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    if (this.dataSource.filteredData.length > 0) {
      this.disablebtnflag = false;
      this.isTableHasDataY = true;
    }
    else {
      this.disablebtnflag = true;
      this.isTableHasDataY = false;
    }
      
  }
  //End Attached and Detached


  // Get Employee for bind grid
 
  totalEmp: any;
  indexView: number;
  getEmployeeForIncrement(ID: any, indexView: any) {
    this.totalAttach = 0;
    this.indexView = indexView;
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regincrement.getEmployeeForIncrements(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(result => {
      this.data = result;
     // this.checkedObject = result;
     // this.dataSource = "";
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginatorAttach;
      this.dataSource.sort = this.sort;
      this.totalEmp = this.data.length;
      for (let i = 0; i < this.dataSource.filteredData.length; i++) {
        if (this.dataSource.filteredData[i].status == "F") {
          this.disableflag = true;
          this.showhidediv2 = false;
          this.showhidediv3 = false;
          this.disablebtnflag = true;
          this.disableFDflag = true;
          this.dataSourceEmp.filteredData[this.indexView].status = "Forwarded";
        }
        else if (this.dataSource.filteredData[i].status == "E") {
          this.dataSourceEmp.filteredData[this.indexView].status = "Entered";
          this.disableflag = false;
          this.showhidediv2 = true;
          this.showhidediv3 = true;
          this.disablebtnflag = false;
          this.disableFDflag = false;
        }
        else if (this.dataSource.filteredData[i].status == "V") {
          this.dataSourceEmp.filteredData[this.indexView].status = "Verified";
          this.showhidediv2 = false;
          this.showhidediv3 = false;
          this.disablebtnflag = false;
          this.disableFDflag = false;
        }
        else {
          this.dataSourceEmp.filteredData[this.indexView].status = "";
          this.showhidediv2 = true;
          this.showhidediv3 = true;
          this.disablebtnflag = true;
          this.disableFDflag = true;}
          
      }
      if (this.dataSource.filteredData.length > 0)
        this.isTableHasDataY = true;
      else
        this.isTableHasDataY = false;
    })

    this.regincrement.getEmployeeForNonIncrement(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(result => {
      this.dataSourceDeAttach = new MatTableDataSource(result);
      this.dataDeAttach = result;
      this.dataSourceDeAttach.paginator = this.paginatorDetach;  //sortDetach
      this.dataSourceDeAttach.sort = this.sortDetach;
      if (this.dataSourceDeAttach.filteredData.length > 0)
        this.isTableHasDataX = true;
      else
        this.isTableHasDataX = false;
    })

    this.showhidediv1 = true;
    this.showhidediv2 = true;
    this.showhidediv3 = true;
  }
 // End

 
  // Save Increment Data
  insertRegIncrementData() {
    this.checktrue= false;
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      if (this.totalAttach == 0)
        swal('There are no record found!')
      else
        swal('Please attach record!')
    }
    else {
      this.IncrementData = [];

      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.IncrementData.push(d.empID);
          this.IncrementData.push(d.empCode);
          this.IncrementData.push(d.payLevel);
          this.IncrementData.push(d.wefDate);
          if (d.wefDate == null) {
            swal('Wef date should not be blank');
            this.checktrue = true;
            return false;
          }
          this.IncrementData.push(d.remark);
          this.IncrementData.push(d.oldBasic);
          this.IncrementData.push(d.newBasic);
          this.IncrementData.push(this.username);
          this.IncrementData.push(this.ArrddlDesign.msDesigMastID);
          this.IncrementData.push(d.orderNo);
          this.IncrementData.push(d.orderDate);
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });
      }
      if (this.checktrue == false) {
        this.regincrement.insertRegIncrementData(myArray).subscribe(result => {
          if (result != undefined) {
            this.deletepopup = false;
            swal(result)
          }
        });}
      this.disableFDflag = false;
    }
    this.IncrementData = [];
    this.selectionAttach.clear();
  }

  //End


  // Forward to checker

  forwardtocheckerFor() {
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      swal('Please attach records!')
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.IncrementData.push(d.id);
          this.IncrementData.push(d.empID);
          this.IncrementData.push(this.username);
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });
      }
      this.regincrement.forwardtocheckerForInc(myArray).subscribe(result => {
        if (result != undefined) {
            this.deletepopup = false;
            swal(result)
          }
        });
      }
      this.IncrementData = [];
    }

  //End


  clearData() {
    this.showhidediv0 = false;
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv3 = false;
    this.deletepopup = false;
    this.form.resetForm();
    this._pScaleObj.msCddirID = 0;
  }
  
  //Filter

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginatorAttach) {
      this.dataSource.paginatorAttach.firstPage();
    }
    if (this.dataSource.filteredData.length > 0)
      this.isTableHasDataY = true;
    else
      this.isTableHasDataY = false;
  }

  applyFilterDetach(filterValue: string) {
    this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceDeAttach.paginatorDetach) {
      this.dataSourceDeAttach.paginatorDetach.firstPage();
    }
    if (this.dataSourceDeAttach.filteredData.length > 0)
      this.isTableHasDataX = true;
    else
      this.isTableHasDataX = false;
  }

  //End

  // Delete Order Details

  setDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }

  deleteIncOrderDetails(id) {
    this.regInc.OrderType = "Regular Increment";
    this.regincrement.deleteOrderDetails(id, this.regInc.OrderType).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        swal(result)
      }
      this.getEmpWithOrderNo();
    })
  }
  // End






}

