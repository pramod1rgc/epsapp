import { Component, OnInit, ViewChild } from '@angular/core';
import { PayscaleService } from '../../services/payscale.service';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { AnnualincrementService } from '../../services/increment/annualincrement.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';


@Component({
  selector: 'app-annual-increment-report',
  templateUrl: './annual-increment-report.component.html',
  styleUrls: ['./annual-increment-report.component.css']
})
export class AnnualIncrementReportComponent implements OnInit {
  CommissionCodelist: any;
  salaryMonthlist: any;
  _pScaleObj: PayscaleModel;
  dataSourceEmp: any;
  dataSource: any;
  regInc: any;
  PayLevel: any;
  msCddirID: any;
  orderID: any;
  showhideGried: boolean;
  showhideGriedReport: boolean;
  isTableHasData = true;
  constructor(private payscale: PayscaleService, private incrementService: AnnualincrementService) { }


  displayedColumns: string[] = ['OrderNo', 'OrderDate', 'NoofEmployee', 'Status', 'action'];
  displayedColumn: string[] = ['serialNo', 'empName', 'empCode', 'desigCode', 'oldBasic','nextIncDate'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('annualIncrment') form: any;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this._pScaleObj = new PayscaleModel();
    this.bindPayCommission();
    this.showhideGried = false;
    this.showhideGriedReport = false;
  }



  bindPayCommission() {
    this.payscale.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }

  getOrderWithEmployee() {
    debugger;
    this.showhideGriedReport = false;
    this.PayLevel = this._pScaleObj.msCddirID;
    this.incrementService.GetOrderWithEmployee(this.PayLevel).subscribe(result => {
      this.dataSourceEmp = new MatTableDataSource(result);
      this.showhideGried = true;
      if (this.dataSourceEmp.filteredData.length > 0)
        this.isTableHasData = true;
      else
        this.isTableHasData = false;
    })
  }

  getAnnualIncReport(ID: any) {
    debugger;
    this.showhideGriedReport = true;
    //this.regInc.orderID = ID;
    this.PayLevel = this._pScaleObj.msCddirID;
    this.incrementService.GetAnnualIncReportData(this.PayLevel,ID).subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
       this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.showhideGriedReport = true;
    })
  }

  

}
