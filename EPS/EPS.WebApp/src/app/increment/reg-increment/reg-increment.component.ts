import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, FormControl, Form, Validators } from '@angular/forms';

import { PayscaleService } from '../../services/payscale.service';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { DesignationModel } from '../../model/Shared/DDLCommon';
import { RegincrementService } from '../../services/increment/regincrement.service';
import { SelectionModel } from '@angular/cdk/collections';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { regularModel } from '../../increment/regular-increment/regularmodel';

export interface EmpAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

export interface EmpDeAttach {
  EmpName: string;
  Desigination: string;
  OldBasic: number;
  NewBasic: number;
  NextIncDate: Date;
  PayLevel: string;
}

@Component({
  selector: 'app-reg-increment',
  templateUrl: './reg-increment.component.html',
  styleUrls: ['./reg-increment.component.css']
})

export class RegIncrementComponent implements OnInit {
  formSave: FormGroup;
  CommissionCodelist: any;
  salaryMonthlist: any;
  _pScaleObj: PayscaleModel;
  ArrddlDesign: DesignationModel;
  PermDdoId: string;
  Design: any[];
  msDesigMastID: any;
  wefDate: any;
  Remark: any;
  incID: any;
  noofEmployee: any;
  dataSource: any;
  dataSourceEmp: any;
  dataeourceemp1: any;
  Message: any;
  username: any;
  IncrementData: any[] = [];
  regInc: any = {};
  regInc1: any[] = [];
  dataDeAttach: any[] = [];
  dataSourceDeAttach: any;
  data: any;
  showPH: boolean;
  disableflag: any;
  disablebtnflag: any;
  showhidediv0: any;
  showhidediv1: any;
  showhidediv2: any;
  showhidediv3: any;
  selectionAttach = new SelectionModel<EmpAttach>(true);
  selectionDeAttach = new SelectionModel<EmpDeAttach>(true);
  checkedObject: any[] = [];
  DcheckedObject: any[] = [];
  deletepopup: boolean;
  constructor(private payscale: PayscaleService, private regincrement: RegincrementService, private _formBuilder: FormBuilder) { }

  public designCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  private _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['OrderNo', 'OrderDate', 'NoofEmployee', 'Status', 'action'];
  displayedColumns1: string[] = ['select', 'EmpName', 'Desigination', 'OldBasic', 'NewBasic', 'WefDate', 'NextIncDate', 'PayLevel', 'Remark'];//,
  displayedColumns2: string[] = ['select', 'EmpName', 'Desigination', 'OldBasic', 'NewBasic', 'NextIncDate', 'PayLevel'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('paginatorDetach') paginatorDetach: MatPaginator;
  @ViewChild('sortDetach') sortDetach: MatSort;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('f') form: any;
  @ViewChild('mainf') formain: any;

  ngOnInit() {
    this._pScaleObj = new PayscaleModel();
    this.bindPayCommission();
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.bindDesignation(this.PermDdoId);
    this.username = sessionStorage.getItem('username');
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = false;
    this.disableflag = false;
    this.showhidediv3 = false;
    this.disablebtnflag = false;

    this.formSave = this._formBuilder.group({
      IncrementSave: this._formBuilder.array([
      ])  //this.addDuesRateDetailsFormGroup()

    });
  }
  addDuesRateDetailsFormGroup(): FormGroup {
    return this._formBuilder.group({

      id: [null, Validators.required],
      incID: [null, Validators.required],
      empName: [null, Validators.required],
      oldBasic: [null, Validators.required],
      nextIncDate: [null, Validators.required],
      oldWefDate: [null, Validators.required],
      payLevel: [null, Validators.required],
      noofIncrement: [null, Validators.required],
      orderNo: [null, Validators.required],
      orderDate: [null, Validators.required],
      orderType: [null, Validators.required],
      wefDate: [null, Validators.required],
      remark: [null, Validators.required],
      desigCode: [null, Validators.required],
      empCode: [null, Validators.required],
      salaryMonth: [null, Validators.required],
      loginUser: [null, Validators.required],
      changeSubType: [null, Validators.required],
      orderTypeID: [null, Validators.required],
      noofEmployee: [null, Validators.required],
      status: [null, Validators.required],
      desigination: [null, Validators.required],
      clientIP: [null, Validators.required],
      newBasic: [null, Validators.required],
      empID: [null, Validators.required],
      desigDesc: [null, Validators.required],
      select: [null, Validators.required],

    });
  }

  get IncrementSave(): FormArray {

    return this.formSave.get('IncrementSave') as FormArray;
  }


  bindPayCommission() {
    this.payscale.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }


  bindDesignation(value) {

    this.regincrement.getAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }

  //Create Order No
  createOrder() {
    this.regInc.loginUser = this.username;
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Regular Increment";
    this.regincrement.createOrderNo(this.regInc).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        swal(result)
      }
      this.form.resetForm();
      this.getEmpWithOrderNo(this.regInc.DesigCode);
    })

  }

  getEmpWithOrderNo(value) {
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regInc.OrderType = "Regular Increment";
    this.regincrement.getOrderDetails(this.regInc.DesigCode, this.regInc.PayLevel, this.regInc.OrderType).subscribe(result => {
      this.dataSourceEmp = new MatTableDataSource(result);
      this.dataeourceemp1 = result[0];
    })
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv0 = true;
    this.showhidediv3 = false;
  }

  //End Order No


  // Attached and Detached Employee Data
  isAllSelectedForAttach() {
    const numSelected = this.selectionAttach.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  isAllSelectedForDeAttach() {
    // debugger;
    const numSelected = this.selectionDeAttach.selected.length;
    const numRows = this.dataSourceDeAttach.data.length;
    return numSelected === numRows;
  }

  masterToggleForAttach() {
    this.checkedObject = null;
    this.isAllSelectedForAttach() ?
      this.selectionAttach.clear() :
      this.dataSource.data.forEach(row => this.selectionAttach.select(row));

    this.checkedObject = this.dataSource.data;
    this.checkedObject = this.selectionAttach.selected;
    //this.checkedObject = this.regInc1.wefDate;
    //this.checkedObject = this.regInc1.Remark; 
  }

  //working
  masterToggleForDeAttach() {
    //debugger;
    this.checkedObject = null;
    this.isAllSelectedForDeAttach() ?
      this.selectionDeAttach.clear() :
      this.dataSourceDeAttach.data.forEach(row => this.selectionDeAttach.select(row));
    this.checkedObject = this.dataSourceDeAttach.data;
    this.DcheckedObject = this.dataSourceDeAttach.data;
    // this.DcheckedObject = this.selectionDeAttach.selected;
  }


  addCheckedObj() {
    this.checkedObject = null;
    this.checkedObject = this.selectionAttach.selected;

  }


  deAttachSelectedRows() {
    // debugger;
    this.checkedObject = null;
    if (this.selectionAttach.hasValue()) {
      this.selectionAttach.selected.forEach(item => {
        let index: number = this.data.findIndex(d => d === item);
        this.dataSource.data.splice(index, 1);
        // this.dataDeAttach.push(item);
        this.dataDeAttach.unshift(item);
        this.dataSourceDeAttach = new MatTableDataSource(this.dataDeAttach);
        this.dataSource = new MatTableDataSource<EmpAttach>(this.dataSource.data);
        this.dataSource.paginator = this.paginator;
        this.dataSourceDeAttach.paginator = this.paginatorDetach;
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true);
    }
    else {
      alert("Please attach at least one Employee");
    }
    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
  }

  totalAttach: number;


  attachSelectedRows() {

    this.checkedObject = null;
    if (this.selectionDeAttach.hasValue()) {
      this.selectionDeAttach.selected.forEach(item => {
        debugger;
        let index: number = this.dataDeAttach.findIndex(d => d === item);
        this.dataDeAttach.splice(index, 1);
        //this.data.unshift(item);
        // this.dataSource = new MatTableDataSource(this.dataSource.data);

        // add a row from formarray .
      //  var abc = new FormArray(item);

        this.IncrementSave.push(this._formBuilder.group(item));

        // (<FormArray>this.formSave.get('IncrementSave')).push(this.addDuesRateDetailsFormGroup());

        //this.formSave.reset();
        this.formSave.setControl('IncrementSave', this.IncrementSave);

     //   this.formSave.addControl(item,);
        console.log(this.IncrementSave);
        debugger;

        //this.dataSourceDeAttach = new MatTableDataSource<EmpDeAttach>(this.dataDeAttach);
      });
      this.selectionAttach = new SelectionModel<EmpAttach>(true);
      this.selectionDeAttach = new SelectionModel<EmpDeAttach>(true);
      this.dataSourceDeAttach.paginator = this.paginatorDetach;


    }
    else {
      alert("Please attach at least one Employee ");
    }

    //for (let i = 0; i < this.dataSourceEmp.filteredData.length; i++) {
    //  this.dataSourceEmp.filteredData[i].noofEmployee = 0;
    //}

    this.totalAttach = this.data.length;
    this.dataSourceEmp.filteredData[this.indexView].noofEmployee = this.totalAttach;
    // this.dataeourceemp1[this.indexView].noofEmployee = this.totalAttach;    


  }
  //End Attached and Detached

  // Get Employee for bind grid

  x: number = 0;
  totalEmp: any;
  indexView: number;

  getEmployeeForIncrement(ID: any, indexView: any) {
    this.indexView = indexView;
    this.regInc.DesigCode = this.ArrddlDesign.msDesigMastID;
    this.regInc.PayLevel = this._pScaleObj.msCddirID;
    this.regincrement.getAllAsFormArray(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(result => {

      this.data = result;
      debugger;
      this.formSave.setControl('IncrementSave', result);


      //this.data.forEach(d => {
      //  this['wefDate' + this.x] = d.wefDate;
      //  this['remark' + this.x] = d.remark;
      //  this.x = this.x + 1;
      //});
      //this.checkedObject = result;
      //this.dataSource = new MatTableDataSource(result);
      //this.dataSource.paginator = this.paginator;
      //this.dataSource.sort = this.sort;
      //this.totalEmp = this.checkedObject.length;
      //this.disablebtnflag = false;
      //for (let i = 0; i < this.dataSource.filteredData.length; i++) {
      //  if (this.dataSource.filteredData[i].status == "F") {
      //    this.disableflag = true;
      //    this.showhidediv2 = false;
      //    this.showhidediv3 = false;
      //    this.disablebtnflag = true;
      //    this.dataSourceEmp.filteredData[this.indexView].status = "Forwarded";
      //  }
      //  else if (this.dataSource.filteredData[i].status == "E") {
      //    this.dataSourceEmp.filteredData[this.indexView].status = "Entered";
      //    this.disableflag = false;
      //    this.showhidediv2 = true;
      //    this.showhidediv3 = true;
      //    this.disablebtnflag = false;
      //  }
      //  else if (this.dataSource.filteredData[i].status == "V") {
      //    this.dataSourceEmp.filteredData[this.indexView].status = "Verified";
      //    this.showhidediv2 = false;
      //    this.showhidediv3 = false;
      //    this.disablebtnflag = false;
      //  }
      //  else {
      //    this.dataSourceEmp.filteredData[this.indexView].status = "";
      //    this.showhidediv2 = true;
      //    this.showhidediv3 = true;
      //    this.disablebtnflag = false;
      //  }

      //}
    })

    this.regincrement.getEmployeeForNonIncrement(this.regInc.DesigCode, this.regInc.PayLevel, ID).subscribe(result => {
      this.dataSourceDeAttach = new MatTableDataSource(result);
      this.dataDeAttach = result;
      this.dataSourceDeAttach.paginator = this.paginatorDetach;  //sortDetach
      this.dataSourceDeAttach.sort = this.sortDetach;
    })

    this.showhidediv1 = true;
    this.showhidediv2 = true;
    this.showhidediv3 = true;
  }
  // End


  // Save Increment Data

  id: number = 0;
  insertRegIncrementData() {
    debugger;
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      swal('Please attach at least one record!')
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          //let index: number = this.checkedObject.findIndex(c => c === d);
          //alert(index)
          this.IncrementData.push(d.empID);
          this.IncrementData.push(d.empCode);
          this.IncrementData.push(d.payLevel);
          this.IncrementData.push(this['wefDate' + this.id]);
          this.IncrementData.push(this['remark' + this.id]);
          this.IncrementData.push(d.oldBasic);
          this.IncrementData.push(d.newBasic);

          this.IncrementData.push(this.username);
          this.IncrementData.push(this.ArrddlDesign.msDesigMastID);
          this.IncrementData.push(d.orderNo);
          this.IncrementData.push(d.orderDate);
          this.id = this.id + 1;
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });

      }

      this.regincrement.insertRegIncrementData(myArray).subscribe(result => {
        if (result != undefined) {
          this.deletepopup = false;
          swal(result)
        }
      });
    }
    this.IncrementData = [];
  }

  //End


  // Forward to checker

  forwardtocheckerFor() {
    let myArray = [];
    if (this.checkedObject == null || this.checkedObject.length == 0) {
      swal('Please attach at least one record!')
    }
    else {
      if (this.checkedObject != null || this.checkedObject.length != 0) {
        this.checkedObject.forEach(d => {
          this.IncrementData.push(d.id);
          this.IncrementData.push(d.empID);
          this.IncrementData.push(this.username);
          this.regInc1 = this.IncrementData;
          myArray.push(this.regInc1);
          this.IncrementData = [];
        });
      }
      this.regincrement.forwardtocheckerForInc(myArray).subscribe(result => {
        if (result != undefined) {
          this.deletepopup = false;
          swal(result)
        }
      });
    }
    this.IncrementData = [];
  }

  //End





  clearData() {
    this.showhidediv0 = false;
    this.showhidediv1 = false;
    this.showhidediv2 = false;
    this.showhidediv3 = false;
    this._pScaleObj.msCddirID = 0;
    this.form.resetForm();
  }
  refreshData() {
    this.clearData();
  }

  //Filter

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterDetach(filterValue: string) {
    this.dataSourceDeAttach.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceDeAttach.paginatorDetach) {
      this.dataSourceDeAttach.paginatorDetach.firstPage();
    }
  }


  getcheckbox(item: any) {

    var a = this.formSave.setControl;
    //if (this.delarr.find(x => x == item)) {
    //  this.delarr.splice(this.delarr.indexOf(item), 1)
    //}
    //else {
    //  this.delarr.push(item);
    //}
  }
  //End

}
