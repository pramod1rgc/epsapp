import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegIncrementComponent } from './reg-increment.component';

describe('RegIncrementComponent', () => {
  let component: RegIncrementComponent;
  let fixture: ComponentFixture<RegIncrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegIncrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegIncrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
