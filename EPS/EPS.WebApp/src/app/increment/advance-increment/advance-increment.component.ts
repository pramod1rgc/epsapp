
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSelect } from '@angular/material';
import { AdvincrementService } from '../../services/increment/advincrement.service';
import swal from 'sweetalert2';
import { PayscaleModel } from '../../model/masters/PayscaleModel';
import { PayscaleService } from '../../services/payscale.service';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
import { takeUntil } from 'rxjs/operators';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

@Component({
  selector: 'app-advance-increment',
  templateUrl: './advance-increment.component.html',
  styleUrls: ['./advance-increment.component.css']
})
export class AdvanceIncrementComponent implements OnInit {
  Design: any[];
  EMP: any[];
  selectedIndex = 0;
  msDesigMastID: any;
  msCddirID: any;
  ArrddlDesign: DesignationModel;
  _pScaleObj: PayscaleModel;
  ArrddlEmployee: EmpModel;
  username: any;
  CommissionCodelist: any;
  advanceInc: any = {};
  btnUpdatetext: any;
  savebuttonstatus: boolean;
  dataSource: any;
  empCode: string;
  IncID: any;
  Message: any;
  disbleflag = false;
  setDeletIDOnPopup: any;
  validatingForm: FormGroup;
  showPH: boolean;
  deletepopup: boolean;
  forwardpopup: boolean;
  PermDdoId: string;
  showhidediv= true;
  disableFDflag= true;
  @Output() desigchange = new EventEmitter();
  constructor(private advincservice: AdvincrementService, private payscale: PayscaleService, private _Service: LeavesMgmtService,
 ) {
    
  }
  public empCtrl: FormControl = new FormControl();
  public empFilterCtrl: FormControl = new FormControl();
  public filteredEmp: ReplaySubject<EmpModel[]> = new ReplaySubject<EmpModel[]>(1);

  public designCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesign: ReplaySubject<DesignationModel[]> = new ReplaySubject<DesignationModel[]>(1);
  private _onDestroy = new Subject<void>();
  displayedColumns: string[] = ['EmpName', 'OldBasic', 'OldWefDate', 'NextIncDate', 'PayLevel','Flag','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('advIncrement') form: any;
  @ViewChild('singleSelect') singleSelect: MatSelect;

  ngOnInit() {
    this._pScaleObj = new PayscaleModel()
    this.bindPayCommission();
    this.btnUpdatetext = "Save";
    this.username = sessionStorage.getItem('username');
    this.savebuttonstatus = true;
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.bindDropDownDesignation(this.PermDdoId);
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
    
  }



  bindPayCommission() {
    this.payscale.BindCommissionCode().subscribe(result => {
      this.CommissionCodelist = result;
    })
  }


  bindDropDownDesignation(value) {
    this.advincservice.getAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      this.designCtrl.setValue(this.Design);
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  bindDropDownEmployee(value) {
    this.advanceInc.msCddirID = this._pScaleObj.msCddirID
    this.desigchange.emit();
    this.advincservice.GetEmployeeByDesigPayComm(value,this.advanceInc.msCddirID).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      this.empCtrl.setValue(this.EMP);
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  backForwordSearchDesignation(value) {
    this._Service.GetAllDesignation(value).subscribe(data => {
      this.ArrddlDesign = data;
      this.Design = data;
      // set initial selection
      this.designCtrl.setValue(this.Design);
      // load the initial Design list
      this.filteredDesign.next(this.Design);
    });
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesign();
      });
  }
  backForwordSearchEmployee(value) {
    this.advanceInc.msCddirID = this._pScaleObj.msCddirID
    this.advincservice.GetEmployeeByDesigPayComm(value,this.advanceInc.msCddirID).subscribe(data => {
      this.ArrddlEmployee = data;
      this.EMP = data;
      // set initial selection
      this.empCtrl.setValue(this.EMP);
      // load the initial EMP list
      this.filteredEmp.next(this.EMP);
    });
    this.empFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmp();
      });
  }
  public selectionChange($event?: StepperSelectionEvent): void {
    console.log('stepper.selectedIndex: ' + this.selectedIndex
      + '; $event.selectedIndex: ' + $event.selectedIndex);
    if ($event.selectedIndex === 0) { return; } // First step is still selected

    this.selectedIndex = $event.selectedIndex;
  }

  private filterDesign() {
    if (!this.Design) {
      return;
    }
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesign.next(this.Design.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredDesign.next(

      this.Design.filter(Design => Design.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  private filterEmp() {
    if (!this.EMP) {
      return;
    }
    let search = this.empFilterCtrl.value;
    if (!search) {
      this.filteredEmp.next(this.EMP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEmp.next(

      this.EMP.filter(EMP => EMP.empName.toLowerCase().indexOf(search) > -1)
    );
  }
  

  getIncrementDetails(value) {
    this.showhidediv = true;
    this.disableFDflag = true;
    this.empCode = value
    this.advincservice.GetIncrementDetails(value).subscribe(result => {
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      for (let i = 0; i < this.dataSource.filteredData.length; i++) {
        if (this.dataSource.filteredData[i].status == "F") {
          this.showhidediv = false;
        }
        else
          this.showhidediv = true;
      }
    })
    this.btnUpdatetext = 'Save';
    this.advanceInc.IncID = 0;
    this.resetForm();
  }


  createAdvIncrement() {
    debugger;
    this.advanceInc.loginUser = this.username;
    this.advanceInc.empCode = this.empCode;
    this.advanceInc.paylevel = this._pScaleObj.msCddirID
    this.advincservice.CreateAdvIncrement(this.advanceInc).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        swal(result)
      }
      this.resetForm();
      this.btnUpdatetext = 'Save';
      this.getIncrementDetails(this.empCode)
    })
    
    
  }


  editAdvIncrement(IncID: any) {
    this.advanceInc.IncID = this.IncID;
    this.advincservice.EditAdvIncrement(IncID).subscribe(result => {
      this.advanceInc = result[0];
      this.btnUpdatetext = 'Update';
      this.disableFDflag = false;
    })}

  deleteAdvDetails(IncID: any) {
    this.advincservice.DeleteAdvDetails(IncID).subscribe(result => {
      if (result != undefined) {
        this.deletepopup = false;
        swal(result)
      }
      this.getIncrementDetails(this.advanceInc.Empcode);
      this.form.resetForm();
      this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
      this.bindDropDownDesignation(this.PermDdoId);
      this.bindDropDownEmployee(0);

    })
  }
  

  setDeleteId(msPayScaleID) {
    this.setDeletIDOnPopup = msPayScaleID;
  }
  

  resetForm() {
    this.btnUpdatetext = 'Save';
    this.disbleflag = false;
    this.form.resetForm();
    this.advanceInc.IncID = 0;
   
  }


  forwardtoChecker() {
     this.advanceInc.loginUser = this.username;
    this.advanceInc.empCode = this.empCode;
    this.IncID = this.advanceInc.IncID;
    this.advincservice.Forwardtochecker(this.advanceInc).subscribe(result => {
      if (result != undefined) {
        this.forwardpopup = false;
        swal(result)
      }
      this.getIncrementDetails(this.advanceInc.Empcode)
      this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
      this.bindDropDownDesignation(this.PermDdoId);
      this.bindDropDownEmployee(0);
      this.resetForm();
      this.btnUpdatetext = 'Save';
    })

  }

  refreshdata() {
    this.PermDdoId = sessionStorage.getItem('EmpPermDDOId');
    this.bindDropDownDesignation(this.PermDdoId);
    this.msDesigMastID = 0;
    this.bindDropDownEmployee(this.msDesigMastID)
    this.dataSource = '';
    this.resetForm();
  }

  charaterOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 8 ) {
      return true;
    }
    return false;
  }
}
