//A 15-feb+
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar, MatCheckbox } from '@angular/material';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';
import { CommonLeavesSanctionComponent } from '../common-leaves-sanction/common-leaves-sanction.component';
//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
import swal from 'sweetalert2';
//import dayjs from 'dayjs';
import { DesigEmpOrdnoComponent } from '../../shared-module/desig-emp-ordno/desig-emp-ordno.component'
import { DISABLED } from '@angular/forms/src/model';
import * as $ from 'jquery';
//import { forEach } from '@angular/router/src/utils/collection';
export interface PeriodicElement {
  CurtExtenOrderNo: string;
  CurtExtenOrderDate: string;
  Leavetype: string;
  FromDate: string;
  ToDate: string;
}
var ELEMENT_DATA: PeriodicElement[];
@Component({
  selector: 'app-leaves-cancell',
  templateUrl: './leaves-cancell.component.html',
  styleUrls: ['./leaves-cancell.component.css']
})
export class LeavesCancellComponent implements OnInit {
  displayedCancel: string[] = ['CancelOrderNo', 'CancelOrderDate','LeaveType','FromDate','ToDate','NoOfDays','CancelReason','Status'];
  highlightedRows = [];
  dataSourceCancel = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSourceCancelgrid:any[] =[];
  alldataSourceCancelgrid: any[];

  dataSourceCancelHistory = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSourceCancelHistorygrid: any[] = [];
  alldataSourceCancelHistorygrid: any[];
  
  currentdate=new Date();
  //dataSourceCurtail = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  //displayedColumnCUrtExten: string[] = ['CurtExtenOrderNo', 'CurtExtenOrderDate', 'Leavetype', 'FromDate', 'ToDate', 'action'];
  leaveSanctionDetailsForm: FormGroup;
  leaveSanctionDetails: FormArray;
  leaveCancelForm: FormGroup;
  ArrddlReason: ReasonForLeaveModel;
  ArrddlLeavesType: LeavesTypeModel[];
  showAddButton: boolean = false;
  showDeleteButton: boolean = false;
  showCheckBox0: boolean = false;
  radiochecked: boolean = true;
  isVarified: boolean;
  isReasonEmpty: boolean;
  reasonText: string;
  leaveMainid: string;
  SanctionOrderNo: any;
  Cancel: string = "Cancel";
  //buttonText: string;
  employeeCode: string;
  PermDdoId: string = sessionStorage.getItem('EmpPermDDOId');
  UserRoleId: string = sessionStorage.getItem('userRoleID');
  firstdate: Date;
  lastdate: Date;
  datedif: number;
  parentLeaveData: any;
  showReturnReason: boolean = false;
  deletepopup: boolean;
  hooCheckerLogin: boolean;
  constructor(private _Service: LeavesMgmtService, private snackBar: MatSnackBar, private formBuilder: FormBuilder) { this.DetailsForm(); this.CancelForm() }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('b') paginator1: MatPaginator;
  @ViewChild('a') sort1: MatSort;
  @ViewChild('LeaveSanctionForm') form1: any;
  @ViewChild('LeaveCancelForm') form2: any;
  @ViewChild(DesigEmpOrdnoComponent) child: DesigEmpOrdnoComponent; 
  ngOnInit() {
    debugger;
    this.BindDropDownLeavesReason();
    this.BindDropDownLeavesType();

    this.leaveSanctionDetailsForm.disable();
    this.leaveCancelForm.disable();
    this.leaveCancelForm.get('cancelOrderDt').disable();
    this.isVarified = true;
    this.isReasonEmpty = false;
  }

  DetailsForm() {

    this.leaveSanctionDetailsForm = this.formBuilder.group({
      empCd: [this.employeeCode],
      empName: [null],
      permDDOId: [this.PermDdoId],
      orderNo: [{ value: null, disabled: false }, Validators.required],
      orderDT: [{ value: null, disabled: false }, Validators.required],
      leaveCategory: [{ value: null, disabled: false }, Validators.required],
      leaveReasonCD: [null, Validators.required],         
      remarks: [null],
      verifyFlg: [null],
      leaveReasonDesc: [null],
      leaveMainId: [0],
      leaveSanctionDetails: this.formBuilder.array([this.createItem()])
    });
  }
  createItem(): FormGroup {
    return this.formBuilder.group({
      //leaveDetailsId: [{ value: null, disabled: false }, Validators.required],
      leaveCd: [{ value: null, disabled: false }, Validators.required],
      fromDT: [{ value: null, disabled: false }, Validators.required],
      toDT: [{ value: null, disabled: false }, Validators.required],
      noDays: [{ value: null, disabled: false }, Validators.required],
      maxLeave: [{ value: null, disabled: false }, Validators.required],
      isMaxLeave: [{ value: false, disabled: false }]
    });
  }
  CancelForm() {
    this.leaveCancelForm = this.formBuilder.group({
      preLeaveMainId: [this.leaveMainid],     
      cancelOrderNo: [null, Validators.required],
      cancelOrderDt: [null, Validators.required],
      cancelReason: [null, Validators.required],
      returnReason:[null],
      cancelRemark: [null],
      cancelVerifyFlg: [null],

    });
  }
  BindDropDownLeavesReason() {
    this._Service.GetLeavesReason().subscribe(data => {
      this.ArrddlReason = data;
    });

  }
  BindDropDownLeavesType() {
    this._Service.GetLeavesType().subscribe(data => {
      this.ArrddlLeavesType = data;
    });
  }
  //Search Form
  designationChange() {
    debugger;
    this.leaveSanctionDetailsForm.disable();
    this.leaveCancelForm.disable();
    this.showReturnReason = false;
    this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
   
    this.CancelForm();
    
    //this.leaveCancelForm.get('cancelOrderDt').disable();
    if (this.UserRoleId == "8") { }
    else {
     
    }
    this.leaveSanctionDetailsForm.disable();
    this.leaveCancelForm.disable();
  }
  employechange(value) {
    debugger;
    this.leaveSanctionDetailsForm.disable();
    this.leaveCancelForm.disable();
    this.showReturnReason = false;
    this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
   // this.leaveSanctionDetailsForm.disable();
    this.CancelForm();
   // this.leaveCancelForm.disable();
    //this.leaveCancelForm.get('cancelOrderDt').disable();
    if (this.UserRoleId == "8") {

    }
    else {

    }

    this._Service.GetLeavesCancel(this.PermDdoId, value, this.UserRoleId).subscribe(result => {
      debugger;
      this.alldataSourceCancelgrid = result;
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData(event);

      this.dataSourceCancel = new MatTableDataSource(result);
      this.dataSourceCancel.paginator = this.paginator;
      this.dataSourceCancel.sort = this.sort;


      //this.dataSourceCancelgrid = result;
      //this.dataSourceCancel = new MatTableDataSource(result);
      //this.dataSourceCancel.paginator = this.paginator;
      //this.dataSourceCancel.sort = this.sort;
    })


    this._Service.GetLeavesCancelHistory(this.PermDdoId, value, this.UserRoleId).subscribe(result => {
      debugger;

      this.alldataSourceCancelHistorygrid = result;
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData1(event);

      this.dataSourceCancelHistory = new MatTableDataSource(result);
      this.dataSourceCancelHistory.paginator = this.paginator1;
      this.dataSourceCancelHistory.sort = this.sort1;

      //this.dataSourceCancelHistorygrid = result;
      //this.dataSourceCancelHistory = new MatTableDataSource(result);
      //this.dataSourceCancel.paginator = this.paginator;
      //this.dataSourceCancel.sort = this.sort;
    })
    this.leaveSanctionDetailsForm.disable();
    this.leaveCancelForm.disable();
  }

  getPaginationData(event) {
    debugger;
    this.dataSourceCancelgrid = this.alldataSourceCancelgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  getPaginationData1(event) {
    debugger;
    this.dataSourceCancelHistorygrid = this.alldataSourceCancelHistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  GetLeavesSanction(value) {
    debugger; this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionDetailsForm.disable();
    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.CancelForm();
    this.leaveCancelForm.disable();

    if (this.UserRoleId == "8") {

    }
    else {

    }

    //this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");


    this._Service.GetVerifiedLeaveForCancel(this.PermDdoId, value).subscribe(result => {
      debugger;
      this.leaveMainid = result.leavesSanctionMainModel.leaveMainId;
      this.leaveSanctionDetailsForm.get('leaveMainId').setValue(this.leaveMainid);
      this.leaveSanctionDetailsForm.get('empCd').setValue(result.leavesSanctionMainModel.empCd);
      this.leaveSanctionDetailsForm.get('permDDOId').setValue(result.leavesSanctionMainModel.permDDOId);
      this.leaveSanctionDetailsForm.get('orderNo').setValue(result.leavesSanctionMainModel.orderNo);
      this.leaveSanctionDetailsForm.get('orderDT').setValue(result.leavesSanctionMainModel.orderDT);
      this.leaveSanctionDetailsForm.get('leaveCategory').setValue(result.leavesSanctionMainModel.leaveCategory);
      this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(result.leavesSanctionMainModel.leaveReasonCD);
      
      this.leaveCancelForm.get('preLeaveMainId').setValue(this.leaveMainid);
      this.leaveCancelForm.get('cancelOrderNo').setValue(result.cancelOrderNo);
      this.leaveCancelForm.get('cancelOrderDt').setValue(result.cancelOrderDt);
      this.leaveCancelForm.get('cancelReason').setValue(result.cancelReason);
      debugger;
      if (result.returnReason != null) {
        this.showReturnReason = true;
        this.leaveCancelForm.get('returnReason').setValue(result.returnReason);
      }
      else { this.showReturnReason = false; }

      this.leaveCancelForm.get('cancelRemark').setValue(result.cancelRemark);

      for (var i = 0; i < result.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
        //(<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveDetailsId').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveDetailsId);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
      }
      this.leaveSanctionDetailsForm.disable();

      if (this.UserRoleId == "8") {
        this.leaveCancelForm.disable();
        this.leaveCancelForm.get('cancelOrderDt').disable();
      }
      else {
        this.leaveCancelForm.enable();
        this.leaveCancelForm.get('cancelOrderDt').enable();
      }
    })


  }
  get getleaveDetails() {
    return this.leaveSanctionDetailsForm.get('leaveSanctionDetails') as FormArray;
  }
  //operational Form

  LeaveTypeItemChange(i, value) {
    debugger;
    //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
    //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);

  }
  ChangeLeavesType(value, index) {
    debugger;
    //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
    //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
  }
  ChangeLeaveCategory(value) {
    //this.objlsmain.leaveSanctionDetails = [new LeavesSanctionDetailsModel()];
    //this.showDeleteButton = value == 'Single' ? false : true;
    //this.showAddButton = value == 'Single' ? false : true;
    //alert(this.findInvalidControls());
  }
  fillnoofDays(value, index) {
    debugger;

    //if (this.leaveSanctionDetailsForm.get('fromDT_' + index).value != "undefined" || this.leaveSanctionDetailsForm.get('toDT_' + index).value) {
    //  this.firstdate = new Date(this.leaveSanctionDetailsForm.get('fromDT_' + index).value);
    //  this.lastdate = new Date(this.leaveSanctionDetailsForm.get('toDT_' + index).value);
    //  if (this.firstdate > this.lastdate) {
    //    alert('From date should be less than to date');
    //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(0);
    //    value == "s" ? this.leaveSanctionDetailsForm.get('fromDT_' + index).setValue("") : this.leaveSanctionDetailsForm.get('toDT_' + index).setValue("");
    //  }
    //  else {
    //    this.datedif = Math.abs(this.firstdate.getTime() - this.lastdate.getTime());
    //    this.datedif = Math.ceil(this.datedif / (1000 * 3600 * 24)) + 1;
    //    this.leaveSanctionDetailsForm.get('noDays_' + index).setValue(this.datedif);
    //  }

    //}
  }

  SaveLeavesCancel(value) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCancelForm.get('cancelOrderNo').value) {
      this.leaveCancelForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
      this._Service.SaveLeavesCancel(this.leaveCancelForm.value, value).subscribe(result => {
        if (parseInt(result) >= 1) {
          if (value == "SI") {
            this.snackBar.open('Saved Successfully', null, { duration: 4000 });
          }
          else {
            this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
          }
          this.leaveCancelForm.reset();
          this.leaveSanctionDetailsForm.reset();
          this.form1.resetForm();
          this.form2.resetForm();
          this.child.BindDropDownOrder(this.employeeCode);
        } else {
          if (value == "SI") {
            this.snackBar.open('Save  not Successfull', null, { duration: 4000 });
          }
          else {
            this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
          }
        }
      })
    }
    else {
      alert('Sanction orderNo and Cancel Order No Cant be same.');
    }
    this.leaveSanctionDetailsForm.disable();  
  }
  //ForwardToChecker() {
  //  this.leaveCancelForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
  //  this.leaveCancelForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
  //  this.leaveCancelForm.get('cancelEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
  //  this.leaveCancelForm.get('cancelPermDdoId').setValue(this.PermDdoId);
  //  if (this.leaveCancelForm.get('cancelId').value == null) {
  //    this.leaveCancelForm.get('cancelId').setValue(0);
  //  }
  //  debugger;
  //  this._Service.SaveLeavescurtExten(this.leaveCancelForm.value, "F").subscribe(result => {
  //    this.snackBar.open('Save Successfully', null, { duration: 5000 });
  //    this.GetLeavesSanction(this.employeeCode);
  //  })
  //}
  VerifiedSanc(flag: boolean) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (flag) {
      this._Service.VerifyReturnSanction(this.leaveSanctionDetailsForm.get('leaveMainId').value, "SV", "Cance", null).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Verified Successfully', null, { duration: 4000 });
          this.leaveCancelForm.reset();
          this.leaveSanctionDetailsForm.reset();
          this.form1.resetForm();
          this.form2.resetForm();
          this.employechange(this.employeeCode);
        } else {
          this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
        }
      })
      $('.dialog__close-btn').click();
      this.hooCheckerLogin = false;
    } else {
      this.isVarified = true;
    }

  }


  RejectedSanc(flag: boolean) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (flag) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        return false;
      }
      this._Service.VerifyReturnSanction(this.leaveSanctionDetailsForm.get('leaveMainId').value, "SR", "Cance", this.reasonText).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Returned Successfully', null, { duration: 4000 });
          this.leaveCancelForm.reset();
          this.leaveSanctionDetailsForm.reset();
          this.form1.resetForm();
          this.form2.resetForm();
          this.employechange(this.employeeCode);
          this.reasonText = null;
          this.hooCheckerLogin = false;;
        } else {
          this.snackBar.open('Return not Successfull', null, { duration: 4000 });
        }
      })
      $('.dialog__close-btn').click();
    }
    else {
      this.isVarified = false;
      return false;
    }
  }


  ResetLeavesSanction() {
    this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");


  }

  //Grid operation
  ShowLeavesSanction(value) {
    debugger;
    this.leaveMainid = value.leaveMainId;
    this.SanctionOrderNo = value.orderNo;

    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.form2.resetForm();
    this.showDeleteButton = value.leaveCategory == 'Single' ? false : true;
    this.showAddButton = value.leaveCategory == 'Single' ? false : true;
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
    this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
    for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
    }
  }
  EditCancelLeaves(element,flg) {
    debugger;
   
    this.leaveMainid = element.leavesSanctionMainModel.leaveMainId;
    this.employeeCode = element.leavesSanctionMainModel.empCd;
    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.showDeleteButton = element.leaveCategory == 'Single' ? false : true;
    this.showAddButton = element.leaveCategory == 'Single' ? false : true;
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
    this.leaveCancelForm.get('cancelOrderNo').setValue(element.cancelOrderNo);
    this.leaveCancelForm.get('cancelReason').setValue(element.cancelReason);
    this.leaveCancelForm.get('cancelRemark').setValue(element.cancelRemark);
    this.leaveCancelForm.get('cancelOrderDt').setValue(element.cancelOrderDt);
    if (element.leavesSanctionMainModel.returnReason != null) {
      this.showReturnReason = true;
      this.leaveCancelForm.get('returnReason').setValue(element.leavesSanctionMainModel.returnReason);
    }
    else { this.showReturnReason = false; }
    
    

    for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
    }
    this.leaveSanctionDetailsForm.disable();
    if (flg == "dis") {
      this.leaveCancelForm.disable();
    }
    else {
      this.leaveCancelForm.enable();
      //this.leaveCancelForm.get('curtLeaveCd').disable();
      //this.leaveCancelForm.get('curtFromDT').disable();
      //this.leaveCurtailForm.get('curtNoDays').setValue('50');
      //this.showForward = false;
      //this.showCancel = false;
      //this.showSave = false;
    }

  }

  DeleteCancelLeaves(leaveMainId: string, flag: boolean) {
    debugger;
    this.leaveMainid = flag ? leaveMainId : "0";
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (!flag) {
      this._Service.DeleteLeavesSanction(leaveMainId, 'Canc').subscribe(result => {
        this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
        this.child.BindDropDownOrder(this.employeeCode);
      });
      this.leaveSanctionDetailsForm.disable();
    }
    this.deletepopup = false;
  }

  checkValidation() {


  }
  validateOrdNo(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48)) {
      return true;
    }
    return false;
  }

  rowHighlight(row) {
    this.highlightedRows.pop();
    this.highlightedRows.push(row)
  }
  abc() {
    alert(this.findInvalidControls());
  }
  //Extra methods
  public findInvalidControls() {
    debugger;
    const invalid = [];
    const controls = this.leaveSanctionDetailsForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

}
