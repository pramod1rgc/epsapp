

//A 15-feb+
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar, MatCheckbox } from '@angular/material';
import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';
import { CommonLeavesSanctionComponent } from '../common-leaves-sanction/common-leaves-sanction.component';
//import { EmpModel, DesignationModel } from '../../model/Shared/DDLCommon';
import { DesigEmpOrdnoComponent } from '../../shared-module/desig-emp-ordno/desig-emp-ordno.component'
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
//import dayjs from 'dayjs';
import { DISABLED } from '@angular/forms/src/model';
import * as $ from 'jquery';
//import { forEach } from '@angular/router/src/utils/collection';
export interface PeriodicElement {
  CurtExtenOrderNo: string;
  CurtExtenOrderDate: string;
  Leavetype: string;
  FromDate: string;
  ToDate: string;
}
export interface PeriodicElement1 {
  SanctionOrderNo: string;
  SanctionOrderDate: number;
  LeaveReason: number;
  LeaveCategory: string;
}
var ELEMENT_DATA: PeriodicElement[];
var ELEMENT_DATA1: PeriodicElement1[];
@Component({
  selector: 'app-leaves-curtail',
  templateUrl: './leaves-curtail.component.html',
  styleUrls: ['./leaves-curtail.component.css'],
  providers: [DatePipe]
})
export class LeavesCurtailComponent implements OnInit {

  btnclose() {
    this.is_btnStatus = false;
  }
  is_btnStatus = true;

  dataSource= new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSourceforgrid: any[]=[];
  alldataSourceforgrid: any[];

  dataSourceCurtailHistory = new MatTableDataSource<PeriodicElement1>(ELEMENT_DATA1);
  dataSourceCurtailHistorygrid: any[] = [];
  alldataSourceCurtailHistorygrid: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('b') paginator1: MatPaginator;
  @ViewChild('a') sort1: MatSort;
  currentdate=new Date();
  //dataSourceCurtail = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  //displayedColumnCUrtExten: string[] = ['CurtExtenOrderNo', 'CurtExtenOrderDate', 'Leavetype', 'FromDate', 'ToDate', 'action'];
  orderList: any;
  leaveSanctionDetailsForm: FormGroup;
  leaveSanctionDetails: FormArray;
  leaveCurtailForm: FormGroup;
  ArrddlReason: ReasonForLeaveModel;
  ArrddlLeavesType: LeavesTypeModel[];
  radiochecked: boolean = true;
  leaveMainid: string;
  leaveCurtailid: number;
  SanctionOrderNo: any;
  Curtail: string = "Curtail";
  mxcutaildate: Date;
  mncutaildate: Date;
  buttonText: string;
  employeeCode: string;
  PermDdoId: string = sessionStorage.getItem('EmpPermDDOId');
  UserRoleId: string = sessionStorage.getItem('userRoleID');
  firstdate: string;
  lastdate: string;
  datedif: number;
  showCancel: boolean = true;
  showForward: boolean = true;
  showSave: boolean = true;
  parentLeaveData: any;
  isVarified: boolean;
  isReasonEmpty: boolean;
  reasonText: string;
  showReturnReason: boolean = false;
  deletepopup: boolean;
  hooCheckerLogin: boolean;
  constructor(private datePipe: DatePipe,private _Service: LeavesMgmtService, private snackBar: MatSnackBar, private formBuilder: FormBuilder) { this.DetailsForm(); this.CurtailForm() }


  @ViewChild('LeaveSanctionForm') form1: any;
  @ViewChild('LeaveCurtailForm') form2: any;
  @ViewChild(DesigEmpOrdnoComponent) child: DesigEmpOrdnoComponent; 
  ngOnInit() {
    debugger;
    this.BindDropDownLeavesReason();
    this.BindDropDownLeavesType();

    this.leaveSanctionDetailsForm.disable();
    this.leaveCurtailForm.disable();
    debugger;
    if (this.UserRoleId == "8") {

    }
    else {
      this.buttonText = "Save";
      this.showForward = true;
      this.showCancel = true;
      this.showSave = true;
    }
    this.isVarified = true;
    this.isReasonEmpty = false;
  }

  DetailsForm() {
    this.leaveSanctionDetailsForm = this.formBuilder.group({
      leaveMainId: [0],
      empCd: [this.employeeCode],
      permDDOId: [this.PermDdoId],
      orderNo: [null, Validators.required],
      orderDT: [null, Validators.required],
      leaveCategory: [null, Validators.required],
      leaveReasonCD: [null, Validators.required],
      leaveReasonDesc:[null],
      remarks:[null],
      leaveSanctionDetails: this.formBuilder.array([this.createItem()])
    });
  }
  createItem(): FormGroup {
    return this.formBuilder.group({
      leaveDetailsId: [0],
      leaveCd: [ null, Validators.required],
      fromDT: [null,  Validators.required],
      toDT: [null, Validators.required],
      noDays: [ null, Validators.required],
      //maxLeave: [{ value: null, disabled: false }, Validators.required],
      //isMaxLeave: [{ value: false, disabled: false }]
    });
  }
  CurtailForm() {
    this.leaveCurtailForm = this.formBuilder.group({
      curtId: [0],
      curtOrderNo: [null, Validators.required],
      curtOrderDT: [null, Validators.required],     
      isCurtleave: [null, Validators.required],
      curtReason: [null, Validators.required],
      curtRemarks: [null],
      returnReason:[null],
      curtLeaveDetailsId:[0],
      curtLeaveCd: [{ value: null, disabled: true}, Validators.required],
      curtFromDT: [{ value: null, disabled: true }, Validators.required],
      curtToDT: [null, Validators.required],
      curtNoDays: [{ value: null, disabled: true }, Validators.required],
      curtEmpCd: [null],
      curtPermDdoId: [null],
      preOrderNo: [null],
      preLeaveMainId: [null],
    });
  }
  BindDropDownLeavesReason() {
    this._Service.GetLeavesReason().subscribe(data => {
      this.ArrddlReason = data;
    });

  }
  BindDropDownLeavesType() {
    this._Service.GetLeavesType().subscribe(data => {
      this.ArrddlLeavesType = data;
    });
  }
  //Search Form
  designationChange() {
    debugger;
    this.leaveSanctionDetailsForm.disable();
    this.leaveCurtailForm.disable();
    this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.CurtailForm();
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    //this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
    //this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
    //this.leaveCurtailForm.get('curtFromDT').setValue(result.curtFromDT);
    //this.leaveCurtailForm.get('curtToDT').setValue(result.curtToDT);
    this.showReturnReason = false;
    if (this.UserRoleId == "8") {

    }
    else {
      this.buttonText = "Save";
      this.showForward = true;
      this.showCancel = true;
      this.showSave = true;
    }
    this.leaveSanctionDetailsForm.disable();
    this.leaveCurtailForm.disable();
  }
  employechange(value) {
    debugger;
    this.leaveSanctionDetailsForm.disable();
    this.leaveCurtailForm.disable();
    this.showReturnReason = false;
    this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.CurtailForm();
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
    this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
    this.leaveSanctionDetailsForm.disable();
    this.leaveCurtailForm.disable();
    if (this.UserRoleId == "8") {

    }
    else {
      this.buttonText = "Save";
      this.showForward = true;
      this.showCancel = true;
      this.showSave = true;
    }
    this._Service.GetLeavesCurtailDetail(this.PermDdoId, value, this.UserRoleId).subscribe(result => {
      debugger;
      this.alldataSourceforgrid = result;
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData(event);

      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    })


    this._Service.GetLeavesCurtailHistory(this.PermDdoId, value, this.UserRoleId).subscribe(result => {
      debugger;

      this.alldataSourceCurtailHistorygrid = result;
      let event = { pageIndex: 0, pageSize: 5 };
      this.getPaginationData1(event);

      //this.dataSourceHistory = new MatTableDataSource(result);
      //this.dataSourceHistory.paginator = this.paginator1;
      //this.dataSourceHistory.sort = this.sort1;

      //this.alldataSourceCurtailHistorygrid = result;
      //let event = { pageIndex: 0, pageSize: 5 };
      //this.getPaginationData1(event);


      this.dataSourceCurtailHistory = new MatTableDataSource(result);
      this.dataSourceCurtailHistory.paginator = this.paginator1;
      this.dataSourceCurtailHistory.sort = this.sort1;
    })

   
  
  }
  GetLeavesSanction(value) {
    debugger; this.DetailsForm();
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionDetailsForm.disable();   
    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.CurtailForm();
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    this.leaveCurtailForm.disable();

    //if (this.UserRoleId == "8") {

    //}
    //else {
      this.buttonText = "Save";
      this.showForward = true;
      this.showCancel = true;
      this.showSave = true;
    //}
  
    //this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
   
    
    this._Service.GetVerifiedLeavesSanction(this.PermDdoId, value).subscribe(result => {
      debugger;
      this.leaveMainid = result.leavesSanctionMainModel.leaveMainId;
      this.leaveSanctionDetailsForm.get('leaveMainId').setValue(this.leaveMainid);
      this.leaveSanctionDetailsForm.get('empCd').setValue(result.leavesSanctionMainModel.empCd);
      this.leaveSanctionDetailsForm.get('permDDOId').setValue(result.leavesSanctionMainModel.permDdoId);
      this.leaveSanctionDetailsForm.get('orderNo').setValue(result.leavesSanctionMainModel.orderNo);
      this.leaveSanctionDetailsForm.get('orderDT').setValue(result.leavesSanctionMainModel.orderDT);
      this.leaveSanctionDetailsForm.get('leaveCategory').setValue(result.leavesSanctionMainModel.leaveCategory);
      this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(result.leavesSanctionMainModel.leaveReasonCD);
      
      
     
      for (var i = 0; i < result.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveDetailsId').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveDetailsId);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
        (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(result.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);
      }
      this.leaveCurtailid = result.curtId;
      this.leaveCurtailForm.get('curtId').setValue(result.curtId);
      this.leaveCurtailForm.get('curtOrderNo').setValue(result.curtOrderNo);
      this.leaveCurtailForm.get('curtOrderDT').setValue(result.curtOrderDT);
      if (result.isCurtleave == null || result.isCurtleave == 'Curtail') {
        this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
        this.mxcutaildate = result.curtToDT;
        this.mncutaildate = result.curtFromDT;
      }
      else {
        this.leaveCurtailForm.get('isCurtleave').setValue(result.isCurtleave);
        this.mxcutaildate = this.currentdate;
        this.mncutaildate = result.curtToDT;
      }
      
      

      this.leaveCurtailForm.get('curtReason').setValue(result.curtReason);
      this.leaveCurtailForm.get('curtRemarks').setValue(result.curtRemarks);
      debugger;
      if (result.returnReason != null) {
        this.showReturnReason = true;
        this.leaveCurtailForm.get('returnReason').setValue(result.returnReason);
      }
      else { this.showReturnReason = false; }

      

      this.leaveCurtailForm.get('curtLeaveDetailsId').setValue(result.curtLeaveDetailsId);
      this.leaveCurtailForm.get('curtLeaveCd').setValue(result.curtLeaveCd);
      this.leaveCurtailForm.get('curtFromDT').setValue(result.curtFromDT);
      this.leaveCurtailForm.get('curtToDT').setValue(result.curtToDT);
      this.leaveCurtailForm.get('curtNoDays').setValue(result.curtNoDays);

      this.leaveCurtailForm.get('curtEmpCd').setValue(result.leavesSanctionMainModel.empCd);
      this.leaveCurtailForm.get('curtPermDdoId').setValue(result.leavesSanctionMainModel.permDdoId);
      this.leaveCurtailForm.get('preOrderNo').setValue(result.leavesSanctionMainModel.orderNo);
      this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveMainid);

      //result.curtId == 0 ? this.buttonText = "Save" : this.buttonText = "Update";
      this.leaveSanctionDetailsForm.disable();
      //if (this.UserRoleId == "8") {
      //  this.leaveCurtailForm.disable();
      //}
      //else {
        this.leaveCurtailForm.enable();
        this.leaveCurtailForm.get('curtLeaveCd').disable();
        this.leaveCurtailForm.get('curtFromDT').disable();
        //this.leaveCurtailForm.get('curtNoDays').setValue('50');
        this.showForward = false;
        this.showCancel = false;
        this.showSave = false;
      //}
    })

   
    //this.abc();
  }
  get getleaveDetails() {
    return this.leaveSanctionDetailsForm.get('leaveSanctionDetails') as FormArray;
  }
  //operational Form

  LeaveTypeItemChange(i, value) {
    debugger;
    //alert(this.leaveSanctionDetailsForm.get('leaveCd_' + i));
    //this.leaveSanctionDetailsForm.get('leaveCd_' + i).setValue(value);

  }
  ChangeLeavesType(value, index) {
    debugger;
    //this.showCheckBox0 = this.leaveSanctionDetailsForm.get('leaveCd_' + index).value == 'CHL' ? true : false;
    //this.leaveSanctionDetailsForm.addControl('isMaxLeave_' + index, new FormControl());
  }
  ChangeLeaveCategory(value) {
    if (value == 'Curtail') {
      this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
      this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
    }
    else {
      this.mxcutaildate = this.currentdate;
      this.mncutaildate = this.leaveCurtailForm.get('curtToDT').value;
    }
  }
  getPaginationData(event) {
    this.dataSourceforgrid = this.alldataSourceforgrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  getPaginationData1(event) {
    this.dataSourceCurtailHistorygrid = this.alldataSourceCurtailHistorygrid.slice(event.pageIndex * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }
  SaveLeavesCurtExten(value) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    let detailsFormLength: number = this.leaveSanctionDetailsForm.get('leaveSanctionDetails').value.length - 1;
    let sancdays: number = parseInt((<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(detailsFormLength).get('noDays').value);
    let curtdays: number = this.leaveCurtailForm.get('curtNoDays').value;
    if (sancdays != curtdays)
    //if (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);)
    {
      sancdays > curtdays ? this.leaveCurtailForm.get('isCurtleave').setValue('Curtail') : this.leaveCurtailForm.get('isCurtleave').setValue('Extension');
    }
    else {
      alert('please Extend or curtail the leave');
      return false;
    }
    if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCurtailForm.get('curtOrderNo').value)
    { 
    this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
    this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
    this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
    this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
    if (this.leaveCurtailForm.get('curtId').value == null) {
      this.leaveCurtailForm.get('curtId').setValue(0);
      }
        this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, value).subscribe(result => {
          if (parseInt(result) >= 1) {
            if (value == "I") {
              this.snackBar.open('Saved Successfully', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
            }
            this.snackBar.open('Saved Successfully', null, { duration: 4000 });
            this.child.BindDropDownOrder(this.employeeCode);
            this.leaveCurtailForm.reset();
            this.leaveSanctionDetailsForm.reset();
            this.form1.resetForm();
            this.form2.resetForm();
          
            //this.GetLeavesSanction("0");
          } else {
            if (value == "I") {
              this.snackBar.open('Save  not Successfull', null, { duration: 4000 });
            }
            else {
              this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
            }
          }
        })
        
  }
    else {
      alert('Sanction orderNo and Curtail/Extension Order No Cant be same.');
    }
    this.leaveSanctionDetailsForm.disable();  
  }
  ForwardToChecker() {

    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    debugger;
    let detailsFormLength: number = this.leaveSanctionDetailsForm.get('leaveSanctionDetails').value.length - 1;
    let sancdays: number = parseInt((<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(detailsFormLength).get('noDays').value);
    let curtdays: number = this.leaveCurtailForm.get('curtNoDays').value;
    if (sancdays != curtdays)
    //if (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);)
    {
      sancdays > curtdays ? this.leaveCurtailForm.get('isCurtleave').setValue('Curtail') : this.leaveCurtailForm.get('isCurtleave').setValue('Extension');
    }
    else {
      alert('please Extend or curtail the leave');
      return false;
    }
    if (this.leaveSanctionDetailsForm.get('orderNo').value != this.leaveCurtailForm.get('curtOrderNo').value) {
      this.leaveCurtailForm.get('preLeaveMainId').setValue(this.leaveSanctionDetailsForm.get('leaveMainId').value);
      this.leaveCurtailForm.get('preOrderNo').setValue(this.leaveSanctionDetailsForm.get('orderNo').value);
      this.leaveCurtailForm.get('curtEmpCd').setValue(this.leaveSanctionDetailsForm.get('empCd').value);
      this.leaveCurtailForm.get('curtPermDdoId').setValue(this.PermDdoId);
      if (this.leaveCurtailForm.get('curtId').value == null) {
        this.leaveCurtailForm.get('curtId').setValue(0);
      }
      if (this.UserRoleId == "9") {
        this._Service.SaveLeavescurtExten(this.leaveCurtailForm.value, "F").subscribe(result => {
          if (parseInt(result) >= 1) {
            this.snackBar.open('Forwarded Successfully', null, { duration: 4000 });
            this.leaveCurtailForm.reset();
            this.leaveSanctionDetailsForm.reset();
            this.form1.resetForm();
            this.form2.resetForm();
            this.child.BindDropDownOrder(this.employeeCode);
            //this.GetLeavesSanction("0");
          } else {
            this.snackBar.open('Forward not Successfull', null, { duration: 4000 });
          }
        })
      }   
    }
    else {
      alert('Sanction orderNo and Curtail/Extension Order No Cant be same.');
    }
    if (this.UserRoleId == "8") {
      this.leaveCurtailForm.disable();
    }
    //this.child.BindDropDownOrder(this.leaveSanctionDetailsForm.get('empCd').value);
    this.leaveSanctionDetailsForm.disable(); 
  }

  ResetLeavesSanction() {
    this.showReturnReason = false;

    this.leaveCurtailForm.reset();
    this.leaveSanctionDetailsForm.reset();
    this.form1.resetForm();
    this.form2.resetForm();

    this.leaveSanctionDetailsForm.get('leaveCategory').setValue("Single");
    this.leaveSanctionDetailsForm.disable();
    //if (this.UserRoleId == "8") {
    //  this.leaveCurtailForm.disable();
    //}
  }


  VerifiedSanc(flag: boolean) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (flag) {     
      this._Service.VerifyReturnSanction(this.leaveCurtailForm.get('curtId').value, "V", "CurtE", null).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Verified Successfully', null, { duration: 4000 });
          this.leaveCurtailForm.reset();
          this.leaveSanctionDetailsForm.reset();
          this.form1.resetForm();
          this.form2.resetForm();
          this.employechange(this.employeeCode);
        } else {
          this.snackBar.open('Verification not Successfull', null, { duration: 4000 });
        }
      })
      $('.dialog__close-btn').click();  
    this.hooCheckerLogin = false;
  } else {
  this.isVarified = true;
}

  }


  RejectedSanc(flag: boolean) {
    debugger;
    this.employeeCode = this.leaveSanctionDetailsForm.get('empCd').value;
    if (flag) {
      this.isReasonEmpty = false;
      if (this.reasonText.trim() === '') {
        this.isReasonEmpty = true;
        return false;
      }   
      this._Service.VerifyReturnSanction(this.leaveCurtailForm.get('curtId').value, "R", "CurtE", this.reasonText).subscribe(result => {
        debugger;
        if (parseInt(result) >= 1) {
          this.snackBar.open('Returned Successfully', null, { duration: 4000 });
          this.leaveCurtailForm.reset();
          this.leaveSanctionDetailsForm.reset();
          this.form1.resetForm();
          this.form2.resetForm();
          this.employechange(this.employeeCode);;
          this.reasonText = null;
          this.hooCheckerLogin = false;
         // $('.dialog__close-btn').click();
        } else {
          this.snackBar.open('Return not Successfull', null, { duration: 4000 });
        }
      })
      $('.dialog__close-btn').click();
    }
    else {
      this.isVarified = false;
      return false;
    }
    
  }

  //Grid operation
  ShowLeavesSanction(value) {
    debugger;
    this.leaveMainid = value.leaveMainId;
    this.SanctionOrderNo = value.orderNo;

    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.form2.resetForm();
    this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
    this.mxcutaildate = this.leaveCurtailForm.get('curtToDT').value;
    this.mncutaildate = this.leaveCurtailForm.get('curtFromDT').value;
   //this.leaveCurtailForm.setControl('leaveSanctionDetails', new FormArray([]));
    //this.leaveCurtailForm.reset();
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    this.buttonText = "Save";
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(value.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(value.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(value.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(value.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(value.leaveReasonCD);
    this.leaveSanctionDetailsForm.get('remarks').setValue(value.remarks);
    for (var i = 0; i < value.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(value.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(value.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(value.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(value.leaveSanctionDetails[i].noDays);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('maxLeave').setValue(value.leaveSanctionDetails[i].maxLeave);
    }
    this.leaveSanctionDetailsForm.disable();
    if (this.UserRoleId == "8") {
      this.leaveCurtailForm.disable();
    }
  }
  EditCurtailLeaves(element,flg) {
    debugger;
    this.leaveMainid = element.leavesSanctionMainModel.leaveMainId
    this.employeeCode = element.curtEmpCd;
    this.leaveSanctionDetailsForm.setControl('leaveSanctionDetails', new FormArray([]));
    this.showCancel = false;
    this.showForward = false;
    this.showSave = false;
    //this.buttonText = "Update";
    this.leaveSanctionDetailsForm.get('empCd').setValue(this.employeeCode);
    this.leaveSanctionDetailsForm.get('permDDOId').setValue(this.PermDdoId);
    this.leaveSanctionDetailsForm.get('leaveMainId').setValue(element.leavesSanctionMainModel.leaveMainId);
    this.leaveSanctionDetailsForm.get('orderDT').setValue(element.leavesSanctionMainModel.orderDT);
    this.leaveSanctionDetailsForm.get('orderNo').setValue(element.leavesSanctionMainModel.orderNo);
    this.leaveSanctionDetailsForm.get('leaveCategory').setValue(element.leavesSanctionMainModel.leaveCategory);
    this.leaveSanctionDetailsForm.get('leaveReasonCD').setValue(element.leavesSanctionMainModel.leaveReasonCD);
    this.leaveCurtailForm.get('curtOrderNo').setValue(element.curtOrderNo);
    this.leaveCurtailForm.get('curtOrderDT').setValue(element.curtOrderDT);
    this.leaveCurtailForm.get('curtFromDT').setValue(element.curtFromDT);
    this.leaveCurtailForm.get('curtToDT').setValue(element.curtToDT);
    if (element.isCurtleave == null || element.isCurtleave == 'Curtail') {
      this.leaveCurtailForm.get('isCurtleave').setValue("Curtail");
      this.mxcutaildate = element.curtToDT;
      this.mncutaildate = element.curtFromDT;
    }
    else {
      this.leaveCurtailForm.get('isCurtleave').setValue(element.isCurtleave);
      this.mxcutaildate = this.currentdate;
      this.mncutaildate = element.curtToDT;
    }
    this.leaveCurtailForm.get('curtReason').setValue(element.curtReason);
    this.leaveCurtailForm.get('curtRemarks').setValue(element.curtRemarks);
    this.leaveCurtailForm.get('curtLeaveCd').setValue(element.curtLeaveCd);
    this.leaveCurtailForm.get('curtNoDays').setValue(element.curtNoDays);
    this.leaveCurtailForm.get('curtLeaveDetailsId').setValue(element.curtLeaveDetailsId);
    if (element.returnReason != null) {
      this.showReturnReason = true;
      this.leaveCurtailForm.get('returnReason').setValue(element.returnReason);
    }
    else { this.showReturnReason = false; }
    
    this.leaveCurtailForm.get('curtId').setValue(element.curtId);
    for (var i = 0; i < element.leavesSanctionMainModel.leaveSanctionDetails.length; i++) {
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).push(this.createItem());
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('leaveCd').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].leaveCd);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('fromDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].fromDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('toDT').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].toDT);
      (<FormArray>this.leaveSanctionDetailsForm.get('leaveSanctionDetails')).at(i).get('noDays').setValue(element.leavesSanctionMainModel.leaveSanctionDetails[i].noDays);   
    }
    this.leaveSanctionDetailsForm.disable();
    if (flg == "dis") {
      this.leaveCurtailForm.disable();
    }
    else {
      this.leaveCurtailForm.enable();
      this.leaveCurtailForm.get('curtLeaveCd').disable();
      this.leaveCurtailForm.get('curtFromDT').disable();
      //this.leaveCurtailForm.get('curtNoDays').setValue('50');
      this.showForward = false;
      this.showCancel = false;
      this.showSave = false;
    }

  }
  
  DeleteCurtailLeaves(leaveMainId: string, flag: boolean) {
    debugger;
    this.leaveMainid = flag ? leaveMainId : "0";
    this.employeeCode=this.leaveSanctionDetailsForm.get('empCd').value;
    if (!flag) {
      this._Service.DeleteLeavesSanction(leaveMainId, 'Curt').subscribe((result: any) => {
        this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
        this.child.BindDropDownOrder(this.employeeCode);
      });
      this.leaveSanctionDetailsForm.disable();
    }
    this.deletepopup = false;
  }
  checkValidation() {

    //if (this.leaveSanctionDetailsForm.get("leaveCategory").value == "Combined") {
    //  if (this.objlsmain.leaveSanctionDetails.length == 1) {
    //    alert('Please select single leave Type');
    //    return false;
    //  }
    //}

  }
  fillnoofDays(value) {
    debugger;
    this.firstdate = this.datePipe.transform(this.leaveCurtailForm.get('curtFromDT').value, 'yyyy-MM-dd');
    this.lastdate = this.datePipe.transform(this.leaveCurtailForm.get('curtToDT').value, 'yyyy-MM-dd');

    if ((this.firstdate != null) && (this.lastdate != null)) {
      debugger;
      if (this.leaveCurtailForm.get('curtFromDT').value > this.leaveCurtailForm.get('curtToDT').value) {
        alert('From date should be less than to date');     
        this.leaveCurtailForm.get('curtToDT').setValue("");
      }
      else {       
        this.datedif = this.leaveCurtailForm.get('curtToDT').value.diff(this.leaveCurtailForm.get('curtFromDT').value, 'days');
        this.leaveCurtailForm.get('curtNoDays').setValue(this.datedif + 1);        
      }
    }
  }
  validateOrdNo(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode > 44 && charCode < 48)) {
      return true;
    }
    return false;
  }
  rowHighlight(row) {
    debugger;
    //this.highlightedRows.pop();
    //this.highlightedRows.push(row)
  }
  abc() {
    alert(this.findInvalidControls());
  }
  //Extra methods
  public findInvalidControls() {
    debugger;
    const invalid = [];
    const controls = this.leaveCurtailForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }




}

