
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatSnackBar, MatCheckbox } from '@angular/material';
//import { LeavesMgmtService } from '../../services/leaves-mgmt/leaves-mgmt.service';
import { LeavesSanctionMainModel, LeavesSanctionDetailsModel, ReasonForLeaveModel, LeavesTypeModel } from '../../model/leavesmodel/leavessanctionmodel';

export interface PeriodicElement {
  SanctionOrderNo: string;
  SanctionOrderDate: number;
  LeaveReason: number;
  LeaveCategory: string;
}
var ELEMENT_DATA: PeriodicElement[];
@Component({
  selector: 'app-common-leaves-sanction',
  templateUrl: './common-leaves-sanction.component.html',
  styleUrls: ['./common-leaves-sanction.component.css']
})
export class CommonLeavesSanctionComponent implements OnInit {

  displayedColumns: string[] = ['SanctionOrderNo', 'SanctionOrderDate', 'LeaveReason', 'LeaveCategory', 'action'];
  highlightedRows = [];
  @Input() dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @Input() ShowinfoButton: boolean;
  @Input() ShoweditButton: boolean;
  @Input() ShowdeleteButton: boolean;
  @Output() EditLeavesSanction = new EventEmitter<object>();
  @Output() DissableLeavesSanction = new EventEmitter<object>();
  @Output() DeleteLeavesSanction = new EventEmitter<object>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() { }

  ngOnInit() {
    //this.dataSource.sort = this.sort;
    //this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit() {

    //this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    debugger;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  EditLeaves(leaveMainId,orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails) {
    this.EditLeavesSanction.emit({leaveMainId,orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails });
   
  }
  DissableLeaves(leaveMainId, orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails) {
    this.DissableLeavesSanction.emit({ leaveMainId, orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails});
    //this.EditLeavesSanction(orderNo, orderDT, leaveCategory, leaveReasonCD, remarks, leaveSanctionDetails);
    //this.leaveSanctionCreationForm.disable();
    //this.showCancel = true;
    //this.showForward = true;
    //this.showSave = true;
  }
  DeleteLeaves(value) {
    this.DeleteLeavesSanction.emit(value);
    
    //this._Service.DeleteLeavesSanction(value).subscribe(result => {
    //  this.snackBar.open('Deleted Successfully', null, { duration: 5000 });
    //  this.ResetLeavesSanction();
    //  this.GetLeavesSanction(this.objlsmain.empCd);
    //})
  }
  rowHighlight(row) {
    this.highlightedRows.pop();
    this.highlightedRows.push(row)
  }

}
