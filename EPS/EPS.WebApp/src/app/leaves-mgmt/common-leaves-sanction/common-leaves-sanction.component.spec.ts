import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonLeavesSanctionComponent } from './common-leaves-sanction.component';

describe('CommonLeavesSanctionComponent', () => {
  let component: CommonLeavesSanctionComponent;
  let fixture: ComponentFixture<CommonLeavesSanctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonLeavesSanctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonLeavesSanctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
