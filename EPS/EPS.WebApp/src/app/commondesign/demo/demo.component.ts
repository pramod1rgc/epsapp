import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DemoServiceService } from './demo-service.service';


import { HttpErrorResponse } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { DemoModel } from '../demo/demo-model';
import { PayrollModule } from '../../payroll/payroll.module';
import { common } from '../../payroll/common';
import { Alert } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';

declare let ClientIP: any;

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css'],
  providers: [DemoServiceService]
})
export class DemoComponent implements OnInit {

  demoModel: DemoModel;
  selectedRadio: any;
  SSValidationError: any
  abc: string;

  privateIP:any;
  publicIP:any;
  jokes: Object[];
  constructor(private router: Router, private _Service: DemoServiceService, private _common: common,
    private translate: TranslateService, private http: HttpClient
  ) {
  //  this.translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');
    console.log(translate);
    this.privateIP = ClientIP;
    this.http.get('https://api.ipify.org?format=json').subscribe(data => {
      this.publicIP = data['ip'];
      alert(this.publicIP);
    });
  }

  demoDetails: any = [];

  switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    this.demoModel = new DemoModel();
    this.selectedRadio = 'N';

  }
  redirectto() {
    this.SSValidationError = null;
    this._Service.Demo(this.demoModel).subscribe(data => {
      this.demoDetails = data;
    },
      errq => {
        if (errq instanceof HttpErrorResponse) {
          if (errq.status == 400) {
            this.SSValidationError = JSON.parse(errq.error);
            alert(errq.error.replace(/[.*+?^${}"()]/g, '').replace(/[,]/g, '\n'));
          }
        }
      });
  }
  getLocalIp() {
    /**
   * To read private IP
   */
    //this.privateIP = ClientIP;
    //alert(this.privateIP);
    /**
     * To read public IP
     */
    this.http.get('https://api.ipify.org?format=json').subscribe(data => {
      this.publicIP = data['ip'];
      alert( this.publicIP);
    });
  }
  }

