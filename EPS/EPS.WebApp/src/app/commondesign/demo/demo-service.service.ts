import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { AppConfig, APP_CONFIG } from '../global/global.module';
import { Observable } from 'rxjs';
import {AppConfig,APP_CONFIG } from '../../global/global.module';
@Injectable({
  providedIn: 'root'
})
export class DemoServiceService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  Demo(demoModel) {

    return this.httpclient.post(this.config.api_base_url + 'Demo/Demo', demoModel, { responseType: 'text' });
    //return this.httpclient.get(this.BaseUrl + 'LoginRole/LoginCheck?username=' + username + '&password=' + password , { responseType: 'text' });
    // return this.httpclient.get(this.config.login + userId + '&password=' + password + '&logintype=' + logintype, { responseType: 'text' });
  }
}
