import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonddlComponent } from './commonddl.component';

describe('CommonddlComponent', () => {
  let component: CommonddlComponent;
  let fixture: ComponentFixture<CommonddlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonddlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonddlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
