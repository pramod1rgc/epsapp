import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExtensionofserviceComponent } from '../change/extensionofservice/extensionofservice.component';
import { DateofbirthComponent } from './dateofbirth/dateofbirth.component';
import { NamegenderComponent } from './namegender/namegender.component';
import { ChangeModule } from './change.module';
import { PfNpsComponent } from './pf-nps/pf-nps.component';
import { PanNoComponent } from './pan-no/pan-no.component';
import { HraCityClassComponent } from './hra-city-class/hra-city-class.component';
import { ExservicemanComponent } from './exserviceman/exserviceman.component';
import { DesignationComponent } from './designation/designation.component';
import { CgegisDetailsComponent } from './cgegis-details/cgegis-details.component';
import { CghsDetailsComponent } from './cghs-details/cghs-details.component';
import { JoininModeComponent } from './joinin-mode/joinin-mode.component';
import { CastecategoryDetailsComponent } from './castecategory-details/castecategory-details.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { EntitlementOfficeComponent } from './entitlement-office/entitlement-office.component';
import { DojControllerComponent } from './doj-controller/doj-controller.component';
import { TptaCityClassComponent } from './tpta-city-class/tpta-city-class.component';
import { DoeGovServiceComponent } from './doe-gov-service/doe-gov-service.component';
import { DorGovServiceComponent } from './dor-gov-service/dor-gov-service.component';
import { AddTaPDComponent } from './add-ta-pd/add-ta-pd.component';
import { StateGisComponent } from './state-gis/state-gis.component';

const routes: Routes = [
  {
    path: '', component: ChangeModule, children: [
      { path: 'eos', component: ExtensionofserviceComponent },

      { path: 'namegender', component: NamegenderComponent },
      { path: 'dob', component: DateofbirthComponent },
      { path: 'pfnps', component: PfNpsComponent },
      { path: 'panno', component: PanNoComponent },
      { path: 'hracc', component: HraCityClassComponent },      
      { path: 'designation', component: DesignationComponent },
      { path: 'exserviceman', component: ExservicemanComponent },

      { path: 'cgegis', component: CgegisDetailsComponent },
      
      {
        path: 'joiningmode', component: JoininModeComponent
      },
      { path: 'cghs', component: CghsDetailsComponent },
      { path: 'castecategory', component: CastecategoryDetailsComponent },
      { path: 'bankdetails', component: BankDetailsComponent },
      { path: 'contactdetails', component: ContactDetailsComponent },
      { path: 'entitledofficevehicle', component: EntitlementOfficeComponent },
      { path: 'doj', component: DojControllerComponent },
      { path: 'tptaclass', component: TptaCityClassComponent },
      { path: 'doe', component: DoeGovServiceComponent },
      { path: 'dor', component: DorGovServiceComponent },
      { path: 'adddoubleta', component: AddTaPDComponent },
      { path: 'stategis', component: StateGisComponent }
      
    ]
  }
];




@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangeRoutingModule { }
