import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { ExtensionofService } from '../../services/Change/extensionofservice.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-extensionofservice',
  templateUrl: './extensionofservice.component.html',
  styleUrls: ['./extensionofservice.component.css']

})

export class ExtensionofserviceComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  searchTerm: string = '';
  JDreadonly: boolean = true;

  extensionOfServiceForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['S.No', 'EmpCode', 'EmployeeName', 'OrderNo', 'OrderDate', 'FromDate', 'ToDate', 'Status', 'Action']

  list: any = [];
  allData: any[] = [];

  flag: any;
  maxDate = new Date();
  EDITpara: string = '';
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  public joiningDate: any;

  constructor(private _service: ExtensionofService, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');

  }
  ngOnInit() {

    this.Btntxt = 'Save';
    this.RejectPopupcreateForm();
    this.createForm();
    this.Bindddltype();

    this.infoScreen = true;
    this.JDreadonly = true;


  }

  Bindddltype() {

    this._service.GetDdlvalue('extension').subscribe(res => {
      this.list = res;
    })
  }

  GetcommonMethod(value) {

    this.formGroupDirective.resetForm();
    this.empCd = value;

    if (this.empCd) {
      this.extensionOfServiceForm.enable();
      //bind form
      this.getExtensionOfServiceDetails(this.empCd, this.roleId);

    }
    else {
      this.extensionOfServiceForm.disable();
    }


    this.editable = false;
    this.Btntxt = 'Save';
    this.fwdtochker = false;
    this.infoScreen = false;

    //this.extensionOfServiceForm.patchValue({
    //  JoiningDate: this.joiningDate
    //});

    this.dataSource.filteredData.length = 0; //clr search text in different select emp
    if (this.roleId == '5') {
      this.extensionOfServiceForm.disable();
    }

  }

  changeEvent(event) {
    this.extensionOfServiceForm.controls.ToDate.setValue(''); //clr todte value when selecting fromdt
    //this.extensionOfServiceForm.controls.JoiningDate.setValue(event.value);
  }

  createForm() {
    this.extensionOfServiceForm = new FormGroup({

      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      //  OrderNo: new FormControl('', [Validators.required, Validators.minLength(3), this.maxLength (100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),
      OrderDate: new FormControl('', Validators.required),
      FromDate: new FormControl('', Validators.required),
      ToDate: new FormControl('', Validators.required),
      JoiningDate: new FormControl('', { updateOn: 'blur', validators: [Validators.required] }),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),
      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      ddoId: new FormControl(this.ddoId),
      oldOrderNo: new FormControl('')
    });

    this.extensionOfServiceForm.disable();

  }


  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }


  onSubmit(flag?: any) {

    this.extensionOfServiceForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });

    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }
    if (this.extensionOfServiceForm.valid) {

      this._service.InsertEOSDetails(this.extensionOfServiceForm.value, flag).subscribe(result => {

        //this.pageNumber = 1;
        this.getExtensionOfServiceDetails(this.empCd, this.roleId);

        this.Btntxt = 'Save';
        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {


            swal(this._msg.updateMsg);
            //bind form
            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
          }
          else {

            swal(this._msg.updateFailedMsg);

          }
        }
        else {
          if (result == '1') {

            swal(this._msg.saveMsg);
            //this.extensionOfServiceForm.reset();
            //bind form
            this.formGroupDirective.resetForm();
          }

          else {

            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();

          }
        }
        this.fwdtochker = false;
        this.extensionOfServiceForm.disable();

      });

    }

  }
  //  export interface ELEMENT_DATA1 {
  //  empCd: string;
  //  empName: string;
  //  orderNo: string;
  //  orderDate: Date;
  //  fromDate: Date;
  //  toDate: Date;
  //}
  pipe: DatePipe;
  //datatable data
  getExtensionOfServiceDetails(empCd: any, roleId: any) {

    this.pageNumber = 1;
    this._service.GetExtensionOfServiceDetails(empCd, roleId).subscribe(data => {

      this.dataSource = new MatTableDataSource(data);
      //filter in Data source
      this.pipe = new DatePipe('en');
      const defaultPredicate = this.dataSource.filterPredicate;
      this.dataSource.filterPredicate = (data, filter) => {
        //const orderDate = this.pipe.transform(data.orderDate, 'dd/MM/yyyy');
        //const fromDate = this.pipe.transform(data.fromDate, 'dd/MM/yyyy');
        //// return orderDate.indexOf(filter) >= 0 || fromDate.indexOf(filter) >= 0 || defaultPredicate(data, filter);
        //if ((fromDate != null && fromDate.indexOf(filter) > -1) || (fromDate != null && fromDate.indexOf(filter) > -1)) {
        //  return orderDate.indexOf(filter) >= 0 || fromDate.indexOf(filter) >= 0 || data.empName.indexOf(filter) >= 0 || defaultPredicate(data, filter);
        //}
        //else {
        if (data.orderNo != null && data.orderNo.indexOf(filter) > -1) {
          return data.orderNo.toLowerCase().includes(filter);
        }
        else {
          return data.empCd.toLowerCase().includes(filter) || data.empName.toLowerCase().includes(filter);
        }

        //}
      }
      //END Of filter in Data source
      this.pageSize = this.dataSource.data.length;
      this.dataSource.paginator = this.paginator;
                  
      var self = this;

      let list: string[] = [];
      let list1: string[] = [];
      //enable or disbale form using current status of orderNo user
      data.forEach(element => {
        list.push(element.joiningDate);

      });
      this.extensionOfServiceForm.controls.JoiningDate.setValue(list[0]);
      //get joiningDtflag
      this.joiningDate = list[0];
      //get status
      data.forEach(element => {
        list1.push(element.status);

      });
      if (list1[0] == 'E' || list1[0] == 'U' || list1[0] == 'P' || list1[0] == 'R') {

        self.extensionOfServiceForm.disable();

        this.infoScreen = true;
      }
      else {
        self.extensionOfServiceForm.enable();
      }
      if (this.roleId == '5') {
        self.extensionOfServiceForm.disable();
        this.infoScreen = false;
      }


    });
  }

  //EDIT


  btnEditClick(obj) {


    this.extensionOfServiceForm.setValue({

      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      FromDate: obj.fromDate,
      ToDate: obj.toDate,
      JoiningDate: obj.joiningDate,
      Remarks: obj.remarks,
      empCd: this.empCd,
      rejectionRemark: obj.rejectionRemark,
      ddoId: this.ddoId,
      username: this.username,
      oldOrderNo: obj.orderNo
    });

    this.oldOrderNo = obj.orderNo;

    this.extensionOfServiceForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';

    let empStatus = obj.status;

    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }


    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }



  }

  //getEmpTransDetails(pageSize, pageNumber) {
  //  // this.getExtensionOfServiceDetails();

  //  let employeeId = this.empCd;
  //  this.totalCount = 0;
  //  this.dataSource = new MatTableDataSource<any>();
  //  this._service.FetchEOSDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId).subscribe(result => {
  //    
  //    if (result && result != undefined && result.length > 0) {
  //      this.dataSource = new MatTableDataSource(result);
  //      this.dataSource.paginator = this.paginator;
  //      this.dataSource.sort = this.sort;
  //      //this.allData = result;
  //      this.totalCount = result.length;
  //    }
  //  });
  //}

  applyFiltercurrent(filterValue: string) {


    this.paginator.pageIndex = 0;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      // this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {

      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }

  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;
  }


  confirmDelete() {

    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'extension').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {

        swal(this._msg.deleteMsg);
        this.formGroupDirective.resetForm();
        this.extensionOfServiceForm.enable();
        this.infoScreen = false;
      }
      else {

        swal(this._msg.deleteFailedMsg);
      }
      this.getExtensionOfServiceDetails(this.empCd, this.roleId);
      this.DeletePopup = false;
    });
  }
  Cancel() {

    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');
    this.rejectionRemark = null;
  }
  cancelForm() {

    this.formGroupDirective.resetForm();
    this.extensionOfServiceForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {

    this.oldOrderNo = '';
    this.extensionOfServiceForm.setValue({

      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      FromDate: obj.fromDate,
      ToDate: obj.toDate,
      JoiningDate: obj.joiningDate,
      Remarks: obj.remarks,
      empCd: this.empCd,
      rejectionRemark: obj.rejectionRemark,
      ddoId: this.ddoId,
      username: this.username,
      oldOrderNo: this.oldOrderNo

    });


    let empStatus = obj.status;
    this.extensionOfServiceForm.disable();

    if (this.roleId == '5' && empStatus == 'N') {
      this.extensionOfServiceForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }
  }


  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.extensionOfServiceForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.extensionOfServiceForm.get('OrderNo').value;

    //
    this._service.ForwardToCheckerUserDtls(employeeId, orderNo, null, null).subscribe(result => {

      if (result == 1) {

        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.fwdtochker = false;

      }
      else {

        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }
      this.getExtensionOfServiceDetails(this.empCd, this.roleId);

    });
    this.Btntxt = 'Save';
  }


  updateStatus(status: string) {




    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.extensionOfServiceForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {


            swal(this._msg.DDORejectedByChecker);

            this.getExtensionOfServiceDetails(this.empCd, this.roleId);

            this.formGroupDirective.reset();
            this.extensionOfServiceForm.disable();
            this.infoScreen = false;
          }
          else {

            swal(this._msg.updateFailedMsg);

          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {

          swal(this._msg.DDOVerifiedByChecker);

          this.getExtensionOfServiceDetails(this.empCd, this.roleId);

          this.formGroupDirective.reset();
          this.extensionOfServiceForm.disable();

          this.infoScreen = false;
        }
        else {

          swal(this._msg.updateFailedMsg);

        }
        this.ApprovePopup = false;
        this.RejectPopup = false;


      });
    }
  }
}

