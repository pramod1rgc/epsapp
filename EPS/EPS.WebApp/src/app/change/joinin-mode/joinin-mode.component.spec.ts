import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoininModeComponent } from './joinin-mode.component';

describe('JoininModeComponent', () => {
  let component: JoininModeComponent;
  let fixture: ComponentFixture<JoininModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoininModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoininModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
