import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateGisComponent } from './state-gis.component';

describe('StateGisComponent', () => {
  let component: StateGisComponent;
  let fixture: ComponentFixture<StateGisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateGisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateGisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
