import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CghsDetailsComponent } from './cghs-details.component';

describe('CghsDetailsComponent', () => {
  let component: CghsDetailsComponent;
  let fixture: ComponentFixture<CghsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CghsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CghsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
