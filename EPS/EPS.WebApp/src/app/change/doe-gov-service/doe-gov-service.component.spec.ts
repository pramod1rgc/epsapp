import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoeGovServiceComponent } from './doe-gov-service.component';

describe('DoeGovServiceComponent', () => {
  let component: DoeGovServiceComponent;
  let fixture: ComponentFixture<DoeGovServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoeGovServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoeGovServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
