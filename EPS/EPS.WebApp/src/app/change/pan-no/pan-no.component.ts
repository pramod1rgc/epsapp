import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-pan-no',
  templateUrl: './pan-no.component.html',
  styleUrls: ['./pan-no.component.css']
  
})
export class PanNoComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  PanNoForm: FormGroup;
  RejectPopupForm: FormGroup;
  displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentPANNo', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedPANNo', 'Status']

  list: any = [];
  EDITpara: string = '';
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;

  constructor(private _service: ChangeSr, private _msg: CommonMsg){
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');
   
  }


  ngOnInit() {
   

    this.Btntxt = 'Save';
    this.createForm();
    this.RejectPopupcreateForm();   
    this.infoScreen = true;
  }

  GetcommonMethod(value) {
   
    this.formGroupDirective.resetForm();
    this.empCd = value;
    

    if (this.empCd) {
      this.PanNoForm.enable();
      //bind form
      this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
      this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');

    }
    else {
      this.PanNoForm.disable();
    }

  
    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.PanNoForm.disable();
    }
  }
  

  createForm() {
    this.PanNoForm = new FormGroup({

      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),

      OrderDate: new FormControl('', Validators.required),
      PANNo: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("^[a-zA-Z0-9]*$")]),
      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')
     
    });
    this.PanNoForm.disable();

  }

  RejectPopupcreateForm() {
   
    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }
  

  onSubmit(flag?: any) {
   
    this.PanNoForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });
   
    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }

    if (this.PanNoForm.valid) {
     
      this._service.InsertPanNoFormDetails(this.PanNoForm.value, flag).subscribe(result => {
       
        //this.pageNumber = 1;
        this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
        this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');
        
        this.Btntxt = 'Save';

        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {

            
            swal(this._msg.updateMsg);
            //bind form
            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
          }
          else {
            
            swal(this._msg.updateFailedMsg);

          }
        }
        else {
          if (result == '1') {
            
            swal(this._msg.saveMsg);
            this.formGroupDirective.resetForm();
          }

          else {
            
            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();

          }
        }

        this.fwdtochker = false;
        this.PanNoForm.disable();
      });

    }
  }

  getPanNoFormDetails1(empCd: any, roleId: any, flag: any) {
   
    this.pageNumber = 1;
    this._service.GetPanNoFormDetails(empCd, roleId, flag).subscribe(data => {
      var self = this;
     
      if (flag == 'current') {
       
        this.dataSource = new MatTableDataSource(data);
        //filter in Data source
        this.dataSource.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.currentPANNo.toLowerCase().includes(filter);
        }
        //END Of filter in Data source

        this.pageSize = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        let list: string[] = [];
        //enable or disbale form using current status of orderNo user
        data.forEach(element => {
          list.push(element.status);

        });
        if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
         
          self.PanNoForm.disable();

          this.infoScreen = true;
        }
        else {
          self.PanNoForm.enable();
        }
        if (this.roleId == '5') {
          self.PanNoForm.disable();
          this.infoScreen = false;
        }
      }
      else if (flag == 'history') {
       
        this.dataSource1 = new MatTableDataSource(data);
        //filter in Data source
        this.dataSource1.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.revisedPANNo.toLowerCase().includes(filter);
        }
        //END Of filter in Data source
        this.pageSize = this.dataSource1.data.length;
        this.dataSource1.paginator = this.historyPagination;
      }

    });
  }

  btnEditClick(obj) {
   
   

    this.PanNoForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,
      PANNo: obj.panNo,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;
    
   
    this.PanNoForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';

    let empStatus = obj.status;
   
    //if (empStatus == 'P' || empStatus == 'V' || empStatus == 'R') {
    //  this.editable = false;
    //}
    //else
    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }


    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }

  }

  //getEmpTransDetails(pageSize, pageNumber, flag1: any) {
  //  // this.getExtensionOfServiceDetails();

  //  let employeeId = this.empCd;
  //  this.totalCount = 0;
  //  this.totalCounthis = 0;
  //  if (flag1 == 'current') {
  //    this.dataSource = new MatTableDataSource<any>();
  //    this._service.FetchPanNosrchDetails(employeeId, pageNumber, pageSize, this.searchTerm, this.roleId, flag1).subscribe(result => {
  //     
  //      if (result && result != undefined && result.length > 0) {
  //        this.dataSource = new MatTableDataSource(result);
  //        this.dataSource.paginator = this.paginator;

  //        this.totalCount = result.length;
  //      }
  //    });

  //  }
  //  else if (flag1 == 'history') {

  //    this.dataSource1 = new MatTableDataSource<any>();
  //    this._service.FetchPanNosrchDetails(employeeId, pageNumber, pageSize, this.searchTermhis, this.roleId, flag1).subscribe(result => {
  //     
  //      if (result && result != undefined && result.length > 0) {
  //        this.dataSource1 = new MatTableDataSource(result);
  //        this.dataSource1.paginator = this.historyPagination;

  //        this.totalCounthis = result.length;
  //      }
  //    });



  //  }


  //}

  applyFiltercurrent(filterValue) {
   

    this.paginator.pageIndex = 0;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  applyFilterhistory(filterValue) {
   
    this.historyPagination.pageIndex = 0;

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches


    this.dataSource1.filter = filterValue;

    if (this.dataSource1.filteredData.length > 0) {
      this.dataSource1.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource1.filteredData.length = 0;
    }
  }


  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {
     
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }
  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;
  }


  confirmDelete() {
   
    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'panno').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {
        
        swal(this._msg.deleteMsg);


        this.formGroupDirective.resetForm();
        this.PanNoForm.enable();
        this.infoScreen = false;
      }
      else {
        
        swal(this._msg.deleteFailedMsg);
      }
      //this.pageNumber = 1;
      this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
      this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');

      ////this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
      ////this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');

      this.DeletePopup = false;

    });
  }
  Cancel() {
   
    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = null;
  }
  cancelForm() {
   
    this.formGroupDirective.resetForm();
    this.PanNoForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {
   
    this.oldOrderNo = '';
    this.PanNoForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      Remarks: obj.remarks,
      PANNo: obj.panNo,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo

    });
   

    let empStatus = obj.status;
    this.PanNoForm.disable();
   
    if (this.roleId == '5' && empStatus == 'N') {
      this.PanNoForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.btnCancel = true;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }
  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.PanNoForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.PanNoForm.get('OrderNo').value;

    this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, null, null).subscribe(result => {
     
      if (result == 1) {
        
        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.fwdtochker = false;

      }
      else {
        
        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }

      this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
      this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');

      //this.pageNumber = 1;
      //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'history');
      //this.getEmpTransDetails(this.pageSize, this.pageNumber, 'current');

    });

    this.Btntxt = 'Save';

  }


  updateStatus(status: string) {
   

   

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.PanNoForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {
            
            swal(this._msg.DDORejectedByChecker);
         
            this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
            this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');

            this.formGroupDirective.reset();
            this.PanNoForm.disable();
            this.infoScreen = false;
            //this.createForm();

          }
          else {
            
            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerPanNoUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {

          
          swal(this._msg.DDOVerifiedByChecker);
          this.getPanNoFormDetails1(this.empCd, this.roleId, 'history');
          this.getPanNoFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.PanNoForm.disable();
          this.infoScreen = false;
          //this.createForm();

        }
        else {
          
          swal(this._msg.updateFailedMsg);

        }
        this.ApprovePopup = false;
        this.RejectPopup = false;


      });
    }
  }

}

