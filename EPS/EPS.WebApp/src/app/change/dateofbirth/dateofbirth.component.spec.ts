import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateofbirthComponent } from './dateofbirth.component';

describe('DateofbirthComponent', () => {
  let component: DateofbirthComponent;
  let fixture: ComponentFixture<DateofbirthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateofbirthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateofbirthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
