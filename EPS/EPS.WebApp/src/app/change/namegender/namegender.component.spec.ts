import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamegenderComponent } from './namegender.component';

describe('NamegenderComponent', () => {
  let component: NamegenderComponent;
  let fixture: ComponentFixture<NamegenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamegenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamegenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
