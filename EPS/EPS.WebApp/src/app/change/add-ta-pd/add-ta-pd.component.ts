import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';

import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-add-ta-pd',
  templateUrl: './add-ta-pd.component.html',
  styleUrls: ['./add-ta-pd.component.css']
})
export class AddTaPDComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  addtapdForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentphsidsble', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedphsidsble', 'Status']

  list: any = [];
  EDITpara: string = '';
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  PhsidsbleOpt: boolean = false;
  
  isSubmitted = false;


  constructor(private _service: ChangeSr, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');
  }

  ngOnInit() {
    this.Btntxt = 'Save';
    this.createForm();
    this.Bindddltype();
    this.RejectPopupcreateForm();
    this.infoScreen = true;
    
  }

  GetcommonMethod(value) {
    debugger;
    this.formGroupDirective.resetForm();
    this.empCd = value;
    this.isSubmitted = false;
    if (this.empCd) {
      this.addtapdForm.enable();
      //bind form
      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');
    }
    else {
      this.addtapdForm.disable();
    }

    this.onChanges();//reset field by Y or N
    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.addtapdForm.disable();
    }


  }

  createForm() {
    this.addtapdForm = new FormGroup({
      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),

      OrderDate: new FormControl('', Validators.required),
      phsidsble: new FormControl('', [Validators.required]),

      phType: new FormControl(''),
      phPcnt: new FormControl(''), phCertNo: new FormControl(''),
      phisSevere: new FormControl(''), phCertDt: new FormControl(''),
      phCertAuth: new FormControl(''), phentDoubleTa: new FormControl(''),

      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')
    });
    this.addtapdForm.disable();

  }


  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }
  onChanges(): void {
    this.addtapdForm.get('phsidsble')
      .valueChanges
      .subscribe(value => {      
        const phsidsble = this.addtapdForm.get('phsidsble');
        const phType = this.addtapdForm.get('phType');
        const phPcnt =this.addtapdForm.get('phPcnt');
        const phCertNo =this.addtapdForm.get('phCertNo');
        const phisSevere = this.addtapdForm.get('phisSevere');
        const phCertDt = this.addtapdForm.get('phCertDt');
        const phCertAuth =this.addtapdForm.get('phCertAuth');
        const phentDoubleTa =this.addtapdForm.get('phentDoubleTa');
        if (value === 'Y') {
          debugger;
          this.PhsidsbleOpt = true;
          phType.enable();
          phType.setValidators([Validators.required]);
          phPcnt.enable();
          phPcnt.setValidators([Validators.required]);
          phCertNo.enable();
          phCertNo.setValidators([Validators.required]);
          phisSevere.enable();
          phisSevere.setValidators([Validators.required]);
          phCertDt.enable();
          phCertDt.setValidators([Validators.required]);
          phCertAuth.enable();
          phCertAuth.setValidators([Validators.required]);
          phentDoubleTa.enable();
          phentDoubleTa.setValidators([Validators.required]);
        }
        else {
          this.PhsidsbleOpt = false;
          phType.value == "";
          phType.disable()
          phType.clearValidators();

          phPcnt.value == "";
          phPcnt.disable()
          phPcnt.clearValidators();

          phCertNo.value == "";
          phCertNo.disable()
          phCertNo.clearValidators();

          phisSevere.value == "";
          phisSevere.disable()
          phisSevere.clearValidators();

          phCertDt.value == "";
          phCertDt.disable()
          phCertDt.clearValidators();

          phCertAuth.value == "";
          phCertAuth.disable()
          phCertAuth.clearValidators();

          phentDoubleTa.value == "";
          phentDoubleTa.disable()
          phentDoubleTa.clearValidators();
        
        }


      });
  }


  Bindddltype() {
    this._service.GetDdlvalue('doubleta_ph').subscribe(res => {
      this.list = res;
    })
  }
  onSubmit(flag?: any) {
    this.isSubmitted = true;
    //debugger;
    this.addtapdForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });

    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }
    if (!this.addtapdForm.valid) {
      return false;
    }
    //console.log(this.addtapdForm.value);
    if (this.addtapdForm.valid) { //change in proc of save & remove snackbar 
      debugger;
      this._service.InsertaddtapdFormDetails(this.addtapdForm.value, flag).subscribe(result => {
        debugger;

        this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
        this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');

        this.Btntxt = 'Save';
        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {
            swal(this._msg.updateMsg);
            //bind form

            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
          }
          else {
            swal(this._msg.updateFailedMsg);
          }
        }
        else {
          if (result == '1') {

            swal(this._msg.saveMsg);
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
          }

          else {

            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();

          }
        }
       
        this.fwdtochker = false;
        this.addtapdForm.disable();
      });

    }
  }


  getaddtapdFormDetails1(empCd: any, roleId: any, flag: any) { //to give role_id parameter & paste pageinate code to 2nd mat-table
    //debugger;
    //this.Bindddltype(); //set ddl
    this.pageNumber = 1;

    this._service.GetaddtapdFormDetails(empCd, roleId, flag).subscribe(data => {
      //debugger;
      var self = this;
      if (flag == 'current') {
       
        this.dataSource = new MatTableDataSource(data);

        //filter in Data source    

        this.dataSource.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.currentphsidsble.toLowerCase().includes(filter);
        }
        //END Of filter in Data source
      
        this.pageSize = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        //enable or disbale form using current status of orderNo user
        let list: string[] = [];

        data.forEach(element => {
          list.push(element.status);

        });

        if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {
          debugger;
          self.addtapdForm.disable();

          this.infoScreen = true;
        }
        else {
          self.addtapdForm.enable();
        }
        if (this.roleId == '5') {
          self.addtapdForm.disable();
          this.infoScreen = false;
        }


      }
      else if (flag == 'history') {
        //debugger;
        this.dataSource1 = new MatTableDataSource(data);
        //filter in Data source    

        this.dataSource1.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.revisedphsidsble.toLowerCase().includes(filter);
        }
        //END Of filter in Data source

        this.pageSize = this.dataSource1.data.length;
        this.dataSource1.paginator = this.historyPagination;
      }

    });
  }

  //EDIT
  compare(o1: any, o2: any) {

    if (o1 == o2)
      return true;
    else
      return false
  }



  btnEditClick(obj) {
    debugger;
    this.addtapdForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,

      phsidsble: obj.phsidsble,

      phType: obj.phType,
      phPcnt: obj.phPcnt, phCertNo: obj.phCertNo,
      phisSevere: obj.phisSevere, phCertDt: obj.phCertDt,
      phCertAuth: obj.phCertAuth, phentDoubleTa: obj.phentDoubleTa,

      Remarks: obj.remarks,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;
    console.log(this.oldOrderNo);
    //debugger;
    this.addtapdForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';

    let empStatus = obj.status;

    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }


    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }

  }



  applyFiltercurrent(filterValue) {
    //debugger;

    this.paginator.pageIndex = 0;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  applyFilterhistory(filterValue) {
    //debugger;
    this.historyPagination.pageIndex = 0;

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource1.filter = filterValue;

    if (this.dataSource1.filteredData.length > 0) {
      this.dataSource1.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource1.filteredData.length = 0;
    }
  }


  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {
      debugger;
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }

  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;
  }


  confirmDelete() {
    //debugger;
    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'doubleta_ph').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {
        swal(this._msg.deleteMsg);

        //this.editable = true;
        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.addtapdForm.enable();
        this.infoScreen = false;


      }
      else {
        swal(this._msg.deleteFailedMsg);
      }

      //this.pageNumber = 1;
      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');
      this.DeletePopup = false;

    });
  }
  Cancel() {
    //debugger;
    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = null;
  }
  cancelForm() {
    //debugger;
    this.formGroupDirective.resetForm();
    this.isSubmitted = false;
    this.addtapdForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {
    //debugger;
    this.oldOrderNo = '';
    this.addtapdForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      phsidsble: obj.phsidsble,

      phType: obj.phType,
      phPcnt: obj.phPcnt, phCertNo: obj.phCertNo,
      phisSevere: obj.phisSevere, phCertDt: obj.phCertDt,
      phCertAuth: obj.phCertAuth, phentDoubleTa: obj.phentDoubleTa,
      Remarks: obj.remarks,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo
    });
    //debugger;

    let empStatus = obj.status;
    this.addtapdForm.disable();

    if (this.roleId == '5' && empStatus == 'N') {
      this.addtapdForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.btnCancel = false;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }

  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.addtapdForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.addtapdForm.get('OrderNo').value;

    this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, null, null).subscribe(result => {
      //debugger;
      if (result == 1) {
        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.isSubmitted = false;
        this.fwdtochker = false;
      }
      else {
        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }

      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
      this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');

    });

    this.Btntxt = 'Save';
  }


  updateStatus(status: string) {
    //debugger;   

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.addtapdForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {

            swal(this._msg.DDORejectedByChecker);

            this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
            this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');

            this.formGroupDirective.reset();
            this.isSubmitted = false;
            this.addtapdForm.disable();
            this.infoScreen = false;
            //this.createForm();

          }
          else {

            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckeraddtapdUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {
          swal(this._msg.DDOVerifiedByChecker);

          this.getaddtapdFormDetails1(this.empCd, this.roleId, 'history');
          this.getaddtapdFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.isSubmitted = false;
          this.addtapdForm.disable();
          this.infoScreen = false;
          //this.createForm();
        }
        else
        {
          swal(this._msg.updateFailedMsg);
        }
        this.ApprovePopup = false;
        this.RejectPopup = false;

      });
    }
  }

}
