import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ChangeSr } from '../../services/Change/ChangeSr.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-exserviceman',
  templateUrl: './exserviceman.component.html',
  styleUrls: ['./exserviceman.component.css']

})
export class ExservicemanComponent implements OnInit {
  Btntxt: string;
  editable: boolean = true;
  fwdtochker: boolean = false;
  infoScreen: boolean = false;
  btnCancel: boolean = false;
  rejectionRemark: string = '';
  controllerId: any;
  roleId: any;
  ddoId: any;

  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  pageSize: number = 0;
  pageNumber: number = 1;

  totalCount: number = 0;
  totalCounthis: number = 0;

  ExSerForm: FormGroup;
  RejectPopupForm: FormGroup;

  displayedColumns = ['Sr.No', 'EmpCode', 'EmployeeName', 'currentexservicemen', 'Status', 'Action']
  displayedColumns1 = ['Sr.No', 'EmpCode', 'EmployeeName', 'revisedexservicemen', 'Status']

  list: any = [];
  EDITpara: string = '';

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('historyPagination', { read: MatPaginator }) historyPagination: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  dataSource1: MatTableDataSource<any> = new MatTableDataSource<any>();//hist

  maxDate = new Date();
  public empCd: string;
  public username: any;
  public oldOrderNo: any;
  isSubmitted = false;
  constructor(private _service: ChangeSr, private _msg: CommonMsg) {
    this.controllerId = sessionStorage.getItem('controllerID');
    this.roleId = sessionStorage.getItem('userRoleID');
    this.ddoId = sessionStorage.getItem('ddoid');
    this.username = sessionStorage.getItem('username');


  }

  ngOnInit() {

    this.Btntxt = 'Save';
    this.createForm();

    this.RejectPopupcreateForm();
    this.infoScreen = true;

  }

  GetcommonMethod(value) {

    this.formGroupDirective.resetForm();
    this.empCd = value;
    this.isSubmitted = false;

    if (this.empCd) {
      this.ExSerForm.enable();
      //bind form
      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');

    }
    else {
      this.ExSerForm.disable();
    }


    this.editable = false;
    this.Btntxt = 'Save';

    this.fwdtochker = false;
    this.infoScreen = false;
    if (this.roleId == '5') {
      this.ExSerForm.disable();
    }



  }




  createForm() {
    this.ExSerForm = new FormGroup({
      OrderNo: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])),
      Remarks: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')]),

      OrderDate: new FormControl('', Validators.required),
      exservicemen: new FormControl('', Validators.required),
      empCd: new FormControl(this.empCd),
      username: new FormControl(this.username),
      rejectionRemark: new FormControl(this.rejectionRemark),
      oldOrderNo: new FormControl('')
    });

    this.ExSerForm.disable();
  }

  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9().,-/ ]*$')])
    });

  }



  onSubmit(flag?: any) {
    this.isSubmitted = true;
    this.ExSerForm.patchValue({
      empCd: this.empCd,
      username: this.username
    });
    //check duplicate record then submit
    if (this.oldOrderNo != null && this.Btntxt == "Update") {
      flag = "EDIT";
    }
    if (this.ExSerForm.valid) {

      this._service.InsertexservicemenFormDetails(this.ExSerForm.value, flag).subscribe(result => {

        this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
        this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');

        this.Btntxt = 'Save';
        if (flag == 'EDIT') {
          this.Btntxt = 'Update';
          if (result == '1') {


            swal(this._msg.updateMsg);
            //bind form
            this.Btntxt = 'Save';
            this.editable = true;
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
          }
          else {

            swal(this._msg.updateFailedMsg);

          }
        }
        else {
          if (result == '1') {

            swal(this._msg.saveMsg);
            this.formGroupDirective.resetForm();
            this.isSubmitted = false;
          }

          else {

            swal(this._msg.saveFailedMsg);
            this.formGroupDirective.resetForm();

          }
        }


        this.fwdtochker = false;
        this.ExSerForm.disable();
      });

    }
  }
  getexservicemenFormDetails1(empCd: any, roleId: any, flag: any) {

    this.pageNumber = 1;
    this._service.GetexservicemenFormDetails(empCd, roleId, flag).subscribe(data => {
      var self = this;
      if (flag == 'current') {

        this.dataSource = new MatTableDataSource(data);
        //filter in Data source        

        this.dataSource.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.currentexservicemen.toLowerCase().includes(filter);
        }

        //END Of filter in Data source


        this.pageSize = this.dataSource.data.length;
        this.dataSource.paginator = this.paginator;
        //enable or disbale form using current status of orderNo user
        let list: string[] = [];

        data.forEach(element => {
          list.push(element.status);

        });

        if (list[0] == 'E' || list[0] == 'U' || list[0] == 'P' || list[0] == 'R') {

          self.ExSerForm.disable();

          this.infoScreen = true;
        }
        else {
          self.ExSerForm.enable();
        }
        if (this.roleId == '5') {
          self.ExSerForm.disable();
          this.infoScreen = false;
        }

      }
      else if (flag == 'history') {

        this.dataSource1 = new MatTableDataSource(data);
        //filter in Data source        

        this.dataSource1.filterPredicate = (data, filter) => {
          return data.empCd.toLowerCase().includes(filter) ||
            data.empName.toLowerCase().includes(filter) ||
            data.revisedexservicemen.toLowerCase().includes(filter);
        }

        //END Of filter in Data source
        this.pageSize = this.dataSource1.data.length;
        this.dataSource1.paginator = this.historyPagination;
      }

    });
  }
  btnEditClick(obj) {
    this.ExSerForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      exservicemen: obj.exservicemen,
      Remarks: obj.remarks,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: obj.orderNo

    });
    this.oldOrderNo = obj.orderNo;


    this.ExSerForm.enable();
    this.infoScreen = false;

    //this.fwdtochker = true;
    this.Btntxt = 'Update';

    let empStatus = obj.status;

    if (empStatus == 'R') {
      this.EDITpara = 'EDIT';
      this.editable = true;
    }
    else if (empStatus == 'E' || empStatus == 'U') {
      this.EDITpara = 'EDIT';
      this.editable = false;
    }
    else {
      this.EDITpara = '';
      this.editable = false;
    }


    if (this.roleId == '6' && empStatus == 'U') {
      this.fwdtochker = true;
    }
    else {
      this.fwdtochker = false;
    }

  }

  applyFiltercurrent(filterValue) {

    this.paginator.pageIndex = 0;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches

    this.dataSource.filter = filterValue;
    if (this.dataSource.filteredData.length > 0) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource.filteredData.length = 0;
    }

  }
  applyFilterhistory(filterValue) {

    this.historyPagination.pageIndex = 0;

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches


    this.dataSource1.filter = filterValue;

    if (this.dataSource1.filteredData.length > 0) {
      this.dataSource1.filter = filterValue.trim().toLowerCase();
    } else {
      this.dataSource1.filteredData.length = 0;
    }
  }


  allowAlphaNumericSpace(event): boolean {
    if (event.target.value === "") {

      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode !== 32) && // space
        (charCode >= 48 && charCode <= 57) || // numeric (0-9)
        (charCode >= 65 && charCode <= 90) || // upper alpha (A-Z)
        (charCode >= 97 && charCode <= 122)) { // lower alpha (a-z)
        return true;
      }
      return false;
    }
  }
  objToBeDeleted: any;

  DeleteChangeDetails(obj) {
    this.objToBeDeleted = obj;
    this.formGroupDirective.resetForm();
    this.Btntxt = 'Save';
    this.fwdtochker = false;

  }


  confirmDelete() {

    this._service.ChangeDeleteDetails(this.empCd, this.objToBeDeleted.orderNo, 'exservicemen').subscribe(result => {

      this.objToBeDeleted = null;
      if (result == 1) {

        swal(this._msg.deleteMsg);


        this.formGroupDirective.resetForm();
        this.isSubmitted = false;
        this.ExSerForm.enable();
        this.infoScreen = false;
      }
      else {

        swal(this._msg.deleteFailedMsg);
      }

      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');

      this.DeletePopup = false;

    });
  }
  Cancel() {

    this.DeletePopup = false;
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

    this.rejectionRemark = null;
  }
  cancelForm() {

    this.formGroupDirective.resetForm();
    this.isSubmitted = false;
    this.ExSerForm.enable();
    this.editable = false;
    this.fwdtochker = false;
    this.Btntxt = 'Save';
  }

  btnInfoClick(obj) {

    this.oldOrderNo = '';
    this.ExSerForm.setValue({
      empCd: this.empCd,
      OrderNo: obj.orderNo,
      OrderDate: obj.orderDate,
      exservicemen: obj.exservicemen,
      Remarks: obj.remarks,
      rejectionRemark: obj.rejectionRemark,
      username: this.username,
      oldOrderNo: this.oldOrderNo
    });


    let empStatus = obj.status;
    this.ExSerForm.disable();

    if (this.roleId == '5' && empStatus == 'N') {
      this.ExSerForm.enable();
    }
    else if (this.roleId == '5' && (empStatus == 'V' || empStatus == 'R')) {
      this.infoScreen = false;
    }
    else if (this.roleId == '6' && (empStatus == 'V' || empStatus == 'P')) {
      this.infoScreen = true;
      this.btnCancel = false;
      this.Btntxt = 'Save';
    }
    else {
      this.infoScreen = true;
      this.fwdtochker = false;
      this.Btntxt = 'Save';
    }

  }

  //fwdtochecker

  forwardToChecker() {
    //debugger;
    if (!this.ExSerForm.valid) {
      return false;
    }
    let employeeId = this.empCd.trim();
    let orderNo = this.ExSerForm.get('OrderNo').value;

    this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, null, null).subscribe(result => {

      if (result == 1) {

        swal(this._msg.forwardDDOCheckerMsg);
        this.formGroupDirective.reset();
        this.isSubmitted = false;
        this.fwdtochker = false;

      }
      else {

        swal(this._msg.forwardDDOCheckerFailedMsg);
        this.fwdtochker = true;
      }
      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
      this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');
    });

    this.Btntxt = 'Save';
  }


  updateStatus(status: string) {

    let rejectionRemark1 = '';
    let employeeId = this.empCd;
    let orderNo = this.ExSerForm.get('OrderNo').value;
    if (status == 'R') {
      rejectionRemark1 = this.RejectPopupForm.get('rejectionRemark1').value;

      if (this.RejectPopupForm.valid) {
        this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
          if (result == 1) {

            swal(this._msg.DDORejectedByChecker);

            this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
            this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');

            this.formGroupDirective.reset();
            this.isSubmitted = false;
            this.ExSerForm.disable();
            this.infoScreen = false;

          }
          else {

            swal(this._msg.updateFailedMsg);
          }
          this.ApprovePopup = false;
          this.RejectPopup = false;

          this.RejectPopupForm.controls['rejectionRemark1'].setValue('');

        });
      }
    }
    else {
      this._service.ForwardToCheckerexservicemenUserDtls(employeeId, orderNo, status, rejectionRemark1).subscribe(result => {
        if (result == 1) {


          swal(this._msg.DDOVerifiedByChecker);

          this.getexservicemenFormDetails1(this.empCd, this.roleId, 'history');
          this.getexservicemenFormDetails1(this.empCd, this.roleId, 'current');

          this.formGroupDirective.reset();
          this.isSubmitted = false;
          this.ExSerForm.disable();
          this.infoScreen = false;

        }
        else {

          swal(this._msg.updateFailedMsg);

        }
        this.ApprovePopup = false;
        this.RejectPopup = false;


      });
    }
  }

}
