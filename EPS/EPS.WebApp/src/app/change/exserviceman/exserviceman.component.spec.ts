import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExservicemanComponent } from './exserviceman.component';

describe('ExservicemanComponent', () => {
  let component: ExservicemanComponent;
  let fixture: ComponentFixture<ExservicemanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExservicemanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExservicemanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
