import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecoveryExcessPaymentRoutingModule } from './recovery-excess-payment-routing.module';
import { RecoveryPaymentComponent } from './recovery-payment/recovery-payment.component';
import { TemporaryPermanentStopComponent } from './temporary-permanent-stop/temporary-permanent-stop.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommanMstComponent } from '../shared-module/comman-mst/comman-mst.component';


@NgModule({
  declarations: [RecoveryPaymentComponent, TemporaryPermanentStopComponent],
  imports: [
    CommonModule,
    RecoveryExcessPaymentRoutingModule,MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTooltipModule,
    NgxMatSelectSearchModule,
    SharedModule 
  ],
  exports: [CommanMstComponent]
})
export class RecoveryExcessPaymentModule { }
