import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemporaryPermanentStopComponent } from './temporary-permanent-stop.component';

describe('TemporaryPermanentStopComponent', () => {
  let component: TemporaryPermanentStopComponent;
  let fixture: ComponentFixture<TemporaryPermanentStopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemporaryPermanentStopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemporaryPermanentStopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
