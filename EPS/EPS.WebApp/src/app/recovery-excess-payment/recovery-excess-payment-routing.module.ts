import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecoveryExcessPaymentModule } from './recovery-excess-payment.module';
import { RecoveryPaymentComponent } from './recovery-payment/recovery-payment.component';
import { TemporaryPermanentStopComponent } from './temporary-permanent-stop/temporary-permanent-stop.component';

const routes: Routes = [{
  path: '', component: RecoveryExcessPaymentModule, children: [

    { path: 'paymentRecovery', component: RecoveryPaymentComponent },
    { path: 'temporary', component: TemporaryPermanentStopComponent },
    
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecoveryExcessPaymentRoutingModule { }
