import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import {designationCode } from '../../model/DeptationModel/depuationin-model';
import { MasterService } from '../../services/master/master.service';
import { DeputationinService } from '../../services/deputation/deputationin.service';
import { DeputationoutService } from '../../services/deputation/deputationout.service';

import { takeUntil } from 'rxjs/operators';
import { MyErrorStateMatcher } from '../../global/error-state-matcher';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-deputation-out',
  templateUrl: './deputation-out.component.html',
  styleUrls: ['./deputation-out.component.css'],
  providers: [CommonMsg]
})
export class DeputationOutComponent implements OnInit {
 
  form: FormGroup;
  deputationTypes: any = [];
  btnUpdatetext: string;
  serviceType: any = [];
  designation: any = [];
  dataSource: any;
  savebuttonstatus: boolean;
  roleId: any;
  DeletePopup: boolean = false;
  ApprovePopup: boolean = false;
  RejectPopup: boolean = false;
  infoScreen: boolean = false;
  rejectionRemark: string = '';
  disableForwardflag: boolean = true;
  btndisabled: boolean = false;
  disableflag: boolean = true;
  flag: boolean = false;
  displayedColumns2: string[] = ['srNo', 'deputeFrom', 'orderNo', 'Status', 'action'];
  @ViewChild('formDirective') private formDirective;
  data: any;
  eventsSubject: Subject<void> = new Subject<void>();
  RejectPopupForm: FormGroup;
  public designationCtrl: FormControl = new FormControl();
  public designFilterCtrl: FormControl = new FormControl();
  public filteredDesignation: Subject<designationCode[]> = new Subject<designationCode[]>();
  constructor(private _formBuilder: FormBuilder, private master: MasterService, private deputationInService: DeputationinService, private deputationOutService: DeputationoutService, private snackBar: MatSnackBar, private _message: CommonMsg) { }
  submitted = false;

  private _onDestroy = new Subject<void>();
  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.btnUpdatetext = 'Save';
    this.roleId = sessionStorage.getItem('userRoleID');
    this.savebuttonstatus = true;
    this.FormDeatils();
    this.RejectPopupcreateForm();
    this.getDeputationType();
    this.getServiceType();
    this.getAllDesignation();
    this.designFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDesignation();
      });
    this.btndisabled = true;
    this.form.disable();
    this.flag=true;
   
  }
  public empCd: string;
  GetcommanMethod(value) {
 
    //this.getLoanDetails(value);
    debugger;
    this.formDirective.resetForm();
    this.btnUpdatetext = 'Save';
    this.empCd = value;
    if (value == "") {
      this.disableForwardflag= true;
      this.btndisabled = true;
      this.dataSource = null
      this.form.disable();
    }
    else {
     
     this.getDeputaiondetails(value, this.roleId);

    
    }
    this.form.get('employeeCode').setValue(this.empCd)
   

  }
  //someDateToBlock: number = 3;
  //dateFilter  = (d: Date): boolean => {
  //  const day = d.getDay();
  //  debugger;
  //  // THIS FUNCTION CANNOT ACCESS THE VARIABLE 'someDateToBlock'
  //  return day !== 0 && day !== 6;
  //}
  //dateFilter = (d: Date = new Date()) => d.getDate() == 1;
  getDeputaiondetails(Empcd: any, roleId: any) {
    debugger;
    this.deputationOutService.getAllDepuatationOutDetails(Empcd, roleId).subscribe(result => {
      
      if (result.length == 0) {
        this.formDirective.resetForm();
        this.form.enable();
        this.dataSource = null;
        this.infoScreen = false;
        this.form.get('employeeCode').setValue(this.empCd);
        this.btndisabled = false;
        this.flag = true;
        if (this.roleId == '5') {
          this.form.disable();
        }
      }
      else {
        this.dataSource = result;
       // this.form.enable();
        this.form.patchValue(result[0]);
        this.form.disable();
        this.btndisabled = true;
        this.infoScreen = true;
        this.flag = false;
        if (result[0].veriFlag == 'E' || result[0].veriFlag == 'U') {
          this.disableForwardflag = false;
        }
        else {
          this.disableForwardflag = true;
        }
      }
      if (this.roleId == '5' && (result[0].veriFlag == 'V' || result[0].veriFlag == 'R')) {
        this.infoScreen = false;
      }
    })

  }

  FormDeatils() {
    this.form = this._formBuilder.group({
      'deputedTypeId': [null, Validators.required],
      'depuTypeName': [null],
      'serviceTypeId': [null, Validators.required],
      'depuOrderNo': [null, Validators.required],
      'depuOrderDate': [null, Validators.required],
      'depuedToOffice': [null, Validators.required],
      'depuOnDesignation': [null, Validators.required],
      'dateOfDeputataion': [null, Validators.required],
      'relievingDate': [null],
      'depuTenureYear': [null, Validators.maxLength(4)],
      'depuTenureMonth': [null, Validators.maxLength(2)],
      'depuTenureDays': [null, Validators.maxLength(2)],

      'employeeCode': [null, Validators.required],

      'empDeputDetailsId': [null],
      'flagUpdate': [null],
      'rejectionRemark': [null]



    });
  }

  btnEditClick(empDeputId) {
    debugger;
    this.formDirective.resetForm();
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.flag = true;
    this.form.patchValue(value);
    this.form.enable();

    
    //if (value.veriFlag == 'N') {
    
    //}
    //else {
      
    //}
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;
      debugger;
      this.btndisabled = false;
      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
      debugger;
      this.disableflag = true;
      this.btndisabled = false;
      if (value.veriFlag == 'N') {
        this.btnUpdatetext = 'Save';
        this.disableForwardflag = true;
      }
      else {
        this.btnUpdatetext = 'Update';
        this.disableForwardflag = true;
      }
      //this.Btntxt = 'Save';
    }
  }
  RejectPopupcreateForm() {

    this.RejectPopupForm = new FormGroup({
      rejectionRemark1: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z]{1}[A-Za-z0-9-/_,. ]+')])
    });

  }
  btnInfoClick(empDeputId) {
    let value = this.dataSource.filter(x => x.empDeputDetailsId == empDeputId)[0]
    this.form.patchValue(value);
    this.form.disable();
    //let empStatus = value.veriFlag;
    if (this.roleId == '5' && (value.veriFlag == 'V' || value.veriFlag == 'R')) {
      this.infoScreen = false;

      this.disableflag = true;
      this.disableForwardflag = true;
    }
    else {
      this.infoScreen = true;
     
      this.disableForwardflag = true;
      //this.Btntxt = 'Save';
    }
  }
  numberOnly(event): boolean {
    debugger;
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  Checkdate() {
   
    if (this.form.get('depuOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('dateOfDeputataion').value || this.form.get('depuOrderDate').value > this.form.get('dateOfDeputataion').value) {
      //this.form.get('deputation_Effective_Date').value
      this.form.get('dateOfDeputataion').setValue("");
    }
    if (this.form.get('depuOrderDate').value.format('YYYY-MM-DD HH:mm:ss') > this.form.get('relievingDate').value || this.form.get('depuOrderDate').value > this.form.get('relievingDate').value) {
      //this.form.get('deputation_Effective_Date').value
      this.form.get('relievingDate').setValue("");
    }
    //alert(dd)
  }
  getDeputationType() {
    this.master.getDeputationType().subscribe(res => {
 
      this.deputationTypes = res;
    })
  }
  getServiceType() {
    this.master.getServiceType().subscribe(res => {
 
      this.serviceType = res;
    })
  }
  getAllDesignation() {
    this.deputationInService.GetAllDesignation(sessionStorage.getItem('controllerID')).subscribe(res => {

    
      this.designation = res;
      this.designationCtrl.setValue(this.designation);
      this.filteredDesignation.next(this.designation);
    })
  }
  private filterDesignation() {
    
    if (!this.designation) {
      return;
    }
    // get the search keyword
    let search = this.designFilterCtrl.value;
    if (!search) {
      this.filteredDesignation.next(this.designation.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredDesignation.next(

      this.designation.filter(designation => designation.desigDesc.toLowerCase().indexOf(search) > -1)
    );
  }
  //get f() { return this.form.controls; }
  onSubmit() {
 //   debugger;
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    else {
      if (this.btnUpdatetext == 'Update') {
        this.form.get('flagUpdate').setValue('update');

      }
      else {
        this.form.get('flagUpdate').setValue('insert');
      }
      this.deputationOutService.InsertUpateDeputationOutDetails(this.form.value).subscribe(result1 => {

        let resultvalue = result1.split("-");

        if (parseInt(resultvalue[0]) >= 1) {
          if (this.btnUpdatetext == 'Update') {
            this.snackBar.open(this._message.updateMsg, null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getDeputaiondetails(this.empCd, this.roleId);
            this.btnUpdatetext = 'Save';
            this.disableForwardflag = false;
          }
          else {
            this.snackBar.open(this._message.saveMsg, null, { duration: 4000 });
            this.form.reset();
            this.formDirective.resetForm();
            this.getDeputaiondetails(this.empCd,this.roleId);
            this.btnUpdatetext == 'Save';
            this.disableForwardflag = false;
          }

        } else {

          if (this.btnUpdatetext == 'Update') {
            if (resultvalue[2] = 'Duplicate Record') {
              this.snackBar.open(this._message.depRepaAlreadyExits, null, { duration: 4000 });
            }
            else {
              this.snackBar.open(this._message.updateFailedMsg, null, { duration: 4000 });
            }

          }
          else if (resultvalue[2] = 'Duplicate Record') {
            this.snackBar.open(this._message.depRepaAlreadyExits, null, { duration: 4000 });
          }
          else {
            this.snackBar.open(this._message.saveFailedMsg, null, { duration: 4000 });
          }
        }
      })

    }

  }
  ResetForm() {
    debugger;

   // location.reload(true);
    this.eventsSubject.next(this.data);
    this.form.reset();
    this.formDirective.resetForm();
    this.btnUpdatetext = 'Save';
    this.disableForwardflag = true;
    this.btndisabled = true;
    this.dataSource = null;
    this.flag = true;
    this.form.disable();
    //if (this.empCd == null || this.empCd == undefined) {
    //  this.form.disable();
    //  // this.getDeputaiondetails(this.empCd, this.roleId);
    //  this.form.get('employeeCode').setValue(this.empCd);
    //}
    //else {
    //  this.form.get('employeeCode').setValue(this.empCd);
    //  if (this.dataSource != null && this.dataSource != undefined) {
    //    let value = this.dataSource[0]
    //    if (this.dataSource[0].veriFlag == 'E' || this.dataSource[0].veriFlag == 'U') {
    //      this.disableForwardflag = false;
         
    //    }
    //    else {
    //      this.disableForwardflag = true;
       
    //    }
  
       
      //}
 
  //  }
    
    //this.form.disable();
  

  }
  Cancel() {
    this.ApprovePopup = false;
    this.RejectPopup = false;
    this.rejectionRemark = '';
    this.DeletePopup = false;
  }
  forwardStatusUpdate(statusFlag: any) {

    var RejectionComment = '';
    if (this.RejectPopupForm.invalid && statusFlag == 'R') {

      this.RejectPopupForm.controls['rejectionRemark1'].markAsDirty();
      return false;
    }
    else {
      let empDeputDetailsId = this.form.get('empDeputDetailsId').value;
      if (statusFlag == 'R') {
        RejectionComment = this.rejectionRemark;
      }
      else {
        RejectionComment = ''
      }

      this.deputationInService.forwardStatusUpdate(empDeputDetailsId, statusFlag, RejectionComment).subscribe(result1 => {
        if (parseInt(result1) >= 1) {
          if (statusFlag == 0) {
            this.snackBar.open(this._message.forwardDDOCheckerMsg, null, { duration: 4000 });
          }
          if (statusFlag == 'V') {
            this.snackBar.open(this._message.VerifyedByChecker, null, { duration: 4000 });
          }
          if (statusFlag == 'R') {
            this.snackBar.open(this._message.RejectedByChecker, null, { duration: 4000 });
          }

          this.form.reset();
          this.formDirective.resetForm();
          this.btnUpdatetext = 'Save';
          this.getDeputaiondetails(this.empCd, this.roleId);
          this.infoScreen = false;
          this.ApprovePopup = false;
          this.RejectPopup = false;
          this.rejectionRemark = '';
        }
      });
    }
  }
}

