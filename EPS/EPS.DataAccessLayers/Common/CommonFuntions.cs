﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Reflection;

namespace EPS.DataAccessLayers.Common
{
    public static class CommonFuntions
    {
        #region Map DB data

        public static T MapFetchData<T>(T obj, IDataReader rdr)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName); // getting property datatype
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                property.SetValue(obj, !DBNull.Value.Equals(rdr[property.Name.ToString()]) ? Convert.ChangeType(rdr[property.Name.ToString()], type) : null); // setting property value according to datatype and propertyname
            }
            return obj;
        }


        public static string MapUpdateParameters<T>(T obj, Database _epsDatabase, System.Data.Common.DbCommand dbCommand)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName); // getting property datatype
                if (type.Namespace.Equals("System"))
                {
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        type = Nullable.GetUnderlyingType(type);
                    }
                    _epsDatabase.AddInParameter(dbCommand, property.Name, (DbType)Enum.Parse(typeof(DbType), type.Name, true), property.GetValue(obj));
                }
                else
                {
                    IEnumerable<dynamic> value = property.GetValue(obj) as IEnumerable<dynamic>;
                    var datatble = CreateDataTable(value);
                    SqlParameter param = new SqlParameter(property.Name, datatble);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);
                }
            }
            return (_epsDatabase.ExecuteNonQuery(dbCommand).ToString());
        }


        private static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            DataTable table = new DataTable();
            var properties = new PropertyInfo[20]; // typeof(T).GetProperties();
            foreach (T l in list)
            {
                Type t = l.GetType();
                properties = t.GetProperties();
                break;
            }
            foreach (var property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName);
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                table.Columns.Add(property.Name, type);
            }
            foreach (T li in list)
            {
                var values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(li, null);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        #endregion
        #region Convert List To Table
      
            public static DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }

        #endregion
        public static string GetIP()
        {
            string strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;

            return addr[addr.Length - 1].ToString();
        }
    }
}
