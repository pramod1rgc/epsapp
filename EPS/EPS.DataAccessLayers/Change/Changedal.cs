﻿using EPS.BusinessModels.Change;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Change
{
    public class Changedal
    {
        private Database epsdatabase = null;

        public Changedal(Database database)
        {
            epsdatabase = database;
        }

        //Common
        public async Task<IEnumerable<ddlvalue>> GetDdlvalue(string Flag)
        {
            List<ddlvalue> obj = new List<ddlvalue>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Getddlchangemodule))
                {
                    epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            ddlvalue objData = new ddlvalue();
                            objData.text1 = objReader["text1"] != DBNull.Value ? Convert.ToString(objReader["text1"]) : null;
                            objData.value1 = objReader["value1"] != DBNull.Value ? Convert.ToString(objReader["value1"]) : null;
                            obj.Add(objData);
                        }
                    }
                }

            });
            if (obj == null)
                return null;
            else
                return obj;
        }

        public async Task<int> ChangeDeleteDetails(string empCd, string orderNo, string Flag)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_CommonDelete))
                {
                    epsdatabase.AddInParameter(dbCommand, "@empCd", DbType.String, empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag.Trim());
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            if (result == 2 || result == 6 || result == 4 || result == 8 || result == 9)
                result = 1;

            return result;
        }


        #region DOB

        public async Task<string> InsertDOBDetails(DOBcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertDOB))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@DOB", DbType.DateTime, objPro.DOB);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2") //2 insert and 2 update
                result = "1";

            return result;
        }

        public async Task<IEnumerable<DOBcls>> GetDOBDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<DOBcls> obj = new List<DOBcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOBDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                DOBcls objData = new DOBcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.currentAnnuationDate = objReader["currentAnnuationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["currentAnnuationDate"]) : objData.currentAnnuationDate = null;
                                objData.currentDOB = objReader["currentDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOB"]) : objData.currentDOB = null;
                                objData.DOB = objReader["DOB"] != DBNull.Value ? Convert.ToDateTime(objReader["DOB"]) : objData.DOB = null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<DOBcls> obj = new List<DOBcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOBDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                DOBcls objData = new DOBcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.revisedAnnuationDate = objReader["revisedAnnuationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedAnnuationDate"]) : objData.revisedAnnuationDate = null;
                                objData.revisedDOB = objReader["revisedDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOB"]) : objData.revisedDOB = null;
                                objData.DOB = objReader["DOB"] != DBNull.Value ? Convert.ToDateTime(objReader["DOB"]) : objData.DOB = null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;


                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }


        public async Task<string> ForwardToCheckerDOBUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOBFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOBFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }





        #endregion

        #region NameGender

        public async Task<string> InsertNameGenderDetails(NameGender objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertNameGender))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@FName", DbType.String, objPro.FirstName.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@MName", DbType.String, objPro.MiddleName.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@LName", DbType.String, objPro.LastName.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Gender", DbType.String, objPro.Gender);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "8") //8 insert and 8 update
                result = "1";

            return result;
        }

        public async Task<IEnumerable<NameGender>> GetNameGenderDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<NameGender> obj = new List<NameGender>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetNameGenderDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                NameGender objData = new NameGender();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.currentDesignation = objReader["currentDesignation"] != DBNull.Value ? Convert.ToString(objReader["currentDesignation"]) : null;
                                objData.currentGender = objReader["currentGender"] != DBNull.Value ? Convert.ToString(objReader["currentGender"]) : null;
                                objData.currentEmpName = objReader["currentEmpName"] != DBNull.Value ? Convert.ToString(objReader["currentEmpName"]) : null;

                                objData.FirstName = objReader["FirstName"] != DBNull.Value ? Convert.ToString(objReader["FirstName"]) : null;
                                objData.MiddleName = objReader["MiddleName"] != DBNull.Value ? Convert.ToString(objReader["MiddleName"]) : null;
                                objData.LastName = objReader["LastName"] != DBNull.Value ? Convert.ToString(objReader["LastName"]) : null;
                                objData.Gender = objReader["Gender"] != DBNull.Value ? Convert.ToString(objReader["Gender"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<NameGender> obj = new List<NameGender>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetNameGenderDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                NameGender objData = new NameGender();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.revisedDesignation = objReader["revisedDesignation"] != DBNull.Value ? Convert.ToString(objReader["revisedDesignation"]) : null;
                                objData.revisedGender = objReader["revisedGender"] != DBNull.Value ? Convert.ToString(objReader["revisedGender"]) : null;
                                objData.revisedEmpName = objReader["revisedEmpName"] != DBNull.Value ? Convert.ToString(objReader["revisedEmpName"]) : null;

                                objData.FirstName = objReader["FirstName"] != DBNull.Value ? Convert.ToString(objReader["FirstName"]) : null;
                                objData.MiddleName = objReader["MiddleName"] != DBNull.Value ? Convert.ToString(objReader["MiddleName"]) : null;
                                objData.LastName = objReader["LastName"] != DBNull.Value ? Convert.ToString(objReader["LastName"]) : null;
                                objData.Gender = objReader["Gender"] != DBNull.Value ? Convert.ToString(objReader["Gender"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerNameGenderUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateNameGenderFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "4" || result == "8")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateNameGenderFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "4")
                    result = "1";

                return result;
            }
        }


        #endregion

        #region pfNps


        public async Task<string> InsertPfNpsFormDetails(PFNPS objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertPfNps))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@pfType", DbType.String, objPro.PfType.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@PRANNo", DbType.String, objPro.PranNo);
                    epsdatabase.AddInParameter(dbCommand, "@Wefdt", DbType.DateTime, objPro.WithEffectDate);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "4" || result == "3") //2 insert 
                result = "1";

            return result;
        }

        public async Task<IEnumerable<PFNPS>> GetPfNpsFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<PFNPS> obj = new List<PFNPS>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetPfNpsDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                PFNPS objData = new PFNPS();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.PfType = objReader["pfType"] != DBNull.Value ? Convert.ToString(objReader["pfType"]) : null;
                                objData.PranNo = objReader["PranNo"] != DBNull.Value ? Convert.ToString(objReader["PranNo"]) : null;
                                objData.currentPFType = objReader["currentPFType"] != DBNull.Value ? Convert.ToString(objReader["currentPFType"]) : null;
                                objData.currentPRANNo = objReader["currentPRANNo"] != DBNull.Value ? Convert.ToString(objReader["currentPRANNo"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<PFNPS> obj = new List<PFNPS>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetPfNpsDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                PFNPS objData = new PFNPS();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.PfType = objReader["pfType"] != DBNull.Value ? Convert.ToString(objReader["pfType"]) : null;
                                objData.PranNo = objReader["PranNo"] != DBNull.Value ? Convert.ToString(objReader["PranNo"]) : null;
                                objData.revisedPFType = objReader["revisedPFType"] != DBNull.Value ? Convert.ToString(objReader["revisedPFType"]) : null;
                                objData.revisedPRANNo = objReader["revisedPRANNo"] != DBNull.Value ? Convert.ToString(objReader["revisedPRANNo"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerPfNpsUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdatePfNpsFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2" || result == "4")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdatePfNpsFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "2")
                    result = "1";

                return result;
            }
        }


        #endregion

        #region panno

        public async Task<string> InsertPanNoFormDetails(PANNumber objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertPanNo))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@PANNo", DbType.String, objPro.PANNo);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<PANNumber>> GetPanNoFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<PANNumber> obj = new List<PANNumber>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetPanNoDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                PANNumber objData = new PANNumber();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.PANNo = objReader["PANNo"] != DBNull.Value ? Convert.ToString(objReader["PANNo"]) : null;
                                objData.currentPANNo = objReader["currentPANNo"] != DBNull.Value ? Convert.ToString(objReader["currentPANNo"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<PANNumber> obj = new List<PANNumber>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetPanNoDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                PANNumber objData = new PANNumber();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.PANNo = objReader["PANNo"] != DBNull.Value ? Convert.ToString(objReader["PANNo"]) : null;
                                objData.revisedPANNo = objReader["revisedPANNo"] != DBNull.Value ? Convert.ToString(objReader["revisedPANNo"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }


        public async Task<string> ForwardToCheckerPanNoUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdatePanNoFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdatePanNoFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }


        #endregion

        #region HRAcityclass

        public async Task<string> InsertHraccFormDetails(HRAcityclass objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertHRA))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@HRAcityclass", DbType.String, objPro.HRAcc);
                    epsdatabase.AddInParameter(dbCommand, "@Wefdt", DbType.DateTime, objPro.WithEffectDate);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<HRAcityclass>> GetHraccFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<HRAcityclass> obj = new List<HRAcityclass>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetHraDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                HRAcityclass objData = new HRAcityclass();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.HRAcc = objReader["HRAcc"] != DBNull.Value ? Convert.ToString(objReader["HRAcc"]) : null;
                                objData.currentHRAcc = objReader["currentHRAcc"] != DBNull.Value ? Convert.ToString(objReader["currentHRAcc"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<HRAcityclass> obj = new List<HRAcityclass>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetHraDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                HRAcityclass objData = new HRAcityclass();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.HRAcc = objReader["HRAcc"] != DBNull.Value ? Convert.ToString(objReader["HRAcc"]) : null;
                                objData.revisedHRAcc = objReader["revisedHRAcc"] != DBNull.Value ? Convert.ToString(objReader["revisedHRAcc"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }




        public async Task<string> ForwardToCheckerHraccUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateHraFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateHraFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }



        #endregion

        #region designation

        public async Task<string> InsertdesignationFormDetails(designationcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertDesignation))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@designation", DbType.String, objPro.designation);
                    epsdatabase.AddInParameter(dbCommand, "@Wefdt", DbType.DateTime, objPro.WithEffectDate);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<designationcls>> GetdesignationFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<designationcls> obj = new List<designationcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDesignationDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                designationcls objData = new designationcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.designation = objReader["designation"] != DBNull.Value ? Convert.ToString(objReader["designation"]) : null;
                                objData.currentdesignation = objReader["currentdesignation"] != DBNull.Value ? Convert.ToString(objReader["currentdesignation"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<designationcls> obj = new List<designationcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDesignationDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                designationcls objData = new designationcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.designation = objReader["designation"] != DBNull.Value ? Convert.ToString(objReader["designation"]) : null;
                                objData.reviseddesignation = objReader["reviseddesignation"] != DBNull.Value ? Convert.ToString(objReader["reviseddesignation"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                objData.WithEffectDate = objReader["WithEffectDate"] != DBNull.Value ? Convert.ToDateTime(objReader["WithEffectDate"]) : objData.WithEffectDate = null;
                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerdesignationUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDesignationFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDesignationFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }


        #endregion

        #region exservicemen

        public async Task<string> InsertexservicemenFormDetails(exservicemencls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertExservicemen))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Exservicemen", DbType.String, objPro.exservicemen);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);


                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<exservicemencls>> GetexservicemenFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<exservicemencls> obj = new List<exservicemencls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetExservicemenDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                exservicemencls objData = new exservicemencls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.exservicemen = objReader["exservicemen"] != DBNull.Value ? Convert.ToString(objReader["exservicemen"]) : null;
                                objData.currentexservicemen = objReader["currentexservicemen"] != DBNull.Value ? Convert.ToString(objReader["currentexservicemen"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<exservicemencls> obj = new List<exservicemencls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetExservicemenDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                exservicemencls objData = new exservicemencls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.exservicemen = objReader["exservicemen"] != DBNull.Value ? Convert.ToString(objReader["exservicemen"]) : null;
                                objData.revisedexservicemen = objReader["revisedexservicemen"] != DBNull.Value ? Convert.ToString(objReader["revisedexservicemen"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerexservicemenUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateExservicemenFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateExservicemenFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }

        #endregion

        #region cghs

        public async Task<string> InsertCGHSFormDetails(CGHScls objPro, string Flag)
        {
            if (objPro.cghsDeduction == "N")
            {
                objPro.cghsCardNo = null;
            }


            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertCGHS))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@cghsDeduction", DbType.String, objPro.cghsDeduction);
                    epsdatabase.AddInParameter(dbCommand, "@cghsCardNo", DbType.String, objPro.cghsCardNo);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);


                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "4")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<CGHScls>> GetCGHSFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<CGHScls> obj = new List<CGHScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCGHSDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                CGHScls objData = new CGHScls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.cghsDeduction = objReader["cghsDeduction"] != DBNull.Value ? Convert.ToString(objReader["cghsDeduction"]) : null;
                                objData.cghsCardNo = objReader["cghsCardNo"] != DBNull.Value ? Convert.ToString(objReader["cghsCardNo"]) : null;
                                objData.currentCGHSDeductionApplicable = objReader["currentCGHSDeductionApplicable"] != DBNull.Value ? Convert.ToString(objReader["currentCGHSDeductionApplicable"]) : objData.currentCGHSDeductionApplicable = null;
                                objData.currentCGHSCardNo = objReader["currentCGHSCardNo"] != DBNull.Value ? Convert.ToString(objReader["currentCGHSCardNo"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<CGHScls> obj = new List<CGHScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCGHSDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                CGHScls objData = new CGHScls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.cghsDeduction = objReader["cghsDeduction"] != DBNull.Value ? Convert.ToString(objReader["cghsDeduction"]) : null;
                                objData.cghsCardNo = objReader["cghsCardNo"] != DBNull.Value ? Convert.ToString(objReader["cghsCardNo"]) : null;
                                objData.revisedCGHSDeductionApplicable = objReader["revisedCGHSDeductionApplicable"] != DBNull.Value ? Convert.ToString(objReader["revisedCGHSDeductionApplicable"]) : objData.revisedCGHSDeductionApplicable = null;
                                objData.revisedCGHSCardNo = objReader["revisedCGHSCardNo"] != DBNull.Value ? Convert.ToString(objReader["revisedCGHSCardNo"]) : null;


                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerCGHSUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCGHSFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2" || result == "4")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCGHSFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "2")
                    result = "1";

                return result;
            }
        }





        #endregion

        #region CGEGIS

        public async Task<string> InsertCGEGISFormDetails(CGEGIScls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertCGEGIS))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@CGEGISapplicable", DbType.String, objPro.cgegisapplicable);
                    epsdatabase.AddInParameter(dbCommand, "@RevisedCGEGISGroup", DbType.String, objPro.cgegisgroup);
                    epsdatabase.AddInParameter(dbCommand, "@RevisedMembershipDate", DbType.DateTime, objPro.membershipdate);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);


                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "6")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<CGEGIScls>> GetCGEGISFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<CGEGIScls> obj = new List<CGEGIScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCGEGISDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                CGEGIScls objData = new CGEGIScls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.currentCGEGISapplicable = objReader["currentCGEGISapplicable"] != DBNull.Value ? Convert.ToString(objReader["currentCGEGISapplicable"]) : null;
                                objData.cgegisapplicable = objReader["cgegisapplicable"] != DBNull.Value ? Convert.ToString(objReader["cgegisapplicable"]) : null;
                                objData.cgegisgroup = objReader["cgegisgroup"] != DBNull.Value ? Convert.ToString(objReader["cgegisgroup"]) : null;
                                objData.membershipdate = objReader["membershipdate"] != DBNull.Value ? Convert.ToDateTime(objReader["membershipdate"]) : objData.membershipdate = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<CGEGIScls> obj = new List<CGEGIScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCGEGISDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                CGEGIScls objData = new CGEGIScls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.revisedCGEGISapplicable = objReader["revisedCGEGISapplicable"] != DBNull.Value ? Convert.ToString(objReader["revisedCGEGISapplicable"]) : null;
                                objData.cgegisapplicable = objReader["cgegisapplicable"] != DBNull.Value ? Convert.ToString(objReader["cgegisapplicable"]) : null;
                                objData.cgegisgroup = objReader["cgegisgroup"] != DBNull.Value ? Convert.ToString(objReader["cgegisgroup"]) : null;
                                objData.membershipdate = objReader["membershipdate"] != DBNull.Value ? Convert.ToDateTime(objReader["membershipdate"]) : objData.membershipdate = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerCGEGISFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCGEGISFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "3" || result == "6")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCGEGISFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "3")
                    result = "1";

                return result;
            }
        }





        #endregion

        #region joiningmode

        public async Task<string> InsertJoiningModeFormDetails(joiningmodecls objPro, string Flag)
        {
            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertJoiningMode))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@JoiningMode", DbType.String, objPro.joiningMode);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";
            return result;
        }

        public async Task<IEnumerable<joiningmodecls>> GetJoiningModeFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<joiningmodecls> obj = new List<joiningmodecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetJoiningModeDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                joiningmodecls objData = new joiningmodecls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.joiningMode = objReader["joiningMode"] != DBNull.Value ? Convert.ToString(objReader["joiningMode"]) : null;
                                objData.currentJoiningMode = objReader["currentJoiningMode"] != DBNull.Value ? Convert.ToString(objReader["currentJoiningMode"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<joiningmodecls> obj = new List<joiningmodecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetJoiningModeDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                joiningmodecls objData = new joiningmodecls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.joiningMode = objReader["joiningMode"] != DBNull.Value ? Convert.ToString(objReader["joiningMode"]) : null;
                                objData.revisedJoiningMode = objReader["revisedJoiningMode"] != DBNull.Value ? Convert.ToString(objReader["revisedJoiningMode"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerJoiningModeFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateJoiningModeFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";
                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateJoiningModeFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });


                return result;
            }
        }





        #endregion

        #region DOJ

        public async Task<string> InsertDOJDetails(DOJcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertDOJ))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@DOJ", DbType.DateTime, objPro.DOJ);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<DOJcls>> GetDOJDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<DOJcls> obj = new List<DOJcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOJDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                DOJcls objData = new DOJcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOJ = objReader["DOJ"] != DBNull.Value ? Convert.ToDateTime(objReader["DOJ"]) : objData.DOJ = null;
                                objData.currentDOJ = objReader["currentDOJ"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOJ"]) : objData.currentDOJ = null;
                                objData.currentDOG = objReader["currentDOG"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOG"]) : objData.currentDOG = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<DOJcls> obj = new List<DOJcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOJDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                DOJcls objData = new DOJcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOJ = objReader["DOJ"] != DBNull.Value ? Convert.ToDateTime(objReader["DOJ"]) : objData.DOJ = null;
                                objData.revisedDOJ = objReader["revisedDOJ"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOJ"]) : objData.revisedDOJ = null;
                                objData.revisedDOG = objReader["revisedDOG"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOG"]) : objData.revisedDOG = null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }


        public async Task<string> ForwardToCheckerDOJUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOJFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOJFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }



        #endregion

        #region DOE

        public async Task<string> InsertDOEDetails(DOEcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertDOE))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@DOE", DbType.DateTime, objPro.DOE);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<DOEcls>> GetDOEDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<DOEcls> obj = new List<DOEcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOEDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                DOEcls objData = new DOEcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOE = objReader["DOE"] != DBNull.Value ? Convert.ToDateTime(objReader["DOE"]) : objData.DOE = null;
                                objData.currentDOE = objReader["currentDOE"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOE"]) : objData.currentDOE = null;
                                objData.currentDOB = objReader["currentDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOB"]) : objData.currentDOB = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<DOEcls> obj = new List<DOEcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDOEDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                DOEcls objData = new DOEcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOE = objReader["DOE"] != DBNull.Value ? Convert.ToDateTime(objReader["DOE"]) : objData.DOE = null;

                                objData.revisedDOE = objReader["revisedDOE"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOE"]) : objData.revisedDOE = null;
                                objData.revisedDOB = objReader["revisedDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOB"]) : objData.revisedDOB = null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }
        }


        public async Task<string> ForwardToCheckerDOEUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOEFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDOEFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }



        #endregion

        #region DOR

        public async Task<string> InsertDORDetails(DORcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertDOR))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@DOR", DbType.DateTime, objPro.DOR);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<DORcls>> GetDORDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<DORcls> obj = new List<DORcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDORDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                DORcls objData = new DORcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOR = objReader["DOR"] != DBNull.Value ? Convert.ToDateTime(objReader["DOR"]) : objData.DOR = null;
                                objData.currentDOR = objReader["currentDOR"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOR"]) : objData.currentDOR = null;
                                objData.currentDOB = objReader["currentDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["currentDOB"]) : objData.currentDOB = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<DORcls> obj = new List<DORcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetDORDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                DORcls objData = new DORcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.DOR = objReader["DOR"] != DBNull.Value ? Convert.ToDateTime(objReader["DOR"]) : objData.DOR = null;

                                objData.revisedDOR = objReader["revisedDOR"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOR"]) : objData.revisedDOR = null;
                                objData.revisedDOB = objReader["revisedDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedDOB"]) : objData.revisedDOB = null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }
        }


        public async Task<string> ForwardToCheckerDORUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDORFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateDORFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }



        #endregion

        #region CasteCategory

        public async Task<string> InsertCasteCategoryFormDetails(CasteCategorycls objPro, string Flag)
        {
            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertCasteCategory))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@CasteCategory", DbType.String, objPro.CasteCategory);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";
            return result;
        }

        public async Task<IEnumerable<CasteCategorycls>> GetCasteCategoryFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<CasteCategorycls> obj = new List<CasteCategorycls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCasteCategoryDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                CasteCategorycls objData = new CasteCategorycls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.CasteCategory = objReader["CasteCategory"] != DBNull.Value ? Convert.ToString(objReader["CasteCategory"]) : null;
                                objData.currentCasteCategory = objReader["currentCasteCategory"] != DBNull.Value ? Convert.ToString(objReader["currentCasteCategory"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<CasteCategorycls> obj = new List<CasteCategorycls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetCasteCategoryDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                CasteCategorycls objData = new CasteCategorycls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.CasteCategory = objReader["CasteCategory"] != DBNull.Value ? Convert.ToString(objReader["CasteCategory"]) : null;
                                objData.revisedCasteCategory = objReader["revisedCasteCategory"] != DBNull.Value ? Convert.ToString(objReader["revisedCasteCategory"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerCasteCategoryFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCasteCategoryFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";
                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateCasteCategoryFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });


                return result;
            }
        }





        #endregion


        #region Entofficevehicle

        public async Task<string> InsertEntofficevehicleFormDetails(Entofficevehiclecls objPro, string Flag)
        {
            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertEntofficevehicle))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Entofficevehicle", DbType.String, objPro.entofficeVehicle);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";
            return result;
        }

        public async Task<IEnumerable<Entofficevehiclecls>> GetEntofficevehicleFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<Entofficevehiclecls> obj = new List<Entofficevehiclecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetEntofficevehicleDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                Entofficevehiclecls objData = new Entofficevehiclecls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.entofficeVehicle = objReader["entofficeVehicle"] != DBNull.Value ? Convert.ToString(objReader["entofficeVehicle"]) : objData.entofficeVehicle = null;
                                objData.currententVehicle = objReader["currententVehicle"] != DBNull.Value ? Convert.ToString(objReader["currententVehicle"]) : objData.currententVehicle = null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);


                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<Entofficevehiclecls> obj = new List<Entofficevehiclecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetEntofficevehicleDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                Entofficevehiclecls objData = new Entofficevehiclecls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.entofficeVehicle = objReader["entofficeVehicle"] != DBNull.Value ? Convert.ToString(objReader["entofficeVehicle"]) : null;
                                objData.revisedentVehicle = objReader["revisedentVehicle"] != DBNull.Value ? Convert.ToString(objReader["revisedentVehicle"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerEntofficevehicleFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateEntofficevehicleFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";
                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateEntofficevehicleFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });


                return result;
            }
        }





        #endregion


        #region BankForm

        public async Task<string> InsertBankFormDetails(BankFormcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertBank))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@IfscCode", DbType.String, objPro.ifscCode.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@SavingAcNo", DbType.String, objPro.savingAcNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@accNoof", DbType.String, !string.IsNullOrEmpty(objPro.accNoof) ? objPro.accNoof.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "10") //10 insert and 10 update
                result = "1";

            return result;
        }

        public async Task<IEnumerable<BankFormcls>> GetBankFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<BankFormcls> obj = new List<BankFormcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetBankDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                BankFormcls objData = new BankFormcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.currentifscCode = objReader["currentifscCode"] != DBNull.Value ? Convert.ToString(objReader["currentifscCode"]) : null;
                                objData.currentbankName = objReader["currentbankName"] != DBNull.Value ? Convert.ToString(objReader["currentbankName"]) : null;
                                objData.currentbranchName = objReader["currentbranchName"] != DBNull.Value ? Convert.ToString(objReader["currentbranchName"]) : null;
                                objData.currentsavingAcNo = objReader["currentsavingAcNo"] != DBNull.Value ? Convert.ToString(objReader["currentsavingAcNo"]) : null;
                                objData.currentconSavingAcNo = objReader["currentsavingAcNo"] != DBNull.Value ? Convert.ToString(objReader["currentsavingAcNo"]) : null;
                                objData.currentaccNoof = objReader["currentaccNoof"] != DBNull.Value ? Convert.ToString(objReader["currentaccNoof"]) : null;

                                objData.ifscCode = objReader["ifscCode"] != DBNull.Value ? Convert.ToString(objReader["ifscCode"]) : null;
                                objData.bankName = objReader["bankName"] != DBNull.Value ? Convert.ToString(objReader["bankName"]) : null;
                                objData.branchName = objReader["branchName"] != DBNull.Value ? Convert.ToString(objReader["branchName"]) : null;
                                objData.savingAcNo = objReader["savingAcNo"] != DBNull.Value ? Convert.ToString(objReader["savingAcNo"]) : null;
                                objData.conSavingAcNo = objReader["savingAcNo"] != DBNull.Value ? Convert.ToString(objReader["savingAcNo"]) : null;
                                objData.accNoof = objReader["accNoof"] != DBNull.Value ? Convert.ToString(objReader["accNoof"]) : null;


                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<BankFormcls> obj = new List<BankFormcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetBankDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                BankFormcls objData = new BankFormcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.revisedifscCode = objReader["revisedifscCode"] != DBNull.Value ? Convert.ToString(objReader["revisedifscCode"]) : null;
                                objData.revisedbankName = objReader["revisedbankName"] != DBNull.Value ? Convert.ToString(objReader["revisedbankName"]) : null;
                                objData.revisedbranchName = objReader["revisedbranchName"] != DBNull.Value ? Convert.ToString(objReader["revisedbranchName"]) : null;
                                objData.revisedsavingAcNo = objReader["revisedsavingAcNo"] != DBNull.Value ? Convert.ToString(objReader["revisedsavingAcNo"]) : null;
                                objData.revisedconSavingAcNo = objReader["revisedsavingAcNo"] != DBNull.Value ? Convert.ToString(objReader["revisedsavingAcNo"]) : null;
                                objData.revisedaccNoof = objReader["revisedAccNoof"] != DBNull.Value ? Convert.ToString(objReader["revisedAccNoof"]) : null;

                                objData.ifscCode = objReader["ifscCode"] != DBNull.Value ? Convert.ToString(objReader["ifscCode"]) : null;
                                objData.bankName = objReader["bankName"] != DBNull.Value ? Convert.ToString(objReader["bankName"]) : null;
                                objData.branchName = objReader["branchName"] != DBNull.Value ? Convert.ToString(objReader["branchName"]) : null;
                                objData.savingAcNo = objReader["savingAcNo"] != DBNull.Value ? Convert.ToString(objReader["savingAcNo"]) : null;
                                objData.conSavingAcNo = objReader["savingAcNo"] != DBNull.Value ? Convert.ToString(objReader["savingAcNo"]) : null;
                                objData.accNoof = objReader["accNoof"] != DBNull.Value ? Convert.ToString(objReader["accNoof"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckerBankFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateBankFwdToChkr))
                    {
                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "5")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateBankFwdToChkr))
                    {
                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "5")
                    result = "1";

                return result;
            }
        }


        #endregion



        #region addtapd

        public async Task<string> InsertaddtapdFormDetails(addtapdcls objPro, string Flag)
        {
            //if (objPro.phsidsble == "N")            {

            //    objPro.phType = null;
            //    objPro.phPcnt = null;
            //    objPro.phCertNo = null;
            //    objPro.phisSevere = null;
            //    objPro.phCertDt = null;
            //    objPro.phCertAuth = null;
            //    objPro.phentDoubleTa = null;
            //}

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_Insertaddtapd))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@phsidsble", DbType.String, objPro.phsidsble.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@phType", DbType.String, !string.IsNullOrEmpty(objPro.phType) ? objPro.phType.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@phPcnt", DbType.String, !string.IsNullOrEmpty(objPro.phPcnt) ? objPro.phPcnt.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@phCertNo", DbType.String, !string.IsNullOrEmpty(objPro.phCertNo) ? objPro.phCertNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@phisSevere", DbType.String, !string.IsNullOrEmpty(objPro.phisSevere) ? objPro.phisSevere.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@phCertDt", DbType.DateTime, objPro.phCertDt);
                    epsdatabase.AddInParameter(dbCommand, "@phCertAuth", DbType.String, !string.IsNullOrEmpty(objPro.phCertAuth) ? objPro.phCertAuth.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@phentDoubleTa", DbType.String, !string.IsNullOrEmpty(objPro.phentDoubleTa) ? objPro.phentDoubleTa.Trim() : string.Empty);

                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "9") //9 insert and 9 update
                result = "1";

            return result;
        }

        public async Task<IEnumerable<addtapdcls>> GetaddtapdFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<addtapdcls> obj = new List<addtapdcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetaddtapdDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                addtapdcls objData = new addtapdcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.currentphsidsble = objReader["currentphsidsble"] != DBNull.Value ? Convert.ToString(objReader["currentphsidsble"]) : null;
                                //objData.currentphType = objReader["currentphType"] != DBNull.Value ? Convert.ToString(objReader["currentphType"]) : null;
                                //objData.currentphPcnt = objReader["currentphPcnt"] != DBNull.Value ? Convert.ToString(objReader["currentphPcnt"]) : null;
                                //objData.currentphCertNo = objReader["currentphCertNo"] != DBNull.Value ? Convert.ToString(objReader["currentphCertNo"]) : null;
                                //objData.currentphisSevere = objReader["currentphisSevere"] != DBNull.Value ? Convert.ToString(objReader["currentphisSevere"]) : null;
                                //objData.currentphCertDt = objReader["currentphCertDt"] != DBNull.Value ? Convert.ToDateTime(objReader["currentphCertDt"]) : objData.currentphCertDt = null;
                                //objData.currentphCertAuth = objReader["currentphCertAuth"] != DBNull.Value ? Convert.ToString(objReader["currentphCertAuth"]) : null;
                                //objData.currentphentDoubleTa = objReader["currentphentDoubleTa"] != DBNull.Value ? Convert.ToString(objReader["currentphentDoubleTa"]) : null;


                                objData.phsidsble = objReader["phsidsble"] != DBNull.Value ? Convert.ToString(objReader["phsidsble"]) : null;
                                objData.phType = objReader["phType"] != DBNull.Value ? Convert.ToString(objReader["phType"]) : null;
                                objData.phPcnt = objReader["phPcnt"] != DBNull.Value ? Convert.ToString(objReader["phPcnt"]) : null;
                                objData.phCertNo = objReader["phCertNo"] != DBNull.Value ? Convert.ToString(objReader["phCertNo"]) : null;
                                objData.phisSevere = objReader["phisSevere"] != DBNull.Value ? Convert.ToString(objReader["phisSevere"]) : null;
                                objData.phCertDt = objReader["phCertDt"] != DBNull.Value ? Convert.ToDateTime(objReader["phCertDt"]) : objData.phCertDt = null;
                                objData.phCertAuth = objReader["phCertAuth"] != DBNull.Value ? Convert.ToString(objReader["phCertAuth"]) : null;
                                objData.phentDoubleTa = objReader["phentDoubleTa"] != DBNull.Value ? Convert.ToString(objReader["phentDoubleTa"]) : null;


                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<addtapdcls> obj = new List<addtapdcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetaddtapdDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                addtapdcls objData = new addtapdcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.revisedphsidsble = objReader["revisedphsidsble"] != DBNull.Value ? Convert.ToString(objReader["revisedphsidsble"]) : null;
                                //objData.revisedphType = objReader["revisedphType"] != DBNull.Value ? Convert.ToString(objReader["revisedphType"]) : null;
                                //objData.revisedphPcnt = objReader["revisedphPcnt"] != DBNull.Value ? Convert.ToString(objReader["revisedphPcnt"]) : null;
                                //objData.revisedphCertNo = objReader["revisedphCertNo"] != DBNull.Value ? Convert.ToString(objReader["revisedphCertNo"]) : null;
                                //objData.revisedphisSevere = objReader["revisedphisSevere"] != DBNull.Value ? Convert.ToString(objReader["revisedphisSevere"]) : null;
                                //objData.revisedphCertDt = objReader["revisedphCertDt"] != DBNull.Value ? Convert.ToDateTime(objReader["revisedphCertDt"]) : objData.revisedphCertDt = null;
                                //objData.revisedphCertAuth = objReader["revisedphCertAuth"] != DBNull.Value ? Convert.ToString(objReader["revisedphCertAuth"]) : null;
                                //objData.revisedphentDoubleTa = objReader["revisedphentDoubleTa"] != DBNull.Value ? Convert.ToString(objReader["revisedphentDoubleTa"]) : null;

                                objData.phsidsble = objReader["phsidsble"] != DBNull.Value ? Convert.ToString(objReader["phsidsble"]) : null;
                                objData.phType = objReader["phType"] != DBNull.Value ? Convert.ToString(objReader["phType"]) : null;
                                objData.phPcnt = objReader["phPcnt"] != DBNull.Value ? Convert.ToString(objReader["phPcnt"]) : null;
                                objData.phCertNo = objReader["phCertNo"] != DBNull.Value ? Convert.ToString(objReader["phCertNo"]) : null;
                                objData.phisSevere = objReader["phisSevere"] != DBNull.Value ? Convert.ToString(objReader["phisSevere"]) : null;
                                objData.phCertDt = objReader["phCertDt"] != DBNull.Value ? Convert.ToDateTime(objReader["phCertDt"]) : objData.phCertDt = null;
                                objData.phCertAuth = objReader["phCertAuth"] != DBNull.Value ? Convert.ToString(objReader["phCertAuth"]) : null;
                                objData.phentDoubleTa = objReader["phentDoubleTa"] != DBNull.Value ? Convert.ToString(objReader["phentDoubleTa"]) : null;

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }

        public async Task<string> ForwardToCheckeraddtapdUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateaddtapdFwdToChkr))
                    {
                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2" || result == "3")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateaddtapdFwdToChkr))
                    {
                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }


        #endregion

        #region TPTA

        public async Task<string> InsertTPTAFormDetails(TPTAcls objPro, string Flag)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertTPTA))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@TPTA", DbType.String, objPro.TPTA);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<TPTAcls>> GetTPTAFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<TPTAcls> obj = new List<TPTAcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetTPTADetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                TPTAcls objData = new TPTAcls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.TPTA = objReader["TPTA"] != DBNull.Value ? Convert.ToString(objReader["TPTA"]) : null;
                                objData.currentTPTA = objReader["currentTPTA"] != DBNull.Value ? Convert.ToString(objReader["currentTPTA"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<TPTAcls> obj = new List<TPTAcls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetTPTADetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                TPTAcls objData = new TPTAcls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;
                                objData.TPTA = objReader["TPTA"] != DBNull.Value ? Convert.ToString(objReader["TPTA"]) : null;
                                objData.revisedTPTA = objReader["revisedTPTA"] != DBNull.Value ? Convert.ToString(objReader["revisedTPTA"]) : null;
                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }


        public async Task<string> ForwardToCheckerTPTAUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateTPTAFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateTPTAFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }


        #endregion


        #region StateGIS

        public async Task<string> InsertStateGISFormDetails(StateGIScls objPro, string Flag)
        {
            string StateGISAdjustedOptionValue = string.Empty;

            if (!string.IsNullOrEmpty(objPro.StateGISAGOffice))//Agency
            {
                StateGISAdjustedOptionValue = objPro.StateGISAGOffice;
            }
            else if (!string.IsNullOrEmpty(objPro.StateGISPAOCode)) //pao
            {
                StateGISAdjustedOptionValue = objPro.StateGISPAOCode;
            }
            else if (!string.IsNullOrEmpty(objPro.StateGISOtherOffice)) //other
            {
                StateGISAdjustedOptionValue = objPro.StateGISOtherOffice;
            }

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertStateGIS))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@StateOtherGIS", DbType.String, objPro.StateOtherGIS);
                    epsdatabase.AddInParameter(dbCommand, "@StateGISAdjusted", DbType.String, !string.IsNullOrEmpty(objPro.StateGISAdjusted) ? objPro.StateGISAdjusted.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@StateGISAdjustedOptionValue", DbType.String, !string.IsNullOrEmpty(StateGISAdjustedOptionValue) ? StateGISAdjustedOptionValue.Trim() : string.Empty);

                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "4")
                result = "1";

            return result;
        }

        public async Task<IEnumerable<StateGIScls>> GetStateGISFormDetails(string empcode, int roleId, string Flag)
        {

            if (Flag == "current")
            {
                List<StateGIScls> obj = new List<StateGIScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetStateGISDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                StateGIScls objData = new StateGIScls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.currentStateOtherGIS = objReader["currentStateOtherGIS"] != DBNull.Value ? Convert.ToString(objReader["currentStateOtherGIS"]) : null;

                                objData.StateOtherGIS = objReader["StateOtherGIS"] != DBNull.Value ? Convert.ToString(objReader["StateOtherGIS"]) : null;
                                objData.StateGISAdjusted = objReader["StateGISAdjusted"] != DBNull.Value ? Convert.ToString(objReader["StateGISAdjusted"]) : null;
                                if (objData.StateGISAdjusted == "A")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISAGOffice = objData.Stategisdemo;
                                }
                                else if (objData.StateGISAdjusted == "P")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISPAOCode = objData.Stategisdemo;
                                }
                                else if (objData.StateGISAdjusted == "O")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISOtherOffice = objData.Stategisdemo;
                                }

                                //objData.StateGISAGOffice = objReader["StateGISAGOffice"] != DBNull.Value ? Convert.ToString(objReader["StateGISAGOffice"]) : null;
                                //objData.StateGISPAOCode = objReader["StateGISPAOCode"] != DBNull.Value ? Convert.ToString(objReader["StateGISPAOCode"]) : null;
                                //objData.StateGISOtherOffice = objReader["StateGISOtherOffice"] != DBNull.Value ? Convert.ToString(objReader["StateGISOtherOffice"]) : null;                                                                                          

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else if (Flag == "history")
            //else
            {
                List<StateGIScls> obj = new List<StateGIScls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetStateGISDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                StateGIScls objData = new StateGIScls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;

                                objData.revisedStateOtherGIS = objReader["revisedStateOtherGIS"] != DBNull.Value ? Convert.ToString(objReader["revisedStateOtherGIS"]) : null;

                                objData.StateOtherGIS = objReader["StateOtherGIS"] != DBNull.Value ? Convert.ToString(objReader["StateOtherGIS"]) : null;
                                objData.StateGISAdjusted = objReader["StateGISAdjusted"] != DBNull.Value ? Convert.ToString(objReader["StateGISAdjusted"]) : null;
                                //objData.StateGISAGOffice = objReader["StateGISAGOffice"] != DBNull.Value ? Convert.ToString(objReader["StateGISAGOffice"]) : null;
                                // objData.StateGISPAOCode = objReader["StateGISPAOCode"] != DBNull.Value ? Convert.ToString(objReader["StateGISPAOCode"]) : null;
                                //objData.StateGISOtherOffice = objReader["StateGISOtherOffice"] != DBNull.Value ? Convert.ToString(objReader["StateGISOtherOffice"]) : null;
                                if (objData.StateGISAdjusted == "A")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISAGOffice = objData.Stategisdemo;
                                }
                                else if (objData.StateGISAdjusted == "P")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISPAOCode = objData.Stategisdemo;
                                }
                                else if (objData.StateGISAdjusted == "O")
                                {
                                    objData.Stategisdemo = objReader["Stategisdemo"] != DBNull.Value ? Convert.ToString(objReader["Stategisdemo"]) : null;
                                    objData.StateGISOtherOffice = objData.Stategisdemo;
                                }



                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;

                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }



        }


        public async Task<string> ForwardToCheckerStateGISUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateStateGISFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateStateGISFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }


        #endregion




        #region MobileNo_Email_EmpCode

        public async Task<string> InsertMobileNoEmailEmpCodeDetails(ConMobileNoEmailEmpCodecls objPro, string flag, string Contactdetails)
        {

            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_InsertMobileNo_Email_EmpCode))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.DateTime, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());

                    epsdatabase.AddInParameter(dbCommand, "@Contactdetails", DbType.String, Contactdetails);

                    epsdatabase.AddInParameter(dbCommand, "@MobileNo", DbType.String, !string.IsNullOrEmpty(objPro.MobileNo) ? objPro.MobileNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@EmailPart1", DbType.String, !string.IsNullOrEmpty(objPro.EmailPart1) ? objPro.EmailPart1.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@EmailPart2", DbType.String, !string.IsNullOrEmpty(objPro.EmailPart2) ? objPro.EmailPart2.Trim() : string.Empty);
                     epsdatabase.AddInParameter(dbCommand, "@EmpEcode", DbType.String, !string.IsNullOrEmpty(objPro.EmpEcode) ? objPro.EmpEcode.Trim() : string.Empty);                    

                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2" || result == "3" )
                result = "1";

            return result;
        }

        public async Task<IEnumerable<ConMobileNoEmailEmpCodecls>> GetMobileNoEmailEmpCodeDetails(string empcode, int roleId, string Flag, string ContactdetailsSelectedOption)
        {

            if (Flag == "current")
            {

                List<ConMobileNoEmailEmpCodecls> obj = new List<ConMobileNoEmailEmpCodecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetMobileNo_Email_EmpCodeDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        epsdatabase.AddInParameter(dbCommand, "ContactdetailsSelectedOption", DbType.String, ContactdetailsSelectedOption);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {

                            while (objReader.Read())
                            {
                                ConMobileNoEmailEmpCodecls objData = new ConMobileNoEmailEmpCodecls();
                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;


                                if (ContactdetailsSelectedOption == "MobileNo")
                                {
                                    objData.MobileNo = objReader["MobileNo"] != DBNull.Value ? Convert.ToString(objReader["MobileNo"]) : null;
                                    objData.currentMobileNo = objReader["currentMobileNo"] != DBNull.Value ? Convert.ToString(objReader["currentMobileNo"]) : null;
                                    objData.contactDetails = ContactdetailsSelectedOption;
                                }
                                else if (ContactdetailsSelectedOption == "Email")
                                {
                                    objData.EmailPart1 = objReader["EmailPart1"] != DBNull.Value ? Convert.ToString(objReader["EmailPart1"]) : null;
                                    objData.EmailPart2 = objReader["EmailPart2"] != DBNull.Value ? Convert.ToString(objReader["EmailPart2"]) : null;
                                    objData.currentEmail = objReader["currentEmail"] != DBNull.Value ? Convert.ToString(objReader["currentEmail"]) : null;
                                    objData.contactDetails = ContactdetailsSelectedOption;
                                }

                                else if (ContactdetailsSelectedOption == "EmpEcode")
                                {
                                     objData.EmpEcode = objReader["EmpEcode"] != DBNull.Value ? Convert.ToString(objReader["EmpEcode"]) : null;
                                     objData.currentEmpEcode = objReader["currentEmpEcode"] != DBNull.Value ? Convert.ToString(objReader["currentEmpEcode"]) : null;
                                    objData.contactDetails = ContactdetailsSelectedOption;
                                }

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;





            }
            else if (Flag == "history")
            //else
            {
                List<ConMobileNoEmailEmpCodecls> obj = new List<ConMobileNoEmailEmpCodecls>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_GetMobileNo_Email_EmpCodeDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode.Trim());
                        epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                        epsdatabase.AddInParameter(dbCommand, "flag", DbType.String, Flag);
                        epsdatabase.AddInParameter(dbCommand, "ContactdetailsSelectedOption", DbType.String, ContactdetailsSelectedOption);
                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                ConMobileNoEmailEmpCodecls objData = new ConMobileNoEmailEmpCodecls();

                                objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : null;
                                objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : null;


                                if (ContactdetailsSelectedOption == "MobileNo")
                                {
                                    objData.MobileNo = objReader["MobileNo"] != DBNull.Value ? Convert.ToString(objReader["MobileNo"]) : null;
                                    objData.revisedMobileNo = objReader["revisedMobileNo"] != DBNull.Value ? Convert.ToString(objReader["revisedMobileNo"]) : null;

                                }
                                else if (ContactdetailsSelectedOption == "Email")
                                {
                                    objData.EmailPart1 = objReader["EmailPart1"] != DBNull.Value ? Convert.ToString(objReader["EmailPart1"]) : null;
                                    objData.EmailPart2 = objReader["EmailPart2"] != DBNull.Value ? Convert.ToString(objReader["EmailPart2"]) : null;

                                    objData.revisedEmail = objReader["revisedEmail"] != DBNull.Value ? Convert.ToString(objReader["revisedEmail"]) : null;
                                }

                                else if (ContactdetailsSelectedOption == "EmpEcode")
                                {
                                    objData.EmpEcode = objReader["EmpEcode"] != DBNull.Value ? Convert.ToString(objReader["EmpEcode"]) : null;
                                    objData.revisedEmpEcode = objReader["revisedEmpEcode"] != DBNull.Value ? Convert.ToString(objReader["revisedEmpEcode"]) : null;
                                }

                                objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                                objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : null;
                                objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : null;
                                objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                                objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                                obj.Add(objData);

                            }

                        }

                    }

                });
                if (obj == null)
                    return null;
                else
                    return obj;

            }
            else
            {
                return null;
            }
        }


        public async Task<string> ForwardToCheckerMobileNoEmailEmpCodeUserDtls(string empCd, string orderNo, string status, string rejectionRemark, string ContactdetailsSelectedOption)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateMobileNo_Email_EmpCodeFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        epsdatabase.AddInParameter(dbCommand, "@ContactdetailsSelectedOption", DbType.String, ContactdetailsSelectedOption);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2")
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_Change_UpdateMobileNo_Email_EmpCodeFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@ContactdetailsSelectedOption", DbType.String, ContactdetailsSelectedOption);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                return result;
            }
        }



        #endregion



    }
}
