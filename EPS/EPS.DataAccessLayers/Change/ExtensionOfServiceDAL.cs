﻿using EPS.BusinessModels.Change;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Change
{
    public class ExtensionOfServiceDAL
    {
        private Database epsdatabase = null;

        public ExtensionOfServiceDAL(Database database)
        {

            epsdatabase = database;
        }


        public async Task<string> InsertEOSDetailsDAL(ExtensionOfServiceModel1 objPro, string Flag)
        {
            
            var result = "0";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertExtensionOfServiceDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objPro.empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, objPro.OrderNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Orderdt", DbType.Date, objPro.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@FromDt", DbType.DateTime, objPro.FromDate);
                    epsdatabase.AddInParameter(dbCommand, "@Todt", DbType.DateTime, objPro.ToDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remarks", DbType.String, objPro.Remarks.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@JoiningDt", DbType.DateTime, objPro.JoiningDate);
                    epsdatabase.AddInParameter(dbCommand, "@oldOrderNo", DbType.String, !string.IsNullOrEmpty(objPro.oldOrderNo) ? objPro.oldOrderNo.Trim() : string.Empty);
                    epsdatabase.AddInParameter(dbCommand, "@LoginBy", DbType.String, objPro.username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, Flag);

                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();

                }
            });
            if (result == "2") //
                result = "1";

            return result;

        }
        //#region Get Items 

        public async Task<IEnumerable<ExtensionOfServiceModelText>> GetExtensionOfServiceDetailsDAL(string empcode, int roleId)
        {

            List<ExtensionOfServiceModelText> obj = new List<ExtensionOfServiceModelText>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetExtensionOfServiceDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empcode);
                    epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int16, roleId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            ExtensionOfServiceModelText objData = new ExtensionOfServiceModelText();
                            objData.empCd = objReader["empCd"] != DBNull.Value ? Convert.ToString(objReader["empCd"]) : objData.empCd = null;
                            objData.empName = objReader["EmpFullName"] != DBNull.Value ? Convert.ToString(objReader["EmpFullName"]) : objData.empName = null;

                            objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
                            objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
                            objData.FromDate = objReader["fromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["fromDate"]) : objData.FromDate = null;
                            objData.ToDate = objReader["toDate"] != DBNull.Value ? Convert.ToDateTime(objReader["toDate"]) : objData.ToDate = null;
                            objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
                            objData.JoiningDate = objReader["joiningDate"] != DBNull.Value ? Convert.ToDateTime(objReader["joiningDate"]) : objData.JoiningDate = null;
                            objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : objData.Status = null;
                            objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : objData.rejectionRemark = null;


                            obj.Add(objData);

                        }

                    }

                }

            });
            if (obj == null)
                return null;
            else
                return obj;


        }
                            

        public async Task<string> ForwardToCheckerUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            if (status == "V" || status == "R")
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateExtensionOfServiceFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@RejectedRemarks", DbType.String, rejectionRemark);
                        epsdatabase.AddInParameter(dbCommand, "@flag", DbType.String, status);
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });
                if (result == "2") //
                    result = "1";

                return result;
            }
            else
            {
                var result = "0";
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateExtensionOfServiceFwdToChkr))
                    {

                        epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCd.Trim());
                        epsdatabase.AddInParameter(dbCommand, "@orderNo", DbType.String, orderNo.Trim());
                        result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    }
                });

                if (result == "2") //
                    result = "1";

                return result;
            }
        }

        //public async Task<IEnumerable<ExtensionOfServiceModelText>> FetchEOSDetails(string employeeId, int pageNumber, int pageSize, string searchTerm, int roleId)
        //{

        //    List<ExtensionOfServiceModelText> obj = new List<ExtensionOfServiceModelText>();
        //    await Task.Run(() =>
        //    {
        //        using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetExtensionOfServiceSearch))
        //        {
        //            epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, employeeId.Trim());
        //            epsdatabase.AddInParameter(dbCommand, "pageSize", DbType.Int16, pageSize);
        //            epsdatabase.AddInParameter(dbCommand, "pageNumber", DbType.Int16, pageNumber);
        //            epsdatabase.AddInParameter(dbCommand, "searchTerm", DbType.String, !string.IsNullOrEmpty(searchTerm) ? searchTerm.Trim() : string.Empty);
        //            epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int16, roleId);

        //            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
        //            {

        //                while (objReader.Read())
        //                {
        //                    ExtensionOfServiceModelText objData = new ExtensionOfServiceModelText();
        //                    objData.OrderNo = objReader["orderNo"] != DBNull.Value ? Convert.ToString(objReader["orderNo"]) : objData.OrderNo = null;
        //                    objData.OrderDate = objReader["orderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["orderDate"]) : objData.OrderDate = null;
        //                    objData.FromDate = objReader["fromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["fromDate"]) : objData.FromDate = null;
        //                    objData.ToDate = objReader["toDate"] != DBNull.Value ? Convert.ToDateTime(objReader["toDate"]) : objData.ToDate = null;
        //                    objData.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : objData.Remarks = null;
        //                    objData.JoiningDate = objReader["JoiningDt"] != DBNull.Value ? Convert.ToDateTime(objReader["JoiningDt"]) : objData.JoiningDate = null;
        //                    objData.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : objData.Status = null;
        //                    objData.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]) : objData.rejectionRemark = null;


        //                    obj.Add(objData);

        //                }

        //            }

        //        }

        //    });
        //    if (obj == null)
        //        return null;
        //    else
        //        return obj;


        //}


    }
}



