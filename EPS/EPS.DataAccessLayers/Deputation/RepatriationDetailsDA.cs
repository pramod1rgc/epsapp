﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Deputation
{
    public class RepatriationDetailsDA
    {
      private  Database epsdatabase = null;

        public RepatriationDetailsDA(Database database)
        {

            epsdatabase = database;
        }

        #region  Get Repartiation Details By Employee Id
        public async Task<IEnumerable<RepatriationDetailsModel>> GetAllRepatriationDetailsByEmployee(string empCd, int roleId)
        {
            List<RepatriationDetailsModel> objmodelList = new List<RepatriationDetailsModel>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetRepatriationDetailsByEmpcode))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleid", DbType.String, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())

                        {
                            RepatriationDetailsModel objmodel = new RepatriationDetailsModel();
                            objmodel.empDeputDetailsId = objReader["EmpDeputDetailsID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpDeputDetailsID"]) : 0;
                            objmodel.repatDeputedTypeId = objReader["DeputedTypeId"] != DBNull.Value ? Convert.ToString(objReader["DeputedTypeId"]).Trim() :  null;
                            objmodel.repatServiceTypeId = objReader["Service_TypeID"] != DBNull.Value ? Convert.ToString(objReader["Service_TypeID"]).Trim() :  null;
                            objmodel.repatOrderNo = objReader["RepatOrderNo"] != DBNull.Value ? Convert.ToString(objReader["RepatOrderNo"]).Trim() :  null;
                            objmodel.repatOrderDate = objReader["RepatOrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RepatOrderDt"]) : objmodel.repatOrderDate = null;
                            objmodel.repatEffecDate = objReader["RevRepatDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RevRepatDt"]) : objmodel.repatEffecDate = null;
                            objmodel.repatRelievingDate = objReader["RelievingDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RelievingDt"]) : objmodel.repatEffecDate = null;
                            objmodel.repatRemarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]).Trim() : null;


                            objmodel.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() :  null;
                            objmodel.repatDepuTypeName = objReader["DepuTypeName"] != DBNull.Value ? Convert.ToString(objReader["DepuTypeName"]).Trim() :  null;
                            objmodel.veriFlag = objReader["DepVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["DepVerifFlag"]).Trim() :  null;
                            objmodel.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]).Trim() :  null;

                            objmodelList.Add(objmodel);
                        }
                    }

                }

            });
            return objmodelList;
        }
        #endregion
        #region  Get CheckDepuatationinEmployee By Employee Id
        public async Task<DeputationInExistEmpModel> CheckDepuatationinEmployee(string empCd, int roleId)
        {
            DeputationInExistEmpModel objmodel = new DeputationInExistEmpModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_CheckDepuatationinEmployee))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleid", DbType.String, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())

                        {
                            objmodel.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            objmodel.DeputationEffectiveDate = objReader["Effective_date"] != DBNull.Value ? Convert.ToDateTime(objReader["Effective_date"]) : objmodel.DeputationEffectiveDate = null;

                            //objmodelList.Add(objmodel);
                        }
                    }

                }

            });
            return objmodel;
        }
        #endregion
        
        #region insert for Repartiation Details
        public async Task<string> InsertUpdateRepatriationDetails(RepatriationDetailsModel objRepatriationDetails)
        {


            var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdateRepatriationDetails))
                {

                    epsdatabase.AddInParameter(dbCommand, "DeputedTypeId", DbType.Int32, objRepatriationDetails.repatDeputedTypeId);
                    epsdatabase.AddInParameter(dbCommand, "Service_TypeID", DbType.Int32, objRepatriationDetails.repatServiceTypeId);
                    epsdatabase.AddInParameter(dbCommand, "RepatOrderNo", DbType.String, objRepatriationDetails.repatOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "RepatOrderDt", DbType.DateTime, objRepatriationDetails.repatOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "Remarks", DbType.String, objRepatriationDetails.repatRemarks);
                    epsdatabase.AddInParameter(dbCommand, "RevRepatDt", DbType.DateTime, objRepatriationDetails.repatEffecDate);
                    epsdatabase.AddInParameter(dbCommand, "RelievingDt", DbType.DateTime, objRepatriationDetails.repatRelievingDate);



                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objRepatriationDetails.employeeCode);

                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objRepatriationDetails.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "createBy", DbType.String, objRepatriationDetails.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "flagUpdate", DbType.String, objRepatriationDetails.flagUpdate);
                    epsdatabase.AddInParameter(dbCommand, "empDeputDetailsId", DbType.Int32, objRepatriationDetails.empDeputDetailsId);
                    epsdatabase.AddOutParameter(dbCommand, "ERROR", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    result = result + "-" + Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ERROR")).Trim();


                }
            });
            return result;

        }
        #endregion
    }
}

