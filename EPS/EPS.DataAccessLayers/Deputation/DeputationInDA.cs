﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Deputation
{
    public class DeputationInDA
    {
       private Database epsdatabase = null;

        public DeputationInDA(Database database)
        {

            epsdatabase = database;
        }
        #region Get Pay Items 

        public async Task<IEnumerable<DeputationInModel>> GetPayItems()
        {

            List<DeputationInModel> objDeputationList = new List<DeputationInModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getPayItems))
                {
                    //epsdatabase.AddInParameter(dbCommand, "IfscCD", DbType.String, IfscCD);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            DeputationInModel objDeputationData = new DeputationInModel();
                            objDeputationData.msPayItemsId = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : objDeputationData.msPayItemsId = 0;
                            objDeputationData.payItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : objDeputationData.payItemsName = null;
                            objDeputationList.Add(objDeputationData);

                        }

                    }

                }

            });
            if (objDeputationList == null)
                return null;
            else
                return objDeputationList;


        }
        #endregion
        #region  Get All Designation

        public async Task<IEnumerable<Designation>> GetAllDesignation(string controllerId)
        {
            List<Designation> objDeputationDetailsList = new List<Designation>(); ;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDesignationAll))
                {
                    epsdatabase.AddInParameter(dbCommand, "controllerId", DbType.String, controllerId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            Designation ObjDeputationInData = new Designation();

                            ObjDeputationInData.desigId = objReader["DesigNo"] != DBNull.Value ? Convert.ToInt32(objReader["DesigNo"]) : ObjDeputationInData.desigId = 0;

                            ObjDeputationInData.desigDesc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : ObjDeputationInData.desigDesc = null;

                            objDeputationDetailsList.Add(ObjDeputationInData);

                        }

                    }

                }

            });

            return objDeputationDetailsList;
        }
        #endregion
        #region  Get Communication Address

        public async Task<IEnumerable<DeputationInModel>> GetCommunicationAddress()
        {
            List<DeputationInModel> objDeputationDetailsList = new List<DeputationInModel>(); ;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCommunicationAddress))
                {
                    //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            DeputationInModel ObjDeputationInData = new DeputationInModel();

                            ObjDeputationInData.pfId = objReader["Pfid"] != DBNull.Value ? Convert.ToInt32(objReader["Pfid"]) : ObjDeputationInData.pfId = 0;

                            ObjDeputationInData.pfAgency_Desc = objReader["PfAgency_desc"] != DBNull.Value ? Convert.ToString(objReader["PfAgency_desc"]).Trim() : ObjDeputationInData.desigDesc = null;

                            objDeputationDetailsList.Add(ObjDeputationInData);

                        }

                    }

                }

            });

            return objDeputationDetailsList;
        }
        #endregion

        #region  Get Scheme_Code  By  MsEmpDuesID

        public async Task<SchemeCode> GetScheme_CodeByMsEmpDuesID(int MsEmpDuesID)
        {
            SchemeCode objSchemeCode = new SchemeCode();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetSchemeCodeByMsEmpDuesID))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsEmpDuesID", DbType.Int32, MsEmpDuesID);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        if (objReader.Read())
                        {


                            objSchemeCode.payItemsDeductionAccCdScheme = objReader["PayItemsAccCdScheme"] != DBNull.Value ? Convert.ToString(objReader["PayItemsAccCdScheme"]).Trim() :  null;


                        }

                    }

                }

            });

            return objSchemeCode;
        }
        #endregion

        #region  Get Add Deduction and Schedule Details

        public async Task<DeputationInModel> GetDeductionScheduleDetails(string EmpCd, int roleId)
        {
            DeputationInModel objDeputationDetailsList = new DeputationInModel();
            List<DeductionSchedule> objdedschdule = new List<DeductionSchedule>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDepuatationInDetails))
                {
                    //epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "roleid", DbType.Int32, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        if (objReader.Read())
                        {
                            objDeputationDetailsList.deputation_Type = objReader["DeputedType"] != DBNull.Value ? Convert.ToString(objReader["DeputedType"]).Trim() :  null;
                            // objDeputationDetailsList.deputation_type = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : objDeputationDetailsList.deputation_type = null;
                            objDeputationDetailsList.service_Type = objReader["Service_TypeID"] != DBNull.Value ? Convert.ToString(objReader["Service_TypeID"]).Trim() :  null;
                            objDeputationDetailsList.deputation_Order = objReader["DepOrderNo"] != DBNull.Value ? Convert.ToString(objReader["DepOrderNo"]).Trim() :  null;
                            objDeputationDetailsList.deputation_Order_Date = objReader["DepOrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["DepOrderDt"]) : objDeputationDetailsList.deputation_Order_Date = null;

                            objDeputationDetailsList.deputation_From_Office = objReader["DepFromOff"] != DBNull.Value ? Convert.ToString(objReader["DepFromOff"]).Trim() : null;

                            // objDeputationDetailsList.deputation_state = objReader["Statecode"] != DBNull.Value ? Convert.ToString(objReader["Statecode"]).Trim() : objDeputationDetailsList.deputation_state = null;

                            objDeputationDetailsList.deputation_State = objReader["Statecode"] != DBNull.Value ? Convert.ToInt32(objReader["Statecode"]) :  0;
                            objDeputationDetailsList.deputation_Effective_Date = objReader["Effective_date"] != DBNull.Value ? Convert.ToDateTime(objReader["Effective_date"]) : objDeputationDetailsList.deputation_Effective_Date = null;
                            objDeputationDetailsList.deputation_Repatration_Date = objReader["RepartationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RepartationDate"]) : objDeputationDetailsList.deputation_Repatration_Date = null;
                            objDeputationDetailsList.desig_Before_Deputation = objReader["DesigBefore"] != DBNull.Value ? Convert.ToInt32(objReader["DesigBefore"]) : 0;
                            objDeputationDetailsList.desig_After_Deputation = objReader["DesigAfter"] != DBNull.Value ? Convert.ToInt32(objReader["DesigAfter"]) :  0;
                            objDeputationDetailsList.empDeputDetailsId = objReader["empDeputDetailsId"] != DBNull.Value ? Convert.ToInt32(objReader["empDeputDetailsId"]) :  0;

                            objDeputationDetailsList.deputationFlag = objReader["DepFlag"] != DBNull.Value ? Convert.ToString(objReader["DepFlag"]).Trim() :  null;
                            objDeputationDetailsList.empCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() :  null;
                            objDeputationDetailsList.deputation_Type_Name = objReader["DepuTypeName"] != DBNull.Value ? Convert.ToString(objReader["DepuTypeName"]).Trim() :  null;
                            objDeputationDetailsList.desig_Before_Remark = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]).Trim() :  null;
                            objDeputationDetailsList.veriFlag = objReader["DepVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["DepVerifFlag"]).Trim() :  null;
                            objDeputationDetailsList.rejectionRemark = objReader["ReasonOfRejection"] != DBNull.Value ? Convert.ToString(objReader["ReasonOfRejection"]) :  null;



                        }
                    }

                }

            });
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDeputationPayitemsDeduction))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, EmpCd);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            DeductionSchedule ObjDeputationInData = new DeductionSchedule();
                            ObjDeputationInData.payItemsDeductionName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : null;
                            ObjDeputationInData.payItemsCd = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : 0;
                            ObjDeputationInData.payItemsDeductionAccCdScheme = objReader["PayItemsAccCdScheme"] != DBNull.Value ? Convert.ToString(objReader["PayItemsAccCdScheme"]).Trim() :  null;
                            ObjDeputationInData.deduction_Schedule = objReader["Deduction_Schedule"] != DBNull.Value ? Convert.ToString(objReader["Deduction_Schedule"]).Trim() : null;
                            ObjDeputationInData.communication_Address = objReader["Communication_Address"] != DBNull.Value ? Convert.ToInt16(objReader["Communication_Address"]) :0;
                            ObjDeputationInData.msEmpDuesId = objReader["MsEmpDuesID"] != DBNull.Value ? Convert.ToInt32(objReader["MsEmpDuesID"]) : 0;
                            objdedschdule.Add(ObjDeputationInData);

                        }

                    }

                }

            });
            objDeputationDetailsList.Deputation_Deduction = objdedschdule;
            return objDeputationDetailsList;
        }
        #endregion
        #region insert for DeputationIn details
        public async Task<int> InsertUpdateDeputationInDetails(DeputationInModel objDeputationIn)
        {

          //  ListtoDataTable lsttodt = new ListtoDataTable();
            DataTable dt = CommonClasses.CommonFunctions.ToDataTable(objDeputationIn.Deputation_Deduction);
            //DataTable dt = lsttodt.ToDataTable(objDeputationIn.Deputation_Deduction);
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdateDeputationIn))
                {
                    SqlParameter tblpara = new SqlParameter("DeductionSchedule", dt);
                    tblpara.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(tblpara);
                    epsdatabase.AddInParameter(dbCommand, "deputation_type", DbType.Int32, objDeputationIn.deputation_Type);
                    epsdatabase.AddInParameter(dbCommand, "service_type", DbType.Int32, objDeputationIn.service_Type);
                    epsdatabase.AddInParameter(dbCommand, "deputation_order", DbType.String, objDeputationIn.deputation_Order);
                    epsdatabase.AddInParameter(dbCommand, "deputation_order_date", DbType.DateTime, objDeputationIn.deputation_Order_Date);
                    epsdatabase.AddInParameter(dbCommand, "deputation_from_office", DbType.String, objDeputationIn.deputation_From_Office);
                    epsdatabase.AddInParameter(dbCommand, "deputation_state", DbType.Int32, objDeputationIn.deputation_State);
                    epsdatabase.AddInParameter(dbCommand, "deputation_effective_date", DbType.DateTime, objDeputationIn.deputation_Effective_Date);
                    epsdatabase.AddInParameter(dbCommand, "deputation_repatration_date", DbType.DateTime, objDeputationIn.deputation_Repatration_Date);
                    epsdatabase.AddInParameter(dbCommand, "desig_before_deputation", DbType.Int32, objDeputationIn.desig_Before_Deputation);
                    epsdatabase.AddInParameter(dbCommand, "desig_after_deputation", DbType.Int32, objDeputationIn.desig_After_Deputation);
                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objDeputationIn.empCd);
                    epsdatabase.AddInParameter(dbCommand, "deputationflag", DbType.String, objDeputationIn.deputationFlag);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objDeputationIn.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "desig_Before_Remark", DbType.String, objDeputationIn.desig_Before_Remark);
                    epsdatabase.AddInParameter(dbCommand, "createdBy", DbType.String, objDeputationIn.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "DeputationInVerifyFlag", DbType.String, objDeputationIn.veriFlag);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion
        #region Update forward Status By Id
        public async Task<int> UpdateforwardStatusUpdateByDepId(int empDeputDetailsId, string status, string rejectionRemark)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DepuUpdateforwardStatusUpdateById))
                {

                    epsdatabase.AddInParameter(dbCommand, "empDeputDetailsId", DbType.Int32, empDeputDetailsId);
                    epsdatabase.AddInParameter(dbCommand, "status", DbType.String, status);
                    epsdatabase.AddInParameter(dbCommand, "rejectionRemark", DbType.String, rejectionRemark);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion
    }
}
//#region Convert List To Table
//public class ListtoDataTable
//{
//    public DataTable ToDataTable<T>(List<T> items)
//    {
//        DataTable dataTable = new DataTable(typeof(T).Name);
//        //Get all the properties by using reflection   
//        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
//        foreach (PropertyInfo prop in Props)
//        {
//            //Setting column names as Property names  
//            dataTable.Columns.Add(prop.Name);
//        }
//        foreach (T item in items)
//        {
//            var values = new object[Props.Length];
//            for (int i = 0; i < Props.Length; i++)
//            {

//                values[i] = Props[i].GetValue(item, null);
//            }
//            dataTable.Rows.Add(values);
//        }

//        return dataTable;
//    }
//}



