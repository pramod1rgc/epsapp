﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Deputation
{
    public class DeputationOutDA
    {
      private  Database epsdatabase = null;

        public DeputationOutDA(Database database)
        {

            epsdatabase = database;
        }
        #region  Get Depuatation Details By Employee Id
        public async Task<IEnumerable<DeputationOutModel>> GetAllDepuatationOutDetailsByEmployee(string EmpCd, int roleId)
        {
            List<DeputationOutModel> objDeputationDetailsList = new List<DeputationOutModel>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDeputationsOutDetailsByEmpcode))
                {

                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "roleid", DbType.Int32, roleId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            DeputationOutModel objDeputationDetails = new DeputationOutModel();
                            objDeputationDetails.empDeputDetailsId = objReader["EmpDeputDetailsID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpDeputDetailsID"]) :  0;
                            objDeputationDetails.deputedTypeId = objReader["DeputedTypeId"] != DBNull.Value ? Convert.ToString(objReader["DeputedTypeId"]).Trim() :  null;
                            // objDeputationDetailsList.deputation_type = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : objDeputationDetailsList.deputation_type = null;
                            objDeputationDetails.serviceTypeId = objReader["Service_TypeID"] != DBNull.Value ? Convert.ToString(objReader["Service_TypeID"]).Trim() :  null;
                            objDeputationDetails.depuOrderNo = objReader["DepOrderNo"] != DBNull.Value ? Convert.ToString(objReader["DepOrderNo"]).Trim() : null;
                            objDeputationDetails.depuOrderDate = objReader["DepOrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["DepOrderDt"]) : objDeputationDetails.depuOrderDate = null;

                            objDeputationDetails.depuedToOffice = objReader["DeputedToOffice"] != DBNull.Value ? Convert.ToString(objReader["DeputedToOffice"]).Trim() : null;

                            // objDeputationDetails.deputation_state = objReader["Statecode"] != DBNull.Value ? Convert.ToString(objReader["Statecode"]).Trim() : objDeputationDetails.deputation_state = null;


                            objDeputationDetails.dateOfDeputataion = objReader["DepDeployDt"] != DBNull.Value ? Convert.ToDateTime(objReader["DepDeployDt"]) : objDeputationDetails.dateOfDeputataion = null;
                            objDeputationDetails.relievingDate = objReader["RelievingDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RelievingDt"]) : objDeputationDetails.relievingDate = null;
                            objDeputationDetails.depuOnDesignation = objReader["OnDesignMastID"] != DBNull.Value ? Convert.ToInt32(objReader["OnDesignMastID"]) :  0;
                            objDeputationDetails.depuTenureYear = objReader["TenureDepuYear"] != DBNull.Value ? Convert.ToInt32(objReader["TenureDepuYear"]) : objDeputationDetails.depuTenureYear = null;
                            objDeputationDetails.depuTenureMonth = objReader["TenureDepumonth"] != DBNull.Value ? Convert.ToInt32(objReader["TenureDepumonth"]) : objDeputationDetails.depuTenureMonth = null;
                            objDeputationDetails.depuTenureDays = objReader["TenureDepudays"] != DBNull.Value ? Convert.ToInt32(objReader["TenureDepudays"]) : objDeputationDetails.depuTenureDays = null;


                            objDeputationDetails.depuFlag = objReader["DepFlag"] != DBNull.Value ? Convert.ToString(objReader["DepFlag"]).Trim() :  null;
                            objDeputationDetails.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() :  null;
                            objDeputationDetails.depuTypeName = objReader["DepuTypeName"] != DBNull.Value ? Convert.ToString(objReader["DepuTypeName"]).Trim() :  null;
                            objDeputationDetails.veriFlag = objReader["DepVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["DepVerifFlag"]).Trim() :  null;
                            objDeputationDetails.rejectionRemark = objReader["rejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["rejectionRemark"]).Trim() :  null;
                            objDeputationDetailsList.Add(objDeputationDetails);
                        }
                    }

                }

            });
            return objDeputationDetailsList;
        }
        #endregion
        #region insert for DeputationOut details
        public async Task<string> InsertUpdateDeputationOutDetails(DeputationOutModel objDeputationOut)
        {


            var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdateDeputationOutDetails))
                {

                    epsdatabase.AddInParameter(dbCommand, "DeputedTypeId", DbType.Int32, objDeputationOut.deputedTypeId);
                    epsdatabase.AddInParameter(dbCommand, "Service_TypeID", DbType.Int32, objDeputationOut.serviceTypeId);
                    epsdatabase.AddInParameter(dbCommand, "DepOrderNo", DbType.String, objDeputationOut.depuOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "DepOrderDt", DbType.DateTime, objDeputationOut.depuOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "DeputedToOffice", DbType.String, objDeputationOut.depuedToOffice);
                    epsdatabase.AddInParameter(dbCommand, "DepDeployDt", DbType.DateTime, objDeputationOut.dateOfDeputataion);
                    epsdatabase.AddInParameter(dbCommand, "RelievingDt", DbType.DateTime, objDeputationOut.relievingDate);
                    epsdatabase.AddInParameter(dbCommand, "OnDesign", DbType.Int32, objDeputationOut.depuOnDesignation);
                    epsdatabase.AddInParameter(dbCommand, "TenureDepuYear", DbType.Int32, objDeputationOut.depuTenureYear);
                    epsdatabase.AddInParameter(dbCommand, "TenureDepumonth", DbType.Int32, objDeputationOut.depuTenureMonth);
                    epsdatabase.AddInParameter(dbCommand, "TenureDepudays", DbType.Int32, objDeputationOut.depuTenureDays);
                    //epsdatabase.AddInParameter(dbCommand, "deputation_repatration_date", DbType.DateTime, objDeputationIn.deputation_repatration_date);

                    //epsdatabase.AddInParameter(dbCommand, "desig_after_deputation", DbType.Int32, objDeputationIn.desig_after_deputation);
                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objDeputationOut.employeeCode);
                    epsdatabase.AddInParameter(dbCommand, "deputationflag", DbType.String, objDeputationOut.depuFlag);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objDeputationOut.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "createBy", DbType.String, objDeputationOut.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "empDeputDetailsId", DbType.Int32, objDeputationOut.empDeputDetailsId);
                    epsdatabase.AddInParameter(dbCommand, "flagUpdate", DbType.String, objDeputationOut.flagUpdate);
                    epsdatabase.AddOutParameter(dbCommand, "ERROR", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    result = result + "-" + Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ERROR")).Trim();

                }
            });
            return result;

        }
        #endregion
    }

}