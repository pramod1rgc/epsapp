﻿using EPS.BusinessModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EPS.DataAccessLayers
{
   public class DADataAccess
    {

        string con = ConfigurationManager.ConnectionStrings["EPSConnection"].ConnectionString;


        // string con=ConfigurationManager
        public IEnumerable<DAModel> DAGet()
        {
            try
            {
                //string con = Startup.ConnectionString;
                List<DAModel> lstDAModel = new List<DAModel>();
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand("SP_DAMaster", conn);
                   
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DAModel MstDAModel = new DAModel();
                        MstDAModel.DAId = Convert.ToInt32(rdr["DAId"]);
                        MstDAModel.DAName = rdr["DAName"].ToString();
                        MstDAModel.DAAddress = rdr["DAAddress"].ToString();

                        lstDAModel.Add(MstDAModel);
                    }
                    conn.Close();
                }
                return lstDAModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DAInsert(DAModel objDAModel)
        {

        }

        public void DAUpdate()
        {

        }
        public void DADelete()
        {

        }

    }
}
