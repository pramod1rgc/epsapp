﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using EPS.BusinessModels;
using EPS.CommonClasses;

namespace EPS.DataAccessLayers
{
   public  class LoanApplicationFormDataAccessLayer
    {
        string con = ConfigurationManager.AppSettings["EPSConnection"].ToString();

        public IEnumerable<LoanApplicationDetailsModel> GetAllEmployeeNameSelectedEmpCodeandDesignationId(string EmpCode, int MsDesignID)
        {
            try
            {
                List<LoanApplicationDetailsModel> ListEmployeeModel = new List<LoanApplicationDetailsModel>();
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand(EPSConstant.SP_GetEmployeeDetails, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empcode", EmpCode);
                    cmd.Parameters.AddWithValue("@designcode", MsDesignID);
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        LoanApplicationDetailsModel ObjApplicationDetailsModel = new LoanApplicationDetailsModel();
                        ObjApplicationDetailsModel.MsEmpID = Convert.ToInt32(rdr["MsEmpID"]);
                        ObjApplicationDetailsModel.EmpCd = rdr["EmpCd"].ToString();
                        ObjApplicationDetailsModel.EmpApptType = Convert.ToString(rdr["EmpApptType"]);
                        ObjApplicationDetailsModel.EmpPfType = rdr["EmpCd"].ToString();
                        ObjApplicationDetailsModel.pay_basic = Convert.ToInt32(rdr["pay_basic"]);
                        ObjApplicationDetailsModel.EmpFullName = Convert.ToString(rdr["EmpFullName"]);
                        ObjApplicationDetailsModel.EmpSupanDt = Convert.ToDateTime(rdr["EmpSupanDt"]);
                        ObjApplicationDetailsModel.DesigDesc = Convert.ToString(rdr["DesigDesc"]);
                        ListEmployeeModel.Add(ObjApplicationDetailsModel);

                    }
                    conn.Close();
                }
                return ListEmployeeModel;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
