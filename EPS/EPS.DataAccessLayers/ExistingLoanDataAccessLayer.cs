﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
namespace EPS.DataAccessLayers
{
    public class ExistingLoanDataAccessLayer
    {
        string con = ConfigurationManager.AppSettings["EPSConnection"].ToString();
        public IEnumerable<tblMsPayLoanRef> BindLoanType()
        {
            try
            {

                List<tblMsPayLoanRef> lstDesign = new List<tblMsPayLoanRef>();
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@mode", '4');
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        tblMsPayLoanRef MstDesignation = new tblMsPayLoanRef();
                        MstDesignation.PayLoanRefLoanCD = Convert.ToInt32(rdr["PayLoanRefLoanCD"]);
                        MstDesignation.PayLoanRefLoanShortDesc = Convert.ToString(rdr["PayLoanRefLoanShortDesc"]);
                        lstDesign.Add(MstDesignation);
                    }
                    conn.Close();
                }
                return lstDesign;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<MstDesignLoanModel> BindDropDownDesign()
        {
            try
            {
                // string con = Startup.ConnectionString;
                List<MstDesignLoanModel> lstDesign = new List<MstDesignLoanModel>();
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand(EPSConstant.SP_GetEmpNamebyDesignId, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@mode", '2');
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        MstDesignLoanModel MstDesignation = new MstDesignLoanModel();
                        MstDesignation.MsDesigMastID = Convert.ToInt32(rdr["MsDesigMastID"]);
                        MstDesignation.DesigDesc = rdr["DesigDesc"].ToString();                      
                        lstDesign.Add(MstDesignation);
                    }
                    conn.Close();
                }
                return lstDesign;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<TblMsPayBillGroup> BindDropDownBillgroup ()
        {
            try
            {
             
                List<TblMsPayBillGroup> lstBillGroup = new List<TblMsPayBillGroup>();
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand(EPSConstant.SP_GetEmpNamebyDesignId, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@mode", '3');
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        TblMsPayBillGroup MstBill = new TblMsPayBillGroup();
                        MstBill.BillgrId = Convert.ToInt32(rdr["BillgrId"]);
                        MstBill.BillgrDesc = rdr["BillgrDesc"].ToString();
                        lstBillGroup.Add(MstBill);
                    }
                    conn.Close();
                }
                return lstBillGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<MsEmpLoanModel> GetAllEmployeeNameSelectedDesignationId(string MsDesignID)
        {
            try
            {
                List<MsEmpLoanModel> ListEmployeeModel = new List<MsEmpLoanModel>();
                 using (SqlConnection conn = new SqlConnection(con))
                    {
                        SqlCommand cmd = new SqlCommand(EPSConstant.SP_GetEmpNamebyDesignId, conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@mode", '1');
                        cmd.Parameters.AddWithValue("@Designid", MsDesignID);
                        conn.Open();
                        SqlDataReader rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                           MsEmpLoanModel ObjEmployeeModel = new MsEmpLoanModel();
                             //ObjEmployeeModel.EmpDesigCd = Convert.ToInt32(rdr["EmpDesigCd"]);
                            ObjEmployeeModel.EmpFirstName = rdr["EmpName"].ToString();
                            //ObjEmployeeModel.MsEmpID = Convert.ToInt32(rdr["MsEmpID"].ToString());  
                            ObjEmployeeModel.EmpCd = Convert.ToString(rdr["EmpCd"].ToString());

                        ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                        conn.Close();
                    }
                    return ListEmployeeModel;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
