﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;

namespace EPS.DataAccessLayers.Increment
{
    public class AnnualIncrement_DAL
    {
        DbCommand dbCommand = null;
        Database epsdatabase = null;
        public AnnualIncrement_DAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get Salary Month
        public IEnumerable<AdvIncrement_Model> GetSalaryMonth()
        {
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetSalaryMonth)) 
            {
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        AdvIncrement_Model AdvModel = new AdvIncrement_Model();
                        AdvModel.IncID = Convert.ToInt32(objReader["ID"]);
                        AdvModel.SalaryMonth = objReader["SalaryMonth"].ToString();
                        list.Add(AdvModel);
                    }
                }
            }
            return list;
        }

        #endregion 


        #region Get Employee With Order No
        /// <summary>
        ///  Get Employee With Order No
        /// </summary>
        public IEnumerable<AdvIncrement_Model> GetOrderWithEmployee(string paycodeID)
        {
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetOrderWithEmployeeReport);  
            epsdatabase.AddInParameter(dbCommand, "@paycodeID", DbType.String, paycodeID);
            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    AdvIncrement_Model advinc_model = new AdvIncrement_Model();
                    advinc_model.OrderNo = objReader["OrderNo"].ToString();
                    advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                    //advinc_model.PayLevel = objReader["PayCode"].ToString();
                    advinc_model.Status = objReader["PayStatus"].ToString();
                    advinc_model.NoofEmployee = Convert.ToInt32(objReader["TotalEmp"].ToString());
                    advinc_model.ID = Convert.ToInt32(objReader["MsPayOrderID"]);
                    list.Add(advinc_model);
                }
            }
            return list;
        }
        #endregion

        #region Get Employee Report data
        /// <summary>
        ///  Get Employee Report data
        /// </summary>
        public IEnumerable<AdvIncrement_Model> GetAnnualIncReportData(string paycodeID, string orderID)
        {
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetAnnualIncReportData);  
            epsdatabase.AddInParameter(dbCommand, "@paycodeID", DbType.String, paycodeID);
            epsdatabase.AddInParameter(dbCommand, "@orderID", DbType.String, orderID);
            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    AdvIncrement_Model advinc_model = new AdvIncrement_Model();
                    advinc_model.OrderNo = objReader["OrderNo"].ToString();
                    advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                    advinc_model.EmpCode = objReader["EmpCd"].ToString();
                    advinc_model.OldBasic = Convert.ToInt32(objReader["BasicPay"]);
                    advinc_model.EmpName = objReader["EmpName"].ToString();
                    advinc_model.NextIncDate = Convert.ToDateTime(objReader["IncDate"]);
                    advinc_model.DesigCode = objReader["DesigDesc"].ToString();
                    advinc_model.serialNo = Convert.ToInt32(objReader["serialNo"]);
                    list.Add(advinc_model);
                }
            }
            return list;
        }
        #endregion



        #region Get Employee due for increment report data
        /// <summary>
        ///  Get Employee due for increment report
        /// </summary>
        public IEnumerable<AdvIncrement_Model> GetEmpDueForIncReport(string designCode)
        {
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpDueForIncReport);  
            epsdatabase.AddInParameter(dbCommand, "@DesignCode", DbType.String, designCode);
            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    AdvIncrement_Model advinc_model = new AdvIncrement_Model();
                    advinc_model.serialNo = Convert.ToInt32(objReader["SerialNo"]);
                    advinc_model.EmpCode = objReader["EmpCd"].ToString();
                    advinc_model.OldBasic = Convert.ToInt32(objReader["BasicPay"]);
                    advinc_model.EmpName = objReader["EmpName"].ToString();
                    advinc_model.NextIncDate = Convert.ToDateTime(objReader["IncrementDate"]);
                    advinc_model.DesigCode = objReader["DesigDesc"].ToString();
                    list.Add(advinc_model);
                }
            }
            return list;
        }
        #endregion

    }
}
