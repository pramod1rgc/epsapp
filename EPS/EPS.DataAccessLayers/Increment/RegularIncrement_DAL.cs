﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace EPS.DataAccessLayers.Increment
{
    public class RegularIncrement_DAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;
        public RegularIncrement_DAL(Database database)
        {
            epsdatabase = database;
        }

        /// <summary>
        /// Get Employee Designation
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        public async Task<List<RegIncrement_Model>> GetEmployeeDesignation(string PermDdoId)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmployeeDesigByPermDDOId);
                epsdatabase.AddInParameter(dbCommand, "@PermDdoId", DbType.String, PermDdoId);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RegIncrement_Model advinc_model = new RegIncrement_Model();
                        advinc_model.MsDesigMastID = objReader["DesigID"] != DBNull.Value ? Convert.ToInt32(objReader["DesigID"]) : advinc_model.MsDesigMastID = 0;
                        advinc_model.DesigDesc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : advinc_model.DesigDesc = null;
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }



        #region Create Order Number
        /// <summary>
        ///  Create Order Number
        /// </summary>
        /// <returns></returns>
        public async Task<string> CreateOrderNo(RegIncrement_Model advModel)
        {
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_CreateOrderNo))
                {
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, advModel.ID);
                    epsdatabase.AddInParameter(dbCommand, "@PayOrderNo", DbType.String, advModel.OrderNo);
                    epsdatabase.AddInParameter(dbCommand, "@PayOrderDate", DbType.DateTime, advModel.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@DesignCode", DbType.String, advModel.DesigCode);
                    epsdatabase.AddInParameter(dbCommand, "@PayCode", DbType.String, advModel.PayLevel);
                    epsdatabase.AddInParameter(dbCommand, "@OrderType", DbType.String, advModel.OrderType);
                    epsdatabase.AddInParameter(dbCommand, "@PayStatus", DbType.String, "1");
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, advModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, advModel.loginUser);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0)
                    {
                        Response = EPSResource.SaveSuccessMessage;
                    }
                    else
                    { Response = EPSResource.AlreadyExistMessage; }
                }
            });
            return Response;
        }

        /// <summary>
        /// Delete order details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> deleteOrderDetails(int id,string type)
        {
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteIncOrderDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsPayOrderID", DbType.Int32, id);
                    epsdatabase.AddInParameter(dbCommand, "@OrderType", DbType.String, type.Trim());
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0)
                    {
                        Response = EPSResource.DeleteSuccessMessage;
                    }
                    else
                    { Response = EPSResource.DeleteFailedMessage; }
                }
            });
            return Response;
        }



        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        #endregion

        #region Get Employee With Order No
        /// <summary>
        ///  Get Employee With Order No
        /// </summary>
        public async Task<List<RegIncrement_Model>> GetOrderDetails(string designID, string paycodeID, string orderType)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpOrderDetails);
                epsdatabase.AddInParameter(dbCommand, "@designID", DbType.String, designID);
                epsdatabase.AddInParameter(dbCommand, "@paycodeID", DbType.String, paycodeID);
                epsdatabase.AddInParameter(dbCommand, "@OrderType", DbType.String, orderType);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RegIncrement_Model advinc_model = new RegIncrement_Model();
                        advinc_model.OrderNo = objReader["PayOrderNo"].ToString();
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                        advinc_model.PayLevel = objReader["PayCode"].ToString();
                        advinc_model.Status = objReader["PayStatus"].ToString();
                        advinc_model.NoofEmployee = Convert.ToInt32(objReader["TotalEmp"].ToString());
                        advinc_model.ID = Convert.ToInt32(objReader["MsPayOrderID"]);
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Get Employee For Increment On behalf of Order No
        /// <summary>
        /// Get Employee For Increment On behalf of Order No
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="salarymonth"></param>
        /// <returns></returns>
        public IEnumerable<RegIncrement_Model> GetEmployeeForIncrement(string designID, string paycodeID, Int32 orderID)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpForIncrement);
            epsdatabase.AddInParameter(dbCommand, "@designID", DbType.String, designID);
            epsdatabase.AddInParameter(dbCommand, "@paycodeID", DbType.String, paycodeID);
            epsdatabase.AddInParameter(dbCommand, "@PayOrderID", DbType.Int32, orderID);
            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    RegIncrement_Model advinc_model = new RegIncrement_Model();
                    advinc_model.ID = Convert.ToInt32(objReader["ID"].ToString());
                    advinc_model.EmpName = objReader["EmpName"].ToString();
                    advinc_model.EmpID = Convert.ToInt32(objReader["MsEmpID"].ToString());
                    advinc_model.EmpCode = objReader["EmpCd"].ToString();
                    advinc_model.OrderNo = objReader["PayOrderNo"].ToString();
                    advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                    advinc_model.PayLevel = objReader["PayCode"].ToString();
                    advinc_model.Desigination = objReader["DesigDesc"].ToString();
                    advinc_model.NewBasic = Convert.ToDecimal(objReader["NewBasicPay"]);
                    advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"]);
                    advinc_model.WefDate = Convert.ToDateTime(objReader["WefDt"]);
                    advinc_model.NextIncDate = Convert.ToDateTime(objReader["NextIncrementDate"]);
                    advinc_model.Remark = objReader["Remarks"].ToString();
                    advinc_model.Status = objReader["VerifFlag"].ToString();
                    list.Add(advinc_model);
                }
            }
            return list;
        }
        #endregion

        #region Get Employee who is not the part of Increment On behalf of Order No
        /// <summary>
        /// Get Employee who is not the part of Increment On behalf of Order No
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="salarymonth"></param>
        /// <returns></returns>
        public async Task<List<RegIncrement_Model>> GetEmployeeForNonIncrement(string designID, string paycodeID, Int32 orderID)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpForNotIncrement);
                epsdatabase.AddInParameter(dbCommand, "@designID", DbType.String, designID);
                epsdatabase.AddInParameter(dbCommand, "@paycodeID", DbType.String, paycodeID);
                epsdatabase.AddInParameter(dbCommand, "@PayOrderID", DbType.Int32, orderID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RegIncrement_Model advinc_model = new RegIncrement_Model();
                        advinc_model.EmpName = objReader["EmpName"].ToString();
                        advinc_model.EmpID = Convert.ToInt32(objReader["MsEmpID"].ToString());
                        advinc_model.EmpCode = objReader["EmpCd"].ToString();
                        advinc_model.OrderNo = objReader["PayOrderNo"].ToString();
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                        advinc_model.PayLevel = objReader["PayCode"].ToString();
                        advinc_model.Desigination = objReader["DesigDesc"].ToString();
                        advinc_model.NewBasic = Convert.ToDecimal(objReader["NewBasicPay"]);
                        advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"]);
                       // advinc_model.WefDate = Convert.ToDateTime(objReader["WefDate"]);
                        advinc_model.NextIncDate = Convert.ToDateTime(objReader["NextIncrementDate"]);
                        ///advinc_model.Remark = objReader["Remarks"].ToString();
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Insert Regular Increment Record
        /// <summary>
        ///  Insert Regular Increment Record
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>

        public async Task<string> InsertRegIncrementData(object[] advObj)
        {
            string message = string.Empty;
            string myIP = GetIP();
            DateTime WefDate;
            DateTime OrderDate;
            await Task.Run(() =>
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[13] {
                new DataColumn("ID", typeof(int)),
                new DataColumn("EmpID", typeof(int)),
                 new DataColumn("EmpCode", typeof(string)),
                 new DataColumn("PayLevel", typeof(string)),
                 new DataColumn("WefDate", typeof(DateTime)),
                  new DataColumn("Remark", typeof(string)),
                    new DataColumn("OldBasic", typeof(decimal)),
                    new DataColumn("NewBasic", typeof(decimal)),
                    new DataColumn("CreatedBy", typeof(string)),
                    new DataColumn("DesigCode", typeof(string)),
                new DataColumn("CreatedIP", typeof(string)),
                    new DataColumn("OrderNo", typeof(string)),
                    new DataColumn("OrderDate", typeof(DateTime)) });
                for (int i = 0; i < advObj.Length; i++)
                {
                    string menuname = advObj[i].ToString();
                    string result = Regex.Replace(menuname, @"[^\w]", ", ");
                    dynamic item = JsonConvert.DeserializeObject<object>(advObj[i].ToString());
                    int ID = i + 1;
                    string EmpID = item[0];
                    string EmpCode = item[1];
                    string PayLevel = item[2];
                    if (Convert.ToString(item[3]) == string.Empty)
                        WefDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                    else
                        WefDate = Convert.ToDateTime(item[3]);
                    string Remark = item[4];
                    decimal OldBasic = Convert.ToDecimal(item[5]);
                    decimal NewBasic = Convert.ToDecimal(item[6]);
                    string CreatedBy = item[7];
                    string DesigCode = item[8];
                    string OrderNo = item[9];
                    if (Convert.ToString(item[10]) == string.Empty)
                        OrderDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                    else
                        OrderDate = Convert.ToDateTime(item[10]);
                    dt.Rows.Add(ID, EmpID, EmpCode, PayLevel, WefDate, Remark, OldBasic, NewBasic, CreatedBy, DesigCode, myIP, OrderNo, OrderDate);
                }
                if (dt.Rows.Count > 0)
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_InsertEmpRegularIncrement))
                    {
                        SqlParameter tblpara = new SqlParameter("EmpIncrementData", dt);
                        tblpara.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(tblpara);
                        int i = epsdatabase.ExecuteNonQuery(dbCommand);
                        if (i < 0)
                        {
                            message = EPSResource.SaveSuccessMessage;
                        }
                        else
                        { message = EPSResource.UpdateSuccessMessage; }
                    }
                }
            });
            return message;
        }

        #endregion

        /// <summary>
        /// Forward to checker For Incement
        /// </summary>
        /// <param name="advObj"></param>
        /// <returns></returns>
        public async Task<string> ForwardtocheckerForInc(object[] advObj)
        {
            string message = string.Empty;
            string myIP = GetIP();
            await Task.Run(() =>
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[4] { new DataColumn("ID", typeof(int)),new DataColumn("EmpID", typeof(int)), new DataColumn("CreatedBy", typeof(string)),
                                                    new DataColumn("CreatedIP", typeof(string))});
                for (int i = 0; i < advObj.Length; i++)
                {
                    string menuname = advObj[i].ToString();
                    string result = Regex.Replace(menuname, @"[^\w]", ", ");
                    dynamic item = JsonConvert.DeserializeObject<object>(advObj[i].ToString());
                    int ID = item[0];
                    string EmpID = item[1];
                    string CreatedBy = item[2];
                    dt.Rows.Add(ID, EmpID, CreatedBy, myIP);
                }
                if (dt.Rows.Count > 0)
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ForwardEmpRegularIncData))
                    {
                        SqlParameter tblpara = new SqlParameter("EmpIncDataForwarded", dt);
                        tblpara.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(tblpara);
                        int i = epsdatabase.ExecuteNonQuery(dbCommand);
                        if (i < 0)
                        {
                            message = EPSResource.ForwardCheckerMessage;
                        }
                        else
                        { message = EPSResource.ForwardCheckerMessage; }
                    }
                }
            });
            return message;
        }


    }
}
