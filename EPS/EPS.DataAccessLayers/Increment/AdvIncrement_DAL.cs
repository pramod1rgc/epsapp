﻿using EPS.BusinessModels.Increment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using EPS.CommonClasses;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Increment
{
    public class AdvIncrement_DAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;
        public AdvIncrement_DAL(Database database)
        {
            epsdatabase = database;
        }

        #region Insert And Update Advance Increment Record
        /// <summary>
        /// Insert And Update Advance Increment Record
        /// </summary>
        /// <param name="advModel"></param>
        /// <returns></returns>
        public async Task<string> CreateAdvIncrement(AdvIncrement_Model advModel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_InsertUpdateAdvIncrement))
                {
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, advModel.IncID);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, advModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "@NoofIncrement", DbType.Int32, advModel.NoofIncrement);
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, advModel.OrderNo);
                    epsdatabase.AddInParameter(dbCommand, "@OrderDate", DbType.DateTime, advModel.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@WefDate", DbType.DateTime, advModel.WefDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remark", DbType.String, advModel.Remark);
                    epsdatabase.AddInParameter(dbCommand, "@OrderType", DbType.String, "Advance Increment");
                    epsdatabase.AddInParameter(dbCommand, "@IPAddress", DbType.String, advModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, advModel.loginUser);
                    epsdatabase.AddInParameter(dbCommand, "@ChangeSubType", DbType.String, advModel.OrderTypeID);
                    epsdatabase.AddInParameter(dbCommand, "@PayComm", DbType.String, advModel.PayLevel);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 && advModel.IncID > 0)
                    {
                        Response = EPSResource.UpdateSuccessMessage;
                    }
                    else if (i > 0)
                    {
                        Response = EPSResource.SaveSuccessMessage;
                    }
                    else
                    { Response = EPSResource.AlreadyExistMessage; }
                }
            });
            return Response;
        }


        #endregion

        #region Get Advance Increment data
        /// <summary>
        ///  Get Advance Increment data
        /// </summary>
        public async Task<List<AdvIncrement_Model>> GetIncrementDetails(string empcode)
        {
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            AdvIncrement_Model advinc_model = new AdvIncrement_Model();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetAdvIncrementDetails);  
                epsdatabase.AddInParameter(dbCommand, "@empcode", DbType.String, empcode);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        advinc_model.IncID = Convert.ToInt32(objReader["IncID"]);
                        advinc_model.OrderNo = objReader["OrderNo"].ToString();
                        advinc_model.NoofIncrement = Convert.ToInt32(objReader["NoofIncrement"]);
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["OrdeRdt"]);
                        advinc_model.OrderType = objReader["ChangeType"].ToString();
                        advinc_model.Remark = objReader["Remarks"].ToString();
                        advinc_model.WefDate = Convert.ToDateTime(objReader["WefDt"].ToString());
                        advinc_model.PayLevel = objReader["PayCommDesc"].ToString();
                        advinc_model.EmpName = objReader["EmpName"].ToString();
                        advinc_model.OldWefDate = Convert.ToDateTime(objReader["DateLastIncrement"].ToString());
                        advinc_model.NextIncDate = Convert.ToDateTime(objReader["DateNextIncrement"].ToString());
                        advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"].ToString());
                        advinc_model.EmpCode = objReader["EmpCd"].ToString();
                        advinc_model.OrderTypeID = Convert.ToInt32(objReader["ChangeSubType"]);
                        advinc_model.Flag = objReader["VerifFlag"].ToString();
                        advinc_model.Status = objReader["Status"].ToString();
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }
        #endregion


        #region Get Employee  by DesignationId
        /// <summary>
        /// Get Employee  by Designation Id
        /// </summary>
        /// <param name="msDesignID"></param>
        /// <param name="paycomm"></param>
        /// <returns></returns>
        public async Task<List<AdvIncrement_Model>> GetEmployeeByDesigPayComm(string msDesignID, string paycomm)
        {
            List<AdvIncrement_Model> objEmployeeList = new List<AdvIncrement_Model>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpNamebyDesignIdPayComm))
                {
                    epsdatabase.AddInParameter(dbCommand, "Designid", DbType.String, msDesignID);
                    epsdatabase.AddInParameter(dbCommand, "PayComm", DbType.String, paycomm);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            AdvIncrement_Model objEmployeeData = new AdvIncrement_Model();
                            objEmployeeData.EmpCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]) : objEmployeeData.EmpCode = "0";
                            objEmployeeData.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]).Trim() : objEmployeeData.EmpName = null;
                            objEmployeeList.Add(objEmployeeData);
                        }
                    }
                }
            });
            if (objEmployeeList == null)
                return null;
            else
                return objEmployeeList;
        }
        #endregion



        #region Edit Advance Increment Details
        /// <summary>
        /// Get advance increment by id for update
        /// </summary>
        /// <param name="incID"></param>
        /// <returns></returns>
        public async Task<List<AdvIncrement_Model>> EditAdvIncrement(int incID)
        {

            AdvIncrement_Model advinc_model = new AdvIncrement_Model();
            List<AdvIncrement_Model> list = new List<AdvIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_EditAdvIncrementDetails);  
                epsdatabase.AddInParameter(dbCommand, "@incID", DbType.Int32, incID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        advinc_model.IncID = Convert.ToInt32(objReader["IncID"]);
                        advinc_model.OrderNo = objReader["OrderNo"].ToString();
                        advinc_model.NoofIncrement = Convert.ToInt32(objReader["NoofIncrement"]);
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["OrdeRdt"]);
                        advinc_model.OrderType = objReader["ChangeType"].ToString();
                        advinc_model.Remark = objReader["Remarks"].ToString();
                        advinc_model.WefDate = Convert.ToDateTime(objReader["WefDt"].ToString());
                        advinc_model.PayLevel = objReader["PayCommDesc"].ToString();
                        advinc_model.EmpName = objReader["EmpName"].ToString();
                        advinc_model.OldWefDate = Convert.ToDateTime(objReader["DateLastIncrement"].ToString());
                        advinc_model.NextIncDate = Convert.ToDateTime(objReader["DateNextIncrement"].ToString());
                        advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"].ToString());
                        advinc_model.EmpCode = objReader["EmpCd"].ToString();
                        advinc_model.OrderTypeID = Convert.ToInt32(objReader["ChangeSubType"]);
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }

        #endregion     

        #region Delete Advance Increment Data
        /// <summary>
        /// Delete Advance Increment Data
        /// </summary>
        /// <param name="IncID"></param>
        /// <returns></returns>
        public async Task<string> DeleteAdvDetails(int incID)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteAdvIncrement);  
                epsdatabase.AddInParameter(dbCommand, "@incID", DbType.Int32, incID);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage; ;
                }
                else { Response = EPSResource.DeleteFailedMessage; ; }
            });
            return Response;
        }
        #endregion


        #region Forward to checker Advance Increment Record
        /// <summary>
        ///  Forward to checker Advance Increment Record
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>
        public async Task<string> Forwardtochecker(AdvIncrement_Model advModel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ForwardtoChkAdvIncrement)) 
                {
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, advModel.IncID);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, advModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "@NoofIncrement", DbType.Int32, advModel.NoofIncrement);
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, advModel.OrderNo);
                    epsdatabase.AddInParameter(dbCommand, "@OrderDate", DbType.DateTime, advModel.OrderDate);
                    epsdatabase.AddInParameter(dbCommand, "@WefDate", DbType.DateTime, advModel.WefDate);
                    epsdatabase.AddInParameter(dbCommand, "@Remark", DbType.String, advModel.Remark);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, advModel.loginUser);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 && advModel.IncID > 0)
                    {
                        Response = EPSResource.ForwardCheckerMessage;
                    }
                    else
                    { Response = EPSResource.AlreadyForwardMessage; }
                }
            });
            return Response;
        }
        #endregion

    }
}
