﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Increment
{
    public class StopIncrement_DAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        public StopIncrement_DAL(Database database)
        {
            epsdatabase = database;
        }
        #region Get Employee For Stop Increment On behalf of Order No
        /// <summary>
        /// Get Employee For Stop Increment On behalf of Order No
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        public async Task<List<RegIncrement_Model>> GetEmployeeStopIncrement(Int32 orderID)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpForStopIncrement);  
                epsdatabase.AddInParameter(dbCommand, "@PayOrderID", DbType.Int32, orderID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RegIncrement_Model advinc_model = new RegIncrement_Model();
                        advinc_model.ID = Convert.ToInt32(objReader["ID"].ToString());
                        advinc_model.EmpName = objReader["EmpName"].ToString();
                        advinc_model.EmpID = Convert.ToInt32(objReader["MsEmpID"].ToString());
                        advinc_model.EmpCode = objReader["EmpCd"].ToString();
                        advinc_model.OrderNo = objReader["PayOrderNo"].ToString();
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                        advinc_model.PayLevel = objReader["PayCode"].ToString();
                        advinc_model.Desigination = objReader["DesigDesc"].ToString();
                        advinc_model.NewBasic = Convert.ToDecimal(objReader["NewBasicPay"]);
                        advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"]);
                        advinc_model.WefDate = Convert.ToDateTime(objReader["WefDt"]);
                        advinc_model.NextIncDate = Convert.ToDateTime(objReader["NextIncrementDate"]);
                        advinc_model.Remark = objReader["Remarks"].ToString();
                        advinc_model.Status = objReader["VerifFlag"].ToString();
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }
        #endregion


        #region Get Employee who is not the part of Increment On behalf of Order No
        /// <summary>
        /// Get Employee who is not the part of Increment On behalf of Order No
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="salarymonth"></param>
        /// <returns></returns>
        public async Task<List<RegIncrement_Model>> GetEmployeeForNotStop(Int32 orderID)
        {
            List<RegIncrement_Model> list = new List<RegIncrement_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetEmpForNotStopIncrement);  
                epsdatabase.AddInParameter(dbCommand, "@PayOrderID", DbType.Int32, orderID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RegIncrement_Model advinc_model = new RegIncrement_Model();
                        advinc_model.EmpName = objReader["EmpName"].ToString();
                        advinc_model.EmpID = Convert.ToInt32(objReader["MsEmpID"].ToString());
                        advinc_model.EmpCode = objReader["EmpCd"].ToString();
                        advinc_model.OrderNo = objReader["PayOrderNo"].ToString();
                        advinc_model.OrderDate = Convert.ToDateTime(objReader["PayOrderDate"]);
                        advinc_model.PayLevel = objReader["PayCode"].ToString();
                        advinc_model.Desigination = objReader["DesigDesc"].ToString();
                        advinc_model.NewBasic = Convert.ToDecimal(objReader["NewBasicPay"]);
                        advinc_model.OldBasic = Convert.ToDecimal(objReader["BasicPay"]);
                        //advinc_model.WefDate = Convert.ToDateTime(objReader["WefDt"]);
                        advinc_model.NextIncDate = Convert.ToDateTime(objReader["NextIncrementDate"]);
                        ///advinc_model.Remark = objReader["Remarks"].ToString();
                        list.Add(advinc_model);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Insert Stop Increment Record
        /// <summary>
        ///  Insert Regular Increment Record
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>

        public async Task<string> InsertStopIncrementData(object[] advObj)
        {
            clientIP = CommonFunctions.GetClientIP(_accessor);
            string message = string.Empty;
            DateTime WefDate;
            DateTime OrderDate;
            await Task.Run(() =>
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[13] { new DataColumn("ID", typeof(int)),new DataColumn("EmpID", typeof(int)), new DataColumn("EmpCode", typeof(string)), new DataColumn("PayLevel", typeof(string)), new DataColumn("WefDate", typeof(DateTime)),
                new DataColumn("Remark", typeof(string)), new DataColumn("OldBasic", typeof(decimal)),new DataColumn("NewBasic", typeof(decimal)),new DataColumn("CreatedBy", typeof(string)),new DataColumn("DesigCode", typeof(string)),
                new DataColumn("CreatedIP", typeof(string)),new DataColumn("OrderNo", typeof(string)),new DataColumn("OrderDate", typeof(DateTime)) });
                for (int i = 0; i < advObj.Length; i++)
                {
                    string menuname = advObj[i].ToString();
                    string result = Regex.Replace(menuname, @"[^\w]", ", ");
                    dynamic item = JsonConvert.DeserializeObject<object>(advObj[i].ToString());
                    int ID = i + 1;
                    string EmpID = item[0];
                    string EmpCode = item[1];
                    string PayLevel = item[2];
                    if (Convert.ToString(item[3]) == string.Empty)
                        WefDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                    else
                        WefDate = Convert.ToDateTime(item[3]);
                    string Remark = item[4];
                    decimal OldBasic = Convert.ToDecimal(item[5]);
                    decimal NewBasic = Convert.ToDecimal(item[5]);
                    string CreatedBy = item[6];
                    string DesigCode = "0";
                    string OrderNo = item[7];
                    if (Convert.ToString(item[8]) == string.Empty)
                        OrderDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                    else
                        OrderDate = Convert.ToDateTime(item[8]);
                    dt.Rows.Add(ID, EmpID, EmpCode, PayLevel, WefDate, Remark, OldBasic, NewBasic, CreatedBy, DesigCode, clientIP, OrderNo, OrderDate);
                }
                if (dt.Rows.Count > 0)
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_InsertEmployeeStopIncrement)) 
                    {
                        SqlParameter tblpara = new SqlParameter("EmpIncrementData", dt);
                        tblpara.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(tblpara);
                        int i = epsdatabase.ExecuteNonQuery(dbCommand);
                        if (i < 0)
                        {
                            message = EPSResource.SaveSuccessMessage;
                        }
                        else
                        {
                            message = EPSResource.UpdateSuccessMessage;
                        }
                    }
                }
            });
            return message;
        }


        #endregion





        public async Task<string> ForwardcheckerForStopInc(object[] advObj)
        {
            clientIP = CommonFunctions.GetClientIP(_accessor);
            string message = string.Empty;
            await Task.Run(() =>
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[4] { new DataColumn("ID", typeof(int)),new DataColumn("EmpID", typeof(int)), new DataColumn("CreatedBy", typeof(string)),
                                                    new DataColumn("CreatedIP", typeof(string))});
                for (int i = 0; i < advObj.Length; i++)
                {
                    string menuname = advObj[i].ToString();
                    string result = Regex.Replace(menuname, @"[^\w]", ", ");
                    dynamic item = JsonConvert.DeserializeObject<object>(advObj[i].ToString());
                    int ID = item[0];
                    string EmpID = item[1];
                    string CreatedBy = item[2];
                    dt.Rows.Add(ID, EmpID, CreatedBy, clientIP);
                }
                if (dt.Rows.Count > 0)
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ForwardEmployeeStopIncData))
                    {
                        SqlParameter tblpara = new SqlParameter("EmpIncDataForwarded", dt);
                        tblpara.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(tblpara);
                        int i = epsdatabase.ExecuteNonQuery(dbCommand);
                        if (i < 0)
                        {
                            message = EPSResource.ForwardCheckerMessage;
                        }
                        else
                        {
                            message = EPSResource.ForwardCheckerMessage;
                        }
                    }
                }
            });
            return message;
        }



    }
}
