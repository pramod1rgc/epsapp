﻿using EPS.BusinessModels;
using System;
using System.Collections.Generic;
using System.Data;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Threading.Tasks;
using System.Data.Common;

namespace EPS.DataAccessLayers
{
    public class MasterDA
    {
        
        private Database epsDataBase = null;

        public MasterDA(Database database)
        {
            epsDataBase = database;
        }

        public async Task<IEnumerable<MasterModel>> GetRelation()
        {
            List<MasterModel> ListControllerModel = new List<MasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetRelation))
                {
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            MasterModel ObjMasterModel = new MasterModel();

                            ObjMasterModel.CddirCodeText = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]) : null;

                            ObjMasterModel.CddirCodeType = objReader["CddirCodeType"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeType"]).Trim() : null;
                            ObjMasterModel.CddirCodeValue = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]).Trim() : null;
                            ObjMasterModel.MsCddirID = objReader["MsCddirID"] != DBNull.Value ? Convert.ToString(objReader["MsCddirID"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<JoiningTypeMaster>> GetJoiningMode(Boolean? IsDeput, int? MsEmpSubTypeID)
        {

            List<JoiningTypeMaster> ListControllerModel = new List<JoiningTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetJoiningMode))
                {
                    epsDataBase.AddInParameter(dbCommand, "IsDeput", DbType.Boolean, IsDeput);
                    epsDataBase.AddInParameter(dbCommand, "MsEmpSubTypeID", DbType.Int32, MsEmpSubTypeID);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            JoiningTypeMaster ObjJoiningModel = new JoiningTypeMaster();

                            ObjJoiningModel.JoiningText = objReader["JoiningText"] != DBNull.Value ? Convert.ToString(objReader["JoiningText"]) : null;

                            ObjJoiningModel.JoiningType = objReader["JoiningType"] != DBNull.Value ? Convert.ToString(objReader["JoiningType"]).Trim() : null;
                            ObjJoiningModel.MsEmpTypeID = objReader["MsEmpTypeID"] != DBNull.Value ? Convert.ToString(objReader["MsEmpTypeID"]).Trim() : null;
                            ObjJoiningModel.JoiningValue = objReader["JoiningValue"] != DBNull.Value ? Convert.ToString(objReader["JoiningValue"]).Trim() : null;

                            ListControllerModel.Add(ObjJoiningModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<JoiningTypeMaster>> GetJoiningCategory(int parentTypeId, Boolean? IsDeput)
        {
            List<JoiningTypeMaster> ListControllerModel = new List<JoiningTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_JoiningCategory))
                {
                    epsDataBase.AddInParameter(dbCommand, "parentTypeID", DbType.Int32, parentTypeId);
                    epsDataBase.AddInParameter(dbCommand, "IsDeput", DbType.Boolean, IsDeput);

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            JoiningTypeMaster ObjJoiningCategory = new JoiningTypeMaster();

                            ObjJoiningCategory.JoiningText = objReader["JoiningText"] != DBNull.Value ? Convert.ToString(objReader["JoiningText"]) : null;

                            ObjJoiningCategory.JoiningType = objReader["JoiningType"] != DBNull.Value ? Convert.ToString(objReader["JoiningType"]).Trim() : null;
                            ObjJoiningCategory.MsEmpTypeID = objReader["MsEmpTypeID"] != DBNull.Value ? Convert.ToString(objReader["MsEmpTypeID"]).Trim() : null;
                            ObjJoiningCategory.JoiningValue = objReader["JoiningValue"] != DBNull.Value ? Convert.ToString(objReader["JoiningValue"]).Trim() : null;

                            ListControllerModel.Add(ObjJoiningCategory);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }

        public async Task<IEnumerable<MasterModel>> GetSalutation()
        {
            List<MasterModel> ListControllerModel = new List<MasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetSalutation))
                {
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            MasterModel ObjMasterModel = new MasterModel();

                            ObjMasterModel.CddirCodeText = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]) : null;

                            ObjMasterModel.CddirCodeType = objReader["CddirCodeType"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeType"]).Trim() : null;
                            ObjMasterModel.CddirCodeValue = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]).Trim() : null;
                            ObjMasterModel.MsCddirID = objReader["MsCddirID"] != DBNull.Value ? Convert.ToString(objReader["MsCddirID"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }

        public async Task<IEnumerable<EmployeeTypeMaster>> GetEmpType(int? serviceId, int? deputationId)
        {
            List<EmployeeTypeMaster> ListControllerModel = new List<EmployeeTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetEmpType))
                {
                    epsDataBase.AddInParameter(dbCommand, "serviceId", DbType.Int32, serviceId);
                    epsDataBase.AddInParameter(dbCommand, "deputationId", DbType.Int32, deputationId);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmployeeTypeMaster ObjEmpTypeModel = new EmployeeTypeMaster();

                            ObjEmpTypeModel.MsEmpTypeID = objReader["MsEmpTypeID"] != DBNull.Value ? Convert.ToString(objReader["MsEmpTypeID"]) : null;

                            ObjEmpTypeModel.CodeText = objReader["CodeText"] != DBNull.Value ? Convert.ToString(objReader["CodeText"]).Trim() : null;
                            ObjEmpTypeModel.CodeType = objReader["CodeType"] != DBNull.Value ? Convert.ToString(objReader["CodeType"]).Trim() : null;
                            ObjEmpTypeModel.CodeValue = objReader["CodeValue"] != DBNull.Value ? Convert.ToString(objReader["CodeValue"]).Trim() : null;

                            ListControllerModel.Add(ObjEmpTypeModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<EmployeeTypeMaster>> GetEmpSubType(int parentTypeId, Boolean? IsDeput)
        {
            List<EmployeeTypeMaster> ListControllerModel = new List<EmployeeTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetEmpSubType))
                {
                    epsDataBase.AddInParameter(dbCommand, "parentTypeID", DbType.Int32, parentTypeId);
                    epsDataBase.AddInParameter(dbCommand, "IsDeput", DbType.Boolean, IsDeput);

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmployeeTypeMaster ObjEmpTypeModel = new EmployeeTypeMaster();

                            ObjEmpTypeModel.MsEmpTypeID = objReader["MsEmpTypeID"] != DBNull.Value ? Convert.ToString(objReader["MsEmpTypeID"]) : null;

                            ObjEmpTypeModel.CodeText = objReader["CodeText"] != DBNull.Value ? Convert.ToString(objReader["CodeText"]).Trim() : null;
                            ObjEmpTypeModel.CodeType = objReader["CodeType"] != DBNull.Value ? Convert.ToString(objReader["CodeType"]).Trim() : null;
                            ObjEmpTypeModel.CodeValue = objReader["CodeValue"] != DBNull.Value ? Convert.ToString(objReader["CodeValue"]).Trim() : null;

                            ListControllerModel.Add(ObjEmpTypeModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }

        public async Task<IEnumerable<MasterModel>> GetDeputationType(int? serviceId)
        {
            List<MasterModel> ListControllerModel = new List<MasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetDeputationType))
                {
                    epsDataBase.AddInParameter(dbCommand, "serviceId", DbType.Int32, serviceId);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            MasterModel ObjMasterModel = new MasterModel();

                            ObjMasterModel.CddirCodeText = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]) : null;

                            ObjMasterModel.CddirCodeType = objReader["CddirCodeType"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeType"]).Trim() : null;
                            ObjMasterModel.CddirCodeValue = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]).Trim() : null;
                            ObjMasterModel.MsCddirID = objReader["MsCddirID"] != DBNull.Value ? Convert.ToString(objReader["MsCddirID"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }



        public async Task<IEnumerable<ServiceTypeMaster>> GetServiceType(Boolean? IsDeput)
        {
            List<ServiceTypeMaster> ListControllerModel = new List<ServiceTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_ServiceType))
                {
                    epsDataBase.AddInParameter(dbCommand, "IsDeput", DbType.Boolean, IsDeput);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            ServiceTypeMaster ObjMasterModel = new ServiceTypeMaster();

                            ObjMasterModel.ServiceTypeId = objReader["ServiceTypeId"] != DBNull.Value ? Convert.ToString(objReader["ServiceTypeId"]) : null;

                            ObjMasterModel.ServiceValue = objReader["ServiceValue"] != DBNull.Value ? Convert.ToString(objReader["ServiceValue"]).Trim() : null;
                            ObjMasterModel.ServiceText = objReader["ServiceText"] != DBNull.Value ? Convert.ToString(objReader["ServiceText"]).Trim() : null;
                            ObjMasterModel.ServiceType = objReader["ServiceType"] != DBNull.Value ? Convert.ToString(objReader["ServiceType"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }

        public async Task<IEnumerable<GenderMaster>> GetGender()
        {
            List<GenderMaster> ListControllerModel = new List<GenderMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GenderType))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            GenderMaster ObjMasterModel = new GenderMaster();

                            ObjMasterModel.Genderid = objReader["MsGenderID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGenderID"]) : 0;

                            ObjMasterModel.GenderName = objReader["Gender"] != DBNull.Value ? Convert.ToString(objReader["Gender"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }



        public async Task<IEnumerable<StateMaster>> GetState()
        {
            List<StateMaster> ListControllerModel = new List<StateMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetState))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            StateMaster ObjMasterModel = new StateMaster();

                            ObjMasterModel.StateId = objReader["StateId"] != DBNull.Value ? Convert.ToInt32(objReader["StateId"]) : 0;

                            ObjMasterModel.StateCode = objReader["StateCode"] != DBNull.Value ? Convert.ToString(objReader["StateCode"]).Trim() : null;
                            ObjMasterModel.StateName = objReader["StateName"] != DBNull.Value ? Convert.ToString(objReader["StateName"]).Trim() : null;
                            ObjMasterModel.StateRBICode = objReader["StateRBICode"] != DBNull.Value ? Convert.ToString(objReader["StateRBICode"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }



        public async Task<IEnumerable<PfTypeMaster>> GetPfType()
        {
            List<PfTypeMaster> ListControllerModel = new List<PfTypeMaster>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetPfType))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PfTypeMaster ObjMasterModel = new PfTypeMaster();

                            ObjMasterModel.MsPfAgencyID = objReader["MsPfAgencyID"] != DBNull.Value ? Convert.ToString(objReader["MsPfAgencyID"]).Trim() : null;
                            ObjMasterModel.PfTypeDesc = objReader["PfTypeDesc"] != DBNull.Value ? Convert.ToString(objReader["PfTypeDesc"]).Trim() : null;
                            ObjMasterModel.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<JoiningAccountOf>> GetJoiningAccount()
        {
            List<JoiningAccountOf> ListControllerModel = new List<JoiningAccountOf>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetLienPeridJoiningAccountOf))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            JoiningAccountOf ObjMasterModel = new JoiningAccountOf();

                            ObjMasterModel.CddirCodeValue = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToInt32(objReader["CddirCodeValue"]) : 0;
                            ObjMasterModel.MsCddirID = objReader["MsCddirID"] != DBNull.Value ? Convert.ToInt32(objReader["MsCddirID"]) : 0;
                            ObjMasterModel.CddirCodeText = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<OfficeCityClass>> GetOfficeCityClassList()
        {
            List<OfficeCityClass> ListControllerModel = new List<OfficeCityClass>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetLienGetOfficeCityClass))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            OfficeCityClass ObjMasterModel = new OfficeCityClass();

                            ObjMasterModel.officeClassCityId = objReader["officeClassCityId"] != DBNull.Value ? Convert.ToString(objReader["officeClassCityId"]).Trim() : null;
                            ObjMasterModel.officeClassCityName = objReader["officeClassCityName"] != DBNull.Value ? Convert.ToString(objReader["officeClassCityName"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }


        public async Task<IEnumerable<OfficeList>> GetOfficeList(int controllerId, int? ddoId)
        {
            List<OfficeList> ListControllerModel = new List<OfficeList>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_LienGetAllOfficelist))
                {
                    epsDataBase.AddInParameter(dbCommand, "ControllerId", DbType.Int32, controllerId);
                    epsDataBase.AddInParameter(dbCommand, "ddoid", DbType.Int32, ddoId);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            OfficeList ObjMasterModel = new OfficeList();

                            ObjMasterModel.officeId = objReader["officeId"] != DBNull.Value ? Convert.ToString(objReader["officeId"]).Trim() : null;
                            ObjMasterModel.officeName = objReader["officeName"] != DBNull.Value ? Convert.ToString(objReader["officeName"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }



        public async Task<IEnumerable<PayCommission>> GetPayCommissionList()
        {
            List<PayCommission> ListControllerModel = new List<PayCommission>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetPayCommisionlien))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PayCommission ObjMasterModel = new PayCommission();

                            ObjMasterModel.payCommId = objReader["payCommID"] != DBNull.Value ? Convert.ToString(objReader["payCommID"]).Trim() : null;
                            ObjMasterModel.payCommDesc = objReader["PayCommDesc"] != DBNull.Value ? Convert.ToString(objReader["payCommDesc"]).Trim() : null;

                            ListControllerModel.Add(ObjMasterModel);

                        }

                    }

                }
            });
            if (ListControllerModel == null)
            {
                return null;
            }
            else
            {
                return ListControllerModel;
            }
        }

    }
    }
