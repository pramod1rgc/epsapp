﻿using System;
using System.Collections.Generic;
using EPS.BusinessModels;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
 


namespace EPS.DataAccessLayers.UserManagment
{
    public class RoleDataAccessLayer
    {
        #region Data Access Layer for Role Assign
     private  Database epsdatabase = null;
       
        public RoleDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        /// <summary>
        /// Create New Role
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        public string CreateNewRole(MstRole mstRole)
        {
            string message = string.Empty;
            string IsActive = "";
            if (mstRole.IsActive == "true")
            {
                IsActive = "1";
            }
            else
            {
                IsActive = "0";
            }
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Mst_InsertUpdateRole_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "RoleID", DbType.String, mstRole.RoleID);
                epsdatabase.AddInParameter(dbCommand, "RoleParentID", DbType.String, mstRole.RoleParentID);
                epsdatabase.AddInParameter(dbCommand, "RoleName", DbType.String, mstRole.RoleName);
                epsdatabase.AddInParameter(dbCommand, "RoleDescription", DbType.String, mstRole.RoleDescription);
                epsdatabase.AddInParameter(dbCommand, "InsertedBy", DbType.String, mstRole.LogInUserName);
                epsdatabase.AddInParameter(dbCommand, "InsertedDate", DbType.String, DateTime.Now.ToString("yyyy-MM-dd"));
                epsdatabase.AddInParameter(dbCommand, "ModifiedBy", DbType.String, "");
                epsdatabase.AddInParameter(dbCommand, "LoggInUserName", DbType.String, mstRole.LogInUserName);
                epsdatabase.AddInParameter(dbCommand, "ModifiedDate", DbType.String, DateTime.Now.ToString("yyyy-MM-dd"));
                epsdatabase.AddInParameter(dbCommand, "IsActive", DbType.String, IsActive);
                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
            }
            return message;
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAlRoles(string username)
        {
            List<RoleIDS> lstRole = null;
            lstRole = new List<RoleIDS>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetAllRoles))
            {
                epsdatabase.AddInParameter(dbCommand, "username", DbType.String, @username);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        RoleIDS MstRole = new RoleIDS();
                        MstRole.RoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        MstRole.RoleName = rdr["RoleName"].ToString();
                        MstRole.RoleStatus = rdr["RoleStatus"].ToString();
                        MstRole.RoleDescription = rdr["RoleDescription"].ToString();
                        MstRole.CreatedDate = rdr["CreatedDate"].ToString();
                        MstRole.DeActDesc = rdr["DeActDesc"].ToString();
                        MstRole.DeactivationDate = rdr["Deactivation_Date"].ToString();
                        MstRole.IsActive = rdr["IsActive"].ToString();
                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }


        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAllActiveRollList(string username)
        {
            List<RoleIDS> lstRole = null;
            lstRole = new List<RoleIDS>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetAllActiveRollList))
            {
                epsdatabase.AddInParameter(dbCommand, "username", DbType.String, @username);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        RoleIDS MstRole = new RoleIDS();
                        MstRole.RoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        MstRole.RoleName = rdr["RoleName"].ToString();
                        MstRole.RoleStatus = rdr["RoleStatus"].ToString();
                        MstRole.RoleDescription = rdr["RoleDescription"].ToString();
                        MstRole.CreatedDate = rdr["CreatedDate"].ToString();
                        MstRole.DeActDesc = rdr["DeActDesc"].ToString();
                        MstRole.DeactivationDate = rdr["Deactivation_Date"].ToString();
                        MstRole.IsActive = rdr["IsActive"].ToString();
                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }

        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAllCustomeRollList(string username)
        {
            List<RoleIDS> lstRole = null;
            lstRole = new List<RoleIDS>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetAllCustomeRoll_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "username", DbType.String, @username);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        RoleIDS MstRole = new RoleIDS();
                        MstRole.RoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        MstRole.RoleName = rdr["RoleName"].ToString();
                        MstRole.RoleStatus = rdr["RoleStatus"].ToString();
                        MstRole.RoleDescription = rdr["RoleDescription"].ToString();
                        MstRole.CreatedDate = rdr["CreatedDate"].ToString();
                        MstRole.IsActive = rdr["IsActive"].ToString();
                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }

        /// <summary>
        /// Get Bind User List ddl
        /// </summary>
        /// <param name="RdStatus"></param>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetBindUserListddl(string RdStatus, string LoggRollID, string LoogInUser, string ControllerID, string PAOID, string DDOID, string CurrUserControllerID)
        {
            List<RoleIDS> lstRole = null;
            lstRole = new List<RoleIDS>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetBindUserListddl_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "Rdtext", DbType.String, RdStatus);
                epsdatabase.AddInParameter(dbCommand, "LoggRollID", DbType.String, LoggRollID);
                epsdatabase.AddInParameter(dbCommand, "LoogInUser", DbType.String, LoogInUser);
                epsdatabase.AddInParameter(dbCommand, "ControllerID", DbType.String, ControllerID);
                epsdatabase.AddInParameter(dbCommand, "PAOID", DbType.String, PAOID);
                epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.String, DDOID);
                epsdatabase.AddInParameter(dbCommand, "CurrUserControllerID", DbType.String, CurrUserControllerID);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        RoleIDS MstRole = new RoleIDS();
                        MstRole.MsUserID = Convert.ToString(rdr["MsUserID"]);
                        MstRole.Text = Convert.ToString(rdr["Text"]);

                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }



        /// <summary>
        /// Get Assign Role For User
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstRole> GetAssignRoleForUser(string roleID)
        {
            List<MstRole> lstRole = null;
            lstRole = new List<MstRole>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetroleOnUserMap_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "Values", DbType.String, roleID);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        MstRole MstRole = new MstRole();
                        MstRole.RoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        MstRole.RoleName = rdr["RoleName"].ToString();

                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }

        /// <summary>
        /// Get User Details
        /// </summary>
        /// <param name="UserRole"></param>
        /// <returns></returns>
        public IEnumerable<MstRole> GetUserDetails(MstRole userRole)
        {
            List<MstRole> lstRole = new List<MstRole>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Mst_UserDetails_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "RoleID", DbType.String, userRole.RoleID.ToString());
                epsdatabase.AddInParameter(dbCommand, "Msg_Out_Status", DbType.String, "");
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        MstRole MstUser = new MstRole();
                        MstUser.UserID = Convert.ToInt32(rdr["MsUserID"]);
                        MstUser.FirstName = rdr["FirstName"].ToString();
                        MstUser.Designation = rdr["RoleName"].ToString();
                        MstUser.MsRoleID = rdr["MsRoleID"].ToString();
                        MstUser.PAN = rdr["PAN"].ToString();
                        MstUser.EmpCd = rdr["EmpCd"].ToString();
                        MstUser.IsActive = rdr["IsActive"].ToString();
                        lstRole.Add(MstUser);
                    }
                    return lstRole;
                }
            }
        }
        //End Of User Details
        /// <summary>
        /// Revoke User
        /// </summary>
        /// <param name="MstRole"></param>
        /// <returns></returns>
        public string RevokeUser(MstRole mstRole)
        {
            string message = string.Empty;
            string IsActive = "";
            if (mstRole.IsActive == "true")
            {
                IsActive = "1";
            }
            else
            {
                IsActive = "0";
            }

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Mst_RevokeUser_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "MsRoleID", DbType.String, mstRole.RoleID);
                epsdatabase.AddInParameter(dbCommand, "IsActive", DbType.String, IsActive);
                epsdatabase.AddInParameter(dbCommand, "RoleDescription", DbType.String, mstRole.RoleDescription);
                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);

                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
            }
            return message;
        }


        /// <summary>
        /// Get User Details ByPan
        /// </summary>
        /// <param name="MstRole"></param>
        /// <returns></returns>
        public IEnumerable<MstRole> GetUserDetailsByPan(MstRole mstRole)
        {
            List<MstRole> lstRole = new List<MstRole>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.USP_GetUserDetailsByPan))
            {
                epsdatabase.AddInParameter(dbCommand, "@Values", DbType.String, mstRole.PAN.ToString().Trim());
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {

                    while (rdr.Read())
                    {
                        MstRole MstUser = new MstRole();
                        MstUser.UserID = Convert.ToInt32(rdr["MsUserID"]);
                        MstUser.FirstName = rdr["FirstName"].ToString();
                        MstUser.EmpCd = rdr["EmpCd"].ToString();
                       
                        MstUser.PAN = rdr["PAN"].ToString();
                       
                        lstRole.Add(MstUser);
                    }
                }
                return lstRole;
            }
        }
        /// <summary>
        /// Save Assign Role For User
        /// </summary>
        /// <param name="MstRole"></param>
        /// <returns></returns>
        public string SaveAssignRoleForUser(MstRole mstRole)
        {
            string message = string.Empty;
            string RolId = string.Empty;
            string myIP = GetIP();
            DataTable dt = new DataTable();
            DataTable dtunselected = new DataTable();
            string UserId = string.Empty;
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("MsUserID", typeof(int)), new DataColumn("MsRoleID", typeof(int)), new DataColumn("CreatedClientIP", typeof(string)) });
            dtunselected.Columns.AddRange(new DataColumn[2] {   new DataColumn("RoleID", typeof(int)), new DataColumn("MsuserID", typeof(int)) });
            if (mstRole.RoleIDs != "" && mstRole.RoleIDs != null)
            {
                string[] RolIds = mstRole.RoleIDs.Split('$');
                for (int i = 0; i < RolIds.Length - 1; i++)
                {                    
                    RolId = RolIds[i];
                     UserId = mstRole.UserID.ToString();
                    dt.Rows.Add(UserId, RolId, myIP);
                }
            }
            else
            {
                RolId = "0";
                 UserId = mstRole.UserID.ToString();
                dt.Rows.Add(UserId, RolId, myIP);

            }

            for (int i = 0; i < mstRole.unselectedRole.Count; i++)
            {
                dtunselected.Rows.Add(mstRole.unselectedRole[i].RoleID, UserId);
            }
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Insert_RoleMapping))
            {

                epsdatabase.AddInParameter(dbCommand, "UpdateCase", DbType.String, mstRole.SetFlag);
                epsdatabase.AddInParameter(dbCommand, "MsRoleID", DbType.String, mstRole.MsRoleID);
                SqlParameter para = new SqlParameter("tblCMsRoleMapping", dt);
                SqlParameter unselected = new SqlParameter("tblunselectedMapping", dtunselected);
                para.SqlDbType = SqlDbType.Structured;
                dbCommand.Parameters.Add(para);

                unselected.SqlDbType = SqlDbType.Structured;
                dbCommand.Parameters.Add(unselected);
                 int i = epsdatabase.ExecuteNonQuery(dbCommand);
               if (i < 0)
                {
                    if (RolId == "0")
                    {
                        message = "Role unAssigned successfully";
                        
                    }
                    else
                    {
                        message = "Role assigned successfully";
                    }
                }
            }
            return message;
        }


        /// <summary>
        /// Get All Menu List
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public List<MstRole> GetAllMenuList(string roleID, string Uname, string CurrURole)
        {
            List<MstRole> headerTree = null;
            List<MstRole> categories = new List<MstRole>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetAllMenueList_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "RoLLID", DbType.String, roleID);
                epsdatabase.AddInParameter(dbCommand, "UserName", DbType.String, Uname);
                epsdatabase.AddInParameter(dbCommand, "CurrURoleID", DbType.String, CurrURole);
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        categories.Add(
                            new MstRole
                            {
                                MsMenuID = Convert.ToInt32(rdr["MsMenuID"]),
                                MainMenuName = rdr["MainMenuName"].ToString(),
                                ParentMenu = (Convert.ToInt32(rdr["ParentMenu"]) != 0) ? Convert.ToInt32(rdr["ParentMenu"]) : (int?)null,
                                Status = rdr["Status"].ToString(),
                                MenupermissionID = rdr["MenupermissionID"].ToString(),
                            });
                    }
                    headerTree = FillRecursive(categories, null);
                    return headerTree;
                }
            }

        }
        /// <summary>
        /// Fill Recursive
        /// </summary>
        /// <param name="flatObjects"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<MstRole> FillRecursive(List<MstRole> flatObjects, int? parentId = null)
        {
            return flatObjects.Where(x => x.ParentMenu.Equals(parentId)).Select(item => new MstRole
            {
                MainMenuName = item.MainMenuName,
                MsMenuID = item.MsMenuID,
                Status = item.Status,
                MenupermissionID = item.MenupermissionID,
                Children = FillRecursive(flatObjects, item.MsMenuID)
            }).ToList();
        }

        /// <summary>
        /// Save Menu PerMission
        /// </summary>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
        public string SaveMenuPerMission(string[] mstMenu)
        {
            string message = string.Empty;
            string myIP = GetIP();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4] { new DataColumn("RoLLID", typeof(int)), new DataColumn("MenuID", typeof(int)), new DataColumn("Status", typeof(char)), new DataColumn("CreatedClientIP", typeof(string)) });
            for (int i = 0; i < mstMenu.Count(); i++)
            {
                string menuname = mstMenu[i].ToString().Replace("s_", "");
                string[] arr = menuname.ToString().Split('_');
                string MsMenuID = arr[0];
                string Status = arr[1];
                string RollID = arr[2];
                dt.Rows.Add(RollID, MsMenuID, Status, myIP);
            }
            if (dt.Rows.Count > 0)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Insert_MsAssignMenueType))
                {
                    SqlParameter tblpara = new SqlParameter("tblAssignMenue", dt);
                    tblpara.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(tblpara);
                    int i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i < 0)
                    {
                        message = "Menu assigned successfully";
                    }

                }
            }

            return message;
        }
        /// <summary>
        /// Check Menu PerMission
        /// </summary>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
        public string CheckMenuPerMission(string[] mstMenu)
        {
            string message = string.Empty;


            string[] Arrcurrentselected = mstMenu[0].ToString().Split('_');
            string[] currentselectedid = Arrcurrentselected[2].ToString().Split('/');
            int selectedid = Convert.ToInt32(currentselectedid[0]);
            int selectedValue = Convert.ToInt32(currentselectedid[1]);
            DataSet ds = new DataSet();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.ChKRadioStatuspermission_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "MsMenuID", DbType.String, selectedid);
                using (ds = epsdatabase.ExecuteDataSet(dbCommand))
                {
                    DataTable parentdt = new DataTable();
                    DataTable childdt = new DataTable();
                    DataTable dt2 = new DataTable();
                    parentdt = ds.Tables[0];
                    childdt = ds.Tables[1];
                    if (parentdt.Rows.Count > 0)
                    {
                        for (int parent = 0; parent < parentdt.Rows.Count; parent++)
                        {
                            for (int menu = 0; menu < mstMenu.Count(); menu++)
                            {
                                string[] arr = mstMenu[menu].ToString().Split('_');
                                string MsMenuID = arr[0];
                                int Status = Convert.ToInt32(arr[1]);
                                if (MsMenuID == parentdt.Rows[parent]["MsMenuID"].ToString())
                                {
                                    if (selectedValue > Status)
                                        return "Sub Menu Access Level should not exceed Main Menu Access Level";
                                }
                            }
                        }
                    }

                    if (childdt.Rows.Count > 0)
                    {
                        for (int child = 0; child < childdt.Rows.Count; child++)
                        {
                            for (int menu = 0; menu < mstMenu.Count(); menu++)
                            {
                                string[] arr = mstMenu[menu].ToString().Split('_');
                                string MsMenuID = arr[0];
                                int Status = Convert.ToInt32(arr[1]);
                                if (MsMenuID == childdt.Rows[child]["MsMenuID"].ToString())
                                {
                                    if (selectedValue < Status)
                                        return "main menu access can not be less than sub menu";
                                }
                            }
                        }
                    }
                }
            }
            return message;
        }

        /// <summary>
        /// GetIP
        /// </summary>
        /// <returns></returns>
        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();

        }
        #endregion
    }
}
