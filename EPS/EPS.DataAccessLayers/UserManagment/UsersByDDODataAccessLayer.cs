﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
   public class UsersByDDODataAccessLayer
    {
        #region Users By DDO DataAccessLayer

        private Database epsdatabase = null;
        public UsersByDDODataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
       
        public async Task< string> AssignedCMD(string empCd,int roleID, int DDOID,int UserID, string active, string Password)
        {
            string status = string.Empty;
            string OutUserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_PayRoleAssignCheckerMaker)) 
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd.Trim());
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, UserID);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO Checker");
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 100);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);

                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status"));
                    OutUserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID"));
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(status) || !string.IsNullOrEmpty(OutUserID) || NewUserID != 0)
            {
                return status + "$" + OutUserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }

        public async Task<IEnumerable<EmployeeModel>> AssignedDDOCheckerEmployeeList(int DDOID)  
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedDDOEmployeeList))
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO Checker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();

                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            ObjEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString().Trim();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }

       
        public async Task<IEnumerable<EmployeeModel>> EmployeeListByDDO(int DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EmployeeListOfDDO))
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO Checker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();

                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            ObjEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString().Trim();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }

        
        public async Task< string> SelfAssignDDOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            string Status = string.Empty;
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssign))
                {
                    epsdatabase.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                    epsdatabase.AddInParameter(dbCommand, "username", DbType.String, username);
                    epsdatabase.AddInParameter(dbCommand, "EmpPermDDOId", DbType.Int32, EmpPermDDOId);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, ddoid);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "SDChecker");
                    epsdatabase.AddInParameter(dbCommand, "Status", DbType.String, IsAssign);
                    epsdatabase.AddOutParameter(dbCommand, "Result", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    result = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Result"));
                }
            }).ConfigureAwait(true);
            return result;
        }
        public async Task<IEnumerable<AssignChecked>> CheckedSelfAssignDDO(string MsUserID)
        {
            List<AssignChecked> ListAssignChecked = new List<AssignChecked>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssignChecked))
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsUserID", DbType.String, MsUserID);
                    epsdatabase.AddInParameter(dbCommand, "@Case", DbType.String, "SDChecked");

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AssignChecked ObjEmployeeModel = new AssignChecked();
                            ObjEmployeeModel.MsRoleUserMapID = Convert.ToInt32(rdr["MsRoleUserMapID"]);
                            ObjEmployeeModel.MsUserID = Convert.ToInt32(rdr["MsUserID"]);
                            ObjEmployeeModel.MsRoleID = Convert.ToInt32(rdr["MsRoleID"]);
                            ListAssignChecked.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListAssignChecked == null)
                return null;
            else
                return ListAssignChecked;
        }
        
        #endregion
    }
}
