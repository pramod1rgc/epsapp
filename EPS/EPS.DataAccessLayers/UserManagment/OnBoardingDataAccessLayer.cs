﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.UserManagment
{
   public class OnBoardingDataAccessLayer
    {
        private Database epsDataBase = null;
        
        public OnBoardingDataAccessLayer(Database database)
        {
            epsDataBase = database;
        }

        public async Task<int> OnBoardingSubmit(OnBoardingModel objOnBoardingModel)
        {
            int result = 0;
            string msg = "";
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ods].[ProcOnBoardingSubmit]"))
                {
                    epsDataBase.AddInParameter(dbCommand, "RequestLetterNo", DbType.String, objOnBoardingModel.RequestLetterNo);
                    epsDataBase.AddInParameter(dbCommand, "RequestLetterDate", DbType.Date, objOnBoardingModel.RequestLetterDate);
                    epsDataBase.AddInParameter(dbCommand, "ControllerID", DbType.String, objOnBoardingModel.ControllerID);
                    epsDataBase.AddInParameter(dbCommand, "PAOID", DbType.String, objOnBoardingModel.PAOID);
                    epsDataBase.AddInParameter(dbCommand, "DDOID", DbType.String, objOnBoardingModel.DDOID);
                    epsDataBase.AddInParameter(dbCommand, "FirstName", DbType.String, objOnBoardingModel.FirstName);
                    epsDataBase.AddInParameter(dbCommand, "LastName", DbType.String, objOnBoardingModel.LastName);
                    epsDataBase.AddInParameter(dbCommand, "Designation", DbType.String, objOnBoardingModel.Designation);
                    epsDataBase.AddInParameter(dbCommand, "EmailID", DbType.String, objOnBoardingModel.EmailID);
                    epsDataBase.AddInParameter(dbCommand, "MobileNo", DbType.String, objOnBoardingModel.MobileNo);
                    epsDataBase.AddInParameter(dbCommand, "CFirstName", DbType.String, objOnBoardingModel.CFirstName);
                    epsDataBase.AddInParameter(dbCommand, "CLastName", DbType.String, objOnBoardingModel.CLastName);
                    epsDataBase.AddInParameter(dbCommand, "CDesignation", DbType.String, objOnBoardingModel.CDesignation);
                    epsDataBase.AddInParameter(dbCommand, "CEmailID", DbType.String, objOnBoardingModel.CEmailID);
                    epsDataBase.AddInParameter(dbCommand, "CMobileNo", DbType.String, objOnBoardingModel.CMobileNo);
                    epsDataBase.AddInParameter(dbCommand, "PFirstName", DbType.String, objOnBoardingModel.PFirstName);
                    epsDataBase.AddInParameter(dbCommand, "PLastName", DbType.String, objOnBoardingModel.PLastName);
                    epsDataBase.AddInParameter(dbCommand, "PDesignation", DbType.String, objOnBoardingModel.PDesignation);
                    epsDataBase.AddInParameter(dbCommand, "PEmailID", DbType.String, objOnBoardingModel.PEmailID);
                    epsDataBase.AddInParameter(dbCommand, "PMobileNo", DbType.String, objOnBoardingModel.PMobileNo);
                    epsDataBase.AddInParameter(dbCommand, "DFirstName", DbType.String, objOnBoardingModel.DFirstName);
                    epsDataBase.AddInParameter(dbCommand, "DLastName", DbType.String, objOnBoardingModel.DLastName);
                    epsDataBase.AddInParameter(dbCommand, "DDesignation", DbType.String, objOnBoardingModel.DDesignation);
                    epsDataBase.AddInParameter(dbCommand, "DEmailID", DbType.String, objOnBoardingModel.DEmailID);
                    epsDataBase.AddInParameter(dbCommand, "DMobileNo", DbType.String, objOnBoardingModel.DMobileNo);
                    epsDataBase.AddOutParameter(dbCommand, "result", DbType.String, 50);
                    epsDataBase.ExecuteNonQuery(dbCommand);
                    msg = Convert.ToString(epsDataBase.GetParameterValue(dbCommand, "result")).Trim();
                    if(msg == "AlreadyExist")
                    {
                        //result = 0;
                    }
                    else
                    {
                        result = 1;
                    }
                }
            });
            return result;

        }

        public  async Task<string> CancelRequestLetterNo(string remarks)
        {
            //throw new NotImplementedException();
            string result = "";
            string msg = "";
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ods].[ProcCancelRequestLetterNo]"))
                {
                    epsDataBase.AddInParameter(dbCommand, "RequestLetterNo", DbType.String, remarks);
                    epsDataBase.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
                    epsDataBase.AddInParameter(dbCommand, "status", DbType.String, 'C');
                    epsDataBase.AddOutParameter(dbCommand, "result", DbType.String, 50);
                    epsDataBase.ExecuteNonQuery(dbCommand);
                    msg = Convert.ToString(epsDataBase.GetParameterValue(dbCommand, "result")).Trim();
                    if (msg == "AlreadyExist")
                    {
                      
                    }
                    else
                    {
                        result = "";
                    }
                }
            });
            return result;
        }

        public List<DropDownListModel> GetAllReuestNoOfOnboarding()
        {
            List<DropDownListModel> listDropDownListModel = null;
            listDropDownListModel = new List<DropDownListModel>();
            using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ods].[ProcGetAllReuestNoOfOnboarding]"))
            {
                using (IDataReader rdr = epsDataBase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        DropDownListModel objDropDownListModel = new DropDownListModel();
                        objDropDownListModel.Values = rdr["ID"].ToString();
                        objDropDownListModel.Text = rdr["Text"].ToString();
                        listDropDownListModel.Add(objDropDownListModel);
                    }
                }
            }
            if (listDropDownListModel == null)
                return null;
            else
                return listDropDownListModel;
        }

        public List<OnBoardingModel> FetchRequestLetterNoRecord(int OnboardingId)
        {
            List<OnBoardingModel> listOnBoardingModel = null;
            listOnBoardingModel = new List<OnBoardingModel>();
            using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ods].[procFetchRequestLetterNoRecord]"))
            {
                epsDataBase.AddInParameter(dbCommand, "@OnboardingId", DbType.Int32, OnboardingId);
                using (IDataReader rdr = epsDataBase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        OnBoardingModel ObjOnBoardingModel = new OnBoardingModel();
                        ObjOnBoardingModel.RequestLetterDate = Convert.ToDateTime(rdr["RequestLetterDate"]);
                        ObjOnBoardingModel.ControllerID = rdr["ControllerID"].ToString();
                        ObjOnBoardingModel.PAOID = rdr["PAOID"].ToString();
                        ObjOnBoardingModel.DDOID = rdr["DDOID"].ToString();
                        ObjOnBoardingModel.FirstName = rdr["FirstName"].ToString();
                        ObjOnBoardingModel.LastName = rdr["LastName"].ToString();
                        ObjOnBoardingModel.Designation = rdr["Designation"].ToString();
                        ObjOnBoardingModel.EmailID = rdr["EmailID"].ToString();
                        ObjOnBoardingModel.MobileNo = rdr["MobileNo"].ToString();
                        ObjOnBoardingModel.CFirstName = rdr["CFirstName"].ToString();
                        ObjOnBoardingModel.CLastName = rdr["CLastName"].ToString();
                        ObjOnBoardingModel.CDesignation = rdr["CDesignation"].ToString();
                        ObjOnBoardingModel.CEmailID = rdr["CEmailID"].ToString();
                        ObjOnBoardingModel.CMobileNo = rdr["CMobileNo"].ToString();
                        ObjOnBoardingModel.PFirstName = rdr["PFirstName"].ToString();
                        ObjOnBoardingModel.PLastName = rdr["PLastName"].ToString();
                        ObjOnBoardingModel.PDesignation = rdr["PDesignation"].ToString();
                        ObjOnBoardingModel.PEmailID = rdr["PEmailID"].ToString();
                        ObjOnBoardingModel.PMobileNo = rdr["PMobileNo"].ToString();
                        ObjOnBoardingModel.DFirstName = rdr["DFirstName"].ToString();
                        ObjOnBoardingModel.DLastName = rdr["DLastName"].ToString();
                        ObjOnBoardingModel.DDesignation = rdr["DDesignation"].ToString();
                        ObjOnBoardingModel.DEmailID = rdr["DEmailID"].ToString();
                        ObjOnBoardingModel.DMobileNo = rdr["DMobileNo"].ToString();
                        listOnBoardingModel.Add(ObjOnBoardingModel);
                    }
                }
            }
            if (listOnBoardingModel == null)
                return null;
            else
                return listOnBoardingModel;
        }
    }
}
