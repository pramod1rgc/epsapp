﻿using System;
using System.Collections.Generic;
using EPS.BusinessModels;
using System.Data;
using System.Net;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Reflection;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.UserManagment
{
    public class MenuDataAccessLayer
    {
        #region Data Access Layer of Menu
      private  Database epsdatabase = null;
        /// <summary>
        /// menu Data AccessL ayer
        /// </summary>
        /// <param name="database"></param>
        public MenuDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        /// <summary>
        /// Bind Drop Down Menu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> BindDropDownMenu()
        {
            List<MstMenuModel> lstMenu = new List<MstMenuModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetMenuDDl))
            {
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        MstMenuModel MstMenu = new MstMenuModel();
                        MstMenu.MsMenuID = Convert.ToInt32(rdr["MsMenuID"]);
                        MstMenu.MainMenuName = rdr["MainMenuName"].ToString();
                        lstMenu.Add(MstMenu);
                    }
                }
            }
            if (lstMenu == null)
                return null;
            else
                return lstMenu;
        }
        /// <summary>
        /// create new manu SaveMenu
        /// </summary>
        /// <param name="hdnMenuId"></param>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
        public string SaveMenu(int hdnMenuId, string uRoleID,MstMenuModel mstMenu)
        {


            // return "";
            string message = string.Empty;
            string hostName = System.Net.Dns.GetHostName(); // Retrive the Name of HOST  
            string myIP = null;
            string curID = "";
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Mst_InsertUpdateMenu_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "MsMenuID", DbType.String, hdnMenuId);
                epsdatabase.AddInParameter(dbCommand, "MainMenuName", DbType.String, mstMenu.MainMenuName.Trim());
                epsdatabase.AddInParameter(dbCommand, "MenuURL", DbType.String, mstMenu.MenuURL.Trim());
                epsdatabase.AddInParameter(dbCommand, "ParentMenu", DbType.Int32, Convert.ToInt32(mstMenu.ParentMenu));
                epsdatabase.AddInParameter(dbCommand, "Status", DbType.String, "");
                epsdatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, myIP);
                epsdatabase.AddInParameter(dbCommand, "IsActive", DbType.String, "1");
                epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, uRoleID);
                epsdatabase.AddInParameter(dbCommand, "ModifiedBy", DbType.String, "");
                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                epsdatabase.AddOutParameter(dbCommand, "MenusId", DbType.String, 500);
                epsdatabase.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
                curID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "MenusId"));
               


                if (curID != null && curID != "")
                {

                    DataTable table = ToDataTable(mstMenu.RoleSIDS);
                    table.Columns.Remove("RoleName");
                    if (table.Rows.Count > 0)
                    {

                        int i;
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[4] { new DataColumn("RoLLID", typeof(int)), new DataColumn("MenuID", typeof(int)), new DataColumn("Status", typeof(char)), new DataColumn("CreatedClientIP", typeof(string)) });

                        for (i = 0; i < table.Rows.Count; i++)
                        {
                            int RoleID = Convert.ToInt32(table.Rows[i]["RoleID"]);
                            dt.Rows.Add(RoleID, curID, "3", myIP);

                        }
                        if (dt.Rows.Count > 0)
                        {
                            using (DbCommand dbCommand1 = epsdatabase.GetStoredProcCommand(EPSConstant.Insert_MsAssignMenueType))
                            {
                                SqlParameter tblpara = new SqlParameter("tblAssignMenue", dt);
                                tblpara.SqlDbType = SqlDbType.Structured;
                                dbCommand1.Parameters.Add(tblpara);
                                epsdatabase.ExecuteNonQuery(dbCommand1);
                               

                            }

                        }
                    }
                }
                return message;
            }
        }

        public DataTable ToDataTable<T>(List<T> items)

        {

            DataTable dataTable = new DataTable(typeof(T).Name);

           

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)

            {


                dataTable.Columns.Add(prop.Name);

            }

            foreach (T item in items)

            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)

                {

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            return dataTable;

        }

        /// <summary>
        ///Get Predefine Role
        /// </summary>
        /// <param name="hdnMenuId"></param>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> GetPredefineRole()
        {
            List<MstMenuModel> lstRole = null;
            lstRole = new List<MstMenuModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetPredefineRole_Sp))
            {
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        MstMenuModel MstRole = new MstMenuModel();
                        MstRole.RoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        MstRole.RoleName = rdr["RoleName"].ToString();

                        lstRole.Add(MstRole);
                    }
                }
            }
            if (lstRole == null)
                return null;
            else
                return lstRole;
        }


        /// <summary>
        ///Get Predefine Role
        /// </summary>
        /// <param name="hdnMenuId"></param>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
       

        public async Task<object[]> FillUpdateMenu(string MenuId)
        {
            object[] listTable = new object[2];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.FillUpdateMenu_SP))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsMenuID", DbType.String, MenuId);
                    DataSet ds = epsdatabase.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];
                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }

        /// <summary>
        /// Bind Menu In Gride
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> BindMenuInGride()
        {
            List<MstMenuModel> lstMenu = new List<MstMenuModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetMenuInGride))
            {
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        MstMenuModel MstMenu = new MstMenuModel();
                        MstMenu.MsMenuID = Convert.ToInt32(rdr["MsMenuID"]);
                        MstMenu.MainMenuName = rdr["MainMenuName"].ToString();
                        MstMenu.MenuURL = rdr["MenuURL"].ToString();
                        MstMenu.ParentMenu = Convert.ToInt32(rdr["ParentMenu"]);
                        MstMenu.ParentMenuName = rdr["ParentMenuName"].ToString();
                        MstMenu.Status = rdr["Status"].ToString();
                        MstMenu.IPAddress = rdr["IPAddress"].ToString();
                        MstMenu.CreatedBy = rdr["CreatedBy"].ToString();
                        MstMenu.CreatedBy = rdr["CreatedBy"].ToString();
                        MstMenu.CreatedStatus = rdr["CreatedStatus"].ToString();
                        MstMenu.ModifiedBy = rdr["ModifiedBy"].ToString();
                        MstMenu.ModifiedDate = rdr["ModifiedDate"].ToString();
                        MstMenu.IsActive = rdr["IsActive"].ToString();
                        lstMenu.Add(MstMenu);
                    }
                }
            }
            return lstMenu;
        }
        /// <summary>
        /// Active Deactive Menu
        /// </summary>
        /// <param name="MstMenu"></param>
        /// <returns></returns>
        public string ActiveDeactiveMenu(MstMenuModel mstMenu)
        {
            string message = string.Empty;
            string IsActive = "";
            if (mstMenu.IsActive == "true")
            {
                IsActive = "1";
            }
            else
            {
                IsActive = "0";
            }
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Mst_MenuActiveAndDeActive_SP))
            {
                epsdatabase.AddInParameter(dbCommand, "MsMenuID", DbType.String, mstMenu.MsMenuID);
                epsdatabase.AddInParameter(dbCommand, "IsActive", DbType.String, IsActive);
                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                epsdatabase.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
            }
            return message;
        }
      
        #endregion
      
    }
}
