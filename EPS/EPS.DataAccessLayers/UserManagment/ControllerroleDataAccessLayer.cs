﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
   public class ControllerroleDataAccessLayer
    {
        #region Controller role Data Access Layer
        private Database _epsDatabase = null;
        public ControllerroleDataAccessLayer(Database database)
        {
            _epsDatabase = database;
        }
                        
        public async Task<List<EmployeeModel>> AssignedEmp(string msControllerID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_AssignedEmp))//"[ODS].[AssignedEmp]"
                {
                    _epsDatabase.AddInParameter(dbCommand, "@ControllerID", DbType.String, msControllerID);
                    _epsDatabase.AddInParameter(dbCommand, "@case", DbType.String, "controller");
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel objEmployeeModel = new EmployeeModel();
                            objEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            objEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            objEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            objEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            objEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            objEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            objEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString().Trim();
                            listEmployeeModel.Add(objEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listEmployeeModel==null)
                return null;
            else
                return listEmployeeModel;
        }

        /// <summary>
        /// Get All Controllers
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task< List<DropDownListModel>> GetAllControllers(string controllerID, string query)
        {
            List<DropDownListModel> listDropDownListModel = null;
            listDropDownListModel = new List<DropDownListModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    _epsDatabase.AddInParameter(dbCommand, "@Query", DbType.String, query);
                    _epsDatabase.AddInParameter(dbCommand, "@ID", DbType.String, controllerID);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DropDownListModel objDropDownListModel = new DropDownListModel();
                            objDropDownListModel.Values = rdr["ID"].ToString();
                            objDropDownListModel.Text = rdr["Text"].ToString();
                            listDropDownListModel.Add(objDropDownListModel);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (listDropDownListModel == null)
                return null;
            else
                return listDropDownListModel;
        }

        /// <summary>
        /// Get All paos
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public  async Task< IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query)
        {
            List<DropDownListModel> listPAOs = null;
            listPAOs = new List<DropDownListModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    _epsDatabase.AddInParameter(dbCommand, "@Query", DbType.String, query);
                    _epsDatabase.AddInParameter(dbCommand, "@ID", DbType.String, controllerID);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DropDownListModel objDropDownListModel = new DropDownListModel();
                            objDropDownListModel.Values = rdr["ID"].ToString();
                            objDropDownListModel.Text = rdr["Text"].ToString();
                            listPAOs.Add(objDropDownListModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listPAOs == null)
                return null;
            else
                return listPAOs;
        }
        /// <summary>
        /// Controller role Select changed
        /// </summary>
        /// <param name="msControllerID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>>ControllerroleSelectchanged(string msControllerID, string PAOID, string DDOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_ControllerroleSelectchanged))
                {
                    _epsDatabase.AddInParameter(dbCommand, "@PAOID", DbType.String, PAOID);
                    _epsDatabase.AddInParameter(dbCommand, "@EmpPermDDOId", DbType.String, DDOID);
                    _epsDatabase.AddInParameter(dbCommand, "@MsControllerID", DbType.String, msControllerID);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel objEmployeeModel = new EmployeeModel();

                            objEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            objEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            objEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            objEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            objEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            objEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            objEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString().Trim();
                            listEmployeeModel.Add(objEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;
        }
        /// <summary>
        /// Paos Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msControllerID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task<string> ControllerAssigned(string empCd, int msControllerID,string active,string Password)
        {
            string status = string.Empty;
            string UserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.USP_PayRoleAssign))//"[ODS].[PayRoleAssign]"
                {
                    _epsDatabase.AddInParameter(dbCommand, "Case", DbType.String, "Controller");
                    _epsDatabase.AddInParameter(dbCommand, "MsControllerID", DbType.String, msControllerID);
                    _epsDatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    _epsDatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    _epsDatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, 1);
                    _epsDatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    _epsDatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    _epsDatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    _epsDatabase.AddOutParameter(dbCommand, "Office", DbType.String, 50);
                    _epsDatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    _epsDatabase.ExecuteNonQuery(dbCommand);
                    status = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "Status")).Trim();
                    UserID = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "UserID")).Trim();
                    NewUserID = Convert.ToInt32(_epsDatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(status)|| !string.IsNullOrEmpty(UserID) || NewUserID != 0)
            {
                return status + "$" + UserID + "&" + NewUserID+"$"+ Office + "$" + RoleStatus;
            }
            else
            {
                return  "";
            }
        }
        /// <summary>
        /// Get All User Roles
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task< IEnumerable<UserType>> GetAllUserRoles(string username)
        {
             List<UserType> listUserType = new List<UserType>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.USP_GetAllUserRoles))//"[ODS].[GetAllUserRoles]"
                {
                    _epsDatabase.AddInParameter(dbCommand, "@username", DbType.String, username.Trim());
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            UserType objUserType = new UserType();
                            objUserType.MsRoleID = Convert.ToInt32(rdr["MsRoleID"]);
                            objUserType.RoleName = rdr["RoleName"].ToString();
                            listUserType.Add(objUserType);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listUserType == null)
                return null;
            else
                return listUserType;
        }
        #endregion
    }
}
