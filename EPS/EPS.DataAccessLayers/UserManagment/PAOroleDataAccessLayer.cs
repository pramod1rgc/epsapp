﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
   public class PAOroleDataAccessLayer
    {
        #region PAO role Data Access Layer
        private Database epsdatabase = null;
        public PAOroleDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        
        public async Task<IEnumerable<EmployeeModel>> AssignedPAOEmp(string msPAOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedEmpPAO))//"[ODS].[AssignedEmpPAO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsPAOID", DbType.String, msPAOID);
                    epsdatabase.AddInParameter(dbCommand, "@case", DbType.String, "PAO");
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel objEmployeeModel = new EmployeeModel();
                            objEmployeeModel.EmpCd = rdr["EmpCd"].ToString();
                            objEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            objEmployeeModel.EmpName = rdr["EmpName"].ToString();
                            objEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString();
                            objEmployeeModel.Email = rdr["Email"].ToString();
                            objEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            listEmployeeModel.Add(objEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;
        }
        public async Task< IEnumerable<EmployeeModel>> AssignedEmpList(string msPAOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedEmpPAO))//"[ODS].[AssignedEmpPAO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsPAOID", DbType.String, msPAOID);
                    epsdatabase.AddInParameter(dbCommand, "@case", DbType.String, "PAO");
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel objEmployeeModel = new EmployeeModel();
                            objEmployeeModel.EmpCd = rdr["EmpCd"].ToString();
                            objEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            objEmployeeModel.EmpName = rdr["EmpName"].ToString();
                            objEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString();
                            objEmployeeModel.Email = rdr["Email"].ToString();
                            objEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            listEmployeeModel.Add(objEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;
        }
        /// <summary>
        /// Get All paos
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task< IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query)
        {
            List<DropDownListModel> listPAOs = null;
            listPAOs = new List<DropDownListModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    epsdatabase.AddInParameter(dbCommand, "@Query", DbType.String, query);
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.String, controllerID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DropDownListModel objDropDownListModel = new DropDownListModel();
                            objDropDownListModel.Values = rdr["ID"].ToString();
                            objDropDownListModel.Text = rdr["Text"].ToString();
                            listPAOs.Add(objDropDownListModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listPAOs == null)
                return null;
            else
                return listPAOs;
        }

        /// <summary>
        /// Get All DDOs Under Selected PAOs
        /// </summary>
        /// <param name="msPAOID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>> GetAllDDOsUnderSelectedPAOs(string msPAOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllDDOsUnderSelectedPAOs))
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsPAOID", DbType.String, msPAOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel objEmployeeModel = new EmployeeModel();
                            objEmployeeModel.EmpCd = rdr["EmpCd"].ToString();
                            objEmployeeModel.DDOName = rdr["DDOName"].ToString();
                            objEmployeeModel.EmpName = rdr["EmpName"].ToString();
                            objEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString();
                            objEmployeeModel.Email = rdr["Email"].ToString();
                            objEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();

                            listEmployeeModel.Add(objEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;
        }

        /// <summary>
        /// Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msPAOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task< string> Assigned(string empCd, int msPAOID, string active, string Password)
        {
            string status = string.Empty;
            string UserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.USP_PayRoleAssign))//EPSConstant.USP_PayRoleAssign       [ODS].[PayRoleAssign]
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "PAO");
                    epsdatabase.AddInParameter(dbCommand, "MsPAOID", DbType.String, msPAOID);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, 1);
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);

                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status")).Trim();
                    UserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID")).Trim();
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(status) || !string.IsNullOrEmpty(UserID) || NewUserID != 0)
            {
                return status + "$" + UserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }
        #endregion
    }
}
