﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.UserManagment
{
   public class HOODataAccessLayer
    {
        private Database epsdatabase = null;
        public HOODataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        
        public async Task<object> HooCheckerEmpList(int DDOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            List<EmployeeModel> listEmployeeModelAsigned = new List<EmployeeModel>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EmployeeListOfHOO))//"[ODS].[EmployeeListOfHOO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "HOO Checker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);

                    DataSet ds = epsdatabase.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];

                    for(int i=0; i< dt.Rows.Count;i++)
                    { 
                        EmployeeModel objEmployeeModel = new EmployeeModel();
                        objEmployeeModel.EmpCd = dt.Rows[i]["EmpCd"].ToString();
                        objEmployeeModel.DDOName = dt.Rows[i]["DDOName"].ToString().Trim();
                        objEmployeeModel.PAOName = dt.Rows[i]["PAOName"].ToString().Trim();
                        objEmployeeModel.EmpName = dt.Rows[i]["EmpName"].ToString();
                        objEmployeeModel.CommonDesigDesc = dt.Rows[i]["CommonDesigDesc"].ToString();
                        objEmployeeModel.Email = dt.Rows[i]["Email"].ToString();
                        objEmployeeModel.ActiveStatus = dt.Rows[i]["ActiveStatus"].ToString();
                        listEmployeeModel.Add(objEmployeeModel);
                    }

                    DataTable dtAssign = ds.Tables[1];
                    for (int i = 0; i < dtAssign.Rows.Count; i++)
                    {
                        EmployeeModel objEmployeeModel = new EmployeeModel();
                        objEmployeeModel.EmpCd = dtAssign.Rows[i]["EmpCd"].ToString();
                        objEmployeeModel.DDOName = dt.Rows[i]["DDOName"].ToString().Trim();
                        objEmployeeModel.PAOName = dt.Rows[i]["PAOName"].ToString().Trim();
                        objEmployeeModel.EmpName = dtAssign.Rows[i]["EmpName"].ToString();
                        objEmployeeModel.CommonDesigDesc = dtAssign.Rows[i]["CommonDesigDesc"].ToString();
                        objEmployeeModel.Email = dtAssign.Rows[i]["Email"].ToString();
                        objEmployeeModel.ActiveStatus = dtAssign.Rows[i]["ActiveStatus"].ToString();
                        listEmployeeModelAsigned.Add(objEmployeeModel);
                    }
                }
            });
            if (listEmployeeModel == null)
                return null;
            else
                return new { listEmployeeModel, listEmployeeModelAsigned };
        }

        public async Task<object> hooMakerEmpList(int DDOID)
        {
            List<EmployeeModel> listEmployeeModel = new List<EmployeeModel>();
            List<EmployeeModel> listEmployeeModelAsigned = new List<EmployeeModel>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EmployeeListOfHOO))//"[ODS].[EmployeeListOfHOO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "HOO Maker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);
                    DataSet ds = epsdatabase.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        EmployeeModel objEmployeeModel = new EmployeeModel();
                        objEmployeeModel.EmpCd = dt.Rows[i]["EmpCd"].ToString();
                        objEmployeeModel.DDOName = dt.Rows[i]["DDOName"].ToString().Trim();
                        objEmployeeModel.PAOName = dt.Rows[i]["PAOName"].ToString().Trim();
                        objEmployeeModel.EmpName = dt.Rows[i]["EmpName"].ToString();
                        objEmployeeModel.CommonDesigDesc = dt.Rows[i]["CommonDesigDesc"].ToString();
                        objEmployeeModel.Email = dt.Rows[i]["Email"].ToString();
                        objEmployeeModel.ActiveStatus = dt.Rows[i]["ActiveStatus"].ToString();
                        listEmployeeModel.Add(objEmployeeModel);
                    }

                    DataTable dt1 = ds.Tables[1];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        EmployeeModel objEmployeeModel = new EmployeeModel();
                        objEmployeeModel.EmpCd = dt1.Rows[i]["EmpCd"].ToString();
                        objEmployeeModel.DDOName = dt1.Rows[i]["DDOName"].ToString().Trim();
                        objEmployeeModel.PAOName = dt1.Rows[i]["PAOName"].ToString().Trim();
                        objEmployeeModel.EmpName = dt1.Rows[i]["EmpName"].ToString();
                        objEmployeeModel.CommonDesigDesc = dt.Rows[i]["CommonDesigDesc"].ToString();
                        objEmployeeModel.Email = dt1.Rows[i]["Email"].ToString();
                        objEmployeeModel.ActiveStatus = dt1.Rows[i]["ActiveStatus"].ToString();
                        listEmployeeModelAsigned.Add(objEmployeeModel);
                    }
                }
            });
            if (listEmployeeModel == null)
                return null;
            else
                return new { listEmployeeModel, listEmployeeModelAsigned };
        }

        public async Task<string> AssignedHOOChecker(string empCd, int DDOID, string active, string Password)
        {
            string status = string.Empty;
            string UserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_PayRoleAssignCheckerHOO)) //"[ODS].[PayRoleAssignCheckerHOO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "MsroleID", DbType.String, 8);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, 1);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "HOOChecker");
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 100);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);

                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status"));
                    UserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID"));
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            });

            if (!string.IsNullOrEmpty(status)|| !string.IsNullOrEmpty(UserID) || NewUserID != 0)
            {
                return status + "$" + UserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }
        public async Task<string> AssignedHOOMaker(string empCd, int DDOID, string active, string Password)
        {
            
            string status = string.Empty;
            string UserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;

            int NewUserID = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_PayRoleAssignCheckerHOO)) // "[ODS].[PayRoleAssignCheckerHOO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "MsroleID", DbType.String, 8);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, 1);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "HOOMaker");
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 100);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);

                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status"));
                    UserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID"));
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            });

            if (!string.IsNullOrEmpty(status) || !string.IsNullOrEmpty(UserID)|| NewUserID != 0)
            {
                return status + "$" + UserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }

        public async Task< string> SelfAssignedHOOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            string Status = string.Empty;
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssign))//"[ODS].[SelfAssign]"
                {
                    epsdatabase.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                    epsdatabase.AddInParameter(dbCommand, "username", DbType.String, username);
                    epsdatabase.AddInParameter(dbCommand, "EmpPermDDOId", DbType.Int32, EmpPermDDOId);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, ddoid);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "SDHOOChecker");
                    epsdatabase.AddInParameter(dbCommand, "Status", DbType.String, IsAssign);
                    epsdatabase.AddOutParameter(dbCommand, "Result", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    result = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Result"));
                }
            }).ConfigureAwait(true);
            return result;
        }
        public async Task<IEnumerable<AssignChecked>> SelfAssignHOOCheckerRole(string MsUserID)
        {
            List<AssignChecked> ListAssignChecked = new List<AssignChecked>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssignChecked))//"[ODS].[SelfAssignChecked]"
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsUserID", DbType.String, MsUserID);
                    epsdatabase.AddInParameter(dbCommand, "@Case", DbType.String, "SDHOOChecked");

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AssignChecked ObjEmployeeModel = new AssignChecked();
                            ObjEmployeeModel.MsRoleUserMapID = Convert.ToInt32(rdr["MsRoleUserMapID"]);
                            ObjEmployeeModel.MsUserID = Convert.ToInt32(rdr["MsUserID"]);
                            ObjEmployeeModel.MsRoleID = Convert.ToInt32(rdr["MsRoleID"]);
                            ListAssignChecked.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListAssignChecked == null)
                return null;
            else
                return ListAssignChecked;
        }

    }
}
