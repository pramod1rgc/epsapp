﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.UserManagment
{
   public class MakerDataAccessLayer
    {
        private Database epsdatabase = null;
        public MakerDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        /// <summary>
        /// DDOID
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>> AssignedmakerEmpList(int DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedDDOMakerEmployeeList))//"[ODS].[AssignedDDOMakerEmployeeList]"
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO Maker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();
                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString();
                            ObjEmployeeModel.Email = rdr["Email"].ToString();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }
        /// <summary>
        /// Employee List By DDO
        /// </summary>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>>makerEmpList(int DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EmployeeListOfDDO))//"[ODS].[EmployeeListOfDDO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO Maker");
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, DDOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();
                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString();
                            ObjEmployeeModel.Email = rdr["Email"].ToString();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }
        /// <summary>
        /// Assigned CMD
        /// </summary>
        /// <param name="empCd"></param>
   
        /// <param name="DDOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public  async Task< string> AssignedMaker(string empCd, int DDOID, int UserID, string active, string Password)
        {
            string status = string.Empty;
            string OutUserID = string.Empty;
            string Office = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_PayRoleAssignCheckerMaker))//"[ODS].[PayRoleAssignCheckerMaker]"
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, UserID);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status"));
                    OutUserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID"));
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(status) || !string.IsNullOrEmpty(OutUserID) || NewUserID != 0)
            {
                return status + "$" + OutUserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="username"></param>
        /// <param name="EmpPermDDOId"></param>
        /// <param name="ddoid"></param>
        /// <param name="IsAssign"></param>
        /// <returns></returns>
        public async Task< string> SelfAssignedDDOMaker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            string Status = string.Empty;
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssign))//"[ODS].[SelfAssign]" check
                {
                    epsdatabase.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                    epsdatabase.AddInParameter(dbCommand, "username", DbType.String, username);
                    epsdatabase.AddInParameter(dbCommand, "EmpPermDDOId", DbType.Int32, EmpPermDDOId);
                    epsdatabase.AddInParameter(dbCommand, "DDOID", DbType.Int32, ddoid);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "SDMaker");
                    epsdatabase.AddInParameter(dbCommand, "Status", DbType.String, IsAssign);
                    epsdatabase.AddOutParameter(dbCommand, "Result", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    result = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Result"));
                }
            }).ConfigureAwait(true);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MsUserID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<AssignChecked>> SelfAssignMakerRole(string MsUserID)
        {
            List<AssignChecked> ListAssignChecked = new List<AssignChecked>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SelfAssignChecked))//"[ODS].[SelfAssignChecked]" check
                {
                    epsdatabase.AddInParameter(dbCommand, "@MsUserID", DbType.String, MsUserID);
                    epsdatabase.AddInParameter(dbCommand, "@Case", DbType.String, "SDMaker");

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AssignChecked ObjEmployeeModel = new AssignChecked();
                            ObjEmployeeModel.MsRoleUserMapID = Convert.ToInt32(rdr["MsRoleUserMapID"]);
                            ObjEmployeeModel.MsUserID = Convert.ToInt32(rdr["MsUserID"]);
                            ObjEmployeeModel.MsRoleID = Convert.ToInt32(rdr["MsRoleID"]);
                            ListAssignChecked.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListAssignChecked == null)
                return null;
            else
                return ListAssignChecked;
        }
    }
}
