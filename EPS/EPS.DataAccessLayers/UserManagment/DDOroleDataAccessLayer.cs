﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
    /// <summary>
    /// DDO role Data Access Layer
    /// </summary>
    public class DDOroleDataAccessLayer
    {
        #region DDO role Data Access Layer
        private Database epsdatabase = null;
        public DDOroleDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }

        public async Task<IEnumerable<EmployeeModel>> AssignedDDO(string DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedDDO))//"[ODS].[AssignedDDO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "@DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "@case", DbType.String, "DDO");
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();
                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            ObjEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }
        public async Task< IEnumerable<EmployeeModel>> AssignedEmpDDO(string DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_AssignedEmpDDO))//"[ODS].[AssignedEmpDDO]"
                {
                    epsdatabase.AddInParameter(dbCommand, "@DDOID", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "@case", DbType.String, "DDO");
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();
                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            ObjEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString();
                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }

        /// <summary>
        /// Get All DDO
        /// </summary>
        /// <param name="PAOID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task< IEnumerable<DropDownListModel>> GetAllDDO(string PAOID, string query)
        {
            List<DropDownListModel> listDropDownListModel = null;
            listDropDownListModel = new List<DropDownListModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    epsdatabase.AddInParameter(dbCommand, "@Query", DbType.String, query);
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.String, PAOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DropDownListModel ObjDropDownListModel = new DropDownListModel();
                            ObjDropDownListModel.Values = rdr["ID"].ToString();
                            ObjDropDownListModel.Text = rdr["Text"].ToString();
                            listDropDownListModel.Add(ObjDropDownListModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (listDropDownListModel == null)
                return null;
            else
                return listDropDownListModel;
        }
        /// <summary>
        /// Employee List
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>> EmployeeList(string DDOID)
        {
            List<EmployeeModel> ListEmployeeModel = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DDOsEmployeeList))
                {
                    epsdatabase.AddInParameter(dbCommand, "@DDOID", DbType.String, DDOID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel ObjEmployeeModel = new EmployeeModel();

                            ObjEmployeeModel.EmpCd = rdr["EmpCd"].ToString().Trim();
                            ObjEmployeeModel.DDOName = rdr["DDOName"].ToString().Trim();
                            ObjEmployeeModel.PAOName = rdr["PAOName"].ToString().Trim();
                            ObjEmployeeModel.EmpName = rdr["EmpName"].ToString().Trim();
                            ObjEmployeeModel.CommonDesigDesc = rdr["CommonDesigDesc"].ToString().Trim();
                            ObjEmployeeModel.Email = rdr["Email"].ToString().Trim();
                            ObjEmployeeModel.ActiveStatus = rdr["ActiveStatus"].ToString().Trim();

                            ListEmployeeModel.Add(ObjEmployeeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            if (ListEmployeeModel == null)
                return null;
            else
                return ListEmployeeModel;
        }
        /// <summary>
        /// Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="DDOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task< string> Assigned(string empCd, string DDOID, string active, string Password)
        {
            string Office = string.Empty;
            string status = string.Empty;
            string UserID = string.Empty;
            string RoleStatus = string.Empty;
            int NewUserID=0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.USP_PayRoleAssign))//"[ODS].[PayRoleAssign]"
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "Case", DbType.String, "DDO");
                    epsdatabase.AddInParameter(dbCommand, "DDOPermId", DbType.String, DDOID);
                    epsdatabase.AddInParameter(dbCommand, "Active", DbType.String, active);
                    epsdatabase.AddInParameter(dbCommand, "Password", DbType.String, Password);
                    epsdatabase.AddInParameter(dbCommand, "LoggedInUserID", DbType.String, 1);
                    epsdatabase.AddOutParameter(dbCommand, "Status", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "UserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "NewUserID", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "Office", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "RoleStatus", DbType.String, 50);

                    epsdatabase.ExecuteNonQuery(dbCommand);
                    status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Status")).Trim();
                    UserID = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "UserID")).Trim();
                    NewUserID = Convert.ToInt32(epsdatabase.GetParameterValue(dbCommand, "NewUserID"));
                    Office = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Office")).Trim();
                    RoleStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "RoleStatus")).Trim();
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(status) || !string.IsNullOrEmpty(UserID) || NewUserID != 0)
            {
                return status + "$" + UserID + "&" + NewUserID + "$" + Office + "$" + RoleStatus;
            }
            else
            {
                return "";
            }
        }
        #endregion
    }
}
