﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LienPeriod
{
    public class TranferofflienperiodDA
    {
        Database epsdatabase = null;

        public TranferofflienperiodDA(Database database)
        {

            epsdatabase = database;
        }
        #region Get Controller List

        public async Task<IEnumerable<ControllerList>> GetControllerList()
        {

            List<ControllerList> objControllerList = new List<ControllerList>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_LienGetControllerList)) 
                {
                    //epsdatabase.AddInParameter(dbCommand, "IfscCD", DbType.String, IfscCD);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            ControllerList objControllerListData = new ControllerList();
                            objControllerListData.controllerId = objReader["ControllerID"] != DBNull.Value ? Convert.ToInt32(objReader["ControllerID"]) : 0;
                            objControllerListData.controllerName = objReader["ControllerName"] != DBNull.Value ? Convert.ToString(objReader["ControllerName"]).Trim() :  null;
                            objControllerList.Add(objControllerListData);

                        }

                    }

                }

            });
            if (objControllerList == null)
                return null;
            else
                return objControllerList;


        }
        #endregion
        #region Get DDO LIst By Controller ID 

        public async Task<IEnumerable<DDOList>> GetDdolistbyControllerid(int controllerId)
        {

            List<DDOList> objDDOList = new List<DDOList>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_LienGetDdoListbyControllerID))
                {
                    epsdatabase.AddInParameter(dbCommand, "ControllerId", DbType.Int32, controllerId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            DDOList objControllerListData = new DDOList();
                            objControllerListData.ddoId = objReader["DDOID"] != DBNull.Value ? Convert.ToInt32(objReader["DDOID"]) : 0;
                            objControllerListData.ddoName = objReader["DDOName"] != DBNull.Value ? Convert.ToString(objReader["DDOName"]).Trim() :  null;
                            objDDOList.Add(objControllerListData);

                        }

                    }

                }

            });
            if (objDDOList == null)
                return null;
            else
                return objDDOList;


        }
        #endregion
        #region Get Office Address Details By Controllerid and DDOId 

        public async Task<IEnumerable<OfficeAddressDetails>> GetOfficeAddressDetail(int controllerId,int ddoId, string officeName)
        {

            List<OfficeAddressDetails> objOfficeAddressDetailsList = new List<OfficeAddressDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_LienGetOfficeDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "ControllerId", DbType.Int32, controllerId);
                    epsdatabase.AddInParameter(dbCommand, "ddoId", DbType.Int32, ddoId);
                    epsdatabase.AddInParameter(dbCommand, "officeName", DbType.String, officeName);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            OfficeAddressDetails objOfficeAddressListData = new OfficeAddressDetails();
                            objOfficeAddressListData.officeId = objReader["OfficeID"] != DBNull.Value ? Convert.ToString(objReader["OfficeID"]) :  null;
                            objOfficeAddressListData.officeName = objReader["OfficeName"] != DBNull.Value ? Convert.ToString(objReader["OfficeName"]) :  null;
                            objOfficeAddressListData.officeAddress = objReader["OfficeAddress"] != DBNull.Value ? Convert.ToString(objReader["OfficeAddress"]) :  null;
                            objOfficeAddressListData.officeEmailId = objReader["OfficeEmailid"] != DBNull.Value ? Convert.ToString(objReader["OfficeEmailid"]) :  null;
                            objOfficeAddressListData.officeTelePhoneNO = objReader["OfficeTel1"] != DBNull.Value ? Convert.ToString(objReader["OfficeTel1"]) :  null;
                            objOfficeAddressListData.ddoId = objReader["DDOID"] != DBNull.Value ? Convert.ToInt32(objReader["DDOID"]) :  0;
                            objOfficeAddressListData.ddoName = objReader["DDOName"] != DBNull.Value ? Convert.ToString(objReader["DDOName"]).Trim() : null;
                            objOfficeAddressListData.paoId = objReader["PAOID"] != DBNull.Value ? Convert.ToInt32(objReader["PAOID"]) :  0;
                            objOfficeAddressListData.paoName = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() :  null;

                            objOfficeAddressDetailsList.Add(objOfficeAddressListData);

                        }

                    }

                }

            });
            if (objOfficeAddressDetailsList == null)
                return null;
            else
                return objOfficeAddressDetailsList;


        }
        #endregion
        #region Get Transfer office lienPeriod Details By EmployeeCode and RoleId 

        public async Task<IEnumerable<TransferofflienperiodModel>> GetTransferofficelienPeriodDetails(string empCd,int roleId)
        {

            List<TransferofflienperiodModel> objTransferofflienperiodList = new List<TransferofflienperiodModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetTransfer_office_lien_Period_DetailsByEmpid))
                {
                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int32, roleId);
                 
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            TransferofflienperiodModel objTransferofflienperiod = new TransferofflienperiodModel();
                            objTransferofflienperiod.mslienId = objReader["MslienID"] != DBNull.Value ? Convert.ToInt32(objReader["MslienID"]) :  0;
                          
                            objTransferofflienperiod.orderNo = objReader["OrderNo"] != DBNull.Value ? Convert.ToString(objReader["OrderNo"]) : null;
                            objTransferofflienperiod.orderDate = objReader["OrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["OrderDt"]) : objTransferofflienperiod.orderDate = null;
                            objTransferofflienperiod.reasonTransfer = objReader["ReasonForTrans"] != DBNull.Value ? Convert.ToString(objReader["ReasonForTrans"]) :  null;
                            objTransferofflienperiod.remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                            objTransferofflienperiod.relevingOrderNo = objReader["RelievOrderNo"] != DBNull.Value ? Convert.ToString(objReader["RelievOrderNo"]) : null;
                            objTransferofflienperiod.relievingOrderDate = objReader["RelieveOrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RelieveOrderDt"]) : objTransferofflienperiod.relievingOrderDate = null;
                            objTransferofflienperiod.relievingTime = objReader["RelieveTime"] != DBNull.Value ? Convert.ToString(objReader["RelieveTime"]) : null;
                            objTransferofflienperiod.relievingDesigcode = objReader["RelieveDesig"] != DBNull.Value ? Convert.ToInt32(objReader["RelieveDesig"]) :  0;
                            objTransferofflienperiod.department = objReader["OffcDtlsCNO"] != DBNull.Value ? Convert.ToString(objReader["OffcDtlsCNO"]) : null;
                            objTransferofflienperiod.ddoId = objReader["PermDDOId"] != DBNull.Value ? Convert.ToInt32(objReader["PermDDOId"]) : 0;
                            objTransferofflienperiod.ddoName = objReader["DDOName"] != DBNull.Value ? Convert.ToString(objReader["DDOName"]).Trim() : null;
                            objTransferofflienperiod.paoId = objReader["POAId"] != DBNull.Value ? Convert.ToInt32(objReader["POAId"]) :  0;
                            objTransferofflienperiod.paoName = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() :  null;
                            objTransferofflienperiod.officeId = objReader["OffAddrssID"] != DBNull.Value ? Convert.ToString(objReader["OffAddrssID"]).Trim() :  null;
                            objTransferofflienperiod.officeName = objReader["OfficeName"] != DBNull.Value ? Convert.ToString(objReader["OfficeName"]).Trim() : null;
                            objTransferofflienperiod.officeAddress = objReader["officeAddress"] != DBNull.Value ? Convert.ToString(objReader["officeAddress"]).Trim() :  null;
                            objTransferofflienperiod.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            objTransferofflienperiod.veriFlag = objReader["VerifFlag"] != DBNull.Value ? Convert.ToString(objReader["VerifFlag"]).Trim() :  null;
                           
                            objTransferofflienperiod.rejectionRemark = objReader["RejectedRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectedRemark"]) :  null;

                            objTransferofflienperiodList.Add(objTransferofflienperiod);
                        }

                    }

                }

            });
            if (objTransferofflienperiodList == null)
                return null;
            else
                return objTransferofflienperiodList;


        }
        #endregion
        #region insert for Transfer Office Lien Period details
        public async Task<string> InsertUpdateTransferOffLienPeriod(TransferofflienperiodModel objtransferofflienperiod)
        {


            var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_InsertUpdateTransfer_office_lien_Period))
                {

                    epsdatabase.AddInParameter(dbCommand, "ordernumber", DbType.String, objtransferofflienperiod.orderNo);
                    epsdatabase.AddInParameter(dbCommand, "orderdate", DbType.DateTime, objtransferofflienperiod.orderDate);
                    epsdatabase.AddInParameter(dbCommand, "reasonforTransfer", DbType.String, objtransferofflienperiod.reasonTransfer);
                    epsdatabase.AddInParameter(dbCommand, "remarks", DbType.String, objtransferofflienperiod.remarks);
                    epsdatabase.AddInParameter(dbCommand, "relieving_orderno", DbType.String, objtransferofflienperiod.relevingOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "relieving_orderdate", DbType.DateTime, objtransferofflienperiod.relievingOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "time", DbType.String, objtransferofflienperiod.relievingTime);
                    epsdatabase.AddInParameter(dbCommand, "designationid", DbType.String, objtransferofflienperiod.relievingDesigcode);
                    epsdatabase.AddInParameter(dbCommand, "departmenttype", DbType.String, objtransferofflienperiod.department);
                    epsdatabase.AddInParameter(dbCommand, "officeId", DbType.String, objtransferofflienperiod.officeId);
                    epsdatabase.AddInParameter(dbCommand, "officeName", DbType.String, objtransferofflienperiod.officeName);
                    epsdatabase.AddInParameter(dbCommand, "paoid", DbType.Int32, objtransferofflienperiod.paoId);
                    epsdatabase.AddInParameter(dbCommand, "ddoid", DbType.Int32, objtransferofflienperiod.ddoId);
                    epsdatabase.AddInParameter(dbCommand, "address", DbType.String, objtransferofflienperiod.officeAddress);


                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objtransferofflienperiod.employeeCode);
                    //epsdatabase.AddInParameter(dbCommand, "deputationflag", DbType.String, objDeputationOut.depuFlag);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objtransferofflienperiod.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "createBy", DbType.String, objtransferofflienperiod.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "flagUpdate", DbType.String, objtransferofflienperiod.flagUpdate);
                    epsdatabase.AddInParameter(dbCommand, "mslienId", DbType.String, objtransferofflienperiod.mslienId);
                    epsdatabase.AddInParameter(dbCommand, "rejectionRemark", DbType.String, objtransferofflienperiod.rejectionRemark);
                    epsdatabase.AddOutParameter(dbCommand, "ERROR", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    result = result + "-" + Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ERROR")).Trim();


                  //  result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion
        #region Update forward Status By Employee Code
        public async Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo,string  status, string rejectionRemark)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_LienUpdateforwardStatusUpdateByEmpCode))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "orderNo", DbType.String, orderNo);
                    epsdatabase.AddInParameter(dbCommand, "rejectionReason", DbType.String, rejectionRemark);
                    epsdatabase.AddInParameter(dbCommand, "statusFlag", DbType.String, status);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion
        #region delete Record Office transfer lien Period by employee code And Order no
        public async Task<int> DeleteTransferOffLienPeriod(string empCd, string orderNo)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_DeletetransferOfficeLienperiod))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "orderNo", DbType.String, orderNo);
              
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion
    }
}
