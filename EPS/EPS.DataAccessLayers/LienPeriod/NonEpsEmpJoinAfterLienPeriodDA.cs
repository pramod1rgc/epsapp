﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LienPeriod
{
    public class NonEpsEmpJoinAfterLienPeriodDA
    {
        Database epsdatabase = null;

        public NonEpsEmpJoinAfterLienPeriodDA(Database database)
        {

            epsdatabase = database;
        }
        #region Get Non EPS Employee Join After Lien Period By EmployeeCode and RoleId 

        public async Task<IEnumerable<NonEpsEmpJoinAfterLienPeriodeModel>> GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId)
        {

            List<NonEpsEmpJoinAfterLienPeriodeModel> objmodel1 = new List<NonEpsEmpJoinAfterLienPeriodeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetjoinNonEpsEmployeeLienPeriod))
                {
                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int32, roleId);
            
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NonEpsEmpJoinAfterLienPeriodeModel objmodel = new NonEpsEmpJoinAfterLienPeriodeModel();
                            objmodel.msAfterLeinId= objReader["msAfterLeinID"] != DBNull.Value ? Convert.ToInt32(objReader["msAfterLeinID"]) :  0;
                      
                            objmodel.relievingOrderNo = objReader["RelievingOrderNo"] != DBNull.Value ? Convert.ToString(objReader["RelievingOrderNo"]) :  null;
                            objmodel.relievingOrderDate = objReader["RelievingOrderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RelievingOrderDate"]) : objmodel.relievingOrderDate = null;
                            
                            objmodel.relievingOffice = objReader["NonEPSRelievingOffice"] != DBNull.Value ? Convert.ToString(objReader["NonEPSRelievingOffice"]) : null;

                         
                            objmodel.relievingBy = objReader["NonEPSRelievedBy"] != DBNull.Value ? Convert.ToString(objReader["NonEPSRelievedBy"]) :  null;
                            objmodel.relievedOn = objReader["RelievedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["RelievedOn"]) : objmodel.relievedOn = null;
                        

                            objmodel.onRelievedDesignation = objReader["NonEPSDesignation"] != DBNull.Value ? Convert.ToString(objReader["NonEPSDesignation"]) :  null;
                            objmodel.payCommissionId = objReader["PayCommission"] != DBNull.Value ? Convert.ToString(objReader["PayCommission"]) :  null;
                           
                        
                            objmodel.payScale = objReader["NonEPSPayScale"] != DBNull.Value ? Convert.ToString(objReader["NonEPSPayScale"]) :  null;
                            objmodel.basicPay = objReader["BasicPay"] != DBNull.Value ? Convert.ToString(objReader["BasicPay"]) : null;
                            objmodel.joinServiceAs = objReader["JoinServiceAs"] != DBNull.Value ? Convert.ToString(objReader["JoinServiceAs"]) :  null; ;
                            objmodel.joiningonAccountof = objReader["JoiningonAccountof"] != DBNull.Value ? Convert.ToInt32(objReader["JoiningonAccountof"]) :  0;
                            objmodel.joiningOrderNo = objReader["JoiningOrderNo"] != DBNull.Value ? Convert.ToString(objReader["JoiningOrderNo"]) :  null;
                            objmodel.joiningOrderDate = objReader["JoiningOrderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["JoiningOrderDate"]) : objmodel.joiningOrderDate = null;
                            objmodel.joiningTime = objReader["JoiningTime"] != DBNull.Value ? Convert.ToString(objReader["JoiningTime"]) :  null;
                            objmodel.officeId = objReader["OfficeCode"] != DBNull.Value ? Convert.ToString(objReader["OfficeCode"]) : objmodel.officeId = null;
                            objmodel.officeCityClass = objReader["OfficeCityClass"] != DBNull.Value ? Convert.ToString(objReader["OfficeCityClass"]) :  null;
                            objmodel.joiningDesigcode = objReader["Designation"] != DBNull.Value ? Convert.ToInt32(objReader["Designation"]) :  0;

                            objmodel.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() :  null;
                            objmodel.veriFlag = objReader["VerifFlag"] != DBNull.Value ? Convert.ToString(objReader["VerifFlag"]).Trim() :  null;
                         
                            objmodel.rejectionRemark = objReader["RejectedRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectedRemark"]) :  null;
                            objmodel1.Add(objmodel);
                        }

                    }

                }

            });
            if (objmodel1 == null)
                return null;
            else
                return objmodel1;


        }
        #endregion

        #region insert NON EPS Employee Join After Lien Period
        public async Task<string> SaveUpdateNonEpsEmployeeJoinAfterLienPeriod(NonEpsEmpJoinAfterLienPeriodeModel objModel)
        {


            var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_InsertUpdateNONEPSEmployeeJoinafterlien_Period))
                {
                    epsdatabase.AddInParameter(dbCommand, "msLienid", DbType.String, objModel.msAfterLeinId);
                    epsdatabase.AddInParameter(dbCommand, "relivingordernumber", DbType.String, objModel.relievingOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "relievingorderdate", DbType.DateTime, objModel.relievingOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "relievingfromOffice", DbType.String, objModel.relievingOffice);
                    epsdatabase.AddInParameter(dbCommand, "relievinBy", DbType.String, objModel.relievingBy);
                    epsdatabase.AddInParameter(dbCommand, "relievindesignation", DbType.String, objModel.onRelievedDesignation);
                    
                    epsdatabase.AddInParameter(dbCommand, "relievingondate", DbType.DateTime, objModel.relievedOn);
                   
                    epsdatabase.AddInParameter(dbCommand, "relievingPayCommId", DbType.Int32, objModel.payCommissionId);
                    epsdatabase.AddInParameter(dbCommand, "relievingPayScale", DbType.String, objModel.payScale);
                   
                    epsdatabase.AddInParameter(dbCommand, "basicPay", DbType.Decimal, objModel.basicPay);
                    epsdatabase.AddInParameter(dbCommand, "joiningSericeId", DbType.Int32, objModel.joinServiceAs);
                    epsdatabase.AddInParameter(dbCommand, "joiningAccountOfId", DbType.Int32, objModel.joiningonAccountof);
                    epsdatabase.AddInParameter(dbCommand, "joiningOrderNo", DbType.String, objModel.joiningOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "joiningOrderDate", DbType.DateTime, objModel.joiningOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "joiningTime", DbType.String, objModel.joiningTime);
                    epsdatabase.AddInParameter(dbCommand, "joiningOfficeId", DbType.String, objModel.officeId);
                    epsdatabase.AddInParameter(dbCommand, "joiningOfficeClass", DbType.String, objModel.officeCityClass);
                    epsdatabase.AddInParameter(dbCommand, "joiningDesignationId", DbType.String, objModel.joiningDesigcode);
                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objModel.employeeCode);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objModel.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "createBy", DbType.String, objModel.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "flagUpdate", DbType.String, objModel.flagUpdate);
                    epsdatabase.AddOutParameter(dbCommand, "ERROR", DbType.String, 500);
                    result= epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    result = result +"-" +Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ERROR")).Trim();

                }
            });
            return result;

        }
        #endregion
        #region Delete NON EPS Employee Join After Lien Period by Employee Code
        public async Task<int> DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(string empCd,string orderNo)
        {


            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_deleteNONEPSEmployeeJoinafterlien_Period))
                {


                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "orderNo", DbType.String, orderNo);

                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion
        #region Update forward Status By Employee Code
        public async Task<int> NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_NONEpsEmpjoinLienUpdateforwardStatus))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "orderNo", DbType.String, orderNo);
                    epsdatabase.AddInParameter(dbCommand, "statusFlag", DbType.String, status);
                    epsdatabase.AddInParameter(dbCommand, "rejectionRemark", DbType.String, rejectionRemark);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion
    }
}
