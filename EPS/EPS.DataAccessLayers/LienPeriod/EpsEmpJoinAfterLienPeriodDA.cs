﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LienPeriod
{
    public class EpsEmpJoinAfterLienPeriodDA
    {
        Database epsdatabase = null;

        public EpsEmpJoinAfterLienPeriodDA(Database database)
        {

            epsdatabase = database;
        }
        #region Get EPS Employee Join After Lien Period By EmployeeCode and RoleId 

        public async Task<IEnumerable<EpsEmpJoinAfterLienPeriodModel>> GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId,int controllerId)
        {

            List<EpsEmpJoinAfterLienPeriodModel> objmodellist = new List<EpsEmpJoinAfterLienPeriodModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetjoinEpsEmployeeLienPeriod))
                {
                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int32, roleId);
                    epsdatabase.AddInParameter(dbCommand, "controllerid", DbType.Int32, controllerId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            EpsEmpJoinAfterLienPeriodModel objmodel = new EpsEmpJoinAfterLienPeriodModel();
                          
                            objmodel.msAfterLeinId = objReader["MsAfterLeinID"] != DBNull.Value ? Convert.ToInt32(objReader["MsAfterLeinID"]) :  0;
                            objmodel.relievingOrderNo = objReader["RelievOrderNo"] != DBNull.Value ? Convert.ToString(objReader["RelievOrderNo"]) :  null;
                            objmodel.relievingOrderDate = objReader["RelieveOrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RelieveOrderDt"]) : objmodel.relievingOrderDate = null;
                            objmodel.relievingOfficeId = objReader["OffAddrssID"] != DBNull.Value ? Convert.ToString(objReader["OffAddrssID"]) :  null;
                            objmodel.relievingOffice = objReader["RelievedOffice"] != DBNull.Value ? Convert.ToString(objReader["RelievedOffice"]) : null;
                    
                            objmodel.relievingDdoId = objReader["DDoid"] != DBNull.Value ? Convert.ToInt32(objReader["DDoid"]) : 0;
                            objmodel.relievingBy = objReader["DDOName"] != DBNull.Value ? Convert.ToString(objReader["DDOName"]) :  null;
                            objmodel.relievedOn = objReader["Relievingon"] != DBNull.Value ? Convert.ToDateTime(objReader["Relievingon"]) : objmodel.relievedOn = null;
                            objmodel.onRelievedDesignationId = objReader["ReleivingDesigid"] != DBNull.Value ? Convert.ToInt32(objReader["ReleivingDesigid"]) :  0;
                          
                            objmodel.onRelievedDesignation = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]) : null;
                            objmodel.payCommissionId = objReader["PayComm"] != DBNull.Value ? Convert.ToInt32(objReader["PayComm"]) :  0;
                            objmodel.payCommission = objReader["PayCommDesc"] != DBNull.Value ? Convert.ToString(objReader["PayCommDesc"]) :  null;
                            objmodel.payScaleId = objReader["PayScalePscScaleCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscScaleCD"]) :  0;
                            objmodel.payScale = objReader["PayScalePscDscr"] != DBNull.Value ? Convert.ToString(objReader["PayScalePscDscr"]) :  null;
                            objmodel.basicPay = objReader["BasicPay"] != DBNull.Value ? Convert.ToString(objReader["BasicPay"]) : null;
                            objmodel.joinServiceAs = objReader["JoinServiceAs"] != DBNull.Value ? Convert.ToString(objReader["JoinServiceAs"]) :  null; ;
                            objmodel.joiningonAccountof = objReader["JoiningonAccountof"] != DBNull.Value ? Convert.ToInt32(objReader["JoiningonAccountof"]) :  0;
                            objmodel.joiningOrderNo = objReader["JoiningOrderNo"] != DBNull.Value ? Convert.ToString(objReader["JoiningOrderNo"]) :  null;
                            objmodel.joiningOrderDate = objReader["JoiningOrderDate"] != DBNull.Value ? Convert.ToDateTime(objReader["JoiningOrderDate"]) : objmodel.joiningOrderDate = null;
                            objmodel.joiningTime = objReader["JoiningTime"] != DBNull.Value ? Convert.ToString(objReader["JoiningTime"]) : null;
                            objmodel.officeId = objReader["OfficeId"] != DBNull.Value ? Convert.ToString(objReader["OfficeId"]) :  null;
                            objmodel.officeCityClass = objReader["OfficeCityClass"] != DBNull.Value ? Convert.ToString(objReader["OfficeCityClass"]) : null;
                            objmodel.joiningDesigcode = objReader["Designationid"] != DBNull.Value ? Convert.ToInt32(objReader["Designationid"]) : 0;

                            objmodel.employeeCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() :  null;
                            objmodel.veriFlag = objReader["VerifFlag"] != DBNull.Value ? Convert.ToString(objReader["VerifFlag"]).Trim() :  null;
                      
                          
                            objmodel.rejectionRemark= objReader["RejectedRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectedRemark"]).Trim() :  null;

                            objmodellist.Add(objmodel);
                        }

                    }

                }

            });
            if (objmodellist == null)
                return null;
            else
                return objmodellist;


        }
        #endregion

        #region insert EPS Employee Join After Lien Period
        public async Task<string> InsertUpdateEpsEmpJoinAfterLienPeriod(EpsEmpJoinAfterLienPeriodModel objModel)
        {


            var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_InsertUpdateEPSEmployeeJoinafterlien_Period))
                {
              
                    epsdatabase.AddInParameter(dbCommand, "relivingordernumber", DbType.String, objModel.relievingOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "relievingorderdate", DbType.DateTime, objModel.relievingOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "relievingOfficeid", DbType.String, objModel.relievingOfficeId);
                    epsdatabase.AddInParameter(dbCommand, "relievingObyddoid", DbType.String, objModel.relievingDdoId);
                    epsdatabase.AddInParameter(dbCommand, "relievingondate", DbType.DateTime, objModel.relievedOn);
                    epsdatabase.AddInParameter(dbCommand, "relievingDesignationid", DbType.Int32, objModel.onRelievedDesignationId);
                    epsdatabase.AddInParameter(dbCommand, "relievingPayCommId", DbType.Int32, objModel.payCommissionId);
                    epsdatabase.AddInParameter(dbCommand, "relievingPayScaleId", DbType.Int32, objModel.payScaleId);
                    epsdatabase.AddInParameter(dbCommand, "basicPay", DbType.Decimal, objModel.basicPay);
                    epsdatabase.AddInParameter(dbCommand, "joiningSericeId", DbType.Int32, objModel.joinServiceAs);
                    epsdatabase.AddInParameter(dbCommand, "joiningAccountOfId", DbType.Int32, objModel.joiningonAccountof);
                    epsdatabase.AddInParameter(dbCommand, "joiningOrderNo", DbType.String, objModel.joiningOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "joiningOrderDate", DbType.DateTime, objModel.joiningOrderDate);
                    epsdatabase.AddInParameter(dbCommand, "joiningTime", DbType.String, objModel.joiningTime);
                    epsdatabase.AddInParameter(dbCommand, "joiningOfficeId", DbType.String, objModel.officeId);
                    epsdatabase.AddInParameter(dbCommand, "joiningOfficeClass", DbType.String, objModel.officeCityClass);
                    epsdatabase.AddInParameter(dbCommand, "joiningDesignationId", DbType.String, objModel.joiningDesigcode);
                    epsdatabase.AddInParameter(dbCommand, "employeecode", DbType.String, objModel.employeeCode);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objModel.ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "createBy", DbType.String, objModel.createdBy);
                    epsdatabase.AddInParameter(dbCommand, "flagUpdate", DbType.String, objModel.flagUpdate);
                    epsdatabase.AddInParameter(dbCommand, "msAfterLeinId", DbType.String, objModel.msAfterLeinId);
                    epsdatabase.AddOutParameter(dbCommand, "ERROR", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand).ToString();
                    result = result + "-" + Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ERROR")).Trim();
            

                }
            });
            return result;

        }
        #endregion
        #region Delete EPS Employee Join After Lien Period by Employee Code
        public async Task<int> DeleteEpsEmployeeJoinByEmpCode(int lienId)
        {


            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_deleteEPSEmployeeJoinafterlien_Period))
                {

                 
                    epsdatabase.AddInParameter(dbCommand, "MsAfterLeinID", DbType.String, lienId);
                 
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion
        #region Update forward Status By Employee Code
        public async Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionReason)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_EpsEmpjoinAfLienUpdateforwardStatusUpda))
                {

                    epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "orderNo", DbType.String, orderNo);

                    epsdatabase.AddInParameter(dbCommand, "statusFlag", DbType.String, status);
                    epsdatabase.AddInParameter(dbCommand, "rejectionReason", DbType.String, rejectionReason);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion
    }
}
