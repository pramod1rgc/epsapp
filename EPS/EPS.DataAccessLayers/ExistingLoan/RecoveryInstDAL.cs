﻿using EPS.BusinessModels.ExistingLoan;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.ExistingLoan
{
    public class RecoveryInstDAL
    {
        Database epsdatabase = null;

        public RecoveryInstDAL(Database database)
        {
            epsdatabase = database;
        }

        public async Task<IEnumerable<RecoveryInstModel>> GetEmpDetails(string EmpCd)
        {
            try
            {
            List<RecoveryInstModel> objFloodModelList = new List<RecoveryInstModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetRecovEmpDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EMPCODE", DbType.String, EmpCd.Trim());
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            RecoveryInstModel objmodel = new RecoveryInstModel();
                            objmodel.OrderNo = objReader["OrderNo"] != DBNull.Value ? Convert.ToString(objReader["OrderNo"]) : objmodel.OrderNo = "";
                            objmodel.BillMonth = objReader["BillMonth"] != DBNull.Value ? Convert.ToString(objReader["BillMonth"]).Trim() : objmodel.BillMonth = null;
                            objmodel.LoanCD = objReader["LoanCD"] != DBNull.Value ? Convert.ToString(objReader["LoanCD"]) : objmodel.LoanCD = null;
                            objmodel.payloanrefloandesc = objReader["payloanrefloandesc"] != DBNull.Value ? Convert.ToString(objReader["payloanrefloandesc"]) : objmodel.payloanrefloandesc = null;

                            objmodel.MsEmpLoanMultiInstId = objReader["MsEmpLoanMultiInstId"] != DBNull.Value ? Convert.ToInt32(objReader["MsEmpLoanMultiInstId"]) : objmodel.MsEmpLoanMultiInstId = 0;
                            objmodel.EmpCD = objReader["EmpCD"] != DBNull.Value ? Convert.ToString(objReader["EmpCD"]) : objmodel.EmpCD = "";
                            objmodel.payloanrefloandesc = objReader["payloanrefloandesc"] != DBNull.Value ? Convert.ToString(objReader["payloanrefloandesc"]) : objmodel.payloanrefloandesc = null;
                            objmodel.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : objmodel.StatusId = 0;

                            objFloodModelList.Add(objmodel);
                        }
                    }
                }
            });
            if (objFloodModelList.Count == 0)
                return null;
            else
                return objFloodModelList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<IEnumerable<SanctionDetailsModel>> BindSanctionDetails(string LoanCd, string EmpCd)
        {
            List<SanctionDetailsModel> objSanctionModelList = new List<SanctionDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_EmpSanctionDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EMPCODE", DbType.String, EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "LoanCd", DbType.Int16, Convert.ToInt16(LoanCd));
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            SanctionDetailsModel objmodel = new SanctionDetailsModel();
                            objmodel.SancOrdDT = objReader["SancOrdDT"] != DBNull.Value ? Convert.ToString(objReader["SancOrdDT"]) : objmodel.SancOrdDT = "";
                            objmodel.SancOrdNo = objReader["SancOrdNo"] != DBNull.Value ? Convert.ToString(objReader["SancOrdNo"]).Trim() : objmodel.SancOrdNo = null;
                            objmodel.LoanAmtDisbursed = objReader["LoanAmtDisbursed"] != DBNull.Value ? Convert.ToInt32(objReader["LoanAmtDisbursed"]) : objmodel.LoanAmtDisbursed = 0;
                            objmodel.PriTotInst = objReader["PriTotInst"] != DBNull.Value ? Convert.ToInt32(objReader["PriTotInst"]) : objmodel.PriTotInst = null;
                            objmodel.PriInstAmt = objReader["PriInstAmt"] != DBNull.Value ? Convert.ToInt32(objReader["PriInstAmt"]) : objmodel.PriTotInst = null;
                            objmodel.OddInstNoPri = objReader["OddInstNoPri"] != DBNull.Value ? Convert.ToInt32(objReader["OddInstNoPri"]) : objmodel.OddInstNoPri = null;
                            objmodel.OddInstAmtPri = objReader["OddInstAmtPri"] != DBNull.Value ? Convert.ToInt32(objReader["OddInstAmtPri"]) : objmodel.OddInstAmtPri = null;
                            objmodel.PriLstInstRec = objReader["PriLstInstRec"] != DBNull.Value ? Convert.ToInt32(objReader["PriLstInstRec"]) : objmodel.PriLstInstRec = null;
                            objmodel.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : objmodel.StatusId = null;

                            objmodel.OutstandingAmt = objmodel.LoanAmtDisbursed - (objmodel.PriTotInst * objmodel.PriInstAmt);
                            objSanctionModelList.Add(objmodel);
                        }
                    }
                }
            });
            if (objSanctionModelList.Count == 0)
                return null;
            else
                return objSanctionModelList;
        }



        public async Task<int> SaveSanctionData(SnactionDataModel ObjSnactionModel)
        {
            var result = 0;
            string IpAddrss = GetIP();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_SaveInstRecovDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, ObjSnactionModel.mode);
                    epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, ObjSnactionModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, ObjSnactionModel.orderNo);
                    epsdatabase.AddInParameter(dbCommand, "OrderDt", DbType.DateTime, ObjSnactionModel.OrderDt);
                    epsdatabase.AddInParameter(dbCommand, "IntRecov", DbType.String, ObjSnactionModel.recoveredInstNo);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, IpAddrss);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;
        }

        public async Task<IEnumerable<InstEmpModel>> GetInstEmpDetails(string EmpCd)
        {
            List<InstEmpModel> objSanctionModelList = new List<InstEmpModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetInstEmpDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EMPCODE", DbType.String, EmpCd.Trim());
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            InstEmpModel objmodel = new InstEmpModel();
                            objmodel.OrderNo = objReader["OrderNo"] != DBNull.Value ? Convert.ToString(objReader["OrderNo"]).Trim() : objmodel.OrderNo = null;
                            objmodel.OrderDt = objReader["OrderDt"] != DBNull.Value ? Convert.ToDateTime(objReader["OrderDt"]) : objmodel.OrderDt = null;
                            objmodel.TotAmtRecov = objReader["TotAmtRecov"] != DBNull.Value ? Convert.ToInt32(objReader["TotAmtRecov"]) : objmodel.TotAmtRecov = 0;
                            objmodel.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : objmodel.StatusId = null;
                            objmodel.BillMonth = objReader["BillMonth"] != DBNull.Value ? Convert.ToInt32(objReader["BillMonth"]) : objmodel.BillMonth = null;

                            objSanctionModelList.Add(objmodel);
                        }
                    }
                }
            });
            if (objSanctionModelList.Count == 0)
                return null;
            else
                return objSanctionModelList;
        }



        public async Task<int> ForwardtoDDOChecker(SnactionDataModel ObjSnactionModel)
        {
            var result = 0; try
            {
       
            string IpAddrss = GetIP();
            await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_SaveInstRecovDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, ObjSnactionModel.mode);
                        epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, ObjSnactionModel.EmpCd);
                        epsdatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, ObjSnactionModel.orderNo);
                        epsdatabase.AddInParameter(dbCommand, "OrderDt", DbType.Date, ObjSnactionModel.OrderDt);
                        epsdatabase.AddInParameter(dbCommand, "IntRecov", DbType.String, ObjSnactionModel.recoveredInstNo);
                        epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, IpAddrss);
                        result = epsdatabase.ExecuteNonQuery(dbCommand);

                    }
                });

            return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<int> Accept(string EmpCD)
        {
            var result = 0;
            try
            {
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_InstRecovChangeStatus))
                    {
                        epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, 1);
                        epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, EmpCD);
                        result = epsdatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> Reject(string EmpCD)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_InstRecovChangeStatus))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, 2);
                    epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, EmpCD);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }



        public async Task<int> EditLoanDetailsById(SnactionDataModel ObjModel)
        {
            var result = 0;
            string IpAddrss = GetIP();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_UpdateDeleteInstRecord)) //[dbo].[UpdateDeleteInstRecord]
                {
                    epsdatabase.AddInParameter(dbCommand, "case", DbType.Int16, 1);
                    //epsdatabase.AddInParameter(dbCommand, "msEmpLoanMultiInstId", DbType.String, ObjModel.MsEmpLoanMultiInstId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, ObjModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, ObjModel.orderNo);
                    epsdatabase.AddInParameter(dbCommand, "OrderDt", DbType.Date, ObjModel.OrderDt); 
                    epsdatabase.AddInParameter(dbCommand, "NoInstDed", DbType.String, ObjModel.recoveredInstNo);
                    epsdatabase.AddInParameter(dbCommand, "msEmpLoanMultiInstId", DbType.Int16, ObjModel.MsEmpLoanMultiInstId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> DeleteLoanDetailsById(int msEmpLoanMultiInstId, string empCD)
        {
            var result = 0;
            string IpAddrss = GetIP();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_UpdateDeleteInstRecord)) //[dbo].[UpdateDeleteInstRecord]
                {
                    epsdatabase.AddInParameter(dbCommand, "case", DbType.Int16, 2);
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCD);
                    epsdatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, "0");
                    epsdatabase.AddInParameter(dbCommand, "OrderDt", DbType.Date,DateTime.Now);
                    epsdatabase.AddInParameter(dbCommand, "NoInstDed", DbType.String, "0");
                    epsdatabase.AddInParameter(dbCommand, "msEmpLoanMultiInstId", DbType.Int16, msEmpLoanMultiInstId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        

        #region Get IP
        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        #endregion

    }
}