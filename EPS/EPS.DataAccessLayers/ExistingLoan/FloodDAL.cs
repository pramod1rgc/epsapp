using EPS.BusinessModels.ExistingLoan;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.ExistingLoan
{
    public class FloodDAL
    {
        Database epsdatabase = null;

        public FloodDAL(Database database)
        {
            epsdatabase = database;
        }
       //string con = ConfigurationManager.ConnectionStrings["EPSConnection"].ToString();

        public async Task<IEnumerable<FloodModel>> GetEmpDetails(int BillGrID, string desigId, int UserID,int mode)
        {
            try
            {
                List<FloodModel> objFloodModelList = new List<FloodModel>();
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillEmployee))
                    {
  
                        epsdatabase.AddInParameter(dbCommand, "DesigSrNo", DbType.Int16, Convert.ToInt32(desigId));
                        epsdatabase.AddInParameter(dbCommand, "PayBillGroupId", DbType.Int32, BillGrID);
                        epsdatabase.AddInParameter(dbCommand, "userid", DbType.Int32, UserID);
                        epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int32, mode);

                        using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                        {
                            while (objReader.Read())
                            {
                                FloodModel objmodel = new FloodModel();
                                objmodel.EmpFirstName = objReader["EmpFirstName"] != DBNull.Value ? Convert.ToString(objReader["EmpFirstName"]) : objmodel.EmpFirstName = "";
                                objmodel.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : objmodel.EmpCd = null;
                                //objmodel.empdesigcd = objReader["empdesigcd"] != DBNull.Value ? Convert.ToString(objReader["empdesigcd"]).Trim() : objmodel.empdesigcd = null;
                                objmodel.DesigDesc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : objmodel.DesigDesc = null;
                                objmodel.PriVerifFlag = objReader["PriVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["PriVerifFlag"]) : objmodel.PriVerifFlag = null;
                                objFloodModelList.Add(objmodel);
                            }
                        }
                    }
                });
                if (objFloodModelList.Count==0)
                    return null;
                else
                    return objFloodModelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjSanctionDetails"></param>
        /// <returns></returns>

        public async Task<int> InsertFloodData(SanctionDetailsMOdel ObjSanctionDetails)
        {
            try
            {
                var result = 0;
                string IpAddress = GetIP();
                //DataTable LoanSanctionTable = ToDataTable(ObjSanctionDetails.totalEmpcode);
                DataTable dt = new DataTable();
                dt.TableName = "tblMsEmpLoanDetailsType";
                DataColumn dc = new DataColumn("Id", typeof(int));
                dt.Columns.Add(dc);
                dc = new DataColumn("totalEmpcode", typeof(string));
                dt.Columns.Add(dc);
                for (int i = 0; i < ObjSanctionDetails.totalEmpcode.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i + 1;
                    dr[1] = ObjSanctionDetails.totalEmpcode[i].ToString();
                    dt.Rows.InsertAt(dr, i);
                }               
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertFloodData))
                    {
                        epsdatabase.AddInParameter(dbCommand, "CreatedIP", DbType.String, IpAddress);
                        epsdatabase.AddInParameter(dbCommand, "SancOrdNo", DbType.String, ObjSanctionDetails.SancOrdNo);
                        epsdatabase.AddInParameter(dbCommand, "SancOrdDt", DbType.String, ObjSanctionDetails.SancOrdDt);
                        //epsdatabase.AddInParameter(dbCommand, "LoanAmtDisbursed", DbType.Int16, ObjSanctionDetails.LoanAmtDisbursed);
                        epsdatabase.AddInParameter(dbCommand, "SchemeId", DbType.Int32, ObjSanctionDetails.SchemeId);
                        epsdatabase.AddInParameter(dbCommand, "PriTotInst", DbType.Int32, ObjSanctionDetails.PriTotInst);

                        epsdatabase.AddInParameter(dbCommand, "PriBalance", DbType.Int32, ObjSanctionDetails.PriBalance);
                        epsdatabase.AddInParameter(dbCommand, "PriInstAmt", DbType.Int16, ObjSanctionDetails.PriInstAmt);
                        epsdatabase.AddInParameter(dbCommand, "PriLstInstRec", DbType.Int16, ObjSanctionDetails.PriLstInstRec);
                        //epsdatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, "12345"); 
                        // dbCommand.Parameters.Add(new SqlParameter("LoanSanction", LoanSanctionTable) { SqlDbType = SqlDbType.Structured });

                        SqlParameter para = new SqlParameter("EmpLoanDetailsType", dt);
                        para.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(para);
                        result = epsdatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<int> ForwordToDdo(SanctionDetailsMOdel ObjSanctionDetails)
        {
            try
            {
                //DataTable dt = ToDataTable(ObjSanctionDetails.totalEmpcode);
                DataTable dt = new DataTable();
                dt.TableName = "tblMsEmpLoanDetailsType";
                DataColumn dc = new DataColumn("Id", typeof(int));
                dt.Columns.Add(dc);
                dc = new DataColumn("totalEmpcode", typeof(string));
                dt.Columns.Add(dc);
                for (int i = 0; i < ObjSanctionDetails.totalEmpcode.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i + 1;
                    dr[1] = ObjSanctionDetails.totalEmpcode[i].ToString();
                    dt.Rows.InsertAt(dr, i);
                }

                var result = 0;
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_FloodForwordToDdo))
                    {
                        //epsdatabase.AddInParameter(dbCommand, "PriVerifFlag", DbType.String, ObjSanctionDetails.PriVerifFlag);
                        epsdatabase.AddInParameter(dbCommand, "Case", DbType.Int16, ObjSanctionDetails.Mode);
                        epsdatabase.AddInParameter(dbCommand, "SancOrdNo", DbType.String, ObjSanctionDetails.SancOrdNo);     
                        epsdatabase.AddInParameter(dbCommand, "SancOrdDt", DbType.String, ObjSanctionDetails.SancOrdDt);
                        //epsdatabase.AddInParameter(dbCommand, "LoanAmtDisbursed", DbType.Int16, ObjSanctionDetails.LoanAmtDisbursed);
                        epsdatabase.AddInParameter(dbCommand, "SchemeId", DbType.Int32, ObjSanctionDetails.SchemeId);
                        epsdatabase.AddInParameter(dbCommand, "PriTotInst", DbType.Int32, ObjSanctionDetails.PriTotInst);
                        epsdatabase.AddInParameter(dbCommand, "PriInstAmt", DbType.Int16, ObjSanctionDetails.PriInstAmt);
                        epsdatabase.AddInParameter(dbCommand, "PriLstInstRec", DbType.Int16, ObjSanctionDetails.PriLstInstRec);

                        SqlParameter para = new SqlParameter("EmpLoanDetailsType", dt);
                        para.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(para);
                        result = epsdatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get IP
        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        #endregion


        public DataTable ToDataTable<T>(List<T> items)
        {
            try
            {
                DataTable dataTable = new DataTable(typeof(T).Name);

                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                //put a breakpoint here and check datatable
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}

