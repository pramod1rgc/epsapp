﻿using EPS.BusinessModels.ExistingLoan;
using EPS.CommonClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.ExistingLoan
{
    public class ExistingLoanAccessLayer
    {
        private static IHttpContextAccessor accessor;
        Database epsdatabase = null;
        public ExistingLoanAccessLayer(Database database)
        {
            epsdatabase = database;
        }


        public async Task<IEnumerable<tblMsPayLoanRef>> BindLoanType()
        {
            List<tblMsPayLoanRef> lstPayLoanRef = new List<tblMsPayLoanRef>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "4");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            tblMsPayLoanRef ObjMstPayLoanRef = new tblMsPayLoanRef();
                            ObjMstPayLoanRef.PayLoanRefLoanCD = Convert.ToInt32(objReader["PayLoanRefLoanCD"]);
                            ObjMstPayLoanRef.PayLoanRefLoanShortDesc = Convert.ToString(objReader["PayLoanRefLoanShortDesc"]);
                            ObjMstPayLoanRef.PayLoanRefLoanHeadACP = Convert.ToString(objReader["PayLoanRefLoanHeadACP"]);
                            lstPayLoanRef.Add(ObjMstPayLoanRef);
                        }
                    }
                }
            });
            if (lstPayLoanRef.Count == 0)
                return null;
            else
                return lstPayLoanRef;
        }

        public async Task<LoanAdvanceDetailsModel> GetAlreadyTakenLoanAdvanceDetails(int PermDdoId, string EmpCode)
        {
            LoanAdvanceDetailsModel loanAdvanceModel = new LoanAdvanceDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetAlreadyTakenLoanAdvanceDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "PermDdoId", DbType.Int16, PermDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EMPCODE", DbType.String, EmpCode);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            loanAdvanceModel.MSPayAdvEmpdetID = Convert.ToInt32(objReader["MSPayAdvEmpdetID"].ToString());
                            loanAdvanceModel.EmpCd = objReader["EmpCD"].ToString();
                            loanAdvanceModel.PayLoanRefLoanShortDesc = objReader["PayLoanRefLoanShortDesc"].ToString();
                            loanAdvanceModel.sancOrdNo = objReader["SancOrdNo"].ToString();
                            loanAdvanceModel.sancOrdDT = Convert.ToDateTime(objReader["SancOrdDT"].ToString());
                            loanAdvanceModel.loanAmtSanc = Convert.ToInt32(objReader["LoanAmtSanc"].ToString());
                            loanAdvanceModel.loanAmtDisbursed = Convert.ToInt32(objReader["LoanAmtDisbursed"].ToString());
                            loanAdvanceModel.priInstAmt = Convert.ToInt32(objReader["PriInstAmt"].ToString());
                            loanAdvanceModel.IntTotInst = Convert.ToInt32(objReader["IntTotInst"].ToString());
                            loanAdvanceModel.oddInstAmtInt = Convert.ToInt32(objReader["OddInstAmtInt"].ToString());
                            loanAdvanceModel.LoanCd = Convert.ToInt32(objReader["LoanCd"].ToString());
                            loanAdvanceModel.oddInstNoInt = Convert.ToInt32(objReader["oddInstNoInt"].ToString());
                            loanAdvanceModel.oddInstNoPri = Convert.ToInt32(objReader["OddInstNoPri"].ToString());
                            loanAdvanceModel.payLoanRefLoanCD = Convert.ToInt32(objReader["LoanCd"].ToString());
                            loanAdvanceModel.oddInstAmtPri = Convert.ToInt32(objReader["OddInstAmtPri"].ToString());
                            loanAdvanceModel.PSchemeID = Convert.ToInt32(objReader["PSchemeID"].ToString());
                            loanAdvanceModel.SchemeCode = Convert.ToString(objReader["SchemeCode"].ToString());
                            loanAdvanceModel.PayLoanRefLoanHeadACP = objReader["PayLoanRefLoanHeadACP"].ToString();
                            loanAdvanceModel.priTotInst = Convert.ToInt32(objReader["PriTotInst"].ToString());
                            loanAdvanceModel.PriVerifFlag = objReader["PriVerifFlag"].ToString();
                            loanAdvanceModel.totalInstallment = loanAdvanceModel.oddInstNoPri + loanAdvanceModel.priTotInst;

                        }
                    }
                }
            });
            if (loanAdvanceModel == null)
                return null;
            else
                return loanAdvanceModel;
        }
        public async Task<HeadAccountModel> GetHeadAccountDetails(int LoanCD)
        {
            HeadAccountModel ObjHeadAccount = new HeadAccountModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetHeadAccount))
                {
                    epsdatabase.AddInParameter(dbCommand, "PayLoanRefLoanCD", DbType.Int16, LoanCD);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ObjHeadAccount.PayLoanRefLoanCD = Convert.ToInt32(objReader["PayLoanRefLoanCD"].ToString());
                            ObjHeadAccount.PayLoanRefLoanShortDesc = objReader["PayLoanRefLoanShortDesc"].ToString();
                            ObjHeadAccount.PayLoanRefLoanHeadACP = objReader["PayLoanRefLoanHeadACP"].ToString();
                        }
                    }
                }
            });
            if (ObjHeadAccount == null)
                return null;
            else
                return ObjHeadAccount;
        }

        //#region Get IP
        //private string GetIP()
        //{
        //    string strHostName = "";
        //    strHostName = System.Net.Dns.GetHostName();
        //    IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
        //    IPAddress[] addr = ipEntry.AddressList;
        //    return addr[addr.Length - 1].ToString();
        //}
        //#endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// 
        public async Task<int> SaveExistingLoanDetails(ExistingLoanApplicationModel objModel)
        {
            int result = 0;
            string IpAddress = CommonFunctions.GetClientIP(accessor);

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertExistingLoanApplicationDetails))
            {
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int32, objModel.mode);
                epsdatabase.AddInParameter(dbCommand, "MSPayAdvEmpdetID", DbType.Int32, objModel.MSPayAdvEmpdetID);
                epsdatabase.AddInParameter(dbCommand, "LoanCd", DbType.Int32, Convert.ToInt16(objModel.LoanCd));

                epsdatabase.AddInParameter(dbCommand, "empCd", DbType.String, objModel.EmpCd.Trim());
                epsdatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, objModel.ddoid);

                if (objModel.PriInstAmt == null)
                {
                    objModel.PriInstAmt = 0;
                    epsdatabase.AddInParameter(dbCommand, "priInstAmt", DbType.Int32, objModel.PriInstAmt);
                }
                else
                {
                    epsdatabase.AddInParameter(dbCommand, "PriInstAmt", DbType.Int32, objModel.PriInstAmt);
                }
                epsdatabase.AddInParameter(dbCommand, "loanAmtSanc", DbType.Int32, objModel.LoanAmtSanc);
                epsdatabase.AddInParameter(dbCommand, "loanAmtDisbursed", DbType.Int32, objModel.LoanAmtDisbursed);
                epsdatabase.AddInParameter(dbCommand, "OddInstAmtPri", DbType.Int32, objModel.OddInstAmtPri);
                epsdatabase.AddInParameter(dbCommand, "OddInstNoPri", DbType.Int32, objModel.OddInstNoPri);

                epsdatabase.AddInParameter(dbCommand, "oddInstNoInt", DbType.Int32, objModel.OddInstNoInt);
                epsdatabase.AddInParameter(dbCommand, "sancOrdDT", DbType.DateTime, objModel.SancOrdDt);
                epsdatabase.AddInParameter(dbCommand, "sancOrdNo", DbType.String, objModel.SancOrdNo);

                if (objModel.PriTotInst == null)
                {
                    objModel.PriTotInst = 0;
                    epsdatabase.AddInParameter(dbCommand, "PriTotInst", DbType.Int32, objModel.PriTotInst);
                }
                else
                {
                    epsdatabase.AddInParameter(dbCommand, "PriTotInst", DbType.Int32, objModel.PriTotInst);
                }
                epsdatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, IpAddress);
                result = epsdatabase.ExecuteNonQuery(dbCommand);
            }
            return result;
        }



        public async Task<IEnumerable<LoanAdvanceDetailsModel>> GetExistLoanDetailsByID(string MSPayAdvEmpdetID)
        {
            List<LoanAdvanceDetailsModel> ListEmployeeModel = new List<LoanAdvanceDetailsModel>();

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetExistLoanByID))
            {
                epsdatabase.AddInParameter(dbCommand, "MSPayAdvEmpdetID", DbType.String, MSPayAdvEmpdetID);
                await Task.Run(() =>
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            LoanAdvanceDetailsModel loanAdvanceModel = new LoanAdvanceDetailsModel();
                            loanAdvanceModel.PayLoanRefLoanShortDesc = objReader["PayLoanRefLoanShortDesc"] != DBNull.Value ? Convert.ToString(objReader["PayLoanRefLoanShortDesc"]) : loanAdvanceModel.PayLoanRefLoanShortDesc = "";
                            loanAdvanceModel.sancOrdNo = objReader["sancOrdNo"] != DBNull.Value ? Convert.ToString(objReader["sancOrdNo"]) : loanAdvanceModel.sancOrdNo = "";
                            loanAdvanceModel.sancOrdDT = objReader["sancOrdDT"] != DBNull.Value ? Convert.ToDateTime(objReader["sancOrdDT"]) : loanAdvanceModel.sancOrdDT = null;
                            loanAdvanceModel.loanAmtSanc = objReader["loanAmtSanc"] != DBNull.Value ? Convert.ToInt32(objReader["loanAmtSanc"]) : loanAdvanceModel.loanAmtSanc = null;
                            loanAdvanceModel.loanAmtDisbursed = objReader["loanAmtDisbursed"] != DBNull.Value ? Convert.ToInt32(objReader["loanAmtDisbursed"]) : loanAdvanceModel.loanAmtDisbursed = null;
                            loanAdvanceModel.priInstAmt = objReader["priInstAmt"] != DBNull.Value ? Convert.ToInt32(objReader["priInstAmt"]) : loanAdvanceModel.priInstAmt = null;
                            loanAdvanceModel.IntTotInst = objReader["IntTotInst"] != DBNull.Value ? Convert.ToInt32(objReader["IntTotInst"]) : loanAdvanceModel.IntTotInst = null;
                            loanAdvanceModel.oddInstAmtInt = objReader["oddInstAmtInt"] != DBNull.Value ? Convert.ToInt32(objReader["oddInstAmtInt"]) : loanAdvanceModel.oddInstAmtInt = null;
                            loanAdvanceModel.LoanCd = objReader["LoanCd"] != DBNull.Value ? Convert.ToInt32(objReader["LoanCd"]) : loanAdvanceModel.LoanCd = null;
                            //loanAdvanceModel.PayLoanRefLoanCD = objReader["PayLoanRefLoanCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayLoanRefLoanCD"]) : loanAdvanceModel.PayLoanRefLoanCD = null;priTotInst

                            loanAdvanceModel.priTotInst = objReader["priTotInst"] != DBNull.Value ? Convert.ToInt32(objReader["priTotInst"]) : loanAdvanceModel.priTotInst = null;
                            loanAdvanceModel.oddInstNoInt = objReader["oddInstNoInt"] != DBNull.Value ? Convert.ToInt32(objReader["oddInstNoInt"]) : loanAdvanceModel.oddInstNoInt = null;
                            loanAdvanceModel.oddInstNoPri = objReader["oddInstNoPri"] != DBNull.Value ? Convert.ToInt32(objReader["oddInstNoPri"]) : loanAdvanceModel.oddInstNoPri = null;
                            loanAdvanceModel.RecoveryStatus = objReader["RecoveryStatus"] != DBNull.Value ? Convert.ToString(objReader["RecoveryStatus"]) : loanAdvanceModel.RecoveryStatus = "";
                            loanAdvanceModel.payLoanRefLoanCD = objReader["payLoanRefLoanCD"] != DBNull.Value ? Convert.ToInt32(objReader["payLoanRefLoanCD"]) : loanAdvanceModel.payLoanRefLoanCD = 0;
                            loanAdvanceModel.oddInstAmtPri = objReader["oddInstNoPri"] != DBNull.Value ? Convert.ToInt32(objReader["oddInstNoPri"]) : loanAdvanceModel.oddInstAmtPri = null;
                            loanAdvanceModel.PSchemeID = objReader["PSchemeID"] != DBNull.Value ? Convert.ToInt32(objReader["PSchemeID"]) : loanAdvanceModel.PSchemeID = null;
                            loanAdvanceModel.SchemeCode = objReader["SchemeCode"] != DBNull.Value ? Convert.ToString(objReader["SchemeCode"]) : loanAdvanceModel.SchemeCode = "";
                            loanAdvanceModel.PayLoanRefLoanHeadACP = objReader["PayLoanRefLoanHeadACP"] != DBNull.Value ? Convert.ToString(objReader["PayLoanRefLoanHeadACP"]) : loanAdvanceModel.PayLoanRefLoanHeadACP = "";
                            loanAdvanceModel.PriVerifFlag = objReader["PriVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["PriVerifFlag"]) : loanAdvanceModel.PriVerifFlag = "";
                            loanAdvanceModel.totalInstallment = loanAdvanceModel.oddInstNoPri + loanAdvanceModel.priTotInst;
                            ListEmployeeModel.Add(loanAdvanceModel);
                        }
                    }
                });
                if (ListEmployeeModel.Count == 0)
                    return null;
                else
                    return ListEmployeeModel;
            }

        }

        #region Get Loan List Data

        public async Task<IEnumerable<LoanAdvanceDetailsModel>> GetEmpLoanListDetails(string PayBillGpID)
        {
            List<LoanAdvanceDetailsModel> ListEmployeeModel = new List<LoanAdvanceDetailsModel>();

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sp_EmpSanctionDetails))
            {
                epsdatabase.AddInParameter(dbCommand, "PayBillGpID", DbType.String, PayBillGpID);
                await Task.Run(() =>
                {
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        LoanAdvanceDetailsModel loanAdvanceModel = new LoanAdvanceDetailsModel();
                        while (rdr.Read())
                        {
                            loanAdvanceModel.EmpCd = rdr["empCode"].ToString();
                            loanAdvanceModel.PayLoanRefLoanShortDesc = rdr["LoanDisc"].ToString();
                            loanAdvanceModel.sancOrdNo = rdr["SancOrdNo"].ToString();
                            loanAdvanceModel.sancOrdDT = Convert.ToDateTime(rdr["SancOrdDT"].ToString());
                            loanAdvanceModel.loanAmtSanc = Convert.ToInt32(rdr["LoanAmtSanc"].ToString());
                            loanAdvanceModel.loanAmtDisbursed = Convert.ToInt32(rdr["LoanAmtDisbursed"].ToString());
                            loanAdvanceModel.priInstAmt = Convert.ToInt32(rdr["PriInstAmt"].ToString());
                            loanAdvanceModel.IntTotInst = Convert.ToInt32(rdr["IntTotInst"].ToString());
                            loanAdvanceModel.oddInstAmtInt = Convert.ToInt32(rdr["OddInstAmtInt"].ToString());
                            loanAdvanceModel.LoanCd = Convert.ToInt32(rdr["LoanCd"].ToString());
                            loanAdvanceModel.oddInstNoInt = Convert.ToInt32(rdr["oddInstNoInt"].ToString());
                            loanAdvanceModel.oddInstNoPri = Convert.ToInt32(rdr["OddInstNoPri"].ToString());
                            loanAdvanceModel.oddInstAmtPri = Convert.ToInt32(rdr["OddInstAmtPri"].ToString());
                            loanAdvanceModel.EmpLoanDetailsID = Convert.ToInt32(rdr["id"].ToString());
                            loanAdvanceModel.LoanDisc = rdr["PayLoanRefLoanDesc"].ToString();
                            loanAdvanceModel.status = Convert.ToBoolean(rdr["Status"].ToString());
                            loanAdvanceModel.IntVerifFlag = Convert.ToBoolean(rdr["IntVerifFlag"].ToString());
                            loanAdvanceModel.RecoveryStatus = rdr["RecoveryStatus"].ToString();
                            loanAdvanceModel.SNo = Convert.ToInt32(rdr["SNo"].ToString());
                            ListEmployeeModel.Add(loanAdvanceModel);
                        }
                    }
                });
                if (ListEmployeeModel.Count == 0)
                    return null;
                else
                    return ListEmployeeModel;
            }
        }

        public async Task<IEnumerable<ExistingLaonStatusModel>> BindExistingLoanStatus(int ddoId, int UserId, int userRoleID)
        {
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanStatus))
            {
                List<ExistingLaonStatusModel> ObjStatus = new List<ExistingLaonStatusModel>();
                if (userRoleID == 6)
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, 4);
                }
                else
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, 5);
                }
                epsdatabase.AddInParameter(dbCommand, "userid", DbType.Int16, UserId);
                epsdatabase.AddInParameter(dbCommand, "loginddoid", DbType.Int16, ddoId);

                await Task.Run(() =>
               {
                   using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                   {
                       while (objReader.Read())
                       {
                           ExistingLaonStatusModel MstStatus = new ExistingLaonStatusModel();
                           MstStatus.MSPayAdvEmpdetID = Convert.ToInt32(objReader["Id"]);
                           MstStatus.StatusId = Convert.ToInt32(objReader["StatusId"]);
                           MstStatus.EmpCD = Convert.ToString(objReader["EMPCode"]);
                           MstStatus.Description = Convert.ToString(objReader["ModuleStatus"]);
                           MstStatus.PayLoanRefLoanShortDesc = Convert.ToString(objReader["LoanType"]);
                           MstStatus.EmpPersVerifFlag = Convert.ToString(objReader["Status"]);
                           MstStatus.LoanCd = Convert.ToInt32(objReader["LoanCd"]);
                           ObjStatus.Add(MstStatus);
                       }
                       #endregion
                   }
               });
                if (ObjStatus.Count == 0)
                    return null;
                else
                    return ObjStatus;
            }
        }

        public async Task<int> ExistingLoanForwordToDDO(ExistingLoanApplicationModel objModel)
        {
            int result = 0;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_ExistingLoanForwordToDDO))
            {
                epsdatabase.AddInParameter(dbCommand, "MSPayAdvEmpdetID", DbType.Int32, objModel.MSPayAdvEmpdetID);
                epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, objModel.EmpCd);
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, objModel.mode);
                result = epsdatabase.ExecuteNonQuery(dbCommand);
            }
            return result;
        }


        public async Task<int> DeleteEmpDetails(string CtrCode, string msPayAdvEmpdetID,int Mode)
        {
            int result = 0;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_ExistingLoanForwordToDDO))
            {
                epsdatabase.AddInParameter(dbCommand, "MSPayAdvEmpdetID", DbType.Int32, msPayAdvEmpdetID);
                epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, CtrCode);
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, Mode);
                result = epsdatabase.ExecuteNonQuery(dbCommand);
            }
            return result;
        }


    }
}