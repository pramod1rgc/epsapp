﻿using EPS.BusinessModels.Shared;
using EPS.BusinessModels.Suspension;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Suspension
{
    public class SuspensionDAL
    {
        //readonly string con = ConfigurationManager.ConnectionStrings["EPSConnection"].ConnectionString;
       private Database _epsDatabase;

        public SuspensionDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<DesignationModel>> GetAllDesignation(string id)
        {
            List<DesignationModel> listOfdesig = new List<DesignationModel>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetAllDesignation))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Id", DbType.String, string.Empty);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DesignationModel ObjDesignationModel = new DesignationModel
                            {
                                DesigId = Convert.ToInt32(rdr["DesigNo"]),
                                DesigDesc = rdr["DesigDesc"].ToString()
                            };
                            listOfdesig.Add(ObjDesignationModel);
                        }
                    }
                }
            });

            return listOfdesig;
        }

        public async Task<IEnumerable<EmpModel>> GetAllEmployees(int desigId, bool isCheckerLogin, string comName)
        {
            List<EmpModel> listOfemp = new List<EmpModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetAllEmployees))
                {
                    _epsDatabase.AddInParameter(dbCommand, "desigId", DbType.String, desigId);
                    _epsDatabase.AddInParameter(dbCommand, "isCheckerLogin", DbType.Boolean, isCheckerLogin);
                    _epsDatabase.AddInParameter(dbCommand, "comName", DbType.String, comName);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmpModel objemp = new EmpModel
                            {
                                EmpCd = rdr["Empcd"].ToString(),
                                EmpName = rdr["empName"].ToString()
                            };
                            listOfemp.Add(objemp);
                        }
                    }
                }
            });

            return listOfemp;
        }

        public async Task<IEnumerable<RegulariseDetailse>> GetRegulariseDetails(int joiningId, bool isCheckerLogin)
        {
            List<RegulariseDetailse> listOfRegDetails = new List<RegulariseDetailse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetRegulariseDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "joiningId", DbType.Int32, joiningId);
                    _epsDatabase.AddInParameter(dbCommand, "isCheckerLogin", DbType.Boolean, isCheckerLogin);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            RegulariseDetailse objreg = new RegulariseDetailse
                            {
                                RegulariseId = Convert.ToInt32(rdr["RegulariseId"]),
                                JoiningId = Convert.ToInt32(rdr["JoiningId"]),
                                SuspenCat = Convert.ToInt32(rdr["SuspenCat"]),
                                SuspenTreated = Convert.ToInt32(rdr["SuspenTreated"]),
                                OrderNo = Convert.ToString(rdr["OrderNo"]),
                                OrderDate = Convert.ToDateTime(rdr["OrderDate"]),
                                Remarks = Convert.ToString(rdr["Remarks"]),
                                FlagStatus = Convert.ToString(rdr["FlagStatus"]),
                                RejectedReason = Convert.ToString(rdr["RejectedReason"]),
                                IPAddress = Convert.ToString(rdr["IPAddress"]),
                                IsEditable = Convert.ToBoolean(rdr["Editable"])
                            };
                            listOfRegDetails.Add(objreg);
                        }
                    }
                }
            });

            return listOfRegDetails;
        }

        public async Task<IEnumerable<JoiningDetails>> GetJoiningDetails(int joinId, bool isCheckerLogin)
        {
            List<JoiningDetails> listOfJoiningDetails = new List<JoiningDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetJoiningDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "joinId", DbType.Int32, joinId);
                    _epsDatabase.AddInParameter(dbCommand, "isCheckerLogin", DbType.Boolean, isCheckerLogin);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            JoiningDetails objJoin = new JoiningDetails
                            {
                                EmpJoiningId = Convert.ToInt32(rdr["EmpJoiningId"]),
                                EmpRevokId = Convert.ToInt32(rdr["EmpRevokId"]),
                                JoiningDate = Convert.ToDateTime(rdr["JoiningDate"]),
                                FnAn = Convert.ToString(rdr["FnAn"]),
                                OrderNo = Convert.ToString(rdr["OrderNo"]),
                                OrderDate = Convert.ToDateTime(rdr["OrderDate"]),
                                FlagStatus = Convert.ToString(rdr["FlagStatus"]),
                                RejectedReason = Convert.ToString(rdr["RejectedReason"]),
                                IPAddress = Convert.ToString(rdr["IPAddress"]),
                                IsEditable = Convert.ToBoolean(rdr["Editable"])
                            };
                            listOfJoiningDetails.Add(objJoin);
                        }
                    }
                }
            });

            return listOfJoiningDetails;
        }

        public async Task<string> SavedRevocationDetails(RevocationDetails objRevok)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_SavedRevocDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, objRevok.EmpRevocId);
                    _epsDatabase.AddInParameter(dbCommand, "SusId", DbType.Int32, objRevok.EmpTransSusId);
                    _epsDatabase.AddInParameter(dbCommand, "RevocationDate", DbType.DateTime, objRevok.RevocationDate);
                    _epsDatabase.AddInParameter(dbCommand, "RevocOrderDt", DbType.DateTime, objRevok.RevocOrderDate);
                    _epsDatabase.AddInParameter(dbCommand, "RevocOrderNo", DbType.String, objRevok.RevocOrderNo);
                    _epsDatabase.AddInParameter(dbCommand, "Remarks", DbType.String, objRevok.Remarks);
                    _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, string.Empty);
                    _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                    _epsDatabase.AddInParameter(dbCommand, "@FlagStatus", DbType.String, objRevok.FlagStatus);

                    msg = _epsDatabase.ExecuteNonQuery(dbCommand).ToString();
                }
            });
            return msg;
        }

        public async Task<IEnumerable<RevocationDetails>> GetRevocationDetails(int susId, int empId, bool isCheckerLogin)
        {
            List<RevocationDetails> listOfRevDetails = new List<RevocationDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetRevocationDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "susId", DbType.String, susId);
                    _epsDatabase.AddInParameter(dbCommand, "empId", DbType.Int32, empId);
                    _epsDatabase.AddInParameter(dbCommand, "isCheckerLogin", DbType.Boolean, isCheckerLogin);

                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            RevocationDetails objRevok = new RevocationDetails
                            {
                                EmpRevocId = Convert.ToInt32(rdr["EmpRevocID"]),
                                EmpTransSusId = Convert.ToInt32(rdr["EmpTransSusId"]),
                                RevocationDate = Convert.ToDateTime(rdr["RevocationDate"]),
                                RevocOrderDate = Convert.ToDateTime(rdr["RevocOrderDt"]),
                                RevocOrderNo = rdr["RevocOrderNo"].ToString(),
                                Remarks = rdr["Remarks"].ToString(),
                                FlagStatus = rdr["FlagStatus"].ToString(),
                                IPAddress = rdr["IPAddress"].ToString(),
                                IsEditable = Convert.ToBoolean(rdr["Editable"]),
                                RejectedReason = rdr["RejectedReason"].ToString()
                            };
                            listOfRevDetails.Add(objRevok);
                        }
                    }
                }
            });
            return listOfRevDetails;
        }

        public async Task<string> SavedExtensionDetails(ExtensionDetails objSus)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_SavedSusExtDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, objSus.Id);
                    _epsDatabase.AddInParameter(dbCommand, "SusId", DbType.Int32, objSus.SuspenId);
                    _epsDatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, objSus.PermddoId);
                    _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objSus.EmpCode);
                    _epsDatabase.AddInParameter(dbCommand, "FromDate", DbType.DateTime, objSus.FromDate);
                    _epsDatabase.AddInParameter(dbCommand, "ToDate", DbType.DateTime, objSus.ToDate);
                    _epsDatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, objSus.OrderNo);
                    _epsDatabase.AddInParameter(dbCommand, "OrderDate", DbType.DateTime, objSus.OrderDate);
                    _epsDatabase.AddInParameter(dbCommand, "SalaryPercent", DbType.Int16, objSus.SalaryPercent);
                    _epsDatabase.AddInParameter(dbCommand, "Reason", DbType.String, objSus.Remarks);
                    _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, string.Empty);
                    _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                    _epsDatabase.AddInParameter(dbCommand, "ModifiedBy", DbType.String, "HOOMaker");
                    _epsDatabase.AddInParameter(dbCommand, "@SusPeriod", DbType.Int16, objSus.SusPeriod);
                    _epsDatabase.AddInParameter(dbCommand, "@Status", DbType.String, objSus.Status);

                    msg = _epsDatabase.ExecuteNonQuery(dbCommand).ToString();
                }
            });
            return msg;
        }

        public async Task<string> VerifiedAndRejectedSuspension(int id, string status, string reason, string compName)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_VerifiedAndRejectedSuspension))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, id);
                    _epsDatabase.AddInParameter(dbCommand, "Status", DbType.String, status);
                    _epsDatabase.AddInParameter(dbCommand, "Reason", DbType.String, reason);
                    _epsDatabase.AddInParameter(dbCommand, "compName", DbType.String, compName);
                    msg = _epsDatabase.ExecuteNonQuery(dbCommand).ToString();
                }
            });
            return msg;
        }

        public async Task<string> DeleteSuspensionDetails(int id, string compName)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_DeleteSuspensionDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, id);
                    _epsDatabase.AddInParameter(dbCommand, "compName", DbType.String, compName);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.DeleteSuccessMessage:EPSResource.SomethingWrongMessage;
                }
            });
            return msg;
        }

        public async Task<IEnumerable<SuspensionDetails>> GetSuspensionDetails(string empCode, bool isCheckerLogin, string comName)
        {
            List<SuspensionDetails> listOfsusDetails = new List<SuspensionDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_GetSuspensionDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "empCode", DbType.String, empCode);
                    _epsDatabase.AddInParameter(dbCommand, "isCheckerLogin", DbType.Boolean, isCheckerLogin);
                    _epsDatabase.AddInParameter(dbCommand, "comName", DbType.String, comName);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            SuspensionDetails objsus = new SuspensionDetails
                            {
                                Id = Convert.ToInt32(rdr["EmpTransSusId"]),
                                FromDate = Convert.ToDateTime(rdr["FromDate"]),
                                SusPeriod = Convert.ToDateTime(rdr["ToDate"]).ToUniversalTime().Subtract(Convert.ToDateTime(rdr["FromDate"]).ToUniversalTime()).Days,
                                ToDate = Convert.ToDateTime(rdr["ToDate"]),
                                OrderDate = Convert.ToDateTime(rdr["OrderDate"]),
                                SalaryPercent = Convert.ToInt32(rdr["SalaryPercent"]),
                                OrderNo = rdr["OrderNo"].ToString(),
                                Remarks = rdr["Remarks"].ToString(),
                                PermddoId = rdr["PermddoId"].ToString(),
                                EmpCode = rdr["EmpCd"].ToString(),
                                //IsTillOrder = Convert.ToBoolean(rdr["IsTillOrder"]),
                                Status = rdr["stDesc"].ToString(),
                                IsEditable = Convert.ToBoolean(rdr["Editable"]),
                                RejectedReason = rdr["RejectedReason"].ToString()

                            };
                            listOfsusDetails.Add(objsus);
                        }
                    }
                }
            });
            return listOfsusDetails;
        }

        // string con=ConfigurationManager

        public async Task<string> SavedNewSuspensionDetails(SuspensionDetails objSus)
        {
                int msg = await Task.Run(() =>
                {
                    DateTime fromDate = objSus.FromDate;
                    using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_SavedNewSuspensionDetails))
                    {
                        _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, objSus.Id);
                        _epsDatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, objSus.PermddoId);
                        _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objSus.EmpCode);
                        _epsDatabase.AddInParameter(dbCommand, "FromDate", DbType.DateTime, objSus.FromDate);
                        _epsDatabase.AddInParameter(dbCommand, "ToDate", DbType.DateTime, fromDate.AddDays(objSus.SusPeriod));
                        _epsDatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, objSus.OrderNo);
                        _epsDatabase.AddInParameter(dbCommand, "OrderDate", DbType.DateTime, objSus.OrderDate);
                        _epsDatabase.AddInParameter(dbCommand, "SalaryPercent", DbType.Int16, objSus.SalaryPercent);
                        _epsDatabase.AddInParameter(dbCommand, "Remarks", DbType.String, objSus.Remarks);
                        _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, string.Empty);
                        _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                        _epsDatabase.AddInParameter(dbCommand, "ModifiedBy", DbType.String, "HOOMaker");
                        // _epsDatabase.AddInParameter(dbCommand, "IsTillOrder", DbType.Boolean, objSus.IsTillOrder);
                        _epsDatabase.AddInParameter(dbCommand, "@Status", DbType.String, objSus.Status);

                        return _epsDatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return msg.ToString();
        }


        public async Task<string> SavedJoiningDetails(JoiningDetails objJoining)
        {
                int msg = await Task.Run(() =>
                {
                    using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_SavedJoiningDetails))
                    {
                        _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, objJoining.EmpJoiningId);
                        _epsDatabase.AddInParameter(dbCommand, "EmpRevocId", DbType.Int32, objJoining.EmpRevokId);
                        _epsDatabase.AddInParameter(dbCommand, "JoiningDate", DbType.DateTime, objJoining.JoiningDate);
                        _epsDatabase.AddInParameter(dbCommand, "JoiningOrderDt", DbType.DateTime, objJoining.OrderDate);
                        _epsDatabase.AddInParameter(dbCommand, "JoiningOrderNo", DbType.String, objJoining.OrderNo);
                        _epsDatabase.AddInParameter(dbCommand, "FnAn", DbType.String, objJoining.FnAn);
                        _epsDatabase.AddInParameter(dbCommand, "FlagStatus", DbType.String, objJoining.FlagStatus);
                        _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, string.Empty);
                        _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                        // _epsDatabase.AddInParameter(dbCommand, "IsTillOrder", DbType.Boolean, objSus.IsTillOrder);
                        return _epsDatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return msg.ToString();
        }



        public async Task<string> SavedRegulariseDetails(RegulariseDetailse objReg)
        {
                int msg = await Task.Run(() =>
                {
                    using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.sus_SavedRegulariseDetails))
                    {
                        _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, objReg.RegulariseId);
                        _epsDatabase.AddInParameter(dbCommand, "EmpJoiningId", DbType.Int32, objReg.JoiningId);
                        _epsDatabase.AddInParameter(dbCommand, "suspenCatId", DbType.Int16, objReg.SuspenCat);
                        _epsDatabase.AddInParameter(dbCommand, "suspenPeriodId", DbType.Int16, objReg.SuspenTreated);
                        _epsDatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, objReg.OrderNo);
                        _epsDatabase.AddInParameter(dbCommand, "OrderDate", DbType.DateTime, objReg.OrderDate);
                        _epsDatabase.AddInParameter(dbCommand, "FlagStatus", DbType.String, objReg.FlagStatus);
                        _epsDatabase.AddInParameter(dbCommand, "Remarks", DbType.String, objReg.Remarks);
                        _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, string.Empty);
                        _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                        // _epsDatabase.AddInParameter(dbCommand, "IsTillOrder", DbType.Boolean, objSus.IsTillOrder);
                        return _epsDatabase.ExecuteNonQuery(dbCommand);
                    }
                });
                return msg.ToString();
        }


    }
}
