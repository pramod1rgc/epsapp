﻿using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.UserProfile
{
    public class UserProfileDataAccessLayer
    {
        private Database epsdatabase = null;
        public UserProfileDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }

        public async Task<object[]> GetUserdetailsProfile(string username)
        {
            object[] listTable = new object[5];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetUserdetailsProfile))
                {
                    epsdatabase.AddInParameter(dbCommand, "username", DbType.String, username);
                    DataSet ds = epsdatabase.ExecuteDataSet(dbCommand);

                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];
                    listTable[2] = ds.Tables[2];
                    listTable[3] = ds.Tables[3];
                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }

    }
}
