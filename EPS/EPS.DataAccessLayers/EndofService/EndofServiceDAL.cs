﻿using EPS.BusinessModels.EndofService;
using EPS.CommonClasses;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace EPS.DataAccessLayers.EndofService
{
   public class EndofServiceDAL
    {
        Database _epsDatabase;

        public EndofServiceDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public IEnumerable<EndofServiceModel> GetSuperAnnotateEmployeesDetails(int designid, string empPermDDOId, int mode)
        {
            List<EndofServiceModel> listOfEndServiceDetails = new List<EndofServiceModel>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.GetEndofServiceEmployee))
            {
                _epsDatabase.AddInParameter(dbCommand, "Designid", DbType.Int16, designid);
                _epsDatabase.AddInParameter(dbCommand, "EmpPermDDOId", DbType.String, empPermDDOId);
                _epsDatabase.AddInParameter(dbCommand, "mode", DbType.Int16, mode);
              
                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        EndofServiceModel objEnd = new EndofServiceModel();
                        CommonClasses.CommonFunctions.MapFetchData(objEnd, rdr);
                        listOfEndServiceDetails.Add(objEnd);
                    }
                }
            }
            
            return listOfEndServiceDetails;
        }
        public IEnumerable<EndofServiceReasonModel> GetEndofServiceReasonIdDAL()
        {
            List<EndofServiceReasonModel> listOfEndServiceDetails = new List<EndofServiceReasonModel>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.GetReasonEndofService))
            {
                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        EndofServiceReasonModel objEnd = new EndofServiceReasonModel();
                        CommonClasses.CommonFunctions.MapFetchData(objEnd, rdr);
                        listOfEndServiceDetails.Add(objEnd);
                    }
                }
            }

            return listOfEndServiceDetails;
        }
       

        public IEnumerable<EmpCurrentDetailModel> EmpCurrentEndDetailsDAL(int msEmpID, string empCd)
        {
            List<EmpCurrentDetailModel> currentEndServiceDetails = new List<EmpCurrentDetailModel>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.GetCurrentEndofServiceEmployee))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpID", DbType.Int16, msEmpID);
                _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);               

                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        EmpCurrentDetailModel objEnd = new EmpCurrentDetailModel();
                        CommonClasses.CommonFunctions.MapFetchData(objEnd, rdr);
                        currentEndServiceDetails.Add(objEnd);
                    }
                }
            }
            
            return currentEndServiceDetails;
        }
        public IEnumerable<EndofServiceDtModel> GetEndofServiceEmployeesListDAL(int msEmpID)
        {
            List<EndofServiceDtModel> currentEndServiceDetails = new List<EndofServiceDtModel>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetEndofServiceEmployeesList))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpID", DbType.Int16, msEmpID);
              

                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        EndofServiceDtModel objEnd = new EndofServiceDtModel();
                        CommonClasses.CommonFunctions.MapFetchData(objEnd, rdr);
                        currentEndServiceDetails.Add(objEnd);
                    }
                }
            }

            return currentEndServiceDetails;
        }
        public string InsertEndofServiceDetails(EndofServiceInsertModel objRej)
        {
                         EndofServiceDtModel insdata = new EndofServiceDtModel();
                         insdata.MsEmpServiceEndID = objRej.MsEmpServiceEndID;
                        insdata.MsEmpID = objRej.MsEmpID;
                        insdata.EmpCd = objRej.EmpCd;
                        insdata.EndOrderNo = objRej.EndOrderNo;
                        insdata.EndOrderDt = objRej.EndOrderDt;
                        insdata.EndReasonId = objRej.EndReasonId;
                        insdata.EndRemark = objRej.EndRemark;
                        insdata.EndServDt = objRej.EndServDt;
                        insdata.DtOfDeath = objRej.DtOfDeath;
                         insdata.StatusID = objRej.StatusId;
                        insdata.CreatedDate = objRej.CreatedDate;
                        insdata.CreatedBy = objRej.CreatedBy;
            string msg = string.Empty;
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_InsertEndofServiceEmployee))
                {
                //insdata.IPAddress = CommonClasses.CommonFunctions.GetIP();
                // msg = CommonClasses.CommonFunctions.MapUpdateParameters(insdata, _epsDatabase, dbCommand);
                _epsDatabase.AddInParameter(dbCommand, "MsEmpServiceEndID ", DbType.Int32, insdata.MsEmpServiceEndID);
                _epsDatabase.AddInParameter(dbCommand, "MsEmpID", DbType.Int32, insdata.MsEmpID);
                _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, insdata.EmpCd);
                _epsDatabase.AddInParameter(dbCommand, "EndServDt", DbType.Date, insdata.EndServDt);
                _epsDatabase.AddInParameter(dbCommand, "EndReason ", DbType.String, insdata.EndReason);
                _epsDatabase.AddInParameter(dbCommand, "EndReasonId", DbType.Int32, insdata.EndReasonId);
                _epsDatabase.AddInParameter(dbCommand, "EndOrderNo", DbType.String, insdata.EndOrderNo);
                _epsDatabase.AddInParameter(dbCommand, "EndOrderDt", DbType.Date, insdata.EndOrderDt);
                _epsDatabase.AddInParameter(dbCommand, "EndRemark", DbType.String, insdata.EndRemark);
                _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, insdata.CreatedBy);
                _epsDatabase.AddInParameter(dbCommand, "CreatedDate", DbType.DateTime, insdata.CreatedDate);
                _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, objRej.IPAddress);
                _epsDatabase.AddInParameter(dbCommand, "StatusID", DbType.String, insdata.StatusID);
                _epsDatabase.AddInParameter(dbCommand, "DtOfDeath", DbType.Date, insdata.DtOfDeath);
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record inserted successfully" : "Something went wrong";
            }
                return msg;
        }
        public string DeleteEndofService(int obj,string ipAddress)
        {
            string msg = string.Empty;
            //string IpAddress = CommonClasses.CommonFunctions.GetIP();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteRejoiningOfEmployee))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpID", DbType.Int32, obj);;            
                _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, ipAddress);
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record deleted successfully" : "Something went wrong";
            }
            return msg;
        }


        public string UpdateEndofServiceStatus(UpdateEndofServiceStatusModel obj)
        {
            string msg = string.Empty;
           // obj.IpAddress = CommonClasses.CommonFunctions.GetIP();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(""))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpID", DbType.Int32, obj.MsEmpID);                       
                _epsDatabase.AddInParameter(dbCommand, "IpAddress", DbType.String, obj.IpAddress);
                _epsDatabase.AddInParameter(dbCommand, "Status", DbType.String, obj.Status);
                _epsDatabase.AddInParameter(dbCommand, "Reason", DbType.String, obj.Reason);
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record updated successfully" : "Something went wrong";
            }
            return msg;
        }
       
    }
}
