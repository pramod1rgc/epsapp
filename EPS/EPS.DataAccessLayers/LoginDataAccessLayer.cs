﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
    public class LoginDataAccessLayer
    {
        private Database epsdatabase = null;
        public LoginDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }

        public async Task<string> LoginCheck(LoginModel loginModel)
        {
            string CurrRoleID = "";
            await Task.Run(() =>
            {
                List<LoginModel> listLoginModel = new List<LoginModel>();
                string Status = string.Empty;
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetLoginDetailsForStatus))
                {
                    epsdatabase.AddInParameter(dbCommand, "@Username", DbType.String, loginModel.Username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@password", DbType.String, loginModel.Password.Trim());
                    epsdatabase.AddOutParameter(dbCommand, "@RoleType", DbType.String, 50);
                    epsdatabase.AddOutParameter(dbCommand, "@loginStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Status = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "@RoleType"));
                    string LoginStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "@loginStatus"));
                    CurrRoleID = Status + "_" + LoginStatus;
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(CurrRoleID))
            {
                return CurrRoleID;
            }
            else
            {
                return "";
            }
        }

        public async Task<string> LoginNewUser(LoginModel loginModel)
        {
            string CurrRoleID = "";
            await Task.Run(() =>
            {
                string Status = string.Empty;
                string Message = "";
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.ChangePasswordOfUser))
                {
                    epsdatabase.AddInParameter(dbCommand, "@Username", DbType.String, loginModel.Username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@password", DbType.String, loginModel.NewPassword.Trim());
                    epsdatabase.AddOutParameter(dbCommand, "@loginStatus", DbType.String, 50);
                    int i = epsdatabase.ExecuteNonQuery(dbCommand);
                    string LoginStatus = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "@loginStatus"));
                    if (i > 1)
                    {
                        if (LoginStatus == "O")
                        {
                            Message = "Pass";
                        }
                        if (LoginStatus == "N")
                        {
                            Message = "Change";
                        }
                        CurrRoleID = Message;
                    }
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(CurrRoleID))
            {
                return CurrRoleID;
            }
            else
            {
                return "";
            }
        }

        public async Task<string> RoleActivation(int ID)
        {
            string Message = "";
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.EPSRoleActivation))
                {
                    epsdatabase.AddInParameter(dbCommand, "@UserID", DbType.String, ID);
                    epsdatabase.AddOutParameter(dbCommand, "@ActivationStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "@ActivationStatus"));
                }
            }).ConfigureAwait(true);
            if (!string.IsNullOrEmpty(Message))
            {
                return Message;
            }
            else
            {
                return "";
            }

        }

        public async Task<List<userDetails>> UserDetails(string username)
        {
            List<userDetails> ListuserDetails = new List<userDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetUserDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@username", DbType.String, username.Trim());
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    IDataReader rdr = epsdatabase.ExecuteReader(dbCommand);
                    while (rdr.Read())
                    {
                        userDetails ObjuserDetails = new userDetails();
                        if (string.IsNullOrEmpty(rdr["MsControllerID"].ToString()))
                        {
                            ObjuserDetails.ControllerID = 0;
                        }
                        else
                        {
                            ObjuserDetails.ControllerID = Convert.ToInt32(rdr["MsControllerID"]);
                        }
                        if (string.IsNullOrEmpty(rdr["MsPAOID"].ToString()))
                        { ObjuserDetails.PAOID = 0; }
                        else
                        {
                            ObjuserDetails.PAOID = Convert.ToInt32(rdr["MsPAOID"]);
                        }

                        if (string.IsNullOrEmpty(rdr["MsDDOID"].ToString()))
                        {
                            ObjuserDetails.DDOID = 0;
                        }
                        else
                        {
                            ObjuserDetails.DDOID = Convert.ToInt32(rdr["MsDDOID"]);
                        }

                        if (string.IsNullOrEmpty(rdr["RoleName"].ToString()))
                        {
                            ObjuserDetails.UserRole = "Not Found";
                        }
                        else
                        {
                            ObjuserDetails.UserRole = Convert.ToString(rdr["RoleName"]);
                        }
                        if (string.IsNullOrEmpty(rdr["MsRoleID"].ToString()))
                        {
                            ObjuserDetails.MsRoleID = null;
                        }
                        else
                        {
                            ObjuserDetails.MsRoleID = Convert.ToInt32(rdr["MsRoleID"]);
                        }
                        if (string.IsNullOrEmpty(rdr["MsUserID"].ToString()))
                        {
                            ObjuserDetails.MsUserID = null;
                        }
                        else
                        {
                            ObjuserDetails.MsUserID = Convert.ToInt32(rdr["MsUserID"]);
                        }
                        if (string.IsNullOrEmpty(rdr["EmpPermDDOId"].ToString()))
                        {
                            ObjuserDetails.EmpPermDDOId = null;
                        }
                        else
                        {
                            ObjuserDetails.EmpPermDDOId = Convert.ToString(rdr["EmpPermDDOId"]);
                        }
                        ListuserDetails.Add(ObjuserDetails);
                    }
                }
            }).ConfigureAwait(true);
            if (ListuserDetails == null)
                return null;
            else
                return ListuserDetails;
        }

        /// <summary>
        /// IsMenuPermissionAssigned
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="uroleid"></param>
        /// <returns></returns>
        public async Task<bool> IsMenuPermissionAssigned(string Username, string uroleid)
        {
            bool menuPermission = false;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.BindMenuUserRoleBased_Sp))
                {
                    epsdatabase.AddInParameter(dbCommand, "@UserName", DbType.String, Username.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@RoLLID", DbType.String, uroleid.Trim());
                    IDataReader rdr = epsdatabase.ExecuteReader(dbCommand);
                    while (rdr.Read())
                    {
                        menuPermission = true;
                        break;
                    }
                }
            }).ConfigureAwait(true);
            return menuPermission;
        }
    }
}
