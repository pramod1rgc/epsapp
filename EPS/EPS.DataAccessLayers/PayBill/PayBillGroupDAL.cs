﻿
using EPS.BusinessModels.PayBill;
using EPS.CommonClasses;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.PayBill
{
    public class PayBillGroupDAL
    {
        Database _epsDatabase;
        public PayBillGroupDAL(Database database)
        {
            _epsDatabase = database;
        }


        public PayBillGroupDAL()
        {

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        /// <summary>
        /// To Get the Account head for PaybillGroup
        /// </summary>
        /// <param name="PermDDOId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AccountHeads>> GetAccountHeads(string PermDDOId)
        {
            // string con = Startup.ConnectionString; 
            List<AccountHeads> listAccountHeads = new List<AccountHeads>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAccountHeads))
                {

                    _epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, PermDDOId);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            AccountHeads objAccountHeadModel = new AccountHeads();

                            objAccountHeadModel.MsPSchemeID = objReader["MsPSchemeID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPSchemeID"]) : objAccountHeadModel.MsPSchemeID = 0;

                            objAccountHeadModel.SchemeCode = objReader["SchemeCode"] != DBNull.Value ? Convert.ToString(objReader["SchemeCode"]).Trim() : objAccountHeadModel.SchemeCode = null;

                            listAccountHeads.Add(objAccountHeadModel);

                        }

                    }

                }
            });
            if (listAccountHeads == null)
                return null;
            else
                return listAccountHeads;
        }


        /// <summary>
        /// Insert or Update PayBillGroup
        /// </summary>
        /// <param name="objBillGroup"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdatePayBillGroup(PayBillGroup objBillGroup)
        {
            int result = 0;
            string Error = "";
            await Task.Run(() =>
            {
               
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdatePayBillGroup))
            {
                _epsDatabase.AddInParameter(dbCommand, "BillGroupId", DbType.Int32, objBillGroup.PayBillGroupId);
                _epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, objBillGroup.PermDDOId);
                _epsDatabase.AddInParameter(dbCommand, "AccountHead", DbType.String, objBillGroup.AccountHead);
                _epsDatabase.AddInParameter(dbCommand, "ServiceType", DbType.String, objBillGroup.ServiceType);
                _epsDatabase.AddInParameter(dbCommand, "GroupId", DbType.String, objBillGroup.GroupIds);
                _epsDatabase.AddInParameter(dbCommand, "BillGroupName", DbType.String, objBillGroup.BillGroupName);
                _epsDatabase.AddInParameter(dbCommand, "BillForGAR", DbType.String, objBillGroup.BillForGAR);
                _epsDatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                result = _epsDatabase.ExecuteNonQuery(dbCommand);
                Error = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "Error"));
            }
            });
            return Error;

        }

        /// <summary>
        /// Get Pay Bill Group Detail by BillGroupId
        /// </summary>
        /// <param name="BillGroupId"></param>
        /// <returns></returns>

        public async Task<PayBillGroupModel> GetPayBillGroupModelDetailsById(int BillGroupId)
        {
            PayBillGroupModel ObjPayBillGroupModel = new PayBillGroupModel();
            // string con = Startup.ConnectionString;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroupDetailByBillGroupId))
                {
                    _epsDatabase.AddInParameter(dbCommand, "BillGroupId", DbType.Int32, BillGroupId);
                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ObjPayBillGroupModel.GroupId = objReader["GroupId"] != DBNull.Value ? Convert.ToString(objReader["GroupId"]).Trim() : ObjPayBillGroupModel.GroupId = null;
                            ObjPayBillGroupModel.BillgrDesc = objReader["BillgrDesc"] != DBNull.Value ? Convert.ToString(objReader["BillgrDesc"]).Trim() : ObjPayBillGroupModel.BillgrDesc = null;
                            ObjPayBillGroupModel.BillgrpType = objReader["BillgrpType"] != DBNull.Value ? Convert.ToString(objReader["BillgrpType"]).Trim() : ObjPayBillGroupModel.BillgrpType = null;
                            ObjPayBillGroupModel.ServiceType = objReader["ServiceType"] != DBNull.Value ? Convert.ToString(objReader["ServiceType"]).Trim() : ObjPayBillGroupModel.ServiceType = null;

                            ObjPayBillGroupModel.SchemeId = objReader["SchemeId"] != DBNull.Value ? Convert.ToInt32(objReader["SchemeId"]) : ObjPayBillGroupModel.SchemeId = 0;



                            ObjPayBillGroupModel.PostType = objReader["PostType"] != DBNull.Value ? Convert.ToInt32(objReader["PostType"]) : ObjPayBillGroupModel.PostType = 0;
                            ObjPayBillGroupModel.BillgrId = BillGroupId;
                        }

                    }
                }
            });
            return ObjPayBillGroupModel;



        }


        /// <summary>
        /// Get pay Bill Group Employee  those are not attached with any billGroup
        /// </summary>
        /// <param name="EmpPermDDOId"></param>
        /// <returns></returns>


        public async Task<IEnumerable<PayBillGroupAttachandDeattachModel>> GetpayBillGroupEmpAttach(string EmpPermDDOId)
        {


            List<PayBillGroupAttachandDeattachModel> objlistPayBillGroupAttachandDeattachModel = new List<PayBillGroupAttachandDeattachModel>();


            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroupEmpAttach))
                {
                    _epsDatabase.AddInParameter(dbCommand, "BillGroupId", DbType.String, EmpPermDDOId);
                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayBillGroupAttachandDeattachModel ObjPayBillGroupAttachandDeattachModel = new PayBillGroupAttachandDeattachModel();


                            ObjPayBillGroupAttachandDeattachModel.EmpCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : ObjPayBillGroupAttachandDeattachModel.EmpCode = null;
                            ObjPayBillGroupAttachandDeattachModel.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]).Trim() : ObjPayBillGroupAttachandDeattachModel.EmpName = null;
                            ObjPayBillGroupAttachandDeattachModel.BillgrID = objReader["BillGrId"] != DBNull.Value ? Convert.ToString(objReader["BillGrId"]).Trim() : ObjPayBillGroupAttachandDeattachModel.BillgrID = null;
                            ObjPayBillGroupAttachandDeattachModel.Desc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : ObjPayBillGroupAttachandDeattachModel.Desc = null;
                            ObjPayBillGroupAttachandDeattachModel.DesigFieldDeptCD = objReader["DesigFieldDeptCD"] != DBNull.Value ? Convert.ToString(objReader["DesigFieldDeptCD"]).Trim() : ObjPayBillGroupAttachandDeattachModel.DesigFieldDeptCD = null;


                            objlistPayBillGroupAttachandDeattachModel.Add(ObjPayBillGroupAttachandDeattachModel);
                        }
                    }
                }



            });

            return objlistPayBillGroupAttachandDeattachModel;
        }


        /// <summary>
        ///  Get pay Bill Group Employee  those have billGroupId
        /// </summary>
        /// <param name="EmpPermDDOId"></param>
        /// <param name="BillGrpId"></param>
        /// <returns></returns>

        public async Task<IEnumerable<PayBillGroupAttachandDeattachModel>> GetpayBillGroupEmpDeAttach(string EmpPermDDOId, string BillGrpId)
        {
            List<PayBillGroupAttachandDeattachModel> objlistPayBillGroupAttachandDeattachModel = new List<PayBillGroupAttachandDeattachModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroupEmpAttach))
                {
                    _epsDatabase.AddInParameter(dbCommand, "EmpPermDDOId", DbType.String, EmpPermDDOId);
                    _epsDatabase.AddInParameter(dbCommand, "BillGroupId", DbType.String, BillGrpId);
                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayBillGroupAttachandDeattachModel ObjPayBillGroupAttachandDeattachModel = new PayBillGroupAttachandDeattachModel();
                            ObjPayBillGroupAttachandDeattachModel.EmpCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : ObjPayBillGroupAttachandDeattachModel.EmpCode = null;
                            ObjPayBillGroupAttachandDeattachModel.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]).Trim() : ObjPayBillGroupAttachandDeattachModel.EmpName = null;
                            ObjPayBillGroupAttachandDeattachModel.BillgrID = objReader["BillGrId"] != DBNull.Value ? Convert.ToString(objReader["BillGrId"]).Trim() : ObjPayBillGroupAttachandDeattachModel.BillgrID = null;
                            ObjPayBillGroupAttachandDeattachModel.Desc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : ObjPayBillGroupAttachandDeattachModel.Desc = null;
                            ObjPayBillGroupAttachandDeattachModel.DesigFieldDeptCD = objReader["DesigFieldDeptCD"] != DBNull.Value ? Convert.ToString(objReader["DesigFieldDeptCD"]).Trim() : ObjPayBillGroupAttachandDeattachModel.DesigFieldDeptCD = null;


                            objlistPayBillGroupAttachandDeattachModel.Add(ObjPayBillGroupAttachandDeattachModel);
                        }
                    }
                }

            });
            return objlistPayBillGroupAttachandDeattachModel;
        }



        /// <summary>
        /// UpDateBillGroupId by Empcode and BillGroupId
        /// </summary>
        /// <param name="Empcode"></param>
        /// <param name="BillGroupId"></param>
        /// <returns></returns>

        public async Task<int> UpdatePayBillGroupByEmpCode(string Empcode, string BillGroupId)
        {
            int result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdatePayBillGroupByEmpCode))
                {
                    _epsDatabase.AddInParameter(dbCommand, "BillGroupId", DbType.String, BillGroupId);
                    _epsDatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, Empcode);
                    result = _epsDatabase.ExecuteNonQuery(dbCommand);
                }
            });

            return result;

        }




        /// <summary>
        /// UpDateBillGroupId by Empcode 
        /// </summary>
        /// <param name="Empcode"></param>
        /// <param name="BillGroupId"></param>
        /// <returns></returns>    


        public async Task<int> UpdatePayBillNullGroupByEmpCode(string Empcode)
        {
            int status = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdatePayBillNullGroupByEmpCode))
                {

                    _epsDatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, Empcode);
                    status = _epsDatabase.ExecuteNonQuery(dbCommand);
                }
            });

            return status;
        }


        #region amit-Account Heads Other Than 1

        /// <summary>
        /// Get List of  Account Heads Other Than 1
        /// </summary>
        /// <param name="FinFromYr"></param>
        /// <param name="FinToYr"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadsOT1(string FinFromYr, string FinToYr, string Flag)
        {

            List<AccountHeadOtherThanOne> listAccountHeadOT1 = new List<AccountHeadOtherThanOne>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAccountHeadOT1))
                {
                    _epsDatabase.AddInParameter(dbCommand, "FinFromYr", DbType.String, FinFromYr);
                    _epsDatabase.AddInParameter(dbCommand, "FinToYr", DbType.String, FinToYr);
                    _epsDatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);
                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            AccountHeadOtherThanOne objAccountHeadOT1Model = new AccountHeadOtherThanOne();
                            objAccountHeadOT1Model.ddoCode = objReader["ddoCode"] != DBNull.Value ? Convert.ToInt32(objReader["ddoCode"]) : 0;
                            objAccountHeadOT1Model.ddoCodeddoName = objReader["ddoCodeddoName"] != DBNull.Value ? Convert.ToString(objReader["ddoCodeddoName"]).Trim() : null;

                            listAccountHeadOT1.Add(objAccountHeadOT1Model);
                        }
                    }
                }

            });
            if (listAccountHeadOT1 == null)
                return null;
            else
                return listAccountHeadOT1;



        }

        /// <summary>
        ///  Get List of  Account Heads Other Than 1 Attach List
        /// </summary>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1AttachList(string DDOCode, string Flag)
        {

            List<AccountHeadOtherThanOne> obj = new List<AccountHeadOtherThanOne>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAccountHeadOT1AttachList))
                {

                    _epsDatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, DDOCode);
                    _epsDatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            AccountHeadOtherThanOne objAccountHeadOT1Model = new AccountHeadOtherThanOne();
                            objAccountHeadOT1Model.AccountHead = objReader["AccountHead"] != DBNull.Value ? Convert.ToString(objReader["AccountHead"]) : null;
                            objAccountHeadOT1Model.ObjectHead = objReader["ObjectHead"] != DBNull.Value ? Convert.ToString(objReader["ObjectHead"]).Trim() :  null;
                            objAccountHeadOT1Model.Category = objReader["Category"] != DBNull.Value ? Convert.ToString(objReader["Category"]) : null;
                            objAccountHeadOT1Model.AccountHeadName = objReader["AccountHeadName"] != DBNull.Value ? Convert.ToString(objReader["AccountHeadName"]).Trim() :   null;
                            objAccountHeadOT1Model.PaybillGroupStatus = objReader["PaybillGroupStatus"] != DBNull.Value ? Convert.ToString(objReader["PaybillGroupStatus"]).Trim() : null;
                            objAccountHeadOT1Model.HiddenAccountHead = objReader["HiddenAccountHead"] != DBNull.Value ? Convert.ToString(objReader["HiddenAccountHead"]).Trim() : null;
                            obj.Add(objAccountHeadOT1Model);

                        }
                    }


                }
            });
            if (obj == null)
                return null;
            else
                return obj;

        }

        /// <summary>
        /// Update account Head Other Than 1
        /// </summary>
        /// <param name="accountHeadvalue"></param>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        public async Task<int> UpdateaccountHeadOT1(string accountHeadvalue, string DDOCode, string Flag)
        {
            int result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdateAccountHeadOT1List))
                {

                    _epsDatabase.AddInParameter(dbCommand, "accountHeadvalue", DbType.String, accountHeadvalue);
                    _epsDatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, DDOCode);
                    _epsDatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);
                    result = _epsDatabase.ExecuteNonQuery(dbCommand);

                }

            });

            return result;

        }

        /// <summary>
        /// Get Activated/deActivated list account Head Other Than 1
        /// </summary>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        
        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1PaybillGroupStatus(string DDOCode, string Flag)
        {

            List<AccountHeadOtherThanOne> obj = new List<AccountHeadOtherThanOne>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetActDeAccountHeadOT1ByDDOCode))
                {
                    _epsDatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, DDOCode);
                    _epsDatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            AccountHeadOtherThanOne objAccountHeadOT1Model = new AccountHeadOtherThanOne();
                            objAccountHeadOT1Model.AccountHead = objReader["AccountHead"] != DBNull.Value ? Convert.ToString(objReader["AccountHead"]) : null;
                            objAccountHeadOT1Model.ObjectHead = objReader["ObjectHead"] != DBNull.Value ? Convert.ToString(objReader["ObjectHead"]).Trim() : null;
                            objAccountHeadOT1Model.Category = objReader["Category"] != DBNull.Value ? Convert.ToString(objReader["Category"]) :   null;
                            objAccountHeadOT1Model.AccountHeadName = objReader["AccountHeadName"] != DBNull.Value ? Convert.ToString(objReader["AccountHeadName"]).Trim() : null;
                            //objAccountHeadOT1Model.PaybillGroupStatus = objReader["PaybillGroupStatus"] != DBNull.Value ? Convert.ToString(objReader["PaybillGroupStatus"]).Trim() : objAccountHeadOT1Model.PaybillGroupStatus = null;
                            objAccountHeadOT1Model.HiddenAccountHead = objReader["HiddenAccountHead"] != DBNull.Value ? Convert.ToString(objReader["HiddenAccountHead"]).Trim() : null;
                            objAccountHeadOT1Model.Descriptions = objReader["Descriptions"] != DBNull.Value ? Convert.ToString(objReader["Descriptions"]).Trim() : null;
                            obj.Add(objAccountHeadOT1Model);

                        }
                    }


                }
            });
            if (obj == null)
                return null;
            else
                return obj;

        }

        /// <summary>
        /// Update Activated/deActivated account Head Other Than 1
        /// </summary>
        /// <param name="accountHeadvalue"></param>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        public async Task<int> UpdateAccountHeadOT1PaybillGroupStatus(string accountHeadvalue, string DDOCode, string Flag)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdateActDeAccountHeadOT1))
                {

                    _epsDatabase.AddInParameter(dbCommand, "accountHeadvalue", DbType.String, accountHeadvalue);
                    _epsDatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, DDOCode);
                    _epsDatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);
                    result = _epsDatabase.ExecuteNonQuery(dbCommand);

                }

            });

            return result;

        }

        #endregion



        /// <summary>
        /// Get NgRecovery
        /// </summary>
        /// <param name="PermDDOId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<NgRecovery>> GetNgRecovery(string PermDDOId)
        {
            // string con = Startup.ConnectionString; 
            List<NgRecovery> listNgRecovery = new List<NgRecovery>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetNgRecovery))
                {

                    _epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, PermDDOId);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            NgRecovery objNgRecovery = new NgRecovery();

                            objNgRecovery.PayNgRecoId = objReader["PayNgRecoId"] != DBNull.Value ? Convert.ToInt32(objReader["PayNgRecoId"]) : objNgRecovery.PayNgRecoId = 0;
                            objNgRecovery.NgDesc = objReader["NgDesc"] != DBNull.Value ? Convert.ToString(objReader["NgDesc"]) : objNgRecovery.NgDesc = null;
                            objNgRecovery.Address = objReader["Address"] != DBNull.Value ? Convert.ToString(objReader["Address"]).Trim() : objNgRecovery.Address = null;
                            objNgRecovery.BankId = objReader["BankId"] != DBNull.Value ? Convert.ToInt32(objReader["BankId"]) : objNgRecovery.BankId = 0;
                            objNgRecovery.BranchId = objReader["BranchId"] != DBNull.Value ? Convert.ToInt32(objReader["BranchId"]) : objNgRecovery.BranchId = 0;
                            objNgRecovery.IfscCd = objReader["IfscCd"] != DBNull.Value ? Convert.ToString(objReader["IfscCd"]).Trim() : objNgRecovery.IfscCd = null;
                            objNgRecovery.BankAcNo = objReader["BnkAcNo"] != DBNull.Value ? Convert.ToString(objReader["BnkAcNo"]).Trim() : objNgRecovery.BankAcNo = null;
                            objNgRecovery.BnfName = objReader["BnfName"] != DBNull.Value ? Convert.ToString(objReader["BnfName"]).Trim() : objNgRecovery.BnfName = null;
                            objNgRecovery.PfmsUniqueCode = objReader["PfmsUniqueCode"] != DBNull.Value ? Convert.ToString(objReader["PfmsUniqueCode"]).Trim() : objNgRecovery.PfmsUniqueCode = null;
                            objNgRecovery.NgDedCd = objReader["NgDedCd"] != DBNull.Value ? Convert.ToInt32(objReader["NgDedCd"]) : objNgRecovery.NgDedCd = 0;
                            objNgRecovery.VendorStatus = objReader["VendorStatus"] != DBNull.Value ? Convert.ToString(objReader["VendorStatus"]).Trim() : objNgRecovery.VendorStatus = null;
                            objNgRecovery.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : objNgRecovery.IsActive = true;
                            listNgRecovery.Add(objNgRecovery);

                        }

                    }

                }
            });
            if (listNgRecovery == null)
                return null;
            else
                return listNgRecovery;
        }

        /// <summary>
        /// Get Bank details By Ifsc Code
        /// </summary>
        /// <param name="IfscCode"></param>
        /// <returns></returns>
        public async Task<IEnumerable<BankDetails>> GetBankdetailsByIfsc(string IfscCode)
        {
            // string con = Startup.ConnectionString; 
            List<BankDetails> listBankDetails = new List<BankDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetBankdetailsByIfsc))
                {

                    _epsDatabase.AddInParameter(dbCommand, "IfscCode", DbType.String, IfscCode);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            BankDetails objBankDetails = new BankDetails();

                            objBankDetails.BankId = objReader["BankId"] != DBNull.Value ? Convert.ToInt32(objReader["BankId"]) : objBankDetails.BankId = 0;
                            objBankDetails.BankName = objReader["BankName"] != DBNull.Value ? Convert.ToString(objReader["BankName"]).Trim() : objBankDetails.BankName = null;
                            objBankDetails.BranchId = objReader["BranchId"] != DBNull.Value ? Convert.ToInt32(objReader["BranchId"]) : objBankDetails.BranchId = 0;
                            objBankDetails.BranchName = objReader["BranchName"] != DBNull.Value ? Convert.ToString(objReader["BranchName"]).Trim() : objBankDetails.BranchName = null;

                            listBankDetails.Add(objBankDetails);

                        }

                    }

                }
            });
            if (listBankDetails == null)
                return null;
            else
                return listBankDetails;
        }

        /// <summary>
        /// Insert Update NgRecovery
        /// </summary>
        /// <param name="objNgRecovery"></param>
        /// <returns></returns>
        public async Task<int> InsertUpdateNgRecovery(NgRecovery objNgRecovery)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdateNgRecovery))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PayNgRecoId", DbType.Int32, objNgRecovery.PayNgRecoId);
                    _epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, objNgRecovery.PermDDOId);
                    _epsDatabase.AddInParameter(dbCommand, "NgDesc", DbType.String, objNgRecovery.NgDesc);
                    _epsDatabase.AddInParameter(dbCommand, "BankId", DbType.String, objNgRecovery.BankId);
                    _epsDatabase.AddInParameter(dbCommand, "BranchId", DbType.String, objNgRecovery.BranchId);
                    _epsDatabase.AddInParameter(dbCommand, "IfscCd", DbType.String, objNgRecovery.IfscCd);
                    _epsDatabase.AddInParameter(dbCommand, "BnkAcNo", DbType.String, objNgRecovery.BankAcNo);

                    _epsDatabase.AddInParameter(dbCommand, "BnfName", DbType.String, objNgRecovery.BnfName);
                    _epsDatabase.AddInParameter(dbCommand, "Address", DbType.String, objNgRecovery.Address);
                    result = _epsDatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PayNgRecoId, IsActive"></param>
        /// <param name="IsActive"></param>
        /// <returns></returns>
        public async Task<int> ActiveDeactiveNgRecovery(NgRecovery objNgRecovery)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_ActiveDeactiveNgRecovery))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PayNgRecoId", DbType.Int32, objNgRecovery.PayNgRecoId);
                    _epsDatabase.AddInParameter(dbCommand, "IsActive", DbType.Boolean, objNgRecovery.IsActive);

                    result = _epsDatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }

        /// <summary>
        /// Get Financial Year
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<IEnumerable<FinancialYears>> GetFinancialYear()
        {
            // string con = Startup.ConnectionString; 
            List<FinancialYears> listGetFinancialYear = new List<FinancialYears>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetFinancialYears))
            {

                //_epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, PermDDOId);

                using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {

                        FinancialYears objGetFinancialYear = new FinancialYears();

                        objGetFinancialYear.FinancialYear = objReader["financialYear"] != DBNull.Value ? Convert.ToInt32(objReader["financialYear"]) : objGetFinancialYear.FinancialYear = 0;

                        objGetFinancialYear.FullDescription = objReader["fullDescription"] != DBNull.Value ? Convert.ToString(objReader["fullDescription"]).Trim() : objGetFinancialYear.FullDescription = null;

                        listGetFinancialYear.Add(objGetFinancialYear);

                    }

                }

            }

            if (listGetFinancialYear == null)
                return null;
            else
                return listGetFinancialYear;
        }

        /// <summary>
        /// Get All Month
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AllMonths>> GetAllMonth()
        {
            // string con = Startup.ConnectionString; 
            List<AllMonths> listAllMonth = new List<AllMonths>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAllMonth))
                {

                    //_epsDatabase.AddInParameter(dbCommand, "PermDDOid", DbType.String, PermDDOId);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            AllMonths objMonth = new AllMonths();

                            objMonth.MsMonthid = objReader["msMonthid"] != DBNull.Value ? Convert.ToInt32(objReader["msMonthid"]) : objMonth.MsMonthid = 0;

                            objMonth.MonthName = objReader["monthName"] != DBNull.Value ? Convert.ToString(objReader["monthName"]).Trim() : objMonth.MonthName = null;

                            listAllMonth.Add(objMonth);

                        }

                    }

                }
            });
            if (listAllMonth == null)
                return null;
            else
                return listAllMonth;
        }

        /// <summary>
        /// Get Employee List By Paybill And Designation
        /// </summary>
        /// <param name="FinYearFrom"></param>
        /// <param name="FinYearTo"></param>
        /// <param name="PermDdoId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PayBillGroupByFinancialYearModel>> GetPayBillGroupByFinancialYear(string FinYearFrom, string FinYearTo, string PermDdoId)
        {
            List<PayBillGroupByFinancialYearModel> lst = new List<PayBillGroupByFinancialYearModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroupByFinancialYear))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PermDDOId", DbType.String, PermDdoId);

                    _epsDatabase.AddInParameter(dbCommand, "FinYearFrom", DbType.String, FinYearFrom);

                    _epsDatabase.AddInParameter(dbCommand, "FinYearTo", DbType.String, FinYearTo);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayBillGroupByFinancialYearModel obj = new PayBillGroupByFinancialYearModel();

                            obj.PayBillGroupId = objReader["PayBillGroupId"] != DBNull.Value ? Convert.ToInt32(objReader["PayBillGroupId"]) : 0;
                            obj.BillGrDesc = objReader["BillGrDesc"] != DBNull.Value ? Convert.ToString(objReader["BillGrDesc"]) : null;

                            lst.Add(obj);
                        }
                    }
                }
            });
            return lst;
        }

        /// <summary>
        /// Get Employee List By Paybill And Designation
        /// </summary>
        /// <param name="PayItemCd"></param>
        /// <returns></returns>
        public async Task<IEnumerable<GetEmployeesByPayItemCodeModel>> GetEmployeesByPayItemCode(int PayItemCd)
        {
            List<GetEmployeesByPayItemCodeModel> lst = new List<GetEmployeesByPayItemCodeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetEmployeesByPayItemCode))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PayItemCd", DbType.Int32, PayItemCd);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetEmployeesByPayItemCodeModel obj = new GetEmployeesByPayItemCodeModel();

                            obj.MsEmpDuesId = objReader["MsEmpDuesID"] != DBNull.Value ? Convert.ToInt32(objReader["MsEmpDuesID"]) : 0;
                            obj.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]) : null;
                            obj.PayItemsCd = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : 0;
                            obj.EmpId = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;

                            lst.Add(obj);
                        }
                    }
                }
            });
            return lst;
        }

        /// <summary>
        /// Get Pay Items From Designation Code
        /// </summary>
        /// <param name="DesigCode"></param>
        /// <returns></returns>
        public async Task<IEnumerable<GetPayItemsFromDesignationCodeModel>> GetPayItemsFromDesignationCode(int DesigCode)
        {
            List<GetPayItemsFromDesignationCodeModel> lst = new List<GetPayItemsFromDesignationCodeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPayItemsFromDesignationCode))
                {
                    _epsDatabase.AddInParameter(dbCommand, "DesigCd", DbType.Int32, DesigCode);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetPayItemsFromDesignationCodeModel obj = new GetPayItemsFromDesignationCodeModel();

                            obj.MsPayItemsId = objReader["MsPayItemsId"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsId"]) : 0;
                            obj.PayItemsCd = objReader["PayItemsCd"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCd"]) : 0;
                            obj.PayItemsNameShort = objReader["PayItemsNameShort"] != DBNull.Value ? Convert.ToString(objReader["PayItemsNameShort"]) : null;
                            obj.PayItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]) : null;
                            obj.IsDeus_Deduct = objReader["IsDeus_Deduct"] != DBNull.Value ? Convert.ToInt16(objReader["IsDeus_Deduct"]) : 0;

                            lst.Add(obj);
                        }
                    }
                }
            });
            return lst;
        }


        /// <summary>
        /// Update Employee Non Computational Dues Deduct Amount
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<string> UpdateEmpNonComputationalDuesDeductAmount(UpdateEmpDuesDeductAmountModel obj)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdateEmpNonComputationalDuesDeductAmount))
                {
                    msg = CommonClasses.CommonFunctions.MapUpdateParameters(obj, _epsDatabase, dbCommand);
                }
            });
            return msg;
        }

        /// <summary>
        /// Get Employee List By Paybill And Designation
        /// </summary>
        /// <param name="PermDDOId"></param>
        /// <param name="PayBillGroupId"></param>
        /// <param name="DesigCd"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EmpList>> GetEmployeeByPaybillAndDesig(string PermDDOId, int PayBillGroupId, int DesigCd)
        {
            // string con = Startup.ConnectionString; 
            List<EmpList> listNgData = new List<EmpList>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetEmployeeByPaybillAndDesig))
                {

                    _epsDatabase.AddInParameter(dbCommand, "PermDDOId", DbType.String, PermDDOId);

                    _epsDatabase.AddInParameter(dbCommand, "PayBillGroupId", DbType.String, PayBillGroupId);

                    _epsDatabase.AddInParameter(dbCommand, "DesigCd", DbType.String, DesigCd);

                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmpList objNgData = new EmpList();

                            objNgData.EmpCode = objReader["EmpCode"] != DBNull.Value ? Convert.ToString(objReader["EmpCode"]).Trim() : objNgData.EmpCode = null;

                            objNgData.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]).Trim() : objNgData.EmpName = null;



                            listNgData.Add(objNgData);

                        }

                    }

                }
            });
            if (listNgData == null)
                return null;
            else
                return listNgData;
        }
        
        /// <summary>
        /// Get Ng Recovery List
        /// </summary>
        /// <param name="PermDDOId"></param>

        /// <returns></returns>
        public async Task<IEnumerable<NgList>> GetNgRecoveryList(string PermDDOId)
        {
            // string con = Startup.ConnectionString; 
            List<NgList> listNgData = new List<NgList>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetNgRecoveryList))
                {

                    _epsDatabase.AddInParameter(dbCommand, "PermDDOId", DbType.String, PermDDOId);
                    _epsDatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);


                    using (IDataReader objReader = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            NgList objNgData = new NgList();

                            objNgData.NgDedCd = objReader["NgDedCd"] != DBNull.Value ? Convert.ToInt32(objReader["NgDedCd"]) : 0;
                            //objReader["MsPayItemsId"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsId"]) : 0;
                            objNgData.NgDesc = objReader["NgDesc"] != DBNull.Value ? Convert.ToString(objReader["NgDesc"]).Trim() : objNgData.NgDesc = null;



                            listNgData.Add(objNgData);

                        }

                    }

                }
            });
            if (listNgData == null)
                return null;
            else
                return listNgData;
        }

        public async Task<int> InsertAllNgEntry(List<EmpList> ngList)
        {


            DataTable ListDT = ToDataTable(ngList);
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_InsertNgRecoveryEntries))
                {

                    dbCommand.Parameters.Add(new SqlParameter("ListData", ListDT) { SqlDbType = SqlDbType.Structured });
                    _epsDatabase.AddInParameter(dbCommand, "Ip", DbType.String, "10.199.72.214");
                    _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "Hoo Maker");
                    _epsDatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    result = _epsDatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(_epsDatabase.GetParameterValue(dbCommand, "Error"));
                    if (Error != "")
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
               
        public async Task<IEnumerable<NgList>> GetAllNgRecoveryandPaybillGrpDetails(string permDdoId)
        {

            List<NgList> objNgList = new List<NgList>();
            List<BillgoupDetails> objBillGroupList = new List<BillgoupDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetAllNgRecoveryandPaybillGrpDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PermDDoId", DbType.String, permDdoId);

                    DataTable dt = _epsDatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        BillgoupDetails objBillGrpData = new BillgoupDetails();
                        objBillGrpData.PayBillGroupId = dt.Rows[j]["PayBillGroupId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["PayBillGroupId"]) : 0;
                        objBillGrpData.BillgrDesc = dt.Rows[j]["BillgrDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["BillgrDesc"]).Trim() : objBillGrpData.BillgrDesc = null;
                        objBillGrpData.MonthId = dt.Rows[j]["MonthId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MonthId"]) : 0;
                        objBillGrpData.MonthName = dt.Rows[j]["MonthName"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["MonthName"]).Trim() : objBillGrpData.MonthName = null;


                        objBillGroupList.Add(objBillGrpData);
                        if (j == dt.Rows.Count - 1)
                        {
                            NgList objNgData = new NgList();
                            objNgData.NgDedCd = dt.Rows[j]["NgDedCd"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["NgDedCd"]) : 0;
                            objNgData.NgDesc = dt.Rows[j]["NgDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NgDesc"]).Trim() : objNgData.NgDesc = null;

                            objNgData.PayBillDetails = objBillGroupList;
                            objNgList.Add(objNgData);
                            objBillGroupList = new List<BillgoupDetails>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["NgDedCd"]) == Convert.ToString(dt.Rows[j + 1]["NgDedCd"])))
                        {
                            NgList objNgData = new NgList();
                            objNgData.NgDedCd = dt.Rows[j]["NgDedCd"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["NgDedCd"]) : 0;
                            objNgData.NgDesc = dt.Rows[j]["NgDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NgDesc"]).Trim() : objNgData.NgDesc = null;


                            objNgData.PayBillDetails = objBillGroupList;
                            objNgList.Add(objNgData);
                            objBillGroupList = new List<BillgoupDetails>();
                        }

                    }

                }

            });



            if (objNgList == null)
                return null;
            else
                return objNgList;


        }
               
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

    }
}
