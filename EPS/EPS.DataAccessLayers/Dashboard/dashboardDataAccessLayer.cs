﻿using System;
using System.Collections.Generic;
using EPS.BusinessModels.Dashboard;
using System.Data;
using EPS.CommonClasses;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Dashboard
{

    /// <summary>
    /// 
    /// </summary>
    public class dashboardDataAccessLayer
    {
        Database epsdatabase = null;
        /// <summary>
        /// Dashboard DataAccessLayer
        /// </summary>
        /// <param name="database"></param>
        public dashboardDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }

        /// <summary>
        /// Get All Menus By User
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="uroleid"></param>
        /// <returns></returns>
        public List<DashboardModel> getAllMenusByUser(string Username, string uroleid)
        {
            List<DashboardModel> headerTree = null;
            List<DashboardModel> categories = new List<DashboardModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.BindMenuUserRoleBased_Sp))
            {
                epsdatabase.AddInParameter(dbCommand, "UserName", DbType.String, Username.Trim());
                epsdatabase.AddInParameter(dbCommand, "RoLLID", DbType.String, uroleid.Trim());
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        categories.Add(
                            new DashboardModel
                            {
                                MsMenuID = Convert.ToInt32(rdr["MsMenuID"]),
                                displayName = rdr["MainMenuName"].ToString(),
                                ParentMenu = (Convert.ToInt32(rdr["ParentMenu"]) != 0) ? Convert.ToInt32(rdr["ParentMenu"]) : (int?)null,
                                route = rdr["MenuUrl"].ToString(),
                                iconName = rdr["IconName"].ToString() != "" ? rdr["IconName"].ToString() : ""//"star_rate" 
                            });
                    }

                    headerTree = FillRecursive(categories, null);
                    return headerTree;
                }
                //return headerTree;
            }
        }

        /// <summary>
        /// Fill Recursive
        /// </summary>
        /// <param name="flatObjects"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<DashboardModel> FillRecursive(List<DashboardModel> flatObjects, int? parentId = null)
        {
            return flatObjects.Where(x => x.ParentMenu.Equals(parentId)).Select(item => new DashboardModel
            {
                displayName = item.displayName,
                MsMenuID = item.MsMenuID,
                route = item.route,
                iconName = item.iconName,
                Children = FillRecursive(flatObjects, item.MsMenuID)
            }).ToList();
        }

        /// <summary>
        /// Get Dashboard Details
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DashboardModel>> GetDashboardDetails()
        {
            
                List<DashboardModel> ListControllerModel = new List<DashboardModel>();
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetDashboardDetails_Sp))
                {

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DashboardModel ObjDasboardModel = new DashboardModel();
                            ObjDasboardModel.ActiveUser = Convert.ToInt32(rdr["ActiveUser"]);
                            ObjDasboardModel.TotalUser = Convert.ToInt32(rdr["TotalUser"]);
                            ObjDasboardModel.TotalDDO = Convert.ToInt32(rdr["TotalDDO"]);
                            ListControllerModel.Add(ObjDasboardModel);
                        }

                    }
                    return  ListControllerModel;
                }
            }
           
        }
   
}
