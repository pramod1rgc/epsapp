﻿using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using static EPS.BusinessModels.IncomeTax.OtherDuesModel;

namespace EPS.DataAccessLayers.IncomeTax
{
    public class OtherDuesDAL
    {
        private Database epsDatabase = null;

        public OtherDuesDAL(Database database)
        {
            epsDatabase = database;
        }

        public async Task<List<MajorSecCode>> GetMajorSectionCode()
        {
            List<MajorSecCode> objMajorSectionCodeList = new List<MajorSecCode>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetMajorSectionCodeForDues))
                {
                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            MajorSecCode objListData = new MajorSecCode();

                            objListData.MajorSectionId = objReader["MajorSectionID"] != DBNull.Value ? Convert.ToInt32(objReader["MajorSectionID"]) : 0;
                            objListData.MajorSectionCode = objReader["Maj_Sec_Code"] != DBNull.Value ? Convert.ToString(objReader["Maj_Sec_Code"]).Trim() : null;
                            objListData.MajorSectionDesc = objReader["Maj_Sec_Desc"] != DBNull.Value ? Convert.ToString(objReader["Maj_Sec_Desc"]).Trim() : null;

                            objMajorSectionCodeList.Add(objListData);
                        }
                    }
                }
            });

            return objMajorSectionCodeList;
        }

        public async Task<List<OtherDues>> GetOtherDues(bool allDues)
        {
            List<OtherDues> objOtherDuesList = new List<OtherDues>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetOtherDues))
                {
                    epsDatabase.AddInParameter(dbCommand, "AllDues", DbType.Boolean, allDues);

                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        OtherDues objListData = new OtherDues();
                       
                        while (objReader.Read())
                        {
                            objListData = new OtherDues();

                            objListData.DuesId = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : 0;
                            objListData.DuesDesc = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : null;

                            objOtherDuesList.Add(objListData);
                        }
                    }
                }
            });

            return objOtherDuesList;
        }

        public async Task<List<OtherDuesDetails>> GetOtherDuesDetails(int pageNumber, int pageSize, string searchTerm)
        {
            List<OtherDuesDetails> objOtherDuesList = new List<OtherDuesDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetOtherDuesDetails))
                {
                    epsDatabase.AddInParameter(dbCommand, "PageSize", DbType.Int16, pageSize);
                    epsDatabase.AddInParameter(dbCommand, "PageNumber", DbType.Int16, pageNumber);
                    epsDatabase.AddInParameter(dbCommand, "SearchTerm", DbType.String, !string.IsNullOrEmpty(searchTerm) ? searchTerm.Trim() : string.Empty);

                    using (IDataReader rdr = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            OtherDuesDetails obj = new OtherDuesDetails();
                            CommonFunctions.MapFetchData(obj, rdr);
                            objOtherDuesList.Add(obj);
                        }
                    }
                }
            });

            return objOtherDuesList;
        }

        public async Task<string> SaveOtherDuesDetails(OtherDuesDetails objOtherDuesDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_SaveOtherDuesDetails))
                {
                    epsDatabase.AddInParameter(dbCommand, "MsEdrId", DbType.Int32, objOtherDuesDetails.MsEdrId);
                    epsDatabase.AddInParameter(dbCommand, "DuesId", DbType.Int32, objOtherDuesDetails.DuesId);
                    epsDatabase.AddInParameter(dbCommand, "UserName", DbType.String, objOtherDuesDetails.UserName);
                    epsDatabase.AddInParameter(dbCommand, "UserIp", DbType.String, objOtherDuesDetails.UserIp);

                    result = Convert.ToInt32(epsDatabase.ExecuteScalar(dbCommand));
                }
            });

            return result > 0 ? EPSResource.UpdateSuccessMessage : EPSResource.UpdateFailedMessage;
        }

        public async Task<int> DeleteOtherDuesDetails(int duesId, string userName, string userIp)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteOtherDuesDetails))
                {
                    epsDatabase.AddInParameter(dbCommand, "DuesId", DbType.Int32, duesId);
                    epsDatabase.AddInParameter(dbCommand, "UserName", DbType.String, userName);
                    epsDatabase.AddInParameter(dbCommand, "UserIp", DbType.String, userIp);

                    result = Convert.ToInt32(epsDatabase.ExecuteScalar(dbCommand));
                }
            });

            return result; // > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.DeleteFailedMessage;
        }

    }
}
