﻿using EPS.BusinessModels.IncomeTax;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.IncomeTax
{
    public class ExemptionDeductionDAL
    {
        private Database epsDatabase = null;

        public ExemptionDeductionDAL(Database database)
        {
            epsDatabase = database;
        }

        public async Task<List<MajorSectionCode>> GetMajorSectionCode()
        {
            List<MajorSectionCode> objMajorSectionCodeList = new List<MajorSectionCode>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetMajorSectionCode))
                {
                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            MajorSectionCode objListData = new MajorSectionCode();

                            objListData.MajorSecId = objReader["MajorSectionID"] != DBNull.Value ? Convert.ToInt32(objReader["MajorSectionID"]) : 0;
                            objListData.MajorSecCode = objReader["Maj_Sec_Code"] != DBNull.Value ? Convert.ToString(objReader["Maj_Sec_Code"]).Trim() : null;
                            objListData.MajorSecDesc = objReader["Maj_Sec_Desc"] != DBNull.Value ? Convert.ToString(objReader["Maj_Sec_Desc"]).Trim() : null;

                            objMajorSectionCodeList.Add(objListData);
                        }
                    }
                }
            });

            return objMajorSectionCodeList;
        }

        public async Task<List<SectionCode>> GetSectionCode(string majorSecCode)
        {
            List<SectionCode> objSectionCodeList = new List<SectionCode>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetSectionCode))
                {
                    epsDatabase.AddInParameter(dbCommand, "MajorSecCode", DbType.String, majorSecCode);

                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            SectionCode objListData = new SectionCode();

                            objListData.SecId = objReader["EdrMajSecCode"] != DBNull.Value ? Convert.ToInt32(objReader["EdrMajSecCode"]) : 0;
                            objListData.SecCode = objReader["EdrSecCode"] != DBNull.Value ? Convert.ToString(objReader["EdrSecCode"]).Trim() : null;
                            objListData.SecDesc = objReader["EdrSecCode"] != DBNull.Value ? Convert.ToString(objReader["EdrSecCode"]).Trim() : null;

                            objSectionCodeList.Add(objListData);
                        }
                    }
                }
            });

            return objSectionCodeList;
        }

        public async Task<List<SubSectionCode>> GetSubSectionCode(string majorSecCode, string secCode)
        {
            List<SubSectionCode> objSectionCodeList = new List<SubSectionCode>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetSubSectionCode))
                {
                    epsDatabase.AddInParameter(dbCommand, "MajorSectionCode", DbType.String, majorSecCode);
                    epsDatabase.AddInParameter(dbCommand, "SectionCode", DbType.String, secCode);

                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            SubSectionCode objListData = new SubSectionCode();

                            objListData.SubSecId = objReader["MsEdrId"] != DBNull.Value ? Convert.ToInt32(objReader["MsEdrId"]) : 0;
                            objListData.SubSecCode = objReader["EdrSubSecCode"] != DBNull.Value ? Convert.ToString(objReader["EdrSubSecCode"]) : null;

                            objSectionCodeList.Add(objListData);
                        }
                    }
                }
            });

            if (objSectionCodeList.Count == 1 && string.IsNullOrEmpty(objSectionCodeList[0].SubSecCode))
            {
                objSectionCodeList[0].SubSecCode = "NA";
                objSectionCodeList[0].SubSecDesc = "NA";
            }

            return objSectionCodeList;
        }

        public async Task<List<SectionCodeDescription>> GetSectionCodeDescription(string majorSecCode, string sectionCode, string subSectionCode)
        {
            List<SectionCodeDescription> objSectionCodeDescList = new List<SectionCodeDescription>();
            List<MajorSectionCodeDetails> objSectionCodeDetailsList = new List<MajorSectionCodeDetails>();

            if (subSectionCode == null || subSectionCode == "NA")
            {
                subSectionCode = string.Empty;
            }

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetSectionCodeDescription))
                {
                    epsDatabase.AddInParameter(dbCommand, "MajorSectionCode", DbType.String, majorSecCode);
                    epsDatabase.AddInParameter(dbCommand, "SectionCode", DbType.String, sectionCode);
                    epsDatabase.AddInParameter(dbCommand, "SubSectionCode", DbType.String, subSectionCode);

                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            SectionCodeDescription ObjSecCodeDesc = new SectionCodeDescription();

                            ObjSecCodeDesc.SecId = objReader["MsEdrId"] != DBNull.Value ? Convert.ToInt32(objReader["MsEdrId"]) : 0;
                            ObjSecCodeDesc.SecCode = objReader["EdrSecCode"] != DBNull.Value ? Convert.ToString(objReader["EdrSecCode"]).Trim() : null;
                            ObjSecCodeDesc.SubSecCode = objReader["EdrSubsecCode"] != DBNull.Value ? Convert.ToString(objReader["EdrSubsecCode"]).Trim() : null;
                            ObjSecCodeDesc.SecDesc = objReader["EdrDesc"] != DBNull.Value ? Convert.ToString(objReader["EdrDesc"]).Trim() : null;

                            objSectionCodeDescList.Add(ObjSecCodeDesc);
                        }
                    }
                }
            });

            return objSectionCodeDescList;
        }

        public async Task<List<SlabLimit>> GetSlabLimit(string payCommission)
        {
            List<SlabLimit> objSlabLimitList = new List<SlabLimit>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetSlabLimit))
                {
                    epsDatabase.AddInParameter(dbCommand, "PayCommId", DbType.String, payCommission);

                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            SlabLimit objListData = new SlabLimit();

                            objListData.SlabLimitId = objReader["MsSlabTypeId"] != DBNull.Value ? Convert.ToInt32(objReader["MsSlabTypeId"]) : 0;
                            objListData.SlabLimitCode = objReader["SlSlab"] != DBNull.Value ? Convert.ToString(objReader["SlSlab"]).Trim() : null;
                            objListData.SlabLimitDesc = objReader["SlDesc"] != DBNull.Value ? Convert.ToString(objReader["SlDesc"]).Trim() : null;

                            objSlabLimitList.Add(objListData);
                        }
                    }
                }
            });

            return objSlabLimitList;
        }

        public async Task<List<PayCommission>> GetPayCommission()
        {
            List<PayCommission> objPayCommList = new List<PayCommission>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetPayCommission))
                {
                    using (IDataReader objReader = epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayCommission objListData = new PayCommission();

                            objListData.PayCommId = objReader["PayCommId"] != DBNull.Value ? Convert.ToInt32(objReader["PayCommId"]) : 0;
                            objListData.PayCommDesc = objReader["PayCommDesc"] != DBNull.Value ? Convert.ToString(objReader["PayCommDesc"]).Trim() : null;

                            objPayCommList.Add(objListData);
                        }
                    }
                }
            });

            return objPayCommList;
        }

        public async Task<List<ExemptionDeductionDetails>> GetExemptionDeductionDetails(int pageNumber, int pageSize, string searchTerm)
        {
            List<ExemptionDeductionDetails> objExemDedList = new List<ExemptionDeductionDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_GetExemptionDeductionRebateDetails))
                {
                    epsDatabase.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
                    epsDatabase.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
                    epsDatabase.AddInParameter(dbCommand, "SearchTerm", DbType.String, searchTerm);

                    using (DataSet objData = epsDatabase.ExecuteDataSet(dbCommand))
                    {
                        if (objData != null && objData.Tables.Count > 0)
                        {
                            DataTable dtSection = objData.Tables[0];
                            DataTable dtRate = objData.Tables[1];

                            foreach (DataRow drow in dtSection.Rows)
                            {
                                ExemptionDeductionDetails objExemDedData = new ExemptionDeductionDetails();

                                objExemDedData.MsEdrId = Convert.ToInt32(drow["DrrID"]);
                                objExemDedData.DrrID = Convert.ToInt32(drow["DrrID"]);
                                objExemDedData.DrrID2 = Convert.ToInt32(drow["DrrID2"]);
                                objExemDedData.FinancialYearFrom = Convert.ToString(drow["DrrFinFYR"]).Trim();
                                objExemDedData.FinancialYearFromDesc = Convert.ToString(drow["FinYearFromDesc"]);
                                objExemDedData.FinancialYearTo = string.IsNullOrEmpty(Convert.ToString(drow["DrrFinTYR"])) ? string.Empty : Convert.ToString(drow["DrrFinTYR"]).Trim();
                                objExemDedData.FinancialYearToDesc = Convert.ToString(drow["FinYearToDesc"]);
                                objExemDedData.SlabLimitsFor = Convert.ToString(drow["SlSlab"]);
                                objExemDedData.SlDesc = Convert.ToString(drow["SlDesc"]);
                                objExemDedData.MajorSectionCode = Convert.ToString(drow["EdrMajSecCode"]).Trim();
                                objExemDedData.MajSecDesc = Convert.ToString(drow["EdrMajSecDesc"]).Trim();
                                objExemDedData.SectionCode = Convert.ToString(drow["EdrSecCode"]);
                                objExemDedData.SubSectionCode = Convert.ToString(drow["EdrSubsecCode"]);
                                objExemDedData.SectionDesc = Convert.ToString(drow["EdrDesc"]);
                                objExemDedData.TotalCount = Convert.ToInt32(drow["TotalCount"]);

                                DataView dv1 = dtRate.DefaultView;
                                dv1.RowFilter = "Id2 = '" + objExemDedData.DrrID2 + "'";
                                DataTable dtRateNew = dv1.ToTable();

                                foreach (DataRow row in dtRateNew.Rows)
                                {
                                    ExemptionDeductionRateDetailsModel rateDetails = new ExemptionDeductionRateDetailsModel();

                                    rateDetails.ExmDedRebateId = Convert.ToInt32(row["ExemDedRebateDetailID"]);
                                    rateDetails.Id2 = Convert.ToInt32(row["Id2"]);
                                    rateDetails.LowerLimit = Convert.ToString(row["LLimit"]);
                                    rateDetails.UpperLimit = Convert.ToString(row["Ulimit"]);
                                    rateDetails.PercentageValue = Convert.ToString(row["PvFlag"]);
                                    rateDetails.NormalRebate = Convert.ToString(row["Rebate"]);
                                    rateDetails.SpclRebate = Convert.ToString(row["RebateSpecial"]);
                                    rateDetails.MaxSaving = Convert.ToString(row["MaxSaving"]);

                                    objExemDedData.RateDetails.Add(rateDetails);
                                }

                                objExemDedList.Add(objExemDedData);
                            }
                        }

                    }
                }
            });

            return objExemDedList;
        }

        public async Task<string> UpsertExemptionDeductionDetails(ExemptionDeductionDetails objExemptionDeduction)
        {
            bool updateFlag = false;
            DataTable dt = new DataTable();
            dt.TableName = "tblDetailsType";

            DataColumn dc = new DataColumn("Id", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("ExemDedId", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("LowerLimit", typeof(decimal));
            dt.Columns.Add(dc);
            dc = new DataColumn("UpperLimit", typeof(decimal));
            dt.Columns.Add(dc);
            dc = new DataColumn("RebateNormal", typeof(decimal));
            dt.Columns.Add(dc);
            dc = new DataColumn("RebateSpecial", typeof(decimal));
            dt.Columns.Add(dc);
            dc = new DataColumn("MaxSaving", typeof(decimal));
            dt.Columns.Add(dc);
            dc = new DataColumn("PVFlag", typeof(char));
            dt.Columns.Add(dc);

            for (int i = 0; i < objExemptionDeduction.RateDetails.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i + 1;
                dr[1] = objExemptionDeduction.RateDetails[i].ExmDedRebateId;
                dr[2] = objExemptionDeduction.RateDetails[i].LowerLimit;
                dr[3] = objExemptionDeduction.RateDetails[i].UpperLimit;
                dr[4] = objExemptionDeduction.RateDetails[i].NormalRebate;
                dr[5] = objExemptionDeduction.RateDetails[i].SpclRebate;
                dr[6] = objExemptionDeduction.RateDetails[i].MaxSaving;
                dr[7] = objExemptionDeduction.RateDetails[i].PercentageValue;
                dt.Rows.InsertAt(dr, i);
            }

            objExemptionDeduction.DrrID2 = objExemptionDeduction.RateDetails[0].Id2;
            updateFlag = objExemptionDeduction.DrrID2 == 0 ? false : true;
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_UpsertExemptionDeductionRebate))
                {
                    epsDatabase.AddInParameter(dbCommand, "MsEdrId", DbType.Int32, objExemptionDeduction.MsEdrId);
                    epsDatabase.AddInParameter(dbCommand, "Id2", DbType.Int32, objExemptionDeduction.DrrID2);
                    epsDatabase.AddInParameter(dbCommand, "FinYearFrom", DbType.String, objExemptionDeduction.FinancialYearFrom.Trim());
                    epsDatabase.AddInParameter(dbCommand, "FinYearTo", DbType.String, !string.IsNullOrEmpty(objExemptionDeduction.FinancialYearTo) ? objExemptionDeduction.FinancialYearTo.Trim() : string.Empty);
                    epsDatabase.AddInParameter(dbCommand, "SlabLimitFor", DbType.String, objExemptionDeduction.SlabLimitsFor);
                    epsDatabase.AddInParameter(dbCommand, "PayCommId", DbType.Int32, 7); //for seventh pay commision
                    epsDatabase.AddInParameter(dbCommand, "UserIP", DbType.String, objExemptionDeduction.UserIp);
                    epsDatabase.AddInParameter(dbCommand, "UserName", DbType.String, objExemptionDeduction.UserName);

                    SqlParameter para = new SqlParameter("ExemDedType", dt);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);

                    result = Convert.ToInt32(epsDatabase.ExecuteScalar(dbCommand));
                }
            });

            if (updateFlag)
            {
                return result == 0 ? EPSResource.AlreadyExistMessage : result > 0 ? EPSResource.UpdateSuccessMessage : EPSResource.UpdateFailedMessage;
            }
            else
            {
                return result == 0 ? EPSResource.AlreadyExistMessage : result > 0 ? EPSResource.SaveSuccessMessage : EPSResource.SaveFailedMessage;
            }
        }

        public async Task<int> DeleteExemptionDeductionDetails(int drrId, int drrId2)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteExemptionDeductionRebate))
                {
                    epsDatabase.AddInParameter(dbCommand, "DrrId", DbType.Int32, drrId);
                    epsDatabase.AddInParameter(dbCommand, "DrrId2", DbType.Int32, drrId2);

                    result = epsDatabase.ExecuteNonQuery(dbCommand);
                }
            });

            return result; // > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.DeleteFailedMessage;
        }

    }
}
