﻿using EPS.BusinessModels.IncomeTax;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.IncomeTax
{
    public class ITRatesDAL
    {
        private Database _epsDatabase;

        public ITRatesDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<GetITRatesDetails>> GetITRatesDAL(string FinYear, string RateType, string RateFor)
        {
            List<GetITRatesDetails> listOfDetails = new List<GetITRatesDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetITRates))
                {
                    _epsDatabase.AddInParameter(dbCommand, "IsrFinYr", DbType.String, !string.IsNullOrEmpty(FinYear) ? FinYear.Trim() : null);
                    _epsDatabase.AddInParameter(dbCommand, "IsrRateType", DbType.String, !string.IsNullOrEmpty(RateType) ? RateType.Trim() : null);
                    _epsDatabase.AddInParameter(dbCommand, "IsrRateFor", DbType.String, !string.IsNullOrEmpty(RateFor) ? RateFor.Trim() : null);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            GetITRatesDetails obj = new GetITRatesDetails();
                            CommonClasses.CommonFunctions.MapFetchData(obj, rdr);
                            listOfDetails.Add(obj);
                        }
                    }
                }
            });
            return listOfDetails;
        }

        public async Task<IEnumerable<ITRateDetailStatus>> UpsertITRatesDAL(UpsertITRatesDetails obj)
        {
            List<ITRateDetailStatus> lst = new List<ITRateDetailStatus>();
            string msg = string.Empty;
            await Task.Run(() =>
            {
                //obj.IpAddress = CommonClasses.CommonFunctions.GetIP();
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpsertITRates))
                {
                    _epsDatabase.AddInParameter(dbCommand, "IsrFinYr", DbType.String, obj.IsrFinYr);
                    _epsDatabase.AddInParameter(dbCommand, "IsrRateType", DbType.String, obj.IsrRateType);
                    _epsDatabase.AddInParameter(dbCommand, "IsrRateFor", DbType.String, obj.IsrRateFor);
                    _epsDatabase.AddInParameter(dbCommand, "IpAddress", DbType.String, obj.IpAddress);
                    _epsDatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, obj.PermDdoId);
                    _epsDatabase.AddInParameter(dbCommand, "DDOId", DbType.String, obj.DDOId);

                    var datatble = CommonFunctions.CreateDataTable(obj.RateDetails);
                    SqlParameter para = new SqlParameter("RateDetails", datatble);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);
                    //msg = System.Convert.ToString(_epsDatabase.ExecuteScalar(dbCommand));

                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            lst.Add(new ITRateDetailStatus
                            {
                                IsrFinYr = Convert.ToString(rdr["FYear"]),
                                IsrLlimit = Convert.ToDouble(rdr["LLimit"]),
                                IsrUlimit = Convert.ToDouble(rdr["ULimit"]),
                                IsrRateFor = Convert.ToString(rdr["RFor"]),
                                IsrRateType = Convert.ToString(rdr["RType"]),
                                IsrAddedAmt = Convert.ToDouble(rdr["Amount"]),
                                IsrPercVal = Convert.ToInt32(rdr["PValue"]),
                                Message = Convert.ToString(rdr["Msg"])
                            });
                        }
                    }
                    //  msg = CommonClasses.CommonFunctions.MapUpdateParameters(obj, _epsDatabase, dbCommand);
                }
            });
            return lst;

        }

        public async Task<string> DeleteITRatesDAL(int id,string ip,int DDOId)
        {
            string msg = string.Empty;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteITRates))
                {
                    _epsDatabase.AddInParameter(dbCommand, "ItSurcharge_RateID", DbType.Int32, id);
                    _epsDatabase.AddInParameter(dbCommand, "Ip", DbType.String, ip);
                    _epsDatabase.AddInParameter(dbCommand, "DDOId", DbType.Int32, DDOId);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.DeleteFailedMessage;
                }
            });
            return msg;
        }


        public async Task<IEnumerable<RateType>> GetAllRateTypeAndRateForDAL(string type = null)
        {
            List<RateType> listOfRateType = new List<RateType>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetRateTypeAndRateFor))
                {
                    _epsDatabase.AddInParameter(dbCommand, "CodeType", DbType.String, string.IsNullOrEmpty(type) ? "" : type);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            listOfRateType.Add(new RateType
                            {
                                ID = Convert.ToInt32(rdr["id"]),
                                CodeText = Convert.ToString(rdr["codeType"]),
                                CodeValue = Convert.ToString(rdr["codeValue"]),
                                CodeType = Convert.ToString(rdr["codeText"])
                            });
                        }
                    }
                }
            });
            return listOfRateType;
        }


    }
}
