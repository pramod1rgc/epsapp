﻿using EPS.BusinessModels.IncomeTax;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.IncomeTax
{
    public class StandardDeductionDAL
    {
        private Database _epsDatabase;

        public StandardDeductionDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<GetStandardDeductionDetails>> GetStandardDeduction(int PageNumber, int PageSize, string SearchTerm)
        {
            List<GetStandardDeductionDetails> listOfDetails = new List<GetStandardDeductionDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetStandardDeductionRule))
                {
                    _epsDatabase.AddInParameter(dbCommand, "PageSize", DbType.Int16, PageSize);
                    _epsDatabase.AddInParameter(dbCommand, "PageNumber", DbType.Int16, PageNumber);
                    _epsDatabase.AddInParameter(dbCommand, "SearchTerm", DbType.String, !string.IsNullOrEmpty(SearchTerm) ? SearchTerm.Trim() : string.Empty);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            GetStandardDeductionDetails obj = new GetStandardDeductionDetails();
                            CommonFunctions.MapFetchData(obj, rdr);
                            listOfDetails.Add(obj);
                        }
                    }
                }
            });

            return listOfDetails;
        }

        public async Task<IEnumerable<GetStandardDeductionEntertainMasterDetail>> GetStandardDeductionEntertainMasterDAL()
        {
            List<GetStandardDeductionEntertainMasterDetail> listOfDetails = new List<GetStandardDeductionEntertainMasterDetail>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetMsITax_SDeductionEntertain))
                {
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            GetStandardDeductionEntertainMasterDetail obj = new GetStandardDeductionEntertainMasterDetail();
                            CommonFunctions.MapFetchData(obj, rdr);
                            listOfDetails.Add(obj);
                        }
                    }
                }
            });
            return listOfDetails;
        }
        
        public async Task<string> UpsertStandardDeductionDAL(UpsertStandardDeductionDetails obj)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpsertStandardDeductionRule))
                {
                    msg = CommonFunctions.MapUpdateParameters(obj, _epsDatabase, dbCommand);
                }
            });
            return msg;
        }

        public async Task<string> DeleteStandardDeductionRuleDAL(DeleteStandardDeductionDetails obj)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteStandardDeductionRule))
                {
                    string temp = CommonFunctions.MapUpdateParameters(obj, _epsDatabase, dbCommand);
                    msg = Convert.ToInt16(temp) > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.DeleteFailedMessage;
                }
            });
            return msg;
        }

        public async Task<string> DeleteStandardDeductionRateDetailDAL(int id)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteStandardDeductionRateDetail))
                {
                    _epsDatabase.AddInParameter(dbCommand, "MsSdentRuleID", DbType.Int32, id);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.DeleteFailedMessage;
                }
            });
            return msg;
        }
    }
}
