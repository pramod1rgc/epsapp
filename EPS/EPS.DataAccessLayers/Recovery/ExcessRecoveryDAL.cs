﻿using EPS.BusinessModels.ExcessRecovery;
using EPS.BusinessModels.Recovery;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace EPS.DataAccessLayers.ExcessRecovery
{
    public class ExcessRecoveryDAL
    {
        Database epsdatabase = null;
        public ExcessRecoveryDAL(Database database)
        {
            epsdatabase = database;
        }

        public void InsertExcessRecovery()
        {

        }

        /// <summary>
        /// Save Recovery Temp Payment 
        /// </summary>
        /// <param name="ObjTempData"></param>
        /// <returns></returns>
        public string SaveTempPayment(TemporaryPermanentStop ObjTempData)
        {
            //

            string message = string.Empty;


            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertTempPermStop))
            {

                epsdatabase.AddInParameter(dbCommand, "RecoveryStatus", DbType.String, ObjTempData.payMentStop);
                epsdatabase.AddInParameter(dbCommand, "OrderNo", DbType.String, ObjTempData.orderNo);
                epsdatabase.AddInParameter(dbCommand, "OrderDt", DbType.String, ObjTempData.orderdate);
                epsdatabase.AddInParameter(dbCommand, "FromDt", DbType.String, ObjTempData.orderfromdate);
                epsdatabase.AddInParameter(dbCommand, "ToDt", DbType.String, ObjTempData.ordertodate);
                epsdatabase.AddInParameter(dbCommand, "Created_Date", DbType.String, ObjTempData.CreatedDate);
                epsdatabase.AddInParameter(dbCommand, "Created_By", DbType.String, ObjTempData.CreatedBy);
                epsdatabase.AddInParameter(dbCommand, "Modified_date", DbType.String, ObjTempData.Modifieddate);
                epsdatabase.AddInParameter(dbCommand, "Modified_By", DbType.String, ObjTempData.ModifiedBy);
                epsdatabase.AddInParameter(dbCommand, "CreatedIP", DbType.String, ObjTempData.CreatedIP);
                epsdatabase.AddInParameter(dbCommand, "ModifiedIP", DbType.String, ObjTempData.ModifiedIP);
                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
            }
            return message;
        }


        public string SaveRecoveryExcess(ExcessRecoveryModel ObjTempData)
        {
            string message = string.Empty;

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand("[ods].[InsertExcessRecoveryPayment]"))
            {
                epsdatabase.AddInParameter(dbCommand, "RecoveryTypes", DbType.String, ObjTempData.RecoveryTypes);
                epsdatabase.AddInParameter(dbCommand, "RecoveryTypeOthers", DbType.String, ObjTempData.RecoveryTypeOthers);
                epsdatabase.AddInParameter(dbCommand, "SanctionOrderNo", DbType.String, ObjTempData.SanctionOrderNo);
                epsdatabase.AddInParameter(dbCommand, "SanctionOrderDate", DbType.String, ObjTempData.SanctionOrderDate);
                epsdatabase.AddInParameter(dbCommand, "AccountHead", DbType.String, ObjTempData.AccountHead);
                epsdatabase.AddInParameter(dbCommand, "ExcessAmountPaid", DbType.String, ObjTempData.ExcessAmountPaid);
                epsdatabase.AddInParameter(dbCommand, "ChallanNo", DbType.String, ObjTempData.ChallanNo);
                epsdatabase.AddInParameter(dbCommand, "ChallanAmount", DbType.String, ObjTempData.ChallanAmount);
                epsdatabase.AddInParameter(dbCommand, "ChequeDDNo", DbType.String, ObjTempData.ChequeDDNo);
                epsdatabase.AddInParameter(dbCommand, "ChallanDate", DbType.String, ObjTempData.ChallanDate);
                epsdatabase.AddInParameter(dbCommand, "Remarks", DbType.String, ObjTempData.Remarks);
                epsdatabase.AddInParameter(dbCommand, "NTRPTransactionID", DbType.String, ObjTempData.NTRPTransactionID);
                epsdatabase.AddInParameter(dbCommand, "TransactionAmount", DbType.String, ObjTempData.TransactionAmount);
                epsdatabase.AddInParameter(dbCommand, "NTRPReceiptDetails", DbType.String, ObjTempData.NTRPReceiptDetails);
                epsdatabase.AddInParameter(dbCommand, "TransactionDate", DbType.String, ObjTempData.TransactionDate);

                //epsdatabase.AddInParameter(dbCommand, "Created_Date", DbType.String, ObjTempData.CreatedDate);
                //epsdatabase.AddInParameter(dbCommand, "Created_By", DbType.String, ObjTempData.CreatedBy);
                //epsdatabase.AddInParameter(dbCommand, "Modified_date", DbType.String, ObjTempData.Modifieddate);
                //epsdatabase.AddInParameter(dbCommand, "Modified_By", DbType.String, ObjTempData.ModifiedBy);
                //epsdatabase.AddInParameter(dbCommand, "CreatedIP", DbType.String, ObjTempData.CreatedIP);
                //epsdatabase.AddInParameter(dbCommand, "ModifiedIP", DbType.String, ObjTempData.ModifiedIP);

                epsdatabase.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Msg_Out_Status"));
            }
            return message;
        }


        /// <summary>
        /// Get Temp Payment Recovery
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TemporaryPermanentStop> getTempPaymentRecovery()
        {

            List<TemporaryPermanentStop> lstRecoveryModel = new List<TemporaryPermanentStop>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand("GetTempPaymentRecover_SP"))
            {

                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        TemporaryPermanentStop ObjRecoveryModel = new TemporaryPermanentStop();
                        ObjRecoveryModel.payMentStop = rdr["RecoveryStatus"].ToString();
                        ObjRecoveryModel.orderNo = rdr["OrderNo"].ToString();
                        ObjRecoveryModel.orderdate = rdr["OrderDt"].ToString();
                        ObjRecoveryModel.orderfromdate = rdr["FromDt"].ToString();
                        ObjRecoveryModel.ordertodate = rdr["ToDt"].ToString();
                        lstRecoveryModel.Add(ObjRecoveryModel);
                    }
                }
            }
            if (lstRecoveryModel == null)
                return null;
            else
                return lstRecoveryModel;

        }

    }
}
