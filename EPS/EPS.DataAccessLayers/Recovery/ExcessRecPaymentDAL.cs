﻿using EPS.BusinessModels.Recovery;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Recovery
{
    public class ExcessRecPaymentDAL
    {
        private Database epsdatabase = null;
        private Database database = null;
        private DbCommand dbCommand = null;

        public ExcessRecPaymentDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get drop down values

        public async Task<List<RecoveryExPaymentModel>> GetFinancialYears()
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetFinancialYears)) 
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                            recExPaymodel.FinYear = objReader["FinancialYear"].ToString();
                            recExPaymodel.FinYearDesc = objReader["FullDescription"].ToString();
                            list.Add(recExPaymodel);
                        }
                    }
                }
            });
            return list;
        }


        public async Task<List<RecoveryExPaymentModel>> GetLoanPaymentType()
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_GetRecoveryLoanType))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                            recExPaymodel.RecoveryType = objReader["PayLoanRefLoanDesc"].ToString();
                            recExPaymodel.RecoveryTypeID = Convert.ToInt32(objReader["PayLoanRefLoanCD"]);
                            list.Add(recExPaymodel);
                        }
                    }
                }
            });
            return list;
        }


        public async Task<List<RecoveryExPaymentModel>> GetRecoveryElement()
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_GetRecoveryElement))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                            recExPaymodel.DeductionTypeId = Convert.ToInt32(objReader["RecStatusID"]);
                            recExPaymodel.DeductionType = objReader["Status"].ToString();
                            list.Add(recExPaymodel);
                        }
                    }
                }
            });
            return list;
        }

        public async Task<List<RecoveryExPaymentModel>> GetAccountHead(string paybill, string ddoId, int finYear, string recoveryType)
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_GetAccountHead);
                epsdatabase.AddInParameter(dbCommand, "@Paybill", DbType.String, paybill);
                epsdatabase.AddInParameter(dbCommand, "@DdoId", DbType.String, ddoId);
                epsdatabase.AddInParameter(dbCommand, "@FinYear", DbType.Int32, finYear);
                epsdatabase.AddInParameter(dbCommand, "@RecoveryType", DbType.String, recoveryType);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                        recExPaymodel.AccHeadId = Convert.ToInt32(objReader["MsPSchemeID"]);

                        recExPaymodel.AccHeadName = objReader["SchemeCode"].ToString();
                        list.Add(recExPaymodel);
                    }
                }
            });
            return list;
        }


        public async Task<List<RecoveryExPaymentModel>> GetRecoveryComponent(string paybill, string EmpId)
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_GetRecoveryComponent);
                epsdatabase.AddInParameter(dbCommand, "@PayBill", DbType.String, paybill);
                epsdatabase.AddInParameter(dbCommand, "@EmpId", DbType.String, EmpId);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                        recExPaymodel.ComponentId = Convert.ToInt32(objReader["PayItemsCD"]);
                        recExPaymodel.ComponentName = objReader["PayItemsNameShort"].ToString();
                        recExPaymodel.ComponentDesc = objReader["PayItemsName"].ToString();
                        list.Add(recExPaymodel);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Insert Update Excess Payment Recovery
        /// <summary>
        ///  Insert Update Excess Payment Recovery
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdateExRecovery(RecoveryExPaymentModel recExPaymentModel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_InsertUpdateExcessRecovery))
                {
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, recExPaymentModel.RecExcessId);
                    epsdatabase.AddInParameter(dbCommand, "@PermDdoId", DbType.Int32, recExPaymentModel.PermDdoId);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, recExPaymentModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "@LoanType", DbType.Int32, recExPaymentModel.RecoveryTypeID);
                    epsdatabase.AddInParameter(dbCommand, "@RecElement", DbType.Int32, recExPaymentModel.DeductionTypeId);
                    epsdatabase.AddInParameter(dbCommand, "@OrderNo", DbType.String, recExPaymentModel.SanctionOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "@OrderDate", DbType.Date, recExPaymentModel.SanctionOrdDate);
                    epsdatabase.AddInParameter(dbCommand, "@FinYear", DbType.String, recExPaymentModel.FinYear);
                    epsdatabase.AddInParameter(dbCommand, "@AccHead", DbType.String, recExPaymentModel.AccHeadId);
                    epsdatabase.AddInParameter(dbCommand, "@ExcessAmt", DbType.Decimal, recExPaymentModel.ExcessAmt);
                    epsdatabase.AddInParameter(dbCommand, "@InstallMentAmt", DbType.Decimal, recExPaymentModel.InstallmentAmt);
                    epsdatabase.AddInParameter(dbCommand, "@NoOfInstallment", DbType.Int32, recExPaymentModel.InstallmentNo);
                    epsdatabase.AddInParameter(dbCommand, "@OddInstallmentAmt", DbType.Decimal, recExPaymentModel.OddInstallmentAmt);
                    epsdatabase.AddInParameter(dbCommand, "@OddNoOfInstallment", DbType.Int32, recExPaymentModel.OddInstallmentNo);
                    epsdatabase.AddInParameter(dbCommand, "@LastInstallment", DbType.Int32, recExPaymentModel.PaidInstallmentNo);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, recExPaymentModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, recExPaymentModel.CreatedBy);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 && recExPaymentModel.RecExcessId > 0)
                    {
                        Response = EPSResource.UpdateSuccessMessage;
                    }
                    else if (i > 0)
                    {
                        Response = EPSResource.SaveSuccessMessage;
                    }
                    else
                    { Response = EPSResource.AlreadyExistMessage; }
                }
            });
            return Response;
        }
        #endregion

        #region Get Excess Payment Recovery
        /// <summary>
        /// Get Excess Payment Recovery
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetExcessRecoveryDetail(string empCode)
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetExcessRecovery_Details);
                epsdatabase.AddInParameter(dbCommand, "@Empcode", DbType.String, empCode);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                        recExPaymodel.RecExcessId = Convert.ToInt32(objReader["MSPayAdvEmpdetID"]);
                        recExPaymodel.RecoveryTypeID = Convert.ToInt32(objReader["LoanCd"]);
                        recExPaymodel.EmpCode = objReader["EmpCD"].ToString();
                        recExPaymodel.SanctionOrderNo = objReader["SancOrdNo"].ToString();
                        recExPaymodel.AccHeadId = Convert.ToInt32(objReader["SchemeId"]);
                        recExPaymodel.SanctionOrdDate = Convert.ToDateTime(objReader["SancOrdDT"]);
                        recExPaymodel.ExcessAmt = Convert.ToDecimal(objReader["LoanAmtSanc"]);
                        recExPaymodel.InstallmentAmt = Convert.ToInt32(objReader["PriInstAmt"]);
                        recExPaymodel.InstallmentNo = Convert.ToInt32(objReader["PriTotInst"]);
                        recExPaymodel.OddInstallmentAmt = Convert.ToDecimal(objReader["OddInstAmtPri"]);
                        recExPaymodel.OddInstallmentNo = Convert.ToInt32(objReader["OddInstNoPri"]);
                        recExPaymodel.PaidInstallmentNo = Convert.ToInt32(objReader["IntTotInst"]);
                        recExPaymodel.FinYear = objReader["FinYear"].ToString();
                        recExPaymodel.RecoveryType = objReader["PayLoanRefLoanDesc"].ToString();
                        recExPaymodel.AccountNo = objReader["SchemeCode"].ToString();
                        recExPaymodel.AccHeadName = objReader["SchemeCode"].ToString();
                        recExPaymodel.Status = objReader["Status"].ToString();
                        recExPaymodel.Description = objReader["Description"].ToString();
                        recExPaymodel.DeductionTypeId = Convert.ToInt32(objReader["RecElementID"]);
                        recExPaymodel.DeductionType = objReader["RecElement"].ToString();
                        recExPaymodel.FinYearDesc = objReader["FullDescription"].ToString();
                        list.Add(recExPaymodel);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Edit Excess Recovery Detail
        /// <summary>
        /// Edit Excess Recovery Detail
        /// </summary>
        /// <param name="RecExcessId"></param>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> EditExcessRecoveryDetail(int recExcessId)
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_EditExcessRecovery_Details);  
                epsdatabase.AddInParameter(dbCommand, "@RecExcessID", DbType.Int32, recExcessId);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                        recExPaymodel.RecExcessId = Convert.ToInt32(objReader["MSPayAdvEmpdetID"]);
                        recExPaymodel.RecoveryTypeID = Convert.ToInt32(objReader["LoanCd"]);
                        recExPaymodel.EmpCode = objReader["EmpCD"].ToString();
                        recExPaymodel.SanctionOrderNo = objReader["SancOrdNo"].ToString();
                        recExPaymodel.AccHeadId = Convert.ToInt32(objReader["SchemeId"]);
                        recExPaymodel.SanctionOrdDate = Convert.ToDateTime(objReader["SancOrdDT"]);
                        recExPaymodel.ExcessAmt = Convert.ToDecimal(objReader["LoanAmtSanc"]);
                        recExPaymodel.InstallmentAmt = Convert.ToInt32(objReader["PriInstAmt"]);
                        recExPaymodel.InstallmentNo = Convert.ToInt32(objReader["PriTotInst"]);
                        recExPaymodel.OddInstallmentAmt = Convert.ToDecimal(objReader["OddInstAmtPri"]);
                        recExPaymodel.OddInstallmentNo = Convert.ToInt32(objReader["OddInstNoPri"]);
                        recExPaymodel.PaidInstallmentNo = Convert.ToInt32(objReader["IntTotInst"]);
                        recExPaymodel.FinYear = objReader["FinYear"].ToString();
                        recExPaymodel.RecoveryType = objReader["PayLoanRefLoanDesc"].ToString();
                        recExPaymodel.AccountNo = objReader["SchemeCode"].ToString();
                        recExPaymodel.AccHeadName = objReader["SchemeCode"].ToString();
                        recExPaymodel.Status = objReader["Status"].ToString();
                        recExPaymodel.Description = objReader["Description"].ToString();
                        recExPaymodel.DeductionTypeId = Convert.ToInt32(objReader["RecElementID"]);
                        recExPaymodel.DeductionType = objReader["RecElement"].ToString();
                        recExPaymodel.FinYearDesc = objReader["FullDescription"].ToString();
                        list.Add(recExPaymodel);
                    }
                }
            });
            return list;
        }
        #endregion     

        #region Delete Excess Recovery Data
        /// <summary>
        /// Delete Excess Recovery Data
        /// </summary>
        /// <param name="RecExcessId"></param>
        /// <returns></returns>
        public async Task<string> DeleteExcessRecoveryData(int RecExcessId)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteExcessRecovery_Details); 
                epsdatabase.AddInParameter(dbCommand, "@RecExcessID", DbType.Int32, RecExcessId);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage;
                }
                else
                {
                    Response = EPSResource.DeleteFailedMessage;
                }
            });
            return Response;
        }
        #endregion

        #region Forward Excess Payment Recovery to ddo checker
        /// <summary>
        ///  Forward Excess Payment Recovery to ddo checker
        /// </summary>
        /// <param name="recExPaymentModel"></param>
        /// <returns></returns>
        public async Task<string> ForwardExRecoveryData(RecoveryExPaymentModel recExPaymentModel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ForwardExcessRecovery_Details)) 
                {
                    epsdatabase.AddInParameter(dbCommand, "@RecExcessID", DbType.Int32, recExPaymentModel.RecExcessId);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 && recExPaymentModel.RecExcessId > 0)
                    {
                        Response = EPSResource.ForwardCheckerMessage;
                    }
                    else
                    {
                        Response = EPSResource.AlreadyForwardMessage;
                    }
                }
            });
            return Response;
        }
        #endregion

        #region Add Component Amount
        /// <summary>
        /// Insert Update Component Amount
        /// </summary>
        /// <param name="recExPaymentModel"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdateComponentAmt(RecoveryExPaymentModel recExPaymentModel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_Usp_InsertUpdateRecComponentAmt)) 
                {
                    epsdatabase.AddInParameter(dbCommand, "@Id", DbType.Int32, recExPaymentModel.RecCmpId);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, recExPaymentModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "@RecYear", DbType.Int32, recExPaymentModel.FinYear);
                    epsdatabase.AddInParameter(dbCommand, "@RecComponent", DbType.String, recExPaymentModel.ComponentName);
                    epsdatabase.AddInParameter(dbCommand, "@RecAmount", DbType.Decimal, recExPaymentModel.BifAmt);
                    epsdatabase.AddInParameter(dbCommand, "@RecOrderNo", DbType.String, recExPaymentModel.SanctionOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, recExPaymentModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, recExPaymentModel.CreatedBy);
                    int i = 0;
                    i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 )
                    {
                        Response = EPSResource.SaveSuccessMessage;
                    }
                    else
                    {
                        Response = EPSResource.SaveFailedMessage;
                    }
                }
            });
            return Response;
        }

        /// <summary>
        /// Delete Component Amount
        /// </summary>
        /// <param name="CmpId"></param>
        /// <returns></returns>
        public async Task<string> DeleteComponentAmt(int CmpId)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_Usp_DeleteComponentAmt);
                epsdatabase.AddInParameter(dbCommand, "@CmpId", DbType.Int32, CmpId);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage;
                }
                else { Response = EPSResource.DeleteFailedMessage; }
            });
            return Response;
        }

        /// <summary>
        /// Get Component Amount Details
        /// </summary>
        /// <param name="EmpId"></param>
        /// <param name="Session"></param>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetComponentAmtDetails(string EmpId, string Session)
        {
            List<RecoveryExPaymentModel> list = new List<RecoveryExPaymentModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ExcessPayment_Usp_GetComponentAmtDetails); 
                epsdatabase.AddInParameter(dbCommand, "@RecYear", DbType.String, Session);
                epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, EmpId);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        RecoveryExPaymentModel recExPaymodel = new RecoveryExPaymentModel();
                        recExPaymodel.RecCmpId = Convert.ToInt32(objReader["MsRecoveryCmpID"]);
                        recExPaymodel.ComponentName = objReader["RecComponent"].ToString();
                        recExPaymodel.BifAmt = Convert.ToDecimal(objReader["RecAmount"]);
                        recExPaymodel.BifTotalAmt = Convert.ToDecimal(objReader["TotalAmount"]);                        
                        list.Add(recExPaymodel);
                    }
                }
            });
            return list;
        }
        #endregion

    }
}
