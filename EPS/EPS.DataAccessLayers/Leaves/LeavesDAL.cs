﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using EPS.BusinessModels.Leaves;
using EPS.CommonClasses;
using System.Threading.Tasks;




namespace EPS.DataAccessLayers.Leaves
{
  public  class LeavesDAL
    {

        Database epsdatabase = null;

        public LeavesDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get All Designation 
        public async Task<IEnumerable<DesignationModel>> GetAllDesignation(string PermDdoId)
        {

            List<DesignationModel> objDesignationList = new List<DesignationModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GeDesigByPermID))
                {
                    epsdatabase.AddInParameter(dbCommand, "PermDDOID", DbType.String, PermDdoId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                      
                        while (objReader.Read())
                        {
                            DesignationModel objDesignationData = new DesignationModel();
                            objDesignationData.MsDesigMastID = objReader["DesigID"] != DBNull.Value ? Convert.ToInt32(objReader["DesigID"]) : 0;
                            objDesignationData.DesigDesc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() :null;
                            objDesignationList.Add(objDesignationData);

                        }

                    }

                }

            });
            if (objDesignationList == null)
                return null;
            else
                return objDesignationList;


        }
        #endregion
      
        #region Get Employee  by DesignationId
        public async Task<IEnumerable<EmpModel>> GetEmployeeByDesig(string MsDesignID)
        {

            List<EmpModel> objEmployeeList = new List<EmpModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpNamebyDesignId))
                {
                    epsdatabase.AddInParameter(dbCommand, "Designid", DbType.String, MsDesignID);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "1");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {                     
                        while (objReader.Read())
                        {
                            EmpModel objEmployeeData = new EmpModel();
                            objEmployeeData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            objEmployeeData.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]).Trim() : null;
                            objEmployeeList.Add(objEmployeeData);

                        }

                    }

                }

            });
            if (objEmployeeList == null)
                return null;
            else
                return objEmployeeList;


        }
        #endregion

        #region Get Order by Emp
        /// <summary>
        /// common method that called from both curtail and cancel page by HOO Maker and Checker.Depending on the page code and userid data varied on both page.
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <param name="userRoleId"></param>
        /// <param name="pageCode"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderModel>> GetOrderByEmployee(string EmpCd,string userRoleId,string pageCode)
        {
            
            List<OrderModel> objOrderList = new List<OrderModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeaveOrderByEmp))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, EmpCd);

                    if (pageCode=="Curtail")
                    {
                        if (userRoleId == "8")
                        { epsdatabase.AddInParameter(dbCommand, "PageCode", DbType.String, "SancF"); }
                        else
                        { epsdatabase.AddInParameter(dbCommand, "PageCode", DbType.String, "SancV"); }
                    }
                    else if (pageCode=="Cancel")
                    {
                        if (userRoleId == "8")
                        { epsdatabase.AddInParameter(dbCommand, "PageCode", DbType.String, "CancF"); }
                        else
                        { epsdatabase.AddInParameter(dbCommand, "PageCode", DbType.String, "CancV"); }

                    }

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            OrderModel objOrderData = new OrderModel();
                            objOrderData.OrderId = objReader["OrdId"] != DBNull.Value ? Convert.ToString(objReader["OrdId"]).Trim() : null;
                            objOrderData.OrderNo = objReader["OrdNo"] != DBNull.Value ? Convert.ToString(objReader["OrdNo"]).Trim() : null;
                            objOrderList.Add(objOrderData);

                        }

                    }

                }

            });
            if (objOrderList == null)
                return null;
            else
                return objOrderList;


        }
        #endregion
        
        #region Get All LeavesType 
        public async Task <IEnumerable<LeavesTypeModel>> GetLeavesType()
        {
            List<LeavesTypeModel> ListLeavesType = new List<LeavesTypeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeavesType))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            LeavesTypeModel ObjLeavesTypeModel = new LeavesTypeModel();
                            ObjLeavesTypeModel.LeaveTypeLeaveCd = objReader["LeaveTypeLeaveCd"] != DBNull.Value ? Convert.ToString(objReader["LeaveTypeLeaveCd"]).Trim() :  null;
                            ObjLeavesTypeModel.LeaveTypeLeaveDesc = objReader["LeaveTypeLeaveDesc"] != DBNull.Value ? Convert.ToString(objReader["LeaveTypeLeaveDesc"]).Trim() : null;
                            ObjLeavesTypeModel.LeaveMaxAtATime = objReader["LeaveMaxAtATime"] != DBNull.Value ? Convert.ToString(objReader["LeaveMaxAtATime"]).Trim() : null;

                            ListLeavesType.Add(ObjLeavesTypeModel);

                        }

                    }

                }

            });
            if (ListLeavesType == null)
                return null;
            else
                return ListLeavesType;

          
        }
        #endregion

        #region Get All LeavesReason 
        public async Task<IEnumerable<LeavesReasonModel>> GetLeavesReason()
        {
            List<LeavesReasonModel> ListLeavesReason = new List<LeavesReasonModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeavesReason))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            LeavesReasonModel ObjLeavesReasonModel = new LeavesReasonModel();
                            ObjLeavesReasonModel.CddirCodeText = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;
                            ObjLeavesReasonModel.CddirCodeValue = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]).Trim() :  null;
                            ListLeavesReason.Add(ObjLeavesReasonModel);

                        }

                    }

                }

            });
            if (ListLeavesReason == null)
                return null;
            else
                return ListLeavesReason;
        }
        #endregion




        #region Sanction

        #region Get Leave Sanction
        public async Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanction(string permDdoId, string empCode,string userRoleId)
        {

            List<LeavesSanctionMainModel> objLeavesSanctionMainList = new List<LeavesSanctionMainModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetSanctionedLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    if (userRoleId=="9")
                    {epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "SanC");}
                    else
                    { epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "SanF"); }
                     epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);

                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j=0;j<dt.Rows.Count;j++)
                    {                        
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() :null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.PayEntDT = dt.Rows[j]["PayEntDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PayEntDT"]).Trim() : null;
                        
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsData.MaxLeave = dt.Rows[j]["MaxLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["MaxLeave"]).Trim() : null;
                        objLeavesSanctionDetailsData.IsMaxLeave = dt.Rows[j]["IsmaxLeave"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[j]["IsmaxLeave"]) : false;
                        objLeavesSanctionDetailsData.IsHalfDay = dt.Rows[j]["IsHalfDay"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[j]["IsHalfDay"]) : false;
                        objLeavesSanctionDetailsData.IsAfterNoon = dt.Rows[j]["Meridiem"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Meridiem"]).Trim() : null;

                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count-1)
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;                                               
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMainData.IsRecovery = dt.Rows[j]["IsRecovery"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["IsRecovery"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveReasonDesc = dt.Rows[j]["LeaveReasonDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonDesc"]).Trim() : null;
                            objLeavesSanctionMainData.Remarks = dt.Rows[j]["Remarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Remarks"]).Trim() : null;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);

                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanctionMainList.Add(objLeavesSanctionMainData);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                            {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMainData.IsRecovery = dt.Rows[j]["IsRecovery"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["IsRecovery"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveReasonDesc = dt.Rows[j]["LeaveReasonDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonDesc"]).Trim() :null;
                            objLeavesSanctionMainData.Remarks = dt.Rows[j]["Remarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Remarks"]).Trim() : null;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanctionMainList.Add(objLeavesSanctionMainData);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }
                   
                    }

                }

            });
             if (objLeavesSanctionMainList == null)
                return null;
            else
                return objLeavesSanctionMainList;


        }
        #endregion

        #region Get Leave Sanction History
        public async Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanctionHistory(string permDdoId, string empCode, string userRoleId)
        {

            List<LeavesSanctionMainModel> objLeavesSanctionMainList = new List<LeavesSanctionMainModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetSanctionedLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    if (userRoleId == "8")
                    { epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "SanV"); }
                    else
                    { epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "SanH"); }

                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() :null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.PayEntDT = dt.Rows[j]["PayEntDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PayEntDT"]).Trim() : null; 
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsData.MaxLeave = dt.Rows[j]["MaxLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["MaxLeave"]).Trim() :null;
                        objLeavesSanctionDetailsData.IsMaxLeave = dt.Rows[j]["IsmaxLeave"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[j]["IsmaxLeave"]) : false;
                        objLeavesSanctionDetailsData.IsHalfDay = dt.Rows[j]["IsHalfDay"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[j]["IsHalfDay"]) :false;
                        objLeavesSanctionDetailsData.IsAfterNoon = dt.Rows[j]["Meridiem"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Meridiem"]).Trim() : null;

                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :  null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :  null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :  null;
                            objLeavesSanctionMainData.IsRecovery = dt.Rows[j]["IsRecovery"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["IsRecovery"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveReasonDesc = dt.Rows[j]["LeaveReasonDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonDesc"]).Trim() :null;
                            objLeavesSanctionMainData.Remarks = dt.Rows[j]["Remarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Remarks"]).Trim() : null;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :null;
                            objLeavesSanctionMainData.VerifyFlg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);

                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanctionMainList.Add(objLeavesSanctionMainData);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;

                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :  null;
                            objLeavesSanctionMainData.IsRecovery = dt.Rows[j]["IsRecovery"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["IsRecovery"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveReasonDesc = dt.Rows[j]["LeaveReasonDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonDesc"]).Trim() : null;
                            objLeavesSanctionMainData.Remarks = dt.Rows[j]["Remarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Remarks"]).Trim() :null;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() : null;
                            objLeavesSanctionMainData.VerifyFlg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanctionMainList.Add(objLeavesSanctionMainData);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }

                    }
                }

            });
            if (objLeavesSanctionMainList == null)
                return null;
            else
                return objLeavesSanctionMainList;

           
        }
        #endregion
        
        #region Save Leave Sanction
        public async Task<int> SaveLeavesSanction(LeavesSanctionMainModel objListLeavesSanctionModel,string Status)
        {
            DataTable LeaveSanctionTable=null;
            if (objListLeavesSanctionModel !=null && objListLeavesSanctionModel.LeaveSanctionDetails != null)

            {

                LeaveSanctionTable = CommonClasses.CommonFunctions.ToDataTable(objListLeavesSanctionModel.LeaveSanctionDetails);
                LeaveSanctionTable.Columns.Remove("LeaveCdDesc");
            }
           
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveLeavesSanction))
                {
                    epsdatabase.AddInParameter(dbCommand, "mainid", DbType.Int32, objListLeavesSanctionModel.LeaveMainId);
                    epsdatabase.AddInParameter(dbCommand, "Opearation", DbType.String,Status);
                    epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, objListLeavesSanctionModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, objListLeavesSanctionModel.PermDDOId);
                    epsdatabase.AddInParameter(dbCommand, "OrdNo", DbType.String, objListLeavesSanctionModel.OrderNo);
                    epsdatabase.AddInParameter(dbCommand, "OrdDt", DbType.String, objListLeavesSanctionModel.OrderDT);
                    epsdatabase.AddInParameter(dbCommand, "LeaveReasonCD", DbType.String, objListLeavesSanctionModel.LeaveReasonCD);
                    epsdatabase.AddInParameter(dbCommand, "LeaveCategory", DbType.String, objListLeavesSanctionModel.LeaveCategory);
                    epsdatabase.AddInParameter(dbCommand, "IsRecovery", DbType.String, objListLeavesSanctionModel.IsRecovery);
                    epsdatabase.AddInParameter(dbCommand, "Remarks", DbType.String, objListLeavesSanctionModel.Remarks);
                    epsdatabase.AddInParameter(dbCommand, "Ip", DbType.String, objListLeavesSanctionModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "Hoo Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    dbCommand.Parameters.Add(new SqlParameter("LeavesSanction", LeaveSanctionTable) { SqlDbType = SqlDbType.Structured});
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion

        #endregion



        #region Curtail/Extended

        #region Get Curtail/Extended Leave
        public async Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailDetail(string permDdoId, string empCode, string userRoleId)
        {

            List<LeavesCurtailSanctionModel> objLeavesSanctionCurtailList = new List<LeavesCurtailSanctionModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            List<LeavesSanctionMainModel> objLeavesSanctionMainList = new List<LeavesSanctionMainModel>();

          

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCurtExtenLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    if (userRoleId == "9")
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "AllM");

                    else
                    {
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "AllC");
                    }
                    epsdatabase.AddInParameter(dbCommand, "OrdId", DbType.String, null);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :  null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() : null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() :   null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {
                            LeavesCurtailSanctionModel objLeavesSanCurtail = new LeavesCurtailSanctionModel();
                            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
                            objLeavesSanCurtail.CurtEmpCd = objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :null;
                            objLeavesSanCurtail.CurtPermDdoId = objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanCurtail.PreOrderNo = objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :null;
                            objLeavesSanCurtail.PreLeaveMainId = objLeavesSanctionMain.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;         
                            objLeavesSanCurtail.CurtLeaveDetailsId = dt.Rows[j]["LeaveDetailsId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveDetailsId"]) : 0;
                            objLeavesSanCurtail.CurtLeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                            objLeavesSanCurtail.CurtFromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() :null;
                            objLeavesSanCurtail.CurtToDT = dt.Rows[j]["CurtExtenToDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenToDt"]).Trim() :null;
                            objLeavesSanCurtail.CurtNoDays = dt.Rows[j]["CurtExtenNoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenNoDays"]).Trim() :null;
                            objLeavesSanCurtail.CurtId = dt.Rows[j]["LeaveCurtExtenID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveCurtExtenID"]) : 0;
                            objLeavesSanCurtail.CurtOrderNo = dt.Rows[j]["CurtExtenOrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdNo"]).Trim() :null;
                            objLeavesSanCurtail.CurtOrderDT = dt.Rows[j]["CurtExtenOrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdDt"]).Trim() :null;
                            objLeavesSanCurtail.IsCurtleave = dt.Rows[j]["CurtOrExtenLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtOrExtenLeave"]).Trim() :null;
                            objLeavesSanCurtail.CurtReason = dt.Rows[j]["CurtExtenReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenReason"]).Trim() :null;
                            objLeavesSanCurtail.CurtRemarks = dt.Rows[j]["CurtExtenRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenRemarks"]).Trim() :null;
                            objLeavesSanCurtail.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :null;
                            objLeavesSanCurtail.Verifyflg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() : null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :null;

                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanCurtail.LeavesSanctionMainModel = objLeavesSanctionMain;
                            objLeavesSanctionCurtailList.Add(objLeavesSanCurtail);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                        {
                            LeavesCurtailSanctionModel objLeavesSanCurtail = new LeavesCurtailSanctionModel();
                            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
                            objLeavesSanCurtail.CurtEmpCd = objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :null;
                            objLeavesSanCurtail.CurtPermDdoId = objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanCurtail.PreOrderNo = objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :null;
                            objLeavesSanCurtail.PreLeaveMainId = objLeavesSanctionMain.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanCurtail.CurtLeaveDetailsId = dt.Rows[j]["LeaveDetailsId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveDetailsId"]) : 0;
                            objLeavesSanCurtail.CurtLeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                            objLeavesSanCurtail.CurtFromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() :null;
                            objLeavesSanCurtail.CurtToDT = dt.Rows[j]["CurtExtenToDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenToDt"]).Trim() :null;
                            objLeavesSanCurtail.CurtNoDays = dt.Rows[j]["CurtExtenNoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenNoDays"]).Trim() :null;
                            objLeavesSanCurtail.CurtId = dt.Rows[j]["LeaveCurtExtenID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveCurtExtenID"]) : 0;
                            objLeavesSanCurtail.CurtOrderNo = dt.Rows[j]["CurtExtenOrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdNo"]).Trim() :null;
                            objLeavesSanCurtail.CurtOrderDT = dt.Rows[j]["CurtExtenOrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdDt"]).Trim() :null;
                            objLeavesSanCurtail.IsCurtleave = dt.Rows[j]["CurtOrExtenLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtOrExtenLeave"]).Trim() : null;
                            objLeavesSanCurtail.CurtReason = dt.Rows[j]["CurtExtenReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenReason"]).Trim() : null;
                            objLeavesSanCurtail.CurtRemarks = dt.Rows[j]["CurtExtenRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenRemarks"]).Trim() :  null;
                            objLeavesSanCurtail.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :  null;
                            objLeavesSanCurtail.Verifyflg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() : null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : null;
                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanCurtail.LeavesSanctionMainModel = objLeavesSanctionMain;
                            objLeavesSanctionCurtailList.Add(objLeavesSanCurtail);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }

                    }
                }

            });
            if (objLeavesSanctionCurtailList == null)
                return null;
            else
                return objLeavesSanctionCurtailList;


        }
        #endregion

        #region Get Curtail/Extended Leave History
        public async Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailHistory(string permDdoId, string empCode, string userRoleId)
        {


            List<LeavesCurtailSanctionModel> objLeavesSanctionCurtailList = new List<LeavesCurtailSanctionModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            List<LeavesSanctionMainModel> objLeavesSanctionMainList = new List<LeavesSanctionMainModel>();



            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCurtExtenLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    if (userRoleId == "9")
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "CurH");
                    else
                    {
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "CuCH");
                    }
                    epsdatabase.AddInParameter(dbCommand, "OrdId", DbType.String, null);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() : null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() : null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {
                            LeavesCurtailSanctionModel objLeavesSanCurtail = new LeavesCurtailSanctionModel();
                            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
                            objLeavesSanCurtail.CurtEmpCd = objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;
                            objLeavesSanCurtail.CurtPermDdoId = objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanCurtail.PreOrderNo = objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :null;
                            objLeavesSanCurtail.PreLeaveMainId = objLeavesSanctionMain.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanCurtail.CurtLeaveDetailsId = dt.Rows[j]["LeaveDetailsId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveDetailsId"]) : 0;
                            objLeavesSanCurtail.CurtLeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                            objLeavesSanCurtail.CurtFromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() :null;
                            objLeavesSanCurtail.CurtToDT = dt.Rows[j]["CurtExtenToDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenToDt"]).Trim() :null;
                            objLeavesSanCurtail.CurtNoDays = dt.Rows[j]["CurtExtenNoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenNoDays"]).Trim() :null;
                            objLeavesSanCurtail.CurtId = dt.Rows[j]["LeaveCurtExtenID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveCurtExtenID"]) : 0;
                            objLeavesSanCurtail.CurtOrderNo = dt.Rows[j]["CurtExtenOrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdNo"]).Trim() :null;
                            objLeavesSanCurtail.CurtOrderDT = dt.Rows[j]["CurtExtenOrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdDt"]).Trim() : null;
                            objLeavesSanCurtail.IsCurtleave = dt.Rows[j]["CurtOrExtenLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtOrExtenLeave"]).Trim() :null;
                            objLeavesSanCurtail.CurtReason = dt.Rows[j]["CurtExtenReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenReason"]).Trim() : null;
                            objLeavesSanCurtail.CurtRemarks = dt.Rows[j]["CurtExtenRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenRemarks"]).Trim() : null;
                            objLeavesSanCurtail.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() : null;
                            objLeavesSanCurtail.Verifyflg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() :null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : null;
                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanCurtail.LeavesSanctionMainModel = objLeavesSanctionMain;
                            objLeavesSanctionCurtailList.Add(objLeavesSanCurtail);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                        {
                            LeavesCurtailSanctionModel objLeavesSanCurtail = new LeavesCurtailSanctionModel();
                            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
                            objLeavesSanCurtail.CurtEmpCd = objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;
                            objLeavesSanCurtail.CurtPermDdoId = objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanCurtail.PreOrderNo = objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanCurtail.PreLeaveMainId = objLeavesSanctionMain.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]) : 0;
                            objLeavesSanCurtail.CurtLeaveDetailsId = dt.Rows[j]["LeaveDetailsId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveDetailsId"]) : 0;
                            objLeavesSanCurtail.CurtLeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() : null;
                            objLeavesSanCurtail.CurtFromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                            objLeavesSanCurtail.CurtToDT = dt.Rows[j]["CurtExtenToDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenToDt"]).Trim() :null;
                            objLeavesSanCurtail.CurtNoDays = dt.Rows[j]["CurtExtenNoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenNoDays"]).Trim() :null;
                            objLeavesSanCurtail.CurtId = dt.Rows[j]["LeaveCurtExtenID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveCurtExtenID"]) : 0;
                            objLeavesSanCurtail.CurtOrderNo = dt.Rows[j]["CurtExtenOrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdNo"]).Trim() :null;
                            objLeavesSanCurtail.CurtOrderDT = dt.Rows[j]["CurtExtenOrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdDt"]).Trim() :null;
                            objLeavesSanCurtail.IsCurtleave = dt.Rows[j]["CurtOrExtenLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtOrExtenLeave"]).Trim() : null;
                            objLeavesSanCurtail.CurtReason = dt.Rows[j]["CurtExtenReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenReason"]).Trim() : null;
                            objLeavesSanCurtail.CurtRemarks = dt.Rows[j]["CurtExtenRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenRemarks"]).Trim() :  null;
                            objLeavesSanCurtail.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() :null;
                            objLeavesSanCurtail.Verifyflg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() : null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : null;
                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            objLeavesSanCurtail.LeavesSanctionMainModel = objLeavesSanctionMain;
                            objLeavesSanctionCurtailList.Add(objLeavesSanCurtail);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }

                    }
                }

            });
            if (objLeavesSanctionCurtailList == null)
                return null;
            else
                return objLeavesSanctionCurtailList;

            }
        #endregion

        #region Get Verified Leave Sanction
        public async Task<LeavesCurtailSanctionModel> GetVerifiedLeavesSanction(string permDdoId, string ordId)
        {

            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
            LeavesCurtailSanctionModel objLeavesSanCurtail = new LeavesCurtailSanctionModel();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeaves))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, null);
                    //if (userRoleId == "9")
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "VeSC");

                    //else
                    //{
                    //    epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "VeCC");
                    //}
                    epsdatabase.AddInParameter(dbCommand, "OrdId", DbType.String, ordId);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() : objLeavesSanctionDetailsData.LeaveCd = null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() : objLeavesSanctionDetailsData.LeaveCdDesc = null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : objLeavesSanctionDetailsData.FromDT = null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : objLeavesSanctionDetailsData.ToDT = null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : objLeavesSanctionDetailsData.NoDays = null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {

                            objLeavesSanCurtail.CurtEmpCd = objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : objLeavesSanctionMain.EmpCd = null;
                            objLeavesSanCurtail.CurtPermDdoId = objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : objLeavesSanctionMain.PermDDOId = null;
                            objLeavesSanCurtail.PreOrderNo = objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : objLeavesSanctionMain.OrderNo = null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : objLeavesSanctionMain.OrderDT = null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : objLeavesSanctionMain.LeaveCategory = null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : objLeavesSanctionMain.LeaveReasonCD = null;
                            objLeavesSanCurtail.PreLeaveMainId = objLeavesSanctionMain.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);
                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;

                            objLeavesSanCurtail.CurtLeaveDetailsId = dt.Rows[j]["LeaveDetailsId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveDetailsId"]) : 0;
                            objLeavesSanCurtail.CurtLeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() : objLeavesSanCurtail.CurtLeaveCd = null;
                            //objLeavesSanCurtail.CurtFromDT= dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : objLeavesSanCurtail.CurtFromDT = null;
                            //objLeavesSanCurtail.CurtToDT= dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : objLeavesSanCurtail.CurtToDT = null;
                            objLeavesSanCurtail.CurtFromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : objLeavesSanCurtail.CurtFromDT = null;
                            objLeavesSanCurtail.CurtToDT = dt.Rows[j]["CurtExtenToDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenToDt"]).Trim() : objLeavesSanCurtail.CurtToDT = null;
                            objLeavesSanCurtail.CurtNoDays = dt.Rows[j]["CurtExtenNoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenNoDays"]).Trim() : objLeavesSanCurtail.CurtNoDays = null;
                            objLeavesSanCurtail.CurtId = dt.Rows[j]["LeaveCurtExtenID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["LeaveCurtExtenID"]) : 0;
                            objLeavesSanCurtail.CurtOrderNo = dt.Rows[j]["CurtExtenOrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdNo"]).Trim() : objLeavesSanCurtail.CurtOrderNo = null;
                            objLeavesSanCurtail.CurtOrderDT = dt.Rows[j]["CurtExtenOrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenOrdDt"]).Trim() : objLeavesSanCurtail.CurtOrderDT = null;
                            objLeavesSanCurtail.IsCurtleave = dt.Rows[j]["CurtOrExtenLeave"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtOrExtenLeave"]).Trim() : objLeavesSanCurtail.IsCurtleave = null;
                            objLeavesSanCurtail.CurtReason = dt.Rows[j]["CurtExtenReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenReason"]).Trim() : objLeavesSanCurtail.CurtReason = null;
                            objLeavesSanCurtail.CurtRemarks = dt.Rows[j]["CurtExtenRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CurtExtenRemarks"]).Trim() : objLeavesSanCurtail.CurtRemarks = null;
                            objLeavesSanCurtail.ReturnReason = dt.Rows[j]["ReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ReturnReason"]).Trim() : objLeavesSanCurtail.ReturnReason = null;
                            objLeavesSanCurtail.LeavesSanctionMainModel = objLeavesSanctionMain;
                            //ListLeavesCurtailModel.Add(objLeavesCurtail);
                            //objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                    }

                }

            });
            if (objLeavesSanCurtail == null)
                return null;
            else
                return objLeavesSanCurtail;


        }
        #endregion

        #region Save Leave CurtExten
        public async Task<int> SaveLeavesCurtExten(LeavesCurtailModel objListeavesCurtailModel, string Status)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveLeavesCurtailExtend))
                {
                    epsdatabase.AddInParameter(dbCommand, "Opearation", DbType.String, Status);

                    epsdatabase.AddInParameter(dbCommand, "LeaveCurtExtenID", DbType.Int32, objListeavesCurtailModel.CurtId);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenOrdNo", DbType.String, objListeavesCurtailModel.CurtOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenOrdDt", DbType.String, objListeavesCurtailModel.CurtOrderDT);
                    epsdatabase.AddInParameter(dbCommand, "CurtOrExtenLeave", DbType.String, objListeavesCurtailModel.IsCurtleave);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenReason", DbType.String, objListeavesCurtailModel.CurtReason);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenRemarks", DbType.String, objListeavesCurtailModel.CurtRemarks);

                    epsdatabase.AddInParameter(dbCommand, "CurtLeaveDeatilsId", DbType.String, objListeavesCurtailModel.CurtLeaveDetailsId);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenToDt", DbType.String, objListeavesCurtailModel.CurtToDT);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenNoDays", DbType.String, objListeavesCurtailModel.CurtNoDays);

                    epsdatabase.AddInParameter(dbCommand, "CurtExtenEmpCd", DbType.String, objListeavesCurtailModel.CurtEmpCd);
                    epsdatabase.AddInParameter(dbCommand, "CurtExtenPermDdoId", DbType.String, objListeavesCurtailModel.CurtPermDdoId);
                    epsdatabase.AddInParameter(dbCommand, "PreOrdNo", DbType.String, objListeavesCurtailModel.PreOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "PreOrderID", DbType.Int32, objListeavesCurtailModel.PreLeaveMainId);
                    epsdatabase.AddInParameter(dbCommand, "Ip", DbType.String, objListeavesCurtailModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "Hoo Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);

                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty( Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion

        #endregion



        #region Cancel

        #region Get Cancel Leave 
        public async Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancel(string permDdoId, string empCode, string userRoleId)
        {

            List<LeavesCancelSanctionModel> ListLeavesCancelModel = new List<LeavesCancelSanctionModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCanceledLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    if (userRoleId == "9")
                        epsdatabase.AddInParameter(dbCommand, "@LeavesState", DbType.String, "CaCM");

                    else
                    {
                        epsdatabase.AddInParameter(dbCommand, "@LeavesState", DbType.String, "CaCC");
                    }
                    epsdatabase.AddInParameter(dbCommand, "@OrdId", DbType.String, null);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() :  null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :  null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["CancelOrderReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderReturnReason"]).Trim() : null;
                            //objLeavesSanctionMainData.VerifyFlg = dt.Rows[j]["Verifyflg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Verifyflg"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            LeavesCancelSanctionModel objLeavesCancel = new LeavesCancelSanctionModel();
                            objLeavesCancel.CancelOrderNo = dt.Rows[j]["CancelOrderNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderNo"]).Trim() :null;
                            objLeavesCancel.CancelOrderDt = dt.Rows[j]["CancelOrderDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderDt"]).Trim() :null;
                            objLeavesCancel.CancelReason = dt.Rows[j]["CancelReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelReason"]).Trim() : null;
                            objLeavesCancel.CancelRemark = dt.Rows[j]["CancelRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelRemarks"]).Trim() :null;
                           objLeavesCancel.CancelVerifyFlg = dt.Rows[j]["VerifyFlg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["VerifyFlg"]).Trim() :null;
                           
                            
                            objLeavesCancel.LeavesSanctionMainModel = objLeavesSanctionMainData;
                            ListLeavesCancelModel.Add(objLeavesCancel);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :  null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanctionMainData.ReturnReason = dt.Rows[j]["CancelOrderReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderReturnReason"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            LeavesCancelSanctionModel objLeavesCancel = new LeavesCancelSanctionModel();
                            objLeavesCancel.CancelOrderNo = dt.Rows[j]["CancelOrderNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderNo"]).Trim() : null;
                            objLeavesCancel.CancelOrderDt = dt.Rows[j]["CancelOrderDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderDt"]).Trim() : null;
                            objLeavesCancel.CancelReason = dt.Rows[j]["CancelReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelReason"]).Trim() : null;
                            objLeavesCancel.CancelRemark = dt.Rows[j]["CancelRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelRemarks"]).Trim() : null;
                           objLeavesCancel.CancelVerifyFlg = dt.Rows[j]["VerifyFlg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["VerifyFlg"]).Trim() : null;
                            objLeavesCancel.LeavesSanctionMainModel = objLeavesSanctionMainData;
                            ListLeavesCancelModel.Add(objLeavesCancel);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }

                    }

                }

            });
            if (ListLeavesCancelModel == null)
                return null;
            else
                return ListLeavesCancelModel;
        }
        #endregion

        #region Get Cancel Leave History
        public async Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancelHistory(string permDdoId, string empCode, string userRoleId)
        {


            List<LeavesCancelSanctionModel> ListLeavesCancelModel = new List<LeavesCancelSanctionModel>();
            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();


            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCanceledLeave))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    if (userRoleId == "9")
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "CaHM");

                    else
                    {
                        epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "CaHC");
                    }
                    epsdatabase.AddInParameter(dbCommand, "OrdId", DbType.String, null);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() : null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() : null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() : null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            LeavesCancelSanctionModel objLeavesCancel = new LeavesCancelSanctionModel();
                            objLeavesCancel.CancelOrderNo = dt.Rows[j]["CancelOrderNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderNo"]).Trim() : null;
                            objLeavesCancel.CancelOrderDt = dt.Rows[j]["CancelOrderDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderDt"]).Trim() : null;
                            objLeavesCancel.CancelReason = dt.Rows[j]["CancelReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelReason"]).Trim() : null;
                            objLeavesCancel.CancelRemark = dt.Rows[j]["CancelRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelRemarks"]).Trim() :  null;
                            objLeavesCancel.CancelVerifyFlg = dt.Rows[j]["VerifyFlg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["VerifyFlg"]).Trim() : null;
                            objLeavesCancel.LeavesSanctionMainModel = objLeavesSanctionMainData;
                            ListLeavesCancelModel.Add(objLeavesCancel);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;

                        }
                        if (!(Convert.ToString(dt.Rows[j]["MsLeaveMainID"]) == Convert.ToString(dt.Rows[j + 1]["MsLeaveMainID"])))
                        {
                            LeavesSanctionMainModel objLeavesSanctionMainData = new LeavesSanctionMainModel();
                            objLeavesSanctionMainData.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :null;
                            objLeavesSanctionMainData.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() :null;
                            objLeavesSanctionMainData.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() :null;
                            objLeavesSanctionMainData.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() : null;
                            objLeavesSanctionMainData.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :null;
                            objLeavesSanctionMainData.LeaveMainId = dt.Rows[j]["MsLeaveMainID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]):0;
                            objLeavesSanctionMainData.LeaveSanctionDetails = objLeavesSanctionDetailsList;
                            LeavesCancelSanctionModel objLeavesCancel = new LeavesCancelSanctionModel();
                            objLeavesCancel.CancelOrderNo = dt.Rows[j]["CancelOrderNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderNo"]).Trim() : null;
                            objLeavesCancel.CancelOrderDt = dt.Rows[j]["CancelOrderDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderDt"]).Trim() :  null;
                            objLeavesCancel.CancelReason = dt.Rows[j]["CancelReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelReason"]).Trim() : null;
                            objLeavesCancel.CancelRemark = dt.Rows[j]["CancelRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelRemarks"]).Trim() :  null;
                           objLeavesCancel.CancelVerifyFlg = dt.Rows[j]["VerifyFlg"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["VerifyFlg"]).Trim() :  null;
                            objLeavesCancel.LeavesSanctionMainModel = objLeavesSanctionMainData;
                            ListLeavesCancelModel.Add(objLeavesCancel);
                            objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                        }

                    }

                }

            });
            if (ListLeavesCancelModel == null)
                return null;
            else
                return ListLeavesCancelModel;


        }
        #endregion

        #region Get Verified Leave For Cancel
        public async Task<LeavesCancelSanctionModel> GetVerifiedLeaveForCancel(string permDdoId, string ordId)
        {

            List<LeavesSanctionDetailsModel> objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
            LeavesSanctionMainModel objLeavesSanctionMain = new LeavesSanctionMainModel();
            LeavesCancelSanctionModel objLeavesCancel = new LeavesCancelSanctionModel();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeaves))
                {
                    epsdatabase.AddInParameter(dbCommand, "DDOCode", DbType.String, permDdoId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, null);
                    //if (userRoleId == "9")
                    epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "VOCa");

                    //else
                    //{
                    //    epsdatabase.AddInParameter(dbCommand, "LeavesState", DbType.String, "VeCC");
                    //}
                    epsdatabase.AddInParameter(dbCommand, "OrdId", DbType.String, ordId);


                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        LeavesSanctionDetailsModel objLeavesSanctionDetailsData = new LeavesSanctionDetailsModel();
                        objLeavesSanctionDetailsData.LeaveCd = dt.Rows[j]["LeaveCd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCd"]).Trim() :null;
                        objLeavesSanctionDetailsData.LeaveCdDesc = dt.Rows[j]["LeaveCdDesc"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCdDesc"]).Trim() :  null;
                        objLeavesSanctionDetailsData.FromDT = dt.Rows[j]["FromDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FromDT"]).Trim() :null;
                        objLeavesSanctionDetailsData.ToDT = dt.Rows[j]["ToDT"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ToDT"]).Trim() : null;
                        objLeavesSanctionDetailsData.NoDays = dt.Rows[j]["NoDays"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["NoDays"]).Trim() :  null;
                        objLeavesSanctionDetailsList.Add(objLeavesSanctionDetailsData);
                        if (j == dt.Rows.Count - 1)
                        {

                            objLeavesCancel.CancelOrderNo = dt.Rows[j]["CancelOrderNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderNo"]).Trim() : null;
                            objLeavesCancel.CancelOrderDt = dt.Rows[j]["CancelOrderDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderDt"]).Trim() :  null;
                            objLeavesCancel.CancelReason = dt.Rows[j]["CancelReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelReason"]).Trim() : null;
                            objLeavesCancel.CancelRemark = dt.Rows[j]["CancelRemarks"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelRemarks"]).Trim() :  null;
                            objLeavesCancel.ReturnReason = dt.Rows[j]["CancelOrderReturnReason"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CancelOrderReturnReason"]).Trim() :  null;
                            objLeavesSanctionMain.PermDDOId = dt.Rows[j]["PermDdoId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PermDdoId"]).Trim() : null;
                            objLeavesSanctionMain.EmpCd = dt.Rows[j]["Empcd"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["Empcd"]).Trim() :  null;
                            objLeavesSanctionMain.OrderNo = dt.Rows[j]["OrdNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdNo"]).Trim() : null;
                            objLeavesSanctionMain.OrderDT = dt.Rows[j]["OrdDt"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["OrdDt"]).Trim() :  null;
                            objLeavesSanctionMain.LeaveCategory = dt.Rows[j]["LeaveCategory"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveCategory"]).Trim() : null;
                            objLeavesSanctionMain.LeaveReasonCD = dt.Rows[j]["LeaveReasonId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["LeaveReasonId"]).Trim() :  null;
                            objLeavesSanctionMain.LeaveMainId = Convert.ToInt32(dt.Rows[j]["MsLeaveMainID"]);
                            objLeavesSanctionMain.LeaveSanctionDetails = objLeavesSanctionDetailsList;

                            objLeavesCancel.LeavesSanctionMainModel = objLeavesSanctionMain;
                            //ListLeavesCurtailModel.Add(objLeavesCurtail);
                            //objLeavesSanctionDetailsList = new List<LeavesSanctionDetailsModel>();
                            break;
                        }
                    }

                }

            });
            if (objLeavesCancel == null)
                return null;
            else
                return objLeavesCancel;


        }
        #endregion

        #region Save Leave Cancel
        public async Task<int> SaveLeavesCancel(LeavesCancelModel objListleavescancelModel, string Status)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveLeavesCancel))
                {
                    epsdatabase.AddInParameter(dbCommand, "Opearation", DbType.String, Status);
                    epsdatabase.AddInParameter(dbCommand, "MsLeaveMainID", DbType.Int32, objListleavescancelModel.PreLeaveMainId);
                    epsdatabase.AddInParameter(dbCommand, "CancelOrderNo", DbType.String, objListleavescancelModel.CancelOrderNo);
                    epsdatabase.AddInParameter(dbCommand, "CancelOrderDt", DbType.String, objListleavescancelModel.CancelOrderDt);
                    epsdatabase.AddInParameter(dbCommand, "CancelReason", DbType.String, objListleavescancelModel.CancelReason);
                    epsdatabase.AddInParameter(dbCommand, "CancelRemarks", DbType.String, objListleavescancelModel.CancelRemark);
                    epsdatabase.AddInParameter(dbCommand, "Ip", DbType.String, objListleavescancelModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "Hoo Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion

        #endregion    

        #region Delete Leave Sanction
        public async Task<int> DeleteLeavesSanction(string sanctMainId, string pageCode)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteLeaves))
                {

                    epsdatabase.AddInParameter(dbCommand, "@id", DbType.String, sanctMainId);
                    epsdatabase.AddInParameter(dbCommand, "@Pagecode", DbType.String, pageCode);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region Verify Return Sanction
        public async Task<int> VerifyReturnSanction(string Id, string Status, string PageCd, string Reason,string ipAddress)
        {
            int result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_ForwardToHOOCheckerSanction))
                {
                    epsdatabase.AddInParameter(dbCommand, "@Id", DbType.String, Id);
                    epsdatabase.AddInParameter(dbCommand, "@PageCode", DbType.String, PageCd);
                    epsdatabase.AddInParameter(dbCommand, "@Status", DbType.String, Status);
                    epsdatabase.AddInParameter(dbCommand, "@Reason", DbType.String, Reason);
                    epsdatabase.AddInParameter(dbCommand, "@FinYear", DbType.Int32, CommonClasses.CommonFunctions.GetCurrentFinancialYear());
                    epsdatabase.AddInParameter(dbCommand, "@Ip", DbType.String, ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, "Hoo Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion

        #region Get Leaves Type Balance       
        public async Task<IEnumerable<LeavesTypeBalanceModel>> GetLeavesTypeBalance(string EmpCd)
        {
            List<LeavesTypeBalanceModel> objLeavesTypeBalanceList = new List<LeavesTypeBalanceModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLeaveBalance))
                {
                    epsdatabase.AddInParameter(dbCommand, "FinYear", DbType.Int32,CommonClasses.CommonFunctions.GetCurrentFinancialYear());
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, EmpCd);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            LeavesTypeBalanceModel objLeavesTypeBalanceData = new LeavesTypeBalanceModel();
                            objLeavesTypeBalanceData.LeaveCd = objReader["LeaveType"] != DBNull.Value ? Convert.ToString(objReader["LeaveType"]).Trim() : null; 
                            objLeavesTypeBalanceData.LeaveBalance = objReader["Balance"] != DBNull.Value ? Convert.ToInt32(objReader["Balance"]) : 0;
                            objLeavesTypeBalanceList.Add(objLeavesTypeBalanceData);

                        }

                    }

                }

            });
            if (objLeavesTypeBalanceList == null)
                return null;
            else
                return objLeavesTypeBalanceList;
            //IEnumerable<LeavesTypeBalanceModel> result = await Task.Run(() => objLeavesDAL.GetLeavesTypeBalance(permDdoId, EmpCd));
            //return result;
        }
        #endregion

        #region Save Total CCL Leave Availed
        public async Task<int> SaveTotalLeaveAvailed(string EmpCd,string TotalLeaveAvailed,string ipAddress)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveTotalLeaveAvailed))
                {
                    epsdatabase.AddInParameter(dbCommand, "FinYear", DbType.Int32, CommonClasses.CommonFunctions.GetCurrentFinancialYear());
                    epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "LeaveAvailed", DbType.Int32,Convert.ToInt32(TotalLeaveAvailed));
                    epsdatabase.AddInParameter(dbCommand, "Ip", DbType.String, ipAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "Hoo Maker");
                    epsdatabase.AddOutParameter(dbCommand, "Error", DbType.String, 500);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion
    }
}
