﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class ServiceDetailsDA
    {

        private Database epsdatabase = null;

        public ServiceDetailsDA(Database database)
        {

            epsdatabase = database;
        }


        #region  Get getMaintainByOfc
        public async Task<IEnumerable<ServiceDetailsModel>> GetMaintainByOfc()
        {


            List<ServiceDetailsModel> objServiceDetailsModel = new List<ServiceDetailsModel>(); ;


            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getMaintainByOfc))
                {
                    //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            ServiceDetailsModel ObjServiceDetailsData = new ServiceDetailsModel();

                            ObjServiceDetailsData.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]).Trim() : null;

                            ObjServiceDetailsData.PAOCodeNAme = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() : null;

                            objServiceDetailsModel.Add(ObjServiceDetailsData);

                        }

                    }

                }

            });

            return objServiceDetailsModel;
        }
        #endregion


        #region  Get getMaintainByOfc
        public async Task<ServiceDetailsModel> GetAllServiceDetails(string empCode, int roleId)
        {


            ServiceDetailsModel ObjServiceDetailsData = new ServiceDetailsModel();


            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getServiceDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ObjServiceDetailsData.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]).Trim() : null;

                            ObjServiceDetailsData.PAOCodeNAme = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() : null;

                            ObjServiceDetailsData.EmpCode = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;

                            ObjServiceDetailsData.ServiceType = objReader["ServiceType"] != DBNull.Value ? Convert.ToString(objReader["ServiceType"]).Trim() : null;

                            ObjServiceDetailsData.ServiceTypeName = objReader["ServiceTypeName"] != DBNull.Value ? Convert.ToString(objReader["ServiceTypeName"]).Trim() : null;

                        }

                    }

                }

            });

            return ObjServiceDetailsData;
        }
        #endregion




        #region insert Update for Service  details
        public async Task<int> SaveUpdateServiceDetails(ServiceDetailsModel objServiceDetailsModel)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdateServiceDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, objServiceDetailsModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "ServiceType", DbType.String, objServiceDetailsModel.ServiceType.Trim());
                    epsdatabase.AddInParameter(dbCommand, "PAOCode", DbType.String, objServiceDetailsModel.PAOCode.Trim());
                    epsdatabase.AddInParameter(dbCommand, "CGHSVerifyFlag", DbType.String, objServiceDetailsModel.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "ClientIP", DbType.String, objServiceDetailsModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objServiceDetailsModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion


    }
}
