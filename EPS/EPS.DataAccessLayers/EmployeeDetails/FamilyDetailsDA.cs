﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class FamilyDetailsDA
    {

       private Database epsdatabase = null;

        public FamilyDetailsDA(Database database)
        {

            epsdatabase = database;
        }

        #region  Get Marital Status 

        public async Task<List<FamilyDetailsModel>> GetMaritalStatus()
        {


            List<FamilyDetailsModel> FamilyDetailsList = new List<FamilyDetailsModel>(); ;

            await Task.Run(() =>
        {
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetMaritalStatus))
            {
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {

                        FamilyDetailsModel FamilyDetailsData = new FamilyDetailsModel();

                        FamilyDetailsData.MaritalStatusID = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]) : null;

                        FamilyDetailsData.MaritalStatusName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;

                        FamilyDetailsList.Add(FamilyDetailsData);

                    }

                }

            }

        });

            return FamilyDetailsList;
        }
        #endregion


        #region insert for family details
        public async Task<int> UpdateEmpFamilyDetails(FamilyDetailsModel objFamilyDetails)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateEmpFamilyDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDependentDetailID", DbType.Int32, objFamilyDetails.MsDependentDetailID);
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objFamilyDetails.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "FirstName", DbType.String, objFamilyDetails.FirstName);
                    epsdatabase.AddInParameter(dbCommand, "MiddleName", DbType.String, objFamilyDetails.MiddleName);
                    epsdatabase.AddInParameter(dbCommand, "LastName", DbType.String, objFamilyDetails.LastName);
                    epsdatabase.AddInParameter(dbCommand, "DOB", DbType.DateTime, objFamilyDetails.DOB);
                    epsdatabase.AddInParameter(dbCommand, "MaritalStatusID", DbType.Int32, objFamilyDetails.MaritalStatusID);
                    epsdatabase.AddInParameter(dbCommand, "RelationshipId", DbType.Int32, objFamilyDetails.RelationshipId);
                    epsdatabase.AddInParameter(dbCommand, "NationalityId", DbType.Int32, objFamilyDetails.NationalityId);
                    epsdatabase.AddInParameter(dbCommand, "PANNumber", DbType.String, objFamilyDetails.PANNumber);
                    epsdatabase.AddInParameter(dbCommand, "AadhaarNumber", DbType.Int64, objFamilyDetails.AadhaarNumber);
                    epsdatabase.AddInParameter(dbCommand, "Saluation", DbType.Int32, objFamilyDetails.Saluation);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objFamilyDetails.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objFamilyDetails.UserId);
                    epsdatabase.AddInParameter(dbCommand, "FamilyDetailsVerifyFlag", DbType.String, objFamilyDetails.VerifyFlag);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion

        #region delete for family details 
        public async Task<int> DeleteFamilyDetails(string empCd, int msDependentDetailID)
        {
            var result = 0;
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteFamilyDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "MsDependentDetailID", DbType.Int32, msDependentDetailID);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion


        #region for get all family details using emp code
        public async Task<List<FamilyDetailsModel>> GetAllFamilyDetails(string empCd, int roleId)
        {
            List<FamilyDetailsModel> FamilyDetailsList = new List<FamilyDetailsModel>();

            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllFamilyDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.String, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            FamilyDetailsModel FamilyDetailsData = new FamilyDetailsModel();


                            FamilyDetailsData.MsDependentDetailID = objReader["MsDependentDetailID"] != DBNull.Value ? Convert.ToInt32(objReader["MsDependentDetailID"]) : 0;
                            FamilyDetailsData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            FamilyDetailsData.MaritalStatusID = objReader["MaritalStatusID"] != DBNull.Value ? Convert.ToString(objReader["MaritalStatusID"]) : null;
                            FamilyDetailsData.RelationshipId = objReader["RelationshipId"] != DBNull.Value ? Convert.ToString(objReader["RelationshipId"]) : null;
                            FamilyDetailsData.NationalityId = objReader["NationalityId"] != DBNull.Value ? Convert.ToString(objReader["NationalityId"]) : null;
                            FamilyDetailsData.Saluation = objReader["Saluation"] != DBNull.Value ? Convert.ToString(objReader["Saluation"]) : null;
                            FamilyDetailsData.FirstName = objReader["FirstName"] != DBNull.Value ? Convert.ToString(objReader["FirstName"]).Trim() : null;
                            FamilyDetailsData.MiddleName = objReader["MiddleName"] != DBNull.Value ? Convert.ToString(objReader["MiddleName"]).Trim() : null;
                            FamilyDetailsData.LastName = objReader["LastName"] != DBNull.Value ? Convert.ToString(objReader["LastName"]).Trim() : null;
                            FamilyDetailsData.PANNumber = objReader["PANNumber"] != DBNull.Value ? Convert.ToString(objReader["PANNumber"]).Trim() : null;
                            FamilyDetailsData.AadhaarNumber = objReader["AadhaarNumber"] != DBNull.Value ? Convert.ToInt64(objReader["AadhaarNumber"]) : 0;
                            FamilyDetailsData.MaritalStatus = objReader["MaritalStatus"] != DBNull.Value ? Convert.ToString(objReader["MaritalStatus"]).Trim() : null;
                            FamilyDetailsData.Relationship = objReader["Relationship"] != DBNull.Value ? Convert.ToString(objReader["Relationship"]).Trim() : null;
                            FamilyDetailsData.DOB = objReader["DOB"] != DBNull.Value ? Convert.ToDateTime(objReader["DOB"]) : FamilyDetailsData.DOB = null;

                            FamilyDetailsList.Add(FamilyDetailsData);

                        }

                    }

                }
            });
            return FamilyDetailsList;
        }
        #endregion


        #region Get   FamilyDetails 

        public async Task<FamilyDetailsModel> GetFamilyDetails(string empCd, int msDependentDetailID)
        {
            FamilyDetailsModel FamilyDetailsData = new FamilyDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getFamilyDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "MsDependentDetailID", DbType.Int32, msDependentDetailID);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            FamilyDetailsData.MsDependentDetailID = objReader["MsDependentDetailID"] != DBNull.Value ? Convert.ToInt32(objReader["MsDependentDetailID"]) : 0;
                            FamilyDetailsData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            FamilyDetailsData.MaritalStatusID = objReader["MaritalStatusID"] != DBNull.Value ? Convert.ToString(objReader["MaritalStatusID"]) : null;
                            FamilyDetailsData.RelationshipId = objReader["RelationshipId"] != DBNull.Value ? Convert.ToString(objReader["RelationshipId"]) : null;
                            FamilyDetailsData.NationalityId = objReader["NationalityId"] != DBNull.Value ? Convert.ToString(objReader["NationalityId"]) : null;
                            FamilyDetailsData.Saluation = objReader["Saluation"] != DBNull.Value ? Convert.ToString(objReader["Saluation"]) : null;
                            FamilyDetailsData.FirstName = objReader["FirstName"] != DBNull.Value ? Convert.ToString(objReader["FirstName"]).Trim() : null;
                            FamilyDetailsData.MiddleName = objReader["MiddleName"] != DBNull.Value ? Convert.ToString(objReader["MiddleName"]).Trim() : null;
                            FamilyDetailsData.LastName = objReader["LastName"] != DBNull.Value ? Convert.ToString(objReader["LastName"]).Trim() : null;
                            FamilyDetailsData.PANNumber = objReader["PANNumber"] != DBNull.Value ? Convert.ToString(objReader["PANNumber"]).Trim() : null;
                            FamilyDetailsData.AadhaarNumber = objReader["AadhaarNumber"] != DBNull.Value ? Convert.ToInt64(objReader["AadhaarNumber"]) : 0;
                            FamilyDetailsData.MaritalStatus = objReader["MaritalStatus"] != DBNull.Value ? Convert.ToString(objReader["MaritalStatus"]).Trim() : null;
                            FamilyDetailsData.Relationship = objReader["Relationship"] != DBNull.Value ? Convert.ToString(objReader["Relationship"]).Trim() : null;
                            FamilyDetailsData.DOB = objReader["DOB"] != DBNull.Value ? Convert.ToDateTime(objReader["DOB"]) : FamilyDetailsData.DOB = null;
                        }

                    }

                }
            });
            return FamilyDetailsData;

        }
        #endregion
    }

}
