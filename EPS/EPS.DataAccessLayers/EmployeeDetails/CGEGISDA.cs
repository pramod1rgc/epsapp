﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class CGEGISDA
    {

        private Database epsdatabase = null;

        public CGEGISDA(Database database)
        {
            epsdatabase = database;
        }

        #region  For Get Insurance Applicable
        public async Task<IEnumerable<CGEGISModel>> GetInsuranceApplicable(string EmpCode)
        {
            List<CGEGISModel> objModelList = new List<CGEGISModel>(); ;

            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetInsuranceApplicable))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String ,EmpCode);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            CGEGISModel ObjModellData = new CGEGISModel();

                            ObjModellData.Gis_deduction = objReader["MsCddirID"] != DBNull.Value ? Convert.ToInt32(objReader["MsCddirID"]) : ObjModellData.Gis_deduction = 0;

                            ObjModellData.Gis_deductionName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : ObjModellData.Gis_deductionName = null;

                            objModelList.Add(ObjModellData);

                        }

                    }

                }

            });

            return objModelList;
        }
        #endregion



        #region  For Get Deputation Case Type
        public async Task<IEnumerable<CGEGISModel>> GetCGEGISCategory(int insuranceApplicableId,string EmpCode)
        {
            List<CGEGISModel> objModelList = new List<CGEGISModel>(); ;

            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCGEGISCategory))
                {
                    epsdatabase.AddInParameter(dbCommand, "insuranceApplicableId", DbType.Int32, insuranceApplicableId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, EmpCode);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            CGEGISModel ObjModellData = new CGEGISModel();

                            ObjModellData.PayGisApplicable = objReader["MsCddirID"] != DBNull.Value ? Convert.ToString(objReader["MsCddirID"]).Trim() : ObjModellData.PayGisApplicable = null;

                            ObjModellData.PayGisApplicableName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : ObjModellData.PayGisApplicableName = null;

                            objModelList.Add(ObjModellData);

                        }

                    }

                }

            });

            return objModelList;
        }
        #endregion



        #region Insert Update CGEGIS Details

        public async Task<int> SaveUpdateCGEGISDetails(CGEGISModel objCGEGISModel)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdateCGEGISDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, objCGEGISModel.EmpCode);
                     
                    epsdatabase.AddInParameter(dbCommand, "Gis_deduction", DbType.Int32, objCGEGISModel.Gis_deduction);
                    epsdatabase.AddInParameter(dbCommand, "PayGisApplicable", DbType.String, objCGEGISModel.PayGisApplicable);
                    epsdatabase.AddInParameter(dbCommand, "PayGISGrp_CD", DbType.String, objCGEGISModel.PayGISGrp_CD);
                    epsdatabase.AddInParameter(dbCommand, "GisMembershipDT", DbType.DateTime, objCGEGISModel.GisMembershipDT);
                    epsdatabase.AddInParameter(dbCommand, "CgegisVerifyFlag", DbType.String, objCGEGISModel.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "ClientIP", DbType.String, objCGEGISModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy",DbType.Int32, objCGEGISModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;  
        }
        #endregion


        #region Get  CGEGIS Details
        public async Task<CGEGISModel> GetCGEGISDetails(string EmpCode ,int RoleId)
        {
            CGEGISModel objCGEGISModel = new CGEGISModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getCGEGISDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, RoleId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            objCGEGISModel.Gis_deduction = objReader["MsCddirIDInsAppl"] != DBNull.Value ? Convert.ToInt32(objReader["MsCddirIDInsAppl"]) : objCGEGISModel.Gis_deduction = null;
                            
                            objCGEGISModel.Gis_deductionName = objReader["Gis_deductionName"] != DBNull.Value ? Convert.ToString(objReader["Gis_deductionName"]).Trim() : objCGEGISModel.Gis_deductionName = null;
                            
                             objCGEGISModel.PayGisApplicable = objReader["PayGisApplicable"] != DBNull.Value ? Convert.ToString(objReader["PayGisApplicable"]).Trim() : objCGEGISModel.PayGisApplicable = null;

                            objCGEGISModel.PayGisApplicableName = objReader["PayGisApplicableName"] != DBNull.Value ? Convert.ToString(objReader["PayGisApplicableName"]).Trim() : objCGEGISModel.PayGisApplicableName = null;

                            objCGEGISModel.PayGISGrp_CD = objReader["PayGISGrp_CD"] != DBNull.Value ? Convert.ToInt32(objReader["PayGISGrp_CD"]) : objCGEGISModel.PayGISGrp_CD = null;

                            objCGEGISModel.GroupName = objReader["GroupName"] != DBNull.Value ? Convert.ToString(objReader["GroupName"]).Trim() : objCGEGISModel.GroupName = null;
                            
                            objCGEGISModel.GisMembershipDT = objReader["GisMembershipDT"] != DBNull.Value ? Convert.ToDateTime(objReader["GisMembershipDT"]) : objCGEGISModel.GisMembershipDT = null;
                             
                        }

                    }

                }
            });
            return objCGEGISModel;

        }
        #endregion
    }
}
