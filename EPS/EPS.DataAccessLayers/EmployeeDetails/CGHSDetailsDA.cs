﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
   public class CGHSDetailsDA
    {

       private Database epsdatabase = null;

        public CGHSDetailsDA(Database database)
        {
            epsdatabase = database;
        }

        #region Insert Update CGHS Details

        public async Task<int> SaveUpdateCGHSDetails(CGHSModel objCGHSModel)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdateCGHSDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objCGHSModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "EmpCghsFlag", DbType.String, objCGHSModel.EmpCghsFlag);
                    epsdatabase.AddInParameter(dbCommand, "CgisDeduct", DbType.String, objCGHSModel.CgisDeduct); // for option
                    epsdatabase.AddInParameter(dbCommand, "EmpCghsNo", DbType.String, objCGHSModel.EmpCghsNo);
                    epsdatabase.AddInParameter(dbCommand, "CghsDescription", DbType.String, objCGHSModel.CghsDescription);
                    epsdatabase.AddInParameter(dbCommand, "CGHSVerifyFlag", DbType.String, objCGHSModel.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "ClientIP", DbType.String, objCGHSModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.Int32, objCGHSModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                 }
            });
            return result;
        }
        #endregion


        #region Get  CGHS Details
        public async Task<CGHSModel> GetCGHSDetails(string empCd,int roleId)
        {
            CGHSModel objCGHSModel = new CGHSModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCGHSDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            objCGHSModel.CgisDeduct = objReader["CgisDeduct"] != DBNull.Value ? Convert.ToString(objReader["CgisDeduct"]) :   null;
                            objCGHSModel.EmpCghsFlag = objReader["EmpCghsFlag"] != DBNull.Value ? Convert.ToString(objReader["EmpCghsFlag"]) :  null;
                            objCGHSModel.EmpCghsNo = objReader["EmpCghsNo"] != DBNull.Value ? Convert.ToString(objReader["EmpCghsNo"]) :   null;
                            objCGHSModel.CghsDescription = objReader["CghsDescription"] != DBNull.Value ? Convert.ToString(objReader["CghsDescription"]) : null;

                             
                        }

                    }

                }
            });
            return objCGHSModel;

        }
        #endregion
    }
}
