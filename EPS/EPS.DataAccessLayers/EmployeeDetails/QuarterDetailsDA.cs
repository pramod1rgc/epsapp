﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class QuarterDetailsDA
    {

       private Database epsdatabase = null;

        public QuarterDetailsDA(Database database)
        {

            epsdatabase = database;
        }


        #region Get get QuarterOwned by
        public async Task<IEnumerable<QuarterDetailsModel>> GetQuarterOwnedby()
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetQuarterOwnedby))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.QrtrOwnedBy = objReader["QrtrOwnedBy"] != DBNull.Value ? Convert.ToInt32(objReader["QrtrOwnedBy"]) :   0;
                            objQuarterDetailsModel.QrtrOwnedByName = objReader["QrtrOwnedByName"] != DBNull.Value ? Convert.ToString(objReader["QrtrOwnedByName"]).Trim() : null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion


        #region Get Allotted To
        public async Task<IEnumerable<QuarterDetailsModel>> GetAllottedTo()
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllottedTo))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.AccmQrtrAllot = objReader["AccmQrtrAllot"] != DBNull.Value ? Convert.ToInt32(objReader["AccmQrtrAllot"]) :0;
                            objQuarterDetailsModel.AccmQrtrAllotName = objReader["AccmQrtrAllotName"] != DBNull.Value ? Convert.ToString(objReader["AccmQrtrAllotName"]).Trim() :null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion


        #region Get RentStatus 
        public async Task<IEnumerable<QuarterDetailsModel>> GetRentStatus()
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetRentStatus))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.AccmRentStat = objReader["AccmRentStat"] != DBNull.Value ? Convert.ToInt32(objReader["AccmRentStat"]) :  0;
                            objQuarterDetailsModel.AccmRentStatName = objReader["AccmRentStatName"] != DBNull.Value ? Convert.ToString(objReader["AccmRentStatName"]).Trim() :  null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion


        #region Get QuarterType 
        public async Task<IEnumerable<QuarterDetailsModel>> GetQuarterType()
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetQuarterType))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.AccmQrtrType = objReader["MsQuarterTypeID"] != DBNull.Value ? Convert.ToInt32(objReader["MsQuarterTypeID"]) :   0;
                            objQuarterDetailsModel.AccmQrtrTypeName = objReader["QuarterType"] != DBNull.Value ? Convert.ToString(objReader["QuarterType"]).Trim() :  null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion

        #region   Get Custodian
        public async Task<IEnumerable<QuarterDetailsModel>> GetCustodian(int QrtrOwnedBy)
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCustodian))
                {
                    epsdatabase.AddInParameter(dbCommand, "QrtrOwnedBy", DbType.String, QrtrOwnedBy);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.CustId = objReader["CustID"] != DBNull.Value ? Convert.ToInt32(objReader["CustID"]) :  0;
                            objQuarterDetailsModel.CustName = objReader["CustDesc"] != DBNull.Value ? Convert.ToString(objReader["CustDesc"]).Trim() :   null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion


        #region   Get GPRA City Location 
        public async Task<IEnumerable<QuarterDetailsModel>> GetGPRACityLocation()
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetGPRACityLocation))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.GpraCityCode = objReader["GpraCityCode"] != DBNull.Value ? Convert.ToString(objReader["GpraCityCode"]).Trim() :  null;
                            objQuarterDetailsModel.GpraCityName = objReader["GpraCityCodeCityName"] != DBNull.Value ? Convert.ToString(objReader["GpraCityCodeCityName"]).Trim() :  null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion


        #region Get Quarter All Details
        public async Task<IEnumerable<QuarterDetailsModel>> QuarterAllDetails(string empCd, int roleId)
        {
            List<QuarterDetailsModel> _listQuarterDetailsModel = new List<QuarterDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_QuarterAllDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.String, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
                            objQuarterDetailsModel.MSEmpAccmID = objReader["EmpGovtRentID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpGovtRentID"]) : 0;
                            objQuarterDetailsModel.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            objQuarterDetailsModel.AccmQrtrAllot = objReader["ErntQrtrAllot"] != DBNull.Value ? Convert.ToInt32(objReader["ErntQrtrAllot"]) : 0;
                            objQuarterDetailsModel.AccmQrtrAllotName = objReader["ErntQrtrAllotName"] != DBNull.Value ? Convert.ToString(objReader["ErntQrtrAllotName"]) : null;
                            objQuarterDetailsModel.AccmOccupDt = objReader["ErntOccupDt"] != DBNull.Value ? Convert.ToDateTime(objReader["ErntOccupDt"]) : objQuarterDetailsModel.AccmOccupDt = null;
                            objQuarterDetailsModel.AllotLetterNo = objReader["AllotLetterNo"] != DBNull.Value ? Convert.ToString(objReader["AllotLetterNo"]) : null;
                            objQuarterDetailsModel.QrtrOwnedBy = objReader["QrtrOwnedBy"] != DBNull.Value ? Convert.ToInt32(objReader["QrtrOwnedBy"]) : 0;
                            objQuarterDetailsModel.QrtrOwnedByName = objReader["QrtrOwnedByName"] != DBNull.Value ? Convert.ToString(objReader["QrtrOwnedByName"]).Trim() : null;
                            objQuarterDetailsModel.CustId = objReader["ErntCustId"] != DBNull.Value ? Convert.ToInt32(objReader["ErntCustId"]) : objQuarterDetailsModel.CustId = 0;
                            objQuarterDetailsModel.CustName = objReader["ErntCustName"] != DBNull.Value ? Convert.ToString(objReader["ErntCustName"]).Trim() : null;
                            objQuarterDetailsModel.AccmQrtrType = objReader["ErntQrtrType"] != DBNull.Value ? Convert.ToInt32(objReader["ErntQrtrType"]) : 0;
                            objQuarterDetailsModel.AccmQrtrTypeName = objReader["ErntQrtrTypeName"] != DBNull.Value ? Convert.ToString(objReader["ErntQrtrTypeName"]).Trim() : null;
                            objQuarterDetailsModel.AccmRentStat = objReader["ErntRentStat"] != DBNull.Value ? Convert.ToInt32(objReader["ErntRentStat"]) : objQuarterDetailsModel.AccmRentStat = 0;
                            objQuarterDetailsModel.AccmRentStatName = objReader["ErntRentStatName"] != DBNull.Value ? Convert.ToString(objReader["ErntRentStatName"]).Trim() : null;
                            objQuarterDetailsModel.AccmRent = objReader["ErntRent"] != DBNull.Value ? Convert.ToInt32(objReader["ErntRent"]) : objQuarterDetailsModel.AccmRent = 0;
                            objQuarterDetailsModel.AccmWaterCharge = objReader["ErntWaterCharge"] != DBNull.Value ? Convert.ToInt32(objReader["ErntWaterCharge"]) : 0;
                            objQuarterDetailsModel.AccmAddRent1 = objReader["erntAddRent1"] != DBNull.Value ? Convert.ToInt32(objReader["erntAddRent1"]) : 0;
                            objQuarterDetailsModel.AccmGarageRent = objReader["ErntGarageRent"] != DBNull.Value ? Convert.ToInt32(objReader["ErntGarageRent"]) : 0;
                            objQuarterDetailsModel.AccmServCharge = objReader["ErntServCharge"] != DBNull.Value ? Convert.ToInt32(objReader["ErntServCharge"]) : 0;
                            objQuarterDetailsModel.CommonAreaMaint = objReader["CommonAreaMaint"] != DBNull.Value ? Convert.ToInt32(objReader["CommonAreaMaint"]) : 0;
                            objQuarterDetailsModel.LawnMaintCharges = objReader["LawnMaintCharges"] != DBNull.Value ? Convert.ToInt32(objReader["LawnMaintCharges"]) : 0;
                            objQuarterDetailsModel.SusBookingYn = objReader["SusBookingYn"] != DBNull.Value ? Convert.ToString(objReader["SusBookingYn"]) : null;
                            objQuarterDetailsModel.Aan = objReader["Aan"] != DBNull.Value ? Convert.ToString(objReader["Aan"]) : objQuarterDetailsModel.Aan = null;
                            objQuarterDetailsModel.GpraCityCode = objReader["GpraCityCode"] != DBNull.Value ? Convert.ToString(objReader["GpraCityCode"]) : null;
                            objQuarterDetailsModel.GpraCityName = objReader["GpraCityName"] != DBNull.Value ? Convert.ToString(objReader["GpraCityName"]).Trim() : null;
                            objQuarterDetailsModel.QrtrAddress1 = objReader["QrtrAddress1"] != DBNull.Value ? Convert.ToString(objReader["QrtrAddress1"]) : null;
                            objQuarterDetailsModel.QrtrAddress2 = objReader["QrtrAddress2"] != DBNull.Value ? Convert.ToString(objReader["QrtrAddress2"]) : null;
                            _listQuarterDetailsModel.Add(objQuarterDetailsModel);
                        }

                    }

                }
            });
            return _listQuarterDetailsModel;

        }
        #endregion



        #region Get Quarter  Details
        public async Task<QuarterDetailsModel> QuarterDetails(string empCd, int mSEmpAccmID)
        {
            QuarterDetailsModel objQuarterDetailsModel = new QuarterDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_QuarterDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "MSEmpAccmID", DbType.Int32, mSEmpAccmID);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {

                            objQuarterDetailsModel.MSEmpAccmID = objReader["EmpGovtRentID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpGovtRentID"]) :0;
                            objQuarterDetailsModel.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            objQuarterDetailsModel.AccmQrtrAllot = objReader["ErntQrtrAllot"] != DBNull.Value ? Convert.ToInt32(objReader["ErntQrtrAllot"]) :  0;
                            objQuarterDetailsModel.AccmOccupDt = objReader["ErntOccupDt"] != DBNull.Value ? Convert.ToDateTime(objReader["ErntOccupDt"]) : objQuarterDetailsModel.AccmOccupDt = null;
                            objQuarterDetailsModel.AllotLetterNo = objReader["AllotLetterNo"] != DBNull.Value ? Convert.ToString(objReader["AllotLetterNo"]).Trim() :  null;
                            objQuarterDetailsModel.AllotLetterDt = objReader["AllotLetterDt"] != DBNull.Value ? Convert.ToDateTime(objReader["AllotLetterDt"]) : objQuarterDetailsModel.AllotLetterDt = null;
                            objQuarterDetailsModel.QrtrOwnedBy = objReader["QrtrOwnedBy"] != DBNull.Value ? Convert.ToInt32(objReader["QrtrOwnedBy"]) : 0;
                            objQuarterDetailsModel.CustId = objReader["ErntCustId"] != DBNull.Value ? Convert.ToInt32(objReader["ErntCustId"]) : objQuarterDetailsModel.CustId = 0;
                            objQuarterDetailsModel.AccmQrtrType = objReader["ErntQrtrType"] != DBNull.Value ? Convert.ToInt32(objReader["ErntQrtrType"]) : 0;
                            objQuarterDetailsModel.AccmRentStat = objReader["ErntRentStat"] != DBNull.Value ? Convert.ToInt32(objReader["ErntRentStat"]) : 0;
                            objQuarterDetailsModel.AccmRent = objReader["ErntRent"] != DBNull.Value ? Convert.ToInt32(objReader["ErntRent"]) : objQuarterDetailsModel.AccmRent = 0;
                            objQuarterDetailsModel.AccmWaterCharge = objReader["ErntWaterCharge"] != DBNull.Value ? Convert.ToInt32(objReader["ErntWaterCharge"]) :  0;
                            objQuarterDetailsModel.AccmAddRent1 = objReader["erntAddRent1"] != DBNull.Value ? Convert.ToInt32(objReader["erntAddRent1"]) :  0;
                            objQuarterDetailsModel.AccmGarageRent = objReader["ErntGarageRent"] != DBNull.Value ? Convert.ToInt32(objReader["ErntGarageRent"]) :  0;
                            objQuarterDetailsModel.AccmServCharge = objReader["ErntServCharge"] != DBNull.Value ? Convert.ToInt32(objReader["ErntServCharge"]) : 0;
                            objQuarterDetailsModel.CommonAreaMaint = objReader["CommonAreaMaint"] != DBNull.Value ? Convert.ToInt32(objReader["CommonAreaMaint"]) :  0;
                            objQuarterDetailsModel.LawnMaintCharges = objReader["LawnMaintCharges"] != DBNull.Value ? Convert.ToInt32(objReader["LawnMaintCharges"]) :  0;
                            objQuarterDetailsModel.SusBookingYn = objReader["SusBookingYn"] != DBNull.Value ? Convert.ToString(objReader["SusBookingYn"]).Trim() :  null;
                            objQuarterDetailsModel.Aan = objReader["Aan"] != DBNull.Value ? Convert.ToString(objReader["Aan"]).Trim() : objQuarterDetailsModel.Aan = null;
                            objQuarterDetailsModel.GpraCityCode = objReader["GpraCityCode"] != DBNull.Value ? Convert.ToString(objReader["GpraCityCode"]).Trim() :  null;
                            objQuarterDetailsModel.QrtrAddress1 = objReader["QrtrAddress1"] != DBNull.Value ? Convert.ToString(objReader["QrtrAddress1"]).Trim() :  null;
                            objQuarterDetailsModel.QrtrAddress2 = objReader["QrtrAddress2"] != DBNull.Value ? Convert.ToString(objReader["QrtrAddress2"]).Trim() : null;
                            objQuarterDetailsModel.AllocAndVac = "allocate";
                            objQuarterDetailsModel.VacatingLetterNo = objReader["VacatingLetterNo"] != DBNull.Value ? Convert.ToString(objReader["VacatingLetterNo"]).Trim() :  null;
                            objQuarterDetailsModel.VacatingLetterDt = objReader["VacatingLetterDt"] != DBNull.Value ? Convert.ToDateTime(objReader["VacatingLetterDt"]) : objQuarterDetailsModel.VacatingLetterDt = null;
                            objQuarterDetailsModel.AccmVacateDt = objReader["ErntVacateDt"] != DBNull.Value ? Convert.ToDateTime(objReader["ErntVacateDt"]) : objQuarterDetailsModel.AccmVacateDt = null;


                        }

                    }

                }
            });
            return objQuarterDetailsModel;

        }
        #endregion



        #region Save Update Quarter Details
        public async Task<int> SaveUpdateQuarterDetails(QuarterDetailsModel objQuarterDetailsModel)
        {
            var result = 0;
            if (objQuarterDetailsModel.AllocAndVac == "allocate")
            {
                await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdateQuarterDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objQuarterDetailsModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "MSEmpAccmID", DbType.Int32, objQuarterDetailsModel.MSEmpAccmID);
                    epsdatabase.AddInParameter(dbCommand, "AccmQrtrAllot", DbType.Int32, objQuarterDetailsModel.AccmQrtrAllot);
                    epsdatabase.AddInParameter(dbCommand, "AccmOccupDt", DbType.DateTime, objQuarterDetailsModel.AccmOccupDt);
                    epsdatabase.AddInParameter(dbCommand, "AllotLetterNo", DbType.String, objQuarterDetailsModel.AllotLetterNo);
                    epsdatabase.AddInParameter(dbCommand, "AllotLetterDt", DbType.DateTime, objQuarterDetailsModel.AllotLetterDt);
                    epsdatabase.AddInParameter(dbCommand, "QrtrOwnedBy", DbType.Int32, objQuarterDetailsModel.QrtrOwnedBy);
                    epsdatabase.AddInParameter(dbCommand, "CustId", DbType.Int32, objQuarterDetailsModel.CustId);
                    epsdatabase.AddInParameter(dbCommand, "AccmQrtrType", DbType.Int32, objQuarterDetailsModel.AccmQrtrType);
                    epsdatabase.AddInParameter(dbCommand, "AccmRentStat", DbType.Int32, objQuarterDetailsModel.AccmRentStat);

                    epsdatabase.AddInParameter(dbCommand, "AccmRent", DbType.Int32, objQuarterDetailsModel.AccmRent);
                    epsdatabase.AddInParameter(dbCommand, "AccmWaterCharge", DbType.Int32, objQuarterDetailsModel.AccmWaterCharge);
                    epsdatabase.AddInParameter(dbCommand, "AccmAddRent1", DbType.Int32, objQuarterDetailsModel.AccmAddRent1); //Electric Charge
                    epsdatabase.AddInParameter(dbCommand, "AccmGarageRent", DbType.Int32, objQuarterDetailsModel.AccmGarageRent);
                    epsdatabase.AddInParameter(dbCommand, "AccmServCharge", DbType.Int32, objQuarterDetailsModel.AccmServCharge);
                    epsdatabase.AddInParameter(dbCommand, "CommonAreaMaint", DbType.Int32, objQuarterDetailsModel.CommonAreaMaint);
                    epsdatabase.AddInParameter(dbCommand, "LawnMaintCharges", DbType.Int32, objQuarterDetailsModel.LawnMaintCharges);
                    epsdatabase.AddInParameter(dbCommand, "SusBookingYn", DbType.String, objQuarterDetailsModel.SusBookingYn);
                    epsdatabase.AddInParameter(dbCommand, "Aan", DbType.String, objQuarterDetailsModel.Aan);
                    epsdatabase.AddInParameter(dbCommand, "GpraCityCode", DbType.String, objQuarterDetailsModel.GpraCityCode);
                    epsdatabase.AddInParameter(dbCommand, "QrtrAddress1", DbType.String, objQuarterDetailsModel.QrtrAddress1);
                    epsdatabase.AddInParameter(dbCommand, "QrtrAddress2", DbType.String, objQuarterDetailsModel.QrtrAddress2);
                   
                    epsdatabase.AddInParameter(dbCommand, "allocAndVac", DbType.String, objQuarterDetailsModel.AllocAndVac);
                    epsdatabase.AddInParameter(dbCommand, "AccmVacateDt", DbType.DateTime, objQuarterDetailsModel.AccmVacateDt);
                    epsdatabase.AddInParameter(dbCommand, "VacatingLetterNo", DbType.String, objQuarterDetailsModel.VacatingLetterNo);
                    epsdatabase.AddInParameter(dbCommand, "VacatingLetterDt", DbType.DateTime, objQuarterDetailsModel.VacatingLetterDt);

                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objQuarterDetailsModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objQuarterDetailsModel.UserId);
                    epsdatabase.AddInParameter(dbCommand, "QuarterDetailsVerifyFlag", DbType.String, objQuarterDetailsModel.VerifyFlag);
                  


                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }


            });
            }
            else
            {
                await Task.Run(() =>
                {

                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdateQuarterDetails))
                    {
                        epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, objQuarterDetailsModel.EmpCd);
                        epsdatabase.AddInParameter(dbCommand, "MSEmpAccmID", DbType.Int32, objQuarterDetailsModel.MSEmpAccmID);
                         epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objQuarterDetailsModel.IpAddress);
                        epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objQuarterDetailsModel.UserId);
                          epsdatabase.AddInParameter(dbCommand, "allocAndVac", DbType.String, objQuarterDetailsModel.AllocAndVac);
                        epsdatabase.AddInParameter(dbCommand, "AccmVacateDt", DbType.DateTime, objQuarterDetailsModel.AccmVacateDt);
                        epsdatabase.AddInParameter(dbCommand, "VacatingLetterNo", DbType.String, objQuarterDetailsModel.VacatingLetterNo);
                        epsdatabase.AddInParameter(dbCommand, "VacatingLetterDt", DbType.DateTime, objQuarterDetailsModel.VacatingLetterDt);
                        result = epsdatabase.ExecuteNonQuery(dbCommand);

                    }


                });


            }
            return result;

        }
        #endregion


        #region delete for DeleteQuarterDetails
        public int DeleteQuarterDetails(string empCd, int mSEmpAccmID)
        {
            int result = 0;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteQuarterDetails))
            {
                epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                epsdatabase.AddInParameter(dbCommand, "MSEmpAccmID", DbType.Int32, mSEmpAccmID);
                result = epsdatabase.ExecuteNonQuery(dbCommand);
            }

            return result;

        }
        #endregion
    }
}
