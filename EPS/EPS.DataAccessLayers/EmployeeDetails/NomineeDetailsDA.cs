﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class NomineeDetailsDA
    {

        private Database epsdatabase = null;

        public NomineeDetailsDA(Database database)
        {

            epsdatabase = database;
        }

        #region Get All  Nominee Details 
        public async Task<IEnumerable<NomineeModel>> GetAllNomineeDetails(string empCd, int roleID)
        {
            List<NomineeModel> _listNomineeModel = new List<NomineeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllNomineeDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "roleID", DbType.Int32, roleID);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NomineeModel objNomineeModel = new NomineeModel();
                            objNomineeModel.NomineeID = objReader["NomineeID"] != DBNull.Value ? Convert.ToInt32(objReader["NomineeID"]) : 0;
                            objNomineeModel.Empcd = objReader["Empcd"] != DBNull.Value ? Convert.ToString(objReader["Empcd"]).Trim() : null;
                            objNomineeModel.NomineeFirstName = objReader["NomineeFirstName"] != DBNull.Value ? Convert.ToString(objReader["NomineeFirstName"]).Trim() : null;
                            objNomineeModel.NomineeMiddleName = objReader["NomineeMiddleName"] != DBNull.Value ? Convert.ToString(objReader["NomineeMiddleName"]).Trim() : null;
                            objNomineeModel.NomineeLastName = objReader["NomineeLastName"] != DBNull.Value ? Convert.ToString(objReader["NomineeLastName"]).Trim() : null;
                            objNomineeModel.NomineeLastName = objReader["NomineeLastName"] != DBNull.Value ? Convert.ToString(objReader["NomineeLastName"]).Trim() : null;
                            objNomineeModel.NomineeDOBMinor = objReader["NomineeDOBMinor"] != DBNull.Value ? Convert.ToDateTime(objReader["NomineeDOBMinor"]) : objNomineeModel.NomineeDOBMinor = null;
                            objNomineeModel.NomineeRelation = objReader["NomineeRelation"] != DBNull.Value ? Convert.ToString(objReader["NomineeRelation"]).Trim() : null;
                            objNomineeModel.NomineeMajorMinorFlag = objReader["NomineeMajorMinorFlag"] != DBNull.Value ? Convert.ToString(objReader["NomineeMajorMinorFlag"]).Trim() : null;
                            objNomineeModel.NomineeGuardianFirstName = objReader["NomineeGuardianFirstName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianFirstName"]).Trim() : null;
                            objNomineeModel.NomineeGuardianMiddleName = objReader["NomineeGuardianMiddleName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianMiddleName"]).Trim() : null;
                            objNomineeModel.NomineeGuardianLastName = objReader["NomineeGuardianLastName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianLastName"]).Trim() : null;
                            objNomineeModel.NomineePercentageShare = objReader["NomineePercentageShare"] != DBNull.Value ? Convert.ToInt32(objReader["NomineePercentageShare"]) : 0;
                            objNomineeModel.NomineeAddressLine1 = objReader["NomineeAddressLine1"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine1"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine2 = objReader["NomineeAddressLine2"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine2"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine3 = objReader["NomineeAddressLine3"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine3"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine4 = objReader["NomineeAddressLine4"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine4"]).Trim() : null;
                            objNomineeModel.NomineeState = objReader["NomineeState"] != DBNull.Value ? Convert.ToString(objReader["NomineeState"]).Trim() : null;
                            objNomineeModel.NomineeCountry = objReader["NomineeCountry"] != DBNull.Value ? Convert.ToString(objReader["NomineeCountry"]).Trim() : null;
                            objNomineeModel.NomineePin = objReader["NomineePin"] != DBNull.Value ? Convert.ToInt32(objReader["NomineePin"]) : 0;
                            objNomineeModel.NomineeRelationName = objReader["NomineeRelationName"] != DBNull.Value ? Convert.ToString(objReader["NomineeRelationName"]) : null;
                            objNomineeModel.NomineeSaluation = objReader["NomineeSaluation"] != DBNull.Value ? Convert.ToString(objReader["NomineeSaluation"]).Trim() : null;
                            objNomineeModel.GuardianSaluation = objReader["GuardianSaluation"] != DBNull.Value ? Convert.ToString(objReader["GuardianSaluation"]).Trim() : null;
                            _listNomineeModel.Add(objNomineeModel);
                        }

                    }

                }
            });
            return _listNomineeModel;

        }
        #endregion


        #region Get   Nominee Details 
        public async Task<NomineeModel> GetNomineeDetails(string empCd, int nomineeID)
        {
            NomineeModel objNomineeModel = new NomineeModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getNomineeDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                    epsdatabase.AddInParameter(dbCommand, "NomineeID", DbType.Int32, nomineeID);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            objNomineeModel.NomineeID = objReader["NomineeID"] != DBNull.Value ? Convert.ToInt32(objReader["NomineeID"]) : 0;
                            objNomineeModel.Empcd = objReader["Empcd"] != DBNull.Value ? Convert.ToString(objReader["Empcd"]).Trim() : null;
                            objNomineeModel.NomineeFirstName = objReader["NomineeFirstName"] != DBNull.Value ? Convert.ToString(objReader["NomineeFirstName"]).Trim() : null;
                            objNomineeModel.NomineeMiddleName = objReader["NomineeMiddleName"] != DBNull.Value ? Convert.ToString(objReader["NomineeMiddleName"]).Trim() : null;
                            objNomineeModel.NomineeLastName = objReader["NomineeLastName"] != DBNull.Value ? Convert.ToString(objReader["NomineeLastName"]).Trim() : null;
                            objNomineeModel.NomineeDOBMinor = objReader["NomineeDOBMinor"] != DBNull.Value ? Convert.ToDateTime(objReader["NomineeDOBMinor"]) : objNomineeModel.NomineeDOBMinor = null;
                            objNomineeModel.NomineeRelation = objReader["NomineeRelation"] != DBNull.Value ? Convert.ToString(objReader["NomineeRelation"]).Trim() : null;
                            objNomineeModel.NomineeMajorMinorFlag = objReader["NomineeMajorMinorFlag"] != DBNull.Value ? Convert.ToString(objReader["NomineeMajorMinorFlag"]).Trim() : null;
                            objNomineeModel.NomineeGuardianFirstName = objReader["NomineeGuardianFirstName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianFirstName"]).Trim() : null;
                            objNomineeModel.NomineeGuardianMiddleName = objReader["NomineeGuardianMiddleName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianMiddleName"]).Trim() : null;
                            objNomineeModel.NomineeGuardianLastName = objReader["NomineeGuardianLastName"] != DBNull.Value ? Convert.ToString(objReader["NomineeGuardianLastName"]).Trim() : null;
                            objNomineeModel.NomineePercentageShare = objReader["NomineePercentageShare"] != DBNull.Value ? Convert.ToInt32(objReader["NomineePercentageShare"]) : 0;
                            objNomineeModel.NomineeAddressLine1 = objReader["NomineeAddressLine1"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine1"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine2 = objReader["NomineeAddressLine2"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine2"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine3 = objReader["NomineeAddressLine3"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine3"]).Trim() : null;
                            objNomineeModel.NomineeAddressLine4 = objReader["NomineeAddressLine4"] != DBNull.Value ? Convert.ToString(objReader["NomineeAddressLine4"]).Trim() : null;
                            objNomineeModel.NomineeState = objReader["NomineeState"] != DBNull.Value ? Convert.ToString(objReader["NomineeState"]).Trim() : null;
                            objNomineeModel.NomineeCountry = objReader["NomineeCountry"] != DBNull.Value ? Convert.ToString(objReader["NomineeCountry"]).Trim() : null;
                            objNomineeModel.NomineePin = objReader["NomineePin"] != DBNull.Value ? Convert.ToInt32(objReader["NomineePin"]) : 0;
                            objNomineeModel.NomineeSaluation = objReader["NomineeSaluation"] != DBNull.Value ? Convert.ToString(objReader["NomineeSaluation"]).Trim() : null;
                            objNomineeModel.GuardianSaluation = objReader["GuardianSaluation"] != DBNull.Value ? Convert.ToString(objReader["GuardianSaluation"]).Trim() : null;

                        }

                    }

                }
            });
            return objNomineeModel;

        }
        #endregion


        #region insert Update for Nominee details
        public async Task<int> UpdateNomineeDetails(NomineeModel objNomineeDetails)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateNomineeDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "NomineeID", DbType.String, objNomineeDetails.NomineeID);
                    epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, objNomineeDetails.Empcd);
                    epsdatabase.AddInParameter(dbCommand, "NomineeFirstName", DbType.String, objNomineeDetails.NomineeFirstName);
                    epsdatabase.AddInParameter(dbCommand, "NomineeMiddleName", DbType.String, objNomineeDetails.NomineeMiddleName);
                    epsdatabase.AddInParameter(dbCommand, "NomineeLastName", DbType.String, objNomineeDetails.NomineeLastName);
                    epsdatabase.AddInParameter(dbCommand, "NomineeDOBMinor", DbType.DateTime, objNomineeDetails.NomineeDOBMinor);
                    epsdatabase.AddInParameter(dbCommand, "NomineeRelation", DbType.String, objNomineeDetails.NomineeRelation);
                    epsdatabase.AddInParameter(dbCommand, "NomineeMajorMinorFlag", DbType.String, objNomineeDetails.NomineeMajorMinorFlag);
                    epsdatabase.AddInParameter(dbCommand, "NomineeGuardianFirstName", DbType.String, objNomineeDetails.NomineeGuardianFirstName);
                    epsdatabase.AddInParameter(dbCommand, "NomineeGuardianMiddleName", DbType.String, objNomineeDetails.NomineeGuardianMiddleName);
                    epsdatabase.AddInParameter(dbCommand, "NomineeGuardianLastName", DbType.String, objNomineeDetails.NomineeGuardianLastName);
                    epsdatabase.AddInParameter(dbCommand, "NomineePercentageShare", DbType.Int32, objNomineeDetails.NomineePercentageShare);
                    epsdatabase.AddInParameter(dbCommand, "NomineeAddressLine1", DbType.String, objNomineeDetails.NomineeAddressLine1);
                    epsdatabase.AddInParameter(dbCommand, "NomineeAddressLine2", DbType.String, objNomineeDetails.NomineeAddressLine2);
                    epsdatabase.AddInParameter(dbCommand, "NomineeAddressLine3", DbType.String, objNomineeDetails.NomineeAddressLine3);
                    epsdatabase.AddInParameter(dbCommand, "NomineeAddressLine4", DbType.String, objNomineeDetails.NomineeAddressLine4);
                    epsdatabase.AddInParameter(dbCommand, "NomineeState", DbType.String, objNomineeDetails.NomineeState);
                    epsdatabase.AddInParameter(dbCommand, "NomineeCountry", DbType.String, objNomineeDetails.NomineeCountry);
                    epsdatabase.AddInParameter(dbCommand, "NomineePin", DbType.Int32, objNomineeDetails.NomineePin);
                    epsdatabase.AddInParameter(dbCommand, "NomineeSaluation", DbType.Int32, objNomineeDetails.NomineeSaluation);
                    epsdatabase.AddInParameter(dbCommand, "GuardianSaluation", DbType.Int32, objNomineeDetails.GuardianSaluation);

                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objNomineeDetails.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objNomineeDetails.UserId);
                    epsdatabase.AddInParameter(dbCommand, "NomineeDetailsVerifyFlag", DbType.String, objNomineeDetails.VerifyFlag);


                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion


        #region delete for Nominee details 
        public int DeleteNomineeDetails(string empCd, int nomineeID)
        {
            int result = 0;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteNomineeDetails))
            {
                epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCd);
                epsdatabase.AddInParameter(dbCommand, "NomineeID", DbType.Int32, nomineeID);
                result = epsdatabase.ExecuteNonQuery(dbCommand);
            }

            return result;

        }
        #endregion

    }
}
