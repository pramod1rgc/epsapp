﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class PostingDetailsDA
    {
        private Database epsdatabase = null;

        public PostingDetailsDA(Database database)
        {

            epsdatabase = database;
        }

        #region  Get HRA City Class

        public async Task<IEnumerable<PostingDetailsModel>> GetHRACity()
        {


            List<PostingDetailsModel> objPostingDetailsList = new List<PostingDetailsModel>(); ;


            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getHRACity))
                {
                    //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PostingDetailsModel ObjPostingDetalsData = new PostingDetailsModel();

                            ObjPostingDetalsData.PayCityClass = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]) : null;

                            ObjPostingDetalsData.HRACityName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;

                            objPostingDetailsList.Add(ObjPostingDetalsData);

                        }

                    }

                }

            });

            return objPostingDetailsList;
        }
        #endregion

        #region  Get TA City Class

        public async Task<IEnumerable<PostingDetailsModel>> GetTACity()
        {


            List<PostingDetailsModel> objPostingDetailsList = new List<PostingDetailsModel>(); ;

            await Task.Run(() =>
          {
              using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getTACity))
              {
                  //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                  using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                  {
                      while (objReader.Read())
                      {

                          PostingDetailsModel ObjPostingDetalsData = new PostingDetailsModel();

                          ObjPostingDetalsData.CityClassTA = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeValue"]) : null;

                          ObjPostingDetalsData.TACityName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;

                          objPostingDetailsList.Add(ObjPostingDetalsData);

                      }

                  }

              }

          });

            return objPostingDetailsList;
        }
        #endregion

        #region  Get All Designation

        public async Task<IEnumerable<PostingDetailsModel>> GetAllDesignation()
        {
            List<PostingDetailsModel> objPostingDetailsList = new List<PostingDetailsModel>(); ;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllDesignation))
                {
                    //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PostingDetailsModel ObjPostingDetalsData = new PostingDetailsModel();

                            ObjPostingDetalsData.DesigID = objReader["MsDesigMastID"] != DBNull.Value ? Convert.ToInt32(objReader["MsDesigMastID"]) : 0;

                            ObjPostingDetalsData.DesigDesc = objReader["DesigDesc"] != DBNull.Value ? Convert.ToString(objReader["DesigDesc"]).Trim() : null;

                            objPostingDetailsList.Add(ObjPostingDetalsData);

                        }

                    }

                }

            });

            return objPostingDetailsList;
        }

        #endregion


        #region insert Update for Posting details
        public async Task<int> SaveUpdatePostingDetails(PostingDetailsModel objPostingDetailsModel)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_SaveUpdatePostingDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, objPostingDetailsModel.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "EmpCuroffDt", DbType.DateTime, objPostingDetailsModel.EmpCuroffDt);
                    epsdatabase.AddInParameter(dbCommand, "EmpCurdesigDt", DbType.DateTime, objPostingDetailsModel.EmpCurdesigDt);
                    epsdatabase.AddInParameter(dbCommand, "EmpFieldDeptCd", DbType.Int32, objPostingDetailsModel.EmpFieldDeptCd);
                    epsdatabase.AddInParameter(dbCommand, "EmpCurpostMode", DbType.Int32, objPostingDetailsModel.EmpCurpostMode);
                    epsdatabase.AddInParameter(dbCommand, "CityClassTA", DbType.String, objPostingDetailsModel.CityClassTA);
                    epsdatabase.AddInParameter(dbCommand, "PayCityClass", DbType.String, objPostingDetailsModel.PayCityClass);
                    epsdatabase.AddInParameter(dbCommand, "PostingVerifFlag", DbType.String, objPostingDetailsModel.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "ClientIP", DbType.String, objPostingDetailsModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objPostingDetailsModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion


        // GetAllPostingDetails
        #region  Get All Designation
        public async Task<PostingDetailsModel> GetAllPostingDetails(string Empcd, int roleId)
        {

            PostingDetailsModel ObjPostingDetalsData = new PostingDetailsModel();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllPostingDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "Empcd", DbType.String, Empcd);
                    epsdatabase.AddInParameter(dbCommand, "roleId", DbType.Int32, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            ObjPostingDetalsData.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]).Trim() : null;
                            ObjPostingDetalsData.EmpCd = objReader["Empcd"] != DBNull.Value ? Convert.ToString(objReader["Empcd"]).Trim() : null;
                            ObjPostingDetalsData.EmpCuroffDt = objReader["EmpCuroffDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpCuroffDt"]) : ObjPostingDetalsData.EmpCuroffDt = null;
                            ObjPostingDetalsData.EmpCurdesigDt = objReader["EmpCurdesigDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpCurdesigDt"]) : ObjPostingDetalsData.EmpCuroffDt = null;
                            ObjPostingDetalsData.EmpFieldDeptCd = objReader["EmpFieldDeptCd"] != DBNull.Value ? Convert.ToInt32(objReader["EmpFieldDeptCd"]) : 0;
                            ObjPostingDetalsData.EmpCurpostMode = objReader["EmpCurpostMode"] != DBNull.Value ? Convert.ToString(objReader["EmpCurpostMode"]).Trim() : null;
                            ObjPostingDetalsData.CityClassTA = objReader["CityClassTA"] != DBNull.Value ? Convert.ToString(objReader["CityClassTA"]).Trim() : null;
                            ObjPostingDetalsData.PayCityClass = objReader["PayCityClass"] != DBNull.Value ? Convert.ToString(objReader["PayCityClass"]).Trim() : null;
                            ObjPostingDetalsData.TACityName = objReader["TACityName"] != DBNull.Value ? Convert.ToString(objReader["TACityName"]).Trim() : null;
                            ObjPostingDetalsData.HRACityName = objReader["HRACityName"] != DBNull.Value ? Convert.ToString(objReader["HRACityName"]).Trim() : null;
                            ObjPostingDetalsData.Designation = objReader["Designation"] != DBNull.Value ? Convert.ToString(objReader["Designation"]).Trim() : null;
                            ObjPostingDetalsData.JoiningMode = objReader["JoiningMode"] != DBNull.Value ? Convert.ToString(objReader["JoiningMode"]).Trim() : null;

                        }

                    }

                }

            });

            return ObjPostingDetalsData;
        }
        #endregion



    }
}
