﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class ForwardToCheckerDA
    {
        private Database epsdatabase = null;

        public ForwardToCheckerDA(Database database)
        {
            epsdatabase = database;
        }

        #region for get all family details using emp code
        public async Task<List<ForwardToCheckerModel>> GetAllEmpDetails(string empCd, int roleId)
        {
            List<ForwardToCheckerModel> ForwardToCheckerList = new List<ForwardToCheckerModel>();

            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllEmpDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.String, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ForwardToCheckerModel ForwardToCheckerModelData = new ForwardToCheckerModel();
                            ForwardToCheckerModelData.EmpCode = objReader["empCode"] != DBNull.Value ? Convert.ToString(objReader["empCode"]).Trim() : null;
                            ForwardToCheckerModelData.EmpName = objReader["empName"] != DBNull.Value ? Convert.ToString(objReader["empName"]).Trim() : null;
                            ForwardToCheckerModelData.EmpDesignation = objReader["empDesignation"] != DBNull.Value ? Convert.ToString(objReader["empDesignation"]).Trim() : null;
                            ForwardToCheckerModelData.ServiceType = objReader["serviceType"] != DBNull.Value ? Convert.ToString(objReader["serviceType"]).Trim() : null;
                            ForwardToCheckerModelData.EmpType = objReader["empType"] != DBNull.Value ? Convert.ToString(objReader["empType"]).Trim() : null;
                            ForwardToCheckerModelData.EmpSubType = objReader["empSubType"] != DBNull.Value ? Convert.ToString(objReader["empSubType"]).Trim() : null;
                            ForwardToCheckerModelData.EmpJoiningMode = objReader["empJoiningMode"] != DBNull.Value ? Convert.ToString(objReader["empJoiningMode"]).Trim() : null;
                            ForwardToCheckerModelData.EmpJoiningCategory = objReader["empJoiningCategory"] != DBNull.Value ? Convert.ToString(objReader["empJoiningCategory"]).Trim() : null;
                            ForwardToCheckerModelData.EmpDateOfBirth = objReader["empDateOfBirth"] != DBNull.Value ? Convert.ToDateTime(objReader["empDateOfBirth"]) : ForwardToCheckerModelData.EmpDateOfBirth = null;
                            ForwardToCheckerModelData.EmpDateOfJoining = objReader["empDateOfJoining"] != DBNull.Value ? Convert.ToDateTime(objReader["empDateOfJoining"]) : ForwardToCheckerModelData.EmpDateOfJoining = null;
                            ForwardToCheckerModelData.EmpPanNo = objReader["empPanNo"] != DBNull.Value ? Convert.ToString(objReader["empPanNo"]) : null;
                            ForwardToCheckerModelData.Status = objReader["empPanNo"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            ForwardToCheckerModelData.Remark = objReader["remark"] != DBNull.Value ? Convert.ToString(objReader["remark"]) : null;
                            ForwardToCheckerList.Add(ForwardToCheckerModelData);
                        }

                    }

                }
            });
            return ForwardToCheckerList;
        }
        #endregion


 

        #region for get all emplloyee complete Details
        public async Task<object[]> GetAllEmployeeCompleteDetails(int roleId,string empCode)
        {

            object[] listTable = new object[11];
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllEmployeeCompleteDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.String, roleId);
                    epsdatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                    DataSet ds = epsdatabase.ExecuteDataSet(dbCommand);

                    listTable[0] = ds.Tables[0]; // For Personal Details
                    listTable[1] = ds.Tables[1]; // For Physical Details
                    listTable[2] = ds.Tables[2];  // For Postinng Details
                    listTable[3] = ds.Tables[3];  // For CGEGIS Details
                    listTable[4] = ds.Tables[4];  // For CGHS Details
                    listTable[5] = ds.Tables[5];  // For Pay Details
                    listTable[6] = ds.Tables[6];  // For Service Details
                    listTable[7] = ds.Tables[7];  // For Bank Details
                    listTable[8] = ds.Tables[8];  // For Quarter Details
                    listTable[9] = ds.Tables[9];  // For Family Details
                    listTable[10] = ds.Tables[10];  // For Nominee Details
                   // listTable[3] = ds.Tables[7];  // For deputation Details
                    
                }
            });
            return listTable;
        }
        #endregion



        #region forward to checker emp details
        public async Task<int> ForwardToCheckerEmpDetails(ForwardToCheckerModel objForwardToCheckerModel)
        {
            //  DataTable dtempcodes = CommonClasses.CommonFunctions.ToDataTable(objForwardToCheckerModel.empCodes);

            DataTable dtempcodes = new DataTable();

            dtempcodes.Columns.Add("EmpCd");

            for (int j = 0; j < objForwardToCheckerModel.EmpCodes.Count; j++)
            {
                // create a DataRow using .NewRow()
                DataRow row = dtempcodes.NewRow();


                row[j] = objForwardToCheckerModel.EmpCodes[j];
                dtempcodes.Rows.Add(row);
            }

            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_ForwardToCheckerEmpDtls))
                {
                    SqlParameter para = new SqlParameter("tblempcodes", dtempcodes);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objForwardToCheckerModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }


        #endregion

         #region Verify Emp Data

        public async Task<int> VerifyEmpData(ForwardToCheckerModel objForwardToCheckerModel)
        {
       
        var result = 0;
        await Task.Run(() =>
            {
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_VerifyEmpData))
            {
                     
               epsdatabase.AddInParameter(dbCommand, "empCode", DbType.String, objForwardToCheckerModel.EmpCode);
               epsdatabase.AddInParameter(dbCommand, "status", DbType.String, objForwardToCheckerModel.Status);
               epsdatabase.AddInParameter(dbCommand, "remark", DbType.String, objForwardToCheckerModel.Remark);
                result = epsdatabase.ExecuteNonQuery(dbCommand);

            }
        });
            return result;

        }
#endregion




    }
}
