﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class PayDetailsDA
    {

        private Database epsdatabase = null;

        public PayDetailsDA(Database database)
        {

            epsdatabase = database;
        }

        #region  Get Organisation Type
        public async Task<IEnumerable<PayDetailsModel>> GetOrganisationType(string empCode)
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetOrganisationType))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.Organisation_Type = objReader["CddirCodeValue"] != DBNull.Value ? Convert.ToInt32(objReader["CddirCodeValue"]) : 0;

                            ObjPayDetailsModelData.Organisation_TypeName = objReader["CddirCodeText"] != DBNull.Value ? Convert.ToString(objReader["CddirCodeText"]).Trim() : null;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion

        #region  Get PayLevel
        public async Task<IEnumerable<PayDetailsModel>> GetPayLevel()
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayLevel))
                {
                    //epsdatabase.AddInParameter(dbCommand, "PayLevel", DbType.String, "PayLevel");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.PayLevel = objReader["PayMatrixLevel"] != DBNull.Value ? Convert.ToString(objReader["PayMatrixLevel"]).Trim() : null;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion



        #region For Get Entitle Office vehicle
        public async Task<IEnumerable<PayDetailsModel>> GetEntitledOffVeh(string Module)
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEntitledOffVeh))
                {
                    epsdatabase.AddInParameter(dbCommand, "Module", DbType.String, Module);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();
                            ObjPayDetailsModelData.EntitledOffVehID = objReader["StatusID"] != DBNull.Value ? Convert.ToInt32(objReader["StatusID"]) : 0;
                            ObjPayDetailsModelData.EntitledOffVehIDName = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]).Trim() : null;
                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion

        #region  Get PayIndex
        public async Task<IEnumerable<PayDetailsModel>> GetPayIndex(string payLevel)
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayIndex))
                {
                    epsdatabase.AddInParameter(dbCommand, "PayLevel", DbType.String, payLevel);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.PayIndex = objReader["MsPayMatrixID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayMatrixID"]) : 0;

                            ObjPayDetailsModelData.PayIndexName = objReader["PayMatrixStage"] != DBNull.Value ? Convert.ToInt32(objReader["PayMatrixStage"]) : 0;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get Basic Pay using PaayLevel and PayIndex

        public async Task<IEnumerable<PayDetailsModel>> GetBasicDetails(string payLevel, int payIndex)
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetBasicDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "PayLevel", DbType.String, payLevel);
                    epsdatabase.AddInParameter(dbCommand, "PayIndex", DbType.String, payIndex);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.BasicPay = objReader["BasicPay"] != DBNull.Value ? Convert.ToInt32(objReader["BasicPay"]) : 0;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get Grade Pay

        public async Task<IEnumerable<PayDetailsModel>> GetGradePay()
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetGradePay))
                {

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.GradePay = objReader["PayScalePscGradePay"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGradePay"]) : 0;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get Pay Scale

        public async Task<IEnumerable<PayDetailsModel>> GetPayScale(int gradePay)
        {
            List<PayDetailsModel> objPayDetailsModel = new List<PayDetailsModel>(); ;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayScale))
                {
                    epsdatabase.AddInParameter(dbCommand, "GradePay", DbType.Int32, gradePay);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();

                            ObjPayDetailsModelData.PscScaleCd = objReader["PayScalePscScaleCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscScaleCD"]) : 0;
                            ObjPayDetailsModelData.PayScalePscDscr = objReader["PayScalePscDscr"] != DBNull.Value ? Convert.ToString(objReader["PayScalePscDscr"]).Trim() : null;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get Non_Computational_Dues
        public async Task<IEnumerable<NonComputationalDues>> GetNonComputationalDues()
        {
            List<NonComputationalDues> objPayDetailsModel = new List<NonComputationalDues>();

            await Task.Run(() =>
            {
                // Note : we get the all dues in payitemsDetails where PayitemCode < 200
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetNonComputationalDues))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NonComputationalDues ObjPayDetailsModelData = new NonComputationalDues();

                            ObjPayDetailsModelData.DuesId = objReader["DuesId"] != DBNull.Value ? Convert.ToInt32(objReader["DuesId"]) : 0;

                            ObjPayDetailsModelData.DuesName = objReader["DuesName"] != DBNull.Value ? Convert.ToString(objReader["DuesName"]).Trim() : null;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get Non Computational Deductions
        public async Task<IEnumerable<NonComputationalDeduction>> GetNonComputationalDeductions()
        {
            List<NonComputationalDeduction> objPayDetailsModel = new List<NonComputationalDeduction>(); ;

            await Task.Run(() =>
            {
                // Note : we get the all deduction in payitemsDetails where PayitemCode > 200
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetNonComputationalDeductions))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NonComputationalDeduction ObjPayDetailsModelData = new NonComputationalDeduction();

                            ObjPayDetailsModelData.DeductionId = objReader["DeductionId"] != DBNull.Value ? Convert.ToInt32(objReader["DeductionId"]) : 0;

                            ObjPayDetailsModelData.DeductionName = objReader["DeductionName"] != DBNull.Value ? Convert.ToString(objReader["DeductionName"]).Trim() : null;

                            objPayDetailsModel.Add(ObjPayDetailsModelData);

                        }

                    }

                }

            });

            return objPayDetailsModel;
        }
        #endregion


        #region  Get All Pay details
        public async Task<PayDetailsModel> GetPayDetails(string empCode, int roleId)
        {
            PayDetailsModel ObjPayDetailsModelData = new PayDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ObjPayDetailsModelData.EmpCode = objReader["EmpCode"] != DBNull.Value ? Convert.ToString(objReader["EmpCode"]).Trim() : null;
                            ObjPayDetailsModelData.PayComm = objReader["PayComm"] != DBNull.Value ? Convert.ToInt32(objReader["PayComm"]) : 0;
                            ObjPayDetailsModelData.PayCommName = objReader["PayCommName"] != DBNull.Value ? Convert.ToString(objReader["PayCommName"]).Trim() : null;
                            ObjPayDetailsModelData.PayLevel = objReader["PayLevel"] != DBNull.Value ? Convert.ToString(objReader["PayLevel"]).Trim() : null;
                            ObjPayDetailsModelData.PayLevelName = objReader["PayLevelName"] != DBNull.Value ? Convert.ToString(objReader["PayLevelName"]).Trim() : null;
                            ObjPayDetailsModelData.PayIndex = objReader["PayIndex"] != DBNull.Value ? Convert.ToInt32(objReader["PayIndex"]) : 0;
                            ObjPayDetailsModelData.PayIndexName = objReader["PayIndexName"] != DBNull.Value ? Convert.ToInt32(objReader["PayIndexName"]) : 0;
                            ObjPayDetailsModelData.BasicPay = objReader["BasicPay"] != DBNull.Value ? Convert.ToInt32(objReader["BasicPay"]) : 0;
                            ObjPayDetailsModelData.BasicPayDT = objReader["BasicPayDT"] != DBNull.Value ? Convert.ToDateTime(objReader["BasicPayDT"]) : ObjPayDetailsModelData.BasicPayDT = null;
                            ObjPayDetailsModelData.IncrpayDT = objReader["IncrpayDT"] != DBNull.Value ? Convert.ToDateTime(objReader["IncrpayDT"]) : ObjPayDetailsModelData.IncrpayDT = null;
                            ObjPayDetailsModelData.Organisation_Type = objReader["Organisation_Type"] != DBNull.Value ? Convert.ToInt32(objReader["Organisation_Type"]) : 0;
                            ObjPayDetailsModelData.PayInPb = objReader["PayInPb"] != DBNull.Value ? Convert.ToInt32(objReader["PayInPb"]) : 0;
                            ObjPayDetailsModelData.GradePay = objReader["GradePay"] != DBNull.Value ? Convert.ToInt32(objReader["GradePay"]) : 0;
                            ObjPayDetailsModelData.PscScaleCd = objReader["PscScaleCd"] != DBNull.Value ? Convert.ToInt32(objReader["PscScaleCd"]) : 0;
                            ObjPayDetailsModelData.EntitledOffVehID = objReader["EntitledOffVeh"] != DBNull.Value ? Convert.ToInt32(objReader["EntitledOffVeh"]) : 0;
                            ObjPayDetailsModelData.PayScalePscDscr = objReader["PayScalePscDscr"] != DBNull.Value ? Convert.ToString(objReader["PayScalePscDscr"]).Trim() : null;


                        }
                    }
                }

                // Get for Dues =============================
                List<NonComputationalDues> objPayDuesDetailsModel = new List<NonComputationalDues>();
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpSaveDuesDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NonComputationalDues ObjEmpSaveDues = new NonComputationalDues();

                            ObjEmpSaveDues.DuesId = objReader["DuesId"] != DBNull.Value ? Convert.ToInt32(objReader["DuesId"]) : 0;

                            ObjEmpSaveDues.DuesName = objReader["DuesName"] != DBNull.Value ? Convert.ToString(objReader["DuesName"]).Trim() : null;

                            objPayDuesDetailsModel.Add(ObjEmpSaveDues);

                        }

                    }

                }

                ObjPayDetailsModelData.Dues = objPayDuesDetailsModel;

                // ================== get Deduction Details============================

                List<NonComputationalDeduction> objPayDetailsModel = new List<NonComputationalDeduction>(); ;


                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpSavedDeductionDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, empCode);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            NonComputationalDeduction ObjSavedDeductionDetails = new NonComputationalDeduction();

                            ObjSavedDeductionDetails.DeductionId = objReader["DeductionId"] != DBNull.Value ? Convert.ToInt32(objReader["DeductionId"]) : 0;

                            ObjSavedDeductionDetails.DeductionName = objReader["DeductionName"] != DBNull.Value ? Convert.ToString(objReader["DeductionName"]).Trim() : null;

                            objPayDetailsModel.Add(ObjSavedDeductionDetails);

                        }

                    }

                }
                ObjPayDetailsModelData.Deduction = objPayDetailsModel;


            });

            return ObjPayDetailsModelData;
        }
        #endregion


        #region insert Update for Posting details

        public async Task<int> SaveUpdatePayDetails(PayDetailsModel objPayDetailsModel)
        {
            DataTable Dues = CommonClasses.CommonFunctions.ToDataTable(objPayDetailsModel.Dues);
            Dues.Columns.Remove("DuesName");
            DataTable Deduction = CommonClasses.CommonFunctions.ToDataTable(objPayDetailsModel.Deduction);
            Deduction.Columns.Remove("DeductionName");
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.USP_SaveUpdatePayDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCode", DbType.String, objPayDetailsModel.EmpCode);
                    epsdatabase.AddInParameter(dbCommand, "Organisation_Type", DbType.Int32, objPayDetailsModel.Organisation_Type);
                    epsdatabase.AddInParameter(dbCommand, "PayComm", DbType.String, objPayDetailsModel.PayComm);
                    //epsdatabase.AddInParameter(dbCommand, "PayBand", DbType.String, objPayDetailsModel.PayBand);
                    epsdatabase.AddInParameter(dbCommand, "PayLevel", DbType.String, objPayDetailsModel.PayLevel);
                    epsdatabase.AddInParameter(dbCommand, "PayIndex", DbType.Int32, objPayDetailsModel.PayIndex);
                    epsdatabase.AddInParameter(dbCommand, "BasicPay", DbType.Int32, objPayDetailsModel.BasicPay);
                    epsdatabase.AddInParameter(dbCommand, "GradePay", DbType.Int32, objPayDetailsModel.GradePay);
                    epsdatabase.AddInParameter(dbCommand, "PscScaleCd", DbType.Int32, objPayDetailsModel.PscScaleCd);
                    epsdatabase.AddInParameter(dbCommand, "PayInPb", DbType.Int32, objPayDetailsModel.PayInPb);
                    epsdatabase.AddInParameter(dbCommand, "EntitledOffVeh", DbType.Int32, objPayDetailsModel.EntitledOffVehID);
                    epsdatabase.AddInParameter(dbCommand, "BasicPayDT", DbType.DateTime, objPayDetailsModel.BasicPayDT);
                    epsdatabase.AddInParameter(dbCommand, "IncrpayDT", DbType.DateTime, objPayDetailsModel.IncrpayDT);
                    SqlParameter para = new SqlParameter("tblDuesMapping", Dues);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);
                    SqlParameter para1 = new SqlParameter("tblDeductionMapping", Deduction);
                    para1.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para1);
                    epsdatabase.AddInParameter(dbCommand, "PayVerifyFlag", DbType.String, objPayDetailsModel.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "ClientIP", DbType.String, objPayDetailsModel.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objPayDetailsModel.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion
    }
}
