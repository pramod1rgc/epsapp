﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using EPS.BusinessModels.EmployeeDetails;
using System.Threading.Tasks;
using System.Data.Common;
using EPS.CommonClasses;
using System.Data;
using System.Collections.Generic;

namespace EPS.DataAccessLayers.EmployeeDetails
{
    public class BankDetailsDA
    {

        private Database epsdatabase = null;

        public BankDetailsDA(Database database)
        {

            epsdatabase = database;
        }


        #region Get Bank Details By IFSC 
        public async Task<BankDetailsModel> GetBankDetailsByIFSC(string ifscCD)
        {
            BankDetailsModel objBankModel = new BankDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getBankDetailsByIFSC))
                {
                    epsdatabase.AddInParameter(dbCommand, "IfscCD", DbType.String, ifscCD);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {

                            //objBankModel.BankId = objReader["BankID"] != DBNull.Value ? Convert.ToInt32(objReader["BankID"]) : 0;
                            //objBankModel.BranchId = objReader["BranchID"] != DBNull.Value ? Convert.ToInt32(objReader["BranchID"]) : 0;
                            objBankModel.BankName = objReader["BankName"] != DBNull.Value ? Convert.ToString(objReader["BankName"]).Trim() : null;
                            objBankModel.BranchName = objReader["BranchName"] != DBNull.Value ? Convert.ToString(objReader["BranchName"]).Trim() : null;


                        }

                    }

                }
            });
            return objBankModel;

        }
        #endregion

        #region Bind IFSC Code
        public IEnumerable<BankDetailsModel> GetAllIFSC()
        {
            List<BankDetailsModel> objIfscModel = new List<BankDetailsModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getAllIFSC))
            {
               
                using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        BankDetailsModel objBankModel = new BankDetailsModel();
                        objBankModel.IfscCD = Convert.ToString(rdr["BankIFSCCode"]).Trim();
                        objIfscModel.Add(objBankModel);

                    }
                }
            }
            return objIfscModel;
        }
        #endregion




        #region Get   Bank Details 
        public async Task<BankDetailsModel> GetBankDetails(string empCD, int roleId)
        {
            BankDetailsModel objBankModel = new BankDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getBankDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, empCD);
                    epsdatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            objBankModel.BankName = objReader["BankName"] != DBNull.Value ? Convert.ToString(objReader["BankName"]).Trim() : null;
                            objBankModel.BankId = objReader["BankID"] != DBNull.Value ? Convert.ToInt32(objReader["BankID"]) : 0;
                            objBankModel.BranchId = objReader["BranchID"] != DBNull.Value ? Convert.ToInt32(objReader["BranchID"]) : 0;
                            objBankModel.BranchName = objReader["BranchName"] != DBNull.Value ? Convert.ToString(objReader["BranchName"]).Trim() : null;
                            objBankModel.IfscCD = objReader["IfscCD"] != DBNull.Value ? Convert.ToString(objReader["IfscCD"]).Trim() : null;
                            objBankModel.BnkAcNo = objReader["BnkAcNo"] != DBNull.Value ? Convert.ToString(objReader["BnkAcNo"]).Trim() : null;
                            objBankModel.confirmbnkAcNo = objReader["BnkAcNo"] != DBNull.Value ? Convert.ToString(objReader["BnkAcNo"]).Trim() : null;

                        }

                    }

                }
            });
            return objBankModel;

        }
        #endregion



        #region insert Update for Bank details
        public async Task<int> UpdateBankDetails(BankDetailsModel objBankDetails)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateBankDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "EmpCD", DbType.String, objBankDetails.EmpCD);
                    epsdatabase.AddInParameter(dbCommand, "BankId", DbType.Int32, objBankDetails.BankId);
                    epsdatabase.AddInParameter(dbCommand, "BranchId", DbType.Int32, objBankDetails.BranchId);
                    epsdatabase.AddInParameter(dbCommand, "BnkAcNo", DbType.String, objBankDetails.BnkAcNo);
                    epsdatabase.AddInParameter(dbCommand, "IfscCD", DbType.String, objBankDetails.IfscCD);
                    epsdatabase.AddInParameter(dbCommand, "IPaddress", DbType.String, objBankDetails.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "ServiceVerifyFlag", DbType.String, objBankDetails.VerifyFlag);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objBankDetails.UserId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion

    }
}
