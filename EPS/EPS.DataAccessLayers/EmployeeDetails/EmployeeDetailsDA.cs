﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers
{
    public class EmployeeDetailsDA
    {
        //  string con = ConfigurationManager.ConnectionStrings["EPSConnection"].ConnectionString;
       private Database epsDataBase = null;

        public EmployeeDetailsDA(Database database)
        {

            epsDataBase = database;
        }


        public async Task<List<EmployeeDetailsModel>> GetVerifyEmpCode()
        {
            // string con = Startup.ConnectionString; 
            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetVerifyEmpCode))
                {
                    //epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmployeeDetailsModel EmployeeDetailsData = new EmployeeDetailsModel();

                            EmployeeDetailsData.Name = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) :  null;

                            EmployeeDetailsData.id = objReader["EMPCd"] != DBNull.Value ? Convert.ToString(objReader["EMPCd"]).Trim() : null;

                            EmployeeDetailsList.Add(EmployeeDetailsData);

                        }

                    }

                }
            });
            if (EmployeeDetailsList == null)
                return null;
            else
                return EmployeeDetailsList;
        }





        public async Task<IEnumerable<EmployeeDetailsModel>> GetPhysicalDisabilityTypes()
        {
            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetPhysicalDisabilityTypes))
                {
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmployeeDetailsModel EmployeeDetailsData = new EmployeeDetailsModel();

                            EmployeeDetailsData.PhType = objReader["PhType"] != DBNull.Value ? Convert.ToString(objReader["PhType"]) : null;

                            EmployeeDetailsData.PhTypeName = objReader["PhTypeName"] != DBNull.Value ? Convert.ToString(objReader["PhTypeName"]).Trim() : null;

                            EmployeeDetailsList.Add(EmployeeDetailsData);

                        }

                    }

                }
            });
            if (EmployeeDetailsList == null)
                return null;
            else
                return EmployeeDetailsList;
        }


        public async Task<EmployeeDetailsModel> GetEmpPersonalDetailsByID(string empCode, int RoleId)
        {
            EmployeeDetailsModel EmployeeDetailsData = new EmployeeDetailsModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.Usp_GetEmpByID))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                    epsDataBase.AddInParameter(dbCommand, "RoleId", DbType.Int32, RoleId);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {



                            EmployeeDetailsData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;
                            EmployeeDetailsData.EmpFirstName = objReader["EmpFirstName"] != DBNull.Value ? Convert.ToString(objReader["EmpFirstName"]).Trim() : null;
                            EmployeeDetailsData.EmpMiddleName = objReader["EmpMiddleName"] != DBNull.Value ? Convert.ToString(objReader["EmpMiddleName"]).Trim() : null;
                            EmployeeDetailsData.EmpLastName = objReader["EmpLastName"] != DBNull.Value ? Convert.ToString(objReader["EmpLastName"]).Trim() : null;
                            EmployeeDetailsData.EmpGender = objReader["EmpGender"] != DBNull.Value ? Convert.ToString(objReader["EmpGender"]).Trim() : null;
                            EmployeeDetailsData.GenderName = objReader["GenderName"] != DBNull.Value ? Convert.ToString(objReader["GenderName"]).Trim() : null;
                            EmployeeDetailsData.Emp_type = objReader["EmpType"] != DBNull.Value ? Convert.ToString(objReader["EmpType"]).Trim() : null;
                            EmployeeDetailsData.Emp_adhaar_no = objReader["EmpAdhaarNo"] != DBNull.Value ? Convert.ToString(objReader["EmpAdhaarNo"]).Trim() : null;
                            EmployeeDetailsData.Emp_pan_no = objReader["EmpPanNo"] != DBNull.Value ? Convert.ToString(objReader["EmpPanNo"]).Trim() : null;
                            EmployeeDetailsData.EmpSubType = objReader["EmpSubType"] != DBNull.Value ? Convert.ToString(objReader["EmpSubType"]).Trim() : null;
                            EmployeeDetailsData.Joining_Catogary = objReader["Joining_Catogary"] != DBNull.Value ? Convert.ToString(objReader["Joining_Catogary"]).Trim() : null;
                            EmployeeDetailsData.EmpTitle = objReader["EmpTitle"] != DBNull.Value ? Convert.ToString(objReader["EmpTitle"]).Trim() : null;
                            EmployeeDetailsData.EmpDOB = objReader["EmpDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpDOB"]) : EmployeeDetailsData.EmpDOB = null;
                            EmployeeDetailsData.EmpCuroffDt = objReader["EmpCuroffDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpCuroffDt"]) : EmployeeDetailsData.EmpCuroffDt = null;
                            EmployeeDetailsData.EmpJoinDt = objReader["EmpJoinDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpJoinDt"]) : EmployeeDetailsData.EmpJoinDt = null;
                            EmployeeDetailsData.EmpPhFlag = objReader["EmpPhFlag"] != DBNull.Value ? Convert.ToString(objReader["EmpPhFlag"]).Trim() : null;
                            EmployeeDetailsData.EmpApptType = objReader["EmpApptType"] != DBNull.Value ? Convert.ToString(objReader["EmpApptType"]).Trim() : null;
                            EmployeeDetailsData.Emp_mobile_no = objReader["EmpMobileNo"] != DBNull.Value ? Convert.ToString(objReader["EmpMobileNo"]).Trim() : null;
                            EmployeeDetailsData.EmpServendDt = objReader["EmpServendDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpServendDt"]) : EmployeeDetailsData.EmpServendDt = null;
                            EmployeeDetailsData.EmpEntryDt = Convert.ToDateTime(objReader["EmpEntryDt"]);
                            EmployeeDetailsData.Emp_Email = objReader["Emp_Email"] != DBNull.Value ? Convert.ToString(objReader["Emp_Email"]).Trim() : null;
                            EmployeeDetailsData.EmpApptTypeName = objReader["EmpApptTypeName"] != DBNull.Value ? Convert.ToString(objReader["EmpApptTypeName"]).Trim() : null;
                            EmployeeDetailsData.Emp_typeName = objReader["Emp_typeName"] != DBNull.Value ? Convert.ToString(objReader["Emp_typeName"]).Trim() : null;
                            EmployeeDetailsData.Joining_CatogaryName = objReader["joining_CatogaryName"] != DBNull.Value ? Convert.ToString(objReader["joining_CatogaryName"]).Trim() : null;
                            EmployeeDetailsData.empSubTypeName = objReader["empSubTypeName"] != DBNull.Value ? Convert.ToString(objReader["empSubTypeName"]).Trim() : null;
                            EmployeeDetailsData.ServiceTypeName = objReader["ServiceTypeName"] != DBNull.Value ? Convert.ToString(objReader["ServiceTypeName"]).Trim() : EmployeeDetailsData.ServiceTypeName = null;
                            EmployeeDetailsData.ServiceType = objReader["ServiceType"] != DBNull.Value ? Convert.ToString(objReader["ServiceType"]).Trim() : null;
                            EmployeeDetailsData.DocumentUpload = objReader["DocumentUpload"] != DBNull.Value ? Convert.ToString(objReader["DocumentUpload"]).Trim() : null;
                            EmployeeDetailsData.DocumentUploadName = objReader["DocumentUpload"] != DBNull.Value ? Convert.ToString(objReader["DocumentUpload"]).Trim().Substring(objReader["DocumentUpload"].ToString().LastIndexOf("\\") + 1) : null;
                            EmployeeDetailsData.isDeput = objReader["isDeput"] != DBNull.Value ? Convert.ToBoolean(objReader["isDeput"]) : EmployeeDetailsData.isDeput = null;
                            EmployeeDetailsData.DepartmentCD = objReader["DepartmentCD"] != DBNull.Value ? Convert.ToString(objReader["DepartmentCD"]).Trim() : null;
                            EmployeeDetailsData.RegularisationDate = objReader["regularisationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["regularisationDate"]) : EmployeeDetailsData.RegularisationDate = null;
                            EmployeeDetailsData.LastDateIncrement = objReader["LastDateIncrement"] != DBNull.Value ? Convert.ToDateTime(objReader["LastDateIncrement"]) : EmployeeDetailsData.LastDateIncrement = null;
                            EmployeeDetailsData.RegularisationTime = objReader["regularisationTime"] != DBNull.Value ? Convert.ToString(objReader["regularisationTime"]).Trim() : null;
                            EmployeeDetailsData.DeputTypeID = objReader["deputTypeID"] != DBNull.Value ? Convert.ToString(objReader["deputTypeID"]).Trim() : null;
                            EmployeeDetailsData.DeputName = objReader["DeputName"] != DBNull.Value ? Convert.ToString(objReader["DeputName"]).Trim() : null;
                            EmployeeDetailsData.MsDesigMastID = objReader["MsDesigMastID"] != DBNull.Value ? Convert.ToInt32(objReader["MsDesigMastID"]) : 0;


                        }

                    }

                }
            });
            return EmployeeDetailsData;
        }




        public async Task<List<EmployeeDetailsModel>> GetEmpPHDetails(string empCode)
        {
            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetEmpPHDetails))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            EmployeeDetailsModel EmployeeDetailsData = new EmployeeDetailsModel();

                            EmployeeDetailsData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]).Trim() : null;

                            //  EmployeeDetailsData.EmpFirstName = objReader["EmpFirstName"] != DBNull.Value ? Convert.ToString(objReader["EmpFirstName"]).Trim() : EmployeeDetailsData.EmpFirstName = null;
                            EmployeeDetailsData.PhType = objReader["PhType"] != DBNull.Value ? Convert.ToString(objReader["PhType"]).Trim() : null;
                            EmployeeDetailsData.PhPcnt = objReader["PhPcnt"] != DBNull.Value ? Convert.ToDecimal(objReader["PhPcnt"]) : EmployeeDetailsData.PhPcnt = null;
                            EmployeeDetailsData.PhCertNo = objReader["PhCertNo"] != DBNull.Value ? Convert.ToString(objReader["PhCertNo"]).Trim() : null;
                            EmployeeDetailsData.PhCertDt = objReader["PhCertDt"] != DBNull.Value ? Convert.ToDateTime(objReader["PhCertDt"]) : EmployeeDetailsData.PhCertDt = null;
                            EmployeeDetailsData.PhCertAuth = objReader["PhCertAuth"] != DBNull.Value ? Convert.ToString(objReader["PhCertAuth"]).Trim() : null;
                            EmployeeDetailsData.PhAdmOrdno = objReader["PhAdmOrdno"] != DBNull.Value ? Convert.ToString(objReader["PhAdmOrdno"]).Trim() : null;
                            EmployeeDetailsData.PhAdmOrddt = objReader["PhAdmOrddt"] != DBNull.Value ? Convert.ToDateTime(objReader["PhAdmOrddt"]) : EmployeeDetailsData.PhAdmOrddt = null;
                            EmployeeDetailsData.PhRemarks = objReader["PhRemarks"] != DBNull.Value ? Convert.ToString(objReader["PhRemarks"]).Trim() : null;
                            EmployeeDetailsData.PhVfFlg = objReader["PhVfFlg"] != DBNull.Value ? Convert.ToString(objReader["PhVfFlg"]).Trim() : null;
                            EmployeeDetailsData.EmpDoubleTa = objReader["EmpDoubleTa"] != DBNull.Value ? Convert.ToString(objReader["EmpDoubleTa"]).Trim() : null;
                            EmployeeDetailsData.IsSevere = objReader["IsSevere"] != DBNull.Value ? Convert.ToString(objReader["IsSevere"]).Trim() :  null;
                            EmployeeDetailsList.Add(EmployeeDetailsData);

                        }

                    }

                }
            });
            return EmployeeDetailsList;
        }





        public async Task<List<EmployeeDetailsModel>> MakerGetEmpList(int RoleId)
        {

            List<EmployeeDetailsModel> EmployeeDetailsList = new List<EmployeeDetailsModel>();
            
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_MakerGetEmpList))
                {
                    
                    epsDataBase.AddInParameter(dbCommand, "RoleId", DbType.Int32, RoleId);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            EmployeeDetailsModel EmployeeDetailsData = new EmployeeDetailsModel();
                            // DateTime a =  objReader["EmpDOB"].ToString("dd/MM/yyyy")

                            EmployeeDetailsData.Name = objReader["FullName"] != DBNull.Value ? Convert.ToString(objReader["FullName"]) :null;
                            EmployeeDetailsData.EmpDOB = objReader["EmpDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpDOB"]) : EmployeeDetailsData.EmpDOB = null;
                            EmployeeDetailsData.EmpJoinDt = objReader["EmpJoinDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpJoinDt"]) : EmployeeDetailsData.EmpJoinDt = null;
                            EmployeeDetailsData.EmpCd = objReader["EmpCd"] != DBNull.Value ? Convert.ToString(objReader["EmpCd"]) : null;
                            EmployeeDetailsData.EmpVerifFlag = objReader["EmpVerifFlag"] != DBNull.Value ? Convert.ToString(objReader["EmpVerifFlag"]) : null;
                            EmployeeDetailsData.ServiceType = objReader["ServiceType"] != DBNull.Value ? Convert.ToString(objReader["ServiceType"]) : null;
                            EmployeeDetailsData.isDeput = objReader["IsDeput"] != DBNull.Value ? Convert.ToBoolean(objReader["IsDeput"]) : EmployeeDetailsData.isDeput = null;
                            EmployeeDetailsData.Emp_type = objReader["EmpType"] != DBNull.Value ? Convert.ToString(objReader["EmpType"]) : null;
                            EmployeeDetailsData.EmpSubType = objReader["EmpSubType"] != DBNull.Value ? Convert.ToString(objReader["EmpSubType"]) : null;
                            EmployeeDetailsData.DeputTypeID = objReader["DeputTypeID"] != DBNull.Value ? Convert.ToString(objReader["DeputTypeID"]) : null;
                            EmployeeDetailsData.Joining_Catogary = objReader["Joining_Catogary"] != DBNull.Value ? Convert.ToString(objReader["Joining_Catogary"]) : null;
                            EmployeeDetailsData.EmpApptType = objReader["EmpApptType"] != DBNull.Value ? Convert.ToString(objReader["EmpApptType"]) : null;
                            EmployeeDetailsData.EmpFirstName = objReader["EmpFirstName"] != DBNull.Value ? Convert.ToString(objReader["EmpFirstName"]).Trim() : null;
                            EmployeeDetailsData.EmpMiddleName = objReader["EmpMiddleName"] != DBNull.Value ? Convert.ToString(objReader["EmpMiddleName"]).Trim() : null;
                            EmployeeDetailsData.EmpLastName = objReader["EmpLastName"] != DBNull.Value ? Convert.ToString(objReader["EmpLastName"]).Trim() : null;
                            EmployeeDetailsData.EmpGender = objReader["EmpGender"] != DBNull.Value ? Convert.ToString(objReader["EmpGender"]).Trim() : null;
                            EmployeeDetailsData.Emp_adhaar_no = objReader["EmpAdhaarNo"] != DBNull.Value ? Convert.ToString(objReader["EmpAdhaarNo"]).Trim() : null;
                            EmployeeDetailsData.Emp_pan_no = objReader["EmpPanNo"] != DBNull.Value ? Convert.ToString(objReader["EmpPanNo"]).Trim() : null;
                            EmployeeDetailsData.EmpTitle = objReader["EmpTitle"] != DBNull.Value ? Convert.ToString(objReader["EmpTitle"]).Trim() : null;
                            EmployeeDetailsData.EmpDOB = objReader["EmpDOB"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpDOB"]) : EmployeeDetailsData.EmpDOB = null;
                            EmployeeDetailsData.EmpJoinDt = objReader["EmpJoinDt"] != DBNull.Value ? Convert.ToDateTime(objReader["EmpJoinDt"]) : EmployeeDetailsData.EmpJoinDt = null;
                            EmployeeDetailsData.DepartmentCD = objReader["DepartmentCD"] != DBNull.Value ? Convert.ToString(objReader["DepartmentCD"]).Trim() : null;
                            EmployeeDetailsData.RegularisationDate = objReader["regularisationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["regularisationDate"]) : EmployeeDetailsData.RegularisationDate = null;
                            EmployeeDetailsData.LastDateIncrement = objReader["LastDateIncrement"] != DBNull.Value ? Convert.ToDateTime(objReader["LastDateIncrement"]) : EmployeeDetailsData.LastDateIncrement = null;
                            EmployeeDetailsData.MsDesigMastID = objReader["MsDesigMastID"] != DBNull.Value ? Convert.ToInt32(objReader["MsDesigMastID"]) : 0;
                            EmployeeDetailsData.RegularisationTime = objReader["regularisationTime"] != DBNull.Value ? Convert.ToString(objReader["regularisationTime"]).Trim() : null;
                            EmployeeDetailsData.EmpEntryDt = Convert.ToDateTime(objReader["EmpEntryDt"]);
                            EmployeeDetailsData.DocumentUpload = objReader["DocumentUpload"] != DBNull.Value ? Convert.ToString(objReader["DocumentUpload"]).Trim() : null;
                            EmployeeDetailsData.DocumentUploadName = objReader["DocumentUpload"] != DBNull.Value ? Convert.ToString(objReader["DocumentUpload"]).Trim().Substring(objReader["DocumentUpload"].ToString().LastIndexOf("\\") + 1) : null;
                            // EmployeeDetailsData.StatusCode = ResponseStatus.Success;
                            EmployeeDetailsList.Add(EmployeeDetailsData);

                        }

                    }

                }
            });

            if (EmployeeDetailsList == null)
                return null;
            else
                return EmployeeDetailsList;
        }




        public async Task<int> UpdateEmpDetails(EmployeeDetailsModel objEmployeeDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_UpdateEmpDetails))
                {
                    epsDataBase.AddInParameter(dbCommand, "empServendDt", DbType.DateTime, objEmployeeDetails.EmpServendDt);
                    epsDataBase.AddInParameter(dbCommand, "empCd", DbType.String, objEmployeeDetails.EmpCd);
                    epsDataBase.AddInParameter(dbCommand, "empPhFlag", DbType.String, objEmployeeDetails.EmpPhFlag);
                    epsDataBase.AddInParameter(dbCommand, "emp_mobile_no", DbType.String, objEmployeeDetails.Emp_mobile_no);
                    epsDataBase.AddInParameter(dbCommand, "emp_Email", DbType.String, objEmployeeDetails.Emp_Email.Split("@")[0]);
                    epsDataBase.AddInParameter(dbCommand, "emp_Email2", DbType.String, objEmployeeDetails.Emp_Email.Split("@")[1]);
                    epsDataBase.AddInParameter(dbCommand, "PersonalVerifFlag", DbType.String, objEmployeeDetails.VerifyFlag);
                    epsDataBase.AddInParameter(dbCommand, "UserId", DbType.String, objEmployeeDetails.UserId);
                    epsDataBase.AddInParameter(dbCommand, "ClientIP", DbType.String, objEmployeeDetails.IpAddress);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }



        ///=======================Save and update physical details================
        public async Task<int> SavePHDetails(EmployeeDetailsModel objEmployeeDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_SavePHDetails))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, objEmployeeDetails.EmpCd);
                    epsDataBase.AddInParameter(dbCommand, "PhType", DbType.String, objEmployeeDetails.PhType);
                    epsDataBase.AddInParameter(dbCommand, "PhPcnt", DbType.Decimal, objEmployeeDetails.PhPcnt);
                    epsDataBase.AddInParameter(dbCommand, "PhCertNo", DbType.String, objEmployeeDetails.PhCertNo);
                    epsDataBase.AddInParameter(dbCommand, "PhCertAuth", DbType.String, objEmployeeDetails.PhCertAuth);
                    epsDataBase.AddInParameter(dbCommand, "PhAdmOrdno", DbType.String, objEmployeeDetails.PhAdmOrdno);
                    epsDataBase.AddInParameter(dbCommand, "PhAdmOrddt", DbType.DateTime, objEmployeeDetails.PhAdmOrddt);
                    epsDataBase.AddInParameter(dbCommand, "PhCertDt", DbType.DateTime, objEmployeeDetails.PhCertDt);
                    epsDataBase.AddInParameter(dbCommand, "PhRemarks", DbType.String, objEmployeeDetails.PhRemarks);
                    epsDataBase.AddInParameter(dbCommand, "PhVfFlg", DbType.String, objEmployeeDetails.PhVfFlg);
                    epsDataBase.AddInParameter(dbCommand, "EmpDoubleTa", DbType.String, objEmployeeDetails.EmpDoubleTa);
                    epsDataBase.AddInParameter(dbCommand, "IsSevere", DbType.String, objEmployeeDetails.IsSevere);
                    epsDataBase.AddInParameter(dbCommand, "UserId", DbType.String, objEmployeeDetails.UserId);
                    epsDataBase.AddInParameter(dbCommand, "ClientIP", DbType.String, objEmployeeDetails.IpAddress);

                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }



        public async Task<int> InsertUpdateEmpDetails(EmployeeDetailsModel objEmployeeDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_InsertUpdateEmployee))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, objEmployeeDetails.EmpCd);
                    epsDataBase.AddInParameter(dbCommand, "EmpApptType", DbType.String, objEmployeeDetails.EmpApptType);
                    epsDataBase.AddInParameter(dbCommand, "EmpSubType", DbType.String, objEmployeeDetails.EmpSubType);
                    epsDataBase.AddInParameter(dbCommand, "Joining_Catogary", DbType.String, objEmployeeDetails.Joining_Catogary);
                    epsDataBase.AddInParameter(dbCommand, "EmpTitle", DbType.String, objEmployeeDetails.EmpTitle);
                    epsDataBase.AddInParameter(dbCommand, "EmpFirstName", DbType.String, objEmployeeDetails.EmpFirstName);
                    epsDataBase.AddInParameter(dbCommand, "EmpMiddleName", DbType.String, objEmployeeDetails.EmpMiddleName);
                    epsDataBase.AddInParameter(dbCommand, "EmpLastName", DbType.String, objEmployeeDetails.EmpLastName);
                    epsDataBase.AddInParameter(dbCommand, "EmpDOB", DbType.DateTime, objEmployeeDetails.EmpDOB);
                    epsDataBase.AddInParameter(dbCommand, "EmpJoinDt", DbType.DateTime, objEmployeeDetails.EmpJoinDt);
                    epsDataBase.AddInParameter(dbCommand, "EmpGender", DbType.String, objEmployeeDetails.EmpGender);
                    epsDataBase.AddInParameter(dbCommand, "emp_type", DbType.String, objEmployeeDetails.Emp_type);
                    epsDataBase.AddInParameter(dbCommand, "emp_adhaar_no", DbType.String, objEmployeeDetails.Emp_adhaar_no);
                    epsDataBase.AddInParameter(dbCommand, "emp_pan_no", DbType.String, objEmployeeDetails.Emp_pan_no);
                    epsDataBase.AddInParameter(dbCommand, "emp_entrydate", DbType.DateTime, objEmployeeDetails.EmpEntryDt);
                    epsDataBase.AddInParameter(dbCommand, "emp_servicetype", DbType.String, objEmployeeDetails.ServiceType);
                    epsDataBase.AddInParameter(dbCommand, "empdocupload", DbType.String, objEmployeeDetails.DocumentUpload);
                    epsDataBase.AddInParameter(dbCommand, "isDeput", DbType.Boolean, objEmployeeDetails.isDeput);
                    epsDataBase.AddInParameter(dbCommand, "depatmentCode", DbType.String, objEmployeeDetails.DepartmentCD);
                    epsDataBase.AddInParameter(dbCommand, "regularisationDate", DbType.DateTime, objEmployeeDetails.RegularisationDate);
                    epsDataBase.AddInParameter(dbCommand, "regularisationTime", DbType.String, objEmployeeDetails.RegularisationTime);
                    epsDataBase.AddInParameter(dbCommand, "LastDateIncrement", DbType.DateTime, objEmployeeDetails.LastDateIncrement);
                    epsDataBase.AddInParameter(dbCommand, "deputTypeID", DbType.Int16, objEmployeeDetails.DeputTypeID);
                    epsDataBase.AddInParameter(dbCommand, "EmpVerifFlag", DbType.String, objEmployeeDetails.EmpVerifFlag);
                    epsDataBase.AddInParameter(dbCommand, "EmpOffId", DbType.String, objEmployeeDetails.EmpOffId);
                    epsDataBase.AddInParameter(dbCommand, "UserId", DbType.Int32, objEmployeeDetails.UserId);
                    epsDataBase.AddInParameter(dbCommand, "ClientIP", DbType.String, objEmployeeDetails.IpAddress);
                    epsDataBase.AddInParameter(dbCommand, "MsDesigMastID", DbType.Int32, objEmployeeDetails.MsDesigMastID);

                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }



        #region delete for employee details 
        public async Task<int> DeleteEmployeeDetails(string EmpCd)
        {

            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_DeleteEmployeeDetails))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, EmpCd);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        #endregion


        public async Task<bool> GetIsDeputEmp(string empCode)
        {
            Boolean Isdeput = false;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_GetIsDeputEmp))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            Isdeput = objReader["IsDeput"] != DBNull.Value ? Convert.ToBoolean(objReader["IsDeput"]) : false;

                        }

                    }

                }
            });
            return Isdeput;
        }


        public async Task<int> VerifyRejectionEmployeeDtl(EmployeeDetailsModel objEmployeeDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.SP_VerifyRejectionEmployeeDtl))
                {
                    epsDataBase.AddInParameter(dbCommand, "EmpCd", DbType.String, objEmployeeDetails.EmpCd);
                    epsDataBase.AddInParameter(dbCommand, "EmpVerifFlag", DbType.String, objEmployeeDetails.EmpVerifFlag);
                    epsDataBase.AddInParameter(dbCommand, "EmpRemark", DbType.String, objEmployeeDetails.EmpRemark);

                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }

    }
}
