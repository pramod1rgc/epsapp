﻿using EPS.BusinessModels.Promotion;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Promotion
{
    public class PromotionDAL
    {
        private Database _epsDatabase;

        public PromotionDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        #region PromotionWithoutTransfer
        public async Task<string> UpdatePromotionDetailsWithoutTransfer(PromotionDetails objPro)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpsertPromotionDetailsWithoutTransfer))
                {
                    objPro.TrnGround = 1;
                    msg = CommonClasses.CommonFunctions.MapUpdateParameters(objPro, _epsDatabase, dbCommand);
                }

            });
            return msg;
        }





        public async Task<IEnumerable<GetPromotionDetails>> GetPromotionDetailsWithoutTransfer(string empCode, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
            List<GetPromotionDetails> listOfProDetails = new List<GetPromotionDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetPromotionDetailsWithoutTransfer))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Empcd", DbType.String, empCode);
                    _epsDatabase.AddInParameter(dbCommand, "pageSize", DbType.Int16, pageSize);
                    _epsDatabase.AddInParameter(dbCommand, "pageNumber", DbType.Int16, pageNumber);
                    _epsDatabase.AddInParameter(dbCommand, "searchTerm", DbType.String, !string.IsNullOrEmpty(searchTerm) ? searchTerm.Trim() : string.Empty);
                    _epsDatabase.AddInParameter(dbCommand, "RoleId", DbType.Int16, roleId);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            GetPromotionDetails objpro = new GetPromotionDetails();
                            CommonClasses.CommonFunctions.MapFetchData(objpro, rdr);
                            listOfProDetails.Add(objpro);
                        }
                    }
                }
            });
            return listOfProDetails;
        }





        public async Task<string> DeletePromotionDetails(int id)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeletePromotionDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "EmpTransDetId", DbType.Int32, id);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.SomethingWrongMessage;
                    // msg = temp > 0 ? "Record deleted successfully" : "Something went wrong";
                }
            });
            return msg;
        }


        public async Task<string> ForwardTransferDetailToChecker(ForwardTransferDetails obj)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_ForwardTransferDetailsToChecker))
                {
                    _epsDatabase.AddInParameter(dbCommand, "MsEmpTransDet", DbType.Int32, obj.MsEmpTransDet);
                    _epsDatabase.AddInParameter(dbCommand, "VerifyFlag", DbType.String, obj.VerifyFlag);
                    _epsDatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, obj.PermDdoId);
                    _epsDatabase.AddInParameter(dbCommand, "DDOId", DbType.String, obj.DDOId);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.UpdateSuccessMessage: EPSResource.SomethingWrongMessage;
                    // msg = temp > 0 ? "Record updated successfully" : "Something went wrong";
                }
            });
            return msg;
        }

        public async Task<string> UpdateStatus(ForwardTransferDetails obj)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_ForwardTransferDetailsToChecker))
                {
                    _epsDatabase.AddInParameter(dbCommand, "MsEmpTransDet", DbType.Int32, obj.MsEmpTransDet);
                    _epsDatabase.AddInParameter(dbCommand, "VerifyFlag", DbType.String, obj.VerifyFlag);
                    _epsDatabase.AddInParameter(dbCommand, "PermDdoId", DbType.String, obj.PermDdoId);
                    _epsDatabase.AddInParameter(dbCommand, "RejectionRemark", DbType.String, obj.RejectionRemark);
                    _epsDatabase.AddInParameter(dbCommand, "DDOId", DbType.String, obj.DDOId);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.UpdateSuccessMessage : EPSResource.SomethingWrongMessage;
                    // msg = temp > 0 ? "Record updated successfully" : "Something went wrong";
                }
            });
            return msg;
        }

        #endregion


        #region ReversionWithoutTransfer

        public async Task<string> UpdateReversionDetailsWithoutTransfer(ReversionDetails objRev)
        {
            string msg = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpsertReversionDetailsWithoutTransfer))
                {
                    objRev.TrnGround = 2;
                    msg = CommonClasses.CommonFunctions.MapUpdateParameters(objRev, _epsDatabase, dbCommand);
                }
            });
            return msg;
        }


        public async Task<IEnumerable<GetReversionDetails>> GetReversionDetailsWithoutTransfer(string empCode, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
            List<GetReversionDetails> listOfRevDetails = new List<GetReversionDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetReversionDetailsWithoutTransfer))
                {
                    _epsDatabase.AddInParameter(dbCommand, "Empcd", DbType.String, empCode);
                    _epsDatabase.AddInParameter(dbCommand, "pageNumber", DbType.Int16, pageNumber);
                    _epsDatabase.AddInParameter(dbCommand, "pageSize", DbType.Int16, pageSize);
                    _epsDatabase.AddInParameter(dbCommand, "searchTerm", DbType.String, !string.IsNullOrEmpty(searchTerm) ? searchTerm.Trim() : string.Empty);
                    _epsDatabase.AddInParameter(dbCommand, "RoleId", DbType.Int16, roleId);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            GetReversionDetails objrev = new GetReversionDetails();
                            CommonClasses.CommonFunctions.MapFetchData(objrev, rdr);
                            listOfRevDetails.Add(objrev);
                        }
                    }
                }
            });
            return listOfRevDetails;
        }


        public async Task<string> DeleteReversionDetails(int id)
        {
            string msg = string.Empty;
            await Task.Run(() => {
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteReversionDetails))
                {
                    _epsDatabase.AddInParameter(dbCommand, "EmpTransDetId", DbType.Int32, id);
                    int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                    msg = temp > 0 ? EPSResource.DeleteSuccessMessage : EPSResource.SomethingWrongMessage;
                    //   msg = temp > 0 ? "Record deleted successfully" : "Something went wrong";
                }
            });
            return msg;
        }

        #endregion
    }
}
