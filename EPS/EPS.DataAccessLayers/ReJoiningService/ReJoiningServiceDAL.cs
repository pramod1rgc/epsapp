﻿using EPS.BusinessModels.ReJoiningService;
using EPS.CommonClasses;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace EPS.DataAccessLayers.ReJoiningService
{
    public class ReJoiningServiceDAL
    {
        Database _epsDatabase;

        public ReJoiningServiceDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        public IEnumerable<GetServiceEndEmployees> GetServiceEndEmployeesDetails(string empCode, string empName, string empPanNo)
        {
            List<GetServiceEndEmployees> listOfEndServiceDetails = new List<GetServiceEndEmployees>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_SearchEndOfServiceEmployees))
            {
                _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, empCode);
                _epsDatabase.AddInParameter(dbCommand, "EmpName", DbType.String, empName);
                _epsDatabase.AddInParameter(dbCommand, "PanNo", DbType.String, empPanNo);
                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        GetServiceEndEmployees objEnd = new GetServiceEndEmployees();
                        CommonClasses.CommonFunctions.MapFetchData(objEnd, rdr);
                        listOfEndServiceDetails.Add(objEnd);
                    }
                }
            }
            return listOfEndServiceDetails;
        }


        public IEnumerable<GetReJoiningServiceEmployees> GetReJoiningServiceEmployeesDetails(int pageNumber, int pageSize, string SearchTerm, string empCd, int roleId)
        {
            List<GetReJoiningServiceEmployees> listOfRejoiningServiceDetails = new List<GetReJoiningServiceEmployees>();
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_GetRejoiningOfEmployeesList))
            {
                _epsDatabase.AddInParameter(dbCommand, "pageNumber", DbType.Int32, pageNumber);
                _epsDatabase.AddInParameter(dbCommand, "pageSize", DbType.Int32, pageSize);
                _epsDatabase.AddInParameter(dbCommand, "SearchTerm", DbType.String, !string.IsNullOrEmpty(SearchTerm) ? SearchTerm.Trim() : string.Empty);
                _epsDatabase.AddInParameter(dbCommand, "empCd", DbType.String, empCd);
                _epsDatabase.AddInParameter(dbCommand, "RoleId", DbType.Int32, roleId);
                using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        GetReJoiningServiceEmployees objRej = new GetReJoiningServiceEmployees();
                        CommonClasses.CommonFunctions.MapFetchData(objRej, rdr);
                        listOfRejoiningServiceDetails.Add(objRej);
                    }
                }
            }
            return listOfRejoiningServiceDetails;
        }

        public string UpsertReJoiningServiceDetails(UpsertReJoiningServiceDetails objRej)
        {
            try
            {
                string msg = string.Empty;
                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpsertRejoiningEmployeeData))
                {
                   // objRej.IpAddress = CommonClasses.CommonFunctions.GetIP();
                    msg = CommonClasses.CommonFunctions.MapUpdateParameters(objRej, _epsDatabase, dbCommand);
                }

                return msg;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string DeleteReJoiningServiceDetails(DeleteRejoiningService obj)
        {
            string msg = string.Empty;
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_DeleteRejoiningOfEmployee))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpReinstateDetID", DbType.Int32, obj.MsEmpReinstateDetID);
                _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, obj.EmpCd);
                _epsDatabase.AddInParameter(dbCommand, "ToDDOId", DbType.String, obj.ToDDOId);
                _epsDatabase.AddInParameter(dbCommand, "IpAddress", DbType.String, obj.IpAddress);
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record deleted successfully" : "Something went wrong";
            }
            return msg;
        }


        public string UpdateReJoiningServiceStatus(UpdateReJoiningServiceStatus obj)
        {
            string msg = string.Empty;
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.SP_UpdateRejoiningEmployeeStatus))
            {
                _epsDatabase.AddInParameter(dbCommand, "MsEmpReinstateDetID", DbType.Int32, obj.MsEmpReinstateDetID);
                _epsDatabase.AddInParameter(dbCommand, "EmpCd", DbType.String, obj.EmpCd);
                _epsDatabase.AddInParameter(dbCommand, "ToDDOId", DbType.String, obj.ToDDOId);
                _epsDatabase.AddInParameter(dbCommand, "IpAddress", DbType.String, obj.IpAddress);
                _epsDatabase.AddInParameter(dbCommand, "Status", DbType.String, obj.Status);
                _epsDatabase.AddInParameter(dbCommand, "Reason", DbType.String, obj.Reason);
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record updated successfully" : "Something went wrong";
            }
            return msg;
        }
    }
}
