﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class GPFInterestRateDAL
    {
        Database epsdatabase = null;
        Database database = null;
        //DbCommand dbCommand = null;
        public GPFInterestRateDAL()
        {
        }
        public GPFInterestRateDAL(Database database)
        {
            epsdatabase = database;
        }
        #region Get DA Details
        /// <summary>
        /// Bind all record in Grid=====================
        /// </summary>
        /// <returns></returns>
        public async Task<List<GPFInterestRateBM>> GetGPFInterestRateDetails()
        {
            List<GPFInterestRateBM> list = new List<GPFInterestRateBM>();
            database = epsdatabase;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = database.GetStoredProcCommand(EPSConstant.Usp_GetGpfIRateMasterDetails))
                {

                    using (IDataReader objReader = database.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GPFInterestRateBM _objgpf = new GPFInterestRateBM();
                            _objgpf.GPFInterestID = Convert.ToInt32(objReader["GPFInterestID"]);
                            _objgpf.WefMonthYear = Convert.ToString(objReader["WefMonthYear"]);
                            _objgpf.ToMonthYear = Convert.ToString(objReader["ToMonthYear"]);
                            _objgpf.NewGPFInterestRate = Convert.ToDecimal(objReader["NewGPFInterestRate"]);
                            _objgpf.GPFRuleReferenceNumber = objReader["GPFRuleReferenceNumber"].ToString();
                            list.Add(_objgpf);
                        }
                    }
                }
            });
            if (list == null)
                return null;
            else
                return list;
        }


        #endregion



        #region Insert and Update
        /// <summary>
        /// Insert and Update Da State And Central
        /// </summary>
        /// <param name="_objda"></param>
        /// <returns></returns>
        public async Task<string> InsertandUpdate(GPFInterestRateBM objgpf)
        {
       
            string finaloutput = string.Empty;
            string result = string.Empty;
            int i = 0;
            database = epsdatabase;
            //string getResultAlreadyExists = null ;
            //await Task.Run(() =>
            //{
            //    using (DbCommand dbCommand = database.GetStoredProcCommand(EPSConstant.Usp_getAlreadyExixtsRateMasterDetails))
            //    {
            //        database.AddInParameter(dbCommand, "GPFInterestRateID", DbType.Int32, objgpf.GPFInterestID);
            //        database.AddInParameter(dbCommand, "IntFromDt", DbType.String, objgpf.WefMonthYear);
            //        database.AddInParameter(dbCommand, "IntUptoDt", DbType.String, objgpf.ToMonthYear);
            //        using (IDataReader objReader = database.ExecuteReader(dbCommand))
            //        {
            //            while (objReader.Read())
            //            {

            //                getResultAlreadyExists = objReader["Result"] != DBNull.Value ? Convert.ToString(objReader["Result"]).Trim() : null;
            //            }
            //        }
            //    }
            //});

            //if(getResultAlreadyExists == "False")
            //{
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = database.GetStoredProcCommand(EPSConstant.usp_InsertUpdateGpfIRateMaster))
                    {
                        database.AddInParameter(dbCommand, "GPFInterestRateID", DbType.Int32, objgpf.GPFInterestID);
                        database.AddInParameter(dbCommand, "IntRate", DbType.String, objgpf.NewGPFInterestRate);
                        database.AddInParameter(dbCommand, "IntFromDt", DbType.DateTime, objgpf.WefMonthYear);
                        database.AddInParameter(dbCommand, "IntUptoDt", DbType.DateTime, objgpf.ToMonthYear);
                        database.AddInParameter(dbCommand, "GPFRuleReferenceNumber", DbType.String, objgpf.GPFRuleReferenceNumber.Trim());
                        database.AddInParameter(dbCommand, "@IpAddress", DbType.String, objgpf.ipAddress);
                        database.AddOutParameter(dbCommand, "@finaloutput", DbType.String, 50);
                        i = database.ExecuteNonQuery(dbCommand);
                        finaloutput = database.GetParameterValue(dbCommand, "finaloutput").ToString();
                        if (objgpf.GPFInterestID > 0)
                        {
                            if (i > 0)
                            {
                                result = EPSResource.UpdateSuccessMessage;
                            }
                            else if (finaloutput.Length > 0)
                            {
                                result = finaloutput;
                            }
                            else
                            {
                                result = EPSResource.UpdateFailedMessage;
                            }
                        }
                        else if (objgpf.GPFInterestID == 0)
                        {
                            if (i > 0)
                            {
                                result = EPSResource.SaveSuccessMessage;
                            }
                            else if (finaloutput.Length > 0)
                            {
                                result = finaloutput;
                            }
                            else
                            {
                                result = EPSResource.SaveFailedMessage;
                            }
                        }

                    }
                });
            

           //else
           // {
           //     return getResultAlreadyExists = "Wef month & year already exist";
           // }
            return result;
        }

        #endregion

        #region Delete
        /// <summary>
        /// soft delete da master
        /// </summary>
        /// <param name="MsDaMasterID"></param>
        /// <returns></returns>
        public async Task<string> Delete(int GPFInterestID)
        {
            string result = string.Empty;
            database = epsdatabase;
            int i = 0;
            await Task.Run(() =>
                {
                    using (DbCommand dbCommand = database.GetStoredProcCommand(EPSConstant.usp_DeleteGpfInterestrRate))
                    {
                        database.AddInParameter(dbCommand, "@GPFInterestID", DbType.Int32, GPFInterestID);
                    i = database.ExecuteNonQuery(dbCommand);
                        if (i > 0)
                        {
                            result = EPSResource.DeleteSuccessMessage; 
                        }
                        else { result = EPSResource.DeleteFailedMessage; }
                    }
                });
            return result;
        }

        #endregion


    }
}
