﻿using EPS.BusinessModels.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using EPS.CommonClasses;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace EPS.DataAccessLayers.Masters
{
   public class TPTA_DAL
    {
        private Database epsdatabase = null;
        private Database database = null;
        private DbCommand dbCommand = null;
        
        public TPTA_DAL(Database database)
        {
            epsdatabase = database;
        }


        #region Insert And Update Tpta Master Record
        /// <summary>
        ///  Insert And Update Tpta Master Record
        /// </summary>
        /// <param name="objHraModel"></param>
        /// <returns></returns>
        public async Task<string> CreateTptaMaster(HRAModel objHraModel)
        {
            DataTable dt = new DataTable();
            dt.TableName = "tblDetailsType";

            DataColumn dc = new DataColumn("Id", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("SlabNo", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("City", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("SlabType", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("LowerLimit", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("UpperLimit", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Minvalue", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("TptaAmount", typeof(string));
            dt.Columns.Add(dc);

            for (int i = 0; i < objHraModel.RateDetails.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i + 1;
                dr[1] = objHraModel.RateDetails[i].slabNo;
                dr[2] = objHraModel.RateDetails[i].city;
                dr[3] = objHraModel.RateDetails[i].slabType;
                dr[4] = objHraModel.RateDetails[i].lowerLimit;
                dr[5] = objHraModel.RateDetails[i].upperLimit;
                dr[6] = objHraModel.RateDetails[i].minvalue;
                dr[7] = objHraModel.RateDetails[i].tptaAmount;
                dt.Rows.InsertAt(dr, i);
            }
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_InsertUpdateTptaMaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "ID", DbType.Int32, objHraModel.TptaMasterID);
                    epsdatabase.AddInParameter(dbCommand, "PAYCOMM", DbType.Int32, objHraModel.payCommissionCode);
                    epsdatabase.AddInParameter(dbCommand, "State", DbType.Int32, objHraModel.stateId); 
                    epsdatabase.AddInParameter(dbCommand, "EmployeeType", DbType.Int32, objHraModel.empTypeID);
                    epsdatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, objHraModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objHraModel.loginUser);
                    SqlParameter para = new SqlParameter("TptaMaster", dt);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);

                    result = Convert.ToInt32(epsdatabase.ExecuteScalar(dbCommand));
                }
            });
            if (result > 0 && objHraModel.TptaMasterID > 0) 
            {
                return EPSResource.UpdateSuccessMessage;
            }
            else if (result == 0 )
            {
                return EPSResource.SaveSuccessMessage;
            }
            else
            { return EPSResource.AlreadyExistMessage; }
            
        }

        #endregion

        #region Get Tpta Master Data
        /// <summary>
        ///  Get Tpta Master Data
        /// </summary>
        public async Task<List<HRAModel>> GetTptaMasterDetails()
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_GetTptaMasterDetails)) 
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            HRAModel hramodel = new HRAModel();
                           
                            hramodel.payCommissionCode = objReader["PayCommisionCode"].ToString();
                            hramodel.StateCode = objReader["StateCode"].ToString();
                            hramodel.CityClass = objReader["CityClass"].ToString();
                            hramodel.TptaMasterID = Convert.ToInt32(objReader["TptaMasterID"]);                            
                            hramodel.slabtypDesc = objReader["SlabType"].ToString();
                            hramodel.PayCommisionName = objReader["PayCommisionName"].ToString();
                            hramodel.StateName = objReader["StateName"].ToString();                            
                            hramodel.EmployeeType = objReader["EmployeeType"].ToString();
                            hramodel.empTypeID = objReader["EmpTypeID"].ToString();
                            hramodel.stateId = Convert.ToInt32(objReader["StateCode"].ToString());

                            hramodel.UpperLimit = objReader["UpperLimit"].ToString();
                            hramodel.LowerLimit = objReader["LowerLimit"].ToString();
                            hramodel.TptaAmount = objReader["TptaAmount"].ToString();
                            hramodel.SlabNo = objReader["SlabNo"].ToString();
                            hramodel.Minvalue = objReader["MinValue"].ToString();

                            HRARateModel rateDetails = new HRARateModel();
                            rateDetails.slabNo = objReader["SlabNo"].ToString();
                            rateDetails.slabType = Convert.ToInt32(objReader["MsSlabTypeID"]);
                            rateDetails.tptaAmount = objReader["TptaAmount"].ToString();
                            rateDetails.tptaMasterID = Convert.ToInt32(objReader["TptaMasterID"]);
                            rateDetails.upperLimit = objReader["UpperLimit"].ToString();
                            rateDetails.lowerLimit = objReader["LowerLimit"].ToString();
                            rateDetails.city = objReader["MsCityclassID"].ToString();
                            rateDetails.minvalue = objReader["MinValue"].ToString();
                            rateDetails.StateId = Convert.ToInt32(objReader["StateCode"]);
                            hramodel.RateDetails.Add(rateDetails);

                            list.Add(hramodel);
                        }
                                               
                    }
                }
            });
            return list;
        }
        #endregion


        #region Edit TPTA Master Details
        /// <summary>
        /// Edit Tpta Master Details 
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> EditTptaMasterDetails(int MasterID)
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_EditTptaMasterDetails);
                epsdatabase.AddInParameter(dbCommand, "@TptaMasterID", DbType.Int32, MasterID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        HRAModel hramodel = new HRAModel();
                        hramodel.payCommissionCode = objReader["PayCommisionCode"].ToString();
                        hramodel.StateCode = objReader["StateCode"].ToString();
                        hramodel.CityClass = objReader["CityClass"].ToString();

                        //hramodel.slabType = Convert.ToInt32(objReader["MsSlabTypeID"]);
                        //hramodel.city = objReader["MsCityclassID"].ToString();
                        //hramodel.TptaMasterID = Convert.ToInt32(objReader["TptaMasterID"]);
                        //hramodel.slabNo = objReader["SlabNo"].ToString();
                        //hramodel.lowerLimit = objReader["LowerLimit"].ToString();
                        //hramodel.upperLimit = objReader["UpperLimit"].ToString();
                        //hramodel.minvalue = objReader["MinValue"].ToString();
                        //hramodel.TptaAmount = objReader["TptaAmount"].ToString();

                        hramodel.slabtypDesc = objReader["SlabType"].ToString();
                        hramodel.PayCommisionName = objReader["PayCommisionName"].ToString();
                        hramodel.StateName = objReader["StateName"].ToString();

                        hramodel.EmployeeType = objReader["EmployeeType"].ToString();
                        hramodel.empTypeID = objReader["EmpTypeID"].ToString();
                        hramodel.stateId = Convert.ToInt32(objReader["StateCode"].ToString());
                        list.Add(hramodel);
                    }
                }
            });
            return list;
        }
        #endregion     

        #region Delete TPTA Master Data
        /// <summary>
        /// Delete TPTA Master Data
        /// </summary>
        /// <param name="TptaMasterID"></param>
        /// <returns></returns>
        public async Task<string> DeleteTptaMaster(int TptaMasterID)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_DeleteTptaMaster); 
                epsdatabase.AddInParameter(dbCommand, "@TptaMasterID", DbType.Int32, TptaMasterID);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage;
                }
                else { Response = EPSResource.DeleteFailedMessage; }
            });
            return Response;
        }
            
        #endregion
    }
}
