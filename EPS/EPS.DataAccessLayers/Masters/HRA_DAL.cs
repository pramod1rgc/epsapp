﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class HRA_DAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;
        public HRA_DAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get PayScale Code
        public async Task<List<HRAModel>> BindPayScaleCode()
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_BindPayScaleCode))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            HRAModel hraModel = new HRAModel();
                            hraModel.PayScaleCode = objReader["PayScaleCode"].ToString();
                            hraModel.payScaleID = objReader["PayScaleID"].ToString();
                            list.Add(hraModel);
                        }
                    }
                }
            });
            return list;
        }
        public async Task<List<HRAModel>> BindPayLevel()
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Master_GetPayScaleLevel))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            HRAModel hraModel = new HRAModel();
                            hraModel.PayScaleCode = objReader["PayScaleCode"].ToString();
                            hraModel.payScaleID = objReader["PayScaleID"].ToString();
                            hraModel.payscaleLevel = objReader["PayScalePayLevel"].ToString();
                            list.Add(hraModel);
                        }
                    }
                }
            });
            return list;
        }
        #endregion

        #region Get Pay Commission By Employee Type
        /// <summary>
        /// Get Pay Commission By Employee Type
        /// </summary>
        /// <param name="employeeType"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> GetPayCommissionByEmployeeType(string employeeType)
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_GetPayCommissionByEmployeeType);
                epsdatabase.AddInParameter(dbCommand, "@EmployeeType", DbType.String, employeeType);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        HRAModel hraModel = new HRAModel();
                        hraModel.PayCommisionName = objReader["PayCommisionName"].ToString();
                        hraModel.payCommissionCode = objReader["payCommissionCode"].ToString();
                        list.Add(hraModel);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Get City Details By StateCode
        /// <summary>
        /// Get City Details
        /// </summary>
        /// <param name="StateCode"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> GetCityDetails(int StateID)
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_GetGetCityDetailsByStateCode);
                epsdatabase.AddInParameter(dbCommand, "@StateID", DbType.String, StateID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        HRAModel hraModel = new HRAModel();
                        hraModel.City = objReader["City"].ToString();
                        hraModel.CityCode = objReader["CityCode"].ToString();
                        list.Add(hraModel);
                    }
                }
            });
            return list;
        }
        #endregion
  
        /// <summary>
        /// Get City Class on behalf of paycommission
        /// </summary>
        /// <param name="payCommId"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> GetCityClass(int payCommId)
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_GetGetCityClassByPayComm);
                epsdatabase.AddInParameter(dbCommand, "@PaycommID", DbType.String, payCommId);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        HRAModel hraModel = new HRAModel();
                        hraModel.City = objReader["MsCityclassID"].ToString();
                        hraModel.CityClass = objReader["CclCityclass"].ToString();
                        list.Add(hraModel);
                    }
                }
            });
            return list;
        }



        #region Insert And Update Hra Master Record
        /// <summary>
        ///  Insert And Update Hra Master Record
        /// </summary>
        /// <param name="objHraModel"></param>
        /// <returns></returns>
        /// 

        public async Task<string> CreateHraMaster(HRAModel objHraModel)
        {
            DataTable dt = new DataTable();
            dt.TableName = "tblDetailsType";

            DataColumn dc = new DataColumn("Id", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("SlabNo", typeof(int));
            dt.Columns.Add(dc);
            dc = new DataColumn("City", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("SlabType", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("LowerLimit", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("UpperLimit", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Minvalue", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("TptaAmount", typeof(string));
            dt.Columns.Add(dc);

            for (int i = 0; i < objHraModel.RateDetails.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i + 1;
                dr[1] = objHraModel.RateDetails[i].slabNo;
                dr[2] = objHraModel.RateDetails[i].city;
                dr[3] = objHraModel.RateDetails[i].slabType;
                dr[4] = objHraModel.RateDetails[i].lowerLimit;
                dr[5] = objHraModel.RateDetails[i].upperLimit;
                dr[6] = objHraModel.RateDetails[i].minvalue;
                dr[7] = objHraModel.RateDetails[i].tptaAmount;
                dt.Rows.InsertAt(dr, i);
            }
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_InsertUpdateHraMaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "ID", DbType.Int32, objHraModel.HraMasterID);
                    epsdatabase.AddInParameter(dbCommand, "PAYCOMM", DbType.Int32, objHraModel.payCommissionCode);
                    epsdatabase.AddInParameter(dbCommand, "State", DbType.Int32, objHraModel.stateId);
                    epsdatabase.AddInParameter(dbCommand, "EmployeeType", DbType.Int32, objHraModel.empTypeID);
                    epsdatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, objHraModel.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, objHraModel.loginUser);
                    SqlParameter para = new SqlParameter("TptaMaster", dt);
                    para.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(para);

                    result = Convert.ToInt32(epsdatabase.ExecuteScalar(dbCommand));
                }
            });
            if (result > 0 && objHraModel.TptaMasterID > 0)
            {
                return EPSResource.UpdateSuccessMessage;
            }
            else if (result == 0)
            {
                return EPSResource.SaveSuccessMessage;
            }
            else
            { return EPSResource.AlreadyExistMessage; }

        }

        #endregion

        #region Get Master Data
        /// <summary>
        ///  Get Master Data
        /// </summary>


        public async Task<List<HRAModel>> GetHraMasterDetails()
        {
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_GetHraMasterDetails))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            HRAModel hramodel = new HRAModel();

                            hramodel.payCommissionCode = objReader["PayCommisionCode"].ToString();
                            hramodel.StateCode = objReader["StateCode"].ToString();
                            hramodel.CityClass = objReader["CityClass"].ToString();
                            hramodel.HraMasterID = Convert.ToInt32(objReader["HraMasterID"]);
                            hramodel.slabtypDesc = objReader["SlabType"].ToString();
                            hramodel.PayCommisionName = objReader["PayCommisionName"].ToString();
                            hramodel.StateName = objReader["StateName"].ToString();
                            hramodel.EmployeeType = objReader["EmployeeType"].ToString();
                            hramodel.empTypeID = objReader["EmpTypeID"].ToString();
                            hramodel.stateId = Convert.ToInt32(objReader["StateCode"].ToString());

                            hramodel.UpperLimit = objReader["UpperLimit"].ToString();
                            hramodel.LowerLimit = objReader["LowerLimit"].ToString();
                            hramodel.TptaAmount = objReader["HraAmount"].ToString();
                            hramodel.SlabNo = objReader["SlabNo"].ToString();
                            hramodel.Minvalue = objReader["MinValue"].ToString();

                            HRARateModel rateDetails = new HRARateModel();
                            rateDetails.slabNo = objReader["SlabNo"].ToString();
                            rateDetails.slabType = Convert.ToInt32(objReader["MsSlabTypeID"]);
                            rateDetails.tptaAmount = objReader["HraAmount"].ToString();
                            rateDetails.tptaMasterID = Convert.ToInt32(objReader["HraMasterID"]);
                            rateDetails.upperLimit = objReader["UpperLimit"].ToString();
                            rateDetails.lowerLimit = objReader["LowerLimit"].ToString();
                            rateDetails.city = objReader["MsCityclassID"].ToString();
                            rateDetails.minvalue = objReader["MinValue"].ToString();
                            rateDetails.StateId = Convert.ToInt32(objReader["StateCode"]);
                            hramodel.RateDetails.Add(rateDetails);

                            list.Add(hramodel);
                        }

                    }
                }
            });
            return list;
        }

       
        #endregion

        #region Edit HRA Master Details
        /// <summary>
        /// Edit Hra Master Details
        /// </summary>
        /// <param name="hraMasterID"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> EditHraMasterDetails(int hraMasterID)
        {
            HRAModel hramodel = new HRAModel();
            List<HRAModel> list = new List<HRAModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_EditHraMasterDetails);
                epsdatabase.AddInParameter(dbCommand, "@HraMasterID", DbType.Int32, hraMasterID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        hramodel.HraMasterID = Convert.ToInt32(objReader["HraMasterID"]);
                        hramodel.payCommissionCode = objReader["PayCommisionCode"].ToString();
                        hramodel.PayCommID = Convert.ToInt32(objReader["PayCommisionCode"]);
                        hramodel.StateCode = objReader["StateCode"].ToString();
                        hramodel.CityCode = objReader["CityCode"].ToString();
                        hramodel.CityName = objReader["CityName"].ToString();
                        hramodel.CityName = objReader["CityName"].ToString();
                        hramodel.ClassName = objReader["ClassName"].ToString();
                        hramodel.PayCommisionName = objReader["PayCommisionName"].ToString();
                        hramodel.StateName = objReader["StateName"].ToString();
                        hramodel.HraAmount = objReader["HraAmount"].ToString();
                        hramodel.EmployeeType = objReader["EmployeeType"].ToString();
                        hramodel.empTypeID = objReader["EmpTypeID"].ToString();
                        hramodel.stateId = Convert.ToInt32(objReader["StateCode"].ToString());
                        hramodel.payscaleLevel = objReader["PayScalePayLevel"].ToString();                        
                        hramodel.slabtypDesc = objReader["SlabType"].ToString();
                        list.Add(hramodel);
                    }
                }
            });
            return list;
        }
        #endregion     

        #region Delete Hra Master Data
        /// <summary>
        /// Delete Hra Master Data
        /// </summary>
        /// <param name="HraMasterID"></param>
        /// <returns></returns>
        public async Task<string> DeleteHraMaster(int hraMasterID)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_DeleteHraMaster);
                epsdatabase.AddInParameter(dbCommand, "@HraMasterID", DbType.Int32, hraMasterID);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage;
                }
                else
                {
                    Response = EPSResource.DeleteFailedMessage;
                }
            });
            return Response;
        }
        #endregion

    }
}
