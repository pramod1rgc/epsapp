﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class PsuMasterDAL
    {
        Database _epsDatabase;
        public PsuMasterDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            _epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<PsuMasterModel.City>> GetDisttList(int stateId)
        {
            List<PsuMasterModel.City> cityList = new List<PsuMasterModel.City>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.usp_psuGetCityList))
                {
                    _epsDatabase.AddInParameter(dbCommand, "stateId", DbType.Int32, stateId);
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PsuMasterModel.City objCity = new PsuMasterModel.City
                            {
                                CityId = Convert.ToInt32(rdr["CityId"]),
                                CityName = rdr["CName"].ToString(),
                                StateId = Convert.ToInt32(rdr["CityStateID"])
                            };
                            cityList.Add(objCity);
                        }
                    }
                }
            });

            return cityList;
        }

        public string DeletePsuMasterDetails(int id)
        {
            string msg = string.Empty;
            using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.usp_psuMasterDeleteDetails))
            {
                _epsDatabase.AddInParameter(dbCommand, "Id", DbType.Int32, id);             
                int temp = _epsDatabase.ExecuteNonQuery(dbCommand);
                msg = temp > 0 ? "Record deleted successfully" : "Something went wrong";
            }
            return msg;
        }

        public async Task<IEnumerable<PsuMasterModel.PsuMaster>> GetPsuMasterDetails()
        {
            List<PsuMasterModel.PsuMaster> psuList = new List<PsuMasterModel.PsuMaster>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.usp_psuMasterGetDetails))
                {
                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PsuMasterModel.PsuMaster psuObj = new PsuMasterModel.PsuMaster
                            {

                                PsuId = Convert.ToInt32(rdr["PsuId"]),
                                PsuCode = rdr["PsuCode"].ToString(),
                                PDesc = rdr["PDesc"].ToString(),
                                PDescBilingual = rdr["PDescBilingual"].ToString(),
                                PAddress = rdr["PAddress"].ToString(),
                                PPinCode = Convert.ToInt32(rdr["PPinCode"]),
                                PPhoneNo = rdr["PPhoneNo"].ToString(),
                                PFaxNo = rdr["PFaxNo"].ToString(),
                                PEmail = rdr["PEmail"].ToString(),
                                PStateName = Convert.ToInt32(rdr["PStateName"]),
                                PCity = Convert.ToInt32(rdr["PCity"]),
                                PIsBankDetails = Convert.ToInt32(rdr["PIsBankDetails"]),
                                PChequeInFavour = rdr["PChequeInFavour"].ToString(),
                                PIfscCode = rdr["PIfscCode"].ToString(),
                                PBankName = rdr["PBankName"].ToString(),
                                PBranchName = rdr["PBranchName"].ToString(),
                                PBankAccNum = rdr["PBankAccNum"].ToString(),
                                PConfirmAccNum = rdr["PConfirmAccNum"].ToString(),
                                IPAddress = rdr["IPAddress"].ToString(),
                                StateName = rdr["StateName"].ToString(),
                                CityName = rdr["CityName"].ToString()
                            };
                            psuList.Add(psuObj);
                        }
                    }
                }
            });

            return psuList;
        }

        public async Task<string> SavedPSUDetails(PsuMasterModel.PsuMaster objPSU)
        {
            try
            {
                int msg = await Task.Run(() =>
                {

                    using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.usp_psuMasterSaveDetails))
                    {
                        _epsDatabase.AddInParameter(dbCommand, "PsuId", DbType.Int32, objPSU.PsuId);
                        _epsDatabase.AddInParameter(dbCommand, "PsuCode", DbType.String, objPSU.PsuCode);
                        _epsDatabase.AddInParameter(dbCommand, "PDesc", DbType.String, objPSU.PDesc);
                        _epsDatabase.AddInParameter(dbCommand, "PDesc_Bilingual", DbType.String, objPSU.PDescBilingual);
                        _epsDatabase.AddInParameter(dbCommand, "PAddress", DbType.String, objPSU.PAddress);
                        _epsDatabase.AddInParameter(dbCommand, "PPinCode", DbType.String, objPSU.PPinCode);
                        _epsDatabase.AddInParameter(dbCommand, "PPhoneNo", DbType.String, objPSU.PPhoneNo);
                        _epsDatabase.AddInParameter(dbCommand, "PFaxNo", DbType.String, objPSU.PFaxNo);
                        _epsDatabase.AddInParameter(dbCommand, "PEmail", DbType.String, objPSU.PEmail);
                        _epsDatabase.AddInParameter(dbCommand, "PStateName", DbType.String, objPSU.PStateName);
                        _epsDatabase.AddInParameter(dbCommand, "PCity", DbType.String, objPSU.PCity);
                        _epsDatabase.AddInParameter(dbCommand, "PIsBankDetails", DbType.Boolean, Convert.ToBoolean(objPSU.PIsBankDetails));
                        _epsDatabase.AddInParameter(dbCommand, "PIfscCode", DbType.String, objPSU.PIfscCode);
                        _epsDatabase.AddInParameter(dbCommand, "PBankName", DbType.String, objPSU.PBankName);
                        _epsDatabase.AddInParameter(dbCommand, "PBranchName", DbType.String, objPSU.PBranchName);
                        _epsDatabase.AddInParameter(dbCommand, "PBankAccNum", DbType.String, objPSU.PBankAccNum);
                        _epsDatabase.AddInParameter(dbCommand, "PConfirmAccNum", DbType.String, objPSU.PConfirmAccNum);
                        _epsDatabase.AddInParameter(dbCommand, "PChequeInFavour", DbType.String, objPSU.PChequeInFavour);
                        _epsDatabase.AddInParameter(dbCommand, "IPAddress", DbType.String, objPSU.IPAddress);
                        _epsDatabase.AddInParameter(dbCommand, "CreatedBy", DbType.String, "HOOMaker");
                        // _epsDatabase.AddInParameter(dbCommand, "IsTillOrder", DbType.Boolean, objPSU.IsTillOrder);                     

                        return _epsDatabase.ExecuteNonQuery(dbCommand);
                    }
                });


                return msg.ToString();
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public async Task<IEnumerable<PsuMasterModel.State>> GetStateList()
        {
            List<PsuMasterModel.State> stateList = new List<PsuMasterModel.State>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = _epsDatabase.GetStoredProcCommand(EPSConstant.usp_psuGetStateList))
                {

                    using (IDataReader rdr = _epsDatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PsuMasterModel.State objState = new PsuMasterModel.State
                            {
                                StateId = Convert.ToInt32(rdr["StateId"]),
                                StateName = rdr["SName"].ToString()
                            };
                            stateList.Add(objState);
                        }
                    }
                }
            });

            return stateList;
        }
    }
}
