﻿using System;
using System.Collections.Generic;
using System.Text;
using EPS.CommonClasses;
using EPS.BusinessModels.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Threading.Tasks;
using System.Globalization;

namespace EPS.DataAccessLayers.Masters
{
    public class MsdesignDAL
    {
        Database epsdatabase =null;
       
        public MsdesignDAL( Database database  )
        {

            epsdatabase = database;
        }

        #region Get Common Designation 
        public async Task<List<MsdesignBM>> GetCommondesign()
        {
            List<MsdesignBM> MsdesignList = null;

            MsdesignList = new List<MsdesignBM>();
            MsdesignBM MscommondesginDatas;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsCommonDesign))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            MscommondesginDatas = new MsdesignBM();
                            MscommondesginDatas.CommonDesigGroupCD = objReader["CommonDesigGroupCD"] != DBNull.Value ? Convert.ToInt32(objReader["CommonDesigGroupCD"], CultureInfo.InvariantCulture) : 0;

                            MscommondesginDatas.CommonDesigSrNo = objReader["CommonDesigSrNo"] != DBNull.Value ? Convert.ToInt32(objReader["CommonDesigSrNo"], CultureInfo.InvariantCulture) : 0;

                            MscommondesginDatas.CommonDesigSuperannAge = objReader["CommonDesigSuperannAge"] != DBNull.Value ? Convert.ToInt32(objReader["CommonDesigSuperannAge"], CultureInfo.InvariantCulture) : 0;

                            MscommondesginDatas.CommonDesigDesc = objReader["CommonDesigDesc"] != DBNull.Value ? Convert.ToString(objReader["CommonDesigDesc"], CultureInfo.InvariantCulture).Trim() : null;
                            MscommondesginDatas.GroupCDDesc = objReader["GroupCDDesc"] != DBNull.Value ? Convert.ToString(objReader["GroupCDDesc"], CultureInfo.InvariantCulture).Trim() : null;
                            MscommondesginDatas.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"], CultureInfo.InvariantCulture) : true;

                            MsdesignList.Add(MscommondesginDatas);
                        }
                    }
                }
            }).ConfigureAwait(true);

            return MsdesignList;

        }
        #endregion
        #region Get Common Maxid  
        public int GetMsdesignMaxId()
        {
            int MaxId = 0;

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsCommonDesign))
            {
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "selectMaxId");

                MaxId = Convert.ToInt32(epsdatabase.ExecuteScalar(dbCommand));

                return MaxId;
            }
        }
        #endregion
        #region update Common Designation 
        public string UpdateMsdesign(MsdesignBM msdesignBM)
        {
            string Response = null;

            if (msdesignBM != null)
            {                
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsCommonDesign))
                {
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigDesc", DbType.String, msdesignBM.CommonDesigDesc);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigGroupCD", DbType.Int32, msdesignBM.CommonDesigGroupCD);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigSuperannAge", DbType.Int32, msdesignBM.CommonDesigSuperannAge);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigSrNo", DbType.Int32, msdesignBM.CommonDesigSrNo);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "update");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);

                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"),CultureInfo.InvariantCulture);
                }
            }
            return Response;
        }
        #endregion
        #region insert Common Designation 
        public string InsertMsdesign(MsdesignBM  msdesignBM)
        {
            string Response = null;
            if (msdesignBM != null)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsCommonDesign))
                {
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigSuperannAge", DbType.Int32, msdesignBM.CommonDesigSuperannAge);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigDesc", DbType.String, msdesignBM.CommonDesigDesc);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigGroupCD", DbType.Int32, msdesignBM.CommonDesigGroupCD);
                    epsdatabase.AddInParameter(dbCommand, "CommonDesigSrNo", DbType.Int32, msdesignBM.CommonDesigSrNo);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "insert");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            }
            
            return Response;

        }
        #endregion

        #region delete Common Designation 
        public string DeleteMsdesign(int CommonDesigSrNo)
        {
            string Response = null;

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsCommonDesign))
            {
               
                epsdatabase.AddInParameter(dbCommand, "CommonDesigSrNo", DbType.Int32, CommonDesigSrNo);
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "delete");
                epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);
                Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
            }

            return Response;

        }
        #endregion
    }
}
