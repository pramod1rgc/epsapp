﻿using System;
using System.Collections.Generic;
using System.Text;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using EPS.Repositories.Masters;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace EPS.DataAccessLayers.Masters
{
    public class LeavetypeDAL:ILeavetype
    {
        Database epsdatabase = null;
        public LeavetypeDAL(Database database)
        {
            //DatabaseProviderFactory factory = new DatabaseProviderFactory();
            //epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);

            epsdatabase = database;
        }
        public LeavetypeDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
      
        #region Get LeaveType
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task <List<LeavetypeBM>> GetLeavetypes()
        {
            
            List<LeavetypeBM> LeavetypeList = null;
           
                LeavetypeList = new List<LeavetypeBM>();
                LeavetypeBM LeavetypeDatas;
            
                
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsLeaveType))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            LeavetypeDatas = new LeavetypeBM();
                            LeavetypeDatas.LeaveTypeID = objReader["MsLeaveTypeID"] != DBNull.Value ? Convert.ToInt32(objReader["MsLeaveTypeID"]) : 0;

                            LeavetypeDatas.LeaveTypeCd = objReader["LeaveTypeLeaveCd"] != DBNull.Value ? Convert.ToString(objReader["LeaveTypeLeaveCd"]).Trim() : null;
                            LeavetypeDatas.LeaveTypeDesc = objReader["LeaveTypeLeaveDesc"] != DBNull.Value ? Convert.ToString(objReader["LeaveTypeLeaveDesc"]).Trim() : null;
                            LeavetypeDatas.WEF = objReader["WEF"] != DBNull.Value ? Convert.ToString(objReader["WEF"]).Trim() : null;
                            LeavetypeDatas.WET = objReader["WET"] != DBNull.Value ? Convert.ToString(objReader["WET"]).Trim() : null;
                            LeavetypeDatas.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : true;
                            LeavetypeList.Add(LeavetypeDatas);

                        }

                    }
                }
            
       
            if (LeavetypeList == null)
                return null;
            else
                return LeavetypeList;

        }
        #endregion

        #region Get LeaveType MaxID
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task<int>GetLeavetypeMaxid()
        {

            int MaxId = 0;
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsLeaveType))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "selectMaxId");

                    MaxId = Convert.ToInt32(epsdatabase.ExecuteScalar(dbCommand));
                }
            
            if (MaxId == 0)
                return 0;
            else
                return MaxId;

        }
        #endregion

        #region Update Master Leave Type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task< string> UpdateMstLeavetype(LeavetypeBM LeavetypeBMObj)
        {
            string Response = null;
            if (LeavetypeBMObj != null)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsLeaveType))
                {
                    epsdatabase.AddInParameter(dbCommand, "LeaveTypeLeaveCd", DbType.String, LeavetypeBMObj.LeaveTypeCd);
                    epsdatabase.AddInParameter(dbCommand, "LeaveTypeLeaveDesc", DbType.String, LeavetypeBMObj.LeaveTypeDesc);
                    epsdatabase.AddInParameter(dbCommand, "MsLeaveTypeID", DbType.Int32, LeavetypeBMObj.LeaveTypeID);
                    epsdatabase.AddInParameter(dbCommand, "WEF", DbType.String, LeavetypeBMObj.WEF);
                    epsdatabase.AddInParameter(dbCommand, "WET", DbType.String, LeavetypeBMObj.WET);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "update");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            }
            return Response;

        }
        #endregion

        #region Insert Master Leave type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task< string> InsertMstLeavetype(LeavetypeBM LeavetypeBMObj)
        {
            string Response = null;
            if (LeavetypeBMObj != null)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsLeaveType))
                {
                    epsdatabase.AddInParameter(dbCommand, "LeaveTypeLeaveCd", DbType.String, LeavetypeBMObj.LeaveTypeCd);
                    epsdatabase.AddInParameter(dbCommand, "LeaveTypeLeaveDesc", DbType.String, LeavetypeBMObj.LeaveTypeDesc);
                    epsdatabase.AddInParameter(dbCommand, "WEF", DbType.String, LeavetypeBMObj.WEF);
                    epsdatabase.AddInParameter(dbCommand, "WET", DbType.String, LeavetypeBMObj.WET);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "insert");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            }
            return Response;

        }
        #endregion

        #region Delete Master Leave type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task<string> DeleteMstLeavetype(int MsLeavetypeId)
        {
            string Response = null;

            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsLeaveType))
            {
              
                epsdatabase.AddInParameter(dbCommand, "MsLeaveTypeID", DbType.Int32, MsLeavetypeId);
            
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "delete");
                epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                epsdatabase.ExecuteNonQuery(dbCommand);
                Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
            }

            return Response;

        }
        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
           
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                //context.Dispose();
                // Free any other managed objects here

                epsdatabase = null;
                }
            
           
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~LeavetypeDAL()
        {
            Dispose(false);
        }
    }
}
