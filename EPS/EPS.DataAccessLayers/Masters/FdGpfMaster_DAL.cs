﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class FdGpfMaster_DAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;
        public FdGpfMaster_DAL(Database database)
        {
            epsdatabase = database;
        }

        #region Insert And Update Family definition GPF Master Record
        /// <summary>
        ///  Insert And Update Family definition GPF Master Record
        /// </summary>
        public async Task<string> CreatefdGpfMaster(FdMaster_Model fdmodel)
        {
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_InsertUpdateFdGpfMaster))
                {
                    //if (hramodel.RelationShip.Trim() == string.Empty || hramodel.RelationShip.Trim() == "" || hramodel.RelationShip.Trim() == null)
                    if (string.IsNullOrEmpty(fdmodel.RelationShip.Trim()))
                    {
                        Response = "Relationship should not be blank";
                    }
                    else
                    {
                        if ((fdmodel.RelationShip).Trim().Length >= 3)
                        {
                            epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, fdmodel.FdMasterID);
                            epsdatabase.AddInParameter(dbCommand, "@RelationShip", DbType.String, fdmodel.RelationShip.Trim());
                            epsdatabase.AddInParameter(dbCommand, "@IPAddress", DbType.String, fdmodel.ClientIP);
                            epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, fdmodel.loginUser);
                            int i = 0;
                            i = epsdatabase.ExecuteNonQuery(dbCommand);
                            if (fdmodel.FdMasterID > 0 && i > 0)
                            {
                                Response = EPSResource.UpdateSuccessMessage;
                            }
                            else if (i > 0)
                            {
                                Response = EPSResource.SaveSuccessMessage;
                            }
                            else
                            { Response = EPSResource.AlreadyExistMessage; }
                        }
                        else
                        { Response = "Minimum length is three"; }

                    }
                }
            });
            return Response;
        }
        #endregion

        #region Get Family definition GPF Master Data
        /// <summary>
        ///  Get Family definition GPF Master Data
        /// </summary>
        public async Task<List<FdMaster_Model>> GetFdGpfMasterDetails()
        {
            
            List<FdMaster_Model> list = new List<FdMaster_Model>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_GetFdGpfMasterDetails))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            FdMaster_Model fdmodel = new FdMaster_Model();
                            fdmodel.FdMasterID = Convert.ToInt32(objReader["FdMasterID"]);
                            fdmodel.ID = Convert.ToInt32(objReader["ID"]);
                            fdmodel.RelationShip = objReader["RelationShip"].ToString();
                            fdmodel.SerialNo = objReader["SerialNo"].ToString();
                            list.Add(fdmodel);
                        }
                    }
                }
            });
            return list;
        }

        #endregion

        #region Edit Family definition GPF Master Details
        public async Task<List<FdMaster_Model>> EditFdGpfMasterDetails(int fdMasterID)
        {
            FdMaster_Model fdmodel = new FdMaster_Model();
            List<FdMaster_Model> list = new List<FdMaster_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_EditFdGpfMasterDetails);
                epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, fdMasterID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        fdmodel.FdMasterID = Convert.ToInt32(objReader["FdMasterID"]);
                        fdmodel.RelationShip = objReader["RelationShip"].ToString();
                        fdmodel.ID = Convert.ToInt32(objReader["ID"]);
                        list.Add(fdmodel);
                    }
                }
            });
            return list;
        }
        #endregion     

        #region Delete Family definition GPF Master Data
        /// <summary>
        /// Delete Family definition GPF Master Data
        /// </summary>
        /// <param name="HraMasterID"></param>
        public async Task<string> DeleteFdGpfMaster(int fdMasterID)
        {
            int i = 0;
            string Response = null;
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_DeleteFdGpfMaster);
                epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, fdMasterID);
                i = epsdatabase.ExecuteNonQuery(dbCommand);
                if (i > 0)
                {
                    Response = EPSResource.DeleteSuccessMessage;
                }
                else { Response = EPSResource.DeleteFailedMessage; }
            });
            return Response;
        }
        #endregion

    }
}
