﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace EPS.DataAccessLayers.Masters
{
    public class DAStateCentralDAL
    {
      readonly  Database epsdatabase = null;
        Database database = null;
        DbCommand dbCommand = null;
        public DAStateCentralDAL()
        {
        }
        public DAStateCentralDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get DA Details
        /// <summary>
        /// Bind all record in Grid=====================
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DAStateCentralBM> GetDADetails()
        {

            List<DAStateCentralBM> list = new List<DAStateCentralBM>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetDAMasterDetails);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    DAStateCentralBM _objDa = new DAStateCentralBM();
                    _objDa.MsDaMasterID = Convert.ToInt32(objReader["MsDaMasterID"]);
                    _objDa.DAType = objReader["DAType"].ToString();
                    _objDa.DASubType = objReader["DASubType"].ToString();
                    _objDa.Stateid = Convert.ToInt32(objReader["Stateid"] != DBNull.Value ? objReader["Stateid"] : _objDa.Stateid = 0);
                    _objDa.OrderNo = objReader["OrderNo"].ToString();
                    _objDa.PayCommissionID = Convert.ToInt32(objReader["PayCommissionID"] != DBNull.Value ? objReader["PayCommissionID"] : _objDa.PayCommissionID = 0);
                    _objDa.OrderDate = objReader["OrderDate"].ToString();
                    _objDa.PayCommissionName = objReader["PayCommissionName"].ToString();                
                    _objDa.CurrentDrate = Convert.ToDecimal(objReader["CurrentDrate"] != DBNull.Value ? objReader["CurrentDrate"] : _objDa.CurrentDrate = 0);
                    _objDa.CurrentWEF =objReader["CurrentWEF"].ToString();
                    _objDa.NewDrate = Convert.ToDecimal(objReader["NewDrate"] != DBNull.Value ? objReader["NewDrate"] : _objDa.NewDrate = 0);
                    _objDa.NewWEF = objReader["NewWEF"].ToString();
                    list.Add(_objDa);
                }
            }
            return list;

        }
        #endregion

        #region Get DA Details
        /// <summary>
        /// Bind all record in Grid
        /// </summary>
        /// <param name="DaType"></param>
        /// <returns></returns>
        public DAStateCentralBM GetDACurrentRateWef(string DaType,string Stateid_SubType)
        {
         DAStateCentralBM objDa = new DAStateCentralBM();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetDAMasterCurrent_Rate_Wef);
            database.AddInParameter(dbCommand, "@DAType", DbType.String, DaType);
            database.AddInParameter(dbCommand, "@Stateid_SubType", DbType.String, Stateid_SubType);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    DAStateCentralBM _objda = new DAStateCentralBM();
                    objDa.CurrentDrate = Convert.ToDecimal(objReader["CurrentDrate"] != DBNull.Value ? objReader["CurrentDrate"] : 0);
                    objDa.CurrentWEF = objReader["CurrentWEF"].ToString();     
                }
            }
            return objDa;
        }
        #endregion

        #region Insert and Update
        /// <summary>
        /// Insert and Update Da State And Central
        /// </summary>
        /// <param name="_objda"></param>
        /// <returns></returns>
        public string InsertandUpdate(DAStateCentralBM _objda)
        {
            int i = 0;
            string message = string.Empty;
            string hostName = System.Net.Dns.GetHostName(); // Retrive the Name of HOST  
            database = epsdatabase;
            if(_objda!=null)
            {
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_InsertUpdateDaMaster);
            database.AddInParameter(dbCommand, "MsDaMasterID", DbType.Int32, _objda.MsDaMasterID);
            database.AddInParameter(dbCommand, "DAType", DbType.String, _objda.DAType.Trim());
            database.AddInParameter(dbCommand, "CurrentDrate", DbType.String, _objda.CurrentDrate);
            database.AddInParameter(dbCommand, "CurrentWEF", DbType.String, _objda.CurrentWEF.Trim());
            database.AddInParameter(dbCommand, "DASubType", DbType.String, _objda.DASubType.Trim());
            database.AddInParameter(dbCommand, "PayCommissionID", DbType.Int32, _objda.PayCommissionID);                
            database.AddInParameter(dbCommand, "Stateid", DbType.Int32, _objda.Stateid);
            database.AddInParameter(dbCommand, "OrderNo", DbType.String, _objda.OrderNo.Trim());
            database.AddInParameter(dbCommand, "OrderDate", DbType.String, _objda.OrderDate.Trim());
            database.AddInParameter(dbCommand, "NewDrate", DbType.String, _objda.NewDrate);
            database.AddInParameter(dbCommand, "NewWEF", DbType.String, _objda.NewWEF.Trim());
            database.AddInParameter(dbCommand, "@IpAddress", DbType.String, _objda.IpAddress);    
            i = database.ExecuteNonQuery(dbCommand);               
            if (i > 0 && _objda.MsDaMasterID > 0)
            {
                message = "Updated Successfully";
                return message;
            }
            else if (i > 0)
            {
                message = "Saved Successfully";
                return message;
            }
            }
            else
            {
                message = "Failed To Save";
                return message;
            }
            return message;
        }
        #endregion

        #region Delete
        /// <summary>
        /// soft delete da master
        /// </summary>
        /// <param name="MsDaMasterID"></param>
        /// <returns></returns>
        public string Delete(int MsDaMasterID)
        {
            string message = string.Empty;
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_DeleteDAMaster);
            database.AddInParameter(dbCommand, "@MsDaMasterID", DbType.Int32, MsDaMasterID);
            int i = 0;

            i = database.ExecuteNonQuery(dbCommand);
            if (i > 0)
            {
                message = "Deleted Successfully";
            }
            else { message = "Deletion failed"; }

            return message;
        }
        #endregion


    }
}
