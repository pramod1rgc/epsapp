﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class DeductionMasterDAL
    {
        private Database epsdatabase = null;
        public DeductionMasterDAL(Database database)
        {
            epsdatabase = database;
        }

        public async Task<List<DeductionMasterBM>> CategoryList()
        {
            List<DeductionMasterBM> listDeductionMasterBM = null;
            listDeductionMasterBM = new List<DeductionMasterBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.CategoryList))
                {
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DeductionMasterBM deductionMasterBM = new DeductionMasterBM();
                            deductionMasterBM.CategoryID =Convert.ToInt32(rdr["CategoryID"].ToString());
                            deductionMasterBM.CategoryName = rdr["CategoryName"].ToString();
                            listDeductionMasterBM.Add(deductionMasterBM);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (listDeductionMasterBM == null)
                return null;
            else
                return listDeductionMasterBM;
        }

        public async Task<List<DeductionDescriptionBM>> DeductionDescription()
        {
            List<DeductionDescriptionBM> listDeductionMasterBM = null;
            listDeductionMasterBM = new List<DeductionDescriptionBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.DeductionDescription))//"[ODS].[DeductionDescription]"
                {
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DeductionDescriptionBM deductionMasterBM = new DeductionDescriptionBM();
                            deductionMasterBM.PayItemsCD = rdr["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(rdr["PayItemsCD"]) : 0;
                            deductionMasterBM.PayItemsName = rdr["PayItemsName"] != DBNull.Value ? Convert.ToString(rdr["PayItemsName"]).Trim() : null;
                            listDeductionMasterBM.Add(deductionMasterBM);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (listDeductionMasterBM == null)
                return null;
            else
                return listDeductionMasterBM;
        }

        public async Task<List<DeductionDescriptionBM>> DeductionDescriptionChange(int payItemsCD)
        {
            List<DeductionDescriptionBM> listDeductionMasterBM = null;
            listDeductionMasterBM = new List<DeductionDescriptionBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.DeductionDescriptionChange))//"[ODS].[DeductionDescriptionChange]"
                {
                    epsdatabase.AddInParameter(dbCommand, "payItemsCD", DbType.String, payItemsCD);
                   
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DeductionDescriptionBM deductionMasterBM = new DeductionDescriptionBM();
                            deductionMasterBM.GrantNo = rdr["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(rdr["PayItemsCD"]) : 0;
                            deductionMasterBM.PayItemsNameShort = rdr["PayItemsNameShort"] != DBNull.Value ? Convert.ToString(rdr["PayItemsNameShort"]).Trim() : null;
                            deductionMasterBM.GrantNo = rdr["GrantNum"] != DBNull.Value ? Convert.ToInt32(rdr["GrantNum"]) : 0;
                            deductionMasterBM.FunctionalHead = rdr["FuctionalHead"] != DBNull.Value ? Convert.ToString(rdr["FuctionalHead"]).Trim() : null;
                            deductionMasterBM.ObjectHead = rdr["ObjectHead"] != DBNull.Value ? Convert.ToString(rdr["ObjectHead"]).Trim() : null;
                            deductionMasterBM.CategoryID = rdr["Category"] != DBNull.Value ? Convert.ToInt32(rdr["Category"]) : 0;
                            deductionMasterBM.ExemptedFromITax = rdr["ExemptedFromITax"] != DBNull.Value ? Convert.ToBoolean(rdr["ExemptedFromITax"]) : false;
                            deductionMasterBM.AutoCalculated = rdr["AutoCalculated"] != DBNull.Value ? Convert.ToBoolean(rdr["AutoCalculated"]) : false;
                            deductionMasterBM.SubtractedFromSalary = rdr["SubtractedFromSalary"] != DBNull.Value ? Convert.ToBoolean(rdr["SubtractedFromSalary"]) : false;
                            listDeductionMasterBM.Add(deductionMasterBM);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (listDeductionMasterBM == null)
                return null;
            else
                return listDeductionMasterBM;
        }

        public async Task<List<DeductionDescriptionBM>> GetDeductionDefinitionList()
        {
            List<DeductionDescriptionBM> listDeductionMasterBM = null;
            listDeductionMasterBM = new List<DeductionDescriptionBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetDeductionDefinitionList))//"[ODS].[GetDeductionDefinitionList]"
                {
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            DeductionDescriptionBM deductionMasterBM = new DeductionDescriptionBM();
                            deductionMasterBM.PayItemsCD = rdr["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(rdr["PayItemsCD"]) : 0;
                            deductionMasterBM.PayItemsName = rdr["PayItemsName"] != DBNull.Value ? Convert.ToString(rdr["PayItemsName"]).Trim() : null;
                            deductionMasterBM.PayItemsNameShort = rdr["PayItemsNameShort"] != DBNull.Value ? Convert.ToString(rdr["PayItemsNameShort"]).Trim() : null;
                            deductionMasterBM.GrantNo = rdr["GrantNum"] != DBNull.Value ? Convert.ToInt32(rdr["GrantNum"]) : 0;
                            deductionMasterBM.FunctionalHead = rdr["FuctionalHead"] != DBNull.Value ? Convert.ToString(rdr["FuctionalHead"]).Trim() : null;
                            deductionMasterBM.ObjectHead = rdr["ObjectHead"] != DBNull.Value ? Convert.ToString(rdr["ObjectHead"]).Trim() : null;
                            deductionMasterBM.CategoryID = rdr["Category"] != DBNull.Value ? Convert.ToInt32(rdr["Category"]) : 0;
                            listDeductionMasterBM.Add(deductionMasterBM);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (listDeductionMasterBM == null)
                return null;
            else
                return listDeductionMasterBM;
        }
        

        public async Task<int> DeductionDefinitionSubmit(DeductionDescriptionBM ObjDuesDefinationandDuesMasterModel)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.DeductionDefinitionSubmit))//"[ODS].[DeductionDefinitionSubmit]"
                {
                    epsdatabase.AddInParameter(dbCommand, "PayItemsCD", DbType.Int32, ObjDuesDefinationandDuesMasterModel.PayItemsCD);
                    epsdatabase.AddInParameter(dbCommand, "SubtractedFromSalary", DbType.String, ObjDuesDefinationandDuesMasterModel.SubtractedFromSalary);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsIsTaxable", DbType.String, ObjDuesDefinationandDuesMasterModel.ExemptedFromITax);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsComputable", DbType.Int16, ObjDuesDefinationandDuesMasterModel.AutoCalculated);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            }).ConfigureAwait(true);
            return result;
        }

        //===============================================Deduction Rate Master=================================================

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetOrgnazationTypeForDues()
        {
            List<DeductionRateAndDeductionMasterModel> objlISTOrganizationTypeModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetOrgnazationTypeForDeduction))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel objOrganizationTypeModel = new DeductionRateAndDeductionMasterModel();
                            objOrganizationTypeModel.OrganizationType = objReader["Code"] != DBNull.Value ? objReader["Code"].ToString() : objOrganizationTypeModel.OrganizationType = null;
                            objOrganizationTypeModel.Description = objReader["Description"] != DBNull.Value ? objReader["Description"].ToString() : objOrganizationTypeModel.Description = null;
                            objlISTOrganizationTypeModel.Add(objOrganizationTypeModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            return objlISTOrganizationTypeModel;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetDuesCodeDuesDefination()
        {
            List<DeductionRateAndDeductionMasterModel> ObjlistDuesDefinationandDuesMasterModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetDeductionCodeAndDescrptionForDeductionRate))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel ObjDuesDefinationandDuesMasterModel = new DeductionRateAndDeductionMasterModel();
                            ObjDuesDefinationandDuesMasterModel.DuesCodeDuesDefination = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : ObjDuesDefinationandDuesMasterModel.DuesCodeDuesDefination = 0;
                            ObjDuesDefinationandDuesMasterModel.PayItemsName = objReader["DuesCodeDuesDefination"] != DBNull.Value ? objReader["DuesCodeDuesDefination"].ToString() : ObjDuesDefinationandDuesMasterModel.PayItemsName = null;
                            ObjDuesDefinationandDuesMasterModel.PayRatesPayItemCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : ObjDuesDefinationandDuesMasterModel.PayRatesPayItemCD = 0;
                            ObjlistDuesDefinationandDuesMasterModel.Add(ObjDuesDefinationandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            return ObjlistDuesDefinationandDuesMasterModel;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetCityClassForDuesRate(string payCommId)
        {
            List<DeductionRateAndDeductionMasterModel> objlistCityClassModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetCityClassForDeductionRate))
                {
                    epsdatabase.AddInParameter(dbCommand, "payComId", DbType.String, payCommId);
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel objCityClassModel = new DeductionRateAndDeductionMasterModel();
                            objCityClassModel.CityClass = objReader["CclPaycommId"] != DBNull.Value ? Convert.ToInt32(objReader["CclPaycommId"]) : 0;
                            objCityClassModel.PayCityClass = objReader["CclCityclass"] != DBNull.Value ? objReader["CclCityclass"].ToString() : objCityClassModel.PayCityClass = null;
                            objCityClassModel.MsCityclassID = objReader["MsCityclassID"] != DBNull.Value ? Convert.ToInt32(objReader["MsCityclassID"]) : 0;
                            objlistCityClassModel.Add(objCityClassModel);
                        }
                    }
                }
            }).ConfigureAwait(true);

            return objlistCityClassModel;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetStateForDuesRate()
        {
            List<DeductionRateAndDeductionMasterModel> ObjListstateModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetStateNameForDeductionMaster))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel ObjstateModel = new DeductionRateAndDeductionMasterModel();
                            ObjstateModel.state = objReader["StateCode"] != DBNull.Value ? objReader["StateCode"].ToString() : ObjstateModel.state = null;
                            ObjstateModel.StateName = objReader["StateName"] != DBNull.Value ? objReader["StateName"].ToString() : ObjstateModel.StateName = null;

                            ObjListstateModel.Add(ObjstateModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            return ObjListstateModel;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetPayCommForDuesRate()
        {
            List<DeductionRateAndDeductionMasterModel> ObjListDuesRateandDuesMasterModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetPayCommForDeductionMaster))//sp_GetPatCommForDuesRate
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel ObjDuesRateandDuesMasterModel = new DeductionRateAndDeductionMasterModel();
                            ObjDuesRateandDuesMasterModel.PayCommission = objReader["MsPayCommID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayCommID"]) : ObjDuesRateandDuesMasterModel.PayCommission = 0;
                            ObjDuesRateandDuesMasterModel.PayCommDesc = objReader["PayCommDesc"] != DBNull.Value ? objReader["PayCommDesc"].ToString() : ObjDuesRateandDuesMasterModel.PayCommDesc = null;
                            ObjListDuesRateandDuesMasterModel.Add(ObjDuesRateandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            return ObjListDuesRateandDuesMasterModel;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetSlabtypeByPayCommId(string payCommId)
        {
            List<DeductionRateAndDeductionMasterModel> ObjListDuesRateandDuesMasterModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetSlabTypeForDeductionMaster))//sp_GetSlabTypebyPayCommIdForDuesRate
                {
                    epsdatabase.AddInParameter(dbCommand, "payCommID", DbType.String, payCommId);
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel ObjDuesRateandDuesMasterModel = new DeductionRateAndDeductionMasterModel();
                            ObjDuesRateandDuesMasterModel.SlabType = objReader["MsSlabTypeID"] != DBNull.Value ? Convert.ToInt32(objReader["MsSlabTypeID"]) : ObjDuesRateandDuesMasterModel.SlabType = 0;
                            ObjDuesRateandDuesMasterModel.MsSlabTypeDes = objReader["SlabType"] != DBNull.Value ? objReader["SlabType"].ToString() : ObjDuesRateandDuesMasterModel.MsSlabTypeDes = null;
                            ObjListDuesRateandDuesMasterModel.Add(ObjDuesRateandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(true);
            return ObjListDuesRateandDuesMasterModel;
        }

        public async Task<int> InsertUpdateDuesRateDetails(DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel)
        {
            DataTable dt = CommonClasses.CommonFunctions.ToDataTable(objDuesRateandDuesMasterModel.RateDetails);
            var result = 0;
            Action action = () =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.InsertDeductionRateDetails))//sp_insertandUpDuesRateDetails
                {
                    SqlParameter tblpara = new SqlParameter("RateDetails", dt);
                    tblpara.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(tblpara);

                    epsdatabase.AddInParameter(dbCommand, "MsDueRtDetailId", DbType.String, objDuesRateandDuesMasterModel.MsPayRatesID);
                    epsdatabase.AddInParameter(dbCommand, "payItemId", DbType.String, objDuesRateandDuesMasterModel.DuesCodeDuesDefination);
                    epsdatabase.AddInParameter(dbCommand, "orgtype", DbType.String, objDuesRateandDuesMasterModel.OrganizationType);
                    epsdatabase.AddInParameter(dbCommand, "paycomm", DbType.Int32, objDuesRateandDuesMasterModel.PayCommission);
                    epsdatabase.AddInParameter(dbCommand, "duescode", DbType.Int32, objDuesRateandDuesMasterModel.DuesCodeDuesDefination);
                    epsdatabase.AddInParameter(dbCommand, "cityclass", DbType.Int32, objDuesRateandDuesMasterModel.CityClass);
                    epsdatabase.AddInParameter(dbCommand, "WithEffectFrom", DbType.DateTime, objDuesRateandDuesMasterModel.WithEffectFrom);
                    epsdatabase.AddInParameter(dbCommand, "WithEffectTo", DbType.DateTime, objDuesRateandDuesMasterModel.WithEffectTo);
                    epsdatabase.AddInParameter(dbCommand, "value", DbType.String, objDuesRateandDuesMasterModel.Value);
                    epsdatabase.AddInParameter(dbCommand, "state", DbType.String, objDuesRateandDuesMasterModel.state);
                    epsdatabase.AddInParameter(dbCommand, "slabtype", DbType.String, objDuesRateandDuesMasterModel.SlabType);
                    epsdatabase.AddInParameter(dbCommand, "vletterNo", DbType.String, objDuesRateandDuesMasterModel.VideLetterNo.Trim());
                    epsdatabase.AddInParameter(dbCommand, "vletterdate", DbType.DateTime, objDuesRateandDuesMasterModel.VideLetterDate);
                    epsdatabase.AddInParameter(dbCommand, "Activated", DbType.String, objDuesRateandDuesMasterModel.Activated);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                    if (result == 0)
                    {
                        result = -1;
                    }
                    dt.Dispose();
                }
            };
            await Task.Run(action).ConfigureAwait(true);
            return result;
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetDuesRatemaster()
        {
            List<DeductionRateAndDeductionMasterModel> objDuesDefinationandDuesMasterModelList = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetDeductionRateDetails))//Sp_GetDuesRateMaster
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel = new DeductionRateAndDeductionMasterModel();
                            objDuesRateandDuesMasterModel.MsPayRatesID = objReader["MsDueRtDetailId"] != DBNull.Value ? Convert.ToInt32(objReader["MsDueRtDetailId"]) : 0;
                            objDuesRateandDuesMasterModel.PayRatesPayItemCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : 0;
                            objDuesRateandDuesMasterModel.PayItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]) : null;
                            objDuesRateandDuesMasterModel.WithEffectFrom = objReader["FormDate"] != DBNull.Value ? Convert.ToDateTime(objReader["FormDate"]) : objDuesRateandDuesMasterModel.WithEffectFrom = null;
                            objDuesRateandDuesMasterModel.WithEffectTo = objReader["Todate"] != DBNull.Value ? Convert.ToDateTime(objReader["Todate"]) : objDuesRateandDuesMasterModel.WithEffectTo = null;
                            objDuesRateandDuesMasterModel.MsSlabTypeDes = objReader["SlabType"] != DBNull.Value ? Convert.ToString(objReader["SlabType"]) : objDuesRateandDuesMasterModel.MsSlabTypeDes = null; objDuesRateandDuesMasterModel.SlabType = objReader["DupSlabType"] != DBNull.Value ? Convert.ToInt32(objReader["DupSlabType"]) : objDuesRateandDuesMasterModel.SlabType = 0;
                            objDuesRateandDuesMasterModel.PayCommission = objReader["DupPaycomm"] != DBNull.Value ? Convert.ToInt32(objReader["DupPaycomm"]) : 0;
                            objDuesRateandDuesMasterModel.CityClass = objReader["DupCityClass"] != DBNull.Value ? Convert.ToInt32(objReader["DupCityClass"]) : 0;
                            objDuesRateandDuesMasterModel.VideLetterDate = objReader["DupVLetterDate"] != DBNull.Value ? Convert.ToDateTime(objReader["DupVLetterDate"]) : objDuesRateandDuesMasterModel.VideLetterDate = null;
                            objDuesRateandDuesMasterModel.state = objReader["DupState"] != DBNull.Value ? Convert.ToString(objReader["DupState"]) : null;
                            objDuesRateandDuesMasterModel.OrganizationType = objReader["DupOrgType"] != DBNull.Value ? Convert.ToString(objReader["DupOrgType"]) : null;
                            objDuesRateandDuesMasterModel.Activated = objReader["DupActivated"] != DBNull.Value ? Convert.ToString(objReader["DupActivated"]) : null;
                            objDuesDefinationandDuesMasterModelList.Add(objDuesRateandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(true);

            if (objDuesDefinationandDuesMasterModelList == null)
            {
                return null;
            }
            else
            {
                return objDuesDefinationandDuesMasterModelList;
            }
        }

        public async Task<List<DeductionRateAndDeductionMasterModel>> GetDuesRateDetails1()
        {
            List<DeductionRateAndDeductionMasterModel> objListDuesRateandDuesMasterModel = new List<DeductionRateAndDeductionMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.GetDeductionRateDetails_SP))//sp_GetDuesRateDetails
                {
                    using (DataSet objData = this.epsdatabase.ExecuteDataSet(dbCommand))
                    {
                        if (objData != null && objData.Tables.Count > 0)
                        {
                            DataTable dtSection = objData.Tables[0];
                            DataTable dtRate = objData.Tables[1];

                            foreach (DataRow drow in dtSection.Rows)
                            {
                                DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel = new DeductionRateAndDeductionMasterModel();

                                objDuesRateandDuesMasterModel.MsPayRatesID = Convert.ToInt32(drow["MsDueRtDetailId"]);
                                objDuesRateandDuesMasterModel.MsDueRtDetailId = Convert.ToInt32(drow["MsDueRtDetailId"]);
                                objDuesRateandDuesMasterModel.PayRatesPayItemCD = Convert.ToInt32(drow["PayItemsCD"]);
                                objDuesRateandDuesMasterModel.PayItemsName = Convert.ToString(drow["PayItemsName"]);
                                objDuesRateandDuesMasterModel.DuesCodeDuesDefination = Convert.ToInt32(drow["MsPayItemsID"]);
                                objDuesRateandDuesMasterModel.OrganizationType = Convert.ToString(drow["DupOrgType"]);
                                objDuesRateandDuesMasterModel.PayCommission = Convert.ToInt32(drow["DupPaycomm"]);
                                objDuesRateandDuesMasterModel.CityClass = Convert.ToInt32(drow["DupCityClass"]);
                                objDuesRateandDuesMasterModel.WithEffectFrom = Convert.ToDateTime(drow["FormDate"]);

                                var outputParam = drow["Todate"];
                                if (!(outputParam is DBNull))
                                {
                                    objDuesRateandDuesMasterModel.WithEffectTo = Convert.ToDateTime(drow["Todate"]);
                                }

                                objDuesRateandDuesMasterModel.Value = Convert.ToString(drow["DupValue"]).Trim();
                                objDuesRateandDuesMasterModel.state = Convert.ToString(drow["DupState"]).Trim();
                                objDuesRateandDuesMasterModel.SlabType = Convert.ToInt32(drow["DupSlabType"]);
                                objDuesRateandDuesMasterModel.MsSlabTypeDes = Convert.ToString(drow["SlabType"]);
                                objDuesRateandDuesMasterModel.VideLetterNo = Convert.ToString(drow["DupVLetterNo"]);

                                var outputParamDupVLetter = drow["DupVLetterDate"];
                                if (!(outputParamDupVLetter is DBNull))
                                {
                                    objDuesRateandDuesMasterModel.VideLetterDate = Convert.ToDateTime(drow["DupVLetterDate"]);
                                }

                                objDuesRateandDuesMasterModel.Activated = Convert.ToString(drow["DupActivated"]);

                                DataView dv1 = dtRate.DefaultView;
                                dv1.RowFilter = "DueRtDetailId = '" + objDuesRateandDuesMasterModel.MsDueRtDetailId + "'";
                                DataTable dtRateNew = dv1.ToTable();

                                foreach (DataRow row in dtRateNew.Rows)
                                {
                                    DeductionRateDetails ObjrateDetails = new DeductionRateDetails();
                                    ObjrateDetails.MsDueRtDetailId = Convert.ToInt32(row["DueRtDetailId"]);
                                    var minAmmount = row["MinAmount"];
                                    if (!(minAmmount is DBNull))
                                    {
                                        ObjrateDetails.MinAmount = Convert.ToInt32(row["MinAmount"]);
                                    }
                                    var LLimit = row["LLimit"];
                                    if (!(LLimit is DBNull))
                                    {
                                        ObjrateDetails.LowerLimit = Convert.ToInt32(row["LLimit"]);
                                    }

                                    ObjrateDetails.UpperLimit = Convert.ToInt32(row["Ulimit"]);
                                    ObjrateDetails.ValueDuesRate = Convert.ToInt32(row["Value"]);
                                    ObjrateDetails.SlabNo = Convert.ToInt32(row["SlabNo"]);
                                    objDuesRateandDuesMasterModel.RateDetails.Add(ObjrateDetails);
                                }
                                objListDuesRateandDuesMasterModel.Add(objDuesRateandDuesMasterModel);
                            }
                        }
                    }
                }
            }).ConfigureAwait(true);
            return objListDuesRateandDuesMasterModel;
        }

        /// <summary>
        /// Delete Dues Defination record
        /// </summary>
        /// <param name="DuesCd"></param>
        /// <returns></returns>
        public async Task<int> DeleteDuesRateDetails(string duesratedetailsId)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.DeleteDeductionRateDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDueRtDetailId", DbType.Int32, duesratedetailsId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            }).ConfigureAwait(true);

            return result;
        }

    }
}
