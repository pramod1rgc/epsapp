﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
  public class GpfWithdrawRulesDA
    {
      private  Database epsDataBase = null;

        public GpfWithdrawRulesDA(Database database)
        {

            epsDataBase = database;
        }

        public async Task<int> InsertUpdateGpfWithdrawRules(GpfWithdrawRules objGpfWithdrawRules)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_InsertUpdateGpfWithdrawRules))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfWithdrawRulesRefID", DbType.Int32, objGpfWithdrawRules.MsGpfWithdrawRulesRefID);
                    epsDataBase.AddInParameter(dbCommand, "PfRefNo", DbType.Int32, objGpfWithdrawRules.PfRefNo);
                    epsDataBase.AddInParameter(dbCommand, "PfType", DbType.String, objGpfWithdrawRules.PfType);
                    epsDataBase.AddInParameter(dbCommand, "RuleApplicableFromDate", DbType.DateTime, objGpfWithdrawRules.RuleApplicableFromDate);
                    epsDataBase.AddInParameter(dbCommand, "PfTypeValidTillDate", DbType.DateTime, objGpfWithdrawRules.PfTypeValidTillDate);
                    epsDataBase.AddInParameter(dbCommand, "NoOfWithdraw", DbType.Int32, objGpfWithdrawRules.NoOfWithdraw);
                    epsDataBase.AddInParameter(dbCommand, "MaxPerOfBalance", DbType.Int32, objGpfWithdrawRules.MaxPerOfBalance);
                   // epsDataBase.AddInParameter(dbCommand, "MinService", DbType.Int32, objGpfWithdrawRules.MinService);
                    epsDataBase.AddInParameter(dbCommand, "MaxServiceWithPurpose", DbType.Int32, objGpfWithdrawRules.MaxServiceWithPurpose);
                    epsDataBase.AddInParameter(dbCommand, "MaxServiceWithoutPurpose", DbType.Int32, objGpfWithdrawRules.MaxServiceWithoutPurpose);
                    epsDataBase.AddInParameter(dbCommand, "SealingWithPay", DbType.Int32, objGpfWithdrawRules.SealingWithPay);
                    epsDataBase.AddInParameter(dbCommand, "GpfRuleRefNo", DbType.String, objGpfWithdrawRules.GpfRuleRefNo);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        public async Task<List<GpfWithdrawRules>> GetGpfWithdrawRulesById(int MsGpfWithdrawRulesRefID)
        {
            List<GpfWithdrawRules> GpfWithdrawRulesList = new List<GpfWithdrawRules>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GpfWithdrawRulesById))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfWithdrawRulesRefID", DbType.Int32, MsGpfWithdrawRulesRefID);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            GpfWithdrawRules GpfWithdrawRulesData = new GpfWithdrawRules();
                            GpfWithdrawRulesData.MsGpfWithdrawRulesRefID = objReader["MsGpfWithdrawRulesRefID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGpfWithdrawRulesRefID"]) : 0;
                            GpfWithdrawRulesData.PfRefNo = objReader["PfRefNo"] != DBNull.Value ? Convert.ToInt32(objReader["PfRefNo"]) :  0;
                            GpfWithdrawRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : null;
                            GpfWithdrawRulesData.RuleApplicableFromDate = objReader["RuleApplicableFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicableFromDate"]) : GpfWithdrawRulesData.RuleApplicableFromDate = null;
                            GpfWithdrawRulesData.PfTypeValidTillDate = objReader["PfTypeValidTillDate"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDate"]) : GpfWithdrawRulesData.PfTypeValidTillDate = null;
                            GpfWithdrawRulesData.NoOfWithdraw = objReader["NoOfWithdraw"] != DBNull.Value ? Convert.ToInt32(objReader["NoOfWithdraw"]) :  0;
                            GpfWithdrawRulesData.MaxPerOfBalance = objReader["MaxPerOfBalance"] != DBNull.Value ? Convert.ToInt32(objReader["MaxPerOfBalance"]) :  0;
                           // GpfWithdrawRulesData.MinService = objReader["MinService"] != DBNull.Value ? Convert.ToInt32(objReader["MinService"]) : GpfWithdrawRulesData.MinService = 0;
                            GpfWithdrawRulesData.MaxServiceWithPurpose = objReader["MaxServiceWithPurpose"] != DBNull.Value ? Convert.ToInt32(objReader["MaxServiceWithPurpose"]) :  0;
                            GpfWithdrawRulesData.MaxServiceWithoutPurpose = objReader["MaxServiceWithoutPurpose"] != DBNull.Value ? Convert.ToInt32(objReader["MaxServiceWithoutPurpose"]) :  0;
                            GpfWithdrawRulesData.SealingWithPay = objReader["SealingWithPay"] != DBNull.Value ? Convert.ToInt32(objReader["SealingWithPay"]) :  0;
                            GpfWithdrawRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() :null;
                            GpfWithdrawRulesList.Add(GpfWithdrawRulesData);
                        }

                    }

                }
            });
            return GpfWithdrawRulesList;
        }

        public async Task<List<GpfWithdrawRules>> GetGpfWithdrawRules()
        {

            List<GpfWithdrawRules> GpfWithdrawRulesList = new List<GpfWithdrawRules>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GpfWithdrawRules))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            GpfWithdrawRules GpfWithdrawRulesData = new GpfWithdrawRules();
                            GpfWithdrawRulesData.MsGpfWithdrawRulesRefID = objReader["MsGpfWithdrawRulesRefID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGpfWithdrawRulesRefID"]) :  0;
                            GpfWithdrawRulesData.PfRefNo = objReader["PfRefNo"] != DBNull.Value ? Convert.ToInt32(objReader["PfRefNo"]) :  0;
                            GpfWithdrawRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() :  null;
                            GpfWithdrawRulesData.RuleApplicableFromDate = objReader["RuleApplicableFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicableFromDate"]) : GpfWithdrawRulesData.RuleApplicableFromDate = null;
                            GpfWithdrawRulesData.PfTypeValidTillDate = objReader["PfTypeValidTillDate"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDate"]) : GpfWithdrawRulesData.PfTypeValidTillDate = null;
                            GpfWithdrawRulesData.NoOfWithdraw = objReader["NoOfWithdraw"] != DBNull.Value ? Convert.ToInt32(objReader["NoOfWithdraw"]) :  0;
                            GpfWithdrawRulesData.MaxPerOfBalance = objReader["MaxPerOfBalance"] != DBNull.Value ? Convert.ToInt32(objReader["MaxPerOfBalance"]) : 0;
                           // GpfWithdrawRulesData.MinService = objReader["MinService"] != DBNull.Value ? Convert.ToInt32(objReader["MinService"]) : GpfWithdrawRulesData.MinService = 0;
                            GpfWithdrawRulesData.MaxServiceWithPurpose = objReader["MaxServiceWithPurpose"] != DBNull.Value ? Convert.ToInt32(objReader["MaxServiceWithPurpose"]) :  0;
                            GpfWithdrawRulesData.MaxServiceWithoutPurpose = objReader["MaxServiceWithoutPurpose"] != DBNull.Value ? Convert.ToInt32(objReader["MaxServiceWithoutPurpose"]) :  0;
                            GpfWithdrawRulesData.SealingWithPay = objReader["SealingWithPay"] != DBNull.Value ? Convert.ToInt32(objReader["SealingWithPay"]) :  0;
                            GpfWithdrawRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() :null;
                            GpfWithdrawRulesList.Add(GpfWithdrawRulesData);
                        }

                    }

                }
            });

            if (GpfWithdrawRulesList == null)
            {
                return null;
            }
            else
            {
                return GpfWithdrawRulesList;
            }
        }
        public async Task<int> DeleteGpfWithdrawRules(int MsGpfWithdrawRulesRefID)
        {

            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_DeleteGpfWithdrawRules))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfWithdrawRulesRefID", DbType.Int32, MsGpfWithdrawRulesRefID);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
    }
}
