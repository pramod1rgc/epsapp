﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class GpfAdvanceReasonsDAL
    {
        Database epsdatabase = null;
        Database database = null;
        DbCommand dbCommand = null;
        public GpfAdvanceReasonsDAL()
        {
        }
        public GpfAdvanceReasonsDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Bind Main Reason
        /// <summary>
        /// Bind Main Reason
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GpfAdvanceReasonsBM> BindMainReason()
        {
            List<GpfAdvanceReasonsBM> list = new List<GpfAdvanceReasonsBM>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_GetMainReasonType);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    GpfAdvanceReasonsBM _gpfmainreason = new GpfAdvanceReasonsBM();
                    _gpfmainreason.mainReasonID = Convert.ToInt32(objReader["MainReasonID"]);
                    _gpfmainreason.mainReasonText = objReader["MainReasonText"].ToString();
                    list.Add(_gpfmainreason);
                }
            }
            return list;
        }
        #endregion

        #region Get DA Details
        /// <summary>
        /// Bind all record in Grid=====================
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GpfAdvanceReasonsBM> GetGPFAdvanceReasonsDetails(string AdvWithdraw)
        {
            List<GpfAdvanceReasonsBM> list = new List<GpfAdvanceReasonsBM>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_GetGPFAdvanceReasonsDetails);
            database.AddInParameter(dbCommand, "@AdvWithdraw", DbType.String, AdvWithdraw);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    GpfAdvanceReasonsBM _objgpf = new GpfAdvanceReasonsBM();
                    _objgpf.pfRefNo = Convert.ToInt32(objReader["PfRefNo"]);
                    _objgpf.pfType = objReader["PfType"].ToString();
                    _objgpf.pfTypeName = objReader["PfTypeName"].ToString();
                    _objgpf.reasonDescription = objReader["ReasonDescription"].ToString();
                    _objgpf.mainReasonID = Convert.ToInt32(objReader["MainReason"]);
                    _objgpf.mainReasonText = objReader["MainReasonText"].ToString();
                    _objgpf.outstandingLimitForPreviousAdvance = objReader["OutstandingLimitForPreviousAdvance"].ToString();
                    //_objgpf.monthsBetweenAdvances = objReader["MonthsBetweenAdvances"].ToString();
                    _objgpf.sealingForAdvancePay = objReader["SealingForAdvancePay"].ToString();
                    _objgpf.sealingForAdvanceBalance = objReader["SealingForAdvanceBalance"].ToString();
                    //_objgpf.convertWithdrawals = objReader["ConvertWithdrawals"].ToString();
                    //_objgpf.noOfAdvances = objReader["NoOfAdvances"].ToString();
                    _objgpf.pFRuleReferenceNumber = objReader["PFRuleReferenceNumber"].ToString().Trim();
                    _objgpf.minService = objReader["MinService"].ToString();
                    list.Add(_objgpf);
                }
            }
            return list;

        }
        #endregion
        #region Insert and Update
        /// <summary>
        /// Insert and Update Da State And Central
        /// </summary>
        /// <param name="_objda"></param>
        /// <returns></returns>
        public async Task<string> InsertandUpdate(GpfAdvanceReasonsBM objgpf)
        {
            int i = 0;
            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.usp_InsertUpdateGpfAdvanceReasons))
                {
                    database = epsdatabase;
                    database.AddInParameter(dbCommand, "PfRefNo", DbType.Int32, objgpf.pfRefNo);
                    database.AddInParameter(dbCommand, "PfType", DbType.String, objgpf.pfType);
                    database.AddInParameter(dbCommand, "ReasonDescription", DbType.String, objgpf.reasonDescription);
                    database.AddInParameter(dbCommand, "MainReasonID", DbType.String, objgpf.mainReasonID);
                    database.AddInParameter(dbCommand, "OutstandingLimitForPreviousAdvance", DbType.Decimal, objgpf.outstandingLimitForPreviousAdvance);
                    //database.AddInParameter(dbCommand, "MonthsBetweenAdvances", DbType.String, objgpf.monthsBetweenAdvances);
                    database.AddInParameter(dbCommand, "SealingForAdvancePay", DbType.String, objgpf.sealingForAdvancePay);
                    database.AddInParameter(dbCommand, "SealingForAdvanceBalance", DbType.String, objgpf.sealingForAdvanceBalance);
                    //database.AddInParameter(dbCommand, "ConvertWithdrawals", DbType.String, objgpf.convertWithdrawals);
                    //database.AddInParameter(dbCommand, "NoOfAdvances", DbType.String, objgpf.noOfAdvances);
                    database.AddInParameter(dbCommand, "PFRuleReferenceNumber", DbType.String, objgpf.pFRuleReferenceNumber);
                    database.AddInParameter(dbCommand, "AdvWithdraw", DbType.String, objgpf.AdvWithdraw);
                    database.AddInParameter(dbCommand, "@IpAddress", DbType.String, objgpf.ipAddress);
                    database.AddInParameter(dbCommand, "MinService", DbType.String, objgpf.minService);         
                    i = database.ExecuteNonQuery(dbCommand);
                    if (i > 0 && objgpf.pfRefNo > 0)
                    {
                        if (i > 0)
                        {
                            message = EPSResource.UpdateSuccessMessage;
                        }
                        else
                        {
                            message = EPSResource.UpdateFailedMessage;
                        }
                    }
                    else if (i > 0)
                    {
                        message = EPSResource.SaveSuccessMessage;
                    }
                    else if (i == -1)
                        message = EPSResource.AlreadyExistMessage;
                    else { message = EPSResource.SaveFailedMessage; }
                }
            });
            return message;

        }
        #endregion

        #region Delete
        /// <summary>
        /// soft delete da master
        /// </summary>
        /// <param name="pfRefNo"></param>
        /// <returns></returns>
        public string Delete(int pfRefNo)
        {
            string message = string.Empty;
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_DeleteGpfAdvanceReasons);
            database.AddInParameter(dbCommand, "@pfRefNo", DbType.Int32, pfRefNo);
            int i = 0;
            i = database.ExecuteNonQuery(dbCommand);
            if (i > 0)
            {
                message  = EPSResource.DeleteSuccessMessage; ;
            }
            else { message = EPSResource.DeleteFailedMessage; }

            return message;
        }
        #endregion




    }

}