﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Data;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class GpfDlisExceptionsDAL
    {
        private Database epsDataBase = null;

        public GpfDlisExceptionsDAL(Database database)
        {

            epsDataBase = database;
        }
        public async Task<int> InsertUpdateGpfDlisExceptions(GpfDlisExceptions objGpfDlisExceptions)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ODS].[usp_InsertUpdateGpfDlisExceptions]"))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfDlisExceptionsRefID", DbType.Int32, objGpfDlisExceptions.MsGpfDlisExceptionsRefID);
                  
                    epsDataBase.AddInParameter(dbCommand, "PfType", DbType.String, objGpfDlisExceptions.MsPfTypeId);
                    epsDataBase.AddInParameter(dbCommand, "RuleApplicableFromDate", DbType.DateTime, objGpfDlisExceptions.RuleApplicableFromDate);
                    epsDataBase.AddInParameter(dbCommand, "PfTypeValidTillDate", DbType.DateTime, objGpfDlisExceptions.PfTypeValidTillDate);
              
                    epsDataBase.AddInParameter(dbCommand, "MsPayCommID", DbType.String, objGpfDlisExceptions.MsPayCommId);
                   
                    epsDataBase.AddInParameter(dbCommand, "FromMsPayScaleID", DbType.String, objGpfDlisExceptions.FromMsPayScaleID);
                    epsDataBase.AddInParameter(dbCommand, "ToMsPayScaleID", DbType.String, objGpfDlisExceptions.ToMsPayScaleID);
                    epsDataBase.AddInParameter(dbCommand, "DlisAvgAmt", DbType.Int32, objGpfDlisExceptions.DlisAvgAmt);
                    epsDataBase.AddInParameter(dbCommand, "GpfRuleRefNo", DbType.String, objGpfDlisExceptions.GpfRuleRefNo);
                    epsDataBase.AddInParameter(dbCommand, "UserId", DbType.Int32, objGpfDlisExceptions.UserId);
                    epsDataBase.AddInParameter(dbCommand, "IPAddress", DbType.String, objGpfDlisExceptions.IPAddress);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
        public async Task<List<GpfDlisExceptions>> GetGpfDlisExceptions()
        {

            List<GpfDlisExceptions> GpfDlisExceptionsList = new List<GpfDlisExceptions>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ODS].[usp_GetGpfDlisExceptions]"))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            GpfDlisExceptions GpfDlisExceptions = new GpfDlisExceptions();
                            GpfDlisExceptions.MsGpfDlisExceptionsRefID = objReader["MsGpfDlisExceptionsRefID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGpfDlisExceptionsRefID"]) : 0;
                          
                            GpfDlisExceptions.MsPfTypeId = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : null;
                            GpfDlisExceptions.RuleApplicableFromDate = objReader["RuleApplicableFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicableFromDate"]) : GpfDlisExceptions.RuleApplicableFromDate = null;
                            GpfDlisExceptions.PfTypeValidTillDate = objReader["PfTypeValidTillDate"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDate"]) : GpfDlisExceptions.PfTypeValidTillDate = null;
                            GpfDlisExceptions.MsPayCommId = objReader["PayCommID"] != DBNull.Value ? Convert.ToString(objReader["PayCommID"]) : null;
                            GpfDlisExceptions.FromMsPayScaleID = objReader["FromMsPayScaleID"] != DBNull.Value ? Convert.ToString(objReader["FromMsPayScaleID"]) : null;
                            GpfDlisExceptions.ToMsPayScaleID = objReader["ToMsPayScaleID"] != DBNull.Value ? Convert.ToString(objReader["ToMsPayScaleID"]) : null;
                      
                            GpfDlisExceptions.DlisAvgAmt = objReader["DlisAvgAmt"] != DBNull.Value ? Convert.ToInt32(objReader["DlisAvgAmt"]) : 0;
                         
                           
                            GpfDlisExceptions.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() : null;
                            GpfDlisExceptions.ToPayScale = objReader["ToPayScale"] != DBNull.Value ? Convert.ToString(objReader["ToPayScale"]).Trim() : null;
                            GpfDlisExceptions.FromPayScale = objReader["FromPayScale"] != DBNull.Value ? Convert.ToString(objReader["FromPayScale"]).Trim() : null;
                            GpfDlisExceptions.PfDesc = objReader["PfDesc"] != DBNull.Value ? Convert.ToString(objReader["PfDesc"]).Trim() : null;
                            GpfDlisExceptions.PayCommDesc = objReader["PayCommDesc"] != DBNull.Value ? Convert.ToString(objReader["PayCommDesc"]).Trim() : null;
                            //GpfDlisExceptions.MsPayScaleID = objReader["MsPayScaleID"] != DBNull.Value ? Convert.ToString(objReader["MsPayScaleID"]).Trim() : null;
                            GpfDlisExceptionsList.Add(GpfDlisExceptions);
                        }

                    }

                }
            });

            if (GpfDlisExceptionsList == null)
            {
                return null;
            }
            else
            {
                return GpfDlisExceptionsList;
            }
        }
        public async Task<List<PfType>> GetPfType()
        {

            List<PfType> PfTypeList = new List<PfType>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ODS].[USP_GetPfType]"))
                {

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PfType Pftype = new PfType();


                            Pftype.PfTypes = objReader["PfTypes"] != DBNull.Value ? Convert.ToString(objReader["PfTypes"]).Trim() : null;

                            Pftype.PfDesc = objReader["PfDesc"] != DBNull.Value ? Convert.ToString(objReader["PfDesc"]).Trim() : null;

                            Pftype.MsPfTypeId = objReader["MsPfTypeId"] != DBNull.Value ? Convert.ToString(objReader["MsPfTypeId"]).Trim() : null;
                            PfTypeList.Add(Pftype);
                        }

                    }

                }
            });

            if (PfTypeList == null)
            {
                return null;
            }
            else
            {
                return PfTypeList;
            }
        }
        public async Task<List<PayScale>> GetPayScale(int PayCommId)
        {

            List<PayScale> PayScaleList = new List<PayScale>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ODS].[getPayScaleByPayCommId]"))
                {
                    epsDataBase.AddInParameter(dbCommand, "PayScalePscPayCommCD", DbType.Int32, PayCommId);

                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            PayScale PayScale = new PayScale();


                            PayScale.MsPayScaleID = objReader["MsPayScaleID"] != DBNull.Value ? Convert.ToString(objReader["MsPayScaleID"]).Trim() : null;

                            PayScale.PayScalePscScaleCD = objReader["PayScalePscScaleCD"] != DBNull.Value ? Convert.ToString(objReader["PayScalePscScaleCD"]).Trim() : null;
                            PayScaleList.Add(PayScale);
                        }

                    }

                }
            });

            if (PayScaleList == null)
            {
                return null;
            }
            else
            {
                return PayScaleList;
            }
        }
        public async Task<int> DeleteGpfDlisExceptions(int MsGpfDlisExceptionsRefID)
        {

            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand("[ODS].[usp_DeleteGpfDlisExceptions]"))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfDlisExceptionsRefID", DbType.Int32, MsGpfDlisExceptionsRefID);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);

                }
            });
            return result;

        }
    }
}
