﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class GpfRecoveryRulesDAL
    {
     private  Database epsDataBase = null;
        public GpfRecoveryRulesDAL(Database database)
        {
            epsDataBase = database;
        }
        /// <summary>
        /// InsertUpdateRecoveryRulesDetails
        /// </summary>
        /// <param name="objGpfWithdrawRules"></param>
        /// <returns></returns>
        public async Task<int> InsertUpdateRecoveryRulesDetails(GpfRecoveryRulesBM objGpfWithdrawRules)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_InsertUpdateGpfRecoveryRules))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfRecoveryID", DbType.Int32, objGpfWithdrawRules.MsGpfRecoveryID);
                    epsDataBase.AddInParameter(dbCommand, "PfType", DbType.String, objGpfWithdrawRules.PfType);
                    epsDataBase.AddInParameter(dbCommand, "RuleApplicableFromDate", DbType.DateTime, objGpfWithdrawRules.RuleApplicableFromDate);
                    epsDataBase.AddInParameter(dbCommand, "PfTypeValidTillDate", DbType.DateTime, objGpfWithdrawRules.RulesValidTillDate);
                    epsDataBase.AddInParameter(dbCommand, "GpfRuleRefNo", DbType.String, objGpfWithdrawRules.GpfRuleRefNo);
                    epsDataBase.AddInParameter(dbCommand, "minInstalmentst", DbType.String, objGpfWithdrawRules.minInstalmentst);
                    epsDataBase.AddInParameter(dbCommand, "maxInstalments", DbType.String, objGpfWithdrawRules.maxInstalmentst);
                    epsDataBase.AddInParameter(dbCommand, "@IpAddress", DbType.String, objGpfWithdrawRules.ipAddress);
                    epsDataBase.AddInParameter(dbCommand, "@oddInstallment", DbType.String, objGpfWithdrawRules.oddInstallment);                   
                    result = epsDataBase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;

        }
        /// <summary>
        /// GetGpfRecoveryRulesById
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <returns></returns>
        public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRulesById(int msGpfRecoveryID)
        {
            List<GpfRecoveryRulesBM> GpfRulesList = new List<GpfRecoveryRulesBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GetGpfRecoveryRulesByID))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfRecoveryID", DbType.Int32, msGpfRecoveryID);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {  
                            GpfRecoveryRulesBM gpfRulesData = new GpfRecoveryRulesBM();
                            gpfRulesData.MsGpfRecoveryID = objReader["MsGpfRecoveryID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGpfRecoveryID"]) : 0;
                            gpfRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : gpfRulesData.PfType = null;
                            gpfRulesData.RuleApplicableFromDate = objReader["RuleApplicableFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicableFromDate"]) : gpfRulesData.RuleApplicableFromDate = null;
                            gpfRulesData.RulesValidTillDate = objReader["PfTypeValidTillDate"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDate"]) : gpfRulesData.RulesValidTillDate = null;
                            gpfRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() : gpfRulesData.GpfRuleRefNo = null;
                            gpfRulesData.minInstalmentst = objReader["MinInst"] != DBNull.Value ? Convert.ToDecimal(objReader["MinInst"]) : 0;
                            gpfRulesData.maxInstalmentst = objReader["MaxInst"] != DBNull.Value ? Convert.ToDecimal(objReader["MaxInst"]) : 0;
                            gpfRulesData.oddInstallment = objReader["oddInstallment"] != DBNull.Value ? Convert.ToBoolean(objReader["oddInstallment"]) :false;
                            GpfRulesList.Add(gpfRulesData);
                        }
                    }
                }
            });
            return GpfRulesList;
        }
        /// <summary>
        /// GetGpfRecoveryRules
        /// </summary>
        /// <returns></returns>
        public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRules()
        {
            List<GpfRecoveryRulesBM> gpfRulesList = new List<GpfRecoveryRulesBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GetGpfRecoveryRules))
                {
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GpfRecoveryRulesBM gpfRulesData = new GpfRecoveryRulesBM();
                            gpfRulesData.MsGpfRecoveryID = objReader["MsGpfRecoveryID"] != DBNull.Value ? Convert.ToInt32(objReader["MsGpfRecoveryID"]) : 0;
                            gpfRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : gpfRulesData.PfType = null;
                            gpfRulesData.RuleApplicableFromDate = objReader["RuleApplicableFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicableFromDate"]) : gpfRulesData.RuleApplicableFromDate = null;
                            gpfRulesData.RulesValidTillDate = objReader["PfTypeValidTillDate"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDate"]) : gpfRulesData.RulesValidTillDate = null;
                            gpfRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() : gpfRulesData.GpfRuleRefNo = null;
                            gpfRulesData.minInstalmentst = objReader["MinInst"] != DBNull.Value ? Convert.ToDecimal(objReader["MinInst"]) : 0;
                            gpfRulesData.maxInstalmentst = objReader["MaxInst"] != DBNull.Value ? Convert.ToDecimal(objReader["MaxInst"]) : 0;
                            //gpfRulesData.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToString(objReader["IsActive"]) : gpfRulesData.IsActive = null;
                            gpfRulesList.Add(gpfRulesData);
                        }
                    }
                }
            });

            if (gpfRulesList == null)
                return null;
            else
                return gpfRulesList;
        }
        /// <summary>
        /// DeleteGpfRecoveryRules
        /// </summary>
        /// <param name="MsGpfRecoveryID"></param>
        /// <param name="IsFleg"></param>
        /// <returns></returns>
        public async Task<int> DeleteGpfRecoveryRules(int MsGpfRecoveryID)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_DeleteGpfRecoveryRules))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfRecoveryID", DbType.Int32, MsGpfRecoveryID);               
                    result = epsDataBase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;  
        }
    }
}
