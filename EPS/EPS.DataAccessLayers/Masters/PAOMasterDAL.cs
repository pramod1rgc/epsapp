﻿using System;
using System.Collections.Generic;
using System.Text;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using EPS.Repositories.Masters;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace EPS.DataAccessLayers.Masters
{
    public class PAOMasterDAL
    {

        Database epsdatabase = null;
        public PAOMasterDAL(Database database)
        {
            epsdatabase = database;
        }
        public PAOMasterDAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        #region Get MSPAO

        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails()
        {

            List<PAOMasterBM> MSPAOList = null;
            MSPAOList = new List<PAOMasterBM>();
            PAOMasterBM MSPAODatas;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UspMsPAO))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            MSPAODatas = new PAOMasterBM();
                            MSPAODatas.MsPAOID = objReader["MsPAOID"] != DBNull.Value ? Convert.ToString(objReader["MsPAOID"]) : null;
                            MSPAODatas.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            MSPAODatas.ControllerId = objReader["MsControllerID"] != DBNull.Value ? Convert.ToInt32(objReader["MsControllerID"]) : 0;
                            MSPAODatas.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]).Trim() : null;
                            MSPAODatas.PAOName = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() : null;
                            MSPAODatas.PAOAddress = objReader["PAOAddress"] != DBNull.Value ? Convert.ToString(objReader["PAOAddress"]).Trim() : null;
                            MSPAODatas.PAOLang = objReader["PAONameLang2"] != DBNull.Value ? Convert.ToString(objReader["PAONameLang2"]).Trim() : null;
                            MSPAODatas.PAODescription = objReader["PAOFavouring"] != DBNull.Value ? Convert.ToString(objReader["PAOFavouring"]) : null;
                            MSPAODatas.ContactNo = objReader["ContactNo"] != DBNull.Value ? Convert.ToString(objReader["ContactNo"]).Trim() : null;
                            MSPAODatas.EmailAddress = objReader["EmailAddress"] != DBNull.Value ? Convert.ToString(objReader["EmailAddress"]) : null;
                            MSPAODatas.PinCode = objReader["PINCODE"] != DBNull.Value ? Convert.ToInt32(objReader["PINCODE"]) : 0;
                            MSPAODatas.FaxNo = objReader["FaxNo"] != DBNull.Value ? Convert.ToString(objReader["FaxNo"]) : null;
                            MSPAODatas.StateName = objReader["StateName"] != DBNull.Value ? Convert.ToString(objReader["StateName"]) : null;
                            MSPAODatas.CityName = objReader["CityName"] != DBNull.Value ? Convert.ToString(objReader["CityName"]) : null;
                            MSPAODatas.CityId = objReader["MsCityid"] != DBNull.Value ? Convert.ToInt32(objReader["MsCityid"]) : 0;
                            MSPAODatas.StateId = objReader["MsStateID"] != DBNull.Value ? Convert.ToInt32(objReader["MsStateID"]) : 0;
                            MSPAOList.Add(MSPAODatas);

                        }

                    }
                }
            });
            return MSPAOList == null ? null : MSPAOList;
        }
        #endregion

        #region Get MSPAO Details by MspaoId

        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails(int MsPaoId)
        {

            List<PAOMasterBM> MSPAOList = new List<PAOMasterBM>();
            PAOMasterBM MSPAODatas;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UspMsPAO))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "SelectPao");
                    epsdatabase.AddInParameter(dbCommand, "MsPAOId", DbType.Int32, MsPaoId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            MSPAODatas = new PAOMasterBM();
                            MSPAODatas.MsPAOID = objReader["MsPAOID"] != DBNull.Value ? Convert.ToString(objReader["MsPAOID"]) : null;
                            MSPAODatas.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            MSPAODatas.ControllerId = objReader["MsControllerID"] != DBNull.Value ? Convert.ToInt32(objReader["MsControllerID"]) : 0;
                            MSPAODatas.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]).Trim() : null;
                            MSPAODatas.PAOName = objReader["PAOName"] != DBNull.Value ? Convert.ToString(objReader["PAOName"]).Trim() : null;
                            MSPAODatas.PAOAddress = objReader["PAOAddress"] != DBNull.Value ? Convert.ToString(objReader["PAOAddress"]).Trim() : null;
                            MSPAODatas.PAOLang = objReader["PAONameLang2"] != DBNull.Value ? Convert.ToString(objReader["PAONameLang2"]).Trim() : null;
                            MSPAODatas.PAODescription = objReader["PAOFavouring"] != DBNull.Value ? Convert.ToString(objReader["PAOFavouring"]) : null;
                            MSPAODatas.ContactNo = objReader["ContactNo"] != DBNull.Value ? Convert.ToString(objReader["ContactNo"]).Trim() : null;
                            MSPAODatas.EmailAddress = objReader["EmailAddress"] != DBNull.Value ? Convert.ToString(objReader["EmailAddress"]) : null;
                            MSPAODatas.StateId = objReader["StateId"] != DBNull.Value ? Convert.ToInt32(objReader["StateId"]) : 0;
                            MSPAODatas.CityId = objReader["CityId"] != DBNull.Value ? Convert.ToInt32(objReader["CityId"]) : 0;
                            MSPAOList.Add(MSPAODatas);

                        }

                    }
                }
            });
            return MSPAOList;
        }
        #endregion



        #region Update Master MSPAO

        public async Task<string> UpdateMSPAO(PAOMasterBM PAOMasterBMObj)
        {
            string Response = null;
            if (PAOMasterBMObj != null)
            {
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UspMsPAO))
                    {
                        epsdatabase.AddInParameter(dbCommand, "Cityid", DbType.Int32, PAOMasterBMObj.CityId);
                        epsdatabase.AddInParameter(dbCommand, "PhoneNo", DbType.String, PAOMasterBMObj.ContactNo);
                        epsdatabase.AddInParameter(dbCommand, "Email", DbType.String, CommonFunctions.TrimString(PAOMasterBMObj.EmailAddress));
                        epsdatabase.AddInParameter(dbCommand, "FaxNo", DbType.String, CommonFunctions.TrimString(PAOMasterBMObj.FaxNo));
                        epsdatabase.AddInParameter(dbCommand, "Stateid", DbType.Int32, PAOMasterBMObj.StateId);
                        epsdatabase.AddInParameter(dbCommand, "Address", DbType.String, CommonFunctions.TrimString(PAOMasterBMObj.PAOAddress));
                        epsdatabase.AddInParameter(dbCommand, "Pincode", DbType.String, PAOMasterBMObj.PinCode);
                        epsdatabase.AddInParameter(dbCommand, "ControllerId", DbType.Int32, PAOMasterBMObj.ControllerId);
                        epsdatabase.AddInParameter(dbCommand, "paocode", DbType.String, CommonFunctions.TrimString(PAOMasterBMObj.PAOCode));
                         epsdatabase.AddInParameter(dbCommand, "paoDescription", DbType.String, PAOMasterBMObj.PAODescription);
                        epsdatabase.AddInParameter(dbCommand, "paoDescriptionBilingual", DbType.String, PAOMasterBMObj.PAOLang);
                        epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "update");
                        epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                        epsdatabase.ExecuteNonQuery(dbCommand);
                        Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                    }
                });
            }
            return Response;

        }
        #endregion

        #region Insert Master MSPAO

        public async Task<string> InsertMSPAO(PAOMasterBM PAOMasterBMObj)
        {
            string response = null;
            if (PAOMasterBMObj != null)
            {
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UspMsPAO))
                    {
                        epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "insert");
                        epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                        epsdatabase.ExecuteNonQuery(dbCommand);
                        response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                    }
                });
            }
            return response;

        }
        #endregion

        #region Delete Master MSPAO

        public async Task<string> DeleteMSPAO(int MsMSPAOId)
        {
            string response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UspMsPAO))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsPAOId", DbType.Int32, MsMSPAOId);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "delete");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            });
            return response;

        }
        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called

            // If disposing equals true, dispose all managed and unmanaged resources 
            if (disposing)
            {
                //context.Dispose();
                // Free any other managed objects here

                epsdatabase = null;
            }


        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~PAOMasterDAL()
        {
            Dispose(false);
        }
    }
}

