﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class PraoMasterDAL
    {
        Database epsdatabase = null;
        private static IHttpContextAccessor accessor;
        public PraoMasterDAL(Database database)
        {           
            epsdatabase = database;
        }

        public async Task<IEnumerable<PraoMasterModel>> GetAllController()
        {
            List<PraoMasterModel> ProaModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetControllerCD))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, "1");
                    epsdatabase.AddInParameter(dbCommand, "ControllerCode", DbType.String, "");
                    epsdatabase.AddInParameter(dbCommand, "StateID", DbType.Int16, null);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PraoMasterModel ObjModel = new PraoMasterModel();
                            ObjModel.ControllerID = Convert.ToInt32(objReader["ControllerID"]);
                            ObjModel.ControllerCode = Convert.ToString(objReader["ControllerCode"]);
                            ObjModel.ControllerName = Convert.ToString(objReader["ControllerName"]);
                            ProaModel.Add(ObjModel);
                        }
                    }
                }
            });
            if (ProaModel.Count == 0)
                return null;
            else
                return ProaModel;
        }

        public async Task<IEnumerable<PraoMasterModel>> BindCtrlName(string CtrlrCode)
        {
            List<PraoMasterModel> ProaModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetControllerCD))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, "2");
                    epsdatabase.AddInParameter(dbCommand, "ControllerCode", DbType.String, CtrlrCode);
                    epsdatabase.AddInParameter(dbCommand, "StateID", DbType.Int16, null);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PraoMasterModel ObjModel = new PraoMasterModel();
                            ObjModel.ControllerID = Convert.ToInt32(objReader["ControllerID"]);
                            ObjModel.ControllerName = Convert.ToString(objReader["ControllerName"]);
                            ObjModel.ControllerCode = Convert.ToString(objReader["ControllerCode"]);
                            ObjModel.StateID = objReader["StateID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["StateID"]);
                            ObjModel.cityID = objReader["cityID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["cityID"]);
                            if (ObjModel.StateID == 0)
                            {
                                ObjModel.StateID = null;
                            }
                            if (ObjModel.cityID == 0)
                            {
                                ObjModel.cityID = null;
                            }
                            ProaModel.Add(ObjModel);
                        }
                    }
                }
            });
            if (ProaModel.Count == 0)
                return null;
            else
                return ProaModel;
        }

        public async Task<IEnumerable<PraoMasterModel>> BindState()
        {
            List<PraoMasterModel> ProaModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetControllerCD))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, "3");
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PraoMasterModel ObjModel = new PraoMasterModel();
                            ObjModel.StateID = objReader["StateID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["StateID"]);
                            ObjModel.statecode = Convert.ToString(objReader["statecode"]);
                            ObjModel.StateName = Convert.ToString(objReader["StateName"]);
                            ProaModel.Add(ObjModel);
                        }
                    }
                }
            });
            if (ProaModel.Count == 0)
                return null;
            else
                return ProaModel;
        }



        public async Task<IEnumerable<PraoMasterModel>> BindDistrict(int stateId)
        {
            List<PraoMasterModel> ProaModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetControllerCD))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, "4");
                    epsdatabase.AddInParameter(dbCommand, "ControllerCode", DbType.String, "");
                    epsdatabase.AddInParameter(dbCommand, "StateID", DbType.Int16, stateId);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PraoMasterModel ObjModel = new PraoMasterModel();
                            ObjModel.cityID = objReader["cityID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["cityID"]);
                            ObjModel.DistrictStateID = objReader["DistrictStateID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["DistrictStateID"]);
                            ObjModel.DistrictName = Convert.ToString(objReader["DistrictName"]);
                            ProaModel.Add(ObjModel);
                        }
                    }
                }
            });
            if (ProaModel.Count == 0)
                return null;
            else
                return ProaModel;
        }
        public async Task<int> SaveOrUpdate(PraoMasterModel model)
        {
            int result = 0;
            List<PraoMasterModel> ObjPraoModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdatePraoMaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.Int16, model.Mode);
                    epsdatabase.AddInParameter(dbCommand, "ControllerID", DbType.Int16, model.ControllerID);
                    epsdatabase.AddInParameter(dbCommand, "ControllerName ", DbType.String, model.ControllerName);
                    epsdatabase.AddInParameter(dbCommand, "ControllerStatusId ", DbType.Int16, model.ControllerStatusId);

                    epsdatabase.AddInParameter(dbCommand, "ControllerCode", DbType.String, model.ControllerCode);
                    epsdatabase.AddInParameter(dbCommand, "Address", DbType.String, model.Address);

                    epsdatabase.AddInParameter(dbCommand, "ModifiedBy", DbType.String, model.IpAddress);
                    epsdatabase.AddInParameter(dbCommand, "ModifiedDate", DbType.DateTime, DateTime.Today);
                    epsdatabase.AddInParameter(dbCommand, "ModifiedIP", DbType.String, model.IpAddress);

                    epsdatabase.AddInParameter(dbCommand, "Pincode", DbType.String, model.Pincode);
                    epsdatabase.AddInParameter(dbCommand, "PhoneNo", DbType.String, model.PhoneNo);
                    epsdatabase.AddInParameter(dbCommand, "FaxNo", DbType.String, model.FaxNo);
                    epsdatabase.AddInParameter(dbCommand, "Email", DbType.String, model.Email);
                    epsdatabase.AddInParameter(dbCommand, "StateID", DbType.String, model.StateID);
                    epsdatabase.AddInParameter(dbCommand, "cityID", DbType.Int16, model.cityID);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });

            return result;
        }


        public async Task<IEnumerable<ProMasterDetails>> BindPraoMasterDetails()
        {
            List<ProMasterDetails> ObjProMaster = new List<ProMasterDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPraoMasterDetails))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ProMasterDetails ObjModel = new ProMasterDetails();

                            ObjModel.ControllerID = objReader["ControllerID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["ControllerID"]);
                            ObjModel.ControllerCode = Convert.ToString(objReader["ControllerCode"]);
                            ObjModel.ControllerName = Convert.ToString(objReader["ControllerName"]);
                            ObjModel.PhoneNo = Convert.ToString(objReader["PhoneNo"]);
                            ObjModel.Email = Convert.ToString(objReader["Email"]);
                            ObjModel.StateName = Convert.ToString(objReader["StateName"]);
                            ObjModel.CityName = Convert.ToString(objReader["CityName"]);
                            ObjModel.StateID = objReader["StateID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["StateID"]);
                            ObjModel.cityID = objReader["cityID"] == DBNull.Value ? 0 : Convert.ToInt16(objReader["cityID"]);

                            if (ObjModel.StateID == 0)
                            {
                                ObjModel.StateID = null;
                            }
                            if (ObjModel.cityID == 0)
                            {
                                ObjModel.cityID = null;
                            }

                            ObjProMaster.Add(ObjModel);
                        }
                    }
                }
            });
            if (ObjProMaster.Count == 0)
                return null;
            else
                return ObjProMaster;
        }


        public async Task<IEnumerable<PraoMasterModel>> ViewEditOrDeletePrao(string CtrlCode, string controllerID, int Mode)
        {
            List<PraoMasterModel> ProaModel = new List<PraoMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateDeletePraoDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "Mode", DbType.Int16, Mode);
                    epsdatabase.AddInParameter(dbCommand, "ControllerID", DbType.Int16, controllerID);
                    epsdatabase.AddInParameter(dbCommand, "ControllerCode", DbType.String, CtrlCode);

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PraoMasterModel ObjModel = new PraoMasterModel();

                            ObjModel.IsActive = objReader["IsActive"] == DBNull.Value ? 0 : Convert.ToInt32(objReader["IsActive"]);

                            ObjModel.ControllerID = objReader["ControllerID"] == DBNull.Value ? 0 : Convert.ToInt32(objReader["ControllerID"]);
                            ObjModel.ControllerCode = Convert.ToString(objReader["ControllerCode"]);
                            ObjModel.ControllerName = Convert.ToString(objReader["ControllerName"]);

                            if (ObjModel.IsActive == 1)
                            {
                                ObjModel.Address = Convert.ToString(objReader["Address"]);
                                 if ((objReader["Pincode"]) != DBNull.Value)
                                {
                                    Convert.ToInt32(objReader["Pincode"]);
                                }
                                ObjModel.PhoneNo = Convert.ToString(objReader["PhoneNo"]);
                                ObjModel.FaxNo = Convert.ToString(objReader["FaxNo"]);
                                ObjModel.Email = Convert.ToString(objReader["Email"]);
                                ObjModel.StateID = objReader["StateID"] == DBNull.Value ? 0 : Convert.ToInt32(objReader["StateID"]);
                                ObjModel.cityID = objReader["cityID"] == DBNull.Value ? 0 : Convert.ToInt32(objReader["cityID"]);

                                if (ObjModel.StateID == 0)
                                {
                                    ObjModel.StateID = null;
                                }
                                if (ObjModel.cityID == 0)
                                {
                                    ObjModel.cityID = null;
                                }
                            }
                            ProaModel.Add(ObjModel);
                        }
                    }
                }
            });
            if (ProaModel.Count == 0)
                return null;
            else
                return ProaModel;
        }
    }
}

