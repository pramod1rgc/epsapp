﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class GpfAdvanceRulesDAL
    {
      private  Database epsDataBase = null;
        public GpfAdvanceRulesDAL(Database database)
        {
            epsDataBase = database;
        }
        /// <summary>
        /// objGpfWithdrawRules
        /// </summary>
        /// <param name="objGpfWithdrawRules"></param>
        /// <returns></returns>
        public async Task<int> InsertUpdateAdvanceRulesDetails(GpfAdvanceRulesBM objGpfWithdrawRules)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_InsertUpdateGpfRulesRef))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfAdvanceRulesRefID", DbType.Int32, objGpfWithdrawRules.MsGpfAdvanceRulesRefID);           
                    epsDataBase.AddInParameter(dbCommand, "PfType", DbType.String, objGpfWithdrawRules.PfType);
                    epsDataBase.AddInParameter(dbCommand, "RuleApplicableFromDate", DbType.DateTime, objGpfWithdrawRules.RuleApplicableFromDate);
                    epsDataBase.AddInParameter(dbCommand, "PfTypeValidTillDate", DbType.DateTime, objGpfWithdrawRules.RulesValidTillDate);
                    epsDataBase.AddInParameter(dbCommand, "GpfRuleRefNo", DbType.String, objGpfWithdrawRules.GpfRuleRefNo);
                    epsDataBase.AddInParameter(dbCommand, "MonthsBeforeRetirement", DbType.String, objGpfWithdrawRules.MonthsBeforeRetirement);
                    epsDataBase.AddInParameter(dbCommand, "@IpAddress", DbType.String, objGpfWithdrawRules.ipAddress);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        /// <summary>
        /// GetGpfAdvanceRulesById
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <returns></returns>
        public async Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRulesById(int MsGpfWithdrawRulesRefID)
        {
            List<GpfAdvanceRulesBM> GpfRulesList = new List<GpfAdvanceRulesBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GetGpfAdvanceRulesByID))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfWithdrawRulesRefID", DbType.Int32, MsGpfWithdrawRulesRefID);
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GpfAdvanceRulesBM GpfAdvanceRulesData = new GpfAdvanceRulesBM();
                            GpfAdvanceRulesData.MsGpfAdvanceRulesRefID = objReader["GpfAdvRulesRefId"] != DBNull.Value ? Convert.ToInt32(objReader["GpfAdvRulesRefId"]) : 0;
                            GpfAdvanceRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : GpfAdvanceRulesData.PfType = null;
                            GpfAdvanceRulesData.RuleApplicableFromDate = objReader["RuleApplicablefromDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicablefromDt"]) : GpfAdvanceRulesData.RuleApplicableFromDate = null;
                            GpfAdvanceRulesData.RulesValidTillDate = objReader["PfTypeValidTillDt"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDt"]) : GpfAdvanceRulesData.RulesValidTillDate = null;
                            GpfAdvanceRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() : GpfAdvanceRulesData.GpfRuleRefNo = null;
                            GpfAdvanceRulesData.MonthsBeforeRetirement = objReader["NoOfMonBeforeSuperann"] != DBNull.Value ? Convert.ToInt32(objReader["NoOfMonBeforeSuperann"]): 0;
                            GpfRulesList.Add(GpfAdvanceRulesData);
                        }
                    }
                }
            });
            return GpfRulesList;
        }
        /// <summary>
        /// GetGpfAdvanceRules
        /// </summary>
        /// <returns></returns>
        public async Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRules()
        {
            List<GpfAdvanceRulesBM> GpfRulesList = new List<GpfAdvanceRulesBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_GetGpfAdvanceRules))
                {
                    using (IDataReader objReader = epsDataBase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GpfAdvanceRulesBM GpfRulesData = new GpfAdvanceRulesBM();
                            GpfRulesData.MsGpfAdvanceRulesRefID = objReader["GpfAdvRulesRefId"] != DBNull.Value ? Convert.ToInt32(objReader["GpfAdvRulesRefId"]) : 0;          
                            GpfRulesData.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() : GpfRulesData.PfType = null;
                            GpfRulesData.RuleApplicableFromDate = objReader["RuleApplicablefromDt"] != DBNull.Value ? Convert.ToDateTime(objReader["RuleApplicablefromDt"]) : GpfRulesData.RuleApplicableFromDate = null;
                            GpfRulesData.RulesValidTillDate = objReader["PfTypeValidTillDt"] != DBNull.Value ? Convert.ToDateTime(objReader["PfTypeValidTillDt"]) : GpfRulesData.RulesValidTillDate = null;
                            GpfRulesData.GpfRuleRefNo = objReader["GpfRuleRefNo"] != DBNull.Value ? Convert.ToString(objReader["GpfRuleRefNo"]).Trim() : GpfRulesData.GpfRuleRefNo = null;
                            GpfRulesData.MonthsBeforeRetirement = objReader["NoOfMonBeforeSuperann"] != DBNull.Value ? Convert.ToInt32(objReader["NoOfMonBeforeSuperann"]) : GpfRulesData.MonthsBeforeRetirement = 0;
                            GpfRulesData.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToString(objReader["IsActive"]) : GpfRulesData.IsActive = null;
                            GpfRulesList.Add(GpfRulesData);
                        }
                    }
                }
            });
            if (GpfRulesList == null)
                return null;
            else
                return GpfRulesList;
        }
        /// <summary>
        /// DeleteGpfAdvanceRules
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <param name="IsFleg"></param>
        /// <returns></returns>
        public async Task<int> DeleteGpfAdvanceRules(int MsGpfWithdrawRulesRefID,string IsFleg)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsDataBase.GetStoredProcCommand(EPSConstant.usp_DeleteGpfAdvanceRules))
                {
                    epsDataBase.AddInParameter(dbCommand, "MsGpfAdvanceRulesRefID", DbType.Int32, MsGpfWithdrawRulesRefID);
                    epsDataBase.AddInParameter(dbCommand, "IsFleg", DbType.String, IsFleg);
                    result = epsDataBase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
