﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    public class CityMaster_DAL
    {
        Database epsdatabase = null;

        public CityMaster_DAL(Database database)
        {
            epsdatabase = database;
        }

        public async Task<List<CitymasterModel>> GetCitymasterlist()
        {

            List<CitymasterModel> objCitymasterList = new List<CitymasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCityClassMasterList))
                {

                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {

                            CitymasterModel objCitymasterModel = new CitymasterModel();
                            objCitymasterModel.msCityclassID = objReader["MsCityclassID"] != DBNull.Value ? Convert.ToInt32(objReader["MsCityclassID"]) : objCitymasterModel.msCityclassID = 0;
                            objCitymasterModel.CclPaycommId = objReader["CclPaycommId"] != DBNull.Value ? Convert.ToInt32(objReader["CclPaycommId"]) : objCitymasterModel.CclPaycommId = 0;
                            objCitymasterModel.CclCityclass = objReader["CclCityclass"] != DBNull.Value ? Convert.ToString(objReader["CclCityclass"]).Trim() : objCitymasterModel.CclCityclass = null;
                            objCitymasterModel.PayCommDesc = objReader["PayCommDesc"] != DBNull.Value ? Convert.ToString(objReader["PayCommDesc"]) : objCitymasterModel.PayCommDesc = null;


                            objCitymasterList.Add(objCitymasterModel);


                        }

                    }

                }
            }).ConfigureAwait(true);

            if (objCitymasterList == null)
                return null;
            else
                return objCitymasterList;
        }

        /// <summary>
        /// Delete Dues Defination record
        /// </summary>
        /// <param name="DuesCd"></param>
        /// <returns></returns>
        public async Task<int> DeleteCityClass(int DuesCd)
        {

            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteCityClassMaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "msCityClassId", DbType.Int32, DuesCd);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);

                }
            }).ConfigureAwait(true);

                return result > 0 ? (int)EPSEnum.Response.Delete : (int)EPSEnum.Response.Error;
        }

        public async Task<int> SaveCityClassmasterDetails(CitymasterModel obj)
        {

            int result = 0;
            await Task.Run(() =>
            {



                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpsertCityclassMaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "msclasscityId", DbType.Int32, obj.msCityclassID);
                    epsdatabase.AddInParameter(dbCommand, "CclCityClass", DbType.String, obj.CclCityclass);
                    epsdatabase.AddInParameter(dbCommand, "cclpayComid", DbType.String, obj.CclPaycommId);
                    epsdatabase.AddInParameter(dbCommand, "UserName", DbType.String, obj.UserName);
                    epsdatabase.AddInParameter(dbCommand, "UserIp", DbType.String, obj.UserIp);

                    result = Convert.ToInt32(epsdatabase.ExecuteNonQuery(dbCommand));
                }
            }).ConfigureAwait(true);

            if (result > 0 && obj.msCityclassID > 0)
            {
                //return result > 0 ? "Record has been updated successfully." : "Something went wrong";
                return result > 0 ? (int)EPSEnum.Response.Update : (int)EPSEnum.Response.Error;
            }
            else
            {
                //return result > 0 ? "Records has been saved successfully" : "Something went wrong";
                return result > 0 ? (int)EPSEnum.Response.Insert : (int)EPSEnum.Response.AlreadyExist;
            }
        }


        /// <summary>
        /// CheckRecord Exits in CityMaster
        /// </summary>
        /// <param name="payCommid"></param>
        /// <param name="cityClass"></param>
        /// <returns></returns>
        public async Task<int> CheckRecordAlredyExitsOrNotInCityMaster(CitymasterModel obj)
        {
            int Result = 0;

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_CheckRecordExitsorNotinCityClassmaster))
                {
                    epsdatabase.AddInParameter(dbCommand, "cityClass", DbType.String, obj.CclCityclass);
                    epsdatabase.AddInParameter(dbCommand, "PayCommid", DbType.Int32, obj.CclPaycommId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        if (objReader.Read())
                        {
                            Result = objReader["Recordcheck"] != DBNull.Value ? Convert.ToInt32(objReader["Recordcheck"]) : Result = 0;
                        }

                    }

                }
            }).ConfigureAwait(true);

            return Result;


        }

    }
}
