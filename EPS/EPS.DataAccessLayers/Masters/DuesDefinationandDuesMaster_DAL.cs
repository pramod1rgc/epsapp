﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Masters
{
    /// <summary>
    /// Dues Details
    /// </summary>
    public class DuesDefinationandDuesMaster_DAL
    {
        private Database epsdatabase = null;

        public DuesDefinationandDuesMaster_DAL(Database database)
        {
            this.epsdatabase = database;
        }

        /// <summary>
        /// Insert Dues Defination Record
        /// </summary>
        /// <param name="objDuesDefinationandDuesMaster"></param>
        /// <returns></returns>
        public async Task<int> InsertUpdateDuesDefinationDetails(DuesDefinationandDuesMasterModel objDuesDefinationandDuesMaster)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_InsertUpdateDuesDifination))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsPayItemsID", DbType.Int32, objDuesDefinationandDuesMaster.MsPayItemsID);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsCD", DbType.Int32, objDuesDefinationandDuesMaster.PayItemsCD);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsName", DbType.String, objDuesDefinationandDuesMaster.PayItemsName);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsNameShort", DbType.String, objDuesDefinationandDuesMaster.PayItemsNameShort);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsComputable", DbType.Int16, objDuesDefinationandDuesMaster.PayItemsComputable);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsIsTaxable", DbType.Int16, objDuesDefinationandDuesMaster.PayItemsIsTaxable);
                    epsdatabase.AddInParameter(dbCommand, "PayItemsWef", DbType.DateTime, objDuesDefinationandDuesMaster.withEffectFrom); 
                    epsdatabase.AddInParameter(dbCommand, "PayItemsUpdDt", DbType.DateTime, objDuesDefinationandDuesMaster.withEffectTo);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            }).ConfigureAwait(true);
            return result;
        }

        public async Task<List<DuesDefinationandDuesMasterModel>> GetDuesDefinationmaster()
        {
            List<DuesDefinationandDuesMasterModel> objDuesDefinationandDuesMasterModelList = new List<DuesDefinationandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetDuesDefinationmasterList))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesDefinationandDuesMasterModel objDuesDefinationandDuesMasterModel = NewMethod(objReader);
                            objDuesDefinationandDuesMasterModel.PayItemsCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : objDuesDefinationandDuesMasterModel.PayItemsCD = 0;
                            objDuesDefinationandDuesMasterModel.PayItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]) : objDuesDefinationandDuesMasterModel.PayItemsName = null;
                            objDuesDefinationandDuesMasterModel.PayItemsNameShort = objReader["PayItemsNameShort"] != DBNull.Value ? Convert.ToString(objReader["PayItemsNameShort"]) : objDuesDefinationandDuesMasterModel.PayItemsNameShort = null;
                            // objDuesDefinationandDuesMasterModel.PayItemsWef = objReader["PayItemsWef"] != DBNull.Value ? Convert.ToDateTime(objReader["PayItemsWef"]) : objDuesDefinationandDuesMasterModel.PayItemsWef = null;
                            objDuesDefinationandDuesMasterModel.withEffectFrom = objReader["withEffectFrom"] != DBNull.Value ? Convert.ToDateTime(objReader["withEffectFrom"]) : objDuesDefinationandDuesMasterModel.withEffectFrom = null;
                            objDuesDefinationandDuesMasterModel.withEffectTo = objReader["withEffectTo"] != DBNull.Value ? Convert.ToDateTime(objReader["withEffectTo"]) : objDuesDefinationandDuesMasterModel.withEffectFrom = null;
                            objDuesDefinationandDuesMasterModel.PayItemsComputable = objReader["PayItemsComputable"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsComputable"]) : objDuesDefinationandDuesMasterModel.PayItemsComputable = 0;
                            objDuesDefinationandDuesMasterModel.PayItemsIsTaxable = objReader["PayItemsIsTaxable"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsIsTaxable"]) : objDuesDefinationandDuesMasterModel.PayItemsIsTaxable = 0;
                            objDuesDefinationandDuesMasterModelList.Add(objDuesDefinationandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(false);

            if (objDuesDefinationandDuesMasterModelList == null)
            {
                return null;
            }
            else
            {
                return objDuesDefinationandDuesMasterModelList;
            }
        }

        private static DuesDefinationandDuesMasterModel NewMethod(IDataReader objReader)
        {
            DuesDefinationandDuesMasterModel objDuesDefinationandDuesMasterModel = new DuesDefinationandDuesMasterModel();
            objDuesDefinationandDuesMasterModel.MsPayItemsID = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : objDuesDefinationandDuesMasterModel.MsPayItemsID = 0;
            return objDuesDefinationandDuesMasterModel;
        }

        /// <summary>
        /// Delete Dues Defination record
        /// </summary>
        /// <param name="DuesCd"></param>
        /// <returns></returns>
        public async Task<int> DeleteEmployeeDetails(string DuesCd)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeleteDuesDefination))
                {
                    epsdatabase.AddInParameter(dbCommand, "PayItemsCD", DbType.Int32, DuesCd);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            }).ConfigureAwait(true);
            return result;
        }

        /// <summary>
        /// Get DuesDefinationandDuesMasterByID
        /// </summary>
        /// <param name="DuesCD"></param>
        /// <returns></returns>
        public async Task<DuesDefinationandDuesMasterModel> GetDuesDefinationandDuesMasterByID(string duesCD)
        {
            DuesDefinationandDuesMasterModel ObjDuesDefinationandDuesMasterModel = new DuesDefinationandDuesMasterModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetDuesDefinationmasterById))
                {
                    epsdatabase.AddInParameter(dbCommand, "DuesCd", DbType.String, duesCD);
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        if (objReader.Read())
                        {
                            ObjDuesDefinationandDuesMasterModel.MsPayItemsID = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : 0;
                            ObjDuesDefinationandDuesMasterModel.PayItemsCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : 0;

                            ObjDuesDefinationandDuesMasterModel.PayItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]).Trim() : null;
                            ObjDuesDefinationandDuesMasterModel.PayItemsNameShort = objReader["PayItemsNameShort"] != DBNull.Value ? Convert.ToString(objReader["PayItemsNameShort"]).Trim() : null;

                            ObjDuesDefinationandDuesMasterModel.PayItemsIsTaxable = objReader["PayItemsIsTaxable"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsIsTaxable"]) : 0;
                            ObjDuesDefinationandDuesMasterModel.PayItemsComputable = objReader["PayItemsComputable"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsComputable"]) : 0;
                            ObjDuesDefinationandDuesMasterModel.PayItemsWef = objReader["PayItemsWef"] != DBNull.Value ? Convert.ToDateTime(objReader["PayItemsWef"]) : ObjDuesDefinationandDuesMasterModel.PayItemsWef = null;
                        }

                    }

                }
            }).ConfigureAwait(true);
            return ObjDuesDefinationandDuesMasterModel;
        }

        public async Task<DuesDefinationandDuesMasterModel> GetAutoGenratedDuesCode()
        {
            DuesDefinationandDuesMasterModel ObjDuesDefinationandDuesMasterModel = new DuesDefinationandDuesMasterModel();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetAutoGenratedDuesCode))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            ObjDuesDefinationandDuesMasterModel.PayItemsCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : ObjDuesDefinationandDuesMasterModel.PayItemsCD = 0;
                        }

                    }

                }
            }).ConfigureAwait(true);
            return ObjDuesDefinationandDuesMasterModel;
        }

        //===================DuesReteMaster============================================================
        public async Task<List<DuesRateandDuesMasterModel>> GetOrgnazationTypeForDues()
        {
            List<DuesRateandDuesMasterModel> objlISTOrganizationTypeModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetOrgnazationTypeForDues))
                {

                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel objOrganizationTypeModel = new DuesRateandDuesMasterModel();

                            objOrganizationTypeModel.OrganizationType = objReader["Code"] != DBNull.Value ? objReader["Code"].ToString() : objOrganizationTypeModel.OrganizationType = null;
                            objOrganizationTypeModel.Description = objReader["Description"] != DBNull.Value ? objReader["Description"].ToString() : objOrganizationTypeModel.Description = null;

                            objlISTOrganizationTypeModel.Add(objOrganizationTypeModel);

                        }
                    }
                }
            }).ConfigureAwait(true);

            return objlISTOrganizationTypeModel;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetDuesCodeDuesDefination()
        {
            List<DuesRateandDuesMasterModel> ObjlistDuesDefinationandDuesMasterModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetDuesCodeandDescrptionforDuesRate))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel ObjDuesDefinationandDuesMasterModel = new DuesRateandDuesMasterModel();
                            ObjDuesDefinationandDuesMasterModel.DuesCodeDuesDefination = objReader["MsPayItemsID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayItemsID"]) : ObjDuesDefinationandDuesMasterModel.DuesCodeDuesDefination = 0;
                            ObjDuesDefinationandDuesMasterModel.PayItemsName = objReader["DuesCodeDuesDefination"] != DBNull.Value ? objReader["DuesCodeDuesDefination"].ToString() : ObjDuesDefinationandDuesMasterModel.PayItemsName = null;
                            ObjDuesDefinationandDuesMasterModel.PayRatesPayItemCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : ObjDuesDefinationandDuesMasterModel.PayRatesPayItemCD = 0;
                            ObjlistDuesDefinationandDuesMasterModel.Add(ObjDuesDefinationandDuesMasterModel);

                        }
                    }
                }
            }).ConfigureAwait(true);

            return ObjlistDuesDefinationandDuesMasterModel;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetCityClassForDuesRate(string payCommId)
        {
            List<DuesRateandDuesMasterModel> objlistCityClassModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetCityClassForDuesRate))
                {
                    epsdatabase.AddInParameter(dbCommand, "payComId", DbType.String, payCommId);
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel objCityClassModel = new DuesRateandDuesMasterModel();
                            objCityClassModel.CityClass = objReader["CclPaycommId"] != DBNull.Value ? Convert.ToInt32(objReader["CclPaycommId"]) : 0;
                            objCityClassModel.PayCityClass = objReader["CclCityclass"] != DBNull.Value ? objReader["CclCityclass"].ToString() : objCityClassModel.PayCityClass = null;
                            objCityClassModel.MsCityclassID = objReader["MsCityclassID"] != DBNull.Value ? Convert.ToInt32(objReader["MsCityclassID"]) : 0;

                            objlistCityClassModel.Add(objCityClassModel);
                        }
                    }
                }
            }).ConfigureAwait(true);

            return objlistCityClassModel;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetStateForDuesRate()
        {
            List<DuesRateandDuesMasterModel> ObjListstateModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_getStatenameforDuesMaster))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel ObjstateModel = new DuesRateandDuesMasterModel();
                            ObjstateModel.state = objReader["StateCode"] != DBNull.Value ? objReader["StateCode"].ToString() : ObjstateModel.state = null;
                            ObjstateModel.StateName = objReader["StateName"] != DBNull.Value ? objReader["StateName"].ToString() : ObjstateModel.StateName = null;

                            ObjListstateModel.Add(ObjstateModel);
                        }

                    }
                }
            }).ConfigureAwait(true);

            return ObjListstateModel;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetPayCommForDuesRate()
        {
            List<DuesRateandDuesMasterModel> ObjListDuesRateandDuesMasterModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetPatCommForDuesRate))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel ObjDuesRateandDuesMasterModel = new DuesRateandDuesMasterModel();
                            ObjDuesRateandDuesMasterModel.PayCommission = objReader["MsPayCommID"] != DBNull.Value ? Convert.ToInt32(objReader["MsPayCommID"]) : ObjDuesRateandDuesMasterModel.PayCommission = 0;
                            ObjDuesRateandDuesMasterModel.PayCommDesc = objReader["PayCommDesc"] != DBNull.Value ? objReader["PayCommDesc"].ToString() : ObjDuesRateandDuesMasterModel.PayCommDesc = null;

                            ObjListDuesRateandDuesMasterModel.Add(ObjDuesRateandDuesMasterModel);
                        }
                    }
                }
            }).ConfigureAwait(true);

            return ObjListDuesRateandDuesMasterModel;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetSlabtypeByPayCommId(string payCommId)
        {
            List<DuesRateandDuesMasterModel> ObjListDuesRateandDuesMasterModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetSlabTypebyPayCommIdForDuesRate))
                {
                    epsdatabase.AddInParameter(dbCommand, "payCommID", DbType.String, payCommId);
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel ObjDuesRateandDuesMasterModel = new DuesRateandDuesMasterModel();
                            ObjDuesRateandDuesMasterModel.SlabType = objReader["MsSlabTypeID"] != DBNull.Value ? Convert.ToInt32(objReader["MsSlabTypeID"]) : ObjDuesRateandDuesMasterModel.SlabType = 0;
                            ObjDuesRateandDuesMasterModel.MsSlabTypeDes = objReader["SlabType"] != DBNull.Value ? objReader["SlabType"].ToString() : ObjDuesRateandDuesMasterModel.MsSlabTypeDes = null;
                            ObjListDuesRateandDuesMasterModel.Add(ObjDuesRateandDuesMasterModel);
                        }

                    }

                }
            }).ConfigureAwait(true);

            return ObjListDuesRateandDuesMasterModel;
        }

        public async Task<int> InsertUpdateDuesRateDetails(DuesRateandDuesMasterModel objDuesRateandDuesMasterModel)
        {
            DataTable dt = CommonClasses.CommonFunctions.ToDataTable(objDuesRateandDuesMasterModel.RateDetails);
            var result = 0;
            Action action = () =>
               {
                   using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_insertandUpDuesRateDetails))
                   {
                       SqlParameter tblpara = new SqlParameter("RateDetails", dt);
                       tblpara.SqlDbType = SqlDbType.Structured;
                       dbCommand.Parameters.Add(tblpara);

                       epsdatabase.AddInParameter(dbCommand, "MsDueRtDetailId", DbType.String, objDuesRateandDuesMasterModel.MsPayRatesID);
                       epsdatabase.AddInParameter(dbCommand, "payItemId", DbType.String, objDuesRateandDuesMasterModel.DuesCodeDuesDefination);
                       epsdatabase.AddInParameter(dbCommand, "orgtype", DbType.String, objDuesRateandDuesMasterModel.OrganizationType);
                       epsdatabase.AddInParameter(dbCommand, "paycomm", DbType.Int32, objDuesRateandDuesMasterModel.PayCommission);
                       epsdatabase.AddInParameter(dbCommand, "duescode", DbType.Int32, objDuesRateandDuesMasterModel.DuesCodeDuesDefination);
                       epsdatabase.AddInParameter(dbCommand, "cityclass", DbType.Int32, objDuesRateandDuesMasterModel.CityClass);
                       epsdatabase.AddInParameter(dbCommand, "WithEffectFrom", DbType.DateTime, objDuesRateandDuesMasterModel.WithEffectFrom);
                       epsdatabase.AddInParameter(dbCommand, "WithEffectTo", DbType.DateTime, objDuesRateandDuesMasterModel.WithEffectTo);
                       epsdatabase.AddInParameter(dbCommand, "value", DbType.String, objDuesRateandDuesMasterModel.Value);
                       epsdatabase.AddInParameter(dbCommand, "state", DbType.String, objDuesRateandDuesMasterModel.state);
                       epsdatabase.AddInParameter(dbCommand, "slabtype", DbType.String, objDuesRateandDuesMasterModel.SlabType);
                       epsdatabase.AddInParameter(dbCommand, "vletterNo", DbType.String, objDuesRateandDuesMasterModel.VideLetterNo.Trim());
                       epsdatabase.AddInParameter(dbCommand, "vletterdate", DbType.DateTime, objDuesRateandDuesMasterModel.VideLetterDate);
                       epsdatabase.AddInParameter(dbCommand, "Activated", DbType.String, objDuesRateandDuesMasterModel.Activated);
                       result = epsdatabase.ExecuteNonQuery(dbCommand);
                       
                       if(result==0)
                       {
                           result = -1;
                       }
                       //lsttodt = null;
                       dt.Dispose();
                   }
               };
            await Task.Run(action).ConfigureAwait(true);

            return result;
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetDuesRatemaster()
        {
            List<DuesRateandDuesMasterModel> objDuesDefinationandDuesMasterModelList = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.Sp_GetDuesRateMaster))
                {
                    using (IDataReader objReader = this.epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DuesRateandDuesMasterModel objDuesRateandDuesMasterModel = new DuesRateandDuesMasterModel();
                            objDuesRateandDuesMasterModel.MsPayRatesID = objReader["MsDueRtDetailId"] != DBNull.Value ? Convert.ToInt32(objReader["MsDueRtDetailId"]) : 0;
                            objDuesRateandDuesMasterModel.PayRatesPayItemCD = objReader["PayItemsCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayItemsCD"]) : 0;
                            objDuesRateandDuesMasterModel.PayItemsName = objReader["PayItemsName"] != DBNull.Value ? Convert.ToString(objReader["PayItemsName"]) : null;
                            objDuesRateandDuesMasterModel.WithEffectFrom = objReader["FormDate"] != DBNull.Value ? Convert.ToDateTime(objReader["FormDate"]) : objDuesRateandDuesMasterModel.WithEffectFrom = null;
                            objDuesRateandDuesMasterModel.WithEffectTo = objReader["Todate"] != DBNull.Value ? Convert.ToDateTime(objReader["Todate"]) : objDuesRateandDuesMasterModel.WithEffectTo = null;
                            //objDuesRateandDuesMasterModel.Value = objReader["Value"] != DBNull.Value ? Convert.ToString(objReader["Value"]) : objDuesRateandDuesMasterModel.Value = null;
                            objDuesRateandDuesMasterModel.MsSlabTypeDes = objReader["SlabType"] != DBNull.Value ? Convert.ToString(objReader["SlabType"]) : objDuesRateandDuesMasterModel.MsSlabTypeDes = null; objDuesRateandDuesMasterModel.SlabType = objReader["DupSlabType"] != DBNull.Value ? Convert.ToInt32(objReader["DupSlabType"]) : objDuesRateandDuesMasterModel.SlabType = 0;

                            objDuesRateandDuesMasterModel.PayCommission = objReader["DupPaycomm"] != DBNull.Value ? Convert.ToInt32(objReader["DupPaycomm"]) : 0;
                            objDuesRateandDuesMasterModel.CityClass = objReader["DupCityClass"] != DBNull.Value ? Convert.ToInt32(objReader["DupCityClass"]) : 0;

                            objDuesRateandDuesMasterModel.VideLetterDate = objReader["DupVLetterDate"] != DBNull.Value ? Convert.ToDateTime(objReader["DupVLetterDate"]) : objDuesRateandDuesMasterModel.VideLetterDate = null;

                            objDuesRateandDuesMasterModel.state = objReader["DupState"] != DBNull.Value ? Convert.ToString(objReader["DupState"]) : null;

                            objDuesRateandDuesMasterModel.OrganizationType = objReader["DupOrgType"] != DBNull.Value ? Convert.ToString(objReader["DupOrgType"]) : null;
                            objDuesRateandDuesMasterModel.Activated = objReader["DupActivated"] != DBNull.Value ? Convert.ToString(objReader["DupActivated"]) : null;
                            objDuesDefinationandDuesMasterModelList.Add(objDuesRateandDuesMasterModel);
                        }

                    }

                }
            }).ConfigureAwait(true);

            if (objDuesDefinationandDuesMasterModelList == null)
            {
                return null;
            }
            else
            {
                return objDuesDefinationandDuesMasterModelList;
            }
        }

        public async Task<List<DuesRateandDuesMasterModel>> GetDuesRateDetails1()
        {
            List<DuesRateandDuesMasterModel> objListDuesRateandDuesMasterModel = new List<DuesRateandDuesMasterModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_GetDuesRateDetails))
                {
                    using (DataSet objData = this.epsdatabase.ExecuteDataSet(dbCommand))
                    {
                        if (objData != null && objData.Tables.Count > 0)
                        {
                            DataTable dtSection = objData.Tables[0];
                            DataTable dtRate = objData.Tables[1];

                            foreach (DataRow drow in dtSection.Rows)
                            {
                                DuesRateandDuesMasterModel objDuesRateandDuesMasterModel = new DuesRateandDuesMasterModel();

                                objDuesRateandDuesMasterModel.MsPayRatesID = Convert.ToInt32(drow["MsDueRtDetailId"]);
                                objDuesRateandDuesMasterModel.MsDueRtDetailId = Convert.ToInt32(drow["MsDueRtDetailId"]);

                                objDuesRateandDuesMasterModel.PayRatesPayItemCD = Convert.ToInt32(drow["PayItemsCD"]);
                                objDuesRateandDuesMasterModel.PayItemsName = Convert.ToString(drow["PayItemsName"]);

                                objDuesRateandDuesMasterModel.DuesCodeDuesDefination = Convert.ToInt32(drow["MsPayItemsID"]);

                                //objDuesRateandDuesMasterModel.DuesCodeDuesDefination = Convert.ToInt32(drow["DrrID"]);
                                objDuesRateandDuesMasterModel.OrganizationType = Convert.ToString(drow["DupOrgType"]);
                                objDuesRateandDuesMasterModel.PayCommission = Convert.ToInt32(drow["DupPaycomm"]);
                                // objDuesRateandDuesMasterModel.DuesCodeDuesDefination = Convert.ToInt32(drow["DrrFinFYR"]);
                                objDuesRateandDuesMasterModel.CityClass = Convert.ToInt32(drow["DupCityClass"]);
                                objDuesRateandDuesMasterModel.WithEffectFrom = Convert.ToDateTime(drow["FormDate"]);

                                var outputParam = drow["Todate"];
                                if (!(outputParam is DBNull))
                                {
                                    objDuesRateandDuesMasterModel.WithEffectTo = Convert.ToDateTime(drow["Todate"]);
                                }

                                objDuesRateandDuesMasterModel.Value = Convert.ToString(drow["DupValue"]).Trim();
                                objDuesRateandDuesMasterModel.state = Convert.ToString(drow["DupState"]).Trim();
                                objDuesRateandDuesMasterModel.SlabType = Convert.ToInt32(drow["DupSlabType"]);

                                objDuesRateandDuesMasterModel.MsSlabTypeDes = Convert.ToString(drow["SlabType"]);
                                objDuesRateandDuesMasterModel.VideLetterNo = Convert.ToString(drow["DupVLetterNo"]);

                                var outputParamDupVLetter = drow["DupVLetterDate"];
                                if (!(outputParamDupVLetter is DBNull))
                                {
                                    objDuesRateandDuesMasterModel.VideLetterDate = Convert.ToDateTime(drow["DupVLetterDate"]);
                                }

                                objDuesRateandDuesMasterModel.Activated = Convert.ToString(drow["DupActivated"]);

                                DataView dv1 = dtRate.DefaultView;
                                dv1.RowFilter = "DueRtDetailId = '" + objDuesRateandDuesMasterModel.MsDueRtDetailId + "'";
                                DataTable dtRateNew = dv1.ToTable();

                                foreach (DataRow row in dtRateNew.Rows)
                                {
                                    rateDetails ObjrateDetails = new rateDetails();
                                    ObjrateDetails.MsDueRtDetailId = Convert.ToInt32(row["DueRtDetailId"]);

                                    var minAmmount = row["MinAmount"];

                                    if (!(minAmmount is DBNull))
                                    {
                                        ObjrateDetails.MinAmount = Convert.ToInt32(row["MinAmount"]);
                                    }
                                    var LLimit = row["LLimit"];
                                    if (!(LLimit is DBNull))
                                    {
                                        ObjrateDetails.LowerLimit = Convert.ToInt32(row["LLimit"]);
                                    }

                                    ObjrateDetails.UpperLimit = Convert.ToInt32(row["Ulimit"]);
                                    ObjrateDetails.ValueDuesRate = Convert.ToInt32(row["Value"]);
                                    ObjrateDetails.SlabNo = Convert.ToInt32(row["SlabNo"]);

                                    objDuesRateandDuesMasterModel.RateDetails.Add(ObjrateDetails);

                                }

                                objListDuesRateandDuesMasterModel.Add(objDuesRateandDuesMasterModel);
                            }
                        }

                    }
                }
            }).ConfigureAwait(true);

            return objListDuesRateandDuesMasterModel;
        }

        /// <summary>
        /// Delete Dues Defination record
        /// </summary>
        /// <param name="DuesCd"></param>
        /// <returns></returns>
        public async Task<int> DeleteDuesRateDetails(string duesratedetailsId)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = this.epsdatabase.GetStoredProcCommand(EPSConstant.sp_DeleteDuesRateDetials))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDueRtDetailId", DbType.Int32, duesratedetailsId);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            }).ConfigureAwait(true);

            return result;
        }
    }
}
