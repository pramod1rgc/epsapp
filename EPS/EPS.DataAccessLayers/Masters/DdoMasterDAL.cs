﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using System.Threading.Tasks;


namespace EPS.DataAccessLayers.Masters
{
  public  class DdoMasterDAL
    {
        Database epsdatabase = null;

        public DdoMasterDAL(Database database)
        {
            epsdatabase = database;
        }
    
        #region Get All Controller 
        /// <summary>
        /// Get All Controller
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ControllerMs>> GetAllController()
        {
            List<ControllerMs> objControllerList = new List<ControllerMs>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    epsdatabase.AddInParameter(dbCommand, "Query", DbType.String, "getContrlerDdoMST");
                    epsdatabase.AddInParameter(dbCommand, "ID", DbType.String, null);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {

                        while (objReader.Read())
                        {
                            ControllerMs objcontrollerData = new ControllerMs();
                            objcontrollerData.ControllerId = objReader["Id"] != DBNull.Value ? Convert.ToInt32(objReader["Id"]): 0;
                            objcontrollerData.ControllerCode = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objControllerList.Add(objcontrollerData);

                        }

                    }

                }

            });
            if (objControllerList == null)
                return null;
            else
                return objControllerList;


        }
        #endregion

        #region Get PAO by ControllerId
        /// <summary>
        /// Get PAO by ControllerId
        /// </summary>
        /// <param name="ControllerId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PAO>> GetPAOByControllerId(string ControllerId)
        {

            List<PAO> objPAOList = new List<PAO>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    epsdatabase.AddInParameter(dbCommand, "Query", DbType.String, "getPAODdoMST");
                    epsdatabase.AddInParameter(dbCommand, "ID", DbType.String, ControllerId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            PAO objPAOData = new PAO();
                            objPAOData.PaoId = objReader["Id"] != DBNull.Value ? Convert.ToInt32(objReader["Id"]) : 0;
                            objPAOData.PaoCode = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objPAOList.Add(objPAOData);

                        }

                    }

                }

            });
            if (objPAOList == null)
                return null;
            else
                return objPAOList;


        }
        #endregion

        #region Get DDO by PAO
        /// <summary>
        /// Get DDO by PAOId.
        /// </summary>
        /// <param name="PaoID"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DDO>> GetODdoByPao(string PaoID)
        {

            List<DDO> objDDOList = new List<DDO>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllController))
                {
                    epsdatabase.AddInParameter(dbCommand, "Query", DbType.String, "getDDODdoMST");
                    epsdatabase.AddInParameter(dbCommand, "ID", DbType.String, PaoID);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DDO objDDOData = new DDO();
                            objDDOData.DdoId = objReader["Id"] != DBNull.Value ? Convert.ToString(objReader["Id"]).Trim() : null;
                            objDDOData.DdoCode = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objDDOData.DdoName = objReader["ddoname"] != DBNull.Value ? Convert.ToString(objReader["ddoname"]).Trim() : null;
                            objDDOData.DdoLang = objReader["lang"] != DBNull.Value ? Convert.ToString(objReader["lang"]).Trim() : null;

                            objDDOData.DdoAddress = objReader["DDOAddress"] != DBNull.Value ? Convert.ToString(objReader["DDOAddress"]).Trim() : null;
                            objDDOData.PinCode = objReader["PINCODE"] != DBNull.Value ? Convert.ToInt32(objReader["PINCODE"]): 0;
                            objDDOData.ContactNo = objReader["ContactNo"] != DBNull.Value ? Convert.ToString(objReader["ContactNo"]): null;
                            objDDOData.FaxNo = objReader["FaxNo"] != DBNull.Value ? Convert.ToString(objReader["FaxNo"]): null;
                            objDDOData.EmailAddress = objReader["EmailAddress"] != DBNull.Value ? Convert.ToString(objReader["EmailAddress"]).Trim() : null;
                            objDDOData.StateId = objReader["MsStateID"] != DBNull.Value ? Convert.ToInt32(objReader["MsStateID"]): 0;
                            objDDOData.CityId = objReader["MsCityid"] != DBNull.Value ? Convert.ToInt32(objReader["MsCityid"]): 0;
                            objDDOList.Add(objDDOData);

                        }

                    }

                }

            });
            if (objDDOList == null)
                return null;
            else
                return objDDOList;


        }
        #endregion

        #region Get DDO Category
        /// <summary>
        /// Get DDO category
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DDOType>> GetDDOcategory()
        {

            List<DDOType> objDDOTypeList = new List<DDOType>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DDOType))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            DDOType objDDOTypeData = new DDOType();
                            objDDOTypeData.DdoTypeId = objReader["Id"] != DBNull.Value ? Convert.ToInt32(objReader["Id"]): 0;
                            objDDOTypeData.DdoTypeText = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objDDOTypeList.Add(objDDOTypeData);

                        }

                    }

                }

            });
            if (objDDOTypeList == null)
                return null;
            else
                return objDDOTypeList;


        }
        #endregion

        #region Get State
        /// <summary>
        /// Get State
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<State>> GetState()
        {

            List<State> objStateList = new List<State>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetAllState))
                {
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            State objStatetData = new State();
                            objStatetData.StateId = objReader["Id"] != DBNull.Value ? Convert.ToInt32(objReader["Id"]): 0;
                            objStatetData.StateText = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objStateList.Add(objStatetData);

                        }

                    }

                }

            });
            if (objStateList == null)
                return null;
            else
                return objStateList;


        }
        #endregion

        #region Get City by StatetId
        /// <summary>
        /// Get City
        /// </summary>
        /// <param name="StatetId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<City>> GetCity(string StatetId)
        {

            List<City> objCityList = new List<City>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetCity))
                {
                    epsdatabase.AddInParameter(dbCommand, "Id", DbType.Int32, StatetId);
                    using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            City objCityData = new City();
                            objCityData.CityId = objReader["Id"] != DBNull.Value ? Convert.ToInt32(objReader["Id"]): 0;
                            objCityData.CityText = objReader["Text"] != DBNull.Value ? Convert.ToString(objReader["Text"]).Trim() : null;
                            objCityList.Add(objCityData);

                        }

                    }

                }

            });
            if (objCityList == null)
                return null;
            else
                return objCityList;


        }
        #endregion

        #region DDO Master Details
        /// <summary>
        /// Get DDO Master Details
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DdoMasterModel>> GetDDOMaster()
        {

            List<DdoMasterModel> objDdoMasterModelList = new List<DdoMasterModel>();           
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MasterDDO))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDDOId", DbType.Int32,null);
                    epsdatabase.AddInParameter(dbCommand, "DDOCategory", DbType.Int32,null);
                    epsdatabase.AddInParameter(dbCommand, "Address", DbType.String,null);
                    epsdatabase.AddInParameter(dbCommand, "Pincode", DbType.Int32,null);
                    epsdatabase.AddInParameter(dbCommand, "PhoneNo", DbType.String,null);
                    epsdatabase.AddInParameter(dbCommand, "FaxNo", DbType.String,null);
                    epsdatabase.AddInParameter(dbCommand, "Email", DbType.String,null);
                    epsdatabase.AddInParameter(dbCommand, "State", DbType.Int32,null);
                    epsdatabase.AddInParameter(dbCommand, "City", DbType.Int32,null);

                    epsdatabase.AddInParameter(dbCommand, "Operation", DbType.String, "Select");

                    DataTable dt = epsdatabase.ExecuteDataSet(dbCommand).Tables[0];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        DdoMasterModel objDdoMasterModelData = new DdoMasterModel();
                        objDdoMasterModelData.DdoId = dt.Rows[j]["MsDDOId"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["MsDDOId"]).Trim():null;
                        objDdoMasterModelData.DdoCode = dt.Rows[j]["DDOCode"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["DDOCode"]).Trim():null;
                        objDdoMasterModelData.DdoName = dt.Rows[j]["DDOName"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["DDOName"]).Trim():null;
                        objDdoMasterModelData.DdoLang = dt.Rows[j]["DDONameLang2"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["DDONameLang2"]).Trim():null;

                        objDdoMasterModelData.ControllerId = dt.Rows[j]["MsControllerID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsControllerID"]):0;                        
                        objDdoMasterModelData.ControllerCode = dt.Rows[j]["ControllerCode"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ControllerCode"]).Trim():null;

                        objDdoMasterModelData.PaoId = dt.Rows[j]["MsPAOID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsPAOID"]):0;
                        objDdoMasterModelData.PaoCode = dt.Rows[j]["PaoCode"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["PaoCode"]).Trim():null;
                        
                        objDdoMasterModelData.DdoTypeId = dt.Rows[j]["DDOCategoryId"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["DDOCategoryId"]):0;
                        objDdoMasterModelData.DdoTypeText = dt.Rows[j]["DDOCategoryText"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["DDOCategoryText"]).Trim():null;
                        objDdoMasterModelData.DdoAddress = dt.Rows[j]["DdoAddress"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["DdoAddress"]).Trim():null;
                        objDdoMasterModelData.PinCode = dt.Rows[j]["PINCODE"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["PINCODE"]):0;
                        objDdoMasterModelData.ContactNo = dt.Rows[j]["ContactNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["ContactNo"]):null;
                        objDdoMasterModelData.FaxNo = dt.Rows[j]["FaxNo"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["FaxNo"]):null; 
                        objDdoMasterModelData.EmailAddress = dt.Rows[j]["EmailAddress"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["EmailAddress"]).Trim():null;
                        objDdoMasterModelData.StateId = dt.Rows[j]["MsStateID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsStateID"]):0;
                        objDdoMasterModelData.StateName = dt.Rows[j]["StateName"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["StateName"]).Trim():null;
                        objDdoMasterModelData.CityId = dt.Rows[j]["MsCityid"] != DBNull.Value ? Convert.ToInt32(dt.Rows[j]["MsCityid"]):0;
                        objDdoMasterModelData.CityName = dt.Rows[j]["CityName"] != DBNull.Value ? Convert.ToString(dt.Rows[j]["CityName"]).Trim():null;

                        objDdoMasterModelList.Add(objDdoMasterModelData);
                    }

                }

            });
            if (objDdoMasterModelList == null)
                return null;
            else
                return objDdoMasterModelList;


        }
        #endregion

        #region Save DDO Master
        /// <summary>
        /// Save DDO Master
        /// </summary>
        /// <param name="objDdoMasterModel"></param>
        /// <returns></returns>
        public async Task<int> SaveDDOMaster(DdoMasterModel objDdoMasterModel)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MasterDDO))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDDOId", DbType.Int32, objDdoMasterModel.DdoId);
                    epsdatabase.AddInParameter(dbCommand, "DDOCategory", DbType.Int32, objDdoMasterModel.DdoTypeId);
                    epsdatabase.AddInParameter(dbCommand, "Address", DbType.String, objDdoMasterModel.DdoAddress);
                    epsdatabase.AddInParameter(dbCommand, "Pincode", DbType.Int32, objDdoMasterModel.PinCode);
                    epsdatabase.AddInParameter(dbCommand, "PhoneNo", DbType.String,objDdoMasterModel.ContactNo);
                    epsdatabase.AddInParameter(dbCommand, "FaxNo", DbType.String,objDdoMasterModel.FaxNo);
                    epsdatabase.AddInParameter(dbCommand, "Email", DbType.String, objDdoMasterModel.EmailAddress);
                    epsdatabase.AddInParameter(dbCommand, "State", DbType.Int32, objDdoMasterModel.StateId);
                    epsdatabase.AddInParameter(dbCommand, "City", DbType.Int32, objDdoMasterModel.CityId);
                    epsdatabase.AddInParameter(dbCommand, "Operation", DbType.String, "Update");
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = null; //Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion

        #region Deactivate DDO Master
        /// <summary>
        /// Deactivate DDO Master
        /// </summary>
        /// <param name="objDdoMasterModel"></param>
        /// <returns></returns>
        public async Task<int> DeleteDDOMaster(string ddoId)
        {
            var result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MasterDDO))
                {
                    epsdatabase.AddInParameter(dbCommand, "MsDDOId", DbType.Int32, ddoId);
                    epsdatabase.AddInParameter(dbCommand, "DDOCategory", DbType.Int32, null);
                    epsdatabase.AddInParameter(dbCommand, "Address", DbType.String, null);
                    epsdatabase.AddInParameter(dbCommand, "Pincode", DbType.Int32, null);
                    epsdatabase.AddInParameter(dbCommand, "PhoneNo", DbType.String, null);
                    epsdatabase.AddInParameter(dbCommand, "FaxNo", DbType.String, null);
                    epsdatabase.AddInParameter(dbCommand, "Email", DbType.String, null);
                    epsdatabase.AddInParameter(dbCommand, "State", DbType.Int32, null);
                    epsdatabase.AddInParameter(dbCommand, "City", DbType.Int32, null);
                    epsdatabase.AddInParameter(dbCommand, "Operation", DbType.String, "Delete");
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                    string Error = null; //Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "Error"));
                    if (!String.IsNullOrEmpty(Error))
                    {
                        result = 0;
                    }
                }
            });
            return result;
        }
        #endregion
    }
}
