﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace EPS.DataAccessLayers.Masters
{
   public class MsStateAgDAL
    {

        Database epsdatabase = null;

        public MsStateAgDAL(Database database)
        {

            epsdatabase = database;
        }
        #region Get  MsStateAg 
        public List<MsStateAgBM> GetMsStateAg()
        {


            List<MsStateAgBM> MsStateAgList = null;

            MsStateAgList = new List<MsStateAgBM>();
            MsStateAgBM MsStateAgDatas;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsStateAg_pf))
            {
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "select");
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {

                        MsStateAgDatas = new MsStateAgBM();
                        MsStateAgDatas.PfType = objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]) : null;

                        MsStateAgDatas.PfTypeDesc = objReader["PfTypeDesc"] != DBNull.Value ? Convert.ToString(objReader["PfTypeDesc"]) : null;

                        MsStateAgDatas.StateName = objReader["StateName"] != DBNull.Value ? Convert.ToString(objReader["StateName"]) :  null;

                        MsStateAgDatas.StateAgDesc = objReader["StateAgDesc"] != DBNull.Value ? Convert.ToString(objReader["StateAgDesc"]).Trim() :  null;
                        MsStateAgDatas.StateCode = objReader["StateCode"] != DBNull.Value ? Convert.ToString(objReader["StateCode"]).Trim() :  null;

                        MsStateAgList.Add(MsStateAgDatas);

                    }

                }

            }

            if (MsStateAgList == null)
                return null;
            else
                return MsStateAgList;

        }
        #endregion
        #region Get MsState
        public List<StateBM> GetMsState()
        {
            List<StateBM> MsStateList = null;
            MsStateList = new List<StateBM>();
            StateBM MsStateDatas;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsStateAg_pf))
            {
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "bindstate");
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {

                        MsStateDatas = new StateBM();

                        MsStateDatas.StateName = objReader["StateName"] != DBNull.Value ? Convert.ToString(objReader["StateName"]).Trim() :  null;
                        MsStateDatas.StateCode = objReader["StateCode"] != DBNull.Value ? Convert.ToString(objReader["StateCode"]).Trim() :  null;
                        MsStateList.Add(MsStateDatas);
                    }

                }

            }

            if (MsStateList == null)
                return null;
            else
                return MsStateList;

        }
        #endregion
        #region Get MsPfType
        public List<PfTypeBM>GetMsPfType()
        {
            List<PfTypeBM> MsPfTypeList = null;
            MsPfTypeList = new List<PfTypeBM>();
            PfTypeBM MsPftypeDatas;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsStateAg_pf))
            {
                epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "bindpfType");
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        MsPftypeDatas = new PfTypeBM();
                        MsPftypeDatas.PfType= objReader["PfType"] != DBNull.Value ? Convert.ToString(objReader["PfType"]).Trim() :  null;
                        MsPftypeDatas.PfTypeDesc= objReader["PfDesc"] != DBNull.Value ? Convert.ToString(objReader["PfDesc"]).Trim() :  null;
                        MsPfTypeList.Add(MsPftypeDatas);

                    }

                }

            }

            if (MsPfTypeList == null)
                return null;
            else
                return MsPfTypeList;

        }
        #endregion
        #region update MsStateAg 
        public string UpdateMsStateAg(MsStateAgBM MsStateAgBM)
        {
            string Response = null;
            if (MsStateAgBM != null)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsStateAg_pf))
                {
                    epsdatabase.AddInParameter(dbCommand, "StateAgDesc", DbType.String, MsStateAgBM.StateAgDesc);
                    epsdatabase.AddInParameter(dbCommand, "StateCode", DbType.String, MsStateAgBM.StateCode);
                    epsdatabase.AddInParameter(dbCommand, "PfType", DbType.String, MsStateAgBM.PfType);
                    epsdatabase.AddInParameter(dbCommand, "PfTypeDesc", DbType.String, MsStateAgBM.PfTypeDesc);
                    epsdatabase.AddInParameter(dbCommand, "MsStateAgId", DbType.Int32, MsStateAgBM.MsStateAgId);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "update");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            }
            return Response;

        }
        #endregion
        #region insert MsStateAg 
        public string InsertMsStateAg(MsStateAgBM MsStateAgBM)
        {
            string Response = null;
            if (MsStateAgBM != null)
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_MsStateAg_pf))
                {
                    epsdatabase.AddInParameter(dbCommand, "StateAgDesc", DbType.String, MsStateAgBM.StateAgDesc);
                    epsdatabase.AddInParameter(dbCommand, "StateCode", DbType.String, MsStateAgBM.StateCode);
                    epsdatabase.AddInParameter(dbCommand, "PfType", DbType.String, MsStateAgBM.PfType);
                    epsdatabase.AddInParameter(dbCommand, "PfTypeDesc", DbType.String, MsStateAgBM.PfTypeDesc);
                    epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "insert");
                    epsdatabase.AddOutParameter(dbCommand, "ResponseStatus", DbType.String, 50);
                    epsdatabase.ExecuteNonQuery(dbCommand);
                    Response = Convert.ToString(epsdatabase.GetParameterValue(dbCommand, "ResponseStatus"));
                }
            }
            return Response;

        }
        #endregion
    }
}
