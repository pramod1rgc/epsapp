﻿using EPS.BusinessModels.Shared;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.Shared
{
    public class SharedDAL
    {
        private DbCommand dbCommand = null;
        private Database epsdatabase = null;

        public SharedDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get PayScale Code
        public IEnumerable<PayBillGroupModel> GetPayBillGroup(string PermDdoid, string Flag)
        {
            List<PayBillGroupModel> ListPayBillGroupModel = new List<PayBillGroupModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroup)) //EPSConstant.usp_BindPayScaleCode
            {
                epsdatabase.AddInParameter(dbCommand, "PermDDOId", DbType.String, PermDdoid);
                epsdatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        PayBillGroupModel ObjPayBillGroupModel = new PayBillGroupModel
                        {
                            payBillGroupId = Convert.ToInt32(objReader["PayBillGroupId"]),
                            billgrId = objReader["BillgrId"].ToString(),
                            billgrDesc = objReader["BillgrDesc"].ToString(),
                            serviceType = objReader["ServiceType"].ToString(),
                            groups = objReader["Groups"].ToString().Trim(),
                            schemeId = Convert.ToInt32(objReader["SchemeId"]),
                            serviceTypeId = Convert.ToInt32(objReader["ServiceTypeId"]),
                            billForGAR = Convert.ToBoolean(objReader["BillForGAR"]),
                            schemeCode = objReader["SchemeCode"].ToString()
                        };
                        ListPayBillGroupModel.Add(ObjPayBillGroupModel);
                    }
                }
            }
            return ListPayBillGroupModel;
        }
        #endregion

        #region Get Pay Bill Group By Perm DDOId  
        public async Task<List<PayBillGroupModel>> GetPayBillGroupByPermDDOId(string PermDdoid, string Flag)
        {
            List<PayBillGroupModel> ListPayBillGroupModel = new List<PayBillGroupModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetPayBillGroupByPermDDOId);
                epsdatabase.AddInParameter(dbCommand, "@PermDDOId", DbType.String, PermDdoid);
                epsdatabase.AddInParameter(dbCommand, "@Flag", DbType.String, Flag);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        PayBillGroupModel ObjPayBillGroupModel = new PayBillGroupModel
                        {
                            payBillGroupId = Convert.ToInt32(objReader["PayBillGroupId"]),
                            billgrId = objReader["BillgrId"].ToString(),
                            billgrDesc = objReader["BillgrDesc"].ToString()

                        };
                        ListPayBillGroupModel.Add(ObjPayBillGroupModel);
                    }
                }
            });
            return ListPayBillGroupModel;
        }
        #endregion     

        #region Get Designation
        public async Task<List<DesignationModel>> GetDesignation(string BillGrpid, string Flag)
        {
            List<DesignationModel> ListDesignationModel = new List<DesignationModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDesignation);
                epsdatabase.AddInParameter(dbCommand, "@BillgrID", DbType.String, BillGrpid);
                epsdatabase.AddInParameter(dbCommand, "@Flag", DbType.String, Flag);

                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        DesignationModel ObjDesignationModel = new DesignationModel
                        {
                            DesigId = Convert.ToInt32(objReader["DesigNo"]),
                            DesigDesc = objReader["DesigDesc"].ToString()
                        };
                        ListDesignationModel.Add(ObjDesignationModel);
                    }
                }
            });
            return ListDesignationModel;
        }
        #endregion

        #region Get Designation By BillgrID
        public async Task<List<DesignationModel>> GetDesignationByBillgrID(string BillGrpid, string PermDdoid, string Flag)
        {
            List<DesignationModel> ListDesignationModel = new List<DesignationModel>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDesignationByBillgrID);
                epsdatabase.AddInParameter(dbCommand, "BillgrID", DbType.String, BillGrpid);
                epsdatabase.AddInParameter(dbCommand, "PermDDOId", DbType.String, PermDdoid);
                epsdatabase.AddInParameter(dbCommand, "Flag", DbType.String, Flag);


                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        DesignationModel ObjDesignationModel = new DesignationModel
                        {
                            DesigId = Convert.ToInt32(objReader["DesigSrNo"]),
                            DesigDesc = objReader["DESIGDESC"].ToString()
                        };
                        ListDesignationModel.Add(ObjDesignationModel);
                    }
                }
            });
            return ListDesignationModel;
        }
        #endregion

        #region Get Emp
        public IEnumerable<EmpModel> GetEmp(string DesigId, string flag)
        {
            List<EmpModel> ListEmployeeModel = new List<EmpModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmployee))
            {
                epsdatabase.AddInParameter(dbCommand, "DesignationID", DbType.String, DesigId);
                epsdatabase.AddInParameter(dbCommand, "Flag", DbType.String, flag);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        EmpModel ObjEmployeeModel = new EmpModel
                        {
                            EmpCd = objReader["Empcd"].ToString()
                        };

                        ObjEmployeeModel.EmpName = objReader["EmpName"].ToString();
                        ListEmployeeModel.Add(ObjEmployeeModel);
                    }
                }
            }
            return ListEmployeeModel;
        }
        #endregion 

        #region Get All Designation
        public IEnumerable<DesignationModel> GetAllDesignation(string id)
        {
            List<DesignationModel> listOfdesig = new List<DesignationModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.sus_GetAllDesignation))
            {
                epsdatabase.AddInParameter(dbCommand, "Id", DbType.String, id);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        DesignationModel ObjDesignationModel = new DesignationModel
                        {
                            DesigId = Convert.ToInt32(objReader["DesigNo"]),
                            DesigDesc = objReader["DesigDesc"].ToString()
                        };
                        listOfdesig.Add(ObjDesignationModel);
                    }
                }
                return listOfdesig;
            }
        }
        #endregion

        #region Get All Designation Of Department
        public IEnumerable<DesignationModel> GetAllDesignationOfDepartment(string id)
        {
            List<DesignationModel> listOfdesig = new List<DesignationModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetDesignationAll))
            {
                epsdatabase.AddInParameter(dbCommand, "controllerId", DbType.String, id);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        DesignationModel ObjDesignationModel = new DesignationModel
                        {
                            DesigId = Convert.ToInt32(objReader["DesigNo"]),
                            DesigDesc = objReader["DesigDesc"].ToString()
                        };
                        listOfdesig.Add(ObjDesignationModel);
                    }
                }
            }
            return listOfdesig;
        }
        #endregion

        #region Get Emp With Designation Details
        public IEnumerable<EmpDesigModel> GetEmpWithDesignationDetails(bool SuspendedEmp, string controllerId, string DDOId)
        {
            List<EmpDesigModel> ListEmployeeModel = new List<EmpDesigModel>();
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmployeeWithDesignationDetails))
            {
                if (SuspendedEmp)
                {
                    epsdatabase.AddInParameter(dbCommand, "SuspendedEmployees", DbType.Boolean, 1);
                }
                else
                {
                    epsdatabase.AddInParameter(dbCommand, "SuspendedEmployees", DbType.Boolean, 0);
                }
                epsdatabase.AddInParameter(dbCommand, "ControllerId", DbType.String, controllerId);
                epsdatabase.AddInParameter(dbCommand, "DdoId", DbType.String, DDOId);

                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        EmpDesigModel ObjEmployeeModel = new EmpDesigModel
                        {
                            EmpCd = objReader["Empcd"].ToString()
                        };
                        ObjEmployeeModel.EmpName = objReader["EmpName"].ToString();
                        ObjEmployeeModel.DesignationCd = objReader["DesignationCd"].ToString();
                        ListEmployeeModel.Add(ObjEmployeeModel);
                    }
                }

            }
            return ListEmployeeModel;

        }
        #endregion
    }
}