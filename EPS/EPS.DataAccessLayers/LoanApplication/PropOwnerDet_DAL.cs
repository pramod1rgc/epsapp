﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LoanApplication
{
    /// <summary>
    /// PropOwnerDetDAL
    /// </summary>
    public class PropOwnerDetDAL
    {
       private Database epsdatabase = null;
        private Database database = null;
        DbCommand dbCommand = null;
        public PropOwnerDetDAL()
        {
        }
        public PropOwnerDetDAL(Database database)
        {
            epsdatabase = database;
        }
        #region Insert And Update Prop Owner Master Record
        public async Task<string> CreatePropOwnerDetMaster(PropOwner_Model propmodel)
        {
            string hostName = Dns.GetHostName();
            string message = string.Empty;
            string Response = null;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertUpdatePropOwnerDetlsMaster)) //EPSConstant.Usp_InsertUpdatePropOwnerDetlsMaster
                {
                    epsdatabase.AddInParameter(dbCommand, "@ID", DbType.Int32, propmodel.MsPropOwnerDetlsID);
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanPurposeID", DbType.Int32, propmodel.PayLoanPurposeID);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerName", DbType.String, propmodel.OwnerName);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerPanNo", DbType.String, propmodel.OwnerPanNo);
                    epsdatabase.AddInParameter(dbCommand, "@BankId", DbType.Int32, propmodel.BankId);
                    epsdatabase.AddInParameter(dbCommand, "@BranchID", DbType.Int32, propmodel.BranchID);
                    epsdatabase.AddInParameter(dbCommand, "@BankIFSCCODE", DbType.String, propmodel.BankIFSCCODE);
                    epsdatabase.AddInParameter(dbCommand, "@BankAccountNo", DbType.String, propmodel.BankAccountNo);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, propmodel.CreatedIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, propmodel.CreatedBy);
                    int i = epsdatabase.ExecuteNonQuery(dbCommand);
                    if (i > 0 && propmodel.MsPropOwnerDetlsID > 0)
                    {
                        Response = EPSResource.UpdateSuccessMessage;
                    }
                    else if (i > 0)
                    {
                        Response = EPSResource.SaveSuccessMessage;
                    }
                    else
                    {
                        Response = EPSResource.AlreadyExistMessage;
                    }

                }
            });
            return Response;
        }
        #endregion


        #region Get Master Details BY ID      
        public async Task<IEnumerable<PropOwner_Model>> GetPropOwnerDetMasterDetailsByID(string MasterID)
        {
            List<PropOwner_Model> list = new List<PropOwner_Model>();
            database = epsdatabase;
            await Task.Run(() =>
            {
                database.AddInParameter(dbCommand, "@MasterID", DbType.Int32, MasterID);
                using (IDataReader objReader = database.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        PropOwner_Model propmodel = new PropOwner_Model();
                        propmodel.MsPropOwnerDetlsID = Convert.ToInt32(objReader["MsPropOwnerDetlsID"]);
                        propmodel.PayLoanPurposeID = Convert.ToInt32(objReader["PayLoanPurposeID"]);
                        propmodel.OwnerName = objReader["OwnerName"].ToString();
                        propmodel.OwnerPanNo = objReader["OwnerPanNo"].ToString();
                        propmodel.BankId = Convert.ToInt32(objReader["BankId"]);
                        propmodel.BranchID = Convert.ToInt32(objReader["BranchID"]);
                        propmodel.BankIFSCCODE = objReader["BankIFSCCODE"].ToString();
                        propmodel.BankAccountNo = objReader["BankAccountNo"].ToString();
                        propmodel.CreatedBy = objReader["CreatedBy"].ToString();
                        list.Add(propmodel);
                    }
                }
            });
            return list;
        }
        #endregion

        #region Edit PropOwner Master Details
        public async Task<IEnumerable<PropOwner_Model>> EditPropOwnerDetMasterDetails(string MasterID)
        {

            List<PropOwner_Model> list = new List<PropOwner_Model>();
            await Task.Run(() =>
            {
                dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EditTptaMasterDetails);
                epsdatabase.AddInParameter(dbCommand, "@TptaMasterID", DbType.Int32, MasterID);
                using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
                {
                    while (objReader.Read())
                    {
                        PropOwner_Model propmodel = new PropOwner_Model();
                        propmodel.MsPropOwnerDetlsID = Convert.ToInt32(objReader["MsPropOwnerDetlsID"]);
                        propmodel.PayLoanPurposeID = Convert.ToInt32(objReader["PayLoanPurposeID"]);
                        propmodel.OwnerName = objReader["OwnerName"].ToString();
                        propmodel.OwnerPanNo = objReader["OwnerPanNo"].ToString();
                        propmodel.BankId = Convert.ToInt32(objReader["BankId"]);
                        propmodel.BranchID = Convert.ToInt32(objReader["BranchID"]);
                        propmodel.BankIFSCCODE = objReader["BankIFSCCODE"].ToString();
                        propmodel.BankAccountNo = objReader["BankAccountNo"].ToString();
                        propmodel.CreatedBy = objReader["CreatedBy"].ToString();
                        list.Add(propmodel);
                    }
                }
            });
            return list;
        }
        #endregion


    }
}
