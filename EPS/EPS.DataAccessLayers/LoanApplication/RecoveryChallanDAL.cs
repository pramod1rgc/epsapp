﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LoanApplication
{
   public  class RecoveryChallanDAL
    {
        Database epsdatabase = null;
        public RecoveryChallanDAL(Database database)
        {
            epsdatabase = database;
        }
       // string con = ConfigurationManager.AppSettings["EPSConnection"].ToString();
        //public async Task<IEnumerable<tblMsPayLoanRef>> BindLoanType()
        //{
        //    List<tblMsPayLoanRef> lstPayLoanRef = new List<tblMsPayLoanRef>();
        //    await Task.Run(() =>
        //    {
        //        using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
        //        {
        //            epsdatabase.AddInParameter(dbCommand, "mode", DbType.String, "4");
        //            using (IDataReader objReader = epsdatabase.ExecuteReader(dbCommand))
        //            {
        //                while (objReader.Read())
        //                {
        //                    tblMsPayLoanRef ObjMstPayLoanRef = new tblMsPayLoanRef();
        //                    ObjMstPayLoanRef.PayLoanRefLoanCD = Convert.ToInt32(objReader["PayLoanRefLoanCD"]);
        //                    ObjMstPayLoanRef.PayLoanRefLoanShortDesc = Convert.ToString(objReader["PayLoanRefLoanShortDesc"]);                          
        //                    lstPayLoanRef.Add(ObjMstPayLoanRef);
        //                }
        //            }
        //        }
        //    });
        //    if (lstPayLoanRef.Count == 0)
        //        return null;
        //    else
        //        return lstPayLoanRef;
        //}

        #region Recovery Challan
        public async Task<List<RecoveryChallan>> GetRecoveryChallan(string empCode, int msDesignID)
        {
            List<RecoveryChallan> listEmployeeModel = new List<RecoveryChallan>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetRecoveryChallan))
                {
                    epsdatabase.AddInParameter(dbCommand, "@empcode", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "@designcode", DbType.String, msDesignID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            RecoveryChallan objApplicationDetailsModel = new RecoveryChallan();
                            CommonClasses.CommonFunctions.MapFetchData(objApplicationDetailsModel, rdr);
                            if (objApplicationDetailsModel.ChallanDate == DateTime.MinValue)
                            {
                                objApplicationDetailsModel.ChallanDate = DateTime.Now;
                            }

                            if (rdr["InstallmentAmount"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.InstallmentAmount = Convert.ToInt32(rdr["InstallmentAmount"]);
                            }

                            if (rdr["TotalNoofInstallments"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.TotalNoInstalments = Convert.ToInt32(rdr["TotalNoofInstallments"]);
                            }
                            if (rdr["OddInstlAmt"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.OddInstlAmt = Convert.ToInt32(rdr["OddInstlAmt"]);
                            }
                            if (rdr["OddInstlNo"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.OddInstlNo = Convert.ToInt32(rdr["OddInstlNo"]);
                            }
                            if (rdr["LastInstalmentNoRecovered"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.LastInstalmentNoRecovered = Convert.ToInt32(rdr["LastInstalmentNoRecovered"]);
                            }
                            if (rdr["OutstandingAmount"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.OutstandingAmount = Convert.ToInt32(rdr["OutstandingAmount"]);
                            }

                            if(rdr["ChequeNoDDNo"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.ChequeNoDDNo = Convert.ToInt32(rdr["ChequeNoDDNo"]);
                            }

                            if (rdr["ChallanAmt"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.ChallanAmt = Convert.ToInt32(rdr["ChallanAmt"]);
                            }

                            listEmployeeModel.Add(objApplicationDetailsModel);
                        }
                    }
                }
            });
            return listEmployeeModel;
        }
        #endregion

        #region insert EPS Employee Join After Lien Period
        public async Task<string> InsertUpdateRecoveryChallan(RecoveryChallan objModel)
        {
           var result = "";
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Sp_InsertUpdateRecoveryChallan))
                {
                    RecoveryChallan objApplicationDetailsModel = new RecoveryChallan();
                    CommonClasses.CommonFunctions.MapUpdateParameters(objModel,epsdatabase, dbCommand);

                }
            });
            return result;

        }
        #endregion

    }
}
