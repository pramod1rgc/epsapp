﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace EPS.DataAccessLayers.LoanApplication
{
    public class LoanApplicationDAL
    {
        private readonly Database epsdatabase = null;
        public LoanApplicationDAL(Database database)
        {
            epsdatabase = database;
        }

        #region Get All EmpDetails
        public async Task<List<LoanApplicationDetailsModel>> GetAllEmpDetails(string empCode, int msDesignID)
        {
            List<LoanApplicationDetailsModel> listEmployeeModel = new List<LoanApplicationDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmployeeDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@empcode", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "@designcode", DbType.String, msDesignID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            LoanApplicationDetailsModel objApplicationDetailsModel = new LoanApplicationDetailsModel();
                            objApplicationDetailsModel.MsEmpID = Convert.ToInt32(rdr["MsEmpID"]);
                            objApplicationDetailsModel.EmpCd = rdr["EmpCd"].ToString();
                            objApplicationDetailsModel.EmpApptType = Convert.ToString(rdr["EmpApptType"]);
                            objApplicationDetailsModel.EmpPfType = Convert.ToBoolean(rdr["EmpPfType"]);
                            objApplicationDetailsModel.pay_basic = Convert.ToString(rdr["pay_basic"]);
                            objApplicationDetailsModel.EmpFullName = Convert.ToString(rdr["EmpFullName"]);
                            objApplicationDetailsModel.EmpSupanDt = Convert.ToDateTime(rdr["EmpSupanDt"]);
                            objApplicationDetailsModel.EmpJoinDt = Convert.ToDateTime(rdr["EmpJoinDt"]);
                            objApplicationDetailsModel.DesigDesc = Convert.ToString(rdr["DesigDesc"]);
                            if (rdr["LoanAmtSanc"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.LoanAmtSanc = Convert.ToInt32(rdr["LoanAmtSanc"]);
                            }

                            if (rdr["PriInstAmt"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.PriInstAmt = Convert.ToInt32(rdr["PriInstAmt"]);
                            }
                            if (rdr["IntTotInst"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.IntTotInst = Convert.ToInt32(rdr["IntTotInst"]);
                            }
                            if (rdr["OddInstNoInt"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.OddInstNoInt = Convert.ToInt32(rdr["OddInstNoInt"]);
                            }
                            if (rdr["OddInstAmtInt"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.OddInstAmtInt = Convert.ToInt32(rdr["OddInstAmtInt"]);
                            }
                            objApplicationDetailsModel.payLoanRefLoanCD = Convert.ToInt32(rdr["LoanCd"]);
                            objApplicationDetailsModel.payLoanPurposeCode = Convert.ToInt32(rdr["PayLoanPurposeCode"]);
                            objApplicationDetailsModel.PayLoanPurposeID = Convert.ToInt32(rdr["PayLoanPurposeID"]);
                            objApplicationDetailsModel.Rule_18_1964_Pur = Convert.ToBoolean(rdr["Rule_18_1964_Pur"]);
                            objApplicationDetailsModel.Anti_Price_Pc = Convert.ToString(rdr["Anti_Price_Pc"]);
                            objApplicationDetailsModel.Off_On_Leave = Convert.ToBoolean(rdr["Off_On_Leave"]);
                            objApplicationDetailsModel.Pre_Adv_Simi_Pur = Convert.ToBoolean(rdr["Pre_Adv_Simi_Pur"]);
                            objApplicationDetailsModel.Adv_Draw_Date = Convert.ToDateTime(rdr["Adv_Draw_Date"]);
                            if (rdr["Adv_Amount"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Adv_Amount = Convert.ToInt32(rdr["Adv_Amount"]);
                            }

                            objApplicationDetailsModel.Comp_Certi_Info_1 = Convert.ToBoolean(rdr["Comp_Certi_Info_1"]);
                            objApplicationDetailsModel.Comp_Certi_Info_2 = Convert.ToBoolean(rdr["Comp_Certi_Info_2"]);
                            objApplicationDetailsModel.Upload_Sign = Convert.ToString(rdr["Upload_Sign"]);
                            objApplicationDetailsModel.Address = Convert.ToString(rdr["Address"]);
                            objApplicationDetailsModel.Selct_Area = Convert.ToBoolean(rdr["Selct_Area"]);
                            objApplicationDetailsModel.Demar_Dev = Convert.ToBoolean(rdr["Demar_Dev"]);
                            if (rdr["Area_Sq_Ft"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Area_Sq_Ft = Convert.ToInt32(rdr["Area_Sq_Ft"]);
                            }

                            if (rdr["Cost"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Cost = Convert.ToInt32(rdr["Cost"]);
                            }

                            if (rdr["Amt_Act_Pay"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Amt_Act_Pay = Convert.ToInt32(rdr["Amt_Act_Pay"]);
                            }

                            objApplicationDetailsModel.Purp_Acquire = rdr["Purp_Acquire"] != DBNull.Value ? Convert.ToString(rdr["Purp_Acquire"]) : string.Empty;
                            objApplicationDetailsModel.Unexp_Port_Lease = Convert.ToString(rdr["Unexp_Port_Lease"]);
                            objApplicationDetailsModel.Pop_Decl_1 = Convert.ToBoolean(rdr["Pop_Decl_1"]);
                            objApplicationDetailsModel.Pop_Decl_2 = Convert.ToBoolean(rdr["Pop_Decl_2"]);
                            objApplicationDetailsModel.Pop_Decl_3 = Convert.ToBoolean(rdr["Pop_Decl_3"]);
                            if (rdr["Flr_Area"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Flr_Area = Convert.ToInt32(rdr["Flr_Area"]);
                            }

                            if (rdr["Est_Cost"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Est_Cost = Convert.ToInt32(rdr["Est_Cost"]);
                            }

                            if (rdr["Amt_Adv_Req"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Amt_Adv_Req = Convert.ToInt32(rdr["Amt_Adv_Req"]);
                            }
                            if (rdr["Num_Inst"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Num_Inst = Convert.ToInt32(rdr["Num_Inst"]);
                            }
                            objApplicationDetailsModel.Pur_Area = Convert.ToInt32(rdr["Pur_Area"]);
                            objApplicationDetailsModel.Const_Dec_1 = Convert.ToBoolean(rdr["Const_Dec_1"]);
                            objApplicationDetailsModel.Const_Dec_2 = Convert.ToBoolean(rdr["Const_Dec_2"]);
                            objApplicationDetailsModel.Const_Dec_3 = Convert.ToBoolean(rdr["Const_Dec_3"]);
                            if (rdr["Plint_Area"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Plint_Area = Convert.ToInt32(rdr["Plint_Area"]);
                            }
                            if (rdr["Plth_Prop_Enlarge"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Plth_Prop_Enlarge = Convert.ToInt32(rdr["Plth_Prop_Enlarge"]);
                            }
                            if (rdr["Coc"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Coc = Convert.ToInt32(rdr["Coc"]);
                            }
                            if (rdr["DesNoInsRePaidForAdv"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.DesNoInsRePaidForAdv = Convert.ToInt32(rdr["DesNoInsRePaidForAdv"]);
                            }
                            if (rdr["Cop_Enlarge"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Cop_Enlarge = Convert.ToInt32(rdr["Cop_Enlarge"]);
                            }
                            if (rdr["Tot_Cost"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Tot_Cost = Convert.ToInt32(rdr["Tot_Cost"]);
                            }
                            objApplicationDetailsModel.Agency_Whom_Pur = Convert.ToString(rdr["Agency_Whom_Pur"]);
                            objApplicationDetailsModel.Dtls_Enlarg_1 = Convert.ToBoolean(rdr["Dtls_Enlarg_1"]);
                            objApplicationDetailsModel.Dtls_Enlarg_2 = Convert.ToBoolean(rdr["Dtls_Enlarg_2"]);
                            objApplicationDetailsModel.Dtls_Enlarg_3 = Convert.ToBoolean(rdr["Dtls_Enlarg_3"]);
                            objApplicationDetailsModel.Const_Date = Convert.ToDateTime(rdr["Const_Date"]);
                            if (rdr["Prc_Settled"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Prc_Settled = Convert.ToInt32(rdr["Prc_Settled"]);
                            }
                            if (rdr["Amt_Paid"] != DBNull.Value)
                            {
                                objApplicationDetailsModel.Amt_Paid = Convert.ToInt32(rdr["Amt_Paid"]);
                            }
                            objApplicationDetailsModel.Dtls_Built_Flat_1 = Convert.ToBoolean(rdr["Dtls_Built_Flat_1"]);
                            objApplicationDetailsModel.Dtls_Built_flat_2 = Convert.ToBoolean(rdr["Dtls_Built_flat_2"]);
                            objApplicationDetailsModel.Dtls_Built_flat_3 = Convert.ToBoolean(rdr["Dtls_Built_flat_3"]);
                            objApplicationDetailsModel.OwnerName = rdr["OwnerName"].ToString();
                            objApplicationDetailsModel.OwnerPanNo = rdr["OwnerPanNo"].ToString();
                            objApplicationDetailsModel.Descriptions = rdr["Descriptions"].ToString();
                            objApplicationDetailsModel.BankIFSCCODE = rdr["BankIFSCCODE"].ToString();
                            objApplicationDetailsModel.BankAccountNo = rdr["BankAccountNo"].ToString();
                            objApplicationDetailsModel.PermDdoId = Convert.ToInt32(rdr["DDOID"].ToString());

                            listEmployeeModel.Add(objApplicationDetailsModel);
                        }
                    }
                }
            });
            return listEmployeeModel;
        }
        #endregion

        #region Get All Sanction Details
        public async Task<IEnumerable<Sanction>> GetAllSanctionDetails(string empCode, int msDesignID, int mode)
        {
            List<Sanction> listEmployeeModel = new List<Sanction>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UPS_GetSanctionDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@EMPCODE", DbType.String, empCode);
                    epsdatabase.AddInParameter(dbCommand, "@DESIGNCODE", DbType.String, msDesignID);
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            Sanction objApplicationDetailsModel = new Sanction();
                            objApplicationDetailsModel.MsEmpID = rdr["MsEmpID"] != DBNull.Value ? Convert.ToInt32(rdr["MsEmpID"]) : 0;
                            objApplicationDetailsModel.EmpCd = rdr["EmpCd"].ToString();
                            objApplicationDetailsModel.EmpName = rdr["EmpName"].ToString();
                            objApplicationDetailsModel.Description = rdr["description"].ToString();
                            objApplicationDetailsModel.PayLoanRefLoanCD = Convert.ToInt32(rdr["PayLoanRefLoanCD"]);
                            objApplicationDetailsModel.EmpApptType = Convert.ToString(rdr["EmpApptType"]);
                            objApplicationDetailsModel.LoanAmtSanc = Convert.ToInt32(rdr["LoanAmtSanc"]);
                            objApplicationDetailsModel.SancOrdNo = Convert.ToString(rdr["SancOrdNo"]);
                            objApplicationDetailsModel.SancOrdDT = Convert.ToDateTime(rdr["SancOrdDT"].ToString());
                            objApplicationDetailsModel.LoanAmtDisbursed = Convert.ToInt32(rdr["LoanAmtDisbursed"]);
                            objApplicationDetailsModel.IfscCD = Convert.ToString(rdr["IfscCD"]);
                            objApplicationDetailsModel.BnkAcNo = Convert.ToString(rdr["BnkAcNo"]);
                            objApplicationDetailsModel.BankName = Convert.ToString(rdr["BankName"].ToString());
                            objApplicationDetailsModel.BranchName = Convert.ToString(rdr["BranchName"]);
                            objApplicationDetailsModel.PriVerifFlag = rdr["PriVerifFlag"] != DBNull.Value ? Convert.ToInt32(rdr["PriVerifFlag"]) : 0;
                            objApplicationDetailsModel.Description = rdr["description"] != DBNull.Value ? Convert.ToString(rdr["description"]).Trim() : string.Empty;
                            objApplicationDetailsModel.MsPropOwnerDetlsID = rdr["MsPropOwnerDetlsID"] != DBNull.Value ? Convert.ToInt32(rdr["MsPropOwnerDetlsID"]) : 0;
                            objApplicationDetailsModel.Sanction_Remarks = rdr["Sanction_Remarks"] != DBNull.Value ? Convert.ToString(rdr["Sanction_Remarks"]).Trim() : string.Empty;
                            objApplicationDetailsModel.Release_Remarks = rdr["Release_Remarks"] != DBNull.Value ? Convert.ToString(rdr["Release_Remarks"]).Trim() : string.Empty;
                            objApplicationDetailsModel.MSPayAdvEmpdetID = rdr["PayAdvEmpdetID"] != DBNull.Value ? Convert.ToInt32(rdr["PayAdvEmpdetID"]) : 0;
                            listEmployeeModel.Add(objApplicationDetailsModel);
                        }
                    }
                }
            });

            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;

        }
        #endregion

        #region Update Sanction Details
        public async Task<int> UpdateSanctionDetails(Sanction objSanction)
        {
            int result = 0;
            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.UPS_UpdateSanctionDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@ids", DbType.Int32, objSanction.MsEmpID);
                    epsdatabase.AddInParameter(dbCommand, "@SancOrdDT", DbType.DateTime, objSanction.SancOrdDT);
                    epsdatabase.AddInParameter(dbCommand, "@SancOrdNo", DbType.String, objSanction.SancOrdNo);
                    epsdatabase.AddInParameter(dbCommand, "@LoanAmtSanc", DbType.Int32, objSanction.LoanAmtSanc);
                    epsdatabase.AddInParameter(dbCommand, "@BankIFSCCODE", DbType.String, objSanction.IfscCD);
                    epsdatabase.AddInParameter(dbCommand, "@BankAccountNo", DbType.String, objSanction.BnkAcNo);
                    epsdatabase.AddInParameter(dbCommand, "@Descriptions", DbType.String, objSanction.Description);
                    epsdatabase.AddInParameter(dbCommand, "@PriVerifFlag", DbType.String, objSanction.PriVerifFlag);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCode", DbType.String, objSanction.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "@LoanAmtDisbursed", DbType.Int32, objSanction.LoanAmtDisbursed);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, objSanction.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerName", DbType.String, objSanction.EmpName);
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanPurposeID", DbType.Int32, objSanction.PayLoanRefLoanCD);
                    epsdatabase.AddInParameter(dbCommand, "@MsPropOwnerDetlsID", DbType.Int32, objSanction.MsPropOwnerDetlsID);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region Get Employee Loan Details by empCode
        public async Task<IEnumerable<LoanApplicationDetailsModel>> GetEmpLoanDetailsByID(string empCode)
        {
            List<LoanApplicationDetailsModel> listEmployeeModel = new List<LoanApplicationDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetLoanByID))
                {
                    epsdatabase.AddInParameter(dbCommand, "@empcode", DbType.String, empCode);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            LoanApplicationDetailsModel objApplicationDetailsModel = new LoanApplicationDetailsModel();

                            objApplicationDetailsModel.MsEmpID = Convert.ToInt32(rdr["MsEmpID"]);
                            objApplicationDetailsModel.EmpCd = rdr["EmpCd"].ToString();
                            objApplicationDetailsModel.EmpApptType = Convert.ToString(rdr["EmpApptType"]);
                            objApplicationDetailsModel.EmpPfType = Convert.ToBoolean(rdr["EmpPfType"]);
                            objApplicationDetailsModel.pay_basic = Convert.ToString(rdr["pay_basic"]);
                            objApplicationDetailsModel.EmpFullName = Convert.ToString(rdr["EmpFullName"]);
                            objApplicationDetailsModel.EmpSupanDt = Convert.ToDateTime(rdr["EmpSupanDt"]);
                            objApplicationDetailsModel.DesigDesc = Convert.ToString(rdr["DesigDesc"]);
                            objApplicationDetailsModel.LoanAmtSanc = Convert.ToInt32(rdr["LoanAmtSanc"]);
                            objApplicationDetailsModel.PriInstAmt = Convert.ToInt32(rdr["PriInstAmt"]);
                            objApplicationDetailsModel.IntTotInst = Convert.ToInt32(rdr["IntTotInst"]);
                            objApplicationDetailsModel.OddInstNoInt = Convert.ToInt32(rdr["OddInstNoInt"]);
                            objApplicationDetailsModel.OddInstAmtInt = Convert.ToInt32(rdr["OddInstAmtInt"]);
                            objApplicationDetailsModel.payLoanRefLoanCD = Convert.ToInt32(rdr["LoanCd"]);
                            objApplicationDetailsModel.payLoanPurposeCode = Convert.ToInt32(rdr["PayLoanPurposeCode"]);
                            objApplicationDetailsModel.PayLoanPurposeID = Convert.ToInt32(rdr["PayLoanPurposeID"]);
                            objApplicationDetailsModel.Rule_18_1964_Pur = Convert.ToBoolean(rdr["Rule_18_1964_Pur"]);
                            objApplicationDetailsModel.Anti_Price_Pc = Convert.ToString(rdr["Anti_Price_Pc"]);
                            objApplicationDetailsModel.Adv_Draw_Date = Convert.ToDateTime(rdr["Adv_Draw_Date"]);
                            objApplicationDetailsModel.Comp_Certi_Info_1 = Convert.ToBoolean(rdr["Comp_Certi_Info_1"]);
                            objApplicationDetailsModel.Comp_Certi_Info_2 = Convert.ToBoolean(rdr["Comp_Certi_Info_2"]);
                            objApplicationDetailsModel.Upload_Sign = Convert.ToString(rdr["Upload_Sign"]);
                            objApplicationDetailsModel.Address = Convert.ToString(rdr["Address"]);
                            objApplicationDetailsModel.Selct_Area = Convert.ToBoolean(rdr["Selct_Area"]);
                            objApplicationDetailsModel.Demar_Dev = Convert.ToBoolean(rdr["Demar_Dev"]);
                            objApplicationDetailsModel.Area_Sq_Ft = Convert.ToInt32(rdr["Area_Sq_Ft"]);
                            objApplicationDetailsModel.Cost = Convert.ToInt32(rdr["Cost"]);
                            objApplicationDetailsModel.IntPurPrsnComp = Convert.ToBoolean(rdr["IntPurPrsnComp"]);
                            objApplicationDetailsModel.Rule_18_1964_Pur = Convert.ToBoolean(rdr["Rule_18_1964_Pur"]);
                            objApplicationDetailsModel.Off_On_Leave = Convert.ToBoolean(rdr["Off_On_Leave"]);
                            objApplicationDetailsModel.Pre_Adv_Simi_Pur = Convert.ToBoolean(rdr["Pre_Adv_Simi_Pur"]);
                            objApplicationDetailsModel.Adv_Amount = Convert.ToInt32(rdr["Adv_Amount"]);
                            objApplicationDetailsModel.DesNoInsRePaidForAdv = Convert.ToInt32(rdr["DesNoInsRePaidForAdv"]);
                            objApplicationDetailsModel.Amt_Act_Pay = Convert.ToInt32(rdr["Amt_Act_Pay"]);
                            objApplicationDetailsModel.Purp_Acquire = Convert.ToString(rdr["Purp_Acquire"]);
                            objApplicationDetailsModel.Unexp_Port_Lease = Convert.ToString(rdr["Unexp_Port_Lease"]);
                            objApplicationDetailsModel.Pop_Decl_1 = Convert.ToBoolean(rdr["Pop_Decl_1"]);
                            objApplicationDetailsModel.Pop_Decl_2 = Convert.ToBoolean(rdr["Pop_Decl_2"]);
                            objApplicationDetailsModel.Pop_Decl_3 = Convert.ToBoolean(rdr["Pop_Decl_3"]);
                            objApplicationDetailsModel.Flr_Area = Convert.ToInt32(rdr["Flr_Area"]);
                            objApplicationDetailsModel.Est_Cost = Convert.ToInt32(rdr["Est_Cost"]);
                            objApplicationDetailsModel.Amt_Adv_Req = Convert.ToInt32(rdr["Amt_Adv_Req"]);
                            objApplicationDetailsModel.Num_Inst = Convert.ToInt32(rdr["Num_Inst"]);
                            objApplicationDetailsModel.Pur_Area = Convert.ToInt32(rdr["Pur_Area"]);
                            objApplicationDetailsModel.Const_Dec_1 = Convert.ToBoolean(rdr["Const_Dec_1"]);
                            objApplicationDetailsModel.Const_Dec_2 = Convert.ToBoolean(rdr["Const_Dec_2"]);
                            objApplicationDetailsModel.Const_Dec_3 = Convert.ToBoolean(rdr["Const_Dec_3"]);
                            objApplicationDetailsModel.Plint_Area = Convert.ToInt32(rdr["Plint_Area"]);
                            objApplicationDetailsModel.Plth_Prop_Enlarge = Convert.ToInt32(rdr["Plth_Prop_Enlarge"]);
                            objApplicationDetailsModel.Coc = Convert.ToInt32(rdr["Coc"]);
                            objApplicationDetailsModel.Cop_Enlarge = Convert.ToInt32(rdr["Cop_Enlarge"]);
                            objApplicationDetailsModel.Tot_Cost = Convert.ToInt32(rdr["Tot_Cost"]);
                            objApplicationDetailsModel.Agency_Whom_Pur = Convert.ToString(rdr["Agency_Whom_Pur"]);
                            objApplicationDetailsModel.Dtls_Enlarg_1 = Convert.ToBoolean(rdr["Dtls_Enlarg_1"]);
                            objApplicationDetailsModel.Dtls_Enlarg_2 = Convert.ToBoolean(rdr["Dtls_Enlarg_2"]);
                            objApplicationDetailsModel.Dtls_Enlarg_3 = Convert.ToBoolean(rdr["Dtls_Enlarg_3"]);
                            objApplicationDetailsModel.Const_Date = Convert.ToDateTime(rdr["Const_Date"]);
                            objApplicationDetailsModel.Prc_Settled = Convert.ToInt32(rdr["Prc_Settled"]);
                            objApplicationDetailsModel.Amt_Paid = Convert.ToInt32(rdr["Amt_Paid"]);
                            objApplicationDetailsModel.Dtls_Built_Flat_1 = Convert.ToBoolean(rdr["Dtls_Built_Flat_1"]);
                            objApplicationDetailsModel.Dtls_Built_flat_2 = Convert.ToBoolean(rdr["Dtls_Built_flat_2"]);
                            objApplicationDetailsModel.Dtls_Built_flat_3 = Convert.ToBoolean(rdr["Dtls_Built_flat_3"]);
                            listEmployeeModel.Add(objApplicationDetailsModel);
                        }
                    }
                }
            });
            if (listEmployeeModel == null)
                return null;
            else
                return listEmployeeModel;

        }
        #endregion

        #region Edit Employee Loan Details by EmpId            
        public async Task<IEnumerable<LoanApplicationDetailsModel>> EditLoanDetailsByEMPID(int id)
        {
            List<LoanApplicationDetailsModel> listEmployeeModel = new List<LoanApplicationDetailsModel>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_EditLoanDetailsByEMPID))
                {
                    epsdatabase.AddInParameter(dbCommand, "@empid", DbType.Int32, id);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            LoanApplicationDetailsModel objApplicationDetailsModel = new LoanApplicationDetailsModel();
                            objApplicationDetailsModel.MsEmpID = Convert.ToInt32(rdr["MsEmpID"]);
                            objApplicationDetailsModel.EmpCd = rdr["EmpCd"].ToString();
                            objApplicationDetailsModel.EmpApptType = Convert.ToString(rdr["EmpApptType"]);
                            objApplicationDetailsModel.EmpPfType = Convert.ToBoolean(rdr["EmpPfType"]);
                            objApplicationDetailsModel.pay_basic = Convert.ToString(rdr["pay_basic"]);
                            objApplicationDetailsModel.EmpFullName = Convert.ToString(rdr["EmpFullName"]);
                            objApplicationDetailsModel.EmpSupanDt = Convert.ToDateTime(rdr["EmpSupanDt"]);
                            objApplicationDetailsModel.DesigDesc = Convert.ToString(rdr["DesigDesc"]);
                            objApplicationDetailsModel.LoanAmtSanc = Convert.ToInt32(rdr["LoanAmtSanc"]);
                            objApplicationDetailsModel.PriInstAmt = Convert.ToInt32(rdr["PriInstAmt"]);
                            objApplicationDetailsModel.IntTotInst = Convert.ToInt32(rdr["IntTotInst"]);
                            objApplicationDetailsModel.OddInstNoInt = Convert.ToInt32(rdr["OddInstNoInt"]);
                            objApplicationDetailsModel.OddInstAmtInt = Convert.ToInt32(rdr["OddInstAmtInt"]);
                            objApplicationDetailsModel.payLoanRefLoanCD = Convert.ToInt32(rdr["LoanCd"]);
                            objApplicationDetailsModel.payLoanPurposeCode = Convert.ToInt32(rdr["PayLoanPurposeCode"]);
                            objApplicationDetailsModel.PayLoanPurposeID = Convert.ToInt32(rdr["PayLoanPurposeID"]);
                            objApplicationDetailsModel.Rule_18_1964_Pur = Convert.ToBoolean(rdr["Rule_18_1964_Pur"]);
                            objApplicationDetailsModel.Anti_Price_Pc = Convert.ToString(rdr["Anti_Price_Pc"]);
                            objApplicationDetailsModel.DesNoInsRePaidForAdv = Convert.ToInt32(rdr["DesNoInsRePaidForAdv"]);
                            objApplicationDetailsModel.Adv_Draw_Date = Convert.ToDateTime(rdr["Adv_Draw_Date"]);
                            objApplicationDetailsModel.Comp_Certi_Info_1 = Convert.ToBoolean(rdr["Comp_Certi_Info_1"]);
                            objApplicationDetailsModel.Comp_Certi_Info_2 = Convert.ToBoolean(rdr["Comp_Certi_Info_2"]);
                            objApplicationDetailsModel.Upload_Sign = Convert.ToString(rdr["Upload_Sign"]);
                            objApplicationDetailsModel.Address = Convert.ToString(rdr["Address"]);
                            objApplicationDetailsModel.Selct_Area = Convert.ToBoolean(rdr["Selct_Area"]);
                            objApplicationDetailsModel.Demar_Dev = Convert.ToBoolean(rdr["Demar_Dev"]);
                            objApplicationDetailsModel.Area_Sq_Ft = Convert.ToInt32(rdr["Area_Sq_Ft"]);
                            objApplicationDetailsModel.Cost = Convert.ToInt32(rdr["Cost"]);
                            objApplicationDetailsModel.Tot_Plint_Area = Convert.ToInt32(rdr["Tot_Plint_Area"]);
                            objApplicationDetailsModel.IntPurPrsnComp = Convert.ToBoolean(rdr["IntPurPrsnComp"]);
                            objApplicationDetailsModel.Rule_18_1964_Pur = Convert.ToBoolean(rdr["Rule_18_1964_Pur"]);
                            objApplicationDetailsModel.Off_On_Leave = Convert.ToBoolean(rdr["Off_On_Leave"]);
                            objApplicationDetailsModel.Pre_Adv_Simi_Pur = Convert.ToBoolean(rdr["Pre_Adv_Simi_Pur"]);
                            objApplicationDetailsModel.Adv_Amount = Convert.ToInt32(rdr["Adv_Amount"]);
                            objApplicationDetailsModel.Amt_Act_Pay = Convert.ToInt32(rdr["Amt_Act_Pay"]);
                            objApplicationDetailsModel.Purp_Acquire = Convert.ToString(rdr["Purp_Acquire"]);
                            objApplicationDetailsModel.Unexp_Port_Lease = Convert.ToString(rdr["Unexp_Port_Lease"]);
                            objApplicationDetailsModel.Pop_Decl_1 = Convert.ToBoolean(rdr["Pop_Decl_1"]);
                            objApplicationDetailsModel.Pop_Decl_2 = Convert.ToBoolean(rdr["Pop_Decl_2"]);
                            objApplicationDetailsModel.Pop_Decl_3 = Convert.ToBoolean(rdr["Pop_Decl_3"]);
                            objApplicationDetailsModel.Flr_Area = Convert.ToInt32(rdr["Flr_Area"]);
                            objApplicationDetailsModel.Est_Cost = Convert.ToInt32(rdr["Est_Cost"]);
                            objApplicationDetailsModel.Amt_Adv_Req = Convert.ToInt32(rdr["Amt_Adv_Req"]);
                            objApplicationDetailsModel.Num_Inst = Convert.ToInt32(rdr["Num_Inst"]);
                            objApplicationDetailsModel.Pur_Area = Convert.ToInt32(rdr["Pur_Area"]);
                            objApplicationDetailsModel.Const_Dec_1 = Convert.ToBoolean(rdr["Const_Dec_1"]);
                            objApplicationDetailsModel.Const_Dec_2 = Convert.ToBoolean(rdr["Const_Dec_2"]);
                            objApplicationDetailsModel.Const_Dec_3 = Convert.ToBoolean(rdr["Const_Dec_3"]);
                            objApplicationDetailsModel.Plint_Area = Convert.ToInt32(rdr["Plint_Area"]);
                            objApplicationDetailsModel.Plth_Prop_Enlarge = Convert.ToInt32(rdr["Plth_Prop_Enlarge"]);
                            objApplicationDetailsModel.Coc = Convert.ToInt32(rdr["Coc"]);
                            objApplicationDetailsModel.Cop_Enlarge = Convert.ToInt32(rdr["Cop_Enlarge"]);
                            objApplicationDetailsModel.Tot_Cost = Convert.ToInt32(rdr["Tot_Cost"]);
                            objApplicationDetailsModel.Agency_Whom_Pur = Convert.ToString(rdr["Agency_Whom_Pur"]);
                            objApplicationDetailsModel.Dtls_Enlarg_1 = Convert.ToBoolean(rdr["Dtls_Enlarg_1"]);
                            objApplicationDetailsModel.Dtls_Enlarg_2 = Convert.ToBoolean(rdr["Dtls_Enlarg_2"]);
                            objApplicationDetailsModel.Dtls_Enlarg_3 = Convert.ToBoolean(rdr["Dtls_Enlarg_3"]);
                            objApplicationDetailsModel.Const_Date = Convert.ToDateTime(rdr["Const_Date"]);
                            objApplicationDetailsModel.Prc_Settled = Convert.ToInt32(rdr["Prc_Settled"]);
                            objApplicationDetailsModel.Amt_Paid = Convert.ToInt32(rdr["Amt_Paid"]);
                            objApplicationDetailsModel.Dtls_Built_Flat_1 = Convert.ToBoolean(rdr["Dtls_Built_Flat_1"]);
                            objApplicationDetailsModel.Dtls_Built_flat_2 = Convert.ToBoolean(rdr["Dtls_Built_flat_2"]);
                            objApplicationDetailsModel.Dtls_Built_flat_3 = Convert.ToBoolean(rdr["Dtls_Built_flat_3"]);
                            objApplicationDetailsModel.PriVerifFlag = Convert.ToInt32(rdr["PriVerifFlag"]);
                            objApplicationDetailsModel.OwnerName = rdr["OwnerName"].ToString();
                            objApplicationDetailsModel.OwnerPanNo = rdr["OwnerPanNo"].ToString();
                            objApplicationDetailsModel.Descriptions = rdr["Descriptions"].ToString();
                            objApplicationDetailsModel.BankName = rdr["BankName"].ToString();
                            objApplicationDetailsModel.BranchName = rdr["BranchName"].ToString();
                            objApplicationDetailsModel.BankIFSCCODE = rdr["BankIFSCCODE"].ToString();
                            objApplicationDetailsModel.BankAccountNo = rdr["BankAccountNo"].ToString();
                            objApplicationDetailsModel.Remarks = rdr["Remarks"].ToString();
                            objApplicationDetailsModel.LoanApp_Remarks = rdr["LoanApp_Remarks"].ToString();
                            objApplicationDetailsModel.Sanction_Remarks = rdr["Sanction_Remarks"].ToString();
                            objApplicationDetailsModel.Release_Remarks = rdr["Release_Remarks"].ToString();
                            listEmployeeModel.Add(objApplicationDetailsModel);
                        }
                    }
                }
            });
            return listEmployeeModel;
        }
        #endregion

        #region Delete Employee Loan Details by EmpId
        public async Task<int> DeleteLoanDetailsByEMPID(int id, int mode)
        {

            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_DeletetLoanDetailsByEMPID))
                {
                    epsdatabase.AddInParameter(dbCommand, "@id", DbType.Int32, id);
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region Update Employee Loan Details by EmpId
        public async Task<int> UpdateLoanDetailsbyID(int id, int mode, string remarks, int priVerifFlag)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateLoanDetailsbyID))
                {
                    epsdatabase.AddInParameter(dbCommand, "@ids", DbType.Int32, id);
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    epsdatabase.AddInParameter(dbCommand, "@remarks", DbType.String, remarks);
                    epsdatabase.AddInParameter(dbCommand, "@priVerifFlag", DbType.Int32, priVerifFlag);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region  Employee Loan Details by EmpId
        public async Task<IEnumerable<Employee>> GetEmpcode(string username)
        {
            List<Employee> listEmpModel = new List<Employee>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_getEmpCode))
                {
                    epsdatabase.AddInParameter(dbCommand, "@username", DbType.String, username.Trim());
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            Employee objModel = new Employee();
                            objModel.empcode = Convert.ToString(rdr["empcode"]);
                            objModel.ddoid = rdr["ddoid"].ToString();
                            objModel.ServiceTime = Convert.ToString(rdr["ServiceTime"]);
                            objModel.serviceType = Convert.ToString(rdr["serviceType"]);
                            listEmpModel.Add(objModel);
                        }
                    }
                }
            });
            return listEmpModel;

        }
        #endregion

        #region  Employee Loan Details by EmpId
        public async Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillgroup(string desigId, string BillgrID, string Flag)
        {
            List<EmpDesigModel> listEmpModel = new List<EmpDesigModel>();
            await Task.Run(() =>
            {

                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpfilterbydesignBillgrID))
                {

                    epsdatabase.AddInParameter(dbCommand, "@DesignationID", DbType.String, desigId.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Flag", DbType.String, Flag);
                    epsdatabase.AddInParameter(dbCommand, "@BillgrID", DbType.String, string.IsNullOrEmpty(BillgrID) || BillgrID == "null" || BillgrID == "undefined" ? "0" : BillgrID.Trim());
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmpDesigModel objModel = new EmpDesigModel();
                            objModel.EmpCd = Convert.ToString(rdr["Empcd"]);
                            objModel.EmpName = rdr["EmpName"].ToString();
                            objModel.BillgrID = Convert.ToInt32(rdr["BillgrID"]);
                            objModel.PermDDOId = Convert.ToString(rdr["EmpPermDDOId"]);
                            listEmpModel.Add(objModel);
                        }
                    }
                }
            });
            return listEmpModel;

        }

        public async Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillID(string desigId, string BillgrID, string PermDdoid, string Flag)
        {
            List<EmpDesigModel> listEmpModel = new List<EmpDesigModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.GetEmpfilterbydesignBillID))
                {

                    epsdatabase.AddInParameter(dbCommand, "@DesignationID", DbType.String, string.IsNullOrEmpty(desigId) || desigId == "null" || desigId == "undefined" ? "0" : desigId.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@BillgrID", DbType.String, string.IsNullOrEmpty(BillgrID) || BillgrID == "null" || BillgrID == "undefined" ? "0" : BillgrID.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@PermDDOId", DbType.String, PermDdoid.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Flag", DbType.String, Flag);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        if (((System.Data.SqlClient.SqlDataReader)((DataReaderWrapper)rdr).InnerReader).HasRows)
                        {
                            while (rdr.Read())
                            {
                                EmpDesigModel objModel = new EmpDesigModel();
                                objModel.EmpCd = Convert.ToString(rdr["Empcd"]);
                                objModel.EmpName = rdr["EmpName"].ToString();
                                objModel.BillgrID = Convert.ToInt32(rdr["BillgrID"]);
                                objModel.PermDDOId = Convert.ToString(rdr["EmpPermDDOId"]);
                                listEmpModel.Add(objModel);
                            }
                        }
                    }
                }
            });
            return listEmpModel;

        }
        #endregion

        #region  Employee Loan Details by EmpId
        public async Task<string> GetBillgroupfilterbydesignID(string desigId, string Flag)
        {
            string str = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetBillgroupfilterbydesignID))
                {

                    epsdatabase.AddInParameter(dbCommand, "@DesignationID", DbType.String, desigId.Trim());
                    epsdatabase.AddInParameter(dbCommand, "@Flag", DbType.String, Flag);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            str = Convert.ToString(rdr["BillgrID"]);
                        }
                    }
                }
            });
            return str;

        }
        #endregion

        #region Bind Loan Type
        public async Task<IEnumerable<tblMsPayLoanRef>> BindLoanType(string loantypeFlag)
        {
            List<tblMsPayLoanRef> lstDesign = new List<tblMsPayLoanRef>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
                {
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.String, loantypeFlag);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            tblMsPayLoanRef MstDesignation = new tblMsPayLoanRef();
                            MstDesignation.PayLoanRefLoanCD = rdr["PayLoanRefLoanCD"] != DBNull.Value ? Convert.ToInt32(rdr["PayLoanRefLoanCD"]) : 0;
                            MstDesignation.PayLoanRefLoanShortDesc = Convert.ToString(rdr["PayLoanRefLoanShortDesc"]);
                            lstDesign.Add(MstDesignation);
                        }
                    }
                }
            });
            return lstDesign;
        }
        #endregion

        #region Get Employee Loan Details
        public async Task<IEnumerable<tblMsLoanStatus>> BindLoanStatus(int mode, int userid, int loginddoid)
        {
            List<tblMsLoanStatus> lstStatus = new List<tblMsLoanStatus>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanStatus))
                {
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.Int32, mode);
                    epsdatabase.AddInParameter(dbCommand, "@userid", DbType.Int32, userid);
                    epsdatabase.AddInParameter(dbCommand, "@loginddoid", DbType.Int32, loginddoid);

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            tblMsLoanStatus MstStatus = new tblMsLoanStatus();
                            MstStatus.Id = Convert.ToInt32(rdr["Id"]);
                            MstStatus.EMPCode = Convert.ToString(rdr["EMPCode"]);
                            MstStatus.LoanCode = Convert.ToString(rdr["LoanType"]);
                            MstStatus.PurposeCode = Convert.ToString(rdr["PurposeCode"]);
                            MstStatus.Status = Convert.ToString(rdr["Status"]);
                            MstStatus.StatusId = Convert.ToInt32(rdr["StatusId"]);
                            MstStatus.ModuleStatus = Convert.ToString(rdr["ModuleStatus"]);
                            lstStatus.Add(MstStatus);
                        }
                    }
                }
            });
            return lstStatus;
        }
        #endregion

        #region Get Forward to checker Details
        public async Task<IEnumerable<tblMsLoanStatus>> GetFwdtochkerDetails()
        {
            List<tblMsLoanStatus> lstStatus = new List<tblMsLoanStatus>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetForwardtocheckerLoanDetails))
                {

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            tblMsLoanStatus MstStatus = new tblMsLoanStatus();
                            MstStatus.Id = Convert.ToInt32(rdr["Id"]);
                            MstStatus.EMPCode = Convert.ToString(rdr["EMPCode"]);
                            MstStatus.LoanCode = Convert.ToString(rdr["LoanType"]);
                            MstStatus.PurposeCode = Convert.ToString(rdr["PurposeCode"]);
                            MstStatus.Status = Convert.ToString(rdr["Status"]);
                            lstStatus.Add(MstStatus);
                        }
                    }
                }
            });
            return lstStatus;
        }
        #endregion

        #region Bind Loanpurpose Selected PayLoanCode
        public async Task<IEnumerable<PayLoanPurpose>> BindLoanpurposeSelectedPayLoanCode(string payLoanCode, string empCode)
        {
            if (payLoanCode == "undefined")
            {
                payLoanCode = "0";
            }
            List<PayLoanPurpose> listEmpModel = new List<PayLoanPurpose>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
                {
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.String, '1');//using this mode variable for loan purpose type.
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanCode", DbType.String, payLoanCode);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, empCode);

                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PayLoanPurpose objEmpModel = new PayLoanPurpose();
                            objEmpModel.PayLoanCode = Convert.ToInt32(rdr["PayLoanCode"]);
                            objEmpModel.PayLoanPurposeDescription = rdr["PayLoanPurposeDescription"].ToString();
                            objEmpModel.PayLoanPurposeCode = Convert.ToInt32(rdr["PayLoanPurposeCode"]);
                            listEmpModel.Add(objEmpModel);
                        }
                    }
                }
            });
            return listEmpModel;
        }
        #endregion

        #region Bind Selected PayLoanCode purpsecode
        public async Task<IEnumerable<PayLoanPurpose>> BindSelectedPayLoanCodepurpsecode(string payLoanCode, string purposeCode)
        {
            List<PayLoanPurpose> listEmployeeModel = new List<PayLoanPurpose>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
                {
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.String, '5');
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanCode", DbType.String, payLoanCode);
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanPurposeCode", DbType.String, purposeCode);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PayLoanPurpose objEmpModel = new PayLoanPurpose();
                            objEmpModel.PayLoanCode = Convert.ToInt32(rdr["PayLoanCode"]);
                            objEmpModel.PayLoanPurposeDescription = rdr["PayLoanPurposeDescription"].ToString();
                            objEmpModel.PayLoanPurposeCode = Convert.ToInt32(rdr["PayLoanPurposeCode"]);
                            listEmployeeModel.Add(objEmpModel);
                        }
                    }
                }
            });
            return listEmployeeModel;
        }
        #endregion

        #region Save Loan Details
        public async Task<int> SaveLoanDetails(LoanApplicationDetailsModel objLoanAppDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_InsertNewLoanApplicationDetails))
                {
                    epsdatabase.AddInParameter(dbCommand, "@PermDdoId", DbType.Int32, objLoanAppDetails.PermDdoId);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objLoanAppDetails.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "@LoanAmtSanc", DbType.Int32, objLoanAppDetails.LoanAmtSanc);
                    epsdatabase.AddInParameter(dbCommand, "@PriInstAmt", DbType.Int32, objLoanAppDetails.PriInstAmt);
                    epsdatabase.AddInParameter(dbCommand, "@IntTotInst", DbType.Int32, objLoanAppDetails.IntTotInst);
                    epsdatabase.AddInParameter(dbCommand, "@OddInstNoInt", DbType.Int32, objLoanAppDetails.OddInstNoInt);
                    epsdatabase.AddInParameter(dbCommand, "@OddInstAmtInt", DbType.Int32, objLoanAppDetails.OddInstAmtInt);
                    epsdatabase.AddInParameter(dbCommand, "@MsEmpID", DbType.Int32, objLoanAppDetails.MsEmpID);
                    epsdatabase.AddInParameter(dbCommand, "@payLoanRefLoanCD", DbType.Int32, objLoanAppDetails.payLoanRefLoanCD);
                    epsdatabase.AddInParameter(dbCommand, "@payLoanPurposeCode", DbType.Int32, objLoanAppDetails.payLoanPurposeCode);
                    epsdatabase.AddInParameter(dbCommand, "@Rule_18_1964_Pur", DbType.Boolean, objLoanAppDetails.Rule_18_1964_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@IntPurPrsnComp", DbType.Boolean, objLoanAppDetails.IntPurPrsnComp);
                    epsdatabase.AddInParameter(dbCommand, "@Anti_Price_Pc", DbType.String, objLoanAppDetails.Anti_Price_Pc);
                    epsdatabase.AddInParameter(dbCommand, "@Off_On_Leave", DbType.Boolean, objLoanAppDetails.Off_On_Leave);
                    epsdatabase.AddInParameter(dbCommand, "@Pre_Adv_Simi_Pur", DbType.Boolean, objLoanAppDetails.Pre_Adv_Simi_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@Adv_Draw_Date", DbType.DateTime, objLoanAppDetails.Adv_Draw_Date);
                    epsdatabase.AddInParameter(dbCommand, "@Adv_Amount", DbType.Int32, objLoanAppDetails.Adv_Amount);
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanPurposeID", DbType.Int32, objLoanAppDetails.payLoanPurposeCode);
                    epsdatabase.AddInParameter(dbCommand, "@Plth_Prop_Enlarge", DbType.Int32, objLoanAppDetails.Plth_Prop_Enlarge);
                    epsdatabase.AddInParameter(dbCommand, "@Comp_Certi_Info_1", DbType.Boolean, objLoanAppDetails.Comp_Certi_Info_1);
                    epsdatabase.AddInParameter(dbCommand, "@Comp_Certi_Info_2", DbType.Boolean, objLoanAppDetails.Comp_Certi_Info_2);
                    epsdatabase.AddInParameter(dbCommand, "@Upload_Sign", DbType.String, objLoanAppDetails.Upload_Sign);
                    epsdatabase.AddInParameter(dbCommand, "@Address", DbType.String, objLoanAppDetails.Address);
                    epsdatabase.AddInParameter(dbCommand, "@DesNoInsRePaidForAdv", DbType.Int32, objLoanAppDetails.DesNoInsRePaidForAdv);
                    epsdatabase.AddInParameter(dbCommand, "@Selct_Area", DbType.Boolean, objLoanAppDetails.Selct_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Demar_Dev", DbType.Boolean, objLoanAppDetails.Demar_Dev);
                    epsdatabase.AddInParameter(dbCommand, "@Area_Sq_Ft", DbType.Int32, objLoanAppDetails.Area_Sq_Ft);
                    epsdatabase.AddInParameter(dbCommand, "@Cost", DbType.Int32, objLoanAppDetails.Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Act_Pay", DbType.Int32, objLoanAppDetails.Amt_Act_Pay);
                    epsdatabase.AddInParameter(dbCommand, "@Purp_Acquire", DbType.String, objLoanAppDetails.Purp_Acquire);
                    epsdatabase.AddInParameter(dbCommand, "@Unexp_Port_Lease", DbType.String, objLoanAppDetails.Unexp_Port_Lease);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_1", DbType.Boolean, objLoanAppDetails.Pop_Decl_1);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_2", DbType.Boolean, objLoanAppDetails.Pop_Decl_2);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_3", DbType.Boolean, objLoanAppDetails.Pop_Decl_3);
                    epsdatabase.AddInParameter(dbCommand, "@Flr_Area", DbType.Int32, objLoanAppDetails.Flr_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Est_Cost", DbType.Int32, objLoanAppDetails.Est_Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Adv_Req", DbType.Int32, objLoanAppDetails.Amt_Adv_Req);
                    epsdatabase.AddInParameter(dbCommand, "@Num_Inst", DbType.Int32, objLoanAppDetails.Num_Inst);
                    epsdatabase.AddInParameter(dbCommand, "@Pur_Area", DbType.Int32, objLoanAppDetails.Pur_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_1", DbType.Boolean, objLoanAppDetails.Const_Dec_1);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_2", DbType.Boolean, objLoanAppDetails.Const_Dec_2);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_3", DbType.Boolean, objLoanAppDetails.Const_Dec_3);
                    epsdatabase.AddInParameter(dbCommand, "@Coc", DbType.Int32, objLoanAppDetails.Coc);
                    epsdatabase.AddInParameter(dbCommand, "@Plint_Area", DbType.Int32, objLoanAppDetails.Plint_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Cop_Enlarge", DbType.Int32, objLoanAppDetails.Cop_Enlarge);
                    epsdatabase.AddInParameter(dbCommand, "@Tot_Plint_Area", DbType.Int32, objLoanAppDetails.Tot_Plint_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Tot_Cost", DbType.Int32, objLoanAppDetails.Tot_Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_1", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_1);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_2", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_2);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_3", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_3);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Date", DbType.DateTime, objLoanAppDetails.Const_Date);
                    epsdatabase.AddInParameter(dbCommand, "@Prc_Settled", DbType.Int32, objLoanAppDetails.Prc_Settled);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Paid", DbType.Int32, objLoanAppDetails.Amt_Paid);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_1", DbType.Boolean, objLoanAppDetails.Dtls_Built_Flat_1);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_2", DbType.Boolean, objLoanAppDetails.Dtls_Built_flat_2);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_3", DbType.Boolean, objLoanAppDetails.Dtls_Built_flat_3);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, objLoanAppDetails.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, objLoanAppDetails.CreatedBy);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerName", DbType.String, objLoanAppDetails.OwnerName);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerPanNo", DbType.String, objLoanAppDetails.OwnerPanNo);
                    epsdatabase.AddInParameter(dbCommand, "@Descriptions", DbType.String, objLoanAppDetails.Descriptions);
                    epsdatabase.AddInParameter(dbCommand, "@BankIFSCCODE", DbType.String, objLoanAppDetails.BankIFSCCODE);
                    epsdatabase.AddInParameter(dbCommand, "@BankAccountNo", DbType.String, objLoanAppDetails.BankAccountNo);
                    epsdatabase.AddInParameter(dbCommand, "@Agency_Whom_Pur", DbType.String, objLoanAppDetails.Agency_Whom_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@PriVerifFlag", DbType.String, objLoanAppDetails.PriVerifFlag);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region Update Loan Details
        public async Task<int> UpdateLoanDetails(LoanApplicationDetailsModel objLoanAppDetails)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_UpdateLoanApplicationDetails))
                {

                    epsdatabase.AddInParameter(dbCommand, "@empid", DbType.Int32, objLoanAppDetails.MsEmpID);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, objLoanAppDetails.EmpCd);
                    epsdatabase.AddInParameter(dbCommand, "@LoanAmtSanc", DbType.Int32, objLoanAppDetails.LoanAmtSanc);
                    epsdatabase.AddInParameter(dbCommand, "@PriInstAmt", DbType.Int32, objLoanAppDetails.PriInstAmt);
                    epsdatabase.AddInParameter(dbCommand, "@IntTotInst", DbType.Int32, objLoanAppDetails.IntTotInst);
                    epsdatabase.AddInParameter(dbCommand, "@OddInstNoInt", DbType.Int32, objLoanAppDetails.OddInstNoInt);
                    epsdatabase.AddInParameter(dbCommand, "@OddInstAmtInt", DbType.Int32, objLoanAppDetails.OddInstAmtInt);
                    epsdatabase.AddInParameter(dbCommand, "@MsEmpID", DbType.Int32, objLoanAppDetails.MsEmpID);
                    epsdatabase.AddInParameter(dbCommand, "@payLoanRefLoanCD", DbType.Int32, objLoanAppDetails.payLoanRefLoanCD);
                    epsdatabase.AddInParameter(dbCommand, "@payLoanPurposeCode", DbType.Int32, objLoanAppDetails.payLoanPurposeCode);
                    epsdatabase.AddInParameter(dbCommand, "@Rule_18_1964_Pur", DbType.Boolean, objLoanAppDetails.Rule_18_1964_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@IntPurPrsnComp", DbType.Boolean, objLoanAppDetails.IntPurPrsnComp);
                    epsdatabase.AddInParameter(dbCommand, "@Anti_Price_Pc", DbType.String, objLoanAppDetails.Anti_Price_Pc);
                    epsdatabase.AddInParameter(dbCommand, "@Off_On_Leave", DbType.Boolean, objLoanAppDetails.Off_On_Leave);
                    epsdatabase.AddInParameter(dbCommand, "@Pre_Adv_Simi_Pur", DbType.Boolean, objLoanAppDetails.Pre_Adv_Simi_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@Adv_Draw_Date", DbType.DateTime, objLoanAppDetails.Adv_Draw_Date);
                    epsdatabase.AddInParameter(dbCommand, "@Adv_Amount", DbType.Int32, objLoanAppDetails.Adv_Amount);
                    epsdatabase.AddInParameter(dbCommand, "@PayLoanPurposeID", DbType.Int32, objLoanAppDetails.payLoanPurposeCode);
                    epsdatabase.AddInParameter(dbCommand, "@Comp_Certi_Info_1", DbType.Boolean, objLoanAppDetails.Comp_Certi_Info_1);
                    epsdatabase.AddInParameter(dbCommand, "@Comp_Certi_Info_2", DbType.Boolean, objLoanAppDetails.Comp_Certi_Info_2);
                    epsdatabase.AddInParameter(dbCommand, "@Upload_Sign", DbType.String, objLoanAppDetails.Upload_Sign);
                    epsdatabase.AddInParameter(dbCommand, "@Address", DbType.String, objLoanAppDetails.Address);
                    epsdatabase.AddInParameter(dbCommand, "@Selct_Area", DbType.Boolean, objLoanAppDetails.Selct_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Demar_Dev", DbType.Boolean, objLoanAppDetails.Demar_Dev);
                    epsdatabase.AddInParameter(dbCommand, "@Area_Sq_Ft", DbType.Int32, objLoanAppDetails.Area_Sq_Ft);
                    epsdatabase.AddInParameter(dbCommand, "@Cost", DbType.Int32, objLoanAppDetails.Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Act_Pay", DbType.Int32, objLoanAppDetails.Amt_Act_Pay);
                    epsdatabase.AddInParameter(dbCommand, "@Purp_Acquire", DbType.String, objLoanAppDetails.Purp_Acquire);
                    epsdatabase.AddInParameter(dbCommand, "@Unexp_Port_Lease", DbType.String, objLoanAppDetails.Unexp_Port_Lease);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_1", DbType.Boolean, objLoanAppDetails.Pop_Decl_1);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_2", DbType.Boolean, objLoanAppDetails.Pop_Decl_2);
                    epsdatabase.AddInParameter(dbCommand, "@Pop_Decl_3", DbType.Boolean, objLoanAppDetails.Pop_Decl_3);
                    epsdatabase.AddInParameter(dbCommand, "@Flr_Area", DbType.Int32, objLoanAppDetails.Flr_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Est_Cost", DbType.Int32, objLoanAppDetails.Est_Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Adv_Req", DbType.Int32, objLoanAppDetails.Amt_Adv_Req);
                    epsdatabase.AddInParameter(dbCommand, "@Num_Inst", DbType.Int32, objLoanAppDetails.Num_Inst);
                    epsdatabase.AddInParameter(dbCommand, "@Pur_Area", DbType.Int32, objLoanAppDetails.Pur_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_1", DbType.Boolean, objLoanAppDetails.Const_Dec_1);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_2", DbType.Boolean, objLoanAppDetails.Const_Dec_2);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Dec_3", DbType.Boolean, objLoanAppDetails.Const_Dec_3);
                    epsdatabase.AddInParameter(dbCommand, "@Plth_Prop_Enlarge", DbType.Int32, objLoanAppDetails.Plth_Prop_Enlarge);
                    epsdatabase.AddInParameter(dbCommand, "@DesNoInsRePaidForAdv", DbType.Int32, objLoanAppDetails.DesNoInsRePaidForAdv);
                    epsdatabase.AddInParameter(dbCommand, "@Coc", DbType.Int32, objLoanAppDetails.Coc);
                    epsdatabase.AddInParameter(dbCommand, "@Plint_Area", DbType.Int32, objLoanAppDetails.Plint_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Cop_Enlarge", DbType.Int32, objLoanAppDetails.Cop_Enlarge);
                    epsdatabase.AddInParameter(dbCommand, "@Tot_Plint_Area", DbType.Int32, objLoanAppDetails.Tot_Plint_Area);
                    epsdatabase.AddInParameter(dbCommand, "@Tot_Cost", DbType.Int32, objLoanAppDetails.Tot_Cost);
                    epsdatabase.AddInParameter(dbCommand, "@Agency_Whom_Pur", DbType.String, objLoanAppDetails.Agency_Whom_Pur);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_1", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_1);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_2", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_2);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Enlarg_3", DbType.Boolean, objLoanAppDetails.Dtls_Enlarg_3);
                    epsdatabase.AddInParameter(dbCommand, "@Const_Date", DbType.DateTime, objLoanAppDetails.Const_Date);
                    epsdatabase.AddInParameter(dbCommand, "@Prc_Settled", DbType.Int32, objLoanAppDetails.Prc_Settled);
                    epsdatabase.AddInParameter(dbCommand, "@Amt_Paid", DbType.Int32, objLoanAppDetails.Amt_Paid);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_1", DbType.Boolean, objLoanAppDetails.Dtls_Built_Flat_1);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_2", DbType.Boolean, objLoanAppDetails.Dtls_Built_flat_2);
                    epsdatabase.AddInParameter(dbCommand, "@Dtls_Built_Flat_3", DbType.Boolean, objLoanAppDetails.Dtls_Built_flat_3);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedIP", DbType.String, objLoanAppDetails.ClientIP);
                    epsdatabase.AddInParameter(dbCommand, "@CreatedBy", DbType.String, objLoanAppDetails.CreatedBy);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerName", DbType.String, objLoanAppDetails.OwnerName);
                    epsdatabase.AddInParameter(dbCommand, "@Descriptions", DbType.String, objLoanAppDetails.Descriptions);
                    epsdatabase.AddInParameter(dbCommand, "@OwnerPanNo", DbType.String, objLoanAppDetails.OwnerPanNo);
                    epsdatabase.AddInParameter(dbCommand, "@BankIFSCCODE", DbType.String, objLoanAppDetails.BankIFSCCODE);
                    epsdatabase.AddInParameter(dbCommand, "@BankAccountNo", DbType.String, objLoanAppDetails.BankAccountNo);
                    epsdatabase.AddInParameter(dbCommand, "@PriVerifFlag", DbType.String, objLoanAppDetails.PriVerifFlag);
                    result = epsdatabase.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
        #endregion

        #region Bind Loan type and purposes tatus
        public async Task<IEnumerable<PayLoanPurposeStatus>> BindLoantypeandpurposestatus(string loanBillID)
        {

            List<PayLoanPurposeStatus> listEmployeeModel = new List<PayLoanPurposeStatus>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.SP_GetEmpLoanTypeandPurpos))
                {
                    epsdatabase.AddInParameter(dbCommand, "@mode", DbType.String, '3');
                    epsdatabase.AddInParameter(dbCommand, "@LoanBillID", DbType.String, loanBillID);
                    using (IDataReader rdr = epsdatabase.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            PayLoanPurposeStatus objEmpModel = new PayLoanPurposeStatus();
                            objEmpModel.Status = Convert.ToString(rdr["Status"]);
                            objEmpModel.PayLoanRefLoanShortDesc = Convert.ToString(rdr["PayLoanRefLoanShortDesc"]);
                            objEmpModel.PayLoanPurposeDescription = Convert.ToString(rdr["PayLoanPurposeDescription"]);
                            listEmployeeModel.Add(objEmpModel);
                        }
                    }
                }
            });
            return listEmployeeModel;

        }
        #endregion


        #region Validate Cooling Period
        public async Task<bool> ValidateCoolingPeriod(int LoanCode, int PurposeCode, string EmpCd)
        {
            bool result = false;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand(EPSConstant.Usp_ValidateCoolingPeriod))
                {
                    epsdatabase.AddInParameter(dbCommand, "@LoanCode", DbType.Int16, LoanCode);
                    epsdatabase.AddInParameter(dbCommand, "@PurposeCode", DbType.Int16, PurposeCode);
                    epsdatabase.AddInParameter(dbCommand, "@EmpCd", DbType.String, EmpCd);

                    int rdr = Convert.ToInt32(epsdatabase.ExecuteScalar(dbCommand));

                    result = rdr > 0 ? true : false;
                }
            });

            return result;
        }
        #endregion

    }
}
