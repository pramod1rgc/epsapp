﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace EPS.DataAccessLayers
{
    public class payscaleDataAccessLayer
    {
        Database epsdatabase = null;
        Database database = null;
        DbCommand dbCommand = null;
        public payscaleDataAccessLayer()
        {
        }
        public payscaleDataAccessLayer(Database database)
        {
            epsdatabase = database;
        }
        #region Bind Commission Code
        /// <summary>
        /// Bind Commission Code
        /// </summary>
        /// <returns></returns>
        public IEnumerable<payscaleModel> BindCommissionCode()
        {
            List<payscaleModel> list = new List<payscaleModel>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_GetPayScaleCommissionCode);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    payscaleModel _payscale = new payscaleModel();
                    _payscale.MsCddirID = Convert.ToInt32(objReader["CddirCodeValue"]);
                    _payscale.CddirCodeText = objReader["cddirCodeText"].ToString();
                    list.Add(_payscale);
                }
            }
            return list;
        }
        #endregion
        #region Bind PayScale Group
        /// <summary>
        /// Bind PayScale Group
        /// </summary>
        /// <returns></returns>
        public IEnumerable<payscaleModel> BindPayScaleGroup()
        {
            List<payscaleModel> list = new List<payscaleModel>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetPayScaleGroup);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    payscaleModel _payscale = new payscaleModel();
                    _payscale.PaysGroup_id = Convert.ToInt32(objReader["CddirCodeValue"]);
                    _payscale.PaysGroup_Text = objReader["cddirCodeText"].ToString();
                    list.Add(_payscale);
                }
            }
            return list;
        }
        #endregion
        #region Pay Scale Code
        /// <summary>
        /// Pay Scale Code
        /// </summary>
        /// <param name="CddirCodeValue"></param>
        /// <returns></returns>
        public string PayScaleCode(string CddirCodeValue)
        {

            string PayScaleCode = string.Empty;
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_GetPayScaleCode);
            database.AddInParameter(dbCommand, "@CddirCodeValue", DbType.String, CddirCodeValue);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    PayScaleCode = objReader["PayScaleCode"].ToString();
                }
            }
            return PayScaleCode;
        }
        #endregion
        #region Delete
        /// <summary>
        /// soft delete Payscale master
        /// </summary>
        /// <param name="MsPayScaleID"></param>
        /// <returns></returns>
        public string Delete(int MsPayScaleID)
        {
            int i = 0;
            string message = string.Empty;
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.usp_DeletePayScale);
            database.AddInParameter(dbCommand, "@MsPayScaleID", DbType.Int32, MsPayScaleID);
            i = database.ExecuteNonQuery(dbCommand);
            if (i > 0)
            {
                message = "Deleted Successfully";
            }
            else { message = "Deletion failed"; }
            return message;
        }
        #endregion
        #region Get PayScale Details BY Commission Code
        /// <summary>
        ///  Get PayScale Details BY Commission Code
        /// </summary>
        /// <param name="CommCDID"></param>
        /// <returns></returns>
        public IEnumerable<payscaleModel> GetPayScaleDetailsBYComCode(string CommCDID)
        {
            List<payscaleModel> list = new List<payscaleModel>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetPayScaleDetails);
            database.AddInParameter(dbCommand, "@MsCddirID", DbType.Int32, CommCDID);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    payscaleModel _payscale = new payscaleModel();
                    _payscale.MsPayScaleID = Convert.ToInt32(objReader["MsPayScaleID"]);
                    _payscale.MsCddirID = Convert.ToInt32(objReader["PayScalePscPayCommCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscPayCommCD"]) : _payscale.MsCddirID = 0);
                    _payscale.Paysfm_id = Convert.ToInt32(objReader["PayScalePfmForCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePfmForCD"]) : _payscale.Paysfm_id = 0);
                    _payscale.PaysGroup_id = Convert.ToInt32(objReader["PayScalePscGroupCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGroupCD"]) : _payscale.PaysGroup_id = 0);
                    _payscale.PayScaleCode = objReader["PayScalePscScaleCD"].ToString();
                    _payscale.PayScaleLevel = objReader["PayScalePayLevel"].ToString();
                    _payscale.PayScaleGradePay = Convert.ToDecimal(objReader["PayScalePscGradePay"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGradePay"]) : _payscale.PayScaleGradePay = 0);
                    _payscale.PayScaleWithEffectFrom = objReader["PayScalePscWef"].ToString();
                    _payscale.PayScaledescription = objReader["PayScalePscDscr"].ToString();
                    _payscale.PayScalePscLoLimit = Convert.ToDecimal(objReader["PayScalePscLoLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscLoLimit"]) : _payscale.PayScalePscLoLimit = 0);
                    _payscale.PayScalePscInc1 = Convert.ToDecimal(objReader["PayScalePscInc1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc1"]) : _payscale.PayScalePscInc1 = 0);
                    _payscale.PayScalePscStage1 = Convert.ToDecimal(objReader["PayScalePscStage1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage1"]) : _payscale.PayScalePscStage1 = 0);
                    _payscale.PayScalePscInc2 = Convert.ToDecimal(objReader["PayScalePscInc2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc2"]) : _payscale.PayScalePscInc2 = 0);
                    _payscale.PayScalePscStage2 = Convert.ToDecimal(objReader["PayScalePscStage2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage2"]) : _payscale.PayScalePscStage2 = 0);
                    _payscale.PayScalePscInc3 = Convert.ToDecimal(objReader["PayScalePscInc3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc3"]) : _payscale.PayScalePscInc3 = 0);
                    _payscale.PayScalePscStage3 = Convert.ToDecimal(objReader["PayScalePscStage3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage3"]) : _payscale.PayScalePscStage3 = 0);
                    _payscale.PayScalePscInc4 = Convert.ToDecimal(objReader["PayScalePscInc4"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc4"]) : _payscale.PayScalePscInc4 = 0);
                    _payscale.PayScalePscUpLimit = Convert.ToDecimal(objReader["PayScalePscUpLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscUpLimit"]) : _payscale.PayScalePscUpLimit = 0);
                    list.Add(_payscale);
                }
            }
            return list;
        }
        #endregion
        #region Get PayScale Details
        /// <summary>
        /// Get PayScale Details
        /// </summary>
        /// <returns></returns>
        public IEnumerable<payscaleModel> GetPayScaleDetails()
        {

            List<payscaleModel> list = new List<payscaleModel>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetPayScaleDetails);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    payscaleModel _payscale = new payscaleModel();
                    _payscale.MsPayScaleID = Convert.ToInt32(objReader["MsPayScaleID"]);
                    _payscale.MsCddirID = Convert.ToInt32(objReader["PayScalePscPayCommCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscPayCommCD"]) : _payscale.MsCddirID = 0);
                    _payscale.Paysfm_id = Convert.ToInt32(objReader["PayScalePfmForCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePfmForCD"]) : _payscale.Paysfm_id = 0);
                    _payscale.PaysGroup_id = Convert.ToInt32(objReader["PayScalePscGroupCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGroupCD"]) : _payscale.PaysGroup_id = 0);
                    _payscale.PayScaleCode = objReader["PayScalePscScaleCD"].ToString();
                    _payscale.PayScaleLevel = objReader["PayScalePayLevel"].ToString();
                    _payscale.PayScaleGradePay = Convert.ToDecimal(objReader["PayScalePscGradePay"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGradePay"]) : _payscale.PayScaleGradePay = 0);
                    _payscale.PayScaleWithEffectFrom = objReader["PayScalePscWef"].ToString();
                    _payscale.PayScaledescription = objReader["PayScalePscDscr"].ToString();
                    _payscale.PayScalePscLoLimit = Convert.ToDecimal(objReader["PayScalePscLoLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscLoLimit"]) : _payscale.PayScalePscLoLimit = 0);
                    _payscale.PayScalePscInc1 = Convert.ToDecimal(objReader["PayScalePscInc1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc1"]) : _payscale.PayScalePscInc1 = 0);
                    _payscale.PayScalePscStage1 = Convert.ToDecimal(objReader["PayScalePscStage1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage1"]) : _payscale.PayScalePscStage1 = 0);
                    _payscale.PayScalePscInc2 = Convert.ToDecimal(objReader["PayScalePscInc2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc2"]) : _payscale.PayScalePscInc2 = 0);
                    _payscale.PayScalePscStage2 = Convert.ToDecimal(objReader["PayScalePscStage2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage2"]) : _payscale.PayScalePscStage2 = 0);
                    _payscale.PayScalePscInc3 = Convert.ToDecimal(objReader["PayScalePscInc3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc3"]) : _payscale.PayScalePscInc3 = 0);
                    _payscale.PayScalePscStage3 = Convert.ToDecimal(objReader["PayScalePscStage3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage3"]) : _payscale.PayScalePscStage3 = 0);
                    _payscale.PayScalePscInc4 = Convert.ToDecimal(objReader["PayScalePscInc4"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc4"]) : _payscale.PayScalePscInc4 = 0);
                    _payscale.PayScalePscUpLimit = Convert.ToDecimal(objReader["PayScalePscUpLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscUpLimit"]) : _payscale.PayScalePscUpLimit = 0);
                    list.Add(_payscale);
                }
            }
            return list;
        }
        #endregion      
        #region Edit PayScale Details
        /// <summary>
        /// Edit PayScale Details
        /// </summary>
        /// <param name="MsPayScaleID"></param>
        /// <returns></returns>
        public payscaleModel EditPayScaleDetails(string MsPayScaleID)
        {

            payscaleModel _payscale = new payscaleModel();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_PayScaleDetailsByID);
            database.AddInParameter(dbCommand, "@MsPayScaleID", DbType.Int32, MsPayScaleID);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    _payscale.MsPayScaleID = Convert.ToInt32(objReader["MsPayScaleID"]);
                    _payscale.MsCddirID = Convert.ToInt32(objReader["PayScalePscPayCommCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscPayCommCD"]) : _payscale.MsCddirID = 0);
                    _payscale.Paysfm_id = Convert.ToInt32(objReader["PayScalePfmForCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePfmForCD"]) : _payscale.Paysfm_id = 0);
                    _payscale.PaysGroup_id = Convert.ToInt32(objReader["PayScalePscGroupCD"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGroupCD"]) : _payscale.PaysGroup_id = 0);
                    _payscale.PayScaleCode = objReader["PayScalePscScaleCD"].ToString();
                    _payscale.PayScaleLevel = objReader["PayScalePayLevel"].ToString();
                    _payscale.PayScaleGradePay = Convert.ToDecimal(objReader["PayScalePscGradePay"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscGradePay"]) : _payscale.PayScaleGradePay = 0);
                    _payscale.PayScaleWithEffectFrom = objReader["PayScalePscWef"].ToString();
                    _payscale.PayScaledescription = objReader["PayScalePscDscr"].ToString();
                    _payscale.PayScalePscLoLimit = Convert.ToDecimal(objReader["PayScalePscLoLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscLoLimit"]) : _payscale.PayScalePscLoLimit = 0);
                    _payscale.PayScalePscInc1 = Convert.ToDecimal(objReader["PayScalePscInc1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc1"]) : _payscale.PayScalePscInc1 = 0);
                    _payscale.PayScalePscStage1 = Convert.ToDecimal(objReader["PayScalePscStage1"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage1"]) : _payscale.PayScalePscStage1 = 0);
                    _payscale.PayScalePscInc2 = Convert.ToDecimal(objReader["PayScalePscInc2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc2"]) : _payscale.PayScalePscInc2 = 0);
                    _payscale.PayScalePscStage2 = Convert.ToDecimal(objReader["PayScalePscStage2"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage2"]) : _payscale.PayScalePscStage2 = 0);
                    _payscale.PayScalePscInc3 = Convert.ToDecimal(objReader["PayScalePscInc3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc3"]) : _payscale.PayScalePscInc3 = 0);
                    _payscale.PayScalePscStage3 = Convert.ToDecimal(objReader["PayScalePscStage3"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscStage3"]) : _payscale.PayScalePscStage3 = 0);
                    _payscale.PayScalePscInc4 = Convert.ToDecimal(objReader["PayScalePscInc4"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscInc4"]) : _payscale.PayScalePscInc4 = 0);
                    _payscale.PayScalePscUpLimit = Convert.ToDecimal(objReader["PayScalePscUpLimit"] != DBNull.Value ? Convert.ToInt32(objReader["PayScalePscUpLimit"]) : _payscale.PayScalePscUpLimit = 0);
                }
            }
            return _payscale;
        }
        #endregion     
        #region Bind PayScale Format
        /// <summary>
        ///  Bind PayScale Format
        /// </summary>
        /// <returns></returns>
        public IEnumerable<payscaleModel> BindPayScaleFormat()
        {
            List<payscaleModel> list = new List<payscaleModel>();
            database = epsdatabase;
            dbCommand = database.GetStoredProcCommand(EPSConstant.sp_GetPayScaleformat);
            using (IDataReader objReader = database.ExecuteReader(dbCommand))
            {
                while (objReader.Read())
                {
                    payscaleModel _payscale = new payscaleModel();
                    _payscale.Paysfm_id = Convert.ToInt32(objReader["pfm_cd"]);
                    _payscale.Paysfm_Text = objReader["pfm_desc"].ToString();
                    list.Add(_payscale);
                }
            }
            return list;
        }
        #endregion
        #region Bind PayScale GisGroup
        //public IEnumerable<payscaleModel> BindPayScaleGisGroup()
        //{
        //    try
        //    {
        //        List<payscaleModel> list = new List<payscaleModel>();
        //        using (SqlConnection conn = new SqlConnection(con))
        //        {
        //            SqlCommand cmd = new SqlCommand(EPSConstant.sp_GetPayGISGroup, conn);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            conn.Open();
        //            SqlDataReader rdr = cmd.ExecuteReader();
        //            while (rdr.Read())
        //            {
        //                payscaleModel _payscale = new payscaleModel();
        //                _payscale.PayGISGroupID = Convert.ToInt32(rdr["CddirCodeValue"]);
        //                _payscale.PayGISGroupName = rdr["cddirCodeText"].ToString();

        //                list.Add(_payscale);
        //            }
        //            conn.Close();
        //        }
        //        return list;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion
        #region Insert And Update PayScale Record
        /// <summary>
        ///  Insert And Update PayScale Record
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>
        public string CreatePayScale(payscaleModel payscale)
        {
            string message = string.Empty;
            string hostName = System.Net.Dns.GetHostName(); // Retrive the Name of HOST  
            string myIP = GetIP();// Dns.GetHostByName(hostName).AddressList[1].ToString();
            string Response = null;       
                database = epsdatabase;
                dbCommand = database.GetStoredProcCommand(EPSConstant.sp_CreatePayScale);
                database.AddInParameter(dbCommand, "@MsPayScaleID", DbType.Int32, payscale.MsPayScaleID);
                database.AddInParameter(dbCommand, "@PayScalePscPayCommCD", DbType.Int32, payscale.MsCddirID);
                database.AddInParameter(dbCommand, "@PayScalePfmForCD", DbType.Int32, payscale.Paysfm_id);// = MstRole.RoleID;
                database.AddInParameter(dbCommand, "@PayScalePscGroupCD", DbType.Int32, payscale.PaysGroup_id);
                database.AddInParameter(dbCommand, "@PayScalePscScaleCD", DbType.String, payscale.PayScaleCode);
                database.AddInParameter(dbCommand, "@PayScalePayLevel", DbType.String, payscale.PayScaleLevel);
                database.AddInParameter(dbCommand, "@PayScalePscGradePay", DbType.String, payscale.PayScaleGradePay);
                database.AddInParameter(dbCommand, "@PayScalePscGisGroupCD", DbType.Int32, payscale.PayGISGroupID);
                database.AddInParameter(dbCommand, "@PayScalePscWef", DbType.String, payscale.PayScaleWithEffectFrom);
                database.AddInParameter(dbCommand, "@PayScalePscDscr", DbType.String, payscale.PayScaledescription);
                database.AddInParameter(dbCommand, "@PayScalePscLoLimit", DbType.Decimal, payscale.PayScalePscLoLimit);
                database.AddInParameter(dbCommand, "@PayScalePscInc1", DbType.Decimal, payscale.PayScalePscInc1);
                database.AddInParameter(dbCommand, "@PayScalePscStage1", DbType.Decimal, payscale.PayScalePscStage1);
                database.AddInParameter(dbCommand, "@PayScalePscInc2", DbType.Decimal, payscale.PayScalePscInc2);
                database.AddInParameter(dbCommand, "@PayScalePscStage2", DbType.Decimal, payscale.PayScalePscStage2);
                database.AddInParameter(dbCommand, "@PayScalePscInc3", DbType.Decimal, payscale.PayScalePscInc3);
                database.AddInParameter(dbCommand, "@PayScalePscStage3", DbType.Decimal, payscale.PayScalePscStage3);
                database.AddInParameter(dbCommand, "@PayScalePscInc4", DbType.Decimal, payscale.PayScalePscInc4);
                database.AddInParameter(dbCommand, "@PayScalePscUpLimit", DbType.Decimal, payscale.PayScalePscUpLimit);
                database.AddInParameter(dbCommand, "@IpAddress", DbType.String, myIP);
                int i = 0;          
                    i = database.ExecuteNonQuery(dbCommand);
                    if (i > 0 && payscale.MsPayScaleID > 0)
                    {
                        Response = "Updated Successfully";
                    }
                    else if (i > 0)
                    {
                        Response = "Saved Successfully";
                    }
            return Response;
        }
        #endregion
        private string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();

        }
    }
}
