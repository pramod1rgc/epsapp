﻿using EPS.BusinessModels.Change;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Change
{
    public interface IExtensionOfServiceRepository : IDisposable
    {
        Task<IEnumerable<ddlvalue>> GetDdlvalue(string Flag);
        Task<int> ChangeDeleteDetails(string empCd, string orderNo, string Flag);
        Task<string> InsertEOSDetailsBAL(ExtensionOfServiceModel1 objPro, string Flag);
        Task<IEnumerable<ExtensionOfServiceModelText>> GetExtensionOfServiceDetailsBAL(string empcode, int roleId);
        Task<string> ForwardToCheckerUserDtls(string empCd, string orderNo, string status, string rejectionRemark);
    }
}
