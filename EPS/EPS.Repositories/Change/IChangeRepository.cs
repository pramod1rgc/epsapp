﻿using EPS.BusinessModels.Change;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Change
{
    public interface IChangeRepository : IDisposable
    {
        Task<IEnumerable<ddlvalue>> GetDdlvalue(string Flag);
        Task<int> ChangeDeleteDetails(string empCd, string orderNo, string Flag);

        Task<string> InsertDOBDetails(DOBcls objPro, string Flag);
        Task<IEnumerable<DOBcls>> GetDOBDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerDOBUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertNameGenderDetails(NameGender objPro, string Flag);
        Task<IEnumerable<NameGender>> GetNameGenderDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerNameGenderUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertPfNpsFormDetails(PFNPS objPro, string Flag);
        Task<IEnumerable<PFNPS>> GetPfNpsFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerPfNpsUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertPanNoFormDetails(PANNumber objPro, string Flag);
        Task<IEnumerable<PANNumber>> GetPanNoFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerPanNoUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertHraccFormDetails(HRAcityclass objPro, string Flag);
        Task<IEnumerable<HRAcityclass>> GetHraccFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerHraccUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertdesignationFormDetails(designationcls objPro, string Flag);
        Task<IEnumerable<designationcls>> GetdesignationFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerdesignationUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertexservicemenFormDetails(exservicemencls objPro, string Flag);
        Task<IEnumerable<exservicemencls>> GetexservicemenFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerexservicemenUserDtls(string empCd, string orderNo, string status, string rejectionRemark);
        
        Task<string> InsertCGHSFormDetails(CGHScls objPro, string Flag);
        Task<IEnumerable<CGHScls>> GetCGHSFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerCGHSUserDtls(string empCd, string orderNo, string status, string rejectionRemark);
        
        Task<string> InsertCGEGISFormDetails(CGEGIScls objPro, string Flag);
        Task<IEnumerable<CGEGIScls>> GetCGEGISFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerCGEGISFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertJoiningModeFormDetails(joiningmodecls objPro, string Flag);
        Task<IEnumerable<joiningmodecls>> GetJoiningModeFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerJoiningModeFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertDOJDetails(DOJcls objPro, string Flag);
        Task<IEnumerable<DOJcls>> GetDOJDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerDOJUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertDOEDetails(DOEcls objPro, string Flag);
        Task<IEnumerable<DOEcls>> GetDOEDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerDOEUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertDORDetails(DORcls objPro, string Flag);
        Task<IEnumerable<DORcls>> GetDORDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerDORUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertCasteCategoryFormDetails(CasteCategorycls objPro, string Flag);
        Task<IEnumerable<CasteCategorycls>> GetCasteCategoryFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerCasteCategoryFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertEntofficevehicleFormDetails(Entofficevehiclecls objPro, string Flag);
        Task<IEnumerable<Entofficevehiclecls>> GetEntofficevehicleFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerEntofficevehicleFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertBankFormDetails(BankFormcls objPro, string Flag);
        Task<IEnumerable<BankFormcls>> GetBankFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerBankFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertaddtapdFormDetails(addtapdcls objPro, string Flag);
        Task<IEnumerable<addtapdcls>> GetaddtapdFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckeraddtapdUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertTPTAFormDetails(TPTAcls objPro, string Flag);
        Task<IEnumerable<TPTAcls>> GetTPTAFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerTPTAUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        Task<string> InsertStateGISFormDetails(StateGIScls objPro, string Flag);
        Task<IEnumerable<StateGIScls>> GetStateGISFormDetails(string empcode, int roleId, string Flag);
        Task<string> ForwardToCheckerStateGISUserDtls(string empCd, string orderNo, string status, string rejectionRemark);

        //
        Task<string> InsertMobileNoEmailEmpCodeDetails(ConMobileNoEmailEmpCodecls objPro, string flag, string Contactdetails);
        Task<IEnumerable<ConMobileNoEmailEmpCodecls>> GetMobileNoEmailEmpCodeDetails(string empcode, int roleId, string Flag, string ContactdetailsSelectedOption);
        Task<string> ForwardToCheckerMobileNoEmailEmpCodeUserDtls(string empCd, string orderNo, string status, string rejectionRemark, string ContactdetailsSelectedOption);

    }
}
