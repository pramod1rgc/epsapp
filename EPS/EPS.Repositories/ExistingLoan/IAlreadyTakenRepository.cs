﻿using EPS.BusinessModels.ExistingLoan;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.ExistingLoan
{
    public interface IAlreadyTakenRepository : IDisposable
    {
        Task<IEnumerable<tblMsPayLoanRef>> GetAllDesignation();
        Task<IEnumerable<tblMsPayLoanRef>> BindDropDownLoanType();
        Task<LoanAdvanceDetailsModel> GetAlreadyTakenLoanAdvanceDetails(int PermDdoId, string EmpCode);
        Task<HeadAccountModel> GetHeadAccount(int LoanCD);
        Task<int> SaveLoanData(ExistingLoanApplicationModel _obj);
        Task<IEnumerable<LoanAdvanceDetailsModel>> GetExistLoanDetailsByID(string MSPayAdvEmpdetID);
        Task<IEnumerable<LoanAdvanceDetailsModel>> GetExistingEmpLoanDetails(string PayBillGpID);
        Task<IEnumerable<ExistingLaonStatusModel>> BindExistingLoanStatus(int UserId,int ddoId, int userRoleID);
        Task<int> ExistingLoanForwordToDDO(ExistingLoanApplicationModel _obj);
        //Task<IEnumerable<LoanAdvanceDetailsModel>> GetEmpLoanListDetails(string PayBillGpID);
        Task<int> DeleteEmpDetails(string CtrCode, string msPayAdvEmpdetID,int Mode);

    }
}
