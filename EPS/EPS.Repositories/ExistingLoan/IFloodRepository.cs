﻿using EPS.BusinessModels.ExistingLoan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.ExistingLoan
{
    public interface IFloodRepository :IDisposable
    {
        Task<IEnumerable<FloodModel>> GetEmpDetails(int BillGrID, string desigId, int UserID, int mode);
        Task<int> InsertFloodData(SanctionDetailsMOdel ObjSanctionDetails);
        Task<int> ForwordToDdo(SanctionDetailsMOdel ObjSanctionDetails);
    }
}
