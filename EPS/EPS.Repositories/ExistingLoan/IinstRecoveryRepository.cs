﻿using EPS.BusinessModels.ExistingLoan;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.ExistingLoan
{
   public interface IinstRecoveryRepository :IDisposable
    {
        Task<IEnumerable<RecoveryInstModel>> GetEmpDetails(string EmpCD);
        Task<IEnumerable<SanctionDetailsModel>> BindSanctionDetails(string LoanCd, string EmpCd);
        Task<int> SaveSanctionData(SnactionDataModel ObjSnactionModel);
        Task<int> ForwardtoDDOChecker(SnactionDataModel ObjSnactionModel);
        Task<IEnumerable<InstEmpModel>> GetInstEmpDetails(string LoanCd);
        Task<int> Accept(string EmpCD);
        Task<int> Reject(string EmpCD);
        Task<int> EditLoanDetailsById(SnactionDataModel ObjModel);
        Task<int> DeleteLoanDetailsById(int msEmpLoanMultiInstId, string empCD);
    }
}
