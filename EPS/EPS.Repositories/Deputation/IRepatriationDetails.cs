﻿using EPS.BusinessModels.Deputation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Deputation
{
   public  interface IRepatriationDetails
    {
        Task<IEnumerable<RepatriationDetailsModel>> GetAllRepatriationDetailsByEmployee(string empCd, int roleId);
        Task<DeputationInExistEmpModel> CheckDepuatationinEmployee(string empCd, int roleId);
        Task<string> InsertUpdateRepatriationDetails(RepatriationDetailsModel objRepatriationDetails);
    }
}
