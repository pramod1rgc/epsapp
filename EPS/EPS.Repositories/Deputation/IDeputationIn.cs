﻿using EPS.BusinessModels.Deputation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Deputation
{
    public interface IDeputationIn
    {
        Task<IEnumerable<DeputationInModel>> GetPayItems();
        Task<IEnumerable<Designation>> GetAllDesignation(string controllerId);
        Task<IEnumerable<DeputationInModel>> GetCommunicationAddress();
        Task<DeputationInModel> GetDeductionScheduleDetails(string EmpCd, int roleId);
        Task<SchemeCode> GetScheme_CodeByMsEmpDuesID(int MsEmpDuesID);
        Task<int> InsertUpdateDeputationInDetails(DeputationInModel objDeputationIn);
        Task<int> UpdateforwardStatusUpdateByDepId(int empDeputDetailsId, string status, string rejectionRemark);
    }
}
