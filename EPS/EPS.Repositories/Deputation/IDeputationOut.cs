﻿using EPS.BusinessModels.Deputation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Deputation
{
   public interface IDeputationOut
    {
        Task<IEnumerable<DeputationOutModel>> GetAllDepuatationOutDetailsByEmployee(string EmpCd, int roleId);
        Task<string> InsertUpdateDeputationOutDetails(DeputationOutModel objDeputationOut);
    }
}
