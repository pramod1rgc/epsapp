﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface IPostingDetailsRepository
    {
        Task<IEnumerable<PostingDetailsModel>> GetHRACity();
        Task<IEnumerable<PostingDetailsModel>> GetTACity();
        Task<IEnumerable<PostingDetailsModel>> GetAllDesignation();
        Task<int> SaveUpdatePostingDetails(PostingDetailsModel objPostingDetailsModel);
        Task<PostingDetailsModel> GetAllPostingDetails(string Empcd, int roleId);
    }
}
