﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface IPayDetailsRepository
    {
        Task<IEnumerable<PayDetailsModel>> GetOrganisationType(string empCode);
        Task<IEnumerable<PayDetailsModel>> GetPayLevel();
        Task<IEnumerable<PayDetailsModel>> GetEntitledOffVeh(string module);
        Task<IEnumerable<PayDetailsModel>> GetPayIndex(string payLevel);
        Task<IEnumerable<PayDetailsModel>> GetBasicDetails(string payLevel, int payIndex);
        Task<IEnumerable<PayDetailsModel>> GetGradePay();
        Task<IEnumerable<PayDetailsModel>> GetPayScale(int gradePay);
        Task<IEnumerable<NonComputationalDues>> GetNonComputationalDues();
        Task<IEnumerable<NonComputationalDeduction>> GetNonComputationalDeductions();
        Task<PayDetailsModel> GetPayDetails(string empCode, int roleId);
        Task<int> SaveUpdatePayDetails(PayDetailsModel objPayDetailsModel);


    }
}
