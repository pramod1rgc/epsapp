﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace EPS.Repositories.EmployeeDetails
{
   public interface IBankDetailsRepository 
    {  
         Task<BankDetailsModel> GetBankDetailsByIFSC(string ifscCD);

        IEnumerable<BankDetailsModel> GetAllIFSC();

         Task<BankDetailsModel> GetBankDetails(string empCD, int roleId);

        Task<int> UpdateBankDetails(BankDetailsModel objBankDetails);
    }
}
