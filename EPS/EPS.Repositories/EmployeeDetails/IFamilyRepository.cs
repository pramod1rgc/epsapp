﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
    public interface IFamilyRepository
    {
        Task<List<FamilyDetailsModel>> GetMaritalStatus();

        Task<FamilyDetailsModel> GetFamilyDetails(string empCd, int msDependentDetailID);

        Task<List<FamilyDetailsModel>> GetAllFamilyDetails(string empCd, int roleId);

        Task<int> UpdateEmpFamilyDetails(FamilyDetailsModel objFamilyDetails);

        Task<int> DeleteFamilyDetails(string empCd, int msDependentDetailID);
    }
}
