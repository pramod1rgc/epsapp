﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface IQuaterDetailsRepository
    {
        Task<IEnumerable<QuarterDetailsModel>> GetQuarterOwnedby();
        Task<IEnumerable<QuarterDetailsModel>> GetAllottedTo();
        Task<IEnumerable<QuarterDetailsModel>> GetRentStatus();
        Task<IEnumerable<QuarterDetailsModel>> GetQuarterType();
        Task<IEnumerable<QuarterDetailsModel>> GetCustodian(int qrtrOwnedBy);
        Task<IEnumerable<QuarterDetailsModel>> GetGPRACityLocation();
        Task<IEnumerable<QuarterDetailsModel>> QuarterAllDetails(string empCd, int roleId);
        Task<QuarterDetailsModel> QuarterDetails(string empCd, int mSEmpAccmID);
        Task<int> SaveUpdateQuarterDetails(QuarterDetailsModel objQuarterDetailsModel);
        Task<int> DeleteQuarterDetails(string empCd, int mSEmpAccmID);
    }
}
