﻿using EPS.BusinessModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface IMasterRepository
    {
        Task<IEnumerable<MasterModel>> GetRelation();
        Task<IEnumerable<JoiningTypeMaster>> GetJoiningMode(Boolean? IsDeput, int? MsEmpSubTypeID);
        Task<IEnumerable<JoiningTypeMaster>> GetJoiningCategory(int parantTypeId, Boolean? IsDeput);
        Task<IEnumerable<MasterModel>> GetSalutation();
        Task<IEnumerable<EmployeeTypeMaster>> GetEmpSubType(int parantTypeId, Boolean? IsDeput);
        Task<IEnumerable<EmployeeTypeMaster>> GetEmpType(int? serviceId, int? deputationId);
        Task<IEnumerable<MasterModel>> GetDeputationType(int? serviceId);
        Task<IEnumerable<ServiceTypeMaster>> GetServiceType(Boolean? IsDeput);
        Task<IEnumerable<GenderMaster>> GetGender();
        Task<IEnumerable<StateMaster>> GetState();
        Task<IEnumerable<PfTypeMaster>> GetPfType();
        Task<IEnumerable<JoiningAccountOf>> GetJoiningAccount();
        Task<IEnumerable<OfficeList>> GetOfficeList(int controllerId, int? ddoId);
        Task<IEnumerable<OfficeCityClass>> GetOfficeCityClassList();
        Task<IEnumerable<PayCommission>> GetPayCommissionList();
    }
}
