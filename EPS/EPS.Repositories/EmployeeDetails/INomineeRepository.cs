﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface INomineeRepository
    {
        Task<NomineeModel> GetNomineeDetails(string empCd, int nomineeID);

        Task<IEnumerable<NomineeModel>> GetAllNomineeDetails(string empcode, int roleID);

        Task<int> UpdateNomineeDetails(NomineeModel objNomineeDetails);

        Task<int> DeleteNomineeDetails(string empCd, int nomineeID);
    }
}
