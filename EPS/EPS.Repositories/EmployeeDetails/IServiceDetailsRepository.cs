﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
  public interface IServiceDetailsRepository
    {
        Task<IEnumerable<ServiceDetailsModel>> GetMaintainByOfc();
        Task<ServiceDetailsModel> GetAllServiceDetails(string empCode, int roleId);
        Task<int> SaveUpdateServiceDetails(ServiceDetailsModel objServiceDetailsModel);
    }
}
