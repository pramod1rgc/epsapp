﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface ICGEGISRepository
    {
        Task<IEnumerable<CGEGISModel>> GetInsuranceApplicable(string EmpCode);

        Task<IEnumerable<CGEGISModel>> GetCGEGISCategory(int insuranceApplicableId, string EmpCode);

        Task<int> SaveUpdateCGEGISDetails(CGEGISModel objCGEGISModel);

        Task<CGEGISModel> GetCGEGISDetails(string EmpCode, int RoleId);
    }
}
