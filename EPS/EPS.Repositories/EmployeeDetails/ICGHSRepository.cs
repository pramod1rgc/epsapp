﻿using EPS.BusinessModels.EmployeeDetails;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface ICGHSRepository
    {
        Task<int> SaveUpdateCGHSDetails(CGHSModel objCGHSModel);

        Task<CGHSModel> GetCGHSDetails(string empCd, int roleId);
    }
}
