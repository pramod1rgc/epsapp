﻿using EPS.BusinessModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.EmployeeDetails
{
   public interface IEmployeeDetailsRepository 
    {    
        Task<List<EmployeeDetailsModel>> GetVerifyEmpCode();

        Task<IEnumerable<EmployeeDetailsModel>> GetPhysicalDisabilityTypes();

        Task<EmployeeDetailsModel> GetEmpPersonalDetailsByID(string EmpCd, int RoleId);

        Task<List<EmployeeDetailsModel>> GetEmpPHDetails(string EmpCd);

        Task<IEnumerable<EmployeeDetailsModel>> MakerGetEmpList(int RoleId);

        Task<int> UpdateEmpDetails(EmployeeDetailsModel ObjEmpDetails);

        Task<int> SavePHDetails(EmployeeDetailsModel ObjEmpDetails);

        Task<int> InsertUpdateEmpDetails(EmployeeDetailsModel ObjEmpDetails);

        Task<int> DeleteEmployeeDetails(string EmpCd);

        Task<bool> GetIsDeputEmp(string EmpCd);

        Task<int> VerifyRejectionEmployeeDtl(EmployeeDetailsModel ObjEmpDetails);

    }
}
