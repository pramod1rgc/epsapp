﻿using EPS.BusinessModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IDDOAdminRepository
    {
        Task<IEnumerable<DropDownListModel>> GetAllDDO(string PAOID, string query);
        Task<IEnumerable<EmployeeModel>> AssignedDDO(string DDOID);
        Task<IEnumerable<EmployeeModel>> AssignedEmpDDO(string DDOID);
        Task<IEnumerable<EmployeeModel>> EmployeeList(string DDOID);
        Task<string> Assigned(string empCd, string DDOID, string active, string Password);
    }
}
