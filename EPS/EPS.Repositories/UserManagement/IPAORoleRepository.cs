﻿using EPS.BusinessModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IPAORoleRepository
    {
        Task<IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query);
        Task<IEnumerable<EmployeeModel>> GetAllDDOsUnderSelectedPAOs(string msPAOID);
        Task<IEnumerable<EmployeeModel>> AssignedPAOEmp(string msPAOID);
        Task<IEnumerable<EmployeeModel>> AssignedEmpList(string msPAOID);
        Task<string> Assigned(string empCd, int msPAOID, string active, string Password);
    }
}
