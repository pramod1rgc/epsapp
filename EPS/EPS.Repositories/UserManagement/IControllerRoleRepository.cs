﻿using EPS.BusinessModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IControllerRoleRepository
    {
        Task<List<EmployeeModel>> AssignedEmp(string msControllerID);
        Task<List<DropDownListModel>> GetAllControllers(string controllerID, string query);
        Task<IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query);
        Task<IEnumerable<EmployeeModel>> ControllerroleSelectchanged(string msControllerID, string PAOID, string DDOID);
        Task<string> ControllerAssigned(string empCd, int msControllerID, string active, string Password);
        Task<IEnumerable<UserType>> GetAllUserRoles(string username);
    }
}
