﻿using EPS.BusinessModels.UserManagment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IHOORoleRepository
    {
        Task<object> HooCheckerEmpList(int DDOID);
        Task<object> hooMakerEmpList(int DDOID);
        Task<string> AssignedHOOChecker(string empCd, int DDOID, string active, string Password);
        Task<string> AssignedHOOMaker(string empCd, int DDOID, string active, string Password);
        Task<string> SelfAssignedHOOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign);
        Task<IEnumerable<AssignChecked>> SelfAssignHOOCheckerRole(string UserID);

    }
}
