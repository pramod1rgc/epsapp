﻿using EPS.BusinessModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface ILoginRepository
    {
        Task<string> LoginCheck(LoginModel loginModel);
        Task<List<userDetails>> UserDetails(string username);
        Task<string> LoginNewUser(LoginModel loginModel);
        Task<string> RoleActivation(int ID);
        Task<bool> IsMenuPermissionAssigned(string Username, string uroleid);
    }
}
