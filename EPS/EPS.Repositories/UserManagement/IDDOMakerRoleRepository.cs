﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IDDOMakerRoleRepository
    {
        Task<IEnumerable<EmployeeModel>> AssignedmakerEmpList(int DDOID);
        Task<IEnumerable<EmployeeModel>> makerEmpList(int DDOID);
        Task<string> AssignedMaker(string empCd, int DDOID, int UserID, string active, string Password);
        Task<string> SelfAssignedDDOMaker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign);
        Task<IEnumerable<AssignChecked>> SelfAssignMakerRole(string UserID);
    }
}
