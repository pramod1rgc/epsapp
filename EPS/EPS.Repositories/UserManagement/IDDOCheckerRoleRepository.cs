﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.UserManagement
{
    public interface IDDOCheckerRoleRepository
    {
        Task<IEnumerable<EmployeeModel>> AssignedDDOCheckerEmployeeList(int DDOID);
        Task<IEnumerable<EmployeeModel>> EmployeeListByDDO(int DDOID);
        Task<string> AssignedCMD(string empCd, int RoleID, int DDOID, int UserID, string active, string Password);
        Task<string> SelfAssignDDOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign);
        Task<IEnumerable<AssignChecked>> CheckedSelfAssignDDO(string UserID);
    }
}
