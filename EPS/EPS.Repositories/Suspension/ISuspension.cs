﻿using EPS.BusinessModels.Shared;
using EPS.BusinessModels.Suspension;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Suspension
{
    public interface ISuspension 
    {

        Task<string> SavedNewSuspensionDetails(SuspensionDetails obj);

        Task<IEnumerable<DesignationModel>> GetAllDesignation(string id);

        Task<IEnumerable<EmpModel>> GetAllEmployees(int desigId, bool isCheckerLogin, string comName);

        Task<IEnumerable<SuspensionDetails>> GetSuspensionDetails(string empCode, bool isCheckerLogin, string comName);

        Task<string> DeleteSuspensionDetails(int id, string compName);

        Task<string> VerifiedAndRejectedSuspension(int id, string status, string reason, string compName);

        Task<string> SavedExtensionDetails(ExtensionDetails obj);

        Task<string> SavedRevocationDetails(RevocationDetails objRevok);

        Task<IEnumerable<RevocationDetails>> GetRevocationDetails(int susId, int empId, bool isCheckerLogin);

        Task<string> SavedJoiningDetails(JoiningDetails objJoining);

        Task<IEnumerable<JoiningDetails>> GetJoiningDetails(int joinId, bool isCheckerLogin);

        Task<string> SavedRegulariseDetails(RegulariseDetailse objReg);

        Task<IEnumerable<RegulariseDetailse>> GetRegulariseDetails(int joiningId, bool isCheckerLogin);


    }
}
