﻿using EPS.BusinessModels.LienPeriod;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.LienPeriod
{
    public interface INonEpsEmployeejoinAfterLienPeriod
    {
        Task<IEnumerable<NonEpsEmpJoinAfterLienPeriodeModel>> GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId);
        Task<string> SaveUpdateNonEpsEmployeeJoinAfterLienPeriod(NonEpsEmpJoinAfterLienPeriodeModel objmodel);
        Task<int> DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(string empCd, string orderNo);
        Task<int> NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark);
    }
}
