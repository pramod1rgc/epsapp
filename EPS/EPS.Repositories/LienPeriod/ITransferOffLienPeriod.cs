﻿using EPS.BusinessModels.LienPeriod;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.LienPeriod
{
   public interface ITransferOffLienPeriod
    {
        Task<IEnumerable<ControllerList>> GetControllerList();
        Task<IEnumerable<DDOList>> GetDdolistbyControllerid(int controllerId);
        Task<IEnumerable<OfficeAddressDetails>> GetOfficeAddressDetails(int controllerId, int ddoId, string officeName);
        Task<IEnumerable<TransferofflienperiodModel>> GetTransferofficelienPeriodDetails(string empCd, int roleId);
        Task<string> InsertUpdateTransferOffLienPeriod(TransferofflienperiodModel objTransferofflienPeriod);
        Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark);
        Task<int> DeleteTransferOffLienPeriod(string empCd, string orderNo);
    }
}
