﻿using EPS.BusinessModels.LienPeriod;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.LienPeriod
{
  public  interface IEpsEmpJoinAfterLienPeriod
    {
        Task<IEnumerable<EpsEmpJoinAfterLienPeriodModel>> GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId, int controllerId);
        Task<string> InsertUpdateEpsEmployeeJoinAfterLienPeriod(EpsEmpJoinAfterLienPeriodModel objmodel);
        Task<int> DeleteEpsEmployeeJoinByEmpCode(int lienId);
        Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark);
    }
}
