﻿using EPS.BusinessModels.Leaves;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace EPS.Repositories.LeaveMangement
{
   public interface ILeaveManagementRepository:IDisposable
    {
        Task<IEnumerable<DesignationModel>> GetAllDesignation(string PermDdoId);
        Task<IEnumerable<EmpModel>> GetEmployeeByDesig(string MsDesignID);
        Task<IEnumerable<OrderModel>> GetOrderByEmployee(string EmpCd, string userRoleId, string pageCode);
        Task<IEnumerable<LeavesTypeModel>> GetLeavesType();
        Task<IEnumerable<LeavesReasonModel>> GetLeavesReason();

        Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanction(string permDdoId, string empCode, string userRoleId);
        Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanctionHistory(string permDdoId, string empCode, string userRoleId);
        Task<int> SaveLeavesSanction(LeavesSanctionMainModel objListLeavesSanctionModel, string Status);

        Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailDetail(string permDdoId, string empCode, string userRoleId);
        Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailHistory(string permDdoId, string empCode, string userRoleId);
        Task<LeavesCurtailSanctionModel> GetVerifiedLeavesSanction(string permDdoId, string ordId);
        Task<int> SaveLeavesCurtExetn(LeavesCurtailModel ObjLeavesCurtailModelDetails, string Status);


        Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancel(string permDdoId, string empCode, string userRoleId);
        Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancelHistory(string permDdoId, string empCode, string userRoleId);
        Task<LeavesCancelModel> GetVerifiedLeaveForCancel(string permDdoId, string ordId);
        Task<int> SaveLeavesCancel(LeavesCancelModel objListleavescancelModel, string Status);

        Task<int> DeleteLeavesSanction(string sanctMainId, string pageCode);
        Task<int> VerifyReturnSanction(string Id, string Status, string PageCd, string Reason,string ipAddress);
        Task<IEnumerable<LeavesTypeBalanceModel>> GetLeavesTypeBalance(string EmpCd);
        Task<int> SaveTotalLeaveAvailed(string EmpCd,string TotalLeaveAvailed,string ipAddress);


    }
}
