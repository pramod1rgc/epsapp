﻿using EPS.BusinessModels.LoanApplication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.LoanApplication
{
   public interface IRecoveryChallanRepository : IDisposable
    {
        Task<List<RecoveryChallan>> GetRecoveryChallan(string empCode, int msDesignID);
        Task<string> InsertUpdateRecoveryChallan(RecoveryChallan objmodel);
    }
}
