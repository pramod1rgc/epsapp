﻿using EPS.BusinessModels.LoanApplication;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.LoanApplication
{
    public interface ILoanApplicationRepository : IDisposable
    {
        Task<IEnumerable<LoanApplicationDetailsModel>> GetEmpDetailsbyEmpCodeDesignId();
        Task<IEnumerable<Sanction>> GetSanctionDetailsbyEmpCodeDesignId(string empCode, int msDesignID, int mode);
        Task<bool> ValidateCoolingPeriod(int LoanCode, int PurposeCode, string EmpCd);
        Task<IEnumerable<tblMsPayLoanRef>> BindLoanType(string loantypeFlag);
        Task<IEnumerable<LoanApplicationDetailsModel>> GetEmpLoanDetailsByID(string EmpCd);
        Task<IEnumerable<LoanApplicationDetailsModel>> EditLoanDetailsByEMPID(int id);
        Task<List<LoanApplicationDetailsModel>> GetEmpDetailsbyEmpCodeDesignId(string empCode, int msDesignID);
        Task<IEnumerable<Employee>> GetEmpCode(string username);
        Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillgroup(string desigId, string BillgrID, string Flag);
        Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillID(string desigId, string BillgrID, string PermDdoid, string Flag);
        Task<string> GetBillgroupfilterbydesignID(string desigId, string Flag);
        Task<IEnumerable<tblMsLoanStatus>> BindLoanStatus(int mode, int userid, int loginddoid = 0);
        Task<IEnumerable<PayLoanPurpose>> LoanpurposeSelectedPayLoanCode(string payLoanCode, string empCode);
        Task<IEnumerable<PayLoanPurpose>> SelectedPayLoanCodepurposecode(string payLoanCode, string purposeCode);
        Task<int> SaveLoanDetails(LoanApplicationDetailsModel objLoanAppDetails);
        Task<int> UpdateLoanDetails(LoanApplicationDetailsModel objLoanAppDetails);
        Task<int> DeleteLoanDetailsByEMPID(int id, int mode);
        Task<int> UpdateLoanDetailsbyID(int id, int mode, string remarks, int priVerifFlag);
        Task<IEnumerable<tblMsLoanStatus>> GetFwdtochkerDetails();
        Task<int> UpdateSanctionDetails(Sanction objSanction);
    }
}
