﻿using EPS.BusinessModels.LoanApplication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.LoanApplication
{
    public interface IPropertyRepository : IDisposable
    {
        Task<string> CreatePropOwnerDetMaster(PropOwner_Model POwnerModel);
        Task<IEnumerable<PropOwner_Model>> GetPropOwnerDetMasterDetailsByID(string MasterID);
        Task<IEnumerable<PropOwner_Model>> EditPropOwnerDetMasterDetails(string MasterID);
    }
}
