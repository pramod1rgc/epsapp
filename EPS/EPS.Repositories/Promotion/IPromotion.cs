﻿using EPS.BusinessModels.Promotion;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Promotion
{
    public interface IPromotion : IDisposable
    {
        Task<string> UpdateReversionDetailsWithoutTransferBAL(ReversionDetails objRev);
        Task<IEnumerable<GetReversionDetails>> GetReversionDetailsWithoutTransferBAL(string empCd, int pageNumber, int pageSize, string searchTerm, int roleId);
        Task<string> DeleteReversionDetails(int id);
        Task<string> UpdatePromotionDetailsWithoutTransferBAL(PromotionDetails objPro);
        Task<IEnumerable<GetPromotionDetails>> GetPromotionDetailsWithoutTransferBAL(string empCd, int pageNumber, int pageSize, string searchTerm, int roleId);
        Task<string> DeletePromotionDetails(int id);
        Task<string> ForwardTransferDetailToChecker(ForwardTransferDetails obj);
        Task<string> UpdateStatus(ForwardTransferDetails obj);
    }
}

