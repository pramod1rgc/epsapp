﻿using EPS.BusinessModels.Increment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Increment
{
    public interface IRegIncrementRepository: IDisposable
    {
        Task<List<RegIncrement_Model>> GetEmployeeDesignation(string PermDdoId);

        Task<List<RegIncrement_Model>> GetOrderDetails(string designID, string paycodeID, string orderType);

        IEnumerable<RegIncrement_Model> GetEmployeeForIncrement(string designID, string paycodeID, int orderID);

        Task<List<RegIncrement_Model>> GetEmployeeForNonIncrement(string designID, string paycodeID, int orderID);

        Task<string> CreateOrderNo(RegIncrement_Model objData);

        Task<string> InsertRegIncrementData(object[] objData);

        Task<string> deleteOrderDetails(int id, string type);

        Task<string> ForwardtocheckerForInc(object[] objData);
    }
}
