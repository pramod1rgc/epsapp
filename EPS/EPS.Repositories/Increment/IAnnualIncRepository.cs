﻿using EPS.BusinessModels.Increment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Increment
{
    public interface IAnnualIncRepository: IDisposable
    {
        IEnumerable<AdvIncrement_Model> GetAnnualIncReportData(string paycodeID, string orderID);

        IEnumerable<AdvIncrement_Model>GetEmpDueForIncReport(string designCode);

        IEnumerable<AdvIncrement_Model> GetOrderWithEmployee(string paycodeID);

        IEnumerable<AdvIncrement_Model> GetSalaryMonth();
    }
}
