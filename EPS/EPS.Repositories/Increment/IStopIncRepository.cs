﻿using EPS.BusinessModels.Increment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Increment
{
    public interface IStopIncRepository
    {
        Task<string> InsertStopIncrementData(object[] objData);
        Task<string> ForwardcheckerForStopInc(object[] objData);  //RegIncrement_Model
        Task<List<RegIncrement_Model>> GetEmployeeStopIncrement(int orderID);
        Task<List<RegIncrement_Model>> GetEmployeeForNotStop(int orderID);
    }
}
