﻿using EPS.BusinessModels.Increment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Increment
{
   public interface IAdvIncrementRepository: IDisposable
    {
        Task<List<AdvIncrement_Model>> GetEmployeeByDesigPayComm(string msDesignID, string paycomm);

        Task<List<AdvIncrement_Model>> GetIncrementDetails(string empcode);

        Task<List<AdvIncrement_Model>> EditAdvIncrement(int incID);

        Task<string> CreateAdvIncrement(AdvIncrement_Model objData);

        Task<string> Forwardtochecker(AdvIncrement_Model objData);

        Task<string> DeleteAdvDetails(int incID);
    }
}
