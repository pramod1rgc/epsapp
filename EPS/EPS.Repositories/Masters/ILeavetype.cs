﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface ILeavetype: IDisposable
    {
        Task<List<LeavetypeBM>> GetLeavetypes();

        #region Get LeaveType MaxID
        
        Task<int> GetLeavetypeMaxid();

        #endregion
        #region Update Master Leave Type
        
        Task<string> UpdateMstLeavetype(LeavetypeBM leavetypeBM);

        #endregion
        #region Insert Master Leave type

        Task<string> InsertMstLeavetype(LeavetypeBM leavetypeBM);

        #endregion
        #region delete Master Leave type

        Task<string> DeleteMstLeavetype(int msleavetypeId);
        
        #endregion
    }
}
