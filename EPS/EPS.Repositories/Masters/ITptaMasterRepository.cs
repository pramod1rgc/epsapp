﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface ITptaMasterRepository : IDisposable
    {
        Task<List<HRAModel>> GetTptaMasterDetails();

        Task<List<HRAModel>> EditTptaMasterDetails(int MasterID);

        Task<string> CreateTptaMaster(HRAModel objData);

        Task<string> DeleteTptaMaster(int MasterID);
    }
}
