﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IPAOMaster:IDisposable
    {
        Task<IEnumerable<PAOMasterBM>> GetMSPAODetails();
        #region Get Pao Master details

        Task<IEnumerable<PAOMasterBM>> GetMSPAODetails(int PaoId);

        #endregion
        #region Update  Pao Master

        Task<string> UpdateMSPAO(PAOMasterBM  pAOMasterBM);

        #endregion
        #region Insert Pao Master 

        Task<string> InsertMSPAO(PAOMasterBM pAOMasterBM);

        #endregion
        #region delete Pao Master 

        Task<string> DeleteMSPAO(int paoId);

        #endregion
    }
}
