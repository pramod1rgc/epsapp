﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
  public interface IGpfAdvanceRulesRepository
    {
          Task<int> InsertUpdateAdvanceRulesDetails(GpfAdvanceRulesBM objGpfAdvanceRules);
          Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRulesById(int msGpfAdvanceRulesRefID);
          Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRules();
          Task<int> DeleteGpfAdvanceRules(int msGpfAdvanceRulesRefID, string isflag);
    }
}

