﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IFdGpfMasterRepository: IDisposable
    {
        Task<List<FdMaster_Model>> GetFdGpfMasterDetails();

        Task<List<FdMaster_Model>> EditFdGpfMasterDetails(int fdMasterID);

        Task<string> CreatefdGpfMaster(FdMaster_Model objData);

        Task<string> DeleteFdGpfMaster(int fdMasterID);
    }
}
