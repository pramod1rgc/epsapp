﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface ICityMasterRepository : IDisposable
    {
        Task<IEnumerable<CitymasterModel>> GetAllRecords();

        Task<int> SaveRecord(CitymasterModel objData);

        Task<int> DeleteRecord(int id);


        Task<int> CheckRecordAlredyExitsinCityMaster(CitymasterModel obj);

    }
}
