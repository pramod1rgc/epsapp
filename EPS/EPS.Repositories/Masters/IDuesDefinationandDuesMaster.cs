﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IDuesDefinationandDuesMaster : IDisposable
    {
        Task<int> InsertUpdateDuesDefinationMasterDetails(DuesDefinationandDuesMasterModel ObjDuesDefinationandDuesMasterModel);
        Task<IEnumerable<DuesDefinationandDuesMasterModel>> GetDuesDefinationandDuesMasterList();
        Task<int> DeleteDuesDefinationmasterDetails(string payitemCd);
        Task<DuesDefinationandDuesMasterModel> GetDuesDefinationandDuesMasterByID(string DuesCd);
        Task<DuesDefinationandDuesMasterModel> GetAutoGenratedDuesCode();

        Task<IEnumerable<DuesRateandDuesMasterModel>> GetOrgnazationTypeForDues();
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetDuesCodeDuesDefination();
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetCityClassForDuesRate(string payCommId);
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetStateForDuesRate();
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetPayCommForDuesRate();
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetSlabtypeByPayCommId(string payCommId);
        Task<int> InsertUpdateDuesRateDetails(DuesRateandDuesMasterModel objDuesRateandDuesMasterModel);
        Task<IEnumerable<DuesRateandDuesMasterModel>> GetDuesCodeDuesRate();
        Task<int> DeleteDuesRateDetails(string msduesratedetails);
    }
}
