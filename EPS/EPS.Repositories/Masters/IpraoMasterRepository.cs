﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessModels.Masters;

namespace EPS.Repositories.Masters
{
    public interface IpraoMasterRepository : IDisposable
    { 
        Task<IEnumerable<PraoMasterModel>> GetAllController();
        Task<IEnumerable<PraoMasterModel>> BindCtrlName(string CtrlrCode); 
        Task<IEnumerable<PraoMasterModel>> BindState(); 
        Task<IEnumerable<PraoMasterModel>> BindDistrict(int stateId);
        //Task<int> SaveLoanData(ExistingLoanApplicationModel _obj);
        Task<int> SaveOrUpdate(PraoMasterModel model);
        Task<IEnumerable<ProMasterDetails>> BindPraoMasterDetails(); 
        Task<IEnumerable<PraoMasterModel>> ViewEditOrDeletePrao(string CtrlCode, string controllerID, int Mode);
    }
}
