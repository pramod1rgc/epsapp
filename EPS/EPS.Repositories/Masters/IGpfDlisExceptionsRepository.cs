﻿using EPS.BusinessModels.Masters;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace EPS.Repositories.Masters
{
    public interface IGpfDlisExceptionsRepository
    {
        Task<int> InsertUpdateGpfDlisExceptions(GpfDlisExceptions objGpfDlisExceptions);
        Task<List<GpfDlisExceptions>> GetGpfDlisExceptions();
        Task<List<PfType>> GetPfType();
        Task<List<PayScale>> GetPayScale(int PayCommId);
        Task<int> DeleteGpfDlisExceptions(int MsGpfDlisExceptionsRefID);
    }
}
