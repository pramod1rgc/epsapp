﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
      public  interface IGpfWithdrawRuleRepository
    {
        Task<int> InsertUpdateEmpDetails(GpfWithdrawRules ObjGpfWithdrawRules);
        Task<List<GpfWithdrawRules>> GetGpfWithdrawRulesById(int MsGpfWithdrawRulesRefID);
        Task<List<GpfWithdrawRules>> GetGpfWithdrawRules();
        Task<int> DeleteGpfWithdrawRules(int MsGpfWithdrawRulesRefID);
    }
}
