﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
   public interface IGpfRecoveryRulesRepository
    {
        Task<int> InsertUpdateGpfRecoveryRules(GpfRecoveryRulesBM objgpfRecoveryRules);
        Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRulesById(int msGpfRecoveryRulesRefID);
        Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRules();
        Task<int> DeleteGpfRecoveryRules(int msGpfRecoveryRulesRefID);


    }
}
