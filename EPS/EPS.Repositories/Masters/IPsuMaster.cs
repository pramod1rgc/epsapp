﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IPsuMaster 
    {

        Task<IEnumerable<PsuMasterModel.State>> GetStateList();

        Task<IEnumerable<PsuMasterModel.City>> GetDisttList(int stateId);
        Task<string> SavedPSUDetails(PsuMasterModel.PsuMaster objPSU);
        Task<IEnumerable<PsuMasterModel.PsuMaster>> GetPsuMasterDetails();

        string DeletePsuMasterDetails(int id);
    }
}
