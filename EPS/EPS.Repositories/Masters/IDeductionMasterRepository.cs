﻿using EPS.BusinessModels.Masters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IDeductionMasterRepository
    {
        Task<List<DeductionMasterBM>> CategoryList();
        //Task<DeductionMasterBM> GetAutoGenratedDeductionCode();
        Task<List<DeductionDescriptionBM>> DeductionDescription();
        Task<List<DeductionDescriptionBM>> DeductionDescriptionChange(int payItemsCD);
        Task<List<DeductionDescriptionBM>> GetDeductionDefinitionList();
        Task<int> DeductionDefinitionSubmit(DeductionDescriptionBM ObjDuesDefinationandDuesMasterModel);

        //============================Deduction Rate master======================================
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetOrgnazationTypeForDues();
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetDuesCodeDuesDefination();
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetCityClassForDuesRate(string payCommId);
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetStateForDuesRate();
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetPayCommForDuesRate();
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetSlabtypeByPayCommId(string payCommId);
        Task<int> InsertUpdateDuesRateDetails(DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel);
        Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetDuesCodeDuesRate();
        Task<int> DeleteDuesRateDetails(string msduesratedetails);
    }
}
