﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
    public interface IHraMasterRepository : IDisposable
    {
        Task<List<HRAModel>> BindPayScaleCode();

        Task<List<HRAModel>> BindPayLevel();

        Task<List<HRAModel>> GetCityDetails(int stateId);

        Task<List<HRAModel>> GetCityClass(int payCommId);

        Task<List<HRAModel>> GetHraMasterDetails();

        Task<List<HRAModel>> EditHraMasterDetails(int hraMasterID);

        Task<List<HRAModel>> GetPayCommissionByEmployeeType(string employeeType);

        Task<string> CreateHraMaster(HRAModel objData);


        Task<string> DeleteHraMaster(int hraMasterID);
    }
}
