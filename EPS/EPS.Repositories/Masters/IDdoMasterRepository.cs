﻿using EPS.BusinessModels.Masters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.Masters
{
   public interface IDdoMasterRepository:IDisposable
    {
       
        Task<IEnumerable<ControllerMs>> GetAllController ();
        Task<IEnumerable<PAO>> GetPAOByControllerId(string ControllerId);
        Task<IEnumerable<DDO>> GetODdoByPao(string PaoCode);
        Task<IEnumerable<DDOType>> GetDDOcategory();
        Task<IEnumerable<State>> GetState();
        Task<IEnumerable<City>> GetCity(string StateId);
        Task<IEnumerable<DdoMasterModel>> GetDDOMaster();
        Task<int> SaveDDOMaster(DdoMasterModel objDdoMasterModel);
        Task<int> DeleteDDOMaster(string ddoId); 

    }
}
