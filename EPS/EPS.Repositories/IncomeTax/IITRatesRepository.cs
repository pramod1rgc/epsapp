﻿using EPS.BusinessModels.IncomeTax;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.IncomeTax
{
    public interface IITRatesRepository : IDisposable
    {
        Task<IEnumerable<GetITRatesDetails>> GetITRatesBAL(string FinYear, string RateType, string RateFor);
        Task<IEnumerable<ITRateDetailStatus>> UpsertITRatesBAL(UpsertITRatesDetails obj);
        Task<string> DeleteITRatesBAL(int id, string ip, int modifiedBy);
        Task<IEnumerable<RateType>> GetRateTypeAndRateForBAL(string type);
    }
}
