﻿using EPS.BusinessModels.IncomeTax;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.IncomeTax
{
    public interface IStandardDeductionRepository : IDisposable
    {
        Task<IEnumerable<StandardDeductionDetails>> GetStandardDeductionRuleBAL(int PageNumber, int PageSize, string SearchTerm);
        Task<IEnumerable<GetStandardDeductionEntertainMasterDetail>> GetStandardDeductionEntertainMasterBAL();
        Task<string> UpsertStandardDeductionBAL(UpsertStandardDeductionDetails obj);
        Task<string> DeleteStandardDeductionBAL(DeleteStandardDeductionDetails obj);
        Task<string> DeleteStandardDeductionRateDetailBAL(int id);
    }
}
