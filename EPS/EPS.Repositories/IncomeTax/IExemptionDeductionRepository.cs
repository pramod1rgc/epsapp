﻿using EPS.BusinessModels.IncomeTax;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.Repositories.IncomeTax
{
    public interface IExemptionDeductionRepository : IDisposable
    {
        Task<List<MajorSectionCode>> GetMajorSectionCode();
        Task<List<SectionCode>> GetSectionCode(string majorSecCode);
        Task<List<SubSectionCode>> GetSubSectionCode(string majorSecCode, string secCode);
        Task<List<SectionCodeDescription>> GetSectionCodeDescription(string majorSecCode, string sectionCode, string subSectionCode);
        Task<List<SlabLimit>> GetSlabLimit(string payCommission);
        Task<List<PayCommission>> GetPayCommission();
        Task<List<ExemptionDeductionDetails>> GetExemptionDeductionDetails(int pageNumber, int pageSize, string searchTerm);
        Task<string> UpsertExemptionDeductionDetails(ExemptionDeductionDetails obj);
        Task<int> DeleteExemptionDeductionDetails(int drrId, int drrId2);
    }
}
