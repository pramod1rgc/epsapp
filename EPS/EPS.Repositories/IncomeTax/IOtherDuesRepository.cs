﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static EPS.BusinessModels.IncomeTax.OtherDuesModel;

namespace EPS.Repositories.IncomeTax
{
    public interface IOtherDuesRepository : IDisposable
    {
        Task<List<MajorSecCode>> GetMajorSectionCode();
        Task<List<OtherDues>> GetOtherDues(bool allDues);
        Task<List<OtherDuesDetails>> GetOtherDuesDetails(int pageNumber, int pageSize, string searchTerm);
        Task<string> SaveOtherDuesDetails(OtherDuesDetails obj);
        Task<int> DeleteOtherDuesDetails(int duesId, string userName, string userIp);
    }
}
