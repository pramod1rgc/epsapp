﻿using EPS.BusinessAccessLayers;
using EPS.BusinessAccessLayers.Leaves;
using EPS.BusinessAccessLayers.LoanApplication;
using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessAccessLayers.PayBill;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Deputation;
using EPS.Repositories.ExistingLoan;
using EPS.Repositories.LeaveMangement;
using EPS.Repositories.LienPeriod;
using EPS.Repositories.LoanApplication;
using EPS.Repositories.Masters;
using EPS.Repositories.PayBIllGroup;
using EPS.Repositories.UserManagement;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace EPS.WebAPI
{
    /// <summary>
    /// Startup Class  
    /// </summary>
    public class Startup
    {
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}
        //public IConfiguration Configuration { get; }

        /// <summary>
        /// Startup Class Constructor 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddApplicationInsightsSettings(developerMode: true);
            }


            Configuration = builder.Build();
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            Environment = env;
        }

        public IHostingEnvironment Environment { get; }
        public IConfigurationRoot Configuration { get; }
        public static string ConnectionString { get; set; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {    // ---logger -----
             // services.Configure<FormOptions>(x => x.ValueCountLimit = 2048);
            services.AddApplicationInsightsTelemetry(Configuration);
            // Nlog configuration
            ILoggerFactory loggerFactory = new LoggerFactory();
            loggerFactory.AddNLog();

            Environment.ConfigureNLog("TraceNLog.config");
            services.AddSingleton<ILoggerFactory>(loggerFactory);
            ConfigureMvc(services, loggerFactory);
            //-----end logger-----
            services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors(o => o.AddPolicy("AllowAnyOrigin", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddSwaggerDocumentation();

            services.Configure<CommonKeys>(Configuration.GetSection("ApplicationSettings"));


            //services.AddCors(option =>
            //{
            //    option.AddPolicy("AllowAnyOrigin", policy => policy.WithOrigins());
            //    option.AddPolicy("AllowGetMethod", policy => policy.WithMethods("*", "*", "*"));
            //});


            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAnyOrigin"));
            });


            //===============Added By Tabarak for Token===================
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            //services.AddMvc();
            //===============End Token==========================

            #region Register the dependency

            services.AddScoped<IPsuMaster, PsuMasterBAL>();
            services.AddScoped<ICityMasterRepository, Citymaster_BAL>();
            services.AddScoped<ILeavetype, LeavetypeDAL>();
            services.AddScoped<IPAOMaster, PAOMasterBAL>();

            services.AddScoped<ILeaveManagementRepository, LeavesBAL>();
            services.AddScoped<ILoanApplicationRepository, LoanApplicationBAL>();
            services.AddScoped<IPropertyRepository, PropOwnerDetBAL>();
            services.AddScoped<IRecoveryChallanRepository, RecoveryChallanBAL>();
            services.AddScoped<IDdoMasterRepository, DdoMasterBAL>();
            services.AddScoped<IPayBillGroupRepository, PayBillGroupBAL>();

            services.AddScoped<IAlreadyTakenRepository, EPS.BusinessAccessLayers.ExistingLoan.ExistingLoanBusinessAccessLayer>();
            services.AddScoped<IFloodRepository, EPS.BusinessAccessLayers.ExistingLoan.FloodBAL>();
            services.AddScoped<IinstRecoveryRepository, EPS.BusinessAccessLayers.ExistingLoan.RecoveryInstBAL>();

            services.AddScoped<EPS.Repositories.Suspension.ISuspension, EPS.BusinessAccessLayers.Suspension.SuspensionBAL>();
            services.AddScoped<EPS.Repositories.Masters.IHraMasterRepository, EPS.BusinessAccessLayers.Masters.HRA_BAL>();
            services.AddScoped<EPS.Repositories.Masters.ITptaMasterRepository, EPS.BusinessAccessLayers.Masters.TPTA_BAL>();
            services.AddScoped<EPS.Repositories.Masters.IFdGpfMasterRepository, EPS.BusinessAccessLayers.Masters.FdGpfMaster_BAL>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IEmployeeDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.EmployeeDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IBankDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.BankDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.ICGEGISRepository, EPS.BusinessAccessLayers.EmployeeDetails.CGEGISBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.ICGHSRepository, EPS.BusinessAccessLayers.EmployeeDetails.CGHSDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IFamilyRepository, EPS.BusinessAccessLayers.EmployeeDetails.FamilyDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.INomineeRepository, EPS.BusinessAccessLayers.EmployeeDetails.NomineeDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IPayDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.PayDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IPostingDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.PostingDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IQuaterDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.QuarterDetailsBA>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IServiceDetailsRepository, EPS.BusinessAccessLayers.EmployeeDetails.ServiceDetailsBA>();
            services.AddScoped<EPS.Repositories.Masters.IGpfWithdrawRuleRepository, EPS.BusinessAccessLayers.Masters.GpfWithdrawRulesBA>();
            services.AddScoped<EPS.Repositories.Increment.IAdvIncrementRepository, EPS.BusinessAccessLayers.Increment.AdvIncrement_BAL>();
            services.AddScoped<EPS.Repositories.Increment.IAnnualIncRepository, EPS.BusinessAccessLayers.Increment.AnnualIncrement_BAL>();
            services.AddScoped<EPS.Repositories.Increment.IRegIncrementRepository, EPS.BusinessAccessLayers.Increment.RegularIncrement_BAL>();
            services.AddScoped<EPS.Repositories.Increment.IStopIncRepository, EPS.BusinessAccessLayers.Increment.StopIncrement_BAL>();
            services.AddScoped<EPS.Repositories.EmployeeDetails.IMasterRepository, EPS.BusinessAccessLayers.MasterBA>();

            services.AddScoped<Repositories.IncomeTax.IExemptionDeductionRepository, BusinessAccessLayers.IncomeTax.ExemptionDeductionBAL>();
            services.AddScoped<Repositories.IncomeTax.IOtherDuesRepository, BusinessAccessLayers.IncomeTax.OtherDuesBAL>();
            services.AddScoped<Repositories.IncomeTax.IStandardDeductionRepository, BusinessAccessLayers.IncomeTax.StandardDeductionBAL>();
            services.AddScoped<Repositories.IncomeTax.IITRatesRepository, BusinessAccessLayers.IncomeTax.ITRatesBAL>();
            //change module
            services.AddScoped<Repositories.Change.IExtensionOfServiceRepository, BusinessAccessLayers.Change.ExtensionOfServiceBAL>();
            services.AddScoped<Repositories.Change.IChangeRepository, BusinessAccessLayers.Change.Changebal>();
            services.AddScoped<IDuesDefinationandDuesMaster, DuesDefinationandDuesMaster_BAL>();

            //PraoMaster
            services.AddScoped<IpraoMasterRepository, PraoBALMaster>();
            // Gpf RecoveryRules
            services.AddScoped<EPS.Repositories.Masters.IGpfRecoveryRulesRepository, EPS.BusinessAccessLayers.Masters.GpfRecoveryRulesBAL>();
            // Gpf AdvanceRules
            services.AddScoped<EPS.Repositories.Masters.IGpfAdvanceRulesRepository, EPS.BusinessAccessLayers.Masters.GpfAdvanceRulesBAL>();

            #region Deputation
            services.AddScoped<IDeputationIn, BusinessAccessLayers.Deputation.DeputationInBA>();
            services.AddScoped<IDeputationOut, BusinessAccessLayers.Deputation.DeputationOutBA>();
            services.AddScoped<IRepatriationDetails, BusinessAccessLayers.Deputation.RepatriationDetailsBA>();
            #endregion
            #region Lien Period
            services.AddScoped<ITransferOffLienPeriod, BusinessAccessLayers.LienPeriod.TransferOffLienPeriodBA>();
            services.AddScoped<IEpsEmpJoinAfterLienPeriod, BusinessAccessLayers.LienPeriod.EpsEmpJoinAfterLienPeriodBA>();
            services.AddScoped<INonEpsEmployeejoinAfterLienPeriod, BusinessAccessLayers.LienPeriod.NonEpsEmployeejoinAfterLienPeriodBA>();
            #endregion
            //User management/Payroll
            #region User management/Payroll 
            services.AddScoped<ILoginRepository, BusinessAccessLayers.LoginBusinessAccessLayer>();
            services.AddScoped<IControllerRoleRepository, BusinessAccessLayers.UserManagment.ControllerroleBusinessAccessLayer >();
            services.AddScoped<IDDOAdminRepository, BusinessAccessLayers.UserManagment.DDOroleBusinessAccessLayer >();
            services.AddScoped<IPAORoleRepository, BusinessAccessLayers.UserManagment.PAOroleBusinessAccessLayer >();
            services.AddScoped<IHOORoleRepository, BusinessAccessLayers.UserManagment.HOOBusinessAccessLayer>();
            services.AddScoped<IDDOCheckerRoleRepository, BusinessAccessLayers.UserManagment.UsersByDDOBusinessAccessLayer>();
            services.AddScoped<IDDOMakerRoleRepository, BusinessAccessLayers.UserManagment.MakerBusinessAccessLayer>();
            #endregion

            #region Deduction Master
            services.AddScoped<IDeductionMasterRepository, BusinessAccessLayers.Masters.DeductionMasterBAL>();
            #endregion
            //Gpf Dlis Exception
            services.AddScoped<EPS.Repositories.Masters.IGpfDlisExceptionsRepository, EPS.BusinessAccessLayers.Masters.GpfDlisExceptionsBAL>();
            #endregion

            services.AddMvc();
        }

        private void ConfigureMvc(IServiceCollection services, ILoggerFactory loggerFactory)
        {
            var mvcBuilder = services.AddMvc();
            //services.AddMvc(options =>
            //{
            //    options.Filters.Add(new EPSExceptionFilters());
            //});
            //mvcBuilder.AddMvcOptions(o => o.Filters.Add(typeof(GlobalExceptionLogger(loggerFactory)));
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerDocumentation();
            }
            //----logger start----
            string conString = Configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            ConnectionClass.ConnectionString = conString;
            LogManager.Configuration.Variables["myConnectionString"] = conString;
            LogManager.KeepVariablesOnReload = true;
            LogManager.Configuration.Reload();
            app.UseHttpsRedirection();
            app.UseMvcWithDefaultRoute();
            app.UseMvc(routes => ConnectionString = conString);

            //-------end logger------

            app.UseCors("AllowAnyOrigin");

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My First Swagger");
            });
        }
    }

    /// <summary>
    /// Extension method for handling Swagger
    /// </summary>
    public static class SwaggerServiceExtensions
    {
        /// <summary>
        /// Add Swagger Documentation
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new Info { Title = "EPS v1.0", Version = "v1.0" });

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                if (File.Exists(xmlPath))
                {
                    //... and tell Swagger to use those XML comments.
                    c.IncludeXmlComments(xmlPath);
                }


                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                //Must require for swagger version > 2.0
                c.AddSecurityRequirement(security);
                c.CustomSchemaIds(i => i.FullName);
            });

            return services;
        }

        /// <summary>
        /// Use Swagger Documentation
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "EPS Project API v1.0");
                c.DocumentTitle = "Title Documentation";
                //Reference link : https://stackoverflow.com/questions/22008452/collapse-expand-swagger-response-model-class
                //Reference link : https://swagger.io/docs/open-source-tools/swagger-ui/usage/deep-linking/
                //  c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                // c.DocExpansion(DocExpansion.Full);
                //    //Reference document: https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-2.2&tabs=visual-studio
                //    //To serve the Swagger UI at the app's root (http://localhost:<port>/), set the RoutePrefix property to an empty string:
                c.RoutePrefix = string.Empty;
            });

            return app;
        }

    }

}
