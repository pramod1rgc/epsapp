﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.LoanApplication;
using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.Repositories.LoanApplication;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.LoanApplication
{
    /// <summary>
    /// PropertyController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PropertyController
    {
        //PropOwnerDetBAL objTptaBAL = new PropOwnerDetBAL();
        private IHttpContextAccessor _accessor;
        private IPropertyRepository _repository;
        private string clientIP = string.Empty;

        /// <summary>
        /// PropertyController Constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public PropertyController(IHttpContextAccessor accessor, IPropertyRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }

        /// <summary>
        /// Save Property Owner Details
        /// </summary>
        /// <param name="propObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavePropertyOwnerDetails([FromBody]PropOwner_Model propObj)
        {
            propObj.CreatedIP = clientIP;
            return await Task.Run(() => _repository.CreatePropOwnerDetMaster(propObj));
        }

        /// <summary>
        /// GetPropOwnerMasterDetailsByID
        /// </summary>
        /// <param name="MstID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<PropOwner_Model>> GetPropOwnerMasterDetailsByID(string MstID)
        {
            return await Task.Run(() => _repository.GetPropOwnerDetMasterDetailsByID(MstID));
        }

        /// <summary>
        /// Edit PropOwnerDet Master Details
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<PropOwner_Model>> EditPropOwnerDetMasterDetails(string MasterID)
        {
            return await Task.Run(() => _repository.EditPropOwnerDetMasterDetails(MasterID));
        }
    }
}