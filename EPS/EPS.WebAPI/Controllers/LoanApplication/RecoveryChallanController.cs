﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.LoanApplication;
using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.Repositories.LoanApplication;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.LoanApplication
{

    /// <summary>
    /// RecoveryChallan Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RecoveryChallanController : Controller
    {
        RecoveryChallanBAL ObjRecoveryChallanBAL = new RecoveryChallanBAL();
        private IHttpContextAccessor _accessor;
        private IRecoveryChallanRepository _repository;



        public RecoveryChallanController(IHttpContextAccessor accessor, IRecoveryChallanRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }
        /// <summary>
        /// BindDropDown LoanType
        /// </summary>
        /// <returns></returns>
        //[HttpGet("[action]")]
        //public async Task<IActionResult> BindDropDownLoanType()
        //{
        //    var mytask = Task.Run(() => ObjRecoveryChallanBAL.GetAllLoanType());
        //    var result = await mytask;
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        return Ok(result);
        //    }
        //}

        /// <summary>
        /// GetEmployee Details by emp code or designid
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="msDesignId"></param>
        /// <returns></returns>
        [HttpGet("[ACTION]")]
        public async Task<IActionResult> GetRecoveryChallan(string empCode, int msDesignId)
        {
            var result = await Task.Run(() => _repository.GetRecoveryChallan(empCode, msDesignId)); ;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objrecoveryChallan"></param>
        /// <returns></returns>
        /// 
        #region Save Loan Details
        [HttpPost("[ACTION]")]
        public async Task<IActionResult> SaveUpdateRecoveryChallan(RecoveryChallan objrecoveryChallan)
        {
            objrecoveryChallan.CreatedIP = CommonFunctions.GetClientIP(_accessor);
            var result = await Task.Run(() => _repository.InsertUpdateRecoveryChallan(objrecoveryChallan)); 
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
            
        }
        #endregion
    }
}
