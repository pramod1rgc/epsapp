﻿using EPS.BusinessAccessLayers;
using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.Repositories.LoanApplication;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// Loan Application Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LoanApplicationController : Controller
    {
        private LoanApplicationBAL objloanBal = new LoanApplicationBAL();
        private IHttpContextAccessor _accessor;
        private ILoanApplicationRepository _repository;
        private static string clientIP = string.Empty;

        /// <summary>
        /// LoanApplication Controller
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public LoanApplicationController(IHttpContextAccessor accessor, ILoanApplicationRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }

        /// <summary>
        /// GetEmployee Details by emp code or designid
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="msDesignId"></param>
        /// <returns></returns>
        [HttpGet("[ACTION]")]
        public async Task<IActionResult> GetEmpDetailsbyEmpcode(string empCode, int msDesignId)
        {
            var result = await Task.Run(() => _repository.GetEmpDetailsbyEmpCodeDesignId(empCode, msDesignId));
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Edit Loan Details by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditLoanDetailsByEmpID(int id)
        {
            var result = await Task.Run(() => _repository.EditLoanDetailsByEMPID(id)); ;

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// GetEmpDetails by empcode
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpDetails(string EmpCd)
        {
            var result = await Task.Run(() => _repository.GetEmpCode(EmpCd));
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Employee filter by designId and  Billgroup
        /// </summary>
        /// <param name="desigId"></param>
        /// <param name="BillgrID"></param>
        /// <param name="Flag"></param>
        /// 
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpfilterbydesignBillgroup(string desigId, string BillgrID, string Flag)
        {
            var result = await Task.Run(() => _repository.GetEmpfilterbydesignBillgroup(desigId, BillgrID, Flag));

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        ///  Employee filter by desigId and BillID and PermDdoid
        /// </summary>
        /// <param name="desigId"></param>
        /// <param name="BillgrID"></param>
        /// <param name="PermDdoid"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpfilterbydesignBillID(string desigId, string BillgrID, string PermDdoid, string Flag)
        {
            var result = await Task.Run(() => _repository.GetEmpfilterbydesignBillID(desigId, BillgrID, PermDdoid, Flag));

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// BillGroup filter by designID
        /// </summary>
        /// <param name="desigId"></param>
        /// <param name="Flag"></param>        
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<string> GetBillgroupfilterbydesignID(string desigId, string Flag)
        {
            return await Task.Run(() => _repository.GetBillgroupfilterbydesignID(desigId, Flag));
        }

        /// <summary>
        /// Get LoanType
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<tblMsPayLoanRef>> GetLoanType(string loantypeFlag)
        {
            return await Task.Run(() => _repository.BindLoanType(loantypeFlag));
        }

        /// <summary>
        /// Get Loan Details by mode and userid
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="userid"></param>
        /// <param name="loginddoid"></param>      
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<tblMsLoanStatus>> GetLoanDetails(int mode, int userid, int loginddoid = 0)
        {
            return await Task.Run(() => _repository.BindLoanStatus(mode, userid, loginddoid));
        }

        /// <summary>
        /// Get Loan purpose by LoanCode
        /// </summary>
        /// <param name="payLoanCode"></param>
        /// <param name="empCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<PayLoanPurpose>> GetLoanpurposebyLoanCode(string payLoanCode, string empCode)
        {
            return await Task.Run(() => _repository.LoanpurposeSelectedPayLoanCode(payLoanCode, empCode));
        }

        /// <summary>
        /// Get Details by LoanCode and Purposecode
        /// </summary>
        /// <param name="payLoanCode"></param>
        /// <param name="purposeCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<PayLoanPurpose>> GetDetailsbyLoanCodePurposecode(string payLoanCode, string purposeCode)
        {
            return await Task.Run(() => _repository.SelectedPayLoanCodepurposecode(payLoanCode, purposeCode));
        }

        /// <summary>
        /// Save New Loan Details 
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<int> SaveLoanDetails([FromBody]LoanApplicationDetailsModel formData)
        {
            LoanApplicationDetailsModel _obj = formData;
            _obj.ClientIP = clientIP;
            return await Task.Run(() => _repository.SaveLoanDetails(_obj));
        }

        /// <summary>
        /// Update Loan Details
        /// </summary>
        /// <param name="objLoanAppdetails"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<int> UpdateLoanDetails(LoanApplicationDetailsModel objLoanAppdetails)
        {
            objLoanAppdetails.ClientIP = clientIP;
            return await Task.Run(() => _repository.UpdateLoanDetails(objLoanAppdetails));
        }

        /// <summary>
        /// DeActive LoanDetails By EMPID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<int> DeleteLoanDetailsByEMPID(int id, int mode)
        {
            return await Task.Run(() => _repository.DeleteLoanDetailsByEMPID(id, mode));
        }

        /// <summary>
        /// Update Loan Details by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <param name="remarks"></param>
        /// <param name="priVerifFlag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<int> UpdateLoanDetailsbyID(int id, int mode, string remarks, int priVerifFlag)
        {
            return await Task.Run(() => _repository.UpdateLoanDetailsbyID(id, mode, remarks, priVerifFlag));
        }

        /// <summary>
        /// Get Forward to checker details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<tblMsLoanStatus>> GetFwdtochkerDetails()
        {
            return await Task.Run(() => _repository.GetFwdtochkerDetails());
        }

        /// <summary>
        /// Get Sanction Details by Empcode
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="msDesignId"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<Sanction>> GetSanctionDetailsbyEmpcode(string empCode, int msDesignId, int mode)
        {
            return await Task.Run(() => _repository.GetSanctionDetailsbyEmpCodeDesignId(empCode, msDesignId, mode));
        }

        /// <summary>
        /// Update Sanction Details
        /// </summary>
        /// <param name="objSanction"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<int> UpdateSanctionDetails(Sanction objSanction)
        {
            objSanction.ClientIP = clientIP;
            return await Task.Run(() => _repository.UpdateSanctionDetails(objSanction));
        }

        /// <summary>
        /// To validate cooling period
        /// </summary>
        /// <param name="LoanCode"></param>
        /// <param name="PurposeCode"></param>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<bool> ValidateCoolingPeriod(int LoanCode, int PurposeCode, string EmpCd)
        {
            return await Task.Run(() => _repository.ValidateCoolingPeriod(LoanCode, PurposeCode, EmpCd));
        }
    }
}