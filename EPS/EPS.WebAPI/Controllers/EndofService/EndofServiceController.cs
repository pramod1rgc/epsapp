﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EndofService;
using EPS.BusinessModels.EndofService;
using EPS.CommonClasses;
//using EPS.WebAPI.Common;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EPS.WebAPI.Controllers.EndofService
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class EndofServiceController : Controller
    {
        private readonly EndofServiceBAL _objRejBAL;
        private IHttpContextAccessor _accessor;
        ILogger _logger;
        string _mediaType = "application/json";
        private static string clientIP = string.Empty;
        public EndofServiceController(IHttpContextAccessor accessor, ILoggerFactory loggerFactory)
            {
            _accessor=accessor;
             _objRejBAL = new EndofServiceBAL();
            _logger = loggerFactory.CreateLogger<DemoController>();
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }
        /// <summary>
      /// GET: EndofService
      /// </summary>
      /// <param name="List"></param>
      /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<EndofServiceModel>> GetEndofServiceEmployees(int designid, string empPermDDOId, int mode)
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            return await Task.Run(() => objRejBAL.GetServiceEndEmployeesBAL(designid, empPermDDOId, mode)).ConfigureAwait(false);
        }
        /// <summary>
        /// GET: EndofService Current Employee
        /// </summary>
        /// <param name="List"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        //public  async Task<HttpResponseMessage> EmpCurrentEndDetails(int msEmpID, string empCd)
        //{ var obj = _objRejBAL.EmpCurrentEndDetailsBAL(msEmpID, empCd).ToList();
        //    var builder = new HttpRequestBuilder<EmpCurrentDetailModel>()
        //                        .AddMethod(HttpMethod.Get).AddContent(obj);
                               

        //    return await  builder.SendAsync();
        //}
       
        public async Task<IEnumerable<EmpCurrentDetailModel>> EmpCurrentEndDetails(int msEmpID, string empCd)
        {
           
            return await Task.Run(() => _objRejBAL.EmpCurrentEndDetailsBAL(msEmpID, empCd)).ConfigureAwait(false);
        }
        /// GET: EndofService Current Employee details
        [HttpGet("[action]")]
        public async Task<IEnumerable<EndofServiceDtModel>> EndofServiceListDetails(int msEmpID)
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            return await Task.Run(() => objRejBAL.GetEndofServiceEmployeesListBAL(msEmpID)).ConfigureAwait(false);
        } /// GET: EndofService Reason details
        [HttpGet("[action]")]
        public async Task<IEnumerable<EndofServiceReasonModel>> EndofServiceReasonDetails()
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            return await Task.Run(() => objRejBAL.GetEndofServiceReasonIdBAL()).ConfigureAwait(false);
        }
        /// <summary>
        /// Insert End of Service
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<string> EndofServiceInsert([FromBody]EndofServiceInsertModel obj)
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            obj.IPAddress = clientIP;
            string successMsg = await Task.Run(() => objRejBAL.InsertEndofServiceDetailsBAL(obj)).ConfigureAwait(false);
            return successMsg; // !successMsg.Equals(string.Empty) ? (successMsg == "1" ? "Data saved successfully!" : (successMsg == "-1" ? "Order Number Already Exist!" : string.Empty)) : "Some Error Occurred.";
        }
        /// <summary>
        /// Update End of Service
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        [ValidateModel]
        public async Task<string> EndofServiceUpdate([FromBody]UpdateEndofServiceStatusModel obj)
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            obj.IpAddress = clientIP;
            string successMsg = await Task.Run(() => objRejBAL.UpdateEndofServiceStatusBAL(obj)).ConfigureAwait(false);
            return successMsg; // !successMsg.Equals(string.Empty) ? (successMsg == "1" ? "Data saved successfully!" : (successMsg == "-1" ? "Order Number Already Exist!" : string.Empty)) : "Some Error Occurred.";
        }
        /// <summary>
        /// Delete End of Service
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        [ValidateModel]
        public async Task<string> DeleteEndofService(int  obj)
        {
            EndofServiceBAL objRejBAL = new EndofServiceBAL();
            string successMsg = await Task.Run(() => objRejBAL.DeleteEndofServiceDetailsBAL(obj,clientIP)).ConfigureAwait(false);
            return successMsg; 
        }
    }
}