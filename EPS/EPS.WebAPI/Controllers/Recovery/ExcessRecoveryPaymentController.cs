﻿using EPS.BusinessAccessLayers.Recovery;
using EPS.BusinessModels.Recovery;
using EPS.CommonClasses;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.ExcessRecovery
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class ExcessRecoveryPaymentController : Controller
    {
        public ExcessRecPaymentBAL objRecBAL = new ExcessRecPaymentBAL();
        private IHttpContextAccessor _accessor;

        /// <summary>
        /// Excess Recovery payment Curd Operation
        /// </summary>
        /// <param name="accessor"></param>
        public ExcessRecoveryPaymentController(IHttpContextAccessor accessor)
        {
            this._accessor = accessor;
        }

        /// <summary>
        /// Find Financial Year
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetFinancialYears()
        {
            var mytask = Task.Run(() => objRecBAL.GetFinancialYears());
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Get Loan Payment Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLoanPaymentType()
        {
            var mytask = Task.Run(() => objRecBAL.GetLoanPaymentType());
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }


        /// <summary>
        /// Get Recovery Element
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecoveryElement()
        {
            var mytask = Task.Run(() => objRecBAL.GetRecoveryElement());
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Get Account Head
        /// </summary>
        /// <param name="paybill"></param>
        /// <param name="ddoId"></param>
        /// <param name="finYear"></param>
        /// <param name="recoveryType"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAccountHead(string paybill,string ddoId, int finYear , string recoveryType )
        {
            var mytask = Task.Run(() => objRecBAL.GetAccountHead(paybill, ddoId, finYear, recoveryType));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Get Recovery Component
        /// </summary>
        /// <param name="paybill"></param>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecoveryComponent(string paybill, string EmpId)
        {
            var mytask = Task.Run(() => objRecBAL.GetRecoveryComponent(paybill, EmpId));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }


        /// <summary>
        /// Insert Update Excess Recovery Payment Record
        /// </summary>
        /// <param name="recPaymentObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateExRecovery([FromBody]RecoveryExPaymentModel recPaymentObj)
        {
            recPaymentObj.ClientIP = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => objRecBAL.InsertUpdateExRecovery(recPaymentObj));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Excess Recovery Payment Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetExcessRecoveryDetail( string empCode)
        {
            var mytask = Task.Run(() => objRecBAL.GetExcessRecoveryDetail(empCode));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Edit Excess Recovery Payment Record
        /// </summary>
        /// <param name="recExcessId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditExcessRecoveryDetail(int recExcessId)
        {
            var mytask = Task.Run(() => objRecBAL.EditExcessRecoveryDetail(recExcessId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Delete Excess Recovery Payment Record
        /// </summary>
        /// <param name="RecExcessId"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteExcessRecoveryData(int RecExcessId)
        {
            Task<string> myTask = Task.Run(() => objRecBAL.DeleteExcessRecoveryData(RecExcessId));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Forward Excess Recovery Payment Record
        /// </summary>
        /// <param name="recPaymentObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardExRecoveryData([FromBody]RecoveryExPaymentModel recPaymentObj)
        {
            recPaymentObj.ClientIP = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => objRecBAL.ForwardExRecoveryData(recPaymentObj));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        /// Insert Update Component Amount 
        /// </summary>
        /// <param name="recPaymentObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateComponentAmt([FromBody]RecoveryExPaymentModel recPaymentObj)
        {
            recPaymentObj.ClientIP = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => objRecBAL.InsertUpdateComponentAmt(recPaymentObj));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Component Amount Details
        /// </summary>
        /// <param name="Session"></param>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetComponentAmtDetails(string EmpId, string Session)
        {
            var mytask = Task.Run(() => objRecBAL.GetComponentAmtDetails(EmpId, Session));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Delete Component Amount
        /// </summary>
        /// <param name="CmpId"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> deleteComponentAmt(int CmpId)
        {
            Task<string> myTask = Task.Run(() => objRecBAL.DeleteComponentAmt(CmpId));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }


    }
}
