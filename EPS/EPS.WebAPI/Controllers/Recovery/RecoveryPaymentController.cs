﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.ExcessRecovery;
using EPS.BusinessModels.ExcessRecovery;
using EPS.BusinessModels.Recovery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.Recovery
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecoveryPaymentController : ControllerBase
    {
        ExcessRecoveryBAL ObjExcessRecoveryBAL = new ExcessRecoveryBAL();
        [HttpPost("[action]")]
        public string SaveTempPayment([FromBody]TemporaryPermanentStop ObjTempData)
        {
            return ObjExcessRecoveryBAL.SaveTempPayment(ObjTempData);
        }

        [HttpPost("[action]")]
        public string SaveRecoveryExcess([FromBody]ExcessRecoveryModel ObjTempData)
        {
            return ObjExcessRecoveryBAL.SaveRecoveryExcess(ObjTempData);

        }
        [HttpGet("[action]")]
        public IEnumerable<TemporaryPermanentStop> getTempPaymentRecovery()
        {
            return ObjExcessRecoveryBAL.getTempPaymentRecovery();

        }
    }
}