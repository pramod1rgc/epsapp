﻿using EPS.BusinessAccessLayers.Deputation;
using EPS.BusinessModels.Deputation;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.Repositories.Deputation;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Deputation
{
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RepatriationDetailsController : Controller
    {
        private IHttpContextAccessor _accessor;
        private IRepatriationDetails _repository;
        /// <summary>
        /// </summary>
        public RepatriationDetailsController(IHttpContextAccessor accessor, IRepatriationDetails repository)
        {
            _accessor = accessor;
            _repository = repository;
        }

        // GET: api/<controller>
      //  RepatriationDetailsBA objDepuatationBa = new RepatriationDetailsBA();

        #region  Repartiation Details  By Employee Code
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllRepatriationDetailsByEmployee(string empCd,int roleId)
        {
            var mytask = Task.Run(() => _repository.GetAllRepatriationDetailsByEmployee(empCd,roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region  Deputation in Check exists  By Employee Code
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> CheckDepuatationinEmployee(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.CheckDepuatationinEmployee(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region insert & update for Repartiation Details
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateRepatriationDetails([FromBody]RepatriationDetailsModel objRepatriationDetails)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            objRepatriationDetails.ipAddress = ip.ToString();
            var mytask = Task.Run(() => _repository.InsertUpdateRepatriationDetails(objRepatriationDetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
    }
}
