﻿using EPS.BusinessAccessLayers.Deputation;
using EPS.BusinessModels.Deputation;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.CommonClasses;
using EPS.Repositories.Deputation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Deputation
{
    /// <summary>
    /// 
    /// </summary>

    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DeputationInController : Controller
    {
        private IHttpContextAccessor _accessor;
        private IDeputationIn _repository;

        
        /// <summary>
        /// </summary>
        /// <param name="accessor"></param>
        public DeputationInController(IHttpContextAccessor accessor, IDeputationIn repository)
        {

            _accessor = accessor;
            _repository = repository;

        }
       // DeputationInBA objdeputaionBA = new DeputationInBA();
        // GET: api/<controller>
        #region  Deputation in  Get Pay Items
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayItems()
        {
            var mytask = Task.Run(() => _repository.GetPayItems());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region  Deputation in  Get Designation By Controller Id
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDesignation(string controllerId)
        {
            var mytask = Task.Run(() => _repository.GetAllDesignation(controllerId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion


        #region  Get Communication Addresss
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCommunicationAddress()
        {
            var mytask = Task.Run(() => _repository.GetCommunicationAddress());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get Depuatation In Details By Employee Code and Role Id
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDeductionScheduleDetails(string EmpCd,int roleId)
        {
            var mytask = Task.Run(() => _repository.GetDeductionScheduleDetails(EmpCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get Scheme Code By MsEmpDuesId
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetScheme_CodeByMsEmpDuesID(int MsEmpDuesID)
        {
            var mytask = Task.Run(() => _repository.GetScheme_CodeByMsEmpDuesID(MsEmpDuesID));
            var result = await mytask;
           // return result.ToString();
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region insert & update for Deputation In Details
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateDeputationInDetails([FromBody]DeputationInModel objDepuatationInDetails)
        {
            objDepuatationInDetails.ipAddress = CommonFunctions.GetClientIP(_accessor);

            var mytask = Task.Run(() => _repository.InsertUpdateDeputationInDetails(objDepuatationInDetails));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Update checker And Maker Forward Status
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateforwardStatusUpdateByDepId(int empDeputDetailsId, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.UpdateforwardStatusUpdateByDepId(empDeputDetailsId, status, rejectionRemark));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
  
    }
}
