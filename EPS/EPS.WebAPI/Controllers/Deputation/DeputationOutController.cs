﻿using EPS.BusinessAccessLayers.Deputation;
using EPS.BusinessModels.Deputation;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.Repositories.Deputation;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Deputation
{
    /// <summary>
    /// </summary>

    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DeputationOutController : Controller
    {
     
        private IHttpContextAccessor _accessor;
        private IDeputationOut _repository;
        /// <summary>
        /// </summary>
        public DeputationOutController(IHttpContextAccessor accessor, IDeputationOut repository)
        {
            _accessor = accessor;
            _repository = repository;

        }
       // DeputationOutBA objDepuatationBa = new DeputationOutBA();
        #region  Deputation Details  By Employee Code & Role id
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDepuatationOutDetailsByEmployee(string EmpCd,int roleId)
        {
            var mytask = Task.Run(() => _repository.GetAllDepuatationOutDetailsByEmployee(EmpCd,roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region insert & update for Deputation Out Details
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateDeputationOutDetails([FromBody]DeputationOutModel objDepuatationOutDetails)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            objDepuatationOutDetails.ipAddress = ip.ToString();
            var mytask = Task.Run(() => _repository.InsertUpdateDeputationOutDetails(objDepuatationOutDetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
    }
}
