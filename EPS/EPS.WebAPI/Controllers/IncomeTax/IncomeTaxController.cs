﻿using EPS.BusinessModels.IncomeTax;
using EPS.CommonClasses;
using EPS.Repositories.IncomeTax;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using static EPS.BusinessModels.IncomeTax.OtherDuesModel;

namespace EPS.WebAPI.Controllers.IncomeTax
{ 
    /// <summary>
    /// Income Tax Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class IncomeTaxController : Controller
    {
        private readonly IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        private IExemptionDeductionRepository _exemptionDeductionRepo;
        private IStandardDeductionRepository _standardDeductionRepo;
        private IOtherDuesRepository _otherDuesRepo;
        private IITRatesRepository _iTRatesRepo;

        /// <summary>
        /// Income Tax Constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="exemptionDeductionRepo"></param>
        /// <param name="standardDeductionRepo"></param>
        /// <param name="otherDuesRepo"></param>
        /// <param name="iTRatesRepo"></param>
        public IncomeTaxController(IHttpContextAccessor accessor, IExemptionDeductionRepository exemptionDeductionRepo, IStandardDeductionRepository standardDeductionRepo, IOtherDuesRepository otherDuesRepo, IITRatesRepository iTRatesRepo)
        {
            _accessor = accessor;
            _exemptionDeductionRepo = exemptionDeductionRepo;
            _standardDeductionRepo = standardDeductionRepo;
            _otherDuesRepo = otherDuesRepo;
            _iTRatesRepo = iTRatesRepo;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }

        #region Exemption deduction & rebate section

        /// <summary>
        /// Get major major section code
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetMajorSectionCode()
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetMajorSectionCode());

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get section code
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSectionCode(string majorSecCode)
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetSectionCode(majorSecCode));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get sub section code
        /// </summary>
        /// <param name="majorSecCode"></param>
        /// <param name="sectionCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSubSectionCode(string majorSecCode, string sectionCode)
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetSubSectionCode(majorSecCode, sectionCode));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get section code, sub section code and description
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSectionCodeDescription(string majorSectionCode, string sectionCode, string subSectionCode)
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetSectionCodeDescription(majorSectionCode, sectionCode, subSectionCode));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get slab limit for data
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSlabLimit(string payCommission)
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetSlabLimit(payCommission));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get pay commission
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayCommission()
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetPayCommission());

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get exemption deduction details
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetExemptionDeductionDetails(int pageNumber, int pageSize, string searchTerm)
        {
            var result = await Task.Run(() => _exemptionDeductionRepo.GetExemptionDeductionDetails(pageNumber, pageSize, searchTerm));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Insert and update exemption deduction and rebate details
        /// </summary>
        /// <param name="objExemptionDeductionDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<IActionResult> UpsertExemptionDeductionDetails([FromBody]ExemptionDeductionDetails objExemptionDeductionDetails)
        {
            objExemptionDeductionDetails.UserIp = clientIP;
            var result = await Task.Run(() => _exemptionDeductionRepo.UpsertExemptionDeductionDetails(objExemptionDeductionDetails));

            return Ok(result);
        }

        /// <summary>
        /// Delete exemption deduction and rebate details
        /// </summary>
        /// <param name="drrId"></param>
        /// <param name="drrId2"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteExemptionDeductionDetails(int drrId, int drrId2)
        {
            ExemptionDeductionDetails obj = new ExemptionDeductionDetails();
            var result = await Task.Run(() => _exemptionDeductionRepo.DeleteExemptionDeductionDetails(drrId, drrId2));

            return Json(result);
        }

        #endregion

        #region Standard Deduction

        /// <summary>
        /// Get Standard Deduction Master
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<GetStandardDeductionEntertainMasterDetail>> GetStandardDeductionEntertainMaster()
        {

            return await Task.Run(() => _standardDeductionRepo.GetStandardDeductionEntertainMasterBAL());
        }

        /// <summary>
        /// Get Standard Deduction Rule
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <param name="SearchTerm"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<StandardDeductionDetails>> GetStandardDeductionRule(int PageNumber, int PageSize, string SearchTerm)
        {

            return await Task.Run(() => _standardDeductionRepo.GetStandardDeductionRuleBAL(PageNumber, PageSize, SearchTerm));
        }

        /// <summary>
        /// Upsert Standard Deduction Rule
        /// </summary>
        /// <param name="objStandardDeductionDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<string> UpsertStandardDeductionRule([FromBody]UpsertStandardDeductionDetails objStandardDeductionDetails)
        {
            objStandardDeductionDetails.IpAddress = clientIP;
            string successMsg = await Task.Run(() => _standardDeductionRepo.UpsertStandardDeductionBAL(objStandardDeductionDetails));
            return successMsg;
        }

        /// <summary>
        /// Delete Standard Deduction Rule
        /// </summary>
        /// <param name="objStandardDeductionDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> DeleteStandardDeductionRule([FromBody]DeleteStandardDeductionDetails objStandardDeductionDetails)
        {

            string successMsg = await Task.Run(() => _standardDeductionRepo.DeleteStandardDeductionBAL(objStandardDeductionDetails));
            return successMsg;
        }

        /// <summary>
        /// Delete Standard deduction rate detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")]
        public async Task<ActionResult> DeleteStandardDeductionRateDetail(int id)
        {

            return Json(await Task.Run(() => _standardDeductionRepo.DeleteStandardDeductionRateDetailBAL(id)));
        }

        #endregion

        #region Other Dues
        /// <summary>
        /// Get other dues
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOtherDues(bool allDues = false)
        {

            var result = await Task.Run(() => _otherDuesRepo.GetOtherDues(allDues));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get major major section code for other dues
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetMajorSectionCodeForDues()
        {

            var result = await Task.Run(() => _otherDuesRepo.GetMajorSectionCode());

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Get other dues details
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOtherDuesDetails(int pageNumber, int pageSize, string searchTerm)
        {

            var result = await Task.Run(() => _otherDuesRepo.GetOtherDuesDetails(pageNumber, pageSize, searchTerm));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Save other dues details in database
        /// </summary>
        /// <param name="objOtherDuesDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<IActionResult> SaveOtherDuesDetails([FromBody]OtherDuesDetails objOtherDuesDetails)
        {
            objOtherDuesDetails.UserIp = clientIP;
            var result = await Task.Run(() => _otherDuesRepo.SaveOtherDuesDetails(objOtherDuesDetails));

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Delete other dues 
        /// </summary>
        /// <param name="duesId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteOtherDuesDetails(int duesId, string userName)
        {

            var result = await Task.Run(() => _otherDuesRepo.DeleteOtherDuesDetails(duesId, userName, clientIP));

            return Json(result);
        }

        #endregion

        #region IT Rates

        /// <summary>
        /// Get IT Rates
        /// </summary>
        /// <param></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IEnumerable<GetITRatesDetails>> GetITRates(string FinYear, string RateType, string RateFor)
        {

            return await Task.Run(() => _iTRatesRepo.GetITRatesBAL(FinYear, RateType, RateFor));

        }

        /// <summary>
        /// Upsert IT Rates
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<IEnumerable<ITRateDetailStatus>> UpsertITRates([FromBody]UpsertITRatesDetails obj)
        {
            obj.IpAddress = clientIP;
            return await Task.Run(() => _iTRatesRepo.UpsertITRatesBAL(obj));
        }

        /// <summary>
        /// Delete IT Rates
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ddo"></param>       
        /// <returns></returns>
        [HttpDelete("[action]/{id}/{modifiedBy}")]
        public async Task<ActionResult> DeleteITRates(int id, int ddo)
        {
            return Json(await Task.Run(() => _iTRatesRepo.DeleteITRatesBAL(id, clientIP, ddo)));

        }
        /// <summary>
        /// Get Rate Type and Rate For 
        /// </summary>
        /// <returns>RateType</returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<RateType>> GetRateTypeAndRateFor(string type)
        {
            return await Task.Run(() => _iTRatesRepo.GetRateTypeAndRateForBAL(type));

        }
        #endregion

    }
}