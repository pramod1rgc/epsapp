﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Pay Details
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PayDetailsController : Controller
    {
       
       private PayDetailsBA objBA = new PayDetailsBA();
        private IHttpContextAccessor _accessor;
        private IPayDetailsRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public PayDetailsController(IHttpContextAccessor accessor, IPayDetailsRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }


        /// <summary>
        /// Get Organisation Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOrganisationType(string EmpCode)
        {
            var mytask = Task.Run(() => _repository.GetOrganisationType(EmpCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }



        /// <summary>
        /// Get Pay Level  
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayLevel(string PayLevel)
        {
            var mytask = Task.Run(() => _repository.GetPayLevel());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        


        /// <summary>
        /// Get Pay Level  
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEntitledOffVeh(string Description,string Module)
        {
            var mytask = Task.Run(() => _repository.GetEntitledOffVeh(Module));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }



        /// <summary>
        /// Get Pay Index Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayIndex(string payLevel)
        {
            var mytask = Task.Run(() => _repository.GetPayIndex(payLevel));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }



        /// <summary>
        /// Get Basic Details Using PayLevel and Pay Index
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBasicDetails(string payLevel,int payIndex)
        {
            var mytask = Task.Run(() => _repository.GetBasicDetails(payLevel, payIndex));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
         

        /// <summary>
        /// Get Grade Pay
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGradePay()
        {
            var mytask = Task.Run(() => _repository.GetGradePay());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
         

        /// <summary>
        /// Get Pay Scale
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayScale(int gradePay)
        {
            var mytask = Task.Run(() => _repository.GetPayScale(gradePay));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
         

        /// <summary>
        /// Get Non Computational Dues
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNonComputationalDues()
        {
            var mytask = Task.Run(() => _repository.GetNonComputationalDues());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        

        /// <summary>
        /// Get Non Computational Deductions  
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNonComputationalDeductions()
        {
            var mytask = Task.Run(() => _repository.GetNonComputationalDeductions());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
             
        }
         
        /// <summary>
        /// Get Pay Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayDetails(string empCode,int roleId)
        {
            var mytask = Task.Run(() => _repository.GetPayDetails(empCode, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Insert Update pay Details
        /// </summary>
        /// <param name="objPayDetailsModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
      public async Task<IActionResult> SaveUpdatePayDetails([FromBody]PayDetailsModel objPayDetailsModel)
        {
            objPayDetailsModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var mytask = Task.Run(() => _repository.SaveUpdatePayDetails(objPayDetailsModel));
            var result = await mytask;
            if (result == 0) 
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);

            }

        }
    }
    
}
