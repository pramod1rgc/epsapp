﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessModels;
using Microsoft.AspNetCore.Http;

using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.Repositories.EmployeeDetails;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.CommonClasses;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// Employee Details.
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
   [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmployeeDetailsController : Controller
    {
       readonly EmployeeDetailsBA ObjEmployeeDetailsBA = new EmployeeDetailsBA();

        private IEmployeeDetailsRepository _repository;

        private IHttpContextAccessor _accessor;

         /// <summary>
         /// Get the employee Details
         /// </summary>
         /// <param name="accessor"></param>
         /// <param name="repository"></param>
        public EmployeeDetailsController(IHttpContextAccessor accessor , IEmployeeDetailsRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }


        /// <summary>
        /// Get Employee Verify Data
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetVerifyEmpCode()
        {
            var mytask = Task.Run(() => _repository.GetVerifyEmpCode());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// get Physical Details
        /// </summary>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPhysicalDisabilityTypes()
        {
            var mytask = Task.Run(() => _repository.GetPhysicalDisabilityTypes());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }



        /// <summary>
        /// get Employee Personal Details by Emp code 
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpPersonalDetailsByID(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetEmpPersonalDetailsByID(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// get Physical Details using employee code.
        /// </summary>
        /// <param name="empCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpPHDetails(string empCd)
        {
            var mytask = Task.Run(() => _repository.GetEmpPHDetails(empCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Save Emp Details
        /// </summary>
        /// <param name="ObjEmpDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SavePHDetails([FromBody]EmployeeDetailsModel ObjEmpDetails)
        {

            ObjEmpDetails.IpAddress = CommonFunctions.GetClientIP(_accessor);

            var myTask = Task.Run(() => _repository.SavePHDetails(ObjEmpDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);

        }

        /// <summary>
        /// Get Maker List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> MakerEmpList(int roleId)
        {
            var mytask = Task.Run(() => _repository.MakerGetEmpList(roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Save and Update Employee Details
        /// </summary>
        /// <param name="ObjEmpDetails"></param>
        /// <returns></returns>


        // POST api/<controller>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateEmpDetails([FromBody]EmployeeDetailsModel ObjEmpDetails)
        {

            ObjEmpDetails.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.UpdateEmpDetails(ObjEmpDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);

        }

        /// <summary>
        ///  Save and Update Employee Details
        /// </summary>
        /// <param name="ObjEmpDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> InsertUpdateEmpDetails([FromBody]EmployeeDetailsModel ObjEmpDetails)
        {
            ObjEmpDetails.IpAddress = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            var myTask = Task.Run(() => _repository.InsertUpdateEmpDetails(ObjEmpDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);


        }
        /// <summary>
        /// Delete Employee Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteEmployeeDetails(string empCd)
        {
            var myTask = Task.Run(() => _repository.DeleteEmployeeDetails(empCd));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);

        }

        /// <summary>
        /// Download Uploaded Files
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DownloadFile(string url)
        {

            if (url != "null")
            {
                // get the file and convert it into a bytearray
                var fileData = System.IO.File.ReadAllBytes(url);
                // MediaTypeHeaderValue mediaTypeHeaderValue = new MediaTypeHeaderValue("application/octet");
                return new FileContentResult(fileData, "application/octet")
                {
                 
                };
            }

            return null;
        }

        /// <summary>
        /// Get the employee is deputation or not
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetIsDeputEmp(string empCd)
        {
            var mytask = Task.Run(() => _repository.GetIsDeputEmp(empCd));
            var result = await mytask;
            return Ok(result);

        }

        /// <summary>
        /// Verify and Reject employee by checker
        /// </summary>
        /// <param name="ObjEmpDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> VerifyRejectionEmployeeDtl([FromBody]EmployeeDetailsModel ObjEmpDetails)
        {
            var myTask = Task.Run(() => _repository.VerifyRejectionEmployeeDtl(ObjEmpDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);

        }

    }
}
