﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ForwardToCheckerController : Controller
    {
       private ForwardToCheckerBA objForwardToCheckerBA = new ForwardToCheckerBA();
        private IHttpContextAccessor _accessor;
       ForwardToCheckerModel objForwardToCheckerModel =new ForwardToCheckerModel();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        /// 

        public ForwardToCheckerController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        #region get All EmpDetails
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]

        public async Task<IActionResult> GetAllEmpDetails(string empCd, int roleId)
        {
            var myTask = Task.Run(() => objForwardToCheckerBA.GetAllEmpDetails(empCd, roleId));
            List<ForwardToCheckerModel> result = await myTask;
            if (result == null)
            {
                return NotFound();  // return  status code 400    
            }
            return Ok(result); //ok return status code 200 ie success

        }
        #endregion

        #region For get all employee complete details
        /// <summary>
        /// Get All Employee Complete Details
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="empCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
      public async Task<IActionResult> GetAllEmployeeCompleteDetails(int roleId ,string empCode)
        {
            var myTask = Task.Run(() => objForwardToCheckerBA.GetAllEmployeeCompleteDetails(roleId, empCode));
            var result = await myTask;
            if (result == null)
            {
                return NotFound();  // return  status code 400    
            }
            return Ok(result); //ok return status code 200 ie success

        }
        #endregion

        /// <summary>
        /// Forward To Checker EmpDetails
        /// </summary>
        /// <param name="objForwardToCheckerModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> ForwardToCheckerEmpDetails([FromBody]ForwardToCheckerModel objForwardToCheckerModel)
        {
            objForwardToCheckerModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => objForwardToCheckerBA.ForwardToCheckerEmpDetails(objForwardToCheckerModel));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }



        /// <summary>
        /// Verify Employee Data
        /// </summary>
        /// <param name="objForwardToCheckerModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> VerifyEmpData([FromBody]ForwardToCheckerModel objForwardToCheckerModel)
        {
            objForwardToCheckerModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => objForwardToCheckerBA.VerifyEmpData(objForwardToCheckerModel));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }

    }
}