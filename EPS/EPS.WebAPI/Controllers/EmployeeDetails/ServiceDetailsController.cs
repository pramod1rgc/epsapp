﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Service Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ServiceDetailsController : Controller
    {

        private ServiceDetailsBA objBA = new ServiceDetailsBA();

        private IHttpContextAccessor _accessor;
        private IServiceDetailsRepository _repository;

       /// <summary>
       /// 
       /// </summary>
       /// <param name="accessor"></param>
       /// <param name="repository"></param>
        public ServiceDetailsController(IHttpContextAccessor accessor, IServiceDetailsRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }


        /// <summary>
        /// Get maintain by office City
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetMaintainByOfc()
        {
            var mytask = Task.Run(() => _repository.GetMaintainByOfc());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get service Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllServiceDetails(string EmpCode, int RoleId)
        {
            var mytask = Task.Run(() => _repository.GetAllServiceDetails(EmpCode, RoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Insert Update Posting Details
        /// </summary>
        /// <param name="objServiceDetailsModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateServiceDetails([FromBody]ServiceDetailsModel objServiceDetailsModel)
        {
            objServiceDetailsModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var mytask = Task.Run(() => _repository.SaveUpdateServiceDetails(objServiceDetailsModel));
            var result = await mytask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);

            }

        }


    }
}
