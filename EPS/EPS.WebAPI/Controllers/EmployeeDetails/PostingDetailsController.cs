﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Employee Posting Details
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PostingDetailsController : Controller
    {

       private PostingDetailsBA objBA = new PostingDetailsBA();
        private IHttpContextAccessor _accessor;
        private IPostingDetailsRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public PostingDetailsController(IHttpContextAccessor accessor, IPostingDetailsRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }

        /// <summary>
        /// Get HRA City
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetHRACity()
        {
            var mytask = Task.Run(() => _repository.GetHRACity());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Get TA City
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetTACity()
        {
            var mytask = Task.Run(() => _repository.GetTACity());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get All Designation
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDesignation()
        {
            var mytask = Task.Run(() => _repository.GetAllDesignation());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Insert Update Posting Details
        /// </summary>
        /// <param name="objPostingDetailsModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> SaveUpdatePostingDetails([FromBody]PostingDetailsModel objPostingDetailsModel)
        {
            objPostingDetailsModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var mytask = Task.Run(() => _repository.SaveUpdatePostingDetails(objPostingDetailsModel));
            var result = await mytask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);

            }

        }


        /// <summary>
        /// Get all details for posting details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllPostingDetails(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetAllPostingDetails(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
    }
}
