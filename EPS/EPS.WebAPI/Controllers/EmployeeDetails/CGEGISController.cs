﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// CGEGIS Details
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CGEGISController : Controller
    {
        public CGEGISBA objBA = new CGEGISBA();
        private IHttpContextAccessor _accessor;
        private ICGEGISRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public CGEGISController(IHttpContextAccessor accessor, ICGEGISRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }


        /// <summary>
        /// Get Insurance Applicable
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetInsuranceApplicable(string EmpCode)
        {
            var mytask = Task.Run(() => _repository.GetInsuranceApplicable(EmpCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

      
        /// <summary>
        /// Get Deputation Case Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCGEGISCategory(int insuranceApplicableId, string EmpCode)
        {
            var mytask = Task.Run(() => _repository.GetCGEGISCategory(insuranceApplicableId, EmpCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Save and Update for bank details
        /// </summary>
        /// <param name="objCGEGISModel"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateCGEGISDetails([FromBody]CGEGISModel objCGEGISModel)
        {
            objCGEGISModel.IpAddress = CommonFunctions.GetClientIP(_accessor);

            var myTask = Task.Run(() => _repository.SaveUpdateCGEGISDetails(objCGEGISModel));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Get CGEGISDetails
        /// </summary>
        /// <param name="EmpCode"></param>
        ///  /// <param name="RoleId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCGEGISDetails(string EmpCode, int RoleId)
        {
            var mytask = Task.Run(() => _repository.GetCGEGISDetails(EmpCode, RoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


    }
}
