﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using Microsoft.AspNetCore.Http;
using EPS.WebAPI.CustomFilters;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using EPS.Repositories.EmployeeDetails;
using EPS.CommonClasses;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Bank controller 
    /// </summary>
    /// 
    [EPSExceptionFilters]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BankDetailsController : Controller
    {
        public BankDetailsBA objBA = new BankDetailsBA();
        private IBankDetailsRepository _repository;

        private IHttpContextAccessor _accessor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public BankDetailsController(IHttpContextAccessor accessor, IBankDetailsRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }


        /// <summary>
        ///  get Bank details by ifsc code 
        /// </summary>
        /// 
    
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBankDetailsByIFSC(string IfscCD)
        {
            var mytask = Task.Run(() => _repository.GetBankDetailsByIFSC(IfscCD));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
         
        [HttpGet("[action]")]
        public IEnumerable<BankDetailsModel> GetAllIFSC()
        {
            return _repository.GetAllIFSC();
        }



        /// <summary>
        /// Get Bank details for emp code
        /// </summary>
        /// <param name="EmpCD"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBankDetails(string empCD, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetBankDetails(empCD, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Save and Update for bank details
        /// </summary>
        /// <param name="objNomineeDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateBankDetails([FromBody]BankDetailsModel objBankDetails)
        {
            objBankDetails.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.UpdateBankDetails(objBankDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }
    }
}
