﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// CGHS Details
    /// </summary> 
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CGHSDetailsController : ControllerBase
    {
        CGHSDetailsBA objBA = new CGHSDetailsBA();
        private readonly IHttpContextAccessor _accessor;
        private ICGHSRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public CGHSDetailsController(IHttpContextAccessor accessor, ICGHSRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }


        /// <summary>
        /// </summary>
        /// <param name="objCGEGISModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> SaveUpdateCGHSDetails([FromBody]CGHSModel objCGEGISModel)
        {
            objCGEGISModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.SaveUpdateCGHSDetails(objCGEGISModel));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }


        /// <summary>
        /// Get Bank details for emp code
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCGHSDetails(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetCGHSDetails(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

    }
}