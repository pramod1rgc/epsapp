﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;

using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Nominee Details
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class NomineeDetailsController : Controller
    {

       private NomineeDetailsBA objBA = new NomineeDetailsBA();
        private IHttpContextAccessor _accessor;
        private INomineeRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="_repository"></param>
        public NomineeDetailsController(IHttpContextAccessor accessor, INomineeRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }
        /// <summary>
        /// Get all Nominee Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
     
        public async Task<IActionResult> GetAllNomineeDetails(string empCd,int roleID)
        {
            var mytask = Task.Run(() => _repository.GetAllNomineeDetails(empCd, roleID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }
        /// <summary>
        /// Get Nominee Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="nomineeID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
    
        public async Task<IActionResult> GetNomineeDetails(string empCd, int nomineeID)
        {
            var mytask = Task.Run(() => _repository.GetNomineeDetails(empCd, nomineeID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objNomineeDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
       
        public async Task<IActionResult> UpdateNomineeDetails([FromBody]NomineeModel objNomineeDetails)
        {
            objNomineeDetails.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.UpdateNomineeDetails(objNomineeDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Delete Nominee Details using Emp code and Nominee ID
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="nomineeID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
    
        public async Task<IActionResult> DeleteNomineeDetails(string empCd, int nomineeID)
        {
            var mytask = Task.Run(() => _repository.DeleteNomineeDetails(empCd, nomineeID));
            var result = await mytask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);

            }

        }

    }
}
