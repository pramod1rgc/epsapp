﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Government quater detials
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class QuarterDetailsController : Controller
    {
        private QuarterDetailsBA objBA = new QuarterDetailsBA();
        private IHttpContextAccessor _accessor;
        private IQuaterDetailsRepository _repository;

       /// <summary>
       /// 
       /// </summary>
       /// <param name="accessor"></param>
       /// <param name="repository"></param>
        public QuarterDetailsController(IHttpContextAccessor accessor, IQuaterDetailsRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }

        /// <summary>
        /// Get QuarterOwned by Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetQuarterOwnedby()
        {
            var mytask = Task.Run(() => _repository.GetQuarterOwnedby());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Allotted To
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllottedTo()
        {
            var mytask = Task.Run(() => _repository.GetAllottedTo());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }



        /// <summary>
        /// Get Allotted To
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRentStatus()
        {
            var mytask = Task.Run(() => _repository.GetRentStatus());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }


        /// <summary>
        /// Get  Quarter Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetQuarterType()
        {
            var mytask = Task.Run(() => _repository.GetQuarterType());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }



        /// <summary>
        /// Get  Quarter Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCustodian(int qrtrOwnedBy)
        {
            var mytask = Task.Run(() => _repository.GetCustodian(qrtrOwnedBy));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }


        /// <summary>
        /// Get GPRACityLocation
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGPRACityLocation()
        {
            var mytask = Task.Run(() => _repository.GetGPRACityLocation());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }

        /// <summary>
        /// Get Nominee Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="mSEmpAccmID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> QuarterDetails(string empCd, int mSEmpAccmID)
        {
            var mytask = Task.Run(() => _repository.QuarterDetails(empCd, mSEmpAccmID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get Quarter All Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> QuarterAllDetails(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.QuarterAllDetails(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objQuarterDetailsModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> SaveUpdateQuarterDetails([FromBody]QuarterDetailsModel objQuarterDetailsModel)
        {
            objQuarterDetailsModel.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.SaveUpdateQuarterDetails(objQuarterDetailsModel));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }


        /// <summary>
        /// Delete Nominee Details using Emp code and Nominee ID
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="mSEmpAccmID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteQuarterDetails(string empCd, int mSEmpAccmID)
        {
            var mytask = Task.Run(() => _repository.DeleteQuarterDetails(empCd, mSEmpAccmID));
            var result = await mytask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);

            }

        }


    }
}
