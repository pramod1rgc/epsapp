﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.EmployeeDetails;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.Repositories.EmployeeDetails;

using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.EmployeeDetails
{
    /// <summary>
    /// Family Details 
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FamilyDetailsController : Controller
    {
        // GET: /<controller>/

       private FamilyDetailsBA objFamilyDetailsBA = new FamilyDetailsBA();
        private IHttpContextAccessor _accessor;
        private IFamilyRepository _repository;

      /// <summary>
      /// 
      /// </summary>
      /// <param name="accessor"></param>
      /// <param name="repository"></param>
        public FamilyDetailsController(IHttpContextAccessor accessor, IFamilyRepository repository)
        {
            _repository = repository;
               _accessor = accessor;
        }

        #region for get maritalStatus
        /// <summary>
        /// for get maritalStatus
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]

        public async Task<IActionResult> GetMaritalStatus()
        {
            var mytask = Task.Run(() => objFamilyDetailsBA.GetMaritalStatus());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion


        #region for get Family Details=================
        /// <summary>
        /// check for get Family Details
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msDependentDetailID"></param>
        /// <param name="EmpCd"></param>
        /// <param name="MsDependentDetailID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetFamilyDetails(string empCd, int msDependentDetailID)
        {
            var mytask = Task.Run(() => _repository.GetFamilyDetails(empCd, msDependentDetailID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]

        public async Task<IActionResult> GetAllFamilyDetails(string EmpCd, int RoleId)
        {
            var myTask = Task.Run(() => _repository.GetAllFamilyDetails(EmpCd, RoleId));
            List<FamilyDetailsModel> result = await myTask;
            if (result == null)
            {
                return NotFound();  // return  status code 400    
            }
            return Ok(result); //ok return status code 200 ie success

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objFamilyDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> UpdateEmpFamilyDetails([FromBody]FamilyDetailsModel objFamilyDetails)
        {
            objFamilyDetails.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.UpdateEmpFamilyDetails(objFamilyDetails));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Delete for family details
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <param name="MsDependentDetailID"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteFamilyDetails(string EmpCd, int MsDependentDetailID)
        {
            var mytask = Task.Run(() => _repository.DeleteFamilyDetails(EmpCd, MsDependentDetailID));
            var result = await mytask;
            if (result == 0)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);

        }
    }
}
