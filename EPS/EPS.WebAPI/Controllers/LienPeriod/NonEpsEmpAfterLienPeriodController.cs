﻿using EPS.BusinessAccessLayers.LienPeriod;
using EPS.BusinessModels.LienPeriod;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.CommonClasses;
using EPS.Repositories.LienPeriod;

namespace EPS.WebAPI.Controllers.LienPeriod
{

    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class NonEpsEmpAfterLienPeriodController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private INonEpsEmployeejoinAfterLienPeriod _repository;
        /// <summary>
        /// </summary>
        public NonEpsEmpAfterLienPeriodController(IHttpContextAccessor accessor, INonEpsEmployeejoinAfterLienPeriod repository)
        {
            _repository = repository;
            _accessor = accessor;

        }
       // NonEpsEmployeejoinAfterLienPeriodBA objNonEpsEmpJoinAfterLienPeriodBA = new NonEpsEmployeejoinAfterLienPeriodBA();
        #region  Get Non EPS Employee Details Join After Lien period
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Save and update Non Eps Employee join details after lien period
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateNonEpsEmployeeJoinAfterLienPeriod([FromBody]NonEpsEmpJoinAfterLienPeriodeModel objmodel)
        {
            //var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            objmodel.ipAddress = CommonFunctions.GetClientIP(_accessor);
           // objmodel.ipAddress = ip.ToString();
            var mytask = Task.Run(() => _repository.SaveUpdateNonEpsEmployeeJoinAfterLienPeriod(objmodel));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result.ToString());
            }

        }
        #endregion
        #region Delete Non EPS Employee Join by Lien Id 
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(string empCd,string orderNo)
        {
            var mytask = Task.Run(() => _repository.DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(empCd, orderNo));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Update cheker & maker Forward Status
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
    }
}