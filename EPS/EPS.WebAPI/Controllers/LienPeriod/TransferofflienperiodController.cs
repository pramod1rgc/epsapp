﻿using EPS.BusinessAccessLayers.LienPeriod;
using EPS.BusinessModels.LienPeriod;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.CommonClasses;
using EPS.Repositories.LienPeriod;



// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.LienPeriod
{
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TransferofflienperiodController : Controller
    {
        private IHttpContextAccessor _accessor;
        private ITransferOffLienPeriod _repository;
        /// <summary>
        /// </summary>
        public TransferofflienperiodController(IHttpContextAccessor accessor, ITransferOffLienPeriod repository)
        {
            _repository = repository;
            _accessor = accessor;

        }
        //TransferOffLienPeriodBA objTransferOffLienPeriodBA = new TransferOffLienPeriodBA();
        // GET: api/<controller>
        #region Get Controller List
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetControllList()
        {
            var mytask = Task.Run(() => _repository.GetControllerList());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Get DDO List By Controller Id
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDdolistbyControllerid(int controllerId)
        {
            var mytask = Task.Run(() => _repository.GetDdolistbyControllerid(controllerId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get Office Address Details By Controller Id and DDO ID Or OFFice Name
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> getOfficeAddressDetails(int controllerId,int ddoId,string officeName)
        {
            var mytask = Task.Run(() => _repository.GetOfficeAddressDetails(controllerId,ddoId,officeName));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region get Transfer Office Details By Employee Id and Role id
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> getTransferofficelienPeriodDetails(string empCd, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetTransferofficelienPeriodDetails(empCd,roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Save and Update Transfer Office lien Period Details
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateTransferOffLienPeriod([FromBody]TransferofflienperiodModel objTransferofflienPeriodDetails)
        {
            
            objTransferofflienPeriodDetails.ipAddress = CommonFunctions.GetClientIP(_accessor);
           
            var mytask = Task.Run(() => _repository.InsertUpdateTransferOffLienPeriod(objTransferofflienPeriodDetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Update Cheker and maker Forward Status
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.UpdateforwardStatusUpdateByEmpCode(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Delete Transfer  Office lien period details 
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteTransferOffLienPeriod(string empCd,string orderNo)
        {
            var mytask = Task.Run(() => _repository.DeleteTransferOffLienPeriod(empCd, orderNo));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
    }
}
