﻿using EPS.BusinessAccessLayers.LienPeriod;
using EPS.BusinessModels.LienPeriod;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.CommonClasses;
using EPS.Repositories.LienPeriod;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.LienPeriod
{
    /// <summary>
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EpsEmpJoinOffAfterLienPeriodController : Controller
    {
        private IHttpContextAccessor _accessor;
        private IEpsEmpJoinAfterLienPeriod _repository;



        /// <summary>
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public EpsEmpJoinOffAfterLienPeriodController(IHttpContextAccessor accessor, IEpsEmpJoinAfterLienPeriod repository)
        {

            _accessor = accessor;
            _repository = repository;

        }
       // EpsEmpJoinAfterLienPeriodBA objEpsEmpJoinAfterLienPeriodBA = new EpsEmpJoinAfterLienPeriodBA();
        #region  Get EPS Employee Details Join After Lien period
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId,int controllerId)
        {
            var mytask = Task.Run(() => _repository.GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd, roleId,controllerId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Save and update Eps Employee join details after lien period
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateEpsEmployeeJoinAfterLienPeriod([FromBody]EpsEmpJoinAfterLienPeriodModel objmodel)
        {
            //var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            //objmodel.ipAddress = ip.ToString();
            objmodel.ipAddress = CommonFunctions.GetClientIP(_accessor);
            var mytask = Task.Run(() => _repository.InsertUpdateEpsEmployeeJoinAfterLienPeriod(objmodel));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Delete EPS Employee Join by Lien Id 
        /// <summary>
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteEpsEmployeeJoinById(int lienId)
        {
            var mytask = Task.Run(() => _repository.DeleteEpsEmployeeJoinByEmpCode(lienId));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Update cheker & maker Forward Status
        /// <summary>
        /// </summary>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.UpdateforwardStatusUpdateByEmpCode(empCd, orderNo, status,rejectionRemark));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

    }
}
