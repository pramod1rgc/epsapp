﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers;
using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.Repositories.UserManagement;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace EPS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LoginRoleController : Controller
    {
        //public LoginBusinessAccessLayer loginBusinessAccessLayer = new LoginBusinessAccessLayer();
        private IConfiguration _config;
        private ILoginRepository _repository;

        public LoginRoleController(IConfiguration config, ILoginRepository repository)
        {
            _config = config;
            _repository = repository;
        }


        //public string SHA512()
        //{
        //    var hashAlgorithm = new Org.BouncyCastle.Crypto.Digests.Sha3Digest(512);

        //    // Choose correct encoding based on your usecase
        //    byte[] input = Encoding.ASCII.GetBytes("test");

        //    hashAlgorithm.BlockUpdate(input, 0, input.Length);

        //    byte[] result = new byte[64]; // 512 / 8 = 64
        //    hashAlgorithm.DoFinal(result, 0);

        //    string hashString = BitConverter.ToString(result);
        //    hashString = hashString.Replace("-", "").ToLowerInvariant();

        //    Console.WriteLine(hashString);

        //    return "";
        //}

        /// <summary>
        /// Login Check
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> LoginCheck(string Username, string Password)
        {
            LoginModel loginModel = new LoginModel();
            LoginModel obj = new LoginModel();
            loginModel.Username = Username;
            loginModel.Password = Password;
            List<LoginModel> objList = new List<LoginModel>();
            string Status = string.Empty;
            int CurrRoleID = 0;
            var tokenString = string.Empty;
            var mytask = Task.Run(() => _repository.LoginCheck(loginModel));
            var result = await mytask;
            string[] Arrrolestatus = result.Split('_');
            CurrRoleID = Convert.ToInt32(Arrrolestatus[0]);
            if (CurrRoleID <= 100)
            {
                Status = "yes";
                tokenString = BuildToken(loginModel);
                GeneratedToken.Token = tokenString;
                obj.JWT_Secret = tokenString;
                obj.UserType = Arrrolestatus[1];
                obj.Status = Status;
                objList.Add(obj);
            }
            else
            {
                Status = "No";
            }
            if (obj == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(obj);
            }
        }

        /// <summary>
        /// Build Token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        private string BuildToken(LoginModel user)
        {
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Email, user.Password),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
               };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(300),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// User Details
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<userDetails>> UserDetails(string username)
        {
            var mytask = Task.Run(() => _repository.UserDetails(username));
            var result = await mytask;
            return result;
            
        }

        /// <summary>
        /// Login New User
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> LoginNewUser([FromBody] LoginModel loginModel)
        {
            var mytask = Task.Run(() => _repository.LoginNewUser(loginModel));
            var result = await mytask;
            return result;

        }

        /// <summary>
        /// Role Activation
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<string> RoleActivation(int ID)
        {
            var mytask = Task.Run(() => _repository.RoleActivation(ID));
            var result = await mytask;
            return result;
        }

        /// <summary>
        /// currentr Url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public string currentrUrl(string url)
        {
            string[] webUrl = url.Split("login");
            GeneratedToken.Url = webUrl[0];
            return "";

        }

        /// <summary>
        /// IsMenuPermissionAssigned
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="uroleid"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<bool> IsMenuPermissionAssigned(string Username, string uroleid)
        {
            //return _repository.IsMenuPermissionAssigned(Username, uroleid);
            var mytask = Task.Run(() => _repository.IsMenuPermissionAssigned(Username, uroleid));
            var result = await mytask;
            return result;
        }

    }
}
