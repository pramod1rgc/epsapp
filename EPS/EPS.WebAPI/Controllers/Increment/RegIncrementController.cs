﻿using EPS.BusinessAccessLayers.Increment;
using EPS.BusinessModels.Increment;
using EPS.Repositories.Increment;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Increment
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RegIncrementController:Controller
    {
        RegularIncrement_BAL objReg = new RegularIncrement_BAL();
        private IHttpContextAccessor _accessor;
        private IRegIncrementRepository _repository;
        /// <summary>
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="increpository"></param>
        public RegIncrementController(IHttpContextAccessor accessor, IRegIncrementRepository increpository)
        {
            this._accessor = accessor;
            this._repository = increpository;
        }

        /// <summary>
        /// Get Employee Designation
        /// </summary>
        /// <param name="PermDdoId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeDesignation(string PermDdoId)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeDesignation(PermDdoId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Create Order No
        /// </summary>
        /// <param name="advmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> CreateOrderNo([FromBody]RegIncrement_Model advmObj)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            advmObj.ClientIP = ip.ToString();
            var result = await Task.Run(() => _repository.CreateOrderNo(advmObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }

        /// <summary>
        /// Get Order Details
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="orderType"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOrderDetails(string designID, string paycodeID,string orderType)
        {
            var mytask = Task.Run(() => _repository.GetOrderDetails(designID, paycodeID, orderType));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Get Employee For Increment On behalf of Order No
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="orderID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<RegIncrement_Model> GetEmployeeForIncrement(string designID, string paycodeID, int orderID)
        {
            return _repository.GetEmployeeForIncrement(designID, paycodeID, orderID);
        }

        /// <summary>
        /// Get Employee who is not the part of Increment On behalf of Order No
        /// </summary>
        /// <param name="designID"></param>
        /// <param name="paycodeID"></param>
        /// <param name="orderID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeForNonIncrement(string designID, string paycodeID, int orderID)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeForNonIncrement(designID, paycodeID, orderID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Insert Regular Increment Data
        /// </summary>
        /// <param name="advmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertRegIncrementData([FromBody] object[] advObj)
        {
            var result = await Task.Run(() => _repository.InsertRegIncrementData(advObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
        /// <summary>
        /// Delete Order details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> deleteOrderDetails(int id,string type)
        {
            var result = await Task.Run(() => _repository.deleteOrderDetails(id, type)).ConfigureAwait(true);
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }




        /// <summary>
        /// Forward to ddo Checker
        /// </summary>
        /// <param name="advObj"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardtocheckerForInc([FromBody]object[] advObj)
        {
            var result = await Task.Run(() => _repository.ForwardtocheckerForInc(advObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }


    }
}
