﻿using EPS.BusinessAccessLayers.Increment;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using EPS.Repositories.Increment;
using EPS.BusinessModels.Increment;
using EPS.CommonClasses;

namespace EPS.WebAPI.Controllers.Increment
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class StopIncrementController: Controller
    {
        StopIncrement_BAL objStopInc = new StopIncrement_BAL();
        private IHttpContextAccessor _accessor;
        private IStopIncRepository _repository;
        private static string clientIP = string.Empty;
        /// <summary>
        /// Controller for perform crud operation of stop increment form
        /// </summary>
        /// <param name="accessor"></param>
        public StopIncrementController(IHttpContextAccessor accessor, IStopIncRepository incRepository)
        {
            this._accessor = accessor;
            this._repository = incRepository;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeStopIncrement(int orderID)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeStopIncrement(orderID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Employee who is not the part of stop Increment On behalf of Order No
        /// </summary>
        /// <param name="orderID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeForNotStop(int orderID)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeForNotStop(orderID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Insert stop Increment Data
        /// </summary>
        /// <param name="advmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertStopIncrementData([FromBody]object[] advObj)
        {
            var myTask = Task.Run(() => _repository.InsertStopIncrementData(advObj));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Forward to ddo Checker stop Increment
        /// </summary>
        /// <param name="advObj"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardcheckerForStopInc([FromBody] object[] advObj)  //RegIncrement_Model
        {
           // advObj.ClientIP = clientIP;
            var myTask = Task.Run(() => _repository.ForwardcheckerForStopInc(advObj));
            var result = await myTask;
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
    }
}
