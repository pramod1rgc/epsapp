﻿using EPS.BusinessAccessLayers.Increment;
using EPS.BusinessModels.Increment;
using EPS.Repositories.Increment;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Increment
{
    /// <summary>
    ///  Controller for advance incrment crud operation
    /// </summary>

    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AdvIncrementController : Controller
    {
        AdvIncrement_BAL objAdv = new AdvIncrement_BAL();
        private IAdvIncrementRepository _repository;
        private IHttpContextAccessor _accessor;

        public AdvIncrementController(IHttpContextAccessor accessor, IAdvIncrementRepository increpository)
        {
            this._accessor = accessor;
            this._repository = increpository;
        }
        /// <summary>
        /// Create Advance Increment
        /// </summary>
        /// <param name="advmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> CreateAdvIncrement([FromBody]AdvIncrement_Model advmObj)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            advmObj.ClientIP = ip.ToString();
            var result = await Task.Run(() => _repository.CreateAdvIncrement(advmObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
            
        }


        /// <summary>
        /// Get Increment details for bind grid data
        /// </summary>
        /// <param name="empcode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetIncrementDetails(string empcode)
        {
            var mytask = Task.Run(() => _repository.GetIncrementDetails(empcode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get Empoyee for bind drop down
        /// </summary>
        /// <param name="msDesignID"></param>
        /// <param name="paycomm"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeByDesigPayComm(string msDesignID, string paycomm)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeByDesigPayComm(msDesignID, paycomm));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        ///  Get details for update advance increment
        /// </summary>
        /// <param name="incID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditAdvIncrement(int incID)
        {
            var mytask = Task.Run(() => _repository.EditAdvIncrement(incID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        /// delete advance details
        /// </summary>
        /// <param name="incID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteAdvDetails(int incID)
        {
            var result = await Task.Run(() => _repository.DeleteAdvDetails(incID)).ConfigureAwait(true);
            if (result == "0" || result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }


        /// <summary>
        /// Forward to DDo checker
        /// </summary>
        /// <param name="advmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Forwardtochecker([FromBody]AdvIncrement_Model advmObj)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            advmObj.ClientIP = ip.ToString();
            var result = await Task.Run(() => _repository.Forwardtochecker(advmObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }
    }
}
