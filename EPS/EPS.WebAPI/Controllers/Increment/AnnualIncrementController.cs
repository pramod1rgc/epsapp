﻿using EPS.BusinessAccessLayers.Increment;
using EPS.BusinessModels.Increment;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using EPS.Repositories.Increment;

namespace EPS.WebAPI.Controllers.Increment
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AnnualIncrementController
    {
        AnnualIncrement_BAL objAdvBAL = new AnnualIncrement_BAL();
        private IAnnualIncRepository _repository;

        public AnnualIncrementController(IAnnualIncRepository increpository)
        {
            this._repository = increpository;
        }



        [HttpGet("[action]")]
        public IEnumerable<AdvIncrement_Model> GetSalaryMonth()
        {
            return _repository.GetSalaryMonth();
        }

        [HttpGet("[action]")]
        public IEnumerable<AdvIncrement_Model> GetOrderWithEmployee(string paycodeID)
        {
            try
            {
                return _repository.GetOrderWithEmployee(paycodeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("[action]")]
        public IEnumerable<AdvIncrement_Model> GetAnnualIncReportData(string paycodeID,string orderID)
        {
            try
            {
                return _repository.GetAnnualIncReportData(paycodeID, orderID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("[action]")]
        public IEnumerable<AdvIncrement_Model> GetEmpDueForIncReport(string designCode)
        {
            try
            {
                return _repository.GetEmpDueForIncReport(designCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
