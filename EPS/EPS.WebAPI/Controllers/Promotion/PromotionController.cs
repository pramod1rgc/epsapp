﻿using EPS.BusinessAccessLayers.Promotion;
using EPS.BusinessModels.Promotion;
using EPS.CommonClasses;
using EPS.Repositories.Promotion;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Promotion
{
    /// <summary>
    /// Promotion controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PromotionController : Controller
    {

        private IPromotion _iPromotion;
        private readonly IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;


        /// <summary>
        /// Income Tax Constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="ipromotion"></param>
        public PromotionController(IHttpContextAccessor accessor,IPromotion ipromotion)
        {
            _accessor = accessor;
            _iPromotion = ipromotion;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }
        #region PromotionWithoutTransfer
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<string> UpdatePromotionDetailsWithoutTransfer([FromBody]PromotionDetails obj)
        {
            obj.IpAddress = clientIP;
            return await Task.Run(()=> _iPromotion.UpdatePromotionDetailsWithoutTransferBAL(obj));
           // PromotionBAL objProBAL = new PromotionBAL();
           // string successMsg = await Task.Run(() => objProBAL.UpdatePromotionDetailsWithoutTransferBAL(obj));
           // return successMsg; // !successMsg.Equals(string.Empty) ? (successMsg == "1" ? "Data saved successfully!" : (successMsg == "-1" ? "Order Number Already Exist!" : string.Empty)) : "Some Error Occurred.";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchTerm"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<GetPromotionDetails>> GetPromotionDetailsWithoutTransfer(string empCode, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
           return await Task.Run(() => _iPromotion.GetPromotionDetailsWithoutTransferBAL(empCode, pageNumber, pageSize, searchTerm, roleId));
           // PromotionBAL objProBAL = new PromotionBAL();
            //return await Task.Run(() => objProBAL.GetPromotionDetailsWithoutTransferBAL(empCode, pageNumber, pageSize, searchTerm, roleId));
        }


        /// <summary>
        /// Delete Promotion record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePromotionDetails(int id)
        {
            return Json(await Task.Run(() => _iPromotion.DeletePromotionDetails(id)));
            //PromotionBAL objProBAL = new PromotionBAL();
            //return Json(await Task.Run(() => objProBAL.DeletePromotionDetails(id)));
        }


        /// <summary>
        /// Forward to checker
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> ForwardTransferDetailToChecker([FromBody]ForwardTransferDetails obj)
        {
            return await Task.Run(() => _iPromotion.ForwardTransferDetailToChecker(obj));
        }


        /// <summary>
        /// Update Status
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> UpdateStatus([FromBody]ForwardTransferDetails obj)
        {
            return await Task.Run(() => _iPromotion.UpdateStatus(obj));
        }

        #endregion



        #region ReversionWithoutTransfer
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<string> UpdateReversionDetailsWithoutTransfer([FromBody]ReversionDetails obj)
        {
            obj.IpAddress = clientIP;
           return await _iPromotion.UpdateReversionDetailsWithoutTransferBAL(obj);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchTerm"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<GetReversionDetails>> GetReversionDetailsWithoutTransfer(string empCode, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
            return await Task.Run(() => _iPromotion.GetReversionDetailsWithoutTransferBAL(empCode, pageNumber, pageSize, searchTerm, roleId));
        }


        /// <summary>
        /// Delete Reversion record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("reversion/{id}")]
        public async Task<ActionResult> DeleteReversionDetails(int id)
        {
            return Json(await Task.Run(() => _iPromotion.DeleteReversionDetails(id)));
        }

        #endregion
    }
}
