﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessAccessLayers;
using EPS.BusinessModels;
using EPS.WebAPI.CustomFilters;

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// Role Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class RoleController : Controller
    {
        #region Role Assining
        RoleBusinessAccessLayer ObjRoleBusinessAccessLayer = new RoleBusinessAccessLayer();
        /// <summary>
        /// Create New Role
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public string CreateNewRole([FromBody]MstRole mstRole)
        {
            return ObjRoleBusinessAccessLayer.CreateNewRole(mstRole);
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<RoleIDS> GetAlRoles(string username)
        {

            return ObjRoleBusinessAccessLayer.GetAlRoles(username);

        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<RoleIDS> GetAllActiveRollList(string username)
        {
            return ObjRoleBusinessAccessLayer.GetAllActiveRollList(username);
        }

        /// <summary>
        /// Get All CustomeRoll List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<RoleIDS> GetAllCustomeRollList(string username)
        {
            return ObjRoleBusinessAccessLayer.GetAllCustomeRollList(username);
        }


        /// <summary>
        /// Get Bind User List ddl
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<RoleIDS> GetBindUserListddl(string RdStatus, string LoggRollID, string LoogInUser, string ControleerID, string PAOID, string DDOID, string CurrUserControllerID)
        {
            return ObjRoleBusinessAccessLayer.GetBindUserListddl(RdStatus, LoggRollID, LoogInUser, ControleerID, PAOID, DDOID, CurrUserControllerID);
        }


        /// <summary>
        /// Get User Details
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public IEnumerable<MstRole> GetUserDetails([FromBody]MstRole userRole)
        {

            return ObjRoleBusinessAccessLayer.GetUserDetails(userRole);

        }
        /// <summary>
        /// Revoke User
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string RevokeUser([FromBody]MstRole mstRole)
        {

            return ObjRoleBusinessAccessLayer.RevokeUser(mstRole);

        }
        /// <summary>
        /// Get User Details By Pan
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public IEnumerable<MstRole> GetUserDetailsByPan([FromBody]MstRole mstRole)
        {

            return ObjRoleBusinessAccessLayer.GetUserDetailsByPan(mstRole);

        }
        /// <summary>
        /// Save Assign Role For User
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string SaveAssignRoleForUser([FromBody]MstRole mstRole)
        {
            return ObjRoleBusinessAccessLayer.SaveAssignRoleForUser(mstRole);

        }
        /// <summary>
        /// Get All Menu List
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public List<MstRole> GetAllMenuList(string roleID, string Uname, string CurrURole)
        {
            return ObjRoleBusinessAccessLayer.GetAllMenuList(roleID, Uname, CurrURole);
        }

        [HttpPost("[action]")]
        public IEnumerable<MstRole> GetAssignRoleForUser(string roleID)
        {
            return ObjRoleBusinessAccessLayer.GetAssignRoleForUser(roleID);
        }
        /// <summary>
        /// Save Menu Per Mission
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string SaveMenuPerMission([FromBody]string[] mstMenu)
        {
            return ObjRoleBusinessAccessLayer.SaveMenuPerMission(mstMenu);

        }
        /// <summary>
        /// Check Menu Per Mission
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string CheckMenuPerMission([FromBody]string[] mstMenu)
        {
            return ObjRoleBusinessAccessLayer.CheckMenuPerMission(mstMenu);
        }
        #endregion


    }
}