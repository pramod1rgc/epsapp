﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.UserManagment;
using EPS.BusinessModels.UserManagment;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.UserManagment
{
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class OnBoardingController : ControllerBase
    {
        readonly OnBoardingBussinessAccessLayer objBA = new OnBoardingBussinessAccessLayer();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjOnBoardingModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> OnBoardingSubmit([FromBody]OnBoardingModel ObjOnBoardingModel)
        {
            // ObjEmpDetails.IpAddress = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            var myTask = Task.Run(() => objBA.OnBoardingSubmit(ObjOnBoardingModel));
            int result = await myTask;
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllReuestNoOfOnboarding()
        {
            var myTask = Task.Run(() => objBA.GetAllReuestNoOfOnboarding());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Fetch Request Letter No Record
        /// </summary>
        /// <param name="OnboardingId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> FetchRequestLetterNoRecord(int OnboardingId)
        {
            var myTask = Task.Run(() => objBA.FetchRequestLetterNoRecord(OnboardingId));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> CancelRequestLetterNo(string Remarks)
        {
            string Subject = string.Empty;
            string EmployeeEmailID = string.Empty;
            EmployeeEmailID = "tabarakzee@gmail.com";
            var myTask = Task.Run(() => objBA.CancelRequestLetterNo(Remarks));
            var result = await myTask;
            if (result != null)
            {
                Subject = "Cancel Request No";
                string Body = "<br/>Your request has been cancel due to unmatch records" + ".</b> <br/> Please contact to your concern person" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
    }
}
