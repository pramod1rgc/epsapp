﻿using System;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.Repositories.UserManagement;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static EPS.CommonClasses.EPSEnum;

namespace EPS.WebAPI.Controllers.UserManagment
{

    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class makerController : Controller
    {
        private IDDOMakerRoleRepository _repository;
        public makerController(IDDOMakerRoleRepository repository)
        {
            _repository= repository;
        }
        /// <summary>
        /// Assigned maker Emp List
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedmakerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => _repository.AssignedmakerEmpList(DDOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// maker Emp List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> makerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => _repository.makerEmpList(DDOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Assigned Maker 
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="DDOID"></param>
        /// <param name="UserID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedMaker(string empCd, int DDOID,int UserID, string active)
        {
            string status = string.Empty;
            string RoleName = string.Empty;
            string EmployeeEmailID = string.Empty;
            string Subject = string.Empty;
            string CurrentUserID = string.Empty;
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string URL = GeneratedToken.Url;
            string port = "activate";
            var myTask = Task.Run(() => _repository.AssignedMaker(empCd, DDOID, UserID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");

            EmployeeEmailID = "tabarakzee@gmail.com";
            RoleName = ArrResult[0].ToUpper().Trim();

            if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.ASN))
            {
                status = EPSResource.ASN;
                Subject = "New User created";
                CurrentUserID = "User ID:    " + NewUserID[0];
                var varifyUrl = URL + port + "?id=" + NewUserID[1];
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + CurrentUserID + "<br/>" + "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                              " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if(RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.USN))
            {
                status = EPSResource.USN;
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            return Ok(status);
        }

        /// <summary>
        /// Self Assigned DDO Maker
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="username"></param>
        /// <param name="EmpPermDDOId"></param>
        /// <param name="ddoid"></param>
        /// <param name="IsAssign"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> SelfAssignedDDOMaker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            var mytask = Task.Run(() => _repository.SelfAssignedDDOMaker(UserID, username, EmpPermDDOId, ddoid, IsAssign));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Self Assign Maker Role
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> SelfAssignMakerRole(string UserID)
        {
            var mytask = Task.Run(() => _repository.SelfAssignMakerRole(UserID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

    }
}
