﻿using EPS.BusinessAccessLayers;
using EPS.BusinessModels;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// Menu Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MenuController : Controller
    {
        #region For menu 
        MenuBusinessAccessLayer ObjmenuBusinessAccessLayer = new MenuBusinessAccessLayer();
        /// <summary>
        /// For Bind Drop Down Menu
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<MstMenuModel> BindDropDownMenu()
        {

            return ObjmenuBusinessAccessLayer.BindDropDownMenu();


        }
        /// <summary>
        /// Save Menu
        /// </summary>
        /// <param name="hdnMenuId"></param>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string SaveMenu(int hdnMenuId,string uRoleID, [FromBody]MstMenuModel mstMenu)
        {

            return ObjmenuBusinessAccessLayer.SaveMenu(hdnMenuId, uRoleID, mstMenu);


        }
        /// <summary>
        /// Bind Menu In Gride
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<MstMenuModel> BindMenuInGride()
        {
            return ObjmenuBusinessAccessLayer.BindMenuInGride();
        }

        /// <summary>
        ///  Get Predefine Role
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<MstMenuModel> GetPredefineRole()
        {
            return ObjmenuBusinessAccessLayer.GetPredefineRole();
        }

        /// <summary>
        ///  Get Predefine Role
        /// </summary>
        /// <returns></returns>
        //[HttpGet("[action]")]
        //public IEnumerable<MstMenuModel> FillUpdateMenu(string MenuId)
        //{
        //    return ObjmenuBusinessAccessLayer.FillUpdateMenu(MenuId);
        //}

        [HttpGet("[action]")]

        public async Task<IActionResult> FillUpdateMenu(string MenuId)
        {
            var myTask = Task.Run(() => ObjmenuBusinessAccessLayer.FillUpdateMenu(MenuId));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }


        /// <summary>
        /// Active Deactive Menu
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string ActiveDeactiveMenu([FromBody]MstMenuModel mstMenu)
        {
            return ObjmenuBusinessAccessLayer.ActiveDeactiveMenu(mstMenu);

        }
        #endregion
    }
}
