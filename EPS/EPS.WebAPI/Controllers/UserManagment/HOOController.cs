﻿using System;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.Repositories.UserManagement;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static EPS.CommonClasses.EPSEnum;

namespace EPS.WebAPI.Controllers.UserManagment
{
    /// <summary>
    /// HOO Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class HOOController : Controller
    {
        private IHOORoleRepository _repository;
        public HOOController(IHOORoleRepository repository)
        {
            _repository = repository;
        }

        string status = string.Empty;
        string RoleName = string.Empty;
        string EmployeeEmailID = string.Empty;
        string Subject = string.Empty;
        string UserID = string.Empty;

        /// <summary>
        /// Hoo Checker EmpList
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> HooCheckerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => _repository.HooCheckerEmpList( DDOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// hoo Maker EmpList
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> hooMakerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => _repository.hooMakerEmpList( DDOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        
        /// <summary>
        /// Assigned HOO Checker
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedHOOChecker(string empCd, int DDOID, string active)
        {
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string URL = GeneratedToken.Url;
            string port = "activate";
            var myTask = Task.Run(() => _repository.AssignedHOOChecker(empCd, DDOID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");
            UserID = "User ID:    " + NewUserID[0];
            var varifyUrl = URL + port + "?id=" + NewUserID[1];
            EmployeeEmailID = "tabarakzee@gmail.com";
            RoleName = ArrResult[0].ToUpper().Trim();
            if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.ASN))
            {
                status = EPSResource.ASN;
                Subject = "New Role Assigned";
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + UserID + "<br/>" + "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                              " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.USN))
            {
                status = EPSResource.USN;
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            return Ok(status);
        }


        /// <summary>
        /// Assigned HOO Maker
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="DDOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedHOOMaker(string empCd, int DDOID, string active)
        {
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string URL = GeneratedToken.Url;
            string port = "activate";
            EmployeeEmailID = "tabarakzee@gmail.com";
            var myTask = Task.Run(() => _repository.AssignedHOOMaker(empCd, DDOID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");
            RoleName = ArrResult[0].ToUpper().Trim();
            UserID = "User ID:    " + NewUserID[0];
            var varifyUrl = URL + port + "?id=" + NewUserID[1];
            if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.ASN))
            {
                status = "Assigned Successfully";
                Subject = "New Role Assigned";
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + UserID + "<br/>" + "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                              " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.USN))
            {
                status = "Unassigned Successfully";
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            return Ok(status);
        }

        /// <summary>
        /// Self Assigned HOO Checker
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="username"></param>
        /// <param name="EmpPermDDOId"></param>
        /// <param name="ddoid"></param>
        /// <param name="IsAssign"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> SelfAssignedHOOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            var mytask = Task.Run(() => _repository.SelfAssignedHOOChecker(UserID, username, EmpPermDDOId, ddoid, IsAssign));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Self Assign HOO Checker Role
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> SelfAssignHOOCheckerRole(string UserID)
        {
            var mytask = Task.Run(() => _repository.SelfAssignHOOCheckerRole(UserID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

    }
}
