﻿using System;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.Repositories.UserManagement;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static EPS.CommonClasses.EPSEnum;

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// ddo role Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DDOroleController : Controller
    {
        #region ddo role Controller
        private IDDOAdminRepository _repository;
       
        public DDOroleController(IDDOAdminRepository repository)
        {
            _repository = repository;
        }
        
        /// <summary>
        /// Get All DDO
        /// </summary>
        /// <param name="PAOID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDDO(string PAOID, string query)
        {
            var myTask = Task.Run(() => _repository.GetAllDDO(PAOID, query));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Assigned DDO
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedDDO(string DDOID)
        {
            var myTask = Task.Run(() => _repository.AssignedDDO(DDOID));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
        /// <summary>
        /// Assigned Emp DDO
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedEmpDDO(string DDOID)
        {
            var myTask = Task.Run(() => _repository.AssignedEmpDDO(DDOID));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Employee List
        /// </summary>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EmployeeList(string DDOID)
        {
            var myTask = Task.Run(() => _repository.EmployeeList(DDOID));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="DDOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> Assigned(string empCd, string DDOID, string active)
        {
            string status = string.Empty;
            string RoleName = string.Empty;
            string EmployeeEmailID = string.Empty;
            string Subject = string.Empty;
            string UserID = string.Empty;
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string URL = GeneratedToken.Url;
            string port = "activate";

            var myTask = Task.Run(() => _repository.Assigned(empCd, DDOID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");
            EmployeeEmailID = "tabarakzee@gmail.com";
            RoleName = ArrResult[0].ToUpper().Trim();

            string RoleAssignStatus = string.Empty;
            string RoleAssignStatusEnum = string.Empty;
            RoleAssignStatus = RoleStatus[0].ToUpper().Trim();
            RoleAssignStatusEnum =Convert.ToString(RoleSatusEnum.ASN);

            if (RoleStatus[0].ToUpper().Trim() == "ASN")
            {
                status = EPSResource.ASN;
                Subject = "New User created";
                UserID = "User ID:    " + NewUserID[0];
                var varifyUrl = URL + port + "?id=" + NewUserID[1];
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + UserID + "<br/>" + "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                              " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if(RoleStatus[0].ToUpper().Trim() == "USN")
            {
                status = EPSResource.USN;
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            return Ok(status); 
        }
        #endregion
    }
}
