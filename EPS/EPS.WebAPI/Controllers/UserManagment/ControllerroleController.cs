﻿using System.Threading.Tasks;
using EPS.CommonClasses;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Mvc;
using System;
using static EPS.CommonClasses.EPSEnum;
using EPS.Repositories.UserManagement;

namespace EPS.WebAPI.Controllers
{
    #region Controller Role Assign
    /// <summary>
    /// For Controller Assign
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ControllerroleController : Controller
    {
      
        private IControllerRoleRepository _repository;

        public ControllerroleController(IControllerRoleRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Assigned Employee
        /// </summary>
        /// <param name="controllerID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedEmp(string controllerID)
        {
            var myTask = Task.Run(() => _repository.AssignedEmp(controllerID));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// For Get All Controllers
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllControllers(string controllerID, string query)
        {
            var myTask = Task.Run(() => _repository.GetAllControllers(controllerID, query));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Get All paos
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllpaos(string controllerID, string query)
        {
            var myTask = Task.Run(() => _repository.GetAllpaos(controllerID, query));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// on Controller role Select changed
        /// </summary>
        /// <param name="msControllerID"></param>
        /// <param name="PAOID"></param>
        /// <param name="DDOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> onControllerroleSelectchanged(string msControllerID, string PAOID, string DDOID)
        {
            var myTask = Task.Run(() => _repository.ControllerroleSelectchanged(msControllerID, PAOID, DDOID));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Paos Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msControllerID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> PaosAssigned(string empCd, int msControllerID, string active)
        {
            string status = string.Empty;
            string RoleName = string.Empty;
            string EmployeeEmailID = string.Empty;
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string Subject = string.Empty;
            string UserID = string.Empty;

            //string URL = _CommonKeys.Value.Client_URL; // "http://10.199.72.57/";
            // string URL = "https://localhost:44371/";  // for local use
            string URL = GeneratedToken.Url;
            string port = "activate";
           
            var myTask = Task.Run(() => _repository.ControllerAssigned(empCd, msControllerID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");

            UserID = "User ID:    " + NewUserID[0];
            var varifyUrl = URL + port + "?id=" + NewUserID[1];
            EmployeeEmailID = "tabarakzee@gmail.com";
            RoleName = ArrResult[0].ToUpper().Trim();
            if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.ASN))
            {
                status = EPSResource.ASN;
                Subject = "New Role Assigned";
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + UserID + "<br/>"+ "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                              " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if(RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.USN)) {
                status = EPSResource.USN;
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";

                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            return Ok(status);
           
        }


        /// <summary>
        /// Get All User Roles
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllUserRoles(string username)
        {
            var myTask = Task.Run(() => _repository.GetAllUserRoles(username));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        #endregion
    }
}
