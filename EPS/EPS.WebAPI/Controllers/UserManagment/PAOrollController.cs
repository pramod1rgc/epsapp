﻿using System;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.Repositories.UserManagement;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static EPS.CommonClasses.EPSEnum;

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// PAO role Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PAOrollController : Controller
    {
        #region PAO Assign
        
        private IPAORoleRepository _repository;

        public PAOrollController(IPAORoleRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Get All paos
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllpaos(string controllerID, string query)
        {
            var mytask = Task.Run(() => _repository.GetAllpaos(controllerID, query));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// AssignedPAOEmp
        /// </summary>
        /// <param name="PAOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedPAOEmp(string PAOID)
        {
            var mytask = Task.Run(() => _repository.AssignedPAOEmp(PAOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Assigned Emp List
        /// </summary>
        /// <param name="PAOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AssignedEmpList(string PAOID)
        {
            var mytask = Task.Run(() => _repository.AssignedEmpList(PAOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        
        /// <summary>
        /// Get All DDOs Under Selected PAOs
        /// </summary>
        /// <param name="msPAOID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDDOsUnderSelectedPAOs(string msPAOID)
        {
            var mytask = Task.Run(() => _repository.GetAllDDOsUnderSelectedPAOs(msPAOID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msPAOID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> Assigned(string empCd, int msPAOID, string active)
        {
            string status = string.Empty;
            string RoleName = string.Empty;
            string EmployeeEmailID = string.Empty;
            string Password = CommonClasses.CommonFunctions.RandomPassword();
            string Subject = string.Empty;
            string UserID = string.Empty;
            string URL = GeneratedToken.Url;
            string port = "activate"; 
            var myTask = Task.Run(() => _repository.Assigned(empCd, msPAOID, active, Password));
            var result = await myTask;
            string[] ArrResult = result.Split("$");
            string[] NewUserID = ArrResult[1].Split("&");
            string[] Office = ArrResult[2].Split("$");
            string[] RoleStatus = ArrResult[3].Split("$");
            EmployeeEmailID = "tabarakzee@gmail.com";

            if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.ASN))
            {
                status = EPSResource.ASN;
                RoleName = ArrResult[0].ToUpper().Trim();
                Subject = "New User created";
                UserID = "User ID:    " + NewUserID[0];
                var varifyUrl = URL + port + "?id=" + NewUserID[1];
                string Body = "<br/>You have been assigned as a <b> " + RoleName + "</b>  for office <b>" + Office[0] + ".</b><br/><br/>Your login credentials are given below. <br/>" + UserID + "<br/>" + "Password:     " + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                          " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "EPS Admin </b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
            else if (RoleStatus[0].ToUpper().Trim() == Convert.ToString(RoleSatusEnum.USN))
            {
                status = EPSResource.USN;
                Subject = "Role deactivated";
                string Body = "<br/>You have been unassigned from the role of  <b> " + RoleName + "</b> for office <b>" + Office[0] + ".</b>" + "<br/><br/>From <b>" + "EPS Admin</b>";
                CommonClasses.CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            }
                return Ok(status);
        }

        #endregion
    }
}
