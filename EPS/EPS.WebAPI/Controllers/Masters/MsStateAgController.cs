﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// This controller contains all methods for StateAg master
    /// </summary>

    [EPSExceptionFilters]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MsStateAgController : ControllerBase
    {
        MsStateAgBAL _BAL = new MsStateAgBAL();
        /// <summary>
        /// Used to get State Ag  list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<MsStateAgBM>>GetMsStateAglist()
        {
                return await Task.Run(() => _BAL.GetMsStateAg()).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to get State list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task< IEnumerable<StateBM>> GetMsStateList()
        {
            return await Task.Run(() => _BAL.GetMsState()).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to get PfType list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
      public async Task<IEnumerable<PfTypeBM>>GetMsPfTypeList()
        {
            return await Task.Run(() => _BAL.GetMsPfType()).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to update MsStateAg details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public  async Task<string>upadateMsStateAg([FromBody] MsStateAgBM BM)
        {
            return await Task.Run(() => _BAL.UpdateMsStateAg(BM)).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to insert MsStateAg details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> InsertMsStateAg([FromBody] MsStateAgBM BM)
        {
            return await Task.Run(() => _BAL.InsertMsStateAg(BM)).ConfigureAwait(true);
        }
    }
}
