﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// This controller contains all methods for PSU master
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PsuMasterController : Controller
    {
        // private IHttpContextAccessor _accessor;
        private IPsuMaster _repository;

        /// <summary>
        /// PsuMaster constructor
        /// </summary>
        /// <param name="repository"></param>
        public PsuMasterController(IPsuMaster repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Get Distt List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetStateList()
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    return _repository.GetStateList();
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Distt List
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDisttList(int  stateId)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    return _repository.GetDisttList(stateId);
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objPSU"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavedPSUDetails([FromBody]PsuMasterModel.PsuMaster objPSU)
        {
            try
            {
                  string successMsg = await Task.Run(() =>
                {
                    return _repository.SavedPSUDetails(objPSU);
                });

                return successMsg;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>     
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPsuMasterDetails()
        {
            try
            {

                var res = await Task.Run(() =>
                {
                    return _repository.GetPsuMasterDetails();
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Delete suspension record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public JsonResult DeletePsuMasterDetails(int id)
        {
            try
            {
             
                return Json(_repository.DeletePsuMasterDetails(id));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
