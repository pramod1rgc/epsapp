﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class FdGpfMasterController : Controller
    {
        public FdGpfMaster_BAL objGpfMasterBAL = new FdGpfMaster_BAL();

        private IHttpContextAccessor _accessor;
        private IFdGpfMasterRepository _repository;

        /// <summary>
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public FdGpfMasterController(IHttpContextAccessor accessor, IFdGpfMasterRepository repository)
        {
            this._repository = repository;
            this._accessor = accessor;
        }

        #region Family Definition GPF Master Function/Method
        /// <summary>
        /// Insert and Update Data
        /// </summary>
        /// <param name="fdmObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> CreatefdGpfMaster([FromBody]FdMaster_Model fdmObj)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            fdmObj.ClientIP = ip.ToString();
            var result = await Task.Run(() => _repository.CreatefdGpfMaster(fdmObj)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }

        /// <summary>
        /// Get deatials for shown data in gried
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet("[action]")]
        public async Task<IActionResult> GetFdGpfMasterDetails()
        {
            var mytask = Task.Run(() => _repository.GetFdGpfMasterDetails());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get details for update
        /// </summary>
        /// <param name="fdMasterID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditFdGpfMasterDetails(int fdMasterID)
        {
            var mytask = Task.Run(() => _repository.EditFdGpfMasterDetails(fdMasterID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Delete Family definition data
        /// </summary>
        /// <param name="fdMasterID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteFdGpfMaster(int fdMasterID)
        {
            var result = await Task.Run(() => _repository.DeleteFdGpfMaster(fdMasterID)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }
        #endregion

    }
}
