﻿using EPS.Repositories.Masters;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class PraoMasterController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private IpraoMasterRepository _repository;

     /// <summary>
     /// 
     /// </summary>
     /// <param name="accessor"></param>
     /// <param name="repository"></param>
  
        public PraoMasterController(IHttpContextAccessor accessor, IpraoMasterRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        
        [HttpGet("[action]")]
        public async Task<IActionResult> GetControllerCode()
        {
            var mytask = Task.Run(() => _repository.GetAllController());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CtrlrCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindCtrlName(string CtrlrCode)
        {
            var mytask = Task.Run(() => _repository.BindCtrlName(CtrlrCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
/// <summary>
/// 
/// </summary>
/// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> BindState()
        {
            var mytask = Task.Run(() => _repository.BindState());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
/// <summary>
/// 
/// </summary>
/// <param name="stateId"></param>
/// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindDistrict(int stateId)
        {
            var mytask = Task.Run(() => _repository.BindDistrict(stateId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="ModelData"></param>
       /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<IActionResult> SaveOrUpdate(PraoMasterModel ModelData)
        {
            ModelData.IpAddress = CommonFunctions.GetClientIP(_accessor);
            var mytask = Task.Run(() => _repository.SaveOrUpdate(ModelData));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindPraoMasterDetails()
        {
            var mytask = Task.Run(() => _repository.BindPraoMasterDetails());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="CtrlCode"></param>
       /// <param name="controllerID"></param>
       /// <param name="Mode"></param>
       /// <returns></returns>
       //
        [HttpGet("[action]")]
        public async Task<IActionResult> ViewEditOrDeletePrao(string CtrlCode,string controllerID,int Mode)
        {
            var mytask = Task.Run(() => _repository.ViewEditOrDeletePrao(CtrlCode, controllerID, Mode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}