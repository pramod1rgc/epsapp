﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using EPS.Repositories.Masters;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// GPF Withdraw Rules
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class GpfWithdrawRulesController: Controller
    {
       private GpfWithdrawRulesBA ObjGpfWithdrawRulesBA = new GpfWithdrawRulesBA();
        private IGpfWithdrawRuleRepository _repository;

        private IHttpContextAccessor _accessor;
        /// <summary>
        /// Get GPF Withdraw Rules
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public GpfWithdrawRulesController(IHttpContextAccessor accessor, IGpfWithdrawRuleRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }

        /// <summary>
        /// Insert and Update GpfWithdrawRules
        /// </summary>
        /// <param name="ObjGpfWithdrawRules"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
         public async Task<IActionResult> InsertUpdateGpfWithdrawRules([FromBody]GpfWithdrawRules ObjGpfWithdrawRules)
        {
            var myTask = Task.Run(() => _repository.InsertUpdateEmpDetails(ObjGpfWithdrawRules));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else { return Ok(result); }
               


        }
        /// <summary>
        /// Get Gpf Withdraw Rules By Id
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfWithdrawRulesById(int MsGpfWithdrawRulesRefID)
        {
            var mytask = Task.Run(() => _repository.GetGpfWithdrawRulesById(MsGpfWithdrawRulesRefID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get All Gpf Withdraw Rules
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfWithdrawRules()
        {
            var mytask = Task.Run(() => _repository.GetGpfWithdrawRules());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Delete Gpf Withdraw Rules
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteGpfWithdrawRules(int MsGpfWithdrawRulesRefID)
        {
            var myTask = Task.Run(() => _repository.DeleteGpfWithdrawRules(MsGpfWithdrawRulesRefID));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else { 
         return Ok(result);
            }
              

        }
    }
}
