﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class PayscaleController : Controller
    {
        PayScale_BAL objPayScaleBAL = new PayScale_BAL();
        private IHttpContextAccessor _accessor;
        /// <summary>
        /// PayscaleController
        /// </summary>
        /// <param name="accessor"></param>
        public PayscaleController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        /// <summary>
        /// Bind Commission Code
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindCommissionCode()
        {
            var mytask = Task.Run(() => objPayScaleBAL.BindCommissionCode());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Get PayScale Code
        /// </summary>
        /// <param name="cddirCodeValue"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayScaleCode(string cddirCodeValue)
        {
            var mytask = Task.Run(() => objPayScaleBAL.PayScaleCode(cddirCodeValue));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Get PayScale Details
        /// </summary>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayScaleDetails()
        {
            var mytask = Task.Run(() => objPayScaleBAL.GetPayScaleDetails());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Get PayScale Details By Commission Code
        /// </summary>
        /// <param name="commCDID"></param>
        /// <returns></returns>


        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayScaleDetailsBYComCode(string commCDID)
        {
            var mytask = Task.Run(() => objPayScaleBAL.GetPayScaleDetailsBYComCode(commCDID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        /// Bind PayScale Format
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<PayScaleModel> BindPayScaleFormat()
        {
            return objPayScaleBAL.BindPayScaleFormat();
        }
        /// <summary>
        /// Bind PayScale Group
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindPayScaleGroup()
        {
            var mytask = Task.Run(() => objPayScaleBAL.BindPayScaleGroup());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Insert And Update Payscale Record
        /// </summary>
        /// <param name="_pScaleObj"></param>
        /// <returns></returns>
        /// 
        [HttpPost("[action]")]
        public async Task<IActionResult> CreatePayScale([FromBody]PayScaleModel _pScaleObj)
        {
            _pScaleObj.ipAddress = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            var myTask = Task.Run(() => objPayScaleBAL.InsertUpdatePayScale(_pScaleObj));
            var result = await myTask;
            if (string.IsNullOrEmpty(result))
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Delete PayScale
        /// </summary>
        /// <param name="msPayScaleID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Delete(int msPayScaleID)
        {
            var myTask = Task.Run(() => objPayScaleBAL.Delete(msPayScaleID));
            var result = await myTask;
            if (string.IsNullOrEmpty(result))
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);


        }

        /// <summary>
        /// Edit PayScale
        /// </summary>
        /// <param name="msPayScaleID"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> EditPayScaleDetails(string msPayScaleID)
        {
            var myTask = Task.Run(() => objPayScaleBAL.EditPayScaleDetails(msPayScaleID));
            var result = await myTask;
            if (result == null)
            {
                return NotFound();
            }
            else
                return Ok(result);
        }
    }
}
