﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// Controller for Hra Master 
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    
    [ApiController]

    public class HraMasterController: Controller
    {
       public HRA_BAL objHraBAL = new HRA_BAL();
        private IHraMasterRepository _repository;
        private IHttpContextAccessor _accessor;

        /// <summary>
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public HraMasterController(IHttpContextAccessor accessor, IHraMasterRepository repository)
        {
            this._repository = repository;
            this._accessor = accessor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindPayScaleCode()
        {
            var mytask = Task.Run(() => _repository.BindPayScaleCode());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Pay Level Data
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindPayLevel()
        {
            var mytask = Task.Run(() => _repository.BindPayLevel());
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        
        /// <summary>
        /// Get pay Commission by employee type
        /// </summary>
        /// <param name="employeeType"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayCommissionByEmployeeType(string employeeType)
        {
            var mytask = Task.Run(() => _repository.GetPayCommissionByEmployeeType(employeeType));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }


        /// <summary>
        /// Get city details
        /// </summary>
        /// <param name="StateID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCityDetails(int StateID)
        {
            var mytask = Task.Run(() => _repository.GetCityDetails(StateID));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        /// <summary>
        /// Get City Class On behalf of paycommission
        /// </summary>
        /// <param name="payCommId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCityClass(int payCommId)
        {
            var mytask = Task.Run(() => _repository.GetCityClass(payCommId));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }


        /// <summary>
        /// Create Hra Master
        /// </summary>
        /// <param name="hraObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> CreateHraMaster([FromBody]HRAModel hraObj)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            hraObj.ClientIP = ip.ToString();
            var result = await Task.Run(() => _repository.CreateHraMaster(hraObj));
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }

        /// <summary>
        /// Get Hra Master Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetHraMasterDetails()
        {
            var mytask = Task.Run(() => _repository.GetHraMasterDetails());
            var result = await mytask;
            if (result == null)
               return NotFound();
            else
              return Ok(result);
        }
        
        /// <summary>
        /// Edit Hra Master Details by id
        /// </summary>
        /// <param name="hraMasterID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditHraMasterDetails(int hraMasterID)
        {
            var mytask = Task.Run(() => _repository.EditHraMasterDetails(hraMasterID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        
        /// <summary>
        /// Delete Hra Master data
        /// </summary>
        /// <param name="hraMasterID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteHraMaster(int hraMasterID)
        {
            var result = await Task.Run(() => _repository.DeleteHraMaster(hraMasterID)).ConfigureAwait(true);
            if (result == "0" || result==null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
    }
}
