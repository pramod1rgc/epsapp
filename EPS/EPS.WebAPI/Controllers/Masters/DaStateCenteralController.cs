﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]   
    
    public class DaStateCenteralController : Controller
    {
        /// GET: api/<controller>
        private IHttpContextAccessor _accessor;
        DAStateCentralBAL objbal = new DAStateCentralBAL();
        /// <summary>
        /// DaStateCenteral
        /// </summary>
        /// <param name="accessor"></param>
        public DaStateCenteralController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        /// <summary>
        /// InsertandUpdate
        /// </summary>
        /// <param name="objdamaster"></param>
        /// <returns></returns>       
        [HttpPost("[action]")]
        public string InsertandUpdate([FromBody]DAStateCentralBM objdamaster)
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            objdamaster.IpAddress = ip.ToString();
            return objbal.InsertandUpdate(objdamaster);
        }

        /// <summary>
        /// Get current DaRate And Current Wef(With effect from)
        /// </summary>
        /// <param name="datype"></param>
        ///<param name="subType"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public DAStateCentralBM GetDACurrent_Rate_Wef(string datype,string subType)
        {
            return objbal.GetDACurrentRateWef(datype, subType);
        }

        /// <summary>
        /// Get current DaRate And Current Wef(With effect from)
        /// </summary>
        /// <param name="datype"></param>
        /// <param name="stateId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public DAStateCentralBM GetDACurrent_Rate_Wef_state(string datype,string stateId)
        {
            return objbal.GetDACurrentRateWef(datype, stateId);
        }
        
        /// <summary>
        /// Get DA Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<DAStateCentralBM> GetDADetails()
        {
            return objbal.GetDADetails();
        }

        /// <summary>
        ///soft Delete
        /// </summary>
        /// <param name="msDaMasterID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string Delete(int msDaMasterID)
        {
            return objbal.Delete(msDaMasterID);
        }
    }
}
