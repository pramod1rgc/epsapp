﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// DuesDefinationmater
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DuesDefinationandDuesMasterController : Controller
    {
        private IHttpContextAccessor _accessor;
        private IDuesDefinationandDuesMaster _repository;

        /// <summary>
        /// Dues Defination
        /// </summary>
        public DuesDefinationandDuesMasterController(IHttpContextAccessor accessor, IDuesDefinationandDuesMaster repository)
        {
            _repository = repository;
            _accessor = accessor;
        }

        /// <summary>
        ///  Save and Update Dues Defination master  Details
        /// </summary>
        /// <param name="ObjDuesDefinationandDuesMasterModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateDuesDefinationandDuesMasterDetails([FromBody]DuesDefinationandDuesMasterModel ObjDuesDefinationandDuesMasterModel)
        {
            var myTask = Task.Run(() => _repository.InsertUpdateDuesDefinationMasterDetails(ObjDuesDefinationandDuesMasterModel));
            var result = await myTask.ConfigureAwait(true);
            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);
              
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Maker List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDuesdefinationMasterList()
        {
            var mytask = Task.Run(() => _repository.GetDuesDefinationandDuesMasterList());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Delete Dues Defination Details
        /// </summary>
        /// <param name="DuesCd"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteDuesDefinationmasterDetails(string DuesCd)
        {
            var myTask = Task.Run(() => _repository.DeleteDuesDefinationmasterDetails(DuesCd));
            var result = await myTask.ConfigureAwait(true);
            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);

            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// get Dure Master  Details by Dues code
        /// </summary>
        /// <param name="DuesCD"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpPersonalDetailsByID(string DuesCD)
        {
            var mytask = Task.Run(() => _repository.GetDuesDefinationandDuesMasterByID(DuesCD));
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get AutoGenrated Dues Code
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAutoGenratedDuesCode()
        {
            var mytask = Task.Run(() => _repository.GetAutoGenratedDuesCode());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        //================================DuesRateMaster==========================
        /// <summary>
        /// Get Orgnization Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOrgnazationTypeForDues()
        {
            var mytask = Task.Run(() => _repository.GetOrgnazationTypeForDues());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Dues Code and Defination
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDuesCodeDuesDefination()
        {
            var mytask = Task.Run(() => _repository.GetDuesCodeDuesDefination());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get City Class for Dues Rate
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCityClassForDuesRate(string payCommId)
        {
            var mytask = Task.Run(() => _repository.GetCityClassForDuesRate(payCommId));
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// get State For Dues Rate Master
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetStateForDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetStateForDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Pat Comm Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayCommForDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetPayCommForDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get SlabType by pay Comm Id
        /// </summary>
        /// <param name="payCommId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSlabtypeByPayCommId(string payCommId)
        {
            var mytask = Task.Run(() => _repository.GetSlabtypeByPayCommId(payCommId));
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// SaveUpdateDuesRateDetails
        /// </summary>
        /// <param name="objDuesRateandDuesMasterModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateDuesRateDetails([FromBody]DuesRateandDuesMasterModel objDuesRateandDuesMasterModel)
        {           
            var mytask = Task.Run(() => _repository.InsertUpdateDuesRateDetails(objDuesRateandDuesMasterModel));
            var result = await mytask.ConfigureAwait(true);
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Dues Rate and Defination
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDuesCodeDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetDuesCodeDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Delete Dues Rate Details
        /// </summary>
        /// <param name="msduesratedetailsid"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteDuesRateDetails(string msduesratedetailsid)
        {
            var myTask = Task.Run(() => _repository.DeleteDuesRateDetails(msduesratedetailsid));
            var result = await myTask.ConfigureAwait(true);
            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);

            }
            else
            {
                return Ok(result);
            }
        }
    }
}
