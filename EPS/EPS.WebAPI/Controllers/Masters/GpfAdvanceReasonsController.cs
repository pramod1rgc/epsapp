﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
   // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class GpfAdvanceReasonsController : Controller
    {
        private IHttpContextAccessor accessor;
       
        private GpfAdvanceReasonsBAL objbal = new GpfAdvanceReasonsBAL();
        /// <summary>
        /// DaStateCenteral
        /// </summary>
        /// <param name="accessor"></param>
        public GpfAdvanceReasonsController(IHttpContextAccessor _accessor)
        {
            accessor = _accessor;
        }
        /// <summary>
        /// Bind Main Reason
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<GpfAdvanceReasonsBM> BindMainReason()
        {
            return objbal.BindMainReason();
        }
        /// <summary>
        /// GpfAdvancereasonsInsertandUpdate
        /// </summary>
        /// <param name="objgpfmaster"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> GpfAdvancereasonsInsertandUpdate([FromBody]GpfAdvanceReasonsBM objgpfmaster)
        {
            objgpfmaster.ipAddress = accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            var myTask = Task.Run(() => objbal.InsertandUpdate(objgpfmaster));
            var result = await myTask;
            if (result == "")
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);


        }
        /// <summary>
        /// Get GPF Advance Reasons master  Details
        /// </summary>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> GetGPFAdvanceReasonsDetails(string AdvWithdraw)
        {
            var mytask = Task.Run(() => objbal.GetGPFAdvanceReasonsDetails(AdvWithdraw));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        ///soft Delete
        /// </summary>
        /// <param name="pfRefNo"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public string Delete1(int pfRefNo)
        {
            return objbal.Delete(pfRefNo);
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> Delete(int pfRefNo)
        {
            Task<string> myTask = Task.Run(() => objbal.Delete(pfRefNo));
            var result = await myTask;
            if (result == "")
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
    }
}

