﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// City Master Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class CityMasterController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        private ICityMasterRepository _repository;

        /// <summary>
        /// City master constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public CityMasterController(IHttpContextAccessor accessor, ICityMasterRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }

        /// <summary>
        /// Get City master List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCitymasterList()
        {
            //var mytask = Task.Run(() => citymasterBAL.GetCitymasterList());
            var mytask = Task.Run(() => _repository.GetAllRecords());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Save City Class  details in database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<IActionResult> SaveCityClassmasterDetails([FromBody]CitymasterModel obj)
        {
            obj.UserIp = clientIP;
            var result = await Task.Run(() => _repository.SaveRecord(obj)).ConfigureAwait(true);

            //if (result == 0)
            //{
            //    return NotFound();
            //}

            string response = CommonFunctions.GetResponseMessage(result);

            return Ok(response);
        }

        /// <summary>
        /// Delete Dues Definition Details
        /// </summary>
        /// <param name="duesCd"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteCityClass(string duesCd)
        {
            int duesId = Convert.ToInt32(duesCd);
            var result = await Task.Run(() => _repository.DeleteRecord(duesId)).ConfigureAwait(true);

            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        /// Check Record Exits Or Not Definition Details
        /// </summary>
        /// <param name="payCommid"></param>
        /// <param name="cityClass"></param>
        /// <returns></returns>
        //  [HttpPost("[action]")]
        //[ValidateModel]
        //public async Task<IActionResult> CheckRecordexitsOrNotinCitymaster([FromBody]CitymasterModel obj)
        //{
        //    //var mytask = Task.Run(() => citymasterBAL.GetCitymasterList());
        //    var mytask = Task.Run(() => _repository.CheckRecordAlredyExitsinCityMaster(obj));
        //    var result = await mytask;
        //    return Ok(result);
        //}
    }
}
