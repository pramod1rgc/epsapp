﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TptaMasterController: Controller
    {
       public TPTA_BAL objTptaBAL = new TPTA_BAL();
        private ITptaMasterRepository _repository;
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        /// <summary>
        /// TPTA Master Curd Operation
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="accessor"></param>
        public TptaMasterController(IHttpContextAccessor accessor, ITptaMasterRepository repository)
        {
            this._repository = repository;
            this._accessor = accessor;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }
        
        
        /// <summary>
        /// Insert Update TPTA Master Record
        /// </summary>
        /// <param name="hraObj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        //[ValidateModel]
        public async Task<IActionResult> CreateTptaMaster([FromBody]HRAModel hraObj)
        {
            hraObj.ClientIP = clientIP;
            var result = await Task.Run(() => _repository.CreateTptaMaster(hraObj));
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);

        }

        /// <summary>
        /// Get TPTA Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetTptaMasterDetails()
        {
            var mytask = Task.Run(() => _repository.GetTptaMasterDetails());
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        
        /// <summary>
        /// Get TPTA Detail for Edit
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> EditTptaMasterDetails(int MasterID)
        {
            var mytask = Task.Run(() => _repository.EditTptaMasterDetails(MasterID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        
        /// <summary>
        /// Delete TPTA Master Record
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteTptaMaster(int MasterID)
        {
            var result = await Task.Run(() => _repository.DeleteTptaMaster(MasterID)).ConfigureAwait(true);
            if (result == "0" || result == null)
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            else
                return Ok(result);
        }
       
    }
}
