﻿using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace EPS.WebAPI.Controllers.Masters
{

    /// <summary>
    /// This controller contains all methods for Leave Type master
    /// </summary>

    [EPSExceptionFilters]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LeavetypeController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private ILeavetype _repository;

        /// <summary>
        /// DDO master constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public LeavetypeController(IHttpContextAccessor accessor, ILeavetype repository)
        {
            _repository = repository;
            _accessor = accessor;
        }
        /// <summary>
        /// Used to get leave type list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
      public async Task<IEnumerable<LeavetypeBM>> GetLeavetypelist()
        {
           
             //return await Task.Run(() => _BAL.GetLeavetypes()).ConfigureAwait(true);
             return await Task.Run(() => _repository.GetLeavetypes()).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to get leave type max id
        /// </summary>
        /// <returns></returns> 
        [HttpGet("[action]")]
     public async Task<int> GetLeavetypeMaxid()
        {
            return await Task.Run(() => _repository.GetLeavetypeMaxid()).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to update leave type details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        
       
           
        [HttpPost("[action]")]
        public async Task<string> upadateLeavetype([FromBody] LeavetypeBM BM)
        {
        
            return await Task.Run(() => _repository.UpdateMstLeavetype(BM)).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to insert leave type details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
       
     
    [HttpPost("[action]")]
      public async Task<string> InsertLeavetype([FromBody] LeavetypeBM BM)
        {
            return await Task.Run(() => _repository.InsertMstLeavetype(BM)).ConfigureAwait(true);
        }
        /// <summary>
        /// Used to delete leave type details
        /// </summary>
        /// <param name="msLeavetypeId"></param>
        /// <returns></returns>
        
        [HttpPost("[action]")]
        public async Task<string> DeleteLeavetype(int msLeavetypeId)
        {
            return await Task.Run(() => _repository.DeleteMstLeavetype(msLeavetypeId)).ConfigureAwait(true);
        }
    }
}