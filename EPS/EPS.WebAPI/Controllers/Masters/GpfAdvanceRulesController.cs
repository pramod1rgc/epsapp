﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GpfAdvanceRulesController : ControllerBase
    {
        private IHttpContextAccessor objaccessor;
        private GpfAdvanceRulesBAL objgpf = new GpfAdvanceRulesBAL();
        /// <summary>
        /// GpfAdvanceRulesController
        /// </summary>
        /// <param name="accessor"></param>
        public GpfAdvanceRulesController(IHttpContextAccessor accessor)
        {
            objaccessor = accessor;
        }
        /// <summary>
        /// Insert and Update GpfAdvanceRules
        /// </summary>
        /// <param name="objGpfAdvanceRules"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateGpfAdvanceRules([FromBody]GpfAdvanceRulesBM objGpfAdvanceRules)
        {
            var result = 0;
            if (objGpfAdvanceRules != null)
            {
                objGpfAdvanceRules.ipAddress = objaccessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var myTask = Task.Run(() => objgpf.InsertUpdateAdvanceRulesDetails(objGpfAdvanceRules));
                result = await myTask;
            }
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else { return Ok(result); }

        }
        /// <summary>
        /// Get Gpf Advance Rules By Id
        /// </summary>
        /// <param name="msGpfAdvanceRulesRefID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfAdvanceRulesById(int msGpfAdvanceRulesRefID)
        {
            var mytask = Task.Run(() => objgpf.GetGpfAdvanceRulesById(msGpfAdvanceRulesRefID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Get All Gpf Advance Rules
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfAdvanceRules()
        {
            var mytask = Task.Run(() => objgpf.GetGpfAdvanceRules());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// DeleteGpfAdvanceRules
        /// </summary>
        /// <param name="msGpfAdvanceRulesRefID"></param>
        /// <param name="isflag"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteGpfAdvanceRules(int msGpfAdvanceRulesRefID, string isflag)
        {
            var myTask = Task.Run(() => objgpf.DeleteGpfAdvanceRules(msGpfAdvanceRulesRefID, isflag));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            { return Ok(result); }

        }

    }
}