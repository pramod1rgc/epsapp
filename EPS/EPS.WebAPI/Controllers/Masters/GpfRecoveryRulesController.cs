﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GpfRecoveryRulesController : ControllerBase
    {
        private static string clientIP = string.Empty;
        private IGpfRecoveryRulesRepository _repository;
        private IHttpContextAccessor _accessor;
        GpfRecoveryRulesBAL objgpf = new GpfRecoveryRulesBAL();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public GpfRecoveryRulesController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        /// <summary>
        /// Insert and Update GpfAdvanceRules
        /// </summary>
        /// <param name="objgpfRecoveryRules"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateGpfRecoveryRules([FromBody]GpfRecoveryRulesBM objgpfRecoveryRules)
        {
            objgpfRecoveryRules.ipAddress = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            var myTask = Task.Run(() => objgpf.InsertUpdateGpfRecoveryRules(objgpfRecoveryRules));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);


        }
        /// <summary>
        /// Get Gpf Recovery Rules By Id
        /// </summary>
        /// <param name="msGpfRecoveryRulesRefID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfRecoveryRulesById(int msGpfRecoveryRulesRefID)
        {
            var mytask = Task.Run(() => objgpf.GetGpfRecoveryRulesById(msGpfRecoveryRulesRefID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get All Gpf Recovery Rules
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfRecoveryRules()
        {
            var mytask = Task.Run(() => objgpf.GetGpfRecoveryRules());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Delete Gpf Recovery Rules
        /// </summary>
        /// <param name="isflag"></param>
        /// <param name="msGpfRecoveryRulesRefID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteGpfRecoveryRules(int msGpfRecoveryRulesRefID)
        {
            Task<int> myTask = Task.Run(() => objgpf.DeleteGpfRecoveryRules(msGpfRecoveryRulesRefID));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }


    }
}