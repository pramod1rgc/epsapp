﻿using System.Threading.Tasks;
using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeductionMasterController : Controller
    {
        DeductionMasterBAL deductionMasterBAL = new DeductionMasterBAL();
        private IDeductionMasterRepository _repository;

        /// <summary>
        /// Deduction Master Controller
        /// </summary>
        /// <param name="repository"></param>
        public DeductionMasterController(IDeductionMasterRepository repository)
        {
            _repository = repository;
        }
       

        /// <summary>
        /// Category List
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> CategoryList()
        {
            var myTask = Task.Run(() => _repository.CategoryList());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);

        }

        /// <summary>
        /// Deduction Description
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeductionDescription()
        {
            var myTask = Task.Run(() => _repository.DeductionDescription());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);

        }

        /// <summary>
        /// Deduction Description Change
        /// </summary>
        /// <param name="payItemsCD"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeductionDescriptionChange( int payItemsCD)
        {
            var myTask = Task.Run(() => _repository.DeductionDescriptionChange(payItemsCD));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }
        

        /// <summary>
        /// Deduction Definition List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDeductionDefinitionList()
        {
            var myTask = Task.Run(() => _repository.GetDeductionDefinitionList());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
                return Ok(result);
        }

        /// <summary>
        /// Deduction Definition Submit
        /// </summary>
        /// <param name="ObjDuesDefinationandDuesMasterModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeductionDefinitionSubmit([FromBody]DeductionDescriptionBM ObjDuesDefinationandDuesMasterModel)
        {
            var myTask = Task.Run(() => _repository.DeductionDefinitionSubmit(ObjDuesDefinationandDuesMasterModel));
            var result = await myTask.ConfigureAwait(true);
            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);

            }
            else
            {
                return Ok(result);
            }
        }

        //===========================Deduction Rate Master==================================================

        /// <summary>
        /// Get Orgnization Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOrgnazationTypeForDues()
        {
            var mytask = Task.Run(() => _repository.GetOrgnazationTypeForDues());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Dues Code and Defination
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDuesCodeDuesDefination()
        {
            var mytask = Task.Run(() => _repository.GetDuesCodeDuesDefination());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get City Class for Dues Rate
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCityClassForDuesRate(string payCommId)
        {
            var mytask = Task.Run(() => _repository.GetCityClassForDuesRate(payCommId));
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// get State For Dues Rate Master
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetStateForDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetStateForDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Pat Comm Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayCommForDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetPayCommForDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get SlabType by pay Comm Id
        /// </summary>
        /// <param name="payCommId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetSlabtypeByPayCommId(string payCommId)
        {
            var mytask = Task.Run(() => _repository.GetSlabtypeByPayCommId(payCommId));
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// SaveUpdateDuesRateDetails
        /// </summary>
        /// <param name="objDuesRateandDuesMasterModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUpdateDuesRateDetails([FromBody]DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel)
        {
            var mytask = Task.Run(() => _repository.InsertUpdateDuesRateDetails(objDuesRateandDuesMasterModel));
            var result = await mytask.ConfigureAwait(true);
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Dues Rate and Defination
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDuesCodeDuesRate()
        {
            var mytask = Task.Run(() => _repository.GetDuesCodeDuesRate());
            var result = await mytask.ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Delete Dues Rate Details
        /// </summary>
        /// <param name="msduesratedetailsid"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteDuesRateDetails(string msduesratedetailsid)
        {
            var myTask = Task.Run(() => _repository.DeleteDuesRateDetails(msduesratedetailsid));
            var result = await myTask.ConfigureAwait(true);
            if (result == 0)
            {
                return StatusCode(StatusCodes.Status304NotModified);

            }
            else
            {
                return Ok(result);
            }
        }


    }
}
