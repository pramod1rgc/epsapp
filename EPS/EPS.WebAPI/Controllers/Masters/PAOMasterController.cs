﻿using System.Collections.Generic;
using EPS.BusinessModels.Masters;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// This controller contains all methods for PAO master
    /// </summary>
    [EPSExceptionFilters]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PAOMasterController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private IPAOMaster _repository;

        /// <summary>
        /// DDO master constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public PAOMasterController(IHttpContextAccessor accessor, IPAOMaster repository)
        {
            _repository = repository;
            _accessor = accessor;
        }

        /// <summary>
        /// Used to get Pao  list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails()
        {
            return await Task.Run(() => _repository.GetMSPAODetails()).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to get Pao  list by PaoId
        /// </summary>
        /// <param name="mspaoId"></param>
        /// <returns></returns>
        ///  [HttpGet, Route("GetByID/{customerID?}")]
        [HttpGet, Route("GetMSPAODetails/{mspaoId?}")]
        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails(int mspaoId)
        {
            return await Task.Run(() => _repository.GetMSPAODetails(mspaoId)).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to update pao master details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        //[ValidateModel]
        public async Task<string> UpdateMSPAO([FromBody] PAOMasterBM BM)
        {
            return await Task.Run(() => _repository.UpdateMSPAO(BM)).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to insert pao master details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> InsertMSPAO([FromBody] PAOMasterBM BM)
        {
            return await Task.Run(() => _repository.InsertMSPAO(BM)).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to delete pao master details
        /// </summary>
        /// <param name="mspaoId"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> DeleteMSPAO(int mspaoId)
        {
            return await Task.Run(() => _repository.DeleteMSPAO(mspaoId)).ConfigureAwait(true);
        }
    }
}
