﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using EPS.CommonClasses;
namespace EPS.WebAPI.Controllers.Masters
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]
    public class GPFInterestRateController : ControllerBase
    {
        private IHttpContextAccessor objaccessor;
        private GPFInterestRateBAL objbal = new GPFInterestRateBAL();
        /// <summary>
        /// GpfAdvanceRulesController
        /// </summary>
        /// <param name="accessor"></param>
        public GPFInterestRateController(IHttpContextAccessor accessor)
        {
            objaccessor = accessor;
        }
        /// <summary>
        /// InsertandUpdate
        /// </summary>
        /// <param name="objgpfmaster"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertandUpdate([FromBody]GPFInterestRateBM objgpfmaster)
        {
            var result = string.Empty;
            if (objgpfmaster.NewGPFInterestRate!=0)
           {
                objgpfmaster.ipAddress = objaccessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
                var myTask = Task.Run(() => objbal.InsertandUpdate(objgpfmaster));
                result = await myTask;
            }
            if (result == string.Empty)
            {
                return Ok(EPSResource.ErrorMessage); 
            }
            else { return Ok(result); }

        }

        /// <summary>
        /// Get GPF Interest Rate Details
        /// </summary>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetGPFInterestRateDetails()
        {
            var mytask = Task.Run(() => objbal.GetGPFInterestRateDetails());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        ///soft Delete
        /// </summary>
        /// <param name="gpfInterestID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Delete(int gpfInterestID)
        {
            var myTask = Task.Run(() => objbal.Delete(gpfInterestID));
            var result = await myTask;
            if (result == string.Empty)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            { return Ok(result); }

        }
    }
}