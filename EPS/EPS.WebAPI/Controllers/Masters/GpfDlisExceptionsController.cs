﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// Gpf Dlis Exceptions
    /// </summary>
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class GpfDlisExceptionsController : Controller
    {
        private GpfDlisExceptions ObjGpfDlisExceptionsBA = new GpfDlisExceptions();
        private IGpfDlisExceptionsRepository _repository;

        private IHttpContextAccessor _accessor;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public GpfDlisExceptionsController(IHttpContextAccessor accessor, IGpfDlisExceptionsRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjGpfDlisExceptions"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateGpfDlisExceptions([FromBody]GpfDlisExceptions ObjGpfDlisExceptions)
        {
            ObjGpfDlisExceptions.IPAddress = CommonFunctions.GetClientIP(_accessor);
            var myTask = Task.Run(() => _repository.InsertUpdateGpfDlisExceptions(ObjGpfDlisExceptions));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else { return Ok(result); }



        }
        /// <summary>
        /// Get Gpf Dlis Exceptions
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetGpfDlisExceptions()
        {
            var mytask = Task.Run(() => _repository.GetGpfDlisExceptions());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Pf Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPfType()
        {
            var mytask = Task.Run(() => _repository.GetPfType());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Pay Scale
        /// </summary>
        /// <param name="PayCommId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayScale(int PayCommId)
        {
            var mytask = Task.Run(() => _repository.GetPayScale(PayCommId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MsGpfDlisExceptionsRefID"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteGpfDlisExceptions(int MsGpfDlisExceptionsRefID)
        {
            var myTask = Task.Run(() => _repository.DeleteGpfDlisExceptions(MsGpfDlisExceptionsRefID));
            var result = await myTask;
            if (result == 0)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
            {
                return Ok(result);
            }


        }
    }
}