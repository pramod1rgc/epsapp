﻿
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.Repositories.Masters;
using EPS.WebAPI.CustomFilters;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// DDO Master Controller
    /// </summary>
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class DDOMasterController : Controller
    {
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        private IDdoMasterRepository _repository;

        /// <summary>
        /// DDO master constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public DDOMasterController(IHttpContextAccessor accessor, IDdoMasterRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }
        //DdoMasterBAL ObjDdoBAL = new DdoMasterBAL();

        #region Get All Controller 
        /// <summary>
        /// Get All Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllController()
        {
            var mytask = Task.Run(() => _repository.GetAllController());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get PAO by ControllerId
        /// <summary>
        /// Get PAO by ControllerId
        /// </summary>
        /// <param name="ControllerId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPAOByControllerId(string ControllerId)
        {
            var mytask = Task.Run(() => _repository.GetPAOByControllerId(ControllerId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get DDO by PAO
        /// <summary>
        /// Get DDO by PAOId.
        /// </summary>
        /// <param name="PaoID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetODdoByPao(string PaoID)
        {
            var mytask = Task.Run(() => _repository.GetODdoByPao(PaoID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get DDO Category
        /// <summary>
        /// Get DDO category
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDDOcategory()
        {
            var mytask = Task.Run(() => _repository.GetDDOcategory());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get District
        /// <summary>
        /// Get Get District
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetState()
        {
            var mytask = Task.Run(() => _repository.GetState());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get City by districtId
        /// <summary>
        /// Get Get City by districtId
        /// </summary>
        ///  <param name="DistrictId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCity(string StateId)
        {
            var mytask = Task.Run(() => _repository.GetCity(StateId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region Get DDO Master Details
        /// <summary>
        /// Get DDO Master Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDDOMaster()
        {
            var mytask = Task.Run(() => _repository.GetDDOMaster());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Save DDO Master Details
        /// <summary>
        /// Save DDO Master Details
        /// </summary>
        /// <param name="objDdoMasterModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveDDOMaster([FromBody]DdoMasterModel objDdoMasterModel)
        {
            var mytask = Task.Run(() => _repository.SaveDDOMaster(objDdoMasterModel));
            var result = await mytask;

            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #region Deactivate DDO Master Details
        /// <summary>
        /// Deactivate DDO Master Details
        /// </summary>
        /// <param name="objDdoMasterModel"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteDDOMaster(string ddoId)
        {
            var mytask = Task.Run(() => _repository.DeleteDDOMaster(ddoId));
            var result = await mytask;

            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        


    }
}