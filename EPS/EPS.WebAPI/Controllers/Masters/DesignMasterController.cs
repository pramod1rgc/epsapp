﻿using EPS.BusinessAccessLayers.Masters;
using EPS.BusinessModels.Masters;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Masters
{
    /// <summary>
    /// This controller contains all methods for designation master
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DesignMasterController : ControllerBase
    {
         MsdesignBAL _BAL = new  MsdesignBAL();

        /// <summary>
        /// Used to get designation list
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<MsdesignBM>> GetMsdesignlist()
        {                    
                return await Task.Run(() => _BAL.GetMsdesign()).ConfigureAwait(true);
        }

        /// <summary>
        /// Used to get designation max id
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task< int> GetMsdesignMaxid()
        {
                return await Task.Run(() => _BAL.GetMsdesignMaxid()).ConfigureAwait(true); 
        }

        /// <summary>
        /// Used to update designation details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string>UpdateMsdesign([FromBody] MsdesignBM BM)
        {
            return await Task.Run(() => _BAL.UpdateMsdesign(BM)).ConfigureAwait(true);
            

        }

        /// <summary>
        /// Used to insert designation details
        /// </summary>
        /// <param name="BM"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> InsertMsdesign([FromBody]  MsdesignBM BM)
        {
            return await Task.Run(() => _BAL.InsertMsdesign(BM)).ConfigureAwait(true);
            
        }

        /// <summary>
        /// Used to delete designation details
        /// </summary>
        /// <param name="CommonDesigSrNo"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> DeleteMsdesign(int CommonDesigSrNo)
        {

            return await Task.Run(() => _BAL.DeleteMsdesign(CommonDesigSrNo)).ConfigureAwait(true);
            
        }
    }
}