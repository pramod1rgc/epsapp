﻿using EPS.BusinessAccessLayers.Dashboard;
using EPS.BusinessModels.Dashboard;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Dashboard
{
    /// <summary>
    /// DashboardController
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class DashboardController : ControllerBase
    {
        dashboardBusinessAccessLayer ObjdashboardBusinessAccessLayer = new dashboardBusinessAccessLayer();
        /// <summary>
        /// Get All Menus By User
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="uroleid"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<DashboardModel> getAllMenusByUser(string Username, string uroleid)
        {
            return ObjdashboardBusinessAccessLayer.getAllMenusByUser(Username, uroleid);
        }

        /// <summary>
        /// Get Dashboard Details
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public Task<IEnumerable<DashboardModel>> GetDashboardDetails()
        {
            return  ObjdashboardBusinessAccessLayer.GetDashboardDetails();
        }
    }
}