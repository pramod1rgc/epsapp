﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessModels.PayBill;
using EPS.BusinessAccessLayers.PayBill;

namespace EPS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaybillController : ControllerBase
    {
        PayBillGroupBAL objPayBillBal = new PayBillGroupBAL();
        

        [HttpGet("[action]")]
        public IEnumerable<AccountHeads> GetAccountHeads()
        {
            try
            {
                return objPayBillBal.GetAccountHeads();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("[action]")]
        public int InsertUpdatePayBillGroup([FromBody]EPS.BusinessModels.PayBill.PayBillGroup objBillGroup)
        {
            try
            {
                return objPayBillBal.InsertUpdatePayBillGroup(objBillGroup);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get Pay Bill Group  Details by Id
        /// </summary>
        /// <param name="BillGroupId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public PayBillGroupModel GetPayBillGroupDetailsById (int BillGroupId)
        {
            try
            {
                return objPayBillBal.GetPayBillGroupDetailsById(BillGroupId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}