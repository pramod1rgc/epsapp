﻿using EPS.BusinessAccessLayers.PayBill;
using EPS.BusinessModels.PayBill;
using EPS.CommonClasses;
using EPS.Repositories.PayBIllGroup;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.PayBillGroup
{

    //private static  IPa
    /// <summary>
    /// pay Bill Group
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PaybillController : ControllerBase
    {
        //private static IPayBillGroupBAL interfacePayBillGroup = new PayBillGroupBAL();
        private IPayBillGroupRepository _repository;
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        /// <summary>
        /// City master constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public PaybillController(IHttpContextAccessor accessor, IPayBillGroupRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }
        /// <summary>
        /// Get Account Heads
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAccountHeads(string PermDDOId)
        {
            var myTask = Task.Run(() => _repository.GetAccountHeads(PermDDOId));
            var result = await myTask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Insert Update PayBillGroup
        /// </summary>
        /// <param name="objPayBillGroup"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> InsertUpdatePayBillGroup([FromBody]EPS.BusinessModels.PayBill.PayBillGroup objPayBillGroup)
        {
            var myTask = Task.Run(() => _repository.InsertUpdatePayBillGroup(objPayBillGroup));
            var result = await myTask;
            return result;

        }
        /// <summary>
        /// Get Pay Bill Group  Details by Id
        /// </summary>
        /// <param name="BillGroupId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayBillGroupDetailsById(int BillGroupId)
        {

            var mytask = Task.Run(() => _repository.GetPayBillGroupDetailsById(BillGroupId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get List of  Pay Bill Group  Emp Attach
        /// </summary>
        /// <param name="EmpPermDDOId"></param>
        /// <returns></returns> 
        [HttpGet("[action]")]
        public async Task<IActionResult> GetpayBillGroupEmpAttach(string EmpPermDDOId)
        {

            var mytask = Task.Run(() => _repository.PayBillGroupAttach(EmpPermDDOId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }


        }
        /// <summary>
        /// Get List of  Pay Bill Group  Emp for  DeAttach
        /// </summary>
        /// <param name="EmpPermDDOId"></param>
        /// <param name="BillGrpID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetpayBillGroupEmpDeAttach(string EmpPermDDOId, string BillGrpID)
        {

            var mytask = Task.Run(() => _repository.PayBillGroupDeattach(EmpPermDDOId, BillGrpID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// update BillGroupId on Employee Table using EmpCode
        /// </summary>
        /// <param name="EmpCode"></param>
        /// <param name="BillGrpID"></param>
        /// <returns></returns>
        /// 
        [HttpPost("[action]")]
        public async Task<IActionResult> updatePayBillGroupbyEmpCodeandBillGroupId(string empCode, string BillGroupId)
        {

            var mytask = Task.Run(() => _repository.UpdatePayBillGroupIdusingEmpCodendBillGroupId(empCode, BillGroupId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// update BillGroupId Null Employee Table using EmpCode
        /// </summary>
        /// <param name="EmpCode"></param>
        ///
        /// <returns></returns>
        /// 

        [HttpPost("[action]")]
        public async Task<IActionResult> updatePayBillGroupNullbyEmpCode(string empCode)
        {

            var mytask = Task.Run(() => _repository.UpdatePayBillGroupIdNullusingEmpCodend(empCode));
            var result = await mytask;

            return Ok(result);


        }

        #region amit-Account Heads Other Than 1
        //amit ddocode
        /// <summary>
        /// Get List of  Account Heads Other Than 1
        /// </summary>
        /// <param name="FinFromYr"></param>
        /// <param name="FinToYr"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAccountHeadsOT1(string FinFromYr, string FinToYr, string Flag)
        {

            var mytask = Task.Run(() => _repository.GetAccountHeadsOT1(FinFromYr, FinToYr, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        ///  Get List of  Account Heads Other Than 1 Attach List
        /// </summary>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAccountHeadOT1AttachList(string DDOCode, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetAccountHeadOT1AttachList(DDOCode, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Update account Head Other Than 1
        /// </summary>
        /// <param name="accountHeadvalue"></param>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateaccountHeadOT1(string accountHeadvalue, string DDOCode, string Flag)
        {
            var mytask = Task.Run(() => _repository.UpdateaccountHeadOT1(accountHeadvalue, DDOCode, Flag));
            var result = await mytask;

            return Ok(result);


        }


        /// <summary>
        /// Get Activated/deActivated list account Head Other Than 1
        /// </summary>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAccountHeadOT1PaybillGroupStatus(string DDOCode, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetAccountHeadOT1PaybillGroupStatus(DDOCode, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Update Activated/deActivated account Head Other Than 1
        /// </summary>
        /// <param name="accountHeadvalue"></param>
        /// <param name="DDOCode"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateAccountHeadOT1PaybillGroupStatus(string accountHeadvalue, string DDOCode, string Flag)
        {
            var mytask = Task.Run(() => _repository.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, DDOCode, Flag));
            var result = await mytask;

            return Ok(result);

        }

        #endregion

        /// <summary>
        /// Get NgRecovery
        /// </summary>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNgRecovery(string PermDDOId)
        {
            var mytask = Task.Run(() => _repository.GetNgRecovery(PermDDOId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get Bank details By Ifsc Code
        /// </summary>
        /// <param name="IfscCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBankdetailsByIfsc(string IfscCode)
        {            
            var mytask = Task.Run(() => _repository.GetBankdetailsByIfsc(IfscCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Insert Update Ng Recovery
        /// </summary>
        /// <param name="objNgRecovery"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUpdateNgRecovery(NgRecovery objNgRecovery)
        {
            var mytask = Task.Run(() => _repository.InsertUpdateNgRecovery(objNgRecovery));
            var result = await mytask;
            
            return Ok(result);
            

        }


        /// <summary>
        /// Activate Deactivate Ng Recovery
        /// </summary>
        /// <param name="objNgRecovery"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ActiveDeactiveNgRecovery(NgRecovery objNgRecovery)
        {
            var mytask = Task.Run(() => _repository.ActiveDeactiveNgRecovery(objNgRecovery));
            var result = await mytask;

            return Ok(result);

        }

        /// <summary>
        /// Activate Deactivate Ng Recovery
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetFinancialYear()
        {

            var mytask = Task.Run(() => _repository.GetFinancialYear());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get All Month
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllMonth()
        {
            var mytask = Task.Run(() => _repository.GetAllMonth());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        /// <summary>
        /// Get PayBill Group By Financial Year
        /// </summary>
        /// /// <param name="FinYearFrom"></param>
        /// <param name="FinYearTo"></param>
        /// <param name="PermDdoId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayBillGroupByFinancialYear(string FinYearFrom, string FinYearTo, string PermDdoId)
        {
            var mytask = Task.Run(() => _repository.GetPayBillGroupByFinancialYear(FinYearFrom, FinYearTo, PermDdoId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Employees By Pay Item Code
        /// </summary>
        /// /// <param name="PayItemCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeesByPayItemCode(int PayItemCd)
        {
            
            var mytask = Task.Run(() => _repository.GetEmployeesByPayItemCode(PayItemCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get Pay Items From Designation Code
        /// </summary>
        /// /// <param name="DesigCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayItemsFromDesignationCode(int DesigCode)
        {
            var mytask = Task.Run(() => _repository.GetPayItemsFromDesignationCode(DesigCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Update Employee Non Computational Dues Deduct Amount
        /// </summary>
        /// /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateEmpNonComputationalDuesDeductAmount(UpdateEmpDuesDeductAmountModel obj)
        {
            obj.IpAddress = clientIP;
            var mytask = Task.Run(() => _repository.UpdateEmpNonComputationalDuesDeductAmount(obj));
            var result = await mytask;

            return Ok(result);
        }

        /// <summary>
        /// Get Employee List By Paybill And Designation
        /// </summary>
        /// <param name="PermDDOId"></param>
        /// <param name="PayBillGroupId"></param>
        /// <param name="DesigCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeByPaybillAndDesig(string PermDDOId, int PayBillGroupId, int DesigCd)
        {
            
            var mytask = Task.Run(() => _repository.GetEmployeeByPaybillAndDesig(PermDDOId, PayBillGroupId, DesigCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertAllNgEntry([FromBody]List<EmpList> ngList)
        {
            var mytask = Task.Run(() => _repository.InsertAllNgEntry(ngList));
            var result = await mytask;

            return Ok(result);
        }

        /// <summary>
        /// Get Ng Recovery List
        /// </summary>
        /// <param name="PermDDOId"></param>

        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNgRecoveryList(string PermDDOId)
        {
            var mytask = Task.Run(() => _repository.GetNgRecoveryList(PermDDOId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get All NgRecovery and PaybillGrpDetails
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllNgRecoveryandPaybillGrpDetails(string permDdoId)
        {
            var mytask = Task.Run(() => _repository.GetAllNgRecoveryandPaybillGrpDetails(permDdoId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

    }
}