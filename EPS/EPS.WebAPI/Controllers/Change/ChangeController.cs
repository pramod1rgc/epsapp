﻿using EPS.BusinessAccessLayers.Change;
using EPS.BusinessModels.Change;
using EPS.Repositories.Change;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.Change
{
    /// <summary>
    /// Change Module :- Employee Specific :- Web api Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class ChangeController : Controller
    {
        //Changebal _repository = new Changebal();


        private IChangeRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        public ChangeController(IChangeRepository repository)
        {
            _repository = repository;
        }



        /// <summary>
        /// common dropdown use in Change module
        /// </summary>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDdlvalue(string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDdlvalue(Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// common Delete use in Change module
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> ChangeDeleteDetails(string empCd, string orderNo, string Flag)
        {

            var mytask = Task.Run(() => _repository.ChangeDeleteDetails(empCd, orderNo, Flag));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #region DOB

        /// <summary>
        /// Insert / Update DOB Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        [HttpPost("[action]")]
        public async Task<IActionResult> InsertDOBDetails([FromBody]DOBcls objPro, string Flag)
        {

            var mytask = Task.Run(() => _repository.InsertDOBDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get DOB list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDOBDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDOBDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker DOB list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerDOBUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerDOBUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }




        #endregion

        #region NameGender
        /// <summary>
        /// Insert / Update NameGender Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertNameGenderDetails([FromBody]NameGender objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertNameGenderDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get NameGender list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNameGenderDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetNameGenderDetails(empcode,roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker NameGender list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerNameGenderUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerNameGenderUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region PFNPS
        /// <summary>
        /// Insert / Update PFNPS Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertPfNpsFormDetails([FromBody]PFNPS objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertPfNpsFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get PF/NPS list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPfNpsFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetPfNpsFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker PF/NPS User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerPfNpsUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerPfNpsUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region panno
        /// <summary>
        /// Insert / Update panno Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertPanNoFormDetails([FromBody]PANNumber objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertPanNoFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();

            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get panno list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPanNoFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetPanNoFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker panno User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerPanNoUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerPanNoUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region HRACityClass
        /// <summary>
        /// Insert / Update HRACityClass Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertHraccFormDetails([FromBody]HRAcityclass objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertHraccFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get HRACityClass list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetHraccFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetHraccFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        /// <summary>
        /// Forward To Checker HRACityClass User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerHraccUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerHraccUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region designation

        /// <summary>
        /// Insert / Update designation Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertdesignationFormDetails([FromBody]designationcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertdesignationFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get designation list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetdesignationFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetdesignationFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker designation User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerdesignationUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerdesignationUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region Exservicemen
        /// <summary>
        /// Insert / Update Exservicemen Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertexservicemenFormDetails([FromBody]exservicemencls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertexservicemenFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Exservicemen list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetexservicemenFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetexservicemenFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker Exservicemen User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerexservicemenUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerexservicemenUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #region cghs

        /// <summary>
        /// Insert / Update Exservicemen Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertCGHSFormDetails([FromBody]CGHScls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertCGHSFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Exservicemen list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCGHSFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetCGHSFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker Exservicemen User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerCGHSUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerCGHSUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region CGEGIS

        /// <summary>
        /// Insert / Update CGEGIS Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertCGEGISFormDetails([FromBody]CGEGIScls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertCGEGISFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get CGEGIS list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCGEGISFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetCGEGISFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker CGEGIS User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerCGEGISFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerCGEGISFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region JoiningModeForm
        /// <summary>
        /// Insert / Update JoiningModeForm Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertJoiningModeFormDetails([FromBody]joiningmodecls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertJoiningModeFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get JoiningModeForm list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpGet("[action]")]        
        public async Task<IActionResult> GetJoiningModeFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetJoiningModeFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker JoiningModeForm User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerJoiningModeFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerJoiningModeFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region DOJ
        /// <summary>
        /// Insert / Update DOJ Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertDOJDetails([FromBody]DOJcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertDOJDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get DOJ list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDOJDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDOJDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker DOJ User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerDOJUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerDOJUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region DOE
        /// <summary>
        /// Insert / Update DOE Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertDOEDetails([FromBody]DOEcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertDOEDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get DOE list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDOEDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDOEDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker DOE User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerDOEUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerDOEUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region DOR
        /// <summary>
        /// Insert / Update DOR Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertDORDetails([FromBody]DORcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertDORDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get DOR list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDORDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDORDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker DOR User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerDORUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerDORUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region CasteCategory
        /// <summary>
        /// Insert / Update CasteCategory Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]

        public async Task<IActionResult> InsertCasteCategoryFormDetails([FromBody]CasteCategorycls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertCasteCategoryFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get CasteCategory list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCasteCategoryFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetCasteCategoryFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker CasteCategory User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerCasteCategoryFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerCasteCategoryFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region Entofficevehicle
        /// <summary>
        /// Insert / Update Entofficevehicle Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertEntofficevehicleFormDetails([FromBody]Entofficevehiclecls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertEntofficevehicleFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Entofficevehicle list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEntofficevehicleFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetEntofficevehicleFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker Entofficevehicle User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerEntofficevehicleFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerEntofficevehicleFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region BankForm
        /// <summary>
        /// Insert / Update BankForm Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        [HttpPost("[action]")]
        public async Task<IActionResult> InsertBankFormDetails([FromBody]BankFormcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertBankFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get BankForm list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBankFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetBankFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker BankForm User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerBankFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerBankFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region addtapd
        /// <summary>
        /// Insert / Update addtapd Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        [HttpPost("[action]")]
        public async Task<IActionResult> InsertaddtapdFormDetails([FromBody]addtapdcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertaddtapdFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get addtapd list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetaddtapdFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetaddtapdFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker addtapd User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckeraddtapdUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckeraddtapdUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region TPTA
        /// <summary>
        /// Insert / Update TPTA Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        [HttpPost("[action]")]
        public async Task<IActionResult> InsertTPTAFormDetails([FromBody]TPTAcls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertTPTAFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get TPTA list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetTPTAFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetTPTAFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker TPTA User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerTPTAUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerTPTAUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #region StateGIS
        /// <summary>
        /// Insert / Update StateGIS Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>


        [HttpPost("[action]")]
        public async Task<IActionResult> InsertStateGISFormDetails([FromBody]StateGIScls objPro, string Flag)
        {
            var mytask = Task.Run(() => _repository.InsertStateGISFormDetails(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get StateGIS list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetStateGISFormDetails(string empcode, int roleId, string Flag)
        {
            var mytask = Task.Run(() => _repository.GetStateGISFormDetails(empcode, roleId, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Forward To Checker StateGIS User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerStateGISUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerStateGISUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion



        #region MobileNo_Email_EmpCode
        /// <summary>
        /// Insert / Update MobileNo_Email_EmpCode
        /// </summary>
        /// <param name="objPro"></param>      
        /// <param name="flag"></param>
        /// <param name="Contactdetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertMobileNoEmailEmpCodeDetails([FromBody]ConMobileNoEmailEmpCodecls objPro, string flag, string Contactdetails)
        {
            var mytask = Task.Run(() => _repository.InsertMobileNoEmailEmpCodeDetails(objPro, flag, Contactdetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get MobileNo_Email_EmpCode list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <param name="Flag"></param>
        /// <param name="ContactdetailsSelectedOption"></param>      
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetMobileNoEmailEmpCodeDetails(string empcode, int roleId, string Flag, string ContactdetailsSelectedOption)
        {
            var mytask = Task.Run(() => _repository.GetMobileNoEmailEmpCodeDetails(empcode, roleId, Flag, ContactdetailsSelectedOption));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker MobileNo_Email_EmpCode User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <param name="ContactdetailsSelectedOption"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerMobileNoEmailEmpCodeUserDtls(string empCd, string orderNo, string status, string rejectionRemark, string ContactdetailsSelectedOption)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(empCd, orderNo, status, rejectionRemark, ContactdetailsSelectedOption));
           
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion




    }
}