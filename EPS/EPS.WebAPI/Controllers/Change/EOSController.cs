﻿using EPS.BusinessAccessLayers.Change;
using EPS.BusinessModels.Change;
using EPS.Repositories.Change;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace EPS.WebAPI.Controllers.Change
{

    /// <summary>
    /// Change Module :- Extension of service :- Web api Controller
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EOSController : Controller
    {
        //ExtensionOfServiceBAL objProBAL = new ExtensionOfServiceBAL();
        private IExtensionOfServiceRepository _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        public EOSController(IExtensionOfServiceRepository repository)
        {
            _repository = repository;            
        }

        /// <summary>
        /// common dropdown use in Change module
        /// </summary>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDdlvalue(string Flag)
        {
            var mytask = Task.Run(() => _repository.GetDdlvalue(Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// common Delete use in Change module
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> ChangeDeleteDetails(string empCd, string orderNo, string Flag)
        {

            var mytask = Task.Run(() => _repository.ChangeDeleteDetails(empCd, orderNo, Flag));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }



        /// <summary>
        /// Insert / Update Extension of service Details 
        /// </summary>
        /// <param name="objPro"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>



        [HttpPost("[action]")]
        public async Task<IActionResult> InsertEOSDetails([FromBody]ExtensionOfServiceModel1 objPro, string Flag)
        {

            var mytask = Task.Run(() => _repository.InsertEOSDetailsBAL(objPro, Flag));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Extension Of Service list 
        /// </summary>
        /// <param name="empcode"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetExtensionOfServiceDetails(string empcode, int roleId)
        {
            var mytask = Task.Run(() => _repository.GetExtensionOfServiceDetailsBAL(empcode, roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Forward To Checker User list
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="orderNo"></param>
        /// <param name="status"></param>
        /// <param name="rejectionRemark"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardToCheckerUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {

            var mytask = Task.Run(() => _repository.ForwardToCheckerUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


    }
}
