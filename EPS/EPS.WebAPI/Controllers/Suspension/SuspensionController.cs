﻿using EPS.BusinessModels.Suspension;
using EPS.Repositories.Suspension;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Suspension
{
    /// <summary>
    /// suspension controller
    /// </summary>
    /// 

    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class SuspensionController : Controller
    {
        // private IHttpContextAccessor _accessor;
        private ISuspension _repository;

        /// <summary>
        /// Suspension constructor
        /// </summary>
        /// <param name="repository"></param>
        public SuspensionController(ISuspension repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Get all designation
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDesignation(string Id)
        {
                var res = await Task.Run(() =>
                {
                    return _repository.GetAllDesignation(Id);
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }

        }
        /// <summary>
        /// Get all employees
        /// </summary>
        /// <param name="desigId"></param>
        /// <param name="isCheckerLogin"></param>
        /// <param name="comName"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllEmployees(int desigId, string isCheckerLogin, string comName)
        {
                var res = await Task.Run(() =>
                {
                    return _repository.GetAllEmployees(desigId, Convert.ToBoolean(isCheckerLogin), comName);
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSuspen"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavedNewSuspensionDetails([FromBody]SuspensionDetails objSuspen)
        {
            string successMsg = await Task.Run(() =>
            {
                return _repository.SavedNewSuspensionDetails(objSuspen);
            });
            return successMsg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="isCheckerLogin"></param>
        /// <param name="comName"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<SuspensionDetails>> GetSuspensionDetails(string empCode, string isCheckerLogin, string comName)
        {
                return await _repository.GetSuspensionDetails(empCode, Convert.ToBoolean(isCheckerLogin), comName);   
        }

        /// <summary>
        /// Delete suspension record
        /// </summary>
        /// <param name="id"></param>
        /// <param name="compName"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<string> DeleteSuspensionDetails(int id, string compName)
        {
                return await _repository.DeleteSuspensionDetails(id, compName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAndcompName"></param>
        /// <param name="status"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> VerifiedAndRejectedSuspension(string idAndcompName, string status, string reason)
        {
                int id = Convert.ToInt32(idAndcompName.Split("_")[0]);
                string compName = idAndcompName.Split("_")[1];
                return await _repository.VerifiedAndRejectedSuspension(id, status, reason, compName);
        }

        //[HttpPost("[action]")]
        //public ActionResult VerifiedAndRejectedSuspension(string idAndcompName, string status, string reason)
        //{
        //    int id = Convert.ToInt32(idAndcompName.Split("_")[0]);
        //    string compName = idAndcompName.Split("_")[1];
        //    return Json(_repository.VerifiedAndRejectedSuspension(id, status, reason, compName));
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavedExtensionDetails([FromBody]ExtensionDetails obj)
        {
                return await _repository.SavedExtensionDetails(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objRevok"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavedRevocationDetails([FromBody] RevocationDetails objRevok)
        {
                return await _repository.SavedRevocationDetails(objRevok);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="susId"></param>
        /// <param name="empId"></param>
        /// <param name="isCheckerLogin"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<RevocationDetails>> GetRevocationDetails(int susId, int empId, string isCheckerLogin)
        {
                return await _repository.GetRevocationDetails(susId, empId, Convert.ToBoolean(isCheckerLogin));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objJoining"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<string> SavedJoiningDetails([FromBody] JoiningDetails objJoining)
        {
                string successMsg = await Task.Run(() =>
                {
                    return _repository.SavedJoiningDetails(objJoining);
                });

                return successMsg;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objReg"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SavedRegulariseDetails([FromBody] RegulariseDetailse objReg)
        {
                string successMsg = await Task.Run(() =>
                {
                    return _repository.SavedRegulariseDetails(objReg);
                });

                return successMsg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="joinId"></param>
        /// <param name="isCheckerLogin"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetJoiningDetails(int joinId, string isCheckerLogin)
        {
                var res = await Task.Run(() =>
                {
                    return _repository.GetJoiningDetails(joinId, Convert.ToBoolean(isCheckerLogin));
                });

                if (res == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(res);
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="joiningId"></param>
        /// <param name="isCheckerLogin"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRegulariseDetails(int joiningId, string isCheckerLogin)
        {
            var res = await Task.Run(() =>{
                return _repository.GetRegulariseDetails(joiningId, Convert.ToBoolean(isCheckerLogin));
            });

            if (res == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(res);
            }
        }

    }
}

