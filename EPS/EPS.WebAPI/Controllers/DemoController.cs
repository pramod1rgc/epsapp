﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Mvc;
using EPS.BusinessModels.Demo;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using EPS.CommonClasses;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers
{

    [Route("api/[controller]")]
    //[EPSExceptionFilters]   
    public class DemoController : Controller
    {
        private IHttpContextAccessor _accessor;      
        ILogger _logger;
        int iiiii = 0;
        public DemoController(ILoggerFactory loggerFactory, IHttpContextAccessor accessor)
        {
           // _connection = connection;
               _accessor = accessor;
           _logger = loggerFactory.CreateLogger<DemoController>();
        }
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpPost("[action]")]
        [ValidateModel]
        public string Demo([FromBody] DemoModel demoModel)
        {
            
            return "tttt";

        }
        /// <summary>
        /// Exception method
        /// </summary>
        /// <returns>Int</returns>
        [HttpGet("[action]/{id?}")]
        [EPSExceptionFilters]
        public int GetException(int id)
        {
            
            return id/0;
        }/// <summary>
         /// Get IP From App
         /// </summary>
         /// <returns></returns>
        [HttpGet("[action]")]
        public string GetIP()
        {
           
         var userIP = CommonFunctions.GetClientIP(_accessor);
          return userIP.ToString()+" Connection "+ConnectionClass.ConnectionString;          
        }
          
        }

    }




