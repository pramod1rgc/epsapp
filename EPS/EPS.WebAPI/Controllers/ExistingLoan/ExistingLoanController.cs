﻿
using System.Threading.Tasks;
using EPS.BusinessModels.ExistingLoan;
using EPS.Repositories.ExistingLoan;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.ExistingLoan
{
 /// <summary>
 /// 
 /// </summary>

    [EPSExceptionFilters]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]    
    
    public class ExistingLoanController : Controller
    {
        private IHttpContextAccessor _accessor;
        private IAlreadyTakenRepository _repository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        
        public ExistingLoanController(IHttpContextAccessor accessor, IAlreadyTakenRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
        }


        /// <summary>
        /// Bind DropDown Loan Type
        /// </summary>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> BindDropDownLoanType()
        {
            var mytask = Task.Run(() => _repository.BindDropDownLoanType());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="PermDdoId"></param>
       /// <param name="EmpCode"></param>
       /// <returns></returns>
        [HttpGet("[action]")]

        public async Task<IActionResult> LoanAdvanceDetails(int PermDdoId, string EmpCode)
        {
            var mytask = Task.Run(() => _repository.GetAlreadyTakenLoanAdvanceDetails(PermDdoId, EmpCode));
            var result = await mytask;
            if (result == null)
            {
                return null;
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// Bind Head Account
        /// </summary>
        /// <param name="LoanCD"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindHeadAccount(int LoanCD)
        {
            var mytask = Task.Run(() => _repository.GetHeadAccount(LoanCD));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="ddoId"></param>
      /// <param name="UserId"></param>
      /// <param name="userRoleID"></param>
      /// <returns></returns>
     
        [HttpGet("[action]")]
        public async Task<IActionResult> BindExistingLoanStatus(int ddoId, int UserId,int userRoleID)
        {
            var mytask = Task.Run(() => _repository.BindExistingLoanStatus(ddoId, UserId, userRoleID));
            var result = await mytask;
            if (result == null)
            {
                return null;
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Get ExistLoanDetails By ID
        /// </summary>
        /// <param name="MSPayAdvEmpdetID"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetExistLoanDetailsByID(string MSPayAdvEmpdetID)
        {
            var mytask = Task.Run(() => _repository.GetExistLoanDetailsByID(MSPayAdvEmpdetID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }      
        }

        /// <summary>
        /// Save ExistingLoanData
        /// </summary>
        /// <param name="LoanApplicationModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveExistingLoanData(ExistingLoanApplicationModel LoanApplicationModel)
        {
            var mytask = Task.Run(() => _repository.SaveLoanData(LoanApplicationModel));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }     
        }
        /// <summary>
        /// Get EmpLoanList Details
        /// </summary>
        /// <param name="PayBillGpID"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpLoanListDetails(string PayBillGpID)
        {
            var mytask = Task.Run(() => _repository.GetExistingEmpLoanDetails(PayBillGpID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }       
        }

        /// <summary>
        /// ExistingLoan ForwordToDDO
        /// </summary>
        /// <param name="LoanApplicationModel"></param>
        /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<IActionResult> ExistingLoanForwordToDDO(ExistingLoanApplicationModel LoanApplicationModel)
        {
            var mytask = Task.Run(() => _repository.ExistingLoanForwordToDDO(LoanApplicationModel));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CtrCode"></param>
        /// <param name="msPayAdvEmpdetID"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteEmpDetails(string CtrCode, string msPayAdvEmpdetID,int Mode)
        {
            var mytask = Task.Run(() => _repository.DeleteEmpDetails(CtrCode, msPayAdvEmpdetID, Mode));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
            // return null;
        }
    }
}