﻿using EPS.BusinessAccessLayers.ExistingLoan;
using EPS.BusinessModels.ExistingLoan;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.ExistingLoan
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MultipleInstRecoveryController : Controller
    {
        RecoveryInstBAL ObjRecoveryInstBAL = new RecoveryInstBAL();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecoveryEmpDetails(string EmpCd)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.GetEmpDetails(EmpCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LoanCd"></param>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> BindSanctionDetails(string LoanCd, string EmpCd)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.BindSanctionDetails(LoanCd, EmpCd));
            var result = await mytask;
            if (result == null)
            {
                return null;
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjSnactionModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]

        public async Task<IActionResult> SaveSanctionData([FromBody]SnactionDataModel ObjSnactionModel)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.SaveSanctionData(ObjSnactionModel));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjSnactionModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> ForwardtoDDOChecker([FromBody]SnactionDataModel ObjSnactionModel)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.ForwardtoDDOChecker(ObjSnactionModel));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetInstEmpDetails(string EmpCd)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.GetInstEmpDetails(EmpCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCD"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> Accept(string EmpCD)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.Accept(EmpCD));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCD"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Reject(string EmpCD)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.Reject(EmpCD));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjSnactionModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> EditLoanDetailsById([FromBody]SnactionDataModel ObjSnactionModel)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.EditLoanDetailsById(ObjSnactionModel));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msEmpLoanMultiInstId"></param>
        /// <param name="empCD"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteLoanDetailsById(int msEmpLoanMultiInstId, string empCD)
        {
            var mytask = Task.Run(() => ObjRecoveryInstBAL.DeleteLoanDetailsById(msEmpLoanMultiInstId, empCD));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
          
        }


    }
}