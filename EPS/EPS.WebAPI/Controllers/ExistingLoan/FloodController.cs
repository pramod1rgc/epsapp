﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.ExistingLoan;
using EPS.BusinessModels.ExistingLoan;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace EPS.WebAPI.Controllers.ExistingLoan
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class FloodController : ControllerBase
    {
        FloodBAL ObjFloodBAL = new FloodBAL();

        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpDetails(int BillGrID, string desigId,int UserID,int mode)
        {
            var mytask = Task.Run(() => ObjFloodBAL.GetEmpDetails( BillGrID, desigId, UserID, mode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> InsertFloodData([FromBody] SanctionDetailsMOdel ObjSanctionDetails)
        {
            FloodBAL ObjFloodBAL = new FloodBAL();

            var mytask = Task.Run(() => ObjFloodBAL.InsertFloodData(ObjSanctionDetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }



        [HttpPost("[action]")]
        public async Task<IActionResult> ForwordToDdo([FromBody] SanctionDetailsMOdel ObjSanctionDetails)
        {
            FloodBAL ObjFloodBAL = new FloodBAL();

            var mytask = Task.Run(() => ObjFloodBAL.ForwordToDdo(ObjSanctionDetails));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


    }
}
