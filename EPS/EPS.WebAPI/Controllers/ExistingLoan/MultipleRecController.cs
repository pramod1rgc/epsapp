﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.ExistingLoan;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace EPS.WebAPI.Controllers.ExistingLoan
{
    [Route("api/[controller]")]
    public class MultipleRecController : Controller
    {
        private IHttpContextAccessor _accessor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        public MultipleRecController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecovery(string EmpCd)
        {

            RecoveryInstBAL ObjRecoveryInstBAL = new RecoveryInstBAL();
            var mytask = Task.Run(() => ObjRecoveryInstBAL.GetEmpDetails(EmpCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        
    }
}