﻿using EPS.BusinessAccessLayers.UserProfile;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EPS.WebAPI.Controllers.UserProfile
{
    /// <summary>
    /// UserProfileController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserProfileController : ControllerBase
    {
        UserProfileBusinessAccessLayer objUserProfileBusinessAccessLayer = new UserProfileBusinessAccessLayer();
        //[HttpGet("[action]")]
        //public IEnumerable<UserProfileDetailsModel> GetUserdetailsProfile1(string username)
        //{
        //    try
        //    {
        //        return objUserProfileBusinessAccessLayer.GetUserdetailsProfile(username);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// GetUserdetailsProfile
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        //[HttpPost("[action]")]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetUserdetailsProfile(string username)
        {
            var myTask = Task.Run(() => objUserProfileBusinessAccessLayer.GetUserdetailsProfile(username));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);
            }
            else
            {
                return Ok(result);
            }
        }

    }
}