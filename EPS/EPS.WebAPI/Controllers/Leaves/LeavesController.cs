﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using EPS.WebAPI.CustomFilters;
using EPS.BusinessModels.Leaves;
using EPS.Repositories.LeaveMangement;
using EPS.CommonClasses;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers.Leaves
{

    /// <summary>
    /// Leave Controller
    /// </summary>
    [EPSExceptionFilters]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class LeavesController : Controller
    {
        private IHttpContextAccessor _accessor;
        private static string clientIP = string.Empty;
        private ILeaveManagementRepository _repository;
        /// <summary>
        /// Leaves constructor
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="repository"></param>
        public LeavesController(IHttpContextAccessor accessor, ILeaveManagementRepository repository)
        {
            _repository = repository;
            _accessor = accessor;
            clientIP = CommonFunctions.GetClientIP(_accessor);
        }


        #region Get All Designation
        /// <summary>
        /// Get Designation
        /// </summary>
        ///<param name="PermDdoId"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDesignation(string PermDdoId)
        {
            var mytask = Task.Run(() => _repository.GetAllDesignation(PermDdoId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion


        #region Get Employee by designationId

        /// <summary>
        ///Get Employee by designation ID
        /// </summary>
        /// <param name="Designid"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmployeeByDesig(string MsDesignID)
        {
            var mytask = Task.Run(() => _repository.GetEmployeeByDesig(MsDesignID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion


        #region Get Order by Emp
        /// <summary>
        /// Get Order by Emp
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <param name="userRoleId"></param>
        /// <param name="pageCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOrderByEmployee(string EmpCd,string userRoleId,string pageCode)
        {
            var mytask = Task.Run(() => _repository.GetOrderByEmployee(EmpCd, userRoleId, pageCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion


        #region Get All LeavesType

        /// <summary>
        /// Get LeaveType
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesType()
        {

            var mytask = Task.Run(() => _repository.GetLeavesType());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get All LeavesReason
        /// <summary>
        /// Get Leave Reason
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesReason()
        {


            var mytask = Task.Run(() => _repository.GetLeavesReason());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Sanction

        #region Get Leave Sanction
        /// <summary>
        /// Get details of sanctioned leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesSanction(string permDdoId, string empCode,string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesSanction(permDdoId, empCode, userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get Leave Sanction History
        /// <summary>
        /// Get Status of sanctioned leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesSanctionHistory(string permDdoId, string empCode, string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesSanctionHistory(permDdoId, empCode, userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Save Leave Sanction
        /// <summary>
        /// Save/Forward details of leave
        /// </summary>
        /// <param name="ObjLeavesSanctionDetails"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveLeavesSanction([FromBody]LeavesSanctionMainModel ObjLeavesSanctionDetails,string Status)
        {
            ObjLeavesSanctionDetails.IpAddress = clientIP;
            var mytask = Task.Run(() => _repository.SaveLeavesSanction(ObjLeavesSanctionDetails, Status));
            var result = await mytask;
          
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

        #endregion


        #region Curtail/Extended

        #region Get Curtail/Extended Leave
        /// <summary>
        ///  Get details of Curtail/Extended leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesCurtailDetail(string permDdoId, string empCode, string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesCurtailDetail(permDdoId, empCode, userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get Curtail/Extended Leave History
        /// <summary>
        ///  Get Status of Curtail/Extended leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesCurtailHistory(string permDdoId, string empCode, string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesCurtailHistory(permDdoId, empCode, userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get Verified Leave Sanction
        /// <summary>
        /// Get Verified Leave  for curtail
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="ordId"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetVerifiedLeavesSanction(string permDdoId, string ordId)
        {
            var mytask = Task.Run(() => _repository.GetVerifiedLeavesSanction(permDdoId, ordId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion

 
        #region Save Leave CurtExten
        /// <summary>
        /// Save/Forward details of Curtail/Extended leave
        /// </summary>
        /// <param name="ObjLeavesCurtailDetails"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveLeavesCurtExten([FromBody]LeavesCurtailModel ObjLeavesCurtailDetails, string Status)
        {
            ObjLeavesCurtailDetails.IpAddress=clientIP;
            var mytask = Task.Run(() => _repository.SaveLeavesCurtExetn(ObjLeavesCurtailDetails, Status));
            var result = await mytask;

            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        #endregion



        #region Cancel

        #region Get Cancel Leave 
        /// <summary>
        /// Get details of Canceled leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesCancel(string permDdoId, string empCode, string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesCancel(permDdoId, empCode, userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get Cancel Leave History
        /// <summary>
        ///  Get Status of Canceled leave of employee
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="empCode"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesCancelHistory(string permDdoId, string empCode, string userRoleId)
        {
            var mytask = Task.Run(() => _repository.GetLeavesCancelHistory(permDdoId, empCode,userRoleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Get Verified Leave For Cancel
        /// <summary>
        /// Get Verified Leave For Cancel
        /// </summary>
        /// <param name="permDdoId"></param>
        /// <param name="ordId"></param>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetVerifiedLeaveForCancel(string permDdoId, string ordId)
        {
            var mytask = Task.Run(() => _repository.GetVerifiedLeaveForCancel(permDdoId, ordId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Save Leave Cancel
        /// <summary>
        ///Save/Forward details of canceled leave
        /// </summary>
        /// <param name="ObjLeavesCancelDetails"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> SaveLeavesCancel([FromBody]LeavesCancelModel ObjLeavesCancelDetails, string Status)
        {
            ObjLeavesCancelDetails.IpAddress = clientIP;
             var mytask = Task.Run(() => _repository.SaveLeavesCancel(ObjLeavesCancelDetails, Status));
            var result = await mytask;

            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        #endregion

        #endregion

        #region Delete Leave Sanction
        /// <summary>
        /// Delete sanctioned/curtail/cancel leave as per pagecode and leaveid
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pageCode"></param>
        /// <returns></returns>
        [HttpDelete("DeleteLeavesSanction")]
        public async Task<IActionResult> DeleteLeavesSanction(string id, string pageCode)
        {
            var mytask = Task.Run(() => _repository.DeleteLeavesSanction(id, pageCode));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion


        #region Verify Return Sanction
        /// <summary>
        /// Verify and return leave forwarded be maker
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Status"></param>
        /// <param name="PageCd"></param>
        /// <param name="Reason"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> VerifyReturnSanction(string Id, string Status, string PageCd, string Reason)
        {
            var mytask = Task.Run(() => _repository.VerifyReturnSanction(Id, Status, PageCd, Reason, clientIP));
            var result = await mytask;
            if (result == 0) return NotFound(); else return Ok(result);
        }
        #endregion
        #region Get Leave Type Balance
        /// <summary>
        /// Get Leave Type Balance
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLeavesTypeBalance(string EmpCd)
        {
            var mytask = Task.Run(() => _repository.GetLeavesTypeBalance(EmpCd));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        #endregion

        #region Save Total CCL Leave Availed
        /// <summary>
        /// Save Total CCL Leave Availed
        /// </summary>
        /// <param name="EmpCd"></param>
        /// <param name="TotalLeaveAvailed"></param>
        /// <returns></returns>
        /// 

        [HttpPost("[action]")]
        public async Task<IActionResult> SaveTotalLeaveAvailed(string EmpCd,string TotalLeaveAvailed)
        {
            var mytask = Task.Run(() => _repository.SaveTotalLeaveAvailed(EmpCd, TotalLeaveAvailed,clientIP));
            var result = await mytask;

            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion

    }
}
