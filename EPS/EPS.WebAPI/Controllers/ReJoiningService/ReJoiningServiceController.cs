﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.ReJoiningService;
using EPS.BusinessModels.ReJoiningService;
using EPS.CommonClasses;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.ReJoiningService
{
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    public class ReJoiningServiceController : Controller
    {

        private static string clientIP = string.Empty;
        /// <summary>
        /// ReJoiningService Constructor
        /// </summary>
        /// <param name="accessor"></param>
        public ReJoiningServiceController(IHttpContextAccessor accessor)
        {
            clientIP = CommonFunctions.GetClientIP(accessor);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="empCode"></param>
        /// <param name="empName"></param>
        /// <param name="empPanNo"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<GetServiceEndEmployees>> GetServiceEndEmployees(string empCode, string empName, string empPanNo)
        {
            ReJoiningServiceBAL objRejBAL = new ReJoiningServiceBAL();
            return await Task.Run(() => objRejBAL.GetServiceEndEmployeesDetailsBAL(empCode, empName, empPanNo));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="SearchTerm"></param>
        /// <param name="empCd"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IEnumerable<GetReJoiningServiceEmployees>> GetReJoiningServiceEmployees(int pageNumber, int pageSize, string SearchTerm, string empCd, int roleId)
        {
            ReJoiningServiceBAL objRejBAL = new ReJoiningServiceBAL();
            return await Task.Run(() => objRejBAL.GetReJoiningServiceEmployeesDetailsBAL(pageNumber, pageSize, SearchTerm, empCd, roleId));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ValidateModel]
        public async Task<string> UpsertReJoiningServiceDetails([FromBody]UpsertReJoiningServiceDetails obj)
        {
            ReJoiningServiceBAL objRejBAL = new ReJoiningServiceBAL();
            obj.IpAddress = clientIP;
            string successMsg = await Task.Run(() => objRejBAL.UpsertReJoiningServiceDetailsBAL(obj));
            return successMsg; // !successMsg.Equals(string.Empty) ? (successMsg == "1" ? "Data saved successfully!" : (successMsg == "-1" ? "Order Number Already Exist!" : string.Empty)) : "Some Error Occurred.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> DeleteReJoiningServiceDetails([FromBody]DeleteRejoiningService obj)
        {
            ReJoiningServiceBAL objRejBAL = new ReJoiningServiceBAL();
            obj.IpAddress = clientIP;
            string successMsg = await Task.Run(() => objRejBAL.DeleteReJoiningServiceDetailsBAL(obj));
            return successMsg;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> UpdateReJoiningStatusDetails([FromBody]UpdateReJoiningServiceStatus obj)
        {
            ReJoiningServiceBAL objRejBAL = new ReJoiningServiceBAL();
            obj.IpAddress = clientIP;
            string successMsg = await Task.Run(() => objRejBAL.UpdateReJoiningServiceStatusBAL(obj));
            return successMsg;
        }
    }
}