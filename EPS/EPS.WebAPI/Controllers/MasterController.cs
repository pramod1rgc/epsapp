﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EPS.Repositories.EmployeeDetails;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EPS.WebAPI.Controllers
{
    /// <summary>
    /// Master Apis 
    /// </summary>
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [EPSExceptionFilters]
    public class MasterController : Controller
    {
      //  MasterBA ObjMasterBA = new MasterBA();
        private IHostingEnvironment _hostingEnvironment;
        private IHttpContextAccessor _accessor;
        private IMasterRepository _repository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessor"></param>
        /// <param name="hostingEnvironment"></param>
        /// <param name="repository"></param>
        public MasterController(IHttpContextAccessor accessor, IHostingEnvironment hostingEnvironment, IMasterRepository repository)
        {
            _hostingEnvironment = hostingEnvironment;
            _accessor = accessor;
            _repository = repository;
        }
      
        /// <summary>
        /// Get Relation
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRelation()
        {
            var mytask = Task.Run(() => _repository.GetRelation());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Joining Mode
        /// </summary>
        /// <param name="IsDeput"></param>
        /// <param name="MsEmpSubTypeID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetJoiningMode(Boolean? IsDeput,int? MsEmpSubTypeID)
        {
            var mytask = Task.Run(() => _repository.GetJoiningMode(IsDeput, MsEmpSubTypeID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Joining Category
        /// </summary>
        /// <param name="parentTypeId"></param>
        /// <param name="IsDeput"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetJoiningCategory(int parentTypeId, Boolean? IsDeput)
        {
            var mytask = Task.Run(() => _repository.GetJoiningCategory(parentTypeId,IsDeput));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Get Salutation
        /// </summary>
        /// <returns></returns>

        [HttpGet("[action]")]
        public async Task<IActionResult> GetSalutation()
        {
            var mytask = Task.Run(() => _repository.GetSalutation());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Employee SubType
        /// </summary>
        /// <param name="parentTypeId"></param>
        /// <param name="IsDeput"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpSubType(int parentTypeId, Boolean? IsDeput)
        {
            var mytask = Task.Run(() => _repository.GetEmpSubType(parentTypeId, IsDeput));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Employee Type
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="deputationId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEmpType(int? serviceId,int? deputationId)
        {
            var mytask = Task.Run(() => _repository.GetEmpType(serviceId, deputationId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Deputation Type
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDeputationType(int? serviceId)
        {
            var mytask = Task.Run(() => _repository.GetDeputationType(serviceId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Service Type
        /// </summary>
        /// <param name="IsDeput"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetServiceType(Boolean ? IsDeput)
        {
            var mytask = Task.Run(() => _repository.GetServiceType(IsDeput));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        //[HttpGet("[action]")]
        //public IEnumerable<GenderMaster> GetGender()
        //{
        //    try
        //    {
        //        return ObjMasterBA.GetGender();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Get State
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetState()
        {
            var mytask = Task.Run(() => _repository.GetState());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Pf Type
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPfType()
        {
            var mytask = Task.Run(() => _repository.GetPfType());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Joining Account
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetJoiningAccount()
        {
            var mytask = Task.Run(() => _repository.GetJoiningAccount());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Office List
        /// </summary>
        /// <param name="controllerId"></param>
        /// <param name="ddoId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOfficeList(int  controllerId, int ? ddoId)
        {
            var mytask = Task.Run(() => _repository.GetOfficeList(controllerId, ddoId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get Office City Class List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetOfficeCityClassList()
        {
            var mytask = Task.Run(() => _repository.GetOfficeCityClassList());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// Get PayCommission List
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayCommissionList()
        {
            var mytask = Task.Run(() => _repository.GetPayCommissionList());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        /// <summary>
        /// Upload File
        /// </summary>
        /// <returns></returns>
        [HttpPost("[action]")]
      
        public string uploadFiles()
        {
            //try
            //{
                 
                
                    var file = Request.Form.Files[0];
                    string fullPath = null;
                    string folderName = "Upload";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    if (file.Length > 0)
                    {
                        string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        fullPath = Path.Combine(newPath, fileName);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                    }
                    //return Json("Upload Successful.");
                    // return ObjEmployeeDetailsBA.InsertUpdateEmpDetails(ObjEmpDetails);
                    return fullPath;
                
               
          //  }
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
           
        }

    }
}
