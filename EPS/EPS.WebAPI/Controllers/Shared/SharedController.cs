﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessAccessLayers.Shared;
using EPS.BusinessModels.Shared;
using EPS.WebAPI.CustomFilters;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.Controllers.Shared
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [EPSExceptionFilters]
    [ApiController]
    public class SharedController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PermDdoId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<PayBillGroupModel> GetPayBillGroup(string PermDdoId,string Flag)
        {
            SharedBAL ObjSharedBAL = new SharedBAL();
            return ObjSharedBAL.GetPayBillGroup(PermDdoId, Flag);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PermDdoId"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        //[HttpGet("[action]")]
        //public IEnumerable<PayBillGroupModel> GetPayBillGroupByPermDDOId(string PermDdoId)
        //{
        //    SharedBAL ObjSharedBAL = new SharedBAL();
        //    return ObjSharedBAL.GetPayBillGroupByPermDDOId(PermDdoId);

        //}
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPayBillGroupByPermDDOId(string PermDdoId,string Flag)
        {
            SharedBAL ObjSharedBAL = new SharedBAL();
            var mytask = Task.Run(() => ObjSharedBAL.GetPayBillGroupByPermDDOId(PermDdoId, Flag));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billGroupId"></param>
        /// <param name="Flag"></param>
        /// 
        /// <returns></returns>
        //[HttpGet("[action]")]
        //public static IEnumerable<DesignationModel> GetDesignation(string billGroupId)
        //{
        //    SharedBAL ObjSharedBAL = new SharedBAL();
        //    return ObjSharedBAL.GetDesignation(billGroupId);

        //}
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDesignation(string billGroupId,string Flag)
        {
            SharedBAL ObjSharedBAL = new SharedBAL();
            var mytask = Task.Run(() => ObjSharedBAL.GetDesignation(billGroupId,Flag));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billGroupId"></param>
        /// <param name="PermDdoId"></param>
        /// <param name="Flag"></param>
        /// 
        /// <returns></returns>
        //[HttpGet("[action]")]
        //public IEnumerable<DesignationModel> GetDesignationByBillgrID(string billGroupId, string PermDdoId)
        //{
        //    SharedBAL ObjSharedBAL = new SharedBAL();
        //    return ObjSharedBAL.GetDesignationByBillgrID(billGroupId, PermDdoId);

        //}

        [HttpGet("[action]")]
        public async Task<IActionResult> GetDesignationByBillgrID(string billGroupId, string PermDdoId,string Flag)
        {
            SharedBAL ObjSharedBAL = new SharedBAL();
            var mytask = Task.Run(() => ObjSharedBAL.GetDesignationByBillgrID(billGroupId, PermDdoId, Flag));
            var result = await mytask;
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="desigId"></param>
        /// <param name="pageCode"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<EmpModel> GetEmp(string desigId, string pageCode)
        {
            SharedBAL ObjSharedBAL = new SharedBAL();
            return ObjSharedBAL.GetEmp(desigId, pageCode);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<DesignationModel> GetAllDesignation(string id)
        {

            SharedBAL ObjSharedBAL = new SharedBAL();
            return ObjSharedBAL.GetAllDesignation(id);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<DesignationModel> GetAllDesignationOfDepartment(string id)
        {

            SharedBAL ObjSharedBAL = new SharedBAL();
            return ObjSharedBAL.GetAllDesignationOfDepartment(id);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="suspendedEmployees"></param>
        /// <param name="controllerId"></param>
        /// <param name="DDOId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IEnumerable<EmpDesigModel> GetEmpWithDesignationDetails(bool suspendedEmployees, string controllerId, string DDOId)
        {

            SharedBAL ObjSharedBAL = new SharedBAL();
            return ObjSharedBAL.GetEmployeesWithDesignationDetails(suspendedEmployees, controllerId, DDOId);
        }
    }
}