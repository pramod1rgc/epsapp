﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;

namespace EPS.WebAPI.CustomFilters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {

            if (!actionContext.ModelState.IsValid)
            {

                actionContext.Result = new BadRequestObjectResult(actionContext.ModelState);


            }
        }
    }
}
