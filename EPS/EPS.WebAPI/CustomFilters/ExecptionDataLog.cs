﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPS.BusinessModels.ExceptionLog;
using EPS.DataAccessLayers.ExceptionLog;
using Microsoft.AspNetCore.Mvc.Filters;


namespace EPS.WebAPI.CustomFilters
{
    public class ExecptionDataLog
    {


        public void ExceptionDataLogs(ExceptionContext context)
        {
            ExceptionLogModel logger = new ExceptionLogModel();
            logger.Message = context.Exception.Message;
            logger.Level = "Error".ToString();
            logger.MachineName =  context.HttpContext.Request.Host.ToString()+ context.HttpContext.Request.PathBase.ToString();
            logger.URL = context.HttpContext.Request.Path.ToString();
            logger.Port = context.HttpContext.Request.Protocol.ToString();
            logger.Logger = "/Controller- "+ context.RouteData.Values["controller"].ToString()+"/Action- "+ context.RouteData.Values["Action"].ToString()+ "/Method- " + context.HttpContext.Request.Method.ToString();
            logger.Exception = context.Exception.TargetSite.ToString();
            logger.StackTrace = context.Exception.StackTrace;
            logger.Callsite = context.Exception.Source.ToString();
            logger.Logged = DateTime.Now;
            Log log = new Log();
            
            int i = log.ExceptionLogDataBase(logger);
            if (i <= 0) { log.ExceptionLogFile(logger); };
        }
    }
}
