﻿using System.Collections.Generic;
using EPS.BusinessModels.LoanApplication;
using EPS.DataAccessLayers;
namespace EPS.BusinessAccessLayers
{
    public class ExistingLoanBusinessAccessLayer
    {
        ExistingLoanDataAccessLayer objExistingloanBAL = new ExistingLoanDataAccessLayer();
      
        
        #region Bind Dropdownlist for Bill Group and Design and Emp Name     
        public IEnumerable<MstDesignLoanModel> BindDropDownDesign()
        {
            return objExistingloanBAL.BindDropDownDesign();
        }
        public IEnumerable<tblMsPayLoanRef> BindDropDownLoanType()
        {
            return objExistingloanBAL.BindLoanType();
        }
        public IEnumerable<TblMsPayBillGroup> BindDropDownBillGroup()
        {
            return objExistingloanBAL.BindDropDownBillgroup();
        }
        public IEnumerable<MsEmpLoanModel> GetAllEmployeeNameSelectedDesignId(string MsDesignID)
        {
            return objExistingloanBAL.GetAllEmployeeNameSelectedDesignationId(MsDesignID);
        }
        #endregion
    }
}
