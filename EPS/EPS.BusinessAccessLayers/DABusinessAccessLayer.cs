﻿using EPS.BusinessModels;
using EPS.DataAccessLayers;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessAccessLayers
{
   public class DABusinessAccessLayer
    {
        DADataAccess ObjDADataAccess = new DADataAccess();

        public IEnumerable<DAModel> DAGet()
        {
           return ObjDADataAccess.DAGet();
        }
        public void DAInsert(DAModel objDAModel)
        {

        }

        public void DAUpdate()
        {

        }
        public void DADelete()
        {

        }
    }
}
