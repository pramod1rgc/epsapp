﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
   public class UsersByDDOBusinessAccessLayer: IDDOCheckerRoleRepository
    {
        public UsersByDDODataAccessLayer ObjUsersByDDODataAccessLayer;
        public Database epsdatabase;

        public UsersByDDOBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjUsersByDDODataAccessLayer = new UsersByDDODataAccessLayer(epsdatabase);
        }
        
        public async Task<IEnumerable<EmployeeModel>> AssignedDDOCheckerEmployeeList(int DDOID)
        {
            return await Task.Run(() => ObjUsersByDDODataAccessLayer.AssignedDDOCheckerEmployeeList( DDOID)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<EmployeeModel>> EmployeeListByDDO(int DDOID)
        {
            return await Task.Run(() => ObjUsersByDDODataAccessLayer.EmployeeListByDDO(DDOID)).ConfigureAwait(true);
        }
        public async Task< string> AssignedCMD(string empCd,int RoleID, int DDOID, int UserID, string active, string Password)
        {
            return await Task.Run(() => ObjUsersByDDODataAccessLayer.AssignedCMD(empCd, RoleID, DDOID,UserID, active,  Password)).ConfigureAwait(true);
        }
        public async Task< string> SelfAssignDDOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            return await Task.Run(() => ObjUsersByDDODataAccessLayer.SelfAssignDDOChecker(UserID, username, EmpPermDDOId, ddoid,  IsAssign)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<AssignChecked>> CheckedSelfAssignDDO(string UserID)
        {
            return await Task.Run(() => ObjUsersByDDODataAccessLayer.CheckedSelfAssignDDO(UserID)).ConfigureAwait(true);
        }
    }
}
