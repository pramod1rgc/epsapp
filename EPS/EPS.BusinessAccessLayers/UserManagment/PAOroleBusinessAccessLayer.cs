﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
   public class PAOroleBusinessAccessLayer: IPAORoleRepository
    {
        #region PAO role Business Access Layer
        public PAOroleDataAccessLayer ObjPAOroleDataAccessLayer;
        public Database epsdatabase;
        public PAOroleBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjPAOroleDataAccessLayer = new PAOroleDataAccessLayer(epsdatabase);
        }
        
        public async Task< IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query)
        {
            return await Task.Run(() => ObjPAOroleDataAccessLayer.GetAllpaos(controllerID,  query)).ConfigureAwait(true); 
        }
       
        public async Task<IEnumerable<EmployeeModel>> GetAllDDOsUnderSelectedPAOs(string msPAOID)
        {
            return await Task.Run(() => ObjPAOroleDataAccessLayer.GetAllDDOsUnderSelectedPAOs( msPAOID)).ConfigureAwait(true);
        }

        public async Task<IEnumerable<EmployeeModel>> AssignedPAOEmp(string msPAOID)
        {
            return await Task.Run(() => ObjPAOroleDataAccessLayer.AssignedPAOEmp(msPAOID)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<EmployeeModel>> AssignedEmpList(string msPAOID)
        {
            return await Task.Run(() => ObjPAOroleDataAccessLayer.AssignedEmpList(msPAOID)).ConfigureAwait(true);
        }
        
        public async Task< string> Assigned(string empCd, int msPAOID, string active, string Password)
        {
            return await Task.Run(() => ObjPAOroleDataAccessLayer.Assigned(empCd, msPAOID, active, Password)).ConfigureAwait(true);
        }
        #endregion
    }
}
