﻿using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.UserManagment;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
   public class HOOBusinessAccessLayer: IHOORoleRepository
    {
        public HOODataAccessLayer ObjHOODataAccessLayer;
        public Database epsdatabase;
        public HOOBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjHOODataAccessLayer = new HOODataAccessLayer(epsdatabase);
        }
        
        public async Task<object> HooCheckerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => ObjHOODataAccessLayer.HooCheckerEmpList( DDOID));
             object result = await mytask;
            return result;
        }
        public async Task<object> hooMakerEmpList(int DDOID)
        {
            var mytask = Task.Run(() => ObjHOODataAccessLayer.hooMakerEmpList(DDOID));
            object result = await mytask;
            return result;
        }
        public async Task<string> AssignedHOOChecker(string empCd, int DDOID, string active, string Password)
        {
            var mytask = Task.Run(() => ObjHOODataAccessLayer.AssignedHOOChecker(empCd, DDOID, active, Password));
            string result = await mytask;
            return result;
        }
        public async Task<string> AssignedHOOMaker(string empCd, int DDOID, string active, string Password)
        {
            var mytask = Task.Run(() => ObjHOODataAccessLayer.AssignedHOOMaker(empCd, DDOID, active, Password));
            string result = await mytask;
            return result;
        }

        public async Task< string> SelfAssignedHOOChecker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            return await Task.Run(() => ObjHOODataAccessLayer.SelfAssignedHOOChecker(UserID, username, EmpPermDDOId, ddoid, IsAssign)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<AssignChecked>> SelfAssignHOOCheckerRole(string UserID)
        {
            return await Task.Run(() => ObjHOODataAccessLayer.SelfAssignHOOCheckerRole(UserID)).ConfigureAwait(true);
        }

    }
}
