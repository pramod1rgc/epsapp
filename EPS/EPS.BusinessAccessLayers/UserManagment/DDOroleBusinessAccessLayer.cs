﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace EPS.BusinessAccessLayers.UserManagment
{
    /// <summary>
    /// DDOroleBusinessAccessLayer
    /// </summary>
    public class DDOroleBusinessAccessLayer: IDDOAdminRepository
    {
        #region DDO role Business Access Layer
        public DDOroleDataAccessLayer ObjDDOroleDataAccessLayer;
        public Database epsdatabase;
        public DDOroleBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjDDOroleDataAccessLayer = new DDOroleDataAccessLayer(epsdatabase);
        }
       
        public async Task< IEnumerable<DropDownListModel>> GetAllDDO(string PAOID, string query)
        {
            return await Task.Run(()=> ObjDDOroleDataAccessLayer.GetAllDDO(PAOID, query)).ConfigureAwait(true);
        }
        
        public async Task< IEnumerable<EmployeeModel>> AssignedDDO(string DDOID)
        {
            return await Task.Run(()=> ObjDDOroleDataAccessLayer.AssignedDDO(DDOID)).ConfigureAwait(true);
        }
        public async Task< IEnumerable<EmployeeModel>> AssignedEmpDDO(string DDOID)
        {
            return await Task.Run(()=> ObjDDOroleDataAccessLayer.AssignedEmpDDO(DDOID)).ConfigureAwait(true);
        }
       
        public async Task< IEnumerable<EmployeeModel>> EmployeeList(string DDOID)
        {
            return await Task.Run(()=> ObjDDOroleDataAccessLayer.EmployeeList(DDOID)).ConfigureAwait(true);
        }
      
        public async Task< string> Assigned(string empCd, string DDOID, string active, string Password)
        {
            return  await Task.Run(()=> ObjDDOroleDataAccessLayer.Assigned(empCd, DDOID, active, Password)).ConfigureAwait(true);
        }

       
        #endregion
    }
}
