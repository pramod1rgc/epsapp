﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.UserManagment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
   public class OnBoardingBussinessAccessLayer
    {
        private Database epsDataBase;
        public OnBoardingDataAccessLayer ObjDAL;
        public OnBoardingBussinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjDAL = new OnBoardingDataAccessLayer(epsDataBase);
        }
       
        public async Task<int> OnBoardingSubmit(OnBoardingModel ObjOnBoardingModel)
        {
            var mytask = Task.Run(() => ObjDAL.OnBoardingSubmit(ObjOnBoardingModel));
            int result = await mytask;
            return result;
        }

        public List<DropDownListModel> GetAllReuestNoOfOnboarding()
        {
            return ObjDAL.GetAllReuestNoOfOnboarding();
        }
        public List<OnBoardingModel> FetchRequestLetterNoRecord(int OnboardingId)
        {
            return ObjDAL.FetchRequestLetterNoRecord(OnboardingId);
        }

        public  async Task<string> CancelRequestLetterNo(string remarks)
        {
            var mytask = Task.Run(() => ObjDAL.CancelRequestLetterNo(remarks));
            string result = await mytask;
            return result;
        }
    }
}
