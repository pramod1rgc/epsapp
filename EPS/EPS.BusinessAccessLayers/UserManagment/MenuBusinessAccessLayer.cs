﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers.UserManagment;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace EPS.BusinessAccessLayers
{
   public class MenuBusinessAccessLayer
    {
        #region Business Access Layers
        Database epsdatabase;
        MenuDataAccessLayer objmenuDataAccesslayer;
        /// <summary>
        /// menu Business Access Layer
        /// </summary>
        public MenuBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objmenuDataAccesslayer = new MenuDataAccessLayer(epsdatabase);
        }
        /// <summary>
        /// Bind Drop Down Menu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> BindDropDownMenu()
        {
            return objmenuDataAccesslayer.BindDropDownMenu();
        }
        /// <summary>
        /// Save Menu
        /// </summary>
        /// <param name="hdnMenuId"></param>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        public string SaveMenu(int hdnMenuId,string uRoleID, MstMenuModel mstMenu)
        {
            return objmenuDataAccesslayer.SaveMenu(hdnMenuId, uRoleID, mstMenu);
    
        }

        /// <summary>
        ///Bind Menu In Gride 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> BindMenuInGride()
        {
            return objmenuDataAccesslayer.BindMenuInGride();
  
        }
        /// <summary>
        ///Get Predefine Role
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MstMenuModel> GetPredefineRole()
        {
            return objmenuDataAccesslayer.GetPredefineRole();

        }
        /// <summary>
        ///Get Predefine Role
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<MstMenuModel> FillUpdateMenu(string MenuId)
        //{
        //    return objmenuDataAccesslayer.FillUpdateMenu(MenuId);

        //}
        public async Task<object> FillUpdateMenu(string MenuId)
        {
            var mytask = Task.Run(() => objmenuDataAccesslayer.FillUpdateMenu(MenuId));
            object result = await mytask;
            return result;
        }

        /// <summary>
        /// Active Deactive Menu
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        public string ActiveDeactiveMenu(MstMenuModel mstMenu)
        {
            return objmenuDataAccesslayer.ActiveDeactiveMenu(mstMenu);
        }
        #endregion
    }


}
