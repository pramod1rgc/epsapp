﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
   public class ControllerroleBusinessAccessLayer : IControllerRoleRepository
    {
        #region Controller Business Access Layer
        public ControllerroleDataAccessLayer ObjControllerroleDataAccessLayer;
        public Database epsdatabase;
        
        /// <summary>
        /// Controller role Business Access Layer
        /// </summary>
        public ControllerroleBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjControllerroleDataAccessLayer = new ControllerroleDataAccessLayer(epsdatabase);
        }

        /// <summary>
        /// Assigned Emp
        /// </summary>
        /// <param name="msControllerID"></param>
        /// <returns></returns>
        public async Task<List<EmployeeModel>> AssignedEmp(string msControllerID)
        {
            return  await Task.Run(() => ObjControllerroleDataAccessLayer.AssignedEmp(msControllerID)).ConfigureAwait(true);
           
        }

        /// <summary>
        /// Get All Controllers
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task< List<DropDownListModel>> GetAllControllers(string controllerID, string query)
        {
            return await Task.Run(()=> ObjControllerroleDataAccessLayer.GetAllControllers(controllerID, query)).ConfigureAwait(true);
        }
        /// <summary>
        /// Get All paos
        /// </summary>
        /// <param name="controllerID"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task< IEnumerable<DropDownListModel>> GetAllpaos(string controllerID, string query)
        {
            return await Task.Run(()=> ObjControllerroleDataAccessLayer.GetAllpaos(controllerID, query)).ConfigureAwait(true);
        }

        /// <summary>
        /// Controller role Select changed
        /// </summary>
        /// <param name="msControllerID"></param>
        /// <returns></returns>
        public async Task< IEnumerable<EmployeeModel>> ControllerroleSelectchanged(string msControllerID, string PAOID, string DDOID)
        {
            return  await Task.Run(()=> ObjControllerroleDataAccessLayer.ControllerroleSelectchanged(msControllerID,  PAOID,  DDOID)).ConfigureAwait(true);
        }

        /// <summary>
        /// Paos Assigned
        /// </summary>
        /// <param name="empCd"></param>
        /// <param name="msControllerID"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public  async Task< string> ControllerAssigned(string empCd,int msControllerID,string active,string Password)
        {
            return await Task.Run(()=> ObjControllerroleDataAccessLayer.ControllerAssigned(empCd, msControllerID, active, Password)).ConfigureAwait(true);
        }

        /// <summary>
        /// Get All User Roles
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task< IEnumerable<UserType>> GetAllUserRoles(string username)
        {
            return await Task.Run(()=> ObjControllerroleDataAccessLayer.GetAllUserRoles(username)).ConfigureAwait(true);
        }

        
        #endregion

    }
}
