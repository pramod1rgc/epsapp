﻿using System.Collections.Generic;
using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers.UserManagment;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace EPS.BusinessAccessLayers
{
    public class RoleBusinessAccessLayer
    {

        Database epsdatabase;
        RoleDataAccessLayer RoleDataAccessLayer;
        public RoleBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            RoleDataAccessLayer = new RoleDataAccessLayer(epsdatabase);
        }
        /// <summary>
        /// Create New Role
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        public string CreateNewRole(MstRole mstRole)
        {
            return RoleDataAccessLayer.CreateNewRole(mstRole);
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAlRoles(string username)
        {
            return RoleDataAccessLayer.GetAlRoles(username);
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAllActiveRollList(string username)
        {
            return RoleDataAccessLayer.GetAllActiveRollList(username);
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetAllCustomeRollList(string username)
        {
            return RoleDataAccessLayer.GetAllCustomeRollList(username);
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleIDS> GetBindUserListddl(string RdStatus, string LoggRollID,string LoogInUser,string ControleerID, string PAOID, string DDOID,string CurrUserControllerID)
        {
            return RoleDataAccessLayer.GetBindUserListddl(RdStatus, LoggRollID, LoogInUser, ControleerID, PAOID, DDOID, CurrUserControllerID);
        }
        /// <summary>
        /// Get User Details
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public IEnumerable<MstRole> GetUserDetails(MstRole userRole)
        {
            return RoleDataAccessLayer.GetUserDetails(userRole);
        }
        /// <summary>
        /// Revoke User
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        public string RevokeUser(MstRole mstRole)
        {
            return RoleDataAccessLayer.RevokeUser(mstRole);
        }
        /// <summary>
        /// Get User Details By Pan
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public IEnumerable<MstRole> GetUserDetailsByPan(MstRole userRole)
        {
            return RoleDataAccessLayer.GetUserDetailsByPan(userRole);
        }
        /// <summary>
        /// Save Assign Role For User
        /// </summary>
        /// <param name="mstRole"></param>
        /// <returns></returns>
        public string SaveAssignRoleForUser(MstRole mstRole)
        {
            return RoleDataAccessLayer.SaveAssignRoleForUser(mstRole);
        }
        /// <summary>
        /// Get All Menu List
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public List<MstRole> GetAllMenuList(string roleID, string Uname,string CurrURole)
        {

            return RoleDataAccessLayer.GetAllMenuList(roleID, Uname, CurrURole);
        }
        /// <summary>
        /// Get Assign Role For User
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public IEnumerable<MstRole> GetAssignRoleForUser(string roleID)
        {

            return RoleDataAccessLayer.GetAssignRoleForUser(roleID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        public string SaveMenuPerMission(string[] mstMenu)
        {
            return RoleDataAccessLayer.SaveMenuPerMission(mstMenu);
        }
        /// <summary>
        /// Check Menu PerMission
        /// </summary>
        /// <param name="mstMenu"></param>
        /// <returns></returns>
        public string CheckMenuPerMission(string[] mstMenu)
        {
            return RoleDataAccessLayer.CheckMenuPerMission(mstMenu);
        }
    }
}
