﻿using EPS.BusinessModels;
using EPS.BusinessModels.UserManagment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.UserManagment;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserManagment
{
    public class MakerBusinessAccessLayer: IDDOMakerRoleRepository
    {
        public MakerDataAccessLayer ObjMakerDataAccessLayer;
        public Database epsdatabase;
        public MakerBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjMakerDataAccessLayer = new MakerDataAccessLayer(epsdatabase);
        }
        
        public async Task<IEnumerable<EmployeeModel>> AssignedmakerEmpList(int DDOID)
        {
            return await Task.Run(() => ObjMakerDataAccessLayer.AssignedmakerEmpList(DDOID)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<EmployeeModel>> makerEmpList(int DDOID)
        {
            return await Task.Run(() => ObjMakerDataAccessLayer.makerEmpList(DDOID)).ConfigureAwait(true);
        }
        public async Task<string> AssignedMaker(string empCd, int DDOID, int UserID, string active, string Password)
        {
            return await Task.Run(() => ObjMakerDataAccessLayer.AssignedMaker(empCd, DDOID, UserID, active, Password)).ConfigureAwait(true);
        }
        public async Task<string> SelfAssignedDDOMaker(int UserID, string username, int EmpPermDDOId, int ddoid, string IsAssign)
        {
            return await Task.Run(() => ObjMakerDataAccessLayer.SelfAssignedDDOMaker(UserID, username, EmpPermDDOId, ddoid, IsAssign)).ConfigureAwait(true);
        }
        public async Task<IEnumerable<AssignChecked>> SelfAssignMakerRole(string UserID)
        {
            return await Task.Run(() => ObjMakerDataAccessLayer.SelfAssignMakerRole(UserID)).ConfigureAwait(true);
        }
    }
}
