﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class ServiceDetailsBA: IServiceDetailsRepository
    {
        private Database epsdatabase;
        public ServiceDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<ServiceDetailsModel>> GetMaintainByOfc()
        {
            ServiceDetailsDA objServiceDetailsDA = new ServiceDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objServiceDetailsDA.GetMaintainByOfc());
            var result = await myTask;
            return result;

        }

        #region For Get All Service Details
        public async Task<ServiceDetailsModel> GetAllServiceDetails(string empCode, int roleId)
        {
            ServiceDetailsDA objServiceDetailsDA = new ServiceDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objServiceDetailsDA.GetAllServiceDetails(empCode, roleId));
            var result = await myTask;
            return result;

        }
        #endregion
        #region Save service Details
        /// <summary>
        /// Insert Update Service Details
        /// </summary>
        /// <param name="objPostingDetailsModel"></param>
        /// <returns></returns>
        public async Task<int> SaveUpdateServiceDetails(ServiceDetailsModel objServiceDetailsModel)
        {
            ServiceDetailsDA objServiceDetailsDA = new ServiceDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objServiceDetailsDA.SaveUpdateServiceDetails(objServiceDetailsModel));
            int result = await myTask;
            return result;
        }

        #endregion

    }
}
