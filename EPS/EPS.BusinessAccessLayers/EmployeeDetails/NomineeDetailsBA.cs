﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
   public class NomineeDetailsBA: INomineeRepository
    {
        private Database epsdatabase;
        public NomineeDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        #region getNomineeDetails
        public async Task<NomineeModel> GetNomineeDetails(string empCd, int nomineeID)
        {
            NomineeDetailsDA objNomineeDetailsDA = new NomineeDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objNomineeDetailsDA.GetNomineeDetails(empCd, nomineeID));
            NomineeModel _NomineeModel = await myTask;
            return _NomineeModel;


        }
        #endregion


        #region getAllNomineeDetails
        public async Task<IEnumerable<NomineeModel>> GetAllNomineeDetails(string empcode ,int roleID)
        {
            NomineeDetailsDA objNomineeDetailsDA = new NomineeDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objNomineeDetailsDA.GetAllNomineeDetails(empcode, roleID));
            var result = await myTask;
            return result;

        }
        #endregion


        #region UpdateNomineeDetails
        public async Task<int> UpdateNomineeDetails(NomineeModel objNomineeDetails)
        {
            NomineeDetailsDA objNomineeDetailsDA = new NomineeDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objNomineeDetailsDA.UpdateNomineeDetails(objNomineeDetails));
            int result = await myTask;
            return result;
        }

        #endregion


        #region DeleteNomineeDetails
        public async Task<int> DeleteNomineeDetails(string empCd, int nomineeID)
        {
            NomineeDetailsDA objNomineeDetailsDA = new NomineeDetailsDA(epsdatabase);
            var mytask = Task.Run(() => objNomineeDetailsDA.DeleteNomineeDetails(empCd, nomineeID));
            var result = await mytask;
            return result;
        }
        #endregion

    }
}
