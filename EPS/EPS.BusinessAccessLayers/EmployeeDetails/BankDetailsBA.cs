﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class BankDetailsBA:IBankDetailsRepository
    {
        private Database epsdatabase;

        public BankDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        #region GetBankDetailsByIFSC
        public async Task<BankDetailsModel> GetBankDetailsByIFSC(string ifscCD)
        {
            BankDetailsDA objBankDetailsDA = new BankDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objBankDetailsDA.GetBankDetailsByIFSC(ifscCD));
            BankDetailsModel _BankDetailsModel = await myTask;
            return _BankDetailsModel;
        }
        #endregion




        #region Get All IFSC
        public IEnumerable<BankDetailsModel> GetAllIFSC()
        {
            BankDetailsDA objBankDetailsDA = new BankDetailsDA(epsdatabase);
            return objBankDetailsDA.GetAllIFSC();
        }
        #endregion




        #region GetBankDetails
        public async Task<BankDetailsModel> GetBankDetails(string empCD, int roleId)
        {
            BankDetailsDA objBankDetailsDA = new BankDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objBankDetailsDA.GetBankDetails(empCD, roleId));
            BankDetailsModel _BankDetailsModel = await myTask;
            return _BankDetailsModel;


        }
        #endregion

        #region UpdateBankDetails
        public async Task<int> UpdateBankDetails(BankDetailsModel objBankDetails)
        {
            BankDetailsDA objBankDetailsDA = new BankDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objBankDetailsDA.UpdateBankDetails(objBankDetails));
            int result = await myTask;
            return result;
        }

       
        #endregion
    }
}
