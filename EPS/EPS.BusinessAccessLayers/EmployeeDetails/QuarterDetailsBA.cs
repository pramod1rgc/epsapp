﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class QuarterDetailsBA: IQuaterDetailsRepository
    {
       private Database epsdatabase;
        public QuarterDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<QuarterDetailsModel>> GetQuarterOwnedby()
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.GetQuarterOwnedby());
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<QuarterDetailsModel>> GetAllottedTo()
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.GetAllottedTo());
            var result = await myTask;
            return result;

        }



        #region Get rent Status

        public async Task<IEnumerable<QuarterDetailsModel>> GetRentStatus()
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.GetRentStatus());
            var result = await myTask;
            return result;

        }

        #endregion


        #region Get QuarterType

        public async Task<IEnumerable<QuarterDetailsModel>> GetQuarterType()
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.GetQuarterType());
            var result = await myTask;
            return result;

        }

        #endregion


        #region Get Custodian

        public async Task<IEnumerable<QuarterDetailsModel>> GetCustodian(int qrtrOwnedBy)
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.GetCustodian(qrtrOwnedBy));
            var result = await myTask;
            return result;

        }

        #endregion

        #region Get GPRACityLocation

        public async Task<IEnumerable<QuarterDetailsModel>> GetGPRACityLocation()
        {
            QuarterDetailsDA objNomineeDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objNomineeDetailsDA.GetGPRACityLocation());
            var result = await myTask;
            return result;

        }

        #endregion


        #region get QuarterAllDetails

        public async Task<IEnumerable<QuarterDetailsModel>> QuarterAllDetails(string empCd,int roleId)
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objQuarterDetailsDA.QuarterAllDetails(empCd, roleId));
            var result = await myTask;
            return result;
             
        }
        #endregion


        #region get Quarter Details

        public async Task<QuarterDetailsModel> QuarterDetails(string empCd, int mSEmpAccmID)
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objQuarterDetailsDA.QuarterDetails(empCd, mSEmpAccmID));
            QuarterDetailsModel _QuarterAllDetails = await myTask;
            return _QuarterAllDetails;


        }
        #endregion

        #region SaveUpdateQuarterDetails
        public async Task<int> SaveUpdateQuarterDetails(QuarterDetailsModel objQuarterDetailsModel)
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objQuarterDetailsDA.SaveUpdateQuarterDetails(objQuarterDetailsModel));
            int result = await myTask;
            return result;
        }

        #endregion

        #region  DeleteQuarterDetails
        public async Task<int> DeleteQuarterDetails(string empCd, int mSEmpAccmID)
        {
            QuarterDetailsDA objQuarterDetailsDA = new QuarterDetailsDA(epsdatabase);
            var mytask = Task.Run(() => objQuarterDetailsDA.DeleteQuarterDetails(empCd, mSEmpAccmID));
            var result = await mytask;
            return result;
        }
        #endregion
    }
}
