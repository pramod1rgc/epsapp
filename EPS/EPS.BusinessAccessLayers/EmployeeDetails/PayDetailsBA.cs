﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace EPS.BusinessAccessLayers.EmployeeDetails
{
   public class PayDetailsBA : IPayDetailsRepository
    {

       private Database epsdatabase;
        public PayDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        #region For Get  Organisation Type
        public async Task<IEnumerable<PayDetailsModel>> GetOrganisationType(string empCode)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetOrganisationType(empCode));
            var result = await myTask;
            return result;

        }
        #endregion


        #region For Get Pay Level
        public async Task<IEnumerable<PayDetailsModel>> GetPayLevel()
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetPayLevel());
            var result = await myTask;
            return result;

        }
        #endregion




        #region For Get Entitle Office vehicle
        public async Task<IEnumerable<PayDetailsModel>> GetEntitledOffVeh(string module)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetEntitledOffVeh(module));
            var result = await myTask;
            return result;

        }
        #endregion
        #region For Get Pay Index
        public async Task<IEnumerable<PayDetailsModel>>GetPayIndex(string payLevel)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetPayIndex(payLevel));
            var result = await myTask;
            return result;

        }
        #endregion
        
         

        #region For Get Basic Details
        public async Task<IEnumerable<PayDetailsModel>> GetBasicDetails(string payLevel ,int payIndex)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetBasicDetails(payLevel, payIndex));
            var result = await myTask;
            return result;

        }
        #endregion


        #region For Get Pay Index
        public async Task<IEnumerable<PayDetailsModel>> GetGradePay()
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetGradePay());
            var result = await myTask;
            return result;

        }
        #endregion

        

        #region For Get Pay Scale
        public async Task<IEnumerable<PayDetailsModel>> GetPayScale(int gradePay)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetPayScale(gradePay));
            var result = await myTask;
            return result;

        }
        #endregion

        #region For Get All Non Computational Dues
        public async Task<IEnumerable<NonComputationalDues>> GetNonComputationalDues()
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetNonComputationalDues());
            var result = await myTask;
            return result;

        }
        #endregion


        #region For Get All NonC omputational Deductions
        public async Task<IEnumerable<NonComputationalDeduction>> GetNonComputationalDeductions()
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetNonComputationalDeductions());
            var result = await myTask;
            return result;

        }
        #endregion

        


        #region For Get Pay Details
        public async Task<PayDetailsModel> GetPayDetails(string empCode,int roleId)
        {
            PayDetailsDA objDA = new PayDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetPayDetails(empCode, roleId));
            var result = await myTask;
            return result;

        }
        #endregion



        /// <summary>
        /// Insert Update posting Details
        /// </summary>
        /// <param name="objPostingDetailsModel"></param> 
        /// <returns></returns>
        public async Task<int> SaveUpdatePayDetails(PayDetailsModel objPayDetailsModel)
        {
            PayDetailsDA objPayDetailsDA = new PayDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPayDetailsDA.SaveUpdatePayDetails(objPayDetailsModel));
            int result = await myTask;
            return result;
        }


    }
}
