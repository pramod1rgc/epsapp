﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class ForwardToCheckerBA
    {
       private Database epsdatabase;
        public ForwardToCheckerBA()
        {
             DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region GetAllEmpDetails

        public async Task<List<ForwardToCheckerModel>> GetAllEmpDetails(string empCd, int roleId)
        {
            ForwardToCheckerDA objDA = new ForwardToCheckerDA(epsdatabase);
            var myTask = Task.Run(() => objDA.GetAllEmpDetails(empCd, roleId));
            List<ForwardToCheckerModel> result = await myTask;

            return result;

        }
        #endregion

        
        #region GetAllEmpDetails

        public async Task<object[]> GetAllEmployeeCompleteDetails(int roleId,string empCode)
        {
            ForwardToCheckerDA objDA = new ForwardToCheckerDA(epsdatabase);
            var myTask = Task.Run(() => objDA.GetAllEmployeeCompleteDetails(roleId, empCode));
            var  result = await myTask;
           return result;

        }
        #endregion

        #region ForwardToCheckerEmpDetails
        public async Task<int> ForwardToCheckerEmpDetails(ForwardToCheckerModel objForwardToCheckerModel)
        {
            ForwardToCheckerDA objDA = new ForwardToCheckerDA(epsdatabase);
            var myTask = Task.Run(() => objDA.ForwardToCheckerEmpDetails(objForwardToCheckerModel));
            int result = await myTask;
            return result;
        }

        #endregion

        #region VerifyEmpData

        public async Task<int> VerifyEmpData(ForwardToCheckerModel objForwardToCheckerModel)
        {
            ForwardToCheckerDA objDA = new ForwardToCheckerDA(epsdatabase);
            var myTask = Task.Run(() => objDA.VerifyEmpData(objForwardToCheckerModel));
            int result = await myTask;
            return result;
        }
        #endregion
    }
}
