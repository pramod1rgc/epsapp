﻿using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using EPS.BusinessModels;
using System.Collections.Generic;
using EPS.DataAccessLayers;
using System.Threading.Tasks;
using EPS.Repositories.EmployeeDetails;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class EmployeeDetailsBA : IEmployeeDetailsRepository
    {
      private  Database epsDataBase;
        public EmployeeDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        //============================Get Verify EmpCode===================================== 
        public async Task<List<EmployeeDetailsModel>> GetVerifyEmpCode()
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.GetVerifyEmpCode());
            List<EmployeeDetailsModel> result = await mytask;
            return result;

        }
        //============================Get Physical Disability Types===================================== 
        public async Task<IEnumerable<EmployeeDetailsModel>> GetPhysicalDisabilityTypes()
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.GetPhysicalDisabilityTypes());
            IEnumerable<EmployeeDetailsModel> result = await mytask;
            return result;
        }

        //================Get personal Details========================================         
        public async Task<EmployeeDetailsModel> GetEmpPersonalDetailsByID(string EmpCd,int RoleId)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.GetEmpPersonalDetailsByID(EmpCd, RoleId));
            EmployeeDetailsModel result = await mytask;
            return result;
        }
         
        //================Get Employee physical Details========================================   

        public async Task<List<EmployeeDetailsModel>> GetEmpPHDetails(string EmpCd)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.GetEmpPHDetails(EmpCd));
            List<EmployeeDetailsModel> result = await mytask;
            return result;
        }

        //================Get Employee List========================================   
        public async Task<IEnumerable<EmployeeDetailsModel>> MakerGetEmpList(int RoleId)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.MakerGetEmpList(RoleId));
            IEnumerable<EmployeeDetailsModel> result = await mytask;
            return result;

        }
        //================Update Employee Details========================================   
        public async Task<int> UpdateEmpDetails(EmployeeDetailsModel ObjEmpDetails)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.UpdateEmpDetails(ObjEmpDetails));
            int result = await mytask;
            return result;
        }
        //================Insert Physical Disability========================================   
        public async Task<int> SavePHDetails(EmployeeDetailsModel ObjEmpDetails)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.SavePHDetails(ObjEmpDetails));
            int result = await mytask;
            return result;

        }
        //================Insert Employee Details========================================   
        public async Task<int> InsertUpdateEmpDetails(EmployeeDetailsModel ObjEmpDetails)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.InsertUpdateEmpDetails(ObjEmpDetails));
            int result = await mytask;
            return result;

        }
        //================Delete Employee Details========================================   
        public async Task<int> DeleteEmployeeDetails(string EmpCd)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.DeleteEmployeeDetails(EmpCd));
            int result = await mytask;
            return result;
        }
        //================Get deputational Employee========================================   
        public async Task<bool> GetIsDeputEmp(string EmpCd)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.GetIsDeputEmp(EmpCd));
            bool result = await mytask;
            return result;
        }
        //================Verify and Reject Employee========================================   
        public async Task<int> VerifyRejectionEmployeeDtl(EmployeeDetailsModel ObjEmpDetails)
        {
            EmployeeDetailsDA objEmployeeDetailsDA = new EmployeeDetailsDA(epsDataBase);
            var mytask = Task.Run(() => objEmployeeDetailsDA.VerifyRejectionEmployeeDtl(ObjEmpDetails));
            int result = await mytask;
            return result;

        }

    }
}
