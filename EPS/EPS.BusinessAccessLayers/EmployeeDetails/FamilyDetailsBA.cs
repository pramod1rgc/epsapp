﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{


    public class FamilyDetailsBA: IFamilyRepository
    {

        private Database epsdatabase;
        public FamilyDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        #region GetMaritalStatus
        public async Task<List<FamilyDetailsModel>> GetMaritalStatus()
        {
            FamilyDetailsDA objFamilyDetailsDA = new FamilyDetailsDA(epsdatabase);
            var mytask = Task.Run(() => objFamilyDetailsDA.GetMaritalStatus());
            List<FamilyDetailsModel> result = await mytask;
            return result;
        }

        #endregion


        #region GetFamilyDetails
        public async Task<FamilyDetailsModel> GetFamilyDetails(string empCd, int msDependentDetailID)
        {
            FamilyDetailsDA objFamilyDetailsDA = new FamilyDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objFamilyDetailsDA.GetFamilyDetails(empCd, msDependentDetailID));
            FamilyDetailsModel _FamilyDetailsModel = await myTask;
            return _FamilyDetailsModel;
        }

        #endregion


        #region GetAllFamilyDetails
        public async Task<List<FamilyDetailsModel>> GetAllFamilyDetails(string empCd, int roleId)
        {
            FamilyDetailsDA objFamilyDetailsDA = new FamilyDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objFamilyDetailsDA.GetAllFamilyDetails(empCd, roleId));
            List<FamilyDetailsModel> result = await myTask;

            return result;

        }

        #endregion


        #region UpdateEmpFamilyDetails
        public async Task<int> UpdateEmpFamilyDetails(FamilyDetailsModel objFamilyDetails)
        {
            FamilyDetailsDA objFamilyDetailsDA = new FamilyDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objFamilyDetailsDA.UpdateEmpFamilyDetails(objFamilyDetails));
            int result = await myTask;
            return result;
        }
        #endregion



        #region DeleteFamilyDetails
        public async Task<int> DeleteFamilyDetails(string empCd, int msDependentDetailID)
        {
            FamilyDetailsDA objFamilyDetailsDA = new FamilyDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objFamilyDetailsDA.DeleteFamilyDetails(empCd, msDependentDetailID));
            int result = await myTask;
            return result;
        }
        #endregion

    }


}
