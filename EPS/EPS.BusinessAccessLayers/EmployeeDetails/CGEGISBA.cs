﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class CGEGISBA: ICGEGISRepository
    {

        private Database epsdatabase;
        public CGEGISBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        #region For Get Insurance Applicable
        public async Task<IEnumerable<CGEGISModel>> GetInsuranceApplicable(string EmpCode)
        {
            CGEGISDA objDA = new CGEGISDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetInsuranceApplicable(EmpCode));
            var result = await myTask;
            return result;

        }
        #endregion




        #region For Get  Deputation Case Type
        public async Task<IEnumerable<CGEGISModel>> GetCGEGISCategory(int insuranceApplicableId, string EmpCode)
        {
            CGEGISDA objDA = new CGEGISDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetCGEGISCategory(insuranceApplicableId, EmpCode));
            var result = await myTask;
            return result;

        }
        #endregion



        #region For    Insert and Update CGEGIS/GIS Details
        public async Task<int> SaveUpdateCGEGISDetails(CGEGISModel objCGEGISModel)
        {
            CGEGISDA objDA = new CGEGISDA(epsdatabase);
            var myTask = Task.Run(() => objDA.SaveUpdateCGEGISDetails(objCGEGISModel));
            int result = await myTask;
            return result;
        }
        #endregion


        public async Task<CGEGISModel> GetCGEGISDetails(string EmpCode, int RoleId)
        {
            CGEGISDA objDA = new CGEGISDA(epsdatabase);

            var myTask = Task.Run(() => objDA.GetCGEGISDetails(EmpCode, RoleId));
            CGEGISModel _CGEGISModel = await myTask;
            return _CGEGISModel;


        }
    }
}
