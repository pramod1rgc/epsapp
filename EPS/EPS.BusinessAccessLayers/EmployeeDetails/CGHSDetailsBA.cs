﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class CGHSDetailsBA: ICGHSRepository
    {

        private Database epsdatabase;
        public CGHSDetailsBA()
        {
             DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region For    Insert and Update CGEGIS/GIS Details

        public async Task<int> SaveUpdateCGHSDetails(CGHSModel objCGHSModel)
        {
            CGHSDetailsDA objDA = new CGHSDetailsDA(epsdatabase);

            var myTask = Task.Run(() => objDA.SaveUpdateCGHSDetails(objCGHSModel));
            int result = await myTask;
            return result;
        }
        #endregion


        #region For    Insert and Update CGEGIS/GIS Details
        public async Task<CGHSModel> GetCGHSDetails(string empCd,int roleId)
        {
            CGHSDetailsDA objDA = new CGHSDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objDA.GetCGHSDetails(empCd, roleId));
            CGHSModel _CGEGISModel = await myTask;
            return _CGEGISModel;


        }
        #endregion

    }
}
