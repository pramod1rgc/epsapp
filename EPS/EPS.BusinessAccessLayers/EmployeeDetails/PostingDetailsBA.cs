﻿using EPS.BusinessModels.EmployeeDetails;
using EPS.CommonClasses;
using EPS.DataAccessLayers.EmployeeDetails;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.EmployeeDetails
{
    public class PostingDetailsBA : IPostingDetailsRepository
    {
        private Database epsdatabase;
        public PostingDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        public async Task<IEnumerable<PostingDetailsModel>> GetHRACity()
        {
            PostingDetailsDA objPostingDetailsDA = new PostingDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPostingDetailsDA.GetHRACity());
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<PostingDetailsModel>> GetTACity()
        {
            PostingDetailsDA objPostingDetailsDA = new PostingDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPostingDetailsDA.GetTACity());
            var result = await myTask;
            return result;

        }


        public async Task<IEnumerable<PostingDetailsModel>> GetAllDesignation()
        {
            PostingDetailsDA objPostingDetailsDA = new PostingDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPostingDetailsDA.GetAllDesignation());
            var result = await myTask;
            return result;

        }
        /// <summary>
        /// Insert Update posting Details
        /// </summary>
        /// <param name="objPostingDetailsModel"></param>
        /// <returns></returns>
        public async Task<int> SaveUpdatePostingDetails(PostingDetailsModel objPostingDetailsModel)
        {
            PostingDetailsDA objPostingDetailsDA = new PostingDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPostingDetailsDA.SaveUpdatePostingDetails(objPostingDetailsModel));
            int result = await myTask;
            return result;
        }

        public async Task<PostingDetailsModel> GetAllPostingDetails(string Empcd, int roleId)
        {
            PostingDetailsDA objPostingDetailsDA = new PostingDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objPostingDetailsDA.GetAllPostingDetails(Empcd, roleId));
            var result = await myTask;
            return result;

        }
    }
}
