﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Increment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Increment;

namespace EPS.BusinessAccessLayers.Increment
{
   public class StopIncrement_BAL:IStopIncRepository
    {
        Database epsDatabase;
        StopIncrement_DAL objStopDAL;
        public StopIncrement_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objStopDAL = new StopIncrement_DAL(epsDatabase);
        }

        #region Get Employee For Stop Increment On behalf of Order No

        public async Task<List<RegIncrement_Model>> GetEmployeeStopIncrement(int orderID)
        {
            return await Task.Run(() => objStopDAL.GetEmployeeStopIncrement(orderID));
        }
        #endregion

        #region Get Employee who is not the part of Stop Increment On behalf of Order No

        public async Task<List<RegIncrement_Model>> GetEmployeeForNotStop(int orderID)
        {
            return await Task.Run(() => objStopDAL.GetEmployeeForNotStop(orderID));
        }
        #endregion

        #region Insert Stop Increment Data

        public async Task<string> InsertStopIncrementData(object[] advModel)
        {
            var mytask = Task.Run(() => objStopDAL.InsertStopIncrementData(advModel));
            string result = await mytask;
            return result;
        }

        #endregion

        #region Forward to ddo checker Stop Increment Data
        public async Task<string> ForwardcheckerForStopInc(object[]  advModel) //RegIncrement_Model
        {
            var mytask = Task.Run(() => objStopDAL.ForwardcheckerForStopInc(advModel));
            string result = await mytask;
            return result;
        }
        #endregion
    }
}
