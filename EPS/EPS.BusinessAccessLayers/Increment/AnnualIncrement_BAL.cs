﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Increment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using EPS.Repositories.Increment;
using System;

namespace EPS.BusinessAccessLayers.Increment
{
   public class AnnualIncrement_BAL: IAnnualIncRepository
    {
        Database epsDatabase;
        private bool disposed = false;
        AnnualIncrement_DAL objAnnualDAL;
        public AnnualIncrement_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objAnnualDAL = new AnnualIncrement_DAL(epsDatabase);
        }

        #region Get Salary Month
        public IEnumerable<AdvIncrement_Model> GetSalaryMonth()
        {
            return objAnnualDAL.GetSalaryMonth();
        }
        #endregion

        #region Get Employee With Order No

        public IEnumerable<AdvIncrement_Model> GetOrderWithEmployee(string paycodeID)
        {
            return objAnnualDAL.GetOrderWithEmployee(paycodeID);
        }
        #endregion

        #region Get Employee Report Data

        public IEnumerable<AdvIncrement_Model> GetAnnualIncReportData(string paycodeID, string orderID)
        {
            return objAnnualDAL.GetAnnualIncReportData(paycodeID, orderID);
        }
        #endregion


        #region Get Employee due for increment report data

        public IEnumerable<AdvIncrement_Model> GetEmpDueForIncReport(string designCode)
        {
            return objAnnualDAL.GetEmpDueForIncReport(designCode);
        }
        #endregion


        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~AnnualIncrement_BAL()
        {
            Dispose(false);
        }
        #endregion



    }
}
