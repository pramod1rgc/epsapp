﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Increment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Increment;
using System;

namespace EPS.BusinessAccessLayers.Increment
{
    public class RegularIncrement_BAL:IRegIncrementRepository
    {
        Database epsDatabase;
        private bool disposed = false;
        RegularIncrement_DAL objRegDAL;
        public RegularIncrement_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objRegDAL = new RegularIncrement_DAL(epsDatabase);
        }

        public async Task<List<RegIncrement_Model>> GetEmployeeDesignation(string PermDdoId)
        {
            return await Task.Run(() => objRegDAL.GetEmployeeDesignation(PermDdoId));
        }


        #region Create Advance Increment Data

        public async Task<string> CreateOrderNo(RegIncrement_Model regModel)
        {
            return await Task.Run(() => objRegDAL.CreateOrderNo(regModel));
        }
        #endregion

        #region Get Employee With Order No

        public async Task<List<RegIncrement_Model>> GetOrderDetails(string designID, string paycodeID, string orderType)
        {
            return await Task.Run(() => objRegDAL.GetOrderDetails(designID, paycodeID, orderType));
        }
        #endregion

        #region Get Employee For Increment On behalf of Order No

        public IEnumerable<RegIncrement_Model> GetEmployeeForIncrement(string designID, string paycodeID, int orderID)
        {
            return objRegDAL.GetEmployeeForIncrement(designID, paycodeID, orderID);
        }
        #endregion

        #region Get Employee who is not the part of Increment On behalf of Order No

        public async Task<List<RegIncrement_Model>> GetEmployeeForNonIncrement(string designID, string paycodeID, int orderID)
        {
            return await Task.Run(() => objRegDAL.GetEmployeeForNonIncrement(designID, paycodeID, orderID));
        }
        #endregion

        #region Insert Regular Increment Data
        
        public async Task<string> InsertRegIncrementData(object[] advModel)
        {
            var mytask = Task.Run(() => objRegDAL.InsertRegIncrementData(advModel));
            string result = await mytask;
            return result;
        }

        #endregion

        #region Delete Order details 

        public async Task<string> deleteOrderDetails(int id,string type)
        {
            var mytask = Task.Run(() => objRegDAL.deleteOrderDetails(id, type));
            string result = await mytask;
            return result;
        }

        #endregion

        #region Forward to ddo checker Increment Data
        
        public async Task<string> ForwardtocheckerForInc(object[] advModel)
        {
            var mytask = Task.Run(() => objRegDAL.ForwardtocheckerForInc(advModel));
            string result = await mytask;
            return result;
        }
        #endregion


        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

       
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~RegularIncrement_BAL()
        {
            Dispose(false);
        }
        #endregion
    }
}
