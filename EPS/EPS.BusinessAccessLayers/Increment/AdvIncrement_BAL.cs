﻿using EPS.BusinessModels.Increment;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Increment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Increment;
using System;

namespace EPS.BusinessAccessLayers.Increment
{
    public class AdvIncrement_BAL: IAdvIncrementRepository
    {
        Database epsDatabase;
        private bool disposed = false;
        AdvIncrement_DAL objadvDAL;
        public AdvIncrement_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objadvDAL= new AdvIncrement_DAL(epsDatabase);
        }

        #region Create Advance Increment Data
        public async Task<string> CreateAdvIncrement(AdvIncrement_Model advModel)
        {
            return await Task.Run(() => objadvDAL.CreateAdvIncrement(advModel));
        }
        #endregion

        #region Get Advance Increment Data
        public async Task<List<AdvIncrement_Model>> GetIncrementDetails(string empcode)
        {
            return await Task.Run(() => objadvDAL.GetIncrementDetails(empcode));
        }
        #endregion


        public async Task<List<AdvIncrement_Model>> GetEmployeeByDesigPayComm(string msDesignID, string paycomm)
        {
            return await Task.Run(() => objadvDAL.GetEmployeeByDesigPayComm(msDesignID, paycomm));
        }


        #region Edit Advance Increment Data

        public async Task<List<AdvIncrement_Model>> EditAdvIncrement(int incID)
        {
            return await Task.Run(() => objadvDAL.EditAdvIncrement(incID));
        }
        #endregion


        #region Delete Advance Increment Data
        public async Task<string> DeleteAdvDetails(int incID)
        {
            return await Task.Run(() => objadvDAL.DeleteAdvDetails(incID));
        }
        #endregion

        #region Forward to checker Advance Increment Data
        public async Task<string> Forwardtochecker(AdvIncrement_Model advModel)
        {
            return await Task.Run(() => objadvDAL.Forwardtochecker(advModel));
        }
        #endregion



        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~AdvIncrement_BAL()
        {
            Dispose(false);
        }
        #endregion
    }
}
