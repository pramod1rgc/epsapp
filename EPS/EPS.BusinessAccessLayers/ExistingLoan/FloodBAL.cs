﻿using EPS.CommonClasses;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using EPS.DataAccessLayers.ExistingLoan;
using System.Threading.Tasks;
using EPS.BusinessModels.ExistingLoan;
using EPS.Repositories.ExistingLoan;

namespace EPS.BusinessAccessLayers.ExistingLoan
{
    public class FloodBAL : IFloodRepository
    {
        Database epsdatabase;
        FloodDAL objFloodDAL;
        private bool disposed = false;
        public FloodBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objFloodDAL = new FloodDAL(epsdatabase);
        }

        public async Task<IEnumerable<FloodModel>> GetEmpDetails(int BillGrID, string desigId, int UserID,int mode)
        {
            IEnumerable<FloodModel> result = await Task.Run(() => objFloodDAL.GetEmpDetails(BillGrID, desigId, UserID, mode));
            return result;
        }

        public async Task<int> InsertFloodData(SanctionDetailsMOdel ObjSanctionDetails)
        {
            //IEnumerable<SanctionDetailsMOdel> result = await Task.Run(() => objFloodDAL.InsertFloodData(ObjSanctionDetails, EmployeeCode));
            int result= await Task.Run(() => objFloodDAL.InsertFloodData(ObjSanctionDetails));
            return result;
        }

        public async Task<int> ForwordToDdo(SanctionDetailsMOdel ObjSanctionDetails)
        {
            //IEnumerable<SanctionDetailsMOdel> result = await Task.Run(() => objFloodDAL.InsertFloodData(ObjSanctionDetails, EmployeeCode));
            int result = await Task.Run(() => objFloodDAL.ForwordToDdo(ObjSanctionDetails));
            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~FloodBAL()
        {
            Dispose(false);
        }
    }
}
