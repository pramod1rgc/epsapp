﻿using EPS.BusinessModels.ExistingLoan;
using EPS.CommonClasses;
using EPS.DataAccessLayers.ExistingLoan;
using EPS.Repositories.ExistingLoan;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.ExistingLoan
{
    public class RecoveryInstBAL: IinstRecoveryRepository
    {
        Database epsdatabase;
        RecoveryInstDAL objRecInstDAL;
        private bool disposed = false;
        public RecoveryInstBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objRecInstDAL = new RecoveryInstDAL(epsdatabase);
        }

        public async Task<IEnumerable<RecoveryInstModel>> GetEmpDetails(string EmpCD)
        {
            IEnumerable<RecoveryInstModel> result = await Task.Run(() => objRecInstDAL.GetEmpDetails(EmpCD));
            return result;
        }

        public async Task<IEnumerable<SanctionDetailsModel>> BindSanctionDetails(string LoanCd, string EmpCd)
        {
            IEnumerable<SanctionDetailsModel> result = await Task.Run(() => objRecInstDAL.BindSanctionDetails(LoanCd, EmpCd));
            return result;
        }

        public async Task<int> SaveSanctionData(SnactionDataModel ObjSnactionModel)
        {
            int result = await Task.Run(() => objRecInstDAL.SaveSanctionData(ObjSnactionModel));
            return result;
        }
        //public async Task<int> SaveLeavesCancel(LeavesCancelModel ObjLeavesCancelModelDetails, string Status)
        //{

        //    int result = await Task.Run(() => objLeavesDAL.SaveLeavesCancel(ObjLeavesCancelModelDetails, Status));
        //    return result;
        //}

        public async Task<int> ForwardtoDDOChecker(SnactionDataModel ObjSnactionModel)
        {
            int result = await Task.Run(() => objRecInstDAL.ForwardtoDDOChecker(ObjSnactionModel));
            return result;
        }


        public async Task<IEnumerable<InstEmpModel>> GetInstEmpDetails(string LoanCd)
        {
            IEnumerable<InstEmpModel> result = await Task.Run(() => objRecInstDAL.GetInstEmpDetails(LoanCd));
            return result;
        }

        public async Task<int> Accept(string EmpCD)
        {
            int result = await Task.Run(() => objRecInstDAL.Accept(EmpCD));
            return result;
        }
        public async Task<int> Reject(string EmpCD)
        {
            int result = await Task.Run(() => objRecInstDAL.Reject(EmpCD));
            return result;
        }

        public async Task<int> EditLoanDetailsById(SnactionDataModel ObjModel)
        {
            int result = await Task.Run(() => objRecInstDAL.EditLoanDetailsById(ObjModel));
            return result;
        }

        public async Task<int> DeleteLoanDetailsById(int msEmpLoanMultiInstId,string empCD)
        {
            int result = await Task.Run(() => objRecInstDAL.DeleteLoanDetailsById(msEmpLoanMultiInstId, empCD));
            return result;
        }


        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~RecoveryInstBAL()
        {
            Dispose(false);
        }
    }
}
