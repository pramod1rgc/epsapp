﻿using EPS.BusinessModels.ExistingLoan;
using EPS.CommonClasses;
using EPS.DataAccessLayers.ExistingLoan;
using EPS.Repositories.ExistingLoan;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.ExistingLoan
{
  public class ExistingLoanBusinessAccessLayer : IAlreadyTakenRepository
    {
        Database epsdatabase;
        ExistingLoanAccessLayer objExistingLoanDAL;
        private bool disposed = false;

        public ExistingLoanBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objExistingLoanDAL = new ExistingLoanAccessLayer(epsdatabase);
        }

        public async Task<IEnumerable<tblMsPayLoanRef>> GetAllDesignation()
        {
            IEnumerable<tblMsPayLoanRef> result = await Task.Run(() => objExistingLoanDAL.BindLoanType());
            return result;
        }

        public async Task<IEnumerable<tblMsPayLoanRef>> BindDropDownLoanType()
        {
            IEnumerable<tblMsPayLoanRef> result = await Task.Run(() => objExistingLoanDAL.BindLoanType());
            return result;
        }


        public async Task<LoanAdvanceDetailsModel> GetAlreadyTakenLoanAdvanceDetails(int PermDdoId, string EmpCode)
        {
            LoanAdvanceDetailsModel result = await Task.Run(() => objExistingLoanDAL.GetAlreadyTakenLoanAdvanceDetails(PermDdoId,EmpCode));
            return result;
        }


        public async Task<HeadAccountModel> GetHeadAccount(int LoanCD)
        {
            HeadAccountModel result = await Task.Run(() => objExistingLoanDAL.GetHeadAccountDetails(LoanCD));
            return result;
        }
        public async Task<int>  SaveLoanData(ExistingLoanApplicationModel _obj) 
        {
            int result = await Task.Run(() => objExistingLoanDAL.SaveExistingLoanDetails(_obj));
            return result;

           // return objExistingLoanDAL.SaveExistingLoanDetails(_obj);
        }
        public async Task<IEnumerable<LoanAdvanceDetailsModel>>   GetExistLoanDetailsByID(string MSPayAdvEmpdetID)
        {      
            IEnumerable<LoanAdvanceDetailsModel> result = await Task.Run(() => objExistingLoanDAL.GetExistLoanDetailsByID(MSPayAdvEmpdetID));
            return result;
        }

        public async Task<IEnumerable<LoanAdvanceDetailsModel>> GetExistingEmpLoanDetails(string PayBillGpID)
        {
            IEnumerable<LoanAdvanceDetailsModel> result = await Task.Run(() => objExistingLoanDAL.GetEmpLoanListDetails(PayBillGpID)); ;
            return result;
        }

        public async Task<IEnumerable<ExistingLaonStatusModel>> BindExistingLoanStatus(int ddoId , int UserId,int userRoleID)
        {
            IEnumerable<ExistingLaonStatusModel> result = await Task.Run(() => objExistingLoanDAL.BindExistingLoanStatus(ddoId, UserId, userRoleID));
            return result;
        }
        public async Task<int> ExistingLoanForwordToDDO(ExistingLoanApplicationModel objModel)
        {
            int result = await Task.Run(() => objExistingLoanDAL.ExistingLoanForwordToDDO(objModel));
            return result;
        }

        public async Task<int> DeleteEmpDetails(string CtrCode, string msPayAdvEmpdetID,int Mode)
        {
            int result = await Task.Run(() => objExistingLoanDAL.DeleteEmpDetails(CtrCode, msPayAdvEmpdetID, Mode));
            return result;
        }


        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);
            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~ExistingLoanBusinessAccessLayer()
        {
            Dispose(false);
        }
    }
}
