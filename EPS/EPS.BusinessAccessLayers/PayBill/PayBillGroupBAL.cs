﻿using EPS.BusinessModels.PayBill;
using EPS.CommonClasses;
//using EPS.DataAccessLayers;
using EPS.DataAccessLayers.PayBill;
using EPS.Repositories.PayBIllGroup;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.PayBill
{
    public class PayBillGroupBAL: IPayBillGroupRepository
    {
        Database epsDataBase;
        private bool disposed = false;
        private PayBillGroupDAL objPayBillGroupDAL;

        public PayBillGroupBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);

            objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
        }
        

        /// <summary>
        /// To Get the Account head for PaybillGroup
        /// </summary>
        /// <param name="PermDDOId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AccountHeads>> GetAccountHeads(string PermDDOId)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetAccountHeads(PermDDOId));
            IEnumerable<AccountHeads> result = await myTask;
            return result;
        }

        /// <summary>
        /// Insert or Update PayBillGroup
        /// </summary>
        /// <param name="objBillGroup"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdatePayBillGroup(PayBillGroup objBillGroup)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.InsertUpdatePayBillGroup(objBillGroup));
            string result = await myTask;
            return result;
        }

        public async Task<PayBillGroupModel> GetPayBillGroupDetailsById(int BillGroupId)
        {

            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetPayBillGroupModelDetailsById(BillGroupId));
            PayBillGroupModel result = await mytask;
            return result;


           
        }


        public async Task<IEnumerable<PayBillGroupAttachandDeattachModel>> PayBillGroupAttach(string EmpPermDDOId)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetpayBillGroupEmpAttach(EmpPermDDOId));
           IEnumerable<PayBillGroupAttachandDeattachModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<PayBillGroupAttachandDeattachModel>> PayBillGroupDeattach(string EmpPermDDOId, string BillGrpID)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetpayBillGroupEmpDeAttach(EmpPermDDOId, BillGrpID));
            IEnumerable<PayBillGroupAttachandDeattachModel> result = await mytask;
            return result;

           
        }

        public async Task<int> UpdatePayBillGroupIdusingEmpCodendBillGroupId(string EmpPermDDOId, string BillGrpID)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.UpdatePayBillGroupByEmpCode(EmpPermDDOId, BillGrpID));
            int result = await mytask;
            return result;
        }

        public async Task<int> UpdatePayBillGroupIdNullusingEmpCodend(string EmpPermDDOId)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.UpdatePayBillNullGroupByEmpCode(EmpPermDDOId));
            int result = await mytask;
            return result;
        }


        #region amit-Account Heads Other Than 1
        
        ////amit ddocode
        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadsOT1(string FinFromYr, string FinToYr, string Flag)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetAccountHeadsOT1(FinFromYr, FinToYr, Flag));
            IEnumerable<AccountHeadOtherThanOne> result = await mytask;
            return result;
           
        }
        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1AttachList(string DDOCode,string Flag)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetAccountHeadOT1AttachList(DDOCode, Flag));
            IEnumerable<AccountHeadOtherThanOne> result = await mytask;
            return result;
           
        }

        public async Task<int> UpdateaccountHeadOT1(string accountHeadvalue, string DDOCode, string Flag)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.UpdateaccountHeadOT1(accountHeadvalue, DDOCode, Flag));
            int result = await mytask;
            return result;
            
        }
       
        public async Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1PaybillGroupStatus(string DDOCode, string Flag)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.GetAccountHeadOT1PaybillGroupStatus(DDOCode, Flag));
            IEnumerable<AccountHeadOtherThanOne> result = await mytask;
            return result;  
        }

        public async Task<int> UpdateAccountHeadOT1PaybillGroupStatus(string accountHeadvalue, string DDOCode, string Flag)
        {
            PayBillGroupDAL objPayBillGroupDAL = new PayBillGroupDAL(epsDataBase);
            var mytask = Task.Run(() => objPayBillGroupDAL.UpdateAccountHeadOT1PaybillGroupStatus(accountHeadvalue, DDOCode, Flag));
            int result = await mytask;
            return result;
           
        }
        #endregion



        public async Task<IEnumerable<NgRecovery>> GetNgRecovery(string PermDDOId)
        {
            //return ;
            var myTask = Task.Run(() => objPayBillGroupDAL.GetNgRecovery(PermDDOId));
            IEnumerable<NgRecovery> result = await myTask;
            return result;
        }

        public async Task<IEnumerable<BankDetails>> GetBankdetailsByIfsc(string IfscCode)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetBankdetailsByIfsc(IfscCode));
            IEnumerable<BankDetails> result = await myTask;
            return result;
        }

        public async Task<int> InsertUpdateNgRecovery(NgRecovery objNgRecovery)
        {            
            int result = await Task.Run(() => objPayBillGroupDAL.InsertUpdateNgRecovery(objNgRecovery));
            return result;
        }

        public async Task<int> ActiveDeactiveNgRecovery(NgRecovery objNgRecovery)
        {
            int result = await Task.Run(() => objPayBillGroupDAL.ActiveDeactiveNgRecovery(objNgRecovery));
            return result;
        }

        public async Task<IEnumerable<FinancialYears>> GetFinancialYear()
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetFinancialYear());
            IEnumerable<FinancialYears> result = await myTask;
            return result;
        }

        public async Task<IEnumerable<AllMonths>> GetAllMonth()
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetAllMonth());
            IEnumerable<AllMonths> result = await myTask;
            return result;
        }

        public async Task<IEnumerable<PayBillGroupByFinancialYearModel>> GetPayBillGroupByFinancialYear(string FinYearFrom, string FinYearTo, string PermDdoId)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetPayBillGroupByFinancialYear(FinYearFrom, FinYearTo, PermDdoId));
            IEnumerable<PayBillGroupByFinancialYearModel> result = await myTask;
            return result;
        }

        public async Task<IEnumerable<GetEmployeesByPayItemCodeModel>> GetEmployeesByPayItemCode(int PayItemCd)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetEmployeesByPayItemCode(PayItemCd));
            IEnumerable<GetEmployeesByPayItemCodeModel> result = await myTask;
            return result;
        }

        public async Task<IEnumerable<GetPayItemsFromDesignationCodeModel>> GetPayItemsFromDesignationCode(int DesigCode)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetPayItemsFromDesignationCode(DesigCode));
            IEnumerable<GetPayItemsFromDesignationCodeModel> result = await myTask;
            return result;
        }

        public async Task<string> UpdateEmpNonComputationalDuesDeductAmount(UpdateEmpDuesDeductAmountModel obj)
        {            
            string result = await Task.Run(() => objPayBillGroupDAL.UpdateEmpNonComputationalDuesDeductAmount(obj));
            return result;
        }

        public async Task<IEnumerable<EmpList>> GetEmployeeByPaybillAndDesig(string PermDDOId, int PayBillGroupId, int DesigCd)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetEmployeeByPaybillAndDesig(PermDDOId, PayBillGroupId, DesigCd));
            IEnumerable<EmpList> result = await myTask;
            return result;
        }

        public Task<int> UpdatePayBillGroupByEmpCode(string Empcode, string BillGroupId)
        {
            throw new NotImplementedException();
        }

        public Task<int> InsertAllNgEntry(List<EmpList> ngList)
        {
            return objPayBillGroupDAL.InsertAllNgEntry(ngList);
        }

        public async Task<IEnumerable<NgList>> GetNgRecoveryList(string PermDDOId)
        {
            var myTask = Task.Run(() => objPayBillGroupDAL.GetNgRecoveryList(PermDDOId));
            IEnumerable<NgList> result = await myTask;
            return result;
        }


        public async Task<IEnumerable<NgList>> GetAllNgRecoveryandPaybillGrpDetails(string permDdoId)
        {
            IEnumerable<NgList> result = await Task.Run(() => objPayBillGroupDAL.GetAllNgRecoveryandPaybillGrpDetails(permDdoId));
            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    objPayBillGroupDAL = null;
                    epsDataBase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~PayBillGroupBAL()
        {
            Dispose(false);
        }
    }
}
