﻿using EPS.BusinessModels.PayBill;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace EPS.BusinessAccessLayers.PayBill
{
    public interface IPayBillGroupBAL
    {
        Task<IEnumerable<AccountHeads>> GetAccountHeads(string PermDDOId);

        Task<string> InsertUpdatePayBillGroup(PayBillGroup objBillGroup);

        Task<PayBillGroupModel> GetPayBillGroupDetailsById(int BillGroupId);

        Task<IEnumerable<PayBillGroupAttachandDeattachModel>> PayBillGroupAttach(string EmpPermDDOId);

        Task<IEnumerable<PayBillGroupAttachandDeattachModel>> PayBillGroupDeattach(string EmpPermDDOId, string BillGrpID);

        Task<int> UpdatePayBillGroupIdusingEmpCodendBillGroupId(string EmpPermDDOId, string BillGrpID);
        Task<int> UpdatePayBillGroupIdNullusingEmpCodend(string EmpPermDDOId);
        //amit ddocode

        Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadsOT1(string FinFromYr, string FinToYr, string Flag);
        Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1AttachList(string DDOCode,string Flag);
        Task<int> UpdateaccountHeadOT1(string accountHeadvalue, string DDOCode, string Flag);
        Task<IEnumerable<AccountHeadOtherThanOne>> GetAccountHeadOT1PaybillGroupStatus(string DDOCode, string Flag);
        Task<int> UpdateAccountHeadOT1PaybillGroupStatus(string accountHeadvalue, string DDOCode, string Flag);
        //

        Task<IEnumerable<NgRecovery>> GetNgRecovery(string PermDDOId);
        Task<IEnumerable<BankDetails>> GetBankdetailsByIfsc(string IfscCode);

        Task<int> InsertUpdateNgRecovery(NgRecovery objNgRecovery);
        Task<int> ActiveDeactiveNgRecovery(NgRecovery objNgRecovery);
        Task<IEnumerable<FinancialYears>> GetFinancialYear();
        Task<IEnumerable<AllMonths>> GetAllMonth();

        Task<IEnumerable<PayBillGroupByFinancialYearModel>> GetPayBillGroupByFinancialYear(string FinYearFrom, string FinYearTo, string PermDdoId);

        Task<IEnumerable<GetEmployeesByPayItemCodeModel>> GetEmployeesByPayItemCode(int PayItemCd);

        Task<IEnumerable<GetPayItemsFromDesignationCodeModel>> GetPayItemsFromDesignationCode(int DesigCode);

        Task<string> UpdateEmpNonComputationalDuesDeductAmount(UpdateEmpDuesDeductAmountModel obj);

        Task<IEnumerable<EmpList>> GetEmployeeByPaybillAndDesig(string PermDDOId, int PayBillGroupId, int DesigCd);
        Task<int> UpdatePayBillGroupByEmpCode(string Empcode, string BillGroupId);

        Task<int> InsertAllNgEntry(List<EmpList> ngList);
        Task<IEnumerable<NgList>> GetNgRecoveryList(string PermDDOId);
        Task<IEnumerable<NgList>> GetAllNgRecoveryandPaybillGrpDetails(string permDdoId);
    }
}
