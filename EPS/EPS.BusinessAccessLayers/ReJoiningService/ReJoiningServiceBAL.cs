﻿using EPS.BusinessModels.ReJoiningService;
using EPS.DataAccessLayers.ReJoiningService;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessAccessLayers.ReJoiningService
{
    public class ReJoiningServiceBAL
    {
        ReJoiningServiceDAL objRejDAL = new ReJoiningServiceDAL();

        public IEnumerable<GetServiceEndEmployees> GetServiceEndEmployeesDetailsBAL(string empCd, string empName, string empPanNo)
        {
            return objRejDAL.GetServiceEndEmployeesDetails(empCd, empName, empPanNo);
        }

        public IEnumerable<GetReJoiningServiceEmployees> GetReJoiningServiceEmployeesDetailsBAL(int pageNumber, int pageSize, string SearchTerm, string empCd, int roleId)
        {
            return objRejDAL.GetReJoiningServiceEmployeesDetails(pageNumber, pageSize, SearchTerm, empCd, roleId);
        }

        public string UpsertReJoiningServiceDetailsBAL(UpsertReJoiningServiceDetails objRej)
        {
            return objRejDAL.UpsertReJoiningServiceDetails(objRej);
        }

        public string DeleteReJoiningServiceDetailsBAL(DeleteRejoiningService objRej)
        {
            return objRejDAL.DeleteReJoiningServiceDetails(objRej);
        }

        public string UpdateReJoiningServiceStatusBAL(UpdateReJoiningServiceStatus objRej)
        {
            return objRejDAL.UpdateReJoiningServiceStatus(objRej);
        }
    }
}
