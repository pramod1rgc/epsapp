﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Deputation;
using EPS.Repositories.Deputation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace EPS.BusinessAccessLayers.Deputation
{
    public class DeputationOutBA: IDeputationOut
    {
       private Database epsdatabase;
        public DeputationOutBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<DeputationOutModel>> GetAllDepuatationOutDetailsByEmployee(string EmpCd, int roleId)
        {
            DeputationOutDA objDeputationoutDA = new DeputationOutDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationoutDA.GetAllDepuatationOutDetailsByEmployee(EmpCd, roleId));
            IEnumerable<DeputationOutModel> result = await myTask;
            return result;

        }
        public async Task<string> InsertUpdateDeputationOutDetails(DeputationOutModel objDeputationOut)
        {
            DeputationOutDA objDeputationOutDA = new DeputationOutDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationOutDA.InsertUpdateDeputationOutDetails(objDeputationOut));
            string result = await myTask;
            return result;
        }
    }
}
