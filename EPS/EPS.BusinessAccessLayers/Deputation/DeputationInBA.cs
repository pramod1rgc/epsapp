﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Deputation;
using EPS.Repositories.Deputation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Deputation
{
    public class DeputationInBA : IDeputationIn
    {
      private  Database epsdatabase;
        public DeputationInBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<DeputationInModel>> GetPayItems()
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var mytask = Task.Run(() => objDeputationInDA.GetPayItems());
            IEnumerable<DeputationInModel> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<Designation>> GetAllDesignation(string controllerId)
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.GetAllDesignation(controllerId));
            var result = await myTask;
            return result;

        }
        public async Task<IEnumerable<DeputationInModel>> GetCommunicationAddress()
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.GetCommunicationAddress());
            var result = await myTask;
            return result;

        }
        public async Task<DeputationInModel> GetDeductionScheduleDetails(string EmpCd,int roleId)
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.GetDeductionScheduleDetails(EmpCd,roleId));
            var result = await myTask;
            return result;

        }
        public async Task<SchemeCode> GetScheme_CodeByMsEmpDuesID(int MsEmpDuesID)
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.GetScheme_CodeByMsEmpDuesID(MsEmpDuesID));
            var result = await myTask;
            return result;

        }
        public async Task<int> InsertUpdateDeputationInDetails(DeputationInModel objDeputationIn)
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.InsertUpdateDeputationInDetails(objDeputationIn));
            int result = await myTask;
            return result;
        }
        public async Task<int> UpdateforwardStatusUpdateByDepId(int empDeputDetailsId, string status, string rejectionRemark)
        {
            DeputationInDA objDeputationInDA = new DeputationInDA(epsdatabase);
            var myTask = Task.Run(() => objDeputationInDA.UpdateforwardStatusUpdateByDepId(empDeputDetailsId, status, rejectionRemark));
            int result = await myTask;
            return result;
        }

    }
}
