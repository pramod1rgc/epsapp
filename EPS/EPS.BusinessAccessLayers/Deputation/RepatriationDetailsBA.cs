﻿using EPS.BusinessModels.Deputation;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Deputation;
using EPS.Repositories.Deputation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Deputation
{
    public class RepatriationDetailsBA : IRepatriationDetails
    {
      private  Database epsdatabase;
        public RepatriationDetailsBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }


        public async Task<IEnumerable<RepatriationDetailsModel>> GetAllRepatriationDetailsByEmployee(string empCd, int roleId)
        {
            RepatriationDetailsDA objRepatriationDetailsDA = new RepatriationDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objRepatriationDetailsDA.GetAllRepatriationDetailsByEmployee(empCd, roleId));
            IEnumerable<RepatriationDetailsModel> result = await myTask;
            return result;

        }
        public async Task<DeputationInExistEmpModel> CheckDepuatationinEmployee(string empCd, int roleId)
        {
            RepatriationDetailsDA objRepatriationDetailsDA = new RepatriationDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objRepatriationDetailsDA.CheckDepuatationinEmployee(empCd, roleId));
           DeputationInExistEmpModel result = await myTask;
            return result;

        }
        public async Task<string> InsertUpdateRepatriationDetails(RepatriationDetailsModel objRepatriationDetails)
        {
            RepatriationDetailsDA objRepatriationDetailsDA = new RepatriationDetailsDA(epsdatabase);
            var myTask = Task.Run(() => objRepatriationDetailsDA.InsertUpdateRepatriationDetails(objRepatriationDetails));
            string result = await myTask;
            return result;
        }
    }
}
