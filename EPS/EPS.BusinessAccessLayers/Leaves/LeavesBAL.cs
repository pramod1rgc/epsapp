﻿using System;
using System.Collections.Generic;
using EPS.DataAccessLayers.Leaves;
using EPS.BusinessModels.Leaves;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.Repositories.LeaveMangement;


namespace EPS.BusinessAccessLayers.Leaves
{
   public class LeavesBAL: ILeaveManagementRepository
    {


        Database epsdatabase;
        LeavesDAL objLeavesDAL;
        private bool disposed = false;
        public LeavesBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objLeavesDAL= new LeavesDAL(epsdatabase);
        }


        #region Get All Designation  
        public async Task<IEnumerable<DesignationModel>> GetAllDesignation(string PermDdoId)
        {            
            IEnumerable<DesignationModel> result = await Task.Run(() => objLeavesDAL.GetAllDesignation(PermDdoId)); 
            return result;
        }
        #endregion

        #region Get Employee  by DesignationId
        public async Task<IEnumerable<EmpModel>> GetEmployeeByDesig(string MsDesignID)
        {
            IEnumerable<EmpModel> result = await Task.Run(() => objLeavesDAL.GetEmployeeByDesig(MsDesignID)); 
            return result;
        }
        #endregion

        #region Get Order by Emp
        public async Task<IEnumerable<OrderModel>> GetOrderByEmployee(string EmpCd,string userRoleId,string pageCode)
        {
            IEnumerable<OrderModel> result = await Task.Run(() => objLeavesDAL.GetOrderByEmployee(EmpCd, userRoleId, pageCode));
            return result;
        }
        #endregion

        #region Get All LeavesType 
        public async Task<IEnumerable<LeavesTypeModel>> GetLeavesType()
        {
            IEnumerable<LeavesTypeModel> result = await Task.Run(() => objLeavesDAL.GetLeavesType());
            return result;
        }
        #endregion

        #region Get All LeavesReason 
        public async Task<IEnumerable<LeavesReasonModel>> GetLeavesReason()
        {
            IEnumerable<LeavesReasonModel> result = await Task.Run(() => objLeavesDAL.GetLeavesReason());
            return result;
        }
        #endregion


        #region Sanction

        #region Get Leave Sanction
        public async Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanction(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesSanctionMainModel> result = await Task.Run(() => objLeavesDAL.GetLeavesSanction(permDdoId, empCode, userRoleId));
            return result;
        }
        #endregion

        #region Get Leave Sanction History
        public async Task<IEnumerable<LeavesSanctionMainModel>> GetLeavesSanctionHistory(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesSanctionMainModel> result = await Task.Run(() => objLeavesDAL.GetLeavesSanctionHistory(permDdoId, empCode, userRoleId));
            return result;
        }
        #endregion

        #region Save Leave Sanction 
        public async Task<int> SaveLeavesSanction(LeavesSanctionMainModel ObjLeavesSanctionModelDetails, string Status)
        {
            int result = await Task.Run(() => objLeavesDAL.SaveLeavesSanction(ObjLeavesSanctionModelDetails, Status));
            return result;
        }
        #endregion

        #endregion


        #region Curtail/Extended

        #region Get Curtail/Extended Leave
        public async Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailDetail(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesCurtailSanctionModel> result = await Task.Run(() => objLeavesDAL.GetLeavesCurtailDetail(permDdoId, empCode, userRoleId));
            return result;
        }
        #endregion

        #region Get Curtail/Extended Leave History
        public async Task<IEnumerable<LeavesCurtailSanctionModel>> GetLeavesCurtailHistory(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesCurtailSanctionModel> result = await Task.Run(() => objLeavesDAL.GetLeavesCurtailHistory(permDdoId, empCode, userRoleId));
            return result;
        }
        #endregion

        #region Get Verified Leave Sanction
        public async Task<LeavesCurtailSanctionModel> GetVerifiedLeavesSanction(string permDdoId, string ordId)
        {
            LeavesCurtailSanctionModel result = await Task.Run(() => objLeavesDAL.GetVerifiedLeavesSanction(permDdoId, ordId));
            return result;
        }
        #endregion

        #region Save Leave CurtExten
        public async Task<int> SaveLeavesCurtExetn(LeavesCurtailModel ObjLeavesCurtailModelDetails, string Status)
        {

            int result = await Task.Run(() => objLeavesDAL.SaveLeavesCurtExten(ObjLeavesCurtailModelDetails, Status));
            return result;
        }
        #endregion

        #endregion


        #region Cancel

        #region Get Cancel Leave 
        public async Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancel(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesCancelSanctionModel> result = await Task.Run(() => objLeavesDAL.GetLeavesCancel(permDdoId,empCode,userRoleId));
            return result;
        }
        #endregion

        #region  Get Cancel Leave History 
        public async Task<IEnumerable<LeavesCancelSanctionModel>> GetLeavesCancelHistory(string permDdoId, string empCode, string userRoleId)
        {
            IEnumerable<LeavesCancelSanctionModel> result = await Task.Run(() => objLeavesDAL.GetLeavesCancelHistory(permDdoId, empCode, userRoleId));
            return result;
        }


        #endregion

        #region Get Verified Leave Cancel
        public async Task<LeavesCancelModel> GetVerifiedLeaveForCancel(string permDdoId, string ordId)
        {
            LeavesCancelModel result = await Task.Run(() => objLeavesDAL.GetVerifiedLeaveForCancel(permDdoId, ordId));
            return result;
        }

        #endregion

        #region Save Leave Cancel
        public async Task<int> SaveLeavesCancel(LeavesCancelModel ObjLeavesCancelModelDetails, string Status)
        {
            
            int result = await Task.Run(() => objLeavesDAL.SaveLeavesCancel(ObjLeavesCancelModelDetails, Status));
            return result;
        }

        #endregion

        #endregion





        public async Task<int> DeleteLeavesSanction(string orderNo,string pageCode)
        {
            int result = await Task.Run(() => objLeavesDAL.DeleteLeavesSanction(orderNo, pageCode));
            return result;
        }
        
       
        public async Task<int> VerifyReturnSanction(string Id, string Status,string PageCd,string Reason,string ipAddress)
        {

            int result = await Task.Run(() => objLeavesDAL.VerifyReturnSanction(Id,Status,PageCd,Reason, ipAddress));
            return result;
        }


        #region Get Leaves Type Balance
   
        public async Task<IEnumerable<LeavesTypeBalanceModel>> GetLeavesTypeBalance(string EmpCd)
        {
            IEnumerable<LeavesTypeBalanceModel> result = await Task.Run(() => objLeavesDAL.GetLeavesTypeBalance(EmpCd));
            return result;
        }
        #endregion

        #region Save Total CCL Leave Availed
        public async Task<int> SaveTotalLeaveAvailed(string EmpCd,string TotalLeaveAvailed,string ipAddress)
        {

            int result = await Task.Run(() => objLeavesDAL.SaveTotalLeaveAvailed(EmpCd, TotalLeaveAvailed, ipAddress));
            return result;
        }

        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    objLeavesDAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~LeavesBAL()
        {
            Dispose(false);
        }

    }
}
