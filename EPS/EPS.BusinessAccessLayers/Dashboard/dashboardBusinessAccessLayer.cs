﻿using EPS.BusinessModels.Dashboard;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Dashboard;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Dashboard
{
    public class dashboardBusinessAccessLayer
    {      
        Database epsdatabase;
        dashboardDataAccessLayer objdashboardDataAccessLayer;
        /// <summary>
        /// dashboardBusinessAccessLayer
        /// </summary>
        public dashboardBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objdashboardDataAccessLayer = new dashboardDataAccessLayer(epsdatabase);
        }

        /// <summary>
        /// get All Menus By User
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="UroLLID"></param>
        /// <returns></returns>
        public IEnumerable<DashboardModel> getAllMenusByUser(string Username, string UroLLID)
        {
            return objdashboardDataAccessLayer.getAllMenusByUser(Username, UroLLID);
        }

        /// <summary>
        /// Get Dashboard Details
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<DashboardModel>> GetDashboardDetails()
        {
            return await objdashboardDataAccessLayer.GetDashboardDetails();
        }
    }
}
