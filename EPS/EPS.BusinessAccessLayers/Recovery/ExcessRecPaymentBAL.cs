﻿using EPS.BusinessModels.Recovery;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Recovery;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Recovery
{

    public class ExcessRecPaymentBAL
    {
        private Database epsDatabase;
        public ExcessRecPaymentBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        /// <summary>
        /// Get Financial Year
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetFinancialYears()
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetFinancialYears());
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        /// <summary>
        /// Get Loan Payment Type
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetLoanPaymentType()
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetLoanPaymentType());
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        /// <summary>
        /// Get Recovery Element
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetRecoveryElement()
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetRecoveryElement());
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        /// <summary>
        /// Get Account Head
        /// </summary>
        /// <param name="paybill"></param>
        /// <param name="ddoId"></param>
        /// <param name="finYear"></param>
        /// <param name="recoveryType"></param>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetAccountHead(string paybill, string ddoId, int finYear, string recoveryType)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetAccountHead(paybill, ddoId, finYear, recoveryType));
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        /// <summary>
        /// Get Recovery Component
        /// </summary>
        /// <param name="paybill"></param>
        /// <param name="EmpId"></param>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetRecoveryComponent(string paybill, string EmpId)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetRecoveryComponent(paybill, EmpId));
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        /// <summary>
        /// Insert Update Excess Payment Records
        /// </summary>
        /// <param name="recoveryModel"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdateExRecovery(RecoveryExPaymentModel recoveryModel)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.InsertUpdateExRecovery(recoveryModel));
            string result = await mytask;
            return result;
        }

        #region Get Excess Recovery Details
        /// <summary>
        /// Get Excess Recovery Detail
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> GetExcessRecoveryDetail( string empCode)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetExcessRecoveryDetail(empCode));
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }
        #endregion

        #region Edit Excess Recovery Detail
        /// <summary>
        /// Edit Excess Recovery Detail
        /// </summary>
        /// <param name="RecExcessId"></param>
        /// <returns></returns>
        public async Task<List<RecoveryExPaymentModel>> EditExcessRecoveryDetail(int recExcessId)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.EditExcessRecoveryDetail(recExcessId));
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }
        #endregion

        #region Delete Excess Recovery Records
        /// <summary>
        /// Delete Excess Recovery Records
        /// </summary>
        /// <param name="RecExcessId"></param>
        /// <returns></returns>
        public async Task<string> DeleteExcessRecoveryData(int RecExcessId)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.DeleteExcessRecoveryData(RecExcessId));
            string result = await mytask;
            return result;
        }
        #endregion

        /// <summary>
        /// Forward Excess Recovery Data
        /// </summary>
        /// <param name="recoveryModel"></param>
        /// <returns></returns>
        public async Task<string> ForwardExRecoveryData(RecoveryExPaymentModel recoveryModel)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.ForwardExRecoveryData(recoveryModel));
            string result = await mytask;
            return result;
        }

        #region Add Component Amount

        public async Task<string> InsertUpdateComponentAmt(RecoveryExPaymentModel recoveryModel)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.InsertUpdateComponentAmt(recoveryModel));
            string result = await mytask;
            return result;
        }

        public async Task<List<RecoveryExPaymentModel>> GetComponentAmtDetails(string EmpId, string Session)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.GetComponentAmtDetails(EmpId, Session));
            List<RecoveryExPaymentModel> result = await mytask;
            return result;
        }

        public async Task<string> DeleteComponentAmt(int CmpId)
        {
            ExcessRecPaymentDAL objRecovDAL = new ExcessRecPaymentDAL(epsDatabase);
            var mytask = Task.Run(() => objRecovDAL.DeleteComponentAmt(CmpId));
            string result = await mytask;
            return result;
        }
        #endregion
    }
}
