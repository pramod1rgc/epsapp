﻿using EPS.BusinessModels.ExcessRecovery;
using EPS.BusinessModels.Recovery;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.DataAccessLayers.ExcessRecovery;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessAccessLayers.ExcessRecovery
{
    public class ExcessRecoveryBAL
    {

        Database epsdatabase;
        ExcessRecoveryDAL objExcessRecoveryDAL;
        public ExcessRecoveryBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objExcessRecoveryDAL = new ExcessRecoveryDAL(epsdatabase);
        }
        public void InsertExcessRecovery()
        {

        }

        public string SaveTempPayment(TemporaryPermanentStop ObjTempData)
        {
            return objExcessRecoveryDAL.SaveTempPayment(ObjTempData);
        }

        public string SaveRecoveryExcess(ExcessRecoveryModel ObjTempData)
        {
            return objExcessRecoveryDAL.SaveRecoveryExcess(ObjTempData);
        }
        public IEnumerable<TemporaryPermanentStop> getTempPaymentRecovery()
        {
            return objExcessRecoveryDAL.getTempPaymentRecovery();
        }
    }
}
