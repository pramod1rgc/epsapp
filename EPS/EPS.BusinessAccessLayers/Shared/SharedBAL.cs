﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.BusinessModels.Shared;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Shared;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace EPS.BusinessAccessLayers.Shared
{
    public class SharedBAL
    {
       Database EpsDatabase;
        public SharedBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }             
        public IEnumerable<PayBillGroupModel> GetPayBillGroup(string PermDdoid,string Flag)
        {
             SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            return objSharedDAL.GetPayBillGroup(PermDdoid, Flag);
        } 
        #region Get Pay Bill Group By Perm DDOId
        public async Task<List<PayBillGroupModel>> GetPayBillGroupByPermDDOId(string PermDdoid,string Flag)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            var mytask = Task.Run(() => objSharedDAL.GetPayBillGroupByPermDDOId(PermDdoid, Flag));
            List<PayBillGroupModel> result = await mytask;
            return result;
        }
        #endregion    
        public async Task<List<DesignationModel>> GetDesignation(string BillGrpid,string Flag)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            var mytask = Task.Run(() => objSharedDAL.GetDesignation(BillGrpid, Flag));
            List<DesignationModel> result = await mytask;
            return result;
        }

        //public IEnumerable<DesignationModel> GetDesignationByBillgrID(string BillGrpid, string PermDdoid)
        //{
        //    SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
        //    return objSharedDAL.GetDesignationByBillgrID(BillGrpid, PermDdoid);
        //}

        public async Task<List<DesignationModel>> GetDesignationByBillgrID(string BillGrpid, string PermDdoid,string Flag)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            var mytask = Task.Run(() => objSharedDAL.GetDesignationByBillgrID(BillGrpid, PermDdoid, Flag));
            List<DesignationModel> result = await mytask;
            return result;
        }


        //Employee
        public IEnumerable<EmpModel> GetEmp(string DesigId, string flag)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            return objSharedDAL.GetEmp(DesigId, flag);
        }
        public IEnumerable<DesignationModel> GetAllDesignation(string id)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            return objSharedDAL.GetAllDesignation(id);
        }
        public IEnumerable<DesignationModel> GetAllDesignationOfDepartment(string id)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            return objSharedDAL.GetAllDesignationOfDepartment(id);
        }
        public IEnumerable<EmpDesigModel> GetEmployeesWithDesignationDetails(bool SuspendedEmp, string controllerId, string DDOId)
        {
            SharedDAL objSharedDAL = new SharedDAL(EpsDatabase);
            return objSharedDAL.GetEmpWithDesignationDetails(SuspendedEmp, controllerId, DDOId);
        }
    }
}
