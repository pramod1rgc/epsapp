﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LoanApplication;
using EPS.Repositories.LoanApplication;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.LoanApplication
{
    public class RecoveryChallanBAL: IRecoveryChallanRepository
    {
        Database epsdatabase;
        RecoveryChallanDAL objRecoveryChallanDAL;
        private bool disposed = false;

        public RecoveryChallanBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objRecoveryChallanDAL = new RecoveryChallanDAL(epsdatabase);
        }

       

        //public async Task<IEnumerable<tblMsPayLoanRef>> GetAllLoanType()
        //{
        //    IEnumerable<tblMsPayLoanRef> result = await Task.Run(() => objRecoveryChallanDAL.BindLoanType());
        //    return result;
        //}
        public async Task<List<RecoveryChallan>> GetRecoveryChallan(string empCode, int msDesignID)
        {
            return await Task.Run(() => objRecoveryChallanDAL.GetRecoveryChallan(empCode, msDesignID));
        }

        public async Task<string> InsertUpdateRecoveryChallan(RecoveryChallan objmodel)
        {
            RecoveryChallanDAL objDA = new RecoveryChallanDAL(epsdatabase);
            var myTask = Task.Run(() => objRecoveryChallanDAL.InsertUpdateRecoveryChallan(objmodel));
            string result = await myTask;
            return result;
        }
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                   // RecoveryChallanBAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~RecoveryChallanBAL()
        {
            Dispose(false);
        }
    }
}
