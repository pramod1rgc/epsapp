﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LoanApplication;
using EPS.Repositories.LoanApplication;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.LoanApplication
{
    /// <summary>
    /// PropOwnerDetBAL
    /// </summary>
    public class PropOwnerDetBAL: IPropertyRepository
    {
       private Database EpsDatabase;
        private bool disposed = false;
        public PropOwnerDetBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region Create Master Data
        public async Task<string> CreatePropOwnerDetMaster(PropOwner_Model POwnerModel)
        {

            PropOwnerDetDAL objpropDAL = new PropOwnerDetDAL(EpsDatabase);
            return await Task.Run(()=>objpropDAL.CreatePropOwnerDetMaster(POwnerModel));
        }
        #endregion
        #region Get Master Data by ID
        public async Task <IEnumerable<PropOwner_Model>> GetPropOwnerDetMasterDetailsByID(string MasterID)
        {
            PropOwnerDetDAL objpropDAL = new PropOwnerDetDAL(EpsDatabase);
            return await Task.Run(()=>objpropDAL.GetPropOwnerDetMasterDetailsByID(MasterID));
        }
        #endregion
        #region Edit Master Data
        public async Task<IEnumerable<PropOwner_Model>> EditPropOwnerDetMasterDetails(string MasterID)
        {
            PropOwnerDetDAL objpropDAL = new PropOwnerDetDAL(EpsDatabase);
            return await Task.Run(()=>objpropDAL.EditPropOwnerDetMasterDetails(MasterID));
        }


        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                   // PropOwnerDetBAL = null;
                    EpsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~PropOwnerDetBAL()
        {
            Dispose(false);
        }

    }
}
