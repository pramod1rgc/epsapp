﻿using EPS.BusinessModels.LoanApplication;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LoanApplication;
using EPS.Repositories.LoanApplication;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers
{
    #region Loan Application Form
    //created by shivam gupta on 18 Feb 2019
    public class LoanApplicationBAL : ILoanApplicationRepository
    {
        private LoanApplicationDAL objloanDAL;
        private Database epsdatabase;
        private bool disposed = false;

        public LoanApplicationBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objloanDAL = new LoanApplicationDAL(epsdatabase);
        }

        public async Task<List<LoanApplicationDetailsModel>> GetEmpDetailsbyEmpCodeDesignId(string empCode, int msDesignID)
        {
            return await Task.Run(() => objloanDAL.GetAllEmpDetails(empCode, msDesignID));
        }

        #region Get Sanction Details by EmpCode DesignId
        public async Task<IEnumerable<Sanction>> GetSanctionDetailsbyEmpCodeDesignId(string empCode, int msDesignID, int mode)
        {
            return await Task.Run(() => objloanDAL.GetAllSanctionDetails(empCode, msDesignID, mode));
        }
        #endregion

        #region Bind Loan Type
        public async Task<IEnumerable<tblMsPayLoanRef>> BindLoanType(string loantypeFlag)
        {
            return await Task.Run(() => objloanDAL.BindLoanType(loantypeFlag));
        }
        #endregion

        #region Get Emp Loan Details By ID
        public async Task<IEnumerable<LoanApplicationDetailsModel>> GetEmpLoanDetailsByID(string EmpCd)
        {
            return await Task.Run(() => objloanDAL.GetEmpLoanDetailsByID(EmpCd));
        }
        #endregion

        #region Edit Loan Details By EMP ID
        public async Task<IEnumerable<LoanApplicationDetailsModel>> EditLoanDetailsByEMPID(int id)
        {
            return await Task.Run(() => objloanDAL.EditLoanDetailsByEMPID(id)); ;
        }

        #endregion

        #region Delete Loan Details By EMP ID
        public async Task<int> DeleteLoanDetailsByEMPID(int id, int mode)
        {
            return await Task.Run(() => objloanDAL.DeleteLoanDetailsByEMPID(id, mode));
        }
        #endregion

        #region Update Loan Details by ID
        public async Task<int> UpdateLoanDetailsbyID(int id, int mode, string remarks, int priVerifFlag)
        {
            return await Task.Run(() => objloanDAL.UpdateLoanDetailsbyID(id, mode, remarks, priVerifFlag));
        }
        #endregion
        
        #region Get Emp Code
        public async Task<IEnumerable<Employee>> GetEmpCode(string username)
        {
            return await Task.Run(() => objloanDAL.GetEmpcode(username));
        }
        #endregion

        #region Get Emp filter by design Bill group
        public async Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillgroup(string desigId, string BillgrID, string Flag)
        {
            return await Task.Run(() => objloanDAL.GetEmpfilterbydesignBillgroup(desigId, BillgrID, Flag));
        }
        #endregion
        
        #region Get Emp filter by design BillID
        public async Task<IEnumerable<EmpDesigModel>> GetEmpfilterbydesignBillID(string desigId, string BillgrID, string PermDdoid, string Flag)
        {
            return await Task.Run(() => objloanDAL.GetEmpfilterbydesignBillID(desigId, BillgrID, PermDdoid, Flag));
        }
        #endregion

        #region Get Bill group filter by designID
        public async Task<string> GetBillgroupfilterbydesignID(string desigId, string Flag)
        {
            return await Task.Run(() => objloanDAL.GetBillgroupfilterbydesignID(desigId, Flag));
        }
        #endregion

        #region Bind Loan Status
        public async Task<IEnumerable<tblMsLoanStatus>> BindLoanStatus(int mode, int userid,int loginddoid=0)
        {
            return await Task.Run(() => objloanDAL.BindLoanStatus(mode, userid, loginddoid));
        }
        #endregion

        #region Get Fwd to chker Details
        public async Task<IEnumerable<tblMsLoanStatus>> GetFwdtochkerDetails()
        {
            return await Task.Run(() => objloanDAL.GetFwdtochkerDetails());
        }
        #endregion

        #region Loan purpose Selected PayLoanCode
        public async Task<IEnumerable<PayLoanPurpose>> LoanpurposeSelectedPayLoanCode(string payLoanCode,string empCode)
        {
            return await Task.Run(() => objloanDAL.BindLoanpurposeSelectedPayLoanCode(payLoanCode, empCode));
        }
        #endregion

        #region Selected Pay LoanCode purposecode
        public async Task<IEnumerable<PayLoanPurpose>> SelectedPayLoanCodepurposecode(string payLoanCode, string purposeCode)
        {
            return await Task.Run(() => objloanDAL.BindSelectedPayLoanCodepurpsecode(payLoanCode, purposeCode));
        }
        #endregion

        #region Save Loan Details
        public async Task<int> SaveLoanDetails(LoanApplicationDetailsModel objLoanAppDetails)
        {

            return await Task.Run(() => objloanDAL.SaveLoanDetails(objLoanAppDetails));
        }
        #endregion

        #region Update Loan Details by EmpId
        public async Task<int> UpdateLoanDetails(LoanApplicationDetailsModel objLoanAppDetails)
        {

            return await Task.Run(() => objloanDAL.UpdateLoanDetails(objLoanAppDetails));
        }
        #endregion

        #region Bind Loan type and purpose status
        public async Task<IEnumerable<PayLoanPurposeStatus>> BindLoantypeandpurposestatus(string loanBillID)
        {
            return await Task.Run(() => objloanDAL.BindLoantypeandpurposestatus(loanBillID));
        }
        #endregion

        #region Update Sanction Details
        public async Task<int> UpdateSanctionDetails(Sanction objSanction)
        {
            return await Task.Run(() => objloanDAL.UpdateSanctionDetails(objSanction));
        }

        public Task<IEnumerable<LoanApplicationDetailsModel>> GetEmpDetailsbyEmpCodeDesignId()
        {
            throw new System.NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~LoanApplicationBAL()
        {
            Dispose(false);
        }

        #region Get Sanction Details by EmpCode DesignId
        public async Task<bool> ValidateCoolingPeriod(int LoanCode, int PurposeCode, string EmpCd)
        {
            return await Task.Run(() => objloanDAL.ValidateCoolingPeriod(LoanCode, PurposeCode, EmpCd));
        }
        #endregion

    }
    #endregion

}

