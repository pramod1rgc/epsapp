﻿using EPS.BusinessModels.Shared;
using EPS.BusinessModels.Suspension;
using EPS.DataAccessLayers.Suspension;
using EPS.Repositories.Suspension;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Suspension
{
    public class SuspensionBAL: ISuspension
    {
        SuspensionDAL objsusDAL = new SuspensionDAL();

       
        public Task<string> SavedNewSuspensionDetails(SuspensionDetails obj)
        {
            return objsusDAL.SavedNewSuspensionDetails(obj);
        }

        public Task<IEnumerable<DesignationModel>> GetAllDesignation(string id)
        {
            return objsusDAL.GetAllDesignation(id);
        }

        public Task<IEnumerable<EmpModel>> GetAllEmployees(int desigId,bool isCheckerLogin, string comName)
        {
            return objsusDAL.GetAllEmployees(desigId, isCheckerLogin,comName);
        }

        public async Task<IEnumerable<SuspensionDetails>> GetSuspensionDetails(string empCode,bool isCheckerLogin, string comName)
        {
            return await objsusDAL.GetSuspensionDetails(empCode, isCheckerLogin,comName);
        }

        public async Task<string> DeleteSuspensionDetails(int id, string compName)
        {
            return await objsusDAL.DeleteSuspensionDetails(id, compName);
        }

        public async Task<string> VerifiedAndRejectedSuspension(int id, string status, string reason, string compName)
        {
            return await objsusDAL.VerifiedAndRejectedSuspension(id,status,reason,compName);
        }

        public async Task<string> SavedExtensionDetails(ExtensionDetails obj)
        {
            return await objsusDAL.SavedExtensionDetails(obj);
        }

        public async Task<string> SavedRevocationDetails(RevocationDetails objRevok)
        {
            return await objsusDAL.SavedRevocationDetails(objRevok);
        }

        public async Task<IEnumerable<RevocationDetails>> GetRevocationDetails(int susId,int empId, bool isCheckerLogin)
        {
            return await objsusDAL.GetRevocationDetails(susId,empId, isCheckerLogin);
        }

        public Task<string> SavedJoiningDetails(JoiningDetails objJoining)
        {
            return objsusDAL.SavedJoiningDetails(objJoining);
        }

        public Task<IEnumerable<JoiningDetails>> GetJoiningDetails(int joinId, bool isCheckerLogin)
        {
            return objsusDAL.GetJoiningDetails(joinId, isCheckerLogin);
        }

        public Task<string> SavedRegulariseDetails(RegulariseDetailse objReg)
        {
            return objsusDAL.SavedRegulariseDetails(objReg);
        }

        public Task<IEnumerable<RegulariseDetailse>> GetRegulariseDetails(int joiningId, bool isCheckerLogin)
        {
            return objsusDAL.GetRegulariseDetails(joiningId, isCheckerLogin);
        }

       
    }
}
