﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers
{
    public class LoginBusinessAccessLayer : ILoginRepository
    {
        public LoginDataAccessLayer loginDataAccessLayer; 
        public Database epsdatabase;
        public LoginBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            loginDataAccessLayer = new LoginDataAccessLayer(epsdatabase);
        }
        public async Task<string> LoginCheck(LoginModel loginModel)
        {
            return await Task.Run(() => loginDataAccessLayer.LoginCheck(loginModel)).ConfigureAwait(true);
        }
        public async Task<List<userDetails>> UserDetails(string username)
        {
            return await Task.Run(()=> loginDataAccessLayer.UserDetails(username)).ConfigureAwait(true);
        }
        public async Task<string> LoginNewUser(LoginModel loginModel)
        {
            return await Task.Run(() => loginDataAccessLayer.LoginNewUser(loginModel)).ConfigureAwait(true);
        }
        public async Task<string> RoleActivation(int ID)
        {
            return await Task.Run(() => loginDataAccessLayer.RoleActivation(ID)).ConfigureAwait(true);
        }
        public async Task<bool> IsMenuPermissionAssigned(string Username, string uroleid)
        {
            //return loginDataAccessLayer.IsMenuPermissionAssigned(Username, uroleid);
            return await Task.Run(() => loginDataAccessLayer.IsMenuPermissionAssigned(Username, uroleid)).ConfigureAwait(true);
        }
    }
}
