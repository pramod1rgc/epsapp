﻿using EPS.BusinessModels.IncomeTax;
using EPS.CommonClasses;
using EPS.DataAccessLayers.IncomeTax;
using EPS.Repositories.IncomeTax;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.IncomeTax
{
    public class ExemptionDeductionBAL : IExemptionDeductionRepository
    {
        private Database epsDatabase;
        private ExemptionDeductionDAL objDAL;
        private bool disposed = false;

        public ExemptionDeductionBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<List<MajorSectionCode>> GetMajorSectionCode()
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<MajorSectionCode> objMajorSecCodeList = new List<MajorSectionCode>();

            objMajorSecCodeList = await Task.Run(() => objDAL.GetMajorSectionCode());

            return objMajorSecCodeList;
        }

        public async Task<List<SectionCode>> GetSectionCode(string majorSecCode)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<SectionCode> objSecCodeList = new List<SectionCode>();

            objSecCodeList = await Task.Run(() => objDAL.GetSectionCode(majorSecCode));

            return objSecCodeList;
        }

        public async Task<List<SubSectionCode>> GetSubSectionCode(string majorSecCode, string secCode)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<SubSectionCode> objSubSecCodeList = new List<SubSectionCode>();

            objSubSecCodeList = await Task.Run(() => objDAL.GetSubSectionCode(majorSecCode, secCode));

            return objSubSecCodeList;
        }

        public async Task<List<SectionCodeDescription>> GetSectionCodeDescription(string majorSecCode, string sectionCode, string subSectionCode)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<SectionCodeDescription> objSecCodeDescList = new List<SectionCodeDescription>();

            objSecCodeDescList = await Task.Run(() => objDAL.GetSectionCodeDescription(majorSecCode, sectionCode, subSectionCode));

            return objSecCodeDescList;
        }

        public async Task<List<SlabLimit>> GetSlabLimit(string payCommission)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<SlabLimit> objSlabLimitList = new List<SlabLimit>();

            objSlabLimitList = await Task.Run(() => objDAL.GetSlabLimit(payCommission));

            return objSlabLimitList;
        }

        public async Task<List<PayCommission>> GetPayCommission()
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<PayCommission> objPayCommissionList = new List<PayCommission>();

            objPayCommissionList = await Task.Run(() => objDAL.GetPayCommission());

            return objPayCommissionList;
        }

        public async Task<List<ExemptionDeductionDetails>> GetExemptionDeductionDetails(int pageNumber, int pageSize, string searchTerm)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            List<ExemptionDeductionDetails> objDataList = new List<ExemptionDeductionDetails>();

            objDataList = await Task.Run(() => objDAL.GetExemptionDeductionDetails(pageNumber, pageSize, searchTerm));

            return objDataList;
        }

        public async Task<string> UpsertExemptionDeductionDetails(ExemptionDeductionDetails objExemptionDeduction)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            var result = await Task.Run(() => objDAL.UpsertExemptionDeductionDetails(objExemptionDeduction));

            return result;
        }

        public async Task<int> DeleteExemptionDeductionDetails(int drrId, int drrId2)
        {
            objDAL = new ExemptionDeductionDAL(epsDatabase);
            var result = await Task.Run(() => objDAL.DeleteExemptionDeductionDetails(drrId, drrId2));

            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    // Free any other managed objects here
                    objDAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~ExemptionDeductionBAL()
        {
            Dispose(false);
        }
    }
}
