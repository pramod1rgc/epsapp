﻿using EPS.CommonClasses;
using EPS.DataAccessLayers.IncomeTax;
using EPS.Repositories.IncomeTax;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static EPS.BusinessModels.IncomeTax.OtherDuesModel;

namespace EPS.BusinessAccessLayers.IncomeTax
{
    public class OtherDuesBAL : IOtherDuesRepository
    {
        private Database epsDatabase;
        private OtherDuesDAL objDAL;
        private bool disposed = false;

        public OtherDuesBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<List<MajorSecCode>> GetMajorSectionCode()
        {
            objDAL = new OtherDuesDAL(epsDatabase);
            List<MajorSecCode> objMajorSecCodeList = new List<MajorSecCode>();

            objMajorSecCodeList = await Task.Run(() => objDAL.GetMajorSectionCode()); ;

            return objMajorSecCodeList;
        }

        public async Task<List<OtherDues>> GetOtherDues(bool allDues)
        {
            objDAL = new OtherDuesDAL(epsDatabase);
            List<OtherDues> objOtherDuesList = new List<OtherDues>();

            objOtherDuesList = await Task.Run(() => objDAL.GetOtherDues(allDues)); ;

            return objOtherDuesList;
        }

        public async Task<List<OtherDuesDetails>> GetOtherDuesDetails(int pageNumber, int pageSize, string searchTerm)
        {
            objDAL = new OtherDuesDAL(epsDatabase);
            List<OtherDuesDetails> objDataList = new List<OtherDuesDetails>();

            objDataList = await Task.Run(() => objDAL.GetOtherDuesDetails(pageNumber, pageSize, searchTerm));

            return objDataList;
        }

        public async Task<string> SaveOtherDuesDetails(OtherDuesDetails objOtherDuesDetails)
        {
            objDAL = new OtherDuesDAL(epsDatabase);
            var result = await Task.Run(() => objDAL.SaveOtherDuesDetails(objOtherDuesDetails));

            return result;
        }

        public async Task<int> DeleteOtherDuesDetails(int duesId, string userName, string userIp)
        {
            objDAL = new OtherDuesDAL(epsDatabase);
            var result = await Task.Run(() => objDAL.DeleteOtherDuesDetails(duesId, userName, userIp));

            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    // Free any other managed objects here
                    objDAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~OtherDuesBAL()
        {
            Dispose(false);
        }

    }
}
