﻿using EPS.BusinessModels.IncomeTax;
using EPS.DataAccessLayers.IncomeTax;
using EPS.Repositories.IncomeTax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.IncomeTax
{
    public class StandardDeductionBAL : IStandardDeductionRepository
    {
        StandardDeductionDAL objDal = new StandardDeductionDAL();
        private bool disposed = false;

        public async Task<IEnumerable<StandardDeductionDetails>> GetStandardDeductionRuleBAL(int PageNumber, int PageSize, string SearchTerm)
        {
            List<StandardDeductionDetails> stanDeduList = new List<StandardDeductionDetails>();

            var listOfDetails = await objDal.GetStandardDeduction(PageNumber, PageSize, SearchTerm);

            var groupedData = (from a in listOfDetails
                               group a by new { a.MsSdrID, a.SerSecCode, a.SerFinFyr, a.SerFinTyr, a.SerRateFor, a.TotalCount } into g
                               select g).ToList();

            foreach (var data in groupedData)
            {
                StandardDeductionDetails stanDedu = new StandardDeductionDetails();
                stanDedu.MsSdrID = data.Key.MsSdrID;
                stanDedu.SerSecCode = data.Key.SerSecCode;
                stanDedu.SerFinTyr = data.Key.SerFinTyr;
                stanDedu.SerFinFyr = data.Key.SerFinFyr;
                stanDedu.SerRateFor = data.Key.SerRateFor;
                stanDedu.TotalCount = data.Key.TotalCount;
                stanDedu.RateDetails = new List<RateDetailsModel>();
                int i = 0;
                foreach (var value in data.ToList())
                {
                    i++;
                    RateDetailsModel rate = new RateDetailsModel();
                    rate.Id = i;
                    rate.MsSdentRuleID = value.MsSdentRuleID;
                    rate.SerLLimit = value.SerLLimit;
                    rate.SerULimit = value.SerULimit;
                    rate.SerPercValue = value.SerPercValue;
                    rate.SerValue = value.SerValue;

                    stanDedu.RateDetails.Add(rate);
                }
                stanDeduList.Add(stanDedu);
            }
            return stanDeduList;
        }

        public async Task<IEnumerable<GetStandardDeductionEntertainMasterDetail>> GetStandardDeductionEntertainMasterBAL()
        {
            return await objDal.GetStandardDeductionEntertainMasterDAL();
        }

        public async Task<string> UpsertStandardDeductionBAL(UpsertStandardDeductionDetails obj)
        {
            //obj.IpAddress = CommonClasses.CommonFunctions.GetIP();
            return await objDal.UpsertStandardDeductionDAL(obj);
        }

        public async Task<string> DeleteStandardDeductionBAL(DeleteStandardDeductionDetails obj)
        {
            return await objDal.DeleteStandardDeductionRuleDAL(obj);
        }

        public async Task<string> DeleteStandardDeductionRateDetailBAL(int id)
        {
            return await objDal.DeleteStandardDeductionRateDetailDAL(id);
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    // Free any other managed objects here
                    objDal = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~StandardDeductionBAL()
        {
            Dispose(false);
        }
    }
}
