﻿using EPS.BusinessModels.IncomeTax;
using EPS.DataAccessLayers.IncomeTax;
using EPS.Repositories.IncomeTax;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.IncomeTax
{
    public class ITRatesBAL : IITRatesRepository
    {
        private ITRatesDAL objDal = new ITRatesDAL();
        private bool disposed = false;

        public async Task<IEnumerable<GetITRatesDetails>> GetITRatesBAL(string FinYear, string RateType, string RateFor)
        {
            return await objDal.GetITRatesDAL(FinYear, RateType, RateFor);
        }

        public async Task<IEnumerable<ITRateDetailStatus>> UpsertITRatesBAL(UpsertITRatesDetails obj)
        {
            return await objDal.UpsertITRatesDAL(obj);
        }

        public async Task<string> DeleteITRatesBAL(int id, string ip, int modifiedBy)
        {
            return await objDal.DeleteITRatesDAL(id,ip,modifiedBy);
        }

        public async Task<IEnumerable<RateType>> GetRateTypeAndRateForBAL(string type)
        {
            return await objDal.GetAllRateTypeAndRateForDAL(type);
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    // Free any other managed objects here
                    objDal = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~ITRatesBAL()
        {
            Dispose(false);
        }
    }
}
