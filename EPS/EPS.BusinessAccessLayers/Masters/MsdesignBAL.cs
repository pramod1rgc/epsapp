﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class MsdesignBAL
    {
        Database epsdatabase;
        public MsdesignBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task< List<MsdesignBM>> GetMsdesign()
        {
            MsdesignDAL _DAL = new MsdesignDAL(epsdatabase);
            return await Task.Run(() => _DAL.GetCommondesign()).ConfigureAwait(true);
        }

        public async Task<int> GetMsdesignMaxid()
        {
                MsdesignDAL _DAL = new MsdesignDAL(epsdatabase);
                return await Task.Run(() => _DAL.GetMsdesignMaxId()).ConfigureAwait(true);
        }

        public string UpdateMsdesign(MsdesignBM BM)
        {
            string Respons = null;
            //try
            //{
            //    //List<MsdesignBM> MsdesignList = null;

                MsdesignDAL _DAL = new MsdesignDAL(epsdatabase);
                Respons = _DAL.UpdateMsdesign(BM);

                return Respons;
            //}
            //catch (Exception ex)
            //{
            //    string ma = ex.Message;
            //    return ma;
            //}
        }

        public string InsertMsdesign(MsdesignBM BM)
        {
            string Respons = null;
            //try
            //{
                MsdesignDAL _DAL = new MsdesignDAL(epsdatabase);
                Respons = _DAL.InsertMsdesign(BM);

                return Respons;
            //}
            //catch (Exception ex)
            //{
            //    string ma = ex.Message;
            //    return ma;
            //}
        }

        public string DeleteMsdesign(int CommonDesigSrNo)
        {
            string Respons = null;
            //try
            //{
                MsdesignDAL _DAL = new MsdesignDAL(epsdatabase);
                Respons = _DAL.DeleteMsdesign(CommonDesigSrNo);

                return Respons;
            //}
            //catch (Exception ex)
            //{
            //    string ma = ex.Message;
            //    return ma;
            //}
        }
    }
}
