﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;

namespace EPS.BusinessAccessLayers.Masters
{
    public class DAStateCentralBAL
    {
       readonly  Database EpsDatabase;
        public DAStateCentralBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region
        public IEnumerable<DAStateCentralBM> GetDADetails()
        {
            DAStateCentralDAL objda = new DAStateCentralDAL(EpsDatabase);
            return objda.GetDADetails();
        }
        #endregion

        public DAStateCentralBM GetDACurrentRateWef(string DaType,string StateId)
        {
            DAStateCentralDAL objda = new DAStateCentralDAL(EpsDatabase);
            return objda.GetDACurrentRateWef(DaType, StateId);
        }
        #region
        //================CreateDa==================================
        public string InsertandUpdate(DAStateCentralBM _objda)
        {
            DAStateCentralDAL objda = new DAStateCentralDAL(EpsDatabase);
            return objda.InsertandUpdate(_objda);

        }
        #endregion
        #region
        public string Delete(int MsDaMasterID)
        {
            DAStateCentralDAL objda = new DAStateCentralDAL(EpsDatabase);
            return objda.Delete(MsDaMasterID);
        }
        #endregion
    }
}
