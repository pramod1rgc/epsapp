﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class GpfAdvanceRulesBAL: IGpfAdvanceRulesRepository
    {
        Database epsDataBase;
        /// <summary>
        /// GpfAdvanceRulesBAL
        /// </summary>
        public GpfAdvanceRulesBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        /// <summary>
        /// InsertUpdateAdvanceRulesDetails
        /// </summary>
        /// <param name="ObjGpfWithdrawRules"></param>
        /// <returns></returns>
        public async Task<int> InsertUpdateAdvanceRulesDetails(GpfAdvanceRulesBM objGpfWithdrawRules)
        {
            GpfAdvanceRulesDAL objGpfWithdrawRulesDA = new GpfAdvanceRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.InsertUpdateAdvanceRulesDetails(objGpfWithdrawRules));
            int result = await mytask;
            return result;
        }
        /// <summary>
        /// GetGpfWithdrawRulesById
        /// </summary>
        /// <param name="MsGpfWithdrawRulesRefID"></param>
        /// <returns></returns>
        public async Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRulesById(int msGpfWithdrawRulesRefID)
        {
            GpfAdvanceRulesDAL objGpfWithdrawRulesDA = new GpfAdvanceRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfAdvanceRulesById(msGpfWithdrawRulesRefID));
            List<GpfAdvanceRulesBM> result = await mytask;
            return result;
        }
        /// <summary>
        /// GetGpfWithdrawRules
        /// </summary>
        /// <returns></returns>
        public async Task<List<GpfAdvanceRulesBM>> GetGpfAdvanceRules()
        {
            GpfAdvanceRulesDAL objGpfWithdrawRulesDA = new GpfAdvanceRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfAdvanceRules());
            List<GpfAdvanceRulesBM> result = await mytask;
            return result;
        }
        /// <summary>
        /// DeleteGpfWithdrawRules
        /// </summary>
        /// <param name="MsGpfAdvanceRulesRefID"></param>
        /// <param name="IsFleg"></param>
        /// <returns></returns>
        public async Task<int> DeleteGpfAdvanceRules(int msGpfAdvanceRulesRefID,string isFleg)
        {
            GpfAdvanceRulesDAL objGpfWithdrawRulesDA = new GpfAdvanceRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.DeleteGpfAdvanceRules(msGpfAdvanceRulesRefID, isFleg));
            int result = await mytask;
            return result;
        }
    }
}
