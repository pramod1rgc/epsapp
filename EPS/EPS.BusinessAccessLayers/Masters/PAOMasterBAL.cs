﻿using System;
using System.Collections.Generic;
using EPS.BusinessModels.Masters;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using EPS.CommonClasses;
using System.Threading.Tasks;
using EPS.Repositories.Masters;
namespace EPS.BusinessAccessLayers.Masters
{
    public class PAOMasterBAL : IPAOMaster
    {
        private Database EpsDatabase;
        public PAOMasterBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region Get Pao Master

        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails()
        {
            PAOMasterDAL pAOMasterDAL = new PAOMasterDAL(EpsDatabase);
            return await Task.Run(() => pAOMasterDAL.GetMSPAODetails()).ConfigureAwait(true);
        }
        #endregion
        #region Get Pao Master by MsPaoId

        public async Task<IEnumerable<PAOMasterBM>> GetMSPAODetails(int MspaoId)
        {

            PAOMasterDAL pAOMasterDAL = new PAOMasterDAL(EpsDatabase);
            return await Task.Run(() => pAOMasterDAL.GetMSPAODetails(MspaoId)).ConfigureAwait(true);

        }
        #endregion

        #region Update Master MSPAO

        public async Task<string> UpdateMSPAO(PAOMasterBM PAOMasterBM)
        {
            PAOMasterDAL pAOMasterDAL = new PAOMasterDAL(EpsDatabase);
            return await Task.Run(() => pAOMasterDAL.UpdateMSPAO(PAOMasterBM));
        }
        #endregion
        #region Insert Master MSPAO

        public async Task<string> InsertMSPAO(PAOMasterBM PAOMasterBM)
        {

            PAOMasterDAL pAOMasterDAL = new PAOMasterDAL(EpsDatabase);
            return await Task.Run(() => pAOMasterDAL.InsertMSPAO(PAOMasterBM));

        }
        #endregion
        #region delete Master MSPAO

        public async Task<string> DeleteMSPAO(int msleavetypeId)
        {
            PAOMasterDAL pAOMasterDAL = new PAOMasterDAL(EpsDatabase);
            return await Task.Run(() => pAOMasterDAL.DeleteMSPAO(msleavetypeId));

        }
        #endregion

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            // If disposing equals true, dispose all managed and unmanaged resources 
            if (disposing)
            {
                // Free any other managed objects here
                EpsDatabase = null;
            }
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~PAOMasterBAL()
        {
            Dispose(false);
        }
    }
}
