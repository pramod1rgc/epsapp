﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class DeductionMasterBAL: IDeductionMasterRepository
    {
        public DeductionMasterDAL ObjDeductionMasterDAL;
        public Database epsdatabase;
        public DeductionMasterBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            ObjDeductionMasterDAL = new DeductionMasterDAL(epsdatabase);
        }
        
        public async Task<List<DeductionMasterBM>> CategoryList()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.CategoryList()).ConfigureAwait(true);
        }

        public async Task<List<DeductionDescriptionBM>> DeductionDescriptionChange(int payItemsCD)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.DeductionDescriptionChange(payItemsCD)).ConfigureAwait(true);
        }
        
        public async Task<List<DeductionDescriptionBM>> DeductionDescription()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.DeductionDescription()).ConfigureAwait(true);
        }

        public async Task<List<DeductionDescriptionBM>> GetDeductionDefinitionList()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetDeductionDefinitionList()).ConfigureAwait(true);
        }

        public async Task<int> DeductionDefinitionSubmit(DeductionDescriptionBM ObjDuesDefinationandDuesMasterModel)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.DeductionDefinitionSubmit(ObjDuesDefinationandDuesMasterModel)).ConfigureAwait(true);
        }

        //==============================================Deduction Rate Master============================================

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetOrgnazationTypeForDues()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetOrgnazationTypeForDues()).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetDuesCodeDuesDefination()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetDuesCodeDuesDefination()).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetCityClassForDuesRate(string payCommId)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetCityClassForDuesRate(payCommId)).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetStateForDuesRate()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetStateForDuesRate()).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetPayCommForDuesRate()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetPayCommForDuesRate()).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetSlabtypeByPayCommId(string payCommId)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetSlabtypeByPayCommId(payCommId)).ConfigureAwait(true);
        }

        public async Task<int> InsertUpdateDuesRateDetails(DeductionRateAndDeductionMasterModel objDuesRateandDuesMasterModel)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.InsertUpdateDuesRateDetails(objDuesRateandDuesMasterModel)).ConfigureAwait(true);
        }

        public async Task<IEnumerable<DeductionRateAndDeductionMasterModel>> GetDuesCodeDuesRate()
        {
            return await Task.Run(() => ObjDeductionMasterDAL.GetDuesRateDetails1()).ConfigureAwait(true);
        }

        public async Task<int> DeleteDuesRateDetails(string msduesratedetails)
        {
            return await Task.Run(() => ObjDeductionMasterDAL.DeleteDuesRateDetails(msduesratedetails)).ConfigureAwait(true);
        }

       
    }
}
