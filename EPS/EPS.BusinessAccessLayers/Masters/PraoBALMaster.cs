﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
   public class PraoBALMaster: IpraoMasterRepository
    {

        private Database EpsDatabase;
        private PraoMasterDAL objPraoMasterDAL;
        private bool disposed = false;

        public PraoBALMaster()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);

           objPraoMasterDAL = new PraoMasterDAL(EpsDatabase);
        }

        public async Task<IEnumerable<PraoMasterModel>> GetAllController()
        {
            IEnumerable<PraoMasterModel> result = await Task.Run(() => objPraoMasterDAL.GetAllController());

            return result;
        }
        public async Task<IEnumerable<PraoMasterModel>> BindCtrlName(string CtrlrCode)
        {
            IEnumerable<PraoMasterModel> result = await Task.Run(() => objPraoMasterDAL.BindCtrlName(CtrlrCode));
            return result;
        }
        public async Task<IEnumerable<PraoMasterModel>> BindState()
        {
            IEnumerable<PraoMasterModel> result = await Task.Run(() => objPraoMasterDAL.BindState());
            return result;
        }
        public async Task<IEnumerable<PraoMasterModel>> BindDistrict(int stateId)
        {
            IEnumerable<PraoMasterModel> result = await Task.Run(() => objPraoMasterDAL.BindDistrict(stateId));
            return result;
        }
        public async Task<int> SaveOrUpdate(PraoMasterModel model)
        {
            int result = await Task.Run(() => objPraoMasterDAL.SaveOrUpdate(model));
            return result;
        }
        public async Task<IEnumerable<ProMasterDetails>> BindPraoMasterDetails()
        {
            IEnumerable<ProMasterDetails> result = await Task.Run(() => objPraoMasterDAL.BindPraoMasterDetails());

            return result;
        }
        public async Task<IEnumerable<PraoMasterModel>> ViewEditOrDeletePrao(string CtrlCode, string controllerID, int Mode)
        {
            IEnumerable<PraoMasterModel> result = await Task.Run(() => objPraoMasterDAL.ViewEditOrDeletePrao(CtrlCode, controllerID, Mode));

            return result;
        }


        





        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    objPraoMasterDAL = null;
                    EpsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }



        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~PraoBALMaster()
        {
            Dispose(false);
        }
    }
}
