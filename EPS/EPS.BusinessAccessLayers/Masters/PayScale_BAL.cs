﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public  class PayScale_BAL
    {
        //Payscale_DAL objpayScaleDAL = new Payscale_DAL();
       private Database EpsDatabase;
        #region
        public PayScale_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #endregion
        #region
        /// <summary>
        /// BindCommissionCode
        /// </summary>
        /// <returns></returns>
        public async Task<List<PayScaleModel>> BindCommissionCode()
        {
            Payscale_DAL objPayscaleDAL = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => objPayscaleDAL.BindCommissionCode());
            List<PayScaleModel> result = await mytask;
            return result;
        }
        #endregion
        #region
        /// <summary>
        /// BindPayScaleGroup
        /// </summary>
        /// <returns></returns>
        public async Task<List<PayScaleModel>> BindPayScaleGroup()
        {
            Payscale_DAL Paydal = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => Paydal.BindPayScaleGroup());
            List<PayScaleModel> result = await mytask;
            return result;
        }
        #endregion
        #region
        public async Task<List<PayScaleModel>> GetPayScaleDetails()
        {
            Payscale_DAL Paydal = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => Paydal.GetPayScaleDetails());
            List<PayScaleModel> result = await mytask;
            return result;
        }

        #endregion
        #region
        /// <summary>
        /// BindPayScaleFormat
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PayScaleModel> BindPayScaleFormat()
        {  
                Payscale_DAL Paydal = new Payscale_DAL(EpsDatabase);
                return Paydal.BindPayScaleFormat();  
        }
        #endregion
        #region
        /// <summary>
        /// PayScaleCode
        /// </summary>
        /// <param name="CddirCodeValue"></param>
        /// <returns></returns>
        public async Task<string> PayScaleCode(string cddirCodeValue)
        {
            Payscale_DAL Paydal = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => Paydal.payScaleCode(cddirCodeValue));
            var result = await mytask;
            return result;
        }



        #endregion
        #region
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="MsPayScaleID"></param>
        /// <returns></returns>
        public async Task<string> Delete(int MsPayScaleID)
        {
            Payscale_DAL objPayscale = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => objPayscale.Delete(MsPayScaleID));
            string result = await mytask;
            return result;
        }
        #endregion
        #region
        /// <summary>
        /// CreatePayScale
        /// </summary>
        /// <param name="payscale"></param>
        /// <returns></returns>
        public async Task<string> InsertUpdatePayScale(PayScaleModel payscale)
        {
            Payscale_DAL objPayscale = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => objPayscale.InsertUpdatePayScale(payscale));
            string result = await mytask;
            return result;
        }
        #endregion


        #region
        /// <summary>
        /// EditPayScaleDetails
        /// </summary>
        /// <param name="msPayScaleID"></param>
        /// <returns></returns>
        public async Task<PayScaleModel> EditPayScaleDetails(string msPayScaleID)
        {
            Payscale_DAL objPayscale = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => objPayscale.EditPayScaleDetails(msPayScaleID));
          PayScaleModel result = await mytask;
            return result;
        }

        #endregion
        #region
        /// <summary>
        /// GetPayScaleDetailsBYComCode
        /// </summary>
        /// <param name="CommCDID"></param>
        /// <returns></returns>
        public async Task<List<PayScaleModel>> GetPayScaleDetailsBYComCode(string CommCDID)
        {
            Payscale_DAL Paydal = new Payscale_DAL(EpsDatabase);
            var mytask = Task.Run(() => Paydal.GetPayScaleDetailsBYComCode(CommCDID));
            List<PayScaleModel> result = await mytask;
            return result;
        }
        #endregion
  
    }
}
