﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public  class GpfAdvanceReasonsBAL
    {
        readonly Database EpsDatabase;
        public GpfAdvanceReasonsBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region
        /// <summary>
        /// GetGPFAdvanceReasonsDetails
        /// </summary>
        /// <param name="AdvWithdraw"></param>
        /// <returns></returns>
        public IEnumerable<GpfAdvanceReasonsBM> GetGPFAdvanceReasonsDetails(string AdvWithdraw)
        {
            GpfAdvanceReasonsDAL objgpf = new GpfAdvanceReasonsDAL(EpsDatabase);
            return objgpf.GetGPFAdvanceReasonsDetails(AdvWithdraw);
        }
        #endregion

        #region
        /// <summary>
        /// BindMainReason
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GpfAdvanceReasonsBM> BindMainReason()
        {
            GpfAdvanceReasonsDAL objgpf = new GpfAdvanceReasonsDAL(EpsDatabase);
            return objgpf.BindMainReason();
        }

        #endregion
        #region
        /// <summary>
        /// InsertandUpdate
        /// </summary>
        /// <param name="_objgpf"></param>
        /// <returns></returns>
        public async Task<string> InsertandUpdate(GpfAdvanceReasonsBM modelobjgpf)
        {
            GpfAdvanceReasonsDAL objgpf = new GpfAdvanceReasonsDAL(EpsDatabase);
            var mytask = Task.Run(() => objgpf.InsertandUpdate(modelobjgpf));
            string result = await mytask;
            return result;

        }
        #endregion
        #region
        public string Delete(int pfRefNo)
        {
            GpfAdvanceReasonsDAL objgpf = new GpfAdvanceReasonsDAL(EpsDatabase);
            return objgpf.Delete(pfRefNo);
        }
        #endregion
    }
}
