﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Masters;


namespace EPS.BusinessAccessLayers.Masters
{
   public class GpfWithdrawRulesBA: IGpfWithdrawRuleRepository
    {
        private  Database epsDataBase;
        public GpfWithdrawRulesBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<int> InsertUpdateEmpDetails(GpfWithdrawRules ObjGpfWithdrawRules)
        {
            GpfWithdrawRulesDA objGpfWithdrawRulesDA = new GpfWithdrawRulesDA(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.InsertUpdateGpfWithdrawRules(ObjGpfWithdrawRules));
            int result = await mytask;
            return result;

        }

        public async Task<List<GpfWithdrawRules>> GetGpfWithdrawRulesById(int MsGpfWithdrawRulesRefID)
        {
            GpfWithdrawRulesDA objGpfWithdrawRulesDA = new GpfWithdrawRulesDA(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfWithdrawRulesById(MsGpfWithdrawRulesRefID));
            List<GpfWithdrawRules> result = await mytask;
            return result;
        }

        public async Task<List<GpfWithdrawRules>> GetGpfWithdrawRules()
        {
            GpfWithdrawRulesDA objGpfWithdrawRulesDA = new GpfWithdrawRulesDA(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfWithdrawRules());
            List<GpfWithdrawRules> result = await mytask;
            return result;
        }
      
        public async Task<int> DeleteGpfWithdrawRules(int MsGpfWithdrawRulesRefID)
        {
            GpfWithdrawRulesDA objGpfWithdrawRulesDA = new GpfWithdrawRulesDA(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.DeleteGpfWithdrawRules(MsGpfWithdrawRulesRefID));
           int result = await mytask;
            return result;
        }
    }
}
