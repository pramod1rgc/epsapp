﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class DuesDefinationandDuesMaster_BAL : IDuesDefinationandDuesMaster
    {
        private Database EpsDatabase;
        private bool disposed = false;

        public DuesDefinationandDuesMaster_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<int> InsertUpdateDuesDefinationMasterDetails(DuesDefinationandDuesMasterModel ObjDuesDefinationandDuesMasterModel)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.InsertUpdateDuesDefinationDetails(ObjDuesDefinationandDuesMasterModel));
            int result = await mytask;
            return result;

        }

        public async Task<IEnumerable<DuesDefinationandDuesMasterModel>> GetDuesDefinationandDuesMasterList()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetDuesDefinationmaster());
            IEnumerable<DuesDefinationandDuesMasterModel> result = await mytask;
            return result;

        }

        public async Task<int> DeleteDuesDefinationmasterDetails(string payitemCd)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.DeleteEmployeeDetails(payitemCd));
            int result = await mytask;
            return result;
        }

        public async Task<DuesDefinationandDuesMasterModel> GetDuesDefinationandDuesMasterByID(string DuesCd)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetDuesDefinationandDuesMasterByID(DuesCd));
            DuesDefinationandDuesMasterModel result = await mytask;
            return result;
        }

        public async Task<DuesDefinationandDuesMasterModel> GetAutoGenratedDuesCode()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetAutoGenratedDuesCode());
            DuesDefinationandDuesMasterModel result = await mytask;
            return result;
        }

        //===========================DuesRete=============================================

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetOrgnazationTypeForDues()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetOrgnazationTypeForDues());
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetDuesCodeDuesDefination()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetDuesCodeDuesDefination());
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetCityClassForDuesRate(string payCommId)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetCityClassForDuesRate(payCommId));
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetStateForDuesRate()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetStateForDuesRate());
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetPayCommForDuesRate()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetPayCommForDuesRate());
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetSlabtypeByPayCommId(string payCommId)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetSlabtypeByPayCommId(payCommId));
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<int> InsertUpdateDuesRateDetails(DuesRateandDuesMasterModel objDuesRateandDuesMasterModel)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DA = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var myTask = Task.Run(() => objDuesDefinationandDuesMaster_DA.InsertUpdateDuesRateDetails(objDuesRateandDuesMasterModel));
            int result = await myTask;
            return result;
        }

        public async Task<IEnumerable<DuesRateandDuesMasterModel>> GetDuesCodeDuesRate()
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.GetDuesRateDetails1());
            IEnumerable<DuesRateandDuesMasterModel> result = await mytask;
            return result;
        }

        public async Task<int> DeleteDuesRateDetails(string msduesratedetails)
        {
            DuesDefinationandDuesMaster_DAL objDuesDefinationandDuesMaster_DAL = new DuesDefinationandDuesMaster_DAL(EpsDatabase);
            var mytask = Task.Run(() => objDuesDefinationandDuesMaster_DAL.DeleteDuesRateDetails(msduesratedetails));
            int result = await mytask;
            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    // Free any other managed objects here
                    EpsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~DuesDefinationandDuesMaster_BAL()
        {
            Dispose(false);
        }

    }
}
