﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Masters;

namespace EPS.BusinessAccessLayers.Masters
{
   public class GpfDlisExceptionsBAL: IGpfDlisExceptionsRepository
    {
        private Database epsDataBase;
        public GpfDlisExceptionsBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<int> InsertUpdateGpfDlisExceptions(GpfDlisExceptions objGpfDlisExceptions)
        {
            GpfDlisExceptionsDAL objGpfDlisExceptionsDA = new GpfDlisExceptionsDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfDlisExceptionsDA.InsertUpdateGpfDlisExceptions(objGpfDlisExceptions));
            int result = await mytask;
            return result;

        }
        public async Task<List<GpfDlisExceptions>> GetGpfDlisExceptions()
        {
            GpfDlisExceptionsDAL objGpfDlisExceptionsDA = new GpfDlisExceptionsDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfDlisExceptionsDA.GetGpfDlisExceptions());
            List<GpfDlisExceptions> result = await mytask;
            return result;
        }
        public async Task<List<PfType>> GetPfType()
        {
            GpfDlisExceptionsDAL objGpfDlisExceptionsDA = new GpfDlisExceptionsDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfDlisExceptionsDA.GetPfType());
            List<PfType> result = await mytask;
            return result;
        }
        public async Task<List<PayScale>> GetPayScale(int PayCommId)
        {
            GpfDlisExceptionsDAL objGpfDlisExceptionsDA = new GpfDlisExceptionsDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfDlisExceptionsDA.GetPayScale(PayCommId));
            List<PayScale> result = await mytask;
            return result;
        }
        public async Task<int> DeleteGpfDlisExceptions(int MsGpfDlisExceptionsRefID)
        {
            GpfDlisExceptionsDAL objGpfDlisExceptionsDA = new GpfDlisExceptionsDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfDlisExceptionsDA.DeleteGpfDlisExceptions(MsGpfDlisExceptionsRefID));
            int result = await mytask;
            return result;
        }
    }
}
