﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessAccessLayers.Masters
{
   public class MsStateAgBAL
    {
        Database epsdatabase;
        public MsStateAgBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public List<MsStateAgBM> GetMsStateAg()
        {
            List<MsStateAgBM> MsStateAgList = null;

            MsStateAgDAL _DAL = new MsStateAgDAL(epsdatabase);
            MsStateAgList = _DAL.GetMsStateAg();
            return MsStateAgList;

        }
        public List<StateBM> GetMsState()
        {
            List<StateBM> MsStateList = null;

            MsStateAgDAL _DAL = new MsStateAgDAL(epsdatabase);
            MsStateList = _DAL.GetMsState();
            return MsStateList;
        }
        public List<PfTypeBM>GetMsPfType()
        {
            List<PfTypeBM> MsPfType_List=null;
            MsStateAgDAL _DAL = new MsStateAgDAL(epsdatabase);
            MsPfType_List = _DAL.GetMsPfType();
            return MsPfType_List;
        }

        public string UpdateMsStateAg(MsStateAgBM BM)
        {

            string Respons = null;
            try
            {
                //List<MsStateAgBM> MsStateAgList = null;

                MsStateAgDAL _DAL = new MsStateAgDAL(epsdatabase);
                Respons = _DAL.UpdateMsStateAg(BM);

                return Respons;
            }
            catch (Exception ex)
            {
                string ma = ex.Message;
                return Respons;
            }

        }
        public string InsertMsStateAg(MsStateAgBM BM)
        {

            string Respons = null;
            try
            {

     
                MsStateAgDAL _DAL = new MsStateAgDAL(epsdatabase);
                Respons = _DAL.InsertMsStateAg(BM);

                return Respons;
            }
            catch (Exception ex)
            {
                string ma = ex.Message;
                return Respons;
            }

        }
    }
}
