﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class Citymaster_BAL : ICityMasterRepository
    {
        private Database EpsDatabase;
        private CityMaster_DAL objCityMaster_DAL;
        private bool disposed = false;

        public Citymaster_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);

            objCityMaster_DAL = new CityMaster_DAL(EpsDatabase);
        }

        public async Task<IEnumerable<CitymasterModel>> GetAllRecords()
        {
            IEnumerable<CitymasterModel> result = await Task.Run(() => objCityMaster_DAL.GetCitymasterlist());

            return result;
        }

        public async Task<int> SaveRecord(CitymasterModel objData)
        {
            var result = await Task.Run(() => objCityMaster_DAL.SaveCityClassmasterDetails(objData));

            return result;
        }


        public async Task<int> CheckRecordAlredyExitsinCityMaster(CitymasterModel obj)
        {
            var result = await Task.Run(() => objCityMaster_DAL.CheckRecordAlredyExitsOrNotInCityMaster(obj));
            return result;

        }

        public async Task<int> DeleteRecord(int id)
        {
            var result = await Task.Run(() => objCityMaster_DAL.DeleteCityClass(id));

            return result;
        }

        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    objCityMaster_DAL = null;
                    EpsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }



        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~Citymaster_BAL()
        {
            Dispose(false);
        }
    }
}
