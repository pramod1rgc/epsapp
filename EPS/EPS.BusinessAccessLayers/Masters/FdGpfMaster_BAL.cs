﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Masters;
using System;

namespace EPS.BusinessAccessLayers.Masters
{
    public class FdGpfMaster_BAL:IFdGpfMasterRepository
    {
        private bool disposed = false;
        private Database epsDatabase;
        private FdGpfMaster_DAL objGpfDAL;
        public FdGpfMaster_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objGpfDAL = new FdGpfMaster_DAL(epsDatabase);
        }

        #region Create Family Definition Gpf Master Data
        public async Task<string> CreatefdGpfMaster(FdMaster_Model HRAModel)
        {
            return await Task.Run(() => objGpfDAL.CreatefdGpfMaster(HRAModel));
        }
        #endregion

        #region Get Family Definition Gpf Master Data
        public async Task<List<FdMaster_Model>> GetFdGpfMasterDetails()
        {
            return await Task.Run(() => objGpfDAL.GetFdGpfMasterDetails());
        }
        #endregion

        #region Edit Family Definition Gpf Master Data
        public async Task<List<FdMaster_Model>> EditFdGpfMasterDetails(int hraMasterID)
        {
            return await Task.Run(() => objGpfDAL.EditFdGpfMasterDetails(hraMasterID));
        }
        #endregion

        #region Delete Family Definition Gpf Master Data
        public async Task<string> DeleteFdGpfMaster(int hraMasterID)
        {
            return await Task.Run(() => objGpfDAL.DeleteFdGpfMaster(hraMasterID));
        }
        #endregion


        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~FdGpfMaster_BAL()
        {
            Dispose(false);
        }
        #endregion

    }
}
