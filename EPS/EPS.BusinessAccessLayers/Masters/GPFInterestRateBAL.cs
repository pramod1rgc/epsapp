﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class GPFInterestRateBAL
    {
        GPFInterestRateDAL objgpa = new GPFInterestRateDAL();
        Database EpsDatabase;
        /// <summary>
        /// GPFInterestRateBAL
        /// </summary>
        public GPFInterestRateBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        #region
        public async Task<List<GPFInterestRateBM>> GetGPFInterestRateDetails()
        {
            GPFInterestRateDAL objgpf = new GPFInterestRateDAL(EpsDatabase);
            var mytask = Task.Run(() => objgpf.GetGPFInterestRateDetails());
            List<GPFInterestRateBM> result = await mytask;
            return result;
        }
 
        #endregion

        #region
        /// <summary>
        /// InsertandUpdate
        /// </summary>
        /// <param name="_objgpf"></param>
        /// <returns></returns>
        public async Task<string> InsertandUpdate(GPFInterestRateBM objgpfmaster)
        {
            GPFInterestRateDAL objgpf = new GPFInterestRateDAL(EpsDatabase);
            var mytask = Task.Run(() => objgpf.InsertandUpdate(objgpfmaster));
            string result = await mytask;
            return result;
        }
     
        #endregion
        #region
        /// <summary>
        /// Delete
        /// </summary> 
        /// <param name="gpfInterestID"></param>
        /// <returns></returns>
         public async Task<string> Delete(int gpfInterestID)
        {
            GPFInterestRateDAL objgpf = new GPFInterestRateDAL(EpsDatabase);
            var mytask = Task.Run(() => objgpf.Delete(gpfInterestID));
           string result = await mytask;
            return result;
        }

        #endregion
    }
}
