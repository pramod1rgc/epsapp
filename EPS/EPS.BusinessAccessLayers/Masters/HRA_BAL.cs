﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Masters;
using System;

namespace EPS.BusinessAccessLayers.Masters
{
    public class HRA_BAL: IHraMasterRepository
    {
        private bool disposed = false;
        private Database epsDatabase;
        private HRA_DAL objHraDAL;

        public HRA_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objHraDAL = new HRA_DAL(epsDatabase);
        }
        #region Get PayScale Code

        public async Task<List<HRAModel>> BindPayScaleCode()
        {
            return await Task.Run(() => objHraDAL.BindPayScaleCode());
        }

        public async Task<List<HRAModel>> BindPayLevel()
        {
            return await Task.Run(() => objHraDAL.BindPayLevel());
        }
        #endregion

        #region Get Pay Commission By Employee Type
        public async Task<List<HRAModel>> GetPayCommissionByEmployeeType(string employeeType)
        {
            return await Task.Run(() => objHraDAL.GetPayCommissionByEmployeeType(employeeType));
        }
        #endregion


        #region Get City Details
        public async Task<List<HRAModel>> GetCityDetails(int StateID)
        {
            return await Task.Run(() => objHraDAL.GetCityDetails(StateID));
        }
        #endregion

        #region Get City Class
        public async Task<List<HRAModel>> GetCityClass(int payCommId)
        {
            return await Task.Run(() => objHraDAL.GetCityClass(payCommId));
        }
        #endregion


        #region Create Master Data
        public async Task<string> CreateHraMaster(HRAModel HRAModel)
        {
            return await Task.Run(() => objHraDAL.CreateHraMaster(HRAModel));
        }

        #endregion


        #region Get Master Data
        public async Task<List<HRAModel>> GetHraMasterDetails()
        {
            return await Task.Run(() => objHraDAL.GetHraMasterDetails());
        }
        #endregion
        
        #region Delete Hra Master Data
        public async Task<string> DeleteHraMaster(int hraMasterID)
        {
            return await Task.Run(() => objHraDAL.DeleteHraMaster(hraMasterID));
        }
        #endregion

        #region Edit Master Data
        public async Task<List<HRAModel>> EditHraMasterDetails(int hraMasterID)
        {
            return await Task.Run(() => objHraDAL.EditHraMasterDetails(hraMasterID));
        }

        #endregion

        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~HRA_BAL()
        {
            Dispose(false);
        }
        #endregion

    }
}
