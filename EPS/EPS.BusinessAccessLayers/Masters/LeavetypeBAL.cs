﻿using System;
using System.Collections.Generic;
using System.Text;
using EPS.BusinessModels.Masters;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using EPS.CommonClasses;
using System.Configuration;
using System.Threading.Tasks;
using EPS.Repositories.Masters;

namespace EPS.BusinessAccessLayers.Masters
{
   public class LeavetypeBAL
    {

      Database EpsDatabase;
     public  LeavetypeBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            EpsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);

        }
        #region Get LeaveType
         //code written by Mr. nitin
         // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task< List<LeavetypeBM>> GetLeavetypes( )
        {
           
             LeavetypeDAL objleavetype_DAL = new LeavetypeDAL(EpsDatabase);
             return await Task.Run(() => objleavetype_DAL.GetLeavetypes()).ConfigureAwait(true);
                
        }
        #endregion
        #region Get LeaveType MaxID
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task<int> GetLeavetypeMaxid()
        {
      
            LeavetypeDAL objleavetype_DAL = new LeavetypeDAL(EpsDatabase);
            return await Task.Run(() => objleavetype_DAL.GetLeavetypeMaxid());
    
        }
        #endregion
        #region Update Master Leave Type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task<string> UpdateMstLeavetype(LeavetypeBM leavetypeBM )
        {

                LeavetypeDAL objleavetype_DAL = new LeavetypeDAL(EpsDatabase);
               return await Task.Run(() => objleavetype_DAL.UpdateMstLeavetype(leavetypeBM));

        }
        #endregion
        #region Insert Master Leave type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
        public async Task<string>InsertMstLeavetype(LeavetypeBM leavetypeBM)
        {

                LeavetypeDAL objleavetype_DAL = new LeavetypeDAL(EpsDatabase);
            return await Task.Run(() => objleavetype_DAL.InsertMstLeavetype(leavetypeBM));

        }
        #endregion
        #region delete Master Leave type
        //code written by Mr. nitin
        // Modified By mr. Vijay Kumar and  Mr. Sushil Kumar  Modified date 13-03-2019
      public async Task<string> DeleteMstLeavetype(int msleavetypeId)
        {
            LeavetypeDAL objleavetype_DAL = new LeavetypeDAL(EpsDatabase);
              return await Task.Run(() =>  objleavetype_DAL.DeleteMstLeavetype(msleavetypeId));

        }
        #endregion
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    
                    EpsDatabase = null;
                }
           
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~LeavetypeBAL()
        {
            Dispose(false);
        }
    }
}
