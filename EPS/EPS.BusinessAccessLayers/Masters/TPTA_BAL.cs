﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.Repositories.Masters;
using System;

namespace EPS.BusinessAccessLayers.Masters
{
   public class TPTA_BAL:ITptaMasterRepository
    {
        private bool disposed = false;
        private Database epsDatabase;
        private TPTA_DAL objTptaDAL;


        public TPTA_BAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objTptaDAL = new TPTA_DAL(epsDatabase);
        }
                
        #region Create Master Data
        /// <summary>
        /// Create TPTA Master Data
        /// </summary>
        /// <param name="HRAModel"></param>
        /// <returns></returns>
        public async Task<string> CreateTptaMaster(HRAModel HRAModel)
        {
            return await Task.Run(() => objTptaDAL.CreateTptaMaster(HRAModel));
        }
        
        #endregion

        #region Get Master Data
        /// <summary>
        /// Get tpta record for bind gried
        /// </summary>
        /// <returns></returns>
        public async Task<List<HRAModel>> GetTptaMasterDetails()
        {
            return await Task.Run(() => objTptaDAL.GetTptaMasterDetails());
        }
        #endregion
        
        #region Edit Master Data
        /// <summary>
        /// Get tpta details by id for edit record
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        public async Task<List<HRAModel>> EditTptaMasterDetails(int MasterID)
        {
            return await Task.Run(() => objTptaDAL.EditTptaMasterDetails(MasterID));
        }
        #endregion

        #region Delete Hra Master Data
        /// <summary>
        /// Delete TPTA Master Data
        /// </summary>
        /// <param name="MasterID"></param>
        /// <returns></returns>
        public async Task<string> DeleteTptaMaster(int MasterID)
        {
            return await Task.Run(() => objTptaDAL.DeleteTptaMaster(MasterID));
        }
        #endregion

        #region Dispose function implementation
        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    //  objCityMaster_DAL = null;
                    epsDatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~TPTA_BAL()
        {
            Dispose(false);
        }
        #endregion


    }
}
