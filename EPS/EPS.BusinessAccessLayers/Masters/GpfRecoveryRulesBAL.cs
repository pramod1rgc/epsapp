﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class GpfRecoveryRulesBAL : IGpfRecoveryRulesRepository
    {
        //private Database epsDataBase;
        //public GpfRecoveryRulesBAL()
        //{
        //    DatabaseProviderFactory factory = new DatabaseProviderFactory();
        //    epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        //}
        ///// <summary>
        ///// InsertUpdateReoveryRulesDetails
        ///// </summary>
        ///// <param name="ObjGpfRecoveryRules"></param>
        ///// <returns></returns>
        //public async Task<int> InsertUpdateReoveryRulesDetails(GpfRecoveryRulesBM ObjGpfRecoveryRules)
        //{
        //    GpfRecoveryRulesDAL objGpfRecoveryRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
        //    var mytask = Task.Run(() => objGpfRecoveryRulesDA.InsertUpdateRecoveryRulesDetails(ObjGpfRecoveryRules));
        //    int result = await mytask;
        //    return result;
        //}
        ///// <summary>
        ///// GetGpfRecoveryRulesById
        ///// </summary>
        ///// <param name="MsGpfWithdrawRulesRefID"></param>
        ///// <returns></returns>
        //public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRulesById(int MsGpfWithdrawRulesRefID)
        //{
        //    GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
        //    var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfRecoveryRulesById(MsGpfWithdrawRulesRefID));
        //    List<GpfRecoveryRulesBM> result = await mytask;
        //    return result;
        //}
        ///// <summary>
        ///// GetGpfRecoveryRules
        ///// </summary>
        ///// <returns></returns>
        //public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRules()
        //{
        //    GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
        //    var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfRecoveryRules());
        //    List<GpfRecoveryRulesBM> result = await mytask;
        //    return result;
        //}
        ///// <summary>
        ///// DeleteGpfRecoveryRules
        ///// </summary>
        ///// <param name="MsGpfAdvanceRulesRefID"></param>
        ///// <param name="IsFleg"></param>
        ///// <returns></returns>
        //public async Task<int> DeleteGpfRecoveryRules(int MsGpfAdvanceRulesRefID)
        //{
        //    GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
        //    var mytask = Task.Run(() => objGpfWithdrawRulesDA.DeleteGpfRecoveryRules(MsGpfAdvanceRulesRefID));
        //    int result = await mytask;
        //    return result;
        //}
        private Database epsDataBase;
        private bool disposed = false;
        public GpfRecoveryRulesBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
   
        public async Task<int> DeleteGpfRecoveryRules(int msGpfRecoveryRulesRefID)
        {
            GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.DeleteGpfRecoveryRules(msGpfRecoveryRulesRefID));
            int result = await mytask;
            return result;
        }

        public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRules()
        {
            GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfRecoveryRules());
            List<GpfRecoveryRulesBM> result = await mytask;
            return result;
        }

        public async Task<List<GpfRecoveryRulesBM>> GetGpfRecoveryRulesById(int msGpfRecoveryRulesRefID)
        {
            GpfRecoveryRulesDAL objGpfWithdrawRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfWithdrawRulesDA.GetGpfRecoveryRulesById(msGpfRecoveryRulesRefID));
            List<GpfRecoveryRulesBM> result = await mytask;
            return result;
        }

        public async Task<int> InsertUpdateGpfRecoveryRules(GpfRecoveryRulesBM objgpfRecoveryRules)
        {
            GpfRecoveryRulesDAL objGpfRecoveryRulesDA = new GpfRecoveryRulesDAL(epsDataBase);
            var mytask = Task.Run(() => objGpfRecoveryRulesDA.InsertUpdateRecoveryRulesDetails(objgpfRecoveryRules));
            int result = await mytask;
            return result;

        }
    }
}
