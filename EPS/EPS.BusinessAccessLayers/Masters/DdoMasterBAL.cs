﻿using System.Collections.Generic;
using EPS.BusinessModels.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using EPS.DataAccessLayers.Masters;
using System.Threading.Tasks;
using EPS.CommonClasses;
using System;
using EPS.Repositories.Masters;

namespace EPS.BusinessAccessLayers.Masters
{
   public class DdoMasterBAL: IDdoMasterRepository
    {
       Database epsdatabase;
       DdoMasterDAL objDdoMasterDAL;
        private bool disposed = false;

        public DdoMasterBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objDdoMasterDAL = new DdoMasterDAL(epsdatabase);
        }
        #region Get All Controller  
        public async Task<IEnumerable<ControllerMs>> GetAllController()
        {
            IEnumerable<ControllerMs> result = await Task.Run(() => objDdoMasterDAL.GetAllController());
            return result;
        }
       #endregion

        #region Get PAO  by ControllerId
        public async Task<IEnumerable<PAO>> GetPAOByControllerId(string ControllerId)
        {
            IEnumerable<PAO> result = await Task.Run(() => objDdoMasterDAL.GetPAOByControllerId(ControllerId));
            return result;
        }
        #endregion

        #region Get DDO by PAOId
        public async Task<IEnumerable<DDO>> GetODdoByPao(string PaoID)
        {
            IEnumerable<DDO> result = await Task.Run(() => objDdoMasterDAL.GetODdoByPao(PaoID));
            return result;
        }
        #endregion

        #region Get DDOcategory
        public async Task<IEnumerable<DDOType>> GetDDOcategory()
        {
            IEnumerable<DDOType> result = await Task.Run(() => objDdoMasterDAL.GetDDOcategory());
            return result;
        }
        #endregion

        #region Get Sate
        public async Task<IEnumerable<State>> GetState()
        {
            IEnumerable<State> result = await Task.Run(() => objDdoMasterDAL.GetState());
            return result;
        }
        #endregion

        #region Get City By 
        public async Task<IEnumerable<City>> GetCity(string StateId)
        {
            IEnumerable<City> result = await Task.Run(() => objDdoMasterDAL.GetCity(StateId));
            return result;
        }
        #endregion

        #region Get DDOMaster
        public async Task<IEnumerable<DdoMasterModel>> GetDDOMaster()
        {
            IEnumerable<DdoMasterModel> result = await Task.Run(() => objDdoMasterDAL.GetDDOMaster());
            return result;
        }
        #endregion

        #region Save DDOMaster 
        public async Task<int> SaveDDOMaster(DdoMasterModel objDdoMasterModel)
        {

            int result = await Task.Run(() => objDdoMasterDAL.SaveDDOMaster(objDdoMasterModel));
            return result;
        }
        #endregion
        #region Deactivate DDOMaster 
        public async Task<int> DeleteDDOMaster(string ddoId)
        {

            int result = await Task.Run(() => objDdoMasterDAL.DeleteDDOMaster(ddoId));
            return result;
        }
        #endregion
        



        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources 
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    objDdoMasterDAL = null;
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called 
        /// </summary>
        ~DdoMasterBAL()
        {
            Dispose(false);
        }
    }
}
