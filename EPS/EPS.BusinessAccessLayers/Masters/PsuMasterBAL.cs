﻿using EPS.BusinessModels.Masters;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Masters;
using EPS.Repositories.Masters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Masters
{
    public class PsuMasterBAL : IPsuMaster
    {

        PsuMasterDAL _objPsu = new PsuMasterDAL();

      
        public Task<IEnumerable<PsuMasterModel.City>> GetDisttList(int stateId)
        {
            return _objPsu.GetDisttList(stateId);
        }

        public Task<IEnumerable<PsuMasterModel.PsuMaster>> GetPsuMasterDetails()
        {
            return _objPsu.GetPsuMasterDetails();
        }

        public Task<IEnumerable<PsuMasterModel.State>> GetStateList()
        {
            return _objPsu.GetStateList();
        }

        public Task<string> SavedPSUDetails(PsuMasterModel.PsuMaster objPSU)
        {
            return _objPsu.SavedPSUDetails(objPSU);
        }

        public string DeletePsuMasterDetails(int id)
        {
            return _objPsu.DeletePsuMasterDetails(id);
        }

    }
}
