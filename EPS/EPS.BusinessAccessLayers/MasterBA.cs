﻿using EPS.BusinessModels;
using EPS.CommonClasses;
using EPS.DataAccessLayers;
using EPS.Repositories.EmployeeDetails;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers
{
   public class MasterBA: IMasterRepository
    {
        private Database epsDataBase;
        public MasterBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsDataBase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        //============================Get Relation===================================== 
        public async Task<IEnumerable<MasterModel>> GetRelation()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetRelation());
            IEnumerable<MasterModel> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<JoiningTypeMaster>> GetJoiningMode(Boolean? IsDeput, int? MsEmpSubTypeID)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetJoiningMode(IsDeput, MsEmpSubTypeID));
            IEnumerable<JoiningTypeMaster> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<JoiningTypeMaster>> GetJoiningCategory(int parantTypeId, Boolean? IsDeput)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetJoiningCategory(parantTypeId,IsDeput));
            IEnumerable<JoiningTypeMaster> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<MasterModel>> GetSalutation()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetSalutation());
            IEnumerable<MasterModel> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<EmployeeTypeMaster>> GetEmpSubType(int parantTypeId, Boolean? IsDeput)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetEmpSubType(parantTypeId, IsDeput));
            IEnumerable<EmployeeTypeMaster> result = await mytask;
            return result;
           
        }
        public async Task<IEnumerable<EmployeeTypeMaster>> GetEmpType(int? serviceId, int? deputationId)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetEmpType(serviceId, deputationId));
            IEnumerable<EmployeeTypeMaster> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<MasterModel>> GetDeputationType(int? serviceId)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetDeputationType(serviceId));
            IEnumerable<MasterModel> result = await mytask;
            return result;
        }

        public async Task<IEnumerable<ServiceTypeMaster>> GetServiceType(Boolean ? IsDeput)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetServiceType(IsDeput));
            IEnumerable<ServiceTypeMaster> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<GenderMaster>> GetGender()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetGender());
            IEnumerable<GenderMaster> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<StateMaster>> GetState()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetState());
            IEnumerable<StateMaster> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<PfTypeMaster>> GetPfType()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetPfType());
            IEnumerable<PfTypeMaster> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<JoiningAccountOf>> GetJoiningAccount()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetJoiningAccount());
            IEnumerable<JoiningAccountOf> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<OfficeList>> GetOfficeList(int controllerId,int ? ddoId)
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetOfficeList(controllerId, ddoId));
            IEnumerable<OfficeList> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<OfficeCityClass>> GetOfficeCityClassList()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetOfficeCityClassList());
            IEnumerable<OfficeCityClass> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<PayCommission>> GetPayCommissionList()
        {
            MasterDA objMasterDA = new MasterDA(epsDataBase);
            var mytask = Task.Run(() => objMasterDA.GetPayCommissionList());
            IEnumerable<PayCommission> result = await mytask;
            return result;
        }
        
    }
}
