﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Change;
using Microsoft.Practices.EnterpriseLibrary.Data;
using EPS.BusinessModels.Change;
using EPS.Repositories.Change;
using System;


namespace EPS.BusinessAccessLayers.Change
{
    public class Changebal: IChangeRepository
    {
        private Database epsdatabase;
        private bool disposed = false;
        public Changebal()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
       
        public async Task<IEnumerable<ddlvalue>> GetDdlvalue(string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetDdlvalue(Flag));
            var result = await myTask;
            return result;

        }

        public async Task<int> ChangeDeleteDetails(string empCd, string orderNo, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ChangeDeleteDetails(empCd, orderNo, Flag));
            var result = await myTask;
            return result;
        }
        //DOB
        public async Task<string> InsertDOBDetails(DOBcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertDOBDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<DOBcls>> GetDOBDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetDOBDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerDOBUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerDOBUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //Namegender


        public async Task<string> InsertNameGenderDetails(NameGender objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertNameGenderDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<NameGender>> GetNameGenderDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetNameGenderDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }
        public async Task<string> ForwardToCheckerNameGenderUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerNameGenderUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }
        //pfNps
        public async Task<string> InsertPfNpsFormDetails(PFNPS objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertPfNpsFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<PFNPS>> GetPfNpsFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetPfNpsFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerPfNpsUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerPfNpsUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }
        //PANNo

        public async Task<string> InsertPanNoFormDetails(PANNumber objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertPanNoFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<PANNumber>> GetPanNoFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetPanNoFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }     

        public async Task<string> ForwardToCheckerPanNoUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerPanNoUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }
               
        //HRACityClass
        public async Task<string> InsertHraccFormDetails(HRAcityclass objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertHraccFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<HRAcityclass>> GetHraccFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetHraccFormDetails(empcode,roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerHraccUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerHraccUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //designation
        public async Task<string> InsertdesignationFormDetails(designationcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertdesignationFormDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }
        public async Task<IEnumerable<designationcls>> GetdesignationFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetdesignationFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerdesignationUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerdesignationUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //exservicemen
        public async Task<string> InsertexservicemenFormDetails(exservicemencls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertexservicemenFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<exservicemencls>> GetexservicemenFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetexservicemenFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerexservicemenUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerexservicemenUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //cghs
       
        public async Task<string> InsertCGHSFormDetails(CGHScls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertCGHSFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<CGHScls>> GetCGHSFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetCGHSFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerCGHSUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerCGHSUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //CGEGIS

        public async Task<string> InsertCGEGISFormDetails(CGEGIScls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertCGEGISFormDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<CGEGIScls>> GetCGEGISFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetCGEGISFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerCGEGISFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerCGEGISFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }
        //joiningmode

        public async Task<string> InsertJoiningModeFormDetails(joiningmodecls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertJoiningModeFormDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<joiningmodecls>> GetJoiningModeFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetJoiningModeFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerJoiningModeFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerJoiningModeFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //DOJ

        public async Task<string> InsertDOJDetails(DOJcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertDOJDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<DOJcls>> GetDOJDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetDOJDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerDOJUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerDOJUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //DOE

        public async Task<string> InsertDOEDetails(DOEcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertDOEDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<DOEcls>> GetDOEDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetDOEDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerDOEUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerDOEUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //DORcls

        public async Task<string> InsertDORDetails(DORcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertDORDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<DORcls>> GetDORDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetDORDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerDORUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerDORUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //CasteCategory

        public async Task<string> InsertCasteCategoryFormDetails(CasteCategorycls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertCasteCategoryFormDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<CasteCategorycls>> GetCasteCategoryFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetCasteCategoryFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerCasteCategoryFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerCasteCategoryFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //Entofficevehicle

        public async Task<string> InsertEntofficevehicleFormDetails(Entofficevehiclecls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertEntofficevehicleFormDetails(objPro, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<Entofficevehiclecls>> GetEntofficevehicleFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetEntofficevehicleFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerEntofficevehicleFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerEntofficevehicleFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //BankForm


        public async Task<string> InsertBankFormDetails(BankFormcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertBankFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<BankFormcls>> GetBankFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetBankFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }
        public async Task<string> ForwardToCheckerBankFormUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerBankFormUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //addtapd

        public async Task<string> InsertaddtapdFormDetails(addtapdcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertaddtapdFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }
        public async Task<IEnumerable<addtapdcls>> GetaddtapdFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetaddtapdFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }
        public async Task<string> ForwardToCheckeraddtapdUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckeraddtapdUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //TPTA

        public async Task<string> InsertTPTAFormDetails(TPTAcls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertTPTAFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<TPTAcls>> GetTPTAFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetTPTAFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerTPTAUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerTPTAUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //StateGIS

        public async Task<string> InsertStateGISFormDetails(StateGIScls objPro, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertStateGISFormDetails(objPro, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<IEnumerable<StateGIScls>> GetStateGISFormDetails(string empcode, int roleId, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetStateGISFormDetails(empcode, roleId, Flag));
            var result = await myTask;
            return result;

        }

        public async Task<string> ForwardToCheckerStateGISUserDtls(string empCd, string orderNo, string status, string rejectionRemark)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerStateGISUserDtls(empCd, orderNo, status, rejectionRemark));
            var result = await myTask;
            return result;
        }

        //MobileNo_Email_EmpCode

        public async Task<string> InsertMobileNoEmailEmpCodeDetails(ConMobileNoEmailEmpCodecls objPro, string flag, string Contactdetails)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertMobileNoEmailEmpCodeDetails(objPro, flag, Contactdetails));
            var result = await myTask;
            return result;
        }

        public async Task<IEnumerable<ConMobileNoEmailEmpCodecls>> GetMobileNoEmailEmpCodeDetails(string empcode, int roleId, string Flag, string ContactdetailsSelectedOption)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetMobileNoEmailEmpCodeDetails(empcode, roleId, Flag, ContactdetailsSelectedOption));
            var result = await myTask;
            return result;
        }

        public async Task<string> ForwardToCheckerMobileNoEmailEmpCodeUserDtls(string empCd, string orderNo, string status, string rejectionRemark, string ContactdetailsSelectedOption)
        {
            Changedal objProDAL = new Changedal(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerMobileNoEmailEmpCodeUserDtls(empCd, orderNo, status, rejectionRemark, ContactdetailsSelectedOption));
            var result = await myTask; 
            return result;
        }




        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }



        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~Changebal()
        {
            Dispose(false);
        }
    }
}
