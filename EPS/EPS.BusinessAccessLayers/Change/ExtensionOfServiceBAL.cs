﻿using EPS.BusinessModels.Change;
using EPS.CommonClasses;
using EPS.DataAccessLayers.Change;
using EPS.Repositories.Change;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Change
{
    public class ExtensionOfServiceBAL : IExtensionOfServiceRepository
    {
        private Database epsdatabase;
        private bool disposed = false;
        public ExtensionOfServiceBAL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }

        public async Task<IEnumerable<ddlvalue>> GetDdlvalue(string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase); //change dal herer
            var myTask = Task.Run(() => objProDAL.GetDdlvalue(Flag));
            var result = await myTask;
            return result;

        }

        public async Task<int> ChangeDeleteDetails(string empCd, string orderNo, string Flag)
        {
            Changedal objProDAL = new Changedal(epsdatabase);//change dal herer
            var myTask = Task.Run(() => objProDAL.ChangeDeleteDetails(empCd, orderNo, Flag));
            var result = await myTask;
            return result;
        }
        public async Task<string> InsertEOSDetailsBAL(ExtensionOfServiceModel1 objPro, string Flag)
        {
            ExtensionOfServiceDAL objProDAL = new ExtensionOfServiceDAL(epsdatabase);
            var myTask = Task.Run(() => objProDAL.InsertEOSDetailsDAL(objPro, Flag));
            var result = await myTask;
            return result;

        }
        public async Task<IEnumerable<ExtensionOfServiceModelText>> GetExtensionOfServiceDetailsBAL(string empcode, int roleId)
        {
            ExtensionOfServiceDAL objProDAL = new ExtensionOfServiceDAL(epsdatabase);
            var myTask = Task.Run(() => objProDAL.GetExtensionOfServiceDetailsDAL(empcode,roleId));
            var result = await myTask;
            return result;

        }
               

        public async Task<string> ForwardToCheckerUserDtls(string empCd, string orderNo,string status, string rejectionRemark)
        {
            ExtensionOfServiceDAL objProDAL = new ExtensionOfServiceDAL(epsdatabase);
            var myTask = Task.Run(() => objProDAL.ForwardToCheckerUserDtls(empCd, orderNo,status,rejectionRemark));
            var result = await myTask;
            return result;
        }


        /// <summary>
        /// Managed and unmanaged resources can be disposed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources
                if (disposing)
                {
                    //context.Dispose();
                    // Free any other managed objects here
                    epsdatabase = null;
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Implement IDisposable
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources
            Dispose(true);

            // Suppress finalization
            GC.SuppressFinalize(this);
        }



        /// <summary>
        /// This destructor will run only if the Dispose method does not get called
        /// </summary>
        ~ExtensionOfServiceBAL()
        {
            Dispose(false);
        }

    }
}
