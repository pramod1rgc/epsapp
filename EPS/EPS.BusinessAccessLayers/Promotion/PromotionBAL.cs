﻿using EPS.BusinessModels.Promotion;
using EPS.DataAccessLayers.Promotion;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.Promotion
{
   public class PromotionBAL
    {
        PromotionDAL objProDAL = new PromotionDAL();

        #region PromotionWithoutTransfer
        public async Task<string> UpdatePromotionDetailsWithoutTransferBAL(PromotionDetails objPro)
        {
            return await objProDAL.UpdatePromotionDetailsWithoutTransfer(objPro);
        }

        public async Task<IEnumerable<GetPromotionDetails>> GetPromotionDetailsWithoutTransferBAL(string empCd, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
            return await objProDAL.GetPromotionDetailsWithoutTransfer(empCd, pageNumber, pageSize, searchTerm, roleId);
        }

        public async Task<string> DeletePromotionDetails(int id)
        {
            return await objProDAL.DeletePromotionDetails(id);
        }

        public async Task<string> ForwardTransferDetailToChecker(ForwardTransferDetails obj)
        {
            return await objProDAL.ForwardTransferDetailToChecker(obj);
        }

        public async Task<string> UpdateStatus(ForwardTransferDetails obj)
        {
            return await objProDAL.UpdateStatus(obj);
        }
        #endregion

        #region ReversionWithoutTransfer

        public async Task<string> UpdateReversionDetailsWithoutTransferBAL(ReversionDetails objRev)
        {
            return await objProDAL.UpdateReversionDetailsWithoutTransfer(objRev);
        }

        public async Task<IEnumerable<GetReversionDetails>> GetReversionDetailsWithoutTransferBAL(string empCd, int pageNumber, int pageSize, string searchTerm, int roleId)
        {
            return await objProDAL.GetReversionDetailsWithoutTransfer(empCd, pageNumber, pageSize, searchTerm, roleId);
        }

        public async Task<string> DeleteReversionDetails(int id)
        {
            return await objProDAL.DeleteReversionDetails(id);
        }

        #endregion
    }
}
