﻿using System;
using System.Collections.Generic;
using System.Text;
using EPS.BusinessModels.EndofService;
using EPS.DataAccessLayers.EndofService;
namespace EPS.BusinessAccessLayers.EndofService
{
   public class EndofServiceBAL
    {
        EndofServiceDAL objEndDAL = new EndofServiceDAL();

        public IEnumerable<EndofServiceModel> GetServiceEndEmployeesBAL(int designid, string empPermDDOId, int mode)
        {
            return objEndDAL.GetSuperAnnotateEmployeesDetails(designid, empPermDDOId, mode);
        }
        public IEnumerable<EmpCurrentDetailModel> EmpCurrentEndDetailsBAL(int msEmpID, string empCd)
        {
            return objEndDAL.EmpCurrentEndDetailsDAL(msEmpID,empCd);
        }
        public IEnumerable<EndofServiceDtModel> GetEndofServiceEmployeesListBAL(int msEmpID)
        {
            return objEndDAL.GetEndofServiceEmployeesListDAL(msEmpID);
        }
        public string InsertEndofServiceDetailsBAL(EndofServiceInsertModel objRej)
        {
            return objEndDAL.InsertEndofServiceDetails(objRej);
        }
        public string DeleteEndofServiceDetailsBAL(int objRej,string ipAddress)
        {
            return objEndDAL.DeleteEndofService(objRej, ipAddress);
        }

        public string UpdateEndofServiceStatusBAL(UpdateEndofServiceStatusModel objRej)
        {
            return objEndDAL.UpdateEndofServiceStatus(objRej);
        }
        public IEnumerable<EndofServiceReasonModel> GetEndofServiceReasonIdBAL()
        {
            return objEndDAL.GetEndofServiceReasonIdDAL();
        }
    }
}
