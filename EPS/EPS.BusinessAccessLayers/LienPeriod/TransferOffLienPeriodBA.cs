﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LienPeriod;
using EPS.Repositories.LienPeriod;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.LienPeriod
{
    public class TransferOffLienPeriodBA: ITransferOffLienPeriod
    {
        Database epsdatabase;
        public TransferOffLienPeriodBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<ControllerList>> GetControllerList()
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var mytask = Task.Run(() => objTranferofflienperiodDA.GetControllerList());
            IEnumerable<ControllerList> result = await mytask;
            return result;
        }
        public async Task<IEnumerable<DDOList>> GetDdolistbyControllerid(int controllerId)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var mytask = Task.Run(() => objTranferofflienperiodDA.GetDdolistbyControllerid(controllerId));
            IEnumerable<DDOList> result = await mytask;
            return result;
        }
     
        public async Task<IEnumerable<OfficeAddressDetails>> GetOfficeAddressDetails(int controllerId,int ddoId,string officeName)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var mytask = Task.Run(() => objTranferofflienperiodDA.GetOfficeAddressDetail(controllerId,ddoId,officeName));
            IEnumerable<OfficeAddressDetails> result = await mytask;
            return result;
        }
    

        public async Task<IEnumerable<TransferofflienperiodModel>> GetTransferofficelienPeriodDetails(string empCd, int roleId)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var mytask = Task.Run(() => objTranferofflienperiodDA.GetTransferofficelienPeriodDetails(empCd, roleId));
           IEnumerable<TransferofflienperiodModel> result = await mytask;
            return result;
        }
        public async Task<string> InsertUpdateTransferOffLienPeriod(TransferofflienperiodModel objTransferofflienPeriod)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var myTask = Task.Run(() => objTranferofflienperiodDA.InsertUpdateTransferOffLienPeriod(objTransferofflienPeriod));
            string result = await myTask;
            return result;
        }
        public async Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var myTask = Task.Run(() => objTranferofflienperiodDA.UpdateforwardStatusUpdateByEmpCode(empCd, orderNo, status, rejectionRemark));
            int result = await myTask;
            return result;
        }
        public async Task<int> DeleteTransferOffLienPeriod(string empCd, string orderNo)
        {
            TranferofflienperiodDA objTranferofflienperiodDA = new TranferofflienperiodDA(epsdatabase);
            var myTask = Task.Run(() => objTranferofflienperiodDA.DeleteTransferOffLienPeriod(empCd, orderNo));
            int result = await myTask;
            return result;
        }
        



    }
}
