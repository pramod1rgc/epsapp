﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LienPeriod;
using EPS.Repositories.LienPeriod;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.LienPeriod
{
    public  class EpsEmpJoinAfterLienPeriodBA : IEpsEmpJoinAfterLienPeriod
    {
        Database epsdatabase;
        public EpsEmpJoinAfterLienPeriodBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<EpsEmpJoinAfterLienPeriodModel>> GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId,int controllerId)
        {
            EpsEmpJoinAfterLienPeriodDA objEpsEmpJoinAfterLienPeriodDA = new EpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var mytask = Task.Run(() => objEpsEmpJoinAfterLienPeriodDA.GetEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd, roleId, controllerId));
            IEnumerable<EpsEmpJoinAfterLienPeriodModel> result = await mytask;
            return result;
        }
        
        public async Task<string> InsertUpdateEpsEmployeeJoinAfterLienPeriod(EpsEmpJoinAfterLienPeriodModel objmodel)
        {
            EpsEmpJoinAfterLienPeriodDA objDA = new EpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.InsertUpdateEpsEmpJoinAfterLienPeriod(objmodel));
            string result = await myTask;
            return result;
        }
        public async Task<int> DeleteEpsEmployeeJoinByEmpCode(int lienId)
        {
            EpsEmpJoinAfterLienPeriodDA objDA = new EpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.DeleteEpsEmployeeJoinByEmpCode(lienId));
            int result = await myTask;
            return result;
        }
        public async Task<int> UpdateforwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {
            EpsEmpJoinAfterLienPeriodDA objDA = new EpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.UpdateforwardStatusUpdateByEmpCode(empCd, orderNo,status, rejectionRemark));
            int result = await myTask;
            return result;
        }

    }
}
