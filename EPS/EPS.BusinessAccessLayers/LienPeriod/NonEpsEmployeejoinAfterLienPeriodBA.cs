﻿using EPS.BusinessModels.LienPeriod;
using EPS.CommonClasses;
using EPS.DataAccessLayers.LienPeriod;
using EPS.Repositories.LienPeriod;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.LienPeriod
{
    public class NonEpsEmployeejoinAfterLienPeriodBA :INonEpsEmployeejoinAfterLienPeriod
    {
        Database epsdatabase;
        public NonEpsEmployeejoinAfterLienPeriodBA()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
        }
        public async Task<IEnumerable<NonEpsEmpJoinAfterLienPeriodeModel>> GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(string empCd, int roleId)
        {
            NonEpsEmpJoinAfterLienPeriodDA objnonEpsEmpJoinAfterLienPeriodDA = new NonEpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var mytask = Task.Run(() => objnonEpsEmpJoinAfterLienPeriodDA.GetNonEpsEmpJoinOffAfterLienPeriodDetailByEmp(empCd, roleId));
            IEnumerable<NonEpsEmpJoinAfterLienPeriodeModel> result = await mytask;
            return result;
        }

        public async Task<string> SaveUpdateNonEpsEmployeeJoinAfterLienPeriod(NonEpsEmpJoinAfterLienPeriodeModel objmodel)
        {
            NonEpsEmpJoinAfterLienPeriodDA objDA = new NonEpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.SaveUpdateNonEpsEmployeeJoinAfterLienPeriod(objmodel));
            string result = await myTask;
            return result;
        }
        public async Task<int> DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(string empCd,string orderNo)
        {
            NonEpsEmpJoinAfterLienPeriodDA objDA = new NonEpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.DeleteNonEpsEmployeeJoinAfterLienPeriodByEmpCode(empCd, orderNo));
            int result = await myTask;
            return result;
        }
        public async Task<int> NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(string empCd, string orderNo, string status, string rejectionRemark)
        {
            NonEpsEmpJoinAfterLienPeriodDA objDA = new NonEpsEmpJoinAfterLienPeriodDA(epsdatabase);
            var myTask = Task.Run(() => objDA.NonEpsEmpLienPeriodUpdateForwardStatusUpdateByEmpCode(empCd, orderNo, status,rejectionRemark));
            int result = await myTask;
            return result;
        }
    }
}
