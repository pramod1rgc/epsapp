﻿using EPS.CommonClasses;
using EPS.DataAccessLayers.UserProfile;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Threading.Tasks;

namespace EPS.BusinessAccessLayers.UserProfile
{
    public class UserProfileBusinessAccessLayer
    {
        private Database epsdatabase;
        UserProfileDataAccessLayer objUserProfileDataAccessLayer;
        public UserProfileBusinessAccessLayer()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();

            epsdatabase = factory.Create(EPSConstant.EPSDataBaseConnection);
            objUserProfileDataAccessLayer = new UserProfileDataAccessLayer(epsdatabase);
        }

        public async Task<object> GetUserdetailsProfile(string UserName)
        {
            var mytask = Task.Run(() => objUserProfileDataAccessLayer.GetUserdetailsProfile(UserName));
            object result = await mytask;
            return result;
        }
    }
}
