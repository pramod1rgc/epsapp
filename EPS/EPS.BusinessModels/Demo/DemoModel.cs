﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.Demo
{
    public class DemoModel : IValidatableObject 
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FirstName == LastName)
            {
                yield return new ValidationResult("First name should not be equal to Last name",

                    new[] { "FirstName" });
            }
            if (FirstName == LastName)
            {
                yield return new ValidationResult("Last name Should not be equal to First name",

                    new[] { "LastName" });

            }
        }
    }
}
