﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.Change
{
    public class ExtensionOfServiceModel1
    {
        //[Required]
        public string OrderNo { get; set; }
        public string empCd { get; set; }
        public string oldOrderNo { get; set; }
        public string username { get; set; }
        public string empName { get; set; }
        //[Required]
        public DateTime? OrderDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? JoiningDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string rejectionRemark { get; set; }
    }


    public class ExtensionOfServiceModelText
    {
        //[Required]
        public string empName { get; set; }
        public string OrderNo { get; set; }
        public string empCd { get; set; }
        public string oldOrderNo { get; set; }
        public string username { get; set; }

        //[Required]
        public DateTime? OrderDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? JoiningDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string rejectionRemark { get; set; }

    }
}
