﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Change
{
   public class BaseClassChange
    {
        public string OrderNo { get; set; }
        public string oldOrderNo { get; set; }
        public DateTime? OrderDate { get; set; }        
        public string Remarks { get; set; }
        public string empCd { get; set; }
        public string empName { get; set; }
        public string username { get; set; }
        public string Status { get; set; }
        public string rejectionRemark { get; set; }
    }
}
