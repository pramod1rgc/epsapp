﻿using System;

namespace EPS.BusinessModels.Change
{
    public class ddlvalue
    {

        public string text1 { get; set; }
        public string value1 { get; set; }


    }
    public class DOBcls : BaseClassChange
    {
        public DateTime? DOB { get; set; }
        public DateTime? revisedDOB { get; set; }
        public DateTime? revisedAnnuationDate { get; set; }
        public DateTime? currentAnnuationDate { get; set; }
        public DateTime? currentDOB { get; set; }
    }

    public class NameGender : BaseClassChange
    {

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string currentEmpName { get; set; }
        public string revisedEmpName { get; set; }
        public string currentDesignation { get; set; }
        public string revisedDesignation { get; set; }
        public string currentGender { get; set; }
        public string revisedGender { get; set; }


    }

    public class PFNPS : BaseClassChange
    {

        public string PfType { get; set; }
        public string PranNo { get; set; }
        public DateTime? WithEffectDate { get; set; }

        public string currentPFType { get; set; }
        public string currentPRANNo { get; set; }
        public string revisedPFType { get; set; }
        public string revisedPRANNo { get; set; }
    }

    public class PANNumber : BaseClassChange
    {
        public string PANNo { get; set; }
        public string currentPANNo { get; set; }
        public string revisedPANNo { get; set; }
    }

    public class HRAcityclass : BaseClassChange
    {

        public string HRAcc { get; set; }
        public DateTime? WithEffectDate { get; set; }
        public string currentHRAcc { get; set; }
        public string revisedHRAcc { get; set; }
    }

    public class designationcls : BaseClassChange
    {
        public string designation { get; set; }
        public DateTime? WithEffectDate { get; set; }
        public string currentdesignation { get; set; }
        public string reviseddesignation { get; set; }
    }

    public class exservicemencls : BaseClassChange
    {
        public string exservicemen { get; set; }
        public string currentexservicemen { get; set; }
        public string revisedexservicemen { get; set; }
    }

    public class CGHScls : BaseClassChange
    {
        public string cghsDeduction { get; set; }
        public string cghsCardNo { get; set; }
        public string currentCGHSDeductionApplicable { get; set; }
        public string currentCGHSCardNo { get; set; }
        public string revisedCGHSDeductionApplicable { get; set; }
        public string revisedCGHSCardNo { get; set; }
    }

    public class CGEGIScls : BaseClassChange
    {
        public string cgegisapplicable { get; set; }
        public string cgegisgroup { get; set; }
        public DateTime? membershipdate { get; set; }
        public string currentCGEGISapplicable { get; set; }
        public string revisedCGEGISapplicable { get; set; }
    }

    public class joiningmodecls : BaseClassChange
    {
        public string joiningMode { get; set; }
        public string currentJoiningMode { get; set; }
        public string revisedJoiningMode { get; set; }
    }


    public class DOJcls : BaseClassChange
    {
        public DateTime? DOJ { get; set; }
        public DateTime? revisedDOJ { get; set; }
        public DateTime? revisedDOG { get; set; }
        public DateTime? currentDOJ { get; set; }
        public DateTime? currentDOG { get; set; }
    }

    public class DOEcls : BaseClassChange
    {
        public DateTime? DOE { get; set; }
        public DateTime? revisedDOE { get; set; }
        public DateTime? revisedDOB { get; set; }
        public DateTime? currentDOE { get; set; }
        public DateTime? currentDOB { get; set; }
    }
    public class DORcls : BaseClassChange
    {
        public DateTime? DOR { get; set; }
        public DateTime? revisedDOR { get; set; }
        public DateTime? revisedDOB { get; set; }
        public DateTime? currentDOR { get; set; }
        public DateTime? currentDOB { get; set; }
    }
    public class CasteCategorycls : BaseClassChange
    {
        public string CasteCategory { get; set; }
        public string currentCasteCategory { get; set; }
        public string revisedCasteCategory { get; set; }
    }

    public class Entofficevehiclecls : BaseClassChange
    {
        public string entofficeVehicle { get; set; }
        public string currententVehicle { get; set; }
        public string revisedentVehicle { get; set; }
    }
    public class BankFormcls : BaseClassChange
    {

        public string ifscCode { get; set; }
        public string bankName { get; set; }
        public string branchName { get; set; }
        public string savingAcNo { get; set; }
        public string conSavingAcNo { get; set; }
        public string accNoof { get; set; }

        public string revisedifscCode { get; set; }
        public string revisedbankName { get; set; }
        public string revisedbranchName { get; set; }
        public string revisedsavingAcNo { get; set; }
        public string revisedconSavingAcNo { get; set; }
        public string revisedaccNoof { get; set; }
      
        public string currentifscCode { get; set; }
        public string currentbankName { get; set; }
        public string currentbranchName { get; set; }
        public string currentsavingAcNo { get; set; }
        public string currentconSavingAcNo { get; set; }
        public string currentaccNoof { get; set; }
     

    }
    public class addtapdcls : BaseClassChange
    {

        public string phsidsble { get; set; }
        public string phType { get; set; }
        public string phPcnt { get; set; }
        public string phCertNo { get; set; }
        public string phisSevere { get; set; }
        public DateTime? phCertDt { get; set; }
        public string phCertAuth { get; set; }       
        public string phentDoubleTa { get; set; }


        public string revisedphsidsble { get; set; }
        public string revisedphType { get; set; }
        public string revisedphPcnt { get; set; }
        public string revisedphCertNo { get; set; }
        public string revisedphisSevere { get; set; }
        public DateTime? revisedphCertDt { get; set; }
        public string revisedphCertAuth { get; set; }
        public string revisedphentDoubleTa { get; set; }

        public string currentphsidsble { get; set; }
        public string currentphType { get; set; }
        public string currentphPcnt { get; set; }
        public string currentphCertNo { get; set; }
        public string currentphisSevere { get; set; }
        public DateTime? currentphCertDt { get; set; }
        public string currentphCertAuth { get; set; }
        public string currentphentDoubleTa { get; set; }

    }

    public class TPTAcls : BaseClassChange
    {
        public string TPTA { get; set; }
        public string currentTPTA { get; set; }
        public string revisedTPTA { get; set; }
    }

    public class StateGIScls : BaseClassChange
    {
        public string StateOtherGIS { get; set; }
        public string StateGISAdjusted { get; set; }
        public string Stategisdemo { get; set; }
        public string StateGISAGOffice { get; set; }
        public string StateGISPAOCode { get; set; }
        public string StateGISOtherOffice { get; set; }
        

        public string currentStateOtherGIS { get; set; }
        public string currentStateGISAdjusted { get; set; }
        public string currentStateGISAGOffice { get; set; }
        public string currentStateGISPAOCode { get; set; }
        public string currentStateGISOtherOffice { get; set; }

        public string revisedStateOtherGIS { get; set; }
        public string revisedStateGISAdjusted { get; set; }
        public string revisedStateGISAGOffice { get; set; }
        public string revisedStateGISPAOCode { get; set; }
        public string revisedStateGISOtherOffice { get; set; }

    }
       

    public class ConMobileNoEmailEmpCodecls : BaseClassChange
    {       
        public string MobileNo { get; set; }
        public string revisedMobileNo { get; set; }
        public string currentMobileNo { get; set; }

        public string EmailPart1 { get; set; }
        public string EmailPart2 { get; set; }
        public string revisedEmail { get; set; }
        public string currentEmail { get; set; }

        public string EmpEcode { get; set; }
        public string revisedEmpEcode { get; set; }
        public string currentEmpEcode { get; set; }

        public string contactDetails { get; set; }
    }

}
