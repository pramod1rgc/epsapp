﻿using System;

namespace EPS.BusinessModels.LienPeriod
{
    public class TransferofflienperiodModel
    {
        public int? mslienId { get; set; }
       
        public string employeeCode { get; set; }
        public string orderNo { get; set; }
        public DateTime? orderDate { get; set; }
        public string reasonTransfer { get; set; }
        public string remarks { get; set; }
        public string relevingOrderNo { get; set; }
        public DateTime? relievingOrderDate { get; set; }
        public string relievingTime { get; set; }
        public Int32 ? relievingDesigcode { get; set; }
    
        public string department { get; set; }
        public string officeId { get; set; }
        public string officeName { get; set; }
        public string  officeNameother { get; set; }
        public string paoName { get; set; }
        public int ? paoId { get; set; }
        public string ddoName { get; set; }
        public int ? ddoId { get; set; }
        public string officeAddress { get; set; }
    
        public string ipAddress { get; set; }
        public string createdBy { get; set; }
        public string veriFlag { get; set; }
        
        public string flagUpdate { get; set; }
        public string rejectionRemark { get; set; }


    }
    public class ControllerList
    {
        public int controllerId { get; set; }
        public string controllerName { get; set; }
    }
    public class DDOList
    {
        public int ddoId { get; set; } 
        public string ddoName { get; set; }
    }
    public class OfficeAddressDetails {
       public string ddoPermId { get; set; }
     public string officeId { get; set; }
       public string officeName { get; set; }
       public string officeAddress { get; set; }
       public string officeEmailId { get; set; }
        public string officeTelePhoneNO { get; set; }
        public int ddoId { get; set; }
        public string ddoName { get; set; }
       public int paoId { get; set; }
       public string paoName { get; set; }
    
    }
}
