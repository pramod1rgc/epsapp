﻿using System;

namespace EPS.BusinessModels.LienPeriod
{
    public  class EpsEmpJoinAfterLienPeriodModel
    {
        public int msAfterLeinId { get; set; }
      
        public string employeeCode { get; set; }
        public string relievingOrderNo { get; set; }
        public DateTime ? relievingOrderDate { get; set; }
        public string relievingOffice { get; set; }
        public string relievingOfficeId { get; set; }
        public string relievingBy { get; set; }
        public int relievingDdoId { get; set; }
        public DateTime ? relievedOn { get; set; }
        public int onRelievedDesignationId { get; set; }
        public string onRelievedDesignation { get; set; }
        public int payCommissionId { get; set; }
        public string payCommission { get; set; }
        public int payScaleId { get; set; }
        public string payScale { get; set; }
        public string basicPay { get; set; }
        public string joinServiceAs { get; set; }
        public int joiningonAccountof { get; set; }
        public string joiningOrderNo { get; set; }
        public DateTime ? joiningOrderDate { get; set; }
        public string joiningTime { get; set; }
        public string officeId { get; set; }
        public string officeCityClass { get; set; }
        public int joiningDesigcode { get; set; }
        public string veriFlag { get; set; }
       
        public string ipAddress { get; set; }
        public string createdBy { get; set; }
   
        public string flagUpdate { get; set; }
        public string rejectionRemark { get; set; }

    }
}
