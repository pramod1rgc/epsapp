﻿namespace EPS.BusinessModels.LoanApplication
{
    public class PropOwner_Model
    {
        public int MsPropOwnerDetlsID;
        public int PayLoanPurposeID;
        public string OwnerName;
        public string OwnerPanNo;
        public int BankId;
        public int BranchID;
        public string BankIFSCCODE;
        public string BankAccountNo;
        public string CreatedIP;
        public string ModifiedIP;
        public string CreatedBy;
        public string CreatedDate;
        public string ModifiedBy;
        public string ModifiedDate;
    }
}
