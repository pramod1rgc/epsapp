﻿using System;

namespace EPS.BusinessModels.LoanApplication
{
    public class Sanction
    {
        public string EmpCd { get; set; }
        public int MsEmpID { get; set; }
        public int MSPayAdvEmpdetID { get; set; }
        public string SancOrdNo { get; set; }
        public DateTime SancOrdDT { get; set; }
        public int LoanAmtSanc { get; set; }
        public int LoanAmtDisbursed { get; set; }
        public string EmpName { get; set; }
        public string Description { get; set; }
        public string EmpApptType { get; set; }
        public int PayLoanRefLoanCD { get; set; }
        public string IfscCD { get; set; }
        public int? MsPropOwnerDetlsID { get; set; }
        public string BnkAcNo { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public int PriVerifFlag { get; set; }
        public string Sanction_Remarks { get; set; }
        public string Release_Remarks { get; set; }
        public string ClientIP { get; set; }
    }
}
