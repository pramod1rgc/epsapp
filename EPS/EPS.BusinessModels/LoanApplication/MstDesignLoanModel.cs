﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.LoanApplication
{
    public class  MstDesignLoanModel
    {      
        public int MsDesigMastID { get; set; }      
        public string DesigDesc { get; set; }      
    }
    public class TblMsPayBillGroup
    {
        public int BillgrId { get; set; }
        public string BillgrDesc { get; set; }
    }
}
