﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.LoanApplication
{
    public class MsAdvloanModel
    {

        public   int MsEmpId { get; set; }
        public   int PayLoanPurposeID { get; set; }
        public   bool Int_Pur_Prsn_Comp { get; set; }
        public   bool Rule_18_1964_Pur { get; set; }
        public   string Anti_Price_Pc { get; set; }
        public   bool Off_On_Leave { get; set; }
        public   bool Pre_Adv_Simi_Pur { get; set; }
        public   DateTime Adv_Draw_Date { get; set; }
        public   int Adv_Amount { get; set; }
        public   bool Comp_Certi_Info_1 { get; set; }
        public   bool Comp_Certi_Info_2 { get; set; }
        public   string Upload_Sign { get; set; }
        public   string Address { get; set; }
        public   bool Selct_Area { get; set; }
        public   bool Demar_Dev { get; set; }
        public   int Area_Sq_Ft { get; set; }
        public   int Cost { get; set; }
        public   int Amt_Act_Pay { get; set; }
        public int DesNoInsRePaidForAdv { get; set; }

        public string Purp_Acquire { get; set; }
        public   string Unexp_Port_Lease { get; set; }

        public   int Pop_Decl_1 { get; set; }
        public   int Pop_Decl_2 { get; set; }
        public   int Pop_Decl_3 { get; set; }
        public   int Flr_Area { get; set; }
        public int Est_Cost { get; set; }
        public int Amt_Adv_Req { get; set; }
        public int Num_Inst { get; set; }
        public int Pur_Area { get; set; }
        public int Const_Dec_1 { get; set; }
        public int Const_Dec_2 { get; set; }
        public int Const_Dec_3 { get; set; }
        public int Plint_Area { get; set; }
        public int Plth_Prop_Enlarge { get; set; }
        public int Coc { get; set; }
        public int Cop_Enlarge { get; set; }
        public int Tot_Cost { get; set; }
        public int AOD_Req { get; set; }
        public int Dtls_Enlarg_1 { get; set; }
        public int Dtls_Enlarg_2 { get; set; }
        public int Dtls_Enlarg_3 { get; set; }
        public DateTime Const_Date { get; set; }
        public int Prc_Settled { get; set; }
        public int Amt_Paid { get; set; }
        public int Dtls_Built_Flat_1 { get; set; }
        public int Dtls_Built_flat_2 { get; set; }
        public int Dtls_Built_flat_3 { get; set; }
    }
}
