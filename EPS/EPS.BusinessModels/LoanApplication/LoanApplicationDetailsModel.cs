﻿using System;

namespace EPS.BusinessModels.LoanApplication
{
    public class LoanApplicationDetailsModel
    {
        public int MsEmpID { get; set; }
        public int PermDdoId { get; set; }        
        public string EmpCd { get; set; }
        public string CreatedBy { get; set; }
        public string EmpApptType { get; set; }
        public bool EmpPfType { get; set; }
        public string pay_basic { get; set; }
        public string EmpFullName { get; set; }
        public DateTime? EmpSupanDt { get; set; }
        public DateTime? EmpJoinDt { get; set; }
        public string DesigDesc { get; set; }
        public int? LoanAmtSanc { get; set; }
        public int? DesNoInsRePaidForAdv { get; set; }
        public int? PriInstAmt { get; set; }
        public int? IntTotInst { get; set; }
        public int? OddInstNoInt { get; set; }
        public int? OddInstAmtInt { get; set; }
        public int? payLoanRefLoanCD { get; set; }
        public int? payLoanPurposeCode { get; set; }
        public int? MsAdvloanID { get; set; }
        public int? PayLoanPurposeID { get; set; }
        public bool IntPurPrsnComp { get; set; }
        public int? Desired_No_Installments { get; set; }
        public bool Rule_18_1964_Pur { get; set; }
        public string Anti_Price_Pc { get; set; }
        public bool Off_On_Leave { get; set; }
        public bool Pre_Adv_Simi_Pur { get; set; }
        public DateTime Adv_Draw_Date { get; set; }
        public int? Adv_Amount { get; set; }
        public bool Comp_Certi_Info_1 { get; set; }
        public bool Comp_Certi_Info_2 { get; set; }
        public string Upload_Sign { get; set; }
        public string Address { get; set; }
        public bool Selct_Area { get; set; }
        public bool Demar_Dev { get; set; }
        public int? Area_Sq_Ft { get; set; }
        public int? Cost { get; set; }
        public int? Amt_Act_Pay { get; set; }
        public string Purp_Acquire { get; set; }
        public string Unexp_Port_Lease { get; set; }
        public bool Pop_Decl_1 { get; set; }
        public bool Pop_Decl_2 { get; set; }
        public bool Pop_Decl_3 { get; set; }
        public int? Flr_Area { get; set; }
        public int? Est_Cost { get; set; }
        public int? Amt_Adv_Req { get; set; }
        public int? Num_Inst { get; set; }
        public int? Pur_Area { get; set; }
        public bool Const_Dec_1 { get; set; }
        public bool Const_Dec_2 { get; set; }
        public bool Const_Dec_3 { get; set; }
        public int? Plint_Area { get; set; }
        public int? Plth_Prop_Enlarge { get; set; }
        public int? Coc { get; set; }
        public int? Cop_Enlarge { get; set; }
        public int? Tot_Plint_Area { get; set; }
        public int? Tot_Cost { get; set; }
        public string Agency_Whom_Pur { get; set; }
        public bool Dtls_Enlarg_1 { get; set; }
        public bool Dtls_Enlarg_2 { get; set; }
        public bool Dtls_Enlarg_3 { get; set; }
        public DateTime? Const_Date { get; set; }
        public int? Prc_Settled { get; set; }
        public int? Amt_Paid { get; set; }
        public bool Dtls_Built_Flat_1 { get; set; }
        public bool Dtls_Built_flat_2 { get; set; }
        public bool Dtls_Built_flat_3 { get; set; }
        public int? PriVerifFlag { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPanNo { get; set; }
        public string Descriptions { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BankIFSCCODE { get; set; }
        public string BankAccountNo { get; set; }
        public string Remarks { get; set; }
        public string LoanApp_Remarks { get; set; }
        public string Sanction_Remarks { get; set; }
        public string Release_Remarks { get; set; }
        public string description { get; set; }   
        public string ClientIP { get; set; }
    }

    public class tblMsPayLoanRef
    {
        public int? PayLoanRefLoanCD { get; set; }
        public string PayLoanRefLoanShortDesc { get; set; }       
    }

    public class tblMsLoanStatus
    {
        public int Id { get; set; }
        public string EMPCode { get; set; }
        public string LoanCode { get; set; }
        public string PurposeCode { get; set; }
        public string Status { get; set; }
        public string ModuleStatus { get; set; }
        public int StatusId { get; set; }
    }

    public class PayLoanPurpose
    {
        public int PayLoanCode { get; set; }
        public int? PayLoanPurposeCode { get; set; }
        public string PayLoanPurposeDescription { get; set; }
    }

    public class PayLoanPurposeStatus
    {
        public string PayLoanRefLoanShortDesc { get; set; }
        public string PayLoanPurposeDescription { get; set; }
        public string Status { get; set; }
    }

    public class Employee
    {
        public string empcode { get; set; }
        public int? roleid { get; set; }
        public string ddoid { get; set; }
        public int? userid { get; set; }
        public string ServiceTime { get; set; }
        public string serviceType { get; set; }
    }

    public class EmpDesigModel
    {
        public string EmpCd { get; set; }
        public string EmpName { get; set; }
        public int BillgrID { get; set; }
        public string PermDDOId { get; set; }
    }



}
