﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.LoanApplication
{
    public class MsEmpLoanModel
    {
        public int EmpDesigCd { get; set; }
        public string EmpFirstName { get; set; }
        public int MsEmpID { get; set; }
        public string EmpCd { get; set; }
    }
}
