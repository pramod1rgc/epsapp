﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.LoanApplication
{
   public  class RecoveryChallan
    {
        public string LoanType { get; set; }
        public int payLoanRefLoanCD { get; set; }
        public int? AmountDisbursed { get; set; }
        public string CurrentRecoveryof { get; set; }
        public int? TotalNoInstalments { get; set; }
        public string Status { get; set; }
        public int? InstallmentAmount { get; set; }
        public int? TotalNoofInstallments { get; set; } 
        public int? OddInstlAmt { get; set; }
        public int? OddInstlNo { get; set; } 
        public int? OutstandingAmount { get; set; } 
        public int? LastInstalmentNoRecovered { get; set; } 
        public int? PaymentMadeThrgh { get; set; } 
        public int? ChallanNo { get; set; }
        public int? ChallanAmt { get; set; }
        public DateTime ChallanDate { get; set; }
        public int? ChequeNoDDNo { get; set; }
        public int? PaymentMode { get; set; }
        public int? UpdatedOutstandingAmount { get; set; }
        public int? UpdatedLastInstalmentNoRecovered { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedIP { get; set; }
        public int? Flag { get; set; }


    }
}
