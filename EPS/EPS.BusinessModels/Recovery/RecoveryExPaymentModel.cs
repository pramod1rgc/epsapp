﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Recovery
{
    public class RecoveryExPaymentModel
    {
        public string PayBillGroup { get; set; }
        public string Designation { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public string RecoveryType { get; set; }
        public int RecoveryTypeID { get; set; }
        public string SanctionOrderNo { get; set; }
        public DateTime SanctionOrdDate { get; set; }
        public string FinYear { get; set; }
        public string AccountNo { get; set; }
        public decimal ExcessAmt { get; set; }
        public decimal InstallmentAmt { get; set; }
        public int InstallmentNo { get; set; }
        public int OddInstallmentNo { get; set; }
        public decimal OddInstallmentAmt { get; set; }
        public int PaidInstallmentNo { get; set; }
        public string Status { get; set; }
        public int RecExcessId { get; set; }
        public string ClientIP { get; set; }

        public int DeductionTypeId { get; set; }
        public string DeductionType { get; set; }

        public int ComponentId { get; set; }
        public string ComponentName { get; set; }
        public string ComponentDesc { get; set; }
        public int PermDdoId { get; set; }
        public int AccHeadId { get; set; }
        public int EmpId { get; set; }
        public string AccHeadName { get; set; }
        public decimal BifAmt { get; set; }
        public decimal BifTotalAmt { get; set; }
        public string FinYearDesc { get; set; }
        public string CreatedIP { get; set; }
        public string CreatedBy { get; set; }
        public int FinYearId { get; set; }
        public int RecCmpId { get; set; }
        public decimal ExcessAmtPaid { get; set; }
        public string Description { get; set; }
    }
}
