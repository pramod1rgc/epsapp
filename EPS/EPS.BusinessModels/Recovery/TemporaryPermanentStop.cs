﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Recovery
{
    public class TemporaryPermanentStop
    {
        public string payMentStop { get; set; }
        public string orderNo { get; set; }
        public string orderdate { get; set; }
        public string orderfromdate { get; set; }
        public string ordertodate { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Modifieddate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedIP { get; set; }
        public string ModifiedIP { get; set; }
    }
}
