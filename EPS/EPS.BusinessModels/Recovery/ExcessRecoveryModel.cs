﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.ExcessRecovery
{
   public class ExcessRecoveryModel
    {
        //Excess Recovery
        private int billGroup;
        private int designatiuonID;
        private string empCode;
        private string recoveryTypes;
        private string recoveryTypeOthers;
        private string sanctionOrderNo;
        private string sanctionOrderDate;
        private string accountHead;
        private string excessAmountPaid;
        private string excessVia;
        private string challanNo;
        private string challanAmount;
        private string chequeDDNo;
        private DateTime challanDate;
        private string remarks;
        private string nTRPTransactionID;
        private string transactionAmount;
        private string nTRPReceiptDetails;
        private string transactionDate;
        private string recoveryComponent;
        private string recoveryAmount;
        private string recoveryTotalAmount;

        public int BillGroup { get => billGroup; set => billGroup = value; }
        public int DesignatiuonID { get => designatiuonID; set => designatiuonID = value; }
        public string EmpCode { get => empCode; set => empCode = value; }
        public string RecoveryTypes { get => recoveryTypes; set => recoveryTypes = value; }
        public string RecoveryTypeOthers { get => recoveryTypeOthers; set => recoveryTypeOthers = value; }
        public string SanctionOrderNo { get => sanctionOrderNo; set => sanctionOrderNo = value; }
        public string SanctionOrderDate { get => sanctionOrderDate; set => sanctionOrderDate = value; }
        public string AccountHead { get => accountHead; set => accountHead = value; }
        public string ExcessAmountPaid { get => excessAmountPaid; set => excessAmountPaid = value; }
        public string ExcessVia { get => excessVia; set => excessVia = value; }
        public string ChallanNo { get => challanNo; set => challanNo = value; }
        public string ChallanAmount { get => challanAmount; set => challanAmount = value; }
        public string ChequeDDNo { get => chequeDDNo; set => chequeDDNo = value; }
        public DateTime ChallanDate { get => challanDate; set => challanDate = value; }
        public string Remarks { get => remarks; set => remarks = value; }
        public string NTRPTransactionID { get => nTRPTransactionID; set => nTRPTransactionID = value; }
        public string TransactionAmount { get => transactionAmount; set => transactionAmount = value; }
        public string NTRPReceiptDetails { get => nTRPReceiptDetails; set => nTRPReceiptDetails = value; }
        public string TransactionDate { get => transactionDate; set => transactionDate = value; }
        public string RecoveryComponent { get => recoveryComponent; set => recoveryComponent = value; }
        public string RecoveryAmount { get => recoveryAmount; set => recoveryAmount = value; }
        public string RecoveryTotalAmount { get => recoveryTotalAmount; set => recoveryTotalAmount = value; }
    }
}
