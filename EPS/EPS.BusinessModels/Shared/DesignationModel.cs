﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Shared
{
   public class DesignationModel
    {
        public int DesigId { get; set; }
        public string DesigDesc { get; set; }
    }
}
