﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Shared
{
    public class PayBillGroupModel
    {
        private string BillgrId;
        private string BillgrDesc;
        private int PayBillGroupId;
        private string ServiceType;
        private string Groups;
        private int SchemeId;
        private int ServiceTypeId;
        private bool BillForGAR;
        private string SchemeCode;

        public string billgrId { get => BillgrId; set => BillgrId = value; }
        public string billgrDesc { get => BillgrDesc; set => BillgrDesc = value; }
        public int payBillGroupId { get => PayBillGroupId; set => PayBillGroupId = value; }
        public string serviceType { get => ServiceType; set => ServiceType = value; }
        public string groups { get => Groups; set => Groups = value; }
        public int schemeId { get => SchemeId; set => SchemeId = value; }
        public int serviceTypeId { get => ServiceTypeId; set => ServiceTypeId = value; }
        public bool billForGAR { get => BillForGAR; set => BillForGAR = value; }
        public string schemeCode { get => SchemeCode; set => SchemeCode = value; }
    }
}
