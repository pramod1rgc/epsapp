﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Shared
{
    public class EmpModel
    {
        public string EmpCd { get; set; }
        public string EmpName { get; set; }
    }



    public class EmpDesigModel
    {
        public string EmpCd { get; set; }
        public string EmpName { get; set; }
        public string DesignationCd { get; set; }
    }
}
