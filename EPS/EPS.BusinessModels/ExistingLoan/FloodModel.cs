﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.ExistingLoan
{
    public class FloodModel
    {
        public string EmpFirstName { get; set; }
        public string EmpCd { get; set; }
        public string empdesigcd { get; set; }
        public string DesigDesc { get; set; }
        public string PriVerifFlag { get; set; }

        public string DesigSrNo { get; set; }
        public int BillgrId { get; set; }
    }


   
    public class SanctionDetailsMOdel
    {
        //public string EmpCd { get; set; }
        public string  SancOrdNo { get; set; }
        public string SancOrdDt { get; set; }
        public int LoanAmtDisbursed { get; set; }
        public int SchemeId { get; set; }
        public int IntTotInst { get; set; }
        public int IntInstAmt { get; set; }
        public  int IntAmt { get; set; }
        public int PriTotInst { get; set; }
        public string PriVerifFlag { get; set; }
        public int PriBalance { get; set; }
        
        public int PriInstAmt { get; set; }
        public int PriLstInstRec { get; set; }
        public List<SanctionDetailsMOdel> SanctionDetails { get; set; }
        public List<string> totalEmpcode { get; set; }
        public int Mode { get; set; }


    }

    
} 
