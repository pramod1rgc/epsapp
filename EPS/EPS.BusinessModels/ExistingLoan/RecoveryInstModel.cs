﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.ExistingLoan
{
   public class RecoveryInstModel
    {
      public string OrderNo { get; set; }
      public string BillMonth{ get; set; }
      public string LoanCD   { get; set; }
      public string payloanrefloandesc { get; set; }
      public string EmpCD { get; set; }
      public int MsEmpLoanMultiInstId { get; set; }
      public int StatusId { get; set; }

    }

    public class SanctionDetailsModel
    {
        public string SancOrdDT { get; set; }
        public string SancOrdNo { get; set; }
        public int? LoanAmtDisbursed { get; set; }
        public int? PriTotInst { get; set; }
        public int? PriInstAmt { get; set; }
        public int? OddInstNoPri { get; set; }
        public int? OddInstAmtPri { get; set; }
        public int? PriLstInstRec { get; set; }
        public int? OutstandingAmt { get; set; }
        public int? DeductedAmt { get; set; }
        public int? StatusId { get; set; }

    }

    public class SnactionDataModel
    {
        public string orderNo { get; set; }
        public string orderDate { get; set; }
        public int? recoveredInstNo { get; set; }
        public int? loantype { get; set; }
        public int mode { get; set; }
        public string EmpCd { get; set; }
        public int? MsEmpLoanMultiInstId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime SancOrdDT { get; set; }
        public DateTime  OrderDt { get; set; }
     

    }


    public class InstEmpModel
    {
        public string OrderNo { get; set; }
        public DateTime? OrderDt { get; set; }
        public int? TotAmtRecov { get; set; }
        public int? StatusId { get; set; }
        public int? BillMonth { get; set; }
        public int? MsEmpLoanMultiInstId { get; set; }
        public int?  NoInstDed { get; set; }
        public string EmpCd { get; set; }
        
    }
      
}
