﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.ExistingLoan
{
    public class LoanAdvanceDetailsModel
    {
        public string PayLoanRefLoanShortDesc { get; set; }
        public string sancOrdNo { get; set; }
        public DateTime? sancOrdDT { get; set; }
        public int? loanAmtSanc { get; set; }
        public int? loanAmtDisbursed { get; set; }
        public int? priInstAmt { get; set; }
        public int? IntTotInst { get; set; }
        public int? oddInstAmtInt { get; set; }
        public string EmpCd { get; set; }
        public int? EmpDesigCd { get; set; }
        public int? LoanCd { get; set; }
        public int? priTotInst { get; set; }
        public int? totalInstallment { get; set; }
        //public int? PayLoanRefLoanCD { get; set; }

        public int? oddInstNoInt { get; set; }
        public int? oddInstNoPri { get; set; }
        public int? oddInstAmtPri { get; set; }
        public int? PSchemeID { get; set; }
        public string SchemeCode { get; set; }
        //New Added
        public int? EmpLoanDetailsID { get; set; }
        public int? MSPayAdvEmpdetID { get; set; }

        public string LoanDisc { get; set; }
        public bool status { get; set; }
        public bool IntVerifFlag { get; set; }
        public string RecoveryStatus { get; set; }
        public int SNo { get; set; }
        public int payLoanRefLoanCD { get; set; }
        public string PayLoanRefLoanHeadACP { get; set; }
        public string PriVerifFlag { get; set; }
        public string PermDdoId { get; set; }
       
    }
    public class tblMsPayLoanRef
    {

        public int PayLoanRefLoanCD { get; set; }
        public string PayLoanRefLoanShortDesc { get; set; }
        public string PayLoanRefLoanHeadACP { get; set; }

    }

    public class HeadAccountModel
    {
        //public int PSchemeID { get; set; }
        //public string SchemeCode { get; set; }
        //public string MsPayLoanRefID { get; set; }       
        //public int PayLoanRefLoanDesc { get; set; }      
        //public int intPayLoanRefLoanMaxAmt { get; set; }
        //public int PayLoanRefLoanMaxInst { get; set; }
        //public int PayLoanRefLoanIntRate { get; set; }     

        public int? PayLoanRefLoanCD { get; set; }
        public string PayLoanRefLoanShortDesc { get; set; }
        public string PayLoanRefLoanHeadACP { get; set; }
    }

    public class ExistingLoanApplicationModel
    {
        public int? MSPayAdvEmpdetID { get; set; }
        public int LoanBillId { get; set; }
        public int SancOrdId { get; set; }
        public string EmpCd { get; set; }
        public string LoanCd { get; set; }
        public string PermDdoId { get; set; }
        public string SancOrdNo { get; set; }
        public DateTime SancOrdDt { get; set; }
        public string CoopSocName { get; set; }
        public int LoanAmtSanc { get; set; }
        public string VoucherNo { get; set; }
        public DateTime VoucherDt { get; set; }
        public int? LoanAmtDisbursed { get; set; }
        public int? PriTotInst { get; set; }
        public int? PriInstAmt { get; set; }
        public int? OddInstNoPri { get; set; }
        public int? OddInstAmtPri { get; set; }
        public int? PriLstInstRec { get; set; }
        public DateTime PriRecStartDt { get; set; }
        public int? PriRecov { get; set; }
        public int? PriBalance { get; set; }
        public int? PriBalasonDt { get; set; }
        public int IntAmt { get; set; }
        public int? IntTotInst { get; set; }
        public int? IntInstAmt { get; set; }
        public int? OddInstNoInt { get; set; }
        public int? OddInstAmtInt { get; set; }
        public int IntLstInstRec { get; set; }
        public DateTime IntRecStartDt { get; set; }
        public int IntRecov { get; set; }
        public int IntBalance { get; set; }
        public DateTime IntBalasonDt { get; set; }
        public string Remarks { get; set; }
        public string PriVerifFlag { get; set; }
        public DateTime PriVerifDtts { get; set; }
        public string IntVerifFlag { get; set; }
        public string IntVerifDtts { get; set; }
        public string RecoveryStatus { get; set; }
        public int ClubWithSancId { get; set; }
        public string ClosureType { get; set; }
        public string DisbTreaCd { get; set; }
        public string DisbDdoCd { get; set; }
        public string ChgScheduleSancOrdNo { get; set; }
        public DateTime ChgScheduleSancOrdDt { get; set; }
        public string SchemeId { get; set; }
        public string OtherSchemeYn { get; set; }
        public string FinYear { get; set; }
        public string RecoveryAgencyCd { get; set; }
        public string DataSource { get; set; }
        public string LoanAgency { get; set; }
        public int NoOfAdvances { get; set; }
        public string IPAddress { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedIP { get; set; }
        public int? PayLoanPurposeCode { get; set; }
        public int? mode { get; set; }
        public int?  ddoid { get; set; }
    }

    public class ExistingLaonStatusModel
    {
        public int MSPayAdvEmpdetID { get; set; }
        public string EmpCD { get; set; }
        public string PayLoanRefLoanShortDesc { get; set; }
        public string PayLoanPurposeDescription { get; set; }
        public string Description { get; set; }
        public string PriVerifFlag { get; set; }
        public string EmpPersVerifFlag { get; set; }
        public int StatusId { get; set; }
        public int? LoanCd { get; set; }

    }



}
