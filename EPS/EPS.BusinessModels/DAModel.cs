﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class DAModel
    {
        private int dAId;
        private string dAName;
        private string dAAddress;

        public int DAId { get => dAId; set => dAId = value; }
        public string DAName { get => dAName; set => dAName = value; }
        public string DAAddress { get => dAAddress; set => dAAddress = value; }
    }
}
