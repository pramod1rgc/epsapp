﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Dashboard
{
    public class DashboardModel
    {
        public List<DashboardModel> Children { get; set; }
        public int MsMenuID { get; set; }
        public int? ParentMenu { get; set; }
        public string MainMenuName { get; set; }
        public string MenuURL { get; set; }
        public string displayName { get; set; }
        public string iconName { get; set; }
        public string route { get; set; }
        public string Status { get; set; }
        public string mpstatus { get; set; }
        public int MenupermissionID { get; set; }
        //public IList<DashboardModel> submodel {get; set;}


            // Dashboard Details
        public int ActiveUser { get; set; }
        public int TotalUser { get; set; }
        public int TotalDDO { get; set; }
    }
}
