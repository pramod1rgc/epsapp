﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.UserProfile
{
   public class UserProfileDetailsModel
    {
        public string uname { get; set; }
        public string upan { get; set; }
        public string updob { get; set; }
        public string Uempcode { get; set; }
        public string ugender { get; set; }
        public string uControllerID { get; set; }
        public string uPAOCode { get; set; }
        public string uDDOCode { get; set; }
    }
}
