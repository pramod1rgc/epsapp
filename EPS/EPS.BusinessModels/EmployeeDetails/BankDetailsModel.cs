﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.EmployeeDetails
{
    public class BankDetailsModel
    {
        // [Required]
        public string EmpCD
        {
            get; set;
        }
        public int? BankId
        {
            get; set;
        }

       public string BnkAcNo { get; set; }

        public int? BranchId { get; set; }

          public string IfscCD { get; set; }
          public string BankName { get; set; }

        public string BranchName { get; set; }
     
        public string confirmbnkAcNo { get; set; }

        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }

    }
}
