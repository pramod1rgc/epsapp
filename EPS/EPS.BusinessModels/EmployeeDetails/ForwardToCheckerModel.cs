﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.EmployeeDetails
{
  public  class ForwardToCheckerModel
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string EmpDesignation { get; set; }
        public string ServiceType { get; set; }
        public string EmpType { get; set; }
        public string EmpSubType { get; set; }
        public string EmpJoiningMode { get; set; }
        public string EmpJoiningCategory { get; set; }
        public string EmpJoiningSubCategory { get; set; }
        public DateTime? EmpDateOfBirth { get; set; }
        public DateTime ?EmpDateOfJoining { get; set; }
        public string EmpPanNo { get; set; }
        public string IpAddress { get; set; }
        public List<string> EmpCodes = new List<string>();
        public int? UserId { get; set; }
        public string VerifyFlag { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
    }
}
