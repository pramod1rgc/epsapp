﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.EmployeeDetails
{
   public class EmpPhDetails
    {
        public int MsEmpID { get; set; }
        public string PhType { get; set; }
        public string PhTypeName { get; set; }

        public decimal? PhPcnt { get; set; }
        public string PhCertNo { get; set; }
        public DateTime? PhCertDt { get; set; }
        public string PhCertAuth { get; set; }
        public string PhAdmOrdno { get; set; }
        public DateTime? PhAdmOrddt { get; set; }
        public string PhRemarks { get; set; }
        public string PhVfFlg { get; set; }
        public string EmpDoubleTa { get; set; }
        public string IsSevere { get; set; }
    }
}
