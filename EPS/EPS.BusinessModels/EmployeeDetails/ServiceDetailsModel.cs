﻿

namespace EPS.BusinessModels.EmployeeDetails
{
    public  class ServiceDetailsModel
    {
        public string PAOCode { get; set; }

        public string PAOCodeNAme { get; set; }

        public string ServiceType { get; set; }

        public string ServiceTypeName { get; set; }

        public string EmpCode { get; set; }

        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }
    }
}
