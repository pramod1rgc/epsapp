﻿using System;

namespace EPS.BusinessModels.EmployeeDetails
{
   public class FamilyDetailsModel
    {
        
        public int ?MsDependentDetailID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime ? DOB { get; set; }
        public string MaritalStatusID { get; set; }
        public string MaritalStatusName { get; set; }
        public string RelationshipId { get; set; }
        public string NationalityId { get; set; }
        public string PANNumber { get; set; }
        public long   AadhaarNumber { get; set; }
        public string Saluation { get; set; }
        public string EmpCd { get; set; }
        public string MaritalStatus { get; set; }
        public string Relationship { get; set; }
        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }
    }
}
