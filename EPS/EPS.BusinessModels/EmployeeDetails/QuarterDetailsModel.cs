﻿using System;

namespace EPS.BusinessModels.EmployeeDetails
{
    public class QuarterDetailsModel
    {
        public int MSEmpAccmID { get; set; }
        public string EmpCd { get; set; }
        public int AccmQrtrType { get; set; }
        public string AccmQrtrTypeName { get; set; }
        public int AccmQrtrAllot { get; set; }
        public string AccmQrtrAllotName { get; set; }
        public int AccmRentStat { get; set; }
        public string AccmRentStatName { get; set; }

        public int AccmRent { get; set; }

        public int AccmAddRent1 { get; set; } //Electric charge
        public int AccmWaterCharge { get; set; }
        public int AccmGarageRent { get; set; }
        public int AccmServCharge { get; set; }
        public DateTime? AccmOccupDt { get; set; }
        public DateTime? AccmVacateDt { get; set; }
        public string VacatingLetterNo { get; set; }
        public DateTime? VacatingLetterDt { get; set; }
        public string AllotLetterNo { get; set; }
        public DateTime ?AllotLetterDt { get; set; }
        public int LawnMaintCharges { get; set; }
        public string SusBookingYn { get; set; }
      
        public int QrtrOwnedBy { get; set; } // make int in table 
        public string QrtrOwnedByName { get; set; }
        public string GpraCityCode { get; set; }
        public string GpraCityName { get; set; }
        public string Aan { get; set; }
        public int CustId { get; set; }
        public string CustName { get; set; }
        public string QrtrAddress1 { get; set; }
        public string QrtrAddress2 { get; set; }

        public int CommonAreaMaint { get; set; }
       public string AllocAndVac { get; set; }


        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }



    }
}
