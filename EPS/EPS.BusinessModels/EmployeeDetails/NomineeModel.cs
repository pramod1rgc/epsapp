﻿using System;


namespace EPS.BusinessModels.EmployeeDetails
{
    public class NomineeModel
    {
        public int NomineeID { get; set; }
        public string Empcd { get; set; }
        public string NomineeFirstName { get; set; }
        public string NomineeMiddleName { get; set; }
        public string NomineeLastName { get; set; }
        public DateTime? NomineeDOBMinor { get; set; }
        public string NomineeRelation { get; set; }
        public string NomineeMajorMinorFlag { get; set; }
        public string NomineeGuardianFirstName { get; set; }
        public string NomineeGuardianMiddleName { get; set; }
        public string NomineeGuardianLastName { get; set; }
        public int NomineePercentageShare { get; set; }
        public string NomineeAddressLine1 { get; set; }
        public string NomineeAddressLine2 { get; set; }
        public string NomineeAddressLine3 { get; set; }
        public string NomineeAddressLine4 { get; set; }
        public string NomineeState { get; set; }
        public string NomineeCountry { get; set; }
        public int NomineePin { get; set; }
        public string NomineeRelationName { get; set; }
        public string NomineeSaluation { get; set; }
        public string GuardianSaluation { get; set; }
        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }

    }
}
