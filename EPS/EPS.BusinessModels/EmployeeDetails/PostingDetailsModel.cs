﻿using System;


namespace EPS.BusinessModels.EmployeeDetails
{
    public class PostingDetailsModel
    {
        public string Name { get; set; }

        public string EmpCd { get; set; }

        public string TACityName { get; set; }

        public string HRACityName { get; set; }
        public string Designation { get; set; }

        public int DesigID { get; set; }
        public string DesigDesc { get; set; }
        public DateTime? EmpCuroffDt { get; set; }
        public DateTime? EmpCurdesigDt { get; set; }
        public string EmpCurpostMode { get; set; }
        public int EmpFieldDeptCd { get; set; }
        public int EmpDesignation { get; set; }
        public string JoiningMode { get; set; }
        public string CityClassTA { get; set; }
        public string PayCityClass { get; set; }
        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }

    }
}
