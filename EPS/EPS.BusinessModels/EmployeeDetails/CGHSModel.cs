﻿

namespace EPS.BusinessModels.EmployeeDetails
{
   public class CGHSModel
    { 
        public string EmpCd { get; set; }
        public string EmpCghsFlag { get; set; }
        public string CgisDeduct { get; set; }
        public string EmpCghsNo { get; set; }
        public string CghsDescription { get; set; }
        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }

    }
}
