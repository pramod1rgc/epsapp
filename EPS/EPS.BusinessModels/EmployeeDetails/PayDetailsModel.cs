﻿using System;
using System.Collections.Generic;

namespace EPS.BusinessModels.EmployeeDetails
{
    public class PayDetailsModel
    {


        public int ?Organisation_Type { get; set; }

        public string Organisation_TypeName { get; set; }


        public string EmpCode { get; set; }

        public int? PayComm { get; set; }
        public string  PayCommName { get; set; }

        public int? BasicPay { get; set; }

        public int ?PayIndex { get; set; }

        public int ?PayIndexName { get; set; }


        public string PayLevel { get; set; }

        public string PayLevelName { get; set; }

        public DateTime? BasicPayDT { get; set; }

        public DateTime? IncrpayDT { get; set; }

    
        public int ?GradePay { get; set; }

        public int ?PayInPb { get; set; } //pay in payband

        public string PayBand { get; set; } //pay in payband

        public int ?PscScaleCd { get; set; }
        public string PayScalePscDscr { get; set; }

        public List<NonComputationalDues> Dues { get; set; }

        public List<NonComputationalDeduction> Deduction { get; set; }

        public int ?EntitledOffVehID { get; set; }
      
        public string EntitledOffVehIDName { get; set; }

        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }

    }
    public class NonComputationalDues
    {
        //public int MsPayEligibleRefID { get; set; }
        //public string NonComputationaDues { get; set; }

        public int DuesId { get; set; }
        public string DuesName { get; set; }


    }

    public class NonComputationalDeduction
    {
        //public int RSchemeId { get; set; }
        //public string NonComputationaDeductions { get; set; }

        public int DeductionId { get; set; }
        public string DeductionName { get; set; }
    }



}


