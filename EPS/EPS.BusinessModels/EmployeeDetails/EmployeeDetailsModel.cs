﻿using System;

namespace EPS.BusinessModels
{
    
        public class EmployeeDetailsModel
        {
            public string id { get; set; }

            public int MsEmpID { get; set; }
            public string EmpCd { get; set; }
            public string EmpApptType { get; set; }
            public string EmpApptTypeName { get; set; }
            public string EmpPfType { get; set; }
            public string EmpPermDDOId { get; set; }
            public string EmpOffId { get; set; }
            public string EmpTitle { get; set; }
            public string EmpFirstName { get; set; }

            public string EmpMiddleName { get; set; }
            public string EmpLastName { get; set; }
            public string Name { get; set; }
            public string EmpGender { get; set; }
            public string GenderName { get; set; }
            public DateTime? EmpDOB { get; set; }
            public DateTime EmpEntryDt { get; set; }
            public int EmpFieldDeptCd { get; set; }
            public int MsDesigMastID { get; set; }
            public int EmpDesigCd { get; set; }
            public DateTime? EmpCuroffDt { get; set; }
            public DateTime? EmpJoinDt { get; set; }

            public string Emp_type { get; set; }
            public string Emp_typeName { get; set; }

            public string Emp_adhaar_no { get; set; }
            public string Emp_pan_no { get; set; }
            public string EmpSubType { get; set; }
            public string empSubTypeName { get; set; }

            public string Joining_Catogary { get; set; }
            public string Joining_CatogaryName { get; set; }

            public string EmpPhFlag { get; set; }
            public DateTime? EmpServendDt { get; set; }
            public string Emp_mobile_no { get; set; }
            public string Emp_Email { get; set; }
            public string Emp_Email2 { get; set; }

            public string ServiceType { get; set; }
            public string ServiceTypeName { get; set; }
            public string DocumentUpload { get; set; }
            public string DocumentUploadName { get; set; }
            public bool? isDeput { get; set; }
            public string DepartmentCD { get; set; }
            public DateTime? RegularisationDate { get; set; }
            public string RegularisationTime { get; set; }
            public string DeputTypeID { get; set; }
            public DateTime? LastDateIncrement { get; set; }
            public string DeputName { get; set; }
            public string EmpVerifFlag { get; set; }
            public string EmpRemark { get; set; }
            public int UserId { get; set; }
            public string IpAddress { get; set; }
            public string VerifyFlag { get; set; }

            //  public async Task<IActionResult> UploadFiles(List<IFormFile> files)

            //------------------------ For physical disabilities-----------------------------------------------

            public string PhType { get; set; }
            public string PhTypeName { get; set; }
            public int? MSEmpPhId { get; set; }
            public decimal? PhPcnt { get; set; }
            public string PhCertNo { get; set; }
            public DateTime? PhCertDt { get; set; }
            public string PhCertAuth { get; set; }
            public string PhAdmOrdno { get; set; }
            public DateTime? PhAdmOrddt { get; set; }
            public string PhRemarks { get; set; }
            public string PhVfFlg { get; set; }
            public string EmpDoubleTa { get; set; }
            public string IsSevere { get; set; }


        }
    
}
