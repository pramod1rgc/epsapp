﻿using System;
 

namespace EPS.BusinessModels.EmployeeDetails
{
    public class CGEGISModel
    {
        public string EmpCode { get; set; } 
        public int? Gis_deduction { get; set; }

        public string Gis_deductionName { get; set; }

        public string PayGisApplicable { get; set; }

        public string PayGisApplicableName { get; set; }

        public int ? PayGISGrp_CD { get; set; }

        public string GroupName { get; set; }

        public DateTime ?GisMembershipDT { get; set; }

        public string IpAddress { get; set; }
        public int UserId { get; set; }
        public string VerifyFlag { get; set; }
      //PayGISGrp_CD
        // PayGisApplicable
        // gis_deduction
        // GisVerifyFlag
        //GisEnrtyDtts
        //GisUpdDtts
    }
}
