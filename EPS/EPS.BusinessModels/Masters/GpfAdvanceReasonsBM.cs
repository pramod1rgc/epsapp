﻿using System;
namespace EPS.BusinessModels.Masters
{
    public class GpfAdvanceReasonsBM
    {
        public string msPfAgencyID { get; set; }
        public int? pfRefNo { get; set; }
        public string pfType { get; set; }
        public string pfTypeName { get; set; }
        public int? pfTypeID { get; set; }
        public string mainReasonText { get; set; }
        public int? mainReasonID { get; set; }
        public string reasonDescription { get; set; }
        public string outstandingLimitForPreviousAdvance { get; set; }
        public string monthsBetweenAdvances { get; set; }
        public string sealingForAdvancePay { get; set; }
        public string sealingForAdvanceBalance { get; set; }
        public string convertWithdrawals { get; set; }
        public string noOfAdvances { get; set; }
        public string pFRuleReferenceNumber { get; set; }
        public string AdvWithdraw { get; set; }
        public string ipAddress { get; set; }
        public string minService { get; set; }
    }
}
