﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class MsStateAgBM
    {
        public int MsStateAgId { get; set; }
        public string PfType { get; set; }
        public string StateName { get; set; }
        public string PfTypeDesc { get; set; }
        public string StateAgCode { get; set; }
        public string StateAgDesc { get; set; }
        public string StateCode { get; set; }
        public string CreatedIP { get; set; }
    }
  public class StateBM
    {
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }
  public class PfTypeBM
    {
        public string PfType {get; set; }
        public string PfTypeDesc {get; set;}
    }
}
