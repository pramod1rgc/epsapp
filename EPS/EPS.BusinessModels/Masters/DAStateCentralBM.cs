﻿namespace EPS.BusinessModels.Masters
{
    public  class DAStateCentralBM
    {
    public int MsDaMasterID { get; set; }
    public string DAType { get; set; }
    public string DASubType { get; set; }
    public int? Stateid { get; set; }
    public decimal CurrentDrate { get; set; }
    public string CurrentWEF { get; set; }
    public string OrderNo { get; set; }
    public string OrderDate { get; set; }
    public decimal NewDrate { get; set; }
    public string NewWEF { get; set; }
    public string IpAddress { get; set; }
    public int PayCommissionID { get; set; }
        public string PayCommissionName { get; set; }
    }
}
