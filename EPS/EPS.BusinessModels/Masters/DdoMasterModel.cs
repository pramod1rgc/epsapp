﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class DdoMasterModel: DDO
    {
        //private int ddoId;
        //private string ddoCode;
        //private string ddoName;
        //private string ddoNameLang;

        private int controllerId;
        private string controllerCode;
        private int paoId;
        private string paoCode;
        private string ddoTypeText;
        private string stateName;
        private string cityName;


        public int ControllerId { get => controllerId; set => controllerId = value; }
        public string ControllerCode { get => controllerCode; set => controllerCode = value; }
        public int PaoId { get => paoId; set => paoId = value; }
        public string PaoCode { get => paoCode; set => paoCode = value; }
        public string DdoTypeText { get => ddoTypeText; set => ddoTypeText = value; }      
        public string StateName { get => stateName; set => stateName = value; }
        public string CityName { get => cityName; set => cityName = value; }

    }

  public  class ControllerMs
    {
        private int controllerId;
        private string controllerCode;
        public int ControllerId { get => controllerId; set => controllerId = value; }
        public string ControllerCode { get => controllerCode; set => controllerCode = value; }
    }

   public class DDO
    {
        private string ddoId;
        private string ddoCode;
        private string ddoName;
        private string ddoLang;
        private int ddoTypeId;
        private string ddoAddress;
        private int pinCode;
        private string contactNo;
        private string faxNo;
        private string emailAddress;
        private int stateId;
        private int cityId;

        public string DdoId { get => ddoId; set => ddoId = value; }
        public string DdoCode { get => ddoCode; set => ddoCode = value; }
        public string DdoName { get => ddoName; set => ddoName = value; }
        public string DdoLang { get => ddoLang; set => ddoLang = value; }
        public int DdoTypeId { get => ddoTypeId; set => ddoTypeId = value; }
        public string DdoAddress { get => ddoAddress; set => ddoAddress = value; }
        public int PinCode { get => pinCode; set => pinCode = value; }
        public string ContactNo { get => contactNo; set => contactNo = value; }
        public string FaxNo { get => faxNo; set => faxNo = value; }
        public string EmailAddress { get => emailAddress; set => emailAddress = value; }
        public int StateId { get => stateId; set => stateId = value; }
        public int CityId { get => cityId; set => cityId = value; }

    }

   public class PAO
    {
        private int paoId;
        private string paoCode;
        public int PaoId { get => paoId; set => paoId = value; }
        public string PaoCode { get => paoCode; set => paoCode = value; }
       

    }

    public class DDOType
    {
        private int ddoTypeId;
        private string ddoTypeText;
        public int DdoTypeId { get => ddoTypeId; set => ddoTypeId = value; }
        public string DdoTypeText { get => ddoTypeText; set => ddoTypeText = value; }
    }

    public class State
    {
        private int stateId;
        private string stateText;
        public int StateId { get => stateId; set => stateId = value; }
        public string StateText { get => stateText; set => stateText = value; }
    }

    public class City
    {
        private int cityId;
        private string cityText;
        public int CityId { get => cityId; set => cityId = value; }
        public string CityText { get => cityText; set => cityText = value; }
    }


}
