﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.Masters
{
    public class PraoMasterModel : IValidatableObject
    {
        public int Mode { get; set; }
        public int? ControllerID { get; set; }
        [Required]
        [DisplayName("Controller Name is required")]
        public string ControllerName { get; set; }
        [Required]
        [DisplayName("Controller Code is required")]
        public string ControllerCode { get; set; }
        public int? ControllerStatusId { get; set; }
        public string Address { get; set; }
        public int? Pincode { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public int? StateID { get; set; }
        public int? cityID { get; set; }
        public int? IsActive { get; set; }

        public string statecode { get; set; }
        [Required]
        [DisplayName("State Name is required")]
        public string StateName { get; set; }
        public int? DistrictId { get; set; }
        [Required]
        [DisplayName("City Name is required")]
        public string DistrictName { get; set; }
        public int? DistrictStateID { get; set; }
        public string IpAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //double fromToYear = Convert.ToDouble(FinancialYearFrom);
            //double toToYear = Convert.ToDouble(FinancialYearTo);

            //if (fromToYear > toToYear)
            //{
            //    yield return new ValidationResult("Financial Year To can't be less than Financial Year From",

            //        new[] { "Financial Year To" });
            //}

            if (string.IsNullOrEmpty(ControllerName))
            {
                yield return new ValidationResult("Please select ControllerName",

                    new[] { "ControllerName" });
            }

            if (string.IsNullOrEmpty(ControllerCode))
            {
                yield return new ValidationResult("Please select Controller Code",

                    new[] { "Controller Code" });
            }

            if (string.IsNullOrEmpty(StateName))
            {
                yield return new ValidationResult("Please select State Name",

                    new[] { "State Name" });
            }

            if (string.IsNullOrEmpty(DistrictName))
            {
                yield return new ValidationResult("Please select District Name",

                    new[] { "District Name" });
            }           
        }
    }

    public class ProMasterDetails
    {
        public int MsControllerID { get; set; }
        public int ControllerID { get; set; }
        public string ControllerCode { get; set; }
        public string ControllerName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public int? StateID { get; set; }
        public int? cityID { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }

    }

}
