﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace EPS.BusinessModels.Masters
{
    public class HRA_Model
    {
        public int HraMasterID { get; set; }
        public string StateCode { get; set; }
        public string CityCode { get; set; }
        public string CityClass { get; set; }
        public string HraAmount { get; set; }
        public string PayCommisionName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string ClassName { get; set; }
        public int SerialNo { get; set; }
        public int TptaMasterID { get; set; }
        public string UnionTerritoryCode { get; set; }
        public string UnionTerritory { get; set; }
        public string TptaAmount { get; set; }
        public string EmployeeType { get; set; }
        public string payCommissionCode { get; set; }
        public int stateId { get; set; }
        public string loginUser { get; set; }
        public string empTypeID { get; set; }
        public string ClientIP { get; set; }
        public string payscaleLevel { get; set; }
        public int PayCommID { get; set; }
        public string slabtypDesc { get; set; }
        public int slabType { get; set; }        
        public string minvalue { get; set; }
        public string slabNo { get; set; }
        public string City { get; set; }
        public string lowerLimit { get; set; }
        public string upperLimit { get; set; }
        public string payScaleID { get; set; }
        public string value { get; set; }
        public string PayScaleCode { get; set; }
       
    }
    



}
