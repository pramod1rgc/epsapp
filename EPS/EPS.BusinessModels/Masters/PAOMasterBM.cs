﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.Masters
{
    public class PAOMasterBM
    {
        public string MsPAOID { get; set; }
        [Required]
        [DisplayName("PAO Code")]
        public string PAOCode { get; set; }
        public string PAOName { get; set; }
        public string PAOLang { get; set; }
        public string PAOAddress { get; set; }
        public string PAODescription { get; set; }
       // [RegularExpression(@"^\d{6}(-\d{4})?$", ErrorMessage = "please enter valid postal code")]
        public int PinCode { get; set; }
        [DisplayName("Contact No")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "please enter valid contact no")]
        public string ContactNo { get; set; }
        public string FaxNo { get; set; }
        public string EmailAddress { get; set; }
        [Required]
        [DisplayName("State Name")]
        [Range(1,int.MaxValue,ErrorMessage ="please select state name")]
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        [Required]
        public int ControllerId { get; set; }
        public string ControllerCode { get; set; }

    }
}
