﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
    public class FdMaster_Model
    {
        public int FdMasterID { get; set; }
        public int ID { get; set; }
        public string RelationShip { get; set; }
        public string SerialNo { get; set; }
        public int CommutationPer { get; set; }
        public int Age { get; set; }
        public double CommutationValue { get; set; }
        public string loginUser { get; set; }
        public string ClientIP { get; set; }
    }
}
