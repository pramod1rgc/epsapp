﻿using System;

namespace EPS.BusinessModels.Masters
{
   public class GpfWithdrawRules
    {
        public int MsGpfWithdrawRulesRefID { get; set; }
        public int PfRefNo { get; set; }
        public string PfType { get; set; }
        public DateTime? RuleApplicableFromDate { get; set; }
        public DateTime? PfTypeValidTillDate { get; set; }
        public int NoOfWithdraw { get; set; }
        public int MaxPerOfBalance { get; set; }
        public int MinService { get; set; }
        public int MaxServiceWithPurpose { get; set; }
        public int MaxServiceWithoutPurpose { get; set; }
        public int SealingWithPay { get; set; }
        public string GpfRuleRefNo { get; set; }
    }
}
