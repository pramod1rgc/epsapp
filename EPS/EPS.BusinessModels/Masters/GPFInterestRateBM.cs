﻿using System;
namespace EPS.BusinessModels.Masters
{
    public class GPFInterestRateBM
    {
        public int GPFInterestID { get; set; }
      public string WefMonthYear { get; set; }
      public string ToMonthYear { get; set; }
        public decimal NewGPFInterestRate { get; set; }
        public string GPFRuleReferenceNumber { get; set; }
        public string ipAddress { get; set; }
    }
}
