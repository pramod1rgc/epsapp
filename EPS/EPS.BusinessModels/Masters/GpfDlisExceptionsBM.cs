﻿using System;


namespace EPS.BusinessModels.Masters
{
    public class GpfDlisExceptions
    {
        public int MsGpfDlisExceptionsRefID { get; set; }
        public string MsPfTypeId { get; set; }
        public DateTime? RuleApplicableFromDate { get; set; }
        public DateTime? PfTypeValidTillDate { get; set; }
        public string MsPayCommId { get; set; }
        public string FromMsPayScaleID { get; set; }
        public string ToMsPayScaleID { get; set; }
        public int DlisAvgAmt { get; set; }
        public string GpfRuleRefNo { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public string ToPayScale { get; set; }
        public string FromPayScale { get; set; }
        public string PfDesc { get; set; }
        public string PayCommDesc { get; set; }
        public string MsPayScaleID { get; set; }
    }
    public class PfType
    {
        public string MsPfTypeId { get; set; }
        public string PfTypes { get; set; }
        public string PfDesc { get; set; }
    }
    public class PayScale
    {
        public string MsPayScaleID { get; set; }
        public string PayScalePscScaleCD { get; set; }
        public int PayScalePscPayCommCD { get; set; }
    }
}
