﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
    public class DuesDefinationandDuesMasterModel
    {
        public int MsPayItemsID { get; set; }
        public int PayItemsCD { get; set; }

        public string PayItemsNameShort { get; set; }
        public string PayItemsName { get; set; }

        public int PayItemsComputable { get; set; }

        public DateTime? PayItemsWef { get; set; }
        public DateTime? withEffectFrom { get; set; }
        public DateTime? withEffectTo { get; set; }
        public DateTime PayItemsUpdDt { get; set; }


        public string CreatedIP { get; set; }
        public string ModifiedIP { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public string ModifiedDate { get; set; }

        public int PayItemsIsTaxable { get; set; }

    }


    public class DuesRateandDuesMasterModel
    {


        public DuesRateandDuesMasterModel()
        {
            this.RateDetails = new List<rateDetails>();
        }
        public int? MsPayRatesID { get; set; }

        public int MsDueRtDetailId { get; set; }

        public int PayRatesPayItemCD { get; set; }
        public int PayRatesRuleID { get; set; }
        public int PayRatesRateID { get; set; }
        public int PayRatesRateSlNo { get; set; }
        public string PayRatesRateDesc { get; set; }
        public DateTime PayRatesFromDate { get; set; }
        public DateTime PayRatesToDate { get; set; }
        public int PayRatesScaleFrom { get; set; }
        public int PayRatesScaleTo { get; set; }
        public int PayRatesSlabFrom { get; set; }
        public int PayRatesSlabTo { get; set; }
        public string PayRatesCityCD { get; set; }
        public int PayRatesGroupCD { get; set; }
        public float PayRatesStateCodeCensus { get; set; }
        public float PayRatesValue { get; set; }
        public int PayRatesAddFixAmt { get; set; }
        public int PayRatesMaxValue { get; set; }
        public int PayRatesMinValue { get; set; }
        public int PayRatesVideLetterNo { get; set; }
        public DateTime PayRatesVideLetterDt { get; set; }
        public DateTime PayRatesUpdDt { get; set; }
        public string CreatedIP { get; set; }
        public string ModifiedIP { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }


        public DateTime? WithEffectTo { get; set; }

        public DateTime? WithEffectFrom { get; set; }
        public int PayCommission { get; set; }

        public string PayCommDesc { get; set; }

        public int SlabType { get; set; }

        public string MsSlabTypeDes { get; set; }
        public int MsOrganisation { get; set; }
        public string OrganizationType { get; set; }

        public string Description { get; set; }

        public int MsCityclassID { get; set; }
        public int CityClass { get; set; }
        public string PayCityClass { get; set; }

        public int StateId { get; set; }
        public string state { get; set; }

        public string StateName { get; set; }

        public int DuesCodeDuesDefination { get; set; }

        public string PayItemsName { get; set; }
        public string ipAddress { get; set; }
        public string Value { get; set; }

        public string VideLetterNo { get; set; }
        public DateTime? VideLetterDate { get; set; }

        public string Activated { get; set; }
        public List<rateDetails> RateDetails { get;  }
    }

    public class rateDetails
    {

        public int SlabNo { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public int ValueDuesRate { get; set; }
        public int MinAmount { get; set; }
        public int MsDueRtDetailId { get; set; }
    }



}
