﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class DeductionMasterBM
    {
        private int categoryID;
        private string categoryName;

        public int CategoryID { get => categoryID; set => categoryID = value; }
        public string CategoryName { get => categoryName; set => categoryName = value; }
        
    }


    public class DeductionDescriptionBM: DeductionMasterBM
    {
        private int payItemsCD;
        private string payItemsName;
        private string payItemsNameShort;
        private int grantNo;
        private string functionalHead;
        private string objectHead;
        private Boolean exemptedFromITax;
        private Boolean autoCalculated;
        private Boolean subtractedFromSalary;

        public int PayItemsCD { get => payItemsCD; set => payItemsCD = value; }
        public string PayItemsName { get => payItemsName; set => payItemsName = value; }
        public string PayItemsNameShort { get => payItemsNameShort; set => payItemsNameShort = value; }
        public int GrantNo { get => grantNo; set => grantNo = value; }
        public string FunctionalHead { get => functionalHead; set => functionalHead = value; }
        public string ObjectHead { get => objectHead; set => objectHead = value; }
        public bool ExemptedFromITax { get => exemptedFromITax; set => exemptedFromITax = value; }
        public bool AutoCalculated { get => autoCalculated; set => autoCalculated = value; }
        public bool SubtractedFromSalary { get => subtractedFromSalary; set => subtractedFromSalary = value; }
    }

    public class DeductionRateAndDeductionMasterModel
    {


        public DeductionRateAndDeductionMasterModel()
        {
            this.RateDetails = new List<DeductionRateDetails>();
        }
        public int? MsPayRatesID { get; set; }
        public int MsDueRtDetailId { get; set; }
        public int PayRatesPayItemCD { get; set; }
        public int PayRatesRuleID { get; set; }
        public int PayRatesRateID { get; set; }
        public int PayRatesRateSlNo { get; set; }
        public string PayRatesRateDesc { get; set; }
        public DateTime PayRatesFromDate { get; set; }
        public DateTime PayRatesToDate { get; set; }
        public int PayRatesScaleFrom { get; set; }
        public int PayRatesScaleTo { get; set; }
        public int PayRatesSlabFrom { get; set; }
        public int PayRatesSlabTo { get; set; }
        public string PayRatesCityCD { get; set; }
        public int PayRatesGroupCD { get; set; }
        public float PayRatesStateCodeCensus { get; set; }
        public float PayRatesValue { get; set; }
        public int PayRatesAddFixAmt { get; set; }
        public int PayRatesMaxValue { get; set; }
        public int PayRatesMinValue { get; set; }
        public int PayRatesVideLetterNo { get; set; }
        public DateTime PayRatesVideLetterDt { get; set; }
        public DateTime PayRatesUpdDt { get; set; }
        public string CreatedIP { get; set; }
        public string ModifiedIP { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime? WithEffectTo { get; set; }
        public DateTime? WithEffectFrom { get; set; }
        public int PayCommission { get; set; }
        public string PayCommDesc { get; set; }
        public int SlabType { get; set; }
        public string MsSlabTypeDes { get; set; }
        public int MsOrganisation { get; set; }
        public string OrganizationType { get; set; }
        public string Description { get; set; }
        public int MsCityclassID { get; set; }
        public int CityClass { get; set; }
        public string PayCityClass { get; set; }
        public int StateId { get; set; }
        public string state { get; set; }
        public string StateName { get; set; }
        public int DuesCodeDuesDefination { get; set; }
        public string PayItemsName { get; set; }
        public string ipAddress { get; set; }
        public string Value { get; set; }
        public string VideLetterNo { get; set; }
        public DateTime? VideLetterDate { get; set; }
        public string Activated { get; set; }
        public List<DeductionRateDetails> RateDetails { get; }
    }

    public class DeductionRateDetails
    {

        public int SlabNo { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public int ValueDuesRate { get; set; }
        public int MinAmount { get; set; }
        public int MsDueRtDetailId { get; set; }
    }

}
