﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class CitymasterModel
    {
        public int? msCityclassID { get; set; }

        public string CclCityclass { get; set; }

        public int CclPaycommId { get; set; }

        public string PayCommDesc { get; set; }
        public string UserIp { get; set; }

        public string UserName { get; set; }
    }
}
