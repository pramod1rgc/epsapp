﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class PayScaleModel
    {
        private int? msPayScaleID;
        private int? msCddirID;
        private string cddirCodeText;
        private int? paysfm_id;
        private string paysfm_Text;
        private int? paysGroup_id;
        private string paysGroup_Text;
        private string payScaleCode;
        private string payScale;
        private decimal? payScaleGradePay;
        private int? payGISGroupID;
        private string payGISGroupName;
        private string payScaleWithEffectFrom;
        private string payScaledescription;
        private string payScaleLevel;



        public decimal? PayScalePscLoLimit  { get; set; }
        public decimal? PayScalePscInc1     { get; set; }
        public decimal? PayScalePscStage1   { get; set; }
        public decimal? PayScalePscInc2     { get; set; }
        public decimal? PayScalePscStage2   { get; set; }
        public decimal? PayScalePscInc3     { get; set; }
        public decimal? PayScalePscStage3   { get; set; }
        public decimal? PayScalePscInc4     { get; set; }
        public decimal? PayScalePscUpLimit  { get; set; }
        public int? MsCddirID { get => msCddirID; set => msCddirID = value; }
        public string CddirCodeText { get => cddirCodeText; set => cddirCodeText = value; }
        public int? Paysfm_id { get => paysfm_id; set => paysfm_id = value; }
        public string Paysfm_Text { get => paysfm_Text; set => paysfm_Text = value; }
        public int? PaysGroup_id { get => paysGroup_id; set => paysGroup_id = value; }
        public string PaysGroup_Text { get => paysGroup_Text; set => paysGroup_Text = value; }
        public string PayScaleCode { get => payScaleCode; set => payScaleCode = value; }
        public string PayScale { get => payScale; set => payScale = value; }
        public decimal? PayScaleGradePay { get => payScaleGradePay; set => payScaleGradePay = value; }
        public int? PayGISGroupID { get => payGISGroupID; set => payGISGroupID = value; }
        public string PayGISGroupName { get => payGISGroupName; set => payGISGroupName = value; }
        public string PayScaleWithEffectFrom { get => payScaleWithEffectFrom; set => payScaleWithEffectFrom = value; }
        public string PayScaledescription { get => payScaledescription; set => payScaledescription = value; }
        public string PayScaleLevel { get => payScaleLevel; set => payScaleLevel = value; }
        public int? MsPayScaleID { get => msPayScaleID; set => msPayScaleID = value; }
        public string ipAddress { get; set; }
    }
}
