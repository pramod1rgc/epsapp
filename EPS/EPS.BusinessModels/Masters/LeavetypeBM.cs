﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
    public class LeavetypeBM
    {
        public int LeaveTypeID { get; set; }
        public string LeaveTypeCd { get; set; }
        public string LeaveTypeDesc { get; set; }
        public string WEF { get; set; }
        public string WET { get; set; }
        private bool _IsActive;
        public bool IsActive {get;set;}
    }
}
