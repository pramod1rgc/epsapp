﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
  public static class PsuMasterModel
    {
        public class State
        {
            public int StateId { get; set; }
            public string StateName { get; set; }
        }

        public class City
        {
            public int CityId { get; set; }
            public string CityName { get; set; }
            public int StateId { get; set; }
        }

        public class PsuMaster
        {       
            public int PsuId { get; set; }
            public string PsuCode { get; set; }
            public string PDesc { get; set; }
            public string PDescBilingual { get; set; }
            public string PAddress { get; set; }
            public int PPinCode { get; set; }
            public string PPhoneNo { get; set; }
            public string PFaxNo { get; set; }
            public string PEmail { get; set; }
            public int PStateName { get; set; }
            public int PCity { get; set; }
            public int PIsBankDetails { get; set; }
            public string PChequeInFavour { get; set; }
            public string PIfscCode { get; set; }
            public string PBankName { get; set; }
            public string PBranchName { get; set; }
            public string PBankAccNum { get; set; }
            public string PConfirmAccNum { get; set; }          
            public string IPAddress { get; set; }
            public string StateName { get; set; }
            public string CityName { get; set; }
        }
    }

    
}
