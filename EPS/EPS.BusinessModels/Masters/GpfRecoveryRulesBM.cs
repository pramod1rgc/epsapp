﻿using System;

namespace EPS.BusinessModels.Masters
{
    public class GpfRecoveryRulesBM
    {
        public int? MsGpfRecoveryID { get; set; }
        public string PfType { get; set; }
        public DateTime? RuleApplicableFromDate { get; set; }
        public DateTime? RulesValidTillDate { get; set; }
        public string GpfRuleRefNo { get; set; }
        public string ipAddress { get; set; }
        public string IsActive { get; set; }
        public decimal minInstalmentst { get; set; }
        public decimal  maxInstalmentst { get; set; }
        public Boolean oddInstallment { get; set; }
    }
}
