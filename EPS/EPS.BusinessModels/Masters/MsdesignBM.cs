﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Masters
{
   public class MsdesignBM
    {
        public int CommonDesigSrNo { get; set; }
        public string CommonDesigDesc { get; set; }
        public int CommonDesigGroupCD { get; set; }
        public string GroupCDDesc { get; set; }
        public int CommonDesigSuperannAge { get; set; }
        public bool IsActive { get; set; }



    }
}
