﻿using System;

namespace EPS.BusinessModels.Masters
{
    public class GpfAdvanceRulesBM
    {
        public int MsGpfAdvanceRulesRefID { get; set; }
        public int PfRefNo { get; set; }
        public string PfType { get; set; }
        public DateTime? RuleApplicableFromDate { get; set; }
        public DateTime? RulesValidTillDate { get; set; }
        public string GpfRuleRefNo { get; set; }
        public int MonthsBeforeRetirement { get; set; }
        public string ipAddress { get; set; }
        public string IsActive { get; set; }
    }
}   
