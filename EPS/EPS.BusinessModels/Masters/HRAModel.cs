﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.Masters
{
    public class HRAModel
    {
        public HRAModel()
        {
            this.RateDetails = new List<HRARateModel>();
        }

        public int HraMasterID { get; set; }
        public string StateCode { get; set; }
        public string CityCode { get; set; }
        [Required]
        public string CityClass { get; set; }
        [Required]
        public string HraAmount { get; set; }
        [Required]
        public string PayCommisionName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        [Required]
        public string ClassName { get; set; }
        public int SerialNo { get; set; }
        public string payScaleID { get; set; }

        public string PayScaleCode { get; set; }
        public string UnionTerritoryCode { get; set; }
        public string UnionTerritory { get; set; }
        public string City { get; set; }
        public string EmployeeType { get; set; }
        public string payCommissionCode { get; set; }
        public int stateId { get; set; }
        public string loginUser { get; set; }
        public string empTypeID { get; set; }
        public string ClientIP { get; set; }
        public string payscaleLevel { get; set; }
        public int PayCommID { get; set; }
        public string slabtypDesc { get; set; }

        [Required]
        public string LowerLimit { get; set; }
        [Required]
        public string UpperLimit { get; set; }
        [Required]
        public string TptaAmount { get; set; }
        [Required]
        public string Minvalue { get; set; }
        [Required]
        public string SlabNo { get; set; }

        public int TptaMasterID { get; set; }

        [Required]
        public List<HRARateModel> RateDetails { get; set; }
    }

    public class HRARateModel : IValidatableObject
    {
        [Required]
        public int slabType { get; set; }
        [Required]
        public string tptaAmount { get; set; }
        [Required]
        public string minvalue { get; set; }
        [Required]
        public string slabNo { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string lowerLimit { get; set; }
        [Required]
        public string upperLimit { get; set; }
        public int tptaMasterID { get; set; }
        [Required]
        public int StateId { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (slabNo == "")
            {
                yield return new ValidationResult("Slab No Required", new[] { "slabNo" });
            }
            if (lowerLimit == "")
            {
                yield return new ValidationResult("Lower Limit Required", new[] { "lowerLimit" });
            }
            if (upperLimit == "")
            {
                yield return new ValidationResult("Upper Limit Required", new[] { "upperLimit" });
            }
            if (minvalue == "")
            {
                yield return new ValidationResult("Min Value Required", new[] { "minvalue" });
            }
            if (tptaAmount == "")
            {
                yield return new ValidationResult("Tpta Amount Required", new[] { "tptaAmount" });
            }
            if (slabType == 0)
            {
                yield return new ValidationResult("Slab Type Required", new[] { "slabType" });
            }
        }
    }
}
