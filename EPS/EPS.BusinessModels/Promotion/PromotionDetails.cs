﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.Promotion
{
    public class PromotionDetails : IValidatableObject
    {
        public int TransferDetailId { get; set; }

        [DisplayName("Promotion Type")]
        [Required]
        public string PromotionType { get; set; }

        [DisplayName("Order Number")]
        [Required]
        public string OrderNo { get; set; }

        [DisplayName("Order Date")]
        [Required]
        public DateTime? OrderDt { get; set; }

        [DisplayName("Promotion WEF Date")]
        [Required]
        public DateTime? PromotionWefDt { get; set; }

        public string EmpCd { get; set; }
        public string FromFieldDeptCd { get; set; }
        public string ToFieldDeptCd { get; set; }
        public string OfficeCd { get; set; }
        public int TrnGround { get; set; }
        public string DueToCd { get; set; }
        public int ScaleCd { get; set; }
        public int OldDesigCd { get; set; }

        [DisplayName("Remark")]
        [Required]
        public string Remark { get; set; }

        [DisplayName("Designation")]
        [Required]
        public int? ToDesigCd { get; set; }

        public string TrnStatus { get; set; }

        [DisplayName("Joining Date")]
        [Required]
        public DateTime? JoinedDt { get; set; }

        public string PermDdoId { get; set; }
        public string VerifyFlag { get; set; }
        public string RejectionRemark { get; set; }

        [DisplayName("Joining Order number")]
        [Required]
        public string JoinOrderNo { get; set; }

        [DisplayName("Joining Order Date")]
        [Required]
        public DateTime? JoinOrderDt { get; set; }
        
        [DisplayName("Joining Timing")]
        [Required]
        public string JoinedAnBn { get; set; }

        public string ToOfficeCd { get; set; }
        public string DDOId { get; set; }
        public string IpAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (OrderNo.Length < 3)
            {
                yield return new ValidationResult("Minimum length for Order Number required is 3",

                    new[] { "Order Number" });
            }
        }
    }


    public class GetPromotionDetails
    {
        public int TransDetailId { get; set; }
        public string PromotionType { get; set; }
        public string OrderNo { get; set; }
        public string Designation { get; set; }
        public string EmpCd { get; set; }
        public DateTime OrderDt { get; set; }
        public DateTime PromotionWefDt { get; set; }
        public string FromFieldDeptCd { get; set; }
        public string ToFieldDeptCd { get; set; }
        public string OfficeCd { get; set; }
        public int TrnGround { get; set; }
        public string DueToCd { get; set; }
        public int ScaleCd { get; set; }
        public int OldDesigCd { get; set; }
        public string Remark { get; set; }
        public int ToDesigCd { get; set; }
        public string TrnStatus { get; set; }
        public DateTime JoinedDt { get; set; }
        public string PermDdoId { get; set; }
        public string VerifyFlag { get; set; }
        public string RejectionRemark { get; set; }
        public string JoinOrderNo { get; set; }
        public DateTime JoinOrderDt { get; set; }
        public string JoinedAnBn { get; set; }
        public string ToOfficeCd { get; set; }
        public int TotalCount { get; set; }
        public string StatusDesc { get; set; }
    }

    public class ReversionDetails : IValidatableObject
    {
        public int TransferDetailId { get; set; }

        [DisplayName("Reversion Type")]
        [Required]
        public string ReversionType { get; set; }

        [DisplayName("Order Number")]
        [Required]
        public string OrderNo { get; set; }

        [DisplayName("Order Date")]
        [Required]
        public DateTime? OrderDt { get; set; }

        [DisplayName("Reversion WEF Date")]
        [Required]
        public DateTime? ReversionWefDt { get; set; }

        public string EmpCd { get; set; }
        public string FromFieldDeptCd { get; set; }
        public string ToFieldDeptCd { get; set; }
        public string OfficeCd { get; set; }
        public int TrnGround { get; set; }
        public string DueToCd { get; set; }
        public int ScaleCd { get; set; }
        public int OldDesigCd { get; set; }

        [DisplayName("Remark")]
        [Required]
        public string Remark { get; set; }

        [DisplayName("Designation")]
        [Required]
        public int? ToDesigCd { get; set; }

        public string TrnStatus { get; set; }

        [DisplayName("Joining Date")]
        [Required]
        public DateTime? JoinedDt { get; set; }

        public string PermDdoId { get; set; }
        public string VerifyFlag { get; set; }
        public string RejectionRemark { get; set; }

        [DisplayName("Joining Order Number")]
        [Required]
        public string JoinOrderNo { get; set; }

        [DisplayName("Joining Order Date")]
        [Required]
        public DateTime? JoinOrderDt { get; set; }

        [DisplayName("Joining Timing")]
        [Required]
        public string JoinedAnBn { get; set; }

        public string ToOfficeCd { get; set; }
        public string DDOId { get; set; }
        public string IpAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (OrderNo.Length < 3)
            {
                yield return new ValidationResult("Minimum length for Order Number required is 3",

                    new[] { "Order Number" });
            }
        }
    }

    public class GetReversionDetails
    {
        public int TransDetailId { get; set; }
        public string OrderNo { get; set; }
        public string Designation { get; set; }
        public string EmpCd { get; set; }
        public DateTime OrderDt { get; set; }
        public DateTime ReversionWefDt { get; set; }
        public string FromFieldDeptCd { get; set; }
        public string ToFieldDeptCd { get; set; }
        public string OfficeCd { get; set; }
        public int TrnGround { get; set; }
        public string DueToCd { get; set; }
        public int ScaleCd { get; set; }
        public int OldDesigCd { get; set; }
        public string Remark { get; set; }
        public int ToDesigCd { get; set; }
        public string TrnStatus { get; set; }
        public DateTime JoinedDt { get; set; }
        public string PermDdoId { get; set; }
        public string VerifyFlag { get; set; }
        public string RejectionRemark { get; set; }
        public string JoinOrderNo { get; set; }
        public DateTime JoinOrderDt { get; set; }
        public string JoinedAnBn { get; set; }
        public string ToOfficeCd { get; set; }
        public int TotalCount { get; set; }
        public string ReversionType { get; set; }
        public string StatusDesc { get; set; }
    }

    public class ForwardTransferDetails
    {
        public int MsEmpTransDet { get; set; }
        public string VerifyFlag { get; set; }
        public string PermDdoId { get; set; }
        public string RejectionRemark { get; set; }
        public string DDOId { get; set; }
    }
}
