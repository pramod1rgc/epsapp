﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.ReJoiningService
{
    public class GetServiceEndEmployees
    {
        public int MsEmpServiceEndId { get; set; }
        public int MsEmpId { get; set; }
        public string EmpCd { get; set; }
        public string EmpName { get; set; }
        public int EmpApptTypeId { get; set; }
        public string EmpType { get; set; }
        public string EmpGender { get; set; }
        public DateTime EmpDOB { get; set; }
        public DateTime EndServDt { get; set; }
        public string EmpPanNo { get; set; }
        public string DdoId { get; set; }
        public string DdoName { get; set; }
        public int EndReasonId { get; set; }
        public string EmpEndReason { get; set; }
        public DateTime JoiningDate { get; set; }
        public string JoiningTiming { get; set; }
    }


    public class GetReJoiningServiceEmployees
    {
        public int TotalCount { get; set; }
        public int MsEmpReinstateDetID { get; set; }
        public string IsPreviousGovService { get; set; }
        public string EmpCd { get; set; }
        public int MsEmpID { get; set; }
        public DateTime EmpRejoiningDt { get; set; }
        public string EmpJoiningTime { get; set; }
        public string RelApptType { get; set; }
        public int ReEmpTypeID { get; set; }
        public string RejoiningType { get; set; }
        public string JoinApptType { get; set; }
        public int EmpTypeID { get; set; }
        public string CurrentType { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDt { get; set; }
        public string Remark { get; set; }
        public double PensionAmount { get; set; }
        public string DATakenFrom { get; set; }
        public string NocOrderNo { get; set; }
        public DateTime? NocOrderDt { get; set; }
        public string VerifFlag { get; set; }
        public string EmpPanNo { get; set; }
        public DateTime EndServDt { get; set; }
        public string EmpName { get; set; }
        public string EmpGender { get; set; }
        public DateTime EmpDOB { get; set; }
        public string EmpEndReason { get; set; }
        public string Status { get; set; }
        public string ReasonForRej { get; set; }
    }


    public class UpsertReJoiningServiceDetails : IValidatableObject
    {
        public int MsEmpReinstateDetID { get; set; }

        [DisplayName("Is Previous Govt Service")]
        [Required]
        public string IsPreviousGovService { get; set; }

        [DisplayName("Employee Code")]
        [Required]
        public string EmpCd { get; set; }

        [DisplayName("Employee")]
        [Required]
        public int EmpId { get; set; }

        [DisplayName("Employee Rejoining Date")]
        [Required]
        public DateTime? EmpRejoiningDate { get; set; }

        [DisplayName("Employee Rejoining Time")]
        [Required]
        public string EmpJoiningTime { get; set; }

        public double PensionAmount { get; set; }

        public string ReJoiningEmployeeType { get; set; }

        [DisplayName("Rejoining Employee Type")]
        [Required]
        public int ReJoiningEmployeeTypeId { get; set; }
        public string ToDDOId { get; set; }
        public string DATakenFrom { get; set; }
        public string NocOrderNo { get; set; }
        public DateTime? NocOrderDt { get; set; }

        [DisplayName("Order No.")]
        [Required]
        public string OrderNo { get; set; }

        [DisplayName("Order Date")]
        [Required]
        public DateTime? OrderDt { get; set; }

        [DisplayName("Remark")]
        [Required]
        public string Remark { get; set; }
        public string IpAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (OrderNo.Length < 3)
            {
                yield return new ValidationResult("Minimum length for Order Number required is 3",

                    new[] { "Order Number" });
            }
            if(ReJoiningEmployeeTypeId == 33)
            {
                if(PensionAmount == 0)
                {
                    yield return new ValidationResult("Pension Amount cannot be 0.",

                    new[] { "Pension Amount" });
                }
                if (string.IsNullOrEmpty(DATakenFrom))
                {
                    yield return new ValidationResult("DA Taken From cannot be blank.",

                    new[] { "DATakenFrom" });
                }
                if (string.IsNullOrEmpty(NocOrderNo))
                {
                    yield return new ValidationResult("NOC Order No. cannot be blank.",

                    new[] { "NocOrderNo" });
                }
                if (NocOrderDt == null)
                {
                    yield return new ValidationResult("NOC Order Date cannot be blank.",

                    new[] { "NocOrderDt" });
                }
            }
        }
    }

    public class DeleteRejoiningService
    {
        public int MsEmpReinstateDetID { get; set; }
        public string EmpCd { get; set; }
        public string ToDDOId { get; set; }
        public string IpAddress { get; set; }
    }


    public class UpdateReJoiningServiceStatus
    {
        public int MsEmpReinstateDetID { get; set; }
        public string EmpCd { get; set; }
        public string ToDDOId { get; set; }
        public string IpAddress { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
    }
}
