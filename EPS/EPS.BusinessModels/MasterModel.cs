﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class MasterModel
    {
        public string MsCddirID { get; set; }
        public string CddirCodeType { get; set; }
        public string CddirCodeValue { get; set; }
        public string CddirCodeText { get; set; }   

    }

    public class EmployeeTypeMaster
    {
        public string MsEmpTypeID { get; set; }
        public int ParentTypeID { get; set; }
        public string CodeType { get; set; }
        public string CodeText { get; set; }
        public string CodeValue { get; set; }
    }
    public class JoiningTypeMaster
    {
        public string MsEmpTypeID { get; set; }
        public int ParentTypeID { get; set; }
        public string JoiningType { get; set; }
        public string JoiningValue { get; set; }
        public string JoiningText { get; set; }
    }
    public class ServiceTypeMaster
    {
        public string ServiceTypeId { get; set; }
        public string ServiceType { get; set; }
        public string ServiceValue { get; set; }
 
        public string ServiceText { get; set; }
    }
    public class GenderMaster
    {
        public int Genderid { get; set; }
        public string GenderName { get; set; }
      
    }
    public class StateMaster
    {
        public int StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string StateRBICode { get; set; }
        public string StateLGDName { get; set; }
        public string StateLGDCode { get; set; }
       
    }
    public class PfTypeMaster
    {
        public string MsPfAgencyID { get; set; }
        public string PfTypeDesc { get; set; }
        public string PfType { get; set; }

    }

    public class JoiningAccountOf
    {
       public int CddirCodeValue { get; set; }
       public string CddirCodeText { get; set; }
       public int MsCddirID { get; set; }
    }

    public class OfficeList
    {
      public string officeId { get; set; }
      public string officeName { get; set; }
    }
    public class OfficeCityClass
    {
        public string officeClassCityId { get; set; }
        public string officeClassCityName { get; set; }
    }
    public class PayCommission
    {
       public string payCommId { get; set; }
       public string payCommDesc { get; set; }
    }
}
