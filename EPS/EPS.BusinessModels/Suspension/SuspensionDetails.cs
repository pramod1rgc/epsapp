﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Suspension
{
    public class SuspensionDetails
    {
        public int Id { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string OrderNo { get; set; }

        public DateTime OrderDate { get; set; }

        public int SalaryPercent { get; set; }

        public string Remarks { get; set; }

        public string PermddoId { get; set; }

        public string EmpCode { get; set; }

        // public bool IsTillOrder { get; set; }

        public string Status { get; set; }

        public bool IsEditable { get; set; }

        public int SusPeriod { get; set; }

        public string RejectedReason { get; set; }
    }
    public class ExtensionDetails
    {
        public int Id { get; set; }

        public int SuspenId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string OrderNo { get; set; }

        public DateTime OrderDate { get; set; }

        public int SalaryPercent { get; set; }

        public string Remarks { get; set; }

        public string PermddoId { get; set; }

        public string EmpCode { get; set; }

        // public bool IsTillOrder { get; set; }

        public string Status { get; set; }

        public bool IsEditable { get; set; }

        public int SusPeriod { get; set; }

        public string RejectedReason { get; set; }
    }
    public class RevocationDetails
    {
        public int EmpRevocId { get; set; }
        public int EmpTransSusId { get; set; }
        public DateTime RevocationDate { get; set; }
        public string RevocOrderNo { get; set; }
        public DateTime RevocOrderDate { get; set; }
        public string Remarks { get; set; }
        public string FlagStatus { get; set; }
        public string IPAddress { get; set; }
        public bool IsEditable { get; set; }
        public string RejectedReason { get; set; }
    }
    public class JoiningDetails
    {
        public int EmpJoiningId { get; set; }
        public int EmpRevokId { get; set; }
        public DateTime JoiningDate { get; set; }
        public string FnAn { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string FlagStatus { get; set; }
        public string RejectedReason { get; set; }
        public string IPAddress { get; set; }
        public bool IsEditable { get; set; }
    }
    public class RegulariseDetailse
    {
        public int RegulariseId { get; set; }
        public int JoiningId { get; set; }
        public int SuspenCat { get; set; }
        public int SuspenTreated { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string Remarks { get; set; }
        public string FlagStatus { get; set; }
        public string RejectedReason { get; set; }
        public string IPAddress { get; set; }
        public bool IsEditable { get; set; }
    }
    public class EmpDesignationModel
    {
        public int MsDesigMastID { get; set; }

        public string DesigDesc { get; set; }
    }
}
