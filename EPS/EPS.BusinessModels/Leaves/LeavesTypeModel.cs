﻿
namespace EPS.BusinessModels.Leaves
{
    public class LeavesTypeModel
    {
        private string leaveTypeLeaveCd;
        private string leaveTypeLeaveDesc;
        private string leaveMaxAtATime;
        

        public string LeaveTypeLeaveCd { get => leaveTypeLeaveCd; set => leaveTypeLeaveCd = value; }
        public string LeaveTypeLeaveDesc { get => leaveTypeLeaveDesc; set => leaveTypeLeaveDesc = value; }
        public string LeaveMaxAtATime { get => leaveMaxAtATime; set => leaveMaxAtATime = value; }
    }
}
