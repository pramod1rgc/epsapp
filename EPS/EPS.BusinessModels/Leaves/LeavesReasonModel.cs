﻿
namespace EPS.BusinessModels.Leaves
{
    public class LeavesReasonModel
    {
        private string cddirCodeValue;
        private string cddirCodeText;

        public string CddirCodeValue { get => cddirCodeValue; set => cddirCodeValue = value; }
        public string CddirCodeText { get => cddirCodeText; set => cddirCodeText = value; }
    }
}
