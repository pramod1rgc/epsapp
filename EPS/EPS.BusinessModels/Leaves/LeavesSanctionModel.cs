﻿using System;
using System.Collections.Generic;

namespace EPS.BusinessModels.Leaves
{
  public class LeavesSanctionMainModel
    {
        private int leaveMainId;
        private string empCd;
        private string permDdoId;      
        private string orderNo;
        private string orderDT;
        private string leaveCategory;
        private string isRecovery;
        private string leaveReasonCD;
        private string leaveReasonDesc;
        private string remarks;
        private string returnReason;
        private string verifyFlg;        
        private  List<LeavesSanctionDetailsModel> leaveSanctionDetails;


        public string EmpCd { get => empCd; set => empCd = value; }      
        public string PermDDOId { get => permDdoId; set => permDdoId = value; }       
        public string OrderNo { get => orderNo; set => orderNo = value; }
        public string OrderDT { get => orderDT; set => orderDT = value; }
        public string LeaveCategory { get => leaveCategory; set => leaveCategory = value; }
        public string IsRecovery { get => isRecovery; set => isRecovery = value; }
        public string LeaveReasonCD { get => leaveReasonCD; set => leaveReasonCD = value; }
        public string Remarks { get => remarks; set => remarks = value; }
        public string ReturnReason { get => returnReason; set => returnReason = value; }
        public string VerifyFlg { get => verifyFlg; set => verifyFlg = value; }
        public string LeaveReasonDesc { get => leaveReasonDesc; set => leaveReasonDesc = value; }
        public int LeaveMainId { get => leaveMainId; set => leaveMainId = value; }
        public List<LeavesSanctionDetailsModel> LeaveSanctionDetails { get => leaveSanctionDetails; set => leaveSanctionDetails = value; }
        public string IpAddress { get; set; }
    }


public class LeavesSanctionDetailsModel
    {
    private string leaveDetailsId;
    private string leaveCd;
    private string leaveCdDesc;
    private string fromDT;
    private string toDT;
    private string noDays;
   private string payEntDT; 
    private string maxLeave;
    private Boolean ismaxLeave;
    private Boolean isHalfDay;
    private string isAfterNoon;

    public string LeaveDetailsId { get => leaveDetailsId; set => leaveDetailsId = value; }
    public string LeaveCd { get => leaveCd; set => leaveCd = value; }
    public string LeaveCdDesc { get => leaveCdDesc; set => leaveCdDesc = value; }
    public string FromDT { get => fromDT; set => fromDT = value; }
    public string ToDT { get => toDT; set => toDT = value; }
    public string PayEntDT { get => payEntDT; set => payEntDT = value; }
    public string NoDays { get => noDays; set => noDays = value; }
    public string MaxLeave { get => maxLeave; set => maxLeave = value; }
    public Boolean IsMaxLeave { get => ismaxLeave; set => ismaxLeave = value; }
    public Boolean IsHalfDay { get => isHalfDay; set => isHalfDay = value; }
    public string IsAfterNoon { get => isAfterNoon; set => isAfterNoon = value; }
    }
    public class DesignationModel
    {
        private int msDesigMastID;
        private string desigDesc;


        public int MsDesigMastID { get => msDesigMastID; set => msDesigMastID = value; }
        public string DesigDesc { get => desigDesc; set => desigDesc = value; }

    }

    public class EmpModel
    {
        private string empCd;
        private string empName;

        public string EmpCd { get => empCd; set => empCd = value; }
        public string EmpName { get => empName; set => empName = value; }
    }

    public class OrderModel
    {
        private string orderId;
        private string orderNo;

        public string OrderId { get => orderId; set => orderId = value; }
        public string OrderNo { get => orderNo; set => orderNo = value; }
    }
    public class LeavesSanctionHistoryModel
    {

        private string histryOrderNo;
        private string histryOrderDt;
        private string histryLeavetype;
        private string histryFromDt;
        private string histryToDt;
        private string histryNoofDays;
        private string histryLeaveStatus;


        public string HistryOrderNo { get => histryOrderNo; set => histryOrderNo = value; }
        public string HistryOrderDt { get => histryOrderDt; set => histryOrderDt = value; }
        public string HistryLeavetype { get => histryLeavetype; set => histryLeavetype = value; }
        public string HistryFromDt { get => histryFromDt; set => histryFromDt = value; }
        public string HistryToDt { get => histryToDt; set => histryToDt = value; }
        public string HistryNoofDays { get => histryNoofDays; set => histryNoofDays = value; }
        public string HistryLeaveStatus { get => histryLeaveStatus; set => histryLeaveStatus = value; }
          }

}
