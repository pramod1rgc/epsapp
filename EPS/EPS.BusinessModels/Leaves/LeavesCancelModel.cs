﻿
namespace EPS.BusinessModels.Leaves
{
    public class LeavesCancelModel
    {
        private int preLeaveMainId;
        private string cancelOrderNo;
        private string cancelReason;
        private string cancelOrderDt;
        private string cancelVerifyFlg;
        private string returnReason;
        private string cancelRemark;

        public int PreLeaveMainId { get => preLeaveMainId; set => preLeaveMainId = value; }
        public string CancelOrderNo { get => cancelOrderNo; set => cancelOrderNo = value; }
        public string CancelOrderDt { get => cancelOrderDt; set => cancelOrderDt = value; }
        public string CancelReason { get => cancelReason; set => cancelReason = value; }
        public string CancelVerifyFlg { get => cancelVerifyFlg; set => cancelVerifyFlg = value; }
        public string ReturnReason { get => returnReason; set => returnReason = value; }
        public string CancelRemark { get => cancelRemark; set => cancelRemark = value; }
        public string IpAddress { get; set; }

    }
    public class LeavesCancelSanctionModel : LeavesCancelModel
    {
        private LeavesSanctionMainModel leavesSanctionMainModel;
        public LeavesSanctionMainModel LeavesSanctionMainModel { get => leavesSanctionMainModel; set => leavesSanctionMainModel = value; }
    }
    public class LeavesCancelHistoryModel
    {

        private string histryOrderNo;
        private string histryOrderDt;
        private string histryLeavetype;
        private string histryFromDt;
        private string histryToDt;
        private string histryNoOfDays;
        private string histryLeaveStatus;
        private string histryCancelReason;


        public string HistryOrderNo { get => histryOrderNo; set => histryOrderNo = value; }
        public string HistryOrderDt { get => histryOrderDt; set => histryOrderDt = value; }
        public string HistryLeavetype { get => histryLeavetype; set => histryLeavetype = value; }
        public string HistryFromDt { get => histryFromDt; set => histryFromDt = value; }
        public string HistryToDt { get => histryToDt; set => histryToDt = value; }
        public string HistryNoOfDays { get => histryNoOfDays; set => histryNoOfDays = value; }
        public string HistryCancelReason { get => histryCancelReason; set => histryCancelReason = value; }
        public string HistryLeaveStatus { get => histryLeaveStatus; set => histryLeaveStatus = value; }
    }
}
