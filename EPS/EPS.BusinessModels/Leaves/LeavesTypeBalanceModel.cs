﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Leaves
{
    public class LeavesTypeBalanceModel
    {
        private string leaveCd;
        private int leaveBalance;

        public string LeaveCd { get => leaveCd; set => leaveCd = value; }
        public int LeaveBalance { get => leaveBalance; set => leaveBalance = value; }
    }
}
