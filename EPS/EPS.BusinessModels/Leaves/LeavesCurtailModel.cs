﻿
namespace EPS.BusinessModels.Leaves
{
    
    public class LeavesCurtailModel
    {
        private int curtId;
        private string curtOrderNo;
        private string curtOrderDT;
        private string isCurtleave;       
        private string curtReason;
        private string curtRemarks;
        private string returnReason;     
        private int curtLeaveDetailsId;
        private string curtLeaveCd;
        private string curtFromDT;
        private string curtToDT;
        private string curtNoDays;

        private string curtEmpCd;
        private string curtPermDdoId;
        private string preOrderNo;
        private int preLeaveMainId;
        private string verifyflg;


        public int CurtId { get => curtId; set => curtId = value; }
        public string CurtOrderNo { get => curtOrderNo; set => curtOrderNo = value; }
        public string CurtOrderDT { get => curtOrderDT; set => curtOrderDT = value; }
        public string IsCurtleave { get => isCurtleave; set => isCurtleave = value; }
        public string CurtReason { get => curtReason; set => curtReason = value; }
        public string CurtRemarks { get => curtRemarks; set => curtRemarks = value; }
        public string ReturnReason { get => returnReason; set => returnReason = value; }
        public int CurtLeaveDetailsId { get => curtLeaveDetailsId; set => curtLeaveDetailsId = value; }
        public string CurtLeaveCd { get => curtLeaveCd; set => curtLeaveCd = value; }
        public string CurtFromDT { get => curtFromDT; set => curtFromDT = value; }
        public string CurtToDT { get => curtToDT; set => curtToDT = value; }
        public string CurtNoDays { get => curtNoDays; set => curtNoDays = value; }

        public string CurtEmpCd { get => curtEmpCd; set => curtEmpCd = value; }
        public string CurtPermDdoId { get => curtPermDdoId; set => curtPermDdoId = value; }
        public string PreOrderNo { get => preOrderNo; set => preOrderNo = value; }
        public int PreLeaveMainId { get => preLeaveMainId; set => preLeaveMainId = value; }
        public string Verifyflg { get => verifyflg; set => verifyflg = value; }
        public string IpAddress { get; set; }

    }

    public class LeavesCurtailSanctionModel: LeavesCurtailModel
    {
        private LeavesSanctionMainModel leavesSanctionMainModel;
        public LeavesSanctionMainModel LeavesSanctionMainModel { get => leavesSanctionMainModel; set => leavesSanctionMainModel = value; }
    }
    public class LeavesCurtailHistoryModel
    {

        private string histryOrderNo;
        private string histryOrderDt;
        private string histryLeavetype;
        private string histryFromDt;
        private string histryToDt;
        private string histryCurtOrExten;     
        private string histryLeaveStatus;


        public string HistryOrderNo { get => histryOrderNo; set => histryOrderNo = value; }
        public string HistryOrderDt { get => histryOrderDt; set => histryOrderDt = value; }
        public string HistryLeavetype { get => histryLeavetype; set => histryLeavetype = value; }
        public string HistryFromDt { get => histryFromDt; set => histryFromDt = value; }
        public string HistryToDt { get => histryToDt; set => histryToDt = value; }
        public string HistryCurtOrExten { get => histryCurtOrExten; set => histryCurtOrExten = value; }
        public string HistryLeaveStatus { get => histryLeaveStatus; set => histryLeaveStatus = value; }
    }


}
