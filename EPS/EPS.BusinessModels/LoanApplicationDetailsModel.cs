﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
    public class LoanApplicationDetailsModel
    {
        public int      MsEmpID { get; set; }
        public string   EmpCd { get; set; }
        public string   EmpApptType { get; set; }
        public string   EmpPfType { get; set; }
        public int      pay_basic { get; set; }
        public string   EmpFullName { get; set; }            
        public DateTime EmpSupanDt { get; set; }
        public string   DesigDesc { get; set; }


    }
}
