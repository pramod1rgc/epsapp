﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class LoginModel
    {
        /// <summary>
        /// For login
        /// </summary>
        //private string username;
        //private string password;
        public string Username { get ; set ; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string JWT_Secret { get; set; }
        public string Status { get; set; }
        public string UserType { get; set; }
    }
    public class userDetails
    {
        public string Username { get; set; }
        public int? ControllerID { get; set; }
        public int? PAOID { get; set; }
        public int? DDOID { get; set; }
        public string UserRole { get; set; }
        public int? MsRoleID { get; set; }
        public int? MsUserID { get; set; }
        public string EmpPermDDOId { get; set; }
    }
}
