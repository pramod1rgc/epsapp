﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.EndofService
{
    public class EmodofServiceMsIdCD
    {
        public int? MsEmpID { get; set; }
        [DisplayName("Employee CD")]
        [Required]
        public string EmpCd { get; set; }
       
    }
    public class EndofServiceReasonModel
    {
        public int? MsCddirID { get; set; }
        
        public string CddirCodeText { get; set; }

    }
    public  class EndofServiceModel: EmodofServiceMsIdCD
    {
        [DisplayName("Employee Name")]
        [Required]
        public string EmpName { get; set; }

    }
    public class DeleteEndofServiceModel: EmodofServiceMsIdCD
    {
              
        public string IpAddress { get; set; }
    }


    public class UpdateEndofServiceStatusModel: EmodofServiceMsIdCD
    {
           
        public string IpAddress { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
    }
    public class EndofServiceDtModel
    {
        public int? MsEmpServiceEndID { get; set; }
        public int? MsEmpID { get; set; }
        public string EmpCd { get; set; }
        public string EndOrderNo { get; set; }
        public DateTime? EndOrderDt { get; set; }
        public DateTime? EndServDt { get; set; }
        public int? EndReasonId { get; set; }
        public string EndReason { get; set; }      
        public DateTime? DtOfDeath { get; set; }
        public string EndRemark { get; set; }       
        public string StatusID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string IPAddress { get; set; }
    }
    public class EmpCurrentDetailModel : EndofServiceModel
    {
        public string EmpApptType { get; set; }
        public DateTime? SuperanuationDate { get; set; }
        public string CurrentPostingMode { get; set; }
        public DateTime? DateofEntry { get; set; }
        public string BillMonthName { get; set; }
        public int? BillYear { get; set; }
    }
    public class EndofServiceInsertModel : EmodofServiceMsIdCD, IValidatableObject
    {
        public int? MsEmpServiceEndID { get; set; }

        [DisplayName("Employee Appointment Type")]
        [Required]
        public string EmpApptType { get; set; }
        [DisplayName("Employee Superanuation Date")]
        [Required]
        public DateTime? SuperanuationDate { get; set; }
        [DisplayName("Employee Current Posting Mode")]
        [Required]
        public string CurrentPostingMode { get; set; }
        [DisplayName("Employee Date Of Entry")]
        [Required]
        public DateTime? DateofEntry { get; set; }
        [DisplayName("Employee Last Bill Month")]
        [Required]
        public string BillMonthName { get; set; }
        [DisplayName("Employee Last Bill Year")]
        [Required]
        public int? BillYear { get; set; }
        [DisplayName("Employee Order Number")]
        [Required]
        public string EndOrderNo { get; set; }
        [DisplayName("Employee Order Date")]
        [Required]
        public DateTime? EndOrderDt { get; set; }
        [DisplayName("Employee Date of End Service")]
        [Required]
        public DateTime? EndServDt { get; set; }
        [DisplayName("Employee Reason Id")]
        [Required]
        public int? EndReasonId { get; set; }
        [DisplayName("Employee Reason ")]
        //[Required]
        public string EndReason { get; set; }
        public string StatusId { get; set; }
        public DateTime? DtOfDeath { get; set; }
        public string EndRemark { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string IPAddress { get; set; }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (EndOrderNo.Length < 3)
            {
                yield return new ValidationResult("Minimum length for Order Number required is 3",

                    new[] { "Order Number" });
            }
           
        }
    }
}