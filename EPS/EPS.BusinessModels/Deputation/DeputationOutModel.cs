﻿using System;

namespace EPS.BusinessModels.Deputation
{
    public class DeputationOutModel
    {
        public int? empDeputDetailsId { get; set; }
        public string employeeCode { get; set; }
        public string deputedTypeId { get; set; }
        public string serviceTypeId { get; set; }
        public string depuOrderNo { get; set; }
        public DateTime? depuOrderDate { get; set; }
        public string depuedToOffice { get; set; }
        public DateTime? dateOfDeputataion { get; set; }
        public int depuOnDesignation { get; set; }
        public DateTime? relievingDate { get; set; }
        public int? depuTenureYear { get; set; }
        public int? depuTenureMonth { get; set; }
        public int? depuTenureDays { get; set; }
        public string depuFlag { get; set; }
        public string depuTypeName { get; set; }
        public string ipAddress { get; set; }
        public string createdBy { get; set; }
        public string veriFlag { get; set; }
        public string flagUpdate { get; set; }

        public string rejectionRemark { get; set; }






    }
}
