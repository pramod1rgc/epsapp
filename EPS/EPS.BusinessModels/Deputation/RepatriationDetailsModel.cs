﻿using System;

namespace EPS.BusinessModels.Deputation
{
    public class RepatriationDetailsModel
    {
        public int? empDeputDetailsId { get; set; }
        public string employeeCode { get; set; }
        public string repatDeputedTypeId { get; set; }
        public string repatDepuTypeName { get; set; }
        public string repatServiceTypeId { get; set; }
        public string repatOrderNo { get; set; }
        public DateTime? repatOrderDate { get; set; }
        public DateTime? repatEffecDate { get; set; }
        public DateTime? repatRelievingDate { get; set; }
        public string repatRemarks { get; set; }
        public string veriFlag { get; set; }
        public string ipAddress { get; set; }
        public string createdBy { get; set; }
        public string flagUpdate { get; set; }
        public string rejectionRemark { get; set; }

    }
    public class DeputationInExistEmpModel
    {
        public string employeeCode { get; set; }
        public DateTime? DeputationEffectiveDate { get; set; }
    }
}
