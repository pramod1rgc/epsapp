﻿using System;
using System.Collections.Generic;


namespace EPS.BusinessModels.Deputation
{
    public class DeputationInModel
    {
        public int msPayItemsId { get; set; }
        public string payItemsName { get; set; }
        public int desigId { get; set; }
        public string desigDesc { get; set; }
        public int pfId { get; set; }
        public string pfAgency_Desc { get; set; }
        public string deputation_Type { get; set; }
        public string service_Type { get; set; }
        public string deputation_Order { get; set; }
        public DateTime ? deputation_Order_Date { get; set; }
        public string deputation_From_Office { get; set; }
        public int deputation_State { get; set; }
        public DateTime ? deputation_Effective_Date { get; set; }
        public DateTime ? deputation_Repatration_Date { get; set; }
        public int desig_Before_Deputation { get; set; }
        public int desig_After_Deputation { get; set; }
        public string empCd { get; set; }
        public string deputationFlag { get; set; }
        public string desig_Before_Remark { get; set; }
        public string deputation_Type_Name { get; set; }
        public int ? empDeputDetailsId { get; set;}
        public string ipAddress { get; set; }
        public string createdBy { get; set; }
        public string veriFlag { get; set; }
        public string rejectionRemark { get; set; }
        public string flagUpdate { get; set; }
        public List<DeductionSchedule>  Deputation_Deduction { get; set; }
      
    }
    public class DeductionSchedule
    {
        public string payItemsDeductionName { get; set; }
        public string payItemsDeductionAccCdScheme { get; set; }
        public string deduction_Schedule { get; set; }
        public int communication_Address { get; set; }
        public int payItemsCd { get; set; }
        public int msEmpDuesId { get; set; }
    }
    public class Designation {
        public int desigId { get; set; }
        public string desigDesc { get; set; }
    }
    public class SchemeCode
    {
        public string payItemsDeductionAccCdScheme { get; set; }
        public int msEmpDuesID { get; set; }
    }
}
