﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class payscaleModel
    {
        private int msPayScaleID;
        private int msCddirID;
        private string cddirCodeText;
        private int paysfm_id;
        private string paysfm_Text;
        private int paysGroup_id;
        private string paysGroup_Text;
        private string payScaleCode;
        private string payScale;
        private decimal payScaleGradePay;
        private int payGISGroupID;
        private string payGISGroupName;
        private string payScaleWithEffectFrom;
        private string payScaledescription;
    //private string PayScalePscLoLimit;
    //private string PayScalePscInc1;
    //private string PayScalePscStage1;
    //private string PayScalePscInc2;
    //private string PayScalePscStage2;
    //private string PayScalePscInc3;
    //private string PayScalePscStage3;
    //private string PayScalePscInc4;
    //private string PayScalePscUpLimit;
        private string payScaleLevel;



        public decimal PayScalePscLoLimit  { get; set; }
        public decimal PayScalePscInc1     { get; set; }
        public decimal PayScalePscStage1   { get; set; }
        public decimal PayScalePscInc2     { get; set; }
        public decimal PayScalePscStage2   { get; set; }
        public decimal PayScalePscInc3     { get; set; }
        public decimal PayScalePscStage3   { get; set; }
        public decimal PayScalePscInc4     { get; set; }
        public decimal PayScalePscUpLimit  { get; set; }


        public int MsCddirID { get => msCddirID; set => msCddirID = value; }
        public string CddirCodeText { get => cddirCodeText; set => cddirCodeText = value; }
        public int Paysfm_id { get => paysfm_id; set => paysfm_id = value; }
        public string Paysfm_Text { get => paysfm_Text; set => paysfm_Text = value; }
        public int PaysGroup_id { get => paysGroup_id; set => paysGroup_id = value; }
        public string PaysGroup_Text { get => paysGroup_Text; set => paysGroup_Text = value; }
        public string PayScaleCode { get => payScaleCode; set => payScaleCode = value; }
        public string PayScale { get => payScale; set => payScale = value; }
        public decimal PayScaleGradePay { get => payScaleGradePay; set => payScaleGradePay = value; }
        public int PayGISGroupID { get => payGISGroupID; set => payGISGroupID = value; }
        public string PayGISGroupName { get => payGISGroupName; set => payGISGroupName = value; }
        public string PayScaleWithEffectFrom { get => payScaleWithEffectFrom; set => payScaleWithEffectFrom = value; }
        public string PayScaledescription { get => payScaledescription; set => payScaledescription = value; }
        //public string PayScalePscInc11 { get => PayScalePscInc12; set => PayScalePscInc12 = value; }
        //public string PayScalePscLoLimit1 { get => PayScalePscLoLimit; set => PayScalePscLoLimit = value; }
        //public string PayScalePscInc12 { get => PayScalePscInc1; set => PayScalePscInc1 = value; }
        //public string PayScalePscStage11 { get => PayScalePscStage1; set => PayScalePscStage1 = value; }
        //public string PayScalePscInc21 { get => PayScalePscInc2; set => PayScalePscInc2 = value; }
        //public string PayScalePscStage21 { get => PayScalePscStage2; set => PayScalePscStage2 = value; }
        //public string PayScalePscInc31 { get => PayScalePscInc3; set => PayScalePscInc3 = value; }
        //public string PayScalePscStage31 { get => PayScalePscStage3; set => PayScalePscStage3 = value; }
        //public string PayScalePscInc41 { get => PayScalePscInc4; set => PayScalePscInc4 = value; }
        //public string PayScalePscUpLimit1 { get => PayScalePscUpLimit; set => PayScalePscUpLimit = value; }
        public string PayScaleLevel { get => payScaleLevel; set => payScaleLevel = value; }
        public int MsPayScaleID { get => msPayScaleID; set => msPayScaleID = value; }
    }
}
