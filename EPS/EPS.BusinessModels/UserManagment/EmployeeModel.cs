﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
  public class EmployeeModel
    {
        private string empCd;
        private string empName;
        private string empGender;
        private string empDOB;
        private string activeStatus;
        private string commonDesigDesc;
        private string email;
        private string msUserID;
        private string pAOCode;
        private string dDOCode;
        private string dDOName;
        private string pAOName;
        public string EmpCd { get => empCd; set => empCd = value; }
        public string EmpName { get => empName; set => empName = value; }
        public string EmpGender { get => empGender; set => empGender = value; }
        public string EmpDOB { get => empDOB; set => empDOB = value; }
        public string ActiveStatus { get => activeStatus; set => activeStatus = value; }
        public string CommonDesigDesc { get => commonDesigDesc; set => commonDesigDesc = value; }
        public string Email { get => email; set => email = value; }
        public string MsUserID { get => msUserID; set => msUserID = value; }
        public string PAOCode { get => pAOCode; set => pAOCode = value; }
        public string DDOCode { get => dDOCode; set => dDOCode = value; }
        public string DDOName { get => dDOName; set => dDOName = value; }
        public string PAOName { get => pAOName; set => pAOName = value; }
    }
}
