﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class PAOroleModel
    {
        private string msPAOID;
        private string pAOName;
        public string MsPAOID { get => msPAOID; set => msPAOID = value; }
        public string PAOName { get => pAOName; set => pAOName = value; }
    }
}
