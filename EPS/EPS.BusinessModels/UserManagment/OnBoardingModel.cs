﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.UserManagment
{
   public class OnBoardingModel
    {

        //public int BoardingId { get => boardingId; set => boardingId = value; }

        public string RequestLetterNo { get => requestLetterNo; set => requestLetterNo = value; }
        public DateTime RequestLetterDate { get => requestLetterDate; set => requestLetterDate = value; }
        public string ControllerID { get => controllerID; set => controllerID = value; }
        public string PAOID { get => pAOID; set => pAOID = value; }
        public string DDOID { get => dDOID; set => dDOID = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Designation { get => designation; set => designation = value; }
        public string EmailID { get => emailID; set => emailID = value; }
        public string MobileNo { get => mobileNo; set => mobileNo = value; }
        public string CFirstName { get => cFirstName; set => cFirstName = value; }
        public string CLastName { get => cLastName; set => cLastName = value; }
        public string CDesignation { get => cDesignation; set => cDesignation = value; }
        public string CEmailID { get => cEmailID; set => cEmailID = value; }
        public string CMobileNo { get => cMobileNo; set => cMobileNo = value; }
        public string PFirstName { get => pFirstName; set => pFirstName = value; }
        public string PLastName { get => pLastName; set => pLastName = value; }
        public string PDesignation { get => pDesignation; set => pDesignation = value; }
        public string PEmailID { get => pEmailID; set => pEmailID = value; }
        public string PMobileNo { get => pMobileNo; set => pMobileNo = value; }
        public string DFirstName { get => dFirstName; set => dFirstName = value; }
        public string DLastName { get => dLastName; set => dLastName = value; }
        public string DDesignation { get => dDesignation; set => dDesignation = value; }
        public string DEmailID { get => dEmailID; set => dEmailID = value; }
        public string DMobileNo { get => dMobileNo; set => dMobileNo = value; }
       

        //private int boardingId;
        private string requestLetterNo;
        private DateTime requestLetterDate;
        private string controllerID;
        private string pAOID;
        private string dDOID;
        private string firstName;
        private string lastName;
        private string designation;
        private string emailID;
        private string mobileNo;
        private string cFirstName;
        private string cLastName;
        private string cDesignation;
        private string cEmailID;
        private string cMobileNo;
        private string pFirstName;
        private string pLastName;
        private string pDesignation;
        private string pEmailID;
        private string pMobileNo;
        private string dFirstName;
        private string dLastName;
        private string dDesignation;
        private string dEmailID;
        private string dMobileNo;
    }
}
