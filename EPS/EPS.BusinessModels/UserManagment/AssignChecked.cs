﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.UserManagment
{
   public class AssignChecked
    {
        private int msRoleUserMapID;
        private int msUserID;
        private int msRoleID;

        public int MsRoleUserMapID { get => msRoleUserMapID; set => msRoleUserMapID = value; }
        public int MsUserID { get => msUserID; set => msUserID = value; }
        public int MsRoleID { get => msRoleID; set => msRoleID = value; }
    }
}
