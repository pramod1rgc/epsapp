﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
   public class controllerroleModel
    {
        //private string controllerCode;
        private string controllerName;
        private int msControllerID;

        //public string ControllerCode { get => controllerCode; set => controllerCode = value; }
        public string ControllerName { get => controllerName; set => controllerName = value; }
        public int MsControllerID { get => msControllerID; set => msControllerID = value; }
    }

    public class UserType
    {
        private int msRoleID;
        private string roleName;
        public int MsRoleID { get => msRoleID; set => msRoleID = value; }
        public string RoleName { get => roleName; set => roleName = value; }
    }
    public class DropDownListModel
    {
        private string values;
        private string text;

        public string Values { get => values; set => values = value; }
        public string Text { get => text; set => text = value; }

    }
}
