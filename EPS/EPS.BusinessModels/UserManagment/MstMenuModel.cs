﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels
{
    public class MstMenuModel
    {
        #region Menu Class  Property
        public int MsMenuID { get; set; }
        public string MainMenuName { get; set; }

        public string MenuURL { get; set; }
        public int ParentMenu { get; set; }
        public int ChildMenu { get; set; }
        public string S_no { get; set; }
        public string Status { get; set; }
        public string IPAddress { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string IsActive { get; set; }
        public string ParentMenuName { get; set; }
        public string CreatedStatus { get; set; }
        #endregion End Property

        public List<RoleIDS> RoleSIDS { get; set; }

    }

    public class RoleIDS
    {

        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleStatus { get; set; }
        public string RoleDescription { get; set; }
        public string CreatedDate { get; set; }
        public string IsActive { get; set; }
        public string MsUserID { get; set; }
        public string DeActDesc { get; set; }
        public string DeactivationDate { get; set; }
        public string Text
        {
            get; set;
        }

    }
}
