﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EPS.BusinessModels
{
    public class MstRole
    { 
      
        public List<MstRole> unselectedRole { get; set; }
        List<MstRole> treeViewList = new List<MstRole>();
        public List<MstRole> Children { get; set; }
        public int MsMenuID { get; set; }
        public int MsControllerID { get; set; }
        public int? ParentMenu { get; set; }
        public string MainMenuName { get; set; }
        public string Status { get; set; }
        public string MenupermissionID { get; set; }
        public int RoleID { get; set; }
        public int RoleParentID { get; set; }
        public string RoleName { get; set; }
        public string IsActive { get; set; }
        public string InsertedBy { get; set; }
        public string InsertedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string EmpCd { get; set; }
        public string LogInUserName { get; set; }
        public string RoleDescription { get; set; }
        public string LoggRollID { get; set; }

        public string MsRoleID { get; set; }
        //User Details Property
        public int UserID { get; set; }
        public string FirstName { get; set; }
        // public string Emp_code { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string PAN { get; set; }
        public string Designation { get; set; }
        public string IPAddress { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string RoleIDs { get; set; }
        //End User Details 
        //For Message
        public string Message { get; set; }
        //End 

        public string SetFlag { get; set; }

        public List<MenuRoleIDS> RoleSIDS { get; set; }

    }

   
    public class MenuRoleIDS
    {

        public int RoleID { get; set; }
        public string RoleName { get; set; }

    }


}
