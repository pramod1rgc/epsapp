﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.Increment
{
    public class RegIncrement_Model
    {
        public int ID { get; set; }
        public int IncID { get; set; }
        public string EmpName { get; set; }
        public decimal OldBasic { get; set; }
        public DateTime OldWefDate { get; set; }
        public DateTime NextIncDate { get; set; }
        public string PayLevel { get; set; }
        public int NoofIncrement { get; set; }
        public string OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderType { get; set; }
        public DateTime? WefDate { get; set; }
        public string Remark { get; set; }
        public string DesigCode { get; set; }
        public string EmpCode { get; set; }
        public string SalaryMonth { get; set; }
        public string loginUser { get; set; }
        public string ChangeSubType { get; set; }
        public int OrderTypeID { get; set; }
        public int NoofEmployee { get; set; }
        public string Status { get; set; }
        public string Desigination { get; set; }
        public string ClientIP { get; set; }
        public decimal NewBasic { get; set; }
        public int EmpID { get; set; }
        public List<string> IncrementData { get; set; }

        public int MsDesigMastID { get; set; }
        public string DesigDesc { get; set; }

        public List<RegIncrement_Model> regIncrements { get; set; }
        public List<string> EmpIDs { get; set; }

    }
}
