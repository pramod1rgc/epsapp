﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.IncomeTax
{
    public class OtherDuesModel
    {
        public class MajorSecCode
        {
            public int MajorSectionId { get; set; }
            public string MajorSectionCode { get; set; }
            public string MajorSectionDesc { get; set; }
        }

        public class OtherDues
        {
            public int DuesId { get; set; }
            public string DuesDesc { get; set; }
        }

        public class OtherDuesDetails
        {
            public int MsEdrId { get; set; }
            
            [Required]
            [DisplayName("Major Section Code")]
            public string MajorSectionCode { get; set; }

            [Required]
            [DisplayName("Section Code")]
            public string SectionCode { get; set; }

            [Required]
            [DisplayName("Sub Section Code")]
            public string SubSectionCode { get; set; }

            public string SectionDesc { get; set; }

            [Required]
            [Range(1, Int32.MaxValue - 1,ErrorMessage = "The Dues field is required.")]
            [DisplayName("Dues")]
            public int DuesId { get; set; }

            public string DuesDesc { get; set; } 
            
            public string UserIp { get; set; }

            public string UserName { get; set; }

            public int TotalCount { get; set; }
        }
    }
}
