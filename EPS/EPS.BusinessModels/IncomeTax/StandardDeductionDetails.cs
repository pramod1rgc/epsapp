﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.IncomeTax
{
    public class GetStandardDeductionDetails
    {
        public int MsSdentRuleID { get; set; }
        public int MsSdrID { get; set; }
        public string SerSecCode { get; set; }
        public string SerFinFyr { get; set; }
        public string SerFinTyr { get; set; }
        public string SerRateFor { get; set; }
        public double SerLLimit { get; set; }
        public double SerULimit { get; set; }
        public double SerPercValue { get; set; }
        public int SerValue { get; set; }
        public int TotalCount { get; set; }
    }

    public class RateDetailsModel
    {
        public int Id { get; set; }
        public int MsSdentRuleID { get; set; }
        public double SerLLimit { get; set; }
        public double SerULimit { get; set; }
        public double SerPercValue { get; set; }
        public int SerValue { get; set; }
    }

    public class UpsertRateDetailsModel : IValidatableObject
    {
        public int Id { get; set; }
        public int MsSdentRuleID { get; set; }

        [DisplayName("Lower Limit")]
        [Required]
        public double? SerLLimit { get; set; }

        [DisplayName("Upper Limit")]
        [Required]
        public double? SerULimit { get; set; }

        [DisplayName("Percentage Value")]
        [Required]
        public double? SerPercValue { get; set; }

        [DisplayName("Rate Detail Value")]
        [Required]
        public int? SerValue { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (SerLLimit > SerULimit)
            {
                yield return new ValidationResult("Lower Limit should be less than Upper Limit",

                    new[] { "Lower Limit" });
            }
        }
    }

    public class StandardDeductionDetails
    {
        public StandardDeductionDetails()
        {
            this.RateDetails = new List<RateDetailsModel>();
        }
        public int MsSdrID { get; set; }
        public string SerSecCode { get; set; }
        public string SerFinFyr { get; set; }
        public string SerFinTyr { get; set; }
        public string SerRateFor { get; set; }
        public int TotalCount { get; set; }
        public List<RateDetailsModel> RateDetails { get; set; }
    }

    public class UpsertStandardDeductionDetails
    {
        public UpsertStandardDeductionDetails()
        {
            this.RateDetails = new List<UpsertRateDetailsModel>();
        }

        public int? MsSdrID { get; set; }

        [DisplayName("Section Code")]
        [Required]
        public string SerSecCode { get; set; }

        [DisplayName("From Financial Year")]
        [Required]
        public string SerFinFyr { get; set; }

        [DisplayName("To Financial Year")]
        [Required]
        public string SerFinTyr { get; set; }

        [DisplayName("Rate For")]
        [Required]
        public string SerRateFor { get; set; }
        public string IpAddress { get; set; }
        public string PermDdoId { get; set; }
        public string DDOId { get; set; }

        public List<UpsertRateDetailsModel> RateDetails { get; set; }
    }

    public class DeleteStandardDeductionDetails
    {
        public int MsSdrID { get; set; }
        public string SerSecCode { get; set; }
        public string SerFinFyr { get; set; }
        public string SerFinTyr { get; set; }
        public string SerRateFor { get; set; }
    }

    public class GetStandardDeductionEntertainMasterDetail
    {
        public int MsSdrId { get; set; }
        public string SdeSecCode { get; set; }
        public string SdeDesc { get; set; }
        public string SdeSlab { get; set; }
    }
}
