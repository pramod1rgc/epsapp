﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.IncomeTax
{
    public class RateType
    {
        public int ID { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public string CodeText { get; set; }
    }
}
