﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.IncomeTax
{
    public class ITRateDetailStatus
    {
        public string IsrFinYr { get; set; }
        public string FullDescription { get; set; }
        public double IsrLlimit { get; set; }
        public double IsrUlimit { get; set; }
        public double IsrAddedAmt { get; set; }
        public int IsrPercVal { get; set; }
        public string IsrRateType { get; set; }
        public string IsrRateFor { get; set; }
        public int TotalCount { get; set; }
        public string Message { get; set; }
    }
}
