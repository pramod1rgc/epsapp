﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EPS.BusinessModels.IncomeTax
{
    public class MajorSectionCode
    {
        public int MajorSecId;
        public string MajorSecCode;
        public string MajorSecDesc;
    }

    public class SectionCode
    {
        public int SecId;
        public string SecCode;
        public string SecDesc;
    }

    public class SubSectionCode
    {
        public int SubSecId;
        public string SubSecCode;
        public string SubSecDesc;
    }

    public class SectionCodeDescription
    {
        public int SecId;
        public string SecCode;
        public string SubSecCode;
        public string SecDesc;
    }

    public class MajorSectionCodeDetails
    {
        public MajorSectionCode MajorSectionCode { get; set; }

        public List<SectionCodeDescription> SectionCodeDescriptionList { get; set; }
    }

    public class SlabLimit
    {
        public int SlabLimitId;
        public string SlabLimitCode;
        public string SlabLimitDesc;
    }

    public class PayCommission
    {
        public int PayCommId;
        public string PayCommDesc;
    }

    public class ExemptionDeductionRateDetailsModel : IValidatableObject
    {
        public int ExmDedRebateId { get; set; }

        public int Id2 { get; set; }

        [Required]
        [DisplayName("Upper Limit")]
        public string UpperLimit { get; set; }

        [Required]
        [DisplayName("Lower Limit")]
        public string LowerLimit { get; set; }

        [Required]
        [DisplayName("Percentage Value")]
        public string PercentageValue { get; set; }

        [Required]
        [DisplayName("Normal Rebate")]
        public string NormalRebate { get; set; }

        [Required]
        [DisplayName("Special Rebate")]
        public string SpclRebate { get; set; }

        [Required]
        [DisplayName("Max Saving")]
        public string MaxSaving { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            double lowerLimit = Convert.ToDouble(LowerLimit);
            double upperLimit = Convert.ToDouble(UpperLimit);

            if (upperLimit <= lowerLimit)
            {
                yield return new ValidationResult("Upper limit can't be less than lower limit",

                    new[] { "Upper Limit" });
            }

            if (string.IsNullOrEmpty(LowerLimit))
            {
                yield return new ValidationResult("Please enter Lower Limit",

                    new[] { "Lower Limit" });
            }
            else if (LowerLimit.Length > 6)
            {
                yield return new ValidationResult("Lower Limit can't be more than 6 char",

                   new[] { "Lower Limit" });
            }

            if (string.IsNullOrEmpty(UpperLimit))
            {
                yield return new ValidationResult("Please enter Upper Limit",

                    new[] { "Upper Limit" });
            }

            if (string.IsNullOrEmpty(PercentageValue))
            {
                yield return new ValidationResult("Please select Percentage/Value",

                    new[] { "Percentage Value" });
            }

            if (string.IsNullOrEmpty(NormalRebate))
            {
                yield return new ValidationResult("Please enter Normal Rebate",

                    new[] { "Normal Rebate" });
            }

            if (string.IsNullOrEmpty(SpclRebate))
            {
                yield return new ValidationResult("Please enter Special Rebate",

                    new[] { "Special Rebate" });
            }

            if (string.IsNullOrEmpty(MaxSaving))
            {
                yield return new ValidationResult("Please enter Max Saving",

                    new[] { "Max Saving" });
            }
        }
    }

    public class ExemptionDeductionDetails : IValidatableObject
    {
        public ExemptionDeductionDetails()
        {
            this.RateDetails = new List<ExemptionDeductionRateDetailsModel>();
        }

        public int TotalCount { get; set; }

        [Required]
        public int MsEdrId { get; set; }

        public int DrrID { get; set; }

        public int DrrID2 { get; set; }

        [Required]
        [DisplayName("Financial Year From")]
        public string FinancialYearFrom { get; set; }

        public string FinancialYearFromDesc { get; set; }

        public string FinancialYearTo { get; set; }

        public string FinancialYearToDesc { get; set; }

        [Required]
        [DisplayName("Slab Limits For")]
        public string SlabLimitsFor { get; set; }

        public string DRR_DependentOnDesc { get; set; }

        public string SlDesc { get; set; }

        [Required]
        [DisplayName("Major Section Code")]
        public string MajorSectionCode { get; set; }

        public string MajSecDesc { get; set; }

        [Required]
        [DisplayName("Section Code")]
        public string SectionCode { get; set; }

        [Required]
        [DisplayName("Sub Section Code")]
        public string SubSectionCode { get; set; }

        public string SectionDesc { get; set; }

        public string UserName { get; set; }

        public string UserIp { get; set; }

        public List<ExemptionDeductionRateDetailsModel> RateDetails { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //string[] financialYearFrom = FinancialYearFrom.Split("-");
            //double fromToYear = Convert.ToDouble(financialYearFrom[1]);
            //string[] financialYearTo = FinancialYearTo.Split("-");
            //double toToYear = Convert.ToDouble(financialYearTo[1]);

            double fromToYear = Convert.ToDouble(FinancialYearFrom);
            double toToYear = Convert.ToDouble(FinancialYearTo);

            if (fromToYear > toToYear)
            {
                yield return new ValidationResult("Financial Year To can't be less than Financial Year From",

                    new[] { "Financial Year To" });
            }

            if (string.IsNullOrEmpty(MajorSectionCode))
            {
                yield return new ValidationResult("Please select Major Section Code",

                    new[] { "Major Section Code" });
            }

            if (string.IsNullOrEmpty(SectionCode))
            {
                yield return new ValidationResult("Please select Section Code",

                    new[] { "Section Code" });
            }

            if (string.IsNullOrEmpty(SubSectionCode))
            {
                yield return new ValidationResult("Please select Sub Section Code",

                    new[] { "Sub Section Code" });
            }

            if (string.IsNullOrEmpty(FinancialYearFrom))
            {
                yield return new ValidationResult("Please select Financial Year From",

                    new[] { "Financial Year From" });
            }

            if (string.IsNullOrEmpty(SlabLimitsFor))
            {
                yield return new ValidationResult("Please select Slab Limits For",

                    new[] { "Slab Limits For" });
            }
        }

    }

}
