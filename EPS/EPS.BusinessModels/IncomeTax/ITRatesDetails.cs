﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPS.BusinessModels.IncomeTax
{
    public class GetITRatesDetails
    {
        public int ItSurcharge_RateID { get; set; }
        public string IsrFinYr { get; set; }
        public string FullDescription { get; set; }
        public double IsrLlimit { get; set; }
        public double IsrUlimit { get; set; }
        public double IsrAddedAmt { get; set; }
        public int IsrPercVal { get; set; }
        public string IsrRateType { get; set; }
        public string IsrRateFor { get; set; }
        public int TotalCount { get; set; }
    }

    public class UpsertITRatesDetails
    {
        public UpsertITRatesDetails()
        {
            this.RateDetails = new List<UpsertRateDetailsForITRates>();
        }

        [DisplayName("Financial Year")]
        [Required]
        public string IsrFinYr { get; set; }

        [DisplayName("Rate Type")]
        [Required]
        public string IsrRateType { get; set; }

        [DisplayName("Rate For")]
        [Required]
        public string IsrRateFor { get; set; }

        public string IpAddress { get; set; }
        public string PermDdoId { get; set; }
        public string DDOId { get; set; }
        public List<UpsertRateDetailsForITRates> RateDetails { get; set; }
    }

    public class UpsertRateDetailsForITRates : IValidatableObject
    {
        public int Id { get; set; }
        public int ItSurcharge_RateID { get; set; }

        [DisplayName("Lower Limit")]
        [Required]
        public double? IsrLlimit { get; set; }

        [DisplayName("Upper Limit")]
        [Required]
        public double? IsrUlimit { get; set; }

        [DisplayName("Added Amount")]
        [Required]
        public double? IsrAddedAmt { get; set; }

        [DisplayName("Percentage Value")]
        [Required]
        public int? IsrPercVal { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (IsrLlimit > IsrUlimit)
            {
                yield return new ValidationResult("Lower Limit should be less than Upper Limit",

                    new[] { "Lower Limit" });
            }
        }
    }
}
