﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public class PayBillGroup
    {
        public int PayBillGroupId { get; set; }
        public int AccountHead { get; set; }
        //public string PermDDOId { get; set; }
        public string BillGroupName { get; set; }
        //public string EmpId { get; set; }
        public string ServiceType { get; set; }
        public string Group { get; set; }
        public string GroupIds { get; set; }
        public bool BillForGAR { get; set; }
        public string PermDDOId { get; set; }
    }
}
