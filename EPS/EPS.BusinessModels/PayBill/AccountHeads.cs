﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public class AccountHeads
    {
        public int MsPSchemeID { get; set; }
        public string SchemeCode { get; set; }
    }
}
