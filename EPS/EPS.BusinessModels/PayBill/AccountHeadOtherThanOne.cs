﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public class AccountHeadOtherThanOne
    {
        public int ddoCode { get; set; }
        public string ddoCodeddoName { get; set; }
        public string FinFromYr { get; set; }
        public string FinToYr { get; set; }

        public string AccountHead { get; set; }
        public string ObjectHead { get; set; }
        public string Category { get; set; }
        public string AccountHeadName { get; set; }
        public string PaybillGroupStatus { get; set; }
        public string Descriptions { get; set; }
        public string HiddenAccountHead { get; set; }
    }
}
