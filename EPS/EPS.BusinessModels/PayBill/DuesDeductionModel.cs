﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
   public class DuesDeductionModel
    {
    }

    public class PayBillGroupByFinancialYearModel
    {
        public int PayBillGroupId { get; set; }
        public string BillGrDesc { get; set; }
    }

    public class GetEmployeesByPayItemCodeModel
    {
        public int MsEmpDuesId { get; set; }
        public string EmpCd { get; set; }
        public int PayItemsCd { get; set; }
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int Amount { get; set; }
    }

    public class GetPayItemsFromDesignationCodeModel
    {
        public int MsPayItemsId { get; set; }
        public int PayItemsCd { get; set; }
        public string PayItemsNameShort { get; set; }
        public string PayItemsName { get; set; }
        public int IsDeus_Deduct { get; set; }
    }

    public class UpdateEmpDuesDeductAmountModel
    {
        public string IpAddress { get; set; }
        public string PermDdoId { get; set; }
        public string DDOId { get; set; }
        public List<EmpDetailsModel> EmpDetails { get; set; }
    }

    public class EmpDetailsModel
    {
        public int SNo { get; set; }
        public int EmpId { get; set; }
        public string EmpCd { get; set; }
        public int Amount { get; set; }
        public int PayItemCd { get; set; }
    }
}
