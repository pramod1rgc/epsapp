﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public  class PayBillGroupAttachandDeattachModel
    {
        public int msEmpPayId { get; set; }

        public string EmpCode { get; set; }

        public int PayComm { get; set; }
        public int PscScaleCd { get; set; }
        public int PayInPb { get; set; }
        public int GradePay { get; set; }
        public int BasicPay { get; set; }

        public DateTime BasicPayDT { get; set; }
        public DateTime IncrpayDT { get; set; }

        public string BillgrID { get; set; }
        public int PayCityClass { get; set; }
        public int EmpPanNo { get; set; }
        public int PfAgency { get; set; }
        public int PfSeriesCD { get; set; }
        public int Pf_No { get; set; }
        public int Pf_Desc { get; set; }
        public int PranNo { get; set; }
        public int PayGisApplicable { get; set; }
        public int PayGISGrp_CD { get; set; }
        public int GisMembershipDT { get; set; }
        public int EmpPliNo { get; set; }
        public int EmpMsliNo { get; set; }
        public int PayVerifFlag { get; set; }
        public int PayEntryDtts { get; set; }
        public int PayUpdDtts { get; set; }
        public int PfVerifFlag { get; set; }
        public int PayIndex { get; set; }

        public int PfEnrtyDtts { get; set; }
        public int PfUpdDtts { get; set; }
        public int GisVerifyFlag { get; set; }
        public int GisEnrtyDtts { get; set; }
        public int GisUpdDtts { get; set; }
        public int EntitledOffVeh { get; set; }
        public int GpfStopFlag { get; set; }
        public int PayLevel { get; set; }
        public int CityClassTA { get; set; }
        public int PfDescOLD { get; set; }
        public int PayScaleDesc { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public int ModifiedDate { get; set; }
        public int CreatedClientIP { get; set; }
        public int MSEmpID { get; set; }
        public int ModifiedClientIP { get; set; }
        public int IsActive { get; set; }
        public int MsPayEligibleRefID { get; set; }
        public int RSchemeId { get; set; }
        public string EmpName { get; set; }
        public string Desc { get; set; }
        public string DesigFieldDeptCD { get; set; }































    }
}
