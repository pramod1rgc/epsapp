﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public class PayBillGroupModel
    {
        private int payBillGroupId;
        private string permDDOId;
        private string serviceType;
        private int billgrId;
        private string billgrDesc;
        private string billgrpType;
        private double schemeId;
        private string demandNo;
        private int postType;
        private string groupId;
        private DateTime entryDtts;
        private DateTime updDtts;
        private string forJudges;
        private string defaultBillgroup;
        private string billgrFor;
        private int finYearFrom;
         private int finYearTo;
        private int oldSchemeId;
        private string updateFlag;
        private DateTime createdDate;
        private string createdBy;
        private DateTime modifiedDate;
        private string modifiedBy;
        private string clientIP;
        private string mACADD;

        public int PayBillGroupId { get => payBillGroupId; set => payBillGroupId = value; }
        public string PermDDOId { get => permDDOId; set => permDDOId = value; }
        public int BillgrId { get => billgrId; set => billgrId = value; }
        public string BillgrDesc { get => billgrDesc; set => billgrDesc = value; }
        public string BillgrpType { get => billgrpType; set => billgrpType = value; }
        public double SchemeId { get => schemeId; set => schemeId = value; }
        public string DemandNo { get => demandNo; set => demandNo = value; }
        public int PostType { get => postType; set => postType = value; }
        public string GroupId { get => groupId; set => groupId = value; }
        public DateTime EntryDtts { get => entryDtts; set => entryDtts = value; }
        public DateTime UpdDtts { get => updDtts; set => updDtts = value; }
        public string ForJudges { get => forJudges; set => forJudges = value; }
        public string DefaultBillgroup { get => defaultBillgroup; set => defaultBillgroup = value; }
        public string BillgrFor { get => billgrFor; set => billgrFor = value; }
        public int FinYearFrom { get => finYearFrom; set => finYearFrom = value; }
        public int OldSchemeId { get => oldSchemeId; set => oldSchemeId = value; }

        public string UpdateFlag { get => updateFlag; set => updateFlag = value; }
        public DateTime CreatedDate { get => createdDate; set => createdDate = value; }
        public int FinYearTo { get => finYearTo; set => finYearTo = value; }

        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public DateTime ModifiedDate { get => modifiedDate; set => modifiedDate = value; }

        public string ModifiedBy { get => modifiedBy; set => modifiedBy = value; }


        public string ClientIP { get => clientIP; set => clientIP = value; }
        public string MACADD { get => mACADD; set => mACADD = value; }


        public string ServiceType { get => serviceType; set => serviceType = value; }
    }
}
