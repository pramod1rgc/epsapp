﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPS.BusinessModels.PayBill
{
    public class NgRecovery
    {
        private int payNgRecoId;
        private int ngDedCd;
        private string permDDOId;
        private string ngDesc;
        private int? bankId;
        private int? branchId;
        private string ifscCd;
        private string bankAcNo;
        private string bnfName;
        private string pfmsUniqueCode;
        private string vendorStatus;
        private string address;
        private bool isActive;


        public int PayNgRecoId { get => payNgRecoId; set => payNgRecoId = value; }
        public int NgDedCd { get => ngDedCd; set => ngDedCd = value; }
        public string PermDDOId { get => permDDOId; set => permDDOId = value; }
        public string NgDesc { get => ngDesc; set => ngDesc = value; }
        public int? BankId { get => bankId; set => bankId = value; }
        public int? BranchId { get => branchId; set => branchId = value; }
        public string IfscCd { get => ifscCd; set => ifscCd = value; }
        public string BankAcNo { get => bankAcNo; set => bankAcNo = value; }
        public string BnfName { get => bnfName; set => bnfName = value; }

        public string PfmsUniqueCode { get => pfmsUniqueCode; set => pfmsUniqueCode = value; }
        public string VendorStatus { get => vendorStatus; set => vendorStatus = value; }
        public string Address { get => address; set => address = value; }
        public bool IsActive { get => isActive; set => isActive = value; }

    }

    public class BankDetails
    {
        private int bankId;
        private int branchId;
        private string bankName;
        private string branchName;

        public int BankId { get => bankId; set => bankId = value; }
        public int BranchId { get => branchId; set => branchId = value; }
        public string BankName { get => bankName; set => bankName = value; }
        public string BranchName { get => branchName; set => branchName = value; }

    }

    public class NgRecoveryData
    {
        private int payNgRecoEmpdetID;
        private string permDdoId;
        private string empCd;
        private int empId;
        private int ngDedCd;
        private decimal emiAmt;
        private string otherInf;

        public int PayNgRecoEmpdetID { get => payNgRecoEmpdetID; set => payNgRecoEmpdetID = value; }
        public string PermDdoId { get => permDdoId; set => permDdoId = value; }
        public string EmpCd { get => empCd; set => empCd = value; }
        public int EmpId { get => empId; set => empId = value; }
        public int NgDedCd { get => ngDedCd; set => ngDedCd = value; }
        public decimal EmiAmt { get => emiAmt; set => emiAmt = value; }
        public string OtherInf { get => otherInf; set => otherInf = value; }

    }

    public class FinancialYears
    {
        private int financialYear;
        private string fullDescription;

        public int FinancialYear { get => financialYear; set => financialYear = value; }
        public string FullDescription { get => fullDescription; set => fullDescription = value; }
    }

    public class AllMonths
    {
        private int msMonthid;
        private string monthName;

        public int MsMonthid { get => msMonthid; set => msMonthid = value; }
        public string MonthName { get => monthName; set => monthName = value; }
    }

    public class EmpList
    {
        private string permDdoId;
        private string empCode;
        private string empName;
        private decimal subscriptionAmt;
        private decimal recoveryAmt;
        private decimal totalAmt;
        private int installmentCount;
        private int recoveredInstallment;
        private string ngDedCd;
        
        public string PermDdoId { get => permDdoId; set => permDdoId = value; }
        public string EmpCode { get => empCode; set => empCode = value; }
        public string EmpName { get => empName; set => empName = value; }
        public decimal SubscriptionAmt { get => subscriptionAmt; set => subscriptionAmt = value; }
        public decimal RecoveryAmt { get => recoveryAmt; set => recoveryAmt = value; }
        public decimal TotalAmt { get => totalAmt; set => totalAmt = value; }
        public int InstallmentCount { get => installmentCount; set => installmentCount = value; }
        public int RecoveredInstallment { get => recoveredInstallment; set => recoveredInstallment = value; }
        public string NgDedCd { get => ngDedCd; set => ngDedCd = value; }
    }

    public class NgList
    {
        private int ngDedCd;
        private string ngDesc;
        private List<BillgoupDetails> payBillDetails;

        public int NgDedCd { get => ngDedCd; set => ngDedCd = value; }
        public string NgDesc { get => ngDesc; set => ngDesc = value; }
        public List<BillgoupDetails> PayBillDetails { get => payBillDetails; set => payBillDetails = value; }
    }

    public class BillgoupDetails
    {
        
        private int payBillGroupId;
        private string billgrDesc;
        private int monthId;
        private string monthName;


        public int PayBillGroupId { get => payBillGroupId; set => payBillGroupId = value; }
        public string BillgrDesc { get => billgrDesc; set => billgrDesc = value; }
        public int MonthId { get => monthId; set => monthId = value; }
        public string MonthName { get => monthName; set => monthName = value; }
    }
}
