﻿using Microsoft.AspNetCore.Http;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace EPS.CommonClasses
{
    public static class CommonFunctions
    {
        // Set Special Character 
        private static string SPL_CHAR = "<->-%";

        #region converts the value to string.
        /// <summary>
        /// converts the value to string. If the input value is null or blank then returns the default value.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ConvertToString(object obj, string defaultValue)
        {
            string retValue;

            if (obj == null || obj.ToString() == string.Empty)
            {
                retValue = defaultValue;
            }
            else
            {
                retValue = Convert.ToString(obj);
            }
            return retValue;
        }
        #endregion
        #region converts the value to DateTime.
        /// <summary>
        ///  Converts the in value into datetime object type value.
        ///  Checks for null value in advance
        ///  Currently does not handle exception, if the need arises then may create another method which 
        ///  takes a parameter specifying whether the exception is suppressed and default value 
        ///  is passed back or raised.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static DateTime ConvertToDateTime(object obj, DateTime defaultValue)
        {
            DateTime retValue;

            if (obj == null || obj.ToString() == "")
            {
                retValue = defaultValue;
            }
            else
            {
                retValue = Convert.ToDateTime(obj);
            }
            return retValue;
        }
        #endregion
        #region converts the value to Integer.
        /// <summary>
        /// converts the value to integer. If the input value is null or blank then returns the default value.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ConvertToInteger(object obj, int defaultValue)
        {
            int retValue;

            if (obj == null || obj.ToString() == "")
            {
                retValue = defaultValue;
            }
            else
            {
                retValue = Convert.ToInt32(obj);
            }
            return retValue;
        }
        #endregion
        #region converts the value to Bool.
        /// <summary>
        /// converts the value to bool. If the input value is null or blank then returns the default value.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool ConvertToBoolean(object obj, bool defaultValue)
        {
            bool retValue;

            if (obj == null || obj.ToString() == "")
            {
                retValue = defaultValue;
            }
            else
            {
                retValue = Convert.ToBoolean(obj);
            }
            return retValue;
        }

        #endregion
        #region converts the value to Decimal.

        /// <summary>
        /// converts the value to decimal. If the input value is null or blank then returns the default value.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static decimal ConvertToDecimal(object obj, decimal defaultValue)
        {
            decimal retValue;

            if (obj == null || obj.ToString() == "")
            {
                retValue = defaultValue;
            }
            else
            {
                retValue = Convert.ToDecimal(obj);
            }
            return retValue;
        }

        #endregion

        #region Get value to Decimal.
        
        /// <summary>
        /// Converts the decimal value into current culture and returned the string value. 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string GetDecimal(decimal val)
        {
            CultureInfo current = new CultureInfo("hi-IN");
            return val.ToString("N0", current);
        }
        #endregion

        #region GetCurrent Financial Year.
        public static int GetCurrentFinancialYear()
        {
            int result = 2020;
            //if (HttpContext.Current != null && HttpContext.Current.Session[CommonConstants.C_SESSION_FINANCIALYEAR_KEY] != null)
            //{
            //    result = Convert.ToInt32(HttpContext.Current.Session[CommonConstants.C_SESSION_FINANCIALYEAR_KEY].ToString());
            //}
            //if ((result == 0) && (!String.IsNullOrEmpty(ConfigurationManager.AppSettings[CommonConstants.C_CONFIG_FINANCIALYEAR_KEY].ToString())))
            //{
            //    result = Convert.ToInt32(ConfigurationManager.AppSettings[CommonConstants.C_CONFIG_FINANCIALYEAR_KEY]);
            //}
            return result;
        }
        #endregion

        #region Convert Number To Words

        static string[] units = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                            "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                            "eighteen", "nineteen"};

        static string[] tens = { "", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        public static string ConvertToWords(decimal number)
        {
            StringBuilder InWords = new StringBuilder();
            decimal decimalNumber = (number - Math.Floor(number)) * 100;
            number = Math.Floor(number);

            int length = number.ToString().Length;

            switch (length)
            {
                case 11:
                case 10:
                case 9:
                case 8:
                    InWords.Append(FormCrores(number));
                    break;
                case 7:
                case 6:
                    InWords.Append(FormLakhs(number));
                    break;
                case 5:
                case 4:
                    InWords.Append(FormThousands(number));
                    break;
                case 3:
                    InWords.Append(FormHundreds(number));
                    break;
                case 2:
                case 1:
                    InWords.Append(FormTens(number));
                    break;
            }

            if (number > 0)
                InWords.Insert(0, "Rupees ");

            if (decimalNumber > 0)
            {
                if (number > 0)
                    InWords.Append(" and ");
                InWords.Append(FormTens(decimalNumber).Trim());
                InWords.Append("paise");
            }

            if (number > 0 || decimalNumber > 0)
                InWords.Append(" only");
            return InWords.ToString();
        }

        private static string FormTens(decimal number)
        {
            const decimal MINIMUM_VALUE = 10;
            string InWords = string.Empty;

            if (number < 20)
            {
                InWords = units[(int)number];
                return InWords.ToString();
            }

            decimal firstNumber = Math.Floor(number / MINIMUM_VALUE);
            decimal lastNumber = number % MINIMUM_VALUE;

            InWords = tens[(int)firstNumber] + " " + units[(int)lastNumber] + " ";

            return InWords;
        }

        private static string FormHundreds(decimal number)
        {
            const decimal MINIMUM_VALUE = 100;
            string InWords = string.Empty;

            decimal firstNumber = Math.Floor(number / MINIMUM_VALUE);
            decimal balanceNumber = number % MINIMUM_VALUE;

            InWords = units[(int)firstNumber];

            if (firstNumber > 0)
                InWords += " hundred ";

            InWords += FormTens(balanceNumber);

            return InWords;
        }

        private static string FormThousands(decimal number)
        {
            const decimal MINIMUM_VALUE = 1000;
            string InWords = string.Empty;

            decimal firstNumber = Math.Floor(number / MINIMUM_VALUE);
            decimal balanceNumber = number % MINIMUM_VALUE;

            InWords = FormTens(firstNumber).Trim();

            if (firstNumber > 0)
                InWords += " thousand ";

            InWords += FormHundreds(balanceNumber);

            return InWords;
        }
        
        private static string FormLakhs(decimal number)
        {
            const decimal MINIMUM_VALUE = 100000;
            string InWords = string.Empty;

            decimal firstNumber = Math.Floor(number / MINIMUM_VALUE);
            decimal balanceNumber = number % MINIMUM_VALUE;

            InWords = FormTens(firstNumber).Trim();

            if (firstNumber > 0)
                InWords += " lakh(s) ";

            InWords += FormThousands(balanceNumber);

            return InWords;
        }

        private static string FormCrores(decimal number)
        {
            const decimal MINIMUM_VALUE = 10000000;
            string InWords = string.Empty;

            decimal firstNumber = Math.Floor(number / MINIMUM_VALUE);
            decimal balanceNumber = number % MINIMUM_VALUE;

            InWords = FormThousands(firstNumber).Trim();

            if (firstNumber > 0)
                InWords += " crore(s) ";

            InWords += FormLakhs(balanceNumber);

            return InWords;
        }

        #endregion

        // For getting the current financial year start date and end date
        #region Get from Date
        public static DateTime GetFromDate()
        {
            return DateTime.ParseExact("01/04/" + (GetCurrentFinancialYear() - 1).ToString(), "dd/MM/yyyy", null);
        }
        #endregion
        #region Get to Date
        public static DateTime GetToDate()
        {
            DateTime DtTo = DateTime.ParseExact("31/03/" + GetCurrentFinancialYear().ToString(), "dd/MM/yyyy", null);
            if (DtTo > DateTime.Now)
                return DateTime.Now;
            return DtTo;
        }
        #endregion
        #region Get from Date Financial year
        public static DateTime GetFromDateFinYear(int AddDays)
        {
            DateTime DtFrom = DateTime.ParseExact("01/04/" + (GetCurrentFinancialYear() - 1).ToString(), "dd/MM/yyyy", null);
            //DateTime DtTo = DateTime.ParseExact("31/03/" + GetCurrentFinancialYear().ToString(), "dd/MM/yyyy", null);
            if (GetToDate().AddDays(-AddDays) > DtFrom)
                return GetToDate().AddDays(-AddDays);
            return DtFrom;
        }
        #endregion

        #region Replace Spacial Character
        //end
        /// <summary>
        /// Replace special character from User input data. Like % 
        /// </summary>
        /// <param name="User Input Value"></param>
        /// <returns>string</returns>
        public static string ReplaceSpecialCharacter(string value)
        {
            // Remove specific character string read from app config file.
            string[] strvalue = SPL_CHAR.Split('-');
            if (!string.IsNullOrEmpty(value))
            {
                foreach (string st in strvalue)
                {
                    if (value.Contains(st) && st != "")
                    {
                        value = value.Replace(st, "");
                    }
                }
            }
            return value;
        }
        #endregion
        #region Scan HTML
        public static string ScanHTML(string str)
        {
            string[] strChar = new string[22];

            strChar[0] = "<applet>";
            strChar[1] = "<body>";
            strChar[2] = "<frame>";
            strChar[3] = "<script>";
            strChar[4] = "<frameset>";
            strChar[5] = "<iframe>";
            strChar[6] = "<layer>";
            strChar[7] = "<link>";
            strChar[8] = "<ilayer>";
            strChar[9] = "<meta>";
            strChar[10] = "<object>";
            strChar[11] = "</applet>";
            strChar[12] = "</body>";
            strChar[13] = "</frame>";
            strChar[14] = "</script>";
            strChar[15] = "</frameset>";
            strChar[16] = "</iframe>";
            strChar[17] = "</layer>";
            strChar[18] = "</link>";
            strChar[19] = "</ilayer>";
            strChar[20] = "</meta>";
            strChar[21] = "</object>";

            int i;
            int flag = 0;
            for (i = 0; i < strChar.Length; i++)
            {
                if (str.Contains(strChar[i]))
                {
                    flag = 1;
                    break;
                }
            }

            if (flag == 1)
            {
                return strChar[i];
            }
            else
            {
                return "Perfect";
            }
        }
        #endregion

        #region Get Style Control
        ////public static Control GetStyleControl(string style)
        ////{
        ////    HtmlGenericControl control = new HtmlGenericControl("script");
        ////    control.Attributes.Add("type", "text/css");
        ////    control.Attributes.Add("rel", "stylesheet");

        ////    control.InnerHtml = style;

        ////    return control;
        ////}
        #endregion
        #region Convert Byte to String
        public static string ConvertToString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        #endregion
        #region Convert String to Byte
        public static byte[] ConvertToBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        #endregion
        #region Mask Input Text
        public static String MaskInput(String input, int charactersToShowAtEnd)
        {
            if (input.Length < charactersToShowAtEnd)
            {
                charactersToShowAtEnd = input.Length;
            }
            String endCharacters = input.Substring(input.Length - charactersToShowAtEnd);
            return String.Format("{0}{1}", "".PadLeft(input.Length - charactersToShowAtEnd, '*'), endCharacters);
        }
        #endregion
        #region Mask Email Id
        public static string MaskEmailId(string emailid)
        {
            string sFinalEmailId = emailid;
            if (emailid.Contains('@'))
            {
                string sFirstPart = emailid.Split('@')[0].ToString();
                string sSecondPart = emailid.Split('@')[1].ToString();
                int sStartLength = 2;
                sFinalEmailId = string.Format("{0}{1}", sFirstPart.Substring(0, sStartLength), "".PadRight(sFirstPart.Length - sStartLength, '*')) + "@" + string.Format("{0}{1}", sSecondPart.Substring(0, sStartLength), "".PadRight(sSecondPart.Length - sStartLength, '*'));
            }
            return sFinalEmailId;
        }
        #endregion
        #region Convert Number to Word
        // Converting number to word
        public static string ConvertNumbertoWords(int number)
        {
            if (number == 0)
                return "ZERO";
            if (number < 0)
                return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";

            if ((number / 1000000000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000000000) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 10000000) > 0)
            {
                words += ConvertNumbertoWords(number / 10000000) + " CRORE ";
                number %= 10000000;
            }

            if ((number / 100000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " LAKH ";
                number %= 100000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            if (number > 0)
            {
                if (words != "")
                    words += "AND ";
                var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
        #endregion
        #region Convert Date Time to Int
        public static int DateTimeToInt(DateTime? inputDate)
        {
            if (inputDate != null)
                return (int)(inputDate.Value - new DateTime(1900, 1, 1)).TotalDays;
            else
                return 0;
        }
        #endregion
        #region Convert Int to DateTime
        //Reverse
        public static DateTime IntToDateTime(int inputDate)
        {
            return new DateTime(1900, 1, 1).AddDays(inputDate - 2);
        }
        #endregion
        #region Genrate password SHA2String
        public static string GenerateSHA2String(string Password)
        {
            byte[] hash = GenerateSHA2Byte(Password);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        #endregion
        #region Genrate password SHA2Byte
        public static byte[] GenerateSHA2Byte(string Password)
        {
            byte[] OriginalPassword = Encoding.UTF8.GetBytes(Password);
            SHA256Managed objSHA256Managed = new SHA256Managed();
            byte[] HashPassword = objSHA256Managed.ComputeHash(OriginalPassword);
            return HashPassword;
        }
        #endregion
        #region Convert Byte To Hex
        public static string ConvertBytesToHex(byte[] bytes)
        {
            string sString = string.Empty;
            foreach (byte x in bytes)
            {
                sString += String.Format("{0:x2}", x);
            }
            return sString;
        }
        #endregion
        #region Convert Hex to Byte

        public static byte[] ConvertHexToBytes(string hexData)
        {
            List<byte> data = new List<byte>();
            string byteSet = string.Empty;
            int stringLen = hexData.Length;
            int length = 0;
            for (int i = 0; i < stringLen; i = i + 2)
            {
                length = (stringLen - i) > 1 ? 2 : 1;
                byteSet = hexData.Substring(i, length);

                // try and parse the data
                data.Add(Convert.ToByte(byteSet, 16 /*base*/));
            } // next set

            return data.ToArray();
        }
        #endregion
        #region Encrypt EPSSystem query
        /// <summary>
        /// It is used to Encrypt EPSSystem query string data. 
        /// </summary>
        /// <param name="strQueryString">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string EncryptEPS(string PlainText)
        {
            string EncryptionKey = "EPSSystem@10249157242";

            //Getting the bytes of Input String.
            byte[] toEncryptedArray = UTF8Encoding.UTF8.GetBytes(PlainText);

            MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();

            //Gettting the bytes from the Security Key and Passing it to compute the Corresponding Hash Value.
            byte[] securityKeyArray = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(EncryptionKey));

            //De-allocatinng the memory after doing the Job.
            objMD5CryptoService.Clear();

            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();

            //Assigning the Security key to the TripleDES Service Provider.
            objTripleDESCryptoService.Key = securityKeyArray;

            //Mode of the Crypto service is Electronic Code Book.
            objTripleDESCryptoService.Mode = CipherMode.ECB;

            //Padding Mode is PKCS7 if there is any extra byte is added.
            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;

            var objCrytpoTransform = objTripleDESCryptoService.CreateEncryptor();

            //Transform the bytes array to resultArray
            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptedArray, 0, toEncryptedArray.Length);

            //Releasing the Memory Occupied by TripleDES Service Provider for Encryption.
            objTripleDESCryptoService.Clear();

            //Convert and return the encrypted data/byte into string format.
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        #endregion
        #region Decrypt EPSSystem query
        /// <summary>
        /// It is used to decrypt EPSSystem query string data.  
        /// <param name="strQueryString">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string DecryptEPS(string CipherText)
        {

            string EncryptionKey = "EPSSystem@10249157242";
            if (!string.IsNullOrEmpty(CipherText))
            {
                byte[] toEncryptArray = Convert.FromBase64String(CipherText);

                MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();

                //Gettting the bytes from the Security Key and Passing it to compute the Corresponding Hash Value.
                byte[] securityKeyArray = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(EncryptionKey));

                //De-allocatinng the memory after doing the Job.
                objMD5CryptoService.Clear();

                var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();

                //Assigning the Security key to the TripleDES Service Provider.
                objTripleDESCryptoService.Key = securityKeyArray;

                //Mode of the Crypto service is Electronic Code Book.
                objTripleDESCryptoService.Mode = CipherMode.ECB;

                //Padding Mode is PKCS7 if there is any extra byte is added.
                objTripleDESCryptoService.Padding = PaddingMode.PKCS7;

                var objCrytpoTransform = objTripleDESCryptoService.CreateDecryptor();

                //Transform the bytes array to resultArray
                byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                //Releasing the Memory Occupied by TripleDES Service Provider for Decryption.          
                objTripleDESCryptoService.Clear();

                //Convert and return the decrypted data/byte into string format.
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region Get Client IP
        public static string GetClientIP(IHttpContextAccessor accessor)
        {
            var userIpAddress = accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            return Convert.ToString(userIpAddress);
        }
        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// For checking url
        /// </summary>
        /// <param name="url">
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        //public static bool CheckUrl(string url)
        //{
        //    bool flag = true;
        //    HttpSessionState session = System.Web.HttpContext.Current.Session;
        //    if (session["url"] == null)
        //    {
        //        session["url"] = url;
        //    }

        //    if (session["url"].ToString() != url)
        //    {
        //        flag = false;
        //        session["url"] = null;
        //    }

        //    return flag;
        //}

        /// <summary>
        /// DES Decrypt
        /// </summary>
        /// <param name="stringToDecrypt">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string DESDecrypt(string stringToDecrypt)
        {
            byte[] key;
            byte[] iv;
            byte[] inputByteArray;
            try
            {
                key = Convert2ByteArray("AQWSEDRF");
                iv = Convert2ByteArray("HGFEDCBA");
                int len = stringToDecrypt.Length;
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// DES Encrypt
        /// </summary>
        /// <param name="stringToEncrypt">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string DESEncrypt(string stringToEncrypt)
        {
            byte[] key;
            byte[] iv;
            byte[] inputByteArray;
            try
            {
                key = Convert2ByteArray("AQWSEDRF");
                iv = Convert2ByteArray("HGFEDCBA");
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GenerateRandomCode()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
        }

        /// <summary>
        /// The dsc encriptar.
        /// </summary>
        /// <param name="strValor">
        /// The str valor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string DSCEncriptar(string strValor)
        {
            string strEncrKey = "AQWSEDRF";
            byte[] byKey = { };
            byte[] IV = Encoding.UTF8.GetBytes("HGFEDCBA");
            try
            {
                byKey = Encoding.UTF8.GetBytes(strEncrKey);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                des.Mode = CipherMode.ECB;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(strValor);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// It is used to decryption query string data.
        /// </summary>
        /// <param name="strQueryString">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string DecryptQueryString(string strQueryString)
        {
            return DESDecrypt(strQueryString.Replace(' ', '+'));
        }

        /// <summary>
        /// IT IS USED TO ENCRYPTION QUERYSTRING DATA.
        /// </summary>
        /// <param name="strQueryString">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string EncryptQueryString(string strQueryString)
        {
            return DESEncrypt(strQueryString);
        }

        #endregion

        #region Methods Convert2ByteArray

        /// <summary>
        /// For converting to byte array
        /// </summary>
        /// <param name="strInput">
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private static byte[] Convert2ByteArray(string strInput)
        {
            int intCounter;
            char[] arrChar;
            arrChar = strInput.ToCharArray();
            byte[] arrByte = new byte[arrChar.Length];
            for (intCounter = 0; intCounter <= arrByte.Length - 1; intCounter++)
            {
                arrByte[intCounter] = Convert.ToByte(arrChar[intCounter]);
            }

            return arrByte;
        }

        #endregion

        #region Convert Image to Btyes
        public static string ConvertImageToBytes(string imgPath)
        {
            string imgDataURL = string.Empty;

            if (File.Exists(imgPath))
            {

                byte[] byteData = File.ReadAllBytes(imgPath);
                string imreBase64Data = Convert.ToBase64String(byteData, 0, byteData.Length);
                imgDataURL = "data:image/png;base64," + imreBase64Data;
            }
            return imgDataURL;
        }

        #endregion

        #region Convert pdf to Btyes
        public static string ConvertPDFToBytes(string pdfPath)
        {
            string pdfDataURL = string.Empty;

            if (File.Exists(pdfPath))
            {

                byte[] byteData = File.ReadAllBytes(pdfPath);
                string pdfBase64Data = Convert.ToBase64String(byteData, 0, byteData.Length);
                pdfDataURL = "data:application/pdf;base64," + pdfBase64Data;

            }
            return pdfDataURL;
        }

        #endregion

        #region Email and random generated
        //Email and random generated

        public static void SendMail(string MailTo, string Subject, string strBody)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(MailTo);//Enter Receiver mail Id
            mail.From = new MailAddress(MailTo);//"tabaraba890@gmail.com");
            mail.Subject = Subject;// "Refrence id Generate";
            mail.IsBodyHtml = true;
            mail.Body = strBody;
            //if (bytes != null)
            //{
            //    mail.Attachments.Add(new Attachment(new MemoryStream(bytes), Attachments));
            //}
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("epssystem4@gmail.com", "eps@1234"); // Enter seders User name and password  
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }



        public static string RandomPassword()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return "EPS@" + _rdm.Next(_min, _max);
        }
        #endregion

        #region convert DataTable  
        public static DataTable ToDataTable<T>(List<T> items)

        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)

            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name);

            }

            foreach (T item in items)

            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)

                {

                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            //put a breakpoint here and check datatable

            return dataTable;

        }
        #endregion

        public static string GetResponseMessage(int response)
        {
            EPSEnum.Response enumResponse = (EPSEnum.Response)response;

            switch (enumResponse)
            {
                case EPSEnum.Response.Error:
                    return EPSResource.SomethingWrongMessage;

                case EPSEnum.Response.Insert:
                    return EPSResource.SaveSuccessMessage;

                case EPSEnum.Response.Update:
                    return EPSResource.UpdateSuccessMessage;

                case EPSEnum.Response.Delete:
                    return EPSResource.DeleteSuccessMessage;

                case EPSEnum.Response.AlreadyExist:
                    return EPSResource.AlreadyExistMessage;

                default:
                    return EPSResource.ErrorMessage;
            }

        }

        #region Map DB data

        public static T MapFetchData<T>(T obj, IDataReader rdr)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName); // getting property datatype
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                property.SetValue(obj, !DBNull.Value.Equals(rdr[property.Name.ToString()]) ? Convert.ChangeType(rdr[property.Name.ToString()], type) : null); // setting property value according to datatype and propertyname
            }
            return obj;
        }


        public static string MapUpdateParameters<T>(T obj, Database _epsDatabase, System.Data.Common.DbCommand dbCommand)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName); // getting property datatype
                if (type.Namespace.Equals("System"))
                {
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        type = Nullable.GetUnderlyingType(type);
                    }
                    _epsDatabase.AddInParameter(dbCommand, property.Name, (DbType)Enum.Parse(typeof(DbType), type.Name, true), property.GetValue(obj));
                }
                else
                {
                    IEnumerable<dynamic> value = property.GetValue(obj) as IEnumerable<dynamic>;
                    var datatble = CreateDataTable(value);
                    SqlParameter param = new SqlParameter(property.Name, datatble);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);
                }
            }
            return (_epsDatabase.ExecuteNonQuery(dbCommand).ToString());
        }


        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            DataTable table = new DataTable();
            var properties = new PropertyInfo[20]; // typeof(T).GetProperties();
            foreach (T l in list)
            {
                Type t = l.GetType();
                properties = t.GetProperties();
                break;
            }
            foreach (var property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName);
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                table.Columns.Add(property.Name, type);
            }
            foreach (T li in list)
            {
                var values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(li, null);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        #endregion
                      
        public static string TrimString(string str)
        {
            return str == null ? "" : str.Trim();
        }
    }
}
