﻿namespace EPS.CommonClasses
{
    public class EPSConstant
    {
        #region "DashBoard"
        public static string SP_BindMenuUserBased = "[ODS].[BindMenuUserBased_SP]";
        #endregion

        #region "User Manegement"

        public static string Mst_InsertUpdateMenu_SP = "[ODS].[Mst_InsertUpdateMenu_SP]";
        public static string Mst_MenuActiveAndDeActive_SP = "[ODS].[Mst_MenuActiveAndDeActive_SP]";

        public static string SP_GetMenuDDl = "[ODS].[SP_GetMenuDDl]";
        public static string SP_GetMenuInGride = "[ODS].[SP_GetMenuInGride]";
        public static string ChKRadioStatuspermission_SP = "[ODS].[ChKRadioStatuspermission_SP]";
        public static string Insert_MsAssignMenueType = "[ODS].[Insert_MsAssignMenueType]";
        public static string GetAllMenueList_SP = "[ODS].[GetAllMenueList_SP]";
        public static string Insert_RoleMapping = "[ODS].Insert_RoleMapping";
        public static string GetAllRoles = "[ODS].[GetAllRoles]";

        public static string GetAllActiveRollList = "[ODS].[GetAllActiveRoles]";
        public static string GetAllCustomeRoll_SP = "[ODS].[GetAllCustomeRoll_SP]";
        public static string GetBindUserListddl_SP = "[ODS].[GetBindUserListddl_SP]";

        public static string Mst_UserDetails_SP = "[ODS].[Mst_UserDetails_SP]";
        public static string Mst_RevokeUser_SP = "[ODS].[Mst_RevokeRole_SP]";//"[ODS].[Mst_RevokeUser_SP]";

        public static string Mst_InsertUpdateRole_SP = "[ODS].[Mst_InsertUpdateRole_SP]";
        public static string USP_GetUserDetailsByPan = "[ODS].[USP_GetUserDetailsByPan]";
        public static string GetroleOnUserMap_SP = "[ODS].[USP_GetroleOnUserMap]";

        // User creation
        public static string USP_PayRoleAssign = "[ODS].[PayRoleAssign]";
        public static string USP_GetAllUserRoles = "[ODS].[GetAllUserRoles]";
        public static string GetPredefineRole_Sp = "[ODS].[GetPredefineRole_Sp]";
        public static string FillUpdateMenu_SP = "[ODS].[FillUpdateMenu_SP]";
        public static string GetDashboardDetails_Sp = "[ODS].[GetDashboardDetails]";
        public static string BindMenuUserRoleBased_Sp = "[ODS].[BindMenuUserRoleBased_SP]";
        #endregion

        
        #region Masters
        
        public static string SP_MsLeaveType = "[ODS].[usp_MsLeaveType]";
        public static string SP_MsCommonDesign = "[ODS].[usp_MsCommonDesig]";
        public static string SP_MsStateAg_pf = "[ODS].[usp_MsStateAg_pf]";
        public static string sp_GetPayScaleDetails = "[ODS].[usp_GetPayScaleDetails]";
        public static string sp_GetPayScaleformat = "[ODS].[usp_GetPayScaleformat]";
        public static string sp_GetPayScaleGroup = "[ODS].[usp_GetPayScaleGroup]";
        public static string sp_GetPayGISGroup = "[ODS].[usp_GetPayGISGroup]";
        public static string sp_CreatePayScale = "[ODS].[usp_CreatePayScale]";
        public static string usp_GetPayScaleCommissionCode = "[ODS].[usp_GetPayScaleCommissionCode]";
        public static string sp_PayScaleDetailsByID = "[ODS].[sp_PayScaleDetailsByID]";
        public static string usp_GetPayScaleCode = "[ODS].[usp_GetPayScaleCode]";
        public static string usp_DeletePayScale = "[ODS].[usp_DeletePayScale]";

        public static string usp_InsertUpdateDaMaster = "[ODS].[usp_InsertUpdateDaMaster]";
        public static string sp_GetDAMasterDetails = "[ODS].[sp_GetDAMasterDetails]";
        public static string usp_DeleteDAMaster = "[ODS].[usp_DeleteDAMaster]";
        public static string sp_GetDAMasterCurrent_Rate_Wef = "[ODS].[sp_GetDAMasterCurrent_Rate_Wef]";

        public static string Usp_GetGpfIRateMasterDetails = "[ODS].[Usp_GetGpfIRateMasterDetails]";
        public static string usp_InsertUpdateGpfIRateMaster = "[ODS].[usp_InsertUpdateGpfIRateMaster]";
        public static string usp_DeleteGpfInterestrRate = "[ODS].[usp_DeleteGpfInterestrRate]";
        public static string usp_InsertUpdateGpfWithdrawRules = "[ODS].[usp_InsertUpdateGpfWithdrawRules]";
        public static string usp_GpfWithdrawRulesById = "[ODS].[usp_GetGpfWithdrawRulesByID]";
        public static string usp_GpfWithdrawRules = "[ODS].[usp_GetGpfWithdrawRules]";
        public static string usp_DeleteGpfWithdrawRules = "[ODS].[usp_DeleteGpfWithdrawRules]";
        public static string Usp_getAlreadyExixtsRateMasterDetails = "[ODS].[Usp_getAlreadyExixtsRateMasterDetails]";
        
        #region DuesDefinationmaster
        public static string sp_InsertUpdateDuesDifination = "[ODS].[usp_InsertUpdateDauesDifinationMaster]";
        public static string sp_GetDuesDefinationmasterList = "[ODS].[usp_GetDuesDefinationmasterList]";
        public static string SP_DeleteDuesDefination = "[ODS].[usp_DeleteDuesdefinationDetails]";
        public static string Sp_GetDuesDefinationmasterById = "[ODS].[usp_GetDuesDefinatinmasterByID]";
        public static string Sp_GetAutoGenratedDuesCode = "[ODS].[sp_GetDuesCodeAutoGenrated]";
        //=====================DuesRateMaster==============================
        public static string sp_GetOrgnazationTypeForDues = "[ODS].[sp_GetOrgnazationTypeForDues]";
        public static string sp_GetCityClassForDuesRate = "[ODS].[sp_GetCityClassForDuesRate]";
        public static string sp_GetDuesCodeandDescrptionforDuesRate = "[ODS].[sp_GetDuesCodeandDescrptionforDuesRate]";
        public static string sp_getStatenameforDuesMaster = "[ODS].[sp_getStatenameforDuesMaster]";
        public static string sp_GetPatCommForDuesRate = "[ODS].[sp_getPayCommforDuesMaster]";
        public static string sp_GetSlabTypebyPayCommIdForDuesRate = "[ODS].[sp_getSlabTypeforDuesMaster]";
        public static string sp_insertandUpDuesRateDetails = "[ODS].[usp__InsertDuesRateDetails]";
        public static string Sp_GetDuesRateMaster = "[ODS].[usp__GetDuesRateDetails]";
        public static string sp_GetDuesRateDetails = "sp_GetDuesRateDetails";
        public static string sp_DeleteDuesRateDetials = "[ODS].[usp_DeleteDuesRateDetails]";
        #endregion

        #region Family Definition
        public static string Usp_InsertUpdateFdGpfMaster = "[ODS].[Usp_InsertUpdateFdGpfMaster]";
        public static string Usp_GetFdGpfMasterDetails = "[ODS].[Usp_GetFdGpfMasterDetails]";
        public static string usp_EditFdGpfMasterDetails = "[ODS].[usp_EditFdGpfMasterDetails]";
        public static string Usp_DeleteFdGpfMaster = "[Ods].[Usp_DeleteFdGpfMaster]";
        #endregion

        #region HRA Master
        public static string usp_BindPayScaleCode = "[ODS].[Master_GetPayScale_Code]";
        public static string Master_GetPayScaleLevel = "[ODS].[Master_GetPayScaleLevel]";
        public static string usp_GetPayCommissionByEmployeeType = "[ODS].[Usp_GetPayCommissionByEmployeeType]";
        public static string usp_GetGetCityDetailsByStateCode = "[ODS].[Master_GetCityDetail_ByStateCode]";
        public static string usp_GetGetCityClassByPayComm = "[ODS].[GetCityClassByPayCommisionID]";
        public static string usp_InsertUpdateHraMaster = "[ODS].[usp_InsertUpdateHraMaster]";
        public static string Usp_UpdateHRAMaster = "[ODS].[Usp_UpdateHRAMaster]";
        public static string usp_GetHraMasterDetails = "[ODS].[Usp_GetHraMasterDetails]";
        public static string usp_EditHraMasterDetails = "[ODS].[usp_EditHraMasterDetails]";
        public static string usp_DeleteHraMaster = "[ODS].[Usp_DeleteHraMaster]";
        #endregion

        #region TPTA Master
        public static string Usp_InsertUpdateTptaMaster = "[ODS].[Usp_InsertUpdateTptaMaster]";
        public static string usp_GetTptaMasterDetails = "[ODS].[usp_GetTptaMasterDetails]";
        public static string usp_EditTptaMasterDetails = "[ODS].[usp_EditTptaMasterDetails]";
        public static string usp_DeleteTptaMaster = "[Ods].[Usp_DeleteTptaMaster]";
        #endregion

        #region GpfAdvanceReason Master
        public static string usp_GetMainReasonType = "[ODS].[usp_GetMainReasonType]";
        public static string usp_GetGPFAdvanceReasonsDetails = "[ODS].usp_GetGPFAdvanceReasonsDetails";
        public static string usp_InsertUpdateGpfAdvanceReasons = "[ODS].usp_InsertUpdateGpfAdvanceReasons";
        public static string usp_DeleteGpfAdvanceReasons = "[ODS].usp_DeleteGpfAdvanceReasons";
        #endregion
        //#region GpfRulesRef Master
        public static string usp_InsertUpdateGpfRulesRef = "[ODS].[usp_InsertUpdateGpfRulesRef]";
        public static string usp_GetGpfAdvanceRulesByID = "[ODS].usp_GetGpfAdvanceRulesByID";
        public static string usp_GetGpfAdvanceRules = "[ODS].usp_GetGpfAdvanceRules";
        public static string usp_DeleteGpfAdvanceRules = "[ODS].usp_DeleteGpfAdvanceRules";
        //#endregion
        //#region GpfRecovery Ref Master
        public static string usp_InsertUpdateGpfRecoveryRules = "[ODS].usp_InsertUpdateGpfRecoveryRules";
        public static string usp_GetGpfRecoveryRulesByID = "[ODS].usp_GetGpfRecoveryRulesByID";
        public static string usp_GetGpfRecoveryRules = "[ODS].usp_GetGpfRecoveryRules";
        public static string usp_DeleteGpfRecoveryRules = "[ODS].usp_DeleteGpfRecoveryRules";
        //#endregion
        #region PAO Master
        public static string UspMsPAO = "[ODS].[usp_MsPAO]";
        #endregion
        #region DDO Master
        public static string SP_DDOType = "[ODS].[GetDDOTypeCategory]";
        public static string SP_GetAllState = "[ODS].[GetAllState]";
        public static string SP_GetCity = "[ODS].[GetAllCity]";
        public static string SP_MasterDDO = "[ODS].[usp_MsDDO]";
        #endregion

        #region CityClassmaster

        public static string SP_GetCityClassMasterList = "[ODS].[sp_GetCityClassMasterDetails]";
        public static string SP_DeleteCityClassMaster = "[dbo].[usp_DeleteCityClassmaster]";
        public static string SP_UpsertCityclassMaster = "[dbo].[Usp_SaveCityclassmaster]";
        public static string SP_CheckRecordExitsorNotinCityClassmaster = "[ODS].[CheckAlreadyExitsinCityMaster]";


        #endregion

        #region PsuMaster
        public static string usp_psuGetStateList = "[ODS].[usp_psuStateList]";
        public static string usp_psuGetCityList = "[ODS].[usp_psuDisttList]";
        public static string usp_psuMasterSaveDetails= "[usp_psuMasterSaveDetails]";
        public static string usp_psuMasterGetDetails = "[ODS].[usp_psuMasterGetDetails]";
        public static string usp_psuMasterDeleteDetails = "[ODS].[usp_psuMasterDeleteDetails]";

        #endregion

        #endregion

        #region DBConnectionsring
        public static string EPSDataBaseConnection = "EPSConnection";// ConfigurationManager.ConnectionStrings["EPSConnection"].ConnectionString;//
        #endregion

        #region User Management/role assign
        public static string SP_GetAllDDOsUnderSelectedPAO = "[ODS].[GetAllDDOsUnderSelectedPAOs_SP]";
        public static string SP_GetAllController = "[ODS].[GetAllController]";
        public static string SP_ControllerroleSelectchanged = "[ODS].[ControllerroleSelectchanged]";
        public static string SP_PaosAssignedByController = "[ODS].[PaosAssignedByController]";
        public static string SP_GetAllPAOs = "[ODS].[GetAllPAOs]";
        public static string SP_GetAllDDOsUnderSelectedPAOs = "[ODS].[GetAllDDOsUnderSelectedPAOs_SP]";
        public static string SP_PAOsAssigned = "[ODS].[PAOsAssigned]";
        public static string SP_DDOsEmployeeList = "[ODS].[DDOsEmployeeList]";
        public static string SP_DDOsAssigned = "[ODS].[DDOsAssigned]";
        public static string SP_GetEmp = "[ODS].[GetAllEmployeeUnderDDO]";
        public static string SP_GetHeadAccount = "[ODS].[SP_GetHeadAccount]";
        //----------------04/11/2019---------------------------
        public static string SP_AssignedEmp = "[ODS].[AssignedEmp]";
        public static string SP_AssignedDDO = "[ODS].[AssignedDDO]";
        public static string SP_AssignedEmpDDO = "[ODS].[AssignedEmpDDO]";
        public static string SP_EmployeeListOfHOO = "[ODS].[EmployeeListOfHOO]";
        public static string SP_PayRoleAssignCheckerHOO = "[ODS].[PayRoleAssignCheckerHOO]";
        public static string SP_SelfAssign = "[ODS].[SelfAssign]";
        public static string SP_SelfAssignChecked = "[ODS].[SelfAssignChecked]";
        public static string SP_AssignedDDOMakerEmployeeList = "[ODS].[AssignedDDOMakerEmployeeList]";
        public static string SP_EmployeeListOfDDO = "[ODS].[EmployeeListOfDDO]";
        public static string SP_PayRoleAssignCheckerMaker = "[ODS].[PayRoleAssignCheckerMaker]";
        public static string SP_AssignedEmpPAO = "[ODS].[AssignedEmpPAO]";
        public static string SP_GetAllUserRolesByDDO = "[ODS].[GetAllUserRolesByDDO]";
        public static string SP_AssignChecker = "[ODS].[AssignChecker_SP]";
        public static string SP_AssignedDDOEmployeeList = "[ODS].[AssignedDDOEmployeeList]";
        #endregion

        #region User Profile
        public static string GetUserprofileDetails_SP = "[ODS].[Usp_GetUserdetailsProfile]";
        public static string Usp_GetUserdetailsProfile = "ODS.Usp_GetUserdetailsProfile";

        #endregion

        #region LoanManagement

        public static string SP_GetRecoveryChallan = "[ODS].[SP_GetRecoveryChallan]";
        public static string SP_GetEmployeeDetails = "[dbo].[SP_GetEmployeeDetails]";
        public static string SP_InsertUpdatePropOwnerDetlsMaster = "[ODS].[Usp_InsertUpdatePropOwnerDetlsMaster]";
        public static string SP_EditTptaMasterDetails = "[ODS].[usp_EditTptaMasterDetails]";
        public static string sp_GetAlreadyTakenLoanAdvanceDetails = "[dbo].[GetAlreadyTakenLoanAdvanceDetails]";
        public static string SP_GetExistLoanByID = "[ODS].[SP_GetExistLoanByID]";
        public static string SP_GetExistLoanStatus = "[ODS].[SP_GetExistLoanStatus]";
        public static string SP_InsertExistingLoanApplicationDetails = "[dbo].[SP_InsertExistingLoanApplicationDetails]";
        public static string SP_GetEmpNamebyDesignId = "[ODS].[SP_GetEmpNamebyDesignId]";
        public static string SP_GetEmpLoanTypeandPurpos = "[ODS].[SP_GetLoanTypeWithPurpose]";
        public static string SP_GetEmpLoanStatus = "[ODS].[SP_GetEmpLoanStatus]";
        public static string SP_GetForwardtocheckerLoanDetails = "[ODS].[SP_GetForwardtocheckerLoanDetails]";
        public static string SP_ExistingLoanForwordToDDO = "[ODS].[Sp_ExistingLoanChangeStatus]";
        public static string SP_UpDateLoanDetails = "[dbo].[SP_UpDateLoanDetails]";
        public static string SP_InsertNewLoanApplicationDetails = "[dbo].[SP_InsertNewLoanApplicationDetails]";
        public static string SP_UpdateLoanApplicationDetails = "[dbo].[SP_updateNewLoanApplicationDetails]";
        public static string SP_GetEmpLoanListDetails = "[dbo].[SP_GetEmpLoanListDetails]";
        public static string SP_GetLoanByID = "[ODS].[SP_GetLoanByID]";
        public static string SP_EditLoanDetailsByEMPID = "[ODS].[SP_EditLoanDetailsByEMPID]";
        public static string SP_DeletetLoanDetailsByEMPID = "[ODS].[SP_DeletetLoanDetailsByEMPID]";
        public static string SP_UpdateLoanDetailsbyID = "[ODS].[SP_UpdateLoanDetailsbyID]";
        public static string SP_getEmpCode = "[ODS].[getEmpCode]";
        public static string UPS_GetSanctionDetails = "[ODS].[UPS_GetSanctionDetails]";

        public static string UPS_UpdateSanctionDetails = "[ODS].[UPS_UpdateSanctionDetails]";
        public static string SP_GetPayBillEmployee = "[ODS].[SP_GetPayBillEmployee]";
        public static string SP_InsertFloodData = "[ODS].[SP_InsertFloodData]";
        public static string SP_FloodForwordToDdo = "[ODS].[FloodForwordToDdo]";
        public static string SP_GetBillgroupfilterbydesignID = "[ODS].[SP_GetBillgroupfilterbydesignID]";
        public static string SP_GetEmpfilterbydesignBillgrID = "[ODS].[SP_GetEmpfilterbydesignBillgrID]";
        public static string GetEmpfilterbydesignBillID = "[ODS].[SP_GetEmpfilterbydesignBillID]";
        public static string sp_GetRecovEmpDetails = "[dbo].[GetRecovEmpDetails]";
        public static string sp_EmpSanctionDetails = "[dbo].[GetEmpSanctionDetails]";
        public static string sp_SaveInstRecovDetails = "[dbo].[SaveInstRecovDetails]";
        public static string sp_GetInstEmpDetails = "[dbo].[GetInstEmpDetails]";
        public static string Sp_InstRecovChangeStatus = "[ODS].[Sp_InstRecovChangeStatus]";
        public static string Sp_InsertUpdateRecoveryChallan = "[ODS].[usp_InsertUpdateRecoveryChallan]";
        public static string Sp_UpdateDeleteInstRecord = "[dbo].[UpdateDeleteInstRecord]";
        public static string Usp_ValidateCoolingPeriod = "[ODS].[Usp_ValidateCoolingPeriod]";


        //public static string SP_updateNewLoanApplicationDetails = "[ODS].[SP_updateNewLoanApplicationDetails]";
        #endregion

        #region PraoMaster
        public static string SP_GetControllerCD = "[ODS].[SP_GetControllerCD]";
        public static string SP_InsertUpdatePraoMaster = "[dbo].[SP_InsertUpdatePraoMaster]";
        public static string SP_GetPraoMasterDetails = "[dbo].[SP_GetPraoMasterDetails]";
        public static string SP_UpdateDeletePraoDetails = "[dbo].[SP_UpdateDeletePraoDetails]";

        #endregion

        #region ExceptionLog
        public static string SP_Exception_NLog = "[dbo].[SP_Exception_NLog]";
        public static string SP_NLog_AddEntry = "[dbo].[SP_NLog_AddEntry]";
        #endregion

        #region PayBillGroup

        public static string SP_GetAccountHeads = "[ODS].[GetAccountHeads]";
        public static string SP_InsertUpdatePayBillGroup = "[ODS].[USP_InsertUpdatePayBillGroup]";
        public static string SP_GetPayBillGroupDetailByBillGroupId = "[ODS].[GetDetailsofBillGroupByid]";
        public static string SP_GetPayBillGroupEmpAttach = "[ODS].[GetEmployeeAttachandDeatch]";
        public static string SP_GetPayBillGroupEmpDeAttach = "[ODS].[GetEmployeeDeatch]";
        public static string SP_UpdatePayBillGroupByEmpCode = "[ODS].[UpdatePayBillgroupbyEmpCode]";
        public static string SP_UpdatePayBillNullGroupByEmpCode = "[ODS].[UpdatePayBillNullgroupbyEmpCode]";

        public static string SP_GetAccountHeadOT1 = "[ODS].[GetAccountHeadOT1]";
        public static string SP_GetAccountHeadOT1AttachList = "[ODS].[GetAccountHeadOT1ByDDOCode]";
        public static string SP_UpdateAccountHeadOT1List = "[ODS].[UpdateAccountHeadOT1List]";
        public static string SP_GetActDeAccountHeadOT1ByDDOCode = "[ODS].[GetActDeAccountHeadOT1ByDDOCode]";
        public static string SP_UpdateActDeAccountHeadOT1 = "[ODS].[UpdateActDeAccountHeadOT1]";

        public static string SP_GetNgRecovery = "[ODS].[SP_GetNgRecovery]";
        public static string SP_GetBankdetailsByIfsc = "[ODS].[USP_GetBankDetailsByIfscCode]";
        public static string SP_InsertUpdateNgRecovery = "[ODS].[USP_InsertUpdateNgRecovery]";
        public static string SP_ActiveDeactiveNgRecovery = "[ODS].[USP_ActiveDeactiveNgRecovery]";
        public static string SP_GetFinancialYears = "[ODS].[USP_GetFinancialYears]";
        public static string SP_GetAllMonth = "[ODS].[USP_GetAllMonths]";
        public static string SP_GetNgRecoveryDetailsByEmployee = "[ODS].[SP_GetNgRecoveryDetailsByEmployee]";
        public static string SP_GetEmployeeByPaybillAndDesig = "[ODS].[SP_GetEmployeeByPaybillAndDesig]";
        public static string SP_InsertNgRecoveryEntries = "[ODS].[USP_InsertNgRecoveryEntries]";

        public static string SP_GetPayBillGroupByFinancialYear = "[ODS].[GetPayBillGroupByFinancialYear]";
        public static string SP_GetEmployeesByPayItemCode = "[ODS].[GetEmployeesByPayItemCode]";
        public static string SP_GetPayItemsFromDesignationCode = "[ODS].[GetPayItemsFromDesignationCode]";
        public static string SP_UpdateEmpNonComputationalDuesDeductAmount = "[ODS].[UpdateEmpNonComputationalDuesDeductAmount]";
        public static string SP_GetNgRecoveryList = "[ODS].[GetNgRecoveryList]";
        public static string SP_GetAllNgRecoveryandPaybillGrpDetails = "[ODS].[GetAllNgRecoveryandPaybillGrpDetails]";
        #endregion

        #region Leaves
        public static string SP_GetLeaveOrderByEmp = "[ODS].[GetLeaveOrderByEmp]";
        public static string SP_GeDesigByPermID = "[ODS].[GetDesigByPermDDOId]";
        public static string SP_GetLeaves = "[ODS].[GetLeaves]";
        public static string SP_GetLeavesType = "[ODS].[GetLeavesType]";
        public static string SP_GetLeavesReason = "[ODS].[GetLeavesReason]";
        public static string SP_SaveLeavesSanction = "[ODS].[SaveLeavesSanction]";
        public static string SP_DeleteLeaves = "[ODS].[DeleteLeaves]";
        public static string SP_ForwardToHOOCheckerSanction = "[ODS].[UpdateStatusLeaveSanction]";
        public static string SP_SaveLeavesCurtailExtend = "[ODS].[SaveLeavesCurtExten]";
        public static string SP_SaveLeavesCancel = "[ODS].[SaveLeavesCancel]";
        public static string SP_GetSanctionedLeave = "[ODS].[GetSanctionedLeave]";
        public static string SP_GetCurtExtenLeave = "[ODS].[GetCurtExtenLeave]";
        public static string SP_GetCanceledLeave = "[ODS].[GetCanceledLeave]";
        public static string SP_GetLeaveBalance = "[ODS].[GetLeavesTypeBalance]";
        public static string SP_SaveTotalLeaveAvailed = "[ODS].[SaveTotalLeaveAvailed]";
        
        #endregion

        #region Employee Data capture

        public static string SP_MakerGetEmpList = "[ODS].[usp_MakerGetEmpList]";
        public static string SP_JoiningCategory = "[ODS].[usp_GetJoiningCategory]";
        public static string Usp_GetEmpByID = "[ODS].[usp_GetEmpByID]";
        public static string SP_UpdateEmpDetails = "[ODS].[Usp_UpdateEmpDetails]";
        public static string SP_ServiceType = "[ODS].[usp_GetServiceType]";
        public static string SP_GenderType = "[ODS].[usp_GetGender]";
        public static string SP_InsertUpdateEmployee = "[ODS].[usp_InsertEmpDetails]";
        public static string SP_SavePHDetails = "[ODS].[usp_SavePHDetails]";
        public static string SP_GetEmpPHDetails = "[ODS].[usp_GetEmpPHDetails]";
        public static string SP_GetState = "[ODS].[usp_GetState]";
        public static string SP_DeleteEmpDetails = "[ODS].[usp_DeleteEmpDetails]";
        public static string SP_GetMaritalStatus = "[ODS].[usp_GetMaritalStatus]";
        public static string SP_getFamilyDetails = "[ODS].[usp_getFamilyDetails]";
        public static string SP_DeleteFamilyDetails = "[ODS].[usp_DeleteFamilyDetails]";
        public static string SP_UpdateEmpFamilyDetails = "[ODS].[usp_UpdateEmpFamilyDetails]";
        public static string SP_getAllFamilyDetails = "[ODS].[usp_getAllFamilyDetails]";
        public static string SP_DeleteEmployeeDetails = "[ODS].[usp_DeleteEmpDetails]";
        public static string SP_getAllNomineeDetails = "[ODS].[usp_getAllNomineeDetails]";
        public static string SP_getNomineeDetails = "[ODS].[usp_GetNomineeDetails]";
        public static string SP_UpdateNomineeDetails = "[ODS].[usp_UpdateNomineeDetails]";
        public static string SP_DeleteNomineeDetails = "[ODS].[usp_DeleteNomineeDetails]";
        public static string SP_getBankDetailsByIFSC = "[ODS].[usp_getBankDetailsByIFSC]";
        public static string SP_getAllIFSC = "[dbo].[GetAllIFSCCode]";
        public static string SP_getBankDetails = "[ODS].[usp_getBankDetails]";
        public static string SP_UpdateBankDetails = "[ODS].[usp_InsertUpdateEmpBankDetails]";
        public static string SP_getHRACity = "[ODS].[usp_GetHraCityClass]";
        public static string SP_getTACity = "[ODS].[usp_GetTACityClass]";
        public static string SP_getAllDesignation = "[ODS].[USP_getAllDesignation]";
        public static string SP_SaveUpdatePostingDetails = "[ODS].[USP_SaveUpdatePostingDetails]";
        public static string SP_GetAllPostingDetails = "[ODS].[USP_GetAllPostingDetails]";
        public static string SP_getMaintainByOfc = "[ODS].[USP_getMaintainByOfc]";
        public static string SP_getServiceDetails = "[ODS].[USP_getServiceDetails]";
        public static string SP_SaveUpdateServiceDetails = "[ODS].[USP_SaveUpdateServiceDetails]";
        public static string SP_GetIsDeputEmp = "[ODS].[USP_GetIsDeputEmp]";
        public static string SP_getAllEmpDetails = "[ODS].[USP_getAllEmpDetails]";
        public static string SP_getAllEmployeeCompleteDetails = "[ODS].[usp_getAllEmployeeCompleteDetails]";
        public static string SP_ForwardToCheckerEmpDtls = "[ODS].[USP_ForwardToCheckerEmpDtls]";
        public static string SP_GetCGHSDetails = "[ODS].[usp_getCGHSDetails]";
        public static string SP_SaveUpdateCGHSDetails = "[ODS].[usp_SaveUpdateCGHSDetails]";
        public static string SP_GetInsuranceApplicable = "[ODS].[USP_getInsuranceApplicable]";
        public static string SP_GetCGEGISCategory = "[ODS].[USP_getDeputationCaseType]";
        public static string SP_SaveUpdateCGEGISDetails = "[ODS].[usp_Insert_UpdateCGEGISDetails]";
        public static string SP_getCGEGISDetails = "[ODS].[USP_getCGEGISDetails]";
        public static string SP_VerifyEmpData = "[ODS].[USP_VerifyEmpData]";

        public static string SP_GetOrganisationType = "[ODS].[USP_getOrganisationType]";
        public static string SP_GetPayLevel = "[ODS].[USP_getPayLevel]";
        public static string SP_GetEntitledOffVeh = "[ODS].[USP_getEntitledOffVeh]";
        public static string SP_GetPayIndex = "[ODS].[USP_getPayIndex]";
        public static string SP_GetBasicDetails = "[ODS].[USP_getBasicDetails]";
        public static string SP_GetGradePay = "[ODS].[USP_getGradePay]";
        public static string SP_GetPayScale = "[ODS].[getPayScale]";
        public static string SP_GetNonComputationalDues = "[ODS].[USP_NonComputationalDues]";
        public static string SP_GetNonComputationalDeductions = "[ODS].[USP_NonComputationalDeductions]";
        public static string SP_GetPayDetails = "[ODS].[USP_getPayDetails]";
        public static string SP_GetEmpSaveDuesDetails = "[ODS].[USP_getEmpSaveDuesDetails]";
        public static string SP_GetEmpSavedDeductionDetails = "[ODS].[USP_getEmpSavedDeductionDetails]";
        public static string USP_SaveUpdatePayDetails = "[ODS].[USP_SaveUpdatePayDetails]";


        public static string SP_GetQuarterOwnedby = "[ODS].[usp_getQuarterOwnedby]";
        public static string SP_getAllottedTo = "[ODS].[usp_getAllottedTo]";
        public static string SP_GetRentStatus = "[ODS].[usp_getRentStatus]";
        public static string SP_GetQuarterType = "[ODS].[usp_getQuarterType]";
        public static string SP_GetCustodian = "[ODS].[usp_GetCustodian]";
        public static string SP_GetGPRACityLocation = "[ODS].[usp_GetGPRACityLocation]";
        public static string SP_QuarterAllDetails = "[ODS].[usp_getQuarterAllDetails]";
        public static string SP_QuarterDetails = "[ODS].[usp_getQuarterDetails]";
        public static string SP_SaveUpdateQuarterDetails = "[ODS].[usp_SaveUpdateQuarterDetails]";
        public static string SP_DeleteQuarterDetails = "[ODS].[usp_DeleteQuarterDetails]";
        public static string SP_GetVerifyEmpCode = "[ODS].[usp_GetVerifyEmpCode]";
        public static string SP_GetPhysicalDisabilityTypes = "[ODS].[usp_GetPhysicalDisabilityTypes]";
        public static string SP_VerifyRejectionEmployeeDtl = "[ODS].[usp_Verify&RejectionEmployeeDtl]";

        public static string SP_GetRelation = "[ODS].[usp_GetRelation]";
        public static string SP_GetJoiningMode = "[ODS].[usp_GetJoiningMode]";
        public static string SP_GetSalutation = "[ODS].[usp_GetSalutation]";
        public static string SP_GetEmpType = "[ODS].[usp_GetEmpType]";
        public static string SP_GetEmpSubType = "[ODS].[usp_GetEmpSubType]";
        public static string SP_GetDeputationType = "[ODS].[usp_GetDeputationType]";
        public static string SP_GetPfType = "[ODS].[lienPeridJoiningAccountOf]";
        public static string SP_GetLienPeridJoiningAccountOf = "[ODS].[usp_GetDeputationType]";
        public static string SP_GetLienGetOfficeCityClass = "[ODS].[LienGetOfficeCityClass]";
        public static string SP_LienGetAllOfficelist = "[ODS].[LienGetAllOfficelist]";
        public static string SP_GetPayCommisionlien = "[ODS].[USP_GetPayCommisionlien]";

        #endregion

        #region End of Service
        public static string GetReasonEndofService = "[ODS].[GetReasonEndofService]";
        public static string GetEndofServiceEmployee = "[ODS].[SP_GetEmpNamebyDesignId_EndofService]";
        public static string GetCurrentEndofServiceEmployee = "[ODS].[SP_GetCurrentEmpEndofserviceDetails]";
        public static string SP_GetEndofServiceEmployeesList = "[ODS].[SP_GetEndofServiceEmployeesList]";
        public static string SP_DeleteEndofServiceEmployee = "[ODS].[SP_DeleteEndofServiceEmployee]";
        public static string SP_UpdateEndofServiceEmployeeStatus = "[ODS].[SP_UpdateEndofServiceEmployeeStatus]";
        public static string SP_InsertEndofServiceEmployee = "[ODS].[SP_InsertEndofServiceEmployee]";
        #endregion

        #region Suspension

        public static string sus_GetAllDesignation = "[ODS].[usp_susGetAllDesignation]";
        public static string sus_GetAllEmployees = "[ODS].[usp_susGetAllEmployees]";
        public static string sus_SavedNewSuspensionDetails = "[ODS].[usp_susSavedNewSuspensionDetails]";
        public static string sus_GetSuspensionDetails = "[ODS].[usp_susGetSuspensionDetails]";
        public static string sus_DeleteSuspensionDetails = "[ODS].[usp_susDeleteSusDetails]";
        public static string sus_VerifiedAndRejectedSuspension = "[ODS].[usp_susVerifiedAndRejectedSuspension]";
        public static string sus_SavedSusExtDetails = "[ODS].[usp_susSavedSusExtDetails]";
        public static string sus_SavedRevocDetails = "[ODS].[usp_susSavedRevocDetails]";
        public static string sus_GetRevocationDetails = "[ODS].[usp_susGetRevocationDetails]";
        public static string sus_SavedJoiningDetails = "[ODS].[usp_susSavedJoiningDetails]";
        public static string sus_GetJoiningDetails = "[ODS].[usp_susGetJoiningDetails]";
        public static string sus_SavedRegulariseDetails = "[ODS].[usp_susSavedRegulariseDetails]";
        public static string sus_GetRegulariseDetails = "[ODS].[usp_susGetRegulariseDetails]";

        #endregion

        #region Promotion/Reversion
        public static string SP_UpsertPromotionDetailsWithoutTransfer = "[dbo].[SP_UpsertEmployeePromotionDetailsWithoutTransfer]";
        public static string SP_GetPromotionDetailsWithoutTransfer = "[dbo].[SP_GetEmployeePromotionDetailsWithoutTransfer]";
        public static string SP_DeletePromotionDetails = "[dbo].[SP_DeletePromotionDetails]";
        public static string SP_UpsertReversionDetailsWithoutTransfer = "[dbo].[SP_UpsertEmployeeReversionDetailsWithoutTransfer]";
        public static string SP_GetReversionDetailsWithoutTransfer = "[dbo].[SP_GetEmployeeReversionDetailsWithoutTransfer]";
        public static string SP_DeleteReversionDetails = "[dbo].[SP_DeleteReversionDetails]";
        public static string SP_ForwardTransferDetailsToChecker = "[dbo].[SP_ForwardTransferDetailsToChecker]";

        #endregion

        #region Shared Stored Procedure
        public static string SP_GetPayBillGroup = "[ODS].[SP_GetPayBillGroup]";
        public static string SP_GetPayBillGroupByPermDDOId = "[ODS].[SP_GetPayBillGroupByPermDDOId]";
        public static string SP_GetDesignation = "[ODS].[SP_GetDesignation]";
        public static string SP_GetDesignationByBillgrID = "[ODS].[SP_GetDesignationByBillgrID]";
        public static string SP_GetEmployee = "[ODS].[SP_GetEmployee]";
        public static string SP_GetEmployeeWithDesignationDetails = "[ODS].[SP_GetEmployeesWithDesignationDetails]";
        #endregion

        #region Recovery
        public static string SP_InsertTempPermStop = "[ODS].InsertTempPermStop_SP";
        #endregion

        #region Deputation
        public static string SP_getPayItems = "[ODS].[usp_GetPayItems]";
        public static string SP_GetDesignationAll = "[ODS].[USP_GetEmployeeDesignation]";
        public static string SP_GetCommunicationAddress = "[ODS].[USP_GetDeputation_CommunicationAdddress]";
        public static string SP_GetSchemeCodeByMsEmpDuesID = "[ODS].[USP_GetSchemeCodeByMsEmpDuesID]";
        public static string SP_GetDeputationPayitemsDeduction = "[ODS].[USP_GetDeputationPayitemsDeduction]";
        public static string SP_GetDepuatationInDetails = "[ODS].[USP_GetDeputationsDetails]";
        public static string SP_InsertUpdateDeputationIn = "[ODS].[usp_InsertUpdateDeputationIn]";
        public static string SP_DepuUpdateforwardStatusUpdateById = "[ODS].[USP_TDepuUpdateforwardStatusUpdateById]";
        public static string SP_GetDeputationsOutDetailsByEmpcode = "[ODS].[USP_GetDeputationsOutDetailsByEmployeeID]";
        public static string SP_InsertUpdateDeputationOutDetails = "[ODS].[usp_InsertUpdateDeputationOutDetails]";
        public static string SP_GetRepatriationDetailsByEmpcode = "[ODS].[USP_RepatriationDetailsByEmployeeID]";
        public static string SP_CheckDepuatationinEmployee = "[ODS].CheckDepuatationinEmployee";
        public static string SP_InsertUpdateRepatriationDetails = "[ODS].[usp_InsertUpdateRepatriationDetails]";
        #endregion

        #region Lien Period
        public static string SP_LienGetControllerList = "[ODS].[LienGetControllerList]";
        public static string SP_LienGetDdoListbyControllerID = "[ODS].[LienGetDdoListbyControllerID]";
        public static string SP_LienGetOfficeDetails = "[ODS].[LienGetOfficeDetails]";
        public static string Sp_GetTransfer_office_lien_Period_DetailsByEmpid = "[ODS].[USP_GetTranofflienPeriodByEmpid]";
        public static string sp_InsertUpdateTransfer_office_lien_Period = "[ODS].[usp_InsertUpdateTransfer_office_lien_Period]";
        public static string Sp_DeletetransferOfficeLienperiod = "[ODS].[usp_DeletetransferOfficeLienperiod]";
        public static string Sp_LienUpdateforwardStatusUpdateByEmpCode = "[ODS].[USP_LienUpdateforwardStatusUpdateByEmpCode]";
        public static string Sp_GetjoinNonEpsEmployeeLienPeriod = "[ODS].[USP_GetjoinNonEpsEmployeeLienPeriod]";
        public static string Sp_InsertUpdateNONEPSEmployeeJoinafterlien_Period = "[ODS].[usp_InsertUpdateNONEPSEmployeeJoinafterlien_Period]";
        public static string Sp_deleteNONEPSEmployeeJoinafterlien_Period = "[ODS].[usp_deleteNONEPSEmployeeJoinafterlien_Period]";
        public static string SP_NONEpsEmpjoinLienUpdateforwardStatus = "[ODS].[USP_NONEpsEmpjoinLienUpdateforwardStatus]";
        public static string Sp_GetjoinEpsEmployeeLienPeriod = "[ODS].[USP_GetjoinEpsEmployeeLienPeriod]";
        public static string Sp_InsertUpdateEPSEmployeeJoinafterlien_Period = "[ODS].[usp_InsertUpdateEPSEmployeeJoinafterlien_Period]";
        public static string Sp_deleteEPSEmployeeJoinafterlien_Period = "[ODS].[usp_deleteEPSEmployeeJoinafterlien_Period]";
        public static string Sp_EpsEmpjoinAfLienUpdateforwardStatusUpda = "[ODS].[EpsEmpjoinAfLienUpdateforwardStatusUpda]";
        #endregion

        #region Change

        //Common
        public static string SP_Change_CommonDelete = "[ODS].[Change_CommonDelete]";
        public static string SP_Getddlchangemodule = "[ODS].[Change_CommonDdl]";

        //ExtensionOfService
        public static string SP_InsertExtensionOfServiceDetails = "[ODS].[Change_InsertExtensionOfService]";
        public static string SP_GetExtensionOfServiceDetails = "[ODS].[Change_GetExtensionOfServiceDetails]";
        //public static string SP_GetExtensionOfServiceSearch = "[ODS].[GetExtensionOfServiceSearch]";
        public static string SP_UpdateExtensionOfServiceFwdToChkr = "[ODS].[Change_UpdateExtensionOfServiceFwdToChkr]";

        //dob
        public static string SP_Change_InsertDOB = "[ODS].[Change_InsertDOB]";
        public static string SP_Change_GetDOBDetails = "[ODS].[Change_GetDOBDetails]";
        public static string SP_Change_UpdateDOBFwdToChkr = "[ODS].[Change_UpdateDOBFwdToChkr]";

        //NameGender
        public static string SP_Change_InsertNameGender = "[ODS].[Change_InsertNameGender]";
        public static string SP_Change_GetNameGenderDetails = "[ODS].[Change_GetNameGenderDetails]";
        public static string SP_Change_UpdateNameGenderFwdToChkr = "[ODS].[Change_UpdateNameGenderFwdToChkr]";

        //pfnps
        public static string SP_Change_InsertPfNps = "[ODS].[Change_InsertPfNps]";
        public static string SP_Change_GetPfNpsDetails = "[ODS].[Change_GetPfNpsDetails]";
        public static string SP_Change_UpdatePfNpsFwdToChkr = "[ODS].[Change_UpdatePfNpsFwdToChkr]";

        //pan no.
        public static string SP_Change_InsertPanNo = "[ODS].[Change_InsertPanNo]";
        public static string SP_Change_GetPanNoDetails = "[ODS].[Change_GetPanNoDetails]";
        public static string SP_Change_UpdatePanNoFwdToChkr = "[ODS].[Change_UpdatePanNoFwdToChkr]";
        //public static string SP_Change_GetPanNoSearchResults = "[ODS].[Change_GetPanNoSearchResults]";

        //hra
        public static string SP_Change_InsertHRA = "[ODS].[Change_InsertHRA]";
        public static string SP_Change_GetHraDetails = "[ODS].[Change_GetHraDetails]";
        //public static string SP_Change_GetHraSearchResults = "[ODS].[Change_GetHraSearchResults]";
        public static string SP_Change_UpdateHraFwdToChkr = "[ODS].[Change_UpdateHraFwdToChkr]";

        //Designation
        public static string SP_Change_InsertDesignation = "[ODS].[Change_InsertDesignation]";
        public static string SP_Change_GetDesignationDetails = "[ODS].[Change_GetDesignationDetails]";
        public static string SP_Change_UpdateDesignationFwdToChkr = "[ODS].[Change_UpdateDesignationFwdToChkr]";


        //Exservicemen
        public static string SP_Change_InsertExservicemen = "[ODS].[Change_InsertExservicemen]";
        public static string SP_Change_GetExservicemenDetails = "[ODS].[Change_GetExservicemenDetails]";
        public static string SP_Change_UpdateExservicemenFwdToChkr = "[ODS].[Change_UpdateExservicemenFwdToChkr]";

        //CGHS
        public static string SP_Change_InsertCGHS = "[ODS].[Change_InsertCGHS]";
        public static string SP_Change_GetCGHSDetails = "[ODS].[Change_GetCGHSDetails]";
        public static string SP_Change_UpdateCGHSFwdToChkr = "[ODS].[Change_UpdateCGHSFwdToChkr]";

        //CGEGIS
        public static string SP_Change_InsertCGEGIS = "[ODS].[Change_InsertCGEGIS]";
        public static string SP_Change_GetCGEGISDetails = "[ODS].[Change_GetCGEGISDetails]";
        public static string SP_Change_UpdateCGEGISFwdToChkr = "[ODS].[Change_UpdateCGEGISFwdToChkr]";

        //JoiningMode
        public static string SP_Change_InsertJoiningMode = "[ODS].[Change_InsertJoiningMode]";
        public static string SP_Change_GetJoiningModeDetails = "[ODS].[Change_GetJoiningModeDetails]";
        public static string SP_Change_UpdateJoiningModeFwdToChkr = "[ODS].[Change_UpdateJoiningModeFwdToChkr]";
        //DOJ
        public static string SP_Change_InsertDOJ = "[ODS].[Change_InsertDOJ]";
        public static string SP_Change_GetDOJDetails = "[ODS].[Change_GetDOJDetails]";
        public static string SP_Change_UpdateDOJFwdToChkr = "[ODS].[Change_UpdateDOJFwdToChkr]";

        //DOE
        public static string SP_Change_InsertDOE = "[ODS].[Change_InsertDOE]";
        public static string SP_Change_GetDOEDetails = "[ODS].[Change_GetDOEDetails]";
        public static string SP_Change_UpdateDOEFwdToChkr = "[ODS].[Change_UpdateDOEFwdToChkr]";

        //DOR
        public static string SP_Change_InsertDOR = "[ODS].[Change_InsertDOR]";
        public static string SP_Change_GetDORDetails = "[ODS].[Change_GetDORDetails]";
        public static string SP_Change_UpdateDORFwdToChkr = "[ODS].[Change_UpdateDORFwdToChkr]";

        //CasteCategory
        public static string SP_Change_InsertCasteCategory = "[ODS].[Change_InsertCasteCategory]";
        public static string SP_Change_GetCasteCategoryDetails = "[ODS].[Change_GetCasteCategoryDetails]";
        public static string SP_Change_UpdateCasteCategoryFwdToChkr = "[ODS].[Change_UpdateCasteCategoryFwdToChkr]";

        //Entofficevehicle
        public static string SP_Change_InsertEntofficevehicle = "[ODS].[Change_InsertEntofficevehicle]";
        public static string SP_Change_GetEntofficevehicleDetails = "[ODS].[Change_GetEntofficevehicleDetails]";
        public static string SP_Change_UpdateEntofficevehicleFwdToChkr = "[ODS].[Change_UpdateEntofficevehicleFwdToChkr]";
        
        //Bank
        public static string SP_Change_InsertBank = "[ODS].[Change_InsertBank]";
        public static string SP_Change_GetBankDetails = "[ODS].[Change_GetBankDetails]";
        public static string SP_Change_UpdateBankFwdToChkr = "[ODS].[Change_UpdateBankFwdToChkr]";

        //addtapd
        public static string SP_Change_Insertaddtapd = "[ODS].[Change_Insertaddtapd]";
        public static string SP_Change_GetaddtapdDetails = "[ODS].[Change_GetaddtapdDetails]";
        public static string SP_Change_UpdateaddtapdFwdToChkr = "[ODS].[Change_UpdateaddtapdFwdToChkr]";

        //TPTA
        public static string SP_Change_InsertTPTA = "[ODS].[Change_InsertTPTA]";
        public static string SP_Change_GetTPTADetails = "[ODS].[Change_GetTPTADetails]";
        public static string SP_Change_UpdateTPTAFwdToChkr = "[ODS].[Change_UpdateTPTAFwdToChkr]";

        //StateGIS
        public static string SP_Change_InsertStateGIS = "[ODS].[Change_InsertStateGIS]";
        public static string SP_Change_GetStateGISDetails = "[ODS].[Change_GetStateGISDetails]";
        public static string SP_Change_UpdateStateGISFwdToChkr = "[ODS].[Change_UpdateStateGISFwdToChkr]";

        //MobileNo_Email_EmpCode

        public static string SP_Change_InsertMobileNo_Email_EmpCode = "[ODS].[Change_InsertMobileNo_Email_EmpCode]";
        public static string SP_Change_GetMobileNo_Email_EmpCodeDetails = "[ODS].[Change_GetMobileNo_Email_EmpCodeDetails]";
        public static string SP_Change_UpdateMobileNo_Email_EmpCodeFwdToChkr = "[ODS].[Change_UpdateMobileNo_Email_EmpCodeFwdToChkr]";

        #endregion

        #region Exemption Deduction & Rebate
        public static string Usp_GetMajorSectionCode = "[dbo].[Usp_GetMajorSectionCode]";
        public static string Usp_GetSectionCode = "[dbo].[Usp_GetSectionCode]";
        public static string Usp_GetSubSectionCode = "[dbo].[Usp_GetSubSectionCode]";
        public static string Usp_GetSectionCodeDescription = "[dbo].[Usp_GetSectionCodeDescription]";
        public static string Usp_GetSlabLimit = "[dbo].[Usp_GetSlabLimit]";
        public static string Usp_GetPayCommission = "[dbo].[Usp_GetPayCommission]";
        public static string Usp_GetExemptionDeductionRebateDetails = "[dbo].[Usp_GetExemptionDeductionRebateDetails]";
        public static string Usp_UpsertExemptionDeductionRebate = "[dbo].[Usp_UpsertExemptionDeductionRebate]";
        public static string Usp_DeleteExemptionDeductionRebate = "[dbo].[Usp_DeleteExemptionDeductionRebate]";
        #endregion

        #region StandardDeduction

        public static string SP_GetStandardDeductionRule = "[ODS].[GetStandardDeductionRule]";
        public static string SP_UpsertStandardDeductionRule = "[ODS].[UpsertStandardDeductionRule]";
        public static string SP_GetMsITax_SDeductionEntertain = "[ODS].[GetMsITax_SDeductionEntertain]";
        public static string SP_DeleteStandardDeductionRule = "[ODS].[DeleteStandardDeductionRule]";
        public static string SP_DeleteStandardDeductionRateDetail = "[ODS].[DeleteStandardDeductionRateDetail]";

        #endregion

        #region ITRates

        public static string SP_GetITRates = "[ODS].[GetItSurchargeRates]";
        public static string SP_UpsertITRates = "[ODS].[UpsertItSurchargeRates]";
        public static string SP_DeleteITRates = "[ODS].[DeleteItSurchargeRates]";
        public static string SP_GetRateTypeAndRateFor = "USP_GetAllRateTypeANDRateFor";
        #endregion

        #region Other Dues
        public static string Usp_GetMajorSectionCodeForDues = "[dbo].[Usp_GetMajorSectionCodeForDues]";
        public static string Usp_GetOtherDues = "[dbo].[Usp_GetOtherDues]";
        public static string Usp_GetOtherDuesDetails = "[dbo].[Usp_GetOtherDuesDetails]";
        public static string Usp_SaveOtherDuesDetails = "[dbo].[Usp_SaveOtherDuesDetails]";
        public static string Usp_DeleteOtherDuesDetails = "[dbo].[Usp_DeleteOtherDuesDetails]";
        #endregion

        #region ReJoiningService

        public static string SP_SearchEndOfServiceEmployees = "[ODS].[SearchEndOfServiceEmployees]";
        public static string SP_GetRejoiningOfEmployeesList = "[ODS].[GetRejoiningOfEmployeesList]";
        public static string SP_DeleteRejoiningOfEmployee = "[ODS].[DeleteRejoiningOfEmployee]";
        public static string SP_UpdateRejoiningEmployeeStatus = "[ODS].[UpdateRejoiningEmployeeStatus]";
        public static string SP_UpsertRejoiningEmployeeData = "[ODS].[UpsertRejoiningEmployeeData]";

        #endregion

        #region Increment Module

        public static string Usp_InsertUpdateAdvIncrement = "[ODS].[Usp_InsertUpdateAdvIncrement]";
        public static string Usp_GetAdvIncrementDetails = "[ODS].[Usp_GetAdvIncrementDetails]";
        public static string Usp_GetEmpNamebyDesignIdPayComm = "[ODS].[SP_GetEmpNamebyDesignIdPayComm]";
        public static string usp_EditAdvIncrementDetails = "[ODS].[usp_EditAdvIncrementDetails]";
        public static string Usp_DeleteAdvIncrement = "[ODS].[Usp_DeleteAdvIncrement]";
        public static string Usp_ForwardtoChkAdvIncrement = "[ODS].[Usp_ForwardtoChkAdvIncrement]";
        public static string Usp_GetSalaryMonth = "[ODS].[Usp_GetSalaryMonth]";
        public static string Usp_GetOrderWithEmployeeReport = "[ODS].[Usp_GetOrderWithEmployeeReport]";
        public static string Usp_GetAnnualIncReportData = "[ODS].[Usp_GetAnnualIncReportData]";
        public static string Usp_GetEmpDueForIncReport = "[ODS].[Usp_GetEmpDueForIncReport]";
        public static string Usp_GetEmployeeDesigByPermDDOId = "[ODS].[GetEmployeeDesigByPermDDOId]";
        public static string Usp_CreateOrderNo = "[ODS].[Usp_CreateOrderNo]";
        public static string Usp_DeleteIncOrderDetails = "[ODS].[Usp_DeleteIncOrderDetails]";
        public static string Usp_GetEmpOrderDetails = "[ODS].[Usp_GetEmpOrderDetails]";
        public static string Usp_GetEmpForIncrement = "[ODS].[Usp_GetEmpForIncrement]";
        public static string Usp_GetEmpForNotIncrement = "[ODS].[Usp_GetEmpForNotIncrement]";
        public static string Usp_InsertEmpRegularIncrement = "[Ods].[Usp_InsertEmpRegularIncrement]";
        public static string Usp_ForwardEmpRegularIncData = "[Ods].[Usp_ForwardEmpRegularIncData]";
        public static string Usp_GetEmpForStopIncrement = "[ODS].[Usp_GetEmpForStopIncrement]";
        public static string Usp_GetEmpForNotStopIncrement = "[ODS].[Usp_GetEmpForNotStopIncrement]";
        public static string Usp_InsertEmployeeStopIncrement = "[Ods].[Usp_InsertEmployeeStopIncrement]";
        public static string Usp_ForwardEmployeeStopIncData = "[Ods].[Usp_ForwardEmployeeStopIncData]";

        #endregion

        #region Recovery of excess payment
        public static string Usp_ExcessPayment_GetRecoveryLoanType = "[Ods].[Usp_GetRecoveryLoanType]";
        public static string Usp_ExcessPayment_GetRecoveryElement = "[ODS].[Usp_GetRecoveryElement]";
        public static string Usp_ExcessPayment_GetAccountHead = "[ODS].[Usp_GetAccountHead]";
        public static string Usp_ExcessPayment_GetRecoveryComponent = "[Ods].[Usp_GetRecoveryComponent]";
        public static string Usp_ExcessPayment_InsertUpdateExcessRecovery = "[ODS].[Usp_InsertUpdateExcessRecovery]";
        public static string Usp_GetExcessRecovery_Details = "[ODS].[Usp_GetExcessRecovery_Details]";
        public static string Usp_ExcessPayment_Usp_InsertUpdateRecComponentAmt = "[ODS].[Usp_InsertUpdateRecComponentAmt]";
        public static string Usp_ExcessPayment_Usp_DeleteComponentAmt = "[Ods].[Usp_DeleteComponentAmt]";
        public static string Usp_ExcessPayment_Usp_GetComponentAmtDetails = "[ODS].[Usp_GetComponentAmtDetails]";
        public static string Usp_EditExcessRecovery_Details = "[ODS].[Usp_EditExcessRecovery_Details]";
        public static string Usp_DeleteExcessRecovery_Details = "[ODS].[Usp_DeleteExcessRecovery_Details]";
        public static string Usp_ForwardExcessRecovery_Details = "[ODS].[Usp_ForwardExcessRecovery_Details]";
        #endregion

        #region Deduction Master/ Deduction Rate Master
        public static string CategoryList = "[ODS].[CategoryList]";
        public static string DeductionDescription = "[ODS].[DeductionDescription]";
        public static string DeductionDescriptionChange = "[ODS].[DeductionDescriptionChange]";
        public static string GetDeductionDefinitionList = "[ODS].[GetDeductionDefinitionList]";
        public static string DeductionDefinitionSubmit = "[ODS].[DeductionDefinitionSubmit]";
        //================== Deduction Rate Master======================
        public static string GetOrgnazationTypeForDeduction = "[ODS].[GetOrgnazationTypeForDeduction]";
        public static string GetCityClassForDeductionRate = "[ODS].[GetCityClassForDeductionRate]";
        public static string GetDeductionCodeAndDescrptionForDeductionRate = "[ODS].[GetDeductionCodeAndDescrptionForDeductionRate]";
        public static string GetStateNameForDeductionMaster = "[ODS].[GetStateNameForDeductionMaster]";
        public static string GetPayCommForDeductionMaster = "[ODS].[GetPayCommForDeductionMaster]";
        public static string GetSlabTypeForDeductionMaster = "[ODS].[GetSlabTypeForDeductionMaster]";
        public static string InsertDeductionRateDetails = "[ODS].[InsertDeductionRateDetails]";
        public static string GetDeductionRateDetails = "[ODS].[GetDeductionRateDetails]";
        public static string GetDeductionRateDetails_SP = "[ODS].[GetDeductionRateDetails_SP]";
        public static string DeleteDeductionRateDetails = "[ODS].[DeleteDeductionRateDetails]";

        #endregion

        #region user login
        public static string GetLoginDetailsForStatus = "[ODS].[GetLoginDetailsForStatus]";
        public static string ChangePasswordOfUser = "[ODS].[ChangePasswordOfUser]";
        public static string EPSRoleActivation = "[ODS].[EPSRoleActivation]";
        public static string GetUserDetails = "[ODS].[GetUserDetails]";
        #endregion
    }
}

