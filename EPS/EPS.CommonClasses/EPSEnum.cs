﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace EPS.CommonClasses
{
    /// <summary>
    /// Common for CRUD Operarions
    /// </summary>
    public static class EPSEnum
    {
        public enum Response
        {
            Error = 0,
            Insert = 1,
            Update = 2,
            Delete = 3,
            AlreadyExist = 4
        }

        public enum RoleSatusEnum
        {
            ASN,
            USN
        }
    }
}
